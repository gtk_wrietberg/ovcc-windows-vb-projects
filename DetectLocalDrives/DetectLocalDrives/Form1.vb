
Public Class Form1

    Private ReadOnly c_A As Integer = Asc("A")

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim oDriveInfo As System.IO.DriveInfo

        TextBox1.Clear()

        For Each oDriveInfo In System.IO.DriveInfo.GetDrives()
            TextBox1.AppendText(oDriveInfo.Name & " (" & oDriveInfo.Name.Chars(0).ToString & ")")
            TextBox1.AppendText(" - ")
            If oDriveInfo.IsReady Then
                TextBox1.AppendText(oDriveInfo.VolumeLabel)
            Else
                TextBox1.AppendText("not ready")
            End If
            TextBox1.AppendText(" - ")
            TextBox1.AppendText(HumanReadableDriveType(oDriveInfo.DriveType))
            TextBox1.AppendText(vbCrLf)
        Next
    End Sub

    Private Function CalculateNoDrivesValue() As Integer
        Dim oDriveInfo As System.IO.DriveInfo
        Dim cDriveLetter As Char
        Dim iNoDriveValue As Integer = 0

        For Each oDriveInfo In System.IO.DriveInfo.GetDrives()
            If oDriveInfo.DriveType = IO.DriveType.Fixed Or 1 Then
                cDriveLetter = oDriveInfo.Name.ToUpper.Chars(0)
                iNoDriveValue += 2 ^ (Asc(cDriveLetter) - c_A)
            End If
        Next

        Return iNoDriveValue
    End Function

    Private Function HumanReadableDriveType(ByVal oDriveType As System.IO.DriveType) As String
        Select Case oDriveType
            Case IO.DriveType.CDRom
                Return "cdrom"
            Case IO.DriveType.Fixed
                Return "hdd"
            Case IO.DriveType.Network
                Return "network"
            Case IO.DriveType.NoRootDirectory
                Return "no root"
            Case IO.DriveType.Ram
                Return "ram"
            Case IO.DriveType.Removable
                Return "removable"
            Case Else
                Return "unknown"
        End Select
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox(CalculateNoDrivesValue)
    End Sub
End Class
