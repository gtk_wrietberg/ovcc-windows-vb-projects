Public Class frmMain
    Private oPrerequisites As Prerequisites

    Private Panels() As Panel
    Private iActiveGroupBox As Integer

    Private Enum GroupBoxes As Integer
        Prerequisites = 0
        StartScreen = 1
        UninstallConfirmation = 2
        LicenseKey = 3
        LicenseKeyValidation = 4
        Deinstallation = 5
        DoneAndRestart = 6
    End Enum

    Private oComputerName As ComputerName
    Private oLocalization As Localization
    Private WithEvents oLicense As LicenseKey


#Region " ClientAreaMove Handling "
    'Private Const WM_NCHITTEST As Integer = &H84
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    'Private Const HTCLIENT As Integer = &H1
    Private Const HTCAPTION As Integer = &H2
    '    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
    '        Select Case m.Msg
    '            Case WM_NCHITTEST
    '                MyBase.WndProc(m)
    '    'If m.Result = HTCLIENT Then m.Result = HTCAPTION
    '                If m.Result.ToInt32 = HTCLIENT Then m.Result = IntPtr.op_Explicit(HTCAPTION) 'Try this in VS.NET 2002/2003 if the latter line of code doesn't do it... thx to Suhas for the tip.
    '            Case Else
    '    'Make sure you pass unhandled messages back to the default message handler.
    '                MyBase.WndProc(m)
    '        End Select
    '    End Sub
#End Region


    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Panels = New Panel() {Me.pnlPrerequisites, Me.pnlStartScreen, Me.pnlUninstallConfirmation, Me.pnlLicenseKey, Me.pnlLicenseKeyValidation, Me.pnlDeinstallation, Me.pnlDoneAndRestart}
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oPrerequisites = New Prerequisites
        oLogger = New Logger

        Me.Width = 640
        Me.Height = 480

        oPrerequisites.CheckWindowsVersion(cPREREQUISITES_NeededWindowsVersion, False)
        oPrerequisites.CheckInternetConnection()
        oPrerequisites.IsUserAdmin()
        oPrerequisites.IsNotInstalled()

        lblWindowsVersion.Text = oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion)


        Me.StartPosition = FormStartPosition.Manual

        Me.Left = (My.Computer.Screen.Bounds.Width - Me.Width) / 2
        Me.Top = (My.Computer.Screen.Bounds.Height - Me.Height) / 2

        Me.BackColor = Color.White
        Me.BackgroundImage = My.Resources.Resources.brushedsteel640x480_blackborder2

        Me.BackgroundImageLayout = ImageLayout.Tile

        Me.pnlBackgroundBorder.Visible = cGUI_GroupBoxBorder_visible

        btnExit.Top = btnContinue.Top
        btnExit.Left = btnContinue.Left

        btnBack.Top = btnCancel.Top
        btnBack.Left = btnCancel.Left

        btnNo.Top = btnCancel.Top
        btnNo.Left = btnCancel.Left

        btnYes.Top = btnCancel.Top
        btnYes.Left = btnCancel.Left

        picClose.Top = 1
        picClose.Left = Me.Width - picClose.Width - 1
        picClose.Visible = True

        oLocalization = New Localization
        oLocalization.LoadLanguageFile()

        PopulateLanguageComboBox()
        ReloadLocalization()


        oLogger.LogFilePath = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder & "\"

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        'Panels
        Dim i As Integer
        For i = 0 To Panels.Length - 1
            Panels(i).Top = 84
            Panels(i).Left = 16

            Panels(i).Visible = False
        Next

        iActiveGroupBox = -1
        NextGroupBox()
    End Sub

    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picClose.Click
        ExitApplication()
    End Sub

    Private Sub ExitApplication()
        Application.Exit()
    End Sub

#Region "Windows version and Internet connectivity checker"
    Private Sub CheckPrerequisites()
        Dim sError As String

        If oPrerequisites.CheckComplete Then
            If oPrerequisites.AllIsOk Then
                NextGroupBox()
            Else
                oLogger.WriteToLog("Prerequisites error", Logger.MESSAGE_TYPE.LOG_ERROR)

                sError = ""
                If Not oPrerequisites.InternetConnectivity Then
                    oLogger.WriteToLog("No internet connection", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorInternetConnection
                End If
                If Not oPrerequisites.CorrectWindowsVersion Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If

                    If oPrerequisites.HigherWindowsVersionsAreAccepted Then
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " < " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= oLocalization.CurrentLanguage.PrerequisitesErrorMinimumWindowsVersion
                    Else
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " != " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= oLocalization.CurrentLanguage.PrerequisitesErrorWindowsVersion
                    End If

                    sError = sError.Replace("%%NEEDED_WINDOWS_VERSION%%", oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion))
                    sError = sError.Replace("%%RUNNING_WINDOWS_VERSION%%", oPrerequisites.WindowsVersion)
                End If
                If Not oPrerequisites.UserIsAdmin Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("No admin privileges", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorAdminRights
                End If
                If oPrerequisites.IsNotInstalled Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("Not installed", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorNotInstalled
                End If

                lblPrerequisitesError.Text = sError

                UpdateButtons(0, 0, 0, 3, 0, 0)
            End If
        Else
            lblPrerequisitesError.Text = "Unknown error 31. Please contact the helpdesk."

            UpdateButtons(0, 0, 0, 3, 0, 0)
        End If
    End Sub
#End Region

#Region "Localization"
    Private Sub PopulateLanguageComboBox()
        '-----------------------------------
        cmbBoxLanguages.Items.Clear()
        Try

            Dim sLanguages() As String
            Dim i As Integer

            sLanguages = oLocalization.GetLanguageNames

            For i = 0 To sLanguages.Length - 1
                If Not sLanguages(i) Is Nothing Then
                    cmbBoxLanguages.Items.Add(sLanguages(i))
                End If
            Next

            cmbBoxLanguages.SelectedIndex = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ReloadLocalization()
        lblTitle.Text = oLocalization.CurrentLanguage.Title

        btnBack.Text = oLocalization.CurrentLanguage.ButtonBack
        btnCancel.Text = oLocalization.CurrentLanguage.ButtonCancel
        btnContinue.Text = oLocalization.CurrentLanguage.ButtonNext
        btnExit.Text = oLocalization.CurrentLanguage.ButtonExit
        btnNo.Text = oLocalization.CurrentLanguage.ButtonNo
        btnYes.Text = oLocalization.CurrentLanguage.ButtonYes

        lblSelectLanguage.Text = oLocalization.CurrentLanguage.WelcomeSelectLanguage

        lblPrerequisitesRestart.Text = oLocalization.CurrentLanguage.PrerequisitesErrorRestartText

        pnlUninstallConfirmation.Text = oLocalization.CurrentLanguage.TermsAndConditionsTitle
        lblUninstallConfirmation.Text = oLocalization.CurrentLanguage.TermsAndConditionsText

        pnlLicenseKey.Text = oLocalization.CurrentLanguage.LicenseValidationTitle
        lblLicenseValidation.Text = oLocalization.CurrentLanguage.LicenseValidationText.Replace("%%LICENSE_CODE_LENGTH%%", cLICENSE_KeyLength.ToString)

        grpbxLicense.Text = oLocalization.CurrentLanguage.LicenseValidationTitle2

        pnlLicenseKeyValidation.Text = oLocalization.CurrentLanguage.LicenseValidationTitle
        lblLicenseValidationCopy.Text = oLocalization.CurrentLanguage.LicenseValidationText.Replace("%%LICENSE_CODE_LENGTH%%", cLICENSE_KeyLength.ToString)
        grpbxLicenseCopy.Text = oLocalization.CurrentLanguage.LicenseValidationTitle2
        lblLicenseProgress.Text = oLocalization.CurrentLanguage.LicenseValidationProgressText

        pnlDeinstallation.Text = oLocalization.CurrentLanguage.DeinstallationProgressTitle
        lblInstallationProgressText.Text = oLocalization.CurrentLanguage.DeinstallationProgressText

        pnlDoneAndRestart.Text = oLocalization.CurrentLanguage.DoneTitle
        lblReboot.Text = oLocalization.CurrentLanguage.DoneRestartText
        lblReboot2.Text = oLocalization.CurrentLanguage.DoneRestartText2
        radRestartNow.Text = oLocalization.CurrentLanguage.DoneRestartNowText
        radRestartLater.Text = oLocalization.CurrentLanguage.DoneRestartLaterText
    End Sub
#End Region

#Region "Button Handling"
    Private Sub UpdateButtons(ByVal iBack As Integer, ByVal iCancel As Integer, ByVal iContinue As Integer, ByVal iExit As Integer, ByVal iYes As Integer, ByVal iNo As Integer)
        btnBack.Enabled = iBack And 1
        btnBack.Visible = iBack And 2

        btnCancel.Enabled = iCancel And 1
        btnCancel.Visible = iCancel And 2

        btnContinue.Enabled = iContinue And 1
        btnContinue.Visible = iContinue And 2

        btnExit.Enabled = iExit And 1
        btnExit.Visible = iExit And 2

        btnYes.Enabled = iYes And 1
        btnYes.Visible = iYes And 2

        btnNo.Enabled = iYes And 1
        btnNo.Visible = iYes And 2
    End Sub
#End Region

#Region "License code validation"
    Private Sub CheckLicenseCode()
        oLogger.WriteToLog("Validating license code")

        oLicense.LicenseCode = txtLicenseCode.Text

        oLogger.WriteToLog("license code: " & oLicense.LicenseCode, , 1)

        oLicense.CheckLicense()
    End Sub
#End Region

#Region "Groupbox handling"
    Private Sub NextGroupBox()
        iActiveGroupBox += 1
        UpdateGroupBox()
    End Sub

    Private Sub PreviousGroupBox()
        iActiveGroupBox -= 1
        'Skip these group box if we are going BACK
        'Windows version and internet connection error dialog
        If iActiveGroupBox = GroupBoxes.StartScreen Then
            iActiveGroupBox = -1
        End If
        'License validation dialog
        If iActiveGroupBox = GroupBoxes.LicenseKeyValidation Then
            iActiveGroupBox = GroupBoxes.LicenseKey
        End If
        UpdateGroupBox()
    End Sub

    Private Sub UpdateGroupBox()
        If iActiveGroupBox >= Panels.Length Then
            iActiveGroupBox = Panels.Length - 1
        End If
        'We handle the case of <0 in the following select case statement
        'If iActiveGroupBox < 0 Then
        '   iActiveGroupBox = 0
        'End If

        oLogger.WriteToLog("Groupbox / action: " & iActiveGroupBox.ToString, Logger.MESSAGE_TYPE.LOG_DEBUG)

        Select Case iActiveGroupBox
            Case GroupBoxes.Prerequisites
                UpdateButtons(0, 0, 0, 0, 0, 0)
                CheckPrerequisites()
            Case GroupBoxes.StartScreen
                UpdateButtons(0, 3, 3, 0, 0, 0)
            Case GroupBoxes.UninstallConfirmation
                UpdateButtons(0, 0, 0, 0, 3, 3)
            Case GroupBoxes.LicenseKey
                UpdateButtons(3, 0, 2, 0, 0, 0)
                picClose.Visible = True

                txtLicenseCode.SelectAll()
            Case GroupBoxes.LicenseKeyValidation
                UpdateButtons(2, 0, 2, 0, 0, 0)
                picClose.Visible = False

                txtLicenseCodeCopy.Text = txtLicenseCode.Text
                tmrLicenseValidationDelay.Enabled = True
            Case GroupBoxes.Deinstallation
                UpdateButtons(0, 0, 0, 0, 0, 0)
                picClose.Visible = False

                gBusyInstalling = True

                ProgressBarMarqueeStart()
                UnpackResourcesInBackground()
            Case GroupBoxes.DoneAndRestart
                UpdateButtons(0, 0, 0, 3, 0, 0)
                radRestartNow.Checked = True
                radRestartLater.Checked = False
            Case Else
                Application.Exit()
        End Select

        If iActiveGroupBox >= 0 And iActiveGroupBox < Panels.Length Then
            Panels(iActiveGroupBox).Visible = True
            pnlBackgroundBorder.Top = Panels(iActiveGroupBox).Top - 1
            pnlBackgroundBorder.Left = Panels(iActiveGroupBox).Left - 1
            pnlBackgroundBorder.Width = Panels(iActiveGroupBox).Width + 2
            pnlBackgroundBorder.Height = Panels(iActiveGroupBox).Height + 2
            pnlBackgroundBorder.Refresh()
        End If

        Dim i As Integer
        For i = 0 To Panels.Length - 1
            If i <> iActiveGroupBox Then
                Panels(i).Visible = False
            End If
        Next
    End Sub
#End Region


#Region "Events"
#Region "DragWindow events"
    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picboxLogo.MouseDown
        Me.picboxLogo.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub lblTitle_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseDown
        Me.lblTitle.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "License Events"
    Private Sub onLicenseIsInvalid(ByVal ReturnCode As Integer, ByVal ReturnMessage As String) Handles oLicense.LicenseIsInvalid
        Dim sError As String = ""

        oLogger.WriteToLog("invalid", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        oLogger.WriteToLog(ReturnCode, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        oLogger.WriteToLog(ReturnMessage, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

        sError = oLocalization.CurrentLanguage.LicenseValidationProgressError
        Select Case ReturnCode
            Case 1
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage1
            Case 2
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage2
            Case -1
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage3
            Case -2
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage4
            Case Else
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessageUnknown
        End Select

        lblLicenseError.Text = sError
        grpboxLicenseError.Visible = True
        txtLicenseCode.SelectAll()

        PreviousGroupBox()
    End Sub

    Private Sub onLicenseValid() Handles oLicense.LicenseIsValid
        oLogger.WriteToLog("valid", , 2)

        'Save key in registry
        oLogger.WriteToLog("saving key to registry", , 1)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone", "LicenseKey", oLicense.LicenseCode)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("Exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog(".message" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

        'Go to next step
        NextGroupBox()
    End Sub
#End Region

#End Region

    Private Sub tmrLicenseValidationDelay_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrLicenseValidationDelay.Tick
        tmrLicenseValidationDelay.Enabled = False

        CheckLicenseCode()
    End Sub
End Class
