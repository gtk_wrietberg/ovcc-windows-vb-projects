<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.picClose = New System.Windows.Forms.PictureBox
        Me.picboxLogo = New System.Windows.Forms.PictureBox
        Me.lblTitle = New System.Windows.Forms.Label
        Me.lblWindowsVersion = New System.Windows.Forms.Label
        Me.pnlBackgroundBorder = New System.Windows.Forms.Panel
        Me.btnBack = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnContinue = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.pnlPrerequisites = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lblPrerequisitesRestart = New System.Windows.Forms.Label
        Me.lblPrerequisitesError = New System.Windows.Forms.Label
        Me.pnlLanguageSelect = New System.Windows.Forms.Panel
        Me.lblSelectLanguage = New System.Windows.Forms.Label
        Me.cmbBoxLanguages = New System.Windows.Forms.ComboBox
        Me.pnlStartScreen = New System.Windows.Forms.Panel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblStartContinue = New System.Windows.Forms.Label
        Me.lblStartWarning = New System.Windows.Forms.Label
        Me.pnlUninstallConfirmation = New System.Windows.Forms.Panel
        Me.lblUninstallConfirmation = New System.Windows.Forms.Label
        Me.btnNo = New System.Windows.Forms.Button
        Me.btnYes = New System.Windows.Forms.Button
        Me.pnlLicenseKey = New System.Windows.Forms.Panel
        Me.grpboxLicenseError = New System.Windows.Forms.GroupBox
        Me.lblLicenseError = New System.Windows.Forms.Label
        Me.grpbxLicense = New System.Windows.Forms.GroupBox
        Me.txtLicenseCode = New System.Windows.Forms.TextBox
        Me.lblLicenseValidation = New System.Windows.Forms.Label
        Me.pnlLicenseKeyValidation = New System.Windows.Forms.Panel
        Me.grpbxLicenseCopy = New System.Windows.Forms.GroupBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblLicenseProgress = New System.Windows.Forms.Label
        Me.txtLicenseCodeCopy = New System.Windows.Forms.TextBox
        Me.lblLicenseValidationCopy = New System.Windows.Forms.Label
        Me.tmrLicenseValidationDelay = New System.Windows.Forms.Timer(Me.components)
        Me.pnlDeinstallation = New System.Windows.Forms.Panel
        Me.lblInstallationProgressStep = New System.Windows.Forms.Label
        Me.lblInstallationProgressText = New System.Windows.Forms.Label
        Me.progressMarquee = New System.Windows.Forms.ProgressBar
        Me.progressInstallation = New System.Windows.Forms.ProgressBar
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlPrerequisites.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.pnlLanguageSelect.SuspendLayout()
        Me.pnlStartScreen.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.pnlUninstallConfirmation.SuspendLayout()
        Me.pnlLicenseKey.SuspendLayout()
        Me.grpboxLicenseError.SuspendLayout()
        Me.grpbxLicense.SuspendLayout()
        Me.pnlLicenseKeyValidation.SuspendLayout()
        Me.grpbxLicenseCopy.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlDeinstallation.SuspendLayout()
        Me.SuspendLayout()
        '
        'picClose
        '
        Me.picClose.BackColor = System.Drawing.Color.Transparent
        Me.picClose.Image = Global.LobbyPCStandaloneUninstallation.My.Resources.Resources.close_button
        Me.picClose.Location = New System.Drawing.Point(614, 1)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(25, 25)
        Me.picClose.TabIndex = 0
        Me.picClose.TabStop = False
        '
        'picboxLogo
        '
        Me.picboxLogo.BackColor = System.Drawing.Color.Transparent
        Me.picboxLogo.Image = Global.LobbyPCStandaloneUninstallation.My.Resources.Resources.iBAHN_logo_01
        Me.picboxLogo.Location = New System.Drawing.Point(15, 12)
        Me.picboxLogo.Name = "picboxLogo"
        Me.picboxLogo.Size = New System.Drawing.Size(120, 68)
        Me.picboxLogo.TabIndex = 21
        Me.picboxLogo.TabStop = False
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(133, 12)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(492, 68)
        Me.lblTitle.TabIndex = 22
        Me.lblTitle.Text = "LobbyPC uninstallation"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblWindowsVersion
        '
        Me.lblWindowsVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblWindowsVersion.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWindowsVersion.Location = New System.Drawing.Point(440, 62)
        Me.lblWindowsVersion.Name = "lblWindowsVersion"
        Me.lblWindowsVersion.Size = New System.Drawing.Size(184, 18)
        Me.lblWindowsVersion.TabIndex = 25
        Me.lblWindowsVersion.Text = "WindowsVersion"
        Me.lblWindowsVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlBackgroundBorder
        '
        Me.pnlBackgroundBorder.BackColor = System.Drawing.Color.Transparent
        Me.pnlBackgroundBorder.Location = New System.Drawing.Point(14, 616)
        Me.pnlBackgroundBorder.Name = "pnlBackgroundBorder"
        Me.pnlBackgroundBorder.Size = New System.Drawing.Size(124, 31)
        Me.pnlBackgroundBorder.TabIndex = 26
        '
        'btnBack
        '
        Me.btnBack.BackColor = System.Drawing.Color.Transparent
        Me.btnBack.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnBack.Location = New System.Drawing.Point(14, 495)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(197, 36)
        Me.btnBack.TabIndex = 30
        Me.btnBack.Text = "BACK"
        Me.btnBack.UseVisualStyleBackColor = False
        Me.btnBack.Visible = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.Enabled = False
        Me.btnExit.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnExit.Location = New System.Drawing.Point(427, 495)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(197, 36)
        Me.btnExit.TabIndex = 29
        Me.btnExit.Text = "FINISH"
        Me.btnExit.UseVisualStyleBackColor = False
        Me.btnExit.Visible = False
        '
        'btnContinue
        '
        Me.btnContinue.BackColor = System.Drawing.Color.Transparent
        Me.btnContinue.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContinue.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnContinue.Location = New System.Drawing.Point(428, 433)
        Me.btnContinue.Name = "btnContinue"
        Me.btnContinue.Size = New System.Drawing.Size(197, 36)
        Me.btnContinue.TabIndex = 27
        Me.btnContinue.Text = "NEXT"
        Me.btnContinue.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnCancel.Location = New System.Drawing.Point(15, 433)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(197, 36)
        Me.btnCancel.TabIndex = 28
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'pnlPrerequisites
        '
        Me.pnlPrerequisites.BackColor = System.Drawing.Color.Transparent
        Me.pnlPrerequisites.Controls.Add(Me.Panel3)
        Me.pnlPrerequisites.Location = New System.Drawing.Point(104, 685)
        Me.pnlPrerequisites.Name = "pnlPrerequisites"
        Me.pnlPrerequisites.Size = New System.Drawing.Size(608, 342)
        Me.pnlPrerequisites.TabIndex = 31
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblPrerequisitesRestart)
        Me.Panel3.Controls.Add(Me.lblPrerequisitesError)
        Me.Panel3.Location = New System.Drawing.Point(5, 80)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(598, 182)
        Me.Panel3.TabIndex = 1
        '
        'lblPrerequisitesRestart
        '
        Me.lblPrerequisitesRestart.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrerequisitesRestart.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.lblPrerequisitesRestart.Location = New System.Drawing.Point(12, 159)
        Me.lblPrerequisitesRestart.Name = "lblPrerequisitesRestart"
        Me.lblPrerequisitesRestart.Size = New System.Drawing.Size(586, 19)
        Me.lblPrerequisitesRestart.TabIndex = 1
        Me.lblPrerequisitesRestart.Text = "Select FINISH to exit the installation and start again."
        Me.lblPrerequisitesRestart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPrerequisitesError
        '
        Me.lblPrerequisitesError.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrerequisitesError.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.lblPrerequisitesError.Location = New System.Drawing.Point(12, 7)
        Me.lblPrerequisitesError.Name = "lblPrerequisitesError"
        Me.lblPrerequisitesError.Size = New System.Drawing.Size(586, 142)
        Me.lblPrerequisitesError.TabIndex = 0
        Me.lblPrerequisitesError.Text = "Here come the error messages" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lblPrerequisitesError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLanguageSelect
        '
        Me.pnlLanguageSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLanguageSelect.Controls.Add(Me.lblSelectLanguage)
        Me.pnlLanguageSelect.Controls.Add(Me.cmbBoxLanguages)
        Me.pnlLanguageSelect.Location = New System.Drawing.Point(44, 803)
        Me.pnlLanguageSelect.Name = "pnlLanguageSelect"
        Me.pnlLanguageSelect.Size = New System.Drawing.Size(597, 36)
        Me.pnlLanguageSelect.TabIndex = 32
        '
        'lblSelectLanguage
        '
        Me.lblSelectLanguage.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectLanguage.Location = New System.Drawing.Point(10, 6)
        Me.lblSelectLanguage.Name = "lblSelectLanguage"
        Me.lblSelectLanguage.Size = New System.Drawing.Size(305, 20)
        Me.lblSelectLanguage.TabIndex = 14
        Me.lblSelectLanguage.Text = "Please select your language for the installation"
        Me.lblSelectLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbBoxLanguages
        '
        Me.cmbBoxLanguages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBoxLanguages.Enabled = False
        Me.cmbBoxLanguages.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBoxLanguages.FormattingEnabled = True
        Me.cmbBoxLanguages.Location = New System.Drawing.Point(365, 6)
        Me.cmbBoxLanguages.Name = "cmbBoxLanguages"
        Me.cmbBoxLanguages.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbBoxLanguages.Size = New System.Drawing.Size(227, 22)
        Me.cmbBoxLanguages.TabIndex = 13
        Me.cmbBoxLanguages.TabStop = False
        '
        'pnlStartScreen
        '
        Me.pnlStartScreen.BackColor = System.Drawing.Color.Transparent
        Me.pnlStartScreen.Controls.Add(Me.GroupBox2)
        Me.pnlStartScreen.Location = New System.Drawing.Point(788, 547)
        Me.pnlStartScreen.Name = "pnlStartScreen"
        Me.pnlStartScreen.Size = New System.Drawing.Size(608, 342)
        Me.pnlStartScreen.TabIndex = 33
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblStartContinue)
        Me.GroupBox2.Controls.Add(Me.lblStartWarning)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 123)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(598, 96)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'lblStartContinue
        '
        Me.lblStartContinue.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartContinue.ForeColor = System.Drawing.Color.Black
        Me.lblStartContinue.Location = New System.Drawing.Point(8, 74)
        Me.lblStartContinue.Name = "lblStartContinue"
        Me.lblStartContinue.Size = New System.Drawing.Size(586, 19)
        Me.lblStartContinue.TabIndex = 1
        Me.lblStartContinue.Text = "Select Next to start the installation."
        Me.lblStartContinue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStartWarning
        '
        Me.lblStartWarning.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartWarning.ForeColor = System.Drawing.Color.Black
        Me.lblStartWarning.Location = New System.Drawing.Point(6, 15)
        Me.lblStartWarning.Name = "lblStartWarning"
        Me.lblStartWarning.Size = New System.Drawing.Size(586, 46)
        Me.lblStartWarning.TabIndex = 0
        Me.lblStartWarning.Text = "Before starting the installation, it is highly recommended that you close all oth" & _
            "er applications."
        Me.lblStartWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlUninstallConfirmation
        '
        Me.pnlUninstallConfirmation.BackColor = System.Drawing.Color.Transparent
        Me.pnlUninstallConfirmation.Controls.Add(Me.lblUninstallConfirmation)
        Me.pnlUninstallConfirmation.Location = New System.Drawing.Point(672, 12)
        Me.pnlUninstallConfirmation.Name = "pnlUninstallConfirmation"
        Me.pnlUninstallConfirmation.Size = New System.Drawing.Size(608, 342)
        Me.pnlUninstallConfirmation.TabIndex = 34
        '
        'lblUninstallConfirmation
        '
        Me.lblUninstallConfirmation.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUninstallConfirmation.ForeColor = System.Drawing.Color.DarkRed
        Me.lblUninstallConfirmation.Location = New System.Drawing.Point(9, 15)
        Me.lblUninstallConfirmation.Name = "lblUninstallConfirmation"
        Me.lblUninstallConfirmation.Size = New System.Drawing.Size(596, 309)
        Me.lblUninstallConfirmation.TabIndex = 5
        Me.lblUninstallConfirmation.Text = "This application will remove the LobbyPC product from this PC entirely. Are you s" & _
            "ure you want to continue?"
        Me.lblUninstallConfirmation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnNo
        '
        Me.btnNo.BackColor = System.Drawing.Color.Transparent
        Me.btnNo.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNo.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnNo.Location = New System.Drawing.Point(12, 537)
        Me.btnNo.Name = "btnNo"
        Me.btnNo.Size = New System.Drawing.Size(197, 36)
        Me.btnNo.TabIndex = 35
        Me.btnNo.Text = "No"
        Me.btnNo.UseVisualStyleBackColor = False
        Me.btnNo.Visible = False
        '
        'btnYes
        '
        Me.btnYes.BackColor = System.Drawing.Color.Transparent
        Me.btnYes.Enabled = False
        Me.btnYes.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYes.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnYes.Location = New System.Drawing.Point(427, 537)
        Me.btnYes.Name = "btnYes"
        Me.btnYes.Size = New System.Drawing.Size(197, 36)
        Me.btnYes.TabIndex = 36
        Me.btnYes.Text = "Yes"
        Me.btnYes.UseVisualStyleBackColor = False
        Me.btnYes.Visible = False
        '
        'pnlLicenseKey
        '
        Me.pnlLicenseKey.BackColor = System.Drawing.Color.Transparent
        Me.pnlLicenseKey.Controls.Add(Me.grpboxLicenseError)
        Me.pnlLicenseKey.Controls.Add(Me.grpbxLicense)
        Me.pnlLicenseKey.Location = New System.Drawing.Point(669, 371)
        Me.pnlLicenseKey.Name = "pnlLicenseKey"
        Me.pnlLicenseKey.Size = New System.Drawing.Size(608, 342)
        Me.pnlLicenseKey.TabIndex = 37
        '
        'grpboxLicenseError
        '
        Me.grpboxLicenseError.Controls.Add(Me.lblLicenseError)
        Me.grpboxLicenseError.Location = New System.Drawing.Point(6, 224)
        Me.grpboxLicenseError.Name = "grpboxLicenseError"
        Me.grpboxLicenseError.Size = New System.Drawing.Size(598, 48)
        Me.grpboxLicenseError.TabIndex = 10
        Me.grpboxLicenseError.TabStop = False
        Me.grpboxLicenseError.Visible = False
        '
        'lblLicenseError
        '
        Me.lblLicenseError.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseError.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLicenseError.Location = New System.Drawing.Point(5, 7)
        Me.lblLicenseError.Name = "lblLicenseError"
        Me.lblLicenseError.Size = New System.Drawing.Size(587, 38)
        Me.lblLicenseError.TabIndex = 0
        Me.lblLicenseError.Text = "Validating license code failed: lblLicenseError lblLicenseError lblLicenseError l" & _
            "blLicenseError lblLicenseError lblLicenseError lblLqgicenseError"
        Me.lblLicenseError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grpbxLicense
        '
        Me.grpbxLicense.Controls.Add(Me.txtLicenseCode)
        Me.grpbxLicense.Controls.Add(Me.lblLicenseValidation)
        Me.grpbxLicense.Location = New System.Drawing.Point(6, 119)
        Me.grpbxLicense.Name = "grpbxLicense"
        Me.grpbxLicense.Size = New System.Drawing.Size(598, 104)
        Me.grpbxLicense.TabIndex = 9
        Me.grpbxLicense.TabStop = False
        Me.grpbxLicense.Text = "License code"
        '
        'txtLicenseCode
        '
        Me.txtLicenseCode.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.txtLicenseCode.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLicenseCode.Location = New System.Drawing.Point(8, 65)
        Me.txtLicenseCode.MaxLength = 32
        Me.txtLicenseCode.Name = "txtLicenseCode"
        Me.txtLicenseCode.Size = New System.Drawing.Size(584, 32)
        Me.txtLicenseCode.TabIndex = 10
        Me.txtLicenseCode.Text = "AAAAAAAA-BBBBBBBB-CCCCCCCC-DDDDDDDD"
        Me.txtLicenseCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblLicenseValidation
        '
        Me.lblLicenseValidation.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseValidation.Location = New System.Drawing.Point(10, 20)
        Me.lblLicenseValidation.Name = "lblLicenseValidation"
        Me.lblLicenseValidation.Size = New System.Drawing.Size(582, 32)
        Me.lblLicenseValidation.TabIndex = 8
        Me.lblLicenseValidation.Text = "In the box below, type your 32-character license key, which you should have recei" & _
            "ved from your Account Executive."
        '
        'pnlLicenseKeyValidation
        '
        Me.pnlLicenseKeyValidation.BackColor = System.Drawing.Color.Transparent
        Me.pnlLicenseKeyValidation.Controls.Add(Me.grpbxLicenseCopy)
        Me.pnlLicenseKeyValidation.Location = New System.Drawing.Point(759, 906)
        Me.pnlLicenseKeyValidation.Name = "pnlLicenseKeyValidation"
        Me.pnlLicenseKeyValidation.Size = New System.Drawing.Size(608, 342)
        Me.pnlLicenseKeyValidation.TabIndex = 38
        Me.pnlLicenseKeyValidation.UseWaitCursor = True
        '
        'grpbxLicenseCopy
        '
        Me.grpbxLicenseCopy.Controls.Add(Me.Panel1)
        Me.grpbxLicenseCopy.Controls.Add(Me.txtLicenseCodeCopy)
        Me.grpbxLicenseCopy.Controls.Add(Me.lblLicenseValidationCopy)
        Me.grpbxLicenseCopy.Location = New System.Drawing.Point(6, 119)
        Me.grpbxLicenseCopy.Name = "grpbxLicenseCopy"
        Me.grpbxLicenseCopy.Size = New System.Drawing.Size(598, 104)
        Me.grpbxLicenseCopy.TabIndex = 10
        Me.grpbxLicenseCopy.TabStop = False
        Me.grpbxLicenseCopy.Text = "License code"
        Me.grpbxLicenseCopy.UseWaitCursor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(186, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblLicenseProgress)
        Me.Panel1.Location = New System.Drawing.Point(160, 28)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(278, 48)
        Me.Panel1.TabIndex = 11
        Me.Panel1.UseWaitCursor = True
        '
        'lblLicenseProgress
        '
        Me.lblLicenseProgress.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseProgress.Location = New System.Drawing.Point(0, 0)
        Me.lblLicenseProgress.Name = "lblLicenseProgress"
        Me.lblLicenseProgress.Size = New System.Drawing.Size(275, 45)
        Me.lblLicenseProgress.TabIndex = 10
        Me.lblLicenseProgress.Text = "Validating license code, please wait..."
        Me.lblLicenseProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblLicenseProgress.UseWaitCursor = True
        '
        'txtLicenseCodeCopy
        '
        Me.txtLicenseCodeCopy.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.txtLicenseCodeCopy.Enabled = False
        Me.txtLicenseCodeCopy.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLicenseCodeCopy.Location = New System.Drawing.Point(8, 65)
        Me.txtLicenseCodeCopy.MaxLength = 32
        Me.txtLicenseCodeCopy.Name = "txtLicenseCodeCopy"
        Me.txtLicenseCodeCopy.Size = New System.Drawing.Size(584, 32)
        Me.txtLicenseCodeCopy.TabIndex = 10
        Me.txtLicenseCodeCopy.Text = "AAAAAAAA-BBBBBBBB-CCCCCCCC-DDDDDDDD"
        Me.txtLicenseCodeCopy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtLicenseCodeCopy.UseWaitCursor = True
        '
        'lblLicenseValidationCopy
        '
        Me.lblLicenseValidationCopy.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseValidationCopy.Location = New System.Drawing.Point(10, 20)
        Me.lblLicenseValidationCopy.Name = "lblLicenseValidationCopy"
        Me.lblLicenseValidationCopy.Size = New System.Drawing.Size(582, 32)
        Me.lblLicenseValidationCopy.TabIndex = 8
        Me.lblLicenseValidationCopy.Text = "Enter your license validation code. This is a unique sixteen-digit code, which ca" & _
            "n be found in your installation pack."
        Me.lblLicenseValidationCopy.UseWaitCursor = True
        '
        'tmrLicenseValidationDelay
        '
        Me.tmrLicenseValidationDelay.Interval = 500
        '
        'pnlDeinstallation
        '
        Me.pnlDeinstallation.BackColor = System.Drawing.Color.Transparent
        Me.pnlDeinstallation.Controls.Add(Me.lblInstallationProgressStep)
        Me.pnlDeinstallation.Controls.Add(Me.lblInstallationProgressText)
        Me.pnlDeinstallation.Controls.Add(Me.progressMarquee)
        Me.pnlDeinstallation.Controls.Add(Me.progressInstallation)
        Me.pnlDeinstallation.Location = New System.Drawing.Point(245, 555)
        Me.pnlDeinstallation.Name = "pnlDeinstallation"
        Me.pnlDeinstallation.Size = New System.Drawing.Size(608, 385)
        Me.pnlDeinstallation.TabIndex = 39
        Me.pnlDeinstallation.UseWaitCursor = True
        '
        'lblInstallationProgressStep
        '
        Me.lblInstallationProgressStep.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallationProgressStep.Location = New System.Drawing.Point(3, 169)
        Me.lblInstallationProgressStep.Name = "lblInstallationProgressStep"
        Me.lblInstallationProgressStep.Size = New System.Drawing.Size(602, 45)
        Me.lblInstallationProgressStep.TabIndex = 7
        Me.lblInstallationProgressStep.Text = "0 / 0"
        Me.lblInstallationProgressStep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblInstallationProgressStep.UseWaitCursor = True
        Me.lblInstallationProgressStep.Visible = False
        '
        'lblInstallationProgressText
        '
        Me.lblInstallationProgressText.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallationProgressText.Location = New System.Drawing.Point(6, 58)
        Me.lblInstallationProgressText.Name = "lblInstallationProgressText"
        Me.lblInstallationProgressText.Size = New System.Drawing.Size(598, 45)
        Me.lblInstallationProgressText.TabIndex = 6
        Me.lblInstallationProgressText.Text = "InstallationProgressText"
        Me.lblInstallationProgressText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblInstallationProgressText.UseWaitCursor = True
        '
        'progressMarquee
        '
        Me.progressMarquee.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.progressMarquee.ForeColor = System.Drawing.Color.Firebrick
        Me.progressMarquee.Location = New System.Drawing.Point(5, 313)
        Me.progressMarquee.Name = "progressMarquee"
        Me.progressMarquee.Size = New System.Drawing.Size(598, 30)
        Me.progressMarquee.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressMarquee.TabIndex = 5
        Me.progressMarquee.UseWaitCursor = True
        '
        'progressInstallation
        '
        Me.progressInstallation.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.progressInstallation.ForeColor = System.Drawing.Color.Firebrick
        Me.progressInstallation.Location = New System.Drawing.Point(5, 350)
        Me.progressInstallation.Name = "progressInstallation"
        Me.progressInstallation.Size = New System.Drawing.Size(598, 30)
        Me.progressInstallation.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressInstallation.TabIndex = 3
        Me.progressInstallation.UseWaitCursor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1292, 1036)
        Me.Controls.Add(Me.pnlDeinstallation)
        Me.Controls.Add(Me.pnlLicenseKeyValidation)
        Me.Controls.Add(Me.pnlLicenseKey)
        Me.Controls.Add(Me.btnYes)
        Me.Controls.Add(Me.btnNo)
        Me.Controls.Add(Me.pnlUninstallConfirmation)
        Me.Controls.Add(Me.pnlStartScreen)
        Me.Controls.Add(Me.pnlPrerequisites)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnContinue)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.pnlBackgroundBorder)
        Me.Controls.Add(Me.lblWindowsVersion)
        Me.Controls.Add(Me.picClose)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.picboxLogo)
        Me.Controls.Add(Me.pnlLanguageSelect)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMain"
        Me.Text = "LobbyPC uninstallation"
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlPrerequisites.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.pnlLanguageSelect.ResumeLayout(False)
        Me.pnlStartScreen.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.pnlUninstallConfirmation.ResumeLayout(False)
        Me.pnlLicenseKey.ResumeLayout(False)
        Me.grpboxLicenseError.ResumeLayout(False)
        Me.grpbxLicense.ResumeLayout(False)
        Me.grpbxLicense.PerformLayout()
        Me.pnlLicenseKeyValidation.ResumeLayout(False)
        Me.grpbxLicenseCopy.ResumeLayout(False)
        Me.grpbxLicenseCopy.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.pnlDeinstallation.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picClose As System.Windows.Forms.PictureBox
    Friend WithEvents picboxLogo As System.Windows.Forms.PictureBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblWindowsVersion As System.Windows.Forms.Label
    Friend WithEvents pnlBackgroundBorder As System.Windows.Forms.Panel
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnContinue As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents pnlPrerequisites As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblPrerequisitesRestart As System.Windows.Forms.Label
    Friend WithEvents lblPrerequisitesError As System.Windows.Forms.Label
    Friend WithEvents pnlLanguageSelect As System.Windows.Forms.Panel
    Friend WithEvents lblSelectLanguage As System.Windows.Forms.Label
    Friend WithEvents cmbBoxLanguages As System.Windows.Forms.ComboBox
    Friend WithEvents pnlStartScreen As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblStartContinue As System.Windows.Forms.Label
    Friend WithEvents lblStartWarning As System.Windows.Forms.Label
    Friend WithEvents pnlUninstallConfirmation As System.Windows.Forms.Panel
    Friend WithEvents lblUninstallConfirmation As System.Windows.Forms.Label
    Friend WithEvents btnNo As System.Windows.Forms.Button
    Friend WithEvents btnYes As System.Windows.Forms.Button
    Friend WithEvents pnlLicenseKey As System.Windows.Forms.Panel
    Friend WithEvents grpboxLicenseError As System.Windows.Forms.GroupBox
    Friend WithEvents lblLicenseError As System.Windows.Forms.Label
    Friend WithEvents grpbxLicense As System.Windows.Forms.GroupBox
    Friend WithEvents txtLicenseCode As System.Windows.Forms.TextBox
    Friend WithEvents lblLicenseValidation As System.Windows.Forms.Label
    Friend WithEvents pnlLicenseKeyValidation As System.Windows.Forms.Panel
    Friend WithEvents grpbxLicenseCopy As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblLicenseProgress As System.Windows.Forms.Label
    Friend WithEvents txtLicenseCodeCopy As System.Windows.Forms.TextBox
    Friend WithEvents lblLicenseValidationCopy As System.Windows.Forms.Label
    Friend WithEvents tmrLicenseValidationDelay As System.Windows.Forms.Timer
    Friend WithEvents pnlDeinstallation As System.Windows.Forms.Panel
    Friend WithEvents lblInstallationProgressStep As System.Windows.Forms.Label
    Friend WithEvents lblInstallationProgressText As System.Windows.Forms.Label
    Friend WithEvents progressMarquee As System.Windows.Forms.ProgressBar
    Friend WithEvents progressInstallation As System.Windows.Forms.ProgressBar

End Class
