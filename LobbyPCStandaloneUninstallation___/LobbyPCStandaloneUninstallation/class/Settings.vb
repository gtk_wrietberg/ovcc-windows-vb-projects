Public Class Settings
    Private mInstaller_pcAnywhere_Path As String
    Private mInstaller_Altiris_Path As String
    Private mInstaller_iBAHNUpdate_Path As String
    Private mInstaller_SiteKiosk_Path As String
    Private mInstaller_AutoStart_Path As String
    Private mInstaller_CreateShortcut_Path As String
    Private mInstaller_PostSiteKioskUpdating_Path As String
    Private mInstaller_PcHasRebooted_Path As String
    Private mInstaller_LobbyPCAgent_Path As String
    Private mInstaller_LobbyPCWatchdog_Path As String

    Public Sub New()

    End Sub

#Region "Properties"
    Public Property Path_pcAnywhere() As String
        Get
            Return mInstaller_pcAnywhere_Path
        End Get
        Set(ByVal value As String)
            mInstaller_pcAnywhere_Path = value
        End Set
    End Property

    Public Property Path_Altiris() As String
        Get
            Return mInstaller_Altiris_Path
        End Get
        Set(ByVal value As String)
            mInstaller_Altiris_Path = value
        End Set
    End Property

    Public Property Path_iBAHNUpdate() As String
        Get
            Return mInstaller_iBAHNUpdate_Path
        End Get
        Set(ByVal value As String)
            mInstaller_iBAHNUpdate_Path = value
        End Set
    End Property

    Public Property Path_SiteKiosk() As String
        Get
            Return mInstaller_SiteKiosk_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SiteKiosk_Path = value
        End Set
    End Property

    Public Property Path_AutoStart() As String
        Get
            Return mInstaller_AutoStart_Path
        End Get
        Set(ByVal value As String)
            mInstaller_AutoStart_Path = value
        End Set
    End Property

    Public Property Path_CreateShortcut() As String
        Get
            Return mInstaller_CreateShortcut_Path
        End Get
        Set(ByVal value As String)
            mInstaller_CreateShortcut_Path = value
        End Set
    End Property

    Public Property Path_PostSiteKioskUpdating() As String
        Get
            Return mInstaller_PostSiteKioskUpdating_Path
        End Get
        Set(ByVal value As String)
            mInstaller_PostSiteKioskUpdating_Path = value
        End Set
    End Property

    Public Property Path_PcHasRebooted() As String
        Get
            Return mInstaller_PcHasRebooted_Path
        End Get
        Set(ByVal value As String)
            mInstaller_PcHasRebooted_Path = value
        End Set
    End Property

    Public Property Path_LobbyPCAgent() As String
        Get
            Return mInstaller_LobbyPCAgent_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LobbyPCAgent_Path = value
        End Set
    End Property

    Public Property Path_LobbyPCWatchdog() As String
        Get
            Return mInstaller_LobbyPCWatchdog_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LobbyPCWatchdog_Path = value
        End Set
    End Property
#End Region
End Class
