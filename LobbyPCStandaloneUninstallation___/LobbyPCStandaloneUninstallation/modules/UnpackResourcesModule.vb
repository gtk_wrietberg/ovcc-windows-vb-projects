Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim oMyResources As New MyResources

        oMyResources.DestinationFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder

        oLogger.WriteToLog("Unpacking Altiris package", , 1)
        If oMyResources.UnpackResource("Altiris") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_Altiris = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking iBAHNUpdate package", , 1)
        If oMyResources.UnpackResource("iBAHNUpdate") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_iBAHNUpdate = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking LobbyPCAgent package", , 1)
        If oMyResources.UnpackResource("LobbyPCAgent") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_LobbyPCAgent = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking LobbyPCWatchdog package", , 1)
        If oMyResources.UnpackResource("LobbyPCWatchdog") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_LobbyPCWatchdog = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking pcAnywhere package", , 1)
        If oMyResources.UnpackResource("pcAnywhere", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_pcAnywhere = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking shortcut creator", , 1)
        If oMyResources.UnpackResource("CreateShortcut") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_CreateShortcut = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking PostSiteKioskUpdating", , 1)
        If oMyResources.UnpackResource("LobbyPCStandalonePostSiteKiosk", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_PostSiteKioskUpdating = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking PcHasRebooted", , 1)
        If oMyResources.UnpackResource("PcHasRebooted", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_PcHasRebooted = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking SiteKiosk package", , 1)
        If oMyResources.UnpackResource("SiteKiosk7", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_SiteKiosk = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        'This one goes in the standard iBAHN folder
        oMyResources.DestinationFolder = cPATHS_iBAHNProgramFilesFolder

        oLogger.WriteToLog("Unpacking LobbyPCAutoStart", , 1)
        If oMyResources.UnpackResource("LobbyPCAutoStart", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_AutoStart = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If
    End Sub
End Module
