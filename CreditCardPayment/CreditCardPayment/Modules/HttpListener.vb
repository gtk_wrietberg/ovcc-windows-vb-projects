Module HttpListener
    Private ReadOnly responseString As String = "<HTML><BODY> Hello world!</BODY></HTML>"

    Private Sub DoListenStuff()
        'Create a listener.
        Dim listener As New System.Net.HttpListener
        Dim context As System.Net.HttpListenerContext
        Dim request As System.Net.HttpListenerRequest
        Dim response As System.Net.HttpListenerResponse

        listener.Prefixes.Add("http://localhost:8181/HttpListenerApp/")
        listener.Start()

        context = listener.GetContext()
        request = context.Request
        response = context.Response

        Dim qString As New System.Collections.Specialized.NameValueCollection
        Dim str As String = ""
        qString = request.QueryString

        str = str & "Credit card name  : " & qString.Item("cname") & vbCrLf
        str = str & "Credit card number: " & qString.Item("cnum") & vbCrLf
        str = str & "Credit card date  : " & qString.Item("cdate") & vbCrLf
        str = str & "Credit card cvc   : " & qString.Item("cvc")


        '?cnum=510510510555&cdate=1209&cvc=1234

        Dim buffer As Byte()

        buffer = System.Text.Encoding.UTF8.GetBytes(responseString)

        response.ContentLength64 = buffer.Length

        Dim s As System.IO.Stream = response.OutputStream
        s.Write(buffer, 0, buffer.Length)
        s.Close()

        listener.Stop()
    End Sub
End Module
