Module Settings
    Public Sub LoadSettings()

    End Sub

    Private Function FindActiveSiteKioskConfigFile() As String
        Return RegValue(Microsoft.Win32.RegistryHive.LocalMachine, "SOFTWARE\Provisio\SiteKiosk", "LastCfg")
    End Function

    Private Function FindActiveStartPageFromSiteKioskConfigFile(ByVal sSkcfgFile As String) As String
        Try
            Dim xmlDoc As Xml.XmlDocument
            Dim xmlRootNode As Xml.XmlNode
            Dim xmlNodes As Xml.XmlNodeList
            Dim xmlNode As Xml.XmlNode
            Dim i As Integer, sRet As String

            xmlDoc = New Xml.XmlDocument
            xmlDoc.Load(sSkcfgFile)

            xmlNodes = xmlDoc.GetElementsByTagName("startpageconfig")
            If xmlNodes.Count > 0 Then
                For i = 0 To xmlNodes(0).ChildNodes.Count - 1
                    If xmlNodes(0).ChildNodes.Item(i).Name.Equals("startpage") Then
                        sRet = xmlNodes(0).ChildNodes.Item(i).InnerText
                    End If
                Next
            End If

            Return sRet.Replace("file://", "")
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Module
