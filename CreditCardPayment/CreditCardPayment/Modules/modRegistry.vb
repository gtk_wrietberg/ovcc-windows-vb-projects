Imports Microsoft.Win32

Module modRegistry
    Public Function RegValue(ByVal Hive As RegistryHive, ByVal Key As String, ByVal ValueName As String, Optional ByRef ErrInfo As String = "") As String
        Dim objParent As RegistryKey
        Dim objSubkey As RegistryKey
        Dim sAns As String = ""

        Select Case Hive
            Case RegistryHive.ClassesRoot
                objParent = Registry.ClassesRoot
            Case RegistryHive.CurrentConfig
                objParent = Registry.CurrentConfig
            Case RegistryHive.CurrentUser
                objParent = Registry.CurrentUser
            Case RegistryHive.LocalMachine
                objParent = Registry.LocalMachine
            Case RegistryHive.PerformanceData
                objParent = Registry.PerformanceData
            Case RegistryHive.Users
                objParent = Registry.Users
            Case Else
                objParent = Registry.LocalMachine
        End Select

        Try
            objSubkey = objParent.OpenSubKey(Key)
            If Not objSubkey Is Nothing Then
                sAns = (objSubkey.GetValue(ValueName))
            End If
        Catch ex As Exception
            ErrInfo = ex.Message
        Finally
            If ErrInfo = "" And sAns = "" Then
                ErrInfo = "No value found for requested registry key"
            End If
        End Try

        Return sAns
    End Function
End Module
