Public Class frmMain
    Private WithEvents oCreditCard As CreditCardAuthentication
    Private appSettings As Settings

    Private iFadeCount As Integer = 0
    Private iFadeStep As Integer = 5
    Private ReadOnly iFadeCountMax As Integer = 85
    Private ReadOnly iFadeCountMin As Integer = 0

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = True
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sArg As String, iArg As Integer
        Dim bKillAll As Boolean = False

        gAppStartDate = Now

        For iArg = 0 To My.Application.CommandLineArgs.Count - 1
            sArg = My.Application.CommandLineArgs.Item(iArg)

            If sArg.IndexOf("--kill") >= 0 Then
                bKillAll = True
            End If
        Next

        If bKillAll Then
            KillExistingInstances(True)

            End
        Else
            KillExistingInstances(False)
        End If

        picLogo.Width = picLogo.Image.Width
        picLogo.Height = picLogo.Image.Height

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = "v" & myBuildInfo.FileMajorPart & "." & myBuildInfo.FileMinorPart

        WriteLogEntry("----------")
        WriteLogEntry(Application.ProductName & " v" & myBuildInfo.FileVersion)

        Me.Width = picLogo.Width + 2 * picLogo.Left
        Me.Height = picLogo.Height + 2 * picLogo.Top
        Me.Top = My.Computer.Screen.Bounds.Height - Me.Height
        Me.Left = My.Computer.Screen.Bounds.Width - Me.Width

        iFadeCount = iFadeCountMin
        Me.Opacity = iFadeCount / 100.0

        appSettings = New Settings
        oCreditCard = New CreditCardAuthentication

        oCreditCard.Errors = appSettings.Errors

        If appSettings.CreditCardTestMode Then
            oCreditCard.Server = "pilot-payflowpro.paypal.com"
        Else
            oCreditCard.Server = "payflowpro.paypal.com"
        End If

        oCreditCard.Port = 443
        oCreditCard.Timeout = 45

        oCreditCard.Debugging = appSettings.DebuggingEnabled
        oCreditCard.User = appSettings.CreditCardUserName
        oCreditCard.Vendor = appSettings.CreditCardVendor
        oCreditCard.Partner = appSettings.CreditCardPartner
        oCreditCard.PassWord = appSettings.CreditCardPassWord
        oCreditCard.Comment1 = My.Computer.Name.ToUpper
        oCreditCard.Comment2 = "LobbyPC purchase"

        oCreditCard.MerchantDescription = appSettings.MerchantDescription
        oCreditCard.MerchantServiceEmail = appSettings.MerchantServiceEmail

        'This is for debugging purposes, leave commented out in live version
        'oCreditCard.ForceVoid = True

        WriteLogEntry("Credit card region: " & appSettings.CreditCardRegion)

        If appSettings.ApplicationVisible Then
            tmrFade.Enabled = True
        Else
            MainSub()
        End If
    End Sub

    Private Sub MainSub()
        tmrStart.Enabled = True
    End Sub

    Private Sub KillExistingInstances(Optional ByVal bKillAll As Boolean = False)
        Dim p() As Process, i As Integer

        p = Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName)

        For i = 0 To p.Length - 1
            If p(i).Id <> Process.GetCurrentProcess.Id Then
                p(i).Kill()
            End If
        Next

        If bKillAll Then
            Process.GetCurrentProcess.Kill()
        End If
    End Sub

    Private Sub tmrStart_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrStart.Tick
        tmrStart.Enabled = False

        oCreditCard.ProcessCreditCardRequest()

        If oCreditCard.Success Then
            'We could do something here, fireworks, champagne, etc...
            'But we don't
        Else
            'Boohoo
        End If

        If appSettings.ApplicationVisible Then
            tmrFade.Enabled = True
        Else
            ExitApplication()
        End If
    End Sub

    Private Sub tmrFade_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrFade.Tick
        Me.Opacity = iFadeCount / 100.0

        iFadeCount = iFadeCount + iFadeStep

        If iFadeCount > iFadeCountMax Then
            iFadeStep = -iFadeStep

            iFadeCount = iFadeCountMax

            tmrFade.Enabled = False

            MainSub()
        End If

        If iFadeCount < iFadeCountMin Then
            tmrFade.Enabled = False

            ExitApplication()
        End If
    End Sub

    Public Sub ExitApplication()
        WriteLogEntry("==========")

        End
    End Sub
End Class
