var currency = SiteKiosk.Plugins("SiteCash").CreateCurrencyFormatter().CurrencySymbol;
var currencycode = SiteKiosk.Plugins("SiteCash").CreateCurrencyFormatter().ISOCode;
var SiteCash = SiteKiosk.Plugins("SiteCash");
var CreditCard = SiteCash.Devices("CreditCard");

var AmountDigits = new Array();
var CacheAmountDigits = new Array();

var CacheTimeDigits = new Array();
var TimeDisplayArray = new Array();

var MyPricePerHour = SiteKiosk.Plugins("SiteCash").GetPricePerHourForURL("sitecash://baseprice", false);

var CurrentPricePerSecond = (MyPricePerHour/3600);

	// Some Variables one might want to change:
	// First, the time the dialog initially waits before it increments (this is formatted in thousands of a second):
var WaitBeforeInc = 300;
	// Second, the interval time after that:
var IntervalInc = 400;
	// then, we have the time an error stays:
var TimeForError = 2000;


var CurrentAdd = CreditCard.AmountIncrement*100;

var MaximumAmount = CreditCard.MaxAmount*100;
var MinimumAmount = CreditCard.MinAmount*100;

var CurrentTime = 0;
var MinuteDisplayWidth = 3;
var SecondDisplayWidth = 2;

var DisplayWidth = 5;
var DisplayArray = new Array();

var timeout = 60000;

var CurrentAmount = MinimumAmount;

var CurAmountTimer = 0;

var StartAdd = CurrentAdd;
var MaxAdd = 100;

var ErrorTimer = 0;

var cnt = 0;
var dlg;
var dlgWait;

var gk_CreditCardInfo = null;

function ResetWindowTimeOut() {
	window.clearTimeout(WindowTimeOut);
	WindowTimeOut = window.setTimeout("OnScreenSaverBegin();", timeout);
}

function DialogClose() {
	_KillCreditCardValidationApplication();
	
	Dialog.CloseDialog();
}

function IncAmount(bStart)
{
	ResetWindowTimeOut();
	if (bStart)
	{
		SetAmountDigits(parseInt(CurrentAmount+=CurrentAdd));
		window.clearTimeout(CurAmountTimer);
		CurAmountTimer = window.setTimeout("IncAmountTimer();", WaitBeforeInc);
	}
	else
	{
		CurrentAdd = StartAdd;
		window.clearTimeout(CurAmountTimer);
		window.clearInterval(CurAmountTimer);
	}
}

function DescAmount(bStart)
{
	ResetWindowTimeOut();
	if (bStart)
	{
		SetAmountDigits(parseInt(CurrentAmount-=CurrentAdd));
		window.clearTimeout(CurAmountTimer);
		CurAmountTimer = window.setTimeout("DescAmountTimer();", WaitBeforeInc);
	}
	else
	{
		CurrentAdd = StartAdd;
		window.clearTimeout(CurAmountTimer);
		window.clearInterval(CurAmountTimer);
	}
}

function IncAmountTimer()
{
	window.clearInterval(CurAmountTimer);
	CurAmountTimer = window.setInterval("SetAmountDigits(parseInt(CurrentAmount+=CurrentAdd));", IntervalInc);
}

function DescAmountTimer()
{
	window.clearInterval(CurAmountTimer);
	CurAmountTimer = window.setInterval("SetAmountDigits(parseInt(CurrentAmount-=CurrentAdd));", IntervalInc);
}

function ChangeOKBut(strWhat)
{
	document.all["obLeft"].src = "./images/error/ok_l" + strWhat + ".gif";
	document.all["obMiddle"].style.background = "url(./images/error/ok_m" + strWhat + ".gif)";
	document.all["obRight"].src = "./images/error/ok_r" + strWhat + ".gif";
}
function ChangeCancelBut(strWhat)
{
	document.all["cbLeft"].src = "./images/cancel_" + strWhat + "l.gif";
	document.all["cbMiddle"].style.background = "url(./images/cancel_" + strWhat + "m.gif)";
	document.all["cbRight"].src = "./images/cancel_" + strWhat + "r.gif";
}

function CalculateTime()
{
	return (CurrentAmount/100)/CurrentPricePerSecond;
}

function SetTimeDigits(dSeconds)
{
	if (dSeconds > 59940) // FiftyNineThousandNinehundredFourty (we can not display more than that...)
	{
		SetTimeError();
		return;
	}
	document.all['fntEquals'].style.color = '#19E433';
	var sTime = new String(parseInt(dSeconds));
	var sMinutes = new String(parseInt(dSeconds/60));
	var sSeconds = new String(parseInt(dSeconds%60));

  	for (i = 0; i < sMinutes.length; i++)
  	{
		var no = sMinutes.charAt(i);
  		TimeDisplayArray[i+(MinuteDisplayWidth-sMinutes.length)].src = CacheTimeDigits[no].src;
 	}
  	for (i = 0; i < sSeconds.length; i++)
  	{
		var no = sSeconds.charAt(i);
  		TimeDisplayArray[i+(SecondDisplayWidth-sSeconds.length)+MinuteDisplayWidth].src = CacheTimeDigits[no].src;
 	}

 	for (i = 0; i < ((MinuteDisplayWidth)-sMinutes.length); i++)
 	{
  		if (i >= (MinuteDisplayWidth)-1)
   			TimeDisplayArray[i].src = CacheTimeDigits[0].src;
  		else
   			TimeDisplayArray[i].src = "./images/numbers/time/background.gif";
   
	}
 	for (i = 0; i < ((SecondDisplayWidth)-sSeconds.length); i++)
 	{
  		if (i >= (SecondDisplayWidth)-3)
   			TimeDisplayArray[i+3].src = CacheTimeDigits[0].src;
  		else
   			TimeDisplayArray[i+3].src = "./images/numbers/time/background.gif";
   
	}
}

function SetStatus(sMessage, iPriority)
{
	if (iPriority == 0)
	{
		document.all['fntStatus'].style.color = "#FFFFFF";
		document.all['fntStatus'].style.fontWeight = "normal";
	}
	if (iPriority == 1)
	{
		document.all['fntStatus'].style.color = "#FFCCCC";
		document.all['fntStatus'].style.fontWeight = "bold";
	}
		
	document.all['fntStatus'].innerHTML = sMessage;
}

function SetError()
{
	for (var i = 0; i < DisplayWidth; ++i)
	{
		DisplayArray[i].src = "./images/numbers/amount/e.gif";
	}
	for (var i = 0; i < MinuteDisplayWidth+SecondDisplayWidth; ++i)
	{
		TimeDisplayArray[i].src = "./images/numbers/time/e.gif";
	}
	
	document.all["fntCurrency"].style.color = "#FF0000";
	document.all["fntEquals"].style.color = "#FF0000";
	
		// in the very end, Set our Amount Digits back to normal:
	window.clearTimeout(ErrorTimer);
	ErrorTimer = window.setTimeout("SetStatus(LoadString(2911), 0); document.all['fntEquals'].style.color = '#19E433'; document.all['fntCurrency'].style.color = '#40B1FF'; SetAmountDigits(CurrentAmount);", TimeForError);
}

function SetTimeError()
{
	for (var i = 0; i < MinuteDisplayWidth+SecondDisplayWidth; ++i)
	{
		TimeDisplayArray[i].src = "./images/numbers/time/e.gif";
	}
	
	document.all["fntEquals"].style.color = "#FF0000";
}

function SetAmountDigits(dAmount)
{
	if (dAmount > MaximumAmount)
	{
		dAmount = MaximumAmount;
		CurrentAmount = MaximumAmount;
		SetStatus(LoadString(2916), 1);
		SetError();
		return;
	}
	if (dAmount < MinimumAmount)
	{
		dAmount = MinimumAmount;
		CurrentAmount = MinimumAmount;
		SetStatus(LoadString(2917), 1);
		SetError();
		return;
	}

	CurrentAmount = dAmount;

	var sAmount = new String(dAmount);
	
  	for (i = 0; i < sAmount.length; i++)
  	{
		var no = sAmount.charAt(i);
  		DisplayArray[i+(DisplayWidth-sAmount.length)].src = CacheAmountDigits[no].src;
 	}
 	for (i = 0; i < (DisplayWidth-sAmount.length); i++)
 	{
  		if (i >= DisplayWidth-3)
   			DisplayArray[i].src = CacheAmountDigits[0].src;
  		else
   			DisplayArray[i].src = "./images/numbers/amount/background.gif";
   
	}
	
	try
	{
		SetTimeDigits(CalculateTime());
	}
	catch (e)
	{
	}
}

function Debit()
{
	if((ccfirstname.value.length+cclastname.value.length)<=5) {
		_ShowCreditCardErrorDialog(LoadString(2928),LoadString(2970));
	}
	
	ValidateCreditCardInfo();		
}


//+-------------------------------+
//| Custom credit card validation |
//+-------------------------------+
var __CreditCardValidationApplication_Server="http://localhost:8181/iBahnCreditCardListener/";
var __CreditCardValidationApplication_DataObj=new Object();
var __CreditCardValidationApplication_PostObj=new Object();
var __CreditCardValidationApplication_RetObj=new Object();
var __CreditCardValidationApplication_ServerPageTimeoutId=0;

function ValidateCreditCardInfo() {
	var sDate="";
		
	sDate=ccmonth.options[ccmonth.selectedIndex].text;
	sDate+=ccyear.options[ccyear.selectedIndex].text.replace("20","qq");
	sDate=sDate.replace("qq","");
	
	__CreditCardValidationApplication_DataObj=new Object();
	__CreditCardValidationApplication_DataObj.Name=ccfirstname.value+" "+cclastname.value;
	__CreditCardValidationApplication_DataObj.Number=ccnumber.value;
	__CreditCardValidationApplication_DataObj.Date=sDate;
	__CreditCardValidationApplication_DataObj.CVC=ccsecurity.value;
	_ClearCreditCardTextFields(); 
	
	if(__CreditCardValidationApplication_DataObj.Name.length<3) {
		_ShowCreditCardErrorDialog(LoadString(2919),LoadString(2928));
		return;
	}
		
	if(__CreditCardValidationApplication_DataObj.Number.length<10) {
		_ShowCreditCardErrorDialog(LoadString(2919),LoadString(2928));
		return;
	}
		
	if(__CreditCardValidationApplication_DataObj.CVC.length<3||__CreditCardValidationApplication_DataObj.CVC.length>4) {
		_ShowCreditCardErrorDialog(LoadString(2919),LoadString(2928));
		return;
	}
	
	_ShowCreditCardWaitDialog();	
	_StartCreditCardValidationApplication();
		
	setTimeout('_SendDataToCreditCardValidationApplication()',2000);
}

function _ValidateCreditCardInfoFailure() {
	_ShowCreditCardErrorDialog(LoadString(2928),LoadString(2919));
}

function _ClearCreditCardTextFields() {
	ccfirstname.value="";
	cclastname.value="";
	ccnumber.value="";
	ccsecurity.value="";
	ccyear.selectedIndex=0;
	ccmonth.selectedIndex=0;
	cccardtype.selectedIndex=0;
}

function _ShowCreditCardTextFields() {
	ccfirstname.style.display="block";
	cclastname.style.display="block";
	ccnumber.style.display="block";
	ccsecurity.style.display="block";
	ccyear.style.display="block";
	ccmonth.style.display="block";
	cccardtype.style.display="block";
}

function _HideCreditCardTextFields() {
	ccfirstname.style.display="none";
	cclastname.style.display="none";
	ccnumber.style.display="none";
	ccsecurity.style.display="none";
	ccyear.style.display="none";
	ccmonth.style.display="none";
	cccardtype.style.display="none";
}

function _StartCreditCardValidationApplication() {
	_LogCreditCardValidationMessage("Starting CreditCardPayment.exe");
	SiteKiosk.ExternalApps.Run("c:/ibahn/CreditCardPayment/CreditCardPayment.exe",false);
}

function _KillCreditCardValidationApplication() {
	SiteKiosk.ExternalApps.Run("c:/ibahn/CreditCardPayment/CreditCardPayment.exe --kill",false);
}

function _SendDataToCreditCardValidationApplication() {
	__CreditCardValidationApplication_PostObj=new Object();
	__CreditCardValidationApplication_PostObj=SiteKiosk.Network.CreateHTTPPost();
	__CreditCardValidationApplication_PostObj.OnPostComplete=_OnSendDataToCreditCardValidationApplicationComplete;
	__CreditCardValidationApplication_PostObj.AddParameter("enc","N");
	__CreditCardValidationApplication_PostObj.AddParameter("cname",__CreditCardValidationApplication_DataObj.Name);
	__CreditCardValidationApplication_PostObj.AddParameter("cnum",__CreditCardValidationApplication_DataObj.Number);
	__CreditCardValidationApplication_PostObj.AddParameter("cdate",__CreditCardValidationApplication_DataObj.Date);
	__CreditCardValidationApplication_PostObj.AddParameter("cvc",__CreditCardValidationApplication_DataObj.CVC);
	__CreditCardValidationApplication_PostObj.AddParameter("amount",CurrentAmount/100);
	__CreditCardValidationApplication_PostObj.AddParameter("currency",currencycode);
	
	_LogCreditCardValidationMessage("Validating credit card info");
	
	clearTimeout(__CreditCardValidationApplication_ServerPageTimeoutId);
	__CreditCardValidationApplication_ServerPageTimeoutId=setTimeout('_OnSendDataToCreditCardValidationApplicationTimeout()',30000);
	__CreditCardValidationApplication_PostObj.Submit(__CreditCardValidationApplication_Server);
}

function _OnSendDataToCreditCardValidationApplicationComplete(success,returnvalue) {
	clearTimeout(__CreditCardValidationApplication_ServerPageTimeoutId);
	checkForCorrectAnswer=returnvalue.indexOf("|");
	
	_CloseCreditCardWaitDialog();
	
	_LogCreditCardValidationMessage("Received response:");
	_LogCreditCardValidationMessage(returnvalue);

	if(checkForCorrectAnswer<0||checkForCorrectAnswer>5) {
		//Incorrect answer
		_ShowCreditCardErrorDialog(LoadString(2904),LoadString(2901));
	} else {
		__CreditCardValidationApplication_PostObj=new Object();
		__CreditCardValidationApplication_PostObj=SiteKiosk.Network.CreateHTTPPost();
		__CreditCardValidationApplication_PostObj.OnPostComplete=_OnSendDataToCreditCardValidationApplicationComplete;
		
		valuearray=returnvalue.split("|");
		
		__CreditCardValidationApplication_RetObj=new Object();
		__CreditCardValidationApplication_RetObj.ReturnValue=returnvalue;
		__CreditCardValidationApplication_RetObj.RetCode=parseInt(valuearray[0]);
		__CreditCardValidationApplication_RetObj.RetMsg=valuearray[1];
		__CreditCardValidationApplication_RetObj.RetText=valuearray[2];
		
		if(__CreditCardValidationApplication_RetObj.RetCode==0) {
			SiteCash.Credit(CurrentAmount/100);
			
			SiteKiosk.Logfile.Write(2005,20,"SiteCash","Credit: "+currencycode+" "+((CurrentAmount/100).toFixed(6))+" by Credit Card: ISO/MagTek/etc.");

			_ShowCreditCardSuccessDialog();
		} else {
			_ShowCreditCardErrorDialog("Server message: "+__CreditCardValidationApplication_RetObj.RetMsg,LoadString(2931));
		}
	}
}

function _RepeatString(s,n) {
	if(!n) return "";
	if(n&1) return _RepeatString(s,n-1)+s;
	var t=_RepeatString(s,n>>1);
	return t+t;
}

function _OnSendDataToCreditCardValidationApplicationTimeout() {
	_CloseCreditCardWaitDialog();
	_ShowCreditCardErrorDialog(LoadString(2958),LoadString(2901));
}

function _ShowCreditCardWaitDialog() {
	_CloseCreditCardWaitDialog();
		
	dlg=SiteKiosk.SiteKioskUI.CreateHTMLDialog();
	dlg.LoadDefaultSettings("creditcard-progress");
	dlg.PreventInput=true;
	dlg.ShowDialog();
}

function _CloseCreditCardWaitDialog() {
	if(SiteKiosk.SiteKioskUI.IsDialogRunning("creditcard-progress")) SiteKiosk.SiteKioskUI.CloseHtmlDialogs("creditcard-progress");
}

function _ShowCreditCardSuccessDialog() {
	if(SiteKiosk.SiteKioskUI.IsDialogRunning("creditcard-success")) SiteKiosk.SiteKioskUI.CloseHtmlDialogs("creditcard-success");
	
 	dlg=SiteKiosk.SiteKioskUI.CreateHTMLDialog();
	dlg.LoadDefaultSettings("creditcard-success");
	dlg.ShowDialog();
	
	DialogClose();
}

function _ShowCreditCardErrorDialog(sText,sTitle) {
	if(SiteKiosk.SiteKioskUI.IsDialogRunning("CreditCardErrorDialog")) SiteKiosk.SiteKioskUI.CloseHtmlDialogs("CreditCardErrorDialog");
		
	dlg=SiteKiosk.SiteKioskUI.CreateHTMLDialog();
		
	var DialogMessage=new Object();
	DialogMessage.Text=sText;
	DialogMessage.Title=sTitle;
		
	dlg.URL="file://"+SiteKiosk.SiteKioskDirectory+"SiteCash\\html\\CreditCard\\error.htm";
	dlg.ScrollBars=false;
	dlg.Border=true;
	dlg.Title=false;
	dlg.Sysmenu=false;
	dlg.Width=450;
	dlg.Height=200;
	dlg.Type="CreditCardErrorDialog";
	dlg.TopMostWindow=true;
	dlg.CloseOnInput=false;
	dlg.PreventInput=false;
	dlg.AttachDispatch("DialogMessage",DialogMessage);
	dlg.ShowDialog();

	//DialogClose();
	_KillCreditCardValidationApplication();
}

function _LogCreditCardValidationMessage(sMsg) {
	SiteKiosk.Logfile.Notification("CreditCardValidation - "+sMsg);
}
//+-------------------------------+

function SetCurrency()
{
	document.all["fntCurrency"].innerHTML = currency;
}

function InitializeTimeDigits()
{
	for (i = 0; i < MinuteDisplayWidth+SecondDisplayWidth; i++)
		TimeDisplayArray[i] = document.all("tCifer"+i);

	for (var i = 0; i <= 9; ++i)
	{
		CacheTimeDigits[i] = new Image();
		CacheTimeDigits[i].src = "./images/numbers/time/" + i + ".gif";
	}
	
	try
	{
		SetTimeDigits(CalculateTime());
	}
	catch(e)
	{
	}
}

function InitializeAmountDigits()
{
	for (i = 0; i < DisplayWidth; i++)
		DisplayArray[i] = document.all("Cifer"+i);

	for (var i = 0; i <= 9; ++i)
	{
		CacheAmountDigits[i] = new Image();
		CacheAmountDigits[i].src = "./images/numbers/amount/" + i + ".gif";
	}
	
	SetAmountDigits(CurrentAmount);
}

function InitializeDigits()
{
	InitializeTimeDigits();
	InitializeAmountDigits();
}

function SetLanguage()
{
	document.all["fntCaption"].innerHTML = LoadString(2907);
	document.all["DescrText"].innerHTML = LoadString(2908);
	document.all["fntTimeDescription"].innerHTML = LoadString(2909);
	document.all["fntTimeDescription2"].innerHTML = LoadString(2910);
	document.all["fntStatus"].innerHTML = LoadString(2911);
	document.all["fntDescCC"].innerHTML = LoadString(2912);
	
	document.all["fntDescCCType"].innerHTML = LoadString(2913);
	document.all["fntDescCCNumber"].innerHTML = LoadString(2914);
	document.all["fntDescCCValid"].innerHTML = LoadString(2915);
	document.all["fntDescCCLastName"].innerHTML = LoadString(2940);
	document.all["fntDescCCFirstName"].innerHTML = LoadString(2939);
	
	document.all["fntDescCCSecurity"].innerHTML = "CVC";
	
	document.all["strOK"].innerHTML = LoadString(907);
	document.all["strCancel"].innerHTML = LoadString(302);
}

function OnCardSwiped(ak_CreditCardInfo)
{
	gk_CreditCardInfo = ak_CreditCardInfo;

	var myDate = new Date();
	
	ccnumber.value = ak_CreditCardInfo.Number;
	ccyear.selectedIndex = ak_CreditCardInfo.Year - myDate.getFullYear();
	ccmonth.selectedIndex = ak_CreditCardInfo.Month - 1;
	cclastname.value = ak_CreditCardInfo.LastName;
	ccfirstname.value = ak_CreditCardInfo.FirstName;

	var lb_Found = false;
	for (var i = 0; i < cccardtype.options.length; ++i)
	{
		var curcard = cccardtype.options[i];
		if (curcard.value == ak_CreditCardInfo.CardType)
		{
			cccardtype.selectedIndex = curcard.index;
			lb_Found = true;
		}
	}
	if (!lb_Found)
	{
			// Unknown
		cccardtype.selectedIndex = 0;
	}
	return true;
}

function OnScreenSaverBegin()
{
	DialogClose();
}

var myenum = new Enumerator(CreditCard.AcceptedCards);
for (; !myenum.atEnd(); myenum.moveNext())
{
	var oOption = document.createElement("OPTION");
	cccardtype.options.add(oOption);
	switch (myenum.item())
	{
	case 1:
		oOption.innerText = "Visa";
		break;
	case 2:
		oOption.innerText = "MasterCard";
		break;
	case 3:
		oOption.innerText = "AmericanExpress";
		break;
	case 4:
		oOption.innerText = "DinersClub";
		break;
	case 5:
		oOption.innerText = "Discover";
		break;
	case 6:
		oOption.innerText = "JCB";
		break;
	case 7:
		oOption.innerText = "Visa Delta";
		break;
	case 8:
		oOption.innerText = "Solo";
		break;
	case 9:
		oOption.innerText = "DanKort";
		break;
	}
	oOption.value = myenum.item();
}

var myDate = new Date();
var startYear = myDate.getFullYear();
var endYear = myDate.getFullYear() + 10;
for (var i = startYear; i <= endYear; ++i)
{
	var oOption = document.createElement("OPTION");
	ccyear.options.add(oOption);
	oOption.innerText = i;
	oOption.value = i;
}

	// Actually, we want to do something...
CreditCard.OnCardSwiped = OnCardSwiped;

SiteKiosk.ScreenSaver.OnScreenSaverBegin = OnScreenSaverBegin;

var WindowTimeOut = 0;
ResetWindowTimeOut();

SetLanguage();
InitializeDigits();
SetCurrency();

var CurrencyFormatter = SiteKiosk.Plugins("SiteCash").CreateCurrencyFormatter();
if (CreditCard.ProcessingFee > 0)
{
	document.all["tdPayNote"].style.display = "inline";
	document.all["tdFeeText"].innerHTML = LoadString(2923) + " <u>" + CurrencyFormatter.Format2(CreditCard.ProcessingFee, true) + "</u> " + LoadString(2924);
}

if (!CreditCard.ManualInputEnabled)
{
	document.all["cccardtype"].disabled = true;
	document.all["ccnumber"].disabled = true;
	document.all["ccmonth"].disabled = true;
	document.all["ccyear"].disabled = true;
}

try
{
	OnCardSwiped(CreditCardInfo);
}
catch (e)
{
}