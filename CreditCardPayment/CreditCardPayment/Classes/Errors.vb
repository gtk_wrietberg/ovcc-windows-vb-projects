Public Class Errors
    Private mColCodes As Collection
    Private mColInternal As Collection
    Private mColHuman As Collection

    Public Sub New()
        mColCodes = New Collection
        mColInternal = New Collection
        mColHuman = New Collection
    End Sub

    Public ReadOnly Property Count() As Integer
        Get
            Return mColCodes.Count
        End Get
    End Property

    Public Sub Add(ByVal ErrorCode As Integer, ByVal ErrorMessageInternal As String, ByVal ErrorMessageHuman As String)
        mColCodes.Add(ErrorCode)
        mColInternal.Add(ErrorMessageInternal)
        mColHuman.Add(ErrorMessageHuman)
    End Sub

    Public Function GetErrorMessage(ByVal ErrorCode As Integer, ByRef ErrorMessageInternal As String, ByRef ErrorMessageHuman As String) As Boolean
        Dim i As Integer

        For i = 1 To mColCodes.Count
            If CInt(mColCodes.Item(i)) = ErrorCode Then
                ErrorMessageInternal = CStr(mColInternal.Item(i))
                ErrorMessageHuman = CStr(mColHuman.Item(i))

                Return True
            End If
        Next

        Return False
    End Function


End Class
