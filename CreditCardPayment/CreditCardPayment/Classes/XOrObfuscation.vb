Public Class XOrObfuscation
    Private Const PassPhraseLength = 23
    Private arrPass() As Integer

    Public Sub New()
        InitializePasshrase()
    End Sub

    Public Function Obfuscate(ByVal PlainText As String) As String
        Dim i As Integer, p As Integer
        Dim ObscuredText As String

        InitializePasshrase()

        p = 0
        ObscuredText = ""
        For i = 0 To PassPhraseLength - 1
            ObscuredText = ObscuredText & _D2H(arrPass(i))
        Next
        For i = 0 To Len(PlainText) - 1
            ObscuredText = ObscuredText & _D2H(Asc(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
            p = p + 1
            If p >= PassPhraseLength Then
                p = 0
            End If
        Next

        Obfuscate = ObscuredText
    End Function

    Public Function Deobfuscate(ByVal ObscuredText As String) As String
        Dim a() As Integer
        Dim i As Integer, j As Integer, p As Integer
        Dim PlainText As String

        ReDim a(PassPhraseLength)
        p = 0
        PlainText = ""

        For i = 1 To PassPhraseLength * 2 Step 2
            j = ((i + 1) / 2) - 1
            a(j) = _H2D(Mid(ObscuredText, i, 2))
        Next
        For i = PassPhraseLength * 2 + 1 To Len(ObscuredText) Step 2
            PlainText = PlainText & Chr(_H2D(Mid(ObscuredText, i, 2)) Xor a(p))
            p = p + 1
            If p >= PassPhraseLength Then
                p = 0
            End If
        Next

        Deobfuscate = PlainText
    End Function

    Private Sub InitializePasshrase()
        Dim i As Integer

        Randomize()

        ReDim arrPass(PassPhraseLength)
        For i = 0 To PassPhraseLength - 1
            arrPass(i) = Int(255 * Rnd())
        Next
    End Sub

    Private Function _D2H(ByVal Value As Integer) As String
        Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
        Dim BinVal() As String

        iVal = Value
        Do
            temp = Int(iVal / 16)
            ret = iVal Mod 16
            ReDim Preserve BinVal(i)
            BinVal(i) = _NoToHex(ret)
            i = i + 1
            iVal = temp
        Loop While temp > 0
        For i = UBound(BinVal) To 0 Step -1
            Str = Str & CStr(BinVal(i))
        Next
        If Value < 16 Then
            Str = "0" & Str
        End If

        _D2H = Str
    End Function

    Private Function _H2D(ByVal BinVal As String) As Integer
        Dim iVal As Integer, temp As Integer

        temp = _HexToNo(Mid(BinVal, 2, 1))
        iVal = iVal + temp
        temp = _HexToNo(Mid(BinVal, 1, 1))
        iVal = iVal + (temp * 16)

        _H2D = iVal
    End Function

    Private Function _NoToHex(ByVal i As Integer) As String
        Select Case i
            Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                _NoToHex = CStr(i)
            Case 10
                _NoToHex = "a"
            Case 11
                _NoToHex = "b"
            Case 12
                _NoToHex = "c"
            Case 13
                _NoToHex = "d"
            Case 14
                _NoToHex = "e"
            Case 15
                _NoToHex = "f"
            Case Else
                _NoToHex = ""
        End Select
    End Function

    Private Function _HexToNo(ByVal s As String) As Integer
        Select Case s
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                _HexToNo = CInt(s)
            Case "A", "a"
                _HexToNo = 10
            Case "B", "b"
                _HexToNo = 11
            Case "C", "c"
                _HexToNo = 12
            Case "D", "d"
                _HexToNo = 13
            Case "E", "e"
                _HexToNo = 14
            Case "F", "f"
                _HexToNo = 15
            Case Else
                _HexToNo = 0
        End Select
    End Function
End Class
