Imports System.Xml

Public Class Settings
    Private mSkCfg As String

    Private mApplicationVisible As Boolean
    Private mApplicationDebug As Boolean

    Private mCreditCardRegion As String
    Private mCreditCardUserNameFromSkCfg As String
    Private mCreditCardUserNameFromSkCfg_MANUALLY As String
    Private mCreditCardUserName As String
    Private mCreditCardVendor As String
    Private mCreditCardPassWord As String
    Private mCreditCardPartner As String
    Private mCreditCardTestMode As Boolean

    Private mMerchantDescription As String
    Private mMerchantServiceEmail As String

    Private mErrors As Errors

    Public Sub New(ManualUsername As String)
        mCreditCardUserNameFromSkCfg_MANUALLY = ManualUsername

        _init(True)
    End Sub

    Public Sub New()
        _init(False)
    End Sub

    Private Sub _init(bManual As Boolean)
        mSkCfg = _FindActiveSiteKioskConfigFile()

        _LoadSettingsFromSiteKioskConfigFile()
        If bManual And mCreditCardUserNameFromSkCfg = "" Then
            mCreditCardUserNameFromSkCfg = mCreditCardUserNameFromSkCfg_MANUALLY
        End If
        _LoadSettingsFromXmlFile()

        If mApplicationDebug Or bManual Then
            Debug()
        End If

        _LoadErrorsFromXmlFile()
    End Sub

#Region "Properties"
    ''' <summary>  
    '''SiteKiosk config file full path
    ''' </summary>
    Public ReadOnly Property SiteKioskConfigFile() As String
        Get
            Return mSkCfg
        End Get
    End Property

    ''' <summary>  
    '''Errors
    ''' </summary>
    Public ReadOnly Property Errors() As Errors
        Get
            Return mErrors
        End Get
    End Property

    ''' <summary>  
    '''Is application visible
    ''' </summary>
    Public ReadOnly Property ApplicationVisible() As Boolean
        Get
            Return mApplicationVisible
        End Get
    End Property

    ''' <summary>  
    '''Is debugging enabled
    ''' </summary>
    Public ReadOnly Property DebuggingEnabled() As Boolean
        Get
            Return mApplicationDebug
        End Get
    End Property

    ''' <summary>  
    '''Region for credit card entity
    ''' </summary>
    Public ReadOnly Property CreditCardRegion() As String
        Get
            Return mCreditCardRegion
        End Get
    End Property

    ''' <summary>  
    '''Username for credit card entity
    ''' </summary>
    Public ReadOnly Property CreditCardUserName() As String
        Get
            Return mCreditCardUserName
        End Get
    End Property

    ''' <summary>  
    '''Password for credit card entity
    ''' </summary>
    Public ReadOnly Property CreditCardPassWord() As String
        Get
            Return mCreditCardPassWord
        End Get
    End Property

    ''' <summary>  
    '''Vendor for credit card entity
    ''' </summary>
    Public ReadOnly Property CreditCardVendor() As String
        Get
            Return mCreditCardVendor
        End Get
    End Property

    ''' <summary>  
    '''Partner for credit card entity
    ''' </summary>
    Public ReadOnly Property CreditCardPartner() As String
        Get
            Return mCreditCardPartner
        End Get
    End Property

    ''' <summary>  
    '''Test mode enabled?
    ''' </summary>
    Public ReadOnly Property CreditCardTestMode() As Boolean
        Get
            Return mCreditCardTestMode
        End Get
    End Property

    ''' <summary>  
    '''Merchant description
    ''' </summary>
    Public ReadOnly Property MerchantDescription() As String
        Get
            Return mMerchantDescription
        End Get
    End Property

    ''' <summary>  
    '''Merchant service email address
    ''' </summary>
    Public ReadOnly Property MerchantServiceEmail() As String
        Get
            Return mMerchantServiceEmail
        End Get
    End Property

    Public Property UsernameFromSkCFG As String
        Get
            Return mCreditCardUserNameFromSkCfg_MANUALLY
        End Get
        Set(value As String)
            mCreditCardUserNameFromSkCfg_MANUALLY = value
        End Set
    End Property

#End Region

    Private Function _FindActiveSiteKioskConfigFile() As String
        Return RegValue(Microsoft.Win32.RegistryHive.LocalMachine, "SOFTWARE\Provisio\SiteKiosk", "LastCfg")
    End Function

    Private Sub _LoadSettingsFromSiteKioskConfigFile()
        Try
            Dim xmlDoc As XmlDocument
            Dim xmlNodeList As XmlNodeList
            Dim xmlGateways As XmlNodeList

            Dim i As Integer, j As Integer, bBreak As Boolean

            xmlDoc = New XmlDocument
            xmlDoc.Load(mSkCfg)

            xmlNodeList = xmlDoc.GetElementsByTagName("email-receipt")
            If xmlNodeList.Count > 0 Then
                For i = 0 To xmlNodeList.Count - 1
                    For j = 0 To xmlNodeList(i).ChildNodes.Count - 1
                        If xmlNodeList(i).ChildNodes(j).Name = "merchant" Then
                            mMerchantDescription = xmlNodeList(i).ChildNodes(j).InnerText
                        End If
                        If xmlNodeList(i).ChildNodes(j).Name = "email-address" Then
                            mMerchantServiceEmail = xmlNodeList(i).ChildNodes(j).InnerText
                        End If
                    Next
                Next
            End If

            xmlGateways = xmlDoc.GetElementsByTagName("gateway")
            If xmlGateways.Count > 0 Then
                For i = 0 To xmlGateways.Count - 1
                    If xmlGateways(i).Attributes.GetNamedItem("clsid").Value = "b9bae269-bad6-41eb-a84f-fdfc938cb93a" Then
                        bBreak = True

                        For j = 0 To xmlGateways(i).ChildNodes.Count - 1
                            If xmlGateways(i).ChildNodes(j).Name = "user" Then
                                mCreditCardUserNameFromSkCfg = xmlGateways(i).ChildNodes(j).InnerText
                            End If
                            If xmlGateways(i).ChildNodes(j).Name = "test-mode" Then
                                If xmlGateways(i).ChildNodes(j).InnerText = "true" Then
                                    mCreditCardTestMode = True
                                Else
                                    mCreditCardTestMode = False
                                End If
                            End If
                        Next
                    End If
                    If bBreak Then
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
            mCreditCardUserNameFromSkCfg = ""
        End Try
    End Sub

    Private Sub _LoadSettingsFromXmlFile()
        Try
            Dim xmlDoc As XmlDocument
            Dim xmlSettings As XmlNodeList
            Dim xmlRegions As XmlNodeList

            Dim i As Integer
            Dim oXor As New XOrObfuscation

            xmlDoc = New XmlDocument
            xmlDoc.Load(System.AppDomain.CurrentDomain.BaseDirectory & "CreditCardPayment.settings.xml")

            xmlSettings = xmlDoc.GetElementsByTagName("application-settings")
            If xmlSettings.Count > 0 Then
                If xmlSettings(0).HasChildNodes Then
                    For i = 0 To xmlSettings(0).ChildNodes.Count - 1
                        If xmlSettings(0).ChildNodes(i).Name = "visible" Then
                            If xmlSettings(0).ChildNodes(i).InnerText = "yes" Then
                                mApplicationVisible = True
                            Else
                                mApplicationVisible = False
                            End If
                        End If
                        If xmlSettings(0).ChildNodes(i).Name = "debug" Then
                            If xmlSettings(0).ChildNodes(i).InnerText = "yes" Then
                                mApplicationDebug = True
                            Else
                                mApplicationDebug = False
                            End If
                        End If
                    Next
                End If
            End If

            xmlRegions = xmlDoc.GetElementsByTagName("region")
            If xmlRegions.Count > 0 Then
                For i = 0 To xmlRegions.Count - 1
                    mCreditCardRegion = xmlRegions(i).Attributes.GetNamedItem("name").Value

                    mCreditCardUserName = oXor.Deobfuscate(xmlRegions(i).ChildNodes(0).InnerText)

                    If mCreditCardUserNameFromSkCfg = mCreditCardUserName Then
                        mCreditCardVendor = oXor.Deobfuscate(xmlRegions(i).ChildNodes(1).InnerText)
                        mCreditCardPartner = oXor.Deobfuscate(xmlRegions(i).ChildNodes(2).InnerText)
                        mCreditCardPassWord = oXor.Deobfuscate(xmlRegions(i).ChildNodes(3).InnerText)

                        Exit For
                    End If
                Next

            End If
        Catch ex As Exception
            mCreditCardUserName = ""
            mCreditCardVendor = ""
            mCreditCardPartner = ""
            mCreditCardPassWord = ""
        End Try

        mCreditCardUserName = mCreditCardUserNameFromSkCfg
    End Sub

    Private Sub _LoadErrorsFromXmlFile()
        Try
            Dim xmlDoc As XmlDocument
            Dim xmlErrors As XmlNodeList

            Dim i As Integer, j As Integer
            Dim iCode As Integer = -1, sInternal As String = "", sHuman As String = ""

            mErrors = New Errors

            xmlDoc = New XmlDocument
            xmlDoc.Load(System.AppDomain.CurrentDomain.BaseDirectory & "CreditCardPayment.errors.xml")

            xmlErrors = xmlDoc.GetElementsByTagName("error")


            If xmlErrors.Count > 0 Then
                If mApplicationDebug Then
                    WriteLogEntry("Found " & xmlErrors.Count.ToString & " errors", LogType.Debug)
                End If

                For i = 0 To xmlErrors.Count - 1
                    If xmlErrors(i).HasChildNodes Then
                        For j = 0 To xmlErrors(i).ChildNodes.Count - 1
                            If xmlErrors(i).ChildNodes(j).Name = "code" Then
                                iCode = CInt(xmlErrors(i).ChildNodes(j).InnerText)
                            End If
                            If xmlErrors(i).ChildNodes(j).Name = "internal" Then
                                sInternal = xmlErrors(i).ChildNodes(j).InnerText
                            End If
                            If xmlErrors(i).ChildNodes(j).Name = "human" Then
                                sHuman = xmlErrors(i).ChildNodes(j).InnerText
                            End If
                        Next
                    End If

                    mErrors.Add(iCode, sInternal, sHuman)
                Next
            End If
        Catch ex As Exception
            mErrors = New Errors
        End Try

        If mApplicationDebug Then
            WriteLogEntry("  Total errors loaded: " & CStr(mErrors.Count), LogType.Debug)
        End If
    End Sub

    Private Sub Debug()
        WriteLogEntry("Settings", LogType.Debug)
        WriteLogEntry("  Application", LogType.Debug)
        WriteLogEntry("    visible  : " & mApplicationVisible, LogType.Debug)
        WriteLogEntry("    debugging: " & mApplicationDebug, LogType.Debug)

        WriteLogEntry("  SiteKiosk", LogType.Debug)
        WriteLogEntry("    config file          : " & mSkCfg, LogType.Debug)
        WriteLogEntry("    credit card user name: " & mCreditCardUserNameFromSkCfg, LogType.Debug)

        WriteLogEntry("  Credit Card", LogType.Debug)
        WriteLogEntry("    test mode             : " & mCreditCardTestMode, LogType.Debug)
        WriteLogEntry("    region                : " & mCreditCardRegion, LogType.Debug)
        WriteLogEntry("    user name             : " & mCreditCardUserName, LogType.Debug)
        WriteLogEntry("    vendor                : " & mCreditCardVendor, LogType.Debug)
        WriteLogEntry("    partner               : " & mCreditCardPartner, LogType.Debug)
        WriteLogEntry("    password              : " & mCreditCardPassWord, LogType.Debug)
        WriteLogEntry("    merchant description  : " & mMerchantDescription, LogType.Debug)
        WriteLogEntry("    merchant service email: " & mMerchantServiceEmail, LogType.Debug)
    End Sub
End Class
