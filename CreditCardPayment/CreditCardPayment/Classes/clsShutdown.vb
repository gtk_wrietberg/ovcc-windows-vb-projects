Public Class clsShutdown
    Private Enum ShutdownParam
        Logoff = 0&
        Shutdown = 1&
        Reboot = 2&
    End Enum

    Private Sub _Shutdown(ByVal param As ShutdownParam, ByVal forced As Boolean)
        Dim t As Single
        Dim objWMIService, objComputer As Object
        Dim flags As Long

        flags = param
        If forced Then
            flags = flags Or 4&
        End If

        objWMIService = GetObject("Winmgmts:{impersonationLevel=impersonate,(Debug,Shutdown)}")

        For Each objComputer In objWMIService.InstancesOf("Win32_OperatingSystem")
            t = objComputer.Win32Shutdown(flags, 0)
            If t <> 0 Then
                'MsgBox("Error occurred!!!")
            Else
                'LogOff your system
            End If
        Next
    End Sub

    Public Sub Logoff()
        _Shutdown(ShutdownParam.Logoff, True)
    End Sub

    Public Sub Shutdown()
        _Shutdown(ShutdownParam.Shutdown, True)
    End Sub

    Public Sub Reboot()
        _Shutdown(ShutdownParam.Reboot, True)
    End Sub
End Class
