Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("CreditCardPayment")> 
<Assembly: AssemblyDescription("Handles credit card payment from SiteKiosk v6")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("CreditCardPayment")> 
<Assembly: AssemblyCopyright("Copyright © 2009")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("a7ea9892-2351-466c-94d8-f06a953c285a")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.108.1631")> 
<Assembly: AssemblyFileVersion("7.5.129.1631")> 
