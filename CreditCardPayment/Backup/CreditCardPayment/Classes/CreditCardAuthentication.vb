Imports PayPal.Payments.Common
Imports PayPal.Payments.Common.Utility
Imports PayPal.Payments.DataObjects
Imports PayPal.Payments.Transactions

Public Class CreditCardAuthentication
#Region "Properties"
    Private mDebug As Boolean
    Private mForceVoid As Boolean

    Private mUser As String = ""
    Private mVendor As String = ""
    Private mPartner As String = ""
    Private mPassWord As String = ""
    Private mSwipeData As String = ""
    Private mComment1 As String = ""
    Private mComment2 As String = ""

    Private mMerchantDescription As String = ""
    Private mMerchantServiceEmail As String = ""

    Private mServer As String = ""
    Private mPort As Integer = 0
    Private mTimeout As Integer = 0

    Private mAmount As Decimal
    Private mCurrencyCode As String = ""

    Private mCreditCardName As String = ""
    Private mCreditCardNumber As String = ""
    Private mCreditCardExpiry As String = ""
    Private mCreditCardSecurityCode As String = ""

    Private mResponseCode As Integer = 0
    Private mOriginalResponseCode As Integer = 0
    Private mOriginalResponseMsg As String = ""
    Private mResponseHumanMessage As String = ""
    Private mResponseInternalMessage As String = ""
    Private mResponseText As String = ""
    Private mCVV2Match As String = ""
    Private mPnref As String = ""

    Private oUser As UserInfo
    Private oConnection As PayflowConnectionData
    Private oInvoice As Invoice
    Private oAmount As Currency
    Private oCreditCard As CreditCard
    Private oSwipeCard As SwipeCard
    Private oCardTender As CardTender

    Private bCC_Manual As Boolean

    Private mSuccess As Boolean

    Private mCurrencyCodeCollection As Collection

    Private mErrors As Errors

    Private ReadOnly LocalServerUrl As String = "http://localhost:54321/iBahnCreditCardListener/"
#End Region

    Public Sub New()
        mCurrencyCodeCollection = New Collection
        mCurrencyCodeCollection.Add("EUR")
        mCurrencyCodeCollection.Add("GBP")
        mCurrencyCodeCollection.Add("USD")

        mSwipeData = ""

        mErrors = New Errors

        mSuccess = False
        mForceVoid = False
    End Sub

#Region "Properties"
    Public Property Errors() As Errors
        Get
            Return mErrors
        End Get
        Set(ByVal value As Errors)
            mErrors = value
        End Set
    End Property

    Public Property Debugging() As Boolean
        Get
            Return mDebug
        End Get
        Set(ByVal value As Boolean)
            mDebug = value
        End Set
    End Property

    Public Property ForceVoid() As Boolean
        Get
            Return mForceVoid
        End Get
        Set(ByVal value As Boolean)
            mForceVoid = value
        End Set
    End Property

    Public Property User() As String
        Get
            Return mUser
        End Get
        Set(ByVal Value As String)
            mUser = Value
        End Set
    End Property

    Public Property Vendor() As String
        Get
            Return mVendor
        End Get
        Set(ByVal Value As String)
            mVendor = Value
        End Set
    End Property

    Public Property Partner() As String
        Get
            Return mPartner
        End Get
        Set(ByVal Value As String)
            mPartner = Value
        End Set
    End Property

    Public Property PassWord() As String
        Get
            Return mPassWord
        End Get
        Set(ByVal Value As String)
            mPassWord = Value
        End Set
    End Property

    Public Property SwipeData() As String
        Get
            Return mSwipeData
        End Get
        Set(ByVal Value As String)
            mSwipeData = Value
        End Set
    End Property

    Public Property Comment1() As String
        Get
            Return mComment1
        End Get
        Set(ByVal Value As String)
            mComment1 = Value
        End Set
    End Property

    Public Property Comment2() As String
        Get
            Return mComment2
        End Get
        Set(ByVal Value As String)
            mComment2 = Value
        End Set
    End Property

    Public Property Server() As String
        Get
            Return mServer
        End Get
        Set(ByVal Value As String)
            mServer = Value
        End Set
    End Property

    Public Property Port() As Integer
        Get
            Return mPort
        End Get
        Set(ByVal Value As Integer)
            mPort = Value
        End Set
    End Property

    Public Property Timeout() As Integer
        Get
            Return mTimeout
        End Get
        Set(ByVal Value As Integer)
            mTimeout = Value
        End Set
    End Property

    Public Property CreditCardName() As String
        Get
            Return mCreditCardName
        End Get
        Set(ByVal Value As String)
            mCreditCardName = Value
        End Set
    End Property

    Public Property CreditCardNumber() As String
        Get
            Return mCreditCardNumber
        End Get
        Set(ByVal Value As String)
            mCreditCardNumber = Value
        End Set
    End Property

    Public Property CreditCardExpiry() As String
        Get
            Return mCreditCardExpiry
        End Get
        Set(ByVal Value As String)
            mCreditCardExpiry = Value
        End Set
    End Property

    Public Property CreditCardSecurityCode() As String
        Get
            Return mCreditCardSecurityCode
        End Get
        Set(ByVal Value As String)
            mCreditCardSecurityCode = Value
        End Set
    End Property

    Public Property CurrencyCode() As String
        Get
            Return mCurrencyCode
        End Get
        Set(ByVal Value As String)
            Dim i As Integer

            mCurrencyCode = "USD"

            For i = 1 To mCurrencyCodeCollection.Count
                If CType(mCurrencyCodeCollection.Item(i), String) = Value Then
                    mCurrencyCode = Value
                End If
            Next
        End Set
    End Property

    Public Property MerchantDescription() As String
        Get
            Return mMerchantDescription
        End Get
        Set(ByVal Value As String)
            mMerchantDescription = Value
        End Set
    End Property

    Public Property MerchantServiceEmail() As String
        Get
            Return mMerchantServiceEmail
        End Get
        Set(ByVal Value As String)
            mMerchantServiceEmail = Value
        End Set
    End Property

    Public Property Amount() As Decimal
        Get
            Return Amount
        End Get
        Set(ByVal Value As Decimal)
            mAmount = Value
        End Set
    End Property

    Public ReadOnly Property ResponseCode() As Integer
        Get
            Return mResponseCode
        End Get
    End Property

    Public ReadOnly Property ResponseHumanMessage() As String
        Get
            Return mResponseHumanMessage
        End Get
    End Property

    Public ReadOnly Property ResponseInternalMessage() As String
        Get
            Return mResponseInternalMessage
        End Get
    End Property

    Public ReadOnly Property ResponseText() As String
        Get
            Return mResponseText
        End Get
    End Property

    Public ReadOnly Property Success() As Boolean
        Get
            Return mSuccess
        End Get
    End Property
#End Region

#Region "Privates"
    Private Sub _SetUserInfo()
        oUser = New UserInfo(mUser, mVendor, mPartner, mPassWord)
    End Sub

    Private Sub _SetConnection()
        If mDebug Then
            WriteLogEntry("Connection", LogType.Debug)
            WriteLogEntry("  server =" & mServer, LogType.Debug)
            WriteLogEntry("  port   =" & mPort, LogType.Debug)
            WriteLogEntry("  timeout=" & mTimeout, LogType.Debug)
        End If
        oConnection = New PayflowConnectionData(mServer, mPort, mTimeout)
    End Sub

    Private Sub _SetInvoice()
        oInvoice = New Invoice

        If mDebug Then
            WriteLogEntry("Invoice", LogType.Debug)
            WriteLogEntry("  MerchDescr=" & mMerchantDescription, LogType.Debug)
            WriteLogEntry("  MerchSvc  =" & mMerchantServiceEmail, LogType.Debug)
            WriteLogEntry("  Comment1  =" & mComment1, LogType.Debug)
            WriteLogEntry("  Comment2  =" & mComment2, LogType.Debug)
            WriteLogEntry("  Amt", LogType.Debug)
            WriteLogEntry("    Value=" & CStr(mAmount), LogType.Debug)
            WriteLogEntry("    Code =" & mCurrencyCode, LogType.Debug)
        End If

        oInvoice.Amt = New Currency(mAmount, mCurrencyCode)
        oInvoice.Amt.NoOfDecimalDigits = 2
        oInvoice.Amt.Round = True

        oInvoice.MerchDescr = mMerchantDescription
        oInvoice.MerchSvc = mMerchantServiceEmail
        oInvoice.Comment1 = mComment1
        oInvoice.Comment2 = mComment2
    End Sub

    Private Sub _CardDetailsManual()
        oCreditCard = New CreditCard(mCreditCardNumber, mCreditCardExpiry)
        oCreditCard.Cvv2 = mCreditCardSecurityCode
        oCreditCard.Name = mCreditCardName

        bCC_Manual = True
    End Sub

    Private Sub _CardDetailsSwipe()
        oSwipeCard = New SwipeCard(mSwipeData)

        bCC_Manual = False
    End Sub

    Private Sub _CardTender()
        If bCC_Manual Then
            oCardTender = New CardTender(oCreditCard)
        Else
            oCardTender = New CardTender(oSwipeCard)
        End If
    End Sub

    Private Sub _DoTransaction()
        _AuthorizationTransaction()
    End Sub

    Private Sub _AuthorizationTransaction()
        Dim oTransaction As AuthorizationTransaction
        Dim sRespMsg As String = ""
        Dim iResult As Integer
        Dim oResponse As Response
        Dim oTransactionResponse As TransactionResponse
        Dim sRequestId As String = PayflowUtility.RequestId

        oTransaction = New AuthorizationTransaction(oUser, oConnection, oInvoice, oCardTender, sRequestId)
        oTransaction.Verbosity = "MEDIUM"

        WriteLogEntry("AuthorizationTransaction")

        If mDebug Then
            WriteLogEntry("  RequestId=" & sRequestId, LogType.Debug)
        End If

        oResponse = oTransaction.SubmitTransaction()

        If Not (oResponse Is Nothing) Then
            oTransactionResponse = oResponse.TransactionResponse

            mPnref = oTransactionResponse.Pnref
            mCVV2Match = oTransactionResponse.CVV2Match
            iResult = oTransactionResponse.Result
            sRespMsg = oTransactionResponse.RespMsg

            If mDebug Then
                WriteLogEntry("  Pnref    =" & mPnref, LogType.Debug)
            End If

            WriteLogEntry("  CVV2Match=" & mCVV2Match)
            WriteLogEntry("  Result   =" & iResult)
            WriteLogEntry("  RespText =" & oTransactionResponse.RespText)
            WriteLogEntry("  RespMsg  =" & sRespMsg)

            mOriginalResponseCode = iResult
            mOriginalResponseMsg = sRespMsg

            'If the auth transaction was successful (iResult=0), we still have
            'check if the CVC code was correct. If not, we are voiding the transaction
            'and forcing an 114 error, which means incorrect CVC.
            'If the CVC code is correct, or if PayPal didn't care about the CVC, then
            'we take the money.
            If iResult = 0 Then
                If mCVV2Match = "N" Then
                    _VoidTransaction()
                Else
                    _CaptureTransaction()
                End If

                Exit Sub
            End If

            mResponseCode = iResult
            mResponseText = oTransactionResponse.RespText
            _GetErrorMessage(iResult, sRespMsg, mResponseInternalMessage, mResponseHumanMessage)
        Else
            WriteLogEntry("  Response is Null", LogType.Fail)
        End If
    End Sub

    Private Sub _CaptureTransaction()
        Dim oTransaction As CaptureTransaction
        Dim sRespMsg As String = ""
        Dim iResult As Integer
        Dim oResponse As Response
        Dim oTransactionResponse As TransactionResponse
        Dim sRequestId As String = PayflowUtility.RequestId

        oTransaction = New CaptureTransaction(mPnref, oUser, oConnection, sRequestId)
        oTransaction.Verbosity = "MEDIUM"

        WriteLogEntry("CaptureTransaction")

        If mDebug Then
            WriteLogEntry("  RequestId=" & sRequestId, LogType.Debug)
        End If

        oResponse = oTransaction.SubmitTransaction()

        If Not (oResponse Is Nothing) Then
            oTransactionResponse = oResponse.TransactionResponse

            iResult = oTransactionResponse.Result
            sRespMsg = oTransactionResponse.RespMsg

            WriteLogEntry("  Result   =" & iResult)
            WriteLogEntry("  RespText =" & oTransactionResponse.RespText)
            WriteLogEntry("  RespMsg  =" & sRespMsg)

            mResponseCode = iResult
            mResponseText = oTransactionResponse.RespText
            _GetErrorMessage(iResult, sRespMsg, mResponseInternalMessage, mResponseHumanMessage)
        Else
            WriteLogEntry("  Response is Null", LogType.Fail)
        End If
    End Sub

    Private Sub _VoidTransaction()
        Dim oVoidTransaction As VoidTransaction
        Dim sRespMsg As String = ""
        Dim iResult As Integer
        Dim oVoidResponse As Response
        Dim oVoidTransactionResponse As TransactionResponse
        Dim sRequestId As String = PayflowUtility.RequestId

        oVoidTransaction = New VoidTransaction(mPnref, oUser, oConnection, sRequestId)
        oVoidTransaction.Verbosity = "MEDIUM"

        WriteLogEntry("VoidTransaction")

        If mDebug Then
            WriteLogEntry("  RequestId=" & sRequestId, LogType.Debug)
        End If

        oVoidResponse = oVoidTransaction.SubmitTransaction()

        If Not (oVoidResponse Is Nothing) Then
            oVoidTransactionResponse = oVoidResponse.TransactionResponse

            WriteLogEntry("  Result   =" & oVoidTransactionResponse.Result)
            WriteLogEntry("  RespText =" & oVoidTransactionResponse.RespText)
            WriteLogEntry("  RespMsg  =" & oVoidTransactionResponse.RespMsg)

            iResult = oVoidTransactionResponse.Result
            sRespMsg = oVoidTransactionResponse.RespMsg

            If iResult = 0 Then
                iResult = mOriginalResponseCode
                sRespMsg = mOriginalResponseMsg
            End If

            mResponseCode = iResult
            mResponseText = oVoidTransactionResponse.RespText
            _GetErrorMessage(iResult, sRespMsg, mResponseInternalMessage, mResponseHumanMessage)
        Else
            WriteLogEntry("  VoidResponse is Null", LogType.Fail)
        End If
    End Sub

    Private Sub _GetErrorMessage(ByVal ResultCode As Integer, ByVal ResultMessage As String, ByRef sInternalError As String, ByRef sHumanError As String)
        If Not mErrors.GetErrorMessage(ResultCode, sInternalError, sHumanError) Then
            sInternalError = "Unknown error: " & ResultMessage
            sHumanError = "Internal error (# " & CStr(ResultCode) & ")"
        End If
    End Sub

    Private Function _StringToDecimal(ByVal s As String) As Decimal
        Dim d As Decimal
        Dim sNumberDecimalSeparator As String

        If s.Equals("") Then s = "0"

        sNumberDecimalSeparator = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator

        If sNumberDecimalSeparator.Equals(".") Then
            s = s.Replace(",", ".")
        Else
            s = s.Replace(".", ",")
        End If

        Try
            d = Decimal.Parse(s)
        Catch ex As Exception
            d = New Decimal(0)
        End Try

        Return d
    End Function
#End Region

#Region "Publics"
    Public Sub ProcessCreditCardRequest()
        Dim sResponseString As String = ""
        Dim listener As New System.Net.HttpListener
        Dim context As System.Net.HttpListenerContext
        Dim request As System.Net.HttpListenerRequest
        Dim response As System.Net.HttpListenerResponse = Nothing
        Dim body As System.IO.Stream
        Dim bodytext As String
        Dim encoding As System.Text.Encoding
        Dim reader As System.IO.StreamReader
        Dim qString As New System.Collections.Specialized.NameValueCollection

        mSuccess = False

        Try
            listener.Prefixes.Add(LocalServerUrl)
            listener.Start()
            WriteLogEntry("started listener")
            If mDebug Then
                WriteLogEntry("listening to " & LocalServerUrl, LogType.Debug)
            End If

            context = listener.GetContext()
            request = context.Request
            response = context.Response

            WriteLogEntry("received auth request")

            body = context.Request.InputStream
            encoding = context.Request.ContentEncoding
            reader = New System.IO.StreamReader(body, encoding)
            bodytext = reader.ReadToEnd()

            If mDebug Then
                WriteLogEntry(bodytext, LogType.Debug)
            End If

            qString = System.Web.HttpUtility.ParseQueryString(bodytext, encoding)

            Dim oXor As New XOrObfuscation()
            Dim sTmp As String

            sTmp = qString.Item("enc")
            If sTmp = "Y" Then
                mCreditCardName = oXor.Deobfuscate(qString.Item("cname"))
                mCreditCardNumber = oXor.Deobfuscate(qString.Item("cnum"))
                mCreditCardExpiry = oXor.Deobfuscate(qString.Item("cdate"))
                mCreditCardSecurityCode = oXor.Deobfuscate(qString.Item("cvc"))
                sTmp = oXor.Deobfuscate(qString.Item("amount"))
                mAmount = _StringToDecimal(sTmp)
                mCurrencyCode = oXor.Deobfuscate(qString.Item("currency"))
            Else
                mCreditCardName = qString.Item("cname")
                mCreditCardNumber = qString.Item("cnum")
                mCreditCardExpiry = qString.Item("cdate")
                mCreditCardSecurityCode = qString.Item("cvc")
                sTmp = qString.Item("amount")
                mAmount = _StringToDecimal(sTmp)
                mCurrencyCode = qString.Item("currency")
            End If

            If mCreditCardName Is Nothing Then
                mCreditCardName = "Null null"
            End If

            If mCreditCardName.Equals("") Then
                mCreditCardName = "Name not filled in"
            End If

            If mCreditCardNumber Is Nothing Then
                mCreditCardNumber = "000000000000"
            End If

            If mCreditCardNumber.Equals("") Then
                mCreditCardNumber = "000000000000"
            End If

            If mCreditCardExpiry Is Nothing Then
                mCreditCardExpiry = "000"
            End If

            If mCreditCardExpiry.Equals("") Then
                mCreditCardExpiry = "000"
            End If

            If mCreditCardSecurityCode Is Nothing Then
                mCreditCardSecurityCode = "xxxx"
            End If

            If mCreditCardSecurityCode.Equals("") Then
                mCreditCardSecurityCode = "xxxx"
            End If

            WriteLogEntry("details")
            If mDebug Then
                WriteLogEntry("  name  =" & mCreditCardName, LogType.Debug)
            End If
            WriteLogEntry("  number=" & Microsoft.VisualBasic.Left(mCreditCardNumber, 4) & (New String("*", mCreditCardNumber.Length - 8)) & Microsoft.VisualBasic.Right(mCreditCardNumber, 4))
            If mDebug Then
                WriteLogEntry("  expiry=" & mCreditCardExpiry, LogType.Debug)
            End If
            If mDebug Then
                WriteLogEntry("  cvc   =" & mCreditCardSecurityCode, LogType.Debug)
            End If
            WriteLogEntry("  amount=" & mCurrencyCode & " " & CStr(mAmount))

            _SetUserInfo()
            _SetConnection()
            _SetInvoice()
            If mSwipeData.Equals("") Then
                _CardDetailsManual()
            Else
                _CardDetailsSwipe()
            End If
            _CardTender()
            _DoTransaction()
        Catch ex As Exception
            mResponseCode = -1
            mResponseInternalMessage = ex.Message
            mResponseHumanMessage = "Internal error (# -100)"
            mResponseText = ex.StackTrace

            WriteLogEntry("FAIL", LogType.Fail)
            WriteLogEntry("  ex.Message   =" & ex.Message, LogType.Fail)
            WriteLogEntry("  ex.StackTrace=" & ex.StackTrace, LogType.Fail)
            WriteLogEntry("  ex.Source    =" & ex.Source, LogType.Fail)
        End Try

        WriteLogEntry("Response to client")
        WriteLogEntry("  code            =" & mResponseCode)
        WriteLogEntry("  internal message=" & mResponseInternalMessage)
        WriteLogEntry("  human message   =" & mResponseHumanMessage)
        WriteLogEntry("  text            =" & mResponseText)

        sResponseString = mResponseCode & "|" & mResponseHumanMessage & "|" & mResponseInternalMessage & "|" & mResponseText & "|" & mCVV2Match & "|" & mPnref

        If mDebug Then
            WriteLogEntry("  full response   =" & sResponseString, LogType.Debug)
        End If

        Try
            Dim buffer As Byte()

            WriteLogEntry("sending response to client")

            buffer = System.Text.Encoding.UTF8.GetBytes(sResponseString)

            If response Is Nothing Then
                Throw New Exception("Response object is null")
            End If

            response.ContentLength64 = buffer.Length

            Dim s As System.IO.Stream = response.OutputStream
            s.Write(buffer, 0, buffer.Length)
            s.Close()

            listener.Stop()

            WriteLogEntry("sent")
            mSuccess = True
        Catch ex As Exception
            WriteLogEntry("sending response FAIL!!!", LogType.Fail)
            WriteLogEntry("  ex.Message   =" & ex.Message, LogType.Fail)
            WriteLogEntry("  ex.StackTrace=" & ex.StackTrace, LogType.Fail)
        End Try
    End Sub
#End Region
End Class
