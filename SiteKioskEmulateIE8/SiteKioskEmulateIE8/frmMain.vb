Public Class frmMain

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Visible = False

        Try
            ' [HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION]
            ' "SiteKiosk.exe"=dword:000022b8

            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", "SiteKiosk.exe", 8888) '22b8 hex = 8888 dec
        Catch ex As Exception
            ' Insert fancy error handling. Or not.
        Finally
            Environment.Exit(0)
        End Try
    End Sub
End Class
