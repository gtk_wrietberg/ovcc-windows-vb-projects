﻿Public Class Globals
    Public Shared oLogger As Logger
    Public Shared oComputerName As ComputerName

    Public Shared g_ProgFilesDirectory As String = ""
    Public Shared g_LogFileDirectory As String = ""
    Public Shared g_LogFileName As String = ""
    Public Shared g_LMI_LogFileName As String = ""
    Public Shared g_BackupDirectory As String = ""

    Public Shared g_InstallDirectory As String = ""

    Private Shared ReadOnly cLogMeIn_Path_Base As String = "%%PROGRAM FILES%%\GuestTek\installation_files\LogMeIn"



    Public Shared Sub InitGlobals()
        Dim dDate As Date = Now()


        g_ProgFilesDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & " (x86)"

        If Not IO.Directory.Exists(g_ProgFilesDirectory) Then
            g_ProgFilesDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        g_LogFileDirectory = g_ProgFilesDirectory & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"
        g_LMI_LogFileName = "LMI_" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = g_ProgFilesDirectory & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        g_InstallDirectory = cLogMeIn_Path_Base.Replace("%%PROGRAM FILES%%", g_ProgFilesDirectory)


        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub

End Class
