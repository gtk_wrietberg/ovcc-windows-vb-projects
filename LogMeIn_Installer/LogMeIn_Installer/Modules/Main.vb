
Module Main
    Private WithEvents oProcess As ProcessRunner

    Private ReadOnly cParamName_DeployId As String = "deployid="

    ' Private ReadOnly cLogMeIn_Path As String = "C:\Program Files\iBAHN\installation_files\LogMeIn\files\EngineeringLogMeIn.msi"
    Private ReadOnly cLogMeIn_Path_MSI As String = "\files\LogMeIn.msi"

    Public Sub Main()
        Globals.InitGlobals()

        Globals.oLogger = New Logger

        Globals.oLogger.LogFileDirectory = Globals.g_LogFileDirectory
        Globals.oLogger.LogFileName = Globals.g_LogFileName

        Globals.oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        Globals.oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)



        '--------------------
        oProcess = New ProcessRunner
        Globals.oComputerName = New ComputerName


        'Command line params
        Dim sDeployId As String = ""

        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, cParamName_DeployId) > 0 Then
                sDeployId = arg.Replace(cParamName_DeployId, "")
            End If
        Next

        Globals.oLogger.WriteToLog("Initializing", , 0)
        If sDeployId = "" Then
            Globals.oLogger.WriteToLog("Deploy Id is empty!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            ExitApplication(1)
        Else
            Globals.oLogger.WriteToLog("Deploying with Deploy Id: " & sDeployId, , 1)
        End If


        If Not IO.File.Exists(Globals.g_InstallDirectory & cLogMeIn_Path_MSI) Then
            Globals.oLogger.WriteToLog("Msi file not found!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Globals.oLogger.WriteToLog(Globals.g_InstallDirectory & cLogMeIn_Path_MSI, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            ExitApplication(2)
        End If


        Dim sParams As String

        'msiexec.exe /i EngineeringLogMeIn.msi /quiet DEPLOYID=01_ly2bqho5b6exj49y4hdi5s392c55sd48ebpxx INSTALLMETHOD=5 FQDNDESC=1
        sParams = "/i """ & Globals.g_InstallDirectory & cLogMeIn_Path_MSI & """ /quiet DEPLOYID=" & sDeployId & " INSTALLMETHOD=5 FQDNDESC=1 /l* """ & Globals.g_LogFileDirectory & "\" & Globals.g_LMI_LogFileName & """"

        Globals.oLogger.WriteToLog("Installing LogMeIn")
        Globals.oLogger.WriteToLog("Path   : msiexec.exe", , 1)
        Globals.oLogger.WriteToLog("Params : " & sParams, , 1)
        Globals.oLogger.WriteToLog("Timeout: 1800", , 1)


        oProcess = New ProcessRunner
        oProcess.FileName = "msiexec.exe"
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1800
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()

        Do
            System.Threading.Thread.Sleep(1000)
        Loop Until oProcess.IsProcessDone

        If oProcess.ProcessTimedOut Then
            ExitApplication(3)
        End If

        If oProcess.ErrorsOccurred Then
            ExitApplication(4)
        End If

        ExitApplication(0)
    End Sub

    Public Sub ExitApplication(ByVal iExitCode As Integer)
        Try
            Globals.oLogger.WriteToLog("Exiting...", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
            Globals.oLogger.WriteToLog("exit code: " & iExitCode & " (" & iExitCode.ToString & ")", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            Globals.oLogger.WriteToLog("bye", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            Globals.oLogger.WriteToLogWithoutDate(New String("=", 100))
        Catch ex As Exception
        End Try

        System.Environment.Exit(iExitCode)
    End Sub

    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        Globals.oLogger.WriteToLog("done", , 1)
        Globals.oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        Globals.oLogger.WriteToLog("failed", , 1)
        Globals.oLogger.WriteToLog(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        Globals.oLogger.WriteToLog("started", , 1)
        Globals.oLogger.WriteToLog("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        Globals.oLogger.WriteToLog("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
End Module
