Public Class ServiceThread
    Private tMain As System.Threading.Thread
    Private bStop As Boolean = False
    Private rGen As New Random(Now.Millisecond)

    Public Sub StopWork()
        bStop = True
        If Not tMain Is Nothing Then
            If Not tMain.Join(100) Then
                tMain.Abort()
            End If
        End If
    End Sub

    Public Sub StartWork()
        tMain = System.Threading.Thread.CurrentThread
        Dim i As Integer = rGen.Next
        tMain.Name = "Thread" & i.ToString

        While Not bStop
            WriteLogEntry("Start work: " & tMain.Name, LogType.Debug)
            System.Threading.Thread.Sleep(10000)
            WriteLogEntry("Finished work: " & tMain.Name, LogType.Debug)
        End While
    End Sub
End Class
