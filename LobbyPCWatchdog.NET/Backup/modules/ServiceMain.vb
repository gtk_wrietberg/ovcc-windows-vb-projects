Imports System.ServiceProcess

Public Class LobbyPCWatchdogService
    Inherits System.ServiceProcess.ServiceBase

    Private ServiceWorker As New ServiceThread


#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New LobbyPCWatchdogService}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'LobbyPCWatchdogService
        '
        Me.ServiceName = "LobbyPCWatchdogService"

    End Sub

#End Region

    Protected Overrides Sub OnStart(ByVal args() As String)
        System.IO.Directory.SetCurrentDirectory(GetMyDir)

        Dim wt As System.Threading.Thread
        Dim ts As System.Threading.ThreadStart

        ts = AddressOf ServiceWorker.StartWork
        wt = New System.Threading.Thread(ts)
        wt.Start()
    End Sub

    Protected Overrides Sub OnStop()
        ServiceWorker.StopWork()
    End Sub

    Private Function GetMyDir() As String
        Dim fi As System.IO.FileInfo
        Dim di As System.IO.DirectoryInfo
        Dim pc As System.Diagnostics.Process
        Try
            pc = System.Diagnostics.Process.GetCurrentProcess
            fi = New System.IO.FileInfo(pc.MainModule.FileName)
            di = fi.Directory
            GetMyDir = di.FullName
        Finally
            fi = Nothing
            di = Nothing
            pc = Nothing
        End Try
    End Function
End Class
