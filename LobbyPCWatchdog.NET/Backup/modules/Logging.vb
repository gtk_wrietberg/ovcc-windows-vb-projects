Module Logging
    Private ReadOnly LogBasePath As String = "c:\ibahn\LPCWatchdog.NET\logs\"

    Public Enum LogType
        Notification = 0
        Debug = 1
        Warning = 2
        Fail = 3
    End Enum

    Public Sub WriteLogEntry(ByVal msg As String, Optional ByVal Type As LogType = LogType.Notification)
        Dim sPrefix As String = ""
        Dim sLogFileName As String
        Dim sLogDate As String
        Dim sLogPath As String
        Dim dDate As Date

        'sLogBasePath = System.AppDomain.CurrentDomain.BaseDirectory & "logs\"

        dDate = Now

        sLogDate = dDate.ToString("yyyy\\MM")
        sLogPath = LogBasePath & sLogDate & "\"

        Try
            If Not IO.Directory.Exists(sLogPath) Then
                IO.Directory.CreateDirectory(sLogPath)
            End If
        Catch ex As Exception

        End Try

        Select Case Type
            Case LogType.Notification
                sPrefix = "[.]"
            Case LogType.Warning
                sPrefix = "[!]"
            Case LogType.Fail
                sPrefix = "[X]"
            Case LogType.Debug
                sPrefix = "[d]"
            Case Else
                sPrefix = "[?]"
        End Select

        Try
            Dim LogWriter As System.IO.StreamWriter

            If IO.Directory.Exists(sLogPath) Then
                sLogFileName = dDate.ToString("dd") & ".txt"
                LogWriter = New System.IO.StreamWriter(sLogPath & sLogFileName, True)
            Else
                sLogFileName = dDate.ToString("yyyy-MM-dd") & ".txt"
                LogWriter = New System.IO.StreamWriter(LogBasePath & sLogFileName, True)
            End If

            LogWriter.WriteLine(sPrefix & " " & Now.ToString("s").Replace("T", " ") & " - " & msg)
            LogWriter.Close()
        Catch ex As Exception
            'Too bad, logging would have been nice
        End Try
    End Sub
End Module
