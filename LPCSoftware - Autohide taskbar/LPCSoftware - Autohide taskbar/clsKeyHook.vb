Imports System.Runtime.InteropServices

Public Class clsKeyHook
#Region "WIN API FUNCTIONS"
    <DllImport("user32.dll")> Private Shared Function GetKeyState(ByVal nVirtKey As Long) As Integer
    End Function
#End Region

    Private WithEvents apiLink As EventVB.APIFunctions
    Private WithEvents systemHook As EventVB.ApiSystemHook

    Private Const vbKeyLWin As Integer = 91
    Private Const vbKeyRWin As Integer = 92

    Private Const KEY_DOWN As Integer = &HF0000000

    Public Event KeyBlocked(ByVal VKey As Integer)

    Public Sub New()

    End Sub

    Public Sub Hook()
        apiLink = New EventVB.APIFunctions
        systemHook = apiLink.System.Hooks

        systemHook.StartHook(EventVB.enHookTypes.WH_KEYBOARD_LL, EventVB.HookScopeSettings.HOOK_GLOBAL)
    End Sub

    Public Sub Unhook()
        systemHook.StopHook(EventVB.enHookTypes.WH_KEYBOARD_LL)

        systemHook = Nothing
        apiLink = Nothing
    End Sub

    Private Sub systemHook_KeyDown(ByVal VKey As Integer, ByVal scanCode As Integer, ByVal ExtendedKey As Boolean, ByVal AltDown As Boolean, ByVal Injected As Boolean, ByRef Cancel As Boolean) Handles systemHook.KeyDown
        If VKey = vbKeyLWin Or VKey = vbKeyRWin Then
            Cancel = True
        End If

        If GetKeyState(vbKeyLWin) And KEY_DOWN Or _
           GetKeyState(vbKeyRWin) And KEY_DOWN Then
            Cancel = True
        End If

        If Cancel Then RaiseEvent KeyBlocked(VKey)
    End Sub
End Class
