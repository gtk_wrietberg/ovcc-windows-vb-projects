Public Class frmHidden
    Private oTaskbar As clsTaskBar

    Private Sub frmHidden_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.Visible = False
    End Sub

    Private Sub frmHidden_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        oTaskbar.ShowAll()
    End Sub

    Private Sub frmHidden_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        oTaskbar.ShowAll()
    End Sub

    Private Sub frmHidden_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Opacity = 0.0

        PreventMultipleInstances()


        oTaskbar = New clsTaskBar

        oTaskbar.HideAll()

        txtHWnd.Text = oTaskbar.HWnd_Desktop.ToString() & " ; " & oTaskbar.HWnd_StartButton.ToString() & " ; " & oTaskbar.HWnd_TaskBar.ToString()

        tmrStartChecking.Enabled = True
    End Sub

    Private Sub tmrStartChecking_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrStartChecking.Tick
        tmrStartChecking.Enabled = False

        tmrCheckForSiteKiosk.Enabled = True
    End Sub

    Private Sub tmrCheckForSiteKiosk_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrCheckForSiteKiosk.Tick
        If IsProcessRunning(cProcessNameToCheck) Then
            oTaskbar.HideAll()
        Else
            oTaskbar.ShowAll()
        End If
    End Sub
End Class
