<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHidden
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.tmrCheckForSiteKiosk = New System.Windows.Forms.Timer(Me.components)
        Me.tmrStartChecking = New System.Windows.Forms.Timer(Me.components)
        Me.txtHWnd = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'tmrCheckForSiteKiosk
        '
        Me.tmrCheckForSiteKiosk.Interval = 5000
        '
        'tmrStartChecking
        '
        Me.tmrStartChecking.Interval = 30000
        '
        'txtHWnd
        '
        Me.txtHWnd.Location = New System.Drawing.Point(37, 23)
        Me.txtHWnd.Name = "txtHWnd"
        Me.txtHWnd.Size = New System.Drawing.Size(204, 21)
        Me.txtHWnd.TabIndex = 0
        '
        'frmHidden
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 254)
        Me.Controls.Add(Me.txtHWnd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHidden"
        Me.Opacity = 0
        Me.ShowInTaskbar = False
        Me.Text = "LPCSoftware - Autohide taskbar"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tmrCheckForSiteKiosk As System.Windows.Forms.Timer
    Friend WithEvents tmrStartChecking As System.Windows.Forms.Timer
    Friend WithEvents txtHWnd As System.Windows.Forms.TextBox

End Class
