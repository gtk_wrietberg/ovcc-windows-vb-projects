Module modMain
    Private oTaskbar As clsTaskBar
    Private oKeyHook As clsKeyHook

    Private iDelay As Integer

    Private ReadOnly MaxDelay As Integer = 60

    Public Sub Main()
        oTaskbar = New clsTaskBar
        oKeyHook = New clsKeyHook

        gDebug = False
        gDebugPlus = False
        gEmergencyShow = False

        iDelay = 0


        For Each arg As String In Environment.GetCommandLineArgs()
            If arg = "--debug" Then
                gDebug = True
            End If
            If arg = "--debug-plus" Then
                gDebugPlus = True
            End If
            If arg = "--emergency" Or arg = "--show-all" Or arg = "--showall" Or arg = "--emergency-show" Or arg = "--oops" Then
                gEmergencyShow = True
            End If
        Next

        If gEmergencyShow Then
            oTaskbar.ShowAll()

            Exit Sub
        End If



        Do While True
            If IsSitekioskUserLoggedIn() Then
                If IsSitekioskRunning() Then
                    iDelay = 0
                    oTaskbar.HideAll()
                Else
                    iDelay += 1
                    If iDelay >= MaxDelay Then
                        iDelay = MaxDelay
                        oTaskbar.ShowAll()
                    Else
                        oTaskbar.HideAll()
                    End If
                End If
            Else
                If IsSitekioskRunning() Then
                    oTaskbar.HideAll()
                Else
                    oTaskbar.ShowAll()
                End If
            End If

            System.Threading.Thread.Sleep(1000)
        Loop
    End Sub

    Private Function IsSitekioskRunning() As Boolean
        Dim Processes() As System.Diagnostics.Process = System.Diagnostics.Process.GetProcessesByName("sitekiosk")

        If Processes.Length > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function IsSitekioskUserLoggedIn() As Boolean
        Try
            Dim sUserName As String

            sUserName = System.Environment.UserName

            If sUserName.ToLower = "sitekiosk" Then
                Return True
            End If

        Catch ex As Exception

        End Try
        
        Return False
    End Function
End Module
