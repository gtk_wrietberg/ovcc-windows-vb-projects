Module modMain
    Private oTaskbar As clsTaskBar

    Public Sub Main()
        PreventMultipleInstances()

        oLogger = New clsLogger
        oTaskbar = New clsTaskBar

        gDebug = False
        gDebugPlus = False
        gEmergencyShow = False
        gVisible = False

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg = "--debug" Then
                gDebug = True
            End If
            If arg = "--debug-plus" Then
                gDebugPlus = True
            End If
            If arg = "--emergency" Or arg = "--show-all" Or arg = "--showall" Or arg = "--emergency-show" Or arg = "--oops" Then
                gEmergencyShow = True
            End If
        Next

        oLogger.Enabled = gDebug
        oLogger.ShowMessageBox = gDebugPlus


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(New String("*", 50))
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        If gEmergencyShow Then
            oLogger.WriteToLog("Showing everything, just to be sure", , 0)
            oTaskbar.ShowAll()

            Exit Sub
        End If

        If Environment.UserName.ToLower() <> "sitekiosk" Then
            oLogger.WriteToLog("Not running as SiteKiosk user, exiting...", , 0)
            Exit Sub
        Else
            oLogger.WriteToLog("Hiding everything, until further notice", , 0)
            oTaskbar.HideAll()
            gVisible = False
        End If

        oLogger.WriteToLog("Delay", , 0)
        System.Threading.Thread.Sleep(60000)

        gLoop = True
        Do While gLoop
            System.Threading.Thread.Sleep(10000)
            ToggleShowHide()
        Loop
    End Sub

    Private Sub ToggleShowHide()
        If IsProcessRunning(cProcessName_Emergency) Or Not gLoop Then
            gLoop = False

            Exit Sub
        End If

        If IsProcessRunning(cProcessName_SiteKiosk) Then
            If gVisible Then
                oLogger.WriteToLog("hide all", , 1)
                oTaskbar.HideAll()
            End If

            gVisible = False
        Else
            If Not gVisible Then
                oLogger.WriteToLog("show all", , 1)
                oTaskbar.ShowAll()
            End If

            gVisible = True
        End If
    End Sub
End Module
