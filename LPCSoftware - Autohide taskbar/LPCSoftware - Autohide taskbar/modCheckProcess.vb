Module modCheckProcess
    Public Function IsProcessRunning(ByVal PartOfProcessName As String) As Boolean
        Dim oProcess As Process

        For Each oProcess In Process.GetProcesses
            If oProcess.ProcessName.Contains(PartOfProcessName) Then
                Return True
            End If
        Next

        Return False
    End Function
End Module
