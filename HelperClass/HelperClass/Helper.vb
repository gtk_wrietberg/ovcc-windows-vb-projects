﻿Imports System.Security.Principal
Imports Microsoft.Win32
Imports System.Security.AccessControl
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement

Public Class Helper
    Public Class Initialise
        Private mPassphrase As String = ""
        Private mLocked As Boolean = True
        Public Property Locked() As Boolean
            Get
                Return mLocked
            End Get
            Set(ByVal value As Boolean)
                mLocked = value
            End Set
        End Property

        Public Shared Sub Unlock(Passphrase As String)

        End Sub

        Private Shared Sub Test123(param1 As String)

        End Sub

        Public Shared Sub Test123(param2 As Integer)

        End Sub
    End Class

    Public Class Errors
        Private Class ErrorData
            Private mError_Timestamp As Date
            Private mError_Description As String
            Private mError_Caller As String

            Public Property Timestamp() As Date
                Get
                    Return mError_Timestamp
                End Get
                Set(ByVal value As Date)
                    mError_Timestamp = value
                End Set
            End Property

            Public Property Description() As String
                Get
                    Return mError_Description
                End Get
                Set(ByVal value As String)
                    mError_Description = value
                End Set
            End Property

            Public Property Caller() As String
                Get
                    Return mError_Caller
                End Get
                Set(ByVal value As String)
                    mError_Caller = value
                End Set
            End Property

            Public Overloads Function ToString() As String
                Dim sResult As String = ""

                sResult = mError_Timestamp.ToString & " - " & mError_Description & " - " & mError_Caller

                Return sResult
            End Function
        End Class

        Private Shared mMaxHistory As Integer = 25
        Private Shared lstErrors As New List(Of ErrorData)(mMaxHistory)

        Public Shared Sub Add(Description As String)
            Dim newError As New ErrorData

            newError.Description = Description
            newError.Timestamp = Date.Now

            Dim stackTrace As New StackTrace()

            newError.Caller = stackTrace.GetFrame(1).GetMethod().Name


            lstErrors.Insert(0, newError)

            If lstErrors.Count > mMaxHistory Then
                lstErrors.RemoveRange(mMaxHistory, lstErrors.Count - mMaxHistory)
            End If
        End Sub

        Public Overloads Shared Function ToString(Optional AllErrors As Boolean = False) As String
            Dim sResult As String = ""
            Dim currentError As New ErrorData

            If lstErrors.Count = 0 Then
                Return ""
            End If

            If lstErrors.Count = 1 Then
                AllErrors = False
            End If

            If AllErrors Then
                For i As Integer = 0 To lstErrors.Count - 2
                    currentError = lstErrors.Item(i)
                    sResult &= currentError.ToString & vbCrLf
                Next

                currentError = lstErrors.Item(lstErrors.Count - 1)
                sResult &= currentError.ToString
            Else
                currentError = lstErrors.Item(0)
                sResult = currentError.ToString
            End If

            Return sResult
        End Function
    End Class

#Region "Logger"
    Public Class Logger
        Private ReadOnly DefaultLogFile As String = ""
        Private Shared mLogFilePath As String = ""
        Private Shared mLogFile As String = ""
        Private Shared pPrevDepth As Integer = 0
        Private Shared pDoNotStoreLogLevel As Boolean = False
        Private Shared mLastError As String = ""

        Public Enum MESSAGE_TYPE
            LOG_DEFAULT = 0
            LOG_WARNING = 1
            LOG_ERROR = 2
            LOG_DEBUG = 6
        End Enum

        Public Shared ReadOnly Property LastError() As String
            Get
                Return mLastError
            End Get
        End Property

        Public Shared Property LogFilePath() As String
            Get
                Return mLogFilePath
            End Get
            Set(ByVal value As String)
                If value.EndsWith("\") Then
                    mLogFilePath = value
                Else
                    mLogFilePath = value & "\"
                End If
            End Set
        End Property

        Public Shared Property LogFileName() As String
            Get
                Return mLogFile
            End Get
            Set(ByVal value As String)
                mLogFile = value
            End Set
        End Property

#Region "Write relative"
        Public Shared Function WriteLine_Relative(ByVal LogText As String, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Return WriteToLogRelative(LogText, MESSAGE_TYPE.LOG_DEFAULT, Depth, DoNotWriteToFileJustReturnFullLine)
        End Function

        Public Shared Function WriteDebug_Relative(ByVal LogText As String, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Return WriteToLogRelative(LogText, MESSAGE_TYPE.LOG_DEBUG, Depth, DoNotWriteToFileJustReturnFullLine)
        End Function

        Public Shared Function WriteWarning_Relative(ByVal LogText As String, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Return WriteToLogRelative(LogText, MESSAGE_TYPE.LOG_WARNING, Depth, DoNotWriteToFileJustReturnFullLine)
        End Function

        Public Shared Function WriteError_Relative(ByVal LogText As String, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Return WriteToLogRelative(LogText, MESSAGE_TYPE.LOG_ERROR, Depth, DoNotWriteToFileJustReturnFullLine)
        End Function

        Private Shared Function WriteToLogRelative(ByVal LogText As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal DepthRelative As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            pDoNotStoreLogLevel = True
            Return WriteToLog(LogText, cMessageType, pPrevDepth + DepthRelative)
        End Function
#End Region

#Region "write"
        Public Shared Sub Write_WithoutPrefix(ByVal sMessage As String)
            UpdateLogfile(sMessage)
        End Sub

        Public Shared Sub WriteEmptyLineToLog()
            Write_WithoutPrefix(" ")
        End Sub

        Public Shared Function WriteLine(ByVal LogText As String, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Return WriteToLog(LogText, MESSAGE_TYPE.LOG_DEFAULT, Depth, DoNotWriteToFileJustReturnFullLine)
        End Function

        Public Shared Function WriteDebug(ByVal LogText As String, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Return WriteToLog(LogText, MESSAGE_TYPE.LOG_DEBUG, Depth, DoNotWriteToFileJustReturnFullLine)
        End Function

        Public Shared Function WriteWarning(ByVal LogText As String, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Return WriteToLog(LogText, MESSAGE_TYPE.LOG_WARNING, Depth, DoNotWriteToFileJustReturnFullLine)
        End Function

        Public Shared Function WriteError(ByVal LogText As String, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Return WriteToLog(LogText, MESSAGE_TYPE.LOG_ERROR, Depth, DoNotWriteToFileJustReturnFullLine)
        End Function

        Private Shared Function WriteToLog(ByVal LogText As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal Depth As Integer = 0, Optional ByVal DoNotWriteToFileJustReturnFullLine As Boolean = False) As String
            Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String, iDepthStep As Integer

            If Depth < 0 Then
                Depth = 0
            End If

            LogText = Trim(LogText)

            Select Case cMessageType
                Case MESSAGE_TYPE.LOG_WARNING
                    sMsgTypePrefix = "[*] "
                Case MESSAGE_TYPE.LOG_ERROR
                    sMsgTypePrefix = "[!] "
                Case MESSAGE_TYPE.LOG_DEBUG
                    sMsgTypePrefix = "[#] "
                Case MESSAGE_TYPE.LOG_DEFAULT
                    sMsgTypePrefix = "[.] "
                Case Else
                    sMsgTypePrefix = "[?] "
            End Select

            sMsgDatePrefix = Now.ToString & " - "

            sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

            If Depth < pPrevDepth Then
                For iDepthStep = 1 To Depth
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & vbCrLf
                sMsgPrefix = sMsgPrefix & sMsgTypePrefix & sMsgDatePrefix
            End If

            If Depth > 0 Then
                For iDepthStep = 1 To Depth - 1
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & "|-"
            End If

            If Not DoNotWriteToFileJustReturnFullLine Then
                UpdateLogfile(sMsgPrefix & LogText)
            End If

            If Not pDoNotStoreLogLevel Then
                pPrevDepth = Depth
            End If

            pDoNotStoreLogLevel = False

            Return sMsgPrefix & LogText
        End Function
#End Region

#Region "file write"
        Private Shared Function UpdateLogfile(ByVal sString As String) As Boolean
            Try
                If Not IO.Directory.Exists(mLogFilePath) Then
                    Throw New Exception("path '" & mLogFilePath & "' not found")
                End If

                If mLogFile.Equals("") Then
                    Throw New Exception("filename is empty!")
                End If

                Dim sw As New IO.StreamWriter(mLogFilePath & mLogFile, True)
                sw.WriteLine(sString)
                sw.Close()

                Return True
            Catch ex As Exception
                Errors.Add("UpdateLogfile() error: " & ex.Message)
            End Try

            Return False
        End Function
#End Region
    End Class
#End Region

#Region "Types"
    Public Class Types
        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function


        Public Shared Function RandomString(Length As Integer, Optional CharsToChooseFrom As String = "") As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function
    End Class
#End Region

#Region "Registry"
    Public Class Registry
#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
                Errors.Add("GetValue_Boolean() error: " & ex.Message)
            End Try

            If sTmp = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SetValue_Boolean(keyName As String, valueName As String, value As Boolean)
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "yes", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "no", Microsoft.Win32.RegistryValueKind.String)
                End If
            Catch ex As Exception
                Errors.Add("SetValue_Boolean() error: " & ex.Message)
            End Try
        End Sub
#End Region

#Region "string"
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String = defaultValue

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                Errors.Add("GetValue_String() error: " & ex.Message)
            End Try

            Return sTmp
        End Function

        Public Shared Sub SetValue_String(keyName As String, valueName As String, value As String)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)
            Catch ex As Exception
                Errors.Add("SetValue_String() error: " & ex.Message)
            End Try
        End Sub
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer = defaultValue

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
                iTmp = Helper.Types.CastToInteger_safe(oTmp)
            Catch ex As Exception
                Errors.Add("GetValue_Integer() error: " & ex.Message)
            End Try

            Return iTmp
        End Function

        Public Shared Sub SetValue_Integer(keyName As String, valueName As String, value As Integer)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
            Catch ex As Exception
                Errors.Add("SetValue_Integer() error: " & ex.Message)
            End Try
        End Sub
#End Region

#Region "misc"
        Public Shared Function DeleteValue(key As String, valuename As String)
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key)

                If rk Is Nothing Then
                    Throw New Exception("not found")
                End If

                rk.DeleteValue(valuename, True)

                Return True
            Catch ex As Exception
                Errors.Add("DeleteValue() error: " & ex.Message)
            End Try

            Return False
        End Function

        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception
                Errors.Add("CreateKey() error: " & ex.Message)
            End Try

            Return bRet
        End Function

        Public Shared Function AllowAllForEveryone(key As String) As String
            Dim sRet As String = ""

            Try
                Dim sid As SecurityIdentifier = New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
                Dim account As NTAccount = TryCast(sid.Translate(GetType(NTAccount)), NTAccount)

                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Using rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, RegistryKeyPermissionCheck.ReadWriteSubTree)
                    If rk Is Nothing Then
                        Throw New InvalidOperationException("key '" & key & "' does not exist in HKEY_LOCAL_MACHINE")
                    End If

                    Dim rs As RegistrySecurity = rk.GetAccessControl
                    Dim rar As RegistryAccessRule = New RegistryAccessRule( _
                                                    account.ToString, _
                                                    RegistryRights.FullControl, _
                                                    InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, _
                                                    PropagationFlags.None, _
                                                    AccessControlType.Allow)


                    rs.SetAccessRuleProtection(True, False)
                    rs.AddAccessRule(rar)

                    rk.SetAccessControl(rs)
                End Using

                sRet = "ok"
            Catch ex As Exception
                sRet = ex.Message
                Errors.Add("AllowAllForEveryone() error: " & ex.Message)
            End Try

            Return sRet
        End Function

        'Private Shared ReadOnly _md5 As Security.Cryptography.MD5 = Security.Cryptography.MD5.Create()

        ''' <summary>
        ''' Checks if we can write to certain section of HKEY_LOCAL_MACHINE
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Function CanWriteToRegistrySection(keyName As String, valueName As String) As Boolean
            Dim sTmp As String = "", sTmp_Check As String = "", sTmp_Hash As String

            'Get current value of WriteCheck key
            sTmp = GetValue_String(keyName, valueName)

            'hash it, MD5 is good enough, it's not security related
            sTmp_Hash = MD5.GetHash(sTmp)

            'Write value back to WriteCheck key
            SetValue_String(keyName, valueName, sTmp_Hash)

            'Get new value of WriteCheck key
            sTmp_Check = GetValue_String(keyName, valueName)

            'Values should be not equal
            If Not sTmp.Equals(sTmp_Check) Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region
    End Class
#End Region

#Region "Crypto"
    Public Class SHA512
        Private Shared ReadOnly _sha1 As Security.Cryptography.SHA512 = Security.Cryptography.SHA512.Create()

        Public Shared Function GetHash(source As String) As String
            Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
            Dim sb As New System.Text.StringBuilder()

            Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

            Return sb.ToString()
        End Function

        Public Shared Function VerifyHash(source As String, hash As String) As Boolean
            Dim sourceHash = GetHash(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function

        Public Shared Function GetHashBase64(source As String) As String
            Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

            Return Convert.ToBase64String(data)
        End Function

        Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
            Dim sourceHash = GetHashBase64(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function
    End Class

    Public Class MD5
        Private Shared ReadOnly _md5 As Security.Cryptography.MD5 = Security.Cryptography.MD5.Create()

        Public Shared Function GetHash(source As String) As String
            Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
            Dim sb As New System.Text.StringBuilder()

            Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

            Return sb.ToString()
        End Function

        Public Shared Function VerifyHash(source As String, hash As String) As Boolean
            Dim sourceHash = GetHash(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function

        Public Shared Function GetHashBase64(source As String) As String
            Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

            Return Convert.ToBase64String(data)
        End Function

        Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
            Dim sourceHash = GetHashBase64(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function
    End Class
#End Region

#Region "TimeDate"
    Public Class TimeDate
        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function
    End Class
#End Region

#Region "PuppyPower"
    Public Class PuppyPower
        Private Shared mKeyName As String
        Public Shared Property KeyName() As String
            Get
                Return mKeyName
            End Get
            Set(ByVal value As String)
                mKeyName = value
            End Set
        End Property

        Public Shared ReadOnly Property Username As String
            Get
                Dim sTmp As String

                sTmp = Registry.GetValue_String(mKeyName, "username", "")

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
        End Property

        Public Shared ReadOnly Property Password As String
            Get
                Dim sTmp As String

                sTmp = Registry.GetValue_String(mKeyName, "password", "")

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
        End Property
    End Class
#End Region

#Region "Processes"
    Public Class Processes
        <DllImport("advapi32.dll", SetLastError:=True)> _
        Private Shared Function OpenProcessToken(ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        Private Shared mLastError As String = ""
        Public Shared ReadOnly Property LastError As String
            Get
                Dim sTmp As String = mLastError

                mLastError = ""

                Return sTmp
            End Get
        End Property

        Private Shared Function ConvertPasswordStringToSecureString(ByVal str As String) As Security.SecureString
            Dim password As New Security.SecureString

            For Each c As Char In str.ToCharArray
                password.AppendChar(c)
            Next

            Return password
        End Function

        Public Shared Function IsProcessRunning(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String

            sSearchProcessName__ToLower = sProcessName.ToLower

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            For Each p As Process In Process.GetProcesses
                sProcessName__ToLower = p.ProcessName.ToLower
                If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                    bRet = True
                    Exit For
                End If
            Next

            Return bRet
        End Function

        Public Shared Function IsUserLoggedIn(UserName As String) As Boolean
            Dim iCount As Integer

            iCount = CountProcessesOwnedByUser(UserName)

            If iCount >= 2 Then 'completely arbritrary, assumes if user has 2 or more processes running, (s)he's logged in
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
            Dim wiName As String, lCount As Long

            lCount = 0

            For Each p As Process In Process.GetProcesses
                Try
                    Dim hToken As IntPtr
                    If OpenProcessToken(p.Handle, TokenAccessLevels.Query, hToken) Then
                        Using wi As New WindowsIdentity(hToken)
                            wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                            If wiName.ToLower = sUserNameToCheck.ToLower Then
                                lCount += 1
                            End If
                        End Using
                        CloseHandle(hToken)
                    End If
                Catch ex As Exception
                    Errors.Add("CountProcessesOwnedByUser() error: " & ex.Message)
                End Try
            Next

            Return lCount
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOut As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, waitForExit, timeOut)
        End Function

        Public Shared Function StartProcess(cmdLine As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOut As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, "", "", cmdArguments, waitForExit, timeOut)
        End Function

        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    mLastError = "not started (pid<=0)"
                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception
                        Errors.Add("_StartProcess() error: " & ex.Message)
                    End Try

                    p.WaitForExit(timeOut)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        mLastError = "timed out!"
                        Return False
                    End If
                End If
            Catch ex As Exception
                mLastError = "not started (" & ex.Message & ")"
                Errors.Add("_StartProcess() error: " & ex.Message)

                Return False
            End Try

            Return True
        End Function

        Public Shared Function IsAppAlreadyRunning() As Boolean
            Dim appProc() As Process
            Dim strModName, strProcName As String

            Try
                strModName = Process.GetCurrentProcess.MainModule.ModuleName
                strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

                appProc = Process.GetProcessesByName(strProcName)

                If appProc.Length > 1 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception

            End Try

            Return False
        End Function
    End Class
#End Region

#Region "XOrObfuscation"
    Public Class XOrObfuscation
        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "$#(98)&_Ndf9m0987e5WRW#%rtefgm03947"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            If PlainText.Length > 1000 Then
                Return "(string too long)"
            End If

            InitializePasshrases()

            Try
                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(Asc(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(Asc(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String

            InitializePasshrases()

            Try
                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & Chr(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & Chr(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                PlainText_RandomPassphrase = ""
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = Asc(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            temp = _HexToNo(Mid(BinVal, 2, 1))
            iVal = iVal + temp
            temp = _HexToNo(Mid(BinVal, 1, 1))
            iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class
#End Region

#Region "Windows"
    Public Class Windows
        Public Shared Function IsRunningAsAdmin() As Boolean
            Dim bTmp As Boolean

            Try
                Dim user As WindowsIdentity = WindowsIdentity.GetCurrent()
                Dim principal As New WindowsPrincipal(user)

                bTmp = principal.IsInRole(WindowsBuiltInRole.Administrator)
            Catch ex As UnauthorizedAccessException
                bTmp = False
            Catch ex As Exception
                bTmp = False
            End Try

            Return bTmp
        End Function
    End Class
#End Region

#Region "Forms"
    Public Class Forms
        <DllImport("user32.dll", SetLastError:=True)> _
        Private Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As Integer) As Boolean
        End Function

        Private Const SWP_NOSIZE As Integer = &H1
        Private Const SWP_NOMOVE As Integer = &H2

        Private Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
        Private Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)

        Public Shared Sub TopMost(FormHandle As System.IntPtr, bTopMost As Boolean)
            If bTopMost Then
                SetWindowPos(FormHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            Else
                SetWindowPos(FormHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End If
        End Sub
    End Class
#End Region

#Region "SiteKiosk"
    Public Class SiteKiosk
        Public Shared Function Version(Optional OnlyMajor As Boolean = False) As String
            Dim sVersion As String = Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "Build", "0.0")

            If OnlyMajor Then
                Return sVersion.Split(".")(0)
            Else
                Return sVersion
            End If
        End Function

        Public Shared Function InstallFolder() As String
            Dim sInstallDir As String = Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "0.0")

            Return sInstallDir
        End Function

    End Class
#End Region

#Region "ComputerName"
    Public Class ComputerName
        Private Shared _computername As String
        Private Shared _domain As String

        Private Const JOIN_DOMAIN = 1
        Private Const ACCT_CREATE = 2
        Private Const ACCT_DELETE = 4
        Private Const WIN9X_UPGRADE = 16
        Private Const DOMAIN_JOIN_IF_JOINED = 32
        Private Const JOIN_UNSECURE = 64
        Private Const MACHINE_PASSWORD_PASSED = 128
        Private Const DEFERRED_SPN_SET = 256
        Private Const INSTALL_INVOCATION = 262144

        Public Shared Property ComputerName() As String
            Get
                Return _computername
            End Get
            Set(ByVal value As String)
                If _computername <> "" And InStr(_computername.ToLower, "rietberg".ToLower) <= 0 Then
                    _RenameComputer(value)
                End If
            End Set
        End Property

        Public Shared Property Workgroup() As String
            Get
                Return _domain
            End Get
            Set(ByVal value As String)
                If _computername <> "" And InStr(_computername.ToLower, "rietberg".ToLower) <= 0 Then
                    _JoinWorkgroup(value)
                End If
            End Set
        End Property

        Private Shared Sub _GetComputerNameAndDomain()
            Dim strComputer As String
            Dim objWMIService As Object
            Dim objComputers As Object, objComputer As Object

            strComputer = "."
            objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
            objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

            For Each objComputer In objComputers
                _computername = objComputer.Name
                _domain = objComputer.Domain
            Next
        End Sub

        Private Shared Sub _JoinWorkgroup(ByVal sDomain As String)
            Dim strComputer As String
            Dim objWMIService As Object
            Dim objComputers As Object, objComputer As Object

            strComputer = "."
            objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
            objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

            For Each objComputer In objComputers
                objComputer.JoinDomainOrWorkGroup(sDomain, , , , )
            Next
        End Sub

        Private Shared Sub _RenameComputer(ByVal sComputerName As String)
            Dim strComputer As String
            Dim objWMIService As Object
            Dim objComputers As Object, objComputer As Object

            strComputer = "."
            objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
            objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

            For Each objComputer In objComputers
                objComputer.Rename(sComputerName)
            Next
        End Sub
    End Class

#End Region

#Region "Files and Folders"
    Public Class FilesAndFolders
        Public Class Folders
            Public Shared Function GetProgramFilesFolder() As String
                Dim sPath As String = ""

                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

                If sPath.Equals(String.Empty) Then
                    sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
                End If

                Return sPath
            End Function
        End Class

        Public Class Files
            Public Class Copy
                Private Shared pSuppressCopyLogging As Boolean = False

                Public Shared Property SuppressCopyLogging() As Boolean
                    Get
                        Return pSuppressCopyLogging
                    End Get
                    Set(value As Boolean)
                        pSuppressCopyLogging = value
                    End Set
                End Property

                Public Shared Function CopyFiles(ByVal SourceDir As String, ByVal DestinationDir As String, Optional BackupDir As String = "") As Boolean
                    Logger.WriteLine_Relative("starting file copy", 1)
                    Logger.WriteLine_Relative("source: " & SourceDir, 2)
                    Logger.WriteLine_Relative("dest  : " & DestinationDir, 2)

                    If pSuppressCopyLogging Then
                        Logger.WriteLine_Relative("copy logging disabled", , 2)
                    End If

                    If Not IO.Directory.Exists(SourceDir) Then
                        Logger.WriteLine_Relative("source not found!", , 3)
                        Return False
                    End If

                    RecursiveDirectoryCopy(SourceDir, DestinationDir, BackupDir)

                    Return True
                End Function

                Private Shared Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String, Optional backupDir As String = "")
                    Dim sDir As String
                    Dim dDirInfo As IO.DirectoryInfo
                    Dim sDirInfo As IO.DirectoryInfo
                    Dim sFile As String
                    Dim sFileInfo As IO.FileInfo
                    Dim dFileInfo As IO.FileInfo

                    ' Add trailing separators to the supplied paths if they don't exist.
                    If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                        sourceDir &= System.IO.Path.DirectorySeparatorChar
                    End If
                    If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                        destDir &= System.IO.Path.DirectorySeparatorChar
                    End If

                    'If destination directory does not exist, create it.
                    dDirInfo = New System.IO.DirectoryInfo(destDir)
                    If dDirInfo.Exists = False Then dDirInfo.Create()
                    dDirInfo = Nothing


                    ' Get a list of directories from the current parent.
                    For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
                        sDirInfo = New System.IO.DirectoryInfo(sDir)
                        dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
                        ' Create the directory if it does not exist.
                        If dDirInfo.Exists = False Then dDirInfo.Create()
                        ' Since we are in recursive mode, copy the children also
                        RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName, backupDir)
                        sDirInfo = Nothing
                        dDirInfo = Nothing
                    Next

                    'Get the files from the current parent.
                    For Each sFile In System.IO.Directory.GetFiles(sourceDir)
                        sFileInfo = New System.IO.FileInfo(sFile)
                        dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

                        If Not pSuppressCopyLogging Then
                            Logger.WriteLine_Relative("copying", 1)
                            Logger.WriteLine_Relative("source: " & sFileInfo.FullName, 2)
                            Logger.WriteLine_Relative("dest  : " & dFileInfo.FullName, 2)
                        End If


                        'If File does exists, backup.
                        If dFileInfo.Exists Then
                            If Not pSuppressCopyLogging Then Logger.WriteLine_Relative("already exists", 3)

                            If dFileInfo.IsReadOnly Then
                                If Not pSuppressCopyLogging Then Logger.WriteLine_Relative("file is read-only, fixing...", 4)
                                dFileInfo.IsReadOnly = False
                            End If

                            If Not pSuppressCopyLogging Then Logger.WriteLine_Relative("creating backup", 3)
                            If Backup.BackupFile(dFileInfo.FullName, backupDir) Then

                            Else

                            End If
                        End If

                        Try
                            sFileInfo.CopyTo(dFileInfo.FullName, True)

                            If IO.File.Exists(dFileInfo.FullName) Then
                                If Not pSuppressCopyLogging Then Logger.WriteLine_Relative("ok", 2)
                            Else
                                If Not pSuppressCopyLogging Then Logger.WriteError_Relative("fail", 2)
                            End If
                        Catch ex As Exception
                            If Not pSuppressCopyLogging Then
                                Logger.WriteLine_Relative("copying", 1)
                                Logger.WriteLine_Relative("source: " & sFileInfo.FullName, 2)
                                Logger.WriteLine_Relative("dest  : " & dFileInfo.FullName, 2)
                            End If

                            Logger.WriteError_Relative("fail", 3)
                            Logger.WriteError_Relative("ex.Message=" & ex.Message, 4)
                        End Try

                        sFileInfo = Nothing
                        dFileInfo = Nothing
                    Next
                End Sub

            End Class

            Public Class Backup
                Public Shared Function BackupFile(ByVal sFile As String, sBackupFolder As String) As Boolean
                    Dim sPath As String, sName As String, sFullDest As String
                    Dim oFile As System.IO.FileInfo, oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
                    Dim bRet As Boolean = False

                    Try
                        If Not IO.File.Exists(sFile) Then
                            Throw New Exception(sFile & " doesn´t exist")
                        End If

                        oFile = New System.IO.FileInfo(sFile)

                        If Not IO.Directory.Exists(sBackupFolder) Then
                            Throw New Exception("base backup folder '" & sBackupFolder & "' not found")
                        End If

                        sName = oFile.Name

                        sPath = oFile.DirectoryName
                        sPath = sPath.Replace("C:", "")
                        sPath = sPath.Replace("c:", "")
                        sPath = sPath.Replace("\\", "\")
                        sPath = sBackupFolder & "\" & sPath

                        sFullDest = sPath & "\" & sName

                        oDirInfo = New System.IO.DirectoryInfo(sPath)
                        If Not oDirInfo.Exists Then oDirInfo.Create()

                        Logger.WriteLine_Relative("backup: " & sFullDest, , 1)

                        oFile.CopyTo(sFullDest, True)

                        oFileInfo = New System.IO.FileInfo(sFullDest)
                        If oFileInfo.Exists Then
                            Logger.WriteLine_Relative("ok", , 2)
                            bRet = True
                        Else
                            Logger.WriteLine_Relative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                        End If
                    Catch ex As Exception
                        Logger.WriteLine_Relative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        Logger.WriteLine_Relative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    End Try

                    oFile = Nothing
                    oDirInfo = Nothing
                    oFileInfo = Nothing

                    Return bRet
                End Function

            End Class

            Public Class ReplaceStringInFile
                Public Shared Function FindAndReplace(File As String, Search As List(Of String), Replace As List(Of String)) As Boolean
                    If Search Is Nothing Or Replace Is Nothing Then
                        Return False
                    End If

                    If Not IO.File.Exists(File) Then
                        Return False
                    End If

                    If Search.Count < 1 Or Replace.Count < 1 Then
                        Return False
                    End If

                    If Search.Count <> Replace.Count Then
                        Return False
                    End If


                    Dim lines As New List(Of String)

                    Try
                        Using sr As New IO.StreamReader(File)
                            Dim line As String

                            While Not sr.EndOfStream
                                line = sr.ReadLine

                                For i As Integer = 0 To Search.Count - 1
                                    If line.Contains(Search.Item(i)) Then
                                        line = line.Replace(Search.Item(i), Replace.Item(i))
                                    End If
                                Next

                                lines.Add(line)
                            End While
                        End Using

                        Using sw As New IO.StreamWriter(File)
                            For Each line As String In lines
                                sw.WriteLine(line)
                            Next
                        End Using
                        Return True
                    Catch ex As Exception : Return False : End Try
                End Function
            End Class
        End Class
    End Class

    Public Class CopyFiles
        Private Shared pSuppressCopyLogging As Boolean = False

        Public Shared Property SuppressCopyLogging() As Boolean
            Get
                Return pSuppressCopyLogging
            End Get
            Set(value As Boolean)
                pSuppressCopyLogging = value
            End Set
        End Property

        Public Shared Function CopyFiles(ByVal SourceDir As String, ByVal DestinationDir As String, Optional BackupDir As String = "") As Boolean
            Logger.WriteLine_Relative("starting file copy", 1)
            Logger.WriteLine_Relative("source: " & SourceDir, 2)
            Logger.WriteLine_Relative("dest  : " & DestinationDir, 2)

            If pSuppressCopyLogging Then
                Logger.WriteLine_Relative("copy logging disabled", , 2)
            End If

            If Not IO.Directory.Exists(SourceDir) Then
                Logger.WriteLine_Relative("source not found!", , 3)
                Return False
            End If

            RecursiveDirectoryCopy(SourceDir, DestinationDir, BackupDir)

            Return True
        End Function

        Private Shared Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String, Optional backupDir As String = "")
            Dim sDir As String
            Dim dDirInfo As IO.DirectoryInfo
            Dim sDirInfo As IO.DirectoryInfo
            Dim sFile As String
            Dim sFileInfo As IO.FileInfo
            Dim dFileInfo As IO.FileInfo

            ' Add trailing separators to the supplied paths if they don't exist.
            If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                sourceDir &= System.IO.Path.DirectorySeparatorChar
            End If
            If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                destDir &= System.IO.Path.DirectorySeparatorChar
            End If

            'If destination directory does not exist, create it.
            dDirInfo = New System.IO.DirectoryInfo(destDir)
            If dDirInfo.Exists = False Then dDirInfo.Create()
            dDirInfo = Nothing


            ' Get a list of directories from the current parent.
            For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
                sDirInfo = New System.IO.DirectoryInfo(sDir)
                dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
                ' Create the directory if it does not exist.
                If dDirInfo.Exists = False Then dDirInfo.Create()
                ' Since we are in recursive mode, copy the children also
                RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName, backupDir)
                sDirInfo = Nothing
                dDirInfo = Nothing
            Next

            'Get the files from the current parent.
            For Each sFile In System.IO.Directory.GetFiles(sourceDir)
                sFileInfo = New System.IO.FileInfo(sFile)
                dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

                If Not pSuppressCopyLogging Then
                    Logger.WriteLine_Relative("copying", 1)
                    Logger.WriteLine_Relative("source: " & sFileInfo.FullName, 2)
                    Logger.WriteLine_Relative("dest  : " & dFileInfo.FullName, 2)
                End If


                'If File does exists, backup.
                If dFileInfo.Exists Then
                    If Not pSuppressCopyLogging Then Logger.WriteLine_Relative("already exists", 3)

                    If dFileInfo.IsReadOnly Then
                        If Not pSuppressCopyLogging Then Logger.WriteLine_Relative("file is read-only, fixing...", 4)
                        dFileInfo.IsReadOnly = False
                    End If

                    If Not pSuppressCopyLogging Then Logger.WriteLine_Relative("creating backup", 3)
                    If BackupFile(dFileInfo.FullName, backupDir) Then

                    Else

                    End If
                End If

                Try
                    sFileInfo.CopyTo(dFileInfo.FullName, True)

                    If IO.File.Exists(dFileInfo.FullName) Then
                        If Not pSuppressCopyLogging Then Logger.WriteLine_Relative("ok", 2)
                    Else
                        If Not pSuppressCopyLogging Then Logger.WriteError_Relative("fail", 2)
                    End If
                Catch ex As Exception
                    If Not pSuppressCopyLogging Then
                        Logger.WriteLine_Relative("copying", 1)
                        Logger.WriteLine_Relative("source: " & sFileInfo.FullName, 2)
                        Logger.WriteLine_Relative("dest  : " & dFileInfo.FullName, 2)
                    End If

                    Logger.WriteError_Relative("fail", 3)
                    Logger.WriteError_Relative("ex.Message=" & ex.Message, 4)
                End Try

                sFileInfo = Nothing
                dFileInfo = Nothing
            Next
        End Sub

        Public Shared Function BackupFile(ByVal sFile As String, sBackupFolder As String) As Boolean
            Dim sPath As String, sName As String, sFullDest As String
            Dim oFile As System.IO.FileInfo, oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
            Dim bRet As Boolean = False

            Try
                If Not IO.File.Exists(sFile) Then
                    Throw New Exception(sFile & " doesn´t exist")
                End If

                oFile = New System.IO.FileInfo(sFile)

                If Not IO.Directory.Exists(sBackupFolder) Then
                    Throw New Exception("base backup folder '" & sBackupFolder & "' not found")
                End If

                sName = oFile.Name

                sPath = oFile.DirectoryName
                sPath = sPath.Replace("C:", "")
                sPath = sPath.Replace("c:", "")
                sPath = sPath.Replace("\\", "\")
                sPath = sBackupFolder & "\" & sPath

                sFullDest = sPath & "\" & sName

                oDirInfo = New System.IO.DirectoryInfo(sPath)
                If Not oDirInfo.Exists Then oDirInfo.Create()

                Logger.WriteLine_Relative("backup: " & sFullDest, , 1)

                oFile.CopyTo(sFullDest, True)

                oFileInfo = New System.IO.FileInfo(sFullDest)
                If oFileInfo.Exists Then
                    Logger.WriteLine_Relative("ok", , 2)
                    bRet = True
                Else
                    Logger.WriteLine_Relative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End If
            Catch ex As Exception
                Logger.WriteLine_Relative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                Logger.WriteLine_Relative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End Try

            oFile = Nothing
            oDirInfo = Nothing
            oFileInfo = Nothing

            Return bRet
        End Function

    End Class

#End Region

#Region "Restart/shutdown"
    '/// <summary>
    '/// Implements methods to exit Windows.
    '/// </summary>
    Public Class WindowsController
        '/// <summary>
        '/// Specifies the type of restart options that an application can use.
        '/// </summary>
        Public Enum RestartOptions
            '/// <summary>
            '/// Shuts down all processes running in the security context of the process that called the ExitWindowsEx function. Then it logs the user off.
            '/// </summary>
            LogOff = 0
            '/// <summary>
            '/// Shuts down the system and turns off the power. The system must support the power-off feature.
            '/// </summary>
            PowerOff = 8
            '/// <summary>
            '/// Shuts down the system and then restarts the system.
            '/// </summary>
            Reboot = 2
            '/// <summary>
            '/// Shuts down the system to a point at which it is safe to turn off the power. All file buffers have been flushed to disk, and all running processes have stopped. If the system supports the power-off feature, the power is also turned off.
            '/// </summary>
            ShutDown = 1
            '/// <summary>
            '/// Suspends the system.
            '/// </summary>
            Suspend = -1
            '/// <summary>
            '/// Hibernates the system.
            '/// </summary>
            Hibernate = -2
        End Enum

        '/// <summary>
        '/// An LUID is a 64-bit value guaranteed to be unique only on the system on which it was generated. The uniqueness of a locally unique identifier (LUID) is guaranteed only until the system is restarted.
        '/// </summary>
        <StructLayout(LayoutKind.Sequential, Pack:=1)> _
        Private Structure LUID
            '/// <summary>
            '/// The low order part of the 64 bit value.
            '/// </summary>
            Public LowPart As Integer
            '/// <summary>
            '/// The high order part of the 64 bit value.
            '/// </summary>
            Public HighPart As Integer
        End Structure

        '/// <summary>
        '/// The LUID_AND_ATTRIBUTES structure represents a locally unique identifier (LUID) and its attributes.
        '/// </summary>
        <StructLayout(LayoutKind.Sequential, Pack:=1)> _
        Private Structure LUID_AND_ATTRIBUTES
            '/// <summary>
            '/// Specifies an LUID value.
            '/// </summary>
            Public pLuid As LUID
            '/// <summary>
            '/// Specifies attributes of the LUID. This value contains up to 32 one-bit flags. Its meaning is dependent on the definition and use of the LUID.
            '/// </summary>
            Public Attributes As Integer
        End Structure

        '/// <summary>
        '/// The TOKEN_PRIVILEGES structure contains information about a set of privileges for an access token.
        '/// </summary>
        <StructLayout(LayoutKind.Sequential, Pack:=1)> _
        Private Structure TOKEN_PRIVILEGES
            '/// <summary>
            '/// Specifies the number of entries in the Privileges array.
            '/// </summary>
            Public PrivilegeCount As Integer
            '/// <summary>
            '/// Specifies an array of LUID_AND_ATTRIBUTES structures. Each structure contains the LUID and attributes of a privilege.
            '/// </summary>
            Public Privileges As LUID_AND_ATTRIBUTES
        End Structure

        '/// <summary>Required to enable or disable the privileges in an access token.</summary>
        Private Const TOKEN_ADJUST_PRIVILEGES As Integer = &H20
        '/// <summary>Required to query an access token.</summary>
        Private Const TOKEN_QUERY As Integer = &H8
        '/// <summary>The privilege is enabled.</summary>
        Private Const SE_PRIVILEGE_ENABLED As Integer = &H2
        '/// <summary>Specifies that the function should search the system message-table resource(s) for the requested message.</summary>
        Private Const FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000
        '/// <summary>Forces processes to terminate. When this flag is set, the system does not send the WM_QUERYENDSESSION and WM_ENDSESSION messages. This can cause the applications to lose data. Therefore, you should only use this flag in an emergency.</summary>
        Private Const EWX_FORCE As Integer = 4
        '/// <summary>
        '/// The LoadLibrary function maps the specified executable module into the address space of the calling process.
        '/// </summary>
        '/// <param name="lpLibFileName">Pointer to a null-terminated string that names the executable module (either a .dll or .exe file). The name specified is the file name of the module and is not related to the name stored in the library module itself, as specified by the LIBRARY keyword in the module-definition (.def) file.</param>
        '/// <returns>If the function succeeds, the return value is a handle to the module.<br></br><br>If the function fails, the return value is NULL. To get extended error information, call Marshal.GetLastWin32Error.</br></returns>
        Private Declare Ansi Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" (ByVal lpLibFileName As String) As IntPtr
        '/// <summary>
        '/// The FreeLibrary function decrements the reference count of the loaded dynamic-link library (DLL). When the reference count reaches zero, the module is unmapped from the address space of the calling process and the handle is no longer valid.
        '/// </summary>
        '/// <param name="hLibModule">Handle to the loaded DLL module. The LoadLibrary or GetModuleHandle function returns this handle.</param>
        '/// <returns>If the function succeeds, the return value is nonzero.<br></br><br>If the function fails, the return value is zero. To get extended error information, call Marshal.GetLastWin32Error.</br></returns>
        Private Declare Ansi Function FreeLibrary Lib "kernel32" (ByVal hLibModule As IntPtr) As Integer
        '/// <summary>
        '/// The GetProcAddress function retrieves the address of an exported function or variable from the specified dynamic-link library (DLL).
        '/// </summary>
        '/// <param name="hModule">Handle to the DLL module that contains the function or variable. The LoadLibrary or GetModuleHandle function returns this handle.</param>
        '/// <param name="lpProcName">Pointer to a null-terminated string containing the function or variable name, or the function's ordinal value. If this parameter is an ordinal value, it must be in the low-order word; the high-order word must be zero.</param>
        '/// <returns>If the function succeeds, the return value is the address of the exported function or variable.<br></br><br>If the function fails, the return value is NULL. To get extended error information, call Marshal.GetLastWin32Error.</br></returns>
        Private Declare Ansi Function GetProcAddress Lib "kernel32" (ByVal hModule As IntPtr, ByVal lpProcName As String) As IntPtr
        '/// <summary>
        '/// The SetSuspendState function suspends the system by shutting power down. Depending on the Hibernate parameter, the system either enters a suspend (sleep) state or hibernation (S4). If the ForceFlag parameter is TRUE, the system suspends operation immediately; if it is FALSE, the system requests permission from all applications and device drivers before doing so.
        '/// </summary>
        '/// <param name="Hibernate">Specifies the state of the system. If TRUE, the system hibernates. If FALSE, the system is suspended.</param>
        '/// <param name="ForceCritical">Forced suspension. If TRUE, the function broadcasts a PBT_APMSUSPEND event to each application and driver, then immediately suspends operation. If FALSE, the function broadcasts a PBT_APMQUERYSUSPEND event to each application to request permission to suspend operation.</param>
        '/// <param name="DisableWakeEvent">If TRUE, the system disables all wake events. If FALSE, any system wake events remain enabled.</param>
        '/// <returns>If the function succeeds, the return value is nonzero.<br></br><br>If the function fails, the return value is zero. To get extended error information, call Marshal.GetLastWin32Error.</br></returns>
        Private Declare Ansi Function SetSuspendState Lib "powrprof" (ByVal Hibernate As Integer, ByVal ForceCritical As Integer, ByVal DisableWakeEvent As Integer) As Integer
        '/// <summary>
        '/// The OpenProcessToken function opens the access token associated with a process.
        '/// </summary>
        '/// <param name="ProcessHandle">Handle to the process whose access token is opened.</param>
        '/// <param name="DesiredAccess">Specifies an access mask that specifies the requested types of access to the access token. These requested access types are compared with the token's discretionary access-control list (DACL) to determine which accesses are granted or denied.</param>
        '/// <param name="TokenHandle">Pointer to a handle identifying the newly-opened access token when the function returns.</param>
        '/// <returns>If the function succeeds, the return value is nonzero.<br></br><br>If the function fails, the return value is zero. To get extended error information, call Marshal.GetLastWin32Error.</br></returns>
        Private Declare Ansi Function OpenProcessToken Lib "advapi32" (ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Integer
        '/// <summary>
        '/// The LookupPrivilegeValue function retrieves the locally unique identifier (LUID) used on a specified system to locally represent the specified privilege name.
        '/// </summary>
        '/// <param name="lpSystemName">Pointer to a null-terminated string specifying the name of the system on which the privilege name is looked up. If a null string is specified, the function attempts to find the privilege name on the local system.</param>
        '/// <param name="lpName">Pointer to a null-terminated string that specifies the name of the privilege, as defined in the Winnt.h header file. For example, this parameter could specify the constant SE_SECURITY_NAME, or its corresponding string, "SeSecurityPrivilege".</param>
        '/// <param name="lpLuid">Pointer to a variable that receives the locally unique identifier by which the privilege is known on the system, specified by the lpSystemName parameter.</param>
        '/// <returns>If the function succeeds, the return value is nonzero.<br></br><br>If the function fails, the return value is zero. To get extended error information, call Marshal.GetLastWin32Error.</br></returns>
        Private Declare Ansi Function LookupPrivilegeValue Lib "advapi32" Alias "LookupPrivilegeValueA" (ByVal lpSystemName As String, ByVal lpName As String, ByRef lpLuid As LUID) As Integer
        '/// <summary>
        '/// The AdjustTokenPrivileges function enables or disables privileges in the specified access token. Enabling or disabling privileges in an access token requires TOKEN_ADJUST_PRIVILEGES access.
        '/// </summary>
        '/// <param name="TokenHandle">Handle to the access token that contains the privileges to be modified. The handle must have TOKEN_ADJUST_PRIVILEGES access to the token. If the PreviousState parameter is not NULL, the handle must also have TOKEN_QUERY access.</param>
        '/// <param name="DisableAllPrivileges">Specifies whether the function disables all of the token's privileges. If this value is TRUE, the function disables all privileges and ignores the NewState parameter. If it is FALSE, the function modifies privileges based on the information pointed to by the NewState parameter.</param>
        '/// <param name="NewState">Pointer to a TOKEN_PRIVILEGES structure that specifies an array of privileges and their attributes. If the DisableAllPrivileges parameter is FALSE, AdjustTokenPrivileges enables or disables these privileges for the token. If you set the SE_PRIVILEGE_ENABLED attribute for a privilege, the function enables that privilege; otherwise, it disables the privilege. If DisableAllPrivileges is TRUE, the function ignores this parameter.</param>
        '/// <param name="BufferLength">Specifies the size, in bytes, of the buffer pointed to by the PreviousState parameter. This parameter can be zero if the PreviousState parameter is NULL.</param>
        '/// <param name="PreviousState">Pointer to a buffer that the function fills with a TOKEN_PRIVILEGES structure that contains the previous state of any privileges that the function modifies. This parameter can be NULL.</param>
        '/// <param name="ReturnLength">Pointer to a variable that receives the required size, in bytes, of the buffer pointed to by the PreviousState parameter. This parameter can be NULL if PreviousState is NULL.</param>
        '/// <returns>If the function succeeds, the return value is nonzero. To determine whether the function adjusted all of the specified privileges, call Marshal.GetLastWin32Error.</returns>
        Private Declare Ansi Function AdjustTokenPrivileges Lib "advapi32" (ByVal TokenHandle As IntPtr, ByVal DisableAllPrivileges As Integer, ByRef NewState As TOKEN_PRIVILEGES, ByVal BufferLength As Integer, ByRef PreviousState As TOKEN_PRIVILEGES, ByRef ReturnLength As Integer) As Integer
        '/// <summary>
        '/// The ExitWindowsEx function either logs off the current user, shuts down the system, or shuts down and restarts the system. It sends the WM_QUERYENDSESSION message to all applications to determine if they can be terminated.
        '/// </summary>
        '/// <param name="uFlags">Specifies the type of shutdown.</param>
        '/// <param name="dwReserved">This parameter is ignored.</param>
        '/// <returns>If the function succeeds, the return value is nonzero.<br></br><br>If the function fails, the return value is zero. To get extended error information, call Marshal.GetLastWin32Error.</br></returns>
        Private Declare Ansi Function ExitWindowsEx Lib "user32" (ByVal uFlags As Integer, ByVal dwReserved As Integer) As Integer
        '/// <summary>
        '/// The FormatMessage function formats a message string. The function requires a message definition as input. The message definition can come from a buffer passed into the function. It can come from a message table resource in an already-loaded module. Or the caller can ask the function to search the system's message table resource(s) for the message definition. The function finds the message definition in a message table resource based on a message identifier and a language identifier. The function copies the formatted message text to an output buffer, processing any embedded insert sequences if requested.
        '/// </summary>
        '/// <param name="dwFlags">Specifies aspects of the formatting process and how to interpret the lpSource parameter. The low-order byte of dwFlags specifies how the function handles line breaks in the output buffer. The low-order byte can also specify the maximum width of a formatted output line.</param>
        '/// <param name="lpSource">Specifies the location of the message definition. The type of this parameter depends upon the settings in the dwFlags parameter.</param>
        '/// <param name="dwMessageId">Specifies the message identifier for the requested message. This parameter is ignored if dwFlags includes FORMAT_MESSAGE_FROM_STRING.</param>
        '/// <param name="dwLanguageId">Specifies the language identifier for the requested message. This parameter is ignored if dwFlags includes FORMAT_MESSAGE_FROM_STRING.</param>
        '/// <param name="lpBuffer">Pointer to a buffer for the formatted (and null-terminated) message. If dwFlags includes FORMAT_MESSAGE_ALLOCATE_BUFFER, the function allocates a buffer using the LocalAlloc function, and places the pointer to the buffer at the address specified in lpBuffer.</param>
        '/// <param name="nSize">If the FORMAT_MESSAGE_ALLOCATE_BUFFER flag is not set, this parameter specifies the maximum number of TCHARs that can be stored in the output buffer. If FORMAT_MESSAGE_ALLOCATE_BUFFER is set, this parameter specifies the minimum number of TCHARs to allocate for an output buffer. For ANSI text, this is the number of bytes; for Unicode text, this is the number of characters.</param>
        '/// <param name="Arguments">Pointer to an array of values that are used as insert values in the formatted message. A %1 in the format string indicates the first value in the Arguments array; a %2 indicates the second argument; and so on.</param>
        '/// <returns>If the function succeeds, the return value is the number of TCHARs stored in the output buffer, excluding the terminating null character.<br></br><br>If the function fails, the return value is zero. To get extended error information, call Marshal.GetLastWin32Error.</br></returns>
        Private Declare Ansi Function FormatMessage Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Integer, ByVal lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByVal lpBuffer As StringBuilder, ByVal nSize As Integer, ByVal Arguments As Integer) As Integer
        '/// <summary>
        '/// Exits windows (and tries to enable any required access rights, if necesarry).
        '/// </summary>
        '/// <param name="how">One of the RestartOptions values that specifies how to exit windows.</param>
        '/// <param name="force">True if the exit has to be forced, false otherwise.</param>
        '/// <exception cref="PrivilegeException">There was an error while requesting a required privilege.</exception>
        '/// <exception cref="PlatformNotSupportedException">The requested exit method is not supported on this platform.</exception>

        Public Shared Sub ExitWindows(ByVal how As RestartOptions, Optional ByVal force As Boolean = True)
            Select Case how
                Case RestartOptions.Suspend
                    SuspendSystem(False, force)
                Case RestartOptions.Hibernate
                    SuspendSystem(True, force)
                Case Else
                    ExitWindows(CType(how, Integer), force)
            End Select
        End Sub
        '/// <summary>
        '/// Exits windows (and tries to enable any required access rights, if necesarry).
        '/// </summary>
        '/// <param name="how">One of the RestartOptions values that specifies how to exit windows.</param>
        '/// <param name="force">True if the exit has to be forced, false otherwise.</param>
        '/// <remarks>This method cannot hibernate or suspend the system.</remarks>
        '/// <exception cref="PrivilegeException">There was an error while requesting a required privilege.</exception>
        Protected Shared Sub ExitWindows(ByVal how As Integer, ByVal force As Boolean)
            EnableToken("SeShutdownPrivilege")
            If force Then how = how Or EWX_FORCE
            If ExitWindowsEx(how, 0) = 0 Then Throw New PrivilegeException(FormatError(Marshal.GetLastWin32Error))
        End Sub
        '/// <summary>
        '/// Tries to enable the specified privilege.
        '/// </summary>
        '/// <param name="privilege">The privilege to enable.</param>
        '/// <exception cref="PrivilegeException">There was an error while requesting a required privilege.</exception>
        '/// <remarks>Thanks to Michael S. Muegel for notifying us about a bug in this code.</remarks>
        Protected Shared Sub EnableToken(ByVal privilege As String)
            If (Environment.OSVersion.Platform <> PlatformID.Win32NT) OrElse (Not CheckEntryPoint("advapi32.dll", "AdjustTokenPrivileges")) Then Return
            Dim tokenHandle As IntPtr
            Dim privilegeLUID As LUID
            Dim newPrivileges As TOKEN_PRIVILEGES
            Dim tokenPrivileges As TOKEN_PRIVILEGES
            If OpenProcessToken(Process.GetCurrentProcess.Handle, TOKEN_ADJUST_PRIVILEGES Or TOKEN_QUERY, tokenHandle) = 0 Then Throw New PrivilegeException(FormatError(Marshal.GetLastWin32Error))
            If LookupPrivilegeValue("", privilege, privilegeLUID) = 0 Then Throw New PrivilegeException(FormatError(Marshal.GetLastWin32Error))
            tokenPrivileges.PrivilegeCount = 1
            tokenPrivileges.Privileges.Attributes = SE_PRIVILEGE_ENABLED
            tokenPrivileges.Privileges.pLuid = privilegeLUID
            If AdjustTokenPrivileges(tokenHandle, 0, tokenPrivileges, 4 + (12 * tokenPrivileges.PrivilegeCount), newPrivileges, 4 + (12 * newPrivileges.PrivilegeCount)) = 0 Then Throw New PrivilegeException(FormatError(Marshal.GetLastWin32Error))
        End Sub
        '/// <summary>
        '/// Suspends or hibernates the system.
        '/// </summary>
        '/// <param name="hibernate">True if the system has to hibernate, false if the system has to be suspended.</param>
        '/// <param name="force">True if the exit has to be forced, false otherwise.</param>
        '/// <exception cref="PlatformNotSupportedException">The requested exit method is not supported on this platform.</exception>
        Protected Shared Sub SuspendSystem(ByVal hibernate As Boolean, ByVal force As Boolean)
            If Not CheckEntryPoint("powrprof.dll", "SetSuspendState") Then Throw New PlatformNotSupportedException("The SetSuspendState method is not supported on this system!")
            SetSuspendState(CType(IIf(hibernate, 1, 0), Integer), CType(IIf(force, 1, 0), Integer), 0)
        End Sub
        '/// <summary>
        '/// Checks whether a specified method exists on the local computer.
        '/// </summary>
        '/// <param name="library">The library that holds the method.</param>
        '/// <param name="method">The entry point of the requested method.</param>
        '/// <returns>True if the specified method is present, false otherwise.</returns>
        Protected Shared Function CheckEntryPoint(ByVal library As String, ByVal method As String) As Boolean
            Dim libPtr As IntPtr = LoadLibrary(library)
            If Not libPtr.Equals(IntPtr.Zero) Then
                If Not GetProcAddress(libPtr, method).Equals(IntPtr.Zero) Then
                    FreeLibrary(libPtr)
                    Return True
                End If
                FreeLibrary(libPtr)
            End If
            Return False
        End Function
        '/// <summary>
        '/// Formats an error number into an error message.
        '/// </summary>
        '/// <param name="number">The error number to convert.</param>
        '/// <returns>A string representation of the specified error number.</returns>
        Protected Shared Function FormatError(ByVal number As Integer) As String
            Try
                Dim buffer As New StringBuilder(255)
                FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, IntPtr.Zero, number, 0, buffer, buffer.Capacity, 0)
                Return buffer.ToString()
            Catch e As Exception
                Return "Unspecified error [" + number.ToString() + "]"
            End Try
        End Function
    End Class
    '/// <summary>
    '/// The exception that is thrown when an error occures when requesting a specific privilege.
    '/// </summary>
    Public Class PrivilegeException
        Inherits Exception
        '/// <summary>
        '/// Initializes a new instance of the PrivilegeException class.
        '/// </summary>
        Public Sub New()
            MyBase.New()
        End Sub
        '/// <summary>
        '/// Initializes a new instance of the PrivilegeException class with a specified error message.
        '/// </summary>
        '/// <param name="message">The message that describes the error.</param>
        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub
    End Class
#End Region

#Region "Event log"
    Public Class WindowsEventLog
        Public Shared Function WriteInformation(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Information)
        End Function

        Public Shared Function WriteError(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Error)
        End Function

        Public Shared Function WriteWarning(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Warning)
        End Function

        Public Shared Function WriteEntry(sEvent As String, oType As EventLogEntryType) As String
            Try
                Dim sSource As String = My.Application.Info.ProductName
                Dim sLog As String = "Application"

                If Not EventLog.SourceExists(sSource) Then
                    EventLog.CreateEventSource(New EventSourceCreationData(sSource, sLog))
                End If

                Dim ELog As New EventLog(sLog, ".", sSource)
                ELog.WriteEntry(sEvent, EventLogEntryType.Error)

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
    End Class
#End Region

#Region "Users/Accounts"
    Public Class WindowsUser
        Private Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000

        Private Const SID_USERS As String = "S-1-5-32-545"
        Private Const SID_ADMINISTRATORS As String = "S-1-5-32-544"

        Public Shared Function RemoveUser(sUsername As String, ByRef sError As String) As Boolean
            Try
                sError = ""

                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    AD.Children.Remove(usr)

                    Return True
                End If
            Catch ex As Exception
                sError = ex.Message
            End Try

            Return False
        End Function

        Public Shared Function DoesUserExist(sUsername As String, ByRef sError As String) As Boolean
            Try
                sError = ""

                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    Return True
                End If
            Catch ex As Exception
                sError = ex.Message
            End Try

            Return False
        End Function

        Public Shared Function IsUserAdmin(sUsername As String, ByRef sError As String) As Boolean
            Try
                sError = ""

                Dim DC = New PrincipalContext(ContextType.Machine)

                Dim user = UserPrincipal.FindByIdentity(DC, sUsername)
                Dim groups = user.GetGroups()
                Dim adminGroup As String = GetAdministratorsGroupName()

                For Each group As Principal In groups
                    If group.Name = adminGroup Then
                        Return True
                    End If
                Next
            Catch ex As Exception
                sError = ex.Message
            End Try

            Return False
        End Function

        Public Shared Function IsUserLoggedIn(sUsername As String, ByRef sError As String) As Boolean
            sError = ""

            Try
                If My.User.Name.Substring(My.User.Name.LastIndexOf("\") + 1).ToLower = sUsername.ToLower Then
                    Return True
                End If

            Catch ex As Exception
                sError = ex.Message
            End Try

            Return False
        End Function

        Public Shared Function GetLoggedInUser(ByRef sError As String) As String
            sError = ""

            Try
                Return My.User.Name.Substring(My.User.Name.LastIndexOf("\") + 1).ToLower
            Catch ex As Exception
                sError = ex.Message
            End Try

            Return ""
        End Function

        Public Shared Function AddAdminUser(ByVal sUsername As String, sDescription As String, ByVal sPassWord As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim NewUser As DirectoryEntry = AD.Children.Add(sUsername, "user")
                NewUser.Invoke("SetPassword", New Object() {sPassWord})
                NewUser.Invoke("Put", New Object() {"Description", sDescription})
                NewUser.CommitChanges()

                Dim grp As DirectoryEntry, sGrp As String = GetAdministratorsGroupName()

                If sGrp = "" Then
                    sGrp = "Administrators"
                End If

                grp = AD.Children.Find(sGrp, "group")
                If grp.Name <> "" Then
                    grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
                Else
                    Return "group failed"
                End If

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function AddUser(ByVal sUsername As String, sDescription As String, ByVal sPassWord As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim NewUser As DirectoryEntry = AD.Children.Add(sUsername, "user")
                NewUser.Invoke("SetPassword", New Object() {sPassWord})
                NewUser.Invoke("Put", New Object() {"Description", sDescription})
                NewUser.CommitChanges()

                Dim grp As DirectoryEntry, sGrp As String = GetUsersGroupName()

                If sGrp = "" Then
                    sGrp = "Users"
                End If

                grp = AD.Children.Find(sGrp, "group")
                If grp.Name <> "" Then
                    grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
                End If

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function ChangePassword(ByVal sUsername As String, ByVal sPassWord As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    usr.Invoke("SetPassword", New Object() {sPassWord})
                    usr.CommitChanges()

                    Return "ok"
                Else
                    Return "not found"
                End If
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function RemovePasswordExpiry(ByVal sUsername As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    usr.Properties("UserFlags").Value = ADS_UF_DONT_EXPIRE_PASSWD
                    usr.CommitChanges()

                    Return "ok"
                Else
                    Return "not found"
                End If
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function PromoteUserToAdmin(sUsername As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    Dim grp As DirectoryEntry, sGrp As String = GetAdministratorsGroupName()

                    If sGrp = "" Then
                        sGrp = "Administrators"
                    End If

                    grp = AD.Children.Find(sGrp, "group")
                    If grp.Name <> "" Then
                        grp.Invoke("Add", New Object() {usr.Path.ToString()})
                    End If
                End If

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetAdministratorsGroupName() As String
            Return GetGroupNameFromSid(SID_ADMINISTRATORS)
        End Function

        Public Shared Function GetUsersGroupName() As String
            Return GetGroupNameFromSid(SID_USERS)
        End Function

        Private Shared Function GetGroupNameFromSid(ByVal _sid As String) As String
            Try
                Dim context As PrincipalContext, group As GroupPrincipal

                context = New PrincipalContext(ContextType.Machine)
                group = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, _sid)

                Return group.SamAccountName
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Public Shared Function HideUserInLogonScreen(ByVal sUsername As String) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", sUsername, 0)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

#Region "funky pasword check stuff"
        <DllImport("advapi32.dll", SetLastError:=True)> _
        Private Shared Function LogonUser(pszUsername As String, pszDomain As String, pszPassword As String, dwLogonType As Integer, dwLogonProvider As Integer, ByRef phToken As IntPtr) As Boolean
        End Function

        ' closes open handes returned by LogonUser
        <DllImport("kernel32.dll", CharSet:=CharSet.Auto)> _
        Private Shared Function CloseHandle(handle As IntPtr) As Boolean
        End Function

        <DllImport("Kernel32.dll", EntryPoint:="FormatMessageW", SetLastError:=True, CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function FormatMessage(ByVal dwFlags As Integer, ByRef lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByRef lpBuffer As [String], ByVal nSize As Integer, ByRef Arguments As IntPtr) As Integer
        End Function

        Private Enum LogonType As Integer
            LOGON32_LOGON_INTERACTIVE = 2
            LOGON32_LOGON_NETWORK = 3
            LOGON32_LOGON_BATCH = 4
            LOGON32_LOGON_SERVICE = 5
            LOGON32_LOGON_UNLOCK = 7
            LOGON32_LOGON_NETWORK_CLEARTEXT = 8
            LOGON32_LOGON_NEW_CREDENTIALS = 9
        End Enum

        Private Shared ReadOnly FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000

        Public Shared Function VerifyUserPassword(ByVal Username As String, ByVal Password As String) As String
            Return VerifyUserPassword(Username, Password, "")
        End Function

        Public Shared Function VerifyUserPassword(ByVal Username As String, ByVal Password As String, ByVal Domain As String) As String
            Dim sRet As String = ""

            Try
                Dim lStatus As Long
                Dim sMessage As String

                sMessage = Space(512)

                Dim Token As New IntPtr

                LogonUser(Username, Domain, Password, LogonType.LOGON32_LOGON_INTERACTIVE, 0, Token)
                CloseHandle(Token)

                If Token.ToInt32 <> 0 Then
                    sRet = "ok"
                Else
                    lStatus = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, Err.LastDllError, 0, sMessage, sMessage.Length, 0)
                    sRet = sMessage
                End If
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function
#End Region

    End Class

#End Region
End Class
