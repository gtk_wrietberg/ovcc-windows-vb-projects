Public Class RevenueData
    Private mTerminalName As ArrayList
    Private mRevenueDate As ArrayList
    Private mDuration As ArrayList
    Private mSessions As ArrayList
    Private mPaymentType As ArrayList
    Private mRevenue As ArrayList
    Private mCurrency As ArrayList
    Private mImportDate As ArrayList

    Private iInternalCount As Integer

    Public Sub New()
        Me.Reset()
    End Sub

    Public ReadOnly Property TerminalName() As String
        Get
            If iInternalCount >= 0 And iInternalCount < mTerminalName.Count Then
                Return CType(mTerminalName.Item(iInternalCount), String)
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property RevenueDate() As String
        Get
            If iInternalCount >= 0 And iInternalCount < mTerminalName.Count Then
                Return CType(mRevenueDate.Item(iInternalCount), String)
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property Duration() As Integer
        Get
            If iInternalCount >= 0 And iInternalCount < mTerminalName.Count Then
                Return CType(mDuration.Item(iInternalCount), Integer)
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property Sessions() As Integer
        Get
            If iInternalCount >= 0 And iInternalCount < mTerminalName.Count Then
                Return CType(mSessions.Item(iInternalCount), Integer)
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property PaymentType() As String
        Get
            If iInternalCount >= 0 And iInternalCount < mTerminalName.Count Then
                Return CType(mPaymentType.Item(iInternalCount), String)
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property Revenue() As Integer
        Get
            If iInternalCount >= 0 And iInternalCount < mTerminalName.Count Then
                Return CType(mRevenue.Item(iInternalCount), Integer)
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property Currency() As String
        Get
            If iInternalCount >= 0 And iInternalCount < mTerminalName.Count Then
                Return CType(mCurrency.Item(iInternalCount), String)
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property ImportDate() As String
        Get
            If iInternalCount >= 0 And iInternalCount < mTerminalName.Count Then
                Return CType(mImportDate.Item(iInternalCount), String)
            Else
                Return String.Empty
            End If
        End Get
    End Property

    Public ReadOnly Property InternalCount() As Integer
        Get
            Return iInternalCount
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Return mTerminalName.Count
        End Get
    End Property

    Public Sub Reset()
        mTerminalName = New ArrayList
        mRevenueDate = New ArrayList
        mDuration = New ArrayList
        mSessions = New ArrayList
        mPaymentType = New ArrayList
        mRevenue = New ArrayList
        mCurrency = New ArrayList
        mImportDate = New ArrayList

        iInternalCount = -1
    End Sub

    Public Sub Add(ByVal sTerminalName As String, ByVal sRevenueDate As String, ByVal iDuration As Integer, ByVal iSessions As Integer, ByVal sPaymentType As String, ByVal iRevenue As Integer, ByVal sCurrency As String, ByVal sImportDate As String)
        mTerminalName.Add(sTerminalName)
        mRevenueDate.Add(sRevenueDate)
        mDuration.Add(iDuration)
        mSessions.Add(iSessions)
        mPaymentType.Add(sPaymentType)
        mRevenue.Add(iRevenue)
        mCurrency.Add(sCurrency)
        mImportDate.Add(sImportDate)
    End Sub

    Public Function HasNext() As Boolean
        iInternalCount = iInternalCount + 1

        If iInternalCount >= mTerminalName.Count Then
            Return False
        End If

        Return True
    End Function
End Class
