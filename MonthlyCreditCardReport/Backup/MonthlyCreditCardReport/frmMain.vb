Public Class frmMain
    Private WithEvents oSql As SqlClass
    Private ReadOnly cStartYear As Integer = 2007
    Private bCancel As Boolean

    Private sMonths() As String

#Region "Move form"
    Private newPoint As New System.Drawing.Point()
    Private a As Integer
    Private b As Integer

    Private Sub pnlTop_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlTop.MouseDown
        a = Control.MousePosition.X - Me.Location.X
        b = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub lblTitle_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseDown
        a = Control.MousePosition.X - Me.Location.X
        b = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub pnlTop_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pnlTop.MouseMove
        If e.Button = MouseButtons.Left Then
            MoveForm()
        End If
    End Sub

    Private Sub lblTitle_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseMove
        If e.Button = MouseButtons.Left Then
            MoveForm()
        End If
    End Sub

    Private Sub MoveForm()
        newPoint = Control.MousePosition
        newPoint.X = newPoint.X - (a)
        newPoint.Y = newPoint.Y - (b)
        Me.Location = newPoint
    End Sub
#End Region

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        e.Cancel = bCancel

        If bCancel Then
            UpdateProgress("Closing canceled")
        End If
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim i As Integer, s() As String
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        lblTitle.Text = "Monthly Credit Card Report"
        lblVersion.Text = "v" & myBuildInfo.FileVersion & " (" & IO.File.GetLastWriteTime(Application.ExecutablePath).ToString("yyyy-MM-dd HH:mm:ss") & ")"

        sMonths = New String() {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}

        bCancel = False

        s = txtProgressHistory.Lines
        For i = 0 To 9
            s(i) = ""
        Next
        txtProgressHistory.Lines = s
        txtProgress.Text = ""
        txtProgressHistory.Refresh()
        txtProgress.Refresh()

        panelMain.Left = (Me.Width - panelMain.Width) / 2
        panelMain.Top = (Me.Height - panelMain.Height) / 2

        cmbYear.Items.Clear()
        For i = 0 To Now.Year - cStartYear
            cmbYear.Items.Add(cStartYear + i)
        Next

        cmbYear.SelectedIndex = cmbYear.Items.Count - 1

        progressBarInsert.Minimum = 0
        progressBarInsert.Maximum = 1
        progressBarInsert.Value = 0
        progressBarInsert.Enabled = False

        UpdateProgress("started")

        oSql = New SqlClass
    End Sub

    Private Sub UpdateMonths()
        Dim iYear As Integer, iMonth As Integer, i As Integer

        iYear = cmbYear.SelectedItem.ToString

        cmbMonth.Items.Clear()
        If iYear < Now.Year Then
            iMonth = 11
        Else
            iMonth = Now.Month - 1
        End If

        For i = 0 To iMonth
            cmbMonth.Items.Add(sMonths(i))
        Next
    End Sub

    Private Sub UpdateProgress(ByVal sText As String)
        Dim i As Integer, s() As String

        s = txtProgressHistory.Lines
        For i = 0 To 8
            s(i) = s(i + 1)
        Next
        s(9) = txtProgress.Text
        txtProgressHistory.Lines = s
        txtProgressHistory.Refresh()

        txtProgress.Text = sText
        txtProgress.Refresh()
    End Sub

#Region "oSql events"
    Private Sub oSql_AboutToInsertItems() Handles oSql.AboutToInsertItems
        progressBarInsert.Enabled = True
        UpdateProgress("inserting items, please wait")
        UpdateProgress("please do not interrupt, or suffer a fate worse than a fate worse than death")
    End Sub

    Private Sub oSql_ConnectionClosed(ByVal DataSource As String, ByVal InitialCatalog As String) Handles oSql.ConnectionClosed
        UpdateProgress("connection closed")
    End Sub

    Private Sub oSql_ConnectionFailed(ByVal DataSource As String, ByVal InitialCatalog As String, ByVal ErrorMessage As String) Handles oSql.ConnectionFailed
        UpdateProgress("connection failed: " & ErrorMessage)
    End Sub

    Private Sub oSql_ConnectionOpening(ByVal DataSource As String, ByVal InitialCatalog As String) Handles oSql.ConnectionOpening
        UpdateProgress("opening connection to " & DataSource & "\" & InitialCatalog)
    End Sub

    Private Sub oSql_ConnectionSuccessful(ByVal DataSource As String, ByVal InitialCatalog As String) Handles oSql.ConnectionSuccessful
        UpdateProgress("connected to " & DataSource & "\" & InitialCatalog)
    End Sub

    Private Sub oSql_DataCollected(ByVal Count As Integer) Handles oSql.DataCollected
        If Count <> 1 Then
            UpdateProgress("data collected: " & CStr(Count) & " items")
        Else
            UpdateProgress("data collected: " & CStr(Count) & " item")
        End If

        progressBarInsert.Maximum = Count
    End Sub

    Private Sub oSql_DataCollectionStarted() Handles oSql.DataCollectionStarted
        UpdateProgress("collecting data")
    End Sub

    Private Sub oSql_DebugMessage(ByVal Debug As String) Handles oSql.DebugMessage
        UpdateProgress(Debug)
    End Sub

    Private Sub oSql_ItemInserted(ByVal Done As Integer) Handles oSql.ItemInserted
        progressBarInsert.Value = Done
    End Sub

    Private Sub oSql_ItemsInserted(ByVal Done As Integer) Handles oSql.ItemsInserted
        UpdateProgress("Processed: " & CStr(Done))
    End Sub
#End Region

    Private Sub MainStuff()
        ToggleInputControls(False)

        bCancel = True

        oSql.DbConnectionDataSource = "172.18.192.10"
        oSql.DbConnectionInitialCatalog = "revenue"
        oSql.DbConnectionUsername = "LobbySa"
        oSql.DbConnectionPassword = "mrp00ns"
        oSql.ConstructConnectionString()

        If Not oSql.CollectRevenueData(cmbYear.SelectedIndex + cStartYear, cmbMonth.SelectedIndex + 1) Then
            UpdateProgress("FATAL ERROR")
            Exit Sub
        End If

        oSql.DbConnectionDataSource = "172.18.194.164"
        oSql.DbConnectionInitialCatalog = "LobbyPCStatistics"
        oSql.DbConnectionUsername = "db_user"
        oSql.DbConnectionPassword = "pr3pa1d"
        oSql.ConstructConnectionString()

        oSql.ImportData()

        bCancel = False

        UpdateProgress("done")
    End Sub

    Private Sub ToggleInputControls(ByVal Enabled As Boolean)
        cmbYear.Enabled = Enabled
        cmbMonth.Enabled = Enabled
        btnSubmit.Enabled = Enabled
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        MainStuff()
    End Sub

    Private Sub cmbYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbYear.SelectedIndexChanged
        UpdateMonths()

        CheckDate()
    End Sub

    Private Sub cmbMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMonth.SelectedIndexChanged
        CheckDate()
    End Sub

    Private Sub CheckDate()
        btnSubmit.Enabled = (cmbYear.SelectedIndex > -1) And (cmbMonth.SelectedIndex > -1)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
