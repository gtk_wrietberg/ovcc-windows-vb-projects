Imports System.Data.SqlClient

Public Class SqlClass
    Private mDbConnectionString As String
    
    Private mDataSource As String
    Private mInitialCatalog As String
    Private mUsername As String
    Private mPassword As String

    Private SQLConn As SqlConnection

    Private oRevenueData As RevenueData

    Private iInsertDone As Integer

    Private ReadOnly iMaxTransactionSize As Integer = 100

    Public Event ConnectionOpening(ByVal DataSource As String, ByVal InitialCatalog As String)
    Public Event ConnectionClosed(ByVal DataSource As String, ByVal InitialCatalog As String)
    Public Event ConnectionSuccessful(ByVal DataSource As String, ByVal InitialCatalog As String)
    Public Event ConnectionFailed(ByVal DataSource As String, ByVal InitialCatalog As String, ByVal ErrorMessage As String)
    Public Event ExecutingSql(ByVal SQL As String)
    Public Event AboutToInsertItems()
    Public Event ItemInserted(ByVal Done As Integer)
    Public Event ItemsInserted(ByVal Done As Integer)
    Public Event DataCollectionStarted()
    Public Event DataCollected(ByVal Count As Integer)
    Public Event DebugMessage(ByVal Debug As String)

    Public Sub New()
        mDbConnectionString = ""
        mDataSource = ""
        mInitialCatalog = ""
        mUserName = ""
        mPassword = ""
    End Sub

    Public Sub New(ByVal ConnectionString As String)
        mDbConnectionString = ConnectionString
        mDataSource = ""
        mInitialCatalog = ""
        mUsername = ""
        mPassword = ""
    End Sub

    Public Sub New(ByVal DataSource As String, ByVal InitialCatalog As String, ByVal Username As String, ByVal Password As String)
        mDataSource = DataSource
        mInitialCatalog = InitialCatalog
        mUsername = Username
        mPassword = Password

        Me.ConstructConnectionString()
    End Sub

    Public Property DbConnectionDataSource() As String
        Get
            Return mDataSource
        End Get
        Set(ByVal value As String)
            mDataSource = value
        End Set
    End Property

    Public Property DbConnectionInitialCatalog() As String
        Get
            Return mInitialCatalog
        End Get
        Set(ByVal value As String)
            mInitialCatalog = value
        End Set
    End Property

    Public Property DbConnectionUsername() As String
        Get
            Return mUsername
        End Get
        Set(ByVal value As String)
            mUsername = value
        End Set
    End Property

    Public Property DbConnectionPassword() As String
        Get
            Return mPassword
        End Get
        Set(ByVal value As String)
            mPassword = value
        End Set
    End Property

    Public Property DbConnectionString() As String
        Get
            Return mDbConnectionString
        End Get
        Set(ByVal value As String)
            mDbConnectionString = value
        End Set
    End Property

    Public Sub ConstructConnectionString()
        mDbConnectionString = "Data Source=" & mDataSource & ";Initial Catalog=" & mInitialCatalog & ";User Id=" & mUsername & ";Password=" & mPassword & ";"
    End Sub

    Public Function CollectRevenueData(ByVal RevenueYear As Integer, ByVal RevenueMonth As Integer) As Boolean
        Dim SQLCmd As New SqlCommand()
        Dim SQLdr As SqlDataReader
        Dim SqlStr As String

        SQLConn = New SqlConnection
        oRevenueData = New RevenueData

        SQLConn.ConnectionString = mDbConnectionString

        Try
            RaiseEvent ConnectionOpening(mDataSource, mInitialCatalog)

            SQLConn.Open()
        Catch ex As InvalidOperationException
            RaiseEvent ConnectionFailed(mDataSource, mInitialCatalog, "Cannot open a connection without specifying a data source or server or the connection is already open.")
            Exit Function
        Catch ex As SqlException
            RaiseEvent ConnectionFailed(mDataSource, mInitialCatalog, "A connection-level error occurred while opening the connection.")
            Exit Function
        End Try

        RaiseEvent ConnectionSuccessful(mDataSource, mInitialCatalog)

        Dim dDate As Date, dDate2 As Date
        Dim sDateStart As String, sDateEnd As String, sDateImport As String

        Dim sTerminalName As String
        Dim iRevenueDay As Integer
        Dim iDuration As Integer
        Dim iSessions As Integer
        Dim iRevenue As Integer
        Dim sCurrency As String

        dDate = DateSerial(RevenueYear, RevenueMonth, 1)

        dDate2 = DateAdd(DateInterval.Day, -1, dDate)
        sDateStart = Format(dDate2, "yyyy-MM-dd")

        dDate2 = DateAdd(DateInterval.Month, 1, dDate)
        sDateEnd = Format(dDate2, "yyyy-MM-dd")

        dDate2 = DateSerial(RevenueYear, RevenueMonth, 11)
        sDateImport = Format(dDate2, "yyyy-MM-dd")

        SqlStr = ""
        SqlStr = SqlStr & "SELECT [terminalname],DAY([date]),[duration],[sessions],[revenue],[currency] FROM tblRevenueData "
        SqlStr = SqlStr & "WHERE [date]>'" & sDateStart & "' AND [date]<'" & sDateEnd & "' "
        SqlStr = SqlStr & "AND [revenuetypeid]=2 ORDER BY [terminalname],[date]"

        RaiseEvent ExecutingSql(SqlStr)

        SQLCmd.Connection = SQLConn
        SQLCmd.CommandText = SqlStr
        SQLdr = SQLCmd.ExecuteReader

        If SQLdr.HasRows Then
            RaiseEvent DataCollectionStarted()

            Do While SQLdr.Read
                sTerminalName = SQLdr.GetString(0)
                iRevenueDay = SQLdr.GetInt32(1)
                iDuration = SQLdr.GetInt32(2)
                iSessions = SQLdr.GetInt32(3)
                iRevenue = SQLdr.GetInt32(4)
                sCurrency = SQLdr.GetString(5)

                oRevenueData.Add(sTerminalName, _
                                CStr(RevenueYear) & "-" & CStr(RevenueMonth) & "-" & CStr(iRevenueDay), _
                                iDuration, _
                                iSessions, _
                                "Credit Card", _
                                iRevenue, _
                                sCurrency, _
                                sDateImport)
            Loop
        End If

        RaiseEvent DataCollected(oRevenueData.Count)

        SQLdr.Close()
        SQLConn.Close()
        RaiseEvent ConnectionClosed(mDataSource, mInitialCatalog)

        Return True
    End Function

    Public Sub ImportData()
        SQLConn = New SqlConnection

        SQLConn.ConnectionString = mDbConnectionString

        Dim iTransactionCount As Integer = 0


        iInsertDone = 0

        Try
            RaiseEvent ConnectionOpening(mDataSource, mInitialCatalog)
            SQLConn.Open()

            Dim sSql As String = ""
            Dim SQLcommand As SqlCommand

            RaiseEvent AboutToInsertItems()

            Do While oRevenueData.HasNext
                sSql = sSql & "EXEC [LobbyPCStatistics].[dbo].[spUpdateRevenueStatisticsManuallyByTerminalNameWithErrorHandling] "
                sSql = sSql & "'" & oRevenueData.TerminalName & "',"
                sSql = sSql & "'" & oRevenueData.RevenueDate & "',"
                sSql = sSql & CStr(oRevenueData.Duration) & ","
                sSql = sSql & CStr(oRevenueData.Sessions) & ","
                sSql = sSql & "'" & oRevenueData.PaymentType & "',"
                sSql = sSql & CStr(oRevenueData.Revenue) & ","
                sSql = sSql & "'" & oRevenueData.Currency & "',"
                sSql = sSql & "'" & oRevenueData.ImportDate & "';" & vbCrLf
                iTransactionCount = iTransactionCount + 1

                If iTransactionCount > iMaxTransactionSize Or iTransactionCount >= (oRevenueData.Count - iInsertDone) Then
                    SQLcommand = New SqlCommand(sSql, SQLConn)
                    SQLcommand.ExecuteNonQuery()

                    iInsertDone = iInsertDone + iTransactionCount

                    sSql = ""
                    iTransactionCount = 0

                    RaiseEvent ItemInserted(iInsertDone)
                End If
            Loop

            RaiseEvent ItemsInserted(iInsertDone)
        Catch ex As Exception
            RaiseEvent ConnectionFailed(mDataSource, mInitialCatalog, ex.Message)
        Finally
            SQLConn.Close()

            RaiseEvent ConnectionClosed(mDataSource, mInitialCatalog)
        End Try
    End Sub
End Class
