Public Class ComputerName
    Private _computername As String
    Private _domain As String

    Private _you_are_on_one_of_wouters_machines As Boolean

    Private _last_err As String

    Private Const JOIN_DOMAIN = 1
    Private Const ACCT_CREATE = 2
    Private Const ACCT_DELETE = 4
    Private Const WIN9X_UPGRADE = 16
    Private Const DOMAIN_JOIN_IF_JOINED = 32
    Private Const JOIN_UNSECURE = 64
    Private Const MACHINE_PASSWORD_PASSED = 128
    Private Const DEFERRED_SPN_SET = 256
    Private Const INSTALL_INVOCATION = 262144

    Public Sub New()
        _GetComputerNameAndDomain()
    End Sub

    Public Property ComputerName() As String
        Get
            Return _computername
        End Get
        Set(ByVal value As String)
            If _computername <> "" And Not _you_are_on_one_of_wouters_machines Then
                _RenameComputer(value)
            Else
                _last_err = "REFUSED! Computer name change not permitted on dev machine!"
            End If
        End Set
    End Property

    Public Property Workgroup() As String
        Get
            Return _domain
        End Get
        Set(ByVal value As String)
            If _computername <> "" And Not _you_are_on_one_of_wouters_machines Then
                _JoinWorkgroup(value)
            Else
                _last_err = "REFUSED! Workgroup change not permitted on dev machine!"
            End If
        End Set
    End Property

    Public ReadOnly Property LastErr() As String
        Get
            Dim s As String

            s = _last_err
            _last_err = ""

            Return s
        End Get
    End Property

    Public ReadOnly Property TestOnly() As Boolean
        Get
            Return _you_are_on_one_of_wouters_machines
        End Get
    End Property

    Public Sub Refresh()
        _GetComputerNameAndDomain()
    End Sub

    Private Sub _GetComputerNameAndDomain()
        Dim strComputer As String
        Dim objWMIService As Object
        Dim objComputers As Object, objComputer As Object

        strComputer = "."
        objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
        objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

        For Each objComputer In objComputers
            _computername = objComputer.Name
            _domain = objComputer.Domain
        Next

        If InStr(_computername.ToLower, "rietberg") > 0 Then
            _you_are_on_one_of_wouters_machines = True
        End If
    End Sub

    Private Sub _JoinWorkgroup(ByVal sDomain As String)
        Dim strComputer As String
        Dim objWMIService As Object
        Dim objComputers As Object, objComputer As Object

        strComputer = "."
        objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
        objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

        For Each objComputer In objComputers
            objComputer.JoinDomainOrWorkGroup(sDomain, , , , )
        Next
    End Sub

    Private Sub _RenameComputer(ByVal sComputerName As String)
        Dim strComputer As String
        Dim objWMIService As Object
        Dim objComputers As Object, objComputer As Object

        strComputer = "."
        objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
        objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

        For Each objComputer In objComputers
            objComputer.Rename(sComputerName)
        Next
    End Sub
End Class
