Public Class frmHidden
    Private oMachine As ComputerName
    Private oLogger As Logger

    Private ReadOnly c_WORKGROUP_PREFIX = "FTG_"

    Private Sub frmHidden_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sCurrentWorkgroup As String
        Dim sNewWorkgroup As String
        Dim iExitCode As Integer

        Me.Width = 100
        Me.Height = 100
        Me.Top = -2 * Me.Height
        Me.Left = -2 * Me.Width

        oMachine = New ComputerName
        oLogger = New Logger

        oLogger.LogFilePath = "c:\ibahn"

        oLogger.WriteToLogWithoutDate("******************************************************************")
        oLogger.WriteToLog("Started")

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("---")

        sCurrentWorkgroup = oMachine.Workgroup
        sNewWorkgroup = c_WORKGROUP_PREFIX & sCurrentWorkgroup

        oLogger.WriteToLog("Getting workgroup names")

        oLogger.WriteToLog("Current workgroup name", , 1)
        oLogger.WriteToLog(sCurrentWorkgroup, , 2)
        oLogger.WriteToLog("New workgroup name", , 1)
        oLogger.WriteToLog(sNewWorkgroup, , 2)

        oLogger.WriteToLog("Make the change")

        oMachine.Workgroup = sNewWorkgroup

        oMachine.Refresh()

        sCurrentWorkgroup = oMachine.Workgroup

        oLogger.WriteToLog("Did it work?", , 1)

        If sCurrentWorkgroup.Equals(sNewWorkgroup) Then
            oLogger.WriteToLog("yes", , 2)

            iExitCode = 0
        Else
            oLogger.WriteToLog("no", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(oMachine.LastErr, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            iExitCode = 1
        End If

        oLogger.WriteToLog("bye")
        Environment.Exit(iExitCode)
        Application.Exit()
    End Sub
End Class