﻿Imports System.IO
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports Microsoft.Win32

Public Class Helpers

#Region "errors"
    Public Class Errors
        Public Class ErrorData
            Private mError_Timestamp As Date
            Private mError_Description As String
            Private mError_Caller As String

            Public Property Timestamp() As Date
                Get
                    Return mError_Timestamp
                End Get
                Set(ByVal value As Date)
                    mError_Timestamp = value
                End Set
            End Property

            Public Property Description() As String
                Get
                    Return mError_Description
                End Get
                Set(ByVal value As String)
                    mError_Description = value
                End Set
            End Property

            Public Property Caller() As String
                Get
                    Return mError_Caller
                End Get
                Set(ByVal value As String)
                    mError_Caller = value
                End Set
            End Property

            Public Overloads Function ToString(Optional IncludeTimestamp As Boolean = True) As String
                Dim sResult As String = ""

                If IncludeTimestamp Then
                    sResult = mError_Timestamp.ToString & " - " & mError_Description & " - " & mError_Caller
                Else
                    sResult = mError_Description & " - " & mError_Caller
                End If

                Return sResult
            End Function
        End Class

        Private Shared mMaxHistory As Integer = 25
        Private Shared lstErrors As New List(Of ErrorData)(mMaxHistory)

        Public Shared Sub Add(Description As String)
            Dim newError As New ErrorData

            newError.Description = Description
            newError.Timestamp = Date.Now

            Dim stackTrace As New StackTrace()

            newError.Caller = stackTrace.GetFrame(1).GetMethod().Name


            lstErrors.Insert(0, newError)

            If lstErrors.Count > mMaxHistory Then
                lstErrors.RemoveRange(mMaxHistory, lstErrors.Count - mMaxHistory)
            End If
        End Sub

        Public Shared Function GetLast(Optional IncludeTimestamp As Boolean = True) As String
            If lstErrors.Count > 0 Then
                Return lstErrors.Item(0).ToString
            Else
                Return ""
            End If
        End Function

        Public Overloads Shared Function ToString(Optional IncludeTimestamp As Boolean = True, Optional AllErrors As Boolean = False) As String
            Dim sResult As String = ""
            Dim currentError As New ErrorData

            If lstErrors.Count = 0 Then
                Return ""
            End If

            If lstErrors.Count = 1 Then
                AllErrors = False
            End If

            If AllErrors Then
                For i As Integer = 0 To lstErrors.Count - 2
                    currentError = lstErrors.Item(i)
                    sResult &= currentError.ToString & vbCrLf
                Next

                currentError = lstErrors.Item(lstErrors.Count - 1)
                sResult &= currentError.ToString
            Else
                currentError = lstErrors.Item(0)
                sResult = currentError.ToString
            End If

            Return sResult
        End Function
    End Class
#End Region

#Region "Logging"
    Public Class Logger
        Public Shared Function InitialiseLogger() As Boolean
            Dim sPath As String = ""

            sPath = FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_logs"

            Return _InitialiseLogger(sPath, True)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String) As Boolean
            Return _InitialiseLogger(LogFilePath, True)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String, bAddProductNameAndVersion As Boolean) As Boolean
            Return _InitialiseLogger(LogFilePath, bAddProductNameAndVersion)
        End Function

        Private Shared Function _InitialiseLogger(LogFilePath As String, bAddProductNameAndVersion As Boolean) As Boolean
            Dim dDate As Date = Now()

            If bAddProductNameAndVersion Then
                mLogFilePath = LogFilePath & "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
            Else
                mLogFilePath = LogFilePath
            End If
            mLogFile = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

            Try
                If Not IO.Directory.Exists(Logger.LogFilePath) Then
                    IO.Directory.CreateDirectory(Logger.LogFilePath)
                End If
            Catch ex As Exception

            End Try

            Return IO.Directory.Exists(mLogFilePath)
        End Function


        Private Shared mLogFilePath As String
        Private Shared mLogFile As String = ""
        Private Shared pPrevDepth As Integer = 0
        Private Shared pDoNotStoreLogLevel As Boolean = False

        Private Shared mDebugging As Boolean = True
        Public Shared Property Debugging() As Boolean
            Get
                Return mDebugging
            End Get
            Set(ByVal value As Boolean)
                mDebugging = value
            End Set
        End Property

        Private Shared mExtraPrefix As String
        Public Shared Property ExtraPrefix() As String
            Get
                Return mExtraPrefix
            End Get
            Set(ByVal value As String)
                mExtraPrefix = value
            End Set
        End Property

        Public Enum MESSAGE_TYPE
            LOG_DEFAULT = 0
            LOG_WARNING = 1
            LOG_ERROR = 2
            LOG_DEBUG = 6
        End Enum

        Public Shared Property LogFilePath() As String
            Get
                Return mLogFilePath
            End Get
            Set(ByVal value As String)
                If value.EndsWith("\") Then
                    mLogFilePath = value
                Else
                    mLogFilePath = value & "\"
                End If
            End Set
        End Property

        Public Shared Property LogFileName() As String
            Get
                Return mLogFile
            End Get
            Set(ByVal value As String)
                mLogFile = value
            End Set
        End Property

        Public Shared Function WriteMessage(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteMessageRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteWarning(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteWarningRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteError(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteErrorRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteDebug(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteDebugRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteRelative(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iRelativeDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            pDoNotStoreLogLevel = True
            Return Write(sMessage, cMessageType, pPrevDepth + iRelativeDepth)
        End Function

        Public Shared Function Write(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String, iDepthStep As Integer

            If iDepth < 0 Then
                iDepth = 0
            End If

            If Not mDebugging And cMessageType = MESSAGE_TYPE.LOG_DEBUG Then
                bDoNotWriteToFile = True
            End If

            sMessage = Trim(sMessage)

            Select Case cMessageType
                Case MESSAGE_TYPE.LOG_WARNING
                    sMsgTypePrefix = "[*] "
                Case MESSAGE_TYPE.LOG_ERROR
                    sMsgTypePrefix = "[!] "
                Case MESSAGE_TYPE.LOG_DEBUG
                    sMsgTypePrefix = "[#] "
                Case MESSAGE_TYPE.LOG_DEFAULT
                    sMsgTypePrefix = "[.] "
                Case Else
                    sMsgTypePrefix = "[?] "
            End Select


            sMsgDatePrefix = Now.ToString & " - "

            sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

            If iDepth < pPrevDepth Then
                For iDepthStep = 1 To iDepth
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & vbCrLf
                sMsgPrefix = sMsgPrefix & sMsgTypePrefix & sMsgDatePrefix
            End If

            If iDepth > 0 Then
                For iDepthStep = 1 To iDepth - 1
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & "|-"
            End If

            If Not bDoNotWriteToFile Then
                UpdateLogfile(sMsgPrefix & sMessage)
            End If

            If Not pDoNotStoreLogLevel Then
                pPrevDepth = iDepth
            End If

            pDoNotStoreLogLevel = False

            Return sMsgPrefix & sMessage
        End Function

        Public Shared Sub WriteWithoutDate(ByVal sMessage As String)
            UpdateLogfile(sMessage)
        End Sub

        Public Shared Sub WriteEmptyLine()
            WriteWithoutDate(" ")
        End Sub

        Private Shared Sub UpdateLogfile(ByVal sString As String)
            Try
                Dim sw As New IO.StreamWriter(mLogFilePath & "\" & mLogFile, True)
                sw.WriteLine(sString)
                sw.Close()
            Catch ex As Exception

            End Try
        End Sub
    End Class
#End Region

#Region "files and folders"
    Public Class FilesAndFolders
        Public Shared Function GetFileCompileDate(sFile As String) As String
            Return GetFileCompileDate(sFile, "yyyy-MM-dd HH:mm:ss")
        End Function

        Public Shared Function GetFileCompileDate(sFile As String, DateFormat As String) As String
            Try

                Const PeHeaderOffset As Integer = 60
                Const LinkerTimestampOffset As Integer = 8

                Dim b(2047) As Byte
                Dim s As IO.Stream

                Try
                    s = New IO.FileStream(sFile, IO.FileMode.Open, IO.FileAccess.Read)
                    s.Read(b, 0, 2048)
                Finally
                    If Not s Is Nothing Then s.Close()
                End Try

                Dim i As Integer = BitConverter.ToInt32(b, PeHeaderOffset)

                Dim SecondsSince1970 As Integer = BitConverter.ToInt32(b, i + LinkerTimestampOffset)
                Dim dt As New DateTime(1970, 1, 1, 0, 0, 0)
                dt = dt.AddSeconds(SecondsSince1970)
                dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours)

                Return dt.ToString(DateFormat)
            Catch ex As Exception
                Return GetFileCreationDate(sFile, DateFormat)
            End Try

        End Function

        Public Shared Function GetFileCreationDate(sFile As String) As String
            Return GetFileCreationDate(sFile, "yyyy-MM-dd")
        End Function

        Public Shared Function GetFileCreationDate(sFile As String, DateFormat As String) As String
            Try
                Dim dDate As Date = IO.File.GetCreationTime(sFile)

                Return dDate.ToString(DateFormat)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String) As String
            Try
                Dim dDate As Date = IO.File.GetLastWriteTime(sFile)

                Return dDate.ToString("yyyy-MM-dd")
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetProgramFilesFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sPath.Equals(String.Empty) Then
                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetCurrentFolder() As String
            Dim P As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)

            Return New Uri(P).LocalPath
        End Function

        Public Shared Function GetExecutablePath() As String
            Dim sPath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)

            sPath = New Uri(sPath).LocalPath

            Return sPath
        End Function

        Public Class Permissions
            Public Shared Function FileSecurity_ADD__ReadExecute(FileName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFileSecurity(FileName, secid, FileSystemRights.ReadAndExecute, AccessControlType.Allow, False)
            End Function

            Public Shared Function FileSecurity_REPLACE__Full_Everyone(FileName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFileSecurity(FileName, secid, FileSystemRights.FullControl, AccessControlType.Allow, True)
            End Function

            Public Shared Function FolderSecurity_REPLACE__Full_Everyone(FolderName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, True)
            End Function

            Private Shared Function _AddFileSecurity(ByVal fileName As String, ByVal SecurityIdentifier As IdentityReference, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, PurgeAllOtherRules As Boolean) As Boolean
                Try
                    Dim fSecurity As FileSecurity = File.GetAccessControl(fileName)
                    Dim accessRule As FileSystemAccessRule = New FileSystemAccessRule(SecurityIdentifier, rights, controlType)

                    If PurgeAllOtherRules Then
                        fSecurity.SetAccessRuleProtection(True, False)

                        Dim rules As AuthorizationRuleCollection = fSecurity.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
                        For Each rule As FileSystemAccessRule In rules
                            fSecurity.RemoveAccessRule(rule)
                        Next
                    End If

                    fSecurity.AddAccessRule(accessRule)

                    File.SetAccessControl(fileName, fSecurity)

                    Return True
                Catch ex As Exception
                    Errors.Add(ex.Message)

                    Return False
                End Try
            End Function

            Private Shared Function _AddFolderSecurity(ByVal folderName As String, ByVal SecurityIdentifier As IdentityReference, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, PurgeAllOtherRules As Boolean) As Boolean
                Try
                    Dim fSecurity As DirectorySecurity = Directory.GetAccessControl(folderName)
                    Dim accessRule As FileSystemAccessRule = New FileSystemAccessRule(SecurityIdentifier, rights, controlType)

                    If PurgeAllOtherRules Then
                        fSecurity.SetAccessRuleProtection(True, False)

                        Dim rules As AuthorizationRuleCollection = fSecurity.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
                        For Each rule As FileSystemAccessRule In rules
                            fSecurity.RemoveAccessRule(rule)
                        Next
                    End If

                    fSecurity.AddAccessRule(accessRule)

                    Directory.SetAccessControl(folderName, fSecurity)

                    Return True
                Catch ex As Exception
                    Errors.Add(ex.Message)

                    Return False
                End Try
            End Function
        End Class

        Public Class CopyFiles
            Private pBackupFolder As String
            Private pSourceDir As String
            Private pDestDir As String
            Private pSuppressCopyLogging As Boolean = False

            Public Property BackupDirectory() As String
                Get
                    Return pBackupFolder
                End Get
                Set(ByVal value As String)
                    pBackupFolder = value
                End Set
            End Property

            Public Property SourceDirectory() As String
                Get
                    Return pSourceDir
                End Get
                Set(ByVal value As String)
                    pSourceDir = value
                End Set
            End Property

            Public Property DestinationDirectory() As String
                Get
                    Return pDestDir
                End Get
                Set(ByVal value As String)
                    pDestDir = value
                End Set
            End Property

            Public Property SuppressCopyLogging() As Boolean
                Get
                    Return pSuppressCopyLogging
                End Get
                Set(value As Boolean)
                    pSuppressCopyLogging = value
                End Set
            End Property


            Public Function CopyFiles() As Boolean
                Logger.WriteMessageRelative("starting file copy", 1)
                Logger.WriteMessageRelative("source: " & pSourceDir, 2)
                Logger.WriteMessageRelative("dest  : " & pDestDir, 2)

                If pSuppressCopyLogging Then
                    Logger.WriteMessageRelative("copy logging disabled", 2)
                End If

                If Not IO.Directory.Exists(pSourceDir) Then
                    Logger.WriteMessageRelative("source not found!", 3)

                    Return False
                End If

                RecursiveDirectoryCopy(pSourceDir, pDestDir)

                Return True
            End Function

            Public Function CopyFiles(ByVal SourceDir As String, ByVal DestinationDir As String) As Boolean
                pSourceDir = SourceDir
                pDestDir = DestinationDir

                Return CopyFiles()
            End Function

            Private Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String)
                Dim sDir As String
                Dim dDirInfo As IO.DirectoryInfo
                Dim sDirInfo As IO.DirectoryInfo
                Dim sFile As String
                Dim sFileInfo As IO.FileInfo
                Dim dFileInfo As IO.FileInfo

                ' Add trailing separators to the supplied paths if they don't exist.
                If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                    sourceDir &= System.IO.Path.DirectorySeparatorChar
                End If
                If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                    destDir &= System.IO.Path.DirectorySeparatorChar
                End If

                'If destination directory does not exist, create it.
                dDirInfo = New System.IO.DirectoryInfo(destDir)
                If dDirInfo.Exists = False Then dDirInfo.Create()
                dDirInfo = Nothing


                ' Get a list of directories from the current parent.
                For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
                    sDirInfo = New System.IO.DirectoryInfo(sDir)
                    dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
                    ' Create the directory if it does not exist.
                    If dDirInfo.Exists = False Then dDirInfo.Create()
                    ' Since we are in recursive mode, copy the children also
                    RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName)
                    sDirInfo = Nothing
                    dDirInfo = Nothing
                Next

                ' Get the files from the current parent.
                For Each sFile In System.IO.Directory.GetFiles(sourceDir)
                    sFileInfo = New System.IO.FileInfo(sFile)
                    dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

                    If Not pSuppressCopyLogging Then
                        Logger.WriteMessageRelative("copying", 1)
                        Logger.WriteMessageRelative("source: " & sFileInfo.FullName, 2)
                        Logger.WriteMessageRelative("dest  : " & dFileInfo.FullName, 2)
                    End If


                    'If File does exists, backup.
                    If dFileInfo.Exists Then
                        If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("already exists", 3)

                        If dFileInfo.IsReadOnly Then
                            If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("file is read-only, fixing...", 4)
                            dFileInfo.IsReadOnly = False
                        End If
                        If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("creating backup", 3)
                        If BackupFile(dFileInfo) Then

                        Else

                        End If
                    End If

                    Try
                        sFileInfo.CopyTo(dFileInfo.FullName, True)
                        If IO.File.Exists(dFileInfo.FullName) Then
                            If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("ok", 2)
                        Else
                            If Not pSuppressCopyLogging Then Logger.WriteErrorRelative("fail", 2)
                        End If
                    Catch ex As Exception
                        If pSuppressCopyLogging Then
                            Logger.WriteMessageRelative("copying", 1)
                            Logger.WriteMessageRelative("source: " & sFileInfo.FullName, 2)
                            Logger.WriteMessageRelative("dest  : " & dFileInfo.FullName, 2)
                        End If

                        Logger.WriteErrorRelative("fail", 3)
                        Logger.WriteErrorRelative("ex.Message=" & ex.Message, 4)

                    End Try

                    sFileInfo = Nothing
                    dFileInfo = Nothing
                Next
            End Sub

            Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
                Dim sPath As String, sName As String, sFullDest As String
                Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
                Dim bRet As Boolean = False

                Try
                    sName = oFile.Name

                    sPath = oFile.DirectoryName
                    sPath = sPath.Replace("C:", "")
                    sPath = sPath.Replace("c:", "")
                    sPath = sPath.Replace("\\", "\")
                    sPath = pBackupFolder & "\" & sPath

                    sFullDest = sPath & "\" & sName

                    oDirInfo = New System.IO.DirectoryInfo(sPath)
                    If Not oDirInfo.Exists Then oDirInfo.Create()

                    Logger.WriteMessageRelative("backup: " & sFullDest, , 4)

                    oFile.CopyTo(sFullDest, True)

                    oFileInfo = New System.IO.FileInfo(sFullDest)
                    If oFileInfo.Exists Then
                        Logger.WriteMessageRelative("ok", , 5)
                        bRet = True
                    Else
                        Logger.WriteErrorRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                    End If
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
                End Try

                oDirInfo = Nothing
                oFileInfo = Nothing

                Return bRet
            End Function
        End Class

        Public Class Backup
            Public Shared Function File(filePath As String, backupFolder As String) As Boolean
                Logger.WriteMessageRelative("writing backup", 1)
                Logger.WriteMessageRelative(filePath, 2)

                If Not IO.Directory.Exists(backupFolder) Then
                    Logger.WriteErrorRelative("backup root folder does not exist", 3)

                    Return False
                End If


                If Not IO.File.Exists(filePath) Then
                    Logger.WriteErrorRelative("does not exist", 3)

                    Return False
                End If


                Dim fI As IO.FileInfo = New System.IO.FileInfo(filePath)

                Return _BackupFile(fI, backupFolder)
            End Function

            Private Shared Function _BackupFile(ByVal oFile As IO.FileInfo, backupFolder As String) As Boolean
                Dim sPath As String, sName As String, sFullDest As String
                Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
                Dim bRet As Boolean = False

                Try
                    sName = oFile.Name

                    sPath = oFile.DirectoryName
                    sPath = sPath.Replace("C:", "")
                    sPath = sPath.Replace("c:", "")
                    sPath = sPath.Replace("\\", "\")
                    sPath = backupFolder & "\" & sPath

                    sFullDest = sPath & "\" & sName

                    oDirInfo = New System.IO.DirectoryInfo(sPath)
                    If Not oDirInfo.Exists Then oDirInfo.Create()

                    Logger.WriteMessageRelative("backup: " & sFullDest, 3)

                    oFile.CopyTo(sFullDest, True)

                    oFileInfo = New System.IO.FileInfo(sFullDest)
                    If oFileInfo.Exists Then
                        Logger.WriteMessageRelative("ok", 4)
                        bRet = True
                    Else
                        Logger.WriteErrorRelative("fail", 4)
                    End If
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 4)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 5)
                End Try

                oDirInfo = Nothing
                oFileInfo = Nothing

                Return bRet
            End Function
        End Class
    End Class
#End Region

#Region "registry"
    Public Class Registry
        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function KeyExists_CU(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.CurrentUser, keyName)
        End Function

        Public Shared Function KeyExists_LM(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.LocalMachine, keyName)
        End Function

        Private Shared Function _KeyExists(keyHive As Microsoft.Win32.RegistryHive, keyName As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.LocalMachine Then
                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyHive = RegistryHive.CurrentUser Then
                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                Errors.Add(ex.Message)

                Return False
            End Try
        End Function



        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SetValue_Boolean(keyName As String, valueName As String, value As Boolean)
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "yes", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "no", Microsoft.Win32.RegistryValueKind.String)
                End If
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "string" ' Microsoft.Win32.RegistryKey.OpenSubKey(String name, Boolean writable)
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                sTmp = defaultValue
            Else
                sTmp = Convert.ToString(oTmp)
            End If

            Return sTmp
        End Function

        Public Shared Sub SetValue_String(keyName As String, valueName As String, value As String)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)
            Catch ex As Exception

            End Try
        End Sub


        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String, defaultValue As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, defaultValue)
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(hiveName, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyname As String, valueName As String, defaultValue As String) As String
            Try
                Dim oTmp As Object, sTmp As String
                Dim r As RegistryKey

                If hiveName = RegistryHive.LocalMachine Then
                    r = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyname, False)
                ElseIf hiveName = RegistryHive.CurrentUser Then
                    r = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyname, False)
                Else
                    Return defaultValue
                End If

                oTmp = r.GetValue(valueName, defaultValue)
                sTmp = Convert.ToString(oTmp)

                Return sTmp
            Catch ex As Exception
                Errors.Add("GetValue_ReadOnly_String(): " & ex.Message)

                Return "GetValue_ReadOnly_String(): " & ex.Message
            End Try
        End Function
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            iTmp = Helpers.Types.CastToInteger_safe(oTmp)

            Return iTmp
        End Function

        Public Shared Function SetValue_Integer(keyName As String, valueName As String, value As Integer) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "misc"
        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, True)
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Public Shared Function DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(keyHive, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Private Shared Function _DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Try
                Dim regKey As RegistryKey

                If keyHive = RegistryHive.CurrentUser Then
                    regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyRoot, True)
                ElseIf keyHive = RegistryHive.LocalMachine Then
                    regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyRoot, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                'delete all subkeys
                regKey.DeleteSubKeyTree(keyToDelete)

                If bKeepKeyToDelete Then
                    'put back the root sub key
                    regKey.CreateSubKey(keyToDelete)
                End If

                Return True
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return False
        End Function
#End Region

    End Class
#End Region

#Region "types"
    Public Class Types
        Public Shared Function CastStringToInteger_safe(s As String) As Integer
            Dim iTmp As Integer = -1

            If Integer.TryParse(s, iTmp) Then

            End If

            Return iTmp
        End Function

        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function LeadingZero(num As Integer, Optional len As Integer = 2) As String
            Dim sNum As String = num.ToString

            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Public Shared Function RandomString(Length As Integer) As String
            Return RandomString(Length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        End Function

        Public Shared Function RandomString(Length As Integer, CharsToChooseFrom As String) As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function

        Public Shared Function FromNormalDateToHBDate(Optional WithTime As Boolean = False) As String
            Return FromNormalDateToHBDate(Now, WithTime)
        End Function

        Public Shared Function FromNormalDateToHBDate(d As Date, Optional WithTime As Boolean = False) As String
            Dim sTmp As String = d.ToString("yyyy-MM-dd")

            If WithTime Then
                sTmp &= " " & d.ToString("HH:mm:ss")
            End If

            Return sTmp
        End Function

        Public Shared Function FromHBDateToNormalDate(s As String) As Date
            Dim d As Date

            Try
                d = Date.Parse(s)
            Catch ex As Exception
                d = Nothing
            End Try

            Return d
        End Function

        Public Shared Function IsValidHBDate(s As String) As Boolean
            Dim d As Date

            Try
                d = Date.Parse(s)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
    End Class
#End Region

#Region "time and date"
    Public Class TimeDate
        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function
    End Class
#End Region

End Class
