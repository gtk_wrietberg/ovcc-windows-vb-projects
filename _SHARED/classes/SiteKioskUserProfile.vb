Imports System.Management

Public Class SitekioskUserProfile
	Public Shared Function GetProfileFolder(ByRef SiteKioskUserProfile As String) As Boolean
        Dim sName As String = ""
        Dim sSid As String = ""
        Dim sFolder As String = ""

        Try
            Dim searcher As New ManagementObjectSearcher("root\CIMV2", "SELECT * FROM Win32_UserAccount")

            For Each queryObj As ManagementObject In searcher.Get()
                sName = Convert.ToString(queryObj("Name"))

                If sName.ToLower = "sitekiosk" Then
                    sSid = Convert.ToString(queryObj("SID"))

                    Exit For
                End If
            Next
        Catch err As ManagementException
            Console.WriteLine("(GetProfileFolder) error: " & err.Message)
        End Try

        If sSid <> "" Then
            sFolder = Helpers.Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\" & sSid, "ProfileImagePath", "")
        End If

        SiteKioskUserProfile = sFolder

        If sFolder = "" Then
            Return False
        End If

        Return True
    End Function
end class