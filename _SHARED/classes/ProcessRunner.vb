Imports System.Diagnostics
Imports System.Threading

Public Class ProcessRunner
    Private WithEvents myProcess As New System.Diagnostics.Process
    Private mElapsedTime As Double
    Private eventHandled As Boolean
    Private mTestMode As Boolean


    Private Const cSleepAmount As Double = 0.2#

    Private Const OneSec As Double = 1.0# / (1440.0# * 60.0#)

    Private mMaxTimeoutSeconds As Double = 30
    Private mProcessStyle As ProcessWindowStyle

    Private mFileName As String
    Private mArguments As String
    Private mUserName As String
    Private mPassWord As String
    Private mExternalAppToWaitFor As String

    Private mIsProcessDone As Boolean

    Private mErrorsOccurred As Boolean

    Private mBusyCounter As Integer
    Private mBusyTrigger As Integer
    Private mBusyTriggered As Boolean

    Public Event Started(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Busy(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Failed(ByVal EventProcess As Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String)
    Public Event Killing(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Killed(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event KillFailed(ByVal EventProcess As Process, ByVal ErrorMessage As String)
    Public Event TimeOut(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Test(ByVal EventProcess As Process)
    Public Event Done(ByVal EventProcess As Process, ByVal TimeElapsed As Double)

    Public Sub New()
        mMaxTimeoutSeconds = 60
        mProcessStyle = ProcessWindowStyle.Normal

        mFileName = ""
        mArguments = ""
        mUserName = ""
        mPassWord = ""
        mExternalAppToWaitFor = ""

        mBusyCounter = 0
        mBusyTrigger = 10
        mBusyTriggered = False

        mIsProcessDone = False
        mErrorsOccurred = False
        mTestMode = False

        mElapsedTime = 0.0#
    End Sub

#Region "Properties"
    Public Property TestMode() As Boolean
        Get
            Return mTestMode
        End Get
        Set(ByVal value As Boolean)
            mTestMode = value
        End Set
    End Property

    Public Property BusyTrigger() As Integer
        Get
            Return mBusyTrigger
        End Get
        Set(ByVal value As Integer)
            mBusyTrigger = value
        End Set
    End Property

    Public Property MaxTimeoutSeconds() As Double
        Get
            Return mMaxTimeoutSeconds
        End Get
        Set(ByVal value As Double)
            mMaxTimeoutSeconds = value
        End Set
    End Property

    Public Property ProcessStyle() As ProcessWindowStyle
        Get
            Return mProcessStyle
        End Get
        Set(ByVal value As ProcessWindowStyle)
            mProcessStyle = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return mFileName
        End Get
        Set(ByVal value As String)
            mFileName = value
        End Set
    End Property

    Public Property Arguments() As String
        Get
            Return mArguments
        End Get
        Set(ByVal value As String)
            mArguments = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set
    End Property

    Public Property PassWord() As String
        Get
            Return mPassWord
        End Get
        Set(ByVal value As String)
            mPassWord = value
        End Set
    End Property

    Public Property ExternalAppToWaitFor() As String
        Get
            Return mExternalAppToWaitFor
        End Get
        Set(ByVal value As String)
            mExternalAppToWaitFor = value
        End Set
    End Property

    Public ReadOnly Property IsProcessDone() As Boolean
        Get
            Return mIsProcessDone
        End Get
    End Property

    Public ReadOnly Property ErrorsOccurred() As Boolean
        Get
            Return mErrorsOccurred
        End Get
    End Property

    Public ReadOnly Property BusyTriggered() As Boolean
        Get
            Return mBusyTriggered
        End Get
    End Property
#End Region

    Public Sub StartProcess()
        StartProcess(mFileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String)
        StartProcess(FileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String, ByVal Arguments As String)
        mElapsedTime = 0.0#
        eventHandled = False

        mBusyCounter = 0

        If mTestMode Then
            RaiseEvent Test(myProcess)
            RaiseEvent Done(myProcess, 0.0#)

            Exit Sub
        End If

        Try
            myProcess.StartInfo.FileName = FileName
            myProcess.StartInfo.Arguments = Arguments
            If mUserName <> "" Then
                Dim securePass As New Security.SecureString()

                For Each c As Char In mPassWord
                    securePass.AppendChar(c)
                Next c

                myProcess.StartInfo.UseShellExecute = False
                myProcess.StartInfo.UserName = mUserName
                myProcess.StartInfo.Password = securePass
            End If
            myProcess.StartInfo.WindowStyle = mProcessStyle
            myProcess.EnableRaisingEvents = True
            myProcess.Start()

            RaiseEvent Started(myProcess, mElapsedTime)
        Catch ex As Exception
            RaiseEvent Failed(myProcess, mElapsedTime, ex.Message)
            mErrorsOccurred = True

            Exit Sub
        End Try

        Try
            If myProcess.Id <= 0 Then
                RaiseEvent Failed(myProcess, mElapsedTime, "pid = 0")
                mErrorsOccurred = True

                Exit Sub
            End If
        Catch ex As Exception
            RaiseEvent Failed(myProcess, mElapsedTime, ex.Message)
            mErrorsOccurred = True

            Exit Sub
        End Try

        Do While _TimeoutLoop()
            mElapsedTime += cSleepAmount
            mBusyCounter += 1

            mElapsedTime = Math.Round(mElapsedTime, 2)

            If mElapsedTime > mMaxTimeoutSeconds Then
                RaiseEvent TimeOut(myProcess, mElapsedTime)
                StopProcess()
                Exit Do
            Else
                If mBusyCounter >= mBusyTrigger Then
                    mBusyCounter = 0
                    mBusyTriggered = True
                    RaiseEvent Busy(myProcess, mElapsedTime)
                End If
            End If

            Thread.Sleep(cSleepAmount * 1000)
        Loop

        RaiseEvent Done(myProcess, mElapsedTime)
    End Sub

    Private Function _TimeoutLoop() As Boolean
        Dim bLoop As Boolean

        bLoop = Not eventHandled And Not myProcess.HasExited

        If mExternalAppToWaitFor <> "" Then
            If Process.GetProcessesByName(mExternalAppToWaitFor).Length > 0 Then
                bLoop = True
            End If
        End If

        Return bLoop
    End Function

    Public Sub StopProcess()
        Try
            RaiseEvent Killing(myProcess, mElapsedTime)
            myProcess.Kill()
            RaiseEvent Killed(myProcess, mElapsedTime)
        Catch ex As Exception
            RaiseEvent KillFailed(myProcess, ex.Message)
        End Try
    End Sub

    Public Sub ProcessDone()
        eventHandled = True

        myProcess.Close()

        mIsProcessDone = True
    End Sub
End Class
