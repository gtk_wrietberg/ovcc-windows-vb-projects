﻿Imports Microsoft.Win32
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Security.Principal
Imports System.Security
Imports Microsoft.Win32.SafeHandles
Imports System.Runtime.ConstrainedExecution
Imports System.Security.AccessControl
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.Linq

Public Class Helpers
    Public Class Logger
        Public Shared Function InitialiseLogger() As Boolean
            Dim sPath As String = ""

            sPath = FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_logs"

            Return _InitialiseLogger(sPath, True)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String) As Boolean
            Return _InitialiseLogger(LogFilePath, True)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String, bAddProductNameAndVersion As Boolean) As Boolean
            Return _InitialiseLogger(LogFilePath, bAddProductNameAndVersion)
        End Function

        Private Shared Function _InitialiseLogger(LogFilePath As String, bAddProductNameAndVersion As Boolean) As Boolean
            Dim dDate As Date = Now()


            mLogFilePath_ROOT = LogFilePath
            If bAddProductNameAndVersion Then
                mLogFilePath = LogFilePath & "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
                mLogFileBasePath = LogFilePath & "\" & My.Application.Info.ProductName
            Else
                mLogFilePath = LogFilePath
                mLogFileBasePath = LogFilePath
            End If
            mLogFile = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            Try
                If Not IO.Directory.Exists(mLogFilePath) Then
                    IO.Directory.CreateDirectory(mLogFilePath)
                End If
            Catch ex As Exception

            End Try

            Return IO.Directory.Exists(mLogFilePath)
        End Function

        Public Shared ReadOnly Property LogFilePath_ROOT As String
            Get
                Return mLogFilePath_ROOT
            End Get
        End Property


        Public Shared ReadOnly Property LogFileBasePath() As String
            Get
                Return mLogFileBasePath
            End Get
        End Property


        Private Shared mLogFilePath_ROOT As String
        Private Shared mLogFilePath As String
        Private Shared mLogFileBasePath As String
        Private Shared mLogFile As String = ""
        Private Shared pPrevDepth As Integer = 0
        Private Shared pDoNotStoreLogLevel As Boolean = False

        Private Shared mLogExtension As String = "log"
        Public Shared Property LogExtension() As String
            Get
                Return mLogExtension
            End Get
            Set(ByVal value As String)
                mLogExtension = value
            End Set
        End Property

        Private Shared mLogNameSuffix As String = ""
        Public Shared Property LogNameSuffix() As String
            Get
                Return mLogNameSuffix
            End Get
            Set(ByVal value As String)
                mLogNameSuffix = value
            End Set
        End Property

        Private Shared mDebugging As Boolean = True
        Public Shared Property Debugging() As Boolean
            Get
                Return mDebugging
            End Get
            Set(ByVal value As Boolean)
                mDebugging = value
            End Set
        End Property

        Private Shared mIncludeCaller As Boolean = False
        Public Shared Property IncludeCaller() As Boolean
            Get
                Return mIncludeCaller
            End Get
            Set(ByVal value As Boolean)
                mIncludeCaller = value
            End Set
        End Property

        Private Shared mExtraPrefix As String
        Public Shared Property ExtraPrefix() As String
            Get
                Return mExtraPrefix
            End Get
            Set(ByVal value As String)
                mExtraPrefix = value
            End Set
        End Property

        Public Enum MESSAGE_TYPE
            LOG_DEFAULT = 0
            LOG_WARNING = 1
            LOG_ERROR = 2
            LOG_DEBUG = 6
        End Enum

        Public Shared Property LogFilePath() As String
            Get
                Return mLogFilePath
            End Get
            Set(ByVal value As String)
                If value.EndsWith("\") Then
                    mLogFilePath = value
                Else
                    mLogFilePath = value & "\"
                End If
            End Set
        End Property

        Public Shared Property LogFileName() As String
            Get
                Return mLogFile
            End Get
            Set(ByVal value As String)
                mLogFile = value
            End Set
        End Property

        Public Shared Function WriteMessage(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteMessageRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteWarning(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteWarningRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteError(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteErrorRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteDebug(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteDebugRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteRelative(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iRelativeDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            pDoNotStoreLogLevel = True
            Return Write(sMessage, cMessageType, pPrevDepth + iRelativeDepth)
        End Function

        Public Shared Function WriteRelative(ByVal sMessagePrefix As String, ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iRelativeDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            pDoNotStoreLogLevel = True
            Return Write(sMessagePrefix, sMessage, cMessageType, pPrevDepth + iRelativeDepth)
        End Function

        Public Shared Function Write(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write("", sMessage, cMessageType, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function Write(ByVal sMessagePrefix As String, ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String, sMsgCallerSuffix As String, iDepthStep As Integer

            If iDepth < 0 Then
                iDepth = 0
            End If

            If Not mDebugging And cMessageType = MESSAGE_TYPE.LOG_DEBUG Then
                bDoNotWriteToFile = True
            End If

            sMessage = Trim(sMessage)

            Select Case cMessageType
                Case MESSAGE_TYPE.LOG_WARNING
                    sMsgTypePrefix = "[*] "
                Case MESSAGE_TYPE.LOG_ERROR
                    sMsgTypePrefix = "[!] "
                Case MESSAGE_TYPE.LOG_DEBUG
                    sMsgTypePrefix = "[#] "
                Case MESSAGE_TYPE.LOG_DEFAULT
                    sMsgTypePrefix = "[.] "
                Case Else
                    sMsgTypePrefix = "[?] "
            End Select


            If mIncludeCaller Then
                Try
                    Dim stackTrace As New StackTrace()

                    sMsgCallerSuffix = " ; (" & stackTrace.GetFrame(1).GetMethod().Name & ")"
                Catch ex As Exception
                    sMsgCallerSuffix = " ; (error getting caller: " & ex.Message & ")"
                End Try
            Else
                sMsgCallerSuffix = ""
            End If


            sMsgDatePrefix = Now.ToString & " - "

            sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

            If Not sMessagePrefix.Equals("") Then
                sMsgPrefix = sMsgPrefix & "[" & sMessagePrefix & "] - "
            End If

            If iDepth < pPrevDepth Then
                For iDepthStep = 1 To iDepth
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & vbCrLf
                sMsgPrefix = sMsgPrefix & sMsgTypePrefix & sMsgDatePrefix
            End If

            If iDepth > 0 Then
                For iDepthStep = 1 To iDepth - 1
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & "|-"
            End If

            If Not bDoNotWriteToFile Then
                UpdateLogfile(sMsgPrefix & sMessage & sMsgCallerSuffix)
            End If

            If Not pDoNotStoreLogLevel Then
                pPrevDepth = iDepth
            End If

            pDoNotStoreLogLevel = False

            Return sMsgPrefix & sMessage & sMsgCallerSuffix
        End Function

        Public Shared Sub WriteWithoutDate(ByVal sMessage As String)
            UpdateLogfile(sMessage)
        End Sub

        Public Shared Sub WriteEmptyLine()
            WriteWithoutDate(" ")
        End Sub

        Private Shared Sub UpdateLogfile(ByVal sString As String)
            Try
                Dim sFile As String = mLogFilePath & "\" & mLogFile & "." & mLogNameSuffix & "." & mLogExtension

                sFile = sFile.Replace("..", ".")

                Using sw As New IO.StreamWriter(sFile, True)
                    sw.WriteLine(sString)
                End Using

                mLogNameSuffix = ""
            Catch ex As Exception

            End Try
        End Sub
    End Class


    Public Class Windows
        Public Class Startup
            Private Shared ReadOnly StartupApproved_enabled_bytes As Byte() = {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            Private Shared ReadOnly StartupApproved_disabled_bytes As Byte() = {3, 0, 29, 16, 113, 209, 7, 29, 16, 113, 209, 7}

            Private Shared ReadOnly KEY_Windows_RunAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run"
            Private Shared ReadOnly KEY_Windows_StartupApproved As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run"

            Public Shared Sub SetStartupProgram(AppName As String, AppPath As String)
                Dim sError As String = ""

                Registry.RemoveValue(KEY_Windows_RunAtStartup, AppName, sError)
                Registry.RemoveValue(KEY_Windows_StartupApproved, AppName)
                Registry64.RemoveValue(KEY_Windows_RunAtStartup, AppName, sError)
                Registry64.RemoveValue(KEY_Windows_StartupApproved, AppName)

                If Environment.Is64BitOperatingSystem Then
                    Registry64.SetValue_String(KEY_Windows_RunAtStartup, AppName, AppPath)
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                Else
                    Registry.SetValue_String(KEY_Windows_RunAtStartup, AppName, AppPath)
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                End If
            End Sub

            Public Shared Sub EnableStartupProgram(AppName As String)
                If Environment.Is64BitOperatingSystem Then
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                Else
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                End If
            End Sub

            Public Shared Sub DisableStartupProgram(AppName As String)
                If Environment.Is64BitOperatingSystem Then
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_disabled_bytes)
                Else
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_disabled_bytes)
                End If
            End Sub

        End Class
    End Class

    Public Class XOrObfuscation
        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "$#(98)&_Ndf9m0987e5WRW#%rtefgm03947"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Private Shared mLastError As String
        Public Shared ReadOnly Property LastError() As String
            Get
                Return mLastError
            End Get
        End Property

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            mLastError = ""

            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            Try
                If PlainText.Length > 1000 Then
                    Throw New Exception("input string too long (max 1000)")
                End If

                InitializePasshrases()


                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    If AscW(Mid(PlainText, i + 1, 1)) > 255 Then
                        'uh oh, Unicode
                        Throw New Exception("Only ASCII is supported!")
                    End If

                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(AscW(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(AscW(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""

                mLastError = ex.Message
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String


            Try
                InitializePasshrases()

                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & ChrW(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & ChrW(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                PlainText_RandomPassphrase = ""

                mLastError = ex.Message
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = AscW(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            temp = _HexToNo(Mid(BinVal, 2, 1))
            iVal = iVal + temp
            temp = _HexToNo(Mid(BinVal, 1, 1))
            iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class

    Public Class Types
        Public Shared Function CastStringToInteger_safe(s As String) As Integer
            Dim iTmp As Integer = -1

            If Integer.TryParse(s, iTmp) Then

            End If

            Return iTmp
        End Function

        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function LeadingZero(num As Integer, Optional len As Integer = 2) As String
            Dim sNum As String = num.ToString

            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Public Shared Function RandomString(Length As Integer) As String
            Return RandomString(Length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        End Function

        Public Shared Function RandomString(Length As Integer, CharsToChooseFrom As String) As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function

        Public Shared Function FromNormalDateToHBDate(Optional WithTime As Boolean = False) As String
            Return FromNormalDateToHBDate(Now, WithTime)
        End Function

        Public Shared Function FromNormalDateToHBDate(d As Date, Optional WithTime As Boolean = False) As String
            Dim sTmp As String = d.ToString("yyyy-MM-dd")

            If WithTime Then
                sTmp &= " " & d.ToString("HH:mm:ss")
            End If

            Return sTmp
        End Function

        Public Shared Function FromHBDateToNormalDate(s As String) As Date
            Dim d As Date

            Try
                d = Date.Parse(s)
            Catch ex As Exception
                d = Nothing
            End Try

            Return d
        End Function

        Public Shared Function IsValidHBDate(s As String) As Boolean
            Dim d As Date

            Try
                d = Date.Parse(s)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function MaskString(s As String, Optional Mask As String = "*", Optional ShowCharsAtStartCount As Integer = 2, Optional ShowCharsAtEndCound As Integer = 0) As String
            If s.Length > (ShowCharsAtStartCount + ShowCharsAtEndCound) Then
                Return String.Concat(s.Substring(0, ShowCharsAtStartCount), "".PadLeft(s.Length - (ShowCharsAtStartCount + ShowCharsAtEndCound), Mask), s.Substring(s.Length - ShowCharsAtEndCound, ShowCharsAtEndCound))
            Else
                Return s
            End If
        End Function

        Public Shared Function String2Boolean(s As String) As Boolean
            s = s.ToLower

            Return (s.Equals("yes") Or s.Equals("true") Or s.Equals("1"))
        End Function
    End Class

    Public Class FilesAndFolders
        Public Shared Function GetFileVersion(sFile As String, Optional iParts As Integer = -1) As String
            If Not IO.File.Exists(sFile) Then
                Return ""
            End If

            Dim sVersion As String = ""

            Try
                sVersion = FileVersionInfo.GetVersionInfo(sFile).FileVersion
                sVersion = ParseFileVersion(sVersion, iParts)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

            Return sVersion
        End Function

        Public Shared Function ParseFileVersion(sVersion As String, Optional iParts As Integer = -1) As String
            Try
                Dim sVersionParts As String()

                If sVersion Is Nothing Then
                    sVersion = ""
                End If

                sVersion = sVersion.Split(" ")(0)

                If iParts > 0 Then
                    sVersionParts = sVersion.Split(".")

                    If sVersionParts.Length > iParts Then
                        sVersion = String.Join(".", sVersionParts.Take(iParts))
                    End If
                End If

                Return sVersion
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

            Return ""
        End Function


        Public Shared Function GetFileCompileDate(sFile As String) As String
            Return GetFileCompileDate(sFile, "yyyy-MM-dd HH:mm:ss")
        End Function

        Public Shared Function GetFileCompileDate(sFile As String, DateFormat As String) As String
            Try
                Const PeHeaderOffset As Integer = 60
                Const LinkerTimestampOffset As Integer = 8

                Dim b(2047) As Byte
                'Dim s As IO.Stream
                Dim s As New Object

                Try
                    s = New IO.FileStream(sFile, IO.FileMode.Open, IO.FileAccess.Read)
                    s.Read(b, 0, 2048)
                Finally
                    Try
                        If Not s Is Nothing Then s.Close()
                    Catch ex As Exception

                    End Try
                End Try


                Dim i As Integer = BitConverter.ToInt32(b, PeHeaderOffset)

                Dim SecondsSince1970 As Integer = BitConverter.ToInt32(b, i + LinkerTimestampOffset)
                Dim dt As New DateTime(1970, 1, 1, 0, 0, 0)
                dt = dt.AddSeconds(SecondsSince1970)
                dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours)

                Return dt.ToString(DateFormat)
            Catch ex As Exception
                Return GetFileCreationDate(sFile, DateFormat)
            End Try

        End Function

        Public Shared Function GetFileCreationDate(sFile As String) As String
            Return GetFileCreationDate(sFile, "yyyy-MM-dd")
        End Function

        Public Shared Function GetFileCreationDate(sFile As String, DateFormat As String) As String
            Try
                Dim dDate As Date = IO.File.GetCreationTime(sFile)

                Return dDate.ToString(DateFormat)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String) As String
            Return GetFileLastModifiedDate(sFile, "yyyy-MM-dd")
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String, DateFormat As String) As String
            Try
                Dim dDate As Date = IO.File.GetLastWriteTime(sFile)

                Return dDate.ToString(DateFormat)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDateAsDate(sFile As String) As Date
            Dim dDate As New Date(0)

            Try
                dDate = IO.File.GetLastWriteTime(sFile)
            Catch ex As Exception

            End Try

            Return dDate
        End Function

        Public Shared Function GetFileTypeFromRegistry(FileExtension As String) As String
            Try
                If FileExtension.Contains(" ") Then
                    FileExtension = ""
                    Throw New Exception("space in extension")
                End If

                Dim reg1 As Object = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & FileExtension, "", FileExtension)

                If reg1 Is Nothing Then
                    Throw New Exception("Unknown file type")
                End If

                If reg1.ToString.Equals(FileExtension) Then
                    Throw New Exception("Unknown file type")
                End If


                Dim reg2 As Object = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & reg1, "", FileExtension)

                If reg2 Is Nothing Then
                    Throw New Exception("Unknown file type")
                End If

                If reg2.ToString.Equals(FileExtension) Then
                    Throw New Exception("Unknown file type")
                End If

                Return reg2
            Catch ex As Exception
                If FileExtension.StartsWith(".") Then
                    FileExtension = FileExtension.Substring(1, FileExtension.Length - 1)
                End If

                If FileExtension.Equals("") Then
                    Return "File"
                Else
                    Return FileExtension & " File"
                End If
            End Try

            'Return My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" &
            '                                     My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & FileExtension, "", FileExtension).ToString, "", FileExtension).ToString
        End Function

        Public Shared Function GetFileDescription(FilePath As String) As String
            Dim _s As String = ""

            If Not IO.File.Exists(FilePath) Then
                Return "(" & FilePath & ")"
            End If

            Try
                Dim fi As FileVersionInfo = FileVersionInfo.GetVersionInfo(FilePath)

                _s = fi.FileDescription
            Catch ex As Exception
                _s = "(" & FilePath & ")"
            End Try

            Return _s
        End Function

        Public Class FileSize
            Public Enum HumanReadable
                Use1024 = 0
                Use1024i = 1
                Use1000 = 2
            End Enum

            Public Shared Function GetHumanReadable(FileSize As Long) As String
                Return GetHumanReadable(FileSize, HumanReadable.Use1024)
            End Function

            Public Shared Function GetHumanReadable(FileSize As Long, Base1024 As HumanReadable) As String
                If FileSize < 0 Then
                    Return ""
                End If

                Select Case Base1024
                    Case HumanReadable.Use1000
                        Select Case FileSize
                            Case > (1000 * 1000 * 1000)
                                Return CInt(FileSize / (1000 * 1000 * 1000)).ToString & "GB"
                            Case > (1000 * 1000)
                                Return CInt(FileSize / (1000 * 1000)).ToString & "MB"
                            Case > 1000
                                Return CInt(FileSize / 1000).ToString & "kB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case HumanReadable.Use1024
                        Select Case FileSize
                            Case > (1024 * 1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024 * 1024)).ToString & "GB"
                            Case > (1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024)).ToString & "MB"
                            Case > 1024
                                Return CInt(FileSize / 1024).ToString & "KB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case HumanReadable.Use1024i
                        Select Case FileSize
                            Case > (1024 * 1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024 * 1024)).ToString & "GiB"
                            Case > (1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024)).ToString & "MiB"
                            Case > 1024
                                Return CInt(FileSize / 1024).ToString & "KiB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case Else
                        Return "?B"
                End Select
            End Function
        End Class

        Public Shared Function GetProgramFilesFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sPath.Equals(String.Empty) Then
                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetProgramFilesFolder64bit(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetGuestTekFolder() As String
            Return IO.Path.Combine(GetProgramFilesFolder, "GuestTek")
        End Function

        Public Shared Function GetDefaultBackupFolder() As String
            Dim sTmp As String = IO.Path.Combine(GetProgramFilesFolder, "GuestTek\_backups")

            sTmp &= "\%%Application.ProductName%%\%%Application.ProductVersion%%\%%Backup.Date%%"
            sTmp = sTmp.Replace("%%Application.ProductName%%", My.Application.Info.ProductName)
            sTmp = sTmp.Replace("%%Application.ProductVersion%%", My.Application.Info.Version.ToString)
            sTmp = sTmp.Replace("%%Backup.Date%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))

            Return sTmp
        End Function

        Public Shared Function GetWindowsFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.Windows)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetAppDataLocalFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function


        Public Shared Function GetCurrentFolder() As String
            Dim P As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)

            Return New Uri(P).LocalPath
        End Function

        Public Shared Function GetExecutablePath() As String
            Dim sPath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)

            sPath = New Uri(sPath).LocalPath

            Return sPath
        End Function

        Public Shared Function GetNewestFile(Path As String) As String
            Return GetNewestFile(Path, "*.*", False)
        End Function

        Public Shared Function GetNewestFile(Path As String, SearchPattern As String) As String
            Return GetNewestFile(Path, SearchPattern, False)
        End Function

        Public Shared Function GetNewestFile(Path As String, SearchPattern As String, SearchPatternIsRegularExpression As Boolean) As String
            If Not IO.Directory.Exists(Path) Then
                Return ""
            End If


            If SearchPatternIsRegularExpression Then
                Try
                    Dim _regex = New Regex(SearchPattern)


                    Return Directory.GetFiles(Path).Where(Function(f) _regex.IsMatch(IO.Path.GetFileName(f))).OrderByDescending(Function(f) New FileInfo(f).LastWriteTime).FirstOrDefault()
                Catch ex As Exception
                    'invalid searchpattern

                    Return ex.Message
                End Try
            Else
                Return Directory.GetFiles(Path, SearchPattern).OrderByDescending(Function(f) New FileInfo(f).LastWriteTime).FirstOrDefault()
            End If
        End Function

        Public Shared Function GetNewestFolder(Path As String) As String
            If IO.Directory.Exists(Path) Then
                Return Directory.GetDirectories(Path).OrderByDescending(Function(d) New DirectoryInfo(d).LastWriteTime).FirstOrDefault()
            Else
                Return ""
            End If
        End Function

        Public Class Folder
            Public Shared Function IsRootFolder(_dir As String) As Boolean
                Dim _di As New DirectoryInfo(_dir)

                If _di.Parent Is Nothing Then
                    Return True
                Else
                    Return False
                End If
            End Function

            Public Shared Function FindRecursive(rootdir As String) As List(Of String)
                ' This list stores the results.
                Dim result As New List(Of String)

                ' This stack stores the directories to process.
                Dim stack As New Stack(Of String)

                ' Add the initial directory
                stack.Push(rootdir)

                ' Continue processing for each stacked directory
                Do While (stack.Count > 0)
                    ' Get top directory string
                    Dim dir As String = stack.Pop
                    Try
                        ' Loop through all subdirectories and add them to the stack.
                        Dim directoryName As String
                        For Each directoryName In Directory.GetDirectories(dir)
                            result.Add(directoryName)
                            stack.Push(directoryName)
                        Next

                    Catch ex As Exception
                    End Try
                Loop

                ' Return the list
                Return result
            End Function

            Public Shared Function CleanFolder(folder As String, security_string As String) As Long
                If Not security_string.Equals("I_am_sure") Then
                    Return -1
                End If

                'DANGER DANGER
                Dim iErrors As Long = 0

                For Each d As String In IO.Directory.GetDirectories(folder)
                    Try
                        IO.Directory.Delete(d, True)
                    Catch ex As Exception
                        iErrors += 1
                    End Try
                Next

                For Each f As String In Directory.GetFiles(folder)
                    Try
                        IO.File.Delete(f)
                    Catch ex As Exception
                        iErrors += 1
                    End Try
                Next

                Return iErrors
            End Function

            Public Shared Function DirectoryFileCount(ByVal sPath As String, Optional ByVal bRecursive As Boolean = False) As Long
                Dim Count As Long = 0
                Dim diDir As New IO.DirectoryInfo(sPath)

                Try
                    Dim fil As FileInfo

                    For Each fil In diDir.GetFiles()
                        Count += 1
                    Next fil

                    If bRecursive = True Then
                        Dim diSubDir As DirectoryInfo

                        For Each diSubDir In diDir.GetDirectories()
                            Count += DirectoryFileCount(diSubDir.FullName, True)
                        Next diSubDir
                    End If

                    Return Count
                Catch ex As System.IO.FileNotFoundException
                    Return 0
                Catch exx As Exception
                    Return 0
                End Try

                Return Count
            End Function

            Public Shared Function DirectorySize(ByVal sPath As String, Optional ByVal bRecursive As Boolean = False) As Long
                Dim Size As Long = 0
                Dim diDir As New IO.DirectoryInfo(sPath)

                Try
                    Dim fil As FileInfo

                    For Each fil In diDir.GetFiles()
                        Size += fil.Length
                    Next fil

                    If bRecursive = True Then
                        Dim diSubDir As DirectoryInfo

                        For Each diSubDir In diDir.GetDirectories()
                            Size += DirectorySize(diSubDir.FullName, True)
                        Next diSubDir
                    End If

                    Return Size
                Catch ex As System.IO.FileNotFoundException
                    Return 0
                Catch exx As Exception
                    Return 0
                End Try

                Return Size
            End Function
        End Class

        Public Class File
            Public Shared Function Find(rootdir As String, searchPattern As String) As List(Of IO.FileInfo)
                Try
                    Dim lList As New List(Of IO.FileInfo)

                    If Not IO.Directory.Exists(rootdir) Then
                        Throw New Exception("'" & rootdir & "' not found")
                    End If

                    Dim files() As String = Directory.GetFiles(rootdir, searchPattern)

                    For Each file As String In files
                        lList.Add(New IO.FileInfo(file))
                    Next

                    Return lList
                Catch ex As Exception
                    Return Nothing
                End Try
            End Function

            Public Shared Function FindRecursive(rootdir As String, searchPattern As String) As List(Of String)
                ' This list stores the results.
                Dim result As New List(Of String)

                ' This stack stores the directories to process.
                Dim stack As New Stack(Of String)

                ' Add the initial directory
                stack.Push(rootdir)

                ' Continue processing for each stacked directory
                Do While (stack.Count > 0)
                    ' Get top directory string
                    Dim dir As String = stack.Pop
                    Try
                        ' Add all immediate file paths
                        result.AddRange(Directory.GetFiles(dir, searchPattern))

                        ' Loop through all subdirectories and add them to the stack.
                        Dim directoryName As String
                        For Each directoryName In Directory.GetDirectories(dir)
                            stack.Push(directoryName)
                        Next

                    Catch ex As Exception
                    End Try
                Loop

                ' Return the list
                Return result
            End Function
        End Class
    End Class


#Region "registry"
    Public Class Registry '32-bit 
        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function KeyExists_CU(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.CurrentUser, keyName)
        End Function

        Public Shared Function KeyExists_LM(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.LocalMachine, keyName)
        End Function

        Private Shared Function _KeyExists(keyHive As Microsoft.Win32.RegistryHive, keyName As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.LocalMachine Then
                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyHive = RegistryHive.CurrentUser Then
                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
            Return FindKeyByString(keyName, SearchString, "", ErrorMessage)
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, SearchInValueName As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchInValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchInValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchInValueName).ToString
                            End If


                            subkey.Close()

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                bRet = True

                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function FindValueByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Object
            Dim oRet As Object = Nothing
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each valueName As String In key.GetValueNames
                    If valueName.ToLower.Equals(SearchString.ToLower) Then
                        oRet = key.GetValue(valueName)

                        Exit For
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                oRet = Nothing
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return oRet
        End Function

        Public Shared Function FindKeyBySubValueAndReturnValue(keyName As String, SearchString As String, SearchValueName As String, ReturnValueName As String, ByRef ReturnValue As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"
            ReturnValue = "not found"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchValueName).ToString
                            End If

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                If Not subkey.GetValue(ReturnValueName) Is Nothing Then
                                    ReturnValue = subkey.GetValue(ReturnValueName).ToString

                                    bRet = True
                                End If
                            End If

                            subkey.Close()

                            If bRet Then
                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SetValue_Boolean(keyName As String, valueName As String, value As Boolean)
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "yes", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "no", Microsoft.Win32.RegistryValueKind.String)
                End If
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "binary"
        Public Shared Function GetValue_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_Binary(keyName As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

        Public Shared Sub SetValue_Binary(keyName As String, valueName As String, value As Object)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.Binary)

            Catch ex As Exception

            End Try
        End Sub

        Public Shared Function GetValue_ReadOnly_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_ReadOnly_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_ReadOnly_Binary(keyname As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyname, valueName, defaultValue)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

#End Region

#Region "string" ' Microsoft.Win32.RegistryKey.OpenSubKey(String name, Boolean writable)
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                sTmp = defaultValue
            Else
                sTmp = Convert.ToString(oTmp)
            End If

            Return sTmp
        End Function

        Public Shared Sub SetValue_String(keyName As String, valueName As String, value As String)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)
            Catch ex As Exception

            End Try
        End Sub


        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String, defaultValue As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, defaultValue)
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(hiveName, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyname As String, valueName As String, defaultValue As String) As String
            Try
                Dim oTmp As Object, sTmp As String
                Dim r As RegistryKey

                If hiveName = RegistryHive.LocalMachine Then
                    r = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyname, False)
                ElseIf hiveName = RegistryHive.CurrentUser Then
                    r = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyname, False)
                Else
                    Return defaultValue
                End If

                oTmp = r.GetValue(valueName, defaultValue)
                sTmp = Convert.ToString(oTmp)

                Return sTmp
            Catch ex As Exception
                Return "GetValue_ReadOnly_String(): " & ex.Message
            End Try
        End Function
#End Region

#Region "long"
        Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
            Return GetValue_Long(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
            Dim oTmp As Object, iTmp As Long

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                iTmp = defaultValue
            Else
                iTmp = Helpers.Types.CastToLong_safe(oTmp)
            End If


            Return iTmp
        End Function

        Public Shared Function SetValue_Long(keyName As String, valueName As String, value As Long) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.QWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                iTmp = defaultValue
            Else
                iTmp = Helpers.Types.CastToInteger_safe(oTmp)
            End If


            Return iTmp
        End Function

        Public Shared Function SetValue_Integer(keyName As String, valueName As String, value As Integer) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "misc"
        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, True)
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Public Shared Function DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(keyHive, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Private Shared Function _DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Try
                Dim regKey As RegistryKey

                If keyHive = RegistryHive.CurrentUser Then
                    regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyRoot, True)
                ElseIf keyHive = RegistryHive.LocalMachine Then
                    regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyRoot, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                'delete all subkeys
                regKey.DeleteSubKeyTree(keyToDelete)

                If bKeepKeyToDelete Then
                    'put back the root sub key
                    regKey.CreateSubKey(keyToDelete)
                End If

                Return True
            Catch ex As Exception

            End Try

            Return False
        End Function
#End Region
    End Class

    Public Class Registry64
        Private Shared Function _String2RegKey(keyName As String) As Microsoft.Win32.RegistryKey
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return key
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function KeyExists_CU(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.CurrentUser, keyName)
        End Function

        Public Shared Function KeyExists_LM(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.LocalMachine, keyName)
        End Function

        Private Shared Function _KeyExists(keyHive As Microsoft.Win32.RegistryHive, keyName As String) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.LocalMachine Or keyHive = RegistryHive.CurrentUser Then
                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(keyHive, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function


        Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
            Return FindKeyByString(keyName, SearchString, "", ErrorMessage)
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, SearchInValueName As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchInValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchInValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchInValueName).ToString
                            End If


                            subkey.Close()

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                bRet = True

                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function FindKeyBySubValueAndReturnValue(keyName As String, SearchString As String, SearchValueName As String, ReturnValueName As String, ByRef ReturnValue As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"
            ReturnValue = "not found"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchValueName).ToString
                            End If

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                If Not subkey.GetValue(ReturnValueName) Is Nothing Then
                                    ReturnValue = subkey.GetValue(ReturnValueName).ToString

                                    bRet = True
                                End If
                            End If

                            subkey.Close()

                            If bRet Then
                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function


        'Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
        '    Dim bRet As Boolean = False

        '    ErrorMessage = "ok"

        '    Try
        '        Dim key_root As Microsoft.Win32.RegistryKey
        '        Dim key As Microsoft.Win32.RegistryKey

        '        If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
        '            keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

        '            key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
        '            key = key_root.OpenSubKey(keyName, False)
        '            If key Is Nothing Then
        '                Throw New Exception(keyName & " not found!")
        '            End If
        '        ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
        '            keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

        '            key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
        '            key = key_root.OpenSubKey(keyName, False)
        '            If key Is Nothing Then
        '                Throw New Exception(keyName & " not found!")
        '            End If
        '        Else
        '            Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
        '        End If

        '        For Each subKeyName As String In key.GetSubKeyNames()
        '            If subKeyName.ToLower.Contains(SearchString.ToLower) Then
        '                bRet = True

        '                Exit For
        '            End If
        '        Next
        '    Catch ex As Exception
        '        ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

        '        bRet = False
        '    End Try

        '    Return bRet
        'End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "_GetValue & _SetValue & _DeleteValue"
        Private Shared Function _DeleteValue(keyName As String, valueName As String, thrownOnMissingValue As Boolean) As Boolean
            Dim ret As Boolean = False

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key.DeleteValue(valueName, thrownOnMissingValue)
                ret = True
            Catch ex As Exception

                ret = False
            End Try

            Return ret
        End Function

        Private Shared Function _GetValue(keyName As String, valueName As String) As Object
            Return _GetValue(keyName, valueName, True)
        End Function

        Private Shared Function _GetValue(keyName As String, valueName As String, writable As Boolean) As Object
            Dim oTmp As Object

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                oTmp = key.GetValue(valueName, Nothing)
            Catch ex As Exception
                oTmp = Nothing
            End Try

            Return oTmp
        End Function

        Private Shared Function _SetValue(keyName As String, valueName As String, value As Object, valueKind As RegistryValueKind) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey


                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key.SetValue(valueName, value, valueKind)

                Return True
            Catch ex As Exception

                Return False
            End Try
        End Function


#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String, bRet As Boolean

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    bRet = defaultValue
                End If

                sTmp = Convert.ToString(oTmp)
                If sTmp.Equals("yes") Or sTmp.Equals("no") Then
                    bRet = sTmp.Equals("yes")
                Else
                    bRet = defaultValue
                End If
            Catch ex As Exception
                bRet = defaultValue
            End Try

            Return bRet
        End Function

        Public Shared Function SetValue_Boolean(keyName As String, valueName As String, value As Boolean) As Boolean
            If value Then
                Return SetValue_String(keyName, valueName, "yes")
            Else
                Return SetValue_String(keyName, valueName, "no")
            End If
        End Function
#End Region

#Region "binary"
        Public Shared Function GetValue_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_Binary(keyName As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

        Public Shared Function SetValue_Binary(keyName As String, valueName As String, value As Object) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.Binary)
        End Function


        Public Shared Function GetValue_ReadOnly_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_ReadOnly_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_ReadOnly_Binary(keyname As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = _GetValue(keyname, valueName, False)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

#End Region

#Region "string"
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function

        Public Shared Function SetValue_String(keyName As String, valueName As String, value As String) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.String)
        End Function


        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(keyname As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyname, valueName, False)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    iTmp = defaultValue
                Else
                    iTmp = Helpers.Types.CastToInteger_safe(oTmp)
                End If
            Catch ex As Exception
                iTmp = defaultValue
            End Try

            Return iTmp
        End Function

        Public Shared Function SetValue_Integer(keyName As String, valueName As String, value As Integer) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.DWord)
        End Function
#End Region

#Region "delete value"
        Public Shared Function DeleteValue(keyName As String, valueName As String) As Boolean
            Return DeleteValue(keyName, valueName, True)
        End Function

        Public Shared Function DeleteValue(keyName As String, valueName As String, thrownOnMissingValue As Boolean) As Boolean
            Return _DeleteValue(keyName, valueName, thrownOnMissingValue)
        End Function
#End Region
#End Region

#Region "misc"
        Public Shared Function CreateKey(keyName As String) As Boolean
            Dim bRet As Boolean = False

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key_new As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key_new = key_root.CreateSubKey(keyName)

                If Not key_new Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function DeleteSubKey(keyName As String, keyToDelete As String) As Boolean
            If keyName.StartsWith("HKEY_CURRENT_USER\") Then
                keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                Return _DeleteSubKey(RegistryHive.CurrentUser, keyName, keyToDelete, True)
            ElseIf keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                Return _DeleteSubKey(RegistryHive.LocalMachine, keyName, keyToDelete, True)
            Else
                Return False
            End If
        End Function

        Public Shared Function DeleteSubKey(keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyName, keyToDelete, bKeepKeyToDelete)
        End Function

        Public Shared Function DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(keyHive, keyName, keyToDelete, bKeepKeyToDelete)
        End Function

        Private Shared Function _DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.CurrentUser Or keyHive = RegistryHive.LocalMachine Then
                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(keyHive, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = key_root.OpenSubKey(keyName, True)

                'delete all subkeys
                key.DeleteSubKeyTree(keyToDelete)

                If bKeepKeyToDelete Then
                    'put back the root sub key
                    key.CreateSubKey(keyToDelete)
                End If

                Return True
            Catch ex As Exception

            End Try

            Return False
        End Function
#End Region
    End Class
#End Region

End Class
