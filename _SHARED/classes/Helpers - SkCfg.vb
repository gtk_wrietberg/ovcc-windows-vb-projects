﻿

Imports System.Runtime.InteropServices
Imports System.Security.Principal
Imports Microsoft.Win32

Public Class Helpers
#Region "errors"
    Public Class Errors
        Public Class ErrorData
            Private mError_Timestamp As Date
            Private mError_Description As String
            Private mError_Caller As String

            Public Property Timestamp() As Date
                Get
                    Return mError_Timestamp
                End Get
                Set(ByVal value As Date)
                    mError_Timestamp = value
                End Set
            End Property

            Public Property Description() As String
                Get
                    Return mError_Description
                End Get
                Set(ByVal value As String)
                    mError_Description = value
                End Set
            End Property

            Public Property Caller() As String
                Get
                    Return mError_Caller
                End Get
                Set(ByVal value As String)
                    mError_Caller = value
                End Set
            End Property

            Public Overloads Function ToString() As String
                Return ToString(True, True)
            End Function

            Public Overloads Function ToString(IncludeTimestamp As Boolean) As String
                Return ToString(IncludeTimestamp, True)
            End Function

            Public Overloads Function ToString(IncludeTimestamp As Boolean, IncludeCaller As Boolean) As String
                Dim sResult As String = ""

                If IncludeTimestamp Then
                    sResult = mError_Timestamp.ToString & " - " & mError_Description
                Else
                    sResult = mError_Description
                End If

                If IncludeCaller Then
                    sResult = sResult & " - " & mError_Caller
                End If

                Return sResult
            End Function
        End Class

        Private Shared mMaxHistory As Integer = 25
        Private Shared lstErrors As New List(Of ErrorData)(mMaxHistory)

        Public Shared Sub Add(Description As String)
            Dim newError As New ErrorData

            newError.Description = Description
            newError.Timestamp = Date.Now

            Dim stackTrace As New StackTrace()

            newError.Caller = stackTrace.GetFrame(1).GetMethod().Name


            lstErrors.Insert(0, newError)

            If lstErrors.Count > mMaxHistory Then
                lstErrors.RemoveRange(mMaxHistory, lstErrors.Count - mMaxHistory)
            End If
        End Sub

        Public Shared Function GetLast() As String
            Return GetLast(True, True)
        End Function

        Public Shared Function GetLast(IncludeTimestamp As Boolean) As String
            Return GetLast(IncludeTimestamp, True)
        End Function

        Public Shared Function GetLast(IncludeTimestamp As Boolean, IncludeCaller As Boolean) As String
            If lstErrors.Count > 0 Then
                Return lstErrors.Item(0).ToString(IncludeTimestamp, IncludeCaller)
            Else
                Return ""
            End If
        End Function

        Public Overloads Shared Function ToString(Optional IncludeTimestamp As Boolean = True, Optional AllErrors As Boolean = False) As String
            Dim sResult As String = ""
            Dim currentError As New ErrorData

            If lstErrors.Count = 0 Then
                Return ""
            End If

            If lstErrors.Count = 1 Then
                AllErrors = False
            End If

            If AllErrors Then
                For i As Integer = 0 To lstErrors.Count - 2
                    currentError = lstErrors.Item(i)
                    sResult &= currentError.ToString & vbCrLf
                Next

                currentError = lstErrors.Item(lstErrors.Count - 1)
                sResult &= currentError.ToString
            Else
                currentError = lstErrors.Item(0)
                sResult = currentError.ToString
            End If

            Return sResult
        End Function
    End Class
#End Region

#Region "Processes"
    Public Class Processes
        Public Class HelpersProcessesException
            Inherits Exception

            Public Sub New()
            End Sub

            Public Sub New(ByVal message As String)
            End Sub

            Public Sub New(ByVal message As String, ByVal inner As Exception)
            End Sub
        End Class

        <DllImport("advapi32.dll", SetLastError:=True)>
        Private Shared Function OpenProcessToken(ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)>
        Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        Private Shared mLastError As String = ""
        Public Shared ReadOnly Property LastError As String
            Get
                Dim sTmp As String = mLastError

                mLastError = ""

                Return sTmp
            End Get
        End Property

        Private Shared Function ConvertPasswordStringToSecureString(ByVal str As String) As System.Security.SecureString
            Dim password As New System.Security.SecureString

            For Each c As Char In str.ToCharArray
                password.AppendChar(c)
            Next

            Return password
        End Function

        Public Shared Function IsProcessRunningExactMatch(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String


            sSearchProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

            If Not sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower & ".exe"
            End If

            Try
                For Each p As Process In Process.GetProcesses()
                    Try
                        sProcessName__ToLower = IO.Path.GetFileName(p.Modules(0).FileName).ToLower
                    Catch ex As Exception
                        sProcessName__ToLower = ""
                    End Try

                    If sSearchProcessName__ToLower.Equals(sProcessName__ToLower) Then
                        bRet = True
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function IsProcessRunning(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sProcessName__ToLower As String

            Try
                sProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

                If sProcessName.EndsWith(".exe") Then
                    sProcessName__ToLower = sProcessName__ToLower.Replace(".exe", "")
                End If


                Dim p As Process() = Process.GetProcessesByName(sProcessName__ToLower)

                If p.Length > 0 Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function IsProcessRunning_broken(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String


            sSearchProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            Try
                For Each p As Process In Process.GetProcesses
                    sProcessName__ToLower = p.ProcessName.ToLower
                    If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                        bRet = True
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function KillProcess(Pid As Integer) As Boolean
            Try
                Dim p As Process = Process.GetProcessById(Pid)

                If p IsNot Nothing Then
                    p.Kill()
                End If

                Return True
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Function KillProcesses(sProcessName As String) As Integer
            Dim sSearchProcessName__ToLower As String, sProcessName__ToLower As String
            Dim iCount As Integer = 0

            sSearchProcessName__ToLower = sProcessName.ToLower

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            Try
                For Each p As Process In Process.GetProcesses
                    sProcessName__ToLower = p.ProcessName.ToLower
                    If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                        Try
                            p.Kill()
                            iCount += 1
                        Catch ex As Exception

                        End Try
                    End If
                Next
            Catch ex As Exception
                iCount = -1
            End Try

            Return iCount
        End Function

        Public Shared Function IsUserLoggedIn(UserName As String, Optional CompletelyArbitraryProcessCountThreshold As Integer = 2) As Boolean
            Dim iCount As Integer

            iCount = CountProcessesOwnedByUser(UserName)

            If iCount >= CompletelyArbitraryProcessCountThreshold Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
            Dim wiName As String, lCount As Long

            lCount = 0

            For Each p As Process In Process.GetProcesses
                Try
                    Dim hToken As IntPtr
                    If OpenProcessToken(p.Handle, TokenAccessLevels.Query, hToken) Then
                        Using wi As New WindowsIdentity(hToken)
                            wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                            If wiName.ToLower = sUserNameToCheck.ToLower Then
                                lCount += 1
                            End If
                        End Using
                        CloseHandle(hToken)
                    End If
                Catch ex As Exception

                End Try
            Next

            Return lCount
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String) As Process
            Dim p As New Process

            If _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, sWorkingDirectory, False, p) Then
                Return p
            Else
                'wow, this makes sense
                Return p
            End If
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String, bHidden As Boolean) As Process
            Dim p As New Process

            If _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, sWorkingDirectory, bHidden, p) Then
                Return p
            Else
                'wow, this makes sense
                Return p
            End If
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOutMs As Integer) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, "", waitForExit, timeOutMs, False)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, "", waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, sWorkingDirectory, waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, bLoadUserProfile As Boolean, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, bLoadUserProfile, cmdArguments, sWorkingDirectory, waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcess(cmdLine As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOutMs As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, "", "", False, cmdArguments, "", waitForExit, timeOutMs, False)
        End Function

        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, bLoadUserProfile As Boolean, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            pInfo.WorkingDirectory = sWorkingDirectory
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = bLoadUserProfile
                pInfo.Verb = "runas"
                If hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOutMs)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        Errors.Add("process timed out ('" & pInfo.FileName & "')")

                        Return False
                    Else
                        Dim ret As Integer = -1
                        Try
                            ret = p.ExitCode
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function

        Public Shared Function StartProcessAsUserWithExitcode(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer, ByRef ExitCode As Integer) As Integer
            Return _StartProcessWithExitcode(cmdLine, sUserName, sPassword, cmdArguments, waitForExit, timeOut, False, ExitCode)
        End Function

        Private Shared Function _StartProcessWithExitcode(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer, hidden As Boolean, ByRef ExitCode As Integer) As Integer
            Dim pInfo As New ProcessStartInfo()
            Dim iExitCode As Integer = -1

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = True
                pInfo.Verb = "runas"
                If hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOut)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        Errors.Add("process timed out ('" & pInfo.FileName & "')")

                        Return False
                    Else
                        Dim ret As Integer = -1
                        Try
                            iExitCode = p.ExitCode
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function


        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, WorkingDirectory As String, Hidden As Boolean, ByRef proc As Process) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = True
                pInfo.WorkingDirectory = WorkingDirectory
                pInfo.Verb = "runas"
                If Hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            proc = New Process

            Try
                proc = Process.Start(pInfo)

                If proc.Id <= 0 Then
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function

        Public Shared Function IsAppAlreadyRunning() As Boolean
            Dim appProc() As Process
            Dim strModName, strProcName As String

            Try
                strModName = Process.GetCurrentProcess.MainModule.ModuleName
                strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

                appProc = Process.GetProcessesByName(strProcName)

                If appProc.Length > 1 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Function KillPreviousInstances() As Integer
            Dim thisProcess As Process = Process.GetCurrentProcess()
            Dim thisExe As String = IO.Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().CodeBase)
            Dim pList() As System.Diagnostics.Process = System.Diagnostics.Process.GetProcessesByName(thisExe)
            Dim killCount As Integer = 0

            Try
                For Each eachProcess As Process In pList
                    If (Not thisProcess.Equals(eachProcess)) Then
                        killCount += 1
                        eachProcess.Kill()
                    End If
                Next
            Catch ex As Exception
                Errors.Add(ex.Message)
                killCount = -1
            End Try

            Return killCount
        End Function
    End Class
#End Region

#Region "types"
    Public Class Types
        Public Shared Function CastStringToInteger_safe(s As String) As Integer
            Dim iTmp As Integer = -1

            If Integer.TryParse(s, iTmp) Then

            End If

            Return iTmp
        End Function

        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function LeadingZero(num As Integer, Optional len As Integer = 2) As String
            Dim sNum As String = num.ToString

            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Public Shared Function RandomString(Length As Integer) As String
            Return RandomString(Length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        End Function

        Public Shared Function RandomString(Length As Integer, CharsToChooseFrom As String) As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function

        Public Shared Function FromNormalDateToHBDate(Optional WithTime As Boolean = False) As String
            Return FromNormalDateToHBDate(Now, WithTime)
        End Function

        Public Shared Function FromNormalDateToHBDate(d As Date, Optional WithTime As Boolean = False) As String
            Dim sTmp As String = d.ToString("yyyy-MM-dd")

            If WithTime Then
                sTmp &= " " & d.ToString("HH:mm:ss")
            End If

            Return sTmp
        End Function

        Public Shared Function FromHBDateToNormalDate(s As String) As Date
            Dim d As Date

            Try
                d = Date.Parse(s)
            Catch ex As Exception
                d = Nothing
            End Try

            Return d
        End Function

        Public Shared Function IsValidHBDate(s As String) As Boolean
            Dim d As Date

            Try
                d = Date.Parse(s)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function MaskString(s As String, Optional Mask As String = "*", Optional ShowCharsAtStartCount As Integer = 2, Optional ShowCharsAtEndCound As Integer = 0) As String
            If s.Length > (ShowCharsAtStartCount + ShowCharsAtEndCound) Then
                Return String.Concat(s.Substring(0, ShowCharsAtStartCount), "".PadLeft(s.Length - (ShowCharsAtStartCount + ShowCharsAtEndCound), Mask), s.Substring(s.Length - ShowCharsAtEndCound, ShowCharsAtEndCound))
            Else
                Return s
            End If
        End Function

        Public Shared Function String2Boolean(s As String) As Boolean
            s = s.ToLower

            Return (s.Equals("yes") Or s.Equals("true") Or s.Equals("1"))
        End Function
    End Class
#End Region

#Region "time and date"
    Public Class TimeDate
        Public Shared Function UnixTimestamp() As Long
            Return Convert.ToInt64((DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)
        End Function

        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Return FromUnix(UnixTime, True)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long, bDaylightSavingCorrection As Boolean) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime And bDaylightSavingCorrection Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now, False)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Return ToUnix(dTmp, False)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date, bDaylightSavingCorrection As Boolean) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime And bDaylightSavingCorrection Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function
    End Class
#End Region

    Public Class Registry '32-bit 
        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function KeyExists_CU(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.CurrentUser, keyName)
        End Function

        Public Shared Function KeyExists_LM(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.LocalMachine, keyName)
        End Function

        Private Shared Function _KeyExists(keyHive As Microsoft.Win32.RegistryHive, keyName As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.LocalMachine Then
                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyHive = RegistryHive.CurrentUser Then
                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                Errors.Add(ex.Message)

                Return False
            End Try
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
            Return FindKeyByString(keyName, SearchString, "", ErrorMessage)
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, SearchInValueName As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchInValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchInValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchInValueName).ToString
                            End If


                            subkey.Close()

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                bRet = True

                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function FindValueByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Object
            Dim oRet As Object = Nothing
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each valueName As String In key.GetValueNames
                    If valueName.ToLower.Equals(SearchString.ToLower) Then
                        oRet = key.GetValue(valueName)

                        Exit For
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                oRet = Nothing
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return oRet
        End Function

        Public Shared Function FindKeyBySubValueAndReturnValue(keyName As String, SearchString As String, SearchValueName As String, ReturnValueName As String, ByRef ReturnValue As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"
            ReturnValue = "not found"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchValueName).ToString
                            End If

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                If Not subkey.GetValue(ReturnValueName) Is Nothing Then
                                    ReturnValue = subkey.GetValue(ReturnValueName).ToString

                                    bRet = True
                                End If
                            End If

                            subkey.Close()

                            If bRet Then
                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp.ToLower.Equals("yes") Or sTmp.ToLower.Equals("true") Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function SetValue_Boolean(keyName As String, valueName As String, value As Boolean) As Boolean
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "true", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "false", Microsoft.Win32.RegistryValueKind.String)
                End If

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "binary"
        Public Shared Function GetValue_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_Binary(keyName As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

        Public Shared Sub SetValue_Binary(keyName As String, valueName As String, value As Object)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.Binary)

            Catch ex As Exception

            End Try
        End Sub

        Public Shared Function GetValue_ReadOnly_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_ReadOnly_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_ReadOnly_Binary(keyname As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyname, valueName, defaultValue)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

#End Region

#Region "string" ' Microsoft.Win32.RegistryKey.OpenSubKey(String name, Boolean writable)
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                sTmp = defaultValue
            Else
                sTmp = Convert.ToString(oTmp)
            End If

            Return sTmp
        End Function

        Public Shared Function SetValue_String(keyName As String, valueName As String, value As String) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function


        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String, defaultValue As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, defaultValue)
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(hiveName, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyname As String, valueName As String, defaultValue As String) As String
            Try
                Dim oTmp As Object, sTmp As String
                Dim r As RegistryKey

                If hiveName = RegistryHive.LocalMachine Then
                    r = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyname, False)
                ElseIf hiveName = RegistryHive.CurrentUser Then
                    r = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyname, False)
                Else
                    Return defaultValue
                End If

                oTmp = r.GetValue(valueName, defaultValue)
                sTmp = Convert.ToString(oTmp)

                Return sTmp
            Catch ex As Exception
                Errors.Add("GetValue_ReadOnly_String(): " & ex.Message)

                Return "GetValue_ReadOnly_String(): " & ex.Message
            End Try
        End Function
#End Region

#Region "long"
        Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
            Return GetValue_Long(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
            Dim oTmp As Object, iTmp As Long

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                iTmp = defaultValue
            Else
                iTmp = Helpers.Types.CastToLong_safe(oTmp)
            End If


            Return iTmp
        End Function

        Public Shared Function SetValue_Long(keyName As String, valueName As String, value As Long) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.QWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                iTmp = defaultValue
            Else
                iTmp = Helpers.Types.CastToInteger_safe(oTmp)
            End If


            Return iTmp
        End Function

        Public Shared Function SetValue_Integer(keyName As String, valueName As String, value As Integer) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "misc"
        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, True)
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Public Shared Function DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(keyHive, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Private Shared Function _DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Try
                Dim regKey As RegistryKey

                If keyHive = RegistryHive.CurrentUser Then
                    regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyRoot, True)
                ElseIf keyHive = RegistryHive.LocalMachine Then
                    regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyRoot, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                'delete all subkeys
                regKey.DeleteSubKeyTree(keyToDelete)

                If bKeepKeyToDelete Then
                    'put back the root sub key
                    regKey.CreateSubKey(keyToDelete)
                End If

                Return True
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return False
        End Function
#End Region
    End Class

#Region "sitekiosk"
    Public Class SiteKiosk
        Public Shared ReadOnly RootRegistryKey As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk"


        Public Shared Function IsInstalled() As Boolean
            Dim sPath_SiteKiosk As String = GetSiteKioskInstallFolder()
            Dim sFile_SKConfig As String = GetSiteKioskActiveConfig()

            If Not IO.Directory.Exists(sPath_SiteKiosk) Then
                Return False
            End If

            If Not IO.File.Exists(sFile_SKConfig) Then
                Return False
            End If

            Return True
        End Function

        Public Shared Function GetSiteKioskVersion(Optional bShort As Boolean = False) As String
            Dim sTmp As String = Registry.GetValue_String(RootRegistryKey, "Build", "0.0")

            If bShort Then
                Return sTmp.Split(".")(0)
            Else
                Return sTmp
            End If
        End Function

        Public Class License
            Public Shared Function GetLicensee() As String
                Return Registry.GetValue_String(RootRegistryKey, "Licensee", "")
            End Function

            Public Shared Function GetSignature() As String
                Return Registry.GetValue_String(RootRegistryKey, "Signature", "")
            End Function

            Public Shared Sub SetLicensee(NewLicensee As String)
                Registry.SetValue_String(RootRegistryKey, "Licensee", NewLicensee)
            End Sub

            Public Shared Sub SetSignature(NewSignature As String)
                Registry.SetValue_String(RootRegistryKey, "Signature", NewSignature)
            End Sub
        End Class

        Public Class SkCfg
            'Public Shared Function FindViewableFolders(Name As String) As Hashtable
            '    Dim bRet As Boolean = False

            '    Try
            '        Dim mSkCfgFile As String = GetSiteKioskActiveConfig()

            '        Dim nt As System.Xml.XmlNameTable
            '        Dim ns As System.Xml.XmlNamespaceManager
            '        Dim mSkCfgXml As System.Xml.XmlDocument
            '        Dim mXmlNode_Root As System.Xml.XmlNode
            '        Dim sTmpFilename As String = ""
            '        Dim sTmpDescription As String = ""


            '        mSkCfgXml = New System.Xml.XmlDocument

            '        mSkCfgXml.Load(mSkCfgFile)

            '        nt = mSkCfgXml.NameTable
            '        ns = New System.Xml.XmlNamespaceManager(nt)
            '        ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            '        ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            '        mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            '        If Not mXmlNode_Root Is Nothing Then

            '        Else
            '            Throw New Exception("XmlNodeRoot Is Nothing")
            '        End If


            '        Dim mXmlNode_Programs As System.Xml.XmlNode
            '        Dim mXmlNode_Files As System.Xml.XmlNodeList
            '        Dim mXmlNode_File As System.Xml.XmlNode


            '        mXmlNode_Programs = mXmlNode_Root.SelectSingleNode("sk:programs", ns)
            '        mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)
            '        For Each mXmlNode_File In mXmlNode_Files
            '            sTmpFilename = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower
            '            sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("description").Value.ToLower
            '            If sTmpDescription.Equals("") Then
            '                sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("title").Value.ToLower
            '            End If

            '            If sTmpFilename.Contains(Name) Or sTmpDescription.Contains(Name) Then
            '                bRet = True

            '                Exit For
            '            End If
            '        Next
            '    Catch ex As Exception
            '        Errors.Add(ex.Message)
            '    End Try

            '    Return bRet
            'End Function

            Public Shared Function GetDownloadFolder() As String
                Dim sRet As String = ""

                Try
                    Dim mSkCfgFile As String = SiteKiosk.GetSiteKioskActiveConfig()

                    Dim nt As System.Xml.XmlNameTable
                    Dim ns As System.Xml.XmlNamespaceManager
                    Dim mSkCfgXml As System.Xml.XmlDocument
                    Dim mXmlNode_Root As System.Xml.XmlNode
                    Dim sTmpFilename As String = ""
                    Dim sTmpDescription As String = ""


                    mSkCfgXml = New System.Xml.XmlDocument

                    mSkCfgXml.Load(mSkCfgFile)

                    nt = mSkCfgXml.NameTable
                    ns = New System.Xml.XmlNamespaceManager(nt)
                    ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
                    ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

                    mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

                    If Not mXmlNode_Root Is Nothing Then

                    Else
                        Throw New Exception("XmlNodeRoot Is Nothing")
                    End If


                    Dim mXmlNode_DownloadManager As System.Xml.XmlNode
                    Dim mXmlNode_DownloadFolder As System.Xml.XmlNode


                    mXmlNode_DownloadManager = mXmlNode_Root.SelectSingleNode("sk:download-manager", ns)
                    mXmlNode_DownloadFolder = mXmlNode_DownloadManager.SelectSingleNode("sk:download-folder", ns)


                    If mXmlNode_DownloadFolder Is Nothing Then
                        Throw New Exception("mXmlNode_DownloadFolder Is Nothing")
                    End If

                    sRet = mXmlNode_DownloadFolder.InnerText
                Catch ex As Exception
                    Errors.Add(ex.Message)
                End Try

                Return sRet
            End Function


            Public Shared Function FindExternalApplication(Name As String) As Boolean
                Dim bRet As Boolean = False

                Try
                    Dim mSkCfgFile As String = SiteKiosk.GetSiteKioskActiveConfig()

                    Dim nt As System.Xml.XmlNameTable
                    Dim ns As System.Xml.XmlNamespaceManager
                    Dim mSkCfgXml As System.Xml.XmlDocument
                    Dim mXmlNode_Root As System.Xml.XmlNode
                    Dim sTmpFilename As String = ""
                    Dim sTmpDescription As String = ""


                    mSkCfgXml = New System.Xml.XmlDocument

                    mSkCfgXml.Load(mSkCfgFile)

                    nt = mSkCfgXml.NameTable
                    ns = New System.Xml.XmlNamespaceManager(nt)
                    ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
                    ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

                    mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

                    If Not mXmlNode_Root Is Nothing Then

                    Else
                        Throw New Exception("XmlNodeRoot Is Nothing")
                    End If


                    Dim mXmlNode_Programs As System.Xml.XmlNode
                    Dim mXmlNode_Files As System.Xml.XmlNodeList
                    Dim mXmlNode_File As System.Xml.XmlNode


                    mXmlNode_Programs = mXmlNode_Root.SelectSingleNode("sk:programs", ns)
                    mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)
                    For Each mXmlNode_File In mXmlNode_Files
                        sTmpFilename = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower
                        sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("description").Value.ToLower
                        If sTmpDescription.Equals("") Then
                            sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("title").Value.ToLower
                        End If

                        If sTmpFilename.Contains(Name) Or sTmpDescription.Contains(Name) Then
                            bRet = True

                            Exit For
                        End If
                    Next
                Catch ex As Exception
                    Errors.Add(ex.Message)
                End Try

                Return bRet
            End Function
        End Class

        Public Shared Function GetSiteKioskInstallFolder() As String
            Return Registry.GetValue_String(RootRegistryKey, "InstallDir", "")
        End Function

        Public Shared Function GetSiteKioskActiveConfig() As String
            Return Registry.GetValue_String(RootRegistryKey, "LastCfg", "")
        End Function

        Public Shared Function SetSiteKioskActiveConfig(SkCfg As String) As Boolean
            Registry.SetValue_String(RootRegistryKey, "LastCfg", SkCfg)

            Return SkCfg.Equals(GetSiteKioskActiveConfig())
        End Function

        Public Shared Function GetSiteKioskLogfileFolder() As String
            Dim _s As String = Helpers.SiteKiosk.GetSiteKioskInstallFolder

            If _s.Equals("") Then
                Return ""
            Else
                Return IO.Path.Combine(_s, "logfiles")
            End If
        End Function

        Public Shared Function GetMachineNameFromScript() As String
            Dim sRet As String = ""
            Dim sFileBase As String, sFile As String
            sFileBase = GetSiteKioskInstallFolder()
            sFileBase = sFileBase & "\skins\default\scripts"

            sFile = sFileBase & "\guesttek.js"
            If Not IO.File.Exists(sFile) Then
                sFile = sFileBase & "\guest-tek.js"
                If Not IO.File.Exists(sFile) Then
                    sFile = sFileBase & "\ibahn.js"
                    If Not IO.File.Exists(sFile) Then
                        sFile = sFileBase & "\mycall.js"
                        If Not IO.File.Exists(sFile) Then
                            Return "NO_SCRIPT_FOUND"
                        End If
                    End If
                End If
            End If

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var LocationID=") Then
                            line = line.Replace("var LocationID=", "")
                            line = line.Replace("""", "")
                            line = line.Replace(";", "")

                            sRet = line

                            Exit While
                        End If
                    End While
                End Using
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Private Shared ReadOnly _sReplacements() As String = {
            "[SiteKiosk] Notification: [Memory Status]",
            "[SiteKiosk] Notification: [Process Status]",
            ""}

        Public Shared Function CleanSiteKioskLogLine(ByVal input As String) As String
            Dim result As String = input

            For Each item As String In _sReplacements
                If Not item.Equals("") Then
                    If result.Contains(item) Then
                        result = ""
                        Exit For
                    End If
                End If
            Next

            If result <> "" Then
                'remove unwanted chars
                For i As Integer = 0 To 31
                    result = result.Replace(Chr(i), "")
                Next
                For i As Integer = 127 To 255
                    result = result.Replace(Chr(i), "")
                Next
                'Additionally, let's remove </ < and >
                result = result.Replace("</", "[")
                result = result.Replace("<", "[")
                result = result.Replace("/>", "]")
                result = result.Replace(">", "]")
            End If

            Return result
        End Function

        Public Shared Function TranslateSiteKioskPathToNormalPath(sSkPath As String) As String
            Dim sPath As String = sSkPath

            If sPath.StartsWith("%SiteKioskPath%") Then
                sPath = sPath.Replace("%SiteKioskPath%", GetSiteKioskInstallFolder)
                sPath = sPath.Replace("%%PROGRAMFILES%%/SiteKiosk", GetSiteKioskInstallFolder)
                sPath = sPath.Replace("/", "\")
            ElseIf sPath.StartsWith("file://") Then
                sPath = sPath.Replace("file:///", "")
                sPath = sPath.Replace("file://", "")
                sPath = sPath.Replace("/", "\")
            Else

            End If

            Return sPath
        End Function

        Public Shared Function TranslateNormalPathToSiteKioskPath(sPath As String) As String
            Return "file://" & sPath
        End Function

        Public Shared Function Restart() As Integer
            Return Application.Kill
        End Function

        Public Class Application
            Public Shared Function Kill() As Integer
                Return Processes.KillProcesses("sitekiosk")
            End Function
        End Class
    End Class
#End Region

End Class
