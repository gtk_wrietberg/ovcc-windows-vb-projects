﻿Imports Microsoft.Win32
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Security.Principal
Imports System.Security
Imports Microsoft.Win32.SafeHandles
Imports System.Runtime.ConstrainedExecution
Imports System.Security.AccessControl
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.Linq

Public Class Helpers
    Public Class FilesAndFolders
        Public Shared Function GetFileVersion(sFile As String, Optional iParts As Integer = -1) As String
            If Not IO.File.Exists(sFile) Then
                Return ""
            End If

            Dim sVersion As String = ""

            Try
                sVersion = FileVersionInfo.GetVersionInfo(sFile).FileVersion
                sVersion = ParseFileVersion(sVersion, iParts)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

            Return sVersion
        End Function

        Public Shared Function ParseFileVersion(sVersion As String, Optional iParts As Integer = -1) As String
            Try
                Dim sVersionParts As String()

                If sVersion Is Nothing Then
                    sVersion = ""
                End If

                sVersion = sVersion.Split(" ")(0)

                If iParts > 0 Then
                    sVersionParts = sVersion.Split(".")

                    If sVersionParts.Length > iParts Then
                        sVersion = String.Join(".", sVersionParts.Take(iParts))
                    End If
                End If

                Return sVersion
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

            Return ""
        End Function


        Public Shared Function GetFileCompileDate(sFile As String) As String
            Return GetFileCompileDate(sFile, "yyyy-MM-dd HH:mm:ss")
        End Function

        Public Shared Function GetFileCompileDate(sFile As String, DateFormat As String) As String
            Try
                Const PeHeaderOffset As Integer = 60
                Const LinkerTimestampOffset As Integer = 8

                Dim b(2047) As Byte
                'Dim s As IO.Stream
                Dim s As New Object

                Try
                    s = New IO.FileStream(sFile, IO.FileMode.Open, IO.FileAccess.Read)
                    s.Read(b, 0, 2048)
                Finally
                    Try
                        If Not s Is Nothing Then s.Close()
                    Catch ex As Exception

                    End Try
                End Try


                Dim i As Integer = BitConverter.ToInt32(b, PeHeaderOffset)

                Dim SecondsSince1970 As Integer = BitConverter.ToInt32(b, i + LinkerTimestampOffset)
                Dim dt As New DateTime(1970, 1, 1, 0, 0, 0)
                dt = dt.AddSeconds(SecondsSince1970)
                dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours)

                Return dt.ToString(DateFormat)
            Catch ex As Exception
                Return GetFileCreationDate(sFile, DateFormat)
            End Try

        End Function

        Public Shared Function GetFileCreationDate(sFile As String) As String
            Return GetFileCreationDate(sFile, "yyyy-MM-dd")
        End Function

        Public Shared Function GetFileCreationDate(sFile As String, DateFormat As String) As String
            Try
                Dim dDate As Date = IO.File.GetCreationTime(sFile)

                Return dDate.ToString(DateFormat)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String) As String
            Return GetFileLastModifiedDate(sFile, "yyyy-MM-dd")
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String, DateFormat As String) As String
            Try
                Dim dDate As Date = IO.File.GetLastWriteTime(sFile)

                Return dDate.ToString(DateFormat)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDateAsDate(sFile As String) As Date
            Dim dDate As New Date(0)

            Try
                dDate = IO.File.GetLastWriteTime(sFile)
            Catch ex As Exception

            End Try

            Return dDate
        End Function

        Public Shared Function GetFileTypeFromRegistry(FileExtension As String) As String
            Try
                If FileExtension.Contains(" ") Then
                    FileExtension = ""
                    Throw New Exception("space in extension")
                End If

                Dim reg1 As Object = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & FileExtension, "", FileExtension)

                If reg1 Is Nothing Then
                    Throw New Exception("Unknown file type")
                End If

                If reg1.ToString.Equals(FileExtension) Then
                    Throw New Exception("Unknown file type")
                End If


                Dim reg2 As Object = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & reg1, "", FileExtension)

                If reg2 Is Nothing Then
                    Throw New Exception("Unknown file type")
                End If

                If reg2.ToString.Equals(FileExtension) Then
                    Throw New Exception("Unknown file type")
                End If

                Return reg2
            Catch ex As Exception
                If FileExtension.StartsWith(".") Then
                    FileExtension = FileExtension.Substring(1, FileExtension.Length - 1)
                End If

                If FileExtension.Equals("") Then
                    Return "File"
                Else
                    Return FileExtension & " File"
                End If
            End Try

            'Return My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" &
            '                                     My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & FileExtension, "", FileExtension).ToString, "", FileExtension).ToString
        End Function

        Public Shared Function GetFileDescription(FilePath As String) As String
            Dim _s As String = ""

            If Not IO.File.Exists(FilePath) Then
                Return "(" & FilePath & ")"
            End If

            Try
                Dim fi As FileVersionInfo = FileVersionInfo.GetVersionInfo(FilePath)

                _s = fi.FileDescription
            Catch ex As Exception
                _s = "(" & FilePath & ")"
            End Try

            Return _s
        End Function

        Public Class FileSize
            Public Enum HumanReadable
                Use1024 = 0
                Use1024i = 1
                Use1000 = 2
            End Enum

            Public Shared Function GetHumanReadable(FileSize As Long) As String
                Return GetHumanReadable(FileSize, HumanReadable.Use1024)
            End Function

            Public Shared Function GetHumanReadable(FileSize As Long, Base1024 As HumanReadable) As String
                If FileSize < 0 Then
                    Return ""
                End If

                Select Case Base1024
                    Case HumanReadable.Use1000
                        Select Case FileSize
                            Case > (1000 * 1000 * 1000)
                                Return CInt(FileSize / (1000 * 1000 * 1000)).ToString & "GB"
                            Case > (1000 * 1000)
                                Return CInt(FileSize / (1000 * 1000)).ToString & "MB"
                            Case > 1000
                                Return CInt(FileSize / 1000).ToString & "kB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case HumanReadable.Use1024
                        Select Case FileSize
                            Case > (1024 * 1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024 * 1024)).ToString & "GB"
                            Case > (1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024)).ToString & "MB"
                            Case > 1024
                                Return CInt(FileSize / 1024).ToString & "KB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case HumanReadable.Use1024i
                        Select Case FileSize
                            Case > (1024 * 1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024 * 1024)).ToString & "GiB"
                            Case > (1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024)).ToString & "MiB"
                            Case > 1024
                                Return CInt(FileSize / 1024).ToString & "KiB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case Else
                        Return "?B"
                End Select
            End Function
        End Class

        Public Shared Function GetProgramFilesFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sPath.Equals(String.Empty) Then
                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetProgramFilesFolder64bit(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetGuestTekFolder() As String
            Return IO.Path.Combine(GetProgramFilesFolder, "GuestTek")
        End Function

        Public Shared Function GetDefaultBackupFolder() As String
            Dim sTmp As String = IO.Path.Combine(GetProgramFilesFolder, "GuestTek\_backups")

            sTmp &= "\%%Application.ProductName%%\%%Application.ProductVersion%%\%%Backup.Date%%"
            sTmp = sTmp.Replace("%%Application.ProductName%%", My.Application.Info.ProductName)
            sTmp = sTmp.Replace("%%Application.ProductVersion%%", My.Application.Info.Version.ToString)
            sTmp = sTmp.Replace("%%Backup.Date%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))

            Return sTmp
        End Function

        Public Shared Function GetWindowsFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.Windows)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetAppDataLocalFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function


        Public Shared Function GetCurrentFolder() As String
            Dim P As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)

            Return New Uri(P).LocalPath
        End Function

        Public Shared Function GetExecutablePath() As String
            Dim sPath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)

            sPath = New Uri(sPath).LocalPath

            Return sPath
        End Function

        Public Shared Function GetNewestFile(Path As String) As String
            Return GetNewestFile(Path, "*.*", False)
        End Function

        Public Shared Function GetNewestFile(Path As String, SearchPattern As String) As String
            Return GetNewestFile(Path, SearchPattern, False)
        End Function

        Public Shared Function GetNewestFile(Path As String, SearchPattern As String, SearchPatternIsRegularExpression As Boolean) As String
            If Not IO.Directory.Exists(Path) Then
                Return ""
            End If


            If SearchPatternIsRegularExpression Then
                Try
                    Dim _regex = New Regex(SearchPattern)


                    Return Directory.GetFiles(Path).Where(Function(f) _regex.IsMatch(IO.Path.GetFileName(f))).OrderByDescending(Function(f) New FileInfo(f).LastWriteTime).FirstOrDefault()
                Catch ex As Exception
                    'invalid searchpattern

                    Return ex.Message
                End Try
            Else
                Return Directory.GetFiles(Path, SearchPattern).OrderByDescending(Function(f) New FileInfo(f).LastWriteTime).FirstOrDefault()
            End If
        End Function

        Public Shared Function GetNewestFolder(Path As String) As String
            If IO.Directory.Exists(Path) Then
                Return Directory.GetDirectories(Path).OrderByDescending(Function(d) New DirectoryInfo(d).LastWriteTime).FirstOrDefault()
            Else
                Return ""
            End If
        End Function

        Public Class Folder
            Public Shared Function IsRootFolder(_dir As String) As Boolean
                Dim _di As New DirectoryInfo(_dir)

                If _di.Parent Is Nothing Then
                    Return True
                Else
                    Return False
                End If
            End Function

            Public Shared Function FindRecursive(rootdir As String) As List(Of String)
                ' This list stores the results.
                Dim result As New List(Of String)

                ' This stack stores the directories to process.
                Dim stack As New Stack(Of String)

                ' Add the initial directory
                stack.Push(rootdir)

                ' Continue processing for each stacked directory
                Do While (stack.Count > 0)
                    ' Get top directory string
                    Dim dir As String = stack.Pop
                    Try
                        ' Loop through all subdirectories and add them to the stack.
                        Dim directoryName As String
                        For Each directoryName In Directory.GetDirectories(dir)
                            result.Add(directoryName)
                            stack.Push(directoryName)
                        Next

                    Catch ex As Exception
                    End Try
                Loop

                ' Return the list
                Return result
            End Function

            Public Shared Function CleanFolder(folder As String, security_string As String) As Long
                If Not security_string.Equals("I_am_sure") Then
                    Return -1
                End If

                'DANGER DANGER
                Dim iErrors As Long = 0

                For Each d As String In IO.Directory.GetDirectories(folder)
                    Try
                        IO.Directory.Delete(d, True)
                    Catch ex As Exception
                        iErrors += 1
                    End Try
                Next

                For Each f As String In Directory.GetFiles(folder)
                    Try
                        IO.File.Delete(f)
                    Catch ex As Exception
                        iErrors += 1
                    End Try
                Next

                Return iErrors
            End Function

            Public Shared Function DirectoryFileCount(ByVal sPath As String, Optional ByVal bRecursive As Boolean = False) As Long
                Dim Count As Long = 0
                Dim diDir As New IO.DirectoryInfo(sPath)

                Try
                    Dim fil As FileInfo

                    For Each fil In diDir.GetFiles()
                        Count += 1
                    Next fil

                    If bRecursive = True Then
                        Dim diSubDir As DirectoryInfo

                        For Each diSubDir In diDir.GetDirectories()
                            Count += DirectoryFileCount(diSubDir.FullName, True)
                        Next diSubDir
                    End If

                    Return Count
                Catch ex As System.IO.FileNotFoundException
                    Return 0
                Catch exx As Exception
                    Return 0
                End Try

                Return Count
            End Function

            Public Shared Function DirectorySize(ByVal sPath As String, Optional ByVal bRecursive As Boolean = False) As Long
                Dim Size As Long = 0
                Dim diDir As New IO.DirectoryInfo(sPath)

                Try
                    Dim fil As FileInfo

                    For Each fil In diDir.GetFiles()
                        Size += fil.Length
                    Next fil

                    If bRecursive = True Then
                        Dim diSubDir As DirectoryInfo

                        For Each diSubDir In diDir.GetDirectories()
                            Size += DirectorySize(diSubDir.FullName, True)
                        Next diSubDir
                    End If

                    Return Size
                Catch ex As System.IO.FileNotFoundException
                    Return 0
                Catch exx As Exception
                    Return 0
                End Try

                Return Size
            End Function
        End Class

        Public Class File
            Public Shared Function Find(rootdir As String, searchPattern As String) As List(Of IO.FileInfo)
                Try
                    Dim lList As New List(Of IO.FileInfo)

                    If Not IO.Directory.Exists(rootdir) Then
                        Throw New Exception("'" & rootdir & "' not found")
                    End If

                    Dim files() As String = Directory.GetFiles(rootdir, searchPattern)

                    For Each file As String In files
                        lList.Add(New IO.FileInfo(file))
                    Next

                    Return lList
                Catch ex As Exception
                    Errors.Add(ex.Message)
                    Return Nothing
                End Try
            End Function

            Public Shared Function FindRecursive(rootdir As String, searchPattern As String) As List(Of String)
                ' This list stores the results.
                Dim result As New List(Of String)

                ' This stack stores the directories to process.
                Dim stack As New Stack(Of String)

                ' Add the initial directory
                stack.Push(rootdir)

                ' Continue processing for each stacked directory
                Do While (stack.Count > 0)
                    ' Get top directory string
                    Dim dir As String = stack.Pop
                    Try
                        ' Add all immediate file paths
                        result.AddRange(Directory.GetFiles(dir, searchPattern))

                        ' Loop through all subdirectories and add them to the stack.
                        Dim directoryName As String
                        For Each directoryName In Directory.GetDirectories(dir)
                            stack.Push(directoryName)
                        Next

                    Catch ex As Exception
                    End Try
                Loop

                ' Return the list
                Return result
            End Function
        End Class

        Public Class Permissions
            Public Shared Function GetSecurityIdentifier(UserName As String) As SecurityIdentifier
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return secid
            End Function

            Public Shared Function FileSecurity_ADD__ReadExecute(FileName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFileSecurity(FileName, secid, FileSystemRights.ReadAndExecute, AccessControlType.Allow, False, False)
            End Function

            Public Shared Function FileSecurity_REPLACE__Full_Everyone(FileName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFileSecurity(FileName, secid, FileSystemRights.FullControl, AccessControlType.Allow, True, False)
            End Function

            Public Shared Function FileSecurity_REPLACEandPURGE__Full_Everyone(FileName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFileSecurity(FileName, secid, FileSystemRights.FullControl, AccessControlType.Allow, True, True)
            End Function

            Public Shared Function FileSecurity_ADD(ByVal FileName As String, ByVal UserName As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFileSecurity(FileName, secid, rights, controlType, DisableInheritance, False)
            End Function


            Public Shared Function FolderSecurity_ADD__ReadExecute(FolderName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.ReadAndExecute, AccessControlType.Allow, False, False, False)
            End Function

            Public Shared Function FolderSecurity_REPLACE__Full(FolderName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, False, True, False)
            End Function

            Public Shared Function FolderSecurity_REPLACEandPURGE__Full(FolderName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, False, True, True)
            End Function

            Public Shared Function FolderSecurity_REPLACE__Full_Everyone(FolderName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, False, True, False)
            End Function

            Public Shared Function FolderSecurity_REPLACEandPURGE__Full_Everyone(FolderName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, False, True, True)
            End Function

            Public Shared Function FolderSecurity_ADD(ByVal FolderName As String, ByVal UserName As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFolderSecurity(FolderName, secid, rights, controlType, False, DisableInheritance, False)
            End Function

            Public Shared Function FolderSecurity_ADD__Everyone(ByVal FolderName As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, rights, controlType, False, DisableInheritance, False)
            End Function

            Public Shared Function FolderSecurity_ADDandINHERIT__Everyone(ByVal FolderName As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, rights, controlType, True, DisableInheritance, False)
            End Function



            Private Shared Function _AddFileSecurity(ByVal fileName As String, ByVal SecurityIdentifier As IdentityReference, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean, PurgeAllOtherRules As Boolean) As Boolean
                Try
                    Dim fSecurity As FileSecurity = IO.File.GetAccessControl(fileName)
                    Dim accessRule As FileSystemAccessRule = New FileSystemAccessRule(SecurityIdentifier, rights, controlType)

                    If PurgeAllOtherRules Then
                        DisableInheritance = True

                        Dim rules As AuthorizationRuleCollection = fSecurity.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
                        For Each rule As FileSystemAccessRule In rules
                            fSecurity.RemoveAccessRule(rule)
                        Next
                    End If

                    If DisableInheritance Then
                        fSecurity.SetAccessRuleProtection(True, False)
                    End If


                    fSecurity.AddAccessRule(accessRule)

                    IO.File.SetAccessControl(fileName, fSecurity)

                    Return True
                Catch ex As Exception
                    Errors.Add(ex.Message)

                    Return False
                End Try
            End Function

            Private Shared Function _AddFolderSecurity(ByVal folderName As String, ByVal SecurityIdentifier As IdentityReference, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, SetInheritance As Boolean, DisableInheritance As Boolean, PurgeAllOtherRules As Boolean) As Boolean
                Try
                    Dim fSecurity As DirectorySecurity = IO.Directory.GetAccessControl(folderName)
                    'Dim accessRule As FileSystemAccessRule = New FileSystemAccessRule(SecurityIdentifier, rights, controlType)
                    Dim accessRule As FileSystemAccessRule


                    If SetInheritance Then
                        PurgeAllOtherRules = False

                        accessRule = New FileSystemAccessRule(SecurityIdentifier, rights, InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, controlType)
                    Else
                        accessRule = New FileSystemAccessRule(SecurityIdentifier, rights, controlType)
                    End If

                    If PurgeAllOtherRules Then
                        DisableInheritance = True

                        Dim rules As AuthorizationRuleCollection = fSecurity.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
                        For Each rule As FileSystemAccessRule In rules
                            fSecurity.RemoveAccessRule(rule)
                        Next
                    End If

                    If DisableInheritance Then
                        fSecurity.SetAccessRuleProtection(True, False)
                    End If

                    fSecurity.AddAccessRule(accessRule)

                    IO.Directory.SetAccessControl(folderName, fSecurity)

                    Return True
                Catch ex As Exception
                    Errors.Add(ex.Message)

                    Return False
                End Try
            End Function
        End Class

        Public Class CopyFiles
            Private pBackupFolder As String
            Private pSourceDir As String
            Private pDestDir As String
            Private pSuppressCopyLogging As Boolean = False
            Private pSkipBackup As Boolean = False
            Private pLogDepth As Integer = 0

            Public Property LogDepth() As Integer
                Get
                    Return pLogDepth
                End Get
                Set(value As Integer)
                    pLogDepth = value
                End Set
            End Property

            Public Property BackupDirectory() As String
                Get
                    Return pBackupFolder
                End Get
                Set(ByVal value As String)
                    pBackupFolder = value
                End Set
            End Property

            Public Property SourceDirectory() As String
                Get
                    Return pSourceDir
                End Get
                Set(ByVal value As String)
                    pSourceDir = value
                End Set
            End Property

            Public Property DestinationDirectory() As String
                Get
                    Return pDestDir
                End Get
                Set(ByVal value As String)
                    pDestDir = value
                End Set
            End Property

            Public Property SuppressCopyLogging() As Boolean
                Get
                    Return pSuppressCopyLogging
                End Get
                Set(value As Boolean)
                    pSuppressCopyLogging = value
                End Set
            End Property

            Public Property SkipBackup() As Boolean
                Get
                    Return pSkipBackup
                End Get
                Set(value As Boolean)
                    pSkipBackup = value
                End Set
            End Property

            Private pFileCount_total As Long
            Public Property FileCount_total() As Long
                Get
                    Return pFileCount_total
                End Get
                Set(ByVal value As Long)
                    pFileCount_total = value
                End Set
            End Property

            Private pFileCount_copied As Long
            Public Property FileCount_copied() As Long
                Get
                    Return pFileCount_copied
                End Get
                Set(ByVal value As Long)
                    pFileCount_copied = value
                End Set
            End Property

            Private pFileCount_failed As Long
            Public Property FileCount_failed() As Long
                Get
                    Return pFileCount_failed
                End Get
                Set(ByVal value As Long)
                    pFileCount_failed = value
                End Set
            End Property

            Public Function CopyFiles() As Boolean
                If pSuppressCopyLogging Then
                    Logger.WriteMessageRelative("starting file copy", 1 + pLogDepth)
                    Logger.WriteMessageRelative("source: " & pSourceDir, 2 + pLogDepth)
                    Logger.WriteMessageRelative("dest  : " & pDestDir, 2 + pLogDepth)

                    Logger.WriteMessageRelative("copy logging disabled", 2 + pLogDepth)
                End If

                If Not IO.Directory.Exists(pSourceDir) Then
                    Logger.WriteMessageRelative("source not found!", 3 + pLogDepth)

                    Return False
                End If

                pFileCount_copied = 0
                pFileCount_failed = 0
                pFileCount_total = 0

                RecursiveDirectoryCopy(pSourceDir, pDestDir)

                Return True
            End Function

            Public Function CopyFiles(ByVal SourceDir As String, ByVal DestinationDir As String) As Boolean
                pSourceDir = SourceDir
                pDestDir = DestinationDir

                Return CopyFiles()
            End Function

            Private Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String)
                Dim sDir As String
                Dim dDirInfo As IO.DirectoryInfo
                Dim sDirInfo As IO.DirectoryInfo
                Dim sFile As String
                Dim sFileInfo As IO.FileInfo
                Dim dFileInfo As IO.FileInfo

                ' Add trailing separators to the supplied paths if they don't exist.
                If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                    sourceDir &= System.IO.Path.DirectorySeparatorChar
                End If
                If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                    destDir &= System.IO.Path.DirectorySeparatorChar
                End If

                'If destination directory does not exist, create it.
                dDirInfo = New System.IO.DirectoryInfo(destDir)
                If dDirInfo.Exists = False Then dDirInfo.Create()
                dDirInfo = Nothing


                ' Get a list of directories from the current parent.
                For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
                    sDirInfo = New System.IO.DirectoryInfo(sDir)
                    dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
                    ' Create the directory if it does not exist.
                    If dDirInfo.Exists = False Then dDirInfo.Create()
                    ' Since we are in recursive mode, copy the children also
                    RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName)
                    sDirInfo = Nothing
                    dDirInfo = Nothing
                Next

                ' Get the files from the current parent.
                For Each sFile In System.IO.Directory.GetFiles(sourceDir)
                    sFileInfo = New System.IO.FileInfo(sFile)
                    dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

                    If Not pSuppressCopyLogging Then
                        Logger.WriteMessageRelative("copying", 1 + pLogDepth)
                        Logger.WriteMessageRelative("source: " & sFileInfo.FullName, 2 + pLogDepth)
                        Logger.WriteMessageRelative("dest  : " & dFileInfo.FullName, 2 + pLogDepth)
                    End If


                    'If File does exists, backup.
                    If dFileInfo.Exists Then
                        If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("already exists", 3 + pLogDepth)

                        If pSkipBackup Then
                            If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("backup skipped", 4 + pLogDepth)
                        Else
                            If dFileInfo.IsReadOnly Then
                                If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("file is read-only, fixing...", 4 + pLogDepth)
                                dFileInfo.IsReadOnly = False
                            End If
                            If Not System.IO.Directory.Exists(pBackupFolder) Then
                                If Not pSuppressCopyLogging Then
                                    Logger.WriteMessageRelative("creating backup failed, backup folder not valid", 3 + pLogDepth)
                                End If
                            Else
                                If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("creating backup", 3 + pLogDepth)
                                If BackupFile(dFileInfo) Then

                                Else

                                End If
                            End If
                        End If
                    End If

                    Try
                        pFileCount_total += 1

                        sFileInfo.CopyTo(dFileInfo.FullName, True)
                        If IO.File.Exists(dFileInfo.FullName) Then
                            If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("ok", 2 + pLogDepth)

                            pFileCount_copied += 1
                        Else
                            If Not pSuppressCopyLogging Then Logger.WriteErrorRelative("fail", 2 + pLogDepth)

                            pFileCount_failed += 1
                        End If
                    Catch ex As Exception
                        If pSuppressCopyLogging Then
                            Logger.WriteMessageRelative("copying", 1)
                            Logger.WriteMessageRelative("source: " & sFileInfo.FullName, 2 + pLogDepth)
                            Logger.WriteMessageRelative("dest  : " & dFileInfo.FullName, 2 + pLogDepth)
                        End If

                        Logger.WriteErrorRelative("fail", 3)
                        Logger.WriteErrorRelative("ex.Message=" & ex.Message, 4 + pLogDepth)

                        pFileCount_failed += 1
                    End Try

                    sFileInfo = Nothing
                    dFileInfo = Nothing
                Next
            End Sub

            Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
                Dim sPath As String, sName As String, sFullDest As String
                Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
                Dim bRet As Boolean = False

                Try
                    sName = oFile.Name

                    sPath = oFile.DirectoryName
                    sPath = sPath.Replace("C:", "")
                    sPath = sPath.Replace("c:", "")
                    sPath = sPath.Replace("\\", "\")
                    sPath = pBackupFolder & "\" & sPath

                    sFullDest = sPath & "\" & sName

                    oDirInfo = New System.IO.DirectoryInfo(sPath)
                    If Not oDirInfo.Exists Then oDirInfo.Create()

                    Logger.WriteMessageRelative("backup: " & sFullDest, 4 + pLogDepth)

                    oFile.CopyTo(sFullDest, True)

                    oFileInfo = New System.IO.FileInfo(sFullDest)
                    If oFileInfo.Exists Then
                        Logger.WriteMessageRelative("ok", 5 + pLogDepth)
                        bRet = True
                    Else
                        Logger.WriteErrorRelative("fail", 5 + pLogDepth)
                    End If
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 5 + pLogDepth)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 6 + pLogDepth)
                End Try

                oDirInfo = Nothing
                oFileInfo = Nothing

                Return bRet
            End Function
        End Class

        Public Class Backup
            Public Shared Function File(filePath As String, logLevel As Integer, prefix As String) As Boolean
                Return File(filePath, "", logLevel, prefix)
            End Function

            Public Shared Function File(filePath As String, logLevel As Integer) As Boolean
                Return File(filePath, "", logLevel, "")
            End Function

            Public Shared Function File(filePath As String, backupFolder As String) As Boolean
                Return File(filePath, backupFolder, 0, "")
            End Function

            Public Shared Function File(filePath As String, backupFolder As String, logLevel As Integer) As Boolean
                Return File(filePath, backupFolder, logLevel, "")
            End Function

            Public Shared Function File(filePath As String, backupFolder As String, logLevel As Integer, prefix As String) As Boolean
                Logger.WriteMessageRelative("writing backup", 1 + logLevel)
                Logger.WriteMessageRelative(filePath, 2 + logLevel)

                If Not backupFolder.Equals("") Then
                    If Not IO.Directory.Exists(backupFolder) Then
                        Logger.WriteErrorRelative("backup root folder does not exist", 3 + logLevel)

                        Return False
                    End If
                End If


                If Not IO.File.Exists(filePath) Then
                    Logger.WriteErrorRelative("does not exist", 3 + logLevel)

                    Return False
                End If


                Dim fI As IO.FileInfo = New System.IO.FileInfo(filePath)

                Return _BackupFile(fI, backupFolder, logLevel, prefix)
            End Function

            Private Shared Function _BackupFile(ByVal oFile As IO.FileInfo, backupFolder As String, logLevel As Integer, prefix As String) As Boolean
                Dim sPath As String, sName As String, sFullDest As String
                Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
                Dim bRet As Boolean = False

                Try
                    If backupFolder.Equals("") Then
                        If prefix.Equals("") Then
                            sName = IO.Path.GetFileNameWithoutExtension(oFile.Name) &
                            "_" & Date.Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") &
                            IO.Path.GetExtension(oFile.Name)
                        Else
                            sName = IO.Path.GetFileNameWithoutExtension(oFile.Name) &
                            "__" & prefix & "_" & Date.Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") &
                            IO.Path.GetExtension(oFile.Name)
                        End If

                        sPath = oFile.DirectoryName
                    Else
                        sName = oFile.Name

                        sPath = oFile.DirectoryName
                        sPath = sPath.Replace("C:", "")
                        sPath = sPath.Replace("c:", "")
                        sPath = sPath.Replace("\\", "\")
                        sPath = backupFolder & "\" & sPath
                    End If


                    sFullDest = IO.Path.Combine(sPath, sName)

                    Logger.WriteMessageRelative("backup: " & sFullDest, 3 + logLevel)


                    oDirInfo = New System.IO.DirectoryInfo(sPath)
                    If Not oDirInfo.Exists Then oDirInfo.Create()



                    oFile.CopyTo(sFullDest, True)

                    oFileInfo = New System.IO.FileInfo(sFullDest)
                    If oFileInfo.Exists Then
                        Logger.WriteMessageRelative("ok", 4 + logLevel)
                        bRet = True
                    Else
                        Logger.WriteErrorRelative("fail", 4 + logLevel)
                    End If
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 4 + logLevel)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 5 + logLevel)
                End Try

                oDirInfo = Nothing
                oFileInfo = Nothing

                Return bRet
            End Function
        End Class
	End Class
End Class
