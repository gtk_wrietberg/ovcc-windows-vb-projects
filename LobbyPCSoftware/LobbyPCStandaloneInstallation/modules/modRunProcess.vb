Module modRunProcess
    Public Function ShellandWait(ByVal ProcessPath As String, Optional ByVal WaitTimeOut As Integer = -1, Optional ByVal ProcessStyle As ProcessWindowStyle = ProcessWindowStyle.Normal) As Boolean
        Dim objProcess As System.Diagnostics.Process

        Try
            objProcess = New System.Diagnostics.Process()
            objProcess.StartInfo.FileName = ProcessPath
            objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            objProcess.Start()

            'Wait until the process passes back an exit code 
            objProcess.WaitForExit(WaitTimeOut)

            'Free resources associated with this process
            objProcess.Close()
        Catch
            ShellandWait = False
            MessageBox.Show("Could not start process " & ProcessPath, "Error")
        End Try
    End Function
End Module
