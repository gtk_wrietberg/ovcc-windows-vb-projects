Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sSourceFolder As String
        Dim sDestinationFolder As String

        sSourceFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder
        'sDestinationFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder
        sDestinationFolder = sSourceFolder

        oLogger.WriteToLog("Altiris package", , 1)
        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.exe"

        oLogger.WriteToLog("CreateShortcut package", , 1)
        oSettings.Path_CreateShortcut = sDestinationFolder & "\CreateShortcut.package.exe"

        oLogger.WriteToLog("pcAnywhere package", , 1)
        oSettings.Path_pcAnywhere = sDestinationFolder & "\pcAnywhere.exe"

        oLogger.WriteToLog("iBAHN Updater package (Win7)", , 1)
        oSettings.Path_iBAHNUpdate = sDestinationFolder & "\iBAHNUpdate.package.Win7.exe"

        oLogger.WriteToLog("LobbyPCAgent package", , 1)
        oSettings.Path_LobbyPCAgent = sDestinationFolder & "\LobbyPCAgentPatchInstaller.package.exe"

        oLogger.WriteToLog("LobbyPCWatchdog package", , 1)
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\LobbyPCWatchdog.package.exe"

        oLogger.WriteToLog("SiteKiosk7", , 1)
        oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk7.exe"

        oLogger.WriteToLog("LobbyPCStandalonePostSiteKiosk", , 1)
        oSettings.Path_PostSiteKioskUpdating = sDestinationFolder & "\LobbyPCStandalonePostSiteKiosk.exe"

        oLogger.WriteToLog("PcHasRebooted", , 1)
        oSettings.Path_PcHasRebooted = sDestinationFolder & "\PcHasRebooted.exe"


        sDestinationFolder = cPATHS_iBAHNProgramFilesFolder

        oLogger.WriteToLog("LobbyPCAutoStart", , 1)
        _CopyFile("LobbyPCAutoStart.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("LobbyPCAutoStart.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_AutoStart = sDestinationFolder & "\LobbyPCAutoStart.exe"
    End Sub

    Private Sub _CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, ByVal iLogLevel As Integer)
        oLogger.WriteToLog("copying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)

        oLogger.WriteToLog("retrying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
    End Sub
End Module
