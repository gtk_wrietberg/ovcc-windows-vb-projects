﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("LobbyPCSoftwareRemoval")> 
<Assembly: AssemblyDescription("vJY ; XP/7 ; 32/64")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("LobbyPCSoftwareRemoval")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("245f5872-c184-426c-b933-fbfd48d8440b")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.1.7.1245")> 
<Assembly: AssemblyFileVersion("3.1.7.1245")> 
