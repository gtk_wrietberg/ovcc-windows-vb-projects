Imports System.Runtime.InteropServices
Imports System.ComponentModel

Public Class Form1

    Private Structure TOKEN_PRIVILEGES
        Public PrivilegeCount As UInteger
        Public Privileges As LUID_AND_ATTRIBUTES
        Public Function Size() As Integer
            Return Marshal.SizeOf(Me)
        End Function
    End Structure

    Private Structure LUID
        Private lowPart As UInt32
        Private highPart As Int32
    End Structure

    Private Structure LUID_AND_ATTRIBUTES
        Public luid As LUID
        Public attributes As UInt32
    End Structure

    Private Const TOKEN_ADJUST_PRIVLEGES As UInteger = &H20UI
    Private Const TOKEN_QUERY As UInteger = &H8UI
    Private Const SE_PRIVILEGE_ENABLED As UInteger = &H2UI
    Private Const HKEY_USERS As UInteger = &H80000003UI
    Private Const SE_RESTORE_NAME As String = "SeRestorePrivilege"
    Private Const SE_BACKUP_NAME As String = "SeBackupPrivilege"

    ' System.Diagnostics.Process.Handle
    <DllImport("kernel32")> _
    Private Shared Function GetCurrentProcess() As IntPtr
    End Function

    <DllImport("advapi32", SetLastError:=True)> _
    Private Shared Function OpenProcessToken( _
    ByVal ProcessHandle As IntPtr, _
    ByVal DesiredAccess As UInteger, _
    ByRef TokenHandle As IntPtr) As Integer
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function LookupPrivilegeValue( _
    ByVal lpSystemName As String, _
    ByVal lpName As String, _
    ByRef lpLuid As LUID) As Integer
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function AdjustTokenPrivileges( _
    ByVal TokenHandle As IntPtr, _
    ByVal DisableAllPrivileges As Integer, _
    ByRef NewState As TOKEN_PRIVILEGES, _
    ByVal BufferLength As Integer, _
    ByRef PreviousState As TOKEN_PRIVILEGES, _
    ByRef ReturnLength As Integer) As Integer
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function RegLoadKey( _
    ByVal hKey As UInteger, _
    ByVal lpSubKey As String, _
    ByVal lpFile As String) As Integer
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function RegUnLoadKey( _
    ByVal hKey As UInteger, _
    ByVal lpSubKey As String) As Integer
    End Function

    Private strKeyName As String
    Private MyToken As IntPtr
    Private TP1 As TOKEN_PRIVILEGES
    Private TP2 As TOKEN_PRIVILEGES
    Private hBlob As IntPtr

    'Sub New()
    ' InitializeComponent()
    ' Me.Controls.AddRange(New Control() {Text1, Command1, Command2})
    ' Command1.Location = New Point(0, Text1.Bottom + 10)
    ' Command2.Location = New Point(0, Command1.Bottom + 10)
    'End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        strKeyName = "keyLoaded"
        Text1.Text = "< Enter Path to Hive Here >"
        Command2.Enabled = False

        Dim hProcess As IntPtr = GetCurrentProcess() ' is -1 as it should.

        Dim retVal As Integer = OpenProcessToken( _
        hProcess, _
        TOKEN_ADJUST_PRIVLEGES Or TOKEN_QUERY, MyToken)

        If retVal = 0 Then Throw New Win32Exception
        Dim RestoreLuid As LUID

        retVal = LookupPrivilegeValue(Nothing, SE_RESTORE_NAME, RestoreLuid)

        If retVal = 0 Then Throw New Win32Exception
        Dim BackupLuid As LUID
        retVal = LookupPrivilegeValue(Nothing, SE_BACKUP_NAME, BackupLuid)
        If retVal = 0 Then Throw New Win32Exception
        TP1.PrivilegeCount = 1
        TP1.Privileges.attributes = SE_PRIVILEGE_ENABLED
        TP1.Privileges.luid = RestoreLuid
        Dim returnLength As Integer = 0
        Dim oldPrivileges As TOKEN_PRIVILEGES
        retVal = AdjustTokenPrivileges(MyToken, 0, TP1, TP1.Size, oldPrivileges, returnLength)
        If retVal = 0 Then
            Throw New Win32Exception
        End If
        TP2.PrivilegeCount = 1
        TP2.Privileges.attributes = SE_PRIVILEGE_ENABLED
        TP2.Privileges.luid = BackupLuid
        retVal = AdjustTokenPrivileges(MyToken, 0, TP2, TP2.Size, oldPrivileges, returnLength)
        If retVal = 0 Then
            Throw New Win32Exception
        End If

    End Sub

    Private Sub Command1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Command1.Click
        Dim retval As Integer = RegLoadKey(HKEY_USERS, strKeyName, Text1.Text)
        If retval <> 0 Then
            MessageBox.Show(retval & ", " & Hex(retval))
            Throw New Win32Exception(retval)
        End If
        Command2.Enabled = True
    End Sub

    Private Sub Command2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Command2.Click
        Dim retval As Integer = RegUnLoadKey(HKEY_USERS, strKeyName)
        If retval <> 0 Then
            Throw New Win32Exception
        End If
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim returnLength As Integer = 0
        Dim retval As Integer = AdjustTokenPrivileges(MyToken, 0, TP1, TP1.Size, Nothing, returnLength)
        If retval = 0 Then Throw New Win32Exception
        retval = AdjustTokenPrivileges(MyToken, 0, TP2, TP2.Size, Nothing, returnLength)
        If retval = 0 Then Throw New Win32Exception
    End Sub

End Class