Friend Module OtherMethods

    Private frmA As frmAbout
    Private frmO As frmOptions
    Private frmP As frmPassword

    Public Sub ExitApplication()
        'Perform any clean-up here
        'Then exit the application
        Application.Exit()
    End Sub

    Public Sub ShowDialog_About()
        If frmA IsNot Nothing AndAlso Not frmA.IsDisposed Then Exit Sub

        frmA = New frmAbout
        frmA.Icon = My.Resources.volume_ok
        frmA.Text = "About - " & Application.ProductName
        frmA.ShowDialog()
        frmA = Nothing
    End Sub

    Public Sub ShowDialog_Options()
        If frmO IsNot Nothing AndAlso Not frmO.IsDisposed Then Exit Sub

        frmO = New frmOptions
        frmO.Icon = My.Resources.volume_ok
        frmO.Text = "Options -" & Application.ProductName
        frmO.ShowDialog()
        frmO = Nothing
    End Sub

    Public Sub ShowDialog_Password()
        If frmP IsNot Nothing AndAlso Not frmP.IsDisposed Then Exit Sub

        Dim bPasswordOk As Boolean = False

        frmP = New frmPassword
        frmP.Icon = My.Resources.volume_ok
        frmP.Text = "Password please - " & Application.ProductName
        frmP.ShowDialog()
        bPasswordOk = (frmP.DialogResult = DialogResult.Yes)
        frmP = Nothing

        If bPasswordOk Then
            ShowDialog_Options()
        End If
    End Sub
End Module