Public Class frmPassword

    Private Sub frmPassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtPassword.Text = ""
    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            e.Handled = True
            If txtPassword.Text = PASSWORD Then
                Me.DialogResult = Windows.Forms.DialogResult.Yes
            Else
                Me.DialogResult = Windows.Forms.DialogResult.No
            End If
        End If
    End Sub
End Class