Public Class AppContext
    Inherits ApplicationContext

#Region " Storage "

    Private WithEvents Tray As NotifyIcon
    Private WithEvents MainMenu As ContextMenuStrip

    Private WithEvents mnuDisplayOptions As ToolStripMenuItem
    Private WithEvents mnuSep1 As ToolStripSeparator
    Private WithEvents mnuAbout As ToolStripMenuItem

    Private mIconCount As Integer = 0

#End Region

#Region " Constructor "

    Public Sub New()
        'Initialize the menus
        mnuDisplayOptions = New ToolStripMenuItem("Options")
        mnuSep1 = New ToolStripSeparator()
        mnuAbout = New ToolStripMenuItem("About")
        MainMenu = New ContextMenuStrip
        'MainMenu.Items.AddRange(New ToolStripItem() {mnuDisplayOptions, mnuSep1, mnuAbout})
        MainMenu.Items.AddRange(New ToolStripItem() {mnuDisplayOptions})

        'Initialize the tray
        Tray = New NotifyIcon
        Tray.Icon = My.Resources.volume_ok
        Tray.ContextMenuStrip = MainMenu
        Tray.Text = Application.ProductName

        'Display
        Tray.Visible = True
    End Sub

    Public Sub TrayIcon_Nok()
        Select Case mIconCount
            Case 0
                Tray.Icon = My.Resources.volume_nok0
            Case 1
                Tray.Icon = My.Resources.volume_nok1
            Case 2
                Tray.Icon = My.Resources.volume_nok2
            Case 3
                Tray.Icon = My.Resources.volume_nok3
        End Select

        mIconCount += 1
        If mIconCount > 3 Then mIconCount = 0
    End Sub

    Public Sub TrayIcon_Ok()
        Tray.Icon = My.Resources.volume_ok
    End Sub
#End Region

#Region " Event handlers "

    Private Sub AppContext_ThreadExit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ThreadExit
        'Guarantees that the icon will not linger.
        Tray.Visible = False
    End Sub

    Private Sub mnuDisplayOptions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDisplayOptions.Click
        ShowDialog_Password()
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        ShowDialog_About()
    End Sub

    Private Sub Tray_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tray.DoubleClick
        ShowDialog_Password()
    End Sub

#End Region

End Class