Module Main
    Private oTimer As System.Timers.Timer
    Private oAppContext As AppContext

    Public Sub Main()
        g_MaxVolume = MAX_VOLUME

        oTimer = New System.Timers.Timer()

        AddHandler oTimer.Elapsed, AddressOf OnTimedEvent_CheckVolume

        oTimer.Interval = 100
        oTimer.Enabled = True

        oAppContext = New AppContext
        Application.EnableVisualStyles()
        Application.Run(oAppContext)
    End Sub

    Private Sub OnTimedEvent_CheckVolume(ByVal source As Object, ByVal e As Timers.ElapsedEventArgs)
        If g_MaxVolume < 0 Then g_MaxVolume = 0
        If g_MaxVolume > MAX_VOLUME Then g_MaxVolume = MAX_VOLUME

        Try
            If VolumeControl.GetVolume > g_MaxVolume Then
                oAppContext.TrayIcon_Nok()

                VolumeControl.SetVolume(VolumeControl.GetVolume - VOLUME_CHANGE_SPEED)
                If VolumeControl.GetVolume < g_MaxVolume Then
                    VolumeControl.SetVolume(g_MaxVolume)
                End If
            Else
                oAppContext.TrayIcon_Ok()
            End If
        Catch ex As Exception

        End Try
    End Sub
End Module
