Public Class frmOptions


    Private Sub frmOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        trackVolume.Value = 100 * (g_MaxVolume / MAX_VOLUME)
    End Sub

    Private Sub trackVolume_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trackVolume.ValueChanged
        g_MaxVolume = (trackVolume.Value / 100) * MAX_VOLUME
    End Sub
End Class