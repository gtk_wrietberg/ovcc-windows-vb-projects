Imports System.IO
Imports System.Net
Imports System.Security.Cryptography
Imports System.Text
Imports System.Text.Encoding
Imports System.Text.RegularExpressions
Imports System.Xml

Public Class LicenseKey
    Private mRawServerResponse As String
    Private mToken As String
    Private mExpectedReturnToken As String
    Private mReturnToken As String
    Private mLicenseCodeLength As Integer
    Private mLicenseCode As String
    Private mReturnCode As Integer
    Private mReturnMessage As String
    Private mLicenseCount As Integer
    Private mLicenseCountMax As Integer
    Private mWorkgroup As String
    Private mMachineBaseName As String
    Private mMachineName As String

    '62.50.212.70
    '172.18.192.10
    'Private ReadOnly cLicenseValidationURL As String = "http://172.18.192.10/standalone_license/requesthandler.asp"
    Private ReadOnly cLicenseValidationURL As String = "http://62.50.212.70/standalone_license/requesthandler_withstupiddelay.asp"

    Private ReadOnly cDefaultTimeout = 10000

    Public Event LicenseIsValid()
    Public Event LicenseIsInvalid(ByVal ReturnCode As Integer, ByVal ReturnMessage As String)

#Region "properties"
    Public ReadOnly Property RawServerResponse() As Integer
        Get
            Return mRawServerResponse
        End Get
    End Property

    Public Property LicenseCodeLength() As Integer
        Get
            Return mLicenseCodeLength
        End Get
        Set(ByVal value As Integer)
            mLicenseCodeLength = value
        End Set
    End Property

    Public Property LicenseCode() As String
        Get
            Return mLicenseCode
        End Get
        Set(ByVal value As String)
            mLicenseCode = value
        End Set
    End Property

    Public ReadOnly Property ReturnCode() As Integer
        Get
            Return mReturnCode
        End Get
    End Property

    Public ReadOnly Property ReturnMessage() As String
        Get
            Return mReturnMessage
        End Get
    End Property

    Public ReadOnly Property LicenseCount() As Integer
        Get
            Return mLicenseCount
        End Get
    End Property

    Public ReadOnly Property LicenseCountMax() As Integer
        Get
            Return mLicenseCountMax
        End Get
    End Property

    Public ReadOnly Property Workgroup() As String
        Get
            Return mWorkgroup
        End Get
    End Property

    Public ReadOnly Property MachineBaseName() As String
        Get
            Return mMachineName
        End Get
    End Property

    Public ReadOnly Property MachineName() As String
        Get
            Return (mMachineName & "PC" & _zero_padding((mLicenseCount + 1), 3)).ToUpper
        End Get
    End Property
#End Region

#Region "privates"
    Private Sub _cleanup()
        Dim re As Regex = New Regex("[^a-z0-9]", RegexOptions.IgnoreCase)

        mLicenseCode = re.Replace(mLicenseCode, "")
    End Sub

    Private Sub _make_tokens()
        mToken = _sha1(Now.ToLongTimeString & Now.Millisecond.ToString)
        mExpectedReturnToken = _sha1("ibahn" & mToken & mLicenseCode)
    End Sub

    Private Function _sha1(ByVal sText As String) As String
        Dim sha1Obj As New SHA1CryptoServiceProvider
        Dim bytesToHash() As Byte = ASCII.GetBytes(sText)

        bytesToHash = sha1Obj.ComputeHash(bytesToHash)

        Dim strResult As String = ""

        For Each b As Byte In bytesToHash
            strResult += b.ToString("x2")
        Next

        Return strResult
    End Function

    Private Function _validate() As Boolean
        Try
            Dim Request As WebRequest = WebRequest.Create(cLicenseValidationURL)

            Request.Method = "POST"

            Dim PostData As String = "<iBAHN><StandAlone><License>" & mLicenseCode & "</License><Token></Token></StandAlone></iBAHN>"
            Dim ByteArray As Byte() = Encoding.UTF8.GetBytes(PostData)
            Dim Instance As New WebException
            Dim Value As WebExceptionStatus

            Request.ContentType = "text/xml"
            Request.ContentLength = ByteArray.Length
            Request.Timeout = 10000

            Dim dataStream As Stream = Request.GetRequestStream()
            dataStream.Write(ByteArray, 0, ByteArray.Length)
            dataStream.Close()

            Value = Instance.Status

            Dim response As WebResponse = Request.GetResponse()

            dataStream = response.GetResponseStream()

            Dim reader As New StreamReader(dataStream)
            mRawServerResponse = reader.ReadToEnd()

            reader.Close()
            dataStream.Close()
            response.Close()

            Return True
        Catch ex As Exception
            mReturnCode = -1
            mReturnMessage = ex.Message
            mLicenseCount = 0
            mLicenseCountMax = 0

            Return False
        End Try
    End Function

    Private Function _parse() As Boolean
        Try
            Dim xmldoc As New XmlDocument
            Dim xmlRoot As XmlNode
            Dim xmlNodeLicense As XmlNode

            xmldoc.LoadXml(mRawServerResponse)

            xmlRoot = xmldoc.SelectSingleNode("iBAHN")
            xmlNodeLicense = xmlRoot.SelectSingleNode("StandAlone").SelectSingleNode("License")

            Try
                mReturnCode = Integer.Parse(xmlNodeLicense.SelectSingleNode("ReturnCode").InnerText)
            Catch ex As Exception
                mReturnCode = 666
            End Try

            mReturnMessage = xmlNodeLicense.SelectSingleNode("ReturnMessage").InnerText

            Try
                mLicenseCount = Integer.Parse(xmlNodeLicense.SelectSingleNode("LicenseCount").InnerText)
            Catch ex As Exception
                mLicenseCount = 0
            End Try

            Try
                mLicenseCountMax = Integer.Parse(xmlNodeLicense.SelectSingleNode("LicenseCountMax").InnerText)
            Catch ex As Exception
                mLicenseCountMax = 0
            End Try

            mWorkgroup = xmlNodeLicense.SelectSingleNode("Workgroup").InnerText
            mMachineName = xmlNodeLicense.SelectSingleNode("MachineName").InnerText

            Return True
        Catch ex As Exception
            mReturnCode = -2
            mReturnMessage = ex.Message
            mLicenseCount = 0
            mLicenseCountMax = 0

            Return False
        End Try
    End Function

    Private Function _zero_padding(ByVal iNum As Integer, ByVal iZeros As Integer) As String
        Dim sNum As String

        sNum = iNum.ToString
        If sNum.Length < iZeros Then
            sNum = (New String("0", iZeros - sNum.Length)) & sNum
        End If

        Return sNum
    End Function
#End Region

#Region "publics"
    Public Sub CheckLicense()
        '_cleanup()
        '_make_tokens()

        If mLicenseCode.Equals("00000000000000000000000000000000") Then
            mWorkgroup = "ENG_WORKGROUP"
            mMachineName = "ENG_TEST0"
            mLicenseCount = 0
            mLicenseCountMax = 0

            RaiseEvent LicenseIsValid()

            Exit Sub
        End If

        If mLicenseCode.Length <> mLicenseCodeLength Then
            mReturnCode = -1
            mReturnMessage = "Invalid license key"

            RaiseEvent LicenseIsInvalid(mReturnCode, mReturnMessage)

            Exit Sub
        End If

        If _validate() Then
            If _parse() Then
            End If
        End If

        If mReturnCode = 0 Then
            RaiseEvent LicenseIsValid()
        Else
            RaiseEvent LicenseIsInvalid(mReturnCode, mReturnMessage)
        End If
    End Sub
#End Region
End Class
