Imports System.Threading

Public Class Form1
    Private WithEvents oLicense As LicenseKey
    Private licenseThread As Thread

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oLicense = New LicenseKey
    End Sub

    Private Sub oLicense_LicenseIsInvalid(ByVal ReturnCode As Integer, ByVal ReturnMessage As String) Handles oLicense.LicenseIsInvalid
        MsgBox("ReturnCode=" & ReturnCode.ToString & " - ReturnMessage=" & ReturnMessage)
    End Sub

    Private Sub oLicense_LicenseIsValid() Handles oLicense.LicenseIsValid
        MsgBox("ok")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        TextBox1.Text = ""

        Me.licenseThread = New Thread(New ThreadStart(AddressOf Me.CheckLicenseCode))
        Me.licenseThread.Start()
    End Sub

    Public Sub AddLineToTextBox1(ByVal sLine As String)
        TextBox1.Text &= sLine & vbCrLf
    End Sub

    Private Sub CheckLicenseCode()

        oLicense.LicenseCodeLength = 32
        oLicense.LicenseCode = txtLicenseCode.Text

        oLicense.CheckLicense()
    End Sub

    Private Sub txtLicenseCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLicenseCode.TextChanged
        Button1.Enabled = (txtLicenseCode.Text.Length = 32)
    End Sub
End Class
