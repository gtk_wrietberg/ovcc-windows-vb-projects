Module Module1
    Public ReadOnly cREGKEY_LPCSOFTWARE As String = "SOFTWARE\iBAHN\LobbyPCSoftwareOnly"
    Public ReadOnly cPATHS_iBAHNProgramFilesFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\LobbyPCSoftwareOnly"

    Public Sub CleanUpTest()
        _CleanUpRegistry()
    End Sub

    Private Sub _CleanUpRegistry()
        Dim oLogger As New Logger


        oLogger.WriteToLog("Cleaning registry")

        oLogger.WriteToLog("registry rollback", , 1)
        oLogger.WriteToLog("searching for rollback file", , 2)
        If IO.File.Exists(cPATHS_iBAHNProgramFilesFolder & "\LobbyPCSoftwareOnlyInstallation.registry.backup.xml") Then
            oLogger.WriteToLog("ok", , 3)

            oLogger.WriteToLog("opening", , 2)

            Try
                Dim mXmlDoc As New Xml.XmlDocument
                Dim mXmlNodeRoot As Xml.XmlNode
                Dim mXmlNodelistKeys As Xml.XmlNodeList

                Dim mXmlNodeName As Xml.XmlNode, mXmlNodeValueName As Xml.XmlNode
                Dim mXmlNodeValue As Xml.XmlNode, mXmlNodeType As Xml.XmlNode

                mXmlDoc.Load(cPATHS_iBAHNProgramFilesFolder & "\LobbyPCSoftwareOnlyInstallation.registry.backup.xml")
                oLogger.WriteToLog("ok", , 3)

                mXmlNodeRoot = mXmlDoc.SelectSingleNode("LobbyPCSoftwareOnly")
                mXmlNodelistKeys = mXmlNodeRoot.SelectSingleNode("registry-backup").SelectNodes("key")

                '<key>
                '  <name>HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPCSoftwareOnly\Config</name>
                '  <valuename>BlockedDrives</valuename>
                '  <old>
                '    <value>(does not exist)</value>
                '    <type>0</type>
                '  </old>
                '  <new>
                '    <value>C,</value>
                '    <type>0</type>
                '  </new>
                '</key>

                Dim rk As Microsoft.Win32.RegistryKey, sRegKey As String

                For Each mXmlNodeKey As Xml.XmlNode In mXmlNodelistKeys


                    mXmlNodeName = mXmlNodeKey.SelectSingleNode("name")
                    mXmlNodeValueName = mXmlNodeKey.SelectSingleNode("valuename")
                    mXmlNodeValue = mXmlNodeKey.SelectSingleNode("old").SelectSingleNode("value")
                    mXmlNodeType = mXmlNodeKey.SelectSingleNode("old").SelectSingleNode("type")

                    oLogger.WriteToLog(mXmlNodeKey.SelectSingleNode("name").InnerText & "\" & mXmlNodeValueName.InnerText)

                    sRegKey = mXmlNodeName.InnerText

                    If mXmlNodeValue.InnerText = "(does not exist)" And mXmlNodeType.InnerText = "0" Then
                        If mXmlNodeName.InnerText.StartsWith("HKEY_LOCAL_MACHINE\") Then
                            sRegKey = mXmlNodeName.InnerText.Replace("HKEY_LOCAL_MACHINE\", "")

                            rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sRegKey, True)

                            oLogger.WriteToLog("HKEY_LOCAL_MACHINE\")
                        ElseIf mXmlNodeName.InnerText.StartsWith("HKEY_USERS\") Then
                            sRegKey = mXmlNodeName.InnerText.Replace("HKEY_USERS\", "")

                            rk = Microsoft.Win32.Registry.Users.OpenSubKey(sRegKey, True)

                            oLogger.WriteToLog("HKEY_USERS\")
                        End If

                        oLogger.WriteToLog("rk.OpenSubKey(" & sRegKey & ", True)")

                        For Each rkgvn As String In rk.GetValueNames
                            oLogger.WriteToLog(rkgvn)
                        Next

                        oLogger.WriteToLog("rk.DeleteValue(" & mXmlNodeValueName.InnerText & ")")
                        rk.DeleteValue(mXmlNodeValueName.InnerText)
                    Else
                        Microsoft.Win32.Registry.SetValue(mXmlNodeName.InnerText, mXmlNodeValueName.InnerText, mXmlNodeValue.InnerText, Integer.Parse(mXmlNodeType.InnerText))
                    End If
                Next
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End Try
        Else
            oLogger.WriteToLog("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If



    End Sub

    Private Class Logger
        Public Enum MESSAGE_TYPE
            LOG_DEFAULT = 0
            LOG_WARNING = 1
            LOG_ERROR = 2
            LOG_DEBUG = 6
        End Enum

        Public Sub WriteToLog(ByVal s As String, Optional ByVal o1 As Object = Nothing, Optional ByVal o2 As Object = Nothing)
            MsgBox(s, MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly)
        End Sub

    End Class
End Module
