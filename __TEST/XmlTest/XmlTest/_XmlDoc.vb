Public Class _XmlDoc
    Private ReadOnly DefaultXmlFile As String = "LobbyPCSoftwareOnlyInstallation.registry.backup.xml"
    Private ReadOnly DefaultXmlFilePath As String = "C:\temp\"

    Private mXmlDocument As Xml.XmlDocument
    Private mXmlNodeRoot As Xml.XmlNode
    Private mXmlNodeRegistry As Xml.XmlNode

    Private Function _CreateXml() As Boolean
        Try
            If IO.File.Exists(DefaultXmlFilePath & DefaultXmlFile) Then
                IO.File.Delete(DefaultXmlFilePath & DefaultXmlFile)
            End If

            mXmlDocument = New Xml.XmlDocument

            mXmlNodeRoot = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "LobbyPCSoftwareOnly", "")

            mXmlNodeRegistry = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "registry-backup", "")

            Return True
        Catch ex As Exception
            MsgBox("ERROR in _CreateXml()")
        End Try
    End Function

    Private Function _SaveXml() As Boolean
        Try
            mXmlNodeRoot.AppendChild(mXmlNodeRegistry)
            mXmlDocument.AppendChild(mXmlNodeRoot)

            mXmlDocument.Save(DefaultXmlFilePath & DefaultXmlFile)

            Return True
        Catch ex As Exception
            MsgBox("ERROR in _SaveXml()")
        End Try
    End Function

    Public Function Open() As Boolean
        Return _CreateXml()
    End Function

    Public Function Close() As Boolean
        Return _SaveXml()
    End Function

    Public Function Add(ByVal keyName As String, ByVal valueName As String, ByVal valueOld As Object, ByVal typeOld As Microsoft.Win32.RegistryValueKind, ByVal valueNew As Object, ByVal typeNew As Microsoft.Win32.RegistryValueKind) As Boolean
        Dim xmlNodeKey As Xml.XmlNode, xmlNodeKeyOld As Xml.XmlNode, xmlNodeKeyNew As Xml.XmlNode
        Dim xmlNodeName As Xml.XmlNode, xmlNodeValueName As Xml.XmlNode
        Dim xmlNodeValueOld As Xml.XmlNode, xmlNodeTypeOld As Xml.XmlNode
        Dim xmlNodeValueNew As Xml.XmlNode, xmlNodeTypeNew As Xml.XmlNode

        xmlNodeKey = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "key", "")

        xmlNodeName = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "name", "")
        xmlNodeName.InnerText = keyName

        xmlNodeValueName = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "valuename", "")
        xmlNodeValueName.InnerText = valueName

        xmlNodeKeyOld = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "old", "")
        xmlNodeKeyNew = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "new", "")

        xmlNodeValueOld = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "value", "")
        xmlNodeTypeOld = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "type", "")
        xmlNodeValueNew = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "value", "")
        xmlNodeTypeNew = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "type", "")

        xmlNodeValueOld.InnerText = valueOld.ToString
        xmlNodeTypeOld.InnerText = typeOld
        xmlNodeValueNew.InnerText = valueNew.ToString
        xmlNodeTypeNew.InnerText = typeNew

        xmlNodeKeyOld.AppendChild(xmlNodeValueOld)
        xmlNodeKeyOld.AppendChild(xmlNodeTypeOld)
        xmlNodeKeyNew.AppendChild(xmlNodeValueNew)
        xmlNodeKeyNew.AppendChild(xmlNodeTypeNew)

        xmlNodeKey.AppendChild(xmlNodeName)
        xmlNodeKey.AppendChild(xmlNodeValueName)
        xmlNodeKey.AppendChild(xmlNodeKeyOld)
        xmlNodeKey.AppendChild(xmlNodeKeyNew)

        mXmlNodeRegistry.AppendChild(xmlNodeKey)
    End Function
End Class