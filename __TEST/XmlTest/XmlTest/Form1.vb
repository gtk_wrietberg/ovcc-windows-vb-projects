Imports System.Xml

Public Class Form1
    Private mXmlDocument As XmlDocument
    Private mXmlNodeRoot As XmlNode
    Private mXmlNodeRegistry As XmlNode

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        mXmlDocument = New XmlDocument

        mXmlNodeRoot = mXmlDocument.CreateNode(XmlNodeType.Element, "", "LobbyPCSoftwareOnly", "")

        mXmlNodeRegistry = mXmlDocument.CreateNode(XmlNodeType.Element, "", "registry-backup", "")
        mXmlNodeRoot.AppendChild(mXmlNodeRegistry)

        mXmlDocument.AppendChild(mXmlNodeRoot)

        mXmlDocument.Save("c:\temp\LPCTEST.xml")
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim o As New _XmlDoc

        o.Open()
        o.Add("HKEY_USERS\.DEFAULT\Control Panel\PowerCfg", "CurrentPowerPolicy", "2", Microsoft.Win32.RegistryValueKind.Unknown, "3", Microsoft.Win32.RegistryValueKind.String)
        o.Add("HKEY_USERS\.DEFAULT\Control Panel\PowerCfg2", "CurrentPowerPolicy2", "3", Microsoft.Win32.RegistryValueKind.DWord, "4", Microsoft.Win32.RegistryValueKind.DWord)
        o.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        CleanUpTest()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim s As String = ""

        s &= "Microsoft.Win32.RegistryValueKind.Binary = " & Microsoft.Win32.RegistryValueKind.Binary
        s &= vbCrLf
        s &= "Microsoft.Win32.RegistryValueKind.Binary = " & Microsoft.Win32.RegistryValueKind.DWord
        s &= vbCrLf
        s &= "Microsoft.Win32.RegistryValueKind.Binary = " & Microsoft.Win32.RegistryValueKind.String
        s &= vbCrLf
    End Sub
End Class
