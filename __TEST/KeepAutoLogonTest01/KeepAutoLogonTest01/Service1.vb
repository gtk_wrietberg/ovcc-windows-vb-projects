﻿Public Class KeepAutoLogonService
    Private mThreading_Main As Threading.Thread

    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Thread_Main()
        Dim s As String = ""
        Dim val01 As String = ""
        Dim val02 As String = ""
        Dim val03 As String = ""

        Dim regKey_root As Microsoft.Win32.RegistryKey
        Dim regKey_winlogon As Microsoft.Win32.RegistryKey

        regKey_root = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64)
        regKey_winlogon = regKey_root.OpenSubKey("SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", True)

        Helpers.Logger.Write("checking values", , 0)

        Do While True
            s = regKey_winlogon.GetValue("AutoAdminLogon", "(not set)").ToString

            If Not val01.Equals(s) Then
                Helpers.Logger.Write("AutoAdminLogon", , 1)
                Helpers.Logger.Write(val01 & " --> " & s, , 2)

                val01 = s
            End If


            s = regKey_winlogon.GetValue("DefaultUserName", "(not set)").ToString

            If Not val02.Equals(s) Then
                Helpers.Logger.Write("DefaultUserName", , 1)
                Helpers.Logger.Write(val01 & " --> " & s, , 2)

                val02 = s
            End If


            s = regKey_winlogon.GetValue("DefaultPassword", "(not set)").ToString

            If Not val03.Equals(s) Then
                Helpers.Logger.Write("DefaultPassword", , 1)
                Helpers.Logger.Write(val01 & " --> " & s, , 2)

                val03 = s
            End If

            'Helpers.Registry.SetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "DefaultUserName ", "testuser")
            'Helpers.Registry.SetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "DefaultPassword ", "t3stus3r")
            'Helpers.Registry.SetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "AutoAdminLogon ", "1")

            Threading.Thread.Sleep(1000)
        Loop
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Helpers.Logger.InitialiseLogger()


        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("service started", , 0)



        StartMainThread()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.

        KillThreads()

        Helpers.Logger.Write("service stopped", , 0)
    End Sub

End Class
