Imports System.net
Imports System.IO
Imports System.Collections.Specialized

Public Class Form1
    Private ReadOnly responseString As String = "<HTML><BODY> Hello world!</BODY></HTML>"

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        DoListenStuff
    End Sub

    Private Sub DoListenStuff()
        'Create a listener.
        Dim listener As New HttpListener()
        Dim context As HttpListenerContext
        Dim request As HttpListenerRequest
        Dim response As HttpListenerResponse

        listener.Prefixes.Add("http://localhost:8181/HttpListenerApp/")
        listener.Start()

        context = listener.GetContext()
        request = context.Request
        response = context.Response

        Dim qString As New NameValueCollection
        Dim str As String = ""
        qString = request.QueryString

        str = str & "Credit card name  : " & qString.Item("cname") & vbCrLf
        str = str & "Credit card number: " & qString.Item("cnum") & vbCrLf
        str = str & "Credit card date  : " & qString.Item("cdate") & vbCrLf
        str = str & "Credit card cvc   : " & qString.Item("cvc")

        TextBox1.Text = str
        TextBox1.Refresh()

        '?cnum=510510510555&cdate=1209&cvc=1234

        Dim buffer As Byte()

        buffer = System.Text.Encoding.UTF8.GetBytes(responseString)

        response.ContentLength64 = buffer.Length

        Dim s As Stream = response.OutputStream
        s.Write(buffer, 0, buffer.Length)
        s.Close()

        listener.Stop()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim a As AliasAccount

        a = New AliasAccount("woutertest", "Baf12345")

        MsgBox("1: " & a.currentWindowsUsername)

        a.BeginImpersonation()

        MsgBox("2: " & a.currentWindowsUsername)

        DoListenStuff()

        a.EndImpersonation()

        MsgBox("3: " & a.currentWindowsUsername)
    End Sub
End Class
