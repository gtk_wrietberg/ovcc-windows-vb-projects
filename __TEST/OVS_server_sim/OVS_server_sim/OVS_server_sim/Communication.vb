﻿Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports System.Xml
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class Communication
    Public Shared Function ValidateRemoteCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
        Return True
    End Function

    Public Class JSON
        Public Event DebugMessage(msg As String)


        Private mServerUrl As String
        Public Property ServerUrl() As String
            Get
                Return mServerUrl
            End Get
            Set(ByVal value As String)
                mServerUrl = value
            End Set
        End Property

        Public Sub New()
            mServerUrl = ""
        End Sub

        Public Sub New(ServerUrl As String)
            mServerUrl = ServerUrl
        End Sub


        Public Request As New JObject

        Public LastError As String = ""

        Public Function Send() As Boolean
            Dim webClient As New WebClient()
            Dim resByte As Byte()
            Dim resString As String = ""
            Dim reqString() As Byte

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12

            Try
                RaiseEvent DebugMessage("sending request to: " & mServerUrl)
                ' RaiseEvent DebugMessage(Request.ToString)

                webClient.Headers("content-type") = "application/json"
                'webClient.Headers.Add("User-Agent: " & Constants.Communication.UserAgentString)
                reqString = Encoding.Default.GetBytes(JsonConvert.SerializeObject(Me.Request, Newtonsoft.Json.Formatting.Indented))
                resByte = webClient.UploadData(mServerUrl, "post", reqString)
                resString = Encoding.Default.GetString(resByte)

                RaiseEvent DebugMessage(resString)

                webClient.Dispose()

                Me.LastError = ""

                Return True
            Catch ex As Exception
                RaiseEvent DebugMessage("sending request to: " & mServerUrl & "' failed!")
                RaiseEvent DebugMessage("raw response")
                RaiseEvent DebugMessage(resString)
                RaiseEvent DebugMessage("exception")
                RaiseEvent DebugMessage(ex.Message)

                Me.LastError = ex.Message
            End Try

            Return False
        End Function
    End Class
End Class
