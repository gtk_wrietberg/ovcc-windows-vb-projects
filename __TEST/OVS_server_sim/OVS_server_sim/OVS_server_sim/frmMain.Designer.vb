﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        btnSaveRandom = New Button()
        txt__os_nodename = New TextBox()
        Label1 = New Label()
        Label2 = New Label()
        txt__localmac = New TextBox()
        txt__synapseid = New TextBox()
        Label6 = New Label()
        txt__ovs_version = New TextBox()
        Label5 = New Label()
        pnlDebug = New Panel()
        grpDebug = New GroupBox()
        txtDebug = New TextBox()
        grpData = New GroupBox()
        chkDebugging = New CheckBox()
        Label3 = New Label()
        numPhonehomes = New NumericUpDown()
        Panel1 = New Panel()
        bgworkerSendPhonehome = New ComponentModel.BackgroundWorker()
        Panel2 = New Panel()
        Panel3 = New Panel()
        pnlDebug.SuspendLayout()
        grpDebug.SuspendLayout()
        grpData.SuspendLayout()
        CType(numPhonehomes, ComponentModel.ISupportInitialize).BeginInit()
        Panel1.SuspendLayout()
        SuspendLayout()
        ' 
        ' btnSaveRandom
        ' 
        btnSaveRandom.Location = New Point(392, 16)
        btnSaveRandom.Name = "btnSaveRandom"
        btnSaveRandom.Size = New Size(198, 48)
        btnSaveRandom.TabIndex = 0
        btnSaveRandom.Text = "Send random phonehome"
        btnSaveRandom.UseVisualStyleBackColor = True
        ' 
        ' txt__os_nodename
        ' 
        txt__os_nodename.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        txt__os_nodename.Location = New Point(109, 16)
        txt__os_nodename.Name = "txt__os_nodename"
        txt__os_nodename.Size = New Size(277, 23)
        txt__os_nodename.TabIndex = 1
        txt__os_nodename.Text = "ovs.test01.com"
        ' 
        ' Label1
        ' 
        Label1.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        Label1.Location = New Point(-36, 19)
        Label1.Name = "Label1"
        Label1.Size = New Size(139, 20)
        Label1.TabIndex = 2
        Label1.Text = "os_nodename"
        Label1.TextAlign = ContentAlignment.MiddleRight
        ' 
        ' Label2
        ' 
        Label2.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        Label2.Location = New Point(-36, 44)
        Label2.Name = "Label2"
        Label2.Size = New Size(139, 20)
        Label2.TabIndex = 4
        Label2.Text = "localmac"
        Label2.TextAlign = ContentAlignment.MiddleRight
        ' 
        ' txt__localmac
        ' 
        txt__localmac.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        txt__localmac.Location = New Point(109, 41)
        txt__localmac.Name = "txt__localmac"
        txt__localmac.Size = New Size(277, 23)
        txt__localmac.TabIndex = 3
        txt__localmac.Text = "00:11:22:33:44:55:66"
        ' 
        ' txt__synapseid
        ' 
        txt__synapseid.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        txt__synapseid.Location = New Point(109, 91)
        txt__synapseid.Name = "txt__synapseid"
        txt__synapseid.Size = New Size(277, 23)
        txt__synapseid.TabIndex = 11
        txt__synapseid.Text = "12345"
        ' 
        ' Label6
        ' 
        Label6.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        Label6.Location = New Point(-36, 94)
        Label6.Name = "Label6"
        Label6.Size = New Size(139, 20)
        Label6.TabIndex = 12
        Label6.Text = "synapseid"
        Label6.TextAlign = ContentAlignment.MiddleRight
        ' 
        ' txt__ovs_version
        ' 
        txt__ovs_version.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        txt__ovs_version.Location = New Point(109, 66)
        txt__ovs_version.Name = "txt__ovs_version"
        txt__ovs_version.Size = New Size(277, 23)
        txt__ovs_version.TabIndex = 9
        txt__ovs_version.Text = "1.3.3"
        ' 
        ' Label5
        ' 
        Label5.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        Label5.Location = New Point(-36, 69)
        Label5.Name = "Label5"
        Label5.Size = New Size(139, 20)
        Label5.TabIndex = 10
        Label5.Text = "ovs_version"
        Label5.TextAlign = ContentAlignment.MiddleRight
        ' 
        ' pnlDebug
        ' 
        pnlDebug.BackColor = Color.Gainsboro
        pnlDebug.BorderStyle = BorderStyle.FixedSingle
        pnlDebug.Controls.Add(grpDebug)
        pnlDebug.Location = New Point(12, 158)
        pnlDebug.Name = "pnlDebug"
        pnlDebug.Size = New Size(607, 261)
        pnlDebug.TabIndex = 10
        ' 
        ' grpDebug
        ' 
        grpDebug.Controls.Add(txtDebug)
        grpDebug.Location = New Point(3, 3)
        grpDebug.Name = "grpDebug"
        grpDebug.Size = New Size(597, 253)
        grpDebug.TabIndex = 13
        grpDebug.TabStop = False
        grpDebug.Text = "Debugging"
        ' 
        ' txtDebug
        ' 
        txtDebug.BorderStyle = BorderStyle.FixedSingle
        txtDebug.Font = New Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        txtDebug.Location = New Point(6, 22)
        txtDebug.Multiline = True
        txtDebug.Name = "txtDebug"
        txtDebug.ReadOnly = True
        txtDebug.ScrollBars = ScrollBars.Vertical
        txtDebug.Size = New Size(585, 224)
        txtDebug.TabIndex = 0
        ' 
        ' grpData
        ' 
        grpData.Controls.Add(chkDebugging)
        grpData.Controls.Add(Label3)
        grpData.Controls.Add(numPhonehomes)
        grpData.Controls.Add(txt__synapseid)
        grpData.Controls.Add(Label1)
        grpData.Controls.Add(Label6)
        grpData.Controls.Add(Label2)
        grpData.Controls.Add(txt__ovs_version)
        grpData.Controls.Add(txt__localmac)
        grpData.Controls.Add(Label5)
        grpData.Controls.Add(txt__os_nodename)
        grpData.Controls.Add(btnSaveRandom)
        grpData.Location = New Point(3, 3)
        grpData.Name = "grpData"
        grpData.Size = New Size(597, 126)
        grpData.TabIndex = 11
        grpData.TabStop = False
        grpData.Text = "Data"
        ' 
        ' chkDebugging
        ' 
        chkDebugging.AutoSize = True
        chkDebugging.Checked = True
        chkDebugging.CheckState = CheckState.Checked
        chkDebugging.Location = New Point(469, 95)
        chkDebugging.Name = "chkDebugging"
        chkDebugging.Size = New Size(121, 19)
        chkDebugging.TabIndex = 15
        chkDebugging.Text = "Debug timestamp"
        chkDebugging.TextAlign = ContentAlignment.MiddleCenter
        chkDebugging.UseVisualStyleBackColor = True
        ' 
        ' Label3
        ' 
        Label3.Font = New Font("Segoe UI", 9F, FontStyle.Bold)
        Label3.Location = New Point(392, 69)
        Label3.Name = "Label3"
        Label3.Size = New Size(149, 20)
        Label3.TabIndex = 14
        Label3.Text = "# requests"
        Label3.TextAlign = ContentAlignment.MiddleRight
        ' 
        ' numPhonehomes
        ' 
        numPhonehomes.Location = New Point(547, 66)
        numPhonehomes.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        numPhonehomes.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        numPhonehomes.Name = "numPhonehomes"
        numPhonehomes.Size = New Size(43, 23)
        numPhonehomes.TabIndex = 13
        numPhonehomes.Value = New Decimal(New Integer() {1, 0, 0, 0})
        ' 
        ' Panel1
        ' 
        Panel1.BackColor = Color.Gainsboro
        Panel1.BorderStyle = BorderStyle.FixedSingle
        Panel1.Controls.Add(grpData)
        Panel1.Location = New Point(12, 12)
        Panel1.Name = "Panel1"
        Panel1.Size = New Size(607, 134)
        Panel1.TabIndex = 12
        ' 
        ' bgworkerSendPhonehome
        ' 
        bgworkerSendPhonehome.WorkerReportsProgress = True
        bgworkerSendPhonehome.WorkerSupportsCancellation = True
        ' 
        ' Panel2
        ' 
        Panel2.BackColor = Color.Black
        Panel2.BorderStyle = BorderStyle.FixedSingle
        Panel2.Location = New Point(18, 18)
        Panel2.Name = "Panel2"
        Panel2.Size = New Size(607, 134)
        Panel2.TabIndex = 13
        ' 
        ' Panel3
        ' 
        Panel3.BackColor = Color.Black
        Panel3.BorderStyle = BorderStyle.FixedSingle
        Panel3.Location = New Point(18, 164)
        Panel3.Name = "Panel3"
        Panel3.Size = New Size(607, 261)
        Panel3.TabIndex = 14
        ' 
        ' frmMain
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.WhiteSmoke
        ClientSize = New Size(639, 438)
        Controls.Add(Panel1)
        Controls.Add(pnlDebug)
        Controls.Add(Panel2)
        Controls.Add(Panel3)
        FormBorderStyle = FormBorderStyle.FixedSingle
        Name = "frmMain"
        Text = "OVS server sim"
        pnlDebug.ResumeLayout(False)
        grpDebug.ResumeLayout(False)
        grpDebug.PerformLayout()
        grpData.ResumeLayout(False)
        grpData.PerformLayout()
        CType(numPhonehomes, ComponentModel.ISupportInitialize).EndInit()
        Panel1.ResumeLayout(False)
        ResumeLayout(False)
    End Sub

    Friend WithEvents btnSaveRandom As Button
    Friend WithEvents txt__os_nodename As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txt__localmac As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txt__os_type As TextBox
    Friend WithEvents txt__os_version As TextBox
    Friend WithEvents pnlDebug As Panel
    Friend WithEvents txtDebug As TextBox
    Friend WithEvents txt__ovs_version As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txt__synapseid As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents grpData As GroupBox
    Friend WithEvents grpDebug As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents numPhonehomes As NumericUpDown
    Friend WithEvents bgworkerSendPhonehome As System.ComponentModel.BackgroundWorker
    Friend WithEvents chkDebugging As CheckBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel

End Class
