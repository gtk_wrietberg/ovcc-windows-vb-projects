﻿Imports System.ComponentModel

Public Class frmMain
    Private WithEvents mComm As Communication.JSON


    Delegate Sub _ThreadSafe_Callback__btnSaveRandom(ByVal [enabled] As Boolean)
    Public Sub _ThreadSafe__btnSaveRandom(ByVal [enabled] As Boolean)
        If btnSaveRandom.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__btnSaveRandom(AddressOf _ThreadSafe__btnSaveRandom)
            btnSaveRandom.Invoke(d, New Object() {[enabled]})
        Else
            btnSaveRandom.Enabled = [enabled]
        End If
    End Sub

    Delegate Sub _ThreadSafe_Callback__txtDebug(ByVal [text] As String)
    Public Sub _ThreadSafe__txtDebug(ByVal [text] As String)
        If txtDebug.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__txtDebug(AddressOf _ThreadSafe__txtDebug)
            txtDebug.Invoke(d, New Object() {[text]})
        Else
            txtDebug.AppendText([text] & vbCrLf)
        End If
    End Sub


    Private mRandomTimestamp As Long = 0


    Private Sub btnSaveRandom_Click(sender As Object, e As EventArgs) Handles btnSaveRandom.Click
        _ThreadSafe__btnSaveRandom(False)

        txtDebug.Clear()

        bgworkerSendPhonehome.RunWorkerAsync()
    End Sub

    Private Sub mComm_DebugMessage(msg As String) Handles mComm.DebugMessage
        _ThreadSafe__txtDebug(msg)
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub buildJson()
        mComm.Request.RemoveAll()
        If chkDebugging.Checked Then
            mComm.Request.Add("debugging", 1)

            If mRandomTimestamp <= 0 Then
                mRandomTimestamp = RandomLong(1500000000000, 1700000000000)
            Else
                mRandomTimestamp += (10 * 60 * 1000) 'add 10 minutes
            End If

            mComm.Request.Add("nowTimestamp", mRandomTimestamp)

            Dim dDate As Date = FromUnix(mRandomTimestamp / 1000)
            mComm.Request.Add("nowYear", dDate.Year)
            mComm.Request.Add("nowMonth", dDate.Month - 1)
            mComm.Request.Add("nowDay", dDate.Day)
            mComm.Request.Add("nowHour", dDate.Hour)
            mComm.Request.Add("nowMin30", (100 * dDate.Hour) + CInt(Math.Floor(dDate.Minute / 30)))
            mComm.Request.Add("nowMin15", (100 * dDate.Hour) + CInt(Math.Floor(dDate.Minute / 15)))
        End If

        mComm.Request.Add("os_nodename", txt__os_nodename.Text)
        mComm.Request.Add("ovs_version", txt__ovs_version.Text)
        mComm.Request.Add("currenttimestamp", RandomLong(1600000000000, 1730000000000))
        mComm.Request.Add("localmac", txt__localmac.Text)
        mComm.Request.Add("contenttotalfilesize", RandomLong(100000000, 999999999))
        mComm.Request.Add("rxbitrate", RandomInteger(0, 10000))
        mComm.Request.Add("rxbytes", RandomLong(1000, 999999999))
        mComm.Request.Add("ccmsmultitasks", RandomInteger(0, 100))
        mComm.Request.Add("taskswaiting", RandomInteger(0, 100))
        mComm.Request.Add("memfree", RandomLong(1000, 999999999))
        mComm.Request.Add("txbytes", RandomLong(1000, 999999999))
        mComm.Request.Add("allcontenttotalfilesize", RandomLong(1000, 999999999))
        mComm.Request.Add("tasksfailed", RandomInteger(0, 100))
        mComm.Request.Add("diskfreepercentage", RandomInteger(0, 100))
        mComm.Request.Add("taskscompleted", RandomInteger(0, 100))
        mComm.Request.Add("lasttxbytes", RandomLong(1000, 999999999))
        mComm.Request.Add("uptimestring", "")
        mComm.Request.Add("synapseid", txt__synapseid.Text)
        mComm.Request.Add("taskstotal", RandomInteger(0, 100))
        mComm.Request.Add("txbitrate", RandomInteger(0, 10000))
        mComm.Request.Add("contentfilecount", RandomInteger(0, 100))
        mComm.Request.Add("httpbitratembps", RandomDouble(10))
        mComm.Request.Add("tasksrunning", RandomInteger(0, 100))
        mComm.Request.Add("lastruntimestamp", RandomLong(1600000000000, 1730000000000))
        mComm.Request.Add("lastccmsdownloadstarted", RandomInteger(0, 100))
        mComm.Request.Add("os_version", "2.6.32-754.3.5.el6.x86_64")
        mComm.Request.Add("cpuaverage", RandomDouble(10).ToString & " 0.02 0.00")
        mComm.Request.Add("httpplayoutcount", RandomInteger(0, 100))
        mComm.Request.Add("ccmstotalfilesize", RandomLong(1000, 999999999))
        mComm.Request.Add("totalbitrate", RandomInteger(0, 1000))
        mComm.Request.Add("ccmsmulticastloopcount", RandomInteger(0, 100))
        mComm.Request.Add("uptime", RandomInteger(0, 1000000))
        mComm.Request.Add("ccmsfilecount", RandomInteger(0, 1000))
        mComm.Request.Add("lastrxbytes", RandomLong(1000, 999999999))
        mComm.Request.Add("multicastloopcount", RandomInteger(0, 1000))
        mComm.Request.Add("os_type", "GNU/Linux")
        mComm.Request.Add("localip", "127.0.0.1")
        mComm.Request.Add("diskfree", RandomLong(1000, 999999999))
        mComm.Request.Add("totalfilecount", RandomInteger(0, 100))
        mComm.Request.Add("expectedccmsfiles", RandomInteger(0, 100))
        mComm.Request.Add("actualccmsfiles", RandomInteger(0, 100))
        mComm.Request.Add("expectedccmsstreams", RandomInteger(0, 100))
        mComm.Request.Add("actualccmsstreams", RandomInteger(0, 100))
        mComm.Request.Add("activedownloadcount", RandomInteger(0, 100))
    End Sub

    Private Function RandomInteger(min As Integer, max As Integer) As Integer
        Dim Generator As System.Random = New System.Random()
        Return Generator.Next(min, max)
    End Function

    Private Function RandomLong(min As Long, max As Long) As Long
        Dim Generator As System.Random = New System.Random()
        Return Generator.NextInt64(min, max)
    End Function

    Private Function RandomDouble(factor As Integer) As Double
        Dim Generator As System.Random = New System.Random()
        Return Generator.NextDouble() * factor
    End Function

    Public Shared Function UnixTimestamp() As Long
        Return UnixTimestamp(DateTime.UtcNow)
    End Function

    Public Shared Function UnixTimestamp(dt As DateTime) As Long
        Return Convert.ToInt64((dt - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)
    End Function

    Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
        Dim dTmp As Date

        dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

        If dTmp.IsDaylightSavingTime Then
            dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
        End If

        Return dTmp
    End Function

    Private Sub bgworkerSendPhonehome_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworkerSendPhonehome.DoWork
        mComm = New Communication.JSON
        mComm.ServerUrl = "https://api-ovs-dev.guesttek.cloud/dev/ovsstatus"

        For i As Integer = 0 To numPhonehomes.Value - 1
            _ThreadSafe__txtDebug("===============================================")
            _ThreadSafe__txtDebug("phonehome #" & (i + 1).ToString)

            buildJson()
            mComm.Send()

            Threading.Thread.Sleep(1000)
        Next

        _ThreadSafe__txtDebug("===============================================")
        _ThreadSafe__txtDebug("done")
    End Sub

    Private Sub bgworkerSendPhonehome_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworkerSendPhonehome.RunWorkerCompleted
        mRandomTimestamp = 0

        _ThreadSafe__btnSaveRandom(True)
    End Sub
End Class
