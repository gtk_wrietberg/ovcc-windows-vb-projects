﻿
Imports System.Data.SQLite
Imports System.Diagnostics
Imports System.Windows

Namespace sqlite_test

    ''' <summary>
    ''' Interaction logic for MainWindow.xaml
    ''' </summary>
    Partial Public Class MainWindow
        Inherits Window

        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub Button_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)

            Dim sqlLiteFileName As String = "sample.sqlite"

            ' Source: https://www.youtube.com/watch?v=APVit-pynwQ&t=5

            Dim createQuery As String = "CREATE TABLE IF NOT EXISTS
                    [Mytable] (
                    [Id]     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    [NAME]   NVARCHAR(2048) NULL,
                    [GENDER] NVARCHAR(2048) NULL)"

            SQLiteConnection.CreateFile("sample.sqlite")

            Using conn As SQLiteConnection = New SQLiteConnection("data source =" & sqlLiteFileName)
                Using cmd As SQLiteCommand = New SQLiteCommand(conn)

                    conn.Open()
                    cmd.CommandText = createQuery
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "INSERT INTO MyTable(Name, Gender)values('alex','male')"
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "INSERT INTO MyTable(Name, Gender)values('diane','female')"
                    cmd.ExecuteNonQuery()

                    cmd.CommandText = "select * from MyTable"

                    Using reader As SQLiteDataReader = cmd.ExecuteReader()
                        While reader.Read()
                            Dim output As String = reader("Name").ToString() + ":"c + reader("Gender").ToString()

                            OutputTextBox.Text += output & vbLf
                        End While
                    End Using

                End Using
            End Using
        End Sub

        Private Sub Hyperlink_RequestNavigate(ByVal sender As Object,
                                              ByVal e As System.Windows.Navigation.RequestNavigateEventArgs)
            If e Is Nothing Then Return

            Dim uriSite As String = e.Uri.ToString()
            Dim sInfo As ProcessStartInfo = New ProcessStartInfo(uriSite)

            Process.Start(sInfo)
        End Sub
    End Class
End Namespace
