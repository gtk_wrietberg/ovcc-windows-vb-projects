﻿Imports _04_sqlite_tut.sqlite_test.ViewModels

Namespace sqlite_test

    Partial Public Class MainWindow
        Inherits Window

        Public Sub New()
            InitializeComponent()
            Me.DataContext = New AppViewModel()
        End Sub

        Private Sub Hyperlink_RequestNavigate(ByVal sender As Object, ByVal e As System.Windows.Navigation.RequestNavigateEventArgs)
            If e Is Nothing Then Return

            Dim uriSite As String = e.Uri.ToString()
            Dim sInfo As ProcessStartInfo = New ProcessStartInfo(uriSite)

            Process.Start(sInfo)
        End Sub
    End Class
End Namespace
