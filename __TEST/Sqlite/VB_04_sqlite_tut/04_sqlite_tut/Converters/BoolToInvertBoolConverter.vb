Imports System.Globalization

Namespace sqlite_test.Converters

    Public Class BoolToInvertBoolConverter
        Implements IValueConverter

        Public Function Convert(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.Convert
            If TypeOf value Is Boolean = False Then Return Binding.DoNothing

            Return Not CBool(value)
        End Function

        Public Function ConvertBack(ByVal value As Object, ByVal targetType As Type, ByVal parameter As Object, ByVal culture As CultureInfo) As Object Implements IValueConverter.ConvertBack
            If TypeOf value Is Boolean = False Then Return Binding.DoNothing

            Return Not CBool(value)
        End Function
    End Class
End Namespace
