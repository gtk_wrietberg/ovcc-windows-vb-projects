
Imports _04_sqlite_tut.sqlite_test.Enums

Namespace sqlite_test.Models

#Region "constructors"
    Public Class MetaLocationModel

        ''' <summary>
        ''' Parameterized Class Constructor
        ''' </summary>
        Public Sub New(ByVal par_id As Integer,
                       ByVal par_iso As String,
                       ByVal par_localName As String,
                       ByVal par_type As LocationType,
                       ByVal par_in_Location As Long,
                       ByVal par_geo_lat As Double,
                       ByVal par_geo_lng As Double,
                       ByVal par_db_id As String)

            Me.New()

            ID = par_id
            ISO = par_iso
            LocalName = par_localName
            Type = par_type
            In_Location = par_in_Location
            Geo_lat = par_geo_lat
            Geo_lng = par_geo_lng
            DB_id = par_db_id
        End Sub

        ''' <summary>
        ''' Class Constructor
        ''' </summary>
        Public Sub New()
        End Sub
#End Region

#Region "properties"
        Public Property ID As Integer

        Public Property ISO As String

        Public Property LocalName As String

        Public Property Type As LocationType

        Public Property In_Location As Long

        Public Property Geo_lat As Double

        Public Property Geo_lng As Double

        Public Property DB_id As String
#End Region
    End Class
End Namespace
