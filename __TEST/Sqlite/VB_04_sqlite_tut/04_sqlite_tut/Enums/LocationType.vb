
Namespace sqlite_test.Enums

    Public Enum LocationType
        City = 0
        Region = 10
        Country = 100
    End Enum
End Namespace
