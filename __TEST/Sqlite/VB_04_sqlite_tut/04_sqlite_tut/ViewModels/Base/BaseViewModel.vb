Imports System.ComponentModel
Imports System.Linq.Expressions

Namespace sqlite_test.ViewModels.Base

    ''' <summary>
    ''' Every ViewModel class is required to implement the INotifyPropertyChanged
    ''' interface in order to tell WPF when a property changed (for instance, when
    ''' a method or setter is executed).
    ''' 
    ''' Therefore, the PropertyChanged method has to be called when data changes,
    ''' because the relevant properties may or may not be bound to GUI elements,
    ''' which in turn have to refresh their display.
    ''' 
    ''' The PropertyChanged method is to be called by the members and properties of
    ''' the class that derives from this class. Each call contains the name of the
    ''' property that has to be refreshed.
    ''' 
    ''' The BaseViewModel is derived from from System.Windows.DependencyObject to allow
    ''' resulting ViewModels the implementation of dependency properties. Dependency properties
    ''' in turn are useful when working with IValueConverter and ConverterParameters.
    ''' </summary>
    Public Class BaseViewModel '(Of ViewType As IView)
        Implements INotifyPropertyChanged

        ''' <summary>
        ''' Standard event handler of the <seealso cref="INotifyPropertyChanged"/> interface
        ''' </summary>
        Public Event PropertyChanged As PropertyChangedEventHandler _
        Implements INotifyPropertyChanged.PropertyChanged

        '    Private ReadOnly _view As ViewType

        '    Public Sub New(ByRef view As ViewType)
        '        Me._view = view
        '        Me.View.DataContext = Me
        '    End Sub
        '    Public ReadOnly Property View As ViewType
        '        Get
        '            Return Me._view
        '        End Get
        '    End Property

        ''' <summary>
        ''' Tell bound controls (via WPF binding) to refresh their display.
        ''' 
        ''' Sample call: Me.OnPropertyChanged("IsSelected");
        ''' where 'Me' is derived from <seealso cref="BaseViewModel"/>
        ''' and IsSelected is a property.
        ''' </summary>
        ''' <param name="propertyName">Name of property to refresh</param>
        Public Sub OnPropertyChanged(ByVal propertyName As String)
            RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
        End Sub

        ''' <summary>
        ''' Tell bound controls (via WPF binding) to refresh their display.
        ''' 
        ''' Sample call: Me.NotifyPropertyChanged(() => this.IsSelected)
        ''' where 'Me' is derived from <seealso cref="BaseViewModel"/>
        ''' and IsSelected is a property.
        ''' </summary>
        ''' <typeparam name="TProperty"></typeparam>
        ''' <param name="property"></param>
        Public Sub NotifyPropertyChanged(Of TProperty)(ByVal [property] As Expression(Of Func(Of TProperty)))
            Dim lambda = CType([property], LambdaExpression)
            Dim memberExpression As MemberExpression

            If TypeOf lambda.Body Is UnaryExpression Then
                Dim unaryExpression = CType(lambda.Body, UnaryExpression)

                memberExpression = CType(unaryExpression.Operand, MemberExpression)
            Else
                memberExpression = CType(lambda.Body, MemberExpression)
            End If

            Me.OnPropertyChanged(memberExpression.Member.Name)
        End Sub
    End Class

End Namespace