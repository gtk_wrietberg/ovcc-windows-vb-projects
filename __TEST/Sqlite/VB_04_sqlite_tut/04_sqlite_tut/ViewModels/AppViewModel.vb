
Imports System.Collections.Generic
Imports System.Data.SQLite
Imports System.IO
Imports System.Threading.Tasks
Imports System.Windows.Input
Imports System.Xml.Serialization
Imports _04_sqlite_tut.sqlite_test.Models
Imports _04_sqlite_tut.sqlite_test.ViewModels.Base

Namespace sqlite_test.ViewModels

    Public Class AppViewModel
        Inherits Base.BaseViewModel

        Private _IsProcessing As Boolean
        Private _MessageText As String
        Private _InsertDataCommand As ICommand
        Private _ConvertDataCommand As ICommand

        Public Property IsProcessing As Boolean
            Get
                Return _IsProcessing
            End Get

            Set(ByVal value As Boolean)
                If _IsProcessing <> value Then
                    _IsProcessing = value
                    NotifyPropertyChanged(Function() IsProcessing)
                End If
            End Set
        End Property

        Public Property MessageText As String
            Get
                Return _MessageText
            End Get

            Set(ByVal value As String)
                If _MessageText <> value Then
                    _MessageText = value
                    NotifyPropertyChanged(Function() MessageText)
                End If
            End Set
        End Property

        Public ReadOnly Property InsertDataCommand As ICommand
            Get
                If _InsertDataCommand Is Nothing Then
                    _InsertDataCommand = New RelayCommand(Of Object)(Sub(p)
                                                                         Task.Run(Sub()
                                                                                      IsProcessing = True
                                                                                      Try
                                                                                          InsertRecords()
                                                                                      Catch exp As System.Exception
                                                                                          MessageText += vbLf & vbLf & "AN ERROR OCURRED: " & exp.Message & vbLf
                                                                                      Finally
                                                                                          IsProcessing = False
                                                                                      End Try
                                                                                  End Sub)
                                                                     End Sub)
                End If

                Return _InsertDataCommand
            End Get
        End Property

        Public ReadOnly Property ConvertDataCommand As ICommand
            Get
                If _ConvertDataCommand Is Nothing Then
                    _ConvertDataCommand = New RelayCommand(Of Object)(Sub(p)
                                                                          Dim iLines As Integer = 0
                                                                          Task.Run(Sub()
                                                                                       IsProcessing = True
                                                                                       Try
                                                                                           MessageText += vbLf & vbLf & vbLf & " Converting to Xml... please wait..." & vbLf

                                                                                           ' Output resulting information into an XML formatted file
                                                                                           WriteCountryXmlModels(".\Resources\countries.xml")
                                                                                           WriteRegionXmlModels(".\Resources\regions.xml")
                                                                                           WriteCityXmlModels(".\Resources\cities.xml")

                                                                                           MessageText += "Xml files converted." & vbLf
                                                                                           MessageText += "(Check the ./bin/Resources/ folder below the executable)" & vbLf

                                                                                       Catch exp As System.Exception
                                                                                           MessageText += vbLf & vbLf & "AN ERROR OCURRED: " & exp.Message & vbLf
                                                                                           MessageText += vbLf & vbLf & "At line:" & iLines & vbLf
                                                                                       Finally
                                                                                           IsProcessing = False
                                                                                       End Try
                                                                                   End Sub)
                                                                      End Sub)
                End If

                Return _ConvertDataCommand
            End Get
        End Property

#Region "InsertSQLiteRecord"
        Private Sub InsertRecords()
            Dim db As SQLiteDatabase = New SQLiteDatabase()
            Dim query As String = String.Empty
            Dim queryString As String = String.Empty
            Dim iLines As Integer = 0

            Try
                db.OpenConnection()
                If db.ConnectionState = False Then
                    MessageText = "ERROR: Cannot open Database connectiton." & vbLf & db.Status
                End If

                MessageText += db.Status + vbLf
                Dim createQuery As String = "CREATE TABLE IF NOT EXISTS
                    [meta_location] (
                    [id]           INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    [iso]          VARCHAR(50)   DEFAULT NULL,
                    [local_name]   varchar(255)  DEFAULT NULL,
                    [type]         char(2)       DEFAULT NULL,
                    [in_location]  unsigned int  DEFAULT NULL,
                    [geo_lat]      double(18,11) DEFAULT NULL,
                    [geo_lng]      double(18,11) DEFAULT NULL,
                    [db_id]        varchar(50)   DEFAULT NULL)"

                Using cmd As SQLiteCommand = New SQLiteCommand(db.Connection)
                    cmd.CommandText = createQuery
                    cmd.ExecuteNonQuery()
                End Using

                Dim cmdDeleteTable = New SQLiteCommand("delete from meta_location", db.Connection)
                cmdDeleteTable.ExecuteNonQuery()

                Dim lines = System.IO.File.ReadLines(".\Resources\lokasyon.txt")

                query = "INSERT INTO meta_location ([id], [iso], [local_name], [type], [in_location], [geo_lat], [geo_lng], [db_id])VALUES"

                ' https://www.jokecamp.com/blog/make-your-sqlite-bulk-inserts-very-fast-in-c/
                Using transaction = db.Connection.BeginTransaction()

                    ' Write data out to database
                    For Each line In lines                 ' Ignore empty lines
                        If line.Trim().Length > 0 Then

                            ' Get rid of comma at the end of the line
                            Dim valueLine = line.Replace("),", ")")

                            ' Adjust single quote escape from \' to SQLite ''
                            valueLine = valueLine.Replace("\'", "''")
                            valueLine = valueLine.Replace("\r", "")     ' Get ride of these
                            valueLine = valueLine.Replace("\n", "")

                            queryString = query & valueLine

                            Dim cmdInsert = New SQLiteCommand(queryString, db.Connection)
                            cmdInsert.ExecuteNonQuery()
                            iLines += 1
                        End If
                    Next

                    transaction.Commit()
                End Using

                MessageText += vbLf & vbLf & "Lines Inserted: " & iLines & vbLf
                queryString = "SELECT [type], count(*) as [count]" & " from meta_location" & " group by [type]"

                Dim sqlite_cmd = db.Connection.CreateCommand()

                sqlite_cmd.CommandText = queryString

                ' Now the SQLiteCommand object can give us a DataReader-Object:
                Dim sqlite_datareader As SQLiteDataReader = sqlite_cmd.ExecuteReader()

                ' The SQLiteDataReader allows us to run through the result lines:
                While sqlite_datareader.Read()
                    ' Print out the content of the text field:
                    'System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");

                    Dim textType As String = sqlite_datareader.GetString(0)
                    Dim intCount As Int32 = sqlite_datareader.GetValue(1)
                    MessageText += "type: '" & textType & "', count: " & intCount & " " & vbLf
                End While

                ' Output resulting information into cs formatted text file
                ' These files could be included in another VS project to solve
                ' another problem... see Models/Generate.cs as an example.
                WriteCountryModels(db, ".\Resources\countries.cs")
                WriteRegionModels(db, ".\Resources\regions.cs")
                WriteCityModels(db, ".\Resources\cities.cs")

            Catch exp As System.Exception
                MessageText += vbLf & vbLf & "AN ERROR OCURRED: " & exp.Message & vbLf
                MessageText += vbLf & vbLf & "At line:" & iLines & vbLf
                MessageText += "Query was: " & queryString & vbLf
            Finally
                db.CloseConnection()
            End Try
        End Sub

        ''' <summary>
        ''' Writes the contents of all Country objects into a text file.
        ''' </summary>
        ''' <param name="db"></param>
        ''' <param name="outputFile"></param>
        Private Sub WriteCountryModels(ByVal db As SQLiteDatabase, ByVal outputFile As String)
            Dim queryString As String = "SELECT [id], [iso], [local_name], [geo_lat], [geo_lng], [db_id]" & " from meta_location" & " where type = 'CO' order by [local_name]"
            Dim iLines As Integer = 0
            Try
                ' Finding all countries
                Dim countries As List(Of String) = New List(Of String)()
                Dim sqlite_cmd = db.Connection.CreateCommand()
                sqlite_cmd.CommandText = queryString

                ' Now the SQLiteCommand object can give us a DataReader-Object:
                Dim sqlite_datareader As SQLiteDataReader = sqlite_cmd.ExecuteReader()

                ' The SQLiteDataReader allows us to run through the result lines:
                While sqlite_datareader.Read()

                    Dim value3 As String
                    Dim value4 As String

                    If (sqlite_datareader.GetValue(3).Equals(System.DBNull.Value)) Then
                        value3 = "default(double)"
                    Else
                        value3 = String.Format("{0}", sqlite_datareader.GetValue(3))
                    End If

                    If (sqlite_datareader.GetValue(4).Equals(System.DBNull.Value)) Then
                        value4 = "default(double)"
                    Else
                        value4 = String.Format("{0}", sqlite_datareader.GetValue(4))
                    End If

                    Dim output As String = String.Format("countries.Add(new MetaLocationModel({0}, ""{1}"", ""{2}"", LocationType.Country, -1, {3}, {4}, ""{5}""));",
                                                         sqlite_datareader.GetValue(0),
                                                         sqlite_datareader.GetValue(1),
                                                         sqlite_datareader.GetValue(2),
                                                         value3,
                                                         value4,
                                                         sqlite_datareader.GetValue(5))

                    countries.Add(output)
                End While

                System.IO.File.WriteAllLines(outputFile, countries)

            Catch exp As System.Exception
                MessageText += vbLf & vbLf & "ERROR in WriteCountryModels: " & exp.Message & vbLf
                MessageText += vbLf & vbLf & "At line:" & iLines & vbLf
                MessageText += "Query was: " & queryString & vbLf
            End Try
        End Sub

        ''' <summary>
        ''' Writes the contents of all Region objects into a text file.
        ''' </summary>
        ''' <param name="db"></param>
        ''' <param name="outputFile"></param>
        Private Sub WriteRegionModels(ByVal db As SQLiteDatabase, ByVal outputFile As String)

            ' [id], [iso], [local_name], [type], [in_location], [geo_lat], [geo_lng], [db_id]
            Dim queryString As String = "SELECT [id], [iso], [local_name], [in_location], [geo_lat], [geo_lng], [db_id]" & " from meta_location" & " where type = 'RE' order by [local_name]"

            Dim iLines As Integer = 0
            Try
                ' Finding all countries
                Dim countries As List(Of String) = New List(Of String)()
                Dim sqlite_cmd = db.Connection.CreateCommand()

                sqlite_cmd.CommandText = queryString

                ' Now the SQLiteCommand object can give us a DataReader-Object:
                Dim sqlite_datareader As SQLiteDataReader = sqlite_cmd.ExecuteReader()

                ' The SQLiteDataReader allows us to run through the result lines:
                While sqlite_datareader.Read()
                    Dim value4 As String
                    Dim value5 As String

                    If (sqlite_datareader.GetValue(4).Equals(System.DBNull.Value)) Then
                        value4 = "default(double)"
                    Else
                        value4 = String.Format("{0}", sqlite_datareader.GetValue(4))
                    End If

                    If (sqlite_datareader.GetValue(5).Equals(System.DBNull.Value)) Then
                        value5 = "default(double)"
                    Else
                        value5 = String.Format("{0}", sqlite_datareader.GetValue(5))
                    End If

                    Dim output As String = String.Format("countries.Add(new MetaLocationModel({0}, ""{1}"", ""{2}"", {3}, {4}, {5}, {6}, ""{7}""));",
                                                         sqlite_datareader.GetValue(0),
                                                         sqlite_datareader.GetValue(1),
                                                         sqlite_datareader.GetValue(2),
                                                         "LocationType.Region",
                                                         sqlite_datareader.GetValue(3),
                                                         value4,
                                                         value5,
                                                         sqlite_datareader.GetValue(6))

                    countries.Add(output)
                End While

                System.IO.File.WriteAllLines(outputFile, countries)

            Catch exp As System.Exception
                MessageText += vbLf & vbLf & "ERROR in WriteRegionModels: " & exp.Message & vbLf
                MessageText += vbLf & vbLf & "At line:" & iLines & vbLf
                MessageText += "Query was: " & queryString & vbLf
            End Try
        End Sub

        ''' <summary>
        ''' Writes the contents of all City objects into a text file.
        ''' </summary>
        ''' <param name="db"></param>
        ''' <param name="outputFile"></param>
        Private Sub WriteCityModels(ByVal db As SQLiteDatabase, ByVal outputFile As String)
            ' [id], [iso], [local_name], [type], [in_location], [geo_lat], [geo_lng], [db_id]
            Dim queryString As String = "SELECT [id], [iso], [local_name], [in_location], [geo_lat], [geo_lng], [db_id]" & " from meta_location" & " where type = 'CI' order by [local_name]"

            Dim iLines As Integer = 0
            Try
                ' Finding all countries
                Dim countries As List(Of String) = New List(Of String)()

                Dim sqlite_cmd = db.Connection.CreateCommand()
                sqlite_cmd.CommandText = queryString

                ' Now the SQLiteCommand object can give us a DataReader-Object:
                Dim sqlite_datareader As SQLiteDataReader = sqlite_cmd.ExecuteReader()

                While sqlite_datareader.Read()
                    Dim value4 As String
                    Dim value5 As String

                    If (sqlite_datareader.GetValue(4).Equals(System.DBNull.Value)) Then
                        value4 = "default(double)"
                    Else
                        value4 = String.Format("{0}", sqlite_datareader.GetValue(4))
                    End If

                    If (sqlite_datareader.GetValue(5).Equals(System.DBNull.Value)) Then
                        value5 = "default(double)"
                    Else
                        value5 = String.Format("{0}", sqlite_datareader.GetValue(5))
                    End If

                    Dim output As String = String.Format("cities.Add(new MetaLocationModel({0}, ""{1}"", ""{2}"", {3}, {4}, {5}, {6}, ""{7}""));",
                                                         sqlite_datareader.GetValue(0), sqlite_datareader.GetValue(1),
                                                         sqlite_datareader.GetValue(2),
                                                         "LocationType.City",
                                                         sqlite_datareader.GetValue(3),
                                                         value4,
                                                         value5,
                                                         sqlite_datareader.GetValue(6))

                    countries.Add(output)
                End While

                System.IO.File.WriteAllLines(outputFile, countries)
            Catch exp As System.Exception
                MessageText += vbLf & vbLf & "ERROR in WriteCityModels: " & exp.Message & vbLf
                MessageText += vbLf & vbLf & "At line:" & iLines & vbLf
                MessageText += "Query was: " & queryString & vbLf
            End Try
        End Sub
#End Region

#Region "WriteXmlFiles"
        Private Sub WriteCityXmlModels(ByVal filename As String)
            Dim items = New List(Of MetaLocationModel)()

            items = GenerateCity.Items(items)
            Using sw As StreamWriter = New StreamWriter(filename)
                Using writer As TextWriter = TextWriter.Synchronized(sw)
                    Dim serialization As XmlSerializer = New XmlSerializer(GetType(List(Of MetaLocationModel)))

                    serialization.Serialize(writer, items)
                End Using
            End Using
        End Sub

        Private Sub WriteRegionXmlModels(ByVal filename As String)
            Dim items = New List(Of MetaLocationModel)()

            items = Generate.Regions(items)
            Using sw As StreamWriter = New StreamWriter(filename)
                Using writer As TextWriter = TextWriter.Synchronized(sw)
                    Dim serialization As XmlSerializer = New XmlSerializer(GetType(List(Of MetaLocationModel)))

                    serialization.Serialize(writer, items)
                End Using
            End Using
        End Sub

        Private Sub WriteCountryXmlModels(ByVal filename As String)
            Dim items As List(Of MetaLocationModel) = New List(Of MetaLocationModel)()

            items = Generate.Countries(items)
            Using sw As StreamWriter = New StreamWriter(filename)
                Using writer As TextWriter = TextWriter.Synchronized(sw)
                    Dim serialization As XmlSerializer = New XmlSerializer(GetType(List(Of MetaLocationModel)))

                    serialization.Serialize(writer, items)
                End Using
            End Using
        End Sub
#End Region

    End Class
End Namespace
