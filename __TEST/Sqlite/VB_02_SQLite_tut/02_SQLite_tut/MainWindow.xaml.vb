﻿
Imports System.Data.SQLite
Imports _02_SQLite_tut.sqlite_test.Models

Namespace sqlite_test

    ''' <summary>
    ''' Interaction logic for MainWindow.xaml
    ''' </summary>
    Partial Public Class MainWindow
        Inherits Window

        Public Sub New()
            InitializeComponent()
            cmbJounralMode.SelectedIndex = 0
        End Sub

        Private Sub Button_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)

            Dim DB As SQLiteDatabase = New SQLiteDatabase()

            Try
                ' Overwrite previously created database if it was already existing
                DB.OpenConnection((If(chkOverWrite.IsChecked = True, True, False)))

                If chkboxJournalMod.IsChecked = True Then
                    ' Setting the journal mode like this will actually persist in the database
                    ' in some cases (WAL) and (the default journal 'DELETE' mode will be changed in effect)
                    ' while it will not be persisted in other cases (Truncate)
                    DB.JournalMode(CType(cmbJounralMode.SelectedItem, JournalMode))
                End If

                Dim version As Long = DB.UserVersion()      ' Get DB User Version for display
                tblUserVersion.Text = version.ToString()

                tblJournalMode.Text = DB.JournalMode()      ' Get DB Journal Mode for display
                If chkOverWrite.IsChecked = True Then OutputTextBox.Text = String.Empty

                If DB.ConnectionState = False Then
                    OutputTextBox.Text = "ERROR: Cannot open Database conneciton." & vbLf & DB.Status
                End If

                OutputTextBox.Text += DB.Status + vbLf

                Dim createQuery As String = "CREATE TABLE IF NOT EXISTS
                    [albums] (
                    [Id]     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    [title]  TEXT NULL,
                    [artist] TEXT NULL)"

                Using cmd As SQLiteCommand = New SQLiteCommand(DB.Connection)
                    cmd.CommandText = createQuery
                    cmd.ExecuteNonQuery()
                End Using

                ' Insert into Database
                Dim query As String = "INSERT INTO albums ('title', 'artist')VALUES(@title, @artist)"
                Using cmd As SQLiteCommand = New SQLiteCommand(query, DB.Connection)

                    cmd.Parameters.AddWithValue("@title", "Trapsoul")
                    cmd.Parameters.AddWithValue("@artist", "Bryson Tiller")
                    Dim result = cmd.ExecuteNonQuery()

                    cmd.Parameters.AddWithValue("@title", "Damn")
                    cmd.Parameters.AddWithValue("@artist", "Kendrick Lamar")
                    result += cmd.ExecuteNonQuery()

                    cmd.Parameters.AddWithValue("@title", "I Told You")
                    cmd.Parameters.AddWithValue("@artist", "Tony Lamez")
                    result += cmd.ExecuteNonQuery()

                    OutputTextBox.Text += String.Format("Rows Added: {0}", result) & vbLf
                End Using

                ' Select Result from Database
                OutputTextBox.Text += vbLf & vbLf & "Reading back result from database:" & vbLf
                query = "select * from albums"

                Using cmd As SQLiteCommand = New SQLiteCommand(query, DB.Connection)
                    Using selectResult As SQLiteDataReader = cmd.ExecuteReader()
                        If selectResult.HasRows = True Then
                            While selectResult.Read()
                                OutputTextBox.Text += String.Format("Album: {0} - Artist {1}", selectResult("title"), selectResult("artist")) & vbLf
                            End While
                        End If
                    End Using
                End Using

                version = DB.UserVersionIncrease()
                tblUserVersion.Text = version.ToString()
            Catch exp As System.Exception
                OutputTextBox.Text += String.Format(vbLf & vbLf & "ERROR: {0}" & vbLf, DB.Status)
                OutputTextBox.Text += exp.StackTrace
            Finally
                DB.CloseConnection()
            End Try
        End Sub

        Private Sub Hyperlink_RequestNavigate(ByVal sender As Object,
                                              ByVal e As System.Windows.Navigation.RequestNavigateEventArgs)
            If e Is Nothing Then Return

            Dim uriSite As String = e.Uri.ToString()
            Dim sInfo As ProcessStartInfo = New ProcessStartInfo(uriSite)

            Process.Start(sInfo)
        End Sub
    End Class
End Namespace
