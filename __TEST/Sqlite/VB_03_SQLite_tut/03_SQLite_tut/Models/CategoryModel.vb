
Namespace sqlite_test.Models

    Public Class CategoryModel

        Public Sub New(ByVal _id As Integer, ByVal _name As String, ByVal _parent As Integer)
            ID = _id
            Name = _name
            Parent = _parent
        End Sub

        Public Sub New(ByVal _id As Integer, ByVal _name As String)
            ID = _id
            Name = _name
        End Sub

        Protected Sub New()
            ID = Nothing
            Name = Nothing
        End Sub

        Public Property ID As Integer

        Public Property Name As String

        Public Property Parent As Integer
    End Class
End Namespace
