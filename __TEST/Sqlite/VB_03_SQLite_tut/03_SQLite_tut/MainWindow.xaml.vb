﻿
Imports _03_SQLite_tut.sqlite_test.Models
Imports System.Data.SQLite

Namespace sqlite_test

    Partial Public Class MainWindow
        Inherits Window

        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub Button_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)
            Dim DB As SQLiteDatabase = New SQLiteDatabase()

            OutputTextBox.Text = String.Empty

            Try
                DB.OpenConnection()
                If DB.ConnectionState = False Then
                    OutputTextBox.Text = "ERROR: Cannot open Database connectiton." & vbLf & DB.Status
                End If

                OutputTextBox.Text += DB.Status + vbLf
                Dim createQuery As String = "CREATE TABLE IF NOT EXISTS
                    [category] (
                    [category_id]  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                    [name]         VARCHAR(20) NULL,
                    [parent]       INT     NULL)"

                Using cmd As SQLiteCommand = New SQLiteCommand(DB.Connection)
                    cmd.CommandText = createQuery
                    cmd.ExecuteNonQuery()
                End Using

                Dim values As List(Of CategoryModel) = New List(Of CategoryModel)()

                values.Add(New CategoryModel(1, "ELECTRONICS"))
                values.Add(New CategoryModel(2, "TELEVISIONS", 1))
                values.Add(New CategoryModel(3, "TUBE", 2))
                values.Add(New CategoryModel(4, "LCD", 2))
                values.Add(New CategoryModel(5, "PLASMA", 2))
                values.Add(New CategoryModel(6, "PORTABLE ELECTRONICS", 1))
                values.Add(New CategoryModel(7, "MP3 PLAYERS", 6))
                values.Add(New CategoryModel(8, "FLASH", 7))
                values.Add(New CategoryModel(9, "CD PLAYERS", 6))
                values.Add(New CategoryModel(10, "2 WAY RADIOS", 6))

                Dim cmdDeleteRecords = New SQLiteCommand("delete from category", DB.Connection)

                cmdDeleteRecords.ExecuteNonQuery()
                Dim query As String = "INSERT INTO category ([category_id],[name],[parent])VALUES(@category_id,@name,@parent)"
                Using cmd As SQLiteCommand = New SQLiteCommand(query, DB.Connection)
                    Dim result As Integer = 0
                    Using transaction = cmd.Connection.BeginTransaction()
                        For Each item In values
                            cmd.Parameters.AddWithValue("@category_id", item.ID)
                            cmd.Parameters.AddWithValue("@name", item.Name)
                            cmd.Parameters.AddWithValue("@parent", item.Parent)
                            result += cmd.ExecuteNonQuery()
                        Next

                        transaction.Commit()
                    End Using

                    OutputTextBox.Text += String.Format("Rows Added: {0}", result) & vbLf
                End Using

                OutputTextBox.Text += vbLf & vbLf & "Reading back result from database:" & vbLf
                query = "SELECT * FROM category ORDER BY category_id"

                Using cmd As SQLiteCommand = New SQLiteCommand(query, DB.Connection)
                    OutputTextBox.Text += String.Format("{0,10} | {1,30} | {2}", "category_id", "name", "parent") & vbLf
                    Using selectResult As SQLiteDataReader = cmd.ExecuteReader()
                        If selectResult.HasRows = True Then
                            While selectResult.Read()
                                OutputTextBox.Text += String.Format("{0, 11} | {1, 30} | {2}", selectResult("category_id"), selectResult("name"), selectResult("parent")) & vbLf
                            End While
                        End If
                    End Using
                End Using

            Catch exp As System.Exception
                OutputTextBox.Text += vbLf & vbLf & "AN ERROR OCURRED: " & exp.Message & vbLf
            Finally
                DB.CloseConnection()
            End Try
        End Sub

        Private Sub Hyperlink_RequestNavigate(ByVal sender As Object,
                                              ByVal e As System.Windows.Navigation.RequestNavigateEventArgs)
            If e Is Nothing Then Return

            Dim uriSite As String = e.Uri.ToString()
            Dim sInfo As ProcessStartInfo = New ProcessStartInfo(uriSite)

            Process.Start(sInfo)
        End Sub
    End Class
End Namespace
