﻿
Imports System.Data.SQLite

Namespace sqlite_test


    ''' <summary>
    ''' Interaction logic for MainWindow.xaml
    ''' </summary>
    Partial Public Class MainWindow
        Inherits Window

        Public Sub New()
            InitializeComponent()
        End Sub

        Private Sub Button_Click(ByVal sender As Object,
                                 ByVal e As RoutedEventArgs)
            ' Source: http://adodotnetsqlite.sourceforge.net/documentation/csharp_example.php
            ' Sample will only work once since it cannot handle the case when database is already created
            ' Locate the file 'database.db' in bin folder and delete if you want to run more than once

            ' [snip] - As VB is purely object-oriented the following lines must be put into a class:

            ' We use these three SQLite objects:
            Dim sqlite_conn As SQLiteConnection
            Dim sqlite_cmd As SQLiteCommand
            Dim sqlite_datareader As SQLiteDataReader

            ' create a new database connection:
            sqlite_conn = New SQLiteConnection("Data Source=database.sqlite;Version=3;New=True;Compress=True;")

            ' open the connection:
            sqlite_conn.Open()

            ' create a new SQL command:
            sqlite_cmd = sqlite_conn.CreateCommand()

            ' Let the SQLiteCommand object know our SQL-Query:
            sqlite_cmd.CommandText = "CREATE TABLE test (id integer primary key, text varchar(100));"

            ' Now lets execute the SQL ;D
            sqlite_cmd.ExecuteNonQuery()

            ' Lets insert something into our new table:
            sqlite_cmd.CommandText = "INSERT INTO test (id, text) VALUES (1, 'Test Text 1');"

            ' And execute this again ;D
            sqlite_cmd.ExecuteNonQuery()

            ' ...and inserting another line:
            sqlite_cmd.CommandText = "INSERT INTO test (id, text) VALUES (2, 'Test Text 2');"

            ' And execute this again ;D
            sqlite_cmd.ExecuteNonQuery()

            ' But how do we read something out of our table ?
            ' First lets build a SQL-Query again:
            sqlite_cmd.CommandText = "SELECT * FROM test"

            ' Now the SQLiteCommand object can give us a DataReader-Object:
            sqlite_datareader = sqlite_cmd.ExecuteReader()

            ' The SQLiteDataReader allows us to run through the result lines:
            While sqlite_datareader.Read()
                ' Print out the content of the text field:
                'System.Console.WriteLine("DEBUG Output: '" + sqlite_datareader["text"] + "'");

                Dim idReader As Object = sqlite_datareader.GetValue(0)
                Dim textReader As String = sqlite_datareader.GetString(1)

                OutputTextBox.Text += idReader & " '" & textReader & "' " & vbLf
            End While

            sqlite_conn.Close()
        End Sub

        Private Sub Hyperlink_RequestNavigate(ByVal sender As Object, ByVal e As System.Windows.Navigation.RequestNavigateEventArgs)
            If e Is Nothing Then Return

            Dim uriSite As String = e.Uri.ToString()
            Dim sInfo As ProcessStartInfo = New ProcessStartInfo(uriSite)

            Process.Start(sInfo)
        End Sub
    End Class
End Namespace
