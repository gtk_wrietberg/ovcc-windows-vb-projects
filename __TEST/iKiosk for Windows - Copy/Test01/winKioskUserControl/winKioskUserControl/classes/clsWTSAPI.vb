Imports System.Runtime.InteropServices

Public Class clsWTSAPI
    Private Class clsSessionInfoContainer
        Private Class clsSessionInfo
            Private mSessionId As Integer
            Private mUsername As String

            Public Property SessionId() As Integer
                Get
                    Return mSessionId
                End Get
                Set(ByVal value As Integer)
                    mSessionId = value
                End Set
            End Property

            Public Property Username() As String
                Get
                    Return mUsername
                End Get
                Set(ByVal value As String)
                    mUsername = value
                End Set
            End Property
        End Class

        Private mList As New List(Of clsSessionInfo)
        Private mCounter As Integer = 0

        Public Sub New()
            mList = New List(Of clsSessionInfo)
            mCounter = 0
        End Sub

        Public Sub Add(ByVal SessionId As Integer, ByVal UserName As String)
            If Not Contains(SessionId) Then
                Dim newItem As New clsSessionInfo
                newItem.SessionId = SessionId
                newItem.Username = UserName

                mList.Add(newItem)
            End If
        End Sub

        Public Function Contains(ByVal SessionId As Integer) As Boolean
            For i As Integer = 0 To mList.Count - 1
                If mList(i).SessionId = SessionId Then
                    Return True
                End If
            Next

            Return False
        End Function

        Public Sub Reset()
            mCounter = 0
        End Sub

        Public Function GetNext(ByRef SessionId As Integer, ByRef UserName As String) As Boolean
            If mCounter < 0 Then mCounter = 0

            If mCounter < mList.Count Then
                SessionId = mList(mCounter).SessionId
                UserName = mList(mCounter).Username
            End If

            mCounter += 1
            Return (mCounter <= mList.Count)
        End Function
    End Class

    '***************************************************************************************************************

    Private Enum WTS_CONNECTSTATE_CLASS
        WTSActive
        WTSConnected
        WTSConnectQuery
        WTSShadow
        WTSDisconnected
        WTSIdle
        WTSListen
        WTSReset
        WTSDown
        WTSInit
    End Enum

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Private Structure WTS_SESSION_INFO
        Dim SessionID As Int32 'DWORD integer
        Dim pWinStationName As String ' integer LPTSTR - Pointer to a null-terminated string containing the name of the WinStation for this session
        Dim State As WTS_CONNECTSTATE_CLASS
    End Structure

    Friend Structure strSessionsInfo
        Dim SessionID As Integer
        Dim StationName As String
        Dim ConnectionState As String
    End Structure

    Private Enum WTS_INFO_CLASS
        WTSInitialProgram
        WTSApplicationName
        WTSWorkingDirectory
        WTSOEMId
        WTSSessionId
        WTSUserName
        WTSWinStationName
        WTSDomainName
        WTSConnectState
        WTSClientBuildNumber
        WTSClientName
        WTSClientDirectory
        WTSClientProductId
        WTSClientHardwareId
        WTSClientAddress
        WTSClientDisplay
        WTSClientProtocolType
        WTSIdleTime
        WTSLogonTime
        WTSIncomingBytes
        WTSOutgoingBytes
        WTSIncomingFrames
        WTSOutgoingFrames
    End Enum
    'Structure for TS Client IP Address
    <StructLayout(LayoutKind.Sequential)> _
    Private Structure _WTS_CLIENT_ADDRESS
        Public AddressFamily As Integer
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=20)> _
        Public Address As Byte()
    End Structure
    'Structure for TS Client Information
    Friend Structure WTS_CLIENT_INFO
        Public WTSStatus As Boolean
        Public WTSUserName As String
        Public WTSStationName As String
        Public WTSDomainName As String
        Public WTSClientName As String
        Public AddressFamily As Integer
        Public Address As Byte()
    End Structure

    'Function for TS Session Information excluding Client IP address
    Private Declare Function WTSQuerySessionInformation Lib "WtsApi32.dll" Alias "WTSQuerySessionInformationW" (ByVal hServer As Int32, _
    ByVal SessionId As Int32, ByVal WTSInfoClass As Int32, <MarshalAs(UnmanagedType.LPWStr)> ByRef ppBuffer As String, ByRef pCount As Int32) As Boolean

    'Function for TS Client IP Address
    Private Declare Function WTSQuerySessionInformation2 Lib "WtsApi32.dll" Alias "WTSQuerySessionInformationW" (ByVal hServer As Int32, _
      ByVal SessionId As Int32, ByVal WTSInfoClass As Int32, ByRef ppBuffer As IntPtr, ByRef pCount As Int32) As Boolean

    Private Declare Function GetCurrentProcessId Lib "Kernel32.dll" Alias "GetCurrentProcessId" () As Int32
    Private Declare Function ProcessIdToSessionId Lib "Kernel32.dll" Alias "ProcessIdToSessionId" (ByVal processID As Int32, ByRef sessionID As Int32) As Boolean
    Private Declare Function WTSGetActiveConsoleSessionId Lib "Kernel32.dll" Alias "WTSGetActiveConsoleSessionId" () As Int32


    <DllImport("wtsapi32.dll", _
    bestfitmapping:=True, _
    CallingConvention:=CallingConvention.StdCall, _
    CharSet:=CharSet.Auto, _
    EntryPoint:="WTSEnumerateSessions", _
    setlasterror:=True, _
    ThrowOnUnmappableChar:=True)> _
    Private Shared Function WTSEnumerateSessions( _
    ByVal hServer As IntPtr, _
    <MarshalAs(UnmanagedType.U4)> _
    ByVal Reserved As Int32, _
    <MarshalAs(UnmanagedType.U4)> _
    ByVal Vesrion As Int32, _
    ByRef ppSessionInfo As IntPtr, _
    <MarshalAs(UnmanagedType.U4)> _
    ByRef pCount As Int32) As Int32
    End Function

    <DllImport("wtsapi32.dll")> _
    Private Shared Sub WTSFreeMemory(ByVal pMemory As IntPtr)
    End Sub

    <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
     Private Shared Function WTSOpenServer(ByVal pServerName As String) As IntPtr
    End Function

    <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
     Private Shared Sub WTSCloseServer(ByVal hServer As IntPtr)
    End Sub

    '---
    Private mSessionInfo As New clsSessionInfoContainer

    Public Sub New()
        mSessionInfo = New clsSessionInfoContainer
    End Sub

    Private Function LoadSessions(ByVal ServerName As String) As Boolean
        mSessionInfo = New clsSessionInfoContainer


        Dim ptrOpenedServer As IntPtr

        Try
            ptrOpenedServer = WTSOpenServer(ServerName)
            If ptrOpenedServer = vbNull Then
                'MessageBox.Show("Terminal Services not running on : " & ServerName)
                LoadSessions = False
                Exit Function
            End If

            Dim FRetVal As Int32
            Dim ppSessionInfo As IntPtr = IntPtr.Zero
            Dim Count As Int32 = 0
            Try
                FRetVal = WTSEnumerateSessions(ptrOpenedServer, 0, 1, ppSessionInfo, Count)
                If FRetVal <> 0 Then
                    Dim sessionInfo() As WTS_SESSION_INFO = New WTS_SESSION_INFO(Count) {}
                    Dim i As Integer
                    Dim session_ptr As System.IntPtr
                    For i = 0 To Count - 1
                        session_ptr = ppSessionInfo.ToInt32() + (i * Marshal.SizeOf(sessionInfo(i)))
                        sessionInfo(i) = CType(Marshal.PtrToStructure(session_ptr, GetType(WTS_SESSION_INFO)), WTS_SESSION_INFO)
                    Next
                    WTSFreeMemory(ppSessionInfo)
                    Dim tmpArr(sessionInfo.GetUpperBound(0)) As strSessionsInfo

                    For i = 0 To tmpArr.GetUpperBound(0)
                        mSessionInfo.Add(sessionInfo(i).SessionID, GetSessionUsername(ptrOpenedServer, sessionInfo(i).SessionID))

                        tmpArr(i).SessionID = sessionInfo(i).SessionID
                        tmpArr(i).StationName = sessionInfo(i).pWinStationName
                        tmpArr(i).ConnectionState = GetConnectionState(sessionInfo(i).State)
                    Next
                    ReDim sessionInfo(-1)
                Else
                    Throw New ApplicationException("No data returned")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message & vbCrLf & System.Runtime.InteropServices.Marshal.GetLastWin32Error)
            End Try
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Exit Function
        Finally
        End Try

        WTSCloseServer(ptrOpenedServer)
        Return True
    End Function

    Private Function GetSessionUsername(ByVal ptrOpenedServer As IntPtr, ByVal SessionID As Integer) As String
        Dim username As String = ""
        Dim username_len As Integer
        If WTSQuerySessionInformation(ptrOpenedServer, SessionID, WTS_INFO_CLASS.WTSUserName, username, username_len) = True Then

        End If

        Return username
    End Function

    Private Function GetConnectionState(ByVal State As WTS_CONNECTSTATE_CLASS) As String
        Dim RetVal As String
        Select Case State
            Case WTS_CONNECTSTATE_CLASS.WTSActive
                RetVal = "Active"
            Case WTS_CONNECTSTATE_CLASS.WTSConnected
                RetVal = "Connected"
            Case WTS_CONNECTSTATE_CLASS.WTSConnectQuery
                RetVal = "Query"
            Case WTS_CONNECTSTATE_CLASS.WTSDisconnected
                RetVal = "Disconnected"
            Case WTS_CONNECTSTATE_CLASS.WTSDown
                RetVal = "Down"
            Case WTS_CONNECTSTATE_CLASS.WTSIdle
                RetVal = "Idle"
            Case WTS_CONNECTSTATE_CLASS.WTSInit
                RetVal = "Initializing."
            Case WTS_CONNECTSTATE_CLASS.WTSListen
                RetVal = "Listen"
            Case WTS_CONNECTSTATE_CLASS.WTSReset
                RetVal = "reset"
            Case WTS_CONNECTSTATE_CLASS.WTSShadow
                RetVal = "Shadowing"
            Case Else
                RetVal = "Unknown connect state"
        End Select
        Return RetVal
    End Function

    Public Function FindSessionIdByUsername(ByVal UserName As String) As Integer
        LoadSessions("")

        Dim iSessionId As Integer, sUserName As String = ""

        Do While mSessionInfo.GetNext(iSessionId, sUserName)
            If Trim(sUserName) = Trim(UserName) Then
                Return iSessionId
            End If
        Loop

        Return -1
    End Function
End Class
