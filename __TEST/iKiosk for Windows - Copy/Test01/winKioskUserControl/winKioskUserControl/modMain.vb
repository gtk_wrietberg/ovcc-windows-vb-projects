Imports System.Threading

Module modMain
    Private WTSAPI As clsWTSAPI
    Private threadAbort As Thread = Nothing
    Private threadLogoff As Thread = Nothing
    Private bAbort As Boolean = False

    Public Sub Main()
        WTSAPI = New clsWTSAPI

        StartThreads()
    End Sub

    Private Sub StartThreads()
        threadAbort = New Thread(New ThreadStart(AddressOf ThreadProc_Abort))
        threadAbort.Start()

        threadLogoff = New Thread(New ThreadStart(AddressOf ThreadProc_Logoff))
        threadLogoff.Start()
    End Sub

    Private Sub ThreadProc_Abort()
        Do While (Not bAbort)
            If IO.File.Exists("c:\temp\abort.ourguest") Then
                bAbort = True
            End If

            Thread.Sleep(5000)
        Loop
    End Sub

    Private Sub ThreadProc_Logoff()
        Do While (Not bAbort)
            Dim iSessionId As Integer = WTSAPI.FindSessionIdByUsername("ourguest")
            If iSessionId >= 0 Then
                If IO.File.Exists("c:\temp\logoff.ourguest") Then
                    Shell("c:\Windows\System32\logoff.exe " & iSessionId.ToString)
                End If
            End If

            Thread.Sleep(5000)
        Loop
    End Sub
End Module
