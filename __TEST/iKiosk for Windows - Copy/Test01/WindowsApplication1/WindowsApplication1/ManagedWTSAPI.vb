Imports System.Runtime.InteropServices

Public Class ManagedWTSAPI
    Private Enum WTS_INFO_CLASS
        WTSInitialProgram
        WTSApplicationName
        WTSWorkingDirectory
        WTSOEMId
        WTSSessionId
        WTSUserName
        WTSWinStationName
        WTSDomainName
        WTSConnectState
        WTSClientBuildNumber
        WTSClientName
        WTSClientDirectory
        WTSClientProductId
        WTSClientHardwareId
        WTSClientAddress
        WTSClientDisplay
        WTSClientProtocolType
        WTSIdleTime
        WTSLogonTime
        WTSIncomingBytes
        WTSOutgoingBytes
        WTSIncomingFrames
        WTSOutgoingFrames
        WTSClientInfo
        WTSSessionInfo
    End Enum

    Private Const WTS_CURRENT_SERVER_HANDLE As Long = 0
    Private Const WTS_CURRENT_SESSION As Long = -1

    Private Declare Function WTSQuerySessionInformation Lib "wtsapi32" Alias "WTSQuerySessionInformationA" (ByVal hServer As Long, ByVal SessionID As Long, ByVal WTSInfoClass As Long, ByVal ppBuffer As Long, ByVal pBytesReturned As Long) As Long
    Private Declare Function lstrcpyA Lib "kernel32" (ByVal RetVal As String, ByVal ptr As Long) As Long
    Private Declare Function lstrlenA Lib "kernel32" (ByVal ptr As Long) As Long

    Public Function GetUsernameBySessionId(ByVal SessionId As Int32) As String
        Dim buffer As IntPtr
        Dim strLen As Int32
        Dim username As String = "SYSTEM"

        If (WTSQuerySessionInformation(WTS_CURRENT_SERVER_HANDLE, SessionId, WTS_INFO_CLASS.WTSUserName, buffer, strLen)) Then
            If strLen > 1 Then
                'username = Marshal.PtrToStringAnsi(buffer)
                username = Marshal.PtrToStringAnsi(buffer, strLen)
                'username = strLen.ToString
                WTSFreeMemory(buffer)
            End If
        End If

        Return username
    End Function

    Private Function GetStrFromPtrA(ByVal lpszA As Long) As String

   GetStrFromPtrA = String$(lstrlenA(ByVal lpszA), 0)
   Call lstrcpyA(ByVal GetStrFromPtrA, ByVal lpszA)

    End Function

    '----------------------------------------



    Private Enum WTS_CONNECTSTATE_CLASS
        WTSActive
        WTSConnected
        WTSConnectQuery
        WTSShadow
        WTSDisconnected
        WTSIdle
        WTSListen
        WTSReset
        WTSDown
        WTSInit
    End Enum

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Private Structure WTS_SESSION_INFO
        Dim SessionID As Int32 'DWORD integer
        Dim pWinStationName As String ' integer LPTSTR - Pointer to a null-terminated string containing the name of the WinStation for this session
        Dim State As WTS_CONNECTSTATE_CLASS
    End Structure

    Friend Structure strSessionsInfo
        Dim SessionID As Integer
        Dim StationName As String
        Dim ConnectionState As String
    End Structure

    Friend Structure strSessionsInfoSimple
        Dim SessionID As Integer
        Dim UserName As String
    End Structure


    <DllImport("wtsapi32.dll", _
    bestfitmapping:=True, _
    CallingConvention:=CallingConvention.StdCall, _
    CharSet:=CharSet.Auto, _
    EntryPoint:="WTSEnumerateSessions", _
    setlasterror:=True, _
    ThrowOnUnmappableChar:=True)> _
    Private Shared Function WTSEnumerateSessions( _
    ByVal hServer As IntPtr, _
    <MarshalAs(UnmanagedType.U4)> _
    ByVal Reserved As Int32, _
    <MarshalAs(UnmanagedType.U4)> _
    ByVal Version As Int32, _
    ByRef ppSessionInfo As IntPtr, _
    <MarshalAs(UnmanagedType.U4)> _
    ByRef pCount As Int32) As Int32
    End Function

    <DllImport("wtsapi32.dll")> _
    Private Shared Sub WTSFreeMemory(ByVal pMemory As IntPtr)
    End Sub

    <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
     Private Shared Function WTSOpenServer(ByVal pServerName As String) As IntPtr
    End Function

    <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
     Private Shared Sub WTSCloseServer(ByVal hServer As IntPtr)
    End Sub

    Friend Function GetSessionsSimple(ByVal ServerName As String) As strSessionsInfoSimple()
        Dim sessioninfo As strSessionsInfo()
        Dim RetVal As strSessionsInfoSimple()

        sessioninfo = GetSessions(ServerName)

        Dim sessioninfosimple(sessioninfo.GetUpperBound(0)) As strSessionsInfoSimple

        For i As Integer = 0 To sessioninfosimple.GetUpperBound(0)
            sessioninfosimple(i).SessionID = sessioninfo(i).SessionID
            sessioninfosimple(i).UserName = GetUsernameBySessionId(sessioninfo(i).SessionID)
        Next

        RetVal = sessioninfosimple

        Return RetVal
    End Function

    Friend Function GetSessions(ByVal ServerName As String) As strSessionsInfo()
        Dim ptrOpenedServer As IntPtr
        Dim RetVal As strSessionsInfo()
        Try
            ptrOpenedServer = WTSOpenServer(ServerName)
            Dim FRetVal As Int32
            Dim ppSessionInfo As IntPtr = IntPtr.Zero
            Dim Count As Int32 = 0
            Try
                FRetVal = WTSEnumerateSessions(ptrOpenedServer, 0, 1, ppSessionInfo, Count)
                If FRetVal <> 0 Then
                    Dim sessionInfo() As WTS_SESSION_INFO = New WTS_SESSION_INFO(Count) {}
                    Dim i As Integer
                    Dim DataSize = Marshal.SizeOf(New WTS_SESSION_INFO)
                    Dim current As Int64
                    current = ppSessionInfo.ToInt64
                    For i = 0 To Count - 1 ' Step i + 1
                        sessionInfo(i) = CType(Marshal.PtrToStructure(New IntPtr(current), GetType(WTS_SESSION_INFO)), WTS_SESSION_INFO)
                        current = current + DataSize
                    Next
                    WTSFreeMemory(ppSessionInfo)
                    Dim tmpArr(sessionInfo.GetUpperBound(0)) As strSessionsInfo
                    For i = 0 To tmpArr.GetUpperBound(0)
                        tmpArr(i).SessionID = sessionInfo(i).SessionID
                        tmpArr(i).StationName = sessionInfo(i).pWinStationName
                        tmpArr(i).ConnectionState = GetConnectionState(sessionInfo(i).State)
                    Next
                    ReDim sessionInfo(-1)
                    RetVal = tmpArr
                Else
                    Throw New ApplicationException("No data returned")
                End If
            Catch ex As Exception
                Throw New Exception(ex.Message & vbCrLf & Marshal.GetLastWin32Error)
            End Try
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Exit Function
        Finally
            WTSCloseServer(ptrOpenedServer)
        End Try

        Return RetVal
    End Function

    Private Function GetConnectionState(ByVal State As WTS_CONNECTSTATE_CLASS) As String
        Dim RetVal As String
        Select Case State
            Case WTS_CONNECTSTATE_CLASS.WTSActive
                RetVal = "Active"
            Case WTS_CONNECTSTATE_CLASS.WTSConnected
                RetVal = "Connected"
            Case WTS_CONNECTSTATE_CLASS.WTSConnectQuery
                RetVal = "Query"
            Case WTS_CONNECTSTATE_CLASS.WTSDisconnected
                RetVal = "Disconnected"
            Case WTS_CONNECTSTATE_CLASS.WTSDown
                RetVal = "Down"
            Case WTS_CONNECTSTATE_CLASS.WTSIdle
                RetVal = "Idle"
            Case WTS_CONNECTSTATE_CLASS.WTSInit
                RetVal = "Initializing."
            Case WTS_CONNECTSTATE_CLASS.WTSListen
                RetVal = "Listen"
            Case WTS_CONNECTSTATE_CLASS.WTSReset
                RetVal = "reset"
            Case WTS_CONNECTSTATE_CLASS.WTSShadow
                RetVal = "Shadowing"
            Case Else
                RetVal = "Unknown connect state"
        End Select
        Return RetVal
    End Function

End Class