Imports System
Imports System.ServiceModel
Imports System.Runtime.Serialization


'HOW TO HOST THE WCF SERVICE IN THIS LIBRARY IN ANOTHER PROJECT
'    You will need to do the following things: 
'    1)    Add a Host project to your solution
'        a.    Right click on your solution
'        b.    Select Add
'        c.    Select New Project
'        d.    Choose an appropriate Host project type (e.g. Console Application)
'    2)    Add a new source file to your Host project
'        a.    Right click on your Host project
'        b.    Select Add
'        c.    Select New Item
'        d.    Select "Code File"
'    3)    Paste the contents of the "MyServiceHost" class below into the new Code File
'    4)    Add an "Application Configuration File" to your Host project
'        a.    Right click on your Host project
'        b.    Select Add
'        c.    Select New Item
'        d.    Select "Application Configuration File"
'    5)    Paste the contents of the App.Config below that defines your service endoints into the new Config File
'    6)    Add the code that will host, start and stop the service
'        a.    Call MyServiceHost.StartService() to start the service and MyServiceHost.EndService() to end the service
'    7)    Add a Reference to System.ServiceModel.dll
'        a.    Right click on your Host Project
'        b.    Select "Add Reference"
'        c.    Select "System.ServiceModel.dll"
'    8)    Add a Reference from your Host project to your Service Library project
'        a.    Right click on your Host Project
'        b.    Select "Add Reference"
'        c.    Select the "Projects" tab
'    9)    Set the Host project as the "StartUp" project for the solution
'        a.    Right click on your Host Project
'        b.    Select "Set as StartUp Project"
'################# START MyServiceHost.cs #################
'Imports System
'Imports System.ServiceModel

'An Indigo service consists of a contract (defined below), 
'a class which implements that interface, and configuration 
'entries that specify behaviors and endpoints associated with 
'that implementation (see <system.serviceModel> in your application
'configuration file).

'Public Class MyServiceHost
'    Friend Shared myServiceHost As ServiceHost = Nothing
'    Friend Shared Sub StartService()
'        ' Consider putting the baseAddress in the configuration system
'        ' and getting it here with AppSettings
'        Dim baseAddress As Uri = New Uri("http://localhost:8080/ConsoleApplicationVB/service1")

'        ' Instantiate a new ServiceHost
'        myServiceHost = New ServiceHost(GetType(WCFServiceLibraryTest01.service1), baseAddress)

'        ' Open myServiceHost
'        myServiceHost.Open()
'    End Sub

'    Friend Shared Sub StopService()
'        ' Call StopService from your application's shutdown logic (i.e. dispose)
'        If myServiceHost.State <> CommunicationState.Closed Then
'            myServiceHost.Close()
'        End If
'    End Sub
'End Class

'################# END MyServiceHost.cs #################

'################# START App.config or Web.config #################
'<system.serviceModel>
'   <services>
'     <service name="WCFServiceLibraryTest01.Service1">
'       <endpoint contract="WCFServiceLibraryTest01.IService1" binding="wsHttpBinding"/>
'     </service>
'   </services>
'</system.serviceModel>
'################# END App.config or Web.config #################

'You have created a class library to define and implement your WCF service.
'You will need to add a reference to this library from another project and add 
'the code to that project to host the service as described below.  Another way
'to create and host a WCF service is by using the Add New Item, WCF Service 
'template within an existing project such as a Console Application or a Windows 
'Application.

<ServiceContract()> _
Public Interface IService1
    <OperationContract()> _
    Function Echo(ByVal myValue As String) As String

    <OperationContract()> _
    Function MyStack_Push(ByVal value As String) As Boolean

    <OperationContract()> _
    Function MyStack_Pull() As String

End Interface

Public Class Service1
    Implements IService1

    Private myStack As New Stack(Of String)


    Public Function Echo(ByVal myValue As String) As String Implements IService1.Echo
        Return "Hello: " + myValue
    End Function

    Public Function MyStack_Push(ByVal value As String) As Boolean Implements IService1.MyStack_Push
        myStack.Push(value)

        Return True
    End Function

    Public Function MyStack_Pull() As String Implements IService1.MyStack_Pull
        Return myStack.Pop
    End Function
End Class

<DataContract()> _
Public Class DataContract_Stack
    Private m_Stack As Stack(Of String)

    Public Property FirstName() As String
        Get
            Return m_firstName
        End Get
        Set(ByVal value As String)
            m_firstName = value
        End Set
    End Property

    Public Property LastName() As String
        Get
            Return m_lastName
        End Get
        Set(ByVal value As String)
            m_lastName = value
        End Set
    End Property
End Class

