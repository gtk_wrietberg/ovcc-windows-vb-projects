﻿'Module1.vb
Imports System
Imports System.ServiceModel
Imports System.ServiceModel.Description
Imports myLib.Service1

Module Module1

    Sub Main()
        ' Step 1 Create a URI to serve as the base address
        Dim baseAddress As New Uri("http://localhost:8000/SayHelloService")

        ' Step 2 Create a ServiceHost instance
        Dim selfHost As New ServiceHost(GetType(myLib.Service1), baseAddress)
        Try

            ' Step 3 Add a service endpoint
            ' Add a service endpoint
            selfHost.AddServiceEndpoint(GetType(myLib.IService1), New WSHttpBinding(), "HelloService")

            ' Step 4 Enable metadata exchange.
            Dim smb As New ServiceMetadataBehavior()
            smb.HttpGetEnabled = True
            selfHost.Description.Behaviors.Add(smb)

            ' Step 5 Start the service
            selfHost.Open()
            Console.WriteLine("The service is ready.")
            Console.WriteLine("Press <ENTER> to terminate service.")
            Console.WriteLine()
            Console.ReadLine()

            ' Close the ServiceHostBase to shutdown the service.
            selfHost.Close()
        Catch ce As CommunicationException
            Console.WriteLine("An exception occurred: {0}", ce.Message)
            selfHost.Abort()
        End Try
    End Sub

End Module
