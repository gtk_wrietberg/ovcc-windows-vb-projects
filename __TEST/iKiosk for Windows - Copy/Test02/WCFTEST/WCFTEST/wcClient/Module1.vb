﻿'Client Module1
Imports System
Imports System.ServiceModel
Imports wcClient.ServiceReference1


Module Module1
    Dim Client As New Service1Client

    Sub Main()
        Console.WriteLine("Client Window")
        Console.WriteLine("{0}", Client.GetData(Console.ReadLine()))

        ' Step 3: Closing the client gracefully closes the connection and cleans up resources.
        Client.Close()

        Console.WriteLine()
        Console.WriteLine("Press <ENTER> to terminate client.")
        Console.ReadLine()

    End Sub

End Module