﻿' IService1
Imports System
Imports System.ServiceModel


<ServiceContract()>
Public Interface IService1

    <OperationContract()>
    Function GetData(ByVal value As String) As String

End Interface
