﻿' service1
Imports System
Imports System.ServiceModel

Public Class Service1
    Implements IService1

    Public Function GetName(ByVal value As String) As String Implements IService1.GetData
        Console.WriteLine("Hello {0}", value)
        Return String.Format("Hello {0}", value)
    End Function

End Class
