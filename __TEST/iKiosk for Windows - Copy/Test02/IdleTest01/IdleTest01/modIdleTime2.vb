Imports system.Runtime.InteropServices

Module modIdleTime2
    <StructLayout(LayoutKind.Sequential)> _
        Structure SYSTEMPOWERINFORMATION
        <MarshalAs(UnmanagedType.U4)> _
        Public MaxIdlenessAllowed As Integer
        <MarshalAs(UnmanagedType.U4)> _
        Public Idleness As Integer
        <MarshalAs(UnmanagedType.U4)> _
        Public TimeRemaining As Integer
        <MarshalAs(UnmanagedType.U4)> _
        Public CoolingMode As Integer
    End Structure

    'Structure SYSTEMPOWERINFORMATION
    '    Public MaxIdlenessAllowed As ULong
    '    Public Idleness As ULong
    '    Public TimeRemaining As ULong
    '    Public CoolingMode As Char
    'End Structure


    '<DllImport("powrprof.dll")> _
    'Private Function CallNtPowerInformation(ByVal InformationLevel As Int32, ByVal lpInputBUffer As IntPtr, ByVal nInputBufferSize As UInt32, ByRef lpOutputBuffer As IntPtr, ByVal nOutputBufferSize As UInt32) As Boolean
    'End Function

    <DllImport("powrprof.dll", EntryPoint:="CallNtPowerInformation", SetLastError:=False)> _
        Private Function GetSystemPowerInformation( _
        ByVal InformationLevel As Int32, _
        ByVal lpInputBUffer As IntPtr, _
        ByVal nInputBufferSize As Integer, _
        <Out()> _
        ByVal lpOutputBuffer As SYSTEMPOWERINFORMATION, _
        ByVal nOutputBufferSize As Integer) As Integer
    End Function

    Public Function IdleTest02() As Integer
        Dim retval As Integer
        Dim spi As SYSTEMPOWERINFORMATION
        Dim status As New IntPtr
        status = Marshal.AllocCoTaskMem(Marshal.SizeOf(spi))


        retval = GetSystemPowerInformation(12, 0, 0, spi, Marshal.SizeOf(spi))

        'Marshal.PtrToStructure(status, spi)

        'Return spi.TimeRemaining
        Return spi.MaxIdlenessAllowed
    End Function
End Module
