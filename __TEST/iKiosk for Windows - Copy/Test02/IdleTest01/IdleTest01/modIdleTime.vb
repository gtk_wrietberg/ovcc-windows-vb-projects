Imports system.Runtime.InteropServices

Module modIdleTime
    '====================Windows Idle time=================
    <StructLayout(LayoutKind.Sequential)> _
    Structure LASTINPUTINFO
        <MarshalAs(UnmanagedType.U4)> _
        Public cbSize As Integer
        <MarshalAs(UnmanagedType.U4)> _
        Public dwTime As Integer
    End Structure

    <DllImport("User32.dll")> _
    Private Function GetLastInputInfo(ByRef plii As LASTINPUTINFO) As Boolean
    End Function

    Private myLastInputInfo As New LASTINPUTINFO

    Public Function GetMachineIdleTime() As Integer
        Dim idleTime As Integer = -1

        myLastInputInfo.cbSize = Marshal.SizeOf(myLastInputInfo)
        myLastInputInfo.dwTime = 0

        

        If GetLastInputInfo(myLastInputInfo) Then
            Dim tc As Integer = Environment.TickCount
            Dim dt As Integer = myLastInputInfo.dwTime

            idleTime = (tc - dt)

            Form1.Label2.Text = tc.ToString & " - " & dt.ToString & " = " & idleTime

            'idleTime = (Environment.TickCount - myLastInputInfo.dwTime)
        End If

        Return idleTime
    End Function
End Module


