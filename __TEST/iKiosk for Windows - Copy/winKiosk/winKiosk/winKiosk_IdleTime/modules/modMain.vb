Imports System.Threading
Imports system.Runtime.InteropServices

Module modMain

    <StructLayout(LayoutKind.Sequential)> _
    Structure LASTINPUTINFO
        <MarshalAs(UnmanagedType.U4)> _
        Public cbSize As Integer
        <MarshalAs(UnmanagedType.U4)> _
        Public dwTime As Integer
    End Structure

    <DllImport("User32.dll")> _
    Private Function GetLastInputInfo(ByRef plii As LASTINPUTINFO) As Boolean
    End Function

    Private myLastInputInfo As New LASTINPUTINFO

    Private Function GetMachineIdleTime() As Integer
        Dim idleTime As Integer = -1

        myLastInputInfo.cbSize = Marshal.SizeOf(myLastInputInfo)
        myLastInputInfo.dwTime = 0
        If GetLastInputInfo(myLastInputInfo) Then
            idleTime = (Environment.TickCount - myLastInputInfo.dwTime) / 1000
        End If

        Return idleTime
    End Function

    Private threadKeepAlive As Thread = Nothing
    Private mKillThread As Boolean = False
    Private myTimer As New System.Timers.Timer

    Public Sub Main()
        myTimer.AutoReset = True
        myTimer.Interval = 1000
        AddHandler myTimer.Elapsed, AddressOf myTimer_Tick
        myTimer.Start()

        threadKeepAlive = New Thread(New ThreadStart(AddressOf ThreadProc_KeepAlive))
        threadKeepAlive.Start()
    End Sub

    Private Sub ThreadProc_KeepAlive()
        Do While Not mKillThread
            Thread.Sleep(1000)
        Loop
    End Sub

    Private Sub myTimer_Tick(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        clsSettings.IdleTime_Value = GetMachineIdleTime()
    End Sub
End Module
