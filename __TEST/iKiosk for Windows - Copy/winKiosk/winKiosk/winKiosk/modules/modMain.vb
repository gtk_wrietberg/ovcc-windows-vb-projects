Imports System.Threading

Module modMain
    Private threadMain As Thread = Nothing
    Private threadPopup_IdleWarning As Thread = Nothing

    Private bAbort As Boolean = False

    Public Sub Main()
        InitGlobals()

        oLogger.WriteToLog("started")

        threadMain = New Thread(New ThreadStart(AddressOf ThreadProc_Main))
        threadMain.Start()
    End Sub

    Private Sub ThreadProc_Main()
        oLogger.WriteToLog("main thread started", , 1)

        Do While (Not bAbort)
            If clsSettings.IdleTime_WarningTriggered Then
                OpenIdleWarningForm()
            Else
                CloseIdleWarningForm()
            End If


            Thread.Sleep(1000)
        Loop
    End Sub

    Private Sub OpenIdleWarningForm()
        Dim bShowPopup As Boolean = False

        If threadPopup_IdleWarning Is Nothing Then
            bShowPopup = True
        Else
            If Not threadPopup_IdleWarning.IsAlive Then
                bShowPopup = True
            End If
        End If

        oLogger.WriteToLog("bShowPopup=" & bShowPopup, , 1)

        If bShowPopup Then
            oLogger.WriteToLog("opening idle popup", , 1)

            threadPopup_IdleWarning = New Thread(New ThreadStart(AddressOf ThreadProc_Popup_IdleWarning))
            threadPopup_IdleWarning.Start()
        End If
    End Sub

    Private Sub CloseIdleWarningForm()
        Dim bClosePopup As Boolean = True

        If threadPopup_IdleWarning Is Nothing Then
            bClosePopup = False
        Else
            If Not threadPopup_IdleWarning.IsAlive Then
                bClosePopup = False
            End If
        End If

        oLogger.WriteToLog("bClosePopup=" & bClosePopup, , 1)

        If bClosePopup Then
            Dim fs As New FormCollection


            Dim openForms As Integer = 0

            oLogger.WriteToLog("close forms", , 2)
            closeForms(frmIdleWarning)
        End If
    End Sub

    Private Sub ThreadProc_Popup_IdleWarning()
        'Check if form is already there
        Dim openForms As Integer = 0

        oLogger.WriteToLog("checking if form is already open", , 2)

        openForms = isFormRunning(frmIdleWarning)

        oLogger.WriteToLog("isFormRunning(frmIdleWarning)=" & openForms, , 2)

        If openForms > 0 Then
            'already there
        Else
            Dim frm As New frmIdleWarning
            Dim frmResult As DialogResult

            oLogger.WriteToLog("opening", , 2)

            'frm.Size = Screen.PrimaryScreen.Bounds.Size
            frm.StartPosition = FormStartPosition.CenterScreen
            frmResult = frm.ShowDialog()

            If frmResult = DialogResult.Yes Then
                bAbort = True
            End If
        End If
    End Sub

    Private Function isFormRunning(ByVal frm As Form) As Integer
        Dim fs As New FormCollection
        Dim cntFrm As Integer = 0


        fs = Application.OpenForms
        For Each f As Form In fs
            oLogger.WriteToLogRelative(f.Name & " = " & frm.Name, , 1)

            If f.Name = frm.Name Then
                cntFrm += 1
                oLogger.WriteToLogRelative("cntFrm = " & cntFrm, , 2)
            End If
        Next

        Return cntFrm
    End Function

    Private Function closeForms(ByVal frm As Form) As Integer
        Dim lstForms As New List(Of Form)
        Dim fs As New FormCollection

        fs = Application.OpenForms
        For Each f As Form In fs
            oLogger.WriteToLogRelative(f.Name & " = " & frm.Name, , 1)

            If f.Name = frm.Name Then
                lstForms.Add(f)
            End If
        Next

        For Each f As Form In lstForms
            f.DialogResult = DialogResult.Abort
            f.Close()
        Next

        Return lstForms.Count
    End Function
End Module
