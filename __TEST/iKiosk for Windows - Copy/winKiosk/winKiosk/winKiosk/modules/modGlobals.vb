Module modGlobals
    Public oLogger As clsLogger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = "c:\iBAHN\_logs"
        g_LogFileDirectory &= "\winKiosk"

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"


        IO.Directory.CreateDirectory(g_LogFileDirectory)


        oLogger = New clsLogger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))
    End Sub
End Module
