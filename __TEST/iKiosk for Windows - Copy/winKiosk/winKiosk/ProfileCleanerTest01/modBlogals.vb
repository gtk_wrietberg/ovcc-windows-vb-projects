Module modBlogals
    Public ReadOnly c_ControlledUserDefault As String = "ourguest"
    Public ReadOnly c_LogoffCommand As String = "C:\Windows\System32\logoff.exe"
    Public ReadOnly c_iBAHN__Root As String = "C:\iBAHN\winKiosk"
    Public ReadOnly c_iBAHN__ControlledUser_CleanProfile As String = "\_clean_profile"
    Public ReadOnly c_USERS As String = "c:\Users\"

    Public oLogger As clsLogger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_ControlledUser As String

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = "c:\iBAHN\_logs"
        g_LogFileDirectory &= "\winKioskUserControlService"

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"


        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(c_iBAHN__Root & c_iBAHN__ControlledUser_CleanProfile)


        oLogger = New clsLogger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        g_ControlledUser = c_ControlledUserDefault
    End Sub
End Module
