Public Class Form1
    Private oCleanup As Cleanup
    Private oUser As clsWindowsUser

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MsgBox(oUser.DisableAccount())
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        oLogger.WriteToLog("cleaning profile", , 2)
        oLogger.WriteToLog("removing current profile", , 3)
        oCleanup = New Cleanup
        oCleanup.AddLocation(c_USERS & g_ControlledUser, True, "*", 0)
        oCleanup.CleanUp_Locations()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            My.Computer.FileSystem.CopyDirectory(c_iBAHN__Root & c_iBAHN__ControlledUser_CleanProfile & "\" & g_ControlledUser, c_USERS & "\" & g_ControlledUser, True)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        MsgBox(oUser.EnableAccount())
    End Sub





    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        InitGlobals()

        oUser = New clsWindowsUser("ourguest")
    End Sub
End Class
