Imports System.Runtime.InteropServices

Public Class clsWTSAPI
    Private Class clsSessionInfoContainer
        Private Class clsSessionInfo
            Private mSessionId As Integer
            Private mUsername As String

            Public Property SessionId() As Integer
                Get
                    Return mSessionId
                End Get
                Set(ByVal value As Integer)
                    mSessionId = value
                End Set
            End Property

            Public Property Username() As String
                Get
                    Return mUsername
                End Get
                Set(ByVal value As String)
                    mUsername = value
                End Set
            End Property
        End Class

        Private mList As New List(Of clsSessionInfo)
        Private mCounter As Integer = 0

        Public Sub New()
            mList = New List(Of clsSessionInfo)
            mCounter = 0
        End Sub

        Public Sub Add(ByVal SessionId As Integer, ByVal UserName As String)
            If Not Contains(SessionId) Then
                Dim newItem As New clsSessionInfo
                newItem.SessionId = SessionId
                newItem.Username = UserName

                mList.Add(newItem)
            End If
        End Sub

        Public Function Contains(ByVal SessionId As Integer) As Boolean
            For i As Integer = 0 To mList.Count - 1
                If mList(i).SessionId = SessionId Then
                    Return True
                End If
            Next

            Return False
        End Function

        Public Sub Reset()
            mCounter = 0
        End Sub

        Public Function GetNext(ByRef SessionId As Integer, ByRef UserName As String) As Boolean
            If mCounter < 0 Then mCounter = 0

            If mCounter < mList.Count Then
                SessionId = mList(mCounter).SessionId
                UserName = mList(mCounter).Username
            End If

            mCounter += 1
            Return (mCounter <= mList.Count)
        End Function
    End Class

    '***************************************************************************************************************

    Private Enum WTS_CONNECTSTATE_CLASS
        WTSActive
        WTSConnected
        WTSConnectQuery
        WTSShadow
        WTSDisconnected
        WTSIdle
        WTSListen
        WTSReset
        WTSDown
        WTSInit
    End Enum

    Private Enum WTS_INFO_CLASS
        WTSInitialProgram
        WTSApplicationName
        WTSWorkingDirectory
        WTSOEMId
        WTSSessionId
        WTSUserName
        WTSWinStationName
        WTSDomainName
        WTSConnectState
        WTSClientBuildNumber
        WTSClientName
        WTSClientDirectory
        WTSClientProductId
        WTSClientHardwareId
        WTSClientAddress
        WTSClientDisplay
        WTSClientProtocolType
        WTSIdleTime
        WTSLogonTime
        WTSIncomingBytes
        WTSOutgoingBytes
        WTSIncomingFrames
        WTSOutgoingFrames
    End Enum

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)> _
    Private Structure WTS_SESSION_INFO
        Dim SessionID As Int32 'DWORD integer
        Dim pWinStationName As String ' integer LPTSTR - Pointer to a null-terminated string containing the name of the WinStation for this session
        Dim State As WTS_CONNECTSTATE_CLASS
    End Structure

    Private Structure strSessionsInfo
        Dim SessionID As Integer
        Dim StationName As String
        Dim ConnectionState As String
    End Structure

    Private Structure SECURITY_ATTRIBUTES
        Public nLength As UInteger
        Public lpSecurityDescriptor As IntPtr
        <MarshalAs(UnmanagedType.Bool)> Public bInheritHandle As Boolean
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure STARTUPINFOW
        Public cb As UInteger
        <MarshalAs(UnmanagedType.LPWStr)> _
        Public lpReserved As String
        <MarshalAs(UnmanagedType.LPWStr)> _
        Public lpDesktop As String
        <MarshalAs(UnmanagedType.LPWStr)> _
        Public lpTitle As String
        Public dwX As UInteger
        Public dwY As UInteger
        Public dwXSize As UInteger
        Public dwYSize As UInteger
        Public dwXCountChars As UInteger
        Public dwYCountChars As UInteger
        Public dwFillAttribute As UInteger
        Public dwFlags As UInteger
        Public wShowWindow As UShort
        Public cbReserved2 As UShort
        Public lpReserved2 As IntPtr
        Public hStdInput As IntPtr
        Public hStdOutput As IntPtr
        Public hStdError As IntPtr
    End Structure

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure PROCESS_INFORMATION
        Public hProcess As IntPtr
        Public hThread As IntPtr
        Public dwProcessId As UInteger
        Public dwThreadId As UInteger
    End Structure

    'Private Declare Function WTSQuerySessionInformation Lib "WtsApi32.dll" Alias "WTSQuerySessionInformationW" (ByVal hServer As Int32, _
    'ByVal SessionId As Int32, ByVal WTSInfoClass As Int32, <MarshalAs(UnmanagedType.LPWStr)> ByRef ppBuffer As String, ByRef pCount As Int32) As Boolean

    '<DllImport("WtsApi32.dll", entrypoint:="WTSQuerySessionInformation", setlasterror:=True)> _
    '    Private Shared Function WTSQuerySessionInformation( _
    '    ByVal hServer As Int32, _
    '    ByVal SessionId As Int32, _
    '    ByVal WTSInfoClass As Int32, _
    '    <MarshalAs(UnmanagedType.LPWStr)> ByRef ppBuffer As String, _
    '    ByRef pCount As Int32) As Boolean
    'End Function

    <DllImport("WtsApi32.dll", entrypoint:="WTSQuerySessionInformation", setlasterror:=True)> _
    Private Shared Function WTSQuerySessionInformation( _
    ByVal hServer As Int32, _
    ByVal SessionId As Int32, _
    ByVal WTSInfoClass As WTS_INFO_CLASS, _
    ByRef ppBuffer As IntPtr, _
    ByRef pCount As Int32) As Boolean
    End Function

    'Private Declare Function WTSQuerySessionInformation2 Lib "WtsApi32.dll" Alias "WTSQuerySessionInformationW" (ByVal hServer As Int32, _
    '  ByVal SessionId As Int32, ByVal WTSInfoClass As Int32, ByRef ppBuffer As IntPtr, ByRef pCount As Int32) As Boolean

    'Private Declare Function GetCurrentProcessId Lib "Kernel32.dll" Alias "GetCurrentProcessId" () As Int32
    'Private Declare Function ProcessIdToSessionId Lib "Kernel32.dll" Alias "ProcessIdToSessionId" (ByVal processID As Int32, ByRef sessionID As Int32) As Boolean
    <DllImport("kernel32.dll", entrypoint:="WTSGetActiveConsoleSessionId", setlasterror:=True)> _
    Private Shared Function WTSGetActiveConsoleSessionId() As UInteger
    End Function

    <DllImport("wtsapi32.dll", _
    bestfitmapping:=True, _
    CallingConvention:=CallingConvention.StdCall, _
    CharSet:=CharSet.Auto, _
    EntryPoint:="WTSEnumerateSessions", _
    setlasterror:=True, _
    ThrowOnUnmappableChar:=True)> _
    Private Shared Function WTSEnumerateSessions( _
    ByVal hServer As IntPtr, _
    <MarshalAs(UnmanagedType.U4)> _
    ByVal Reserved As Int32, _
    <MarshalAs(UnmanagedType.U4)> _
    ByVal Vesrion As Int32, _
    ByRef ppSessionInfo As IntPtr, _
    <MarshalAs(UnmanagedType.U4)> _
    ByRef pCount As Int32) As Int32
    End Function

    <DllImport("wtsapi32.dll")> _
    Private Shared Sub WTSFreeMemory(ByVal pMemory As IntPtr)
    End Sub

    <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
     Private Shared Function WTSOpenServer(ByVal pServerName As String) As IntPtr
    End Function

    <DllImport("wtsapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
     Private Shared Sub WTSCloseServer(ByVal hServer As IntPtr)
    End Sub

    <DllImport("wtsapi32.dll", entrypoint:="WTSQueryUserToken", setlasterror:=True)> _
    Private Shared Function WTSQueryUserToken(ByVal SessionId As UInteger, ByRef phToken As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("kernel32.dll", entrypoint:="CloseHandle", setlasterror:=True)> _
        Private Shared Function CloseHandle(<[In]()> ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("advapi32.dll", entrypoint:="CreateProcessAsUserW", setlasterror:=True)> _
        Private Shared Function CreateProcessAsUser( _
        <[In]()> ByVal hToken As IntPtr, _
        <[In](), MarshalAs(UnmanagedType.LPWStr)> ByVal lpApplicationName As String, _
        ByVal lpCommandLine As System.IntPtr, _
        <[In]()> ByVal lpProcessAttributes As IntPtr, _
        <[In]()> ByVal lpThreadAttributes As IntPtr, _
        <MarshalAs(UnmanagedType.Bool)> ByVal bInheritHandles As Boolean, _
        ByVal dwCreationFlags As UInteger, _
        <[In]()> ByVal lpEnvironment As IntPtr, _
        <[In](), MarshalAs(UnmanagedType.LPWStr)> ByVal lpCurrentDirectory As String, _
        <[In]()> ByRef lpStartupInfo As STARTUPINFOW, _
        <Out()> ByRef lpProcessInformation As PROCESS_INFORMATION) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function


    '---
    Private mSessionInfo As New clsSessionInfoContainer

    Public Sub New()
        mSessionInfo = New clsSessionInfoContainer
    End Sub

    Private Function LoadSessions(ByVal ServerName As String) As Boolean
        mSessionInfo = New clsSessionInfoContainer

        Dim ptrOpenedServer As IntPtr
        Dim bRet As Boolean = True

        Try
            ptrOpenedServer = WTSOpenServer(ServerName)
            If ptrOpenedServer = vbNull Then
                bRet = False

                Throw New Exception("Terminal Services not running on : " & ServerName)
            End If

            Dim FRetVal As Int32
            Dim ppSessionInfo As IntPtr = IntPtr.Zero
            Dim Count As Int32 = 0
            Try
                FRetVal = WTSEnumerateSessions(ptrOpenedServer, 0, 1, ppSessionInfo, Count)
                If FRetVal <> 0 Then
                    Dim sessionInfo() As WTS_SESSION_INFO = New WTS_SESSION_INFO(Count) {}
                    Dim i As Integer
                    Dim session_ptr As System.IntPtr
                    For i = 0 To Count - 1
                        session_ptr = ppSessionInfo.ToInt32() + (i * Marshal.SizeOf(sessionInfo(i)))
                        sessionInfo(i) = CType(Marshal.PtrToStructure(session_ptr, GetType(WTS_SESSION_INFO)), WTS_SESSION_INFO)
                    Next
                    WTSFreeMemory(ppSessionInfo)
                    Dim tmpArr(sessionInfo.GetUpperBound(0)) As strSessionsInfo

                    For i = 0 To tmpArr.GetUpperBound(0)
                        mSessionInfo.Add(sessionInfo(i).SessionID, GetSessionUsername(ptrOpenedServer, sessionInfo(i).SessionID))

                        tmpArr(i).SessionID = sessionInfo(i).SessionID
                        tmpArr(i).StationName = sessionInfo(i).pWinStationName
                        tmpArr(i).ConnectionState = GetConnectionState(sessionInfo(i).State)
                    Next
                    ReDim sessionInfo(-1)
                Else
                    Throw New ApplicationException("No data returned")
                End If
            Catch ex As Exception
                oLogger.WriteToLog("(WTSAPI ERROR) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                oLogger.WriteToLog("(LoadSessions (inner)) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                oLogger.WriteToLog(ex.Message, clsLogger.MESSAGE_TYPE.LOG_ERROR, 2)

                Throw New Exception(ex.Message & vbCrLf & System.Runtime.InteropServices.Marshal.GetLastWin32Error)
            End Try
        Catch ex As Exception
            oLogger.WriteToLog("(WTSAPI ERROR) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
            oLogger.WriteToLog("(LoadSessions (outer)) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog(ex.Message, clsLogger.MESSAGE_TYPE.LOG_ERROR, 2)

            Throw New Exception(ex.Message)
        Finally
        End Try

        Try
            WTSCloseServer(ptrOpenedServer)
        Catch ex As Exception

        End Try

        Return bRet
    End Function

    Private Function GetSessionUsername(ByVal ptrOpenedServer As IntPtr, ByVal SessionID As Integer) As String
        Dim pUsername As IntPtr
        Dim username As String = ""
        Dim username_len As Integer

        Try
            If WTSQuerySessionInformation(ptrOpenedServer, SessionID, WTS_INFO_CLASS.WTSUserName, pUsername, username_len) Then
                username = Marshal.PtrToStringAnsi(pUsername)
                WTSFreeMemory(pUsername)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("(WTSAPI ERROR) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
            oLogger.WriteToLog("(GetSessionUsername) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog(ex.Message, clsLogger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try




        Return username
    End Function

    Private Function GetConnectionState(ByVal State As WTS_CONNECTSTATE_CLASS) As String
        Dim RetVal As String
        Select Case State
            Case WTS_CONNECTSTATE_CLASS.WTSActive
                RetVal = "Active"
            Case WTS_CONNECTSTATE_CLASS.WTSConnected
                RetVal = "Connected"
            Case WTS_CONNECTSTATE_CLASS.WTSConnectQuery
                RetVal = "Query"
            Case WTS_CONNECTSTATE_CLASS.WTSDisconnected
                RetVal = "Disconnected"
            Case WTS_CONNECTSTATE_CLASS.WTSDown
                RetVal = "Down"
            Case WTS_CONNECTSTATE_CLASS.WTSIdle
                RetVal = "Idle"
            Case WTS_CONNECTSTATE_CLASS.WTSInit
                RetVal = "Initializing."
            Case WTS_CONNECTSTATE_CLASS.WTSListen
                RetVal = "Listen"
            Case WTS_CONNECTSTATE_CLASS.WTSReset
                RetVal = "reset"
            Case WTS_CONNECTSTATE_CLASS.WTSShadow
                RetVal = "Shadowing"
            Case Else
                RetVal = "Unknown connect state"
        End Select
        Return RetVal
    End Function

    Public Function FindSessionIdByUsername(ByVal UserName As String) As Integer
        Dim iSessionId As Integer = 0, sUserName As String = ""

        Try
            LoadSessions("")

            Do While mSessionInfo.GetNext(iSessionId, sUserName)
                'oLogger.WriteToLogWithoutDate("iSessionId=" & iSessionId.ToString & " | Username=_" & UserName & "_ | sUserName=_" & sUserName & "_")
                If Trim(sUserName) = Trim(UserName) Then
                    Return iSessionId
                End If
            Loop
        Catch ex As Exception
            oLogger.WriteToLog("(WTSAPI ERROR) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
            oLogger.WriteToLog("(FindSessionIdByUsername) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog(ex.Message, clsLogger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        Return 0
    End Function

    Public Function FindActiveSessionId() As Integer
        Dim iSessionId As Integer = 0

        Try
            iSessionId = WTSGetActiveConsoleSessionId()
        Catch ex As Exception
            oLogger.WriteToLog("(WTSAPI ERROR) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
            oLogger.WriteToLog("(FindActiveSessionId) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog(ex.Message, clsLogger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        Return iSessionId
    End Function

    Public Function CreateProcessInSession(ByVal SessionId As Integer, ByVal ApplicationName As String) As Boolean
        Dim hUserToken As IntPtr = IntPtr.Zero
        Dim bRet As Boolean = False

        Try
            WTSQueryUserToken(SessionId, hUserToken)

            Dim procInfo As New PROCESS_INFORMATION
            Dim startInfo As New STARTUPINFOW


            startInfo.cb = CUInt(Marshal.SizeOf(startInfo))

            bRet = CreateProcessAsUser(hUserToken, ApplicationName, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, False, 0, IntPtr.Zero, Nothing, startInfo, procInfo)
            If Not hUserToken = IntPtr.Zero Then
                CloseHandle(hUserToken)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("(WTSAPI ERROR) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
            oLogger.WriteToLog("(CreateProcessInSession) ", clsLogger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog(ex.Message, clsLogger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        Return bRet
    End Function
End Class
