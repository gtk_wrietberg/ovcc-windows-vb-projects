Imports System.Threading

Public Class serviceMain
    Private threadMain As Thread = Nothing
    Private threadProcesWatchdog As Thread = Nothing

    Private bAbort As Boolean = False
    Private bOurGuestAccountActive As Boolean = False

    Private WithEvents oProcess As clsProcessRunner

    Protected Overrides Sub OnStart(ByVal args() As String)
        InitGlobals()

        oLogger.WriteToLog("service started")

        WTSAPI = New clsWTSAPI
        oUser = New clsWindowsUser(g_ControlledUser_Username)

        StartThreads()
    End Sub

    Protected Overrides Sub OnStop()
        oLogger.WriteToLog("service stopped")

        bAbort = True
    End Sub

    '------------------
    Private Sub StartThreads()
        threadMain = New Thread(New ThreadStart(AddressOf ThreadProc_Main))
        threadMain.Start()

        threadProcesWatchdog = New Thread(New ThreadStart(AddressOf ThreadProc_ProcessWatchdog))
        threadProcesWatchdog.Start()
    End Sub

    Private Sub ThreadProc_Main()
        oLogger.WriteToLog("started main thread")

        Dim iCurrentSessionId As Integer = 0
        Dim idleTime As Integer = 0
        Dim bTriggerLogoff As Boolean = False
        Dim iSessionId As Integer = 0

        Do While (Not bAbort)
            If clsSettings.Settings_Reload Then
                clsSettings.Settings_Reload = False
                Loadsettings()
            End If

            iSessionId = WTSAPI.FindSessionIdByUsername(g_ControlledUser_Username)


            clsSettings.ControlledUser_CurrentSessionId = iSessionId

            'Dim iActiveSessionId As Integer = WTSAPI.FindActiveSessionId()
            'oLogger.WriteToLog("session id: " & iSessionId.ToString, , 1)
            'oLogger.WriteToLog("active session id: " & iActiveSessionId.ToString, , 1)

            If iSessionId > 0 Then
                If iCurrentSessionId <> iSessionId Then
                    oLogger.WriteToLog("found session id: " & iSessionId.ToString, , 1)
                    iCurrentSessionId = iSessionId

                    clsSettings.ControlledUser_IsLoggedIn = True
                End If

                'Check idle time
                If clsHelpers.isProcessRunning(c_PROCESS__winKiosk_IdleTime) Then
                    idleTime = clsSettings.IdleTime_Value
                    If idleTime > (g_IdleTime_Trigger - g_IdleTime_Max) And idleTime <= g_IdleTime_Max Then
                        'Oops, idle for too long
                        clsSettings.IdleTime_WarningTriggered = True
                    Else
                        clsSettings.IdleTime_WarningTriggered = False
                    End If
                    If idleTime > g_IdleTime_Max Then
                        'Enough is enough
                        bTriggerLogoff = True
                    End If
                Else
                    'Idle time is not being updated. Something is wrong. Hopefully the process watchdog will fix it.
                    clsSettings.IdleTime_WarningTriggered = False
                End If

                'Force a logoff, if necessary
                If clsSettings.Logoff_Activate Or bTriggerLogoff Then
                    bTriggerLogoff = False

                    clsSettings.Logoff_IsActivated = True

                    oLogger.WriteToLog("initiating logoff for " & g_ControlledUser_Username, , 2)
                    RunLogoffCommandForSessionId(iSessionId)
                End If
            Else
                'Reset some stuff
                clsSettings.IdleTime_WarningTriggered = False
                clsSettings.Logoff_IsActivated = False


                iCurrentSessionId = 0

                'Check if the user was logged on
                If clsSettings.ControlledUser_IsLoggedIn Then
                    'Logoff detected!
                    oLogger.WriteToLog("logoff detected for " & g_ControlledUser_Username, , 1)
                    clsSettings.ControlledUser_IsLoggedIn = False

                    'Trigger a profile cleanup
                    RestoreDefaultProfile()
                End If

            End If

            Thread.Sleep(5000)
        Loop
    End Sub

    Private Sub ThreadProc_ProcessWatchdog()
        oLogger.WriteToLog("started process watchdog thread")

        Dim iSessionId As Integer = 0

        Do While (Not bAbort)
            iSessionId = iSessionId = WTSAPI.FindSessionIdByUsername(g_ControlledUser_Username)

            If iSessionId > 0 And Not clsSettings.Logoff_IsActivated Then
                If Not clsHelpers.isProcessRunning(c_PROCESS__winKiosk_IdleTime) Then
                    oLogger.WriteToLog("(WATCHDOG) " & c_PROCESS__winKiosk_IdleTime & " is not running!", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                    oLogger.WriteToLog("(WATCHDOG) starting " & clsHelpers.getFullPathForProcessName(c_PROCESS__winKiosk_IdleTime), clsLogger.MESSAGE_TYPE.LOG_WARNING, 0)

                    If WTSAPI.CreateProcessInSession(iSessionId, clsHelpers.getFullPathForProcessName(c_PROCESS__winKiosk_IdleTime)) Then
                        oLogger.WriteToLog("(WATCHDOG) ok", , 0)
                    Else
                        oLogger.WriteToLog("(WATCHDOG) fail", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                    End If
                End If
                If Not clsHelpers.isProcessRunning(c_PROCESS__winKiosk) Then
                    oLogger.WriteToLog("(WATCHDOG) " & c_PROCESS__winKiosk & " is not running!", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                    oLogger.WriteToLog("(WATCHDOG) starting " & clsHelpers.getFullPathForProcessName(c_PROCESS__winKiosk), clsLogger.MESSAGE_TYPE.LOG_WARNING, 0)

                    If WTSAPI.CreateProcessInSession(iSessionId, clsHelpers.getFullPathForProcessName(c_PROCESS__winKiosk)) Then
                        oLogger.WriteToLog("(WATCHDOG) ok", , 0)
                    Else
                        oLogger.WriteToLog("(WATCHDOG) fail", clsLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                    End If
                End If
            End If


            Thread.Sleep(5000)
        Loop
    End Sub



#Region "ProcessRunner events"
    Private Sub RunLogoffCommandForSessionId(ByVal iSessionId As Integer)
        oLogger.WriteToLogRelative("Running Logoff command", , 1)
        oLogger.WriteToLogRelative("Path   : " & c_LogoffCommand, , 2)
        oLogger.WriteToLogRelative("Params : " & iSessionId.ToString, , 2)
        oLogger.WriteToLogRelative("Timeout: 60", , 2)

        oProcess = New clsProcessRunner
        oProcess.FileName = c_LogoffCommand
        oProcess.Arguments = iSessionId.ToString
        oProcess.MaxTimeout = 60
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()

        Do
            System.Threading.Thread.Sleep(1000)
        Loop Until oProcess.IsProcessDone

        If oProcess.ProcessTimedOut Then
            oLogger.WriteToLogRelative("command timed out", , 1)
        End If

        If oProcess.ErrorsOccurred Then
            oLogger.WriteToLogRelative("command exited with errors", , 1)
        End If
    End Sub

    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLogRelative("done", , 1)
        oLogger.WriteToLogRelative("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLogRelative("failed", , 1)
        oLogger.WriteToLogRelative(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLogRelative("started", , 1)
        oLogger.WriteToLogRelative("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLogRelative("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
#End Region
End Class
