Module modRestoreDefaultProfile
    Public Sub RestoreDefaultProfile()
        oLogger.WriteToLog("cleaning profile", , 2)

        oLogger.WriteToLog("disabling account", , 3)
        If oUser.DisableAccount() Then
            oLogger.WriteToLog("ok", , 4)
        Else
            oLogger.WriteToLog("failed", , 4)
            oLogger.WriteToLog(oUser.LastError, , 5)
        End If

        oLogger.WriteToLog("removing current profile", , 3)
        If clsControlledUserProfile.DeleteControlledUserProfile Then
            oLogger.WriteToLog("ok", , 4)
        Else
            oLogger.WriteToLog("failed", , 4)
            oLogger.WriteToLog(clsControlledUserProfile.LastError, , 5)
        End If

        oLogger.WriteToLog("restoring default profile", , 3)
        If clsControlledUserProfile.RestoreControlledUserProfile Then
            oLogger.WriteToLog("ok", , 4)
        Else
            oLogger.WriteToLog("failed", , 4)
            oLogger.WriteToLog(clsControlledUserProfile.LastError, , 5)
        End If

        oLogger.WriteToLog("enabling account", , 3)
        If oUser.EnableAccount() Then
            oLogger.WriteToLog("ok", , 4)
        Else
            oLogger.WriteToLog("failed", , 4)
            oLogger.WriteToLog(oUser.LastError, , 5)
        End If
    End Sub
End Module
