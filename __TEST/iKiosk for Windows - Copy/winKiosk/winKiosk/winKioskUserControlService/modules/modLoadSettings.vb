Module modLoadSettings
    Public Sub Loadsettings()
        'Loading settings
        oLogger.WriteToLog("Loading settings", , 0)
        g_ControlledUser_Username = clsSettings.ControlledUser_Username
        If g_ControlledUser_Username = "" Then
            g_ControlledUser_Username = c_Defaults__ControlledUser
            clsSettings.ControlledUser_Username = c_Defaults__ControlledUser
        End If
        oLogger.WriteToLog("Controlled user", , 1)
        oLogger.WriteToLog(g_ControlledUser_Username, , 2)

        g_IdleTime_Max = clsSettings.IdleTime_Max
        If g_IdleTime_Max <= 0 Then
            g_IdleTime_Max = c_Defaults__MaxIdleTime
            clsSettings.IdleTime_Max = c_Defaults__MaxIdleTime
        End If
        oLogger.WriteToLog("Max idle time", , 1)
        oLogger.WriteToLog(g_IdleTime_Max.ToString, , 2)

        g_IdleTime_Trigger = clsSettings.IdleTime_Trigger
        If g_IdleTime_Trigger <= 0 Then
            g_IdleTime_Trigger = c_Defaults__IdleTimeTrigger
            clsSettings.IdleTime_Trigger = c_Defaults__IdleTimeTrigger
        End If
        If g_IdleTime_Trigger > g_IdleTime_Max Then
            g_IdleTime_Trigger = g_IdleTime_Max
            clsSettings.IdleTime_Trigger = c_Defaults__IdleTimeTrigger
        End If
        oLogger.WriteToLog("Idle time trigger", , 1)
        oLogger.WriteToLog(g_IdleTime_Trigger.ToString, , 2)

        clsSettings.IdleTime_Value = 0
    End Sub
End Module
