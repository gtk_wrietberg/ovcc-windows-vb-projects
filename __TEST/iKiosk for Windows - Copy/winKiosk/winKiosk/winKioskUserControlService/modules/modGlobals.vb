Module modGlobals
    'Defaults settings
    Public ReadOnly c_Defaults__ControlledUser As String = "ourguest"
    Public ReadOnly c_Defaults__MaxIdleTime As Integer = 300 'seconds
    Public ReadOnly c_Defaults__IdleTimeTrigger As Integer = 60 'seconds

    'some stuff
    Public ReadOnly c_LogoffCommand As String = "C:\Windows\System32\logoff.exe"
    Public ReadOnly c_iBAHN__Root As String = "C:\iBAHN\winKiosk"
    Public ReadOnly c_iBAHN__ControlledUser_CleanProfile As String = "\_clean_profile"
    Public ReadOnly c_PROCESS__winKiosk_IdleTime As String = "winKiosk_IdleTime"
    Public ReadOnly c_PROCESS__winKiosk As String = "winKiosk"

    Public oLogger As clsLogger
    Public WTSAPI As clsWTSAPI
    Public oUser As clsWindowsUser

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_ControlledUser_Username As String
    Public g_IdleTime_Trigger As Integer
    Public g_IdleTime_Max As Integer

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = "c:\iBAHN\_logs"
        g_LogFileDirectory &= "\winKioskUserControlService"

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"


        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(c_iBAHN__Root & c_iBAHN__ControlledUser_CleanProfile)


        oLogger = New clsLogger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))


        Loadsettings()
    End Sub
End Module
