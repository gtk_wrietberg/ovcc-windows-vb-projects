Imports Microsoft.Win32

Public Class clsSettings
    'These settings can be only written only by an admin
    'Special settings can be controlled by the ourguest user too
    Private Shared ReadOnly c_Setting__Settings_Reload As String = "Settings_Reload"

    Private Shared ReadOnly c_Setting__Logoff_Activate As String = "Logoff_Activate"
    Private Shared ReadOnly c_Setting__Logoff_IsActivated As String = "Logoff_IsActivated"

    Private Shared ReadOnly c_Setting__ControlledUser_CurrentSessionId As String = "ControlledUser_CurrentSessionId"
    Private Shared ReadOnly c_Setting__ControlledUser_Username As String = "ControlledUser_Username"
    Private Shared ReadOnly c_Setting__ControlledUser_IsLoggedIn As String = "ControlledUser_IsLoggedIn"

    Private Shared ReadOnly c_Setting__IdleTime_Trigger As String = "IdleTime_Trigger"
    Private Shared ReadOnly c_SpecialSetting__IdleTime_WarningTriggered As String = "IdleTime_WarningTriggered"
    Private Shared ReadOnly c_Setting__IdleTime_Max As String = "IdleTime_Max"
    Private Shared ReadOnly c_SpecialSetting__IdleTime_Value As String = "IdleTime_Value"

    Public Shared Property Settings_Reload() As Boolean
        Get
            Return clsRegistryData.LoadSettingAsBoolean(c_Setting__Settings_Reload)
        End Get
        Set(ByVal value As Boolean)
            clsRegistryData.SaveSettingAsBoolean(c_Setting__Settings_Reload, value)
        End Set
    End Property

    Public Shared Property Logoff_Activate() As Boolean
        Get
            Return clsRegistryData.LoadSettingAsBoolean(c_Setting__Logoff_Activate)
        End Get
        Set(ByVal value As Boolean)
            clsRegistryData.SaveSettingAsBoolean(c_Setting__Logoff_Activate, value)
        End Set
    End Property

    Public Shared Property ControlledUser_CurrentSessionId() As Integer
        Get
            Return clsRegistryData.LoadSettingAsInteger(c_Setting__ControlledUser_CurrentSessionId)
        End Get
        Set(ByVal value As Integer)
            clsRegistryData.SaveSettingAsInteger(c_Setting__ControlledUser_CurrentSessionId, value)
        End Set
    End Property

    Public Shared Property Logoff_IsActivated() As Boolean
        Get
            Return clsRegistryData.LoadSettingAsBoolean(c_Setting__Logoff_IsActivated)
        End Get
        Set(ByVal value As Boolean)
            clsRegistryData.SaveSettingAsBoolean(c_Setting__Logoff_IsActivated, value)
        End Set
    End Property

    Public Shared Property ControlledUser_Username() As String
        Get
            Return clsRegistryData.LoadSettingAsString(c_Setting__ControlledUser_Username)
        End Get
        Set(ByVal value As String)
            clsRegistryData.SaveSettingAsString(c_Setting__ControlledUser_Username, value)
        End Set
    End Property

    Public Shared Property ControlledUser_IsLoggedIn() As Boolean
        Get
            Return clsRegistryData.LoadSettingAsBoolean(c_Setting__ControlledUser_IsLoggedIn)
        End Get
        Set(ByVal value As Boolean)
            clsRegistryData.SaveSettingAsBoolean(c_Setting__ControlledUser_IsLoggedIn, value)
        End Set
    End Property

    Public Shared Property IdleTime_Value() As Integer
        Get
            Return clsRegistryDataSpecial.LoadSettingAsInteger(c_SpecialSetting__IdleTime_Value)
        End Get
        Set(ByVal value As Integer)
            clsRegistryDataSpecial.SaveSettingAsInteger(c_SpecialSetting__IdleTime_Value, value)
        End Set
    End Property

    Public Shared Property IdleTime_Trigger() As Integer
        Get
            Return clsRegistryData.LoadSettingAsInteger(c_Setting__IdleTime_Trigger)
        End Get
        Set(ByVal value As Integer)
            clsRegistryData.SaveSettingAsInteger(c_Setting__IdleTime_Trigger, value)
        End Set
    End Property

    Public Shared Property IdleTime_WarningTriggered() As Boolean
        Get
            Return clsRegistryDataSpecial.LoadSettingAsBoolean(c_SpecialSetting__IdleTime_WarningTriggered)
        End Get
        Set(ByVal value As Boolean)
            clsRegistryDataSpecial.SaveSettingAsBoolean(c_SpecialSetting__IdleTime_WarningTriggered, value)
        End Set
    End Property

    Public Shared Property IdleTime_Max() As Integer
        Get
            Return clsRegistryData.LoadSettingAsInteger(c_Setting__IdleTime_Max)
        End Get
        Set(ByVal value As Integer)
            clsRegistryData.SaveSettingAsInteger(c_Setting__IdleTime_Max, value)
        End Set
    End Property


    Private Class clsRegistryData
        Private Shared ReadOnly c_Key__Root As String = "SOFTWARE\\iBAHN\\winKiosk"

        Private Shared Function ReadRegistry(ByVal sValueName As String, Optional ByVal sDefaultValue As String = "") As String
            Dim regKey As RegistryKey
            Dim regRet As String = sDefaultValue

            Try
                regKey = Registry.LocalMachine.OpenSubKey(c_Key__Root, True)
                If regKey Is Nothing Then
                    regKey = Registry.LocalMachine.CreateSubKey(c_Key__Root)
                End If

                regRet = regKey.GetValue(sValueName, sDefaultValue)
            Catch ex As Exception

            End Try

            Return regRet
        End Function

        Private Shared Sub WriteRegistry(ByVal sValueName As String, ByVal oValue As Object, Optional ByVal ValueKind As RegistryValueKind = RegistryValueKind.String)
            Dim regKey As RegistryKey

            Try
                regKey = Registry.LocalMachine.OpenSubKey(c_Key__Root, True)
                If regKey Is Nothing Then
                    regKey = Registry.LocalMachine.CreateSubKey(c_Key__Root)
                End If

                regKey.SetValue(sValueName, oValue, ValueKind)
            Catch ex As Exception

            End Try
        End Sub

        Public Shared Function LoadSettingAsBoolean(ByVal sSettingName As String, Optional ByVal DefaultValue As Boolean = False) As Boolean
            Dim sValue As String

            If DefaultValue Then
                sValue = ReadRegistry(sSettingName, "yes")
            Else
                sValue = ReadRegistry(sSettingName, "no")
            End If

            If sValue = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SaveSettingAsBoolean(ByVal sSettingName As String, ByVal bValue As Boolean)
            If bValue Then
                WriteRegistry(sSettingName, "yes")
            Else
                WriteRegistry(sSettingName, "no")
            End If
        End Sub

        Public Shared Function LoadSettingAsString(ByVal sSettingName As String, Optional ByVal DefaultValue As String = "") As String
            Return ReadRegistry(sSettingName, DefaultValue)
        End Function

        Public Shared Sub SaveSettingAsString(ByVal sSettingName As String, ByVal sValue As String)
            WriteRegistry(sSettingName, sValue)
        End Sub

        Public Shared Function LoadSettingAsInteger(ByVal sSettingName As String, Optional ByVal DefaultValue As Integer = 0) As Integer
            Dim sTmp As String = ReadRegistry(sSettingName, "")
            Dim iTmp As Integer = DefaultValue

            Try
                iTmp = Integer.Parse(sTmp)
            Catch ex As Exception

            End Try


            Return iTmp
        End Function

        Public Shared Sub SaveSettingAsInteger(ByVal sSettingName As String, ByVal iValue As Integer)
            WriteRegistry(sSettingName, iValue, RegistryValueKind.DWord)
        End Sub
    End Class

    Private Class clsRegistryDataSpecial
        Private Shared ReadOnly c_Key__Root As String = "SOFTWARE\\iBAHN\\winKiosk\\_"

        Private Shared Function ReadRegistry(ByVal sValueName As String, Optional ByVal sDefaultValue As String = "") As String
            Dim regKey As RegistryKey

            regKey = Registry.LocalMachine.OpenSubKey(c_Key__Root, True)
            If regKey Is Nothing Then
                regKey = Registry.LocalMachine.CreateSubKey(c_Key__Root)
            End If

            Return regKey.GetValue(sValueName, sDefaultValue)
        End Function

        Private Shared Sub WriteRegistry(ByVal sValueName As String, ByVal oValue As Object, Optional ByVal ValueKind As RegistryValueKind = RegistryValueKind.String)
            Dim regKey As RegistryKey

            regKey = Registry.LocalMachine.OpenSubKey(c_Key__Root, True)
            If regKey Is Nothing Then
                regKey = Registry.LocalMachine.CreateSubKey(c_Key__Root)
            End If

            regKey.SetValue(sValueName, oValue, ValueKind)
        End Sub

        Public Shared Function LoadSettingAsBoolean(ByVal sSettingName As String, Optional ByVal DefaultValue As Boolean = False) As Boolean
            Dim sValue As String

            If DefaultValue Then
                sValue = ReadRegistry(sSettingName, "yes")
            Else
                sValue = ReadRegistry(sSettingName, "no")
            End If

            If sValue = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SaveSettingAsBoolean(ByVal sSettingName As String, ByVal bValue As Boolean)
            If bValue Then
                WriteRegistry(sSettingName, "yes")
            Else
                WriteRegistry(sSettingName, "no")
            End If
        End Sub

        Public Shared Function LoadSettingAsString(ByVal sSettingName As String, Optional ByVal DefaultValue As String = "") As String
            Return ReadRegistry(sSettingName, DefaultValue)
        End Function

        Public Shared Sub SaveSettingAsString(ByVal sSettingName As String, ByVal sValue As String)
            WriteRegistry(sSettingName, sValue)
        End Sub

        Public Shared Function LoadSettingAsInteger(ByVal sSettingName As String, Optional ByVal DefaultValue As Integer = 0) As Integer
            Dim sTmp As String = ReadRegistry(sSettingName, "")
            Dim iTmp As Integer = DefaultValue

            Try
                iTmp = Integer.Parse(sTmp)
            Catch ex As Exception

            End Try


            Return iTmp
        End Function

        Public Shared Sub SaveSettingAsInteger(ByVal sSettingName As String, ByVal iValue As Integer)
            WriteRegistry(sSettingName, iValue, RegistryValueKind.DWord)
        End Sub
    End Class

End Class
