Public Class clsHelpers
    Public Shared Function isProcessRunning(ByVal ProcessName As String) As Boolean
        Dim p() As System.Diagnostics.Process
        p = System.Diagnostics.Process.GetProcessesByName(ProcessName)
        Return (p.Length > 0)
    End Function

    Public Shared Function getApplicationPath() As String
        Dim exePath As String = System.Reflection.Assembly.GetExecutingAssembly.Location
        exePath = exePath.Substring(0, exePath.LastIndexOf("\") + 1)
        Return exePath
    End Function

    Public Shared Function getFullPathForProcessName(ByVal ProcessName As String) As String
        Dim appPath As String = getApplicationPath()

        Return appPath & ProcessName & ".exe"
    End Function
End Class
