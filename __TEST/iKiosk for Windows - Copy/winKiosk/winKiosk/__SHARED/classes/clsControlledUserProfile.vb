Imports System.IO

Public Class clsControlledUserProfile
    Private Shared ReadOnly c_USERS As String = "c:\Users\"
    Private Shared mLastError As String = ""
    Private Shared oFileCleanup As FileCleanup

    Public Shared ReadOnly Property LastError() As String
        Get
            Dim tmpS As String = mLastError
            mLastError = ""

            Return tmpS
        End Get
    End Property

    Public Shared Function DeleteControlledUserProfile() As Boolean
        If g_ControlledUser_Username <> "" And IO.Directory.Exists(c_USERS & g_ControlledUser_Username) Then
            oFileCleanup = New FileCleanup

            oFileCleanup.Folder = c_USERS & g_ControlledUser_Username
            oFileCleanup.LoadFiles("*", 0, True)
            oFileCleanup.DeleteFilesAndFolders()
            oFileCleanup.WriteReport()

            Return True
        Else
            mLastError = "Controlled User name empty or folder '" & c_USERS & g_ControlledUser_Username & "' does not exist!"

            Return False
        End If
    End Function

    Public Shared Function RestoreControlledUserProfile() As Boolean
        If g_ControlledUser_Username <> "" And IO.Directory.Exists(c_iBAHN__Root & c_iBAHN__ControlledUser_CleanProfile & "\" & g_ControlledUser_Username) Then
            Try
                My.Computer.FileSystem.CopyDirectory(c_iBAHN__Root & c_iBAHN__ControlledUser_CleanProfile & "\" & g_ControlledUser_Username, c_USERS & "\" & g_ControlledUser_Username, True)
            Catch ex As Exception
                mLastError = ex.Message
                Return False
            End Try

            Return True
        Else
            mLastError = "Controlled User name empty or folder '" & c_iBAHN__Root & c_iBAHN__ControlledUser_CleanProfile & "\" & g_ControlledUser_Username & "' does not exist!"

            Return False
        End If
    End Function

    Private Shared Sub DeleteDirectory(ByVal directory As DirectoryInfo, Optional ByVal forceReadOnlyDelete As Boolean = True)
        If forceReadOnlyDelete Then
            RemoveReadOnlyAttributeFromFiles(directory, True)
        End If

        directory.Delete(True)
    End Sub

    Private Shared Sub RemoveReadOnlyAttributeFromFiles(ByVal directory As DirectoryInfo, ByVal recursive As Boolean)
        Dim readOnlyFiles() As FileInfo = directory.GetFiles()

        For Each file As FileInfo In readOnlyFiles
            file.Attributes = FileAttributes.Normal
        Next

        If recursive Then
            For Each subDirectory As DirectoryInfo In directory.GetDirectories()
                RemoveReadOnlyAttributeFromFiles(subDirectory, True)
            Next
        End If
    End Sub
End Class
