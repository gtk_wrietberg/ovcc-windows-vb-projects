Imports Microsoft.Win32

Public Class Cleanup
    Private Const cDoesNotExistString As String = "(does not exist)"

    Private mLocations As New List(Of Cleanup_Location)
    Private mCountLocations As Integer

    Private mLocationTotalFileSize As Long
    Private mLocationTotalDeletedFileSize As Long
    Private mLocationTotalDelayedFileSize As Long

    Public Sub New()
        mLocations = New List(Of Cleanup_Location)

        mCountLocations = 0

        mLocationTotalFileSize = 0
        mLocationTotalDeletedFileSize = 0
        mLocationTotalDelayedFileSize = 0
    End Sub

#Region "properties"
    Public ReadOnly Property NumberOfLocations() As Integer
        Get
            Return mLocations.Count
        End Get
    End Property

    Public ReadOnly Property TotalFileSize() As Long
        Get
            Return mLocationTotalFileSize
        End Get
    End Property

    Public ReadOnly Property TotalDeletedFileSize() As Long
        Get
            Return mLocationTotalDeletedFileSize
        End Get
    End Property

    Public ReadOnly Property TotalDelayedFileSize() As Long
        Get
            Return mLocationTotalDelayedFileSize
        End Get
    End Property
#End Region

#Region "reset counters"
    Public Sub ResetCounters()
        mCountLocations = 0
    End Sub

    Public Sub ResetCounterLocations()
        mCountLocations = 0
    End Sub
#End Region

#Region "setters"
    Public Sub AddLocation(ByVal sPath As String, ByVal bRecursive As Boolean, ByVal sFileNamePattern As String, ByVal iMaxFileAge As Integer)
        Dim oConfig_Location As New Cleanup_Location

        oConfig_Location.Path = sPath
        oConfig_Location.Recursive = bRecursive
        oConfig_Location.FileNamePattern = sFileNamePattern
        oConfig_Location.MaxFileAge = iMaxFileAge

        mLocations.Add(oConfig_Location)
    End Sub
#End Region

#Region "getters"
    Public Function GetNextLocation(ByRef sPath As String, ByRef iFound As Integer, ByRef iDeleted As Integer, ByRef iDelayed As Integer) As Boolean
        If mCountLocations > mLocations.Count - 1 Then
            Return False
        End If

        mCountLocations += 1

        sPath = mLocations.Item(mCountLocations - 1).Path
        iFound = mLocations.Item(mCountLocations - 1).Found
        iDeleted = mLocations.Item(mCountLocations - 1).Deleted
        iDelayed = mLocations.Item(mCountLocations - 1).Delayed

        Return True
    End Function
#End Region

#Region "cleaners"
#Region "public"
    Public Function CleanUp_Locations() As Boolean
        Dim oFileCleanUp As FileCleanup
        Dim oLocation As Cleanup_Location
        Dim bRet As Boolean

        bRet = True

        For Each oLocation In mLocations
            oFileCleanUp = New FileCleanup

            oFileCleanUp.Folder = oLocation.Path
            oFileCleanUp.LoadFiles(oLocation.FileNamePattern, oLocation.MaxFileAge, oLocation.Recursive)

            oLocation.Found = oFileCleanUp.FilesCount
            oLocation.TotalFileSize = oFileCleanUp.FilesTotalSize
            mLocationTotalFileSize += oFileCleanUp.FilesTotalSize

            oFileCleanUp.DeleteFilesAndFolders()

            oLocation.Deleted = oFileCleanUp.FilesDeleted
            oLocation.TotalDeletedFileSize = oFileCleanUp.FilesDeletedTotalSize
            mLocationTotalDeletedFileSize += oFileCleanUp.FilesDeletedTotalSize
        Next

        Return bRet
    End Function
#End Region
#End Region

#Region "private classes"
    Private Class Cleanup_Location
        Private mPath As String
        Private mRecursive As Boolean
        Private mFileNamePattern As String
        Private mMaxFileAge As Integer

        Private mFound As Integer
        Private mDeleted As Integer
        Private mDelayed As Integer

        Private mTotalFileSize As Long
        Private mTotalDeletedFileSize As Long
        Private mTotalDelayedFileSize As Long

        Public Property Path() As String
            Get
                Return mPath
            End Get
            Set(ByVal value As String)
                mPath = value
            End Set
        End Property

        Public Property Recursive() As Boolean
            Get
                Return mRecursive
            End Get
            Set(ByVal value As Boolean)
                mRecursive = value
            End Set
        End Property

        Public Property FileNamePattern() As String
            Get
                Return mFileNamePattern
            End Get
            Set(ByVal value As String)
                mFileNamePattern = value
            End Set
        End Property

        Public Property MaxFileAge() As Integer
            Get
                Return mMaxFileAge
            End Get
            Set(ByVal value As Integer)
                mMaxFileAge = value
            End Set
        End Property

        Public Property Found() As Integer
            Get
                Return mFound
            End Get
            Set(ByVal value As Integer)
                mFound = value
            End Set
        End Property

        Public Property Deleted() As Integer
            Get
                Return mDeleted
            End Get
            Set(ByVal value As Integer)
                mDeleted = value
            End Set
        End Property

        Public Property Delayed() As Integer
            Get
                Return mDelayed
            End Get
            Set(ByVal value As Integer)
                mDelayed = value
            End Set
        End Property

        Public Property TotalFileSize() As Long
            Get
                Return mTotalFileSize
            End Get
            Set(ByVal value As Long)
                mTotalFileSize = value
            End Set
        End Property

        Public Property TotalDeletedFileSize() As Long
            Get
                Return mTotalDeletedFileSize
            End Get
            Set(ByVal value As Long)
                mTotalDeletedFileSize = value
            End Set
        End Property

        Public Property TotalDelayedFileSize() As Long
            Get
                Return mTotalDelayedFileSize
            End Get
            Set(ByVal value As Long)
                mTotalDelayedFileSize = value
            End Set
        End Property
    End Class
#End Region
End Class
