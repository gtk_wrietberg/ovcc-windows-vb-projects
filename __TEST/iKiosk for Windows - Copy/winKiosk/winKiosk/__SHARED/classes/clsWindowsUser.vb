Public Class clsWindowsUser
    Private Enum ADS_USER_FLAG_ENUM
        ADS_UF_SCRIPT = 1 ' 0x1
        ADS_UF_ACCOUNTDISABLE = 2 ' 0x2
        ADS_UF_HOMEDIR_REQUIRED = 8 ' 0x8
        ADS_UF_LOCKOUT = 16 ' 0x10
        ADS_UF_PASSWD_NOTREQD = 32 ' 0x20
        ADS_UF_PASSWD_CANT_CHANGE = 64 ' 0x40
        ADS_UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED = 128 ' 0x80
        ADS_UF_TEMP_DUPLICATE_ACCOUNT = 256 ' 0x100
        ADS_UF_NORMAL_ACCOUNT = 512 ' 0x200
        ADS_UF_INTERDOMAIN_TRUST_ACCOUNT = 2048 ' 0x800
        ADS_UF_WORKSTATION_TRUST_ACCOUNT = 4096 ' 0x1000
        ADS_UF_SERVER_TRUST_ACCOUNT = 8192 ' 0x2000
        ADS_UF_DONT_EXPIRE_PASSWD = 65536 ' 0x10000
        ADS_UF_MNS_LOGON_ACCOUNT = 131072 ' 0x20000
        ADS_UF_SMARTCARD_REQUIRED = 262144 ' 0x40000
        ADS_UF_TRUSTED_FOR_DELEGATION = 524288 ' 0x80000
        ADS_UF_NOT_DELEGATED = 1048576 ' 0x100000
        ADS_UF_USE_DES_KEY_ONLY = 2097152 ' 0x200000
        ADS_UF_DONT_REQUIRE_PREAUTH = 4194304 ' 0x400000
        ADS_UF_PASSWORD_EXPIRED = 8388608 ' 0x800000
        ADS_UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION = 16777216 ' 0x1000000
    End Enum

    Private mUsername As String = ""
    Private mComputerName As String = ""

    Private Shared mLastError As String = ""


    Public Sub New()
        mUsername = ""

        GetComputerName()
    End Sub

    Public Sub New(ByVal UserName As String)
        mUsername = UserName

        GetComputerName()
    End Sub

    Private Sub GetComputerName()
        mComputerName = Environment.MachineName
    End Sub

    Public Property UserName() As String
        Get
            Return mUsername
        End Get
        Set(ByVal value As String)
            mUsername = value
        End Set
    End Property

    Public ReadOnly Property ComputerName()
        Get
            Return mComputerName
        End Get
    End Property

    Public ReadOnly Property LastError() As String
        Get
            Dim tmpS As String = mLastError
            mLastError = ""
            Return tmpS
        End Get
    End Property

    Public Function Create(ByVal sFullName As String, ByVal sPassWord As String, Optional ByVal sGroup As String = "Users") As Boolean
        Try
            Dim oSystem As Object, oUser As Object, oGroup As Object

            If mUsername <> "" Then

                oSystem = GetObject("WinNT://" & mComputerName)
                oUser = oSystem.Create("user", mUsername)
                oUser.FullName = sFullName
                oUser.SetPassword(sPassWord)
                oUser.SetInfo()

                oGroup = GetObject("WinNT://" & mComputerName & "/" & sGroup)
                oGroup.Add("WinNT://" & mComputerName & "/" & mUsername)

                oGroup = Nothing
                oUser = Nothing
                oSystem = Nothing

                Return True
            End If
        Catch ex As Exception
            mLastError = ex.Message
        End Try

        Return False
    End Function

    Public Function HideInLogonScreen() As Boolean
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", mUsername, 0)

            Return True
        Catch ex As Exception
            mLastError = ex.Message
        End Try
    End Function

    Public Function GetPasswordExpiry() As Integer
        Dim oUser As Object
        Dim iPassExp As Integer = -1

        Try
            oUser = GetObject("WinNT://" & mComputerName & "/" & mUsername)

            iPassExp = Int(oUser.MaxPasswordAge / 86400) - Int(oUser.PasswordAge / 86400)
        Catch ex As Exception
            mLastError = ex.Message
        End Try


        Return iPassExp
    End Function

    Public Function DisablePasswordExpiry() As Boolean
        Try
            Dim oUser As Object
            Dim iFlagsOld As Integer, iFlagsNew As Integer

            oUser = GetObject("WinNT://" & mComputerName & "/" & mUsername)

            iFlagsOld = oUser.UserFlags
            iFlagsNew = iFlagsOld Or ADS_USER_FLAG_ENUM.ADS_UF_DONT_EXPIRE_PASSWD
            oUser.UserFlags = iFlagsNew
            oUser.SetInfo()

            Return True
        Catch ex As Exception
            mLastError = ex.Message
        End Try

        Return False
    End Function

    Public Function EnableAccount() As Boolean
        Try
            Dim oUser As Object
            Dim iFlagsOld As Integer, iFlagsNew As Integer

            oUser = GetObject("WinNT://" & mComputerName & "/" & mUsername)

            iFlagsOld = oUser.UserFlags
            iFlagsNew = iFlagsOld And Not ADS_USER_FLAG_ENUM.ADS_UF_ACCOUNTDISABLE
            oUser.UserFlags = iFlagsNew
            oUser.SetInfo()

            Return True
        Catch ex As Exception
            mLastError = ex.Message
        End Try

        Return False
    End Function

    Public Function DisableAccount() As Boolean
        Try
            Dim oUser As Object
            Dim iFlagsOld As Integer, iFlagsNew As Integer

            oUser = GetObject("WinNT://" & mComputerName & "/" & mUsername)

            iFlagsOld = oUser.UserFlags
            iFlagsNew = iFlagsOld Or ADS_USER_FLAG_ENUM.ADS_UF_ACCOUNTDISABLE
            oUser.UserFlags = iFlagsNew
            oUser.SetInfo()

            Return True
        Catch ex As Exception
            mLastError = ex.Message
        End Try

        Return False
    End Function

    Public Function ChangePassword(ByVal sNewPassWord As String) As Boolean
        Try
            Dim oUser As Object

            oUser = GetObject("WinNT://" & mComputerName & "/" & mUsername)
            oUser.SetPassword(sNewPassWord)

            oUser = Nothing

            Return True
        Catch ex As Exception
            mLastError = ex.Message
        End Try

        Return False
    End Function

    Public Function ExistsInGroup(Optional ByVal sGroup As String = "Users") As Boolean
        Try
            Dim oUser As Object, oGroup As Object

            oGroup = GetObject("WinNT://" & mComputerName & "/" & sGroup & ",group")

            For Each oUser In oGroup.Members
                If Trim(mUsername) = Trim(oUser.Name) Then
                    Return True
                End If
            Next

            oGroup = Nothing
            oUser = Nothing

            Return True
        Catch ex As Exception
            mLastError = ex.Message
        End Try

        Return False
    End Function
End Class
