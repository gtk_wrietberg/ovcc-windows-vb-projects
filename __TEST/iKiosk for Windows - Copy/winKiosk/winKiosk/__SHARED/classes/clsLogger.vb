Public Class clsLogger
    Private ReadOnly DefaultLogFile As String = "AU_cust.log"
    Private mLogFilePath As String
    Private mLogFile As String = ""
    Private pPrevDepth As Integer = 0
    Private pDoNotStoreLogLevel As Boolean = False

    Public Enum MESSAGE_TYPE
        LOG_DEFAULT = 0
        LOG_WARNING = 1
        LOG_ERROR = 2
        LOG_DEBUG = 6
    End Enum

    Public Property LogFileDirectory() As String
        Get
            Return mLogFilePath
        End Get
        Set(ByVal value As String)
            If value.EndsWith("\") Then
                mLogFilePath = value
            Else
                mLogFilePath = value & "\"
            End If
        End Set
    End Property

    Public Property LogFileName() As String
        Get
            Return mLogFile
        End Get
        Set(ByVal value As String)
            mLogFile = value
        End Set
    End Property

    Public Sub WriteToLogRelative(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iRelativeDepth As Integer = 0)
        pDoNotStoreLogLevel = True
        WriteToLog(sMessage, cMessageType, pPrevDepth + iRelativeDepth)
    End Sub

    Public Sub WriteToLog(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0)
        Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String, iDepthStep As Integer

        If iDepth < 0 Then
            iDepth = 0
        End If

        sMessage = Trim(sMessage)

        Select Case cMessageType
            Case MESSAGE_TYPE.LOG_WARNING
                sMsgTypePrefix = "[*] "
            Case MESSAGE_TYPE.LOG_ERROR
                sMsgTypePrefix = "[!] "
            Case MESSAGE_TYPE.LOG_DEBUG
                sMsgTypePrefix = "[#] "
            Case MESSAGE_TYPE.LOG_DEFAULT
                sMsgTypePrefix = "[.] "
            Case Else
                sMsgTypePrefix = "[?] "
        End Select

        sMsgDatePrefix = Now.ToString & " - "

        sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

        If iDepth < pPrevDepth Then
            For iDepthStep = 1 To iDepth
                sMsgPrefix = sMsgPrefix & "| "
            Next

            sMsgPrefix = sMsgPrefix & vbCrLf
            sMsgPrefix = sMsgPrefix & sMsgTypePrefix & sMsgDatePrefix
        End If

        If iDepth > 0 Then
            For iDepthStep = 1 To iDepth - 1
                sMsgPrefix = sMsgPrefix & "| "
            Next

            sMsgPrefix = sMsgPrefix & "|-"
        End If

        UpdateLogfile(sMsgPrefix & sMessage)

        If Not pDoNotStoreLogLevel Then
            pPrevDepth = iDepth
        End If

        pDoNotStoreLogLevel = False
    End Sub

    Public Sub WriteToLogWithoutDate(ByVal sMessage As String)
        UpdateLogfile(sMessage)
    End Sub

    Public Sub WriteEmptyLineToLog()
        WriteToLogWithoutDate(" ")
    End Sub

    Private Sub UpdateLogfile(ByVal sString As String)
        Try
            Dim sw As New IO.StreamWriter(mLogFilePath & mLogFile, True)
            sw.WriteLine(sString)
            sw.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub New()
        mLogFile = DefaultLogFile
        mLogFilePath = ""
    End Sub
End Class
