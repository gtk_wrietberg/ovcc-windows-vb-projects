Imports System.IO
Imports System.Runtime.InteropServices

Public Class FileCleanup
    Private mDEBUGGING As Boolean

    Private mFolder As String
    Private mAllFiles As New List(Of String)
    Private mSubFolders As New List(Of String)
    Private mAllFilesSizes As New List(Of Long)
    Private mDeletedFiles As New List(Of String)
    Private mFailedFiles As New List(Of String)
    Private mDeletedFolders As New List(Of String)
    Private mFailedFolders As New List(Of String)

    Private mAllFilesSize As Long
    Private mDeletedFilesSize As Long
    Private mFailedFilesSize As Long

    Private Enum DeleteResult As Integer
        NothingHappenedYet = 0
        Deleted = 1
        NotDeletedError = 2
    End Enum

    Public Sub New()
        mDEBUGGING = False

        mFolder = ""

        mAllFilesSize = 0
        mDeletedFilesSize = 0
        mFailedFilesSize = 0

        mAllFiles.Clear()
        mSubFolders.Clear()
        mAllFilesSizes.Clear()
        mDeletedFiles.Clear()
        mFailedFiles.Clear()
        mDeletedFolders.Clear()
        mFailedFolders.Clear()
    End Sub

    Public Sub Reset()
        mAllFilesSize = 0
        mDeletedFilesSize = 0
        mFailedFilesSize = 0

        mAllFiles.Clear()
        mSubFolders.Clear()
        mAllFilesSizes.Clear()
        mDeletedFiles.Clear()
        mFailedFiles.Clear()
        mDeletedFolders.Clear()
        mFailedFolders.Clear()
    End Sub

#Region "properties"
    Public Property Debugging() As Boolean
        Get
            Return mDEBUGGING
        End Get
        Set(ByVal value As Boolean)
            mDEBUGGING = value
        End Set
    End Property

    Public Property Folder() As String
        Get
            Return mFolder
        End Get
        Set(ByVal value As String)
            mFolder = value
        End Set
    End Property

    Public ReadOnly Property AllFiles() As List(Of String)
        Get
            Return mAllFiles
        End Get
    End Property

    Public ReadOnly Property FilesCount() As Integer
        Get
            Return mAllFiles.Count
        End Get
    End Property

    Public ReadOnly Property FilesDeleted() As Integer
        Get
            Return mDeletedFiles.Count
        End Get
    End Property

    Public ReadOnly Property FilesFailed() As Integer
        Get
            Return mFailedFiles.Count
        End Get
    End Property

    Public ReadOnly Property FoldersDeleted() As Integer
        Get
            Return mDeletedFolders.Count
        End Get
    End Property

    Public ReadOnly Property FoldersFailed() As Integer
        Get
            Return mFailedFolders.Count
        End Get
    End Property

    Public ReadOnly Property FilesTotalSize() As Long
        Get
            Return mAllFilesSize
        End Get
    End Property

    Public ReadOnly Property FilesDeletedTotalSize() As Long
        Get
            Return mDeletedFilesSize
        End Get
    End Property

    Public ReadOnly Property FilesFailedTotalSize() As Long
        Get
            Return mFailedFilesSize
        End Get
    End Property
#End Region

    Public Sub LoadFiles(Optional ByVal sSearchPattern As String = "*", Optional ByVal iMaxDaysOld As Integer = 0, Optional ByVal bRecursive As Boolean = False)
        Reset()

        Dim sFolder As String, sSubFolderPattern As String, iIndex As Integer

        If InStr(mFolder, "*") > 0 Then
            iIndex = mFolder.LastIndexOf("\")
            sFolder = Left(mFolder, iIndex)

            sSubFolderPattern = mFolder
            sSubFolderPattern = sSubFolderPattern.Replace(sFolder, "")
            sSubFolderPattern = sSubFolderPattern.Replace("\", "")
            sSubFolderPattern = sSubFolderPattern.Replace("*", "")

            Dim sFolderArray() As String, sSubFolder As String, iFileAge As Integer, sSubFolderName As String

            sFolderArray = Directory.GetDirectories(sFolder)

            For Each sSubFolder In sFolderArray
                sSubFolderName = Right(sSubFolder, sSubFolder.Length - sSubFolder.LastIndexOf("\") - 1)

                If sSubFolderName.StartsWith(sSubFolderPattern, StringComparison.OrdinalIgnoreCase) Then
                    If iMaxDaysOld > 0 Then
                        iFileAge = _GetFolderAgeInDays(sSubFolder)
                        If iFileAge > iMaxDaysOld Then
                            If mDEBUGGING Then oLogger.WriteToLogRelative("searching for files in " & sSubFolder, , 1)

                            _LoadFiles(sSubFolder, sSearchPattern, iMaxDaysOld, bRecursive)
                        End If
                    Else
                        If mDEBUGGING Then oLogger.WriteToLogRelative("searching for files in " & sSubFolder, , 1)

                        _LoadFiles(sSubFolder, sSearchPattern, iMaxDaysOld, bRecursive)
                    End If
                End If
            Next
        Else
            If mDEBUGGING Then oLogger.WriteToLogRelative("searching for files in " & mFolder, , 1)

            _LoadFiles(mFolder, sSearchPattern, iMaxDaysOld, bRecursive)
        End If

        If mAllFiles.Count = 1 Then
            If mDEBUGGING Then oLogger.WriteToLogRelative("found " & mAllFiles.Count.ToString & " file (" & mAllFilesSize & " bytes)", , 2)
        Else
            If mDEBUGGING Then oLogger.WriteToLogRelative("found " & mAllFiles.Count.ToString & " files (" & mAllFilesSize & " bytes)", , 2)
        End If
    End Sub

    Private Sub _LoadFiles(ByVal sFolder As String, ByVal sSearchPattern As String, ByVal iMaxDaysOld As Integer, ByVal bRecursive As Boolean)
        Dim sFileArray() As String, sFile As String, iFileAge As Integer
        Dim sFolderArray() As String, sSubFolder As String, dSubFolder As DirectoryInfo

        If iMaxDaysOld < 0 Then iMaxDaysOld = 0

        If Directory.Exists(sFolder) Then
            If bRecursive Then
                sFolderArray = Directory.GetDirectories(sFolder)

                For Each sSubFolder In sFolderArray
                    dSubFolder = New DirectoryInfo(sSubFolder)

                    If dSubFolder.Attributes And FileAttributes.ReparsePoint Then
                        'Symbolic link! We need to ignore this!
                    Else
                        If iMaxDaysOld > 0 Then
                            iFileAge = _GetFolderAgeInDays(sSubFolder)
                            If iFileAge > iMaxDaysOld Then
                                _AddSubFolder(sSubFolder)
                                _LoadFiles(sSubFolder, sSearchPattern, iMaxDaysOld, bRecursive)
                            End If
                        ElseIf sSubFolder.StartsWith(sSearchPattern.Replace("*", ""), StringComparison.OrdinalIgnoreCase) Then
                            _AddSubFolder(sSubFolder)
                            _LoadFiles(sSubFolder, sSearchPattern, iMaxDaysOld, bRecursive)
                        End If
                    End If
                Next
            End If

            sFileArray = Directory.GetFiles(sFolder, sSearchPattern, SearchOption.TopDirectoryOnly)

            For Each sFile In sFileArray
                If iMaxDaysOld > 0 Then
                    iFileAge = _GetFileAgeInDays(sFile)
                    If iFileAge > iMaxDaysOld Then
                        _AddFile(sFile)
                    End If
                Else
                    _AddFile(sFile)
                End If
            Next
        End If
    End Sub

    Private Sub _AddSubFolder(ByVal sFolder As String)
        mSubFolders.Add(sFolder)
    End Sub

    Private Sub _AddFile(ByVal sFile As String)
        Dim lSize As Long

        lSize = _GetFileSize(sFile)

        mAllFilesSize += lSize

        mAllFilesSizes.Add(lSize)
        mAllFiles.Add(sFile)
    End Sub

    Private Function _GetFileSize(ByVal sFile As String) As Long
        Try
            Dim MyFile As New IO.FileInfo(sFile)

            Return MyFile.Length
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Function _GetFileAgeInDays(ByVal sFile As String) As Integer
        If File.Exists(sFile) Then
            Dim dLastWriteTime As Date

            dLastWriteTime = File.GetLastWriteTime(sFile)

            Return DateDiff(DateInterval.Day, dLastWriteTime, Date.Now)
        Else
            Return -1
        End If
    End Function

    Private Function _GetFolderAgeInDays(ByVal sFolder As String) As Integer
        If Directory.Exists(sFolder) Then
            Dim dLastWriteTime As Date

            dLastWriteTime = Directory.GetLastWriteTime(sFolder)

            Return DateDiff(DateInterval.Day, dLastWriteTime, Date.Now)
        Else
            Return -1
        End If
    End Function

    Public Sub DeleteFilesAndFolders()
        Dim i As Integer, sFile As String, iResult As DeleteResult

        If mDEBUGGING Then oLogger.WriteToLogRelative("deleting files and folders in " & mFolder, , 1)

        For i = 0 To mAllFiles.Count - 1
            sFile = mAllFiles.Item(i)

            iResult = _DeleteFile(i)

            Select Case iResult
                Case DeleteResult.Deleted
                    mDeletedFiles.Add(sFile)
                Case DeleteResult.NotDeletedError
                    mFailedFiles.Add(sFile)
            End Select
        Next

        Dim sFolder As String
        For i = 0 To mSubFolders.Count - 1
            sFolder = mSubFolders.Item(i)

            iResult = _DeleteFolder(i)

            Select Case iResult
                Case DeleteResult.Deleted
                    mDeletedFolders.Add(sFolder)
                Case DeleteResult.NotDeletedError
                    mFailedFolders.Add(sFolder)
            End Select
        Next

        If mDEBUGGING Then oLogger.WriteToLogRelative("done", , 2)
    End Sub

    Private Function _DeleteFile(ByVal iFileIndex As Integer) As DeleteResult
        Dim iRet As DeleteResult = DeleteResult.NotDeletedError
        Dim sFile As String
        Dim lFileSize As Long

        sFile = mAllFiles.Item(iFileIndex)
        lFileSize = mAllFilesSizes.Item(iFileIndex)

        If mDEBUGGING Then oLogger.WriteToLogRelative("deleting file: " & sFile, , 2)

        If File.Exists(sFile) Then
            Try
                _SetFileAttributesToNormal(sFile)
                File.Delete(sFile)
            Catch ex As Exception
                If mDEBUGGING Then oLogger.WriteToLogRelative("error", clsLogger.MESSAGE_TYPE.LOG_ERROR, 3)
                If mDEBUGGING Then oLogger.WriteToLogRelative("exception", clsLogger.MESSAGE_TYPE.LOG_ERROR, 4)
                If mDEBUGGING Then oLogger.WriteToLogRelative(ex.Message, clsLogger.MESSAGE_TYPE.LOG_ERROR, 5)
            End Try
            If File.Exists(sFile) Then
                If mDEBUGGING Then oLogger.WriteToLogRelative("still there", clsLogger.MESSAGE_TYPE.LOG_ERROR, 3)
                mFailedFilesSize += lFileSize
            Else
                If mDEBUGGING Then oLogger.WriteToLogRelative("gone", , 3)
                iRet = DeleteResult.Deleted
                mDeletedFilesSize += lFileSize
            End If
        Else
            If mDEBUGGING Then oLogger.WriteToLogRelative("does not exist", clsLogger.MESSAGE_TYPE.LOG_WARNING, 3)
        End If

        Return iRet
    End Function

    Private Function _DeleteFolder(ByVal iFolderIndex As Integer) As DeleteResult
        Dim iRet As DeleteResult = DeleteResult.NotDeletedError
        Dim sFolder As String

        sFolder = mSubFolders.Item(iFolderIndex)

        If mDEBUGGING Then oLogger.WriteToLogRelative("deleting folder: " & sFolder, , 2)
        If Directory.Exists(sFolder) Then
            Try
                _SetFileAttributesToNormal(sFolder)
                Directory.Delete(sFolder, True)
            Catch ex As Exception
                If mDEBUGGING Then oLogger.WriteToLogRelative("error", clsLogger.MESSAGE_TYPE.LOG_ERROR, 3)
                If mDEBUGGING Then oLogger.WriteToLogRelative("exception", clsLogger.MESSAGE_TYPE.LOG_ERROR, 4)
                If mDEBUGGING Then oLogger.WriteToLogRelative(ex.Message, clsLogger.MESSAGE_TYPE.LOG_ERROR, 5)
            End Try
            If Directory.Exists(sFolder) Then
                If mDEBUGGING Then oLogger.WriteToLogRelative("still there", clsLogger.MESSAGE_TYPE.LOG_ERROR, 3)
                iRet = DeleteResult.NotDeletedError
            Else
                If mDEBUGGING Then oLogger.WriteToLogRelative("gone", , 3)
                iRet = DeleteResult.Deleted
            End If
        Else
            If mDEBUGGING Then oLogger.WriteToLogRelative("does not exist", clsLogger.MESSAGE_TYPE.LOG_WARNING, 3)
        End If

        Return iRet
    End Function

    Private Sub _SetFileAttributesToNormal(ByVal sPath As String)
        If Directory.Exists(sPath) Then
            Dim sSubFolders() As String = Directory.GetDirectories(sPath)
            Dim sSubFolder As String

            For Each sSubFolder In sSubFolders
                Try
                    File.SetAttributes(sSubFolder, FileAttributes.Normal)
                Catch ex As Exception

                End Try

                _SetFileAttributesToNormal(sSubFolder)
            Next

            Dim sFiles() As String = Directory.GetFiles(sPath)
            Dim sFile As String

            For Each sFile In sFiles
                Try
                    File.SetAttributes(sFile, FileAttributes.Normal)
                Catch ex As Exception

                End Try
            Next
        ElseIf File.Exists(sPath) Then
            Try
                File.SetAttributes(sPath, FileAttributes.Normal)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Public Sub WriteReport()
        oLogger.WriteToLogRelative("cleanup report", , 1)

        oLogger.WriteToLogRelative("files", , 2)
        oLogger.WriteToLogRelative("found", , 3)
        oLogger.WriteToLogRelative(mAllFiles.Count.ToString, , 4)
        oLogger.WriteToLogRelative("deleted", , 3)
        oLogger.WriteToLogRelative(mDeletedFiles.Count.ToString, , 4)
        oLogger.WriteToLogRelative("failed", , 3)
        oLogger.WriteToLogRelative(mFailedFiles.Count.ToString, , 4)

        oLogger.WriteToLogRelative("folders", , 2)
        oLogger.WriteToLogRelative("found", , 3)
        oLogger.WriteToLogRelative(mSubFolders.Count.ToString, , 4)
        oLogger.WriteToLogRelative("deleted", , 3)
        oLogger.WriteToLogRelative(mDeletedFolders.Count.ToString, , 4)
        oLogger.WriteToLogRelative("failed", , 3)
        oLogger.WriteToLogRelative(mFailedFolders.Count.ToString, , 4)
    End Sub
End Class
