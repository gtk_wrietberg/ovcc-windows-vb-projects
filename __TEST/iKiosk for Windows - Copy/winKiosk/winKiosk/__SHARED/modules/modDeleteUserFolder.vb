Imports System
Imports System.IO

Public Module DirectoryExtensions
    Private Sub DeleteDirectory(ByVal directory As DirectoryInfo, Optional ByVal forceReadOnlyDelete As Boolean = True)
        If forceReadOnlyDelete Then
            RemoveReadOnlyAttributeFromFiles(directory, True)
        End If

        directory.Delete(True)
    End Sub

    Private Sub RemoveReadOnlyAttributeFromFiles(ByVal directory As DirectoryInfo, ByVal recursive As Boolean)
        Dim readOnlyFiles() As FileInfo = directory.GetFiles()

        For Each file As FileInfo In readOnlyFiles
            file.Attributes = FileAttributes.Normal
        Next

        If recursive Then
            For Each subDirectory As DirectoryInfo In directory.GetDirectories()
                RemoveReadOnlyAttributeFromFiles(subDirectory, True)
            Next
        End If
    End Sub
End Module
