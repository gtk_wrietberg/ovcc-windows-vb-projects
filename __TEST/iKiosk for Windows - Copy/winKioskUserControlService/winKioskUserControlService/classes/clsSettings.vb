Imports Microsoft.Win32

Public Class clsSettings
    Private Shared ReadOnly c_Setting__ActivateLogoff As String = "ActivateLogoff"
    Private Shared ReadOnly c_Setting__ControlledUser As String = "ControlledUser"

    Public Shared Property ActivateLogoff() As Boolean
        Get
            Return clsRegistryData.LoadSettingAsBoolean(c_Setting__ActivateLogoff)
        End Get
        Set(ByVal value As Boolean)
            clsRegistryData.SaveSettingAsBoolean(c_Setting__ActivateLogoff, value)
        End Set
    End Property

    Public Shared Property ControlledUser() As String
        Get
            Return clsRegistryData.LoadSettingAsString(c_Setting__ControlledUser)
        End Get
        Set(ByVal value As String)
            clsRegistryData.SaveSettingAsstring(c_Setting__ControlledUser, value)
        End Set
    End Property


    Private Class clsRegistryData
        Private Shared ReadOnly c_Key__Root As String = "SOFTWARE\\iBAHN\\winKiosk\\UserControl"

        Private Shared Function ReadRegistry(ByVal sValueName As String, Optional ByVal sDefaultValue As String = "") As String
            Dim regKey As RegistryKey

            regKey = Registry.LocalMachine.OpenSubKey(c_Key__Root, True)
            If regKey Is Nothing Then
                regKey = Registry.LocalMachine.CreateSubKey(c_Key__Root)
            End If

            Return regKey.GetValue(sValueName, sDefaultValue)
        End Function

        Private Shared Sub WriteRegistry(ByVal sValueName As String, ByVal sValue As String)
            Dim regKey As RegistryKey

            regKey = Registry.LocalMachine.OpenSubKey(c_Key__Root, True)
            If regKey Is Nothing Then
                regKey = Registry.LocalMachine.CreateSubKey(c_Key__Root)
            End If

            regKey.SetValue(sValueName, sValue, RegistryValueKind.String)
        End Sub

        Public Shared Function LoadSettingAsBoolean(ByVal sSettingName As String) As Boolean
            Dim sValue As String = ReadRegistry(sSettingName, "no")

            If sValue = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SaveSettingAsBoolean(ByVal sSettingName As String, ByVal bValue As Boolean)
            If bValue Then
                WriteRegistry(sSettingName, "yes")
            Else
                WriteRegistry(sSettingName, "no")
            End If
        End Sub

        Public Shared Function LoadSettingAsString(ByVal sSettingName As String) As String
            Return ReadRegistry(sSettingName, "")
        End Function

        Public Shared Sub SaveSettingAsString(ByVal sSettingName As String, ByVal sValue As String)
            WriteRegistry(sSettingName, sValue)
        End Sub
    End Class
End Class
