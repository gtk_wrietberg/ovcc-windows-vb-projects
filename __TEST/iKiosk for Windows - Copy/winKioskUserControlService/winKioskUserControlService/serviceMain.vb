Imports System.Threading

Public Class serviceMain
    Private WTSAPI As clsWTSAPI
    Private threadAbort As Thread = Nothing
    Private threadIdle As Thread = Nothing
    Private threadLogoff As Thread = Nothing
    Private bAbort As Boolean = False

    Private WithEvents oProcess As clsProcessRunner


    Protected Overrides Sub OnStart(ByVal args() As String)
        InitGlobals()

        oLogger.WriteToLog("started main service")

        WTSAPI = New clsWTSAPI

        StartThreads()
    End Sub

    Protected Overrides Sub OnStop()
        oLogger.WriteToLog("stopped")

        bAbort = True
    End Sub

    '------------------
    Private Sub StartThreads()
        threadAbort = New Thread(New ThreadStart(AddressOf ThreadProc_Abort))
        threadAbort.Start()

        threadIdle = New Thread(New ThreadStart(AddressOf ThreadProc_Idle))
        threadIdle.Start()

        threadLogoff = New Thread(New ThreadStart(AddressOf ThreadProc_Logoff))
        threadLogoff.Start()
    End Sub

    Private Sub ThreadProc_Abort()
        oLogger.WriteToLog("started abort thread")

        Do While (Not bAbort)
            If IO.File.Exists("c:\temp\abort.ourguest") Then
                bAbort = True
            End If

            Thread.Sleep(5000)
        Loop
    End Sub

    Private Sub ThreadProc_Idle()
        oLogger.WriteToLog("started idle check thread")

        Dim idleTime As Integer = 0

        Do While (Not bAbort)
            idleTime = GetMachineIdleTime()
            oLogger.WriteToLog("machine idle: " & idleTime.ToString & " ticks", , 1)
            Thread.Sleep(1000)
        Loop
    End Sub

    Private Sub ThreadProc_Logoff()
        oLogger.WriteToLog("started logoff thread")

        Do While (Not bAbort)
            Dim iSessionId As Integer = WTSAPI.FindSessionIdByUsername(c_GuestUser)

            If iSessionId >= 0 Then
                oLogger.WriteToLog("found session id: " & iSessionId.ToString, , 1)

                If clsSettings.ActivateLogoff Then
                    oLogger.WriteToLog("initiating logoff for " & c_GuestUser, , 2)
                    RunLogoffCommandForSessionId(iSessionId)
                End If
            End If

            Thread.Sleep(5000)
        Loop
    End Sub

    Private Sub RunLogoffCommandForSessionId(ByVal iSessionId As Integer)
        oLogger.WriteToLogRelative("Running Logoff command", , 1)
        oLogger.WriteToLogRelative("Path   : " & c_LogoffCommand, , 2)
        oLogger.WriteToLogRelative("Params : " & iSessionId.ToString, , 2)
        oLogger.WriteToLogRelative("Timeout: 60", , 2)

        oProcess = New clsProcessRunner
        oProcess.FileName = c_LogoffCommand
        oProcess.Arguments = iSessionId.ToString
        oProcess.MaxTimeout = 60
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()

        Do
            System.Threading.Thread.Sleep(1000)
        Loop Until oProcess.IsProcessDone

        If oProcess.ProcessTimedOut Then
            oLogger.WriteToLogRelative("command timed out", , 1)
        End If

        If oProcess.ErrorsOccurred Then
            oLogger.WriteToLogRelative("command exited with errors", , 1)
        End If
    End Sub

    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLogRelative("done", , 1)
        oLogger.WriteToLogRelative("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLogRelative("failed", , 1)
        oLogger.WriteToLogRelative(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLogRelative("started", , 1)
        oLogger.WriteToLogRelative("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLogRelative("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
End Class
