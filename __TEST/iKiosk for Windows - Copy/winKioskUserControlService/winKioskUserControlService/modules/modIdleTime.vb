Imports system.Runtime.InteropServices

Module modIdleTime
    '====================Windows Idle time=================
    <StructLayout(LayoutKind.Sequential)> _
    Structure LASTINPUTINFO
        <MarshalAs(UnmanagedType.U4)> _
        Public cbSize As Integer
        <MarshalAs(UnmanagedType.U4)> _
        Public dwTime As Integer
    End Structure

    <DllImport("User32.dll")> _
    Private Function GetLastInputInfo(ByRef plii As LASTINPUTINFO) As Boolean
    End Function

    Private myLastInputInfo As New LASTINPUTINFO

    Public Function GetMachineIdleTime() As Integer
        Dim idleTime As Integer = -1

        myLastInputInfo.cbSize = Marshal.SizeOf(myLastInputInfo)
        myLastInputInfo.dwTime = 0
        If GetLastInputInfo(myLastInputInfo) Then
            idleTime = (Environment.TickCount - myLastInputInfo.dwTime)
        End If

        Return idleTime
    End Function
End Module


