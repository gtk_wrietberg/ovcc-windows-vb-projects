﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Ini File Driver Test Application")> 
<Assembly: AssemblyDescription("Test Application for Ini File Driver")> 
<Assembly: AssemblyCompany("Jason Faulkner")> 
<Assembly: AssemblyProduct("Ini File Driver")> 
<Assembly: AssemblyCopyright("Copyright © Jason Faulkner 2013-2014")> 
<Assembly: AssemblyTrademark("http://JasonFaulkner.com")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("9d53485f-1ee3-4f32-ae15-86d3db673604")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.2.0.09304")> 
<Assembly: AssemblyFileVersion("1.2.0.09304")> 
