﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CreateWindowStation_Csharp_01
{
    class Class1
    {
        [DllImport("user32.dll", EntryPoint = "CreateWindowStation", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr CreateWindowStation(
                    [MarshalAs(UnmanagedType.LPWStr)] string name,
                    [MarshalAs(UnmanagedType.U4)] int reserved,      // must be zero.
                    [MarshalAs(UnmanagedType.U4)] WINDOWS_STATION_ACCESS_MASK desiredAccess,
                    [MarshalAs(UnmanagedType.LPStruct)] SecurityAttributes attributes);

        [StructLayout(LayoutKind.Sequential)]
        public class SecurityAttributes
        {
            #region Struct members
            [MarshalAs(UnmanagedType.U4)]
            private int mStuctLength;

            private IntPtr mSecurityDescriptor;

            [MarshalAs(UnmanagedType.U4)]
            private bool mInheritHandle;
            #endregion

            public SecurityAttributes()
            {
                mStuctLength = Marshal.SizeOf(typeof(SecurityAttributes));
                mSecurityDescriptor = IntPtr.Zero;
            }

            public IntPtr SecurityDescriptor
            {
                get { return mSecurityDescriptor; }
                set { mSecurityDescriptor = value; }
            }

            public bool Inherit
            {
                get { return mInheritHandle; }
                set { mInheritHandle = value; }
            }
        }
        [Flags]
        internal enum WINDOWS_STATION_ACCESS_MASK : uint
        {
            WINSTA_NONE = 0,

            WINSTA_ENUMDESKTOPS = 0x0001,
            WINSTA_READATTRIBUTES = 0x0002,
            WINSTA_ACCESSCLIPBOARD = 0x0004,
            WINSTA_CREATEDESKTOP = 0x0008,
            WINSTA_WRITEATTRIBUTES = 0x0010,
            WINSTA_ACCESSGLOBALATOMS = 0x0020,
            WINSTA_EXITWINDOWS = 0x0040,
            WINSTA_ENUMERATE = 0x0100,
            WINSTA_READSCREEN = 0x0200,

            WINSTA_ALL_ACCESS = (WINSTA_ENUMDESKTOPS | WINSTA_READATTRIBUTES | WINSTA_ACCESSCLIPBOARD |
                            WINSTA_CREATEDESKTOP | WINSTA_WRITEATTRIBUTES | WINSTA_ACCESSGLOBALATOMS |
                            WINSTA_EXITWINDOWS | WINSTA_ENUMERATE | WINSTA_READSCREEN |
                            STANDARD_ACCESS.STANDARD_RIGHTS_REQUIRED),
        }

        /// <summary>
        /// Base class for the safe handles used for Windows Station and Desktop API.
        /// </summary>
        public abstract class BaseSafeHandle : SafeHandleZeroOrMinusOneIsInvalid
        {
            protected BaseSafeHandle(IntPtr handle, bool ownsHandle)
                : base(ownsHandle)
            {
                SetHandle(handle);
            }

            /// <summary>
            /// Close the native handle. The real call is dependent on whether it is
            /// a WindowsStation or a Desktop.
            /// </summary>
            /// <param name="handle">Handle to be closed.</param>
            /// <returns>true if successful, false other wise.</returns>
            protected abstract bool CloseNativeHandle(IntPtr handle);

            protected override bool ReleaseHandle()
            {
                if (IsInvalid)
                {
                    return false;
                }
                bool closed = CloseNativeHandle(this.handle);
                if (closed)
                {
                    SetHandle(IntPtr.Zero);
                }
                return closed;
            }

        }

        public class SafeWindowStationHandle : BaseSafeHandle
        {
            public SafeWindowStationHandle(IntPtr handle, bool ownsHandle)
                : base(handle, ownsHandle)
            { }

            protected override bool CloseNativeHandle(IntPtr handle)
            {
                return WindowStationAndDesktop.CloseWindowStation(handle);
            }
        }
        public static SafeWindowStationHandle CreateWindowStation(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid window station name", "name");
            }

            IntPtr handle = WindowStationAndDesktop.CreateWindowStation(name, 0,
                    WINDOWS_STATION_ACCESS_MASK.WINSTA_ALL_ACCESS, null);
            if (handle == IntPtr.Zero)
            {
                int error = Marshal.GetLastWin32Error();
                throw new Win32Exception(error);
            }

            SafeWindowStationHandle safeHandle = new SafeWindowStationHandle(handle, true);

            return safeHandle;
        }
    }
}
