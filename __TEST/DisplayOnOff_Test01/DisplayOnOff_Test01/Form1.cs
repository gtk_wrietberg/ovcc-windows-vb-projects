﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace DisplayOnOff_Test01
{
    public enum MonitorState
    {
        MonitorStateOn = -1,
        MonitorStateOff = 2,
        MonitorStateStandBy = 1
    }


public partial class Form1 : Form {
 		[DllImport("user32.dll")]
 		private static extern int SendMessage(int hWnd, int hMsg, int wParam, int lParam);
 
 		public Form1() {
 			//InitializeComponent();
 			//SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;
 		}
 
        //void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e) {
        //    SetMonitorInState(MonitorState.MonitorStateOff);
        //}
 
 		private void button1_Click(object sender, EventArgs e) {
            SetMonitorInState(MonitorState.MonitorStateOff);
 		}
 
 		private void SetMonitorInState(MonitorState state) {
 			SendMessage(0xFFFF, 0x112, 0xF170, (int)state);
 		}
 	}
}