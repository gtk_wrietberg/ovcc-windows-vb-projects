﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim objects() As Object = {16.33, -24, 0, 1, "12", "12.7", String.Empty, _
                           "1String", "True", "false", Nothing, _
                           New System.Collections.ArrayList()}
        For Each obj As Object In objects
            If obj IsNot Nothing Then
                Console.Write("{0,-40}  -->  ", _
                              String.Format("{0} ({1})", obj, obj.GetType().Name))
            Else
                Console.Write("{0,-40}  -->  ", "Nothing")
            End If
            Try
                Console.WriteLine("{0}", Convert.ToBoolean(obj))
            Catch ex As FormatException
                Console.WriteLine("Bad Format")
            Catch ex As InvalidCastException
                Console.WriteLine("No Conversion")
            End Try
        Next
    End Sub
End Class
