﻿Public Class Form1
    Private WithEvents oHttp As WebFileDownloader

    Private ReadOnly URL_TEMPLATE As String = "http://www.classicfm.nl/index.php/muziek/playlist/%%YYYYMMDD%%/"

    Private ReadOnly URL_DOWNLOAD_PATH As String = "C:\__temp\zzz_classic_fm"


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ' http://www.classicfm.nl/index.php/muziek/playlist/20170309/11

        ' C:\__temp\zzz_classic_fm

        oHttp = New WebFileDownloader

        oHttp.CurrentPath = URL_DOWNLOAD_PATH


        Dim dDate As Date, sDate As String = ""
        Dim sUrl As String = "", sUrlFull As String = ""

        dDate = New Date(2017, 1, 1)

        Do While Date.Compare(dDate, Date.Now) <= 0
            sUrl = URL_TEMPLATE.Replace("%%YYYYMMDD%%", dDate.ToString("yyyyMMdd"))

            For iHour As Integer = 0 To 23
                sUrlFull = sUrl & LeadingZero(iHour, 2)

                Console.WriteLine(sUrlFull)

                oHttp.DownloadFileWithProgress(sUrlFull, URL_DOWNLOAD_PATH & "\" & dDate.ToString("yyyyMMdd") & "_" & LeadingZero(iHour, 2) & ".txt")
            Next



            dDate = dDate.AddDays(1)
        Loop

    End Sub

    Private Sub oHttp_AmountDownloadedChanged(ByVal iNewProgress As Long) Handles oHttp.AmountDownloadedChanged

    End Sub

    Private Sub oHttp_FileDownloadComplete() Handles oHttp.FileDownloadComplete

    End Sub

    Private Sub oHttp_FileDownloadFailed(ex As Exception) Handles oHttp.FileDownloadFailed

    End Sub

    Private Sub oHttp_FileDownloadSizeObtained(ByVal iFileSize As Long) Handles oHttp.FileDownloadSizeObtained


    End Sub
End Class
