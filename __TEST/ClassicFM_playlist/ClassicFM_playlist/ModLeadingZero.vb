Module ModLeadingZero
    Public Function LeadingZero(ByVal Number As Integer, Optional ByVal PaddingLength As Integer = 5) As String
        Return Number.ToString.PadLeft(PaddingLength, "0")
    End Function
End Module
