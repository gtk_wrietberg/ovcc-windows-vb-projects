@echo off

set PATH=c:\PROGRA~1\lcc\bin;

lcc -O monitoroff.c
lrc monitoroff.rc
lcclnk -o ..\release\monitoroff.exe -s -subsystem windows monitoroff.obj monitoroff.res

if exist monitoroff.obj del monitoroff.obj
if exist monitoroff.res del monitoroff.res

pause