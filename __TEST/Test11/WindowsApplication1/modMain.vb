Module modMain
    Private tmrStartChecking As System.Timers.Timer
    Private tmrCheckForSiteKiosk As System.Timers.Timer


    Public Sub Main()
        tmrStartChecking = New System.Timers.Timer
        tmrStartChecking.Interval = 6000
        AddHandler tmrStartChecking.Elapsed, AddressOf tmrStartChecking_Elapsed

        tmrCheckForSiteKiosk = New System.Timers.Timer
        tmrCheckForSiteKiosk.Interval = 2000
        AddHandler tmrCheckForSiteKiosk.Elapsed, AddressOf tmrCheckForSiteKiosk_Elapsed

        'tmrStartChecking.Start()

        'Do While True
        '    System.Threading.Thread.Sleep(100)
        'Loop

        MsgBox(IsProcessRunning("notepad"))
    End Sub

    Private Sub tmrStartChecking_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        tmrStartChecking.Stop()

        MsgBox("Started")

        tmrCheckForSiteKiosk.Start()
    End Sub

    Private Sub tmrCheckForSiteKiosk_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        MsgBox("Check")
    End Sub

    Public Function IsProcessRunning(ByVal PartOfProcessName As String) As Boolean
        Dim oProcess As Process

        For Each oProcess In Process.GetProcesses
            If oProcess.ProcessName.Contains(PartOfProcessName) Then
                Return True
            End If
        Next

        Return False
    End Function

End Module
