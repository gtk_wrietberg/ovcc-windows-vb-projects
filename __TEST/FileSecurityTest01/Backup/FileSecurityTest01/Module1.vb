Imports System.IO
Imports System.Security.AccessControl
Imports System.Security.Principal

Module Module1

    Sub Main()
        AddFileSecurity("C:\Documents and Settings\wrietberg\My Documents\Visual Studio 2005\Projects\FileSecurityTest01\__test\test.txt", "everyone", FileSystemRights.Write, AccessControlType.Allow)
    End Sub

    Public Sub AddFileSecurity(ByVal fileName As String, ByVal username As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, Optional ByVal Replace As Boolean = False)
        'Dim account As String
        Dim account As NTAccount
        Dim sid As SecurityIdentifier
        Dim accessRule As FileSystemAccessRule

        If username.ToLower.Equals("everyone") Then
        Else
            account = New NTAccount(username)
            sid = CType(account.Translate(Type.GetType("System.Security.Principal.SecurityIdentifier")), SecurityIdentifier)
            accessRule = New FileSystemAccessRule(sid, rights, controlType)
        End If

        'account = Environment.MachineName & "\" & username

        Dim fSecurity As FileSecurity = File.GetAccessControl(fileName)
        accessRule = New FileSystemAccessRule(account, rights, controlType)

        fSecurity.AddAccessRule(accessRule)

        File.SetAccessControl(fileName, fSecurity)
    End Sub


End Module
