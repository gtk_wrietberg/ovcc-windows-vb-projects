﻿Option Strict On

Public Class ComboboxItem

    Public Text As String
    Public Value As Object
    Public Selectable As Boolean = True
    Public ImageIndex As Integer = -1

    Public Overrides Function ToString() As String
        Return Text
    End Function

End Class
