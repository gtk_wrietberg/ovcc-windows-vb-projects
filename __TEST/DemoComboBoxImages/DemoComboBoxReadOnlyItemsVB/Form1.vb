﻿Option Strict On

Public Class Form1

    Private mblnManualSet As Boolean = False
    Private ReadOnly myFont As New Font("Arial", 10, FontStyle.Regular)
    Private ReadOnly myFont2 As New Font("Arial", 9, FontStyle.Italic Or FontStyle.Strikeout)

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FillCombo()
    End Sub

    Private Sub FillCombo()
        ComboBox1.DrawMode = DrawMode.OwnerDrawFixed

        ComboBox1.Items.Add(New ComboboxItem With {.Text = "Test1", .Value = 1, .ImageIndex = 0})
        ComboBox1.Items.Add(New ComboboxItem With {.Text = "Test2", .Value = 2, .ImageIndex = 1})
        ComboBox1.Items.Add(New ComboboxItem With {.Text = "Test3", .Value = 3, .ImageIndex = 2})
        ComboBox1.Items.Add(New ComboboxItem With {.Text = "Test4", .Value = 4, .ImageIndex = 3, .Selectable = False})
        ComboBox1.Items.Add(New ComboboxItem With {.Text = "Test5", .Value = 5, .ImageIndex = 4})
        ComboBox1.Items.Add(New ComboboxItem With {.Text = "Test6", .Value = 6, .ImageIndex = 0})
        ComboBox1.Items.Add(New ComboboxItem With {.Text = "Test7", .Value = 7})
        ComboBox1.Items.Add(New ComboboxItem With {.Text = "Test8", .Value = 8})
        ComboBox1.Items.Add("a regular item") 'to test a plain old item

        'Test manual setting of an unselectable item
        mblnManualSet = True
        ComboBox1.SelectedIndex = 3
        mblnManualSet = False
    End Sub

    Private Sub ComboBox1_DrawItem(sender As Object, e As DrawItemEventArgs) Handles ComboBox1.DrawItem
        e.DrawBackground()
        e.DrawFocusRectangle()

        Console.WriteLine("e.Index=" & e.Index)

        If (e.Index < 0) Then
            imageList1.Draw(e.Graphics, e.Bounds.Left, e.Bounds.Top, 0)
            e.Graphics.DrawString(Text, e.Font, New SolidBrush(e.ForeColor), e.Bounds.Left + imageList1.ImageSize.Width, e.Bounds.Top)
        Else
            Dim objItem As ComboboxItem = TryCast(ComboBox1.Items(e.Index), ComboboxItem)
            If objItem IsNot Nothing Then

                'draw the icon if there is one
                If (objItem.ImageIndex >= 0) Then
                    imageList1.Draw(e.Graphics, e.Bounds.Left, e.Bounds.Top, objItem.ImageIndex)
                End If

                If (Not objItem.Selectable) Then
                    'draw an unselectable item
                    e.Graphics.DrawString(objItem.Text, myFont2, Brushes.LightSlateGray, e.Bounds.Left + imageList1.ImageSize.Width, e.Bounds.Top)
                Else
                    'draw a regular item
                    e.Graphics.DrawString(objItem.Text, myFont, Brushes.Black, e.Bounds.Left + imageList1.ImageSize.Width, e.Bounds.Top)
                End If
            Else
                e.Graphics.DrawString(ComboBox1.Items(e.Index).ToString(), myFont, New SolidBrush(e.ForeColor), e.Bounds.Left + imageList1.ImageSize.Width, e.Bounds.Top)
            End If
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        'If manual set it true
        If mblnManualSet Then Return

        'prevent setting an unselectable item
        Dim objItem As ComboboxItem = TryCast(ComboBox1.SelectedItem, ComboboxItem)
        If objItem Is Nothing Then Return
        If Not objItem.Selectable Then ComboBox1.SelectedIndex = -1
    End Sub

End Class


