﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace DemoComboBoxReadOnlyItemsCS
{
    public partial class Form1 : Form
    {

        private bool _blnManualSet = false;
        private readonly Font _myFont = new Font("Arial", 10, FontStyle.Regular);
        private readonly Font _myFont2 = new Font("Arial", 9, FontStyle.Italic | FontStyle.Strikeout);

        public Form1()
        {
            InitializeComponent();

            FillCombo();
        }

        
        private void FillCombo()
        {
            ComboBox1.DrawMode = DrawMode.OwnerDrawFixed;

            ComboBox1.Items.Add(new ComboboxItem { Text = "Test1", Value = 1, ImageIndex = 0 });
            ComboBox1.Items.Add(new ComboboxItem { Text = "Test2", Value = 2, ImageIndex = 1 });
            ComboBox1.Items.Add(new ComboboxItem { Text = "Test3", Value = 3, ImageIndex = 2 });
            ComboBox1.Items.Add(new ComboboxItem { Text = "Test4", Value = 4, ImageIndex = 3, Selectable = false });
            ComboBox1.Items.Add(new ComboboxItem { Text = "Test5", Value = 5, ImageIndex = 4 });
            ComboBox1.Items.Add(new ComboboxItem { Text = "Test6", Value = 6, ImageIndex = 0 });
            ComboBox1.Items.Add(new ComboboxItem { Text = "Test7", Value = 7 });
            ComboBox1.Items.Add(new ComboboxItem { Text = "Test8", Value = 8 });
            ComboBox1.Items.Add("a regular item"); //to test a plain old item

            ComboBox1.DrawItem += ComboBox1_DrawItem;
            ComboBox1.SelectedIndexChanged += ComboBox1_SelectedIndexChanged;

            //Test manual setting of an unselectable item
            _blnManualSet = true;
            ComboBox1.SelectedIndex = 3;
            _blnManualSet = false;
        }

        private void ComboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();
            if (e.Index < 0)
                e.Graphics.DrawString(Text, e.Font, new SolidBrush(e.ForeColor), e.Bounds.Left + imageList1.ImageSize.Width, e.Bounds.Top);
            else
            {
                if (ComboBox1.Items[e.Index].GetType() == typeof(ComboboxItem))
                {
                    ComboboxItem objItem = (ComboboxItem)ComboBox1.Items[e.Index];

                    //draw the icon if there is one
                    if (objItem.ImageIndex >= 0)
                    {
                        imageList1.Draw(e.Graphics, e.Bounds.Left, e.Bounds.Top, objItem.ImageIndex);
                    }

                    if (!objItem.Selectable)
                    {
                        //draw an unselectable item
                        e.Graphics.DrawString(objItem.Text, _myFont2, Brushes.LightSlateGray, e.Bounds.Left + imageList1.ImageSize.Width, e.Bounds.Top);
                    }
                    else
                    {
                        //draw a regular item
                        e.Graphics.DrawString(objItem.Text, _myFont, Brushes.Black, e.Bounds.Left + imageList1.ImageSize.Width, e.Bounds.Top);
                    }
                }
                else
                    e.Graphics.DrawString(ComboBox1.Items[e.Index].ToString(), _myFont, new SolidBrush(e.ForeColor), e.Bounds.Left + imageList1.ImageSize.Width, e.Bounds.Top);
            }
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //If manual set it true
            if (_blnManualSet) return;

            //prevent setting an unselectable item
            if (ComboBox1.SelectedItem != null && ComboBox1.SelectedItem.GetType() == typeof(ComboboxItem))
            {
                ComboboxItem objItem = (ComboboxItem) ComboBox1.SelectedItem;
                if (objItem == null)
                    return;
                if (!objItem.Selectable)
                    ComboBox1.SelectedIndex = -1;
            }
        }
    }
}
