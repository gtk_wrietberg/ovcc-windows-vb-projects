﻿namespace DemoComboBoxReadOnlyItemsCS
{

    public class ComboboxItem
    {

        public string Text;
        public object Value;
        public bool Selectable = true;
        public int ImageIndex=-1;

        public override string ToString()
        {
            return Text;
        }

    }
}
