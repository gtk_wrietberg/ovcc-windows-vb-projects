

Public Class Form1

    Private Sub btnCreateDb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreateDb.Click
        'Save Dialog Box
        Dim f As New SaveFileDialog
        f.Filter = "SQLite 3 (*.db3)|*.db3|All Files|"
        f.ShowDialog()

        'Create Database
        Dim SQLconnect As New SQLite.SQLiteConnection()
        'Database Doesn't Exist so Created at Path
        SQLconnect.ConnectionString = "Data Source=" & f.FileName & ";"
        SQLconnect.Open()
        SQLconnect.Close()
    End Sub
End Class
