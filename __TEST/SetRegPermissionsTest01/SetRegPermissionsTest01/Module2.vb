Imports System.IO
Imports System.Security.Principal

Module ChangeRegistrySecurity
    Private Const c_RegKey As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings"
    Public Const cUsername As String = "sitekiosk"
    Public Const cSiteKiosk_Username As String = "woutertest"

    Public Function ChangeRegistrySecurityForProxySetter() As Boolean
        Dim sHiveFile As String
        Dim oHive As New RegistryHive

        'oLogger.WriteToLogRelative("searching for registry hive file for " & cSiteKiosk_Username, , 1)
        MsgBox("searching for registry hive file for " & cSiteKiosk_Username)

        If File.Exists("C:\Documents and Settings\" & cSiteKiosk_Username & "\ntuser.dat") Then
            sHiveFile = "C:\Documents and Settings\" & cSiteKiosk_Username & "\ntuser.dat"
        ElseIf File.Exists("C:\Users\" & cSiteKiosk_Username & "\ntuser.dat") Then
            sHiveFile = "C:\Users\" & cSiteKiosk_Username & "\ntuser.dat"
        Else
            'No hive file found, whoops!
            'oLogger.WriteToLogRelative("found none, uh oh.", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            MsgBox("found none, uh oh.")

            Return False
        End If

        'oLogger.WriteToLogRelative("found: " & sHiveFile, , 2)
        MsgBox("found: " & sHiveFile)

        'oLogger.WriteToLogRelative("creating backup, just to be sure", , 1)
        MsgBox("creating backup, just to be sure")
        If BackupFile(sHiveFile) Then
            'oLogger.WriteToLogRelative("ok", , 2)
            MsgBox("ok")
        Else
            'oLogger.WriteToLogRelative("fail", , 2)
            MsgBox("fail")
        End If


        Try
            MsgBox("load hive and try to set permission")
            MsgBox("Initialise")
            oHive.Initialise()

            MsgBox("LoadHive")
            oHive.LoadHive(sHiveFile)

            MsgBox("Set security")
            MsgBox("key")
            MsgBox(oHive.LoadedHive(True) & "\" & c_RegKey)

            SetPermission(cUsername, Microsoft.Win32.RegistryHive.Users, oHive.LoadedHive(True) & "\" & c_RegKey)

            MsgBox("UnloadHive")
            oHive.UnloadHive()

            Return True
        Catch ex As Exception
            MsgBox("fail")
            MsgBox(ex.Message)
        End Try

        Try
            MsgBox("trying it the other way")
            MsgBox("getting sid for " & cSiteKiosk_Username)

            Dim ntSiteKiosk As New NTAccount(cSiteKiosk_Username)
            Dim sidSiteKiosk As SecurityIdentifier
            Dim sRegKey As String

            sidSiteKiosk = ntSiteKiosk.Translate(GetType(SecurityIdentifier))
            MsgBox(sidSiteKiosk.Value)

            sRegKey = sidSiteKiosk.Value & "\" & c_RegKey
            MsgBox("Set security")
            MsgBox("key")
            MsgBox("HKEY_USERS\" & sRegKey)

            SetPermission(cUsername, Microsoft.Win32.RegistryHive.Users, sRegKey)
        Catch ex As Exception
            MsgBox("fail")
            MsgBox(ex.Message)

        End Try

    End Function


    Public Function SetPermission(ByVal sUsername As String, ByVal hive As Microsoft.Win32.RegistryHive, ByVal sRegKey As String) As Boolean
        Dim user As String = Environment.MachineName & "\" & sUsername

        MsgBox("SETPERMISSION: username=" & user)

        Dim rs As New Security.AccessControl.RegistrySecurity()

        rs.AddAccessRule(New Security.AccessControl.RegistryAccessRule(user, _
            Security.AccessControl.RegistryRights.ReadKey Or Security.AccessControl.RegistryRights.SetValue, _
            Security.AccessControl.InheritanceFlags.None, _
            Security.AccessControl.PropagationFlags.InheritOnly, _
            Security.AccessControl.AccessControlType.Allow))

        Dim rk As Microsoft.Win32.RegistryKey = Nothing
        Try
            Select Case hive
                Case Microsoft.Win32.RegistryHive.CurrentUser
                    rk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(sRegKey, True)
                Case Microsoft.Win32.RegistryHive.LocalMachine
                    rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sRegKey, True)
                Case Microsoft.Win32.RegistryHive.Users
                    rk = Microsoft.Win32.Registry.Users.OpenSubKey(sRegKey, True)
                Case Else
                    Return False
            End Select
            rk.SetAccessControl(rs)
        Catch ex As Exception
            MsgBox("SETPERMISSION:" & vbCrLf & ex.Message)
        End Try
    End Function
End Module
