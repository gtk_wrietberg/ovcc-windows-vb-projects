Module FileBackup
    Public Function BackupFile(ByVal sFile As String) As Boolean
        Dim sBackupName As String, sBackupFullPath As String
        Dim bRet As Boolean = False

        Dim oFileOriginal As New IO.FileInfo(sFile), oFileBackup As IO.FileInfo

        If Not oFileOriginal.Exists Then
            Return False
        End If

        Try
            Dim dDate As Date = Now()


            sBackupName = oFileOriginal.Name & ".backup." & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")
            sBackupFullPath = oFileOriginal.DirectoryName & "\" & sBackupName

            oFileOriginal.CopyTo(sBackupFullPath, True)

            oFileBackup = New System.IO.FileInfo(sBackupFullPath)
            If oFileBackup.Exists Then
                bRet = True
            End If
        Catch ex As Exception
        End Try

        oFileOriginal = Nothing
        oFileBackup = Nothing

        Return bRet
    End Function

End Module
