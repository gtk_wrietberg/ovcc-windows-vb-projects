Imports System.Drawing.Imaging
Imports System.IO

Public Class Form1

    Dim gif As Image = Image.FromFile("c:\beanoj2.gif")

    Dim fcount As Integer = gif.GetFrameCount(FrameDimension.Time)
    Dim interval(fcount) As Integer

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim item As PropertyItem = gif.GetPropertyItem(20736)
        Dim index As Integer = 0

        For i As Integer = 0 To gif.GetFrameCount(FrameDimension.Time) - 1
            interval(i) = BitConverter.ToInt32(item.Value, index)
            index += 4
        Next
        ShowGif()
        ' the timing interval storted in the GIF file is of the unit of        centi(-Second())
        Me.Timer1.Interval = interval(findex) * 10
        Me.Timer1.Start()
    End Sub

    Dim findex As Integer = 0
    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        findex += 1
        If (findex = fcount) Then
            findex = 0
        End If

        ShowGif()
        Me.Timer1.Interval = interval(findex) * 10

    End Sub

    Private Sub ShowGif()
        gif.SelectActiveFrame(FrameDimension.Time, findex)
        Me.PictureBox1.Invalidate()
    End Sub

    Private Sub PictureBox1_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox1.Paint
        e.Graphics.DrawImage(gif, New Point(0, 0))
    End Sub
End Class