Imports System.Runtime.InteropServices.Marshal

Public Class MyTextbox
    Inherits System.Windows.Forms.TextBox

    Public Event OnScroll()
    Public Event OnWndProc(ByVal m As System.Windows.Forms.Message)

    '-- Declare
    Private Declare Function GetScrollInfo Lib "user32" (ByVal hWnd As IntPtr, ByVal n As Integer, ByRef lpScrollInfo As SCROLLINFO) As Integer

    '-- Const
    Private Const WM_HSCROLL = &H114 '276
    Private Const WM_VSCROLL = &H115 '276
    Private Const SB_HORZ = 0
    Private Const SIF_TRACKPOS = &H10
    Private Const SIF_RANGE = &H1
    Private Const SIF_POS = &H4
    Private Const SB_THUMBTRACK = 5
    Private Const SIF_PAGE = &H2
    Private Const SIF_ALL = SIF_RANGE Or SIF_PAGE Or SIF_POS Or SIF_TRACKPOS

    '-- Info Struct
    Private Structure SCROLLINFO
        Public cbSize As Integer
        Public fMask As Integer
        Public nMin As Integer
        Public nMax As Integer
        Public nPage As Integer
        Public nPos As Integer
        Public nTrackPos As Integer
    End Structure

    '-- Variable
    Dim i As New SCROLLINFO()

    '-- Override the windows procedure
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        RaiseEvent OnWndProc(m)

        '-- If scrolling ---------
        If m.Msg = WM_HSCROLL Or m.Msg = WM_VSCROLL Then
            '-- Set Mask to all
            i.fMask = SIF_ALL

            '-- Set size of structure
            i.cbSize = SizeOf(i)

            '-- Get the info
            GetScrollInfo(m.HWnd, 0, i)

            RaiseEvent OnScroll()
        End If

        MyBase.WndProc(m)
    End Sub

    Public ReadOnly Property ScrollMin() As Integer
        Get
            Return i.nMin
        End Get
    End Property

    Public ReadOnly Property ScrollMax() As Integer
        Get
            Return i.nMax
        End Get
    End Property

    Public ReadOnly Property ScrollPosition() As Integer
        Get
            Return i.nPos
        End Get
    End Property




End Class
