Imports System.serviceprocess
Imports System.Runtime.InteropServices

Module Module1
    Dim exceptioncaught
#Region "Values for Services"

    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Public Function OpenSCManager(ByVal sMachineName As String, ByVal sDbName As String, ByVal iAccess As Integer) As IntPtr
    End Function
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Public Function CreateService(ByVal hSCM As IntPtr, ByVal sName As String, ByVal sDisplay As String, ByVal iAccess As Integer, ByVal iServiceType As Integer, ByVal iStartType As Integer, ByVal iError As Integer, ByVal sPath As String, ByVal sGroup As String, ByVal iTag As Integer, ByVal sDepends As String, ByVal sUser As String, ByVal sPass As String) As IntPtr
    End Function
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Public Function CloseServiceHandle(ByVal iHandle As IntPtr) As Integer
    End Function
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Public Function OpenService(ByVal hSCM As IntPtr, ByVal sServiceName As String, ByVal iDesiredAccess As Integer) As IntPtr
    End Function
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Public Function DeleteService(ByVal hService As IntPtr) As Boolean
    End Function

    Private Enum ServiceControlManagerEnum
        Connect = &H1
        CreateService = &H2
        EnumerateService = &H4
        Lock = &H8
        QueryLockStatus = &H10
        ModifyBootConfig = &H20
        AllAccess = (StandardRightsRequired Or Connect Or ServiceControlManagerEnum.CreateService Or EnumerateService Or ServiceControlManagerEnum.Lock Or QueryLockStatus Or ModifyBootConfig)
        StandardRightsRequired = &HF0000
    End Enum
    Private Enum ServiceAccessTypeEnum
        QueryConfig = &H1
        ChangeConfig = &H2
        QueryStatus = &H4
        EnumerateDependents = &H8
        Start = &H10
        [Stop] = &H20
        PauseContinue = &H40
        Interrogate = &H80
        UserDefinedControl = &H100
        AllAccess = (StandardRightsRequired Or QueryConfig Or ChangeConfig Or QueryStatus Or EnumerateDependents Or Start Or [Stop] Or PauseContinue Or Interrogate Or UserDefinedControl)
        StandardRightsRequired = &HF0000
    End Enum
    Private Enum ServiceTypeEnum
        Win32OwnProcess = &H10
        AutoStart = &H2
        ErrorNormal = &H1
    End Enum
    Private Enum ServiceControlTypeEnum
        OwnProcess = &H10
        ShareProcess = &H20
        KernelDriver = &H1
        FileSystemDriver = &H2
        InteractiveProcess = &H100
    End Enum
    Private Const c_sBefore As String = "<IDEAConfiguration><Group><Name>Global</Name><BATList uselist=""No""/>"
    Private Const c_sAfter As String = "</Group></IDEAConfiguration>"

#End Region

    Sub Main()
        Console.WriteLine("Enter Name of service to Uninstall")
        Dim sServiceName As String = Console.ReadLine.TrimEnd(" ")
        Call UninstallService(sServiceName)
    End Sub

    Private Sub UninstallService(ByVal sServiceName As String)
        Console.WriteLine("Running Uninstall Service...")

        If IsServiceInstalled(sServiceName) Then
            ' STOP SERVICE
            Call ControlServices(sServiceName, "Stop")
        Else
            Exit Sub
        End If

        Dim hSCM As IntPtr = OpenSCManager(Nothing, Nothing, ServiceControlManagerEnum.AllAccess)
        If hSCM.ToInt32 = 0 Then
            Throw New Exception("Could not delete service. [1]")
        Else
            Dim hService As IntPtr = OpenService(hSCM, sServiceName, ServiceAccessTypeEnum.AllAccess)
            If hService.ToInt32 = 0 Then
                ' TODO: FAILED
            Else
                If Not DeleteService(hService) Then
                    Throw New Exception("Could not delete service. [2]")
                End If

                CloseServiceHandle(hService)
            End If

            CloseServiceHandle(hSCM)
        End If
    End Sub

#Region "Helper Functions"

    Private Function IsServiceInstalled(ByVal sServiceName As String) As Boolean
        Dim bResult As Boolean = False

        Dim oServiceArray() As ServiceProcess.ServiceController
        oServiceArray = ServiceProcess.ServiceController.GetServices

        For Each oServiceController As ServiceProcess.ServiceController In oServiceArray
            If oServiceController.ServiceName.Trim.ToUpper = sServiceName.Trim.ToUpper Then
                Dim i As New ServiceControllerPermissionAttribute(Security.Permissions.SecurityAction.Demand)
                Dim d As New ServiceControllerPermission
                Try
                    If d.Any Then
                        d.ToString()
                    End If
                Catch
                End Try
                bResult = True
                Exit For
            End If
        Next

        Return bResult
    End Function

    Public Function ControlServices(ByVal sServiceName As String, ByVal sTask As String)
        Dim Status As String
        Dim objWinServ As New ServiceController
        objWinServ.ServiceName = sServiceName
        Try
           
            Select Case sTask

                Case "Reset"
                    If objWinServ.Status = ServiceControllerStatus.Running Then
                        'Service is currently running, so you will have to stop it before starting it
                        'First stop all it's child services, then stop the service itself
                        Call CheckForChildServices(sServiceName, "Stop")
                    End If

                    If objWinServ.Status = ServiceControllerStatus.Stopped Then 'This is satisfied if the service was running and then was stopped by code or if it was already stopped
                        'service is already stopped so just start it...so first start all it's parent services
                        Call CheckForParentServices(sServiceName, "Start")
                        StartService(objWinServ.DisplayName)
                    End If

                Case "Start"
                    If objWinServ.Status = ServiceControllerStatus.Stopped Then 'This is satisfied if the service was running and then was stopped by code or if it was already stopped
                        Call CheckForParentServices(sServiceName, "Start")
                        If exceptioncaught = 0 Then
                            Status = "Successfully Started"
                        Else
                            Status = "Could not Start " & objWinServ.DisplayName
                            exceptioncaught = 0
                        End If
                    End If

                Case "Stop"
                    If objWinServ.Status <> ServiceControllerStatus.Stopped Then
                        Call CheckForChildServices(sServiceName, "Stop")
                        If exceptioncaught = 0 Then
                            Status = "Successfully Stopped"
                        Else
                            exceptioncaught = 0
                            Status = "Could not Stop " & objWinServ.DisplayName
                        End If
                    End If
            End Select
        Catch ex As System.InvalidOperationException
            Console.WriteLine("Service does not exist")
            Exit Function
        Catch e As Exception
            Console.WriteLine(e.Message)
            Exit Function
        End Try
        Console.WriteLine(Status)
    End Function

    Private Sub CheckForParentServices(ByVal sServiceName As String, ByVal NextService As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName
        Dim objParentService As ServiceController

        For Each objParentService In objService.ServicesDependedOn
            CheckForParentServices(objParentService.DisplayName, NextService)
        Next
        If NextService = "Start" Then
            Call StartService(sServiceName)
        End If

    End Sub

    Private Sub StartService(ByVal sServiceName As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName

        If objService.Status = ServiceControllerStatus.Stopped Then
            Try
                objService.Start()
                objService.WaitForStatus(ServiceControllerStatus.Running, System.TimeSpan.FromSeconds(20))
            Catch ex As TimeoutException
                Console.WriteLine("Could not Start " & sServiceName & " - TimeOut expired")
                exceptioncaught = 1
            Catch e As Exception
                Console.WriteLine("Could not Start " & sServiceName & " - " & e.Message)
                exceptioncaught = 1
            End Try
        End If

    End Sub

    Private Sub CheckForChildServices(ByVal sServiceName As String, ByVal NextService As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName
        Dim objChildService As ServiceController

        For Each objChildService In objService.DependentServices
            CheckForChildServices(objChildService.DisplayName, NextService)
        Next

        If NextService = "Stop" Then
            Call StopService(sServiceName)
        Else
            Call ContinueService(sServiceName)
        End If

    End Sub

    Private Sub StopService(ByVal sServiceName As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName

        If objService.Status = ServiceControllerStatus.Running Then
            Try
                objService.Stop()
                objService.WaitForStatus(ServiceControllerStatus.Stopped, System.TimeSpan.FromSeconds(20))
            Catch ex As TimeoutException
                exceptioncaught = 1
                Console.WriteLine("Could not Stop " & sServiceName & " - TimeOut expired")
            Catch e As Exception
                exceptioncaught = 1
                Console.WriteLine("Could not Stop " & sServiceName & " - " & e.Message)
            End Try
        End If

    End Sub

    Private Sub ContinueService(ByVal sServiceName As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName
        If objService.Status = ServiceControllerStatus.Paused Then
            Try
                objService.Continue()
                objService.WaitForStatus(ServiceControllerStatus.Running, System.TimeSpan.FromSeconds(20))
            Catch ex As TimeoutException
                exceptioncaught = 1
                Console.WriteLine("Could not change status from Paused To Continue for " & sServiceName & " - TimeOut expired")
            Catch e As Exception
                exceptioncaught = 1
                Console.WriteLine("Could not change status from Paused To Continue for " & sServiceName & " - " & e.Message)
            End Try
        End If

        If objService.Status = ServiceControllerStatus.Stopped Then
            Call StopService(sServiceName)
        End If

    End Sub

    Public Function CheckStatusOfService(ByVal ServiceName As String, ByVal MachineName As String)
        Dim Status As Boolean = False
        Dim objWinServ As New ServiceController
        objWinServ.ServiceName = ServiceName
        objWinServ.MachineName = MachineName

        'If the service is stopped then the button to start it needs to be enabled
        If objWinServ.Status = ServiceControllerStatus.Stopped Then
            Status = True
        End If

        Return Status
    End Function

#End Region

End Module
