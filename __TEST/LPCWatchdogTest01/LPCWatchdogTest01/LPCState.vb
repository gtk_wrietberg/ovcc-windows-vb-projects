﻿Imports System.Runtime.InteropServices



Public Class LPCState
    <DllImport("advapi32.dll", SetLastError:=True)> _
    Private Shared Function OpenProcessToken( _
    ByVal ProcessHandle As IntPtr, _
    ByVal DesiredAccess As Integer, _
    ByRef TokenHandle As IntPtr _
    ) As Boolean
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)> _
    Public Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Private mUsernameToCheck As String

    Public Property UsernameToCheck As String
        Get
            Return mUsernameToCheck
        End Get
        Set(value As String)
            mUsernameToCheck = value
        End Set
    End Property



    Public Sub test()
        MessageBox.Show("lCount: " + _CountProcessesOwnedByUser.ToString)
        '_CountProcessesOwnedByUser()
    End Sub


    Private Function _CountProcessesOwnedByUser() As Long
        Dim wiName As String, lCount As Long

        lCount = 0

        For Each p As Process In Process.GetProcesses
            Try
                Dim hToken As IntPtr
                If OpenProcessToken(p.Handle, Security.Principal.TokenAccessLevels.Query, hToken) Then
                    Using wi As New Security.Principal.WindowsIdentity(hToken)
                        wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                        If wiName.ToLower = mUsernameToCheck.ToLower Then
                            lCount += 1
                            'Debug.WriteLine("!!!MATCH!!! {0} : {1}", p.ProcessName, wiName)
                        Else
                            'Debug.WriteLine("{0} : {1}", p.ProcessName, wiName)
                        End If
                    End Using
                    CloseHandle(hToken)
                End If
            Catch ex As Exception
                'Debug.WriteLine("{0} : {1}", p.ProcessName, ex.Message)
            End Try

        Next

        Return lCount
    End Function
End Class
