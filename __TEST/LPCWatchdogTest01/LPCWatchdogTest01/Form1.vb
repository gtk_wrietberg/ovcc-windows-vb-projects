Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MsgBox(FindProcessByName(TextBox1.Text))
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox(IsUserLoggedOn(TextBox2.Text))

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim lpcAction As LOBBYPC_ACTION

        lpcAction = CheckLobbyPCState()

        CheckBox1.Checked = lpcAction.RESTART
        CheckBox2.Checked = lpcAction.SHOW_WARNING
        CheckBox3.Checked = lpcAction.WARNING_LOOSE
        TextBox3.Text = lpcAction.STATE.ToString
    End Sub

End Class
