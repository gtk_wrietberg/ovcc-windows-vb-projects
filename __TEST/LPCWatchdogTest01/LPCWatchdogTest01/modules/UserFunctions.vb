Module UserFunctions
    Public Function IsUserLoggedOn(ByVal UserName As String) As Boolean
        If UserName.Length < 3 Then
            Return False
        End If

        UserName = UserName.ToLower

        Dim pathMachine As New Management.ManagementPath("\\.\root\cimv2")
        Dim scopeRemoteMachine As New Management.ManagementScope(pathMachine)
        Dim queryUser As New Management.ObjectQuery("SELECT * FROM Win32_ComputerSystem")

        Dim userCurrent As String = String.Empty
        Dim searcherUser As New Management.ManagementObjectSearcher(scopeRemoteMachine, queryUser)
        Dim objectUser As Management.ManagementObject

        For Each objectUser In searcherUser.Get
            If Not objectUser("UserName") Is Nothing Then
                userCurrent = objectUser("UserName").ToString.ToLower

                If userCurrent.Length >= UserName.Length Then
                    If userCurrent.Substring(userCurrent.Length - UserName.Length, UserName.Length).Equals(UserName) Then
                        Return True
                    End If
                End If
            End If
        Next

        Return False
    End Function
End Module
