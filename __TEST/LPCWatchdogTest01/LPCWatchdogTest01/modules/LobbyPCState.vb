Module LobbyPCState
    Public Enum LOBBYPC_STATE
        EXPLORER_RUNNING = 1
        SITEKIOSK_NOT_RUNNING = 2
        NOT_RUNNING_AS_SECURE_USER = 4
        WATCHDOG_WARNING_RUNNING = 8
        WATCHDOG_WARNING_FAILED = 16
        ADMIN_MODE = 32
        ADMIN_MODE_TIMEOUT = 64
        GENERAL_FAILURE = 128
    End Enum

    Public Structure LOBBYPC_ACTION
        Dim RESTART As Boolean
        Dim SHOW_WARNING As Boolean
        Dim WARNING_LOOSE As Boolean
        Dim STATE As Long
    End Structure

    Public Function CheckLobbyPCState() As LOBBYPC_ACTION
        Dim lState As Long

        Dim action As LOBBYPC_ACTION

        action.RESTART = False
        action.SHOW_WARNING = False
        action.WARNING_LOOSE = False


        If FindProcessByName(gProcessName_Explorer) Then
            'The explorer.exe process is running, which means we're not in a proper secure shell
            'We will present the user with the warning popup
            lState = lState + LOBBYPC_STATE.EXPLORER_RUNNING
            action.SHOW_WARNING = True
        End If

        If Not FindProcessByName(gProcessName_SiteKiosk) Then
            'SiteKiosk is not running
            lState = lState + LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING
            action.RESTART = True
        End If

        If Not IsUserLoggedOn(gUserName_SecureUser) Then
            'Secure user not logged in
            lState = lState + LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER
            action.RESTART = True

            If Not lState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING Then
                action.SHOW_WARNING = True
                action.WARNING_LOOSE = True
            End If
        Else
            If lState And LOBBYPC_STATE.EXPLORER_RUNNING Then
                action.SHOW_WARNING = True
                If Not lState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING Then
                    action.WARNING_LOOSE = True
                End If
            End If
        End If

        If FindProcessByName(gProcessName_WarningPopup) Then
            'Warning popup is running
            lState = lState + LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING
            action.RESTART = False
            action.SHOW_WARNING = False
        End If




        action.STATE = lState

        'When both a restart (action.RESTART) and the warning popup (action.SHOW_WARNING) are true,
        'the restart is canceled, and the popup window is shown.
        If action.RESTART And action.SHOW_WARNING Then
            action.RESTART = False
        End If




        Return action
    End Function
End Module
