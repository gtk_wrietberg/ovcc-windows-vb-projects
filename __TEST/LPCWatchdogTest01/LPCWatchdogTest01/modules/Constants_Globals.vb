Module Constants_Globals
    Public gProcessName_SiteKiosk As String = "sitekiosk.exe"
    Public gProcessName_Explorer As String = "explorer.exe"
    Public gProcessName_WarningPopup = "LobbyPCWatchDogWarning.exe"
    Public gProcessName_WarningTrigger = "LobbyPCWatchDogTrigger.exe"

    Public gUserName_SecureUser As String = "SiteKiosk"

    'Registry stuff
    Public ReadOnly RegRootWatchDog As String = "SOFTWARE\iBAHN\LobbyPCWatchdog"
    Public ReadOnly RegKeyUserNameToCheck As String = "UserNameToCheck"
    Public ReadOnly RegKeyProcessNameToCheck As String = "ProcessNameToCheck"
    Public ReadOnly RegKeyLoopTimeout As String = "LoopTimeout"
    Public ReadOnly RegKeyAdminModeTimeout As String = "AdminModeTimeout"
    Public ReadOnly RegKeyAlertState As String = "AlertState"
    Public ReadOnly RegKeyAlertCancelled As String = "AlertCancelled"
    Public ReadOnly RegKeyAlertCancelledTime As String = "AlertCancelledTime"
    Public ReadOnly RegKeyAlertReportAllOk As String = "AlertReportAllOk"
    Public ReadOnly RegKeySound As String = "WarningSound"
    Public ReadOnly RegKeyCurrentLogfile As String = "CurrentLogfile"
    Public ReadOnly RegKeyLogLevel As String = "LogLevel"
    Public ReadOnly RegKeyUseRandomPasswordHints As String = "UseRandomPasswordHints"
    Public ReadOnly RegKeySendReport As String = "SendReport"
    Public ReadOnly RegKeyReportUrl As String = "ReportUrl"
    Public ReadOnly RegKeyDebug As String = "Debug"
    Public ReadOnly RegKeyDeleteOldLogfiles As String = "DeleteOldLogfiles"
    Public ReadOnly RegKeyDeleteOldLogfilesAfterDays As String = "DeleteOldLogfilesAfterDays"
End Module
