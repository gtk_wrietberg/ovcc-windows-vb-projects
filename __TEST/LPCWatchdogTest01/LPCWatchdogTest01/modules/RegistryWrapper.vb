Imports Microsoft.Win32

Module modRegistry
    Public Function Registry_GetValue(ByVal Hive As Microsoft.Win32.RegistryHive, ByVal Key As String, ByVal ValueName As String) As Object
        Dim objParent As RegistryKey
        Dim objSubkey As RegistryKey
        Dim sAns As String = ""

        objParent = _GetHive(Hive)

        Try
            objSubkey = objParent.OpenSubKey(Key)
            If Not objSubkey Is Nothing Then
                sAns = (objSubkey.GetValue(ValueName))
            End If
            objSubkey.Close()
        Catch ex As Exception
            'MsgBox("modRegistry.Registry_GetValue: " & ex.Message)
        End Try

        Try
            objParent.Close()
        Catch ex As Exception

        End Try

        Return sAns
    End Function

    Public Function Registry_SetValue(ByVal Hive As Microsoft.Win32.RegistryHive, ByVal Key As String, ByVal ValueName As String, ByVal Value As Object) As Boolean
        Dim objParent As RegistryKey
        Dim objSubkey As RegistryKey

        objParent = _GetHive(Hive)

        Try
            objSubkey = objParent.OpenSubKey(Key)
            If objSubkey Is Nothing Then
                objSubkey = objParent.CreateSubKey(Key)
            End If
            objSubkey.SetValue(ValueName, Value)
            objSubkey.Close()
        Catch ex As Exception
            'MsgBox("modRegistry.Registry_SetValue: " & ex.Message)

            Return False
        End Try

        Try
            objParent.Close()
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function

    Private Function _GetHive(ByVal Hive As Microsoft.Win32.RegistryHive) As RegistryKey
        Select Case Hive
            Case RegistryHive.ClassesRoot
                Return Registry.ClassesRoot
            Case RegistryHive.CurrentConfig
                Return Registry.CurrentConfig
            Case RegistryHive.CurrentUser
                Return Registry.CurrentUser
            Case RegistryHive.DynData
                Return Registry.DynData
            Case RegistryHive.PerformanceData
                Return Registry.PerformanceData
            Case RegistryHive.Users
                Return Registry.Users
            Case Else
                Return Registry.LocalMachine
        End Select
    End Function
End Module
