/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#region Using directives

using System;
using System.IO;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Text;
using SteganoDotNet.Interfaces;

#endregion

namespace SteganoDotNet.Action
{

    /// <summary>Hides and extract streams in and from text lists.</summary>
    public class ListFileUtility : FileUtility
	{
		/// <summary>A required parameter has not been initialized.</summary>
		public event MissingValueHandler MissingAlphabetFileName;

		string alphabetFileName;
		Collection<string> carrierList;

		 /// <summary>Returns the FileType instance for supported files.</summary>
        public override FileType FileType
		{
			get
			{
				return ListFileType.Current;
			}
		}

        /// <summary>Returns or sets the name and path of a file that will be used an the custom alphabet.</summary>
        public string AlphabetFileName
        {
            get
            {
                return alphabetFileName;
            }
            set
            {
                alphabetFileName = value;
            }
        }

        /// <summary>Constructor.</summary>
        /// <param name="fileName">Name and path of a text file.</param>
        public ListFileUtility(string fileName)
		{
			sourceFileName = fileName;
			carrierList = GetLinesFromFile(fileName);
			Initialize();
		}

        /// <summary>Constructor.</summary>
        /// <param name="rawData">Any Collection&lt;string&gt; with varying, non-empty strings.</param>
        public ListFileUtility(object rawData)
		{
            sourceFileName = NoSourceFileName;
			carrierList = (Collection<string>)rawData;
			Initialize();
		}

		private void Initialize()
		{
			countUsedBitsPerUnit = 1;
			lengthCountUnits = 1; //store length as Byte to save capacity
			countUnits = carrierList.Count;
			CreateCarrierConfigurationControl("ListConfiguration");
		}

        /// <summary>ListConfiguration uses this event handler method to write back new settings.</summary>
        protected override void CarrierConfiguration_AcceptSettings(ICarrierConfiguration sender, EventArgs e)
		{
			IListConfiguration control = (IListConfiguration)sender;

			percentOfMessage = control.PercentOfMessage;
			alphabetFileName = control.AlphabetFileName;
			base.CarrierConfiguration_AcceptSettings(sender, e);
		}

		private string OnMissingAlphabetFileName()
		{
			string fileName = string.Empty;
			if (MissingAlphabetFileName != null)
			{
				MissingValueEventArgs eventArgs = new MissingValueEventArgs();
				MissingAlphabetFileName(this, eventArgs);
				fileName = eventArgs.RequiredValue as string;
				if (fileName == null)
				{
					fileName = string.Empty;
				}
			}
			return fileName;
		}

        /// <summary>Hides the given message stream in the text list.</summary>
        /// <param name="message">Message.</param>
        /// <param name="key">
        /// Not used, can be null.
        /// Use the AlphabetFileName property to customize the attribute sorting.
        /// </param>
        public override void Hide(Stream message, Stream key)
        {
			Collection<string> resultList = new Collection<string>();
			Collection<string> originalList = SortLines(carrierList, GetTextFromFile(alphabetFileName));

            bool messageBit = false;
            int messageByte = message.ReadByte();
            int listElementIndex = 0;
            Random random = new Random();

            //for each byte of the message
            while (messageByte > -1) {
                //for each bit
                for (int bitIndex = 0; bitIndex < 8; bitIndex++) {

                    //decide which line is going to be the next one in the new list
                    
                    messageBit = ((messageByte & (1 << bitIndex)) > 0) ? true : false;

                    if (messageBit) {
                        listElementIndex = 0;
                    } else {
                        listElementIndex = random.Next(1, originalList.Count);
                    }

                    //move the line from old list to new list

                    resultList.Add(originalList[listElementIndex]);
                    originalList.RemoveAt(listElementIndex);
                }

                //repeat this with the next byte of the message
                messageByte = message.ReadByte();
            }

            //copy unused list elements
            foreach (String s in originalList) {
                resultList.Add(s);
            }

			this.outputStream = new MemoryStream();
			StreamWriter writer = new StreamWriter(outputStream, Encoding.Default);
			for (int n = 0; n < resultList.Count-1; n++)
			{
				writer.WriteLine(resultList[n]);
			}
			writer.Write(resultList[resultList.Count-1]);
        }

        /// <summary>Extract a message from the order of lines in a text</summary>
        /// <param name="key">
        /// Not used, can be null.
        /// Use the AlphabetFileName property to customize the attribute sorting.
        /// </param>
        public override Stream Extract(Stream key)
		{
            //initialize empty writer for the message
            BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());

			//last line is irrelevant
            carrierList.RemoveAt(carrierList.Count - 1);

            //sort -> get original list
			
			if (alphabetFileName == null)
			{
				alphabetFileName = OnMissingAlphabetFileName();
				if (alphabetFileName.Length == 0)
				{
					throw new InvalidOperationException("No alphabet file.");
				}
			}

			Collection<string> originalList = SortLines(carrierList, GetTextFromFile(alphabetFileName));
            String[] unchangeableOriginalList = new String[originalList.Count];
            originalList.CopyTo(unchangeableOriginalList, 0);

            int messageBit = 0;
            int messageBitIndex = 0;
            int messageByte = 0;
			int messageLength = 0;

            foreach (String s in carrierList) {
                
                //decide which bit the entry's position hides

                if (s == originalList[0]) {
                    messageBit = 1;
                } else {
                    messageBit = 0;
                }

                //remove the item from the sorted list
                originalList.Remove(s);

                //add the bit to the message
                messageByte += (byte)(messageBit << messageBitIndex);

                messageBitIndex++;
                if (messageBitIndex > 7) {
                    //append the byte to the message
                    messageWriter.Write((byte)messageByte);
					if (messageWriter.BaseStream.Length == messageLength)
					{
						break;
					}
					else
					{
						messageByte = 0;
						messageBitIndex = 0;
					}
                }

				if (messageLength == 0 && messageWriter.BaseStream.Length == 1)
				{
					//first hidden byte was the message's length
					messageWriter.BaseStream.Seek(0, SeekOrigin.Begin);
					messageLength = messageWriter.BaseStream.ReadByte();
					messageWriter.BaseStream.SetLength(0);
				}
            }

            //return original list
			for (int n = 0; n < carrierList.Count; n++)
			{
				carrierList[n] = unchangeableOriginalList[n];
            }

            //return message stream
            messageWriter.Seek(0, SeekOrigin.Begin);
            return messageWriter.BaseStream;
        }

        /// <summary>
        /// Gets the parts of the message that will be hidden in this carrier.
        /// The excerpt begins at the current position of the [message] stream,
        /// its length is specified by the CountBytesToHide property.
        /// </summary>
        /// <param name="message">The full message.</param>
        /// <returns>An excerpt from the message.</returns>
        public override Stream GetMessagePart(Stream message)
		{
			BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());
			messageWriter.Write((byte)countBytesToHide);

			byte[] buffer = new byte[countBytesToHide];
			message.Read(buffer, 0, countBytesToHide);
			messageWriter.Write(buffer);

			messageWriter.Seek(0, SeekOrigin.Begin);
			return messageWriter.BaseStream;
		}
    }
}
