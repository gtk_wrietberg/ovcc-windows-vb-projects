/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action
{
    /// <summary>Describes a part of a sounds.</summary>
	public class Beep {
		private int startSampleIndex;
		private int endSampleIndex;
		private float startSecond;
		private float endSecond;

        /// <summary>Index of the first selected sample in the sound.</summary>
		public int StartSampleIndex
		{
			get { return startSampleIndex; }
		}

        /// <summary>Index of the last selected sample in the sound.</summary>
		public int EndSampleIndex
		{
			get { return endSampleIndex; }
		}

        /// <summary>Start of the selection in seconds.</summary>
		public float StartSecond
		{
			get { return startSecond; }
		}

        /// <summary>End of the selection in seconds.</summary>
        public float EndSecond
		{
			get { return endSecond; }
		}

        /// <summary>Constructor.</summary>
        /// <param name="startSampleIndex">Index of the first selected sample in the sound.</param>
        /// <param name="endSampleIndex">Index of the last selected sample in the sound.</param>
        /// <param name="startSecond">Start of the selection in seconds.</param>
        /// <param name="endSecond">End of the selection in seconds.</param>
		public Beep(int startSampleIndex, int endSampleIndex, float startSecond, float endSecond)
		{
			this.startSampleIndex = startSampleIndex;
			this.endSampleIndex = endSampleIndex;
			this.startSecond = startSecond;
			this.endSecond = endSecond;
		}

	}
}
