/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {
    
    /// <summary>Describes the capabilities of IL source files and .NET assemblies.</summary>
    public class ILFileType : FileType
	{
		private static ILFileType current;
       
        /// <summary>Returns the only instance ILFileType.</summary>
        public static new ILFileType Current
		{
			get
			{
				if (current == null)
				{
					current = new ILFileType();
				}
				return current;
			}
		}

		private ILFileType()
		{
			unitName = "Method";
			typeName = "Intermediate Language";
			supportsClipboard = false;
			supportsNoise = false;
			minBitsPerUnit = 32;
			maxBitsPerUnit = 128;
		}

        /// <summary>Creates an HtmlFileUtility for the given file.</summary>
        /// <param name="fileName">Name and path of the HTML file.</param>
        /// <returns>An HtmlFileUtility object for the given document.</returns>
        public override FileUtility CreateUtility(String fileName)
		{
			return new ILFileUtility(fileName);
		}

        /// <summary>Not supported.</summary>
		public override FileUtility CreateUtility(object rawData)
		{
			throw new NotSupportedException();
		}

	}
}
