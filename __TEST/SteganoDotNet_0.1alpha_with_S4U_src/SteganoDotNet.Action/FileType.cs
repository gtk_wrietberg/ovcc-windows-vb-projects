/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Describes the capabilities of carrier types and creates instances of FileUtility classes.</summary>
    public class FileType
    {
		/// <summary>Display name of a type's carrier unit.</summary>
		protected string unitName;
		/// <summary>Display name of a type.</summary>
		protected string typeName;
		/// <summary>Clipboard flag - reserved for future usage.</summary>
		protected bool supportsClipboard;
		/// <summary>Noise flag - reserved for future usage.</summary>
		protected bool supportsNoise;
		/// <summary>Minimum count of bits that can be hidden in one carrier unit.</summary>
		protected int minBitsPerUnit;
		/// <summary>Maximum count of bits that can be hidden in one carrier unit.</summary>
		protected int maxBitsPerUnit;

		private SortedList<string, FileType> extensionToFileType;
		private string fileDialogFilter;

		private static FileType current;

        /// <summary>Returns the only instance of this class.</summary>
		public static FileType Current
		{
			get
			{
				if (current == null)
				{
					current = new FileType();
					current.Initialize();
				}
				return current;
			}
		}

        /// <summary>Returns a filter string with all supported file types for an OpenFileDialog.</summary>
		public string FileDialogFilter
		{
			get
			{
				return fileDialogFilter;
			}
		}

        /// <summary>Display name of a type.</summary>
        public string TypeName
        {
			get { return typeName; }
		}

        /// <summary>Display name of a type's carrier unit.</summary>
		public string UnitName{
			get { return unitName; }
		}

        /// <summary>Minimum count of bits that can be hidden in one carrier unit.</summary>
        public int MinBitsPerUnit
        {
			get { return minBitsPerUnit; }
		}

        /// <summary>Maximum count of bits that can be hidden in one carrier unit.</summary>
        public int MaxBitsPerUnit
        {
			get { return maxBitsPerUnit; }
		}

		/// <summary>Prevents the construction of another instance.</summary>
        protected FileType()
		{
		}

		private void Initialize()
		{
			extensionToFileType = new SortedList<string, FileType>();

			extensionToFileType.Add(".txt", ListFileType.Current);
			extensionToFileType.Add(".html", HtmlFileType.Current);
			extensionToFileType.Add(".mid", MidiFileType.Current);
			extensionToFileType.Add(".wav", WaveFileType.Current);
			extensionToFileType.Add(".il", ILFileType.Current);
			extensionToFileType.Add(".dll", ILFileType.Current);
			extensionToFileType.Add(".exe", ILFileType.Current);
			extensionToFileType.Add(".bmp24", BitmapFileType.Current);
			extensionToFileType.Add(".png24", BitmapFileType.Current);
			extensionToFileType.Add(".tif", BitmapFileType.Current);
			extensionToFileType.Add(".tiff", BitmapFileType.Current);
			extensionToFileType.Add(".gif", PaletteFileType.Current);

			string extension;
			string extensions = string.Empty;
			string filters = string.Empty;
			foreach (string key in extensionToFileType.Keys)
			{
				extension = key.Trim('8', '2', '4');
				extensions += string.Format("{0};", extension);
				filters += string.Format("*{0};", extension);
			}
			fileDialogFilter = string.Format("{0}|{1}", extensions, filters);

			extensionToFileType.Add(".png8", PaletteFileType.Current);
			extensionToFileType.Add(".bmp8", PaletteFileType.Current);
		}

        /// <summary>Not implemented.</summary>
        public virtual FileUtility CreateUtility(string fileName) { throw new NotSupportedException("The application tried to instantiate an abstract class: FileUtility."); }

        /// <summary>Not implemented.</summary>
        public virtual FileUtility CreateUtility(object rawData) { throw new NotSupportedException("The application tried to instantiate an abstract class: FileUtility."); }

        /// <summary>Returns the FileType instance for the given file.</summary>
        /// <param name="fileName">Name of a file.</param>
        public FileType GetFileType(string fileName)
		{
			string extension = System.IO.Path.GetExtension(fileName).ToLower(CultureInfo.CurrentCulture);

			if (extension == ".png" || extension == ".bmp")
			{
				Image image = Image.FromFile(fileName);
				if (image.PixelFormat == PixelFormat.Format8bppIndexed)
				{
					extension += "8";
				}
				else
				{
					extension += "24";
				}
			}

			return extensionToFileType[extension];
		}
	}
}
