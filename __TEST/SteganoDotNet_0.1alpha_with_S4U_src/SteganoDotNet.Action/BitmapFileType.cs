/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Describes the capabilities of RGB bitmaps and creates instances of BitmapFileUtility.</summary>
	public class BitmapFileType : FileType {

		private static BitmapFileType current;

        /// <summary>returns the only instance BitmapFileType.</summary>
		public static new BitmapFileType Current
		{
			get
			{
				if (current == null)
				{
					current = new BitmapFileType();
				}
				return current;
			}
		}

		private BitmapFileType()
		{
			unitName = "Pixel";
			typeName = "Bitmap";
			supportsClipboard = true;
			supportsNoise = true;
			minBitsPerUnit = 1;
			maxBitsPerUnit = 8;
		}

        /// <summary>Creates a BitmapFileUtility for the given file.</summary>
        /// <param name="fileName">Name and path of the 24 bit image file.</param>
        /// <returns>A BitmapFileUtility object for the given 24 bit image.</returns>
		public override FileUtility CreateUtility(string fileName)
		{
			return new BitmapFileUtility(fileName);
		}

        /// <summary>Creates a BitmapFileUtility for the given image.</summary>
        /// <param name="rawData">A Bitmap object with 24 bit colour depth.</param>
        /// <returns>A BitmapFileUtility object for the given 24 bit image.</returns>
        public override FileUtility CreateUtility(object rawData)
		{
			return new BitmapFileUtility(rawData);
		}

	}
}
