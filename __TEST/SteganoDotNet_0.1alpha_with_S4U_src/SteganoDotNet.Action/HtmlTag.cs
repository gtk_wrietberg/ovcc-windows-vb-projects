﻿/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#region Using directives

using System;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Text;

#endregion

namespace SteganoDotNet.Action
{
    /// <summary>Descibes an HTML tag.</summary>
    public class HtmlTag {

        private enum PostionInTag {
            AttributeName,
            AttributeValue,
            Space
        }

        private int beginPosition;
        private int endPosition;
        private string name;
        private Collection<HtmlTag.HtmlAttribute> attributes;
        
        /// <summary>Character position in the document at which the tag begins.</summary>
        public int BeginPosition {
            get { return beginPosition; }
            set { beginPosition = value; }
        }

        /// <summary>Character position in the document at which the tag ends.</summary>
        public int EndPosition
        {
            get { return endPosition; }
            set { endPosition = value; }
        }

        /// <summary>The tag's name.</summary>
        public string Name {
            get { return name; }
        }

        /// <summary>The tag's attributes.</summary>
        public Collection<HtmlTag.HtmlAttribute> Attributes
        {
            get { return attributes; }
        }

        /// <summary>Constructor.</summary>
        /// <param name="text">Text of the whole HTML tag.</param>
        /// <param name="beginPosition">Character position in the document at which the tag begins.</param>
        /// <param name="endPosition">Character position in the document at which the tag ends.</param>
        public HtmlTag(String text, int beginPosition, int endPosition) {
            this.beginPosition = beginPosition;
            this.endPosition = endPosition;
            this.attributes = new Collection<HtmlAttribute>();

            //separate tag name and attributes
            int index = text.IndexOf(' ');
            if (index < 0) {
                //this is a tag without any attributes
                name = text.Substring(1, text.Length - 1);
            } else {
                name = text.Substring(1, index - 1);
            }

            if (index > 0) {
                text = text.Substring(index);

                //find and list all attributes in this tag
                
                PostionInTag status = PostionInTag.Space;
                int startIndex = 0;
                String attributeName;
                String attributeValue;
                char attributeValueQuotation = '\'';
                HtmlAttribute attribute = null;
                for (int n = 1; n < text.Length; n++) {

                    if ((status == PostionInTag.Space) && ((text[n] == '\'') || (text[n] == '\"'))) {
                        //begin value
                        startIndex = n;
                        attributeValueQuotation = text[n];
                        status = PostionInTag.AttributeValue;
                    }
                    
                    else if ((status == PostionInTag.AttributeValue) && (text[n] == attributeValueQuotation)) {
                        //end value

                        if (attribute != null) {
                            attributeValue = text.Substring(startIndex, n + 1 - startIndex);
                            attribute.Value = attributeValue;
                            attribute = null;
                        }

                        status = PostionInTag.Space;
                    }
                    
                    else if ((status == PostionInTag.Space) && (text[n] != ' ')) {
                        //begin attribute
                        status = PostionInTag.AttributeName;
                        startIndex = n;
                    }
                    
                    else if ((status == PostionInTag.AttributeName) && ((text[n] == '=') || Char.IsWhiteSpace(text[n]) || (n == text.Length-1))) {
                        //end name
                        if (n == text.Length - 1) {
                            //Correct string cursor position.
                            //This is the last character of the tag.
                            //The last attribute does not have a value.
                            n++;
                        }
                        attributeName = text.Substring(startIndex, n - startIndex);
                        
                        attribute = new HtmlAttribute(attributeName);
                        attributes.Add(attribute);

                        status = PostionInTag.Space;
                    }
                    
                    else if ((status != PostionInTag.AttributeValue) && (text[n] == ' ')) {
                        status = PostionInTag.Space;
                    }

                }
            }
        }

        #region Inner Class HtmlAttribute

        /// <summary>Descibes an HTML attribute.</summary>
        public class HtmlAttribute {
            private String name;
            private String value;
            private bool handled;

            /// <summary>The attribute's name.</summary>
            public string Name
            {
                get { return name; }
            }

            /// <summary>The attribute's value.</summary>
            public string Value
            {
                get { return this.value; }
                set { this.value = value; }
            }

            /// <summary>HtmlFileUtility sets this flag internally.</summary>
            public bool Handled
            {
                get { return handled; }
                set { this.handled = value; }
            }
            
            /// <summary>Constructor.</summary>
            /// <param name="name">The attribute's name.</param>
            public HtmlAttribute(string name)
            {
                this.name = name.ToLower();
                this.value = string.Empty;
                handled = false;
            }
        }

        #endregion Inner Class HtmlAttribute
    }
}
