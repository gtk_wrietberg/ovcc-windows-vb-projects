
using System;

namespace SteganoDotNet.Action
{
	public struct Constants
	{
		public const string UserControlsNamespace = "SteganoDotNet.UserControls";

		public const string ErrorBadKey = "{0}: The key cannot be used together with this message. Please change the order of the carrier files, reduce the message part for the file, or choose another key";
		
		public const string WaveSampleRateNotSupported = "The sound has {0}  bits per sample. Please choose a sound with 8 or 16 bits per sample.";
				
	}
}
