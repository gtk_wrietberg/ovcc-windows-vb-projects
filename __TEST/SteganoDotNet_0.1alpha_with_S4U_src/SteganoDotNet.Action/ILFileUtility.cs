/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections;
using System.Collections.ObjectModel;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.Action {
	
	/// <summary>Describes an IL method.</summary>
	public class MethodInfo{
		/// <summary>Method header.</summary>
		public string header;
		/// <summary>Name of the returned type.</summary>
		public string returnType;

		/// <summary>Constructor.</summary>
		/// <param name="header">Method header.</param>
		/// <param name="returnType">Name of the returned type.</param>
		public MethodInfo(string header, string returnType)
		{
			this.header = header;
			this.returnType = returnType;
		}
	}
	
    /// <summary>Hides and extract streams in and from IL Assembler Language code.</summary>
    public class ILFileUtility : FileUtility
    {
		
		//writes the resulting IL code
		private StreamWriter writer = null;
		//length in bytes of the message being extracted
		private int messageLength = 0;
		//has the maximum count of bytes hidden in a method already been written to the carrier file?
		private bool isBytesPerMethodWritten = false;

		//number of lines inserted in each method
		private int linesPerMethod = 0;

		private Stream sourceStream;

		//maximum count of bytes hidden in a method
		private int bytesPerMethod = 1;

        /// <summary>Maximum count of bytes that can be hidden in a method.</summary>
		public int BytesPerMethod{
			get{ return bytesPerMethod; }
			set{ bytesPerMethod = value; }
		}

        /// <summary>Returns the FileType instance for supported files.</summary>
        public override FileType FileType
		{
			get
			{
				return ILFileType.Current;
			}
		}

        /// <summary>Constructor.</summary>
        /// <param name="fileName">Name and path of an IL, EXE or DLL file.</param>
        public ILFileUtility(string fileName)
		{
			sourceFileName = fileName;
			
			//copy source file
			FileStream sourceFileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			byte[] buffer = new byte[sourceFileStream.Length];
			sourceFileStream.Read(buffer, 0, buffer.Length);
			sourceStream = new MemoryStream(buffer);
			sourceFileStream.Close();

			Initialize();
		}
		
		private void Initialize()
		{
			Collection<string> namespaces = new Collection<string>();
			Collection<string> classes = new Collection<string>();
			Collection<MethodInfo> methods = new Collection<MethodInfo>();

			Disassemble();

			Analyse(sourceFileName, namespaces, classes, methods);
			countUnits = methods.Count;
			CreateCarrierConfigurationControl("ILConfiguration", countUnits);
		}

        /// <summary>ILConfiguration uses this event handler method to write back new settings.</summary>
        protected override void CarrierConfiguration_AcceptSettings(ICarrierConfiguration sender, EventArgs e)
		{
			IILConfiguration control = (IILConfiguration)sender;

			bytesPerMethod = control.CountBlocksPerUnit * 4;
			countUsedBitsPerUnit = bytesPerMethod * 8;
			countBytesToHide = bytesPerMethod * countUnits; //calcuated from countBytesToHidePercent
			percentOfMessage = control.PercentOfMessage;
			
			base.CarrierConfiguration_AcceptSettings(sender, e);
		}

		private void Disassemble()
		{
			string sourceFileNameExtension = Path.GetExtension(sourceFileName).ToLower();
			if (sourceFileNameExtension != ".il")
			{
				string outputPath = Path.GetDirectoryName(this.sourceFileName);
				string fileName = Path.GetFileNameWithoutExtension(this.sourceFileName) + ".il";
				outputPath = Path.Combine(outputPath, fileName);

                //string ildasmPath = System.Configuration.ConfigurationManager.AppSettings["ILDAsm"];
            	string ildasmPath = System.Configuration.ConfigurationSettings.AppSettings["ILDAsm"];
            
				//call ILDAsm
				Process process = Process.Start(ildasmPath, String.Format("\"{0}\" /OUT=\"{1}\"", sourceFileName, outputPath));
				process.WaitForExit();

				//use disassembly as carrier file
				this.sourceFileName = outputPath;
			}
		}

		/// <summary>
		/// Assemble the IL (.exe/.dll) or save it as text (.il).
		/// </summary>
		public override void SaveResult(string fileName)
		{
			string extension = Path.GetExtension(fileName).ToLower();
			if (extension != ".il")
			{
				string ilFileName = Path.GetFileNameWithoutExtension(fileName) + ".il";
				base.SaveResult(ilFileName);

				string ilasmPath = System.Configuration.ConfigurationSettings.AppSettings["ILAsm"];
				Process process = Process.Start(ilasmPath, "\"" + ilFileName + "\"");
				process.WaitForExit();
			}
			else
			{
				base.SaveResult(fileName);
			}
		}

		/// <summary>Lists namespaces, classes and methods with return type "void"</summary>
		/// <param name="fileName">Name of the IL file to analyse</param>
		/// <param name="namespaces">Returns the names of all namespaces found in the file</param>
		/// <param name="classes">Returns the names of all classes</param>
		/// <param name="methods">Returns the first lines of all method signatures</param>
		public void Analyse(String fileName, Collection<string> namespaces, Collection<string> classes, Collection<MethodInfo> methods)
		{	
			//current method's header, or null if the method doesn't return "void"
			MethodInfo currentMethod = null;

			//read the IL file
			String[] lines = ReadFile(fileName);
			
			//loop over the lines of the IL file, fill lists
			for(int indexLines=0; indexLines<lines.Length; indexLines++){
				if(lines[indexLines].IndexOf(".namespace ") > 0){
					namespaces.Add( ProcessNamespace(lines[indexLines]) );
				}
				else if(lines[indexLines].IndexOf(".class ") > 0){
					classes.Add( ProcessClass(lines, ref indexLines) );
				}
				else if(lines[indexLines].IndexOf(".method ") > 0){
					currentMethod = ProcessMethod(lines, ref indexLines);
					if(currentMethod != null){
						methods.Add(currentMethod);
					}
				}
			}
		}
	
		/// <summary>Hides a message in an IL file</summary>
		/// <param name="message">Stream with the plain text message</param>
		/// <param name="key">Key for skipping carrier methods</param>
		public override void Hide(Stream message, Stream key)
		{
			HideOrExtract(sourceFileName, message, key, true);
		}

		/// <summary>Extracts a message from an IL file</summary>
		/// <param name="key">Key for skipping carrier methods</param>
		public override Stream Extract(Stream key)
		{
			MemoryStream message = new MemoryStream();
			HideOrExtract(sourceFileName, message, key, false);
			return message;
		}

		/// <summary>Hides or extracts a message in/from an IL file</summary>
		/// <param name="fileNameIn">Name of the IL file</param>
		/// <param name="fileNameOut">Name for the resulting IL file - ignored if [hide] is false</param>
		/// <param name="message">Stream containing the message to hide, or empty stream to store the extracted message</param>
		/// <param name="key">Key stream. Defines the number of unchanged methods between two carrier methods.</param>
		/// <param name="hide">true: hide [message]; false: extract a message</param>
		private void HideOrExtract(String fileNameIn, Stream message, Stream key, bool hide){
			
			if(hide){
				//open the destination file
				this.outputStream = new MemoryStream();
				writer = new StreamWriter(this.outputStream);
			}else{
				//count of bytes hidden in each method is unknown,
				//it will be the first value to extract from the file
				bytesPerMethod = 0;
			}

			String[] lines = ReadFile(fileNameIn);
			bool isMessageComplete = false;

			for(int indexLines=0; indexLines<lines.Length; indexLines++){
				
				if(lines[indexLines].IndexOf(".method ") > 0){
					//found a method!
					if(hide){
						//hide as many bytes as needed
						isMessageComplete = ProcessMethodHide(lines, ref indexLines, message, key);
					}else{
						//extract all bytes hidden in this method
						isMessageComplete = ProcessMethodExtract(lines, ref indexLines, message, key);
					}
				}else if(hide){
					//the line does not belong to a useable method - just copy it
					writer.WriteLine(lines[indexLines]);
				}
				
				if(isMessageComplete){
					break; //nothing else to do
				}
			}
		}

		/// <summary>Reads a file line-by-line</summary>
		/// <param name="fileName">Name of the file</param>
		/// <returns>All lines</returns>
		private String[] ReadFile(String fileName){
			FileStream streamIn = new FileStream(fileName, FileMode.Open);
			StreamReader reader = new StreamReader(streamIn);

			ArrayList listLines = new ArrayList();
			String line;
			while( (line=reader.ReadLine()) != null ){
				listLines.Add(line);
			}
			reader.Close();
			return (String[])listLines.ToArray(typeof(String));
		}

		/// <summary>Extracts the name from a ".namespace name" line</summary>
		/// <param name="line">The .namespace-line</param>
		private String ProcessNamespace(String line){
			if(writer != null){ writer.WriteLine(line); }
			return line.Substring(11);
		}

		/// <summary>Reads the name from a class declaration</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="indexLines">Current index in [lines]</param>
		/// <returns>Name of the class</returns>
		private String ProcessClass(String[] lines, ref int indexLines){
			int indexEndOfName = lines[indexLines].IndexOf("extends")-1;
			if(indexEndOfName < 0){
				indexEndOfName = lines[indexLines].IndexOf("{")-1;
				if(indexEndOfName < 0){
					indexEndOfName = lines[indexLines].Length-1;
				}				
			}
			int indexStartOfName = lines[indexLines].LastIndexOf(' ', indexEndOfName, indexEndOfName)+1;
					
			if(writer != null){ writer.WriteLine(lines[indexLines]); }
			return lines[indexLines].Substring(indexStartOfName, indexEndOfName-indexStartOfName+1);
		}

		/// <summary>Reads a method's header</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="indexLines">Current index in [lines]</param>
		/// <returns>The method's header with line breaks removed</returns>
		private MethodInfo ProcessMethod(String[] lines, ref int indexLines){
			if(lines[indexLines].IndexOf(" void ") > 0){
				String header = GetMethodHeader(lines, ref indexLines);
				return new MethodInfo(header, "void");
			}
			else if(lines[indexLines].IndexOf(" bool ") > 0){
				String header = GetMethodHeader(lines, ref indexLines);
				return new MethodInfo(header, "bool");
			}
			else if(lines[indexLines].IndexOf(" int32 ") > 0){
				String header = GetMethodHeader(lines, ref indexLines);
				return new MethodInfo(header, "int32");
			}
			else if(lines[indexLines].IndexOf(" string ") > 0){
				String header = GetMethodHeader(lines, ref indexLines);
				return new MethodInfo(header, "string");
			}

			return null;
		}

		/// <summary>Hides one or more bytes from the message stream in the IL file</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="indexLines">Current index in [lines]</param>
		/// <param name="message">Stream containing the message</param>
		/// <param name="key">Key stream. Defines the number of unchanged methods between two carrier methods.</param>
		/// <returns>true: the last byte has been hidden; false: more message-bytes are waiting</returns>
		private bool ProcessMethodHide(String[] lines, ref int indexLines, Stream message, Stream key){
			bool isMessageComplete = false;
			int currentMessageValue,	//next message-byte to hide
				positionInitLocals,		//index of the last ".locals init" line
				positionRet,			//index of the "ret" line
				positionStartOfMethodLine; //index of the method's first line
			
			writer.WriteLine(lines[indexLines]); //copy first line

			String returnType = GetReturnType(lines[indexLines]);
			
			if(returnType != null){
				//found a method with return type void/bool/int32/string
				//at it's end the stack will contain only one vaue of the known type,
				//so we remove it, can insert whatever we like and then put back the value

				indexLines++; //next line
						
				//search start of method block, copy all skipped lines
				int oldIndex = indexLines;
				SeekStartOfBlock(lines, ref indexLines);
				CopyBlock(lines, oldIndex, indexLines);
				
				//now we are at the method's opening bracket
				positionStartOfMethodLine = indexLines;
				//go to first line of the method
				indexLines++;
				//get position of last ".locals init" and first "ret"
				positionInitLocals = 0;
				positionRet = 0;
				SeekLastLocalsInit(lines, ref indexLines, ref positionInitLocals, ref positionRet);
				
				if(positionInitLocals == 0){
					//no .locals - insert line at beginning of method
					positionInitLocals = positionStartOfMethodLine;
				}

				//copy from start of method until last .locals, or nothing (if no .locals found)
				CopyBlockAdjustStack(lines, positionStartOfMethodLine, positionInitLocals+1);
				indexLines = positionInitLocals+1;
				//insert local variable
				writer.WriteLine(".locals init (int32 myvalue)");
				//copy rest of the method until the line before "ret"
				CopyBlockAdjustStack(lines, indexLines, positionRet);
				
				//next line is "ret" - nothing left to damage on the stack
				indexLines = positionRet;
				
				if(returnType != "void"){
					//not a void method - store the return value
					writer.Write(writer.NewLine);
					writer.WriteLine(".locals init ("+returnType+" returnvalue)");
					writer.WriteLine("stloc returnvalue");
				}

				//insert lines for [bytesPerMethod] bytes from the message stream
				//combine 4 bytes in one Int32
				int keyValue;
				for(int n=0; n<bytesPerMethod; n+=4){
					isMessageComplete = GetNextMessageValue(message, out currentMessageValue);
					
					//read the next byte from the key
					keyValue = GetKeyValue(key);

					if(keyValue % 2 == 0){
						writer.WriteLine("ldc.i4 "+currentMessageValue.ToString());
						writer.WriteLine("stloc myvalue");
					}else{
						writer.WriteLine("ldstr \"DEBUG - current value is: {0}\"");
						writer.WriteLine("ldc.i4 "+currentMessageValue.ToString());
						writer.WriteLine("box [mscorlib]System.Int32");
						writer.WriteLine("call void [mscorlib]System.Console::WriteLine(string, ");
						writer.WriteLine( "object)" ); //ILDAsm inserts a line break here 
					}
				}

				//bytesPerMethod must be last one in the first method
				if(! isBytesPerMethodWritten){
					writer.WriteLine("ldc.i4 "+bytesPerMethod.ToString());
					writer.WriteLine("stloc myvalue");
					isBytesPerMethodWritten = true;
				}

				if(returnType != "void"){
					//not a void method - load the return value back onto the stack
					writer.WriteLine("ldloc returnvalue");
				}

				//copy current line
				writer.WriteLine(lines[indexLines]);

				if(isMessageComplete){
					//nothing read from the message stream, the message is complete
					//copy rest of the source file
					indexLines++;
					CopyBlock(lines, indexLines, lines.Length-1);
				}
			}
			return isMessageComplete;
		}

		/// <summary>
		/// Returns the next Int32 value to hide.
		/// First call returns message.Length,
		/// next calls return an Int32 containing 4 bytes from the message stream
		/// </summary>
		/// <param name="message">Stream containing the message</param>
		/// <param name="messageValue">Receives the next Int32 value to hide</param>
		/// <returns>true: the last byte has been hidden; false: more message-bytes are waiting</returns>
		private bool GetNextMessageValue(Stream message, out int messageValue)
		{
			bool returnValue = false;
			messageValue = 0;

			for (int n = 0; n < 4; n++)
			{
				int currentMessageValue = message.ReadByte();

				if (currentMessageValue < 0)
				{
					returnValue = true;
					break;
				}
				else
				{
					messageValue += currentMessageValue << (8 * n);
				}
			}

			return returnValue;
		}

		/// <summary>Extracts one or more bytes from the IL file</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="indexLines">Current index in [lines]</param>
		/// <param name="message">Stream to store the extracted bytes in</param>
		/// <param name="key">Key stream. Defines the number of unchanged methods between two carrier methods.</param>
		/// <returns>true: the last byte has been extracted; false: more message-bytes are waiting</returns>
		private bool ProcessMethodExtract(String[] lines, ref int indexLines, Stream message, Stream key){
			bool isMessageComplete = false;
			int positionRet,				//index of the "ret" line
				positionStartOfMethodLine;	//index of the method's first line
		
			String returnType = GetReturnType(lines[indexLines]);
			int keyValue = 0;
			
			if(returnType != null){
				//found a method with return type void/bool/int32/string
				//a part of the message is hidden here

				indexLines++; //next line
				
				//search first line of method block
				SeekStartOfBlock(lines, ref indexLines);
				positionStartOfMethodLine = indexLines;
						
				//go to first line of the method
				indexLines++;
				//get position of "ret"
				positionRet = SeekRet(lines, ref indexLines);
	
				if(bytesPerMethod == 0){
					//go 2 lines back - there we inserted "ldc.i4 "+bytesPerMethod
					indexLines = positionRet - 2;
				}else{
					//go [linesPerMethod] lines per expected message-byte back - there we inserted "ldc.i4 "+currentByte
					linesPerMethod = GetLinesPerMethod(key);
					indexLines = positionRet - linesPerMethod;
				}

				if(returnType != "void"){
					indexLines--; //skip the line "ldloc returnvalue"
				}
				
				int indexValue; //index of the hidden value in a line
				String sValue;	//found value
				int iValue;		//found value, converted
				
				//read [bytesPerMethod] bytes into the message stream
				//if [bytesPerMethod]==0 it has not been read yet
				for(int n=0; (n<bytesPerMethod)||(bytesPerMethod==0); n+=4){	
					
					if(bytesPerMethod > 0){
						//read the next byte from the key
						keyValue = GetKeyValue(key);
						
						//if(keyValue % 2 == 0){
						//	ldc.i4 is the first line of the hidden block
						//	don't change [indexLines]
						if(keyValue % 2 == 1){
							//ldc.i4 is the second line of the hidden block
							indexLines++;
						}
					}
					
					//ILDAsm creates line numbers - find the beginning of the instruction
					indexValue = lines[indexLines].IndexOf("ldc.i4");
					if(indexValue >= 0){
						//jump over "ldc.i4 " and get the constant
						indexValue += 7;
						sValue = lines[indexLines].Substring(indexValue);
						//parse the found value
						if(sValue.IndexOf('x') > 0){
							sValue = sValue.Substring(sValue.IndexOf("x")+1);
							iValue = int.Parse(sValue, System.Globalization.NumberStyles.HexNumber);
						}else{
							iValue = int.Parse(sValue);
						}

						if(bytesPerMethod == 0){ //[bytesPerMethod] has not been read yet
							bytesPerMethod = iValue;
							
							//go [linesPerMethod] lines per expected message-byte back - there we inserted "ldc.i4 "+currentByte.ToString()
							linesPerMethod = GetLinesPerMethod(key);
							indexLines = positionRet - 2 - linesPerMethod;
							
							//reset counter
							n-=4;
						}else{
							if(messageLength == 0){ //messageLength has not been read yet
								messageLength = iValue;
							}else{
								//the header values have already been read,
								//so this is a message value
								for(int indexBytes=0; indexBytes<4; indexBytes++){
									byte bValue = (byte)(iValue >> (indexBytes*8));
									message.WriteByte(bValue);
									isMessageComplete = (message.Length == messageLength);
									if(isMessageComplete){ break; }
								}
								if(isMessageComplete){ break; }
							}

							if(n+4 < bytesPerMethod){ //not the last loop
								if(keyValue % 2 == 0){
									//next message-byte is in the next 2 lines
									indexLines += 2;
								}else{
									//next message-byte is in the next 5 lines,
									//current line is the second one
									indexLines += 4;
								}
							}
						}
					}else{
						//the line we tried to split is not a "ldc.i4" instruction - something has gone wrong!
						throw new ArgumentException("There is no message hidden in this file!");
					}
				}
			}
			return isMessageComplete;
		}

        /// <summary>Calculates the number of lines that will be inserted in one method.</summary>
        /// <param name="key">The kex that will be used to hide the message.</param>
        /// <returns>Number of lines that will be inserted in each method when the given key is applied.</returns>
		private int GetLinesPerMethod(Stream key){
			//count of blocks hidden in every method - a block hides 4 bytes
			int blocksPerMethod = (int)Math.Ceiling((float)bytesPerMethod/(float)4);
			int linesPerMethod = 0;
			int keyPreview = 0;
			long oldKeyPosition = key.Position;
			for(int keyPreviewIndex=0; keyPreviewIndex<blocksPerMethod; keyPreviewIndex++){
				if( (keyPreview = key.ReadByte()) < 0){
					key.Seek(0, SeekOrigin.Begin);
					keyPreview = key.ReadByte();
				}
				//a block can be 2 or 4 lines long
				linesPerMethod += ((keyPreview % 2)==0) ? 2 : 5;
			}
			key.Seek(oldKeyPosition, SeekOrigin.Begin); //reset key stream
			return linesPerMethod;
		}

		/// <summary>Skips all lines until the next "{"</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="indexLines">Current index in [lines]</param>
		private void SeekStartOfBlock(String[] lines, ref int indexLines){
			while(lines[indexLines].IndexOf("{") < 0){
				indexLines++;
			}
		}

		/// <summary>Finds the last ".locals init" and first "ret" line of a method</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="indexLines">Current index in [lines]</param>
		/// <param name="positionInitLocals">Receives the index of the last ".local init" line</param>
		/// <param name="positionRet">Receives the index of the first "ret" line</param>
		private void SeekLastLocalsInit(String[] lines, ref int indexLines, ref int positionInitLocals, ref int positionRet){
			while( ! lines[indexLines].Trim().EndsWith("ret")){ //until end of method
				if(lines[indexLines].IndexOf(".locals init ")>0){
					//.locals ends with ")"
					while(lines[indexLines].IndexOf(")") < 0){
						indexLines++;
					}
					positionInitLocals = indexLines;
				}
				indexLines++;
			}
			positionRet = indexLines;
		}

		/// <summary>Finds the first "ret" line in a method</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="indexLines">Current index in [lines]</param>
		/// <returns>Index of the first "ret" line</returns>
		private int SeekRet(String[] lines, ref int indexLines){
			while( ! lines[indexLines].Trim().EndsWith("ret")){ //until end of method
				indexLines++;
			}
			return indexLines;
		}

		/// <summary>Copies a block of lines from [lines] into the destination file</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="start">Index of the first line to copy</param>
		/// <param name="end">Index of the last line to copy</param>
		private void CopyBlock(String[] lines, int start, int end){
			String[] buffer = new String[end-start];
			Array.Copy(lines, start, buffer, 0, buffer.Length);
			writer.WriteLine(String.Join(writer.NewLine, buffer));
		}


		/// <summary>Copies a block of lines from [lines] into the destination file
		/// and makes sure that the maxstack-value is 2 or greater</summary>
		/// <param name="lines">Lines of the IL file</param>
		/// <param name="start">Index of the first line to copy</param>
		/// <param name="end">Index of the last line to copy</param>
		private void CopyBlockAdjustStack(String[] lines, int start, int end){
			for(int n=start; n<end; n++){
				
				if(lines[n].IndexOf(".maxstack ")>0){
					//parse the stack size
					int indexStart = lines[n].IndexOf(".maxstack ");
					int maxStack = int.Parse( lines[n].Substring(indexStart+10).Trim() );
					//stack size must be 2 or greater
					if(maxStack < 2){
						lines[n] = ".maxstack 2";
					}
				}

				writer.WriteLine(lines[n]);
			}
		}

		/// <summary>Reads the declaration of a method</summary>
		/// <param name="lines">The IL-Text</param>
		/// <param name="indexLines">in: Index of the method header's first line; out: Index of the line containing the "{"</param>
		/// <returns>Header of the method</returns>
		private String GetMethodHeader(String[] lines, ref int indexLines){
			StringBuilder methodBuilder = new StringBuilder();
			methodBuilder.Append(lines[indexLines]);
				
			indexLines++; //next line
			while(lines[indexLines].IndexOf("{") < 0){
				methodBuilder.Append(lines[indexLines]);
				indexLines++;
			}

			return methodBuilder.ToString();
		}

		/// <summary>Reads the return type from a method declaration</summary>
		/// <param name="line">The method's declaration</param>
		/// <returns>Name of the return type</returns>
		private String GetReturnType(String line){
			String returnType = null;
			if(line.IndexOf(" void ") > 0){ returnType = "void"; }
			else if(line.IndexOf(" bool ") > 0){ returnType = "bool"; }
			else if(line.IndexOf(" int32 ") > 0){ returnType = "int32"; }
			else if(line.IndexOf(" string ") > 0){ returnType = "string"; }
			return returnType;
		}
	}
}
