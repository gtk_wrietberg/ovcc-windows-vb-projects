/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action
{
	/// <summary>Arguments for the BeepFound Event.</summary>
    public class BeepFoundEventArgs : EventArgs {

		private Beep beep;
		private int channel;

        /// <summary>Desciptor for the found samples.</summary>
		public Beep Beep
		{
			get { return beep; }
		}

		/// <summary>Index of the channel.</summary>
		public int Channel
		{
			get { return channel; }
		}

        /// <summary>Constructor.</summary>
        /// <param name="beep">Desciptor for the found samples.</param>
		/// <param name="channel">Index of the channel.</param>
		public BeepFoundEventArgs(Beep beep, int channel)
		{
			this.beep = beep;
			this.channel = channel;
		}
	}

    /// <summary>Delegate class for notification about found beeps.</summary>
    /// <remarks>Used while preparing wave carriers for extracting.</remarks>
	public delegate void BeepFoundHandler(object sender, BeepFoundEventArgs e);
}
