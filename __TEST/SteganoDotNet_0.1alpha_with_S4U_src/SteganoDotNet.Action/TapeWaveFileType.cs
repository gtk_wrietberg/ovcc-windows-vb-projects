/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Describes the capabilities of wave sounds for lossy recording and creates instances of TapeWaveFileUtility.</summary>
    public class TapeWaveFileType : FileType
	{

		private static TapeWaveFileType current;

        /// <summary>Returns the only instance TapeWaveFileType.</summary>
        public static new TapeWaveFileType Current
		{
			get
			{
				if (current == null)
				{
					current = new TapeWaveFileType();
				}
				return current;
			}
		}

		private TapeWaveFileType()
		{
			unitName = "Second";
			typeName = "Wave (for Tape)";
			supportsClipboard = true;
			supportsNoise = false;
			minBitsPerUnit = -1;
			maxBitsPerUnit = -1;
		}

        /// <summary>Creates a TapeWaveFileUtility for the given file.</summary>
        /// <param name="fileName">Name and path of the wave file.</param>
        /// <returns>A TapeWaveFileUtility object for the given sound.</returns>
        public override FileUtility CreateUtility(String fileName)
		{
			return new TapeWaveFileUtility(fileName);
		}

		/// <summary>Not supported.</summary>
        public override FileUtility CreateUtility(object rawData)
		{
            throw new NotSupportedException();
		}

	}
}
