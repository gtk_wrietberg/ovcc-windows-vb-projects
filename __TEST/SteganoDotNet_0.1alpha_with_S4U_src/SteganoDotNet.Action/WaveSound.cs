/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Stores format and samples of a wave sound.</summary>
	public class WaveSound {

		private WaveFormat format;
		private short[] samples;

        /// <summary>Returns the format header information.</summary>
		public WaveFormat Format
		{
			get { return format; }
		}

        /// <summary>Returns the number of samples.</summary>
		public int Count
		{
			get { return samples.Length; }
		}

        /// <summary>Returns the wave samples.</summary>
        public short[] Samples
		{
			get { return samples; }
		}

        /// <summary>Returns the left channel samples.</summary>
        public short[] LeftSamples
		{
			get {
				return GetChannelSamples(0);
			}
		}

        /// <summary>Returns the right channel samples.</summary>
		public short[] RightSamples
		{
			get
			{
				return GetChannelSamples(1);
			}
		}

        /// <summary>Returns the sample at the given position.</summary>
		public short this[int indexer]{
			get { return samples[indexer]; }
			set { samples[indexer] = value; }
		}

        /// <summary>Constructor.</summary>
        /// <param name="format">Format header information.</param>
        /// <param name="samples">Wave samples.</param>
		public WaveSound(WaveFormat format, short[] samples)
		{
			this.format = format;
			this.samples = samples;
		}

		/// <summary>Constructor.</summary>
		/// <param name="format">Format header information.</param>
		/// <param name="samplesStream">Wave samples. A stream of 'short' values.</param>
		public WaveSound(WaveFormat format, Stream samplesStream)
		{
			this.format = format;

			if(samplesStream.CanSeek)
			{
				samplesStream.Position = 0;
			}

			BinaryReader reader = new BinaryReader(samplesStream);
			this.samples = new short[samplesStream.Length / 2];
			long indexSamples = 0;
			while (samplesStream.Position < samplesStream.Length)
			{
				this.samples[indexSamples] = reader.ReadInt16();
				indexSamples++;
			}
		}

        /// <summary>Constructor.</summary>
		/// <param name="fileName">Path and name of a RIFF Wave file.</param>
        public WaveSound(string fileName)
        {
			this.ReadStream(new FileStream(fileName, FileMode.Open));
        }

		/// <summary>Constructor.</summary>
		/// <param name="fileContent">RIFF Wave stream.</param>
		public WaveSound(Stream fileContent)
		{
			this.ReadStream(fileContent);
		}

        /// <summary>Separates all samples of one channel.</summary>
        /// <param name="channelIndex">Index of the channel.</param>
        /// <returns>All samples that belong to the requested channel.</returns>
		private short[] GetChannelSamples(int channelIndex)
		{
			short[] channelSamples = new short[samples.Length];
			int channelSamplesIndex = 0;
			for (int n = channelIndex; n < samples.Length; n++)
			{
				channelSamples[channelSamplesIndex] = samples[n];
				channelSamplesIndex++;
			}
			
			//for (int n = channelIndex; n < samples.Length; n ++)
			//{
			//    if (channelIndex == 0)
			//    {	//left samples: high byte
			//        channelSamples[n] = (sbyte)(samples[n] >> 8);
			//    }
			//    else
			//    {	//right samples: low byte
			//        channelSamples[n] = (sbyte)(samples[n] & 0x00FF);
			//    }
			//}
			
			return channelSamples;
		}

        /// <summary>Creates a stream in RIFF Wave format from the wave information.</summary>
        /// <returns>Formatted stream.</returns>
		public Stream CreateStream()
		{
			MemoryStream stream = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(stream);
			Int32 dataLength = (Int32)(samples.Length * format.BitsPerSample / 8);

			writer.Write(System.Text.Encoding.ASCII.GetBytes("RIFF".ToCharArray()));

			writer.Write((Int32)(dataLength + 36)); //File length minus first 8 bytes of RIFF description

			writer.Write(System.Text.Encoding.ASCII.GetBytes("WAVEfmt ".ToCharArray()));

			writer.Write((Int32)16); //length of following chunk: 16

			writer.Write(format.FormatTag);
			writer.Write(format.Channels);
			writer.Write(format.SamplesPerSec);
			writer.Write(format.AvgBytesPerSec);
			writer.Write(format.BlockAlign);
			writer.Write(format.BitsPerSample);

			writer.Write(System.Text.Encoding.ASCII.GetBytes("data".ToCharArray()));

			writer.Write(dataLength);

			for (long n = 0; n < samples.Length; n++) {
				if (format.BitsPerSample == 8)
				{
					writer.Write((byte)samples[n]);
				}
				else
				{
					writer.Write(samples[n]);
				}
			}

			writer.Seek(0, SeekOrigin.Begin);
			return stream;
		}

        /// <summary>Reads a wave file.</summary>
        /// <returns>Format and samples.</returns>
        private void ReadStream(Stream stream)
        {
            BinaryReader reader = new BinaryReader(stream);

            this.format = ReadHeader(reader);

			if(format.BitsPerSample != 8 && format.BitsPerSample != 16){
				string message = string.Format(Constants.WaveSampleRateNotSupported, format.BitsPerSample);
				throw new ArgumentException(message);
			}


            int dataLength = reader.ReadInt32();

            int maxSampleValue = 0;
            int bytesPerSample = format.BitsPerSample / 8;
            int countSamples = dataLength / bytesPerSample;

            //sbyte[] channelSamples8 = new sbyte[countSamples];
			short[] channelSamples16 = new short[countSamples];
			sbyte channelSample8;

            int channelSamplesIndex = 0;
            int absValue;
            for (int sampleIndex = 0; sampleIndex < countSamples; sampleIndex++)
            {

                if (format.BitsPerSample == 8)
                {
					channelSample8 = reader.ReadSByte();
					channelSamples16[channelSamplesIndex] = (short)(channelSample8 + (channelSample8 << 8));
					if (Math.Abs((int)channelSample8) > maxSampleValue)
                    {
						maxSampleValue = Math.Abs((int)channelSample8);
                    }
                }
                else
                {
                    channelSamples16[channelSamplesIndex] = reader.ReadInt16();
                    absValue = Math.Abs((int)channelSamples16[channelSamplesIndex]);
                    if (absValue > maxSampleValue)
                    {
                        maxSampleValue = (absValue > short.MaxValue) ? absValue - 1 : absValue;
                    }
                }

                channelSamplesIndex++;
            }

            reader.Close();

			format.BitsPerSample = 16;
            this.samples = channelSamples16;
        }

        /// <summary>Read a chunk of four bytes from a wave file</summary>
        /// <param name="reader">Reader for the wave file.</param>
        /// <returns>Four characters.</returns>
        private string ReadChunk(BinaryReader reader)
        {
            byte[] ch = new byte[4];
            reader.Read(ch, 0, ch.Length);
            return System.Text.Encoding.ASCII.GetString(ch);
        }

        /// <summary>
        /// Read the header from a wave file, and move the
        /// reader's position to the beginning of the data chunk
        /// </summary>
        /// <param name="reader">Reader for the wave file.</param>
        /// <returns>Format of the wave.</returns>
        private WaveFormat ReadHeader(BinaryReader reader)
        {
            if (ReadChunk(reader) != "RIFF")
                throw new Exception("Invalid file format");

            reader.ReadInt32(); // File length minus first 8 bytes of RIFF description, we don't use it

            if (ReadChunk(reader) != "WAVE")
                throw new Exception("Invalid file format");

            if (ReadChunk(reader) != "fmt ")
                throw new Exception("Invalid file format");

            int len = reader.ReadInt32();
            if (len < 16) // bad format chunk length
                throw new Exception("Invalid file format");

            WaveFormat carrierFormat = new WaveFormat();
            carrierFormat.FormatTag = reader.ReadInt16();
            carrierFormat.Channels = reader.ReadInt16();
            carrierFormat.SamplesPerSec = reader.ReadInt32();
            carrierFormat.AvgBytesPerSec = reader.ReadInt32();
            carrierFormat.BlockAlign = reader.ReadInt16();
            carrierFormat.BitsPerSample = reader.ReadInt16();

            // advance in the stream to skip the wave format block 
            len -= 16; // minimum format size
            while (len > 0)
            {
                reader.ReadByte();
                len--;
            }

            // assume the data chunk is aligned
            string chunk;
            do
            {
                chunk = ReadChunk(reader);
            } while (reader.BaseStream.Position < reader.BaseStream.Length && chunk != "data");

            return carrierFormat;
        }

        /// <summary>Saves the sound to a wave file.</summary>
        /// <param name="fileName">Name and path of the destination file.</param>
		public void SaveToFile(string fileName)
		{
			Stream stream = CreateStream();
			FileStream file = new FileStream(fileName, FileMode.Create);

			int blockLength;
			long remainingLength;
			while (stream.Position < stream.Length)
			{
				remainingLength = stream.Length - stream.Position;

				if (remainingLength > int.MaxValue)
				{
					blockLength = int.MaxValue;
				}
				else
				{
					blockLength = (int)remainingLength;
				}

				byte[] buffer = new byte[blockLength];
				stream.Read(buffer, 0, blockLength);
				file.Write(buffer, 0, blockLength);
			}

			file.Close();
			stream.Close();
		}

	}
}
