﻿/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#region Using directives

using System;
using System.Collections;
using System.Text;

#endregion

namespace SteganoDotNet.Action {

    /// <summary>Compares Strings according to a custom alphabet.</summary>
    public class StringComparer : IComparer {

        private String alphabet;

        internal StringComparer(string alphabet)
        {
            this.alphabet = alphabet;
        }

        int IComparer.Compare(object x, object y)
        {
            String strX = x as String;
            String strY = y as String;

            if (strX == strY) {
                return 0;
            } else if (strX == null) {
                return -1;
            } else if (strY == null) {
                return 1;
            } else {

                if (strX.Length == 0) {
                    return -1;
                } else if (strY.Length == 0) {
                    return 1;
                } else {

                    //both values contain strings

                    int result = 0;
                    int length = (strX.Length < strY.Length) ? strX.Length : strY.Length;

                    for (int charIndex = 0; (charIndex < length) && (result == 0); charIndex++) {
                        //compare character until a difference is found
                        int indexX = alphabet.IndexOf(strX[charIndex]);
                        if (indexX < 0) {
                            //character not found on alphabet - try to find lowerase
                            indexX = alphabet.ToLower().IndexOf(strX.ToLower()[charIndex]);
                        }

                        int indexY = alphabet.IndexOf(strY[charIndex]);
                        if (indexY < 0) {
                            //character not found on alphabet - try to find lowerase
                            indexY = alphabet.ToLower().IndexOf(strY.ToLower()[charIndex]);
                        }

                        result = indexX.CompareTo(indexY);
                    }

                    if (result == 0) {
                        //no difference found - compare length
                        if (strX.Length < strY.Length) {
                            result = -1;
                        } else if (strY.Length < strX.Length) {
                            result = 1;
                        }
                    }

                    return result;
                }
            }
        }
    }
}
