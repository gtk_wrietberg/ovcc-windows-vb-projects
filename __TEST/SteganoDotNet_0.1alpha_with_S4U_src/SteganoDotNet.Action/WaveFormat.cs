/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Format header information of a RIFF Wave file.</summary>
	[StructLayout(LayoutKind.Sequential)]
	public class WaveFormat {

		/// <summary>Format tags - only Pcm is supported.</summary>
		public enum WaveFormatTags
		{
			/// <summary>The only supported format.</summary>
			Pcm = 1,
			/// <summary>Not supprted, listed only for documentation reasons.</summary>
			Float = 3
		}

        private short wFormatTag;
        private short nChannels;
        private int nSamplesPerSec;
        private int nAvgBytesPerSec;
        private short nBlockAlign;
        private short wBitsPerSample;
        private short cbSize;

        /// <summary>wFormatTag header field.</summary>
        public short FormatTag
        {
            get { return wFormatTag; }
            set { wFormatTag = value; }
        }

        /// <summary>nChannels header field.</summary>
        public short Channels
        {
            get { return nChannels; }
            set { nChannels = value; }
        }

        /// <summary>nSamplesPerSec header field.</summary>
        public int SamplesPerSec
        {
            get { return nSamplesPerSec; }
            set { nSamplesPerSec = value; }
        }

        /// <summary>nAvgBytesPerSec header field.</summary>
        public int AvgBytesPerSec
        {
            get { return nAvgBytesPerSec; }
            set { nAvgBytesPerSec = value; }
        }

        /// <summary>nBlockAlign header field.</summary>
        public short BlockAlign
        {
            get{ return nBlockAlign; }
            set { nBlockAlign = value; }
        }

        /// <summary>wBitsPerSample header field.</summary>
        public short BitsPerSample
        {
            get { return wBitsPerSample; }
            set { wBitsPerSample = value; }
        }

        /// <summary>cbSize header field.</summary>
        public short Size
		{
			get { return cbSize; }
			set { cbSize = value; }
		}

        /// <summary>Constructor.</summary>
		public WaveFormat()
		{
			//default constructor
		}

        /// <summary>Constructor.</summary>
        /// <param name="samplesPerSec">Number of samples per second.</param>
        /// <param name="bitsPerSample">Number of bits per sample.</param>
        /// <param name="channels">Number of channels.</param>
		public WaveFormat(int samplesPerSec, short bitsPerSample, short channels)
		{
			wFormatTag = (short)WaveFormatTags.Pcm;
			nChannels = channels;
			nSamplesPerSec = samplesPerSec;
			wBitsPerSample = bitsPerSample;
			cbSize = 0;

			nBlockAlign = (short)(channels * (bitsPerSample / 8));
			nAvgBytesPerSec = samplesPerSec * nBlockAlign;
		}
    }
}
