/* This class has been written by
 * Corinna John (Hannover, Germany)
 * cj@binary-universe.net
 * 
 * You may do with this code whatever you like,
 * except selling it or claiming any rights/ownership.
 * 
 * Please send me a little feedback about what you're
 * using this code for and what changes you'd like to
 * see in later versions.
 */

using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Describes the capabilities of wave sounds and creates instances of TapeWaveFileUtility.</summary>
    public class WaveFileType : FileType
	{

		private static WaveFileType current;

        /// <summary>Returns the only instance WaveFileType.</summary>
        public static new WaveFileType Current
		{
			get
			{
				if (current == null)
				{
					current = new WaveFileType();
				}
				return current;
			}
		}

		private WaveFileType()
		{
			unitName = "Sample";
			typeName = "Wave";
			supportsClipboard = true;
			supportsNoise = true;
			minBitsPerUnit = 1;
			maxBitsPerUnit = 8;
		}

        /// <summary>Creates a WaveFileUtility for the given file.</summary>
        /// <param name="fileName">Name and path of the wave file.</param>
        /// <returns>A WaveFileUtility object for the given sound.</returns>
        public override FileUtility CreateUtility(String fileName)
		{
			return new WaveFileUtility(fileName);
		}

        /// <summary>Creates a WaveFileUtility for the given sound.</summary>
        /// <param name="rawData">Sysm.IO.Stream with the wave sound.</param>
        /// <returns>A WaveFileUtility object for the given sound.</returns>
        public override FileUtility CreateUtility(object rawData)
		{
			return new WaveFileUtility(rawData);
		}

	}
}
