/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace SteganoDotNet.Action
{
    /// <summary>First-in/First-out stack of Int16 values.</summary>
	class FifoInt16Collection {
		private int maxLength;
		private Collection<short> content;

        /// <summary>The collection contains [maxLength] values.</summary>
		public bool IsFilled
		{
			get { return content.Count >= maxLength; }
		}

		/// <summary>Constructor.</summary>
		/// <param name="maxLength">
        /// Maximal size of the collection.
        /// If more values are added, older values are removed.
        /// </param>
        public FifoInt16Collection(int maxLength)
		{
			this.maxLength = maxLength;
			this.content = new Collection<short>();
		}

        /// <summary>Removes all values from the collection.</summary>
		public void Clear()
		{
			content.Clear();
		}

        /// <summary>
        /// Adds a value to the end of the collection.
        /// If there already are [maxLength] values, the first value is removed.
        /// </summary>
        /// <param name="value">The value to add.</param>
		public void Add(short value)
		{
			content.Add(value);
			if (content.Count > maxLength) {
				content.RemoveAt(0);
			}
		}

        /// <summary>Returns the value at the given position.</summary>
        /// <param name="index">Index of the requested value.</param>
        /// <returns>
        /// Value at the given position.
        /// If the index does not exists, the first value is returned.
        /// </returns>
		public short Get(int index)
		{
			if (content.Count > index) {
				return content[index];
			} else {
				return 0;
			}
		}

        /// <summary>Returns the lowest and the highest value.</summary>
        /// <returns>
        /// Two Int16 values.
        /// First element is the lowest value of the collection, second element is the highest one.
        /// </returns>
		public short[] GetMinMaxValues()
		{
			short minimum = 0;
			short maximum = 0;
			foreach (short value in content) {
				if (value > maximum) {
					maximum = value;
				} else if (value < minimum) {
					minimum = value;
				}
			}
			return new short[2] { minimum, maximum };
		}

        /// <summary>Finds values that are higher or lower than adjacent values.</summary>
        /// <param name="tolerance">
        /// Minimum difference between two elements.
        /// If the difference between one element and the next one is bigger
        /// than this value, the second element is considered a local peak.
        /// </param>
        /// <returns>Indices of all local peaks.</returns>
		public Collection<int> GetLocalPeaks(short tolerance)
		{
			Collection<int> localPeaks = new Collection<int>();
			short[] minmax = GetMinMaxValues();
			int minimum = minmax[0];
			int maximum = minmax[1];
			int minTolerance = -tolerance;
			int maxTolerance = tolerance;

			for (int index = 1; index < content.Count-1; index++) {
				if ((maximum - content[index] <= maxTolerance) && (content[index] > content[index + 1]) && (content[index] >= content[index - 1])) {
					localPeaks.Add(index);
				} else if ((minimum - content[index] >= minTolerance) && (content[index] < content[index + 1]) && (content[index] <= content[index - 1])) {
					localPeaks.Add(index);
				}
			}

			return localPeaks;
		}

        /// <summary>Returns the bigest an smallest distance (in element indices) of local peaks</summary>
        /// <param name="tolerance">
        /// Minimum difference between two elements.
        /// If the difference between one element and the next one is bigger
        /// than this value, the second element is considered a local peak.
        /// </param>
        /// <returns>
        /// Two indices.
        /// First index is the lowest distance etween two adjacent peaks, second index is the highest one.
        /// </returns>
        public int[] GetLocalPeaksMinMaxDistance(short tolerance)
		{
			Collection<int> localPeaks = GetLocalPeaks(tolerance);
			int distance;
			int minDistance = localPeaks.Count;
			int maxDistance = 0;
			for (int index = 1; index < localPeaks.Count; index++) {
				distance = localPeaks[index] - localPeaks[index - 1];
				if (distance > maxDistance) {
					maxDistance = distance;
				} else if (distance < minDistance) {
					minDistance = distance;
				}
			}
			return new int[2] { minDistance, maxDistance };
		}

	}
}
