/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {
    
    /// <summary>Describes the capabilities of text lists and creates instances of ListFileUtility.</summary>
    class ListFileType : FileType
    {

		private static ListFileType current;

        /// <summary>Returns the only instance ListFileType.</summary>
        public static new ListFileType Current
		{
			get
			{
				if (current == null)
				{
					current = new ListFileType();
				}
				return current;
			}
		}

		private ListFileType() {
			unitName = "Wort";
			typeName = "Liste";
			supportsClipboard = true;
			supportsNoise = false;
			minBitsPerUnit = 1;
			maxBitsPerUnit = 1;
		}

        /// <summary>Creates an ListFileUtility for the given file.</summary>
        /// <param name="fileName">Name and path of the text file.</param>
        /// <returns>An ListFileUtility object for the given file.</returns>
        public override FileUtility CreateUtility(string fileName)
		{
			return new ListFileUtility(fileName);
		}

        /// <summary>Creates an ListFileUtility for the given file.</summary>
        /// <param name="rawData">Any Collection&lt;string&gt;.</param>
        /// <remarks>The collection should contain different string, not many copies of the same string.</remarks>
        /// <returns>A ListFileUtility object for the given collection.</returns>
        public override FileUtility CreateUtility(object rawData)
		{
			return new ListFileUtility(rawData);
		}

	}
}
