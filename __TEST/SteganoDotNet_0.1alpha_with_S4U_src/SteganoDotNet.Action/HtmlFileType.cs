/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Describes the capabilities of HTML documents and creates instances of HtmlFileUtility.</summary>
    public class HtmlFileType : FileType
    {

		private static HtmlFileType current;

        /// <summary>Returns the only instance HtmlFileType.</summary>
        public static new HtmlFileType Current
		{
			get
			{
				if (current == null)
				{
					current = new HtmlFileType();
				}
				return current;
			}
		}

		private HtmlFileType()
		{
			unitName = "Attribut";
			typeName = "HTML";
			supportsClipboard = true;
			supportsNoise = false;
			minBitsPerUnit = 1;
			maxBitsPerUnit = 1;
		}

        /// <summary>Creates an HtmlFileUtility for the given file.</summary>
        /// <param name="fileName">Name and path of the HTML file.</param>
        /// <returns>An HtmlFileUtility object for the given document.</returns>
        public override FileUtility CreateUtility(String fileName)
		{
			return new HtmlFileUtility(fileName);
		}

        /// <summary>Creates an HtmlFileUtility for the given System.IO.Stream.</summary>
        /// <param name="rawData">A System.IO.Stream that contains an HTML document.</param>
        /// <returns>A BitmapFileUtility object for the given 24 bit image.</returns>
        public override FileUtility CreateUtility(object rawData)
		{
			return new HtmlFileUtility(rawData);
		}

	}
}
