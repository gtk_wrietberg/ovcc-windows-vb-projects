/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Reflection;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.Action {

    /// <summary>Base class of all concrete classes which hide and extract streams in and from carriers.</summary>
	public abstract class FileUtility : IDisposable {

		/// <summary>Dummy file name for carriers in memory.</summary>
		protected const string NoSourceFileName = "Memory";

		/// <summary>Number of bits that will be stored in each carrier unit.</summary>
		protected int countUsedBitsPerUnit;

		/// <summary>Number of carrier units.</summary>
		protected int countUnits;
		
		/// <summary>Number of bytes that will be used to store the message's length.</summary>
		protected int lengthCountUnits = 4; //store length as Int32

		/// <summary>Name and path of the source file.</summary>
		protected string sourceFileName;

		/// <summary>Number of bytes that will be hidden in the carrier file.</summary>
		protected int countBytesToHide;
		
		/// <summary>Amount of data that will be hidden in the carrier file, in percent of the message.</summary>
		protected float percentOfMessage;

		/// <summary>SteganoDotNet.UserControls.ICarrierConfiguration control that can be used to configure the utility.</summary>
		protected ICarrierConfiguration carrierConfigurationControl;

		/// <summary>Destination file name.</summary>
		protected Stream outputStream;

		/// <summary>Embeds a stream in a carrier.</summary>
		/// <param name="message">The stream.</param>
		/// <param name="key">A key stream that is used differently by the concrete classes.</param>
        public abstract void Hide(Stream message, Stream key);

        /// <summary>Extracts a stream from the carrier.</summary>
        /// <param name="key">A key stream that is used differently by the concrete classes.</param>
        /// <returns>The extracted stream.</returns>
        public abstract Stream Extract(Stream key);

		/// <summary>Called after change of settings.</summary>
		public event EventHandler SettingsChanged;

        /// <summary>Returns a SteganoDotNet.UserControls.ICarrierConfiguration control that can be used to configure the utility.</summary>
		public ICarrierConfiguration CarrierConfigurationControl
		{
			get
			{
				return carrierConfigurationControl;
			}
		}

        /// <summary>Returns the FileType instance for supported files.</summary>
        public abstract FileType FileType
		{
			get;
		}

        /// <summary>Returns name and path of the source file.</summary>
        public string SourceFileName
		{
			get { return sourceFileName; }
		}

        /// <summary>Returns or sets the number of bytes that will be hidden in the carrier file.</summary>
		public int CountBytesToHide
		{
			get
			{
				return countBytesToHide;
			}
			set
			{
				countBytesToHide = value;
			}
		}

        /// <summary>Returns or sets the amount of data that will be hidden in the carrier file, in percent of the message.</summary>
        public virtual float PercentOfMessage
		{
			get
			{
				return percentOfMessage;
			}
			set
			{
				percentOfMessage = value;

				if (carrierConfigurationControl != null)
				{
					ITapeWaveConfiguration tapeConfig = carrierConfigurationControl as ITapeWaveConfiguration;				
					tapeConfig.PercentOfMessage = value;
				}

				OnSettingsChanged();
			}
		}

		/// <summary>Returns or sets the number of carrier units.</summary>
		public virtual int CountUnits
		{
			get
			{
				return countUnits;
			}
			set
			{
				countUnits = value;
			}
		}

        /// <summary>Returns or sets the number of bits that will be stored in each carrier unit.</summary>
		public virtual int CountUsedBitsPerUnit
		{
			get
			{
				return countUsedBitsPerUnit;
			}
			set
			{
				countUsedBitsPerUnit = value;
				OnSettingsChanged();
			}
		}

        /// <summary>Returns the maximum number of bytes that can be hidden in the carrier file.</summary>
        public virtual int Capacity
		{
			get {
				int capacity = (int)Math.Floor((((float)(countUnits * countUsedBitsPerUnit)) / 8));
				if (capacity > lengthCountUnits)
				{
					return capacity - lengthCountUnits;
				}
				else
				{
					return 0;
				}
			}
		}

        /// <summary>Returns or sets the destination file name.</summary>
		public Stream OutputStream
		{
			get
			{
				return outputStream;
			}
		}

        /// <summary>Calls the SettingsChanged event handler.</summary>
		protected void OnSettingsChanged()
		{
			if (SettingsChanged != null) {
				SettingsChanged(this, EventArgs.Empty);
			}
		}

        /// <summary>
        /// Should be overwritten by concrete classes, to check, if the key can be used to hide the message.
        /// </summary>
        /// <returns>Always returns string.Empty.</returns>
        public virtual string CheckMessageAndKey(Stream message, Stream key)
        {
            return string.Empty;
        }

		/// <summary>Copy a bit from [messageByte] into to lowest bit of [carrierByte]</summary>
		/// <param name="messageBitIndex">Position of the bit to copy</param>
		/// <param name="messageByte">a byte from the message stream</param>
		/// <param name="carrierBitIndex">Position of the bit in [carrierByte]</param>
		/// <param name="carrierByte">a byte from the carrier file</param>
		/// <returns>Changed [carrierByte]</returns>
		protected byte SetBit(int messageBitIndex, byte messageByte, int carrierBitIndex, byte carrierByte)
		{
			//get one bit of the current message byte...
			bool messageBit = ((messageByte & (1 << messageBitIndex)) > 0);
			//get one bit of the carrier byte
			bool carrierBit = ((carrierByte & (1 << carrierBitIndex)) > 0);

			//place [messageBit] in the corresponding bit of [carrierByte]
			if (messageBit && !carrierBit) {
				carrierByte += (byte)(1 << carrierBitIndex);
			} else if (!messageBit && carrierBit) {
				carrierByte -= (byte)(1 << carrierBitIndex);
			}

			return carrierByte;
		}

		/// <summary>Copy the lowest bit from [carrierByte] into a specific bit of [messageByte]</summary>
		/// <param name="messageBitIndex">Position of the bit in [messageByte]</param>
		/// <param name="messageByte">a byte to write into the message stream</param>
		/// <param name="carrierBitIndex">Position of the bit in [carrierByte]</param>
		/// <param name="carrierByte">a byte from the carrier file</param>
		/// <returns>Changed [messageByte]</returns>
		protected byte GetBit(int messageBitIndex, byte messageByte, int carrierBitIndex, byte carrierByte)
		{
			int carrierBit = ((carrierByte & (1 << carrierBitIndex)) > 0) ? 1 : 0;
			messageByte += (byte)(carrierBit << messageBitIndex);
			return messageByte;
		}

        /// <summary>
        /// Gets the parts of the message that will be hidden in this carrier.
        /// The excerpt begins at the current position of the [message] stream,
        /// its length is specified by the CountBytesToHide property.
        /// </summary>
        /// <param name="message">The full message.</param>
        /// <returns>An excerpt from the message.</returns>
		public virtual Stream GetMessagePart(Stream message)
		{
			BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());
			messageWriter.Write(countBytesToHide);

			byte[] buffer = new byte[countBytesToHide];
			message.Read(buffer, 0, countBytesToHide);
			messageWriter.Write(buffer);

			messageWriter.Seek(0, SeekOrigin.Begin);
			return messageWriter.BaseStream;
		}

        /// <summary>ICarrierConfiguration controls use this event handler method to write back new settings.</summary>
        protected virtual void CarrierConfiguration_AcceptSettings(ICarrierConfiguration sender, EventArgs e)
		{
			OnSettingsChanged();
		}

		/// <summary>
		/// Read the next byte of the key stream.
		/// Reset the stream if it is too short.
		/// </summary>
		/// <returns>The next key byte</returns>
		protected byte GetKeyValue(Stream keyStream)
		{
			int keyValue = 0;
			if (keyStream.Length > 0)
			{
				if ((keyValue = keyStream.ReadByte()) < 0)
				{
					keyStream.Seek(0, SeekOrigin.Begin);
					keyValue = keyStream.ReadByte();
				}
			}
			return (byte)keyValue;
		}

		/// <summary>Save the result of the last Hide() to a file</summary>
		public virtual void SaveResult(string fileName)
		{
			if (this.outputStream == null)
			{
				throw new InvalidOperationException("There is no output stream to save.");
			}

			this.outputStream.Position = 0;

			using (Stream dstStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
			{
				CopyStream(this.outputStream, dstStream);
				dstStream.Close();
			}
		}

		/// <summary>Copy the content of one stream to another stream</summary>
		protected void CopyStream(Stream srcStream, Stream dstStream)
		{
			int blockLength;
			long remainingLength;
			while (srcStream.Position < srcStream.Length)
			{
				remainingLength = srcStream.Length - srcStream.Position;

				if (remainingLength > int.MaxValue)
				{
					blockLength = int.MaxValue;
				}
				else
				{
					blockLength = (int)remainingLength;
				}

				byte[] buffer = new byte[blockLength];
				srcStream.Read(buffer, 0, blockLength);
				dstStream.Write(buffer, 0, blockLength);
			}
		}

        /// <summary>Sorts lines of text according to an alphabet.</summary>
        /// <param name="lines">The lines.</param>
        /// <param name="alphabet">The alphabet to be used.</param>
        /// <returns>Sorted lines.</returns>
		internal Collection<string> SortLines(Collection<string> lines, String alphabet)
		{
			String[] sortedLines = new String[lines.Count];
			lines.CopyTo(sortedLines, 0);

			if ((alphabet == null) || (alphabet.Length == 0))
			{
				//use alphabet
				Array.Sort(sortedLines);
			}
			else
			{
				//user custom character order
				StringComparer comparer = new StringComparer(alphabet);
				Array.Sort(sortedLines, comparer);
			}

			//Collection<string> sortedList = new Collection<string>(sortedLines);
			Collection<string> sortedList = new Collection<string>();
			foreach(string line in sortedLines){ sortedList.Add(line); }
			return sortedList;
		}

        /// <summary>Reads a text file.</summary>
        /// <param name="fileName">Name and path of a text file.</param>
        /// <returns>Content of the text file.</returns>
		protected string GetTextFromFile(string fileName)
		{
			string fileContent = string.Empty;
			if (fileName.Length > 0)
			{
				StreamReader reader = new StreamReader(fileName, Encoding.Default);
				fileContent = reader.ReadToEnd();
				reader.Close();
			}
			return fileContent;
		}

        /// <summary>Reads lines from a text file.</summary>
        /// <param name="fileName">Name and path of a text file.</param>
        /// <returns>Content of the text file.</returns>
        protected Collection<string> GetLinesFromFile(string fileName)
		{
			string fileContent = GetTextFromFile(fileName);
			string[] lines = fileContent.Split('\r');
			Collection<string> linesCollection = new Collection<string>();
			foreach (string line in lines)
			{
				linesCollection.Add(line.Trim('\n'));
			}
			return linesCollection;
		}

		/// <summary>Create a CarrierConfiguration control.</summary>
		/// <param name="className">Name of the class in SteganoDotNet.UserControls.</param>
		protected void CreateCarrierConfigurationControl(string className)
		{
			CreateCarrierConfigurationControl(className, new object[0]);
		}
		
		/// <summary>Create a CarrierConfiguration control.</summary>
		/// <param name="className">Name of the class in SteganoDotNet.UserControls.</param>
		/// <param name="constructorParameters">Constructor parameters.</param>
		protected void CreateCarrierConfigurationControl(string className, params object[] constructorParameters)
		{
			System.Reflection.Assembly assembly = Assembly.Load(Constants.UserControlsNamespace);
			
			if(assembly == null)
			{
				Console.WriteLine("Warning: 'SteganoDotNet.UserControls' is not implemented for Mono.");
			}
			else
			{
				className = string.Format("{0}.{1}", Constants.UserControlsNamespace, className);
				Type type = assembly.GetType(className, false);
				if(type == null)
				{
					Console.WriteLine("Warning: Class 'SteganoDotNet.UserControls.{0}' is not implemented for Mono.", className);
					type = assembly.GetType("CarrierConfiguration", false);
				}
				if(type != null)
				{
					carrierConfigurationControl = (ICarrierConfiguration)Activator.CreateInstance(type, constructorParameters);
					AddEventHandlerToCarrierConfigurationControl("AcceptSettings", "CarrierConfiguration_AcceptSettings");
				}
			}
		}
		
		protected void AddEventHandlerToCarrierConfigurationControl(string eventName, string eventHandlerMethodName)
		{
			if(carrierConfigurationControl != null)
			{
				EventInfo eventAcceptSettings = carrierConfigurationControl.GetType().GetEvent(eventName);
				Type typeAcceptSettings = eventAcceptSettings.EventHandlerType;
			
				System.Reflection.MethodInfo methodHandler = this.GetType().GetMethod(eventHandlerMethodName, BindingFlags.NonPublic | BindingFlags.Instance);
				Delegate delegateHandler = Delegate.CreateDelegate(typeAcceptSettings, this, methodHandler);
			
				System.Reflection.MethodInfo addHandler = eventAcceptSettings.GetAddMethod();
				addHandler.Invoke(carrierConfigurationControl, new object[]{ delegateHandler });
			}
		}

        /// <summary>Overrides Object.GetHashCode().</summary>
        /// <returns>The HashCode of teh OutputFileName property.</returns>
		public override int GetHashCode()
		{
			return sourceFileName.GetHashCode();
		}

        /// <summary>Overrides Object.Equals().</summary>
        /// <param name="obj">Another FileUtility.</param>
        /// <returns>true, if the output file names are the sames, otherwise false.</returns>
		public override bool Equals(object obj)
		{
            FileUtility other = obj as FileUtility;
            if (other == null)
            {
                return false;
            }
            else
            {
                return (this.sourceFileName == other.sourceFileName);
            }
		}

        /// <summary>Returns the source file name.</summary>
        public override string ToString()
		{
			return sourceFileName;
		}

		#region IDisposable Members

		/// <summary>
		/// Disposes the output stream.
		/// </summary>
		public void Dispose()
		{
			if (this.outputStream != null)
			{
				try
				{
					this.outputStream.Close();
				}
				finally
				{
					this.outputStream.Dispose();
					this.outputStream = null;
				}
			}
		}

		#endregion
	}
}
