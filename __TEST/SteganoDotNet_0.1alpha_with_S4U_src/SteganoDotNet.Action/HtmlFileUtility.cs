/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#region Using directives

using System;
using System.IO;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using SteganoDotNet.Interfaces;

#endregion

namespace SteganoDotNet.Action
{
    /// <summary>Hides and extract streams in and from HTML documents.</summary>
    public class HtmlFileUtility : FileUtility
    {
		/// <summary>A required parameter has not been initialized.</summary>
		public event MissingValueHandler MissingAlphabetFileName;

		string alphabetFileName;
		private Stream sourceStream;

        /// <summary>Returns the FileType instance for supported files.</summary>
        public override FileType FileType
		{
			get
			{
				return HtmlFileType.Current;
			}
		}

        /// <summary>Returns or sets the name and path of a file that will be used an the custom alphabet.</summary>
        public string AlphabetFileName
        {
            get
            {
                return alphabetFileName;
            }
            set
            {
                alphabetFileName = value;
            }
        }

		/// <summary>Constructor.</summary>
		/// <param name="fileName">Name and path of an HTML file.</param>
        public HtmlFileUtility(string fileName)
		{
			sourceFileName = fileName;
			
			//copy source file
			FileStream sourceFileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			byte[] buffer = new byte[sourceFileStream.Length];
			sourceFileStream.Read(buffer, 0, buffer.Length);
			sourceStream = new MemoryStream(buffer);
			sourceFileStream.Close();

			Initialize();
		}

        /// <summary>Constructor.</summary>
        /// <param name="rawData">System.IO.Stream filled with an HTML document.</param>
        public HtmlFileUtility(object rawData)
		{
            sourceFileName = NoSourceFileName;
			sourceStream = (Stream)rawData;
			Initialize();
		}

        private void Initialize()
		{
			countUsedBitsPerUnit = 1;
			lengthCountUnits = 1; //store length as Byte to save capacity
			countUnits = GetCapacity();
			CreateCarrierConfigurationControl("ListConfiguration");
		}

        /// <summary>ListConfiguration uses this event handler method to write back new settings.</summary>
        protected override void CarrierConfiguration_AcceptSettings(ICarrierConfiguration sender, EventArgs e)
		{
			IListConfiguration control = (IListConfiguration)sender;

			percentOfMessage = control.PercentOfMessage;
			alphabetFileName = control.AlphabetFileName;
			base.CarrierConfiguration_AcceptSettings(sender, e);
		}

		private string OnMissingAlphabetFileName()
		{
			string fileName = string.Empty;
			if (MissingAlphabetFileName != null)
			{
				MissingValueEventArgs eventArgs = new MissingValueEventArgs();
				MissingAlphabetFileName(this, eventArgs);
				fileName = eventArgs.RequiredValue as string;
				if (fileName == null)
				{
					fileName = string.Empty;
				}
			}
			return fileName;
		}

        /// <summary>Counts the attributes in an HTML document.</summary>
        /// <returns>Count of bytes that can be hidden in the document.</returns>
        public int GetCapacity() {
            int countCarrierAttributes = 0;

			StreamReader reader = new StreamReader(sourceStream, Encoding.Default);
            String htmlDocument = reader.ReadToEnd();
            
            Collection<HtmlTag> tags = FindTags(htmlDocument);

            foreach (HtmlTag tag in tags) {
                countCarrierAttributes += tag.Attributes.Count - 1;
            }

            return countCarrierAttributes;
        }

		/// <summary>Searches a collection of HtmlAttributes for an element with a specific Name value</summary>
		/// <param name="list">The collection to search.</param>
		/// <param name="attributeName">Name of the attribute</param>
		/// <returns>Attribute, or null if the name was not found</returns>
		public HtmlTag.HtmlAttribute GetByName(Collection<HtmlTag.HtmlAttribute> list, string attributeName)
		{
			HtmlTag.HtmlAttribute foundAttribute = null;
			foreach (HtmlTag.HtmlAttribute attribute in list)
			{
				if (attribute.Name == attributeName)
				{
					foundAttribute = attribute;
					break;
				}
			}
			return foundAttribute;
		}

        /// <summary>Hides the given message stream in the HTML document.</summary>
        /// <param name="message">Message.</param>
        /// <param name="key">
        /// Not used, can be null.
        /// Use the AlphabetFileName property to customize the attribute sorting.
        /// </param>
        public override void Hide(Stream message, Stream key)
		{
			//read the carrier document
            StreamReader reader = new StreamReader(sourceFileName, Encoding.Default);
            String htmlDocument = reader.ReadToEnd();
            reader.Close();

            message.Position = 0;

            //list the HTML tags
            Collection<HtmlTag> tags = FindTags(htmlDocument);

			//read custom alphabet
			string alphabet = GetTextFromFile(alphabetFileName);

            StringBuilder insertTextBuilder = new StringBuilder();
            int offset = 0;
            int messageByte = 0;
            int bitIndex = 7;
            bool messageBit = false;
            int listElementIndex = 0;
            Random random = new Random();

            foreach (HtmlTag tag in tags) {

                insertTextBuilder.Remove(0, insertTextBuilder.Length);
                insertTextBuilder.AppendFormat("<{0}", tag.Name);

                //list attribute names

				Collection<string> attributeNames = new Collection<string>();
				for (int n = 0; n < tag.Attributes.Count; n++)
				{
					attributeNames.Add(tag.Attributes[n].Name);
				}

                Collection<string> resultList = new Collection<string>();
				Collection<string> originalList = SortLines(attributeNames, alphabet);

                if (tag.Attributes.Count > 1) {

                    //sort attributes

					for (int n = 0; n < tag.Attributes.Count - 1; n++)
					{

                        //get next bit of the message

                        bitIndex++;
                        if (bitIndex == 8) {
                            bitIndex = 0;
                            if (messageByte > -1) {
                                messageByte = message.ReadByte();
                            }
                        }

                        if (messageByte > -1) {

                            //decide which attribute is going to be the next one in the new tag
                            
                            messageBit = ((messageByte & (1 << bitIndex)) > 0) ? true : false;

                            if (messageBit) {
                                listElementIndex = 0;
                            } else {
                                listElementIndex = random.Next(1, originalList.Count);
                            }
                        } else {
                            listElementIndex = 0;
                        }

                        //move the attribute from old list to new list

                        resultList.Add(originalList[listElementIndex]);
                        originalList.RemoveAt(listElementIndex);
                    }
                }

                if (originalList.Count > 0) {
                    //add the last element - it never hides any data
                    resultList.Add(originalList[0]);
                }


                HtmlTag.HtmlAttribute attribute;
                foreach (String attributeName in resultList) {
					attribute = GetByName(tag.Attributes, attributeName);

                    insertTextBuilder.Append(" ");
                    if (attribute.Value.Length > 0) {
                        insertTextBuilder.AppendFormat("{0}={1}", attribute.Name, attribute.Value);
                    } else {
                        insertTextBuilder.Append(attributeName);
                    }
                }

                //replace old tag with new tag

                tag.BeginPosition += offset;
                tag.EndPosition += offset;

                String insertText = insertTextBuilder.ToString();
                int newLength = insertText.Length;
                if (newLength > 0) {
                    int oldLength = tag.EndPosition - tag.BeginPosition;
                    htmlDocument = htmlDocument.Remove(tag.BeginPosition, oldLength);
                    htmlDocument = htmlDocument.Insert(tag.BeginPosition, insertText);

                    offset += (newLength - oldLength);
                }

                if (messageByte < 0) {
                    break; //finished
                }
            }

            //save the new document
			outputStream = new MemoryStream();
			StreamWriter writer = new StreamWriter(outputStream);
            writer.Write(htmlDocument);
        }

        /// <summary>Extracts a stream from the HTML document.</summary>
        /// <param name="key">
        /// Not used, can be null.
        /// Use the AlphabetFileName property to customize the attribute sorting.
        /// </param>
        /// <returns>The extracted stream.</returns>
        public override Stream Extract(Stream key)
		{
			//initialize empty writer for the message
            BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());

			if (alphabetFileName == null)
			{
				alphabetFileName = OnMissingAlphabetFileName();
				if (alphabetFileName.Length == 0)
				{
					throw new InvalidOperationException("No alphabet file.");
				}
			}
            
            //read the carrier document
            StreamReader reader = new StreamReader(sourceFileName, Encoding.Default);
            String htmlDocument = reader.ReadToEnd();
            reader.Close();

            //list the HTML tags
            Collection<HtmlTag> tags = FindTags(htmlDocument);

			//read custom alphabet
			string alphabet = GetTextFromFile(alphabetFileName);

            int messageLength = 0;
            int messageBit = 0;
            int bitIndex = 0;
            int messageByte = 0;

            foreach (HtmlTag tag in tags) {
             
                if (tag.Attributes.Count > 1) {

                    //list attribute names

                    Collection<string> attributeNames = new Collection<string>();
					Collection<string> carrierList = new Collection<string>();
					for (int n = 0; n < tag.Attributes.Count; n++)
					{
						attributeNames.Add(tag.Attributes[n].Name);
						carrierList.Add(tag.Attributes[n].Name);
					}

					//remove useless attribute
					carrierList.RemoveAt(carrierList.Count - 1);

                    //sort -> get original list
					Collection<string> originalList = SortLines(attributeNames, alphabet);
                   
                    foreach (String s in carrierList) {

                        //decide which bit the entry's position hides

                        if (s == originalList[0]) {
                            messageBit = 1;
                        } else {
                            messageBit = 0;
                        }

                        //remove the color from the sorted list
                        originalList.Remove(s);

                        //add the bit to the message
                        messageByte += (byte)(messageBit << bitIndex);

                        bitIndex++;
                        if (bitIndex > 7) {
                            if (messageLength == 0) {
								messageLength = messageByte;
                            } else {
                                //append the byte to the message
                                messageWriter.Write((byte)messageByte);
                                if (messageWriter.BaseStream.Length == messageLength) {
                                    //finished
                                    break;
                                }
                            }
                            messageByte = 0;
                            bitIndex = 0;
                        }
                    }
                }

                if ((messageLength > 0) && (messageWriter.BaseStream.Length == messageLength)) {
                    //finished
                    break;
                }
            }

            //return message stream
            messageWriter.Seek(0, SeekOrigin.Begin);
            return messageWriter.BaseStream;
        }

        /// <summary>List all HTML tags of a document</summary>
        /// <param name="htmlDocument"></param>
        /// <returns>List with</returns>
        private Collection<HtmlTag> FindTags(String htmlDocument) {
            Collection<HtmlTag> tags = new Collection<HtmlTag>();
            int indexStart = 0, indexEnd = 0;
            String text;
            do {

                indexStart = htmlDocument.IndexOf('<', indexEnd + 1);
                if (indexStart > 0) {
                    indexEnd = htmlDocument.IndexOf('>', indexStart + 1);
                    if (indexEnd > 0) {
                        if (htmlDocument[indexStart + 1] != '/') {
                            //Ende vom Start-Tag gefunden
                            text = htmlDocument.Substring(indexStart, indexEnd - indexStart);
                            tags.Add(new HtmlTag(text, indexStart, indexEnd));
                        }
                    }
                }

            } while (indexStart > 0);

            return tags;
        }

        /// <summary>
        /// Gets the parts of the message that will be hidden in this carrier.
        /// The excerpt begins at the current position of the [message] stream,
        /// its length is specified by the CountBytesToHide property.
        /// </summary>
        /// <param name="message">The full message.</param>
        /// <returns>An excerpt from the message.</returns>
        public override Stream GetMessagePart(Stream message)
		{
			BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());
			messageWriter.Write((byte)countBytesToHide);

			byte[] buffer = new byte[countBytesToHide];
			message.Read(buffer, 0, countBytesToHide);
			messageWriter.Write(buffer);

			messageWriter.Seek(0, SeekOrigin.Begin);
			return messageWriter.BaseStream;
		}
    }
}
