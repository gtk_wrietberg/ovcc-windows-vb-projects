/* This class has been written by
 * Corinna John (Hannover, Germany)
 * cj@binary-universe.net
 * 
 * You may do with this code whatever you like,
 * except selling it or claiming any rights/ownership.
 * 
 * Please send me a little feedback about what you're
 * using this code for and what changes you'd like to
 * see in later versions.
 */

using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Describes the capabilities of HTML documents and creates instances of HtmlFileUtility.</summary>
    public class MidiFileType : FileType
	{

		private static MidiFileType current;

        /// <summary>Returns the only instance MidiFileType.</summary>
        public static new MidiFileType Current
		{
			get
			{
				if (current == null)
				{
					current = new MidiFileType();
				}
				return current;
			}
		}

		private MidiFileType()
		{
			unitName = "Program Change Event";
			typeName = "MIDI";
			supportsClipboard = false;
			supportsNoise = false;
			minBitsPerUnit = 1;
			maxBitsPerUnit = 8;
		}

        /// <summary>Creates an MidiFileUtility for the given file.</summary>
        /// <param name="fileName">Name and path of the MIDI file.</param>
        /// <returns>An MidiFileUtility object for the given file.</returns>
        public override FileUtility CreateUtility(String fileName)
		{
			return new MidiFileUtility(fileName);
		}

		/// <summary>Not supported.</summary>
		public override FileUtility CreateUtility(object rawData)
		{
			throw new NotSupportedException();
		}

	}
}
