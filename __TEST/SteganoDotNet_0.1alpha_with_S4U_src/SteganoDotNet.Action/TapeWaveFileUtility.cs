/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.Action
{

    /// <summary>Hides and extract streams in and from wave sounds.</summary>
    public class TapeWaveFileUtility : FileUtility
	{
		private WaveSound waveSound;
		private string soxPath;
		private short volume = 6000;
		private int frequencyHz = 2500;
		private int beepLength = 32;
		private float beepLengthInSeconds = 1/32;
		private int startTagOffset = -1;

		/// <summary>seconds for Extract()</summary>
		private Collection<float> seconds = new Collection<float>();
		
		/// <summary>Called by FindBeeps for every Beep detected</summary>
		public event BeepFoundHandler BeepFound;

		/// <summary>Tells the dispatcher form to show the configuration dialog</summary>
		public event EventHandler OpenTapeForm;

		/// <summary>Informs the dispatcher about missing SoX executeable</summary>
		public event EventHandler SoundExchangeNotFound;

        /// <summary>Returns the FileType instance for supported files.</summary>
        public override FileType FileType
		{
			get
			{
				return TapeWaveFileType.Current;
			}
		}

        /// <summary>Path of the SoX executable: X:\\somewhere\\sox.exe</summary>
		public string SoxPath
		{
			get{ return soxPath; }
			set { soxPath = value; }
		}

        /// <summary>Calculates the number of bytes that can be hidden in the sound.</summary>
		public override int Capacity
		{
			get
			{
				int capacity = (int)((float)waveSound.Samples.Length / (waveSound.Format.Channels * waveSound.Format.SamplesPerSec * 30));
				return capacity;
			}
		}

        /// <summary>Returns or sets the frequency that will be used to hide/find the message.</summary>
		public int Frequency
		{
			get
			{
				return frequencyHz;
			}
			set
			{
				frequencyHz = value;
			}
		}

        /// <summary>Returns or sets the volume of inserted/extracted samples in maximum sample value.</summary>
        public short Volume
		{
			get
			{
				return volume;
			}
			set
			{
				volume = value;
			}
		}

        /// <summary>Returns or sets the length of an inserted noise in 1/second.</summary>
        public int BeepLength
		{
			get
			{
				return beepLength;
			}
			set
			{
				beepLength = value;
				beepLengthInSeconds = 1 / (float)beepLength;
			}
		}

        /// <summary>Returns or sets the offset for the start-of-message marker in seconds.</summary>
        /// <remarks>Set this property to -1, if you don't want to use a start marker.</remarks>
        public int StartTagOffset
		{
			get
			{
				return startTagOffset;
			}
			set
			{
				startTagOffset = value;
			}
		}

        /// <summary>Returns or sets the collection of detected noise-intervals.</summary>
        public Collection<float> Seconds
		{
			get
			{
				return seconds;
			}
			set
			{
				seconds = new Collection<float>(value);
			}
		}

        /// <summary>Returns format and samples of the wave.</summary>
		public WaveSound WaveSound
		{
			get { return waveSound; }
		}

        /// <summary>Constructor.</summary>
        /// <param name="waveSound">Format and samples of the wave.</param>
		public TapeWaveFileUtility(WaveSound waveSound)
		{
			sourceFileName = null;
			this.waveSound = waveSound;
			Initialize();
		}

        /// <summary>Constructor.</summary>
        /// <param name="fileName">Name and path of a wave file.</param>
		public TapeWaveFileUtility(string fileName)
		{
			sourceFileName = fileName;
			waveSound = ReadFile(fileName);
			Initialize();
		}

		private void Initialize()
		{
			//ensure availability of "Sound Exchange"
			FindSoX();

			if ((waveSound.Format.SamplesPerSec != 44100) || (waveSound.Format.BitsPerSample != 16) || (waveSound.Format.Channels != 2))
			{	//convert to standard format
				if (sourceFileName == null)
				{
					ConvertToDefaultFormat();
				}
				else
				{
					ConvertToDefaultFormat(sourceFileName);
				}
			}

			CreateCarrierConfigurationControl("TapeWaveConfiguration");
			AddEventHandlerToCarrierConfigurationControl("OpenTapeForm", "CarrierConfiguration_OpenTapeForm");

			ITapeWaveConfiguration tapeWaveConfiguration = (ITapeWaveConfiguration)carrierConfigurationControl; 
			tapeWaveConfiguration.Frequency = frequencyHz;
			tapeWaveConfiguration.Volume = volume;
			tapeWaveConfiguration.BeepLength = beepLength;
		}

		private void FindSoX()
		{
			//soxPath = System.Configuration.ConfigurationManager.AppSettings["SoX"];
			soxPath = System.Configuration.ConfigurationSettings.AppSettings["SoX"];

			if (soxPath == null || soxPath.Length == 0 || !File.Exists(soxPath))
			{
				soxPath = string.Empty;
				string executablePath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

				//search for SoX in current directory and all parent directories
				DirectoryInfo info = new DirectoryInfo(executablePath);
				while ((soxPath.Length == 0) && (info != null))
				{
					soxPath = FindSoxInDirectory(info.FullName);
					info = Directory.GetParent(info.FullName);
				}

				if (soxPath.Length == 0)
				{
					//search for SoX in "Program Files"
					soxPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
					soxPath = FindSoxInDirectory(soxPath);
				}

				if (soxPath.Length == 0)
				{
					if (SoundExchangeNotFound != null)
					{
						SoundExchangeNotFound(this, EventArgs.Empty);
					}
				}
			}
		}

		private string FindSoxInDirectory(string parentDirectory)
		{
			string result = String.Empty;
			string[] subDirectories = Directory.GetDirectories(parentDirectory, "sox*");
			if (subDirectories.Length > 0)
			{
				//SoX found in a subdirectory
				String[] files;
				for (int n = 0; n < subDirectories.Length; n++)
				{
					files = Directory.GetFiles(subDirectories[n], "sox.exe", SearchOption.AllDirectories);
					if (files.Length == 1)
					{
						result = files[0];
						break;
					}
				}
			}
			return result;
		}

		/// <summary>Convert the opened 8 bit sound to 16 bit.</summary>
		private void ConvertToDefaultFormat()
		{
			string inFileName = Path.GetTempFileName();
			waveSound.SaveToFile(inFileName);
			ConvertToDefaultFormat(inFileName);
		}

		/// <summary>Convert an 8 bit sound to 16 bit.</summary>
		/// <param name="inFileName">Name and path of the file to convert.</param>
		private void ConvertToDefaultFormat(string inFileName)
		{
			string outFileName = Path.GetTempFileName();
			
			String soxArguments = String.Format(
				"-t wav \"{0}\" -t .wav -r 44100 -c 2 -s -w \"{1}\"",
				inFileName,
				outFileName);

			RunSox(soxArguments);

			this.waveSound = ReadFile(outFileName);
		}

        /// <summary>TapeWaveConfiguration uses this event handler method to write back new settings.</summary>
        protected override void CarrierConfiguration_AcceptSettings(ICarrierConfiguration sender, EventArgs e)
		{
			ITapeWaveConfiguration control = (ITapeWaveConfiguration)sender;

			countUsedBitsPerUnit = -1;
			percentOfMessage = control.PercentOfMessage;
			countBytesToHide = 0; //calculated from countBytesToHidePercent
			beepLength = control.BeepLength;
			
			base.CarrierConfiguration_AcceptSettings(sender, e);
		}

        /// <summary>TapeWaveConfiguration uses this event handler method to open the configuration dialog.</summary>
		private void CarrierConfiguration_OpenTapeForm(object sender, EventArgs e)
		{
			if (OpenTapeForm != null)
			{
				OpenTapeForm(this, e);

				ITapeWaveConfiguration control = (ITapeWaveConfiguration)sender;
				control.Volume = volume;
				control.Frequency = frequencyHz;
				control.BeepLength = beepLength;
				control.StartTagOffset = startTagOffset;

				control.OnAccept(); //close configuration view
			}
		}

        /// <summary>
        /// Gets the parts of the message that will be hidden in this carrier.
        /// The excerpt begins at the current position of the [message] stream,
        /// its length is specified by the CountBytesToHide property.
        /// </summary>
        /// <param name="message">The full message.</param>
        /// <returns>An excerpt from the message.</returns>
        public override Stream GetMessagePart(Stream message)
		{
			BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());
			
			byte[] buffer = new byte[countBytesToHide];
			message.Read(buffer, 0, countBytesToHide);
			messageWriter.Write(buffer);

			messageWriter.Seek(0, SeekOrigin.Begin);
			return messageWriter.BaseStream;
		}

		/// <summary>Creates sine sound of a specific frequency</summary>
		/// <returns></returns>
		private WaveSound CreateBeep()
		{			
			// samples for 1/beepLength seconds
			short[] samples = new short[waveSound.Format.SamplesPerSec / beepLength];
			
			WaveFormat beepFormat = new WaveFormat(waveSound.Format.SamplesPerSec, waveSound.Format.BitsPerSample, 1);
			WaveSound beep = new WaveSound(beepFormat, samples);

			//add random frequencies
			Random random = new Random();
			int count = random.Next(50);
			int minAmplitude = volume/2;
			int maxAmplitude = volume;
			for (int n = 0; n < count; n++)
			{
				AddWave(beep,
					(float)random.NextDouble() * 3900 + 100,
					(float)random.NextDouble() * beepLengthInSeconds,
					(float)random.NextDouble() * beepLengthInSeconds,
					(int)random.Next(minAmplitude, maxAmplitude));
			}

			//add beep frequency
			AddWave(beep, frequencyHz, 0, beepLengthInSeconds, volume);
			//ensure volume
			ChangeAmplitude(beep, volume);

			return beep;
		}

		/// <summary>Adds a sine sound of a specific frequency.</summary>
		/// <param name="waveSound">The sound that's being edited</param>
		/// <param name="frequencyHz">Frequency of the new wave in Hertz.</param>
		/// <param name="offsetSeconds">Starting second of the new wave.</param>
		/// <param name="lengthSeconds">Length of the new wave in seconds.</param>
		/// <param name="amplitude">Maximum amplitude of the new wave.</param>
		public void AddWave(WaveSound waveSound, float frequencyHz, float offsetSeconds, float lengthSeconds, int amplitude)
		{
			//existing wave samples
			short[] samples = waveSound.Samples;

			//interval for 1 Hz
			double xStep = (2 * Math.PI) / waveSound.Format.SamplesPerSec;
			//interval for the requested frequency = 1Hz * frequencyHz
			xStep = xStep * frequencyHz;

			long lastSample;
			double xValue = 0;
			short yValue;
			short channelSample;

			long offsetSamples = (long)(waveSound.Format.Channels * waveSound.Format.SamplesPerSec * offsetSeconds);
			if (offsetSamples < samples.Length) //if the beginning sample exists
			{
				lastSample = (long)(waveSound.Format.Channels * waveSound.Format.SamplesPerSec * (offsetSeconds + lengthSeconds));
				if (lastSample > samples.Length)
				{	//last sample does not exist - shorten the new wave
					lastSample = samples.Length - waveSound.Format.Channels + 1;
				}

				for (long n = offsetSamples; n < lastSample; n += waveSound.Format.Channels)
				{
					xValue += xStep;
					yValue = (short)(Math.Sin(xValue) * amplitude);

					for (int channelIndex = 0; channelIndex < waveSound.Format.Channels; channelIndex++)
					{
						channelSample = samples[n + channelIndex];
						channelSample = (short)((channelSample + yValue) / 2);
						samples[n + channelIndex] = channelSample;
					}
				}
			}
		}

		/// <summary>Changes the volume of a sound.</summary>
		/// <param name="waveSound">The sound that's being edited.</param>
		/// <param name="newMaximum">New maximum amplitude. May not be greater than 127, otherwise the result will be useless.</param>
		public void ChangeAmplitude(WaveSound waveSound, short newMaximum)
		{
			int sampleValue;
			int maxSampleValue = 0;
			foreach (short sample in waveSound.Samples)
			{
				sampleValue = Math.Abs((int)sample);
				if (sampleValue > maxSampleValue)
				{
					maxSampleValue = sampleValue;
				}
			}

			float scale = (float)newMaximum / maxSampleValue;

			for (int waveIndex = 0; waveIndex < waveSound.Count; waveIndex++)
			{
				waveSound[waveIndex] = (short)(waveSound[waveIndex] * scale);
			}
		}

		/// <summary>Reads a wave file.</summary>
		/// <param name="fileName">Name and path of the file.</param>
		/// <returns>Format and samples.</returns>
		private WaveSound ReadFile(string fileName)
		{
			BinaryReader reader = new BinaryReader(new FileStream(fileName, FileMode.Open));

			WaveFormat format = ReadHeader(reader);
			int dataLength = reader.ReadInt32();
			
			int maxSampleValue = 0;
			int bytesPerSample = format.BitsPerSample / 8;
			int countSamples = dataLength / bytesPerSample;

			sbyte[] channelSamples8 = new sbyte[countSamples];
			short[] channelSamples16 = new short[countSamples];

			int channelSamplesIndex = 0;
			int absValue;
			for (int sampleIndex = 0; sampleIndex < countSamples; sampleIndex++) {

				if (format.BitsPerSample == 8) {
					channelSamples8[channelSamplesIndex] = reader.ReadSByte();
					if (Math.Abs((int)channelSamples8[channelSamplesIndex]) > maxSampleValue) {
						maxSampleValue = Math.Abs((int)channelSamples8[channelSamplesIndex]);
					}
				} else {
					channelSamples16[channelSamplesIndex] = reader.ReadInt16();
					absValue = Math.Abs((int)channelSamples16[channelSamplesIndex]);
					if (absValue > maxSampleValue) {
						maxSampleValue = (absValue > short.MaxValue) ? absValue - 1 : absValue;
					}
				}

				channelSamplesIndex++;
			}

			reader.Close();

			if (format.BitsPerSample == 8) {
				for (int n = 0; n < channelSamples8.Length; n++) {
					channelSamples16[n] = (short)channelSamples8[n];
				}
			}

			return new WaveSound(format, channelSamples16);
		}

		/// <summary>Read a chunk of four bytes from a wave file</summary>
		/// <param name="reader">Reader for the wave file.</param>
		/// <returns>Four characters.</returns>
		private string ReadChunk(BinaryReader reader)
		{
			byte[] ch = new byte[4];
			reader.Read(ch, 0, ch.Length);
			return System.Text.Encoding.ASCII.GetString(ch);
		}

		/// <summary>
		/// Read the header from a wave file, and move the
		/// reader's position to the beginning of the data chunk
		/// </summary>
		/// <param name="reader">Reader for the wave file.</param>
		/// <returns>Format of the wave.</returns>
		private WaveFormat ReadHeader(BinaryReader reader)
		{
			if (ReadChunk(reader) != "RIFF")
				throw new Exception("Invalid file format");

			reader.ReadInt32(); // File length minus first 8 bytes of RIFF description, we don't use it

			if (ReadChunk(reader) != "WAVE")
				throw new Exception("Invalid file format");

			if (ReadChunk(reader) != "fmt ")
				throw new Exception("Invalid file format");

			int len = reader.ReadInt32();
			if (len < 16) // bad format chunk length
				throw new Exception("Invalid file format");

			WaveFormat carrierFormat = new WaveFormat();
			carrierFormat.FormatTag = reader.ReadInt16();
			carrierFormat.Channels = reader.ReadInt16();
			carrierFormat.SamplesPerSec = reader.ReadInt32();
			carrierFormat.AvgBytesPerSec = reader.ReadInt32();
			carrierFormat.BlockAlign = reader.ReadInt16();
			carrierFormat.BitsPerSample = reader.ReadInt16();

			// advance in the stream to skip the wave format block 
			len -= 16; // minimum format size
			while (len > 0)
			{
				reader.ReadByte();
				len--;
			}

			// assume the data chunk is aligned
			string chunk;
			do
			{
				chunk = ReadChunk(reader);
			} while (reader.BaseStream.Position < reader.BaseStream.Length && chunk != "data");

			return carrierFormat;
		}

		/// <summary>Finds anything but silence in the sounds</summary>
		/// <remarks>Raises the BeepFound event everytime a sound is detected between two blocks of silence.</remarks>
		/// <param name="tolerance">
		/// Sample values greater than [tolerance] are sound,
		/// sample values less than [tolerance] are silence.
		/// </param>
        public void FindAnything(short tolerance) {
            //size of scan window in samples
			int scanWindowSize = waveSound.Format.SamplesPerSec / beepLength;
            //size of scan window in seconds
            float scanWindowSizeSeconds = (float)scanWindowSize / (float)waveSound.Format.SamplesPerSec;

            for (int channelIndex = 0; channelIndex < waveSound.Format.Channels; channelIndex++)
			{	//scan each channel for high amplitudes

				int startIndex = -1;
				int endIndex = -1;
				int countSilentSamples = 0;
				short sample;
				
				for (int n = channelIndex; n < waveSound.Count; n += waveSound.Format.Channels)
				{
					if (WaveSound[n] == short.MinValue)
					{	//avoid OverflowException
						sample = short.MaxValue;
					}
					else
					{
						sample = Math.Abs(WaveSound[n]);
					}

					if (sample > tolerance)
					{ //found a sound
						countSilentSamples = 0;
						if (startIndex < 0)
						{
							startIndex = n;
						}
					}
					else if (startIndex > -1)
					{ //searched and found silence
						countSilentSamples++;
						if (countSilentSamples == scanWindowSize)
						{
							endIndex = n - scanWindowSize;
							NotifyOnBeep(startIndex, endIndex, scanWindowSizeSeconds, channelIndex);

							//scan next time window
							countSilentSamples = 0;
							startIndex = -1;
						}
					}
				}

				if (startIndex > -1)
				{ //wave ends with a beep
					NotifyOnBeep(startIndex, waveSound.Count - 1, scanWindowSizeSeconds, channelIndex);
				}
			}
        }

		/// <summary>Raises the BeepFound events.</summary>
		/// <param name="startIndex">Index of the sound's first sample.</param>
		/// <param name="endIndex">Index of the sound's last sample.</param>
		/// <param name="channel">Index of the channel that contains the beep.</param>
		/// <param name="silentSeconds"></param>
		private void NotifyOnBeep(int startIndex, int endIndex, float silentSeconds, int channel) {
			if (BeepFound != null) {
				//get the second in the wave at which the sound stops
				float second = (float)endIndex / (float)(waveSound.Format.SamplesPerSec * waveSound.Format.Channels);

				//notify
				BeepFound(this, new BeepFoundEventArgs(
					new Beep(startIndex, endIndex,
					second - silentSeconds, second),
					channel));
			}
		}

		/// <summary>Replaces a part of the sound with a beep.</summary>
		/// <param name="insertAtSecond">Where to put the beep.</param>
		/// <param name="channelIndex">Index of the channel.</param>
		public void InsertBeep(float insertAtSecond, int channelIndex)
		{
			short[] beepWave = CreateBeep().Samples;
			short newSampleValue;
			short countChannels = waveSound.Format.Channels;
			int insertAtSample = (int)(countChannels * waveSound.Format.SamplesPerSec * insertAtSecond);
			int longWaveIndex = insertAtSample;

			for (int index = 0; index < beepWave.Length; index++)
			{
				newSampleValue = (short)(waveSound[longWaveIndex]*0.3 + beepWave[index]*0.7);
				waveSound[longWaveIndex + channelIndex] = newSampleValue;
				longWaveIndex += countChannels;
			}
		}

		/// <summary>Get the minimum duration a sound must have in order to hide [message].</summary>
		/// <param name="message">The message.</param>
		/// <returns>Required seconds.</returns>
		public long CountRequiredSeconds(Stream message)
		{
			message.Position = 0;

			long countSeconds = 0;
			int messageByte;
			byte highHalfByte;
			byte lowHalfByte;

			while ((messageByte = message.ReadByte()) > -1) {
				highHalfByte = (byte)(messageByte >> 4);
				lowHalfByte = (byte)(messageByte - (highHalfByte << 4));

				//intervals of 0 seconds are not possible -> add 1 to all intervals
				countSeconds += highHalfByte + 1;
				countSeconds += lowHalfByte + 1;
			}

			return countSeconds;
		}

		/// <summary>Splits the bytes of a message into four-bit-blocks.</summary>
		/// <param name="message">Stream containing the message.</param>
		/// <returns>Stream containing the same message with two bytes per original byte.</returns>
		private Stream PrepareMessage(Stream message)
		{
			message.Position = 0;
			
			MemoryStream preparedMessage = new MemoryStream();
			int messageByte;
			int highHalfByte;
			int lowHalfByte;

			if (startTagOffset > -1)
			{
				preparedMessage.WriteByte((byte)startTagOffset);
			}
			
			while ((messageByte = message.ReadByte()) > -1) {
				//split into high and low part
				highHalfByte = (messageByte >> 4);
				lowHalfByte = (messageByte - (highHalfByte << 4));

				//intervals of 0 seconds are not possible -> add 1 to all intervals
				preparedMessage.WriteByte((byte)(highHalfByte + 1));
				preparedMessage.WriteByte((byte)(lowHalfByte + 1));
			}

			preparedMessage.Position = 0;
			return preparedMessage;
		}

		/// <summary>Hides a message in the wave.</summary>
		/// <param name="message">Stream containing the message.</param>
		/// <param name="key">Not used, can be null.</param>
		public override void Hide(Stream message, Stream key)
		{
			Stream preparedMessage = PrepareMessage(message);
			int messageByte;
			int offset = 0;
			int channelIndex;
			
			Random channelSelect = new Random();
			byte[] channelSelectRandomNumbers = new byte[preparedMessage.Length];
			int randomIndex = 0;
			channelSelect.NextBytes(channelSelectRandomNumbers);

			while ((messageByte = preparedMessage.ReadByte()) > -1) {
				if (channelSelectRandomNumbers[randomIndex] % 2 == 0)
				{	//use first channel
					channelIndex = 0;
				}
				else
				{	//use second channel
					channelIndex = 1;
				}
				randomIndex++;

				//add a noise to channel [channelIndex] after [messageByte] seconds
				offset += messageByte;
				InsertBeep(offset, channelIndex);
			}

			/*if (outputFileName.Length > 0)
			{
				waveSound.SaveToFile(outputFileName);
			}*/
			this.outputStream = waveSound.CreateStream();
		}

		/// <summary>Reads a message from a series of seconds.</summary>
		/// <param name="key">Not used, can be null.</param>
		/// <returns>Stream containing the decoded message.</returns>
		public override Stream Extract(Stream key)
		{
			MemoryStream message = new MemoryStream();
			if (seconds.Count > 0)
			{
				byte interval;
				int second;
				int previousSecond = 0;
				byte messageByte = 0;
				bool isLowPart = false;

				if (startTagOffset > -1)
				{
					previousSecond = (int)Math.Round(seconds[0]);
					seconds.RemoveAt(0);
				}

				foreach (float floatSecond in seconds)
				{
					second = (int)Math.Round(floatSecond);
					try
					{
						interval = (byte)(second - previousSecond);
						if (interval > 16) { interval = 16; }
					}
					catch (OverflowException ex)
					{
						Console.WriteLine(ex);
						interval = 16; //highest possible value for a half byte + 1
					}

					if (isLowPart)
					{
						messageByte += (byte)(interval - 1);
						message.WriteByte(messageByte);
					}
					else
					{
						messageByte = (byte)((interval - 1) << 4);
					}

					isLowPart = !isLowPart;
					previousSecond = second;
				}
			}

			message.Position = 0;
			return message;
		}

		/// <summary>Lets "Sound Exchange" convert the sound.</summary>
		/// <param name="soxArguments">Arguments for sox.exe</param>
		private void RunSox(string soxArguments)
		{
			ProcessStartInfo startInfo = new ProcessStartInfo(soxPath, soxArguments);
			startInfo.RedirectStandardError = true;
			startInfo.UseShellExecute = false;
			Process sox = Process.Start(startInfo);

			StreamReader errorReader = sox.StandardError;
			String errors = errorReader.ReadLine();
			if (errors != null) {
				throw new ApplicationException("sox failed: " + errors);
			}

			sox.WaitForExit(10000);
		}

		/// <summary>Lets "Sound Exchange" perform a band pass filter on the sound.</summary>
		/// <param name="tempDirectory">Path of the directory for temporary files</param>
		/// <param name="frequency">Frequency that may pass the filter.</param>
		/// <returns>Path of the output file</returns>
		public String FindFrequency(string tempDirectory, int frequency)
		{
			String inFileName = Path.Combine(tempDirectory, "in.wav");
			String outFileName = Path.Combine(tempDirectory, "out.wav");
			this.WaveSound.SaveToFile(inFileName);

			String soxArguments = String.Format(
				"-t wav \"{0}\" -t .wav -s -w \"{1}\" band {2} 10",
				inFileName,
				outFileName,
				frequency);

			RunSox(soxArguments);

			return outFileName;
		}

	}
}
