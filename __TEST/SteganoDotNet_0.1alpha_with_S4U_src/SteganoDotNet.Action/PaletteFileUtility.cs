/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Diagnostics;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.ObjectModel;
using System.Text;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.Action
{

    /// <summary>Hides and extract streams in and from images with 8 bit colour depth.</summary>
    public class PaletteFileUtility : FileUtility
	{
		private bool doStretchPalette;
		private bool doSortPalette;

		private int countStretchUnits;
		private int countSortUnits;
		private int countSortAfterStretchUnits;
		private Bitmap bitmap;

        /// <summary>Returns the FileType instance for supported files.</summary>
        public override FileType FileType
		{
			get
			{
				return PaletteFileType.Current;
			}
		}

		/// <summary>Whether or not to stretch the palette</summary>
		public bool DoStretchPalette
		{
			get { return doStretchPalette; }
			set { doStretchPalette = value; }
		}

		/// <summary>Whether or not to sort the palette</summary>
		public bool DoSortPalette
		{
			get { return doSortPalette; }
			set { doSortPalette = value; }
		}

		/// <summary>Returns the maximum number of bytes that can be hidden in the carrier file.</summary>
		public override int Capacity
		{
			get
			{
				int countAllowedUnits = 0;
				if (doSortPalette && doStretchPalette)
				{
					countAllowedUnits = countUnits;
				}
				else if (doSortPalette)
				{
					countAllowedUnits = countSortUnits;
				}
				else if (doStretchPalette)
				{
					countAllowedUnits = countStretchUnits;
				}

				int capacity = (int)Math.Floor(((float)countAllowedUnits / 8));
				if (capacity > lengthCountUnits)
				{
					return capacity - lengthCountUnits;
				}
				else
				{
					return 0;
				}
			}
		}

        /// <summary>Constructor.</summary>
        /// <param name="fileName">Name and path of an image file.</param>
        public PaletteFileUtility(string fileName)
		{
			sourceFileName = fileName;
			bitmap = (Bitmap)Image.FromFile(fileName);
			Initialize();
		}

        /// <summary>Constructor.</summary>
        /// <param name="rawData">A Bitmap object.</param>
        public PaletteFileUtility(object rawData)
		{
            sourceFileName = NoSourceFileName;
			bitmap = (Bitmap)rawData;
			Initialize();
		}

		private void Initialize()
		{
			countSortUnits = this.bitmap.Palette.Entries.Length - 1;
			
			if (bitmap.Palette.Entries.Length < 129)
			{
				countSortAfterStretchUnits = this.bitmap.Palette.Entries.Length * 2 - 1;
				countStretchUnits = bitmap.Width * bitmap.Height;
			}
			else
			{
				countStretchUnits = 0;
				countSortAfterStretchUnits = 0;
			}
			
			if (countStretchUnits > 3)
			{
				countUnits = countStretchUnits + countSortAfterStretchUnits;
			}
			else if (countSortUnits > 3)
			{
				countUnits = countSortUnits;
			}
			else
			{
				countUnits = 0;
			}

			countUsedBitsPerUnit = 1;
			
			CreateCarrierConfigurationControl("PaletteConfiguration");
		}

        /// <summary>PaletteConfiguration uses this event handler method to write back new settings.</summary>
        protected override void CarrierConfiguration_AcceptSettings(ICarrierConfiguration sender, EventArgs e)
		{
			IPaletteConfiguration control = (IPaletteConfiguration)sender;

			percentOfMessage = control.PercentOfMessage;
			doSortPalette = control.SortPalette;
			doStretchPalette = control.StretchPalette;
			base.CarrierConfiguration_AcceptSettings(sender, e);
		}

        /// <summary>Hides the given message stream in the image.</summary>
        /// <param name="message">Message.</param>
        /// <param name="key">Not used, can be null.</param>
		public override void Hide(Stream message, Stream key)
		{
			bool mustStretchPalette = false;
			int countByteToHideBySort = 0;
			int messageLength = (int)message.Length;

			if (doSortPalette && doStretchPalette)
			{
				int capacitySimpleSort = (int)Math.Floor(((float)this.bitmap.Palette.Entries.Length - 1) / 8);
				if (capacitySimpleSort >= messageLength)
				{	//the message fits into the palette
					mustStretchPalette = false;
					countByteToHideBySort = messageLength;
				}
				else
				{	//split message and stretch palette
					mustStretchPalette = true;
					countByteToHideBySort = (int)Math.Floor((float)this.countSortAfterStretchUnits / 8);
					if (countByteToHideBySort >= messageLength)
					{	//the message fits into the resized palette
						//hide only the length in pixels and the rest in palette order
						countByteToHideBySort = messageLength - 4;
					}
				}
			}
			else if (doSortPalette)
			{
				mustStretchPalette = false;
				countByteToHideBySort = messageLength;
			}
			else if (doStretchPalette)
			{
				mustStretchPalette = true;
				countByteToHideBySort = 0;
			}

			int countByteToHideByStretch = messageLength - countByteToHideBySort;

			if (mustStretchPalette)
			{
				ArrayList newPalette = null; //receives the stretched palette
				Hashtable colorIndexToNewIndices = null; //receives the list of new color indices

				//create a new palette from the existing one
				StretchPalette(ref newPalette, ref colorIndexToNewIndices);

				//create a bitmap with the new palette and the hidden message
				this.bitmap = CreateBitmap(countByteToHideByStretch, newPalette, colorIndexToNewIndices, message, key);
			}
			
			if (doSortPalette)
			{
				SortPalette(message, key);
			}

			//add a marker to the second least significant bits of the last colour
			SetMethodMarker(doSortPalette, mustStretchPalette);

			CreateOutputStream(this.bitmap);
		}

		private void SetMethodMarker(bool isSorted, bool isStretched)
		{	
			ColorPalette palette = this.bitmap.Palette;
			int position = palette.Entries.Length - 1;
			Color color = palette.Entries[position];

			byte markerValueStretch = (byte)(isStretched ? 1 : 0);
			byte red = SetBit(0, markerValueStretch, 1, color.R);
			byte green = SetBit(0, markerValueStretch, 1, color.G);
			byte blue = SetBit(0, markerValueStretch, 1, color.B);

			byte markerValueSort = (byte)(isSorted ? 1 : 0);
			red = SetBit(0, markerValueSort, 2, red);
			green = SetBit(0, markerValueSort, 2, green);
			blue = SetBit(0, markerValueSort, 2, blue);

			palette.Entries[position] = Color.FromArgb(red, green, blue);
			this.bitmap.Palette = palette;
		}

		private void SortPalette(Stream message, Stream key)
		{
			//list the palette entries an integer values
			int[] colors = new int[bitmap.Palette.Entries.Length];
			for (int n = 0; n < colors.Length; n++)
			{
				colors[n] = bitmap.Palette.Entries[n].ToArgb();
			}

			//initialize empty list for the resulting palette
			Collection<int> resultList = new Collection<int>();

			//initialize and fill list for the sorted palette
			//use ArrayList because Collection<int> cannot be sorted (no method)
			//or changed (exception: list is read only)
			ArrayList originalList = new ArrayList(colors);
			originalList.Sort();

			//initialize list for the mapping of old indices to new indices
			SortedList oldIndexToNewIndex = new SortedList(colors.Length);

			Random random = new Random();
			bool messageBit = false;
			int messageByte = message.ReadByte();
			int listElementIndex = 0;

			//for each byte of the message
			while (messageByte > -1)
			{
				//for each bit
				for (int bitIndex = 0; bitIndex < 8; bitIndex++)
				{

					//decide which color is going to be the next one in the new palette

					messageBit = ((messageByte & (1 << bitIndex)) > 0) ? true : false;

					if (messageBit || originalList.Count == 1)
					{	//secret value is '1' or it is the last color
						listElementIndex = 0;
					}
					else
					{
						listElementIndex = random.Next(1, originalList.Count - 1);
					}

					//log change of index for this color

					int originalPaletteIndex = 0;
					originalPaletteIndex = Array.IndexOf(colors, originalList[listElementIndex]);

					if (!oldIndexToNewIndex.ContainsKey(originalPaletteIndex))
					{
						//add mapping, ignore if the original palette contains more than one entry for this color
						oldIndexToNewIndex.Add(originalPaletteIndex, resultList.Count);
					}

					//move the color from old palette to new palette
					resultList.Add((int)originalList[listElementIndex]);
					originalList.RemoveAt(listElementIndex);
				}

				//repeat this with the next byte of the message
				messageByte = message.ReadByte();
			}

			//copy unused palette entries
			foreach (int originalColor in originalList)
			{
				int originalPaletteIndex = Array.IndexOf(colors, originalColor);
				if (!oldIndexToNewIndex.ContainsKey(originalPaletteIndex))
				{
					oldIndexToNewIndex.Add(originalPaletteIndex, resultList.Count);
				}
				resultList.Add(originalColor);
			}

			//create new image
			this.bitmap = CreateBitmap(bitmap, resultList, oldIndexToNewIndex);
		}

		private void HideBitInSecondLsb(byte secretByte, byte secretBitIndex, Collection<int> resultList, Random random)
		{
			int position = random.Next(0, resultList.Count);
			Color color = Color.FromArgb((int)resultList[position]);
			int red = SetBit(secretBitIndex, secretByte, 1, color.R);
			int green = SetBit(secretBitIndex, secretByte, 1, color.G);
			int blue = SetBit(secretBitIndex, secretByte, 1, color.B);
			resultList[position] = Color.FromArgb(red, green, blue).ToArgb();
		}

		private bool ExtractSecondLsb(Random random)
		{
			int position = random.Next(0, this.bitmap.Palette.Entries.Length);
			Color color = this.bitmap.Palette.Entries[position];
			bool bit = GetBit(0, 0, 1, color.R) > 0;
			return bit;		
		}

		/// <summary>Creates a larger palette by duplicating and changing the colors of another palette</summary>
		/// <param name="newPalette">Receives the new palette entries</param>
		/// <param name="colorIndexToNewIndices">
		/// Receives a Hashtable with the original indices as the keys,
		/// and the corresponding new indices as the values
		/// </param>
		private void StretchPalette(ref ArrayList newPalette, ref Hashtable colorIndexToNewIndices)
		{
			ColorPalette oldPalette = this.bitmap.Palette;
			newPalette = new ArrayList(oldPalette.Entries.Length * 2);
			colorIndexToNewIndices = new Hashtable(oldPalette.Entries.Length);
			
			Random random = new Random();
			byte indexInNewPalette;
			Color color, newColor;
			ColorIndexList colorIndexList;

			for (byte n = 0; n < oldPalette.Entries.Length; n++)
			{ //loop over old palette entries
				color = oldPalette.Entries[n]; //original color

				if (colorIndexToNewIndices.ContainsKey(n))
				{
					//this color from the original palette already has one or more copies in the new palette
					colorIndexList = (ColorIndexList)colorIndexToNewIndices[n];
				}
				else
				{
					//make even
					color = Color.FromArgb(
							(color.R % 2 > 0) ? color.R - 1 : color.R,
							(color.G % 2 > 0) ? color.G - 1 : color.G,
							(color.B % 2 > 0) ? color.B - 1 : color.B);
					indexInNewPalette = (byte)newPalette.Add(color); //add color
					colorIndexList = new ColorIndexList(random);
					colorIndexList.Add(indexInNewPalette);
					colorIndexToNewIndices.Add(n, colorIndexList);
				}

				//create a non-exact copy of the color
				newColor = GetSimilarColor(random, newPalette, color);

				//make odd
				newColor = Color.FromArgb(
						(newColor.R % 2 == 0) ? newColor.R + 1 : newColor.R,
						(newColor.G % 2 == 0) ? newColor.G + 1 : newColor.G,
						(newColor.B % 2 == 0) ? newColor.B + 1 : newColor.B);
				
				//add the changed color to the new palette
				indexInNewPalette = (byte)newPalette.Add(newColor);
				//add the new index to the list of alternative indices
				colorIndexList.Add(indexInNewPalette);

				//update the Hashtable
				colorIndexToNewIndices[n] = colorIndexList;
			}
		}

		///// <summary>Calculates a color that looks nearly the same</summary>
		///// <param name="random"></param>
		///// <param name="excludeColors">List of colors that may not be returned again</param>
		///// <param name="color">Original color</param>
		///// <returns>A new color that differs a little from [color]</returns>
		//private Color GetSimilarColor(Random random, ArrayList excludeColors, Color color)
		//{
		//    Color newColor = color;
		//    int countLoops = 0, red, green, blue;
		//    do
		//    {
		//        red = GetSimilarColorComponent(random, newColor.R);
		//        green = GetSimilarColorComponent(random, newColor.G);
		//        blue = GetSimilarColorComponent(random, newColor.B);

		//        //make odd
		//        newColor = Color.FromArgb(
		//                (red % 2 == 0) ? red + 1 : red,
		//                (green % 2 == 0) ? green + 1 : green,
		//                (blue % 2 == 0) ? blue + 1 : blue);

		//        //newColor = Color.FromArgb(red, green, blue);

		//        countLoops++;
		//    } while (excludeColors.Contains(newColor));// && countLoops<50); //make sure that there are no duplicate colors

		//    return newColor;
		//}

		///// <summary>Calculates a color component that looks nearly the same</summary>
		///// <param name="random"></param>
		///// <param name="colorValue">Original color component</param>
		///// <returns>[colorValue]*1.0x if [colorValue] is less than 128, or [colorValue]/1.0x for higher values</returns>
		//private byte GetSimilarColorComponent(Random random, byte colorValue)
		//{
		//    if (colorValue < 128)
		//    {
		//        //colorValue = (byte)Math.Round((colorValue * (1 + random.Next(1, 8) / (float)100)));
		//        colorValue += (byte)(random.Next(2, 32));
		//    }
		//    else
		//    {
		//        //colorValue = (byte)Math.Round((colorValue / (1 + random.Next(1, 8) / (float)100)));
		//        colorValue -= (byte)(random.Next(2, 32));
		//    }
		//    return colorValue;
		//}

		/// <summary>Calculates a color that looks nearly the same</summary>
		/// <param name="random"></param>
		/// <param name="excludeColors">List of colors that may not be returned again</param>
	 	/// <param name="color">Original color</param>
	 	/// <returns>A new color that differs a little from [color]</returns>
		private Color GetSimilarColor(Random random, ArrayList excludeColors, Color color)
		{
			Color newColor = color;
			int countLoops = 0, red, green, blue;
			do
			{
				red = GetSimilarColorComponent(random, newColor.R);
				green = GetSimilarColorComponent(random, newColor.G);
				blue = GetSimilarColorComponent(random, newColor.B);
				newColor = Color.FromArgb(red, green, blue);
				countLoops++;
			} while (excludeColors.Contains(newColor) && (countLoops < 10)); //make sure that there are no duplicate colors

			return newColor;
		}

		/// <summary>Calculates a color component that looks nearly the same</summary>
		/// <param name="random"></param>
		/// <param name="colorValue">Original color component</param>
 		/// <returns>[colorValue]*1.0x if [colorValue] is less than 128, or [colorValue]/1.0x for higher values</returns>
		private byte GetSimilarColorComponent(Random random, byte colorValue)
		{
			if (colorValue < 128)
			{
				colorValue = (byte)(colorValue * (1 + random.Next(1, 8) / (float)100));
			}
			else
			{
				colorValue = (byte)(colorValue / (1 + random.Next(1, 8) / (float)100));
			}
			return colorValue;
		}

		/// <summary>
		/// Creates an image with a stretched palette, converts the pixels of the
		/// original image for that new palette, and hides a message in the converted pixels
		/// </summary>
		/// <param name="bmp">The original image</param>
		/// <param name="palette">The new palette</param>
		/// <param name="colorIndexToNewIndices">
		/// Hashtable which maps every index in the original palette
		/// to a list of indices in the new palette.
		/// </param>
		/// <param name="messageStream">The secret message</param>
		/// <param name="keyStream">A key that specifies the distances between two pixels used to hide a bit</param>
		/// <returns>The new bitmap</returns>
		private Bitmap CreateBitmap(int messagePartLength, ArrayList palette, Hashtable colorIndexToNewIndices, Stream messageStream, Stream keyStream)
		{
			BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

			//size of the image data in bytes
			int imageSize = (bmpData.Height * bmpData.Stride); // +(palette.Count * 4);

			//copy all pixels
			byte[] pixels = new byte[imageSize];
			Marshal.Copy(bmpData.Scan0, pixels, 0, (bmpData.Height * bmpData.Stride));

			int messageBitIndex = 0;
			bool messageBit;
			ColorIndexList newColorIndices;
			Random random = null;// = new Random(GetKeyValue(keyStream));

			//index of the next pixel that's going to hide one bit

			int nextUseablePixelIndex = 0;

			int messageByte = messageStream.ReadByte();

			//loop over the pixels
			for (int pixelIndex = 0; pixelIndex < pixels.Length; pixelIndex++)
			{
				//get the list of new color indices for the current pixel
				newColorIndices = (ColorIndexList)colorIndexToNewIndices[pixels[pixelIndex]];

				if ((pixelIndex < nextUseablePixelIndex) || messageStream.Position > messagePartLength)
				{
					//message complete or this pixel has to be skipped - use a random color
					pixels[pixelIndex] = newColorIndices.GetIndex();
				}
				else
				{
					//get a bit out of the current byte
					messageBit = (messageByte & (1 << messageBitIndex)) > 0;
					//get the index of a similar color in the new palette
					pixels[pixelIndex] = newColorIndices.GetIndex(messageBit);

					if (messageBitIndex == 7)
					{	//one byte has been hidden
						if (messageStream.Position == messagePartLength)
						{	//finished
							continue;
						}
						//proceed to the next byte
						messageBitIndex = 0;
						messageByte = messageStream.ReadByte();
						//start new key sequence
						random = new Random(GetKeyValue(keyStream));
					}
					else
					{
						messageBitIndex++; //next bit
					}

					//locate the next pixel
					if (messageStream.Position > 4)
					{
						float countRemainingPixels = pixels.Length - nextUseablePixelIndex;
						float lengthRemainingStream = messagePartLength - messageStream.Position + 1;
						int lengthBlock = (int)Math.Floor(countRemainingPixels / (8 * lengthRemainingStream));
						nextUseablePixelIndex += random.Next(1, (lengthBlock > 0) ? lengthBlock : 1);
					}
					else
					{	//length of the message has not yet been hidden
						//the recipient cannot know messagePartLength
						nextUseablePixelIndex++;
					}
				}
			}

			//Now we have the palette and the new pixels - enough data to write the bitmap!

			BinaryWriter bw = new BinaryWriter(new MemoryStream());

			//write bitmap file header
			bw.Write(System.Text.ASCIIEncoding.ASCII.GetBytes("BM")); //BITMAPFILEHEADER.bfType;
			bw.Write((Int32)(55 + imageSize)); //BITMAPFILEHEADER.bfSize;
			bw.Write((Int16)0); //BITMAPFILEHEADER.bfReserved1;
			bw.Write((Int16)0); //BITMAPFILEHEADER.bfReserved2;
			bw.Write(
				(Int32)(
				Marshal.SizeOf(typeof(BITMAPINFOHEADER))
				+ Marshal.SizeOf(typeof(BITMAPFILEHEADER))
				+ palette.Count * 4)
				); //BITMAPFILEHEADER.bfOffBits;

			//write bitmap info header
			bw.Write((Int32)Marshal.SizeOf(typeof(BITMAPINFOHEADER)));
			bw.Write((Int32)bitmap.Width); //BITMAPINFOHEADER.biWidth
			bw.Write((Int32)bitmap.Height); //BITMAPINFOHEADER.biHeight
			bw.Write((Int16)1); //BITMAPINFOHEADER.biPlanes
			bw.Write((Int16)8); //BITMAPINFOHEADER.biBitCount
			bw.Write((UInt32)0); //BITMAPINFOHEADER.biCompression
			bw.Write((Int32)(bmpData.Height * bmpData.Stride) + (palette.Count * 4)); //BITMAPINFOHEADER.biSizeImage
			bw.Write((Int32)0); //BITMAPINFOHEADER.biXPelsPerMeter
			bw.Write((Int32)0); //BITMAPINFOHEADER.biYPelsPerMeter
			bw.Write((UInt32)palette.Count); //BITMAPINFOHEADER.biClrUsed
			bw.Write((UInt32)palette.Count); //BITMAPINFOHEADER.biClrImportant

			//write palette
			foreach (Color color in palette)
			{
				bw.Write((UInt32)color.ToArgb());
			}
			//write pixels
			bw.Write(pixels);

			bitmap.UnlockBits(bmpData);

			Bitmap newImage = (Bitmap)Image.FromStream(bw.BaseStream);
			newImage.RotateFlip(RotateFlipType.RotateNoneFlipY);

			bw.Close();
			return newImage;
		}

		/// <summary>Save an image to a file</summary>
		/// <param name="bitmap">The image to save</param>
		private void SaveBitmap(Bitmap bitmap, string fileName)
		{
			String fileNameLower = fileName.ToLower();

			System.Drawing.Imaging.ImageFormat format = System.Drawing.Imaging.ImageFormat.Bmp;
			if (fileNameLower.EndsWith("gif"))
			{
				format = System.Drawing.Imaging.ImageFormat.Gif;
			}
			else if (fileNameLower.EndsWith("png"))
			{
				format = System.Drawing.Imaging.ImageFormat.Png;
			}

			bitmap.Save(fileName, format);
			bitmap.Dispose();
		}

		/// <summary>Save an image to a file</summary>
		/// <param name="bitmap">The image to save</param>
		private void CreateOutputStream(Bitmap bitmap)
		{
			System.Drawing.Imaging.ImageFormat format = System.Drawing.Imaging.ImageFormat.Bmp;
			/*if (fileNameLower.EndsWith("gif"))
			{
				format = System.Drawing.Imaging.ImageFormat.Gif;
			}
			else if (fileNameLower.EndsWith("png"))
			{
				format = System.Drawing.Imaging.ImageFormat.Png;
			}*/

			outputStream = new MemoryStream();
			bitmap.Save(outputStream, format);
			bitmap.Dispose();
		}

        /// <summary>Extracts a stream from the HTML document.</summary>
		/// <param name="key">A stream with varying seed values for a random number generator.</param>
		/// <returns>The extracted stream.</returns>
        public override Stream Extract(Stream key)
		{
			////initialize a Random with the first key value
			//long keyPosition = key.Position;
			//key.Position = 0;
			//Random random = new Random(key.ReadByte());
			//key.Position = keyPosition;

			////get the marker from a second least significant bit of one colour
			//bool isPaletteStretched = ExtractSecondLsb(random);

			int position = this.bitmap.Palette.Entries.Length - 1;
			Color color = this.bitmap.Palette.Entries[position];
			bool isPaletteStretched = GetBit(0, 0, 1, color.R) > 0;
			bool isPaletteSorted = GetBit(0, 0, 2, color.R) > 0;
			
			Stream message = new MemoryStream();
			int messageLength = 0;
			if (isPaletteStretched)
			{
				messageLength = ExtractFromStretchedPalette(key, message, isPaletteSorted);
			}
			if (isPaletteSorted)
			{
				ExtractFromSortedPalette(messageLength, key, message);
			}

			return message;
		}

		private void ExtractFromSortedPalette(int messageLength, Stream key, Stream message)
		{
			//list the palette entries an integer values
			int[] colors = new int[bitmap.Palette.Entries.Length];
			for (int n = 0; n < colors.Length; n++)
			{
				colors[n] = bitmap.Palette.Entries[n].ToArgb();
			}

			//initialize and fill list for the carrier palette
			ArrayList carrierList = new ArrayList(colors);

			//sort the list to restore the original palette
			ArrayList originalList = new ArrayList(colors);
			originalList.Sort();
			
			//the last palette entry holds no data - remove it
			carrierList.RemoveAt(carrierList.Count - 1);

			int messageBit = 0;
			int messageBitIndex = 0;
			int messageByte = 0;
			int color;
			int carrierListIndex;

			//for each color that carries a bit of the message
			for (carrierListIndex = 0; carrierListIndex < carrierList.Count; carrierListIndex++)
			{
				//decide which bit the entry's position hides
				color = (int)carrierList[carrierListIndex];
				if (color == (int)originalList[0])
				{
					messageBit = 1;
				}
				else
				{
					messageBit = 0;
				}

				//remove the color from the sorted palette
				originalList.Remove(color);

				//add the bit to the message
				messageByte += (byte)(messageBit << messageBitIndex);

				messageBitIndex++;
				if (messageBitIndex > 7)
				{
					//append the byte to the message
					message.WriteByte((byte)messageByte);
					if (message.Length == messageLength)
					{	//finished
						break;
					}

					if (messageLength == 0 && message.Length == 4)
					{
						//first hidden Int32 was the message's length
						message.Seek(0, SeekOrigin.Begin);
						BinaryReader reader = new BinaryReader(message);
						messageLength = reader.ReadInt32();
						message.SetLength(0);
					}

					messageByte = 0;
					messageBitIndex = 0;
				}
			}
		}

		/// <summary>Extracts a hidden message from a picture</summary>
		/// <param name="key">The key that has been used to hide the message</param>
		/// <param name="message">Empty stream to receive the extracted message</param>
		public int ExtractFromStretchedPalette(Stream key, Stream message, bool isPaletteSorted)
		{
			//load the carrier image
			BitmapData bmpData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

			//copy all pixels
			byte[] pixels = new byte[bmpData.Stride * bmpData.Height];
			Marshal.Copy(bmpData.Scan0, pixels, 0, pixels.Length);

			Random random = null; //new Random(GetKeyValue(key));
			Color[] palette = bitmap.Palette.Entries;
			byte messageByte = 0, messageBitIndex = 0, pixel = 0;
			int messageLength = 0, messagePartLength = 0, pixelIndex = 0;

			//read pixels until the message is complete
			while ((messagePartLength == 0) || (message.Length < messagePartLength))
			{
				pixel = pixels[pixelIndex];

				if ((palette[pixel].B % 2) == 1)
				{
					//odd blue-component: message-bit was "1"
					messageByte += (byte)(1 << messageBitIndex);
				} //else: messageBit was "0", nothing to do

				if (messageBitIndex == 7)
				{	//a byte is complete
					//save and reset messageByte, reset messageBitIndex
					message.WriteByte(messageByte);
					messageBitIndex = 0;
					messageByte = 0;
					//start new key sequence
					random = new Random(GetKeyValue(key));

					if ((messagePartLength == 0) && (message.Length == 4))
					{
						//message's length has been read
						message.Seek(0, SeekOrigin.Begin);
						messageLength = new BinaryReader(message).ReadInt32();
						message.SetLength(0);
						int countStretchUnits = bitmap.Width * bitmap.Height;
						if (isPaletteSorted)
						{
							//calculate the length of the part hidden by stretching the palette
							int countSortUnits = this.bitmap.Palette.Entries.Length - 1;
							int countByteToHideBySort = (int)Math.Floor((float)countSortUnits / 8);
							messagePartLength = messageLength - countByteToHideBySort;
						}
						else
						{
							messagePartLength = messageLength;
						}
					}
				}
				else
				{
					messageBitIndex++; //next bit
				}

				//locate the next pixel that carries a hidden bit
				if (messagePartLength > 0)
				{
					float countRemainingPixels = pixels.Length - pixelIndex;
					float lengthRemainingStream = messagePartLength - message.Position;
					int lengthBlock = (int)Math.Floor(countRemainingPixels / (8 * lengthRemainingStream));
					pixelIndex += random.Next(1, (lengthBlock > 0) ? lengthBlock : 1);
				}
				else
				{
					pixelIndex++;
				}
			}

			//release the carrier bitmap
			bitmap.UnlockBits(bmpData);

			return messageLength;
		}

		/// <summary>
		/// Creates an image with a palette, converts the pixels of the
		/// original image for that new palette, and hides a message in the converted pixels
		/// </summary>
		/// <param name="bmp">The original image</param>
		/// <param name="palette">The new palette</param>
		/// <param name="oldIndexToNewIndex">Relations map: newIndex = oldIndexToNewIndex[oldIndex]</param>
		/// <returns>The new bitmap</returns>
		internal static Bitmap CreateBitmap(Bitmap bmp, Collection<int> palette, SortedList oldIndexToNewIndex)
		{
			int sizeBitmapInfoHeader = 40;
			int sizeBitmapFileHeader = 14;

			BitmapData bmpData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

			//size of the image data in bytes
			int imageSize = (bmpData.Height * bmpData.Stride) + (palette.Count * 4);

			//copy all pixels
			byte[] pixels = new byte[imageSize];
			Marshal.Copy(bmpData.Scan0, pixels, 0, (bmpData.Height * bmpData.Stride));

			//get the new color index for each pixel
			int pixelColorIndex;
			object tmp;
			for (int pixelIndex = 0; pixelIndex < pixels.Length; pixelIndex++)
			{
				pixelColorIndex = pixels[pixelIndex];
				tmp = oldIndexToNewIndex[pixelColorIndex];
				pixels[pixelIndex] = Convert.ToByte(tmp);
			}

			BinaryWriter bw = new BinaryWriter(new MemoryStream());

			//write bitmap file header
			bw.Write(System.Text.ASCIIEncoding.ASCII.GetBytes("BM")); //BITMAPFILEHEADER.bfType;
			bw.Write((Int32)(55 + imageSize)); //BITMAPFILEHEADER.bfSize;
			bw.Write((Int16)0); //BITMAPFILEHEADER.bfReserved1;
			bw.Write((Int16)0); //BITMAPFILEHEADER.bfReserved2;
			bw.Write(
				(Int32)(
				sizeBitmapInfoHeader
				+ sizeBitmapFileHeader
				+ palette.Count * 4)
				); //BITMAPFILEHEADER.bfOffBits;

			//write bitmap info header
			bw.Write((Int32)sizeBitmapInfoHeader);
			bw.Write((Int32)bmp.Width); //BITMAPINFOHEADER.biWidth
			bw.Write((Int32)bmp.Height); //BITMAPINFOHEADER.biHeight
			bw.Write((Int16)1); //BITMAPINFOHEADER.biPlanes
			bw.Write((Int16)8); //BITMAPINFOHEADER.biBitCount
			bw.Write((UInt32)0); //BITMAPINFOHEADER.biCompression
			bw.Write((Int32)(bmpData.Height * bmpData.Stride) + (palette.Count * 4)); //BITMAPINFOHEADER.biSizeImage
			bw.Write((Int32)0); //BITMAPINFOHEADER.biXPelsPerMeter
			bw.Write((Int32)0); //BITMAPINFOHEADER.biYPelsPerMeter
			bw.Write((UInt32)palette.Count); //BITMAPINFOHEADER.biClrUsed
			bw.Write((UInt32)palette.Count); //BITMAPINFOHEADER.biClrImportant

			//write palette
			foreach (int color in palette)
			{
				bw.Write(color);
			}
			//write pixels
			bw.Write(pixels);

			bmp.UnlockBits(bmpData);

			Bitmap newImage = (Bitmap)Image.FromStream(bw.BaseStream);
			newImage.RotateFlip(RotateFlipType.RotateNoneFlipY);

			bw.Close();
			return newImage;
		}

		/// <summary>Save the image to a file</summary>
		public override void SaveResult(string fileName)
		{
			String fileNameLower = fileName.ToLower();

			ImageFormat format = ImageFormat.Bmp;
			if (fileNameLower.EndsWith("gif"))
			{
				format = System.Drawing.Imaging.ImageFormat.Gif;
			}
			else if (fileNameLower.EndsWith("png"))
			{
				format = System.Drawing.Imaging.ImageFormat.Png;
			}

			Image image = Image.FromStream(this.outputStream);
			image.Save(fileName, format);
			image.Dispose();
		}
	}

	#region ColorIndexList
	internal class ColorIndexList : ArrayList
	{
		/// <summary>counts the alternative indices that have already been returned by GetIndex()</summary>
		private int maxIndexAlreadyUsed = 0;
		/// <summary>Used by GetIndex() to pick a random index</summary>
		private Random random;

		public ColorIndexList(Random random)
		{
			this.random = random;
		}

		/// <summary>Returns a color index that can be used to hide [messageBit]</summary>
		/// <param name="messageBit">One bit of the secret message</param>
		/// <returns>The first color index if [messageBit] is false, one of the alternative color indices if [messageBit] is true</returns>
		public byte GetIndex(bool messageBit)
		{
			if (messageBit)
			{
				//return the index of an odd color, this hides a "1"
				if (maxIndexAlreadyUsed < this.Count - 1)
				{
					//one or more color indices have not been used yet
					maxIndexAlreadyUsed++;
					return (byte)this[maxIndexAlreadyUsed];
				}
				else
				{
					//there are no unused indices - return a random index
					return (byte)this[random.Next(1, this.Count - 1)];
				}
			}
			else
			{
				//return the index of the even color, this hides a "0"
				return (byte)this[0];
			}
		}

		/// <summary>Returns a random color index</summary>
		/// <returns>The next unused color index, or a random color index</returns>
		public byte GetIndex()
		{
			if (maxIndexAlreadyUsed < this.Count - 1)
			{
				//one or more color indices have not been used yet
				maxIndexAlreadyUsed++;
				return (byte)this[maxIndexAlreadyUsed];
			}
			else
			{
				//there are no unused indices - return a random index (original color preferred)
				int index = random.Next(0, this.Count) - 1;
				return (index < 0) ? (byte)this[0] : (byte)this[index];
			}
		}
	}
	#endregion ColorIndexList

	#region bitmap structures

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal struct BITMAPINFOHEADER
	{
		public Int32 biSize;
		public Int32 biWidth;
		public Int32 biHeight;
		public Int16 biPlanes;
		public Int16 biBitCount;
		public UInt32 biCompression;
		public Int32 biSizeImage;
		public Int32 biXPelsPerMeter;
		public Int32 biYPelsPerMeter;
		public UInt32 biClrUsed;
		public UInt32 biClrImportant;
	}

	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal struct BITMAPFILEHEADER
	{
		public Int16 bfType;
		public Int32 bfSize;
		public Int16 bfReserved1;
		public Int16 bfReserved2;
		public Int32 bfOffBits;
	}

	#endregion bitmap structures
}
