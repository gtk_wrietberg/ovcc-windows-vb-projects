/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.Action {

	/// <summary>Stores the colours of a pixel</summary>
	public struct PixelData
	{
		/// <summary>Blue component.</summary>
		public byte Blue;
		/// <summary>Green </summary>
		public byte Green;
		/// <summary>Red component.</summary>
		public byte Red;
	}

    /// <summary>Hides and extract streams in and from images with 24 bit colour depth.</summary>
	public class BitmapFileUtility : FileUtility {

		private int noisePercent = 0;
		private int currentColorComponent = 0;
		private Bitmap bitmap;
        private ImageInfo imageInfo;

        /// <summary>Returns the FileType instance for supported files: BitmapFileType.Current.</summary>
		public override FileType FileType
		{
			get
			{
				return BitmapFileType.Current;
			}
		}

        /// <summary>Returns the count of bytes that will be hidden in the image.</summary>
		public override int Capacity
		{
			get
			{
				return countBytesToHide;
			}
		}

        /// <summary>
        /// Returns region information about the image and extracted text.
        /// Only availabe, if Extract() has run successfully.
        /// </summary>
        public ImageInfo ImageInfo
        {
            get
            {
                return imageInfo;
            }
        }

        /// <summary>Constructor.</summary>
        /// <param name="fileName">Name and path of an image file.</param>
		public BitmapFileUtility(string fileName)
		{
			sourceFileName = fileName;
			bitmap = (Bitmap)Image.FromFile(fileName);
			Initialize();
		}

        /// <summary>Constructor.</summary>
        /// <param name="rawData">A Bitmap object.</param>
		public BitmapFileUtility(object rawData)
		{
            sourceFileName = NoSourceFileName;
			bitmap = (Bitmap)rawData;
			Initialize();
		}

		private void Initialize()
		{
			countUnits = bitmap.Width * bitmap.Height;
			CreateCarrierConfigurationControl("BitmapConfiguration", bitmap);
		}

        /// <summary>BitmapConfiguration uses this event handler method to write back new settings.</summary>
		protected override void CarrierConfiguration_AcceptSettings(ICarrierConfiguration sender, EventArgs e)
		{
			IBitmapConfiguration control = (IBitmapConfiguration)sender;
			
			countUsedBitsPerUnit = 0; //defined per region
			percentOfMessage = control.PercentOfMessage; //calculated from count of bytes
			countBytesToHide = control.CountBytesToHide; //configured in regions dialog
			noisePercent = control.NoisePercent;

            imageInfo = new ImageInfo(bitmap, new Collection<RegionInfo>(control.RegionInfo));

			base.CarrierConfiguration_AcceptSettings(sender, e);
		}

		/// <summary>Converts an image to a 24-bit bitmap with default resolution.</summary>
		/// <param name="original">Any image.</param>
		/// <returns>Formatted image.</returns>
		private Bitmap PaletteToRGB(Bitmap original)
		{
			original = CopyBitmap(original, ImageFormat.Bmp);
			Bitmap image = new Bitmap(original.Width, original.Height, PixelFormat.Format24bppRgb);
			Graphics graphics = Graphics.FromImage(image);
			graphics.DrawImage(original, 0, 0, original.Width, original.Height);
			graphics.Dispose();
			original.Dispose();
			return image;
		}

		/// <summary>Makes sure that a bitmap is not a useless "MemoryBitmap".</summary>
		/// <param name="bitmap">Any image.</param>
		/// <param name="format">Image format.</param>
		/// <returns>Definitely not broken bitmap.</returns>
		private Bitmap CopyBitmap(Bitmap bitmap, ImageFormat format)
		{
			MemoryStream buffer = new MemoryStream();
			bitmap.Save(buffer, format);
			Bitmap saveableBitmap = (Bitmap)Image.FromStream(buffer);
			return saveableBitmap;
		}

		/// <summary>Save an image to a file</summary>
		public override void SaveResult(string fileName)
		{
			String fileNameLower = fileName.ToLower();

			ImageFormat format = ImageFormat.Bmp;
			if ((fileNameLower.EndsWith("tif")) || (fileNameLower.EndsWith("tiff")))
			{
				format = System.Drawing.Imaging.ImageFormat.Tiff;
			}
			else if (fileNameLower.EndsWith("png"))
			{
				format = System.Drawing.Imaging.ImageFormat.Png;
			}

            Image image = Image.FromStream(this.outputStream);
			image.Save(fileName, format);
			image.Dispose();
		}

		/// <summary>Save an image to a file</summary>
		/// <param name="bitmap">The iamge to save</param>
		private void CreateOutputStream(Bitmap bitmap)
		{
			ImageFormat format = ImageFormat.Bmp;
			/*if ((fileNameLower.EndsWith("tif")) || (fileNameLower.EndsWith("tiff")))
			{
				format = System.Drawing.Imaging.ImageFormat.Tiff;
			}
			else if (fileNameLower.EndsWith("png"))
			{
				format = System.Drawing.Imaging.ImageFormat.Png;
			}*/

			//avoid "generic error in GDI+"
			Bitmap saveableBitmap = CopyBitmap(bitmap, format);

			//save bitmap
			outputStream = new MemoryStream();
			saveableBitmap.Save(outputStream, format);
			saveableBitmap.Dispose();
			bitmap.Dispose();
		}

		/// <summary>Hide an Int32 value in pPixel an the following pixels</summary>
		/// <param name="secretValue">The value to hide</param>
		/// <param name="pPixel">The first pixel to use</param>
		private unsafe void HideInt32(Int32 secretValue, ref PixelData* pPixel)
		{
			byte secretByte;

			for (int byteIndex = 0; byteIndex < 4; byteIndex++)
			{
				secretByte = (byte)(secretValue >> (8 * byteIndex));
				HideByte(secretByte, ref pPixel);
			}
		}

		/// <summary>Return one component of a color</summary>
		/// <param name="pPixel">Pointer to the pixel</param>
		/// <param name="colorComponent">The component to return (0-R, 1-G, 2-B)</param>
		/// <returns>The requested component</returns>
		private unsafe byte GetColorComponent(PixelData* pPixel, int colorComponent)
		{
			byte returnValue = 0;
			switch (colorComponent)
			{
				case 0:
					returnValue = pPixel->Red;
					break;
				case 1:
					returnValue = pPixel->Green;
					break;
				case 2:
					returnValue = pPixel->Blue;
					break;
			}
			return returnValue;
		}

		/// <summary>Hide a byte in pPixel an the following pixels</summary>
		/// <param name="secretByte">The value to hide</param>
		/// <param name="pPixel">The first pixel to use</param>
		private unsafe void HideByte(byte secretByte, ref PixelData* pPixel)
		{
			byte colorComponent;

			for (int bitIndex = 0; bitIndex < 8; )
			{
				pPixel += 1;

				//rotate color components
				currentColorComponent = (currentColorComponent == 2) ? 0 : (currentColorComponent + 1);
				//get value of Red, Green or Blue
				colorComponent = GetColorComponent(pPixel, currentColorComponent);

				CopyBitsToColor(1, secretByte, ref bitIndex, ref colorComponent);
				SetColorComponent(pPixel, currentColorComponent, colorComponent);
			}
		}

		/// <summary>Copy one or more bits from the message into a color value</summary>
		/// <param name="bitsPerUnit">Count of bits to copy</param>
		/// <param name="messageByte">Source byte</param>
		/// <param name="messageBitIndex">Index of the first copied bit</param>
		/// <param name="colorComponent">Destination byte</param>
		private void CopyBitsToColor(int bitsPerUnit, byte messageByte, ref int messageBitIndex, ref byte colorComponent)
		{
			for (int carrierBitIndex = 0; carrierBitIndex < bitsPerUnit; carrierBitIndex++)
			{
				colorComponent = SetBit(messageBitIndex, messageByte, carrierBitIndex, colorComponent);
				messageBitIndex++;
			}
		}

		/// <summary>Changes one component of a color</summary>
		/// <param name="pPixel">Pointer to the pixel</param>
		/// <param name="colorComponent">The component to change (0-R, 1-G, 2-B)</param>
		/// <param name="newValue">New value of the component</param>
		private unsafe void SetColorComponent(PixelData* pPixel, int colorComponent, byte newValue)
		{
			switch (colorComponent)
			{
				case 0:
					pPixel->Red = newValue;
					break;
				case 1:
					pPixel->Green = newValue;
					break;
				case 2:
					pPixel->Blue = newValue;
					break;
			}
		}

        /// <summary>Hides the given message stream in the image.</summary>
        /// <param name="message">Message.</param>
        /// <param name="key">A stream with varying seed values for a random number generator.</param>
        public override unsafe void Hide(Stream message, Stream key)
		{
			//make sure that the image is in RGB format
			Bitmap image = PaletteToRGB(bitmap);

			int pixelOffset = 0;
			int maxOffset = 0;
			int messageValue;
			byte keyByte, messageByte, colorComponent;
			Random random;

			BitmapData bitmapData = image.LockBits(
				new Rectangle(0, 0, image.Width, image.Height),
				ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

			//go to the first pixel
			PixelData* pPixel = (PixelData*)bitmapData.Scan0.ToPointer();
			PixelData* pFirstPixel;

			//get the first pixel that belongs to a region
			//and serialise the regions to a map stream
			int firstPixelInRegions = image.Width * image.Height;
			MemoryStream regionData = new MemoryStream();
			BinaryWriter regionDataWriter = new BinaryWriter(regionData);
			foreach (RegionInfo regionInfo in this.imageInfo.RegionInfo)
			{
				regionInfo.PixelIndices.Sort();
				if ((int)regionInfo.PixelIndices[0] < firstPixelInRegions)
				{
					firstPixelInRegions = (int)regionInfo.PixelIndices[0];
				}

				byte[] regionBytes = PointsToBytes(regionInfo.Points);
				regionDataWriter.Write((Int32)regionBytes.Length);
				regionDataWriter.Write((Int32)regionInfo.Capacity);
				regionDataWriter.Write(regionInfo.CountUsedBitsPerPixel);
				regionDataWriter.Write(regionBytes);
			}
			//go to the beginning of the stream
			regionDataWriter.Flush();
			regionData.Seek(0, SeekOrigin.Begin);

			//hide firstPixelInRegions
			HideInt32(firstPixelInRegions, ref pPixel);
			
			//hide length of map stream
			HideInt32((Int32)regionData.Length, ref pPixel);

			//hide regions

			pFirstPixel = pPixel; //don't overwrite already written header

			int regionByte;
			while ((regionByte = regionData.ReadByte()) >= 0)
			{
				keyByte = GetKeyValue(key);
				random = new Random(keyByte);

				for (int regionBitIndex = 0; regionBitIndex < 8; )
				{
					int countRemainingPixels = firstPixelInRegions - 1 - pixelOffset;
					int lengthRemainingStream = (int)(regionData.Length - regionData.Position + 1);
					int lengthBlock = countRemainingPixels / (lengthRemainingStream * 8);

					pixelOffset += random.Next(1, lengthBlock);
					pPixel = pFirstPixel + pixelOffset;

					//place [regionBit] in one bit of the colour component

					//rotate color components
					currentColorComponent = (currentColorComponent == 2) ? 0 : (currentColorComponent + 1);
					//get value of Red, Green or Blue
					colorComponent = GetColorComponent(pPixel, currentColorComponent);

					//put the bits into the color component and write it back into the bitmap
					CopyBitsToColor(1, (byte)regionByte, ref regionBitIndex, ref colorComponent);
					SetColorComponent(pPixel, currentColorComponent, colorComponent);
				}
			}

			// ----------------------------------------- Hide the Message

			//begin with the first pixel
			pPixel = (PixelData*)bitmapData.Scan0.ToPointer();
			pFirstPixel = pPixel; //first pixel of the image

            foreach (RegionInfo regionInfo in this.imageInfo.RegionInfo)
			{

				//go to first pixel of this region
				pPixel = (PixelData*)bitmapData.Scan0.ToPointer();
				pPixel += (int)regionInfo.PixelIndices[0];
				pixelOffset = 0;

				for (int n = 0; n < regionInfo.Capacity; n++)
				{

					messageValue = message.ReadByte();
					if (messageValue < 0) { break; } //end of message
					messageByte = (byte)messageValue;

					keyByte = GetKeyValue(key);
					random = new Random(keyByte);

					for (int messageBitIndex = 0; messageBitIndex < 8; )
					{
						float countRemainingPixels = regionInfo.CountPixels - 1 - pixelOffset;
						float lengthRemainingStream = regionInfo.Capacity - n;
						float countMessageBytesPerPixel = (float)regionInfo.CountUsedBitsPerPixel / 8;
						float lengthBlock = countRemainingPixels * countMessageBytesPerPixel / lengthRemainingStream;

						maxOffset = (int)Math.Floor(lengthBlock);
						pixelOffset += random.Next(1, (maxOffset>0) ? maxOffset : 1);

						pPixel = pFirstPixel + (int)regionInfo.PixelIndices[pixelOffset];

						//place [messageBit] in one bit of the colour component

						//rotate color components
						currentColorComponent = (currentColorComponent == 2) ? 0 : (currentColorComponent + 1);
						//get value of Red, Green or Blue
						colorComponent = GetColorComponent(pPixel, currentColorComponent);

						//put the bits into the color component and write it back into the bitmap
						CopyBitsToColor(regionInfo.CountUsedBitsPerPixel, messageByte, ref messageBitIndex, ref colorComponent);
						SetColorComponent(pPixel, currentColorComponent, colorComponent);
					}
				}
			}

			image.UnlockBits(bitmapData);
			CreateOutputStream(image);
		}

		/// <summary>Convert points (X;Y|X;Y|X;Y) to plain bytes (XYXYXY)</summary>
		private byte[] PointsToBytes(Point[] points)
		{
			MemoryStream stream = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(stream);
			
			for (int pointsIndex = 0; pointsIndex < points.Length; pointsIndex++)
			{
				writer.Write(points[pointsIndex].X);
				writer.Write(points[pointsIndex].Y);
			}

			writer.Flush();
			byte[] result = stream.ToArray();
			return result;
		}

		/// <summary>Convert plain bytes (XYXYXY) to points (X;Y|X;Y|X;Y)</summary>
		private Point[] BytesToPoints(byte[] bytes)
		{
			Point[] result = new Point[bytes.Length / 8];

			MemoryStream stream = new MemoryStream(bytes);
			BinaryReader reader = new BinaryReader(stream);
			stream.Position = 0;

			int resultIndex = 0;
			while (stream.Position < stream.Length)
			{
				result[resultIndex].X = reader.ReadInt32();
				result[resultIndex].Y = reader.ReadInt32();
				resultIndex++;
			}

			return result;
		}

		/// <summary>Extract an Int32 value from pPixel and the following pixels</summary>
		/// <param name="pPixel">The first pixel to use</param>
		/// <returns>The extracted value</returns>
		private unsafe Int32 ExtractInt32(ref PixelData* pPixel)
		{
			int returnValue = 0;
			byte readByte;

			for (int byteIndex = 0; byteIndex < 4; byteIndex++)
			{
				readByte = ExtractByte(ref pPixel);
				returnValue += readByte << (byteIndex * 8);
			}

			return returnValue;
		}

		/// <summary>Extract a byte value from pPixel and the following pixels</summary>
		/// <param name="pPixel">The first pixel to use</param>
		/// <returns>The extracted value</returns>
		private unsafe byte ExtractByte(ref PixelData* pPixel)
		{
			byte colorComponent;
			byte readByte = 0;

			for (int bitIndex = 0; bitIndex < 8; bitIndex++)
			{
				pPixel += 1;
				//rotate color components
				currentColorComponent = (currentColorComponent == 2) ? 0 : (currentColorComponent + 1);
				//get value of Red, Green or Blue
				colorComponent = GetColorComponent(pPixel, currentColorComponent);
				AddBit(bitIndex, ref readByte, 0, colorComponent);
			}

			return readByte;
		}

		/// <summary>Copy the lowest bit from [carrierByte] into a specific bit of [messageByte]</summary>
		/// <param name="messageBitIndex">Position of the bit in [messageByte]</param>
		/// <param name="messageByte">a byte to write into the message stream</param>
		/// <param name="carrierBitIndex">Position of the bit in [carrierByte]</param>
		/// <param name="carrierByte">a byte from the carrier file</param>
		private void AddBit(int messageBitIndex, ref byte messageByte, int carrierBitIndex, byte carrierByte)
		{
			int carrierBit = ((carrierByte & (1 << carrierBitIndex)) > 0) ? 1 : 0;
			messageByte += (byte)(carrierBit << messageBitIndex);
		}

		/// <summary>Extract the header from an image</summary>
		/// <remarks>The header contains information about the regions which carry the message</remarks>
		/// <param name="key">Key stream</param>
		/// <returns>The extracted regions with all meta data that is needed to extract the message</returns>
		public unsafe Collection<RegionInfo> ExtractRegionData(Stream key)
		{
			byte keyByte, colorComponent;
			PixelData* pPixel;
			PixelData* pFirstPixel;
			Random random;
			int pixelOffset = 0;

			BitmapData bitmapData = bitmap.LockBits(
				new Rectangle(0, 0, bitmap.Width, bitmap.Height),
				ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

			//go to the first pixel
			pPixel = (PixelData*)bitmapData.Scan0.ToPointer();

			//get firstPixelInRegions
			int firstPixelInRegions = ExtractInt32(ref pPixel);

			//get length of region information
			int regionDataLength = ExtractInt32(ref pPixel);

			//get region information

			pFirstPixel = pPixel;

			MemoryStream regionData = new MemoryStream();

			byte regionByte;
			while (regionDataLength > regionData.Length)
			{
				regionByte = 0;
				keyByte = GetKeyValue(key);
				random = new Random(keyByte);

				for (int regionBitIndex = 0; regionBitIndex < 8; regionBitIndex++)
				{
					//move to the next pixel

					int countRemainingPixels = firstPixelInRegions - 1 - pixelOffset;
					int lengthRemainingStream = (int)(regionDataLength - regionData.Length);
					int lengthBlock = countRemainingPixels / (lengthRemainingStream * 8);

					pixelOffset += random.Next(1, lengthBlock);
					pPixel = pFirstPixel + pixelOffset;

					//rotate color components
					currentColorComponent = (currentColorComponent == 2) ? 0 : (currentColorComponent + 1);
					//get value of Red, Green or Blue
					colorComponent = GetColorComponent(pPixel, currentColorComponent);

					//extract one bit and add it to [regionByte]
					AddBit(regionBitIndex, ref regionByte, 0, colorComponent);
				}

				//write the extracted byte
				regionData.WriteByte(regionByte);
			}

			bitmap.UnlockBits(bitmapData);

			//read regions from [regionData]

			Collection<RegionInfo> regions = new Collection<RegionInfo>();
			BinaryReader regionReader = new BinaryReader(regionData);

			//extract region header

			regionReader.BaseStream.Seek(0, SeekOrigin.Begin);
			do
			{
				//If the program crashes here,
				//the image is damaged,
				//it contains no hidden data,
				//or you tried to use a wrong key.
				int regionLength = regionReader.ReadInt32();
				int regionCapacity = regionReader.ReadInt32();
				byte regionBitsPerPixel = regionReader.ReadByte();
				byte[] regionContent = regionReader.ReadBytes(regionLength);

				Point[] regionPoints = BytesToPoints(regionContent);
				GraphicsPath regionPath = new GraphicsPath();
				regionPath.AddPolygon(regionPoints);

				//Region anyRegion = new Region(); //dummy region
				//RegionData anyRegionData = anyRegion.GetRegionData(); //dummy region data
				//anyRegionData.Data = regionContent; //get RegionData from bytes

				Region region = new Region(regionPath); //anyRegionData);

				regions.Add(new RegionInfo(region, regionCapacity, regionBitsPerPixel, bitmap.Size));
			} while (regionData.Position < regionData.Length);

			return regions;
		}

		/// <summary>Extracts a stream from the HTML document.</summary>
		/// <param name="key">A stream with varying seed values for a random number generator.</param>
		/// <returns>The extracted stream.</returns>
		public override unsafe Stream Extract(Stream key)
		{
			Bitmap image = bitmap;

			Collection<RegionInfo> regionInfos = ExtractRegionData(key);
			this.imageInfo = new ImageInfo(image, regionInfos);

			BitmapData bitmapData = image.LockBits(
				new Rectangle(0, 0, image.Width, image.Height),
				ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);

			MemoryStream messageStream = new MemoryStream();
			byte keyByte;
			byte messageByte, colorComponent;
			PixelData* pPixel;
			PixelData* pFirstPixel = (PixelData*)bitmapData.Scan0.ToPointer();

			Random random;
			int maxOffset, pixelOffset = 0;

			foreach (RegionInfo regionInfo in this.imageInfo.RegionInfo)
			{
				//go to first pixel of this region
				pFirstPixel = (PixelData*)bitmapData.Scan0.ToPointer();
				pPixel = pFirstPixel + (int)regionInfo.PixelIndices[0];
				pixelOffset = 0;

				for (int n = 0; n < regionInfo.Capacity; n++)
				{

					messageByte = 0;
					keyByte = GetKeyValue(key);
					random = new Random(keyByte);

					for (int messageBitIndex = 0; messageBitIndex < 8; )
					{
						//move to the next pixel

						float countRemainingPixels = regionInfo.CountPixels - pixelOffset - 1;
						float lengthRemainingStream = regionInfo.Capacity - n;
						float countMessageBytesPerPixel = (float)regionInfo.CountUsedBitsPerPixel / 8;
						float lengthBlock = countRemainingPixels * countMessageBytesPerPixel / lengthRemainingStream;
						
						maxOffset = (int)Math.Floor(lengthBlock);
						pixelOffset += random.Next(1, maxOffset);

						pPixel = pFirstPixel + (int)regionInfo.PixelIndices[pixelOffset];

						//rotate color components
						currentColorComponent = (currentColorComponent == 2) ? 0 : (currentColorComponent + 1);
						//get value of Red, Green or Blue
						colorComponent = GetColorComponent(pPixel, currentColorComponent);

						for (int carrierBitIndex = 0; carrierBitIndex < regionInfo.CountUsedBitsPerPixel; carrierBitIndex++)
						{
							AddBit(messageBitIndex, ref messageByte, carrierBitIndex, colorComponent);
							messageBitIndex++;
						}
					}

					//add the re-constructed byte to the message
					messageStream.WriteByte(messageByte);
				}
			}

			//unlock pixels
			image.UnlockBits(bitmapData);

			messageStream.Position = 0;
			StreamReader reader = new StreamReader(messageStream);
			imageInfo.TextMessage = reader.ReadToEnd();

			return messageStream;
		}

		/// <summary>
		/// Gets the parts of the message that will be hidden in this carrier.
		/// The excerpt begins at the current position of the [message] stream,
		/// its length is specified by the CountBytesToHide property.
		/// </summary>
		/// <param name="message">The full message.</param>
		/// <returns>An excerpt from the message.</returns>
		public override Stream GetMessagePart(Stream message)
		{
			BinaryWriter messageWriter = new BinaryWriter(new MemoryStream());

			byte[] buffer = new byte[countBytesToHide];
			message.Read(buffer, 0, countBytesToHide);
			messageWriter.Write(buffer);

			messageWriter.Seek(0, SeekOrigin.Begin);
			return messageWriter.BaseStream;
		}

	}
}
