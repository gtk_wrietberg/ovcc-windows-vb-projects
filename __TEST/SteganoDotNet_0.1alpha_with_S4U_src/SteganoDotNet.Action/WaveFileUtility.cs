/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.Action
{
	/// <summary>Hides/extracts data in/from a wave stream.</summary>
	public class WaveFileUtility : FileUtility
	{
		/// <summary>Format of the source file</summary>
		private WaveFormat carrierFormat;
		
		/// <summary>Position of the "data" chunk in the source file.</summary>
		private long dataPosition;
		
		/// <summary>
		/// The read-only stream.
		/// Clean wave for hiding, Carrier wave for extracting.
		/// </summary>
		private Stream sourceStream;
		
		/// <summary>bits per sample / 8</summary>
		private int bytesPerSample;

		private int noisePercent;

        /// <summary>Returns the FileType instance for supported files.</summary>
        public override FileType FileType
		{
			get
			{
				return WaveFileType.Current;
			}
		}

        /// <summary>
        /// Returns or sets the amount of data that will be hidden in the carrier file, in percent of the message.
        /// Raises the SettingsChanged event.
        /// </summary>
        public override float PercentOfMessage
		{
			get
			{
				return base.PercentOfMessage;
			}
			set
			{
				base.PercentOfMessage = value;
				if(carrierConfigurationControl != null)
				{
					carrierConfigurationControl.PercentOfMessage = value;
				}
				OnSettingsChanged();
			}
		}
       
        /// <summary>
        /// Returns or sets the number of bits that will be stored in each carrier unit.
        /// Raises the SettingsChanged event.
        /// </summary>
		public override int CountUsedBitsPerUnit
		{
			get
			{
				return base.CountUsedBitsPerUnit;
			}
			set
			{
				base.CountUsedBitsPerUnit = value;
				if(carrierConfigurationControl != null)
				{
					((IWaveConfiguration)carrierConfigurationControl).CountUsedBitsPerUnit = value;
				}
				OnSettingsChanged();
			}
		}

        /// <summary>Constructor.</summary>
        /// <param name="fileName">Name and path of a wave file.</param>
        public WaveFileUtility(string fileName)
		{
			sourceFileName = fileName;
			
			//copy source file
			FileStream sourceFileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			byte[] buffer = new byte[sourceFileStream.Length];
			sourceFileStream.Read(buffer, 0, buffer.Length);
			sourceStream = new MemoryStream(buffer);
			sourceFileStream.Close();

			Initialize();
		}

        /// <summary>Constructor.</summary>
		/// <param name="rawData">System.IO.Stream with the wave sound.</param>
        public WaveFileUtility(object rawData)
		{
            sourceFileName = NoSourceFileName;
			sourceStream = (Stream)rawData;
			Initialize();
		}

		private void Initialize(){
			ReadHeader();
			CreateCarrierConfigurationControl("WaveConfiguration");
		}

        /// <summary>TapeWaveConfiguration uses this event handler method to write back new settings.</summary>
        protected override void CarrierConfiguration_AcceptSettings(ICarrierConfiguration sender, EventArgs e)
		{
			IWaveConfiguration control = (IWaveConfiguration)sender;

			countUsedBitsPerUnit = control.CountUsedBitsPerUnit;
			percentOfMessage = control.PercentOfMessage;
			countBytesToHide = 0; //calcuated from countBytesToHidePercent
			noisePercent = control.NoisePercent;
			
			base.CarrierConfiguration_AcceptSettings(sender, e);
		}

		private long SumKeyArray(byte[] values)
		{
			long sum = 0;
			foreach (int value in values)
			{	// '0' causes a distance of one sample,
				// every other key causes a distance of its exact value.
				sum += (value == 0) ? 1 : value;
			}
			return sum;
		}

        /// <summary>Checks whether or not the key can be used to hide the message.</summary>
        /// <param name="message">The message stream.</param>
        /// <param name="key">The key stream.</param>
        /// <returns>A formatted error message, or string.Empty.</returns>
		public override string CheckMessageAndKey(Stream message, Stream key)
		{
			long messageLengthBits = message.Length * 8;
			long countRequiredSamples = 0;
			long keyPosition = key.Position;

			if (messageLengthBits > key.Length)
			{
				long keyLength = key.Length;

				// read existing key
				byte[] keyBytes = new byte[keyLength];
				key.Read(keyBytes, 0, keyBytes.Length);

				// Every byte stands for the distance between two useable samples.
				// The sum of those distances is the required count of samples.
				countRequiredSamples = SumKeyArray(keyBytes);

				// The key must be repeated, until every bit of the message has a key byte.
				double countKeyCopies = messageLengthBits / keyLength;
				countRequiredSamples = (long)(countRequiredSamples * countKeyCopies);
			}
			else
			{
				byte[] keyBytes = new byte[messageLengthBits];
				key.Read(keyBytes, 0, keyBytes.Length);
				countRequiredSamples = SumKeyArray(keyBytes);
			}

			key.Seek(0, SeekOrigin.Begin);

			string returnMessage = string.Empty;
			if (countRequiredSamples > this.countUnits)
			{
				returnMessage = string.Format(
					Constants.ErrorBadKey,
					sourceFileName);
			}

			key.Position = keyPosition;
			return returnMessage;
		}

		private string ReadChunk(BinaryReader reader) {
			byte[] ch = new byte[4];
			reader.Read(ch, 0, ch.Length);
			return System.Text.Encoding.ASCII.GetString(ch);
		}

		private string CopyChunk(BinaryReader reader, BinaryWriter writer) {
			byte[] ch = new byte[4];
			reader.Read(ch, 0, ch.Length);
			
			//copy the chunk
			writer.Write(ch);
			
			return System.Text.Encoding.ASCII.GetString(ch);
		}

        /// <summary>Reads the RIFF Wave header.</summary>
		private void ReadHeader() {
			BinaryReader reader = new BinaryReader(sourceStream);
			
			if (ReadChunk(reader) != "RIFF")
				throw new Exception("Invalid file format");

			reader.ReadInt32(); // File length minus first 8 bytes of RIFF description, we don't use it

			if (ReadChunk(reader) != "WAVE")
				throw new Exception("Invalid file format");

			if (ReadChunk(reader) != "fmt ")
				throw new Exception("Invalid file format");

			int len = reader.ReadInt32();
			if (len < 16) // bad format chunk length
				throw new Exception("Invalid file format");

			carrierFormat = new WaveFormat();
			carrierFormat.FormatTag = reader.ReadInt16();
			carrierFormat.Channels = reader.ReadInt16();
			carrierFormat.SamplesPerSec = reader.ReadInt32();
			carrierFormat.AvgBytesPerSec = reader.ReadInt32();
			carrierFormat.BlockAlign = reader.ReadInt16();
			carrierFormat.BitsPerSample = reader.ReadInt16(); 

			// advance in the stream to skip the wave format block 
			len -= 16; // minimum format size
			while (len > 0) {
				reader.ReadByte();
				len--;
			}

			// assume the data chunk is aligned
			string chunk;
			do{
				chunk = ReadChunk(reader);
			}while(sourceStream.Position < sourceStream.Length && chunk != "data");

			//read length of the data chunk
			int dataLength = reader.ReadInt32();
			countUnits = dataLength / (carrierFormat.BitsPerSample / 8);
			
			this.bytesPerSample = carrierFormat.BitsPerSample / 8;
			this.dataPosition = sourceStream.Position;
		}

		/// <summary>Copies the header from [sourceStream] to [destinationStream].</summary>
		private void CopyHeader() {
			BinaryReader reader = new BinaryReader(sourceStream);
			BinaryWriter writer = new BinaryWriter(this.outputStream);
			byte[] buffer = new byte[16];
			
			//copy "RIFF"+length+"WAVEfmt "
			reader.Read(buffer, 0, 16);
			writer.Write(buffer);

			Int32 len = reader.ReadInt32();
			writer.Write(len);
			
			//copy format
			reader.Read(buffer, 0, 16);
			writer.Write(buffer);
			
			// copy wave format block 
			len -= 16; // minimum format size
			writer.Write( reader.ReadBytes(len) );
			
			// assume the data chunk is aligned
			while(sourceStream.Position < sourceStream.Length && CopyChunk(reader, writer) != "data")
				;


			//copy length of data
			writer.Write( reader.ReadInt32() );
			dataPosition = sourceStream.Position;
		}

		/// <summary>Writes a new header and add wave data.</summary>
		public static Stream CreateStream(WaveFormat format, Stream waveData) {
			MemoryStream stream = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(stream);

			//write RIFF header
			writer.Write(System.Text.Encoding.ASCII.GetBytes("RIFF".ToCharArray()));
			writer.Write((Int32)(waveData.Length + 36)); //File length minus first 8 bytes of RIFF description
			writer.Write(System.Text.Encoding.ASCII.GetBytes("WAVEfmt ".ToCharArray()));
			writer.Write((Int32)16); //length of following chunk: 16

			//write format
			writer.Write((Int16)format.FormatTag);
			writer.Write((Int16)format.Channels);
			writer.Write((Int32)format.SamplesPerSec);
			writer.Write((Int32)format.AvgBytesPerSec);
			writer.Write((Int16)format.BlockAlign);
			writer.Write((Int16)format.BitsPerSample);

			writer.Write(System.Text.Encoding.ASCII.GetBytes("data".ToCharArray()));
			
			//write wave data
			writer.Write((Int32)waveData.Length);

			waveData.Seek(0, SeekOrigin.Begin);
			byte[] b = new byte[waveData.Length];
			waveData.Read(b, 0, (int)waveData.Length);
			writer.Write(b);

			writer.Seek(0, SeekOrigin.Begin);
			return stream;
		}

		private void CopyBitsToSample(int bitsPerUnit, byte messageByte, ref int messageBitIndex, ref byte waveByte){
			for(int carrierBitIndex=0; carrierBitIndex<bitsPerUnit; carrierBitIndex++){
				waveByte = SetBit(messageBitIndex, messageByte, carrierBitIndex, waveByte);
				messageBitIndex++;
			}
		}

		/// <summary>
		/// Hides [messageStream] in [sourceStream],
		/// write the result to [destinationStream].
		/// </summary>
		/// <param name="message">The message to hide.</param>
		/// <param name="key">
		/// A key stream that specifies how many samples shall be
		/// left clean between two changed samples.
		/// </param>
		public override void Hide(Stream message, Stream key)
		{
			byte[] waveBuffer = new byte[bytesPerSample];
			byte[] skipBuffer;
			byte messageByte;
			int messageBuffer; //receives the next byte of the message or -1
			int keyByte; //distance of the next carrier sample
			int bitsPerUnit; //how many bits to hide in each sample

			//carrierFile.CountBitsToHidePerCarrierUnit has not been hidden yet
			bool isCountBitsPerUnitHidden = false;
			
			//reset source stream
			sourceStream.Seek(0, SeekOrigin.Begin);
			
			//open destination file
			this.outputStream = new MemoryStream();
			CopyHeader();

			if(noisePercent > 0){ //add nonsense
				
				Random random = new Random();
				byte[] noiseByte = new byte[1];
				int maxIndexSamples = (int)((sourceStream.Length / bytesPerSample)-1);
				int countNoisySamples = (int)(maxIndexSamples * ((float)noisePercent/100));
				int samplePostition;
				for(int n=0; n<countNoisySamples; n++){
					//get clean sample
					samplePostition = random.Next(0, maxIndexSamples) * bytesPerSample;
					sourceStream.Seek(samplePostition, SeekOrigin.Begin);
					sourceStream.Read(waveBuffer, 0, bytesPerSample);

					//add noise to the sample
					random.NextBytes(noiseByte);
					for(int messageBitIndex=0; messageBitIndex<8; ){
						CopyBitsToSample(
							this.countUsedBitsPerUnit,
							noiseByte[0],
							ref messageBitIndex,
							ref waveBuffer[bytesPerSample-1]);

						sourceStream.Seek(samplePostition, SeekOrigin.Begin);
						sourceStream.Write(waveBuffer, 0, bytesPerSample);
					}
				}

				sourceStream.Seek(dataPosition, SeekOrigin.Begin);
			}

			// ----------------------------------------- Hide the Message

			long streamLength = message.Length;
			while (message.Position < streamLength)
			{
				
				if(isCountBitsPerUnitHidden){
					//read one byte of the message stream
					messageBuffer = message.ReadByte();
					messageByte = (byte)messageBuffer;
					bitsPerUnit = this.countUsedBitsPerUnit;
				}else{
					messageByte = (byte)this.countUsedBitsPerUnit;
					//use default bitcount to hide the correct bitcount
					bitsPerUnit = FileType.MinBitsPerUnit;
				}
				
				//for each bit in message
				for(int messageBitIndex=0; messageBitIndex<8; ){
					
					//read a byte from the key
					keyByte = GetKeyValue(key);
					
					//skip a couple of samples
					skipBuffer = new byte[keyByte * bytesPerSample];
					sourceStream.Read(skipBuffer, 0, skipBuffer.Length);
					outputStream.Write(skipBuffer, 0, skipBuffer.Length);
					
					//read one sample from the wave stream
					sourceStream.Read(waveBuffer, 0, waveBuffer.Length);
					
					CopyBitsToSample(bitsPerUnit, messageByte, ref messageBitIndex, ref waveBuffer[bytesPerSample-1]);
					
					//write the result to destinationStream
					outputStream.Write(waveBuffer, 0, bytesPerSample);
				}

				isCountBitsPerUnitHidden = true;
			}

			//copy the rest of the wave without changes
			waveBuffer = new byte[sourceStream.Length - sourceStream.Position];
			sourceStream.Read(waveBuffer, 0, waveBuffer.Length);
			outputStream.Write(waveBuffer, 0, waveBuffer.Length);

			sourceStream.Close();
		}

		/// <summary>Extracts a message from [sourceStream] into [message].</summary>
		/// <param name="key">
		/// A key stream that specifies how many samples shall be
		/// skipped between two carrier samples.
		/// </param>
		public override Stream Extract(Stream key)
		{
			MemoryStream message = new MemoryStream();

			byte[] waveBuffer = new byte[bytesPerSample];
			byte messageByte, waveByte;
			int keyByte; //distance of the next carrier sample
			int bitsPerUnit; //how many bits are hidden in each sample

			//carrierFile.CountBitsToHidePerCarrierUnit has not been read yet
			bool isCountBitsPerUnitExtracted = false;

			//reset source stream
			sourceStream.Seek(dataPosition, SeekOrigin.Begin);
			
			//for(int n=0; (carrierFile.CountBytesToHide==0 || n<=carrierFile.CountBytesToHide); n++){
			int count = 0; //carrierFile.CountBytesToHide;
			for(; (count>=0 || this.countBytesToHide==0); count--){
				messageByte = 0; //clear the message-byte
				
				if(isCountBitsPerUnitExtracted){
					bitsPerUnit = this.countUsedBitsPerUnit;
				}else{
					//use default bitcount to get the correct bitcount
					bitsPerUnit = FileType.MinBitsPerUnit;
				}

				//for each bit in message
				for(int messageBitIndex=0; messageBitIndex<8; ){ //messageBitIndex++){

					//read a byte from the key
					keyByte = GetKeyValue(key);
					
					//skip a couple of samples
					for(int skippedSamples=0; skippedSamples<keyByte; skippedSamples++){
						//read one sample from the wave stream
						sourceStream.Read(waveBuffer, 0, waveBuffer.Length);
					}
					
					//read one sample from the wave stream
					sourceStream.Read(waveBuffer, 0, waveBuffer.Length);
					
					waveByte = waveBuffer[bytesPerSample-1];
					
					for(int carrierBitIndex=0; carrierBitIndex<bitsPerUnit; carrierBitIndex++){
						messageByte = SetBit(carrierBitIndex, waveByte, messageBitIndex, messageByte);
						//AddBit(messageBitIndex, ref messageByte, carrierBitIndex, waveByte);
						messageBitIndex++;
					}

				}

				if(! isCountBitsPerUnitExtracted){
					this.countUsedBitsPerUnit = messageByte;
					isCountBitsPerUnitExtracted = true;
				}else{
					//add the re-constructed byte to the message
					message.WriteByte(messageByte);
					if(this.countBytesToHide==0 && message.Length==4){
						//first Int32 contains the message's length
						message.Seek(0, SeekOrigin.Begin);
						this.countBytesToHide = new BinaryReader(message).ReadInt32();
						count = this.countBytesToHide;
						message.Seek(0, SeekOrigin.Begin);
						message.SetLength(0);
					}
				}

			}

			sourceStream.Close();
			return message;
		}

        /// <summary>Copies the samples.</summary>
        /// <returns>Wave samples without format information.</returns>
		public Stream GetRawAudioData()
		{
			sourceStream.Position = 0;
			ReadHeader();
			MemoryStream rawDataStream = new MemoryStream();
			int readByte;
			while ((readByte = sourceStream.ReadByte()) > -1)
			{
				rawDataStream.WriteByte((byte)readByte);
			}
			rawDataStream.Position = 0;
			return rawDataStream;
		}
	}
}
