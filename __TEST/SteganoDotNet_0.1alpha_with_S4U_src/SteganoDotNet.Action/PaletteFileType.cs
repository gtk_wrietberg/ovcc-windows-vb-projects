/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;

namespace SteganoDotNet.Action {

    /// <summary>Describes the capabilities of HTML documents and creates instances of HtmlFileUtility.</summary>
    public class PaletteFileType : FileType
	{

		private static PaletteFileType current;

        /// <summary>Returns the only instance PaletteFileType.</summary>
        public static new PaletteFileType Current
		{
			get
			{
				if (current == null)
				{
					current = new PaletteFileType();
				}
				return current;
			}
		}

		private PaletteFileType()
		{
			unitName = "Pixel";
			typeName = "Palette Bitmap";
			supportsClipboard = true;
			supportsNoise = false;
			minBitsPerUnit = 1;
			maxBitsPerUnit = 1;
		}

        /// <summary>Creates a PaletteFileUtility for the given file.</summary>
        /// <param name="fileName">Name and path of the image file.</param>
        /// <returns>An PaletteFileUtility object for the given image.</returns>
        public override FileUtility CreateUtility(string fileName)
		{
			return new PaletteFileUtility(fileName);
		}

        /// <summary>Creates an PaletteFileUtility for the given image.</summary>
        /// <param name="rawData">A Bitmap with 8 bit colour depth.</param>
        /// <returns>A PaletteFileUtility object for the given 8 bit image.</returns>
        public override FileUtility CreateUtility(object rawData)
		{
			return new PaletteFileUtility(rawData);
		}

	}
}
