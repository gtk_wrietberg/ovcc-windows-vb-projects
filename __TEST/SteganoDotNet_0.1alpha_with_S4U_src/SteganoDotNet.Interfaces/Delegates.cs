/*
 Created: 05.08.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;

namespace SteganoDotNet.Interfaces
{
	/// <summary>Delegate for the AcceptSettings event.</summary>
	public delegate void AcceptSettingsHandler(ICarrierConfiguration sender, EventArgs e);

	/// <summary>Delegate for the AcceptSettings event.</summary>
	public delegate void MissingValueHandler(object sender, MissingValueEventArgs e);

	/// <summary>
	/// *FileUtility objects use this class to request values for parameters that have not been filled.
	/// </summary>
	public class MissingValueEventArgs : EventArgs
	{
		private object requiredValue;

		/// <summary>Set this property to the requestesd value.</summary>
		public object RequiredValue
		{
			get { return requiredValue; }
			set { requiredValue = value; }
		}
	}
}