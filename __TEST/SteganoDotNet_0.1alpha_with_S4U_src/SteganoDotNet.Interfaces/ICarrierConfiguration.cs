/*
 Created: 05.08.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.ComponentModel;

namespace SteganoDotNet.Interfaces {

    /// <summary>
    /// Interface for all configuration UI classes.
    /// Every ICarrierConfiguration class derives from System.Windows.Forms.UserControl
    /// and implements this interface.
    /// </summary>
    public interface ICarrierConfiguration {

        /// <summary>Raised when the user submits his changes.</summary>
		event AcceptSettingsHandler AcceptSettings;
		
        /// <summary>Raised when the user cancels the configuration.</summary>
        event EventHandler CancelSettings;

        /// <summary>Returns or sets the filter for output file name selection.</summary>
		string FileExtensionFilter
		{
			get;
			set;
		}

        /// <summary>Returns or sets the length of the entire message that will be hidden in the image.</summary>
		long ExpectedMessageLength
		{
			get;
			set;
		}

		/// <summary>Returns or sets the amount of data that will be hidden in the carrier file, in percent of the message.</summary>
		float PercentOfMessage
		{
			get;
			set;
		}

        /// <summary>Can be overwritten by concrete controls to validate the new settings before they are accepted.</summary>
        void Accept(CancelEventArgs eventArgs);

        /// <summary>Removes the control from the container.</summary>
		void Cancel();

        /// <summary>Raises the AcceptSettings event.</summary>
		void OnAccept();
        
	}
}
