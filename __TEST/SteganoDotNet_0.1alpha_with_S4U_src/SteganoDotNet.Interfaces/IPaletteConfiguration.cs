/*
 Created: 05.08.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;

namespace SteganoDotNet.Interfaces {

    /// <summary>Configuration UI for SteganoDotNet.Action.PaletteFileUtility.</summary>
    public interface IPaletteConfiguration : ICarrierConfiguration
    {
		/// <summary>Returns the selected hiding method.</summary>
		bool SortPalette
		{
			get;
		}

		/// <summary>Returns the selected hiding method.</summary>
		bool StretchPalette
		{
			get;
		}
	}
}
