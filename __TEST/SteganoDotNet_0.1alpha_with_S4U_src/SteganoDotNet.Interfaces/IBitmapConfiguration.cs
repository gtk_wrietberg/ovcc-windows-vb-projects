/*
 Created: 05.08.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.ObjectModel;

namespace SteganoDotNet.Interfaces {

    /// <summary>Configuration UI for SteganoDotNet.Action.BitmapFileUtility.</summary>
	public interface IBitmapConfiguration : ICarrierConfiguration {
		
        /// <summary>Returns or sets, how many percent of the image will be covered with useless pseudo-data.</summary>
		int NoisePercent
		{
			get;
			set;
		}

        /// <summary>Returns the selected regions.</summary>
		Collection<RegionInfo> RegionInfo
		{
			get;
		}

        /// <summary>Returns or sets the number of bytes that will be hidden in the carrier file.</summary>
        int CountBytesToHide
		{
			get;
		}

	}
}
