/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Action;
using SteganoDotNet.UserControls;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.Dispatcher {
	public partial class FreeStyleExtractForm : Form
	{

		internal bool AllowAddRemove
		{
			get
			{
				return tsbAdd.Visible;
			}
			set
			{
				tsbAdd.Visible = value;
				tsbRemove.Visible = value;
			}
		}

		public FreeStyleExtractForm()
		{
			InitializeComponent();
		}

		internal void AddCarrier(string carrierFileName)
		{
			lvCarriers.Items.Add(carrierFileName);
		}

		private void tsbAdd_Click(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = FileType.Current.FileDialogFilter;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				lvCarriers.Items.Add(dlg.FileName);
			}
		}

		private void tsbRemove_Click(object sender, EventArgs e)
		{
			for (int n = lvCarriers.SelectedItems.Count-1; n > -1; n--)
			{
				lvCarriers.SelectedItems[n].Remove();
			}
		}

		private void btnExtract_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			try
			{
				Stream key = sbKey.Stream;
				Stream message = new MemoryStream();
				Stream messagePart;
				foreach (ListViewItem viewItem in lvCarriers.Items)
				{
					//use configured utility, if available
					FileUtility fileUtility = viewItem.Tag as FileUtility;
					if (fileUtility == null)
					{	//create default utility
						FileType fileType = FileType.Current.GetFileType(viewItem.Text);
						fileUtility = fileType.CreateUtility(viewItem.Text);
						AddConfigurationEventHandlers(fileUtility);
						viewItem.Tag = fileUtility;
					}

					messagePart = fileUtility.Extract(key);
					messagePart.Position = 0;
					byte[] buffer = new byte[messagePart.Length];
					messagePart.Read(buffer, 0, (int)messagePart.Length);
					message.Write(buffer, 0, buffer.Length);
					messagePart.Close();
				}

				message.Seek(0, SeekOrigin.Begin);

				if (sbMessage.FileName.Length > 0)
				{
					FileStream fileStream = new FileStream(sbMessage.FileName, FileMode.Create, FileAccess.Write, FileShare.None);
					int blockLength;
					long remainingLength;
					while (message.Position < message.Length)
					{
						remainingLength = message.Length - message.Position;

						if (remainingLength > int.MaxValue)
						{
							blockLength = int.MaxValue;
						}
						else
						{
							blockLength = (int)remainingLength;
						}

						byte[] buffer = new byte[blockLength];
						message.Read(buffer, 0, blockLength);
						fileStream.Write(buffer, 0, blockLength);
					}
					fileStream.Close();
					message.Close();
				}
				else
				{
					StreamReader reader = new StreamReader(message, Encoding.Default);
					string messageText = reader.ReadToEnd();
					sbMessage.MessageText = messageText;
					reader.Close();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(this, ex.ToString());
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		private void tsbOpen_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			try
			{
                ListViewItem viewItem = lvCarriers.SelectedItems[0];
                FileType fileType = FileType.Current.GetFileType(viewItem.Text);
                
                /*if (fileType is WaveFileType)
                {
                    TapeWaveFileUtility utility = viewItem.Tag as TapeWaveFileUtility;
                    if (utility == null)
                    {
                        utility = new TapeWaveFileUtility(viewItem.Text);
                        AddConfigurationEventHandlers(utility);
                    }

                    TapeForm tapeForm = new TapeForm();
                    tapeForm.Open(utility.WaveSound);
                    tapeForm.SoxPath = utility.SoxPath;
                    tapeForm.AllowOpen = false;
                    tapeForm.DisplayHideSettings = false;
                    tapeForm.Frequency = utility.Frequency;
                    tapeForm.Volume = utility.Volume;
                    tapeForm.StartTagOffset = utility.StartTagOffset;
                    if (tapeForm.ShowDialog(this) == DialogResult.OK)
                    {
                        utility.Frequency = tapeForm.Frequency;
                        utility.Volume = tapeForm.Volume;
                        utility.Seconds = tapeForm.FoundSeconds;
                        utility.StartTagOffset = tapeForm.StartTagOffset;
                        viewItem.Tag = utility;
                    }
                }
                else
				 */ 
				if (fileType is BitmapFileType)
                {
                    BitmapFileUtility utility = viewItem.Tag as BitmapFileUtility;
                    if (utility == null)
                    {  
                        //not yet extracted
                        return;
                    }

                    RegionExtractForm regionForm = new RegionExtractForm(utility.ImageInfo);
					Form parentForm = this.FindForm();
					if (parentForm != null)
					{
						regionForm.BackgroundImage = parentForm.BackgroundImage;
					}
                    regionForm.ShowDialog(this);
                }
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		private void AddConfigurationEventHandlers(FileUtility fileUtility)
		{
			ListFileUtility listFileUtility = fileUtility as ListFileUtility;
			if (listFileUtility != null)
			{
				listFileUtility.MissingAlphabetFileName += new MissingValueHandler(AnySortFileUtility_MissingAlphabetFileName);
			}
			else
			{
				HtmlFileUtility htmlFileUtility = fileUtility as HtmlFileUtility;
				if (htmlFileUtility != null)
				{
					htmlFileUtility.MissingAlphabetFileName += new MissingValueHandler(AnySortFileUtility_MissingAlphabetFileName);
				}
				else
				{
					TapeWaveFileUtility tapeWaveFileUtility = fileUtility as TapeWaveFileUtility;
					if (tapeWaveFileUtility != null)
					{
						tapeWaveFileUtility.SoundExchangeNotFound += new EventHandler(TapeWaveUtility_SoundExchangeNotFound);
					}
				}
			}
		}

		private void AnySortFileUtility_MissingAlphabetFileName(object sender, MissingValueEventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				e.RequiredValue = dlg.FileName;
			}
		}

		private void TapeWaveUtility_SoundExchangeNotFound(object sender, EventArgs e)
		{
			//ask user for SoX path
			FindSoxForm dlg = new FindSoxForm();
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				TapeWaveFileUtility utility = (TapeWaveFileUtility)sender;
				utility.SoxPath = dlg.SoxPath;
			}
		}

		private void lvCarriers_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (lvCarriers.SelectedItems.Count > 0)
            {
                FileType fileType = FileType.Current.GetFileType(lvCarriers.SelectedItems[0].Text);
				if (fileType is BitmapFileType)
                {
                    tsbOpen.Enabled = lvCarriers.SelectedItems[0].Tag != null;
                }
            }
		}

		private void btnUp_Click(object sender, EventArgs e)
		{
			if (lvCarriers.SelectedItems.Count > 0)
			{
				ListViewItem selectedItem = lvCarriers.SelectedItems[0];
				if (selectedItem.Index > 0)
				{
					int index = selectedItem.Index;
					ListViewItem upperItem = lvCarriers.Items[selectedItem.Index - 1];
					upperItem.Remove();
					lvCarriers.Items.Insert(index, upperItem);
					selectedItem.Selected = true;
				}
			}
		}

		private void btnDown_Click(object sender, EventArgs e)
		{
			if (lvCarriers.SelectedItems.Count > 0)
			{
				ListViewItem selectedItem = lvCarriers.SelectedItems[0];
				if (selectedItem.Index < lvCarriers.Items.Count - 1)
				{
					int index = selectedItem.Index;
					ListViewItem lowerItem = lvCarriers.Items[selectedItem.Index + 1];
					lowerItem.Remove();
					lvCarriers.Items.Insert(index, lowerItem);
					selectedItem.Selected = true;
				}
			}
		}

	}
}