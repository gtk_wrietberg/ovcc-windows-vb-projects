/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Globalization;
using System.Windows.Forms;
using SteganoDotNet.Action;

namespace SteganoDotNet.Dispatcher {
    public partial class TapeForm : Form {
        public TapeForm()
        {
            InitializeComponent();
        }

		private TapeWaveFileUtility waveUtility;
		private Collection<float> foundSeconds = new Collection<float>();
		private string soxPath;
		private int expectedMessagePartLength;
		private Stream expectedMessagePart;
		private bool isHideMode = false;
		
		public bool AllowOpen
		{
			get
			{
				return waveControl.AllowOpen;
			}
			set
			{
				waveControl.AllowOpen = value;
			}
		}

		public bool DisplayHideSettings
		{
			get
			{
				return isHideMode;
			}
			set
			{
				isHideMode = value;
				panelHide.Visible = value;
				panelFind.Visible = ! value;
			}
		}

		public string SoxPath
		{
			get
			{
				return soxPath;
			}
			set
			{
				soxPath = value;
			}
		}

		public TapeWaveFileUtility TapeWaveFileUtility
		{
			get
			{
				return waveUtility;
			}
		}

		public int Frequency
		{
			get
			{
				if (isHideMode)
				{
					return (int)numHideFrequency.Value;
				}
				else
				{
					return (int)numExtractFrequency.Value;
				}
			}
			set
			{
				numHideFrequency.Value = value;
				numExtractFrequency.Value = value;
			}
		}

		public short Volume
		{
			get
			{
				if (isHideMode)
				{
					return (short)numHideVolume.Value;
				}
				else
				{
					return (short)numExtractVolume.Value;
				}
			}
			set
			{
				numHideVolume.Value = value;
				numExtractVolume.Value = value;
			}
		}

		public int BeepLength
		{
			get
			{
				return (int)numBeepLength.Value;
			}
			set
			{
				numBeepLength.Value = value;
			}
		}

		public int StartTagOffset
		{
			get
			{
				if (!isHideMode && chkRemoveStartTag.Checked)
				{
					return 1;
				}
				else if (isHideMode && chkAddStartTag.Checked)
				{
					return (int)numStartTagOffset.Value;
				}
				else
				{
					return -1;
				}
			}
			set
			{
				if (isHideMode)
				{
					chkAddStartTag.Checked = (value > -1);
					if (chkAddStartTag.Checked)
					{
						numStartTagOffset.Value = value;
					}
					else
					{
						numStartTagOffset.Value = 0;
					}
				}
				else
				{
					chkRemoveStartTag.Checked = (value > -1);
				}
			}
		}

		public int ExpectedMessagePartLength
		{
			get
			{
				return expectedMessagePartLength;
			}
			set
			{
				expectedMessagePartLength = value;
			}
		}

		public Stream ExpectedMessagePart
		{
			get
			{
				return expectedMessagePart;
			}
			set
			{
				expectedMessagePart = value;
				expectedMessagePartLength = (int)expectedMessagePart.Length;
			}
		}

		public Collection<float> FoundSeconds{
			get
			{
				return foundSeconds;
			}
		}

		public void Open(WaveSound waveSound)
		{
			waveControl.WaveSound = waveSound;
		}

		private void TapeForm_Load(object sender, EventArgs e)
		{
			CheckLength(); //display required seconds

			if (this.numHideVolume.Value == 0)
			{
				this.numHideVolume.Value = waveControl.MaxSampleValue;
			}
		}

		private void TapeForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			waveControl.Close();
		}

		private void btnFind_Click(object sender, EventArgs e)
		{
			waveControl.ClearBeeps();

			//amplitudes lower than numExtractVolume.Value are treated as silence
			waveUtility.FindAnything((short)numExtractVolume.Value);

			btnOk.Enabled = true;
		}

		/// <summary>
		/// Check the sound's duration, and display a warning, if it is too short for the message to hide
		/// </summary>
		/// <returns>true: Length is alright; false: sound is too short</returns>
		private bool CheckLength()
		{
			bool returnValue = true;

			Stream worstCaseStream;
			if (expectedMessagePart == null)
			{
				//create stream for minimum capacity
				worstCaseStream = new MemoryStream();
				byte[] buffer = new byte[expectedMessagePartLength];
				for (int n = 0; n < expectedMessagePartLength; n++)
				{
					buffer[n] = 255;
				}
				worstCaseStream.Write(buffer, 0, expectedMessagePartLength);
			}
			else
			{
				worstCaseStream = expectedMessagePart;
			}

			//count maximum of seconds that might be required
			long requiredSeconds = waveUtility.CountRequiredSeconds(worstCaseStream);

			//show count of seconds
			lblExpectedMessageLengthValue.Text = expectedMessagePartLength.ToString(CultureInfo.CurrentCulture);
			lblRequiredSecondsValue.Text = requiredSeconds.ToString(CultureInfo.CurrentCulture);

			//show or remove error message
			if (Math.Floor(waveControl.Duration) < requiredSeconds)
			{
				errorProvider.SetError(lblRequiredSecondsValue, "The message part for this carrier is too long.");
				returnValue = false;
			}
			else
			{
				errorProvider.SetError(lblRequiredSecondsValue, String.Empty);
			}

			panelStep3.Enabled = returnValue;
			btnOk.Enabled = returnValue;
			return returnValue;
		}

		/// <summary>Get a new WaveUtility, after a new file has been opened in the WaveDisplayControl</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void waveControl_Open(object sender, EventArgs e)
		{
			numExtractVolume.Value = waveControl.MaxSampleValue / 2;
			this.waveUtility = waveControl.CreateWaveFileUtility();
			panelFind.Enabled = true;
		}

		/// <summary>Perform a band pass filter on the wave sound</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnExtractBandPass_Click(object sender, EventArgs e)
		{
			this.Cursor = Cursors.WaitCursor;
			try
			{
				String outFileName = waveUtility.FindFrequency(
					Path.GetTempPath(),
					(int)numExtractFrequency.Value);

				waveControl.OpenFromFile(outFileName);
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		/// <summary>Show a warning, if the settings are not good for hiding something in the wave sound</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void btnCheckHideSettings_Click(object sender, EventArgs e)
		{
			long countLoudSamples = 0;
			short minVolume = (short)(numHideVolume.Value - 10);

			this.Cursor = Cursors.WaitCursor;
			try
			{
				//filter for frequency

				String outFileName = waveUtility.FindFrequency(
					Path.GetTempPath(),
					(int)numHideFrequency.Value);

				TapeWaveFileUtility filterUtility = new TapeWaveFileUtility(outFileName);
				WaveSound waveSound = filterUtility.WaveSound;

				//filter for volume, check what is left of the sound

				short[] leftSamples = waveSound.LeftSamples;
				short[] rightSamples = waveSound.RightSamples;
				for (int n = 0; n < leftSamples.Length; n++)
				{
					if ((Math.Abs((int)leftSamples[n]) > minVolume) || (Math.Abs((int)rightSamples[n]) > minVolume))
					{
						countLoudSamples++;
					}
				}
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}

			if (countLoudSamples > 0) {
				MessageBox.Show(String.Format("This frequency might be a bad choice, because the sound contains {0} too loud samples.", countLoudSamples));
				errorProvider.SetError(numHideFrequency, "Frequency not suitable or volume too low.");
			} else {
				errorProvider.SetError(numHideFrequency, String.Empty);
			}

			btnOk.Enabled = true;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			this.Close();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			if (!isHideMode)
			{
				//list the beginning seconds of the selected beeps
				foundSeconds.Clear();
				Collection<Beep> selectedItems = waveControl.SelectedItems;

				//sort seconds
				SortedList<int, float> sortedSelectedItems = new SortedList<int, float>();
				foreach (Beep beep in selectedItems)
				{
					sortedSelectedItems.Add(beep.StartSampleIndex, beep.StartSecond);
				}

				//list seconds
				foreach (float startSecond in sortedSelectedItems.Values)
				{
					foundSeconds.Add(startSecond);
				}
				
			}
			
			this.DialogResult = DialogResult.OK;
			this.Close();
		}
	}
}