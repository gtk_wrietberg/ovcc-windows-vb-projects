namespace SteganoDotNet.Dispatcher {
	partial class CarrierListView
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CarrierListView));
			this.lvCarriers = new System.Windows.Forms.ListView();
			this.colFileType = new System.Windows.Forms.ColumnHeader();
			this.colSource = new System.Windows.Forms.ColumnHeader();
			this.colCapacity = new System.Windows.Forms.ColumnHeader();
			this.colPercentOfMessage = new System.Windows.Forms.ColumnHeader();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.tsbAdd = new System.Windows.Forms.ToolStripButton();
			this.tsbOpen = new System.Windows.Forms.ToolStripButton();
			this.tsbRemove = new System.Windows.Forms.ToolStripButton();
			this.tsbRecordWave = new System.Windows.Forms.ToolStripButton();
			this.tsbPasteBitmap = new System.Windows.Forms.ToolStripButton();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.tsslCountCarriers = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslTextCountCarriers = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslTextCapacity1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslCapacity = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslTextCapacity2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslPercent = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslTextPercent2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.panelSort = new System.Windows.Forms.Panel();
			this.btnDown = new System.Windows.Forms.Button();
			this.btnUp = new System.Windows.Forms.Button();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.tsbSendMail = new System.Windows.Forms.ToolStripButton();
			this.tsbSendFtp = new System.Windows.Forms.ToolStripButton();
			this.tsbSaveAs = new System.Windows.Forms.ToolStripButton();
			this.toolStrip.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.panelSort.SuspendLayout();
			this.SuspendLayout();
			// 
			// lvCarriers
			// 
			this.lvCarriers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFileType,
            this.colSource,
            this.colCapacity,
            this.colPercentOfMessage});
			this.lvCarriers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvCarriers.FullRowSelect = true;
			this.lvCarriers.HideSelection = false;
			this.lvCarriers.Location = new System.Drawing.Point(0, 25);
			this.lvCarriers.MultiSelect = false;
			this.lvCarriers.Name = "lvCarriers";
			this.lvCarriers.Size = new System.Drawing.Size(426, 232);
			this.lvCarriers.TabIndex = 0;
			this.lvCarriers.UseCompatibleStateImageBehavior = false;
			this.lvCarriers.View = System.Windows.Forms.View.Details;
			this.lvCarriers.SelectedIndexChanged += new System.EventHandler(this.lvCarriers_SelectedIndexChanged);
			// 
			// colFileType
			// 
			this.colFileType.Text = "Type";
			this.colFileType.Width = 100;
			// 
			// colSource
			// 
			this.colSource.Text = "Source";
			this.colSource.Width = 100;
			// 
			// colCapacity
			// 
			this.colCapacity.Text = "Capacity (Bytes)";
			this.colCapacity.Width = 100;
			// 
			// colPercentOfMessage
			// 
			this.colPercentOfMessage.Text = "Percent of message";
			this.colPercentOfMessage.Width = 120;
			// 
			// toolStrip
			// 
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdd,
            this.tsbOpen,
            this.tsbRemove,
            this.tsbRecordWave,
            this.tsbPasteBitmap,
            this.toolStripSeparator1,
            this.tsbSaveAs,
            this.tsbSendMail,
            this.tsbSendFtp});
			this.toolStrip.Location = new System.Drawing.Point(0, 0);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size(471, 25);
			this.toolStrip.TabIndex = 2;
			this.toolStrip.Text = "toolStrip1";
			// 
			// tsbAdd
			// 
			this.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdd.Image")));
			this.tsbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbAdd.Name = "tsbAdd";
			this.tsbAdd.Size = new System.Drawing.Size(23, 22);
			this.tsbAdd.ToolTipText = "Add carrier file";
			this.tsbAdd.Click += new System.EventHandler(this.tsbAdd_Click);
			// 
			// tsbOpen
			// 
			this.tsbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbOpen.Enabled = false;
			this.tsbOpen.Image = ((System.Drawing.Image)(resources.GetObject("tsbOpen.Image")));
			this.tsbOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbOpen.Name = "tsbOpen";
			this.tsbOpen.Size = new System.Drawing.Size(23, 22);
			this.tsbOpen.ToolTipText = "Edit configuration";
			this.tsbOpen.Click += new System.EventHandler(this.tsbOpen_Click);
			// 
			// tsbRemove
			// 
			this.tsbRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbRemove.Enabled = false;
			this.tsbRemove.Image = ((System.Drawing.Image)(resources.GetObject("tsbRemove.Image")));
			this.tsbRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbRemove.Name = "tsbRemove";
			this.tsbRemove.Size = new System.Drawing.Size(23, 22);
			this.tsbRemove.ToolTipText = "Remove carrier file";
			this.tsbRemove.Click += new System.EventHandler(this.tsbRemove_Click);
			// 
			// tsbRecordWave
			// 
			this.tsbRecordWave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbRecordWave.Enabled = false;
			this.tsbRecordWave.Image = ((System.Drawing.Image)(resources.GetObject("tsbRecordWave.Image")));
			this.tsbRecordWave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbRecordWave.Name = "tsbRecordWave";
			this.tsbRecordWave.Size = new System.Drawing.Size(23, 22);
			this.tsbRecordWave.Text = "toolStripButton1";
			this.tsbRecordWave.ToolTipText = "Record sound from microphone";
			this.tsbRecordWave.Click += new System.EventHandler(this.tsbRecordWave_Click);
			// 
			// tsbPasteBitmap
			// 
			this.tsbPasteBitmap.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbPasteBitmap.Image = ((System.Drawing.Image)(resources.GetObject("tsbPasteBitmap.Image")));
			this.tsbPasteBitmap.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbPasteBitmap.Name = "tsbPasteBitmap";
			this.tsbPasteBitmap.Size = new System.Drawing.Size(23, 22);
			this.tsbPasteBitmap.Text = "toolStripButton1";
			this.tsbPasteBitmap.ToolTipText = "Paste image from clipboard";
			this.tsbPasteBitmap.Click += new System.EventHandler(this.tsbPasteBitmap_Click);
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslCountCarriers,
            this.tsslTextCountCarriers,
            this.tsslTextCapacity1,
            this.tsslCapacity,
            this.tsslTextCapacity2,
            this.tsslPercent,
            this.tsslTextPercent2});
			this.statusStrip.Location = new System.Drawing.Point(0, 257);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(471, 22);
			this.statusStrip.TabIndex = 3;
			this.statusStrip.Text = "statusStrip1";
			this.toolTip.SetToolTip(this.statusStrip, "Statistik");
			// 
			// tsslCountCarriers
			// 
			this.tsslCountCarriers.Name = "tsslCountCarriers";
			this.tsslCountCarriers.Size = new System.Drawing.Size(13, 17);
			this.tsslCountCarriers.Text = "0";
			// 
			// tsslTextCountCarriers
			// 
			this.tsslTextCountCarriers.Name = "tsslTextCountCarriers";
			this.tsslTextCountCarriers.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
			this.tsslTextCountCarriers.Size = new System.Drawing.Size(110, 17);
			this.tsslTextCountCarriers.Text = "Carriers available";
			// 
			// tsslTextCapacity1
			// 
			this.tsslTextCapacity1.Name = "tsslTextCapacity1";
			this.tsslTextCapacity1.Size = new System.Drawing.Size(49, 17);
			this.tsslTextCapacity1.Text = "Capacity";
			// 
			// tsslCapacity
			// 
			this.tsslCapacity.Name = "tsslCapacity";
			this.tsslCapacity.Size = new System.Drawing.Size(13, 17);
			this.tsslCapacity.Text = "0";
			// 
			// tsslTextCapacity2
			// 
			this.tsslTextCapacity2.Name = "tsslTextCapacity2";
			this.tsslTextCapacity2.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
			this.tsslTextCapacity2.Size = new System.Drawing.Size(54, 17);
			this.tsslTextCapacity2.Text = "Bytes";
			// 
			// tsslPercent
			// 
			this.tsslPercent.Name = "tsslPercent";
			this.tsslPercent.Size = new System.Drawing.Size(13, 17);
			this.tsslPercent.Text = "0";
			// 
			// tsslTextPercent2
			// 
			this.tsslTextPercent2.Name = "tsslTextPercent2";
			this.tsslTextPercent2.Size = new System.Drawing.Size(189, 17);
			this.tsslTextPercent2.Text = "% percent of the message distributed";
			// 
			// panelSort
			// 
			this.panelSort.Controls.Add(this.btnDown);
			this.panelSort.Controls.Add(this.btnUp);
			this.panelSort.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelSort.Location = new System.Drawing.Point(426, 25);
			this.panelSort.Name = "panelSort";
			this.panelSort.Size = new System.Drawing.Size(45, 232);
			this.panelSort.TabIndex = 4;
			// 
			// btnDown
			// 
			this.btnDown.Image = ((System.Drawing.Image)(resources.GetObject("btnDown.Image")));
			this.btnDown.Location = new System.Drawing.Point(6, 126);
			this.btnDown.Name = "btnDown";
			this.btnDown.Size = new System.Drawing.Size(34, 34);
			this.btnDown.TabIndex = 1;
			this.toolTip.SetToolTip(this.btnDown, "Die markierte Datei nach unten schieben");
			this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
			// 
			// btnUp
			// 
			this.btnUp.Image = ((System.Drawing.Image)(resources.GetObject("btnUp.Image")));
			this.btnUp.Location = new System.Drawing.Point(6, 75);
			this.btnUp.Name = "btnUp";
			this.btnUp.Size = new System.Drawing.Size(34, 34);
			this.btnUp.TabIndex = 0;
			this.toolTip.SetToolTip(this.btnUp, "Die markierte Datei nach oben schieben");
			this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
			// 
			// toolTip
			// 
			this.toolTip.IsBalloon = true;
			this.toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.toolTip.ToolTipTitle = "Tr�ger-Dateien";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// tsbSendMail
			// 
			this.tsbSendMail.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbSendMail.Image = ((System.Drawing.Image)(resources.GetObject("tsbSendMail.Image")));
			this.tsbSendMail.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbSendMail.Name = "tsbSendMail";
			this.tsbSendMail.Size = new System.Drawing.Size(23, 22);
			this.tsbSendMail.Text = "EMail seleted results";
			this.tsbSendMail.Click += new System.EventHandler(this.tsbSendMail_Click);
			// 
			// tsbSendFtp
			// 
			this.tsbSendFtp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbSendFtp.Image = ((System.Drawing.Image)(resources.GetObject("tsbSendFtp.Image")));
			this.tsbSendFtp.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbSendFtp.Name = "tsbSendFtp";
			this.tsbSendFtp.Size = new System.Drawing.Size(23, 22);
			this.tsbSendFtp.Text = "Send seleted  result to FTP server";
			this.tsbSendFtp.Click += new System.EventHandler(this.tsbSendFtp_Click);
			// 
			// tsbSaveAs
			// 
			this.tsbSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaveAs.Image")));
			this.tsbSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbSaveAs.Name = "tsbSaveAs";
			this.tsbSaveAs.Size = new System.Drawing.Size(23, 22);
			this.tsbSaveAs.ToolTipText = "Save seleted results as";
			this.tsbSaveAs.Click += new System.EventHandler(this.tsbSaveAs_Click);
			// 
			// CarrierListView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.lvCarriers);
			this.Controls.Add(this.panelSort);
			this.Controls.Add(this.toolStrip);
			this.Controls.Add(this.statusStrip);
			this.Name = "CarrierListView";
			this.Size = new System.Drawing.Size(471, 279);
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.panelSort.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ListView lvCarriers;
		private System.Windows.Forms.ColumnHeader colFileType;
		private System.Windows.Forms.ColumnHeader colCapacity;
		private System.Windows.Forms.ColumnHeader colSource;
		private System.Windows.Forms.ColumnHeader colPercentOfMessage;
		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripButton tsbAdd;
		private System.Windows.Forms.ToolStripButton tsbOpen;
		private System.Windows.Forms.ToolStripButton tsbRemove;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel tsslCountCarriers;
		private System.Windows.Forms.ToolStripStatusLabel tsslTextCountCarriers;
		private System.Windows.Forms.ToolStripStatusLabel tsslPercent;
		private System.Windows.Forms.ToolStripStatusLabel tsslTextPercent2;
		private System.Windows.Forms.ToolStripStatusLabel tsslTextCapacity1;
		private System.Windows.Forms.ToolStripStatusLabel tsslCapacity;
		private System.Windows.Forms.ToolStripStatusLabel tsslTextCapacity2;
		private System.Windows.Forms.ToolStripButton tsbRecordWave;
		private System.Windows.Forms.ToolStripButton tsbPasteBitmap;
		private System.Windows.Forms.Panel panelSort;
		private System.Windows.Forms.Button btnUp;
		private System.Windows.Forms.Button btnDown;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton tsbSendMail;
		private System.Windows.Forms.ToolStripButton tsbSendFtp;
		private System.Windows.Forms.ToolStripButton tsbSaveAs;
	}
}
