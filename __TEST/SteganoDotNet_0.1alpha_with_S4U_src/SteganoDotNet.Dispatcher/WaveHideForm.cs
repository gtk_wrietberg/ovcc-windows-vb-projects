/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Media;
using System.Windows.Forms;
using SteganoDotNet.Action;
using SteganoDotNet.UserControls;

namespace SteganoDotNet.Dispatcher {
	public partial class WaveHideForm : Form {

		private bool handleFileUtilitySettingsChanged = true;
		private bool closeWhenFinished = false;
		private SoundPlayer player;
		private Stream playerStream;

		internal bool AllowAddRemove
		{
			get
			{
				return lvCarriers.AllowAddRemove;
			}
			set
			{
				lvCarriers.AllowAddRemove = value;
			}
		}

		internal bool CloseWhenFinished
		{
			get { return closeWhenFinished; }
			set { closeWhenFinished = value; }
		}

		public WaveHideForm()
		{
			InitializeComponent();
		}

		internal void AddCarrier(TapeWaveFileUtility carrier)
		{
			lvCarriers.AddItem(carrier);
			AddConfigurationEventHandlers(carrier);
		}

		private void OpenCarrier(FileUtility fileUtility)
		{
			CarrierConfiguration control = (CarrierConfiguration)fileUtility.CarrierConfigurationControl;
			control.ExpectedMessageLength = sbMessage.Stream.Length;

			fileUtility.SettingsChanged += new EventHandler(FileUtility_SettingsChanged);
			control.CancelSettings += new EventHandler(CarrierConfiguration_CancelSettings);

			control.Dock = DockStyle.Fill;
			panelCarriers.Controls.Add(control);
			control.BringToFront();

			SetButtonState();
		}

		private void SetButtonState()
		{
			if (panelCarriers.Visible)
			{
				btnBack.Enabled = true;
				btnForward.Enabled = panelCarriers.Controls.Count == 1
					&& lvCarriers.OverallPercent >= 100;
			}
			else if (panelMessage.Visible)
			{
				btnBack.Enabled = true;
				btnForward.Enabled = sbMessage.HasStream;
			}
			else if (panelPlayer.Visible)
			{
				btnBack.Enabled = false;
				btnForward.Enabled = false;
			}
		}

		private void AddConfigurationEventHandlers(TapeWaveFileUtility waveFileUtility)
		{
			waveFileUtility.OpenTapeForm += new EventHandler(TapeWaveUtility_OpenTapeForm);
			waveFileUtility.SoundExchangeNotFound += new EventHandler(TapeWaveUtility_SoundExchangeNotFound);
		}

		private void lvCarriers_AddCarrier(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "Wave *.wav|*.wav";
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				this.Cursor = Cursors.WaitCursor;
				try
				{
					FileType fileType = TapeWaveFileType.Current;
					TapeWaveFileUtility fileUtility = (TapeWaveFileUtility)fileType.CreateUtility(dlg.FileName);

					lvCarriers.AddItem(fileUtility).Selected = true;

					AddConfigurationEventHandlers(fileUtility);
					OpenCarrier(fileUtility);
				}
				finally
				{
					this.Cursor = Cursors.Default;
				}
			}
		}

		private void lvCarriers_OpenCarrier(object sender, EventArgs e)
		{
			FileUtility fileUtility = lvCarriers.SelectedCarrier;
			if (fileUtility != null)
			{
				OpenCarrier(fileUtility);
			}
		}

		private void FileUtility_SettingsChanged(object sender, EventArgs e)
		{
			if (handleFileUtilitySettingsChanged)
			{
				FileUtility fileUtility = (FileUtility)sender;

				if (fileUtility.PercentOfMessage == 0 && fileUtility.CountBytesToHide > 0)
				{	//calculate percentage
					long length = sbMessage.Stream.Length;
					if (length > 0)
					{
						handleFileUtilitySettingsChanged = false;
						fileUtility.PercentOfMessage = 100 * fileUtility.CountBytesToHide / length;
						handleFileUtilitySettingsChanged = true;
					}
				}

				panelCarriers.Controls.Remove((CarrierConfiguration)fileUtility.CarrierConfigurationControl);
				SetButtonState();
			}
		}

		private void CarrierConfiguration_CancelSettings(object sender, EventArgs e)
		{
			panelCarriers.Controls.Remove((CarrierConfiguration)sender);
			lvCarriers.RemoveSelectedItem();
			SetButtonState();
		}

		private void TapeWaveUtility_OpenTapeForm(object sender, EventArgs e)
		{
			TapeWaveFileUtility utility = (TapeWaveFileUtility)sender;
			TapeForm tapeForm = new TapeForm();
			
			this.Cursor = Cursors.WaitCursor;
			try
			{
				tapeForm.Open(utility.WaveSound);
				tapeForm.SoxPath = utility.SoxPath;
				tapeForm.AllowOpen = false;
				tapeForm.DisplayHideSettings = true;
				tapeForm.Frequency = utility.Frequency;
				tapeForm.Volume = utility.Volume;
				tapeForm.BeepLength = utility.BeepLength;
				tapeForm.StartTagOffset = utility.StartTagOffset;

				if (utility.PercentOfMessage == 100)
				{
					tapeForm.ExpectedMessagePart = sbMessage.Stream;
				}
				else
				{
					if (utility.PercentOfMessage == 0)
					{	//no amount specified, yet -> use maximum possible amount
						utility.PercentOfMessage = 100 - (float)lvCarriers.OverallPercent;
					}

					tapeForm.ExpectedMessagePartLength = (int)((float)utility.PercentOfMessage * (float)sbMessage.Stream.Length / 100);
				}
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
			
			if (tapeForm.ShowDialog(this) == DialogResult.OK)
			{
				utility.Frequency = tapeForm.Frequency;
				utility.Volume = tapeForm.Volume;
				utility.BeepLength = tapeForm.BeepLength;
				utility.StartTagOffset = tapeForm.StartTagOffset;

				//% neu berechnen
				TapeWaveFileUtility listUtility = null;
				float hiddenPercent = 0;
				long capacity = lvCarriers.OverallCapacity;
				if (capacity == 0)
				{
					utility.PercentOfMessage = 100;
				}
				else
				{
					foreach (ListViewItem viewItem in lvCarriers.Items)
					{
						listUtility = (TapeWaveFileUtility)viewItem.Tag;
						listUtility.PercentOfMessage = 100 * (float)listUtility.Capacity / (float)capacity;
						hiddenPercent += listUtility.PercentOfMessage;
						lvCarriers.UpdateItem(viewItem);
					}

					if (hiddenPercent < 100)
					{
						listUtility.PercentOfMessage += (100f - hiddenPercent);
					}
					else if (hiddenPercent > 100)
					{
						listUtility.PercentOfMessage -= (hiddenPercent - 100f);
					}
				}
			}
		}

		private void TapeWaveUtility_SoundExchangeNotFound(object sender, EventArgs e)
		{
			//ask user for SoX path
			FindSoxForm dlg = new FindSoxForm();
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				TapeWaveFileUtility utility = (TapeWaveFileUtility)sender;
				utility.SoxPath = dlg.SoxPath;
			}
		}

		private bool HideMessage()
		{
			FileUtility fileUtility;
			Stream messageWithCount = null;
			Hashtable utilitiesToMessageParts = new Hashtable();
			int blockLength;
			long remainingLength;
			long messageLength;
			bool returnValue = false;
	
			if (lvCarriers.Items.Count == 0)
			{
				MessageBox.Show(this, "Bitte w�hlen Sie mindestens eine Tr�gerdatei aus.");
			}
			else
			{
				this.Cursor = Cursors.WaitCursor;

				//create streams for message and key
				Stream message = sbMessage.Stream;
				messageLength = message.Length;
				
				string exception = string.Empty;
				if(lvCarriers.OverallPercent != 100)
				{
					exception = "Bitte verteilen Sie die Nachricht zu 100% auf die Tr�ger.";
				}
				else if ((! lvCarriers.HasTapeWaveUtilities)&&(message.Length > lvCarriers.OverallCapacity))
				{
					exception = "Die Kapazit�t der Tr�ger reicht nicht aus.";
				}				
				else
				{
					foreach (ListViewItem viewItem in lvCarriers.Items)
					{
						fileUtility = (FileUtility)viewItem.Tag;
						blockLength = (int)Math.Ceiling(fileUtility.PercentOfMessage * messageLength / 100);
						remainingLength = message.Length - message.Position;

						fileUtility.CountBytesToHide = (remainingLength < blockLength) ? (int)remainingLength : blockLength;
						messageWithCount = fileUtility.GetMessagePart(message);
						
						utilitiesToMessageParts.Add(fileUtility, messageWithCount);
					}
				}

				if(exception.Length > 0)
				{
					MessageBox.Show(this, exception);
				}
				else
				{
					foreach(object objFileUtility in utilitiesToMessageParts.Keys)
					{
						fileUtility = (FileUtility)objFileUtility;
						messageWithCount = (Stream)utilitiesToMessageParts[objFileUtility];
						fileUtility.Hide(messageWithCount, null);

						string outputFileName = string.Format(
						"{0}_stegano.wav",
						Path.GetFileNameWithoutExtension(fileUtility.SourceFileName));
						outputFileName = Path.Combine(Path.GetDirectoryName(fileUtility.SourceFileName), outputFileName);
						fileUtility.SaveResult(outputFileName);
						
						messageWithCount.Close();
					}

					returnValue = true;
				}

				this.Cursor = Cursors.Default;

				if (closeWhenFinished)
				{
					this.Close();
				}
			}

			return returnValue;
		}

		private void sbKey_StreamChanged(object sender, EventArgs e)
		{
			SetButtonState();
		}

		private void sbMessage_StreamChanged(object sender, EventArgs e)
		{
			SetButtonState();
		}

		private void lvCarriers_SettingsChanged(object sender, EventArgs e)
		{
			SetButtonState();
		}

		private void WaveHideForm_Load(object sender, EventArgs e)
		{
			ShowMessagePanel();
			SetButtonState();
		}

		private void ShowControl(Control control)
		{
			control.Show();
			control.Dock = DockStyle.Fill;
			control.BringToFront();
		}

		private void ShowMessagePanel()
		{
			panelCarriers.Hide();
			panelPlayer.Hide();

			ShowControl(panelMessage);
		}

		private void ShowCarriersPanel()
		{
			panelMessage.Hide();
			panelPlayer.Hide();

			ShowControl(panelCarriers);
		}

		private void ShowPlayerPanel()
		{
			panelMessage.Hide();
			panelCarriers.Hide();

			lvResults.Items.Clear();
			foreach(ListViewItem viewItem in lvCarriers.Items){
				lvResults.Items.Add(((FileUtility)viewItem.Tag).SourceFileName);
			}

			panelPlayer.Show();
			panelPlayer.Dock = DockStyle.Fill;
		}

		/// <summary>Stop the player and close its stream</summary>
		private void PlayerStop()
		{
			if (player != null)
			{
				try
				{
					player.Stop();
					player.Dispose();
				}
				finally
				{
					player = null;
				}
			}
			if (playerStream != null)
			{
				playerStream.Close();
			}
		}

		private void btnForward_Click(object sender, EventArgs e)
		{
			if (panelMessage.Visible)
			{
				ShowCarriersPanel();
			}
			else if (panelCarriers.Visible)
			{
				if (HideMessage())
				{
					ShowPlayerPanel();
				}
			}

			SetButtonState();
		}

		private void btnBack_Click(object sender, EventArgs e)
		{
			if (panelCarriers.Visible)
			{
				ShowMessagePanel();
			}
			else if (panelPlayer.Visible)
			{
				ShowCarriersPanel();
			}

			SetButtonState();
		}

		private void tsbPlay_Click(object sender, EventArgs e)
		{
			foreach (ListViewItem viewItem in lvResults.SelectedItems)
			{
				if (player == null) //not stopped yet
				{
					TapeWaveFileUtility waveUtility = new SteganoDotNet.Action.TapeWaveFileUtility(viewItem.Text);
					WaveSound waveSound = waveUtility.WaveSound;
					playerStream = waveSound.CreateStream();
					this.player = new SoundPlayer(playerStream);
					this.player.Play();
				}
			}
		}

		private void tsbStop_Click(object sender, EventArgs e)
		{
			PlayerStop();
		}

		private void tsbSave_Click(object sender, EventArgs e)
		{
			if (lvResults.SelectedItems.Count > 0)
			{
				SaveFileDialog dlg = new SaveFileDialog();
				dlg.Filter = "*.wav|*.wav";
				dlg.FileName = lvResults.SelectedItems[0].Text;
				if (dlg.ShowDialog() == DialogResult.OK)
				{
					if (dlg.FileName != lvResults.SelectedItems[0].Text)
					{
						File.Move(lvResults.SelectedItems[0].Text, dlg.FileName);
					}
				}
			}
		}

		private void WaveHideForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			//Stop playback, before closing the application
			PlayerStop();
		}

		private void lvCarriers_RecordWave(object sender, EventArgs e)
		{
			// TODO: Integrate a nice sound recorder component
			MessageBox.Show("Not Implemented");

			/* 
			 * RecorderForm recorder = new RecorderForm();
			recorder.ShowDialog(this);
			Stream recordedWave = recorder.RecordedStream;
			WaveSound recordedWaveSound = new WaveSound(recordedWave);
			if (recordedWave != null)
			{
				//add new utility with unique output file name
				TapeWaveFileUtility utility = new TapeWaveFileUtility(recordedWaveSound);
				utility.OutputFileName = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString() + ".wav");
				AddCarrier(utility);
				OpenCarrier(utility);
			}
			 * */
		}

	}
}