/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Action;

namespace SteganoDotNet.Dispatcher {
	public partial class CarrierListView : UserControl
	{

		public event EventHandler AddCarrier;
		public event EventHandler OpenCarrier;
		public event EventHandler SettingsChanged;
		public event EventHandler RecordWave;
		public event EventHandler PasteBitmap;

		private float overallPercent;
		private long overallCapacity;

		public FileUtility SelectedCarrier
		{
			get
			{
				if (lvCarriers.SelectedItems.Count > 0)
				{
					return (FileUtility)lvCarriers.SelectedItems[0].Tag;
				}
				else
				{
					return null;
				}
			}
		}

		public ListView.ListViewItemCollection Items
		{
			get
			{
				return lvCarriers.Items;
			}
		}

		public long OverallCapacity
		{
			get
			{
				return overallCapacity;
			}
		}

		public bool HasTapeWaveUtilities
		{
			get
			{
				bool tapeWaveFound = false;
				foreach (ListViewItem viewItem in lvCarriers.Items)
				{
					if (viewItem.Tag is TapeWaveFileUtility)
					{
						tapeWaveFound = true;
						break;
					}
				}
				return tapeWaveFound;
			}
		}

		public float OverallPercent
		{
			get
			{
				return overallPercent;
			}
		}

		public bool AllowAddRemove
		{
			get
			{
				return tsbAdd.Visible;
			}
			set
			{
				tsbAdd.Visible = value;
				tsbRemove.Visible = value;
			}
		}

		public bool AllowRecordWave
		{
			get
			{
				return tsbRecordWave.Visible;
			}
			set
			{
				tsbRecordWave.Visible = value;
			}
		}

		public bool AllowPasteBitmap
		{
			get
			{
				return tsbPasteBitmap.Visible;
			}
			set
			{
				tsbPasteBitmap.Visible = value;
			}
		}

		public CarrierListView()
		{
			InitializeComponent();
		}

		private void UpdateStatus()
		{
			overallPercent = 0;
			overallCapacity = 0;
			foreach (ListViewItem viewItem in lvCarriers.Items)
			{
				FileUtility fileUtility = (FileUtility)viewItem.Tag;
				overallPercent += fileUtility.PercentOfMessage;
				overallCapacity += fileUtility.Capacity;
			}
			overallPercent = (float)Math.Round(overallPercent, 4);

			tsslCountCarriers.Text = lvCarriers.Items.Count.ToString("n0");
			tsslCapacity.Text = overallCapacity.ToString("n1");
			tsslPercent.Text = overallPercent.ToString("n1");

			if (SettingsChanged != null)
			{
				SettingsChanged(this, EventArgs.Empty);
			}
		}

		public ListViewItem AddItem(FileUtility carrier)
		{
			ListViewItem viewItem = new ListViewItem(new string[] {
				carrier.FileType.TypeName,
				carrier.SourceFileName,
				carrier.Capacity.ToString("n0"),
				carrier.PercentOfMessage.ToString("n1")
			});
			viewItem.Tag = carrier;
			lvCarriers.Items.Add(viewItem);
			viewItem.Selected = true;

			carrier.SettingsChanged += new EventHandler(FileUtility_SettingsChanged);

			//Rectangle viewItemBounds = viewItem.GetBounds(ItemBoundsPortion.Label);
			//NumericUpDown numPercent = new NumericUpDown();
			//numPercent.Top = viewItemBounds.Top;
			//numPercent.Height = viewItemBounds.Height;
			//numPercent.Left = 0;
			//numPercent.Width = panelPercent.Width;

			UpdateStatus();
			return viewItem;
		}

		public void RemoveSelectedItem()
		{
			if (lvCarriers.SelectedItems.Count > 0)
			{
				lvCarriers.SelectedItems[0].Remove();
				UpdateStatus();
			}
		}

		public void UpdateItem(ListViewItem viewItem)
		{
			FileUtility carrier = (FileUtility)viewItem.Tag;
			viewItem.SubItems[1].Text = carrier.SourceFileName;
			viewItem.SubItems[2].Text = carrier.Capacity.ToString("n0");
			viewItem.SubItems[3].Text = carrier.PercentOfMessage.ToString("n1");
			UpdateStatus();
		}

		private void FileUtility_SettingsChanged(object sender, EventArgs e)
		{
			if (lvCarriers.SelectedItems.Count > 0)
			{
				UpdateItem(lvCarriers.SelectedItems[0]);
			}
		}

		private void tsbAdd_Click(object sender, EventArgs e)
		{
			if (AddCarrier != null)
			{
				AddCarrier(this, e);
			}
		}

		private void tsbOpen_Click(object sender, EventArgs e)
		{
			if (OpenCarrier != null)
			{
				OpenCarrier(this, e);
			}
		}

		private void tsbRemove_Click(object sender, EventArgs e)
		{
			RemoveSelectedItem();
		}

		private void tsbRecordWave_Click(object sender, EventArgs e)
		{
			if (RecordWave != null)
			{
				RecordWave(this, EventArgs.Empty);
			}
		}

		private void tsbPasteBitmap_Click(object sender, EventArgs e)
		{
			if (PasteBitmap != null)
			{
				PasteBitmap(this, EventArgs.Empty);
			}
		}

		private void tsbSaveAs_Click(object sender, EventArgs e)
		{
			foreach (ListViewItem viewItem in lvCarriers.SelectedItems)
			{
				FileUtility fileUtility = (FileUtility)viewItem.Tag;
				
				SaveFileDialog dlg = new SaveFileDialog();
				dlg.Title = "Save result from " + Path.GetFileName(fileUtility.SourceFileName);
				dlg.CheckPathExists = true;
				// TODO: Add filter property
				//dlg.Filter = fileUtility.
				if (dlg.ShowDialog() != DialogResult.Cancel)
				{
					fileUtility.SaveResult(dlg.FileName);
				}
			}
		}

		private void tsbSendMail_Click(object sender, EventArgs e)
		{

		}

		private void tsbSendFtp_Click(object sender, EventArgs e)
		{

		}

		private void btnUp_Click(object sender, EventArgs e)
		{
			if (lvCarriers.SelectedItems.Count > 0)
			{
				ListViewItem selectedItem = lvCarriers.SelectedItems[0];
				if (selectedItem.Index > 0)
				{
					int index = selectedItem.Index;
					ListViewItem upperItem = lvCarriers.Items[selectedItem.Index - 1];
					upperItem.Remove();
					lvCarriers.Items.Insert(index, upperItem);
					selectedItem.Selected = true;
				}
			}
		}

		private void btnDown_Click(object sender, EventArgs e)
		{
			if (lvCarriers.SelectedItems.Count > 0)
			{
				ListViewItem selectedItem = lvCarriers.SelectedItems[0];
				if (selectedItem.Index < lvCarriers.Items.Count - 1)
				{
					int index = selectedItem.Index;
					ListViewItem lowerItem = lvCarriers.Items[selectedItem.Index + 1];
					lowerItem.Remove();
					lvCarriers.Items.Insert(index, lowerItem);
					selectedItem.Selected = true;
				}
			}
		}

		private void lvCarriers_SelectedIndexChanged(object sender, EventArgs e)
		{
			bool isItemSelected = (lvCarriers.SelectedItems.Count > 0);
			tsbOpen.Enabled = isItemSelected;
			tsbRemove.Enabled = isItemSelected;
		}
	}
}
