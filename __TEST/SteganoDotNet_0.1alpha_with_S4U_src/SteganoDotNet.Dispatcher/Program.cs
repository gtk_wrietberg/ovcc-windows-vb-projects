/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Forms;
using System.Threading;
using SteganoDotNet.Action;

namespace SteganoDotNet.Dispatcher {
	static class Program {
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();

			//register global error handling
			Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

			try
			{
				Application.Run(new MainForm());
			}
			catch (Exception ex)
			{	//call global error handling
				Application_ThreadException(null, new ThreadExceptionEventArgs(ex));
			}
		}

		private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e){
			string message = "Ein Fehler ist aufgetreten:\r\n\r\n" + e.Exception.ToString();
			MessageBox.Show(message, "Unerwarteter Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}
}