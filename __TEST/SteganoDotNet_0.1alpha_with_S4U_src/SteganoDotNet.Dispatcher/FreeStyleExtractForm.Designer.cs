namespace SteganoDotNet.Dispatcher {
	partial class FreeStyleExtractForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FreeStyleExtractForm));
			this.grpKey = new System.Windows.Forms.GroupBox();
			this.sbKey = new SteganoDotNet.UserControls.StreamBox();
			this.panelCarriers = new System.Windows.Forms.Panel();
			this.lvCarriers = new System.Windows.Forms.ListView();
			this.colFileName = new System.Windows.Forms.ColumnHeader();
			this.panelSort = new System.Windows.Forms.Panel();
			this.btnDown = new System.Windows.Forms.Button();
			this.btnUp = new System.Windows.Forms.Button();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.tsbAdd = new System.Windows.Forms.ToolStripButton();
			this.tsbRemove = new System.Windows.Forms.ToolStripButton();
			this.tsbOpen = new System.Windows.Forms.ToolStripButton();
			this.btnExtract = new System.Windows.Forms.Button();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.grpMessage = new System.Windows.Forms.GroupBox();
			this.sbMessage = new SteganoDotNet.UserControls.StreamBox();
			this.grpKey.SuspendLayout();
			this.panelCarriers.SuspendLayout();
			this.panelSort.SuspendLayout();
			this.toolStrip.SuspendLayout();
			this.grpMessage.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpKey
			// 
			this.grpKey.BackColor = System.Drawing.Color.Transparent;
			this.grpKey.Controls.Add(this.sbKey);
			this.grpKey.Location = new System.Drawing.Point(12, 12);
			this.grpKey.Name = "grpKey";
			this.grpKey.Size = new System.Drawing.Size(492, 133);
			this.grpKey.TabIndex = 5;
			this.grpKey.TabStop = false;
			this.grpKey.Text = "Key";
			this.toolTip.SetToolTip(this.grpKey, "If at least one carrier is neither a palette image nor a text list,");
			// 
			// sbKey
			// 
			this.sbKey.DialogType = SteganoDotNet.UserControls.FileSelectBox.FileDialogType.Open;
			this.sbKey.FileName = "";
			this.sbKey.LabelFile = "Read from file";
			this.sbKey.LabelText = "Enter text";
			this.sbKey.Location = new System.Drawing.Point(6, 19);
			this.sbKey.MessageText = "";
			this.sbKey.Name = "sbKey";
			this.sbKey.Size = new System.Drawing.Size(467, 104);
			this.sbKey.TabIndex = 3;
			// 
			// panelCarriers
			// 
			this.panelCarriers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.panelCarriers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelCarriers.Controls.Add(this.lvCarriers);
			this.panelCarriers.Controls.Add(this.panelSort);
			this.panelCarriers.Controls.Add(this.toolStrip);
			this.panelCarriers.Location = new System.Drawing.Point(12, 295);
			this.panelCarriers.Name = "panelCarriers";
			this.panelCarriers.Size = new System.Drawing.Size(492, 291);
			this.panelCarriers.TabIndex = 6;
			// 
			// lvCarriers
			// 
			this.lvCarriers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFileName});
			this.lvCarriers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvCarriers.FullRowSelect = true;
			this.lvCarriers.HideSelection = false;
			this.lvCarriers.Location = new System.Drawing.Point(0, 25);
			this.lvCarriers.Name = "lvCarriers";
			this.lvCarriers.Size = new System.Drawing.Size(445, 264);
			this.lvCarriers.TabIndex = 4;
			this.toolTip.SetToolTip(this.lvCarriers, "Please list the carrier files in original order.");
			this.lvCarriers.UseCompatibleStateImageBehavior = false;
			this.lvCarriers.View = System.Windows.Forms.View.Details;
			this.lvCarriers.SelectedIndexChanged += new System.EventHandler(this.lvCarriers_SelectedIndexChanged);
			// 
			// colFileName
			// 
			this.colFileName.Text = "Carrier file";
			this.colFileName.Width = 400;
			// 
			// panelSort
			// 
			this.panelSort.Controls.Add(this.btnDown);
			this.panelSort.Controls.Add(this.btnUp);
			this.panelSort.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelSort.Location = new System.Drawing.Point(445, 25);
			this.panelSort.Name = "panelSort";
			this.panelSort.Size = new System.Drawing.Size(45, 264);
			this.panelSort.TabIndex = 5;
			// 
			// btnDown
			// 
			this.btnDown.Image = ((System.Drawing.Image)(resources.GetObject("btnDown.Image")));
			this.btnDown.Location = new System.Drawing.Point(6, 126);
			this.btnDown.Name = "btnDown";
			this.btnDown.Size = new System.Drawing.Size(34, 34);
			this.btnDown.TabIndex = 1;
			this.toolTip.SetToolTip(this.btnDown, "Moves the selected file downwards.");
			this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
			// 
			// btnUp
			// 
			this.btnUp.Image = ((System.Drawing.Image)(resources.GetObject("btnUp.Image")));
			this.btnUp.Location = new System.Drawing.Point(6, 75);
			this.btnUp.Name = "btnUp";
			this.btnUp.Size = new System.Drawing.Size(34, 34);
			this.btnUp.TabIndex = 0;
			this.toolTip.SetToolTip(this.btnUp, "Moves the selected file upwards.");
			this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
			// 
			// toolStrip
			// 
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdd,
            this.tsbRemove,
            this.tsbOpen});
			this.toolStrip.Location = new System.Drawing.Point(0, 0);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size(490, 25);
			this.toolStrip.TabIndex = 3;
			this.toolStrip.Text = "toolStrip1";
			this.toolTip.SetToolTip(this.toolStrip, "Please list the carrier files in original order.");
			// 
			// tsbAdd
			// 
			this.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdd.Image")));
			this.tsbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbAdd.Name = "tsbAdd";
			this.tsbAdd.Size = new System.Drawing.Size(23, 22);
			this.tsbAdd.ToolTipText = "Steganogramm hinzufügen";
			this.tsbAdd.Click += new System.EventHandler(this.tsbAdd_Click);
			// 
			// tsbRemove
			// 
			this.tsbRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbRemove.Image = ((System.Drawing.Image)(resources.GetObject("tsbRemove.Image")));
			this.tsbRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbRemove.Name = "tsbRemove";
			this.tsbRemove.Size = new System.Drawing.Size(23, 22);
			this.tsbRemove.ToolTipText = "Steganogramm entfernen";
			this.tsbRemove.Click += new System.EventHandler(this.tsbRemove_Click);
			// 
			// tsbOpen
			// 
			this.tsbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbOpen.Enabled = false;
			this.tsbOpen.Image = ((System.Drawing.Image)(resources.GetObject("tsbOpen.Image")));
			this.tsbOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbOpen.Name = "tsbOpen";
			this.tsbOpen.Size = new System.Drawing.Size(23, 22);
			this.tsbOpen.ToolTipText = "Konfiguration bearbeiten";
			this.tsbOpen.Click += new System.EventHandler(this.tsbOpen_Click);
			// 
			// btnExtract
			// 
			this.btnExtract.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnExtract.Location = new System.Drawing.Point(426, 592);
			this.btnExtract.Name = "btnExtract";
			this.btnExtract.Size = new System.Drawing.Size(75, 23);
			this.btnExtract.TabIndex = 7;
			this.btnExtract.Text = "Extract";
			this.toolTip.SetToolTip(this.btnExtract, "Applies all settings and extracts the message.");
			this.btnExtract.Click += new System.EventHandler(this.btnExtract_Click);
			// 
			// toolTip
			// 
			this.toolTip.IsBalloon = true;
			this.toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.toolTip.ToolTipTitle = "Nachricht extrahieren";
			// 
			// grpMessage
			// 
			this.grpMessage.BackColor = System.Drawing.Color.Transparent;
			this.grpMessage.Controls.Add(this.sbMessage);
			this.grpMessage.Location = new System.Drawing.Point(12, 151);
			this.grpMessage.Name = "grpMessage";
			this.grpMessage.Size = new System.Drawing.Size(492, 133);
			this.grpMessage.TabIndex = 8;
			this.grpMessage.TabStop = false;
			this.grpMessage.Text = "Message";
			this.toolTip.SetToolTip(this.grpMessage, "The extracted message will be saved to the selected file or displayed here.");
			// 
			// sbMessage
			// 
			this.sbMessage.DialogType = SteganoDotNet.UserControls.FileSelectBox.FileDialogType.Save;
			this.sbMessage.FileName = "";
			this.sbMessage.LabelFile = "Save as";
			this.sbMessage.LabelText = "Display text";
			this.sbMessage.Location = new System.Drawing.Point(6, 19);
			this.sbMessage.MessageText = "";
			this.sbMessage.Name = "sbMessage";
			this.sbMessage.Size = new System.Drawing.Size(467, 104);
			this.sbMessage.TabIndex = 3;
			// 
			// FreeStyleExtractForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(513, 618);
			this.Controls.Add(this.grpMessage);
			this.Controls.Add(this.btnExtract);
			this.Controls.Add(this.panelCarriers);
			this.Controls.Add(this.grpKey);
			this.Name = "FreeStyleExtractForm";
			this.Text = "Freestyle";
			this.grpKey.ResumeLayout(false);
			this.panelCarriers.ResumeLayout(false);
			this.panelCarriers.PerformLayout();
			this.panelSort.ResumeLayout(false);
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.grpMessage.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox grpKey;
		private SteganoDotNet.UserControls.StreamBox sbKey;
		private System.Windows.Forms.Panel panelCarriers;
		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripButton tsbAdd;
		private System.Windows.Forms.ToolStripButton tsbRemove;
		private System.Windows.Forms.ListView lvCarriers;
		private System.Windows.Forms.ColumnHeader colFileName;
		private System.Windows.Forms.Button btnExtract;
		private System.Windows.Forms.ToolStripButton tsbOpen;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.Panel panelSort;
		private System.Windows.Forms.Button btnDown;
		private System.Windows.Forms.Button btnUp;
		private System.Windows.Forms.GroupBox grpMessage;
		private SteganoDotNet.UserControls.StreamBox sbMessage;
	}
}