namespace SteganoDotNet.Dispatcher
{
    partial class TapeForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.btnFind = new System.Windows.Forms.Button();
			this.lblRequiredSecondsValue = new System.Windows.Forms.Label();
			this.numExtractFrequency = new System.Windows.Forms.NumericUpDown();
			this.lblExtractFrequency = new System.Windows.Forms.Label();
			this.lblExtractVolume = new System.Windows.Forms.Label();
			this.numExtractVolume = new System.Windows.Forms.NumericUpDown();
			this.btnExtractBandPass = new System.Windows.Forms.Button();
			this.lblRequiredSeconds = new System.Windows.Forms.Label();
			this.lblExpectedMessageLengthValue = new System.Windows.Forms.Label();
			this.lblExpectedMessageLength = new System.Windows.Forms.Label();
			this.panelStep3 = new System.Windows.Forms.Panel();
			this.lblHideFrequency = new System.Windows.Forms.Label();
			this.numHideVolume = new System.Windows.Forms.NumericUpDown();
			this.numHideFrequency = new System.Windows.Forms.NumericUpDown();
			this.lblHideVolume = new System.Windows.Forms.Label();
			this.btnHideCheckSettings = new System.Windows.Forms.Button();
			this.images = new System.Windows.Forms.ImageList(this.components);
			this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.panelHide = new System.Windows.Forms.Panel();
			this.lblAddStartTag = new System.Windows.Forms.Label();
			this.lblSecond = new System.Windows.Forms.Label();
			this.lblBeepLength = new System.Windows.Forms.Label();
			this.numStartTagOffset = new System.Windows.Forms.NumericUpDown();
			this.numBeepLength = new System.Windows.Forms.NumericUpDown();
			this.chkAddStartTag = new System.Windows.Forms.CheckBox();
			this.panelFind = new System.Windows.Forms.Panel();
			this.chkRemoveStartTag = new System.Windows.Forms.CheckBox();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.waveControl = new SteganoDotNet.Dispatcher.WaveDisplayControl();
			((System.ComponentModel.ISupportInitialize)(this.numExtractFrequency)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numExtractVolume)).BeginInit();
			this.panelStep3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numHideVolume)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numHideFrequency)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			this.panelHide.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numStartTagOffset)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numBeepLength)).BeginInit();
			this.panelFind.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnFind
			// 
			this.btnFind.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnFind.Location = new System.Drawing.Point(180, 41);
			this.btnFind.Name = "btnFind";
			this.btnFind.Size = new System.Drawing.Size(129, 23);
			this.btnFind.TabIndex = 4;
			this.btnFind.Text = "Find markers";
			this.btnFind.UseVisualStyleBackColor = false;
			this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
			// 
			// lblRequiredSecondsValue
			// 
			this.lblRequiredSecondsValue.AutoSize = true;
			this.lblRequiredSecondsValue.Location = new System.Drawing.Point(120, 50);
			this.lblRequiredSecondsValue.Name = "lblRequiredSecondsValue";
			this.lblRequiredSecondsValue.Size = new System.Drawing.Size(64, 13);
			this.lblRequiredSecondsValue.TabIndex = 9;
			this.lblRequiredSecondsValue.Text = "[unbekannt]";
			// 
			// numExtractFrequency
			// 
			this.numExtractFrequency.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numExtractFrequency.Location = new System.Drawing.Point(124, 10);
			this.numExtractFrequency.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
			this.numExtractFrequency.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numExtractFrequency.Name = "numExtractFrequency";
			this.numExtractFrequency.Size = new System.Drawing.Size(50, 20);
			this.numExtractFrequency.TabIndex = 1;
			this.numExtractFrequency.Value = new decimal(new int[] {
            2500,
            0,
            0,
            0});
			// 
			// lblExtractFrequency
			// 
			this.lblExtractFrequency.AutoSize = true;
			this.lblExtractFrequency.Location = new System.Drawing.Point(40, 12);
			this.lblExtractFrequency.Name = "lblExtractFrequency";
			this.lblExtractFrequency.Size = new System.Drawing.Size(79, 13);
			this.lblExtractFrequency.TabIndex = 12;
			this.lblExtractFrequency.Text = "Frequency (Hz)";
			// 
			// lblExtractVolume
			// 
			this.lblExtractVolume.AutoSize = true;
			this.lblExtractVolume.Location = new System.Drawing.Point(9, 43);
			this.lblExtractVolume.Name = "lblExtractVolume";
			this.lblExtractVolume.Size = new System.Drawing.Size(97, 13);
			this.lblExtractVolume.TabIndex = 14;
			this.lblExtractVolume.Text = "Volume (Amplitude)";
			// 
			// numExtractVolume
			// 
			this.numExtractVolume.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numExtractVolume.Location = new System.Drawing.Point(124, 40);
			this.numExtractVolume.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.numExtractVolume.Name = "numExtractVolume";
			this.numExtractVolume.Size = new System.Drawing.Size(50, 20);
			this.numExtractVolume.TabIndex = 3;
			this.numExtractVolume.Value = new decimal(new int[] {
            6000,
            0,
            0,
            0});
			// 
			// btnExtractBandPass
			// 
			this.btnExtractBandPass.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnExtractBandPass.Location = new System.Drawing.Point(180, 7);
			this.btnExtractBandPass.Name = "btnExtractBandPass";
			this.btnExtractBandPass.Size = new System.Drawing.Size(129, 23);
			this.btnExtractBandPass.TabIndex = 2;
			this.btnExtractBandPass.Text = "Filter sound";
			this.btnExtractBandPass.UseVisualStyleBackColor = false;
			this.btnExtractBandPass.Click += new System.EventHandler(this.btnExtractBandPass_Click);
			// 
			// lblRequiredSeconds
			// 
			this.lblRequiredSeconds.AutoSize = true;
			this.lblRequiredSeconds.Location = new System.Drawing.Point(26, 50);
			this.lblRequiredSeconds.Name = "lblRequiredSeconds";
			this.lblRequiredSeconds.Size = new System.Drawing.Size(96, 13);
			this.lblRequiredSeconds.TabIndex = 12;
			this.lblRequiredSeconds.Text = "Required seconds:";
			// 
			// lblExpectedMessageLengthValue
			// 
			this.lblExpectedMessageLengthValue.AutoSize = true;
			this.lblExpectedMessageLengthValue.Location = new System.Drawing.Point(121, 20);
			this.lblExpectedMessageLengthValue.Name = "lblExpectedMessageLengthValue";
			this.lblExpectedMessageLengthValue.Size = new System.Drawing.Size(31, 13);
			this.lblExpectedMessageLengthValue.TabIndex = 32;
			this.lblExpectedMessageLengthValue.Text = "9999";
			// 
			// lblExpectedMessageLength
			// 
			this.lblExpectedMessageLength.AutoSize = true;
			this.lblExpectedMessageLength.Location = new System.Drawing.Point(26, 20);
			this.lblExpectedMessageLength.Name = "lblExpectedMessageLength";
			this.lblExpectedMessageLength.Size = new System.Drawing.Size(85, 13);
			this.lblExpectedMessageLength.TabIndex = 31;
			this.lblExpectedMessageLength.Text = "Message length:";
			// 
			// panelStep3
			// 
			this.panelStep3.BackColor = System.Drawing.Color.Transparent;
			this.panelStep3.Controls.Add(this.lblHideFrequency);
			this.panelStep3.Controls.Add(this.numHideVolume);
			this.panelStep3.Controls.Add(this.numHideFrequency);
			this.panelStep3.Controls.Add(this.lblHideVolume);
			this.panelStep3.Controls.Add(this.btnHideCheckSettings);
			this.panelStep3.Enabled = false;
			this.panelStep3.Location = new System.Drawing.Point(206, 12);
			this.panelStep3.Name = "panelStep3";
			this.panelStep3.Size = new System.Drawing.Size(278, 60);
			this.panelStep3.TabIndex = 30;
			// 
			// lblHideFrequency
			// 
			this.lblHideFrequency.AutoSize = true;
			this.lblHideFrequency.Location = new System.Drawing.Point(6, 8);
			this.lblHideFrequency.Name = "lblHideFrequency";
			this.lblHideFrequency.Size = new System.Drawing.Size(79, 13);
			this.lblHideFrequency.TabIndex = 22;
			this.lblHideFrequency.Text = "Frequency (Hz)";
			// 
			// numHideVolume
			// 
			this.numHideVolume.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numHideVolume.Location = new System.Drawing.Point(123, 36);
			this.numHideVolume.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.numHideVolume.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numHideVolume.Name = "numHideVolume";
			this.numHideVolume.Size = new System.Drawing.Size(50, 20);
			this.numHideVolume.TabIndex = 2;
			this.numHideVolume.Value = new decimal(new int[] {
            6000,
            0,
            0,
            0});
			// 
			// numHideFrequency
			// 
			this.numHideFrequency.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
			this.numHideFrequency.Location = new System.Drawing.Point(123, 6);
			this.numHideFrequency.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
			this.numHideFrequency.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numHideFrequency.Name = "numHideFrequency";
			this.numHideFrequency.Size = new System.Drawing.Size(50, 20);
			this.numHideFrequency.TabIndex = 1;
			this.numHideFrequency.Value = new decimal(new int[] {
            2500,
            0,
            0,
            0});
			// 
			// lblHideVolume
			// 
			this.lblHideVolume.AutoSize = true;
			this.lblHideVolume.Location = new System.Drawing.Point(6, 38);
			this.lblHideVolume.Name = "lblHideVolume";
			this.lblHideVolume.Size = new System.Drawing.Size(97, 13);
			this.lblHideVolume.TabIndex = 24;
			this.lblHideVolume.Text = "Volume (Amplitude)";
			// 
			// btnHideCheckSettings
			// 
			this.btnHideCheckSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.btnHideCheckSettings.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnHideCheckSettings.Location = new System.Drawing.Point(179, 2);
			this.btnHideCheckSettings.Name = "btnHideCheckSettings";
			this.btnHideCheckSettings.Size = new System.Drawing.Size(92, 55);
			this.btnHideCheckSettings.TabIndex = 25;
			this.btnHideCheckSettings.Text = "Check values";
			this.btnHideCheckSettings.UseVisualStyleBackColor = false;
			this.btnHideCheckSettings.Click += new System.EventHandler(this.btnCheckHideSettings_Click);
			// 
			// images
			// 
			this.images.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this.images.ImageSize = new System.Drawing.Size(16, 16);
			this.images.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// errorProvider
			// 
			this.errorProvider.ContainerControl = this;
			// 
			// panelHide
			// 
			this.panelHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.panelHide.BackColor = System.Drawing.Color.Transparent;
			this.panelHide.Controls.Add(this.lblAddStartTag);
			this.panelHide.Controls.Add(this.lblSecond);
			this.panelHide.Controls.Add(this.lblBeepLength);
			this.panelHide.Controls.Add(this.numStartTagOffset);
			this.panelHide.Controls.Add(this.numBeepLength);
			this.panelHide.Controls.Add(this.panelStep3);
			this.panelHide.Controls.Add(this.lblExpectedMessageLength);
			this.panelHide.Controls.Add(this.chkAddStartTag);
			this.panelHide.Controls.Add(this.lblRequiredSeconds);
			this.panelHide.Controls.Add(this.lblExpectedMessageLengthValue);
			this.panelHide.Controls.Add(this.lblRequiredSecondsValue);
			this.panelHide.Location = new System.Drawing.Point(12, 310);
			this.panelHide.Name = "panelHide";
			this.panelHide.Size = new System.Drawing.Size(574, 157);
			this.panelHide.TabIndex = 3;
			// 
			// lblAddStartTag
			// 
			this.lblAddStartTag.AutoSize = true;
			this.lblAddStartTag.Location = new System.Drawing.Point(379, 113);
			this.lblAddStartTag.Name = "lblAddStartTag";
			this.lblAddStartTag.Size = new System.Drawing.Size(47, 13);
			this.lblAddStartTag.TabIndex = 40;
			this.lblAddStartTag.Text = "seconds";
			// 
			// lblSecond
			// 
			this.lblSecond.AutoSize = true;
			this.lblSecond.Location = new System.Drawing.Point(379, 83);
			this.lblSecond.Name = "lblSecond";
			this.lblSecond.Size = new System.Drawing.Size(47, 13);
			this.lblSecond.TabIndex = 37;
			this.lblSecond.Text = "seconds";
			// 
			// lblBeepLength
			// 
			this.lblBeepLength.AutoSize = true;
			this.lblBeepLength.Location = new System.Drawing.Point(249, 82);
			this.lblBeepLength.Name = "lblBeepLength";
			this.lblBeepLength.Size = new System.Drawing.Size(78, 13);
			this.lblBeepLength.TabIndex = 36;
			this.lblBeepLength.Text = "Beep length 1/";
			// 
			// numStartTagOffset
			// 
			this.numStartTagOffset.Location = new System.Drawing.Point(329, 108);
			this.numStartTagOffset.Name = "numStartTagOffset";
			this.numStartTagOffset.Size = new System.Drawing.Size(50, 20);
			this.numStartTagOffset.TabIndex = 5;
			this.numStartTagOffset.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// numBeepLength
			// 
			this.numBeepLength.Location = new System.Drawing.Point(329, 78);
			this.numBeepLength.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
			this.numBeepLength.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.numBeepLength.Name = "numBeepLength";
			this.numBeepLength.Size = new System.Drawing.Size(50, 20);
			this.numBeepLength.TabIndex = 3;
			this.numBeepLength.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
			// 
			// chkAddStartTag
			// 
			this.chkAddStartTag.AutoSize = true;
			this.chkAddStartTag.Location = new System.Drawing.Point(72, 109);
			this.chkAddStartTag.Name = "chkAddStartTag";
			this.chkAddStartTag.Size = new System.Drawing.Size(255, 17);
			this.chkAddStartTag.TabIndex = 4;
			this.chkAddStartTag.Text = "Insert start marker (i.e. for cassette recordings) at";
			// 
			// panelFind
			// 
			this.panelFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.panelFind.BackColor = System.Drawing.Color.Transparent;
			this.panelFind.Controls.Add(this.chkRemoveStartTag);
			this.panelFind.Controls.Add(this.lblExtractFrequency);
			this.panelFind.Controls.Add(this.btnExtractBandPass);
			this.panelFind.Controls.Add(this.btnFind);
			this.panelFind.Controls.Add(this.numExtractVolume);
			this.panelFind.Controls.Add(this.lblExtractVolume);
			this.panelFind.Controls.Add(this.numExtractFrequency);
			this.panelFind.Location = new System.Drawing.Point(12, 310);
			this.panelFind.Name = "panelFind";
			this.panelFind.Size = new System.Drawing.Size(768, 74);
			this.panelFind.TabIndex = 1;
			// 
			// chkRemoveStartTag
			// 
			this.chkRemoveStartTag.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.chkRemoveStartTag.AutoSize = true;
			this.chkRemoveStartTag.Location = new System.Drawing.Point(634, 11);
			this.chkRemoveStartTag.Name = "chkRemoveStartTag";
			this.chkRemoveStartTag.Size = new System.Drawing.Size(131, 17);
			this.chkRemoveStartTag.TabIndex = 5;
			this.chkRemoveStartTag.Text = "There is a start marker";
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnOk.Enabled = false;
			this.btnOk.Location = new System.Drawing.Point(624, 451);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(75, 23);
			this.btnOk.TabIndex = 5;
			this.btnOk.Text = "OK";
			this.btnOk.UseVisualStyleBackColor = false;
			this.btnOk.Click += new System.EventHandler(this.btnOK_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancel.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnCancel.Location = new System.Drawing.Point(705, 451);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(75, 23);
			this.btnCancel.TabIndex = 6;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// waveControl
			// 
			this.waveControl.AllowOpen = true;
			this.waveControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.waveControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.waveControl.Location = new System.Drawing.Point(12, 12);
			this.waveControl.Name = "waveControl";
			this.waveControl.Size = new System.Drawing.Size(768, 280);
			this.waveControl.TabIndex = 4;
			this.waveControl.WaveSound = null;
			this.waveControl.ZoomPercent = 0.1F;
			this.waveControl.Open += new System.EventHandler(this.waveControl_Open);
			// 
			// TapeForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = global::SteganoDotNet.Dispatcher.Properties.Resources.kachel;
			this.ClientSize = new System.Drawing.Size(792, 474);
			this.Controls.Add(this.panelFind);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.waveControl);
			this.Controls.Add(this.panelHide);
			this.Name = "TapeForm";
			this.Text = "Key Frequency";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TapeForm_FormClosing);
			this.Load += new System.EventHandler(this.TapeForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.numExtractFrequency)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numExtractVolume)).EndInit();
			this.panelStep3.ResumeLayout(false);
			this.panelStep3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numHideVolume)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numHideFrequency)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			this.panelHide.ResumeLayout(false);
			this.panelHide.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numStartTagOffset)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numBeepLength)).EndInit();
			this.panelFind.ResumeLayout(false);
			this.panelFind.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

		private System.Windows.Forms.Button btnFind;
		private System.Windows.Forms.Label lblRequiredSecondsValue;
		private System.Windows.Forms.NumericUpDown numExtractFrequency;
		private System.Windows.Forms.Label lblExtractFrequency;
		private System.Windows.Forms.Label lblExtractVolume;
		private System.Windows.Forms.NumericUpDown numExtractVolume;
		private System.Windows.Forms.Button btnExtractBandPass;
		private System.Windows.Forms.Label lblRequiredSeconds;
		private System.Windows.Forms.Button btnHideCheckSettings;
		private System.Windows.Forms.Label lblHideFrequency;
		private System.Windows.Forms.Label lblHideVolume;
		private System.Windows.Forms.NumericUpDown numHideFrequency;
		private System.Windows.Forms.NumericUpDown numHideVolume;
		private System.Windows.Forms.ImageList images;
		private System.Windows.Forms.ErrorProvider errorProvider;
		private System.Windows.Forms.Panel panelStep3;
		private System.Windows.Forms.Label lblExpectedMessageLength;
		private System.Windows.Forms.Label lblExpectedMessageLengthValue;
		private System.Windows.Forms.Panel panelHide;
		private System.Windows.Forms.Panel panelFind;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.Label lblSecond;
		private System.Windows.Forms.Label lblBeepLength;
		private System.Windows.Forms.NumericUpDown numBeepLength;
		private System.Windows.Forms.CheckBox chkAddStartTag;
		private System.Windows.Forms.NumericUpDown numStartTagOffset;
		private WaveDisplayControl waveControl;
		private System.Windows.Forms.Label lblAddStartTag;
		private System.Windows.Forms.CheckBox chkRemoveStartTag;
    }
}

