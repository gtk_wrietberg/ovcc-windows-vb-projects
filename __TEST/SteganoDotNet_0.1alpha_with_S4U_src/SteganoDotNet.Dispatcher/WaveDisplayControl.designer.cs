namespace SteganoDotNet.Dispatcher {
	partial class WaveDisplayControl {
		/// <summary> 
		/// Required designer variable.
		/// </summary>

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WaveDisplayControl));
			this.panelContainer = new System.Windows.Forms.Panel();
			this.picRightWave = new System.Windows.Forms.PictureBox();
			this.picLeftWave = new System.Windows.Forms.PictureBox();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.tsbOpen = new System.Windows.Forms.ToolStripButton();
			this.tsbSave = new System.Windows.Forms.ToolStripButton();
			this.tsbPlay = new System.Windows.Forms.ToolStripButton();
			this.tsbStop = new System.Windows.Forms.ToolStripButton();
			this.tslZoom = new System.Windows.Forms.ToolStripLabel();
			this.tstZoom = new System.Windows.Forms.ToolStripTextBox();
			this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
			this.status = new System.Windows.Forms.StatusStrip();
			this.tsslLengthStatic2 = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslLength = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslLengthStatic1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslVolumeStatic = new System.Windows.Forms.ToolStripStatusLabel();
			this.tsslVolume = new System.Windows.Forms.ToolStripStatusLabel();
			this.panelContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picRightWave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.picLeftWave)).BeginInit();
			this.toolStrip1.SuspendLayout();
			this.status.SuspendLayout();
			this.SuspendLayout();
			// 
			// panelContainer
			// 
			this.panelContainer.AutoScroll = true;
			this.panelContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelContainer.Controls.Add(this.picRightWave);
			this.panelContainer.Controls.Add(this.picLeftWave);
			this.panelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelContainer.Location = new System.Drawing.Point(0, 25);
			this.panelContainer.Name = "panelContainer";
			this.panelContainer.Size = new System.Drawing.Size(850, 400);
			this.panelContainer.TabIndex = 2;
			// 
			// picRightWave
			// 
			this.picRightWave.Location = new System.Drawing.Point(0, 200);
			this.picRightWave.Name = "picRightWave";
			this.picRightWave.Size = new System.Drawing.Size(830, 199);
			this.picRightWave.TabIndex = 3;
			this.picRightWave.TabStop = false;
			// 
			// picLeftWave
			// 
			this.picLeftWave.Location = new System.Drawing.Point(0, 0);
			this.picLeftWave.Name = "picLeftWave";
			this.picLeftWave.Size = new System.Drawing.Size(830, 199);
			this.picLeftWave.TabIndex = 2;
			this.picLeftWave.TabStop = false;
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbOpen,
            this.tsbSave,
            this.tsbPlay,
            this.tsbStop,
            this.tslZoom,
            this.tstZoom,
            this.toolStripLabel1});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(850, 25);
			this.toolStrip1.TabIndex = 3;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// tsbOpen
			// 
			this.tsbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbOpen.Image = ((System.Drawing.Image)(resources.GetObject("tsbOpen.Image")));
			this.tsbOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbOpen.Name = "tsbOpen";
			this.tsbOpen.Size = new System.Drawing.Size(23, 22);
			this.tsbOpen.ToolTipText = "Open File";
			this.tsbOpen.Click += new System.EventHandler(this.tsbOpen_Click);
			// 
			// tsbSave
			// 
			this.tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbSave.Image = ((System.Drawing.Image)(resources.GetObject("tsbSave.Image")));
			this.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbSave.Name = "tsbSave";
			this.tsbSave.Size = new System.Drawing.Size(23, 22);
			this.tsbSave.ToolTipText = "Save File";
			this.tsbSave.Click += new System.EventHandler(this.tsbSave_Click);
			// 
			// tsbPlay
			// 
			this.tsbPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbPlay.Image = ((System.Drawing.Image)(resources.GetObject("tsbPlay.Image")));
			this.tsbPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbPlay.Name = "tsbPlay";
			this.tsbPlay.Size = new System.Drawing.Size(23, 22);
			this.tsbPlay.ToolTipText = "Play";
			this.tsbPlay.Click += new System.EventHandler(this.tsbPlay_Click);
			// 
			// tsbStop
			// 
			this.tsbStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbStop.Image = ((System.Drawing.Image)(resources.GetObject("tsbStop.Image")));
			this.tsbStop.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbStop.Name = "tsbStop";
			this.tsbStop.Size = new System.Drawing.Size(23, 22);
			this.tsbStop.ToolTipText = "Stop";
			this.tsbStop.Click += new System.EventHandler(this.tsbStop_Click);
			// 
			// tslZoom
			// 
			this.tslZoom.Name = "tslZoom";
			this.tslZoom.Size = new System.Drawing.Size(33, 22);
			this.tslZoom.Text = "Zoom";
			// 
			// tstZoom
			// 
			this.tstZoom.Name = "tstZoom";
			this.tstZoom.Size = new System.Drawing.Size(50, 25);
			this.tstZoom.Text = "0,5";
			this.tstZoom.Leave += new System.EventHandler(this.tstZoom_Leave);
			// 
			// toolStripLabel1
			// 
			this.toolStripLabel1.Name = "toolStripLabel1";
			this.toolStripLabel1.Size = new System.Drawing.Size(18, 22);
			this.toolStripLabel1.Text = "%";
			// 
			// status
			// 
			this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslLengthStatic2,
            this.tsslLength,
            this.tsslLengthStatic1,
            this.tsslVolumeStatic,
            this.tsslVolume});
			this.status.Location = new System.Drawing.Point(0, 425);
			this.status.Name = "status";
			this.status.Size = new System.Drawing.Size(850, 22);
			this.status.TabIndex = 4;
			// 
			// tsslLengthStatic2
			// 
			this.tsslLengthStatic2.Name = "tsslLengthStatic2";
			this.tsslLengthStatic2.Size = new System.Drawing.Size(48, 17);
			this.tsslLengthStatic2.Text = "Duration";
			// 
			// tsslLength
			// 
			this.tsslLength.Name = "tsslLength";
			this.tsslLength.Size = new System.Drawing.Size(23, 17);
			this.tsslLength.Text = "0,0";
			// 
			// tsslLengthStatic1
			// 
			this.tsslLengthStatic1.Name = "tsslLengthStatic1";
			this.tsslLengthStatic1.Size = new System.Drawing.Size(23, 17);
			this.tsslLengthStatic1.Text = "sec";
			// 
			// tsslVolumeStatic
			// 
			this.tsslVolumeStatic.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
			this.tsslVolumeStatic.Name = "tsslVolumeStatic";
			this.tsslVolumeStatic.Size = new System.Drawing.Size(104, 17);
			this.tsslVolumeStatic.Text = "Maximum amplitude";
			// 
			// tsslVolume
			// 
			this.tsslVolume.Name = "tsslVolume";
			this.tsslVolume.Size = new System.Drawing.Size(13, 17);
			this.tsslVolume.Text = "0";
			// 
			// WaveDisplayControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panelContainer);
			this.Controls.Add(this.status);
			this.Controls.Add(this.toolStrip1);
			this.Name = "WaveDisplayControl";
			this.Size = new System.Drawing.Size(850, 447);
			this.panelContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.picRightWave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.picLeftWave)).EndInit();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.status.ResumeLayout(false);
			this.status.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel panelContainer;
		private System.Windows.Forms.PictureBox picLeftWave;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripTextBox tstZoom;
		private System.Windows.Forms.ToolStripLabel toolStripLabel1;
		private System.Windows.Forms.ToolStripLabel tslZoom;
		private System.Windows.Forms.ToolStripButton tsbOpen;
		private System.Windows.Forms.ToolStripButton tsbPlay;
		private System.Windows.Forms.ToolStripButton tsbStop;
        private System.Windows.Forms.ToolStripButton tsbSave;
		private System.Windows.Forms.StatusStrip status;
		private System.Windows.Forms.ToolStripStatusLabel tsslLength;
		private System.Windows.Forms.ToolStripStatusLabel tsslLengthStatic1;
		private System.Windows.Forms.ToolStripStatusLabel tsslVolume;
		private System.Windows.Forms.ToolStripStatusLabel tsslVolumeStatic;
		private System.Windows.Forms.ToolStripStatusLabel tsslLengthStatic2;
		private System.Windows.Forms.PictureBox picRightWave;

	}
}
