namespace SteganoDotNet.Dispatcher {
	partial class FreeStyleHideForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FreeStyleHideForm));
			this.btnHide = new System.Windows.Forms.Button();
			this.grpMessage = new System.Windows.Forms.GroupBox();
			this.sbMessage = new SteganoDotNet.UserControls.StreamBox();
			this.grpKey = new System.Windows.Forms.GroupBox();
			this.sbKey = new SteganoDotNet.UserControls.StreamBox();
			this.lvCarriers = new SteganoDotNet.Dispatcher.CarrierListView();
			this.panelCarriers = new System.Windows.Forms.Panel();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.grpMessage.SuspendLayout();
			this.grpKey.SuspendLayout();
			this.panelCarriers.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnHide
			// 
			this.btnHide.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnHide.Enabled = false;
			this.btnHide.Location = new System.Drawing.Point(428, 570);
			this.btnHide.Name = "btnHide";
			this.btnHide.Size = new System.Drawing.Size(75, 23);
			this.btnHide.TabIndex = 1;
			this.btnHide.Text = "Hide";
			this.toolTip.SetToolTip(this.btnHide, "Applies all settings and hides the message in the carriers.");
			this.btnHide.UseVisualStyleBackColor = false;
			this.btnHide.Click += new System.EventHandler(this.btnHide_Click);
			// 
			// grpMessage
			// 
			this.grpMessage.BackColor = System.Drawing.Color.Transparent;
			this.grpMessage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grpMessage.BackgroundImage")));
			this.grpMessage.Controls.Add(this.sbMessage);
			this.grpMessage.Location = new System.Drawing.Point(12, 144);
			this.grpMessage.Name = "grpMessage";
			this.grpMessage.Size = new System.Drawing.Size(492, 135);
			this.grpMessage.TabIndex = 4;
			this.grpMessage.TabStop = false;
			this.grpMessage.Text = "Message";
			this.toolTip.SetToolTip(this.grpMessage, "Please select the file you want to hide, or enter your secret message as plain te" +
					"xt.");
			// 
			// sbMessage
			// 
			this.sbMessage.BackColor = System.Drawing.Color.Transparent;
			this.sbMessage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sbMessage.BackgroundImage")));
			this.sbMessage.DialogType = SteganoDotNet.UserControls.FileSelectBox.FileDialogType.Open;
			this.sbMessage.FileName = "";
			this.sbMessage.LabelFile = "Read from file";
			this.sbMessage.LabelText = "Enter text";
			this.sbMessage.Location = new System.Drawing.Point(6, 19);
			this.sbMessage.MessageText = "";
			this.sbMessage.Name = "sbMessage";
			this.sbMessage.Size = new System.Drawing.Size(467, 104);
			this.sbMessage.TabIndex = 2;
			this.sbMessage.StreamChanged += new System.EventHandler(this.sbMessage_StreamChanged);
			// 
			// grpKey
			// 
			this.grpKey.BackColor = System.Drawing.Color.Transparent;
			this.grpKey.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grpKey.BackgroundImage")));
			this.grpKey.Controls.Add(this.sbKey);
			this.grpKey.Location = new System.Drawing.Point(12, 12);
			this.grpKey.Name = "grpKey";
			this.grpKey.Size = new System.Drawing.Size(492, 133);
			this.grpKey.TabIndex = 5;
			this.grpKey.TabStop = false;
			this.grpKey.Text = "Key";
			this.toolTip.SetToolTip(this.grpKey, "Secure your message with a key, if one or more carriers are neither palette image" +
					" nor text list. Your key can be any file and will be needed to extract the messa" +
					"ge.");
			// 
			// sbKey
			// 
			this.sbKey.BackColor = System.Drawing.Color.Transparent;
			this.sbKey.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sbKey.BackgroundImage")));
			this.sbKey.DialogType = SteganoDotNet.UserControls.FileSelectBox.FileDialogType.Open;
			this.sbKey.FileName = "";
			this.sbKey.LabelFile = "Read from file";
			this.sbKey.LabelText = "Enter text";
			this.sbKey.Location = new System.Drawing.Point(6, 19);
			this.sbKey.MessageText = "";
			this.sbKey.Name = "sbKey";
			this.sbKey.Size = new System.Drawing.Size(467, 104);
			this.sbKey.TabIndex = 3;
			this.sbKey.StreamChanged += new System.EventHandler(this.sbKey_StreamChanged);
			// 
			// lvCarriers
			// 
			this.lvCarriers.AllowAddRemove = true;
			this.lvCarriers.AllowPasteBitmap = true;
			this.lvCarriers.AllowRecordWave = true;
			this.lvCarriers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvCarriers.Location = new System.Drawing.Point(0, 0);
			this.lvCarriers.Name = "lvCarriers";
			this.lvCarriers.Size = new System.Drawing.Size(490, 277);
			this.lvCarriers.TabIndex = 4;
			this.toolTip.SetToolTip(this.lvCarriers, "Collect and configure your carriers here");
			this.lvCarriers.AddCarrier += new System.EventHandler(this.lvCarriers_AddCarrier);
			this.lvCarriers.SettingsChanged += new System.EventHandler(this.lvCarriers_SettingsChanged);
			this.lvCarriers.OpenCarrier += new System.EventHandler(this.lvCarriers_OpenCarrier);
			this.lvCarriers.RecordWave += new System.EventHandler(this.lvCarriers_RecordWave);
			this.lvCarriers.PasteBitmap += new System.EventHandler(this.lvCarriers_PasteBitmap);
			// 
			// panelCarriers
			// 
			this.panelCarriers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelCarriers.Controls.Add(this.lvCarriers);
			this.panelCarriers.Location = new System.Drawing.Point(12, 285);
			this.panelCarriers.Name = "panelCarriers";
			this.panelCarriers.Size = new System.Drawing.Size(492, 279);
			this.panelCarriers.TabIndex = 6;
			// 
			// toolTip
			// 
			this.toolTip.IsBalloon = true;
			this.toolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
			this.toolTip.ToolTipTitle = "Steganogramm erstellen";
			// 
			// FreeStyleHideForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.BackgroundImage = global::SteganoDotNet.Dispatcher.Properties.Resources.kachel;
			this.ClientSize = new System.Drawing.Size(517, 596);
			this.Controls.Add(this.panelCarriers);
			this.Controls.Add(this.grpKey);
			this.Controls.Add(this.grpMessage);
			this.Controls.Add(this.btnHide);
			this.Name = "FreeStyleHideForm";
			this.Text = "Freestyle";
			this.grpMessage.ResumeLayout(false);
			this.grpKey.ResumeLayout(false);
			this.panelCarriers.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnHide;
		private System.Windows.Forms.GroupBox grpMessage;
		private System.Windows.Forms.GroupBox grpKey;
		private SteganoDotNet.UserControls.StreamBox sbMessage;
		private SteganoDotNet.UserControls.StreamBox sbKey;
		private SteganoDotNet.Dispatcher.CarrierListView lvCarriers;
		private System.Windows.Forms.Panel panelCarriers;
		private System.Windows.Forms.ToolTip toolTip;
	}
}