namespace SteganoDotNet.Dispatcher
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.grpHide = new System.Windows.Forms.GroupBox();
			this.btnHideDisc = new System.Windows.Forms.Button();
			this.btnHideFreeStyle = new System.Windows.Forms.Button();
			this.grpExtract = new System.Windows.Forms.GroupBox();
			this.btnExtractDisc = new System.Windows.Forms.Button();
			this.btnExtractFreeStyle = new System.Windows.Forms.Button();
			this.picTitle = new System.Windows.Forms.PictureBox();
			this.grpHide.SuspendLayout();
			this.grpExtract.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.picTitle)).BeginInit();
			this.SuspendLayout();
			// 
			// grpHide
			// 
			this.grpHide.BackColor = System.Drawing.Color.Transparent;
			this.grpHide.Controls.Add(this.btnHideDisc);
			this.grpHide.Controls.Add(this.btnHideFreeStyle);
			this.grpHide.Location = new System.Drawing.Point(17, 155);
			this.grpHide.Name = "grpHide";
			this.grpHide.Size = new System.Drawing.Size(458, 108);
			this.grpHide.TabIndex = 0;
			this.grpHide.TabStop = false;
			this.grpHide.Text = "Hide";
			// 
			// btnHideDisc
			// 
			this.btnHideDisc.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnHideDisc.Location = new System.Drawing.Point(114, 61);
			this.btnHideDisc.Name = "btnHideDisc";
			this.btnHideDisc.Size = new System.Drawing.Size(231, 23);
			this.btnHideDisc.TabIndex = 1;
			this.btnHideDisc.Text = "Audio for analog recording (loss resistant)";
			this.btnHideDisc.UseVisualStyleBackColor = false;
			this.btnHideDisc.Click += new System.EventHandler(this.btnHideDisc_Click);
			// 
			// btnHideFreeStyle
			// 
			this.btnHideFreeStyle.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnHideFreeStyle.Location = new System.Drawing.Point(114, 19);
			this.btnHideFreeStyle.Name = "btnHideFreeStyle";
			this.btnHideFreeStyle.Size = new System.Drawing.Size(231, 23);
			this.btnHideFreeStyle.TabIndex = 0;
			this.btnHideFreeStyle.Text = "Freestyle";
			this.btnHideFreeStyle.UseVisualStyleBackColor = false;
			this.btnHideFreeStyle.Click += new System.EventHandler(this.btnHideFreeStyle_Click);
			// 
			// grpExtract
			// 
			this.grpExtract.BackColor = System.Drawing.Color.Transparent;
			this.grpExtract.Controls.Add(this.btnExtractDisc);
			this.grpExtract.Controls.Add(this.btnExtractFreeStyle);
			this.grpExtract.Location = new System.Drawing.Point(17, 306);
			this.grpExtract.Name = "grpExtract";
			this.grpExtract.Size = new System.Drawing.Size(458, 108);
			this.grpExtract.TabIndex = 1;
			this.grpExtract.TabStop = false;
			this.grpExtract.Text = "Extract";
			// 
			// btnExtractDisc
			// 
			this.btnExtractDisc.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnExtractDisc.Location = new System.Drawing.Point(114, 64);
			this.btnExtractDisc.Name = "btnExtractDisc";
			this.btnExtractDisc.Size = new System.Drawing.Size(231, 23);
			this.btnExtractDisc.TabIndex = 3;
			this.btnExtractDisc.Text = "Audio for analog recording (loss resistant)";
			this.btnExtractDisc.UseVisualStyleBackColor = false;
			this.btnExtractDisc.Click += new System.EventHandler(this.btnExtractDisc_Click);
			// 
			// btnExtractFreeStyle
			// 
			this.btnExtractFreeStyle.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnExtractFreeStyle.Location = new System.Drawing.Point(114, 22);
			this.btnExtractFreeStyle.Name = "btnExtractFreeStyle";
			this.btnExtractFreeStyle.Size = new System.Drawing.Size(231, 23);
			this.btnExtractFreeStyle.TabIndex = 2;
			this.btnExtractFreeStyle.Text = "Freestyle";
			this.btnExtractFreeStyle.UseVisualStyleBackColor = false;
			this.btnExtractFreeStyle.Click += new System.EventHandler(this.btnExtractFreeStyle_Click);
			// 
			// picTitle
			// 
			this.picTitle.Image = ((System.Drawing.Image)(resources.GetObject("picTitle.Image")));
			this.picTitle.Location = new System.Drawing.Point(0, 0);
			this.picTitle.Name = "picTitle";
			this.picTitle.Size = new System.Drawing.Size(500, 116);
			this.picTitle.TabIndex = 2;
			this.picTitle.TabStop = false;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.BackgroundImage = global::SteganoDotNet.Dispatcher.Properties.Resources.kachel;
			this.ClientSize = new System.Drawing.Size(492, 450);
			this.Controls.Add(this.picTitle);
			this.Controls.Add(this.grpExtract);
			this.Controls.Add(this.grpHide);
			this.Name = "MainForm";
			this.Text = "Steganography For You";
			this.grpHide.ResumeLayout(false);
			this.grpExtract.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.picTitle)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox grpHide;
		private System.Windows.Forms.GroupBox grpExtract;
		private System.Windows.Forms.Button btnHideFreeStyle;
		private System.Windows.Forms.Button btnHideDisc;
		private System.Windows.Forms.Button btnExtractDisc;
		private System.Windows.Forms.Button btnExtractFreeStyle;
		private System.Windows.Forms.PictureBox picTitle;
	}
}