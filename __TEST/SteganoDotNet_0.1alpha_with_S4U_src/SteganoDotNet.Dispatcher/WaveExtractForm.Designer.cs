namespace SteganoDotNet.Dispatcher {
	partial class WaveExtractForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WaveExtractForm));
			this.grpMessage = new System.Windows.Forms.GroupBox();
			this.rdoMessageFile = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.txtMessage = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.panelCarriers = new System.Windows.Forms.Panel();
			this.lvCarriers = new System.Windows.Forms.ListView();
			this.colFileName = new System.Windows.Forms.ColumnHeader();
			this.colIsConfigured = new System.Windows.Forms.ColumnHeader();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.tsbAdd = new System.Windows.Forms.ToolStripButton();
			this.tsbOpen = new System.Windows.Forms.ToolStripButton();
			this.tsbRemove = new System.Windows.Forms.ToolStripButton();
			this.panelNavigation = new System.Windows.Forms.Panel();
			this.btnForward = new System.Windows.Forms.Button();
			this.btnBack = new System.Windows.Forms.Button();
			this.grpMessage.SuspendLayout();
			this.panelCarriers.SuspendLayout();
			this.toolStrip.SuspendLayout();
			this.panelNavigation.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpMessage
			// 
			this.grpMessage.BackColor = System.Drawing.Color.Transparent;
			this.grpMessage.Controls.Add(this.rdoMessageFile);
			this.grpMessage.Controls.Add(this.radioButton1);
			this.grpMessage.Controls.Add(this.txtMessage);
			this.grpMessage.Controls.Add(this.textBox1);
			this.grpMessage.Location = new System.Drawing.Point(12, 36);
			this.grpMessage.Name = "grpMessage";
			this.grpMessage.Size = new System.Drawing.Size(492, 135);
			this.grpMessage.TabIndex = 4;
			this.grpMessage.TabStop = false;
			this.grpMessage.Text = "Message";
			// 
			// rdoMessageFile
			// 
			this.rdoMessageFile.AutoSize = true;
			this.rdoMessageFile.Location = new System.Drawing.Point(13, 13);
			this.rdoMessageFile.Name = "rdoMessageFile";
			this.rdoMessageFile.Size = new System.Drawing.Size(64, 17);
			this.rdoMessageFile.TabIndex = 5;
			this.rdoMessageFile.Text = "Save as";
			// 
			// radioButton1
			// 
			this.radioButton1.AutoSize = true;
			this.radioButton1.Checked = true;
			this.radioButton1.Location = new System.Drawing.Point(13, 41);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(70, 17);
			this.radioButton1.TabIndex = 4;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "Enter text";
			// 
			// txtMessage
			// 
			this.txtMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtMessage.Location = new System.Drawing.Point(114, 40);
			this.txtMessage.Multiline = true;
			this.txtMessage.Name = "txtMessage";
			this.txtMessage.Size = new System.Drawing.Size(324, 80);
			this.txtMessage.TabIndex = 2;
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(114, 13);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(324, 20);
			this.textBox1.TabIndex = 1;
			// 
			// panelCarriers
			// 
			this.panelCarriers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelCarriers.Controls.Add(this.lvCarriers);
			this.panelCarriers.Controls.Add(this.toolStrip);
			this.panelCarriers.Location = new System.Drawing.Point(6, 55);
			this.panelCarriers.Name = "panelCarriers";
			this.panelCarriers.Size = new System.Drawing.Size(492, 279);
			this.panelCarriers.TabIndex = 6;
			// 
			// lvCarriers
			// 
			this.lvCarriers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colFileName,
            this.colIsConfigured});
			this.lvCarriers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvCarriers.FullRowSelect = true;
			this.lvCarriers.HideSelection = false;
			this.lvCarriers.Location = new System.Drawing.Point(0, 25);
			this.lvCarriers.Name = "lvCarriers";
			this.lvCarriers.Size = new System.Drawing.Size(490, 252);
			this.lvCarriers.TabIndex = 4;
			this.lvCarriers.UseCompatibleStateImageBehavior = false;
			this.lvCarriers.View = System.Windows.Forms.View.Details;
			this.lvCarriers.SelectedIndexChanged += new System.EventHandler(this.lvCarriers_SelectedIndexChanged);
			// 
			// colFileName
			// 
			this.colFileName.Text = "Carrier file";
			this.colFileName.Width = 300;
			// 
			// colIsConfigured
			// 
			this.colIsConfigured.Text = "Already configured";
			this.colIsConfigured.Width = 100;
			// 
			// toolStrip
			// 
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAdd,
            this.tsbOpen,
            this.tsbRemove});
			this.toolStrip.Location = new System.Drawing.Point(0, 0);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size(490, 25);
			this.toolStrip.TabIndex = 3;
			this.toolStrip.Text = "toolStrip1";
			// 
			// tsbAdd
			// 
			this.tsbAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbAdd.Image = ((System.Drawing.Image)(resources.GetObject("tsbAdd.Image")));
			this.tsbAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbAdd.Name = "tsbAdd";
			this.tsbAdd.Size = new System.Drawing.Size(23, 22);
			this.tsbAdd.ToolTipText = "Hinzufügen";
			this.tsbAdd.Click += new System.EventHandler(this.tsbAdd_Click);
			// 
			// tsbOpen
			// 
			this.tsbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbOpen.Enabled = false;
			this.tsbOpen.Image = ((System.Drawing.Image)(resources.GetObject("tsbOpen.Image")));
			this.tsbOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbOpen.Name = "tsbOpen";
			this.tsbOpen.Size = new System.Drawing.Size(23, 22);
			this.tsbOpen.ToolTipText = "Bearbeiten";
			this.tsbOpen.Click += new System.EventHandler(this.tsbOpen_Click);
			// 
			// tsbRemove
			// 
			this.tsbRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbRemove.Image = ((System.Drawing.Image)(resources.GetObject("tsbRemove.Image")));
			this.tsbRemove.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbRemove.Name = "tsbRemove";
			this.tsbRemove.Size = new System.Drawing.Size(23, 22);
			this.tsbRemove.ToolTipText = "Entfernen";
			this.tsbRemove.Click += new System.EventHandler(this.tsbRemove_Click);
			// 
			// panelNavigation
			// 
			this.panelNavigation.BackColor = System.Drawing.Color.Transparent;
			this.panelNavigation.Controls.Add(this.btnForward);
			this.panelNavigation.Controls.Add(this.btnBack);
			this.panelNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelNavigation.Location = new System.Drawing.Point(0, 405);
			this.panelNavigation.Name = "panelNavigation";
			this.panelNavigation.Size = new System.Drawing.Size(600, 27);
			this.panelNavigation.TabIndex = 9;
			// 
			// btnForward
			// 
			this.btnForward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnForward.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnForward.Location = new System.Drawing.Point(502, 4);
			this.btnForward.Name = "btnForward";
			this.btnForward.Size = new System.Drawing.Size(75, 23);
			this.btnForward.TabIndex = 3;
			this.btnForward.Text = "Continue >>";
			this.btnForward.UseVisualStyleBackColor = false;
			this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
			// 
			// btnBack
			// 
			this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBack.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnBack.Location = new System.Drawing.Point(421, 4);
			this.btnBack.Name = "btnBack";
			this.btnBack.Size = new System.Drawing.Size(75, 23);
			this.btnBack.TabIndex = 2;
			this.btnBack.Text = "<< Back";
			this.btnBack.UseVisualStyleBackColor = false;
			this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
			// 
			// WaveExtractForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = global::SteganoDotNet.Dispatcher.Properties.Resources.kachel;
			this.ClientSize = new System.Drawing.Size(600, 432);
			this.Controls.Add(this.panelNavigation);
			this.Controls.Add(this.panelCarriers);
			this.Controls.Add(this.grpMessage);
			this.Name = "WaveExtractForm";
			this.Text = "Prepare audio files for frequency analysis";
			this.Load += new System.EventHandler(this.WaveExtractForm_Load);
			this.grpMessage.ResumeLayout(false);
			this.grpMessage.PerformLayout();
			this.panelCarriers.ResumeLayout(false);
			this.panelCarriers.PerformLayout();
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.panelNavigation.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox grpMessage;
		private System.Windows.Forms.Panel panelCarriers;
		private System.Windows.Forms.TextBox txtMessage;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton rdoMessageFile;
		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripButton tsbAdd;
		private System.Windows.Forms.ToolStripButton tsbRemove;
		private System.Windows.Forms.ListView lvCarriers;
		private System.Windows.Forms.ColumnHeader colFileName;
		private System.Windows.Forms.ToolStripButton tsbOpen;
		private System.Windows.Forms.Panel panelNavigation;
		private System.Windows.Forms.Button btnForward;
		private System.Windows.Forms.Button btnBack;
		private System.Windows.Forms.ColumnHeader colIsConfigured;
	}
}