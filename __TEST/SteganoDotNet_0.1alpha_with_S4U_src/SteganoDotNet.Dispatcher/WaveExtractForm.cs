/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Action;
using SteganoDotNet.UserControls;

namespace SteganoDotNet.Dispatcher {
	public partial class WaveExtractForm : Form
	{
		internal bool AllowAddRemove
		{
			get
			{
				return tsbAdd.Visible;
			}
			set
			{
				tsbAdd.Visible = value;
				tsbRemove.Visible = value;
			}
		}

		public WaveExtractForm()
		{
			InitializeComponent();
		}

		internal ListViewItem AddCarrier(string carrierFileName)
		{
			ListViewItem item = new ListViewItem(carrierFileName);
			item.SubItems.Add("nein");
			item.ForeColor = Color.Red;
			lvCarriers.Items.Add(item);
			return item;
		}

		private void tsbAdd_Click(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = FileType.Current.FileDialogFilter;
			dlg.Multiselect = true;
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				if (dlg.FileNames.Length == 1)
				{	//only one file selected - open configuration dialog
					ListViewItem viewItem = AddCarrier(dlg.FileName);
					OpenCarrier(viewItem);
				}
				else
				{	//add all selected files
					foreach (string fileName in dlg.FileNames)
					{
						AddCarrier(fileName);
					}
				}
			}
		}

		private void tsbRemove_Click(object sender, EventArgs e)
		{
			for (int n = lvCarriers.SelectedItems.Count-1; n > -1; n--)
			{
				lvCarriers.SelectedItems[n].Remove();
			}
		}

		private void Extract()
		{
			Collection<FileUtility> fileUtilities = new Collection<FileUtility>();
			bool canStart = true;

			foreach (ListViewItem viewItem in lvCarriers.Items)
			{
				FileUtility fileUtility = viewItem.Tag as FileUtility;
				if (fileUtility == null)
				{
					MessageBox.Show(this, "Bitte kofigurieren Sie zuerst alle Tracks.", "Extrahieren", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					canStart = false;
					break;
				}else{
					fileUtilities.Add(fileUtility);
				}
			}

			if (canStart)
			{

				Stream message = new MemoryStream();
				Stream messagePart;
				foreach (FileUtility fileUtility in fileUtilities)
				{
					messagePart = fileUtility.Extract(null);
					messagePart.Seek(0, SeekOrigin.Begin);
					byte[] buffer = new byte[messagePart.Length];
					messagePart.Read(buffer, 0, (int)messagePart.Length);
					message.Write(buffer, 0, buffer.Length);
					messagePart.Close();
				}

				message.Seek(0, SeekOrigin.Begin);
				StreamReader reader = new StreamReader(message);
				string messageText = reader.ReadToEnd();
				reader.Close();

				txtMessage.Text = messageText;
				btnForward.Enabled = false;
			}
		}

		private void tsbOpen_Click(object sender, EventArgs e)
		{
			OpenCarrier( lvCarriers.SelectedItems[0] );
		}

		private void OpenCarrier(ListViewItem viewItem)
		{
			this.Cursor = Cursors.WaitCursor;
			try
			{
				TapeWaveFileUtility utility = viewItem.Tag as TapeWaveFileUtility;
				if (utility == null)
				{
					utility = new TapeWaveFileUtility(viewItem.Text);
					AddConfigurationEventHandlers(utility);
				}

				TapeForm tapeForm = new TapeForm();
				tapeForm.Open(utility.WaveSound);
				tapeForm.SoxPath = utility.SoxPath;
				tapeForm.AllowOpen = false;
				tapeForm.DisplayHideSettings = false;
				tapeForm.Frequency = utility.Frequency;
				tapeForm.Volume = utility.Volume;
				tapeForm.StartTagOffset = utility.StartTagOffset;
				if (tapeForm.ShowDialog(this) == DialogResult.OK)
				{
					utility.Frequency = tapeForm.Frequency;
					utility.Volume = tapeForm.Volume;
					utility.Seconds = tapeForm.FoundSeconds;
					utility.StartTagOffset = tapeForm.StartTagOffset;
					viewItem.Tag = utility;
					viewItem.ForeColor = Color.Black;
					viewItem.SubItems[1].Text = "ja";
				}
			}
			finally
			{
				this.Cursor = Cursors.Default;
			}
		}

		private void AddConfigurationEventHandlers(FileUtility fileUtility)
		{
			TapeWaveFileUtility tapeWaveFileUtility = fileUtility as TapeWaveFileUtility;
			if (tapeWaveFileUtility != null)
			{
				tapeWaveFileUtility.SoundExchangeNotFound += new EventHandler(TapeWaveUtility_SoundExchangeNotFound);
			}
		}

		private void ShowCarriers()
		{
			panelCarriers.Show();
			panelCarriers.Dock = DockStyle.Fill;

			grpMessage.Hide();

			btnForward.Enabled = true;
			btnBack.Enabled = true;
		}

		private void ShowMessage()
		{
			grpMessage.Show();
			grpMessage.Dock = DockStyle.Fill;

			panelCarriers.Hide();

			btnForward.Enabled = true;
			btnBack.Enabled = true;
		}

		private void TapeWaveUtility_SoundExchangeNotFound(object sender, EventArgs e)
		{
			//ask user for SoX path
			FindSoxForm dlg = new FindSoxForm();
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				TapeWaveFileUtility utility = (TapeWaveFileUtility)sender;
				utility.SoxPath = dlg.SoxPath;
			}
		}

		private void lvCarriers_SelectedIndexChanged(object sender, EventArgs e)
		{
			tsbOpen.Enabled = (lvCarriers.SelectedItems.Count > 0 && lvCarriers.SelectedItems[0].Text.EndsWith(".wav"));
		}

		private void WaveExtractForm_Load(object sender, EventArgs e)
		{
			ShowCarriers();
		}

		private void btnForward_Click(object sender, EventArgs e)
		{
			if (panelCarriers.Visible)
			{
				ShowMessage();
			}
			else if (grpMessage.Visible)
			{
				Extract();
			}
		}

		private void btnBack_Click(object sender, EventArgs e)
		{
			if (grpMessage.Visible)
			{
				ShowCarriers();
			}
		}
	}
}