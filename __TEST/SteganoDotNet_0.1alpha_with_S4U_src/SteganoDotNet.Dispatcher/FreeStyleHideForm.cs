/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Action;
using SteganoDotNet.UserControls;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.Dispatcher {
	public partial class FreeStyleHideForm : Form {

		bool handleFileUtilitySettingsChanged = true;
		
		internal bool AllowAddRemove
		{
			get
			{
				return lvCarriers.AllowAddRemove;
			}
			set
			{
				lvCarriers.AllowAddRemove = value;
			}
		}

		public FreeStyleHideForm()
		{
			InitializeComponent();
		}

		internal void AddCarrier(FileUtility carrier)
		{
			lvCarriers.AddItem(carrier);
			AddConfigurationEventHandlers(carrier);
		}

		private void OpenCarrier(FileUtility fileUtility)
		{
			CarrierConfiguration control = (CarrierConfiguration)fileUtility.CarrierConfigurationControl;
			control.ExpectedMessageLength = sbMessage.Stream.Length;

			fileUtility.SettingsChanged += new EventHandler(FileUtility_SettingsChanged);
			control.CancelSettings += new EventHandler(CarrierConfiguration_CancelSettings);

			control.Dock = DockStyle.Fill;
			panelCarriers.Controls.Add(control);
			control.BringToFront();

			SetButtonState();
		}

		private void SetButtonState()
		{
			btnHide.Enabled = panelCarriers.Controls.Count == 1
				&& lvCarriers.OverallPercent >= 100
				&& sbKey.HasStream
				&& sbMessage.HasStream;
		}

		private void AddConfigurationEventHandlers(FileUtility fileUtility)
		{
			TapeWaveFileUtility tapeWaveFileUtility = fileUtility as TapeWaveFileUtility;
			if (tapeWaveFileUtility != null)
			{
				tapeWaveFileUtility.OpenTapeForm += new EventHandler(TapeWaveUtility_OpenTapeForm);
				tapeWaveFileUtility.SoundExchangeNotFound += new EventHandler(TapeWaveUtility_SoundExchangeNotFound);
			}
		}

		private void lvCarriers_AddCarrier(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = FileType.Current.FileDialogFilter;
			if (dlg.ShowDialog() == DialogResult.OK) {
				FileType fileType = FileType.Current.GetFileType(dlg.FileName);
				FileUtility fileUtility = fileType.CreateUtility(dlg.FileName);
				lvCarriers.AddItem(fileUtility).Selected = true;

				AddConfigurationEventHandlers(fileUtility);

				OpenCarrier(fileUtility);
			}
		}

		private void lvCarriers_OpenCarrier(object sender, EventArgs e)
		{
			FileUtility fileUtility = lvCarriers.SelectedCarrier;
			if (fileUtility != null)
			{
				OpenCarrier(fileUtility);
			}
		}

		private void FileUtility_SettingsChanged(object sender, EventArgs e)
		{
			if (handleFileUtilitySettingsChanged)
			{
				FileUtility fileUtility = (FileUtility)sender;

				if (fileUtility.PercentOfMessage == 0 && fileUtility.CountBytesToHide > 0)
				{	//calculate percentage
					long length = sbMessage.Stream.Length;
					if (length > 0)
					{
						handleFileUtilitySettingsChanged = false;
						fileUtility.PercentOfMessage = 100 * fileUtility.CountBytesToHide / length;
						handleFileUtilitySettingsChanged = true;
					}
				}

				this.panelCarriers.Controls.Remove((CarrierConfiguration)fileUtility.CarrierConfigurationControl);
				SetButtonState();
			}
		}

		private void CarrierConfiguration_CancelSettings(object sender, EventArgs e)
		{
			panelCarriers.Controls.Remove((CarrierConfiguration)sender);
			lvCarriers.RemoveSelectedItem();
			SetButtonState();
		}

		private void TapeWaveUtility_OpenTapeForm(object sender, EventArgs e)
		{
			TapeWaveFileUtility utility = (TapeWaveFileUtility)sender;
			TapeForm tapeForm = new TapeForm();
			tapeForm.Open(utility.WaveSound);
			tapeForm.SoxPath = utility.SoxPath;
			tapeForm.AllowOpen = false;
			tapeForm.DisplayHideSettings = true;
			tapeForm.Frequency = utility.Frequency;
			tapeForm.Volume = utility.Volume;

			if (utility.PercentOfMessage == 100)
			{
				tapeForm.ExpectedMessagePart = sbMessage.Stream;
			}
			else
			{
				tapeForm.ExpectedMessagePartLength = (int)(100 * (float)sbMessage.Stream.Length / (float)utility.PercentOfMessage);
			}
			
			if (tapeForm.ShowDialog(this) == DialogResult.OK)
			{
				utility.Frequency = tapeForm.Frequency;
				utility.Volume = tapeForm.Volume;
				utility.BeepLength = tapeForm.BeepLength;
			}
		}

		private void TapeWaveUtility_SoundExchangeNotFound(object sender, EventArgs e)
		{
			//ask user for SoX path
			FindSoxForm dlg = new FindSoxForm();
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				TapeWaveFileUtility utility = (TapeWaveFileUtility)sender;
				utility.SoxPath = dlg.SoxPath;
			}
		}

		private void btnHide_Click(object sender, EventArgs e)
		{
			FileUtility fileUtility;
			Stream messageWithCount = null;
			Hashtable utilitiesToMessageParts = new Hashtable();
			int blockLength;
			long remainingLength;
			long messageLength;
	
			if (lvCarriers.Items.Count == 0)
			{
				MessageBox.Show(this, "Bitte w�hlen Sie mindestens eine Tr�gerdatei aus.");
			}
			else
			{
				this.Cursor = Cursors.WaitCursor;
				try
				{
					//create streams for message and key
					Stream message = sbMessage.Stream;
					Stream key = sbKey.Stream;
					messageLength = message.Length;

					string exception = string.Empty;
					if (lvCarriers.OverallPercent != 100)
					{
						exception = "Please distribute 100% of the message to the carriers.";
					}
					else if ((!lvCarriers.HasTapeWaveUtilities) && (message.Length > lvCarriers.OverallCapacity))
					{
						exception = "The overall capacity of the carriers is too low.";
					}
					else
					{
						foreach (ListViewItem viewItem in lvCarriers.Items)
						{
							fileUtility = (FileUtility)viewItem.Tag;
							blockLength = (int)Math.Ceiling(fileUtility.PercentOfMessage * messageLength / 100);
							remainingLength = message.Length - message.Position;

							fileUtility.CountBytesToHide = (remainingLength < blockLength) ? (int)remainingLength : blockLength;
							messageWithCount = fileUtility.GetMessagePart(message);

							exception = fileUtility.CheckMessageAndKey(messageWithCount, key);
							if (exception.Length > 0)
							{
								break;
							}
							else
							{
								utilitiesToMessageParts.Add(fileUtility, messageWithCount);
							}
						}
					}

					if (exception.Length > 0)
					{	//show message and keep this dialog open
						MessageBox.Show(this, exception);
					}
					else
					{	//process carriers
						foreach (object objFileUtility in utilitiesToMessageParts.Keys)
						{
							fileUtility = (FileUtility)objFileUtility;
							messageWithCount = (Stream)utilitiesToMessageParts[objFileUtility];
							fileUtility.Hide(messageWithCount, key);
							messageWithCount.Close();
						}

						MessageBox.Show(this, "The message has been hidden. Please save the carriers or send them via SMTP or FTP.");
					}

				}
				catch (Exception ex)
				{
					MessageBox.Show(this, ex.ToString());
				}
				finally
				{
					this.Cursor = Cursors.Default;
				}
			}
		}

		private void sbKey_StreamChanged(object sender, EventArgs e)
		{
			SetButtonState();
		}

		private void sbMessage_StreamChanged(object sender, EventArgs e)
		{
			SetButtonState();
		}

		private void lvCarriers_SettingsChanged(object sender, EventArgs e)
		{
			SetButtonState();
		}

        private void lvCarriers_PasteBitmap(object sender, EventArgs e)
        {
            if (Clipboard.ContainsImage())
            {
                FileUtility utility;
                Image image = Clipboard.GetImage();
                if (image.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    utility = new PaletteFileUtility(image);
                }
                else
                {
                    utility = new BitmapFileUtility(image);
                }
                AddCarrier(utility);
				AddConfigurationEventHandlers(utility);
				OpenCarrier(utility);
            }
		}

		private void lvCarriers_RecordWave(object sender, EventArgs e)
		{
			// TODO: WAve recording
			MessageBox.Show("Not Implemented");
		}
	}
}