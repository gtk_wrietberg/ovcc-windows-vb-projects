namespace SteganoDotNet.Dispatcher {
	partial class WaveHideForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WaveHideForm));
			this.grpMessage = new System.Windows.Forms.GroupBox();
			this.sbMessage = new SteganoDotNet.UserControls.StreamBox();
			this.panelCarriers = new System.Windows.Forms.Panel();
			this.lvCarriers = new SteganoDotNet.Dispatcher.CarrierListView();
			this.panelMessage = new System.Windows.Forms.Panel();
			this.panelNavigation = new System.Windows.Forms.Panel();
			this.btnForward = new System.Windows.Forms.Button();
			this.btnBack = new System.Windows.Forms.Button();
			this.panelPlayer = new System.Windows.Forms.Panel();
			this.lvResults = new System.Windows.Forms.ListView();
			this.colResultFileName = new System.Windows.Forms.ColumnHeader();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.tsbSave = new System.Windows.Forms.ToolStripButton();
			this.tsbPlay = new System.Windows.Forms.ToolStripButton();
			this.tsbStop = new System.Windows.Forms.ToolStripButton();
			this.grpMessage.SuspendLayout();
			this.panelCarriers.SuspendLayout();
			this.panelMessage.SuspendLayout();
			this.panelNavigation.SuspendLayout();
			this.panelPlayer.SuspendLayout();
			this.toolStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpMessage
			// 
			this.grpMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.grpMessage.Controls.Add(this.sbMessage);
			this.grpMessage.Location = new System.Drawing.Point(16, 17);
			this.grpMessage.Name = "grpMessage";
			this.grpMessage.Size = new System.Drawing.Size(492, 135);
			this.grpMessage.TabIndex = 4;
			this.grpMessage.TabStop = false;
			this.grpMessage.Text = "Message";
			// 
			// sbMessage
			// 
			this.sbMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.sbMessage.DialogType = SteganoDotNet.UserControls.FileSelectBox.FileDialogType.Open;
			this.sbMessage.FileName = "";
			this.sbMessage.LabelFile = "Read from file";
			this.sbMessage.LabelText = "Enter text";
			this.sbMessage.Location = new System.Drawing.Point(6, 19);
			this.sbMessage.MessageText = "";
			this.sbMessage.Name = "sbMessage";
			this.sbMessage.Size = new System.Drawing.Size(467, 104);
			this.sbMessage.TabIndex = 2;
			this.sbMessage.StreamChanged += new System.EventHandler(this.sbMessage_StreamChanged);
			// 
			// panelCarriers
			// 
			this.panelCarriers.Controls.Add(this.lvCarriers);
			this.panelCarriers.Location = new System.Drawing.Point(9, 12);
			this.panelCarriers.Name = "panelCarriers";
			this.panelCarriers.Size = new System.Drawing.Size(546, 279);
			this.panelCarriers.TabIndex = 6;
			// 
			// lvCarriers
			// 
			this.lvCarriers.AllowAddRemove = true;
			this.lvCarriers.AllowPasteBitmap = false;
			this.lvCarriers.AllowRecordWave = true;
			this.lvCarriers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lvCarriers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvCarriers.Location = new System.Drawing.Point(0, 0);
			this.lvCarriers.Name = "lvCarriers";
			this.lvCarriers.Size = new System.Drawing.Size(546, 279);
			this.lvCarriers.TabIndex = 4;
			this.lvCarriers.AddCarrier += new System.EventHandler(this.lvCarriers_AddCarrier);
			this.lvCarriers.SettingsChanged += new System.EventHandler(this.lvCarriers_SettingsChanged);
			this.lvCarriers.OpenCarrier += new System.EventHandler(this.lvCarriers_OpenCarrier);
			this.lvCarriers.RecordWave += new System.EventHandler(this.lvCarriers_RecordWave);
			// 
			// panelMessage
			// 
			this.panelMessage.BackColor = System.Drawing.Color.Transparent;
			this.panelMessage.Controls.Add(this.grpMessage);
			this.panelMessage.Location = new System.Drawing.Point(0, 64);
			this.panelMessage.Name = "panelMessage";
			this.panelMessage.Size = new System.Drawing.Size(546, 172);
			this.panelMessage.TabIndex = 7;
			// 
			// panelNavigation
			// 
			this.panelNavigation.BackColor = System.Drawing.Color.Transparent;
			this.panelNavigation.Controls.Add(this.btnForward);
			this.panelNavigation.Controls.Add(this.btnBack);
			this.panelNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelNavigation.Location = new System.Drawing.Point(0, 376);
			this.panelNavigation.Name = "panelNavigation";
			this.panelNavigation.Size = new System.Drawing.Size(604, 27);
			this.panelNavigation.TabIndex = 8;
			// 
			// btnForward
			// 
			this.btnForward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnForward.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnForward.Location = new System.Drawing.Point(502, 4);
			this.btnForward.Name = "btnForward";
			this.btnForward.Size = new System.Drawing.Size(75, 23);
			this.btnForward.TabIndex = 3;
			this.btnForward.Text = "Continue >>";
			this.btnForward.UseVisualStyleBackColor = false;
			this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
			// 
			// btnBack
			// 
			this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBack.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnBack.Location = new System.Drawing.Point(421, 4);
			this.btnBack.Name = "btnBack";
			this.btnBack.Size = new System.Drawing.Size(75, 23);
			this.btnBack.TabIndex = 2;
			this.btnBack.Text = "<< Back";
			this.btnBack.UseVisualStyleBackColor = false;
			this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
			// 
			// panelPlayer
			// 
			this.panelPlayer.Controls.Add(this.lvResults);
			this.panelPlayer.Controls.Add(this.toolStrip);
			this.panelPlayer.Location = new System.Drawing.Point(41, 297);
			this.panelPlayer.Name = "panelPlayer";
			this.panelPlayer.Size = new System.Drawing.Size(406, 122);
			this.panelPlayer.TabIndex = 9;
			// 
			// lvResults
			// 
			this.lvResults.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colResultFileName});
			this.lvResults.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lvResults.FullRowSelect = true;
			this.lvResults.HideSelection = false;
			this.lvResults.Location = new System.Drawing.Point(0, 25);
			this.lvResults.Name = "lvResults";
			this.lvResults.Size = new System.Drawing.Size(406, 97);
			this.lvResults.TabIndex = 0;
			this.lvResults.UseCompatibleStateImageBehavior = false;
			this.lvResults.View = System.Windows.Forms.View.Details;
			// 
			// colResultFileName
			// 
			this.colResultFileName.Text = "Generated wave file";
			this.colResultFileName.Width = 500;
			// 
			// toolStrip
			// 
			this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbSave,
            this.tsbPlay,
            this.tsbStop});
			this.toolStrip.Location = new System.Drawing.Point(0, 0);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Size = new System.Drawing.Size(406, 25);
			this.toolStrip.TabIndex = 14;
			this.toolStrip.Text = "toolStrip1";
			// 
			// tsbSave
			// 
			this.tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbSave.Image = ((System.Drawing.Image)(resources.GetObject("tsbSave.Image")));
			this.tsbSave.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbSave.Name = "tsbSave";
			this.tsbSave.Size = new System.Drawing.Size(23, 22);
			this.tsbSave.ToolTipText = "Save File";
			this.tsbSave.Click += new System.EventHandler(this.tsbSave_Click);
			// 
			// tsbPlay
			// 
			this.tsbPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbPlay.Image = ((System.Drawing.Image)(resources.GetObject("tsbPlay.Image")));
			this.tsbPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbPlay.Name = "tsbPlay";
			this.tsbPlay.Size = new System.Drawing.Size(23, 22);
			this.tsbPlay.Text = "Abspielen";
			this.tsbPlay.Click += new System.EventHandler(this.tsbPlay_Click);
			// 
			// tsbStop
			// 
			this.tsbStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbStop.Image = ((System.Drawing.Image)(resources.GetObject("tsbStop.Image")));
			this.tsbStop.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbStop.Name = "tsbStop";
			this.tsbStop.Size = new System.Drawing.Size(23, 22);
			this.tsbStop.Text = "Stop";
			this.tsbStop.Click += new System.EventHandler(this.tsbStop_Click);
			// 
			// WaveHideForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = global::SteganoDotNet.Dispatcher.Properties.Resources.kachel;
			this.ClientSize = new System.Drawing.Size(604, 403);
			this.Controls.Add(this.panelPlayer);
			this.Controls.Add(this.panelNavigation);
			this.Controls.Add(this.panelMessage);
			this.Controls.Add(this.panelCarriers);
			this.Name = "WaveHideForm";
			this.Text = "Prepare audio files for frequency steganography";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WaveHideForm_FormClosing);
			this.Load += new System.EventHandler(this.WaveHideForm_Load);
			this.grpMessage.ResumeLayout(false);
			this.panelCarriers.ResumeLayout(false);
			this.panelMessage.ResumeLayout(false);
			this.panelNavigation.ResumeLayout(false);
			this.panelPlayer.ResumeLayout(false);
			this.panelPlayer.PerformLayout();
			this.toolStrip.ResumeLayout(false);
			this.toolStrip.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox grpMessage;
		private SteganoDotNet.UserControls.StreamBox sbMessage;
		private SteganoDotNet.Dispatcher.CarrierListView lvCarriers;
		private System.Windows.Forms.Panel panelCarriers;
		private System.Windows.Forms.Panel panelMessage;
		private System.Windows.Forms.Panel panelNavigation;
		private System.Windows.Forms.Panel panelPlayer;
		private System.Windows.Forms.Button btnBack;
		private System.Windows.Forms.Button btnForward;
		private System.Windows.Forms.ListView lvResults;
		private System.Windows.Forms.ColumnHeader colResultFileName;
		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.ToolStripButton tsbPlay;
		private System.Windows.Forms.ToolStripButton tsbStop;
		private System.Windows.Forms.ToolStripButton tsbSave;
	}
}