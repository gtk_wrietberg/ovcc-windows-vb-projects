/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.UserControls {

	/// <summary>Configuration UI for SteganoDotNet.Action.WaveFileUtility.</summary>
	public partial class WaveConfiguration : CarrierConfiguration, IWaveConfiguration
	{
		/// <summary>Sets or returns the percentage of the message to store in the carrier.</summary>
		public float PercentOfMessage
		{
			get
			{
				return (float)numPercentOfMessage.Value;
			}
			set
			{
				numPercentOfMessage.Value = (decimal)value;
			}
		}

		/// <summary>Sets or returns the percentage of the carrier to be covered by random noise.</summary>
		public int NoisePercent
		{
			get
			{
				return tbNoisePercent.Value;
			}
			set
			{
				tbNoisePercent.Value = value;
			}
		}

		/// <summary>Returns or sets the amount of data that will be hidden in the carrier file, in percent of the message.</summary>
		public float CountBytesToHidePercent
		{
			get
			{
				return (float)numPercentOfMessage.Value;
			}
			set
			{
				numPercentOfMessage.Value = (decimal)value;
			}
		}

		/// <summary>Returns or sets the number of bits that will be stored in each carrier unit.</summary>
		public int CountUsedBitsPerUnit
		{
			get
			{
				return (int)numBitsPerUnit.Value;
			}
			set
			{
				numBitsPerUnit.Value = value;
			}
		}
		
		/// <summary>Constructor.</summary>
		public WaveConfiguration()
		{
			InitializeComponent();
		}

		/// <summary>Validates the configuration.</summary>
		public override void Accept(CancelEventArgs eventArgs)
		{
			base.Accept(eventArgs);

			errorProvider.SetError(lblPercentOfMessage2, string.Empty);
			if (numPercentOfMessage.Value == 0)
			{
				errorProvider.SetError(lblPercentOfMessage2, "Die Tr�ger-Datei ist nutzlos, wenn sie keinen Teil der Nachricht versteckt.");
				eventArgs.Cancel = true;
			}
		}

		private void tbNoisePercent_ValueChanged(object sender, EventArgs e)
		{
			if((int)numNoisePercent.Value != tbNoisePercent.Value)
			{
				numNoisePercent.Value = tbNoisePercent.Value;
			}
		}

		private void numNoisePercent_ValueChanged(object sender, EventArgs e)
		{
			if(tbNoisePercent.Value != (int)numNoisePercent.Value)
			{
				tbNoisePercent.Value = (int)numNoisePercent.Value;
			}
		}
	}
}
