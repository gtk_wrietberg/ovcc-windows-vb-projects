namespace SteganoDotNet.UserControls {
	partial class TapeWaveConfiguration {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.numPercentOfMessage = new System.Windows.Forms.NumericUpDown();
			this.lblPercentOfMessage = new System.Windows.Forms.Label();
			this.lblFrequency = new System.Windows.Forms.Label();
			this.lblHz = new System.Windows.Forms.Label();
			this.numFrequency = new System.Windows.Forms.NumericUpDown();
			this.lblVolume = new System.Windows.Forms.Label();
			this.numVolume = new System.Windows.Forms.NumericUpDown();
			this.lblBeepLength = new System.Windows.Forms.Label();
			this.numBeepLength = new System.Windows.Forms.NumericUpDown();
			this.lblSecond = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.numStartTagOffset = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numFrequency)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numVolume)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numBeepLength)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numStartTagOffset)).BeginInit();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(192, 73);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(181, 16);
			this.label2.TabIndex = 21;
			this.label2.Text = "% of the message in this carrier";
			// 
			// numPercentOfMessage
			// 
			this.numPercentOfMessage.DecimalPlaces = 1;
			this.numPercentOfMessage.Value = 100;
			this.numPercentOfMessage.Location = new System.Drawing.Point(138, 69);
			this.numPercentOfMessage.Name = "numPercentOfMessage";
			this.numPercentOfMessage.Size = new System.Drawing.Size(50, 20);
			this.numPercentOfMessage.TabIndex = 20;
			// 
			// lblPercentOfMessage
			// 
			this.lblPercentOfMessage.AutoSize = true;
			this.lblPercentOfMessage.Location = new System.Drawing.Point(50, 73);
			this.lblPercentOfMessage.Name = "lblPercentOfMessage";
			this.lblPercentOfMessage.Size = new System.Drawing.Size(29, 13);
			this.lblPercentOfMessage.TabIndex = 24;
			this.lblPercentOfMessage.Text = "Hide";
			// 
			// lblFrequency
			// 
			this.lblFrequency.AutoSize = true;
			this.lblFrequency.Location = new System.Drawing.Point(50, 102);
			this.lblFrequency.Name = "lblFrequency";
			this.lblFrequency.Size = new System.Drawing.Size(57, 13);
			this.lblFrequency.TabIndex = 27;
			this.lblFrequency.Text = "Frequency";
			// 
			// lblHz
			// 
			this.lblHz.Location = new System.Drawing.Point(192, 102);
			this.lblHz.Name = "lblHz";
			this.lblHz.Size = new System.Drawing.Size(27, 13);
			this.lblHz.TabIndex = 26;
			this.lblHz.Text = "Hz";
			// 
			// numFrequency
			// 
			this.numFrequency.Enabled = false;
			this.numFrequency.Location = new System.Drawing.Point(138, 98);
			this.numFrequency.Maximum = new decimal(new int[] {
            4000,
            0,
            0,
            0});
			this.numFrequency.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numFrequency.Name = "numFrequency";
			this.numFrequency.Size = new System.Drawing.Size(50, 20);
			this.numFrequency.TabIndex = 25;
			this.numFrequency.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// lblVolume
			// 
			this.lblVolume.AutoSize = true;
			this.lblVolume.Location = new System.Drawing.Point(50, 126);
			this.lblVolume.Name = "lblVolume";
			this.lblVolume.Size = new System.Drawing.Size(53, 13);
			this.lblVolume.TabIndex = 30;
			this.lblVolume.Text = "Amplitude";
			// 
			// numVolume
			// 
			this.numVolume.Enabled = false;
			this.numVolume.Location = new System.Drawing.Point(138, 124);
			this.numVolume.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.numVolume.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numVolume.Name = "numVolume";
			this.numVolume.Size = new System.Drawing.Size(50, 20);
			this.numVolume.TabIndex = 28;
			this.numVolume.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// lblBeepLength
			// 
			this.lblBeepLength.AutoSize = true;
			this.lblBeepLength.Location = new System.Drawing.Point(50, 153);
			this.lblBeepLength.Name = "lblBeepLength";
			this.lblBeepLength.Size = new System.Drawing.Size(78, 13);
			this.lblBeepLength.TabIndex = 33;
			this.lblBeepLength.Text = "Beep length 1/";
			// 
			// numBeepLength
			// 
			this.numBeepLength.Enabled = false;
			this.numBeepLength.Location = new System.Drawing.Point(138, 150);
			this.numBeepLength.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.numBeepLength.Name = "numBeepLength";
			this.numBeepLength.Size = new System.Drawing.Size(50, 20);
			this.numBeepLength.TabIndex = 32;
			this.numBeepLength.Value = new decimal(new int[] {
            32,
            0,
            0,
            0});
			// 
			// lblSecond
			// 
			this.lblSecond.AutoSize = true;
			this.lblSecond.Location = new System.Drawing.Point(194, 153);
			this.lblSecond.Name = "lblSecond";
			this.lblSecond.Size = new System.Drawing.Size(50, 13);
			this.lblSecond.TabIndex = 34;
			this.lblSecond.Text = "Sekunde";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(20, 183);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(108, 13);
			this.label1.TabIndex = 36;
			this.label1.Text = "Offset for start marker";
			// 
			// numStartTagOffset
			// 
			this.numStartTagOffset.Enabled = false;
			this.numStartTagOffset.Location = new System.Drawing.Point(139, 176);
			this.numStartTagOffset.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			this.numStartTagOffset.Name = "numStartTagOffset";
			this.numStartTagOffset.Size = new System.Drawing.Size(50, 20);
			this.numStartTagOffset.TabIndex = 35;
			this.numStartTagOffset.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
			// 
			// TapeWaveConfiguration
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.numStartTagOffset);
			this.Controls.Add(this.lblSecond);
			this.Controls.Add(this.lblBeepLength);
			this.Controls.Add(this.numBeepLength);
			this.Controls.Add(this.lblVolume);
			this.Controls.Add(this.numVolume);
			this.Controls.Add(this.lblFrequency);
			this.Controls.Add(this.lblHz);
			this.Controls.Add(this.numFrequency);
			this.Controls.Add(this.lblPercentOfMessage);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.numPercentOfMessage);
			this.Name = "TapeWaveConfiguration";
			this.Size = new System.Drawing.Size(467, 212);
			this.ParentChanged += new System.EventHandler(this.TapeWaveConfiguration_ParentChanged);
			this.Controls.SetChildIndex(this.numPercentOfMessage, 0);
			this.Controls.SetChildIndex(this.label2, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage, 0);
			this.Controls.SetChildIndex(this.numFrequency, 0);
			this.Controls.SetChildIndex(this.lblHz, 0);
			this.Controls.SetChildIndex(this.lblFrequency, 0);
			this.Controls.SetChildIndex(this.numVolume, 0);
			this.Controls.SetChildIndex(this.lblVolume, 0);
			this.Controls.SetChildIndex(this.numBeepLength, 0);
			this.Controls.SetChildIndex(this.lblBeepLength, 0);
			this.Controls.SetChildIndex(this.lblSecond, 0);
			this.Controls.SetChildIndex(this.numStartTagOffset, 0);
			this.Controls.SetChildIndex(this.label1, 0);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numFrequency)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numVolume)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numBeepLength)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numStartTagOffset)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numPercentOfMessage;
		private System.Windows.Forms.Label lblPercentOfMessage;
		private System.Windows.Forms.Label lblFrequency;
		private System.Windows.Forms.Label lblHz;
		private System.Windows.Forms.NumericUpDown numFrequency;
		private System.Windows.Forms.Label lblVolume;
		private System.Windows.Forms.NumericUpDown numVolume;
		private System.Windows.Forms.Label lblBeepLength;
		private System.Windows.Forms.NumericUpDown numBeepLength;
		private System.Windows.Forms.Label lblSecond;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numStartTagOffset;
	}
}
