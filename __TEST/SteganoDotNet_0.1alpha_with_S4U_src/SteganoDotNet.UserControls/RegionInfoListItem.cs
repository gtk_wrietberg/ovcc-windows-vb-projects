﻿/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#region Using directives

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Interfaces;

#endregion

namespace SteganoDotNet.UserControls
{
	/// <summary>Represents an item in the image regions list.</summary>
	public partial class RegionInfoListItem : UserControl
    {
        private System.Windows.Forms.NumericUpDown numCountUsedBitsPerPixel;
        private System.Windows.Forms.NumericUpDown numCapacity;
        private System.Windows.Forms.Label lblCountPixels;
        private System.Windows.Forms.Label lblRegionName;
        private System.Windows.Forms.Label lblPercentPixels;
        private System.Windows.Forms.Button btnDelete;

        private RegionInfo regionInfo;

		/// <summary>Returns the region.</summary>
		public RegionInfo RegionInfo
        {
            get { return regionInfo; }
        }

        private int index = 0;

		/// <summary>Returns the index.</summary>
		public int Index
        {
            get { return index; }
            set { index = value; }
        }

        private bool isUpdating; //the control is updating its fields - don't send Select events

		/// <summary>The item is removed from the list.</summary>
		public event EventHandler Delete;

		/// <summary>The item is selected.</summary>
		public event EventHandler Selected;

		/// <summary>Default constructor, only for VS Forms Designer.</summary>
		public RegionInfoListItem() { }

		/// <summary>Constructor.</summary>
		public RegionInfoListItem(RegionInfo regionInfo)
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            this.regionInfo = regionInfo;

            UpdateContent();
        }

		/// <summary>Updates the list item.</summary>
		public void UpdateContent()
        {
            isUpdating = true;

            lblRegionName.Text = "Region " + (index + 1).ToString();
            lblCountPixels.Text = regionInfo.CountPixels.ToString();
            lblPercentPixels.Text = regionInfo.PercentOfImage.ToString("###.##");
            numCountUsedBitsPerPixel.Value = regionInfo.CountUsedBitsPerPixel;
            UpdateCapacity();

            isUpdating = false;
        }

        private void UpdateCapacity() {
            numCapacity.Maximum = (int)Math.Floor(((decimal)(regionInfo.CountPixels * regionInfo.CountUsedBitsPerPixel)) / 8) - 1;

            if (numCapacity.Maximum == 0) {
                //the region is too small to hide any data
                numCapacity.Minimum = 0;
                numCapacity.Maximum = 0;
                numCapacity.Value = 0;
                numCapacity.Enabled = false;
            } else {
                numCapacity.Enabled = true;
                numCapacity.Minimum = 1;
                if (regionInfo.Capacity <= numCapacity.Maximum) {
                    numCapacity.Value = regionInfo.Capacity;
                } else {
                    numCapacity.Value = numCapacity.Maximum;
                    regionInfo.Capacity = (int)numCapacity.Maximum;
                }
            }
        }

		/// <summary>Deselects the list item.</summary>
		public void DeselectItem()
        {
            this.BackColor = SystemColors.Control;
        }

		/// <summary>Selects the list item.</summary>
		public void SelectItem()
        {
            this.BackColor = Color.White;
        }

        private void OnDelete()
        {
            if (Delete != null)
            {
                Delete(this, new EventArgs());
            }
        }

        private void OnSelected()
        {
            if (!isUpdating) {
                if (Selected != null) {
                    Selected(this, new EventArgs());
                }
                SelectItem();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OnDelete();
        }

        private void Label_Click(object sender, System.EventArgs e)
        {
            OnSelected();
        }

        private void NumCapacityEnter(object sender, System.EventArgs e)
        {
            OnSelected();
        }

        private void NumCapacityValueChanged(object sender, System.EventArgs e)
        {
            regionInfo.Capacity = (int)numCapacity.Value;
            OnSelected();
        }


        private void NumCountUsedBitsPerPixelValueChanged(object sender, System.EventArgs e)
        {
            regionInfo.CountUsedBitsPerPixel = (byte)numCountUsedBitsPerPixel.Value;
            UpdateCapacity();
            OnSelected();
        }

    }
}
