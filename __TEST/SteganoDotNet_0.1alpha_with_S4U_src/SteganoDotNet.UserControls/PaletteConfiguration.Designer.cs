namespace SteganoDotNet.UserControls {
	partial class PaletteConfiguration
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPercentOfMessage2 = new System.Windows.Forms.Label();
			this.numPercentOfMessage = new System.Windows.Forms.NumericUpDown();
			this.lblPercentOfMessage1 = new System.Windows.Forms.Label();
			this.lblMethod = new System.Windows.Forms.Label();
			this.cmbMethod = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).BeginInit();
			this.SuspendLayout();
			// 
			// lblPercentOfMessage2
			// 
			this.lblPercentOfMessage2.Location = new System.Drawing.Point(118, 44);
			this.lblPercentOfMessage2.Name = "lblPercentOfMessage2";
			this.lblPercentOfMessage2.Size = new System.Drawing.Size(151, 13);
			this.lblPercentOfMessage2.TabIndex = 21;
			this.lblPercentOfMessage2.Text = "% of the message in this carrier";
			this.lblPercentOfMessage2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// numPercentOfMessage
			// 
			this.numPercentOfMessage.DecimalPlaces = 1;
			this.numPercentOfMessage.Location = new System.Drawing.Point(64, 40);
			this.numPercentOfMessage.Name = "numPercentOfMessage";
			this.numPercentOfMessage.Size = new System.Drawing.Size(50, 20);
			this.numPercentOfMessage.TabIndex = 20;
			this.numPercentOfMessage.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// lblPercentOfMessage1
			// 
			this.lblPercentOfMessage1.AutoSize = true;
			this.lblPercentOfMessage1.Location = new System.Drawing.Point(22, 44);
			this.lblPercentOfMessage1.Name = "lblPercentOfMessage1";
			this.lblPercentOfMessage1.Size = new System.Drawing.Size(29, 13);
			this.lblPercentOfMessage1.TabIndex = 24;
			this.lblPercentOfMessage1.Text = "Hide";
			this.lblPercentOfMessage1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblMethod
			// 
			this.lblMethod.AutoSize = true;
			this.lblMethod.Location = new System.Drawing.Point(8, 77);
			this.lblMethod.Name = "lblMethod";
			this.lblMethod.Size = new System.Drawing.Size(43, 13);
			this.lblMethod.TabIndex = 25;
			this.lblMethod.Text = "Method";
			this.lblMethod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbMethod
			// 
			this.cmbMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cmbMethod.FormattingEnabled = true;
			this.cmbMethod.Items.AddRange(new object[] {
            "Sort Palette",
            "Stretch Palette",
            "Both"});
			this.cmbMethod.Location = new System.Drawing.Point(64, 74);
			this.cmbMethod.Name = "cmbMethod";
			this.cmbMethod.Size = new System.Drawing.Size(121, 21);
			this.cmbMethod.TabIndex = 26;
			// 
			// PaletteConfiguration
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.cmbMethod);
			this.Controls.Add(this.lblMethod);
			this.Controls.Add(this.lblPercentOfMessage1);
			this.Controls.Add(this.lblPercentOfMessage2);
			this.Controls.Add(this.numPercentOfMessage);
			this.Name = "PaletteConfiguration";
			this.Size = new System.Drawing.Size(247, 108);
			this.Controls.SetChildIndex(this.numPercentOfMessage, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage2, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage1, 0);
			this.Controls.SetChildIndex(this.lblMethod, 0);
			this.Controls.SetChildIndex(this.cmbMethod, 0);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblPercentOfMessage2;
		private System.Windows.Forms.NumericUpDown numPercentOfMessage;
		private System.Windows.Forms.Label lblPercentOfMessage1;
		private System.Windows.Forms.Label lblMethod;
		private System.Windows.Forms.ComboBox cmbMethod;
	}
}
