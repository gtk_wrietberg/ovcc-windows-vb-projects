namespace SteganoDotNet.UserControls {
	partial class ListConfiguration {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPercentOfMessage2 = new System.Windows.Forms.Label();
			this.numPercentOfMessage = new System.Windows.Forms.NumericUpDown();
			this.lblPercentOfMessage1 = new System.Windows.Forms.Label();
			this.btnAlphabetFile = new System.Windows.Forms.Button();
			this.txtAlphabetFile = new System.Windows.Forms.TextBox();
			this.lblAlphabetFile = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).BeginInit();
			this.SuspendLayout();
			// 
			// lblPercentOfMessage2
			// 
			this.lblPercentOfMessage2.Location = new System.Drawing.Point(182, 70);
			this.lblPercentOfMessage2.Name = "lblPercentOfMessage2";
			this.lblPercentOfMessage2.Size = new System.Drawing.Size(151, 13);
			this.lblPercentOfMessage2.TabIndex = 21;
			this.lblPercentOfMessage2.Text = "% of the message in this carrier";
			// 
			// numPercentOfMessage
			// 
			this.numPercentOfMessage.DecimalPlaces = 1;
			this.numPercentOfMessage.Value = 100;
			this.numPercentOfMessage.Location = new System.Drawing.Point(126, 66);
			this.numPercentOfMessage.Name = "numPercentOfMessage";
			this.numPercentOfMessage.Size = new System.Drawing.Size(50, 20);
			this.numPercentOfMessage.TabIndex = 20;
			// 
			// lblPercentOfMessage1
			// 
			this.lblPercentOfMessage1.AutoSize = true;
			this.lblPercentOfMessage1.Location = new System.Drawing.Point(91, 70);
			this.lblPercentOfMessage1.Name = "lblPercentOfMessage1";
			this.lblPercentOfMessage1.Size = new System.Drawing.Size(29, 13);
			this.lblPercentOfMessage1.TabIndex = 24;
			this.lblPercentOfMessage1.Text = "Hide";
			// 
			// btnAlphabetFile
			// 
			this.btnAlphabetFile.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnAlphabetFile.Location = new System.Drawing.Point(440, 28);
			this.btnAlphabetFile.Name = "btnAlphabetFile";
			this.btnAlphabetFile.Size = new System.Drawing.Size(25, 23);
			this.btnAlphabetFile.TabIndex = 27;
			this.btnAlphabetFile.Text = "...";
			this.btnAlphabetFile.UseVisualStyleBackColor = false;
			this.btnAlphabetFile.Click += new System.EventHandler(this.btnAlphabetFile_Click);
			// 
			// txtAlphabetFile
			// 
			this.txtAlphabetFile.Location = new System.Drawing.Point(126, 32);
			this.txtAlphabetFile.Name = "txtAlphabetFile";
			this.txtAlphabetFile.Size = new System.Drawing.Size(307, 20);
			this.txtAlphabetFile.TabIndex = 26;
			// 
			// lblAlphabetFile
			// 
			this.lblAlphabetFile.AutoSize = true;
			this.lblAlphabetFile.Location = new System.Drawing.Point(12, 37);
			this.lblAlphabetFile.Name = "lblAlphabetFile";
			this.lblAlphabetFile.Size = new System.Drawing.Size(108, 13);
			this.lblAlphabetFile.TabIndex = 25;
			this.lblAlphabetFile.Text = "Sort with alphabet file";
			// 
			// ListConfiguration
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnAlphabetFile);
			this.Controls.Add(this.txtAlphabetFile);
			this.Controls.Add(this.lblAlphabetFile);
			this.Controls.Add(this.lblPercentOfMessage1);
			this.Controls.Add(this.lblPercentOfMessage2);
			this.Controls.Add(this.numPercentOfMessage);
			this.Name = "ListConfiguration";
			this.Size = new System.Drawing.Size(478, 101);
			this.Controls.SetChildIndex(this.numPercentOfMessage, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage2, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage1, 0);
			this.Controls.SetChildIndex(this.lblAlphabetFile, 0);
			this.Controls.SetChildIndex(this.txtAlphabetFile, 0);
			this.Controls.SetChildIndex(this.btnAlphabetFile, 0);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblPercentOfMessage2;
		private System.Windows.Forms.NumericUpDown numPercentOfMessage;
		private System.Windows.Forms.Label lblPercentOfMessage1;
		private System.Windows.Forms.Button btnAlphabetFile;
		private System.Windows.Forms.TextBox txtAlphabetFile;
		private System.Windows.Forms.Label lblAlphabetFile;
	}
}
