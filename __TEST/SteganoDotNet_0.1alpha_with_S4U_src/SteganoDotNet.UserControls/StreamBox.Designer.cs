namespace SteganoDotNet.UserControls {
	partial class StreamBox {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fsbMessageFileName = new SteganoDotNet.UserControls.FileSelectBox();
			this.txtMessageText = new System.Windows.Forms.TextBox();
			this.lblMessageText = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// fsbMessageFileName
			// 
			this.fsbMessageFileName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.fsbMessageFileName.BrowseButtonEnabled = true;
			this.fsbMessageFileName.DialogType = SteganoDotNet.UserControls.FileSelectBox.FileDialogType.Open;
			this.fsbMessageFileName.FileName = "";
			this.fsbMessageFileName.Filter = "";
			this.fsbMessageFileName.LabelText = "Aus Datei lesen";
			this.fsbMessageFileName.Location = new System.Drawing.Point(21, 1);
			this.fsbMessageFileName.Name = "fsbMessageFileName";
			this.fsbMessageFileName.Size = new System.Drawing.Size(443, 24);
			this.fsbMessageFileName.TabIndex = 0;
			this.fsbMessageFileName.TextBoxEnabled = true;
			this.fsbMessageFileName.FileNameChanged += new System.EventHandler(this.fsbMessageFileName_FileNameChanged);
			// 
			// txtMessageText
			// 
			this.txtMessageText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.txtMessageText.Location = new System.Drawing.Point(129, 33);
			this.txtMessageText.Multiline = true;
			this.txtMessageText.Name = "txtMessageText";
			this.txtMessageText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtMessageText.Size = new System.Drawing.Size(298, 63);
			this.txtMessageText.TabIndex = 1;
			this.txtMessageText.TextChanged += new System.EventHandler(this.txtMessageText_TextChanged);
			// 
			// lblMessageText
			// 
			this.lblMessageText.Location = new System.Drawing.Point(0, 33);
			this.lblMessageText.Name = "lblMessageText";
			this.lblMessageText.Size = new System.Drawing.Size(127, 18);
			this.lblMessageText.TabIndex = 2;
			this.lblMessageText.Text = "Enter text";
			this.lblMessageText.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// StreamBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.lblMessageText);
			this.Controls.Add(this.txtMessageText);
			this.Controls.Add(this.fsbMessageFileName);
			this.Name = "StreamBox";
			this.Size = new System.Drawing.Size(467, 99);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private FileSelectBox fsbMessageFileName;
		private System.Windows.Forms.TextBox txtMessageText;
		private System.Windows.Forms.Label lblMessageText;

	}
}
