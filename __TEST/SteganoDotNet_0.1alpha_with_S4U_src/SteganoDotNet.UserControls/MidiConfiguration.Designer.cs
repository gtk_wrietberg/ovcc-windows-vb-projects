namespace SteganoDotNet.UserControls {
	partial class MidiConfiguration {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblPercentOfMessage2 = new System.Windows.Forms.Label();
			this.numPercentOfMessage = new System.Windows.Forms.NumericUpDown();
			this.lblPercentOfMessage1 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.numHalfBytesPerUnit = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numHalfBytesPerUnit)).BeginInit();
			this.SuspendLayout();
			// 
			// lblPercentOfMessage2
			// 
			this.lblPercentOfMessage2.Location = new System.Drawing.Point(108, 35);
			this.lblPercentOfMessage2.Name = "lblPercentOfMessage2";
			this.lblPercentOfMessage2.Size = new System.Drawing.Size(151, 13);
			this.lblPercentOfMessage2.TabIndex = 21;
			this.lblPercentOfMessage2.Text = "% of the message in this carrier";
			// 
			// numPercentOfMessage
			// 
			this.numPercentOfMessage.DecimalPlaces = 1;
			this.numPercentOfMessage.Value = 100;
			this.numPercentOfMessage.Location = new System.Drawing.Point(54, 31);
			this.numPercentOfMessage.Name = "numPercentOfMessage";
			this.numPercentOfMessage.Size = new System.Drawing.Size(50, 20);
			this.numPercentOfMessage.TabIndex = 20;
			// 
			// lblPercentOfMessage1
			// 
			this.lblPercentOfMessage1.AutoSize = true;
			this.lblPercentOfMessage1.Location = new System.Drawing.Point(11, 35);
			this.lblPercentOfMessage1.Name = "lblPercentOfMessage1";
			this.lblPercentOfMessage1.Size = new System.Drawing.Size(29, 13);
			this.lblPercentOfMessage1.TabIndex = 24;
			this.lblPercentOfMessage1.Text = "Hide";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(7, 64);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(33, 13);
			this.label1.TabIndex = 27;
			this.label1.Text = "Insert";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(108, 64);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(151, 13);
			this.label4.TabIndex = 26;
			this.label4.Text = "half bytes per event";
			// 
			// numHalfBytesPerUnit
			// 
			this.numHalfBytesPerUnit.Location = new System.Drawing.Point(54, 60);
			this.numHalfBytesPerUnit.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
			this.numHalfBytesPerUnit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numHalfBytesPerUnit.Name = "numHalfBytesPerUnit";
			this.numHalfBytesPerUnit.Size = new System.Drawing.Size(50, 20);
			this.numHalfBytesPerUnit.TabIndex = 25;
			this.numHalfBytesPerUnit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// MidiConfiguration
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.numHalfBytesPerUnit);
			this.Controls.Add(this.lblPercentOfMessage1);
			this.Controls.Add(this.lblPercentOfMessage2);
			this.Controls.Add(this.numPercentOfMessage);
			this.Name = "MidiConfiguration";
			this.Size = new System.Drawing.Size(261, 103);
			this.Controls.SetChildIndex(this.numPercentOfMessage, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage2, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage1, 0);
			this.Controls.SetChildIndex(this.numHalfBytesPerUnit, 0);
			this.Controls.SetChildIndex(this.label4, 0);
			this.Controls.SetChildIndex(this.label1, 0);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numHalfBytesPerUnit)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblPercentOfMessage2;
		private System.Windows.Forms.NumericUpDown numPercentOfMessage;
		private System.Windows.Forms.Label lblPercentOfMessage1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown numHalfBytesPerUnit;
	}
}
