﻿/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#region Using directives

using System;
using System.Drawing;
using System.Collections.ObjectModel;

#endregion

namespace SteganoDotNet.UserControls
{
    /// <summary>Describes a carrier image that is being used by a SteganoDotNet.Action.BitmapFileUtility></summary>
    public class ImageInfo
    {
        private string destinationFileName = string.Empty;
        private string textMessage = string.Empty;
        private Collection<RegionInfo> regionInfo;
        private Image image;
        
        /// <summary>Returns the image.</summary>
        public Image Image
        {
            get { return image; }
        }

        /// <summary>Returns the selected regions or sets the extracted regions.</summary>
        public Collection<RegionInfo> RegionInfo
        {
            get { return regionInfo; }
            set { regionInfo = value; }
        }

        /// <summary>Returns or sets the destination file name.</summary>
        public string DestinationFileName
        {
            get { return destinationFileName; }
            set { destinationFileName = value; }
        }

        /// <summary>Returns or sets the plain text that has been extracted from this image.</summary>
        public string TextMessage
        {
            get { return textMessage; }
            set { textMessage = value; }
        }

        /// <summary>Constructor.</summary>
        /// <param name="image">The image.</param>
        /// <param name="regionInfo">The selected or extracted regions.</param>
        public ImageInfo(Image image, Collection<RegionInfo> regionInfo)
        {
            this.image = image;
            this.regionInfo = regionInfo;
        }
    }
}
