/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.UserControls {

    /// <summary>Configuration UI for SteganoDotNet.Action.BitmapFileUtility.</summary>
	public partial class BitmapConfiguration : CarrierConfiguration, IBitmapConfiguration {
			
		private Bitmap bitmap;
		private RegionForm regionForm;
		private Collection<RegionInfo> regionInfo = new Collection<RegionInfo>();
		private int countBytesToHide;

        /// <summary>Returns or sets the amount of data that will be hidden in the carrier file, in percent of the message.</summary>
		public float PercentOfMessage
		{
			get
			{
				return (float)numPercentOfMessage.Value;
			}
			set
			{
				numPercentOfMessage.Value = (decimal)value;
			}
		}

        /// <summary>Returns or sets, how many percent of the image will be covered with useless pseudo-data.</summary>
		public int NoisePercent
		{
			get
			{
				return tbNoisePercent.Value;
			}
			set
			{
				tbNoisePercent.Value = value;
			}
		}

        /// <summary>Returns the selected regions.</summary>
		public Collection<RegionInfo> RegionInfo
		{
			get
			{
				return regionInfo;
			}
		}

        /// <summary>Returns or sets the number of bytes that will be hidden in the carrier file.</summary>
        public int CountBytesToHide
		{
			get
			{
				return countBytesToHide;
			}
		}

        /// <summary>Returns or sets the amount of data that will be hidden in the carrier file, in percent of the message.</summary>
        public float CountBytesToHidePercent
		{
			get
			{
				if (expectedMessageLength > 0)
				{
					return 100 * countBytesToHide / expectedMessageLength;
				}
				else
				{
					return 0;
				}
			}
		}

        /// <summary>Constructor.</summary>
        /// <param name="bitmap">The carrier image to configure.</param>
		public BitmapConfiguration(Bitmap bitmap)
		{
			this.bitmap = bitmap;
			InitializeComponent();
		}

		/// <summary>Validates the configuration.</summary>
		public override void Accept(CancelEventArgs eventArgs) {
			base.Accept(eventArgs); 
			
			errorProvider.SetError(lblPercentOfMessage2, string.Empty);
			errorProvider.SetError(btnRegions, string.Empty);

			if (numPercentOfMessage.Value == 0)
			{
				errorProvider.SetError(lblPercentOfMessage2, "Die Tr�ger-Datei ist nutzlos, wenn sie keinen Teil der Nachricht versteckt.");
				eventArgs.Cancel = true;
			}
			if (countBytesToHide == 0)
			{
				errorProvider.SetError(btnRegions, "Ohne markierte Regionen kann das Bild keine Nachricht aufnehmen.");
				eventArgs.Cancel = true;
			}
		}

		private void btnRegions_Click(object sender, EventArgs e)
		{
			int messagePartLength = (int)Math.Ceiling(numPercentOfMessage.Value * expectedMessageLength / 100);
			if (regionForm == null)
			{
				regionForm = new RegionForm(bitmap, messagePartLength);
				Form parentForm = this.FindForm();
				if (parentForm != null)
				{
					regionForm.BackgroundImage = parentForm.BackgroundImage;
				}
			}
			else
			{
				regionForm.MessageLength = messagePartLength;
			}

			if(regionForm.ShowDialog(this) == DialogResult.OK)
			{
				ImageInfo imageInfo = regionForm.ImageInfo;
				regionInfo = imageInfo.RegionInfo;

				countBytesToHide = 0;
				foreach (RegionInfo region in regionInfo)
				{
					countBytesToHide += region.Capacity;
				}
			}
		}

		private void tbNoisePercent_ValueChanged(object sender, EventArgs e)
		{
			if((int)numNoisePercent.Value != tbNoisePercent.Value)
			{
				numNoisePercent.Value = tbNoisePercent.Value;
			}
		}

		private void numNoisePercent_ValueChanged(object sender, EventArgs e)
		{
			if(tbNoisePercent.Value != (int)numNoisePercent.Value)
			{
				tbNoisePercent.Value = (int)numNoisePercent.Value;
			}
		}
	}
}
