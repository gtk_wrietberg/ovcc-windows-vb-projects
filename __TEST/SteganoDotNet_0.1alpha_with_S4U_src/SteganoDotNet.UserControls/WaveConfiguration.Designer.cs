namespace SteganoDotNet.UserControls {
	partial class WaveConfiguration {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label3 = new System.Windows.Forms.Label();
			this.lblNoisePercent = new System.Windows.Forms.Label();
			this.tbNoisePercent = new System.Windows.Forms.TrackBar();
			this.numNoisePercent = new System.Windows.Forms.NumericUpDown();
			this.lblPercentOfMessage2 = new System.Windows.Forms.Label();
			this.numPercentOfMessage = new System.Windows.Forms.NumericUpDown();
			this.lblPercentOfMessage1 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.numBitsPerUnit = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbNoisePercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numNoisePercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numBitsPerUnit)).BeginInit();
			this.SuspendLayout();
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Location = new System.Drawing.Point(397, 94);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(66, 23);
			this.label3.TabIndex = 19;
			this.label3.Text = "% of the file";
			// 
			// lblNoisePercent
			// 
			this.lblNoisePercent.Location = new System.Drawing.Point(8, 94);
			this.lblNoisePercent.Name = "lblNoisePercent";
			this.lblNoisePercent.Size = new System.Drawing.Size(94, 23);
			this.lblNoisePercent.TabIndex = 18;
			this.lblNoisePercent.Text = "Add random noise";
			this.lblNoisePercent.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// tbNoisePercent
			// 
			this.tbNoisePercent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbNoisePercent.Location = new System.Drawing.Point(97, 90);
			this.tbNoisePercent.Maximum = 100;
			this.tbNoisePercent.Name = "tbNoisePercent";
			this.tbNoisePercent.Size = new System.Drawing.Size(242, 42);
			this.tbNoisePercent.TabIndex = 16;
			this.tbNoisePercent.ValueChanged += new System.EventHandler(this.tbNoisePercent_ValueChanged);
			// 
			// numNoisePercent
			// 
			this.numNoisePercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.numNoisePercent.DecimalPlaces = 1;
			this.numNoisePercent.Location = new System.Drawing.Point(335, 91);
			this.numNoisePercent.Name = "numNoisePercent";
			this.numNoisePercent.Size = new System.Drawing.Size(56, 20);
			this.numNoisePercent.TabIndex = 17;
			this.numNoisePercent.ValueChanged += new System.EventHandler(this.numNoisePercent_ValueChanged);
			// 
			// lblPercentOfMessage2
			// 
			this.lblPercentOfMessage2.Location = new System.Drawing.Point(108, 36);
			this.lblPercentOfMessage2.Name = "lblPercentOfMessage2";
			this.lblPercentOfMessage2.Size = new System.Drawing.Size(151, 13);
			this.lblPercentOfMessage2.TabIndex = 21;
			this.lblPercentOfMessage2.Text = "% of the message in this carrier";
			// 
			// numPercentOfMessage
			// 
			this.numPercentOfMessage.DecimalPlaces = 1;
			this.numPercentOfMessage.Value = 100;
			this.numPercentOfMessage.Location = new System.Drawing.Point(54, 32);
			this.numPercentOfMessage.Name = "numPercentOfMessage";
			this.numPercentOfMessage.Size = new System.Drawing.Size(50, 20);
			this.numPercentOfMessage.TabIndex = 20;
			// 
			// lblPercentOfMessage1
			// 
			this.lblPercentOfMessage1.AutoSize = true;
			this.lblPercentOfMessage1.Location = new System.Drawing.Point(10, 39);
			this.lblPercentOfMessage1.Name = "lblPercentOfMessage1";
			this.lblPercentOfMessage1.Size = new System.Drawing.Size(29, 13);
			this.lblPercentOfMessage1.TabIndex = 24;
			this.lblPercentOfMessage1.Text = "Hide";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(10, 68);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(44, 13);
			this.label1.TabIndex = 27;
			this.label1.Text = "Change";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(108, 65);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(151, 13);
			this.label4.TabIndex = 26;
			this.label4.Text = "bits per sample";
			// 
			// numBitsPerUnit
			// 
			this.numBitsPerUnit.Location = new System.Drawing.Point(54, 61);
			this.numBitsPerUnit.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
			this.numBitsPerUnit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numBitsPerUnit.Name = "numBitsPerUnit";
			this.numBitsPerUnit.Size = new System.Drawing.Size(50, 20);
			this.numBitsPerUnit.TabIndex = 25;
			this.numBitsPerUnit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// WaveConfiguration
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.numNoisePercent);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.numBitsPerUnit);
			this.Controls.Add(this.lblPercentOfMessage1);
			this.Controls.Add(this.lblPercentOfMessage2);
			this.Controls.Add(this.numPercentOfMessage);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lblNoisePercent);
			this.Controls.Add(this.tbNoisePercent);
			this.Name = "WaveConfiguration";
			this.Size = new System.Drawing.Size(467, 131);
			this.Controls.SetChildIndex(this.tbNoisePercent, 0);
			this.Controls.SetChildIndex(this.lblNoisePercent, 0);
			this.Controls.SetChildIndex(this.label3, 0);
			this.Controls.SetChildIndex(this.numPercentOfMessage, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage2, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage1, 0);
			this.Controls.SetChildIndex(this.numBitsPerUnit, 0);
			this.Controls.SetChildIndex(this.label4, 0);
			this.Controls.SetChildIndex(this.label1, 0);
			this.Controls.SetChildIndex(this.numNoisePercent, 0);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbNoisePercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numNoisePercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numBitsPerUnit)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblNoisePercent;
		private System.Windows.Forms.TrackBar tbNoisePercent;
		private System.Windows.Forms.NumericUpDown numNoisePercent;
		private System.Windows.Forms.Label lblPercentOfMessage2;
		private System.Windows.Forms.NumericUpDown numPercentOfMessage;
		private System.Windows.Forms.Label lblPercentOfMessage1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown numBitsPerUnit;
	}
}
