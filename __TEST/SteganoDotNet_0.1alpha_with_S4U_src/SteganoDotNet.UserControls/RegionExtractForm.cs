﻿/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#region Using directives

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using SteganoDotNet.Interfaces;

#endregion

namespace SteganoDotNet.UserControls
{
    /// <summary>Displays extracted regions and plain text for one carrier image.</summary>
    public partial class RegionExtractForm : Form
    {
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClose;

		/// <summary>Constructor.</summary>
		public RegionExtractForm(ImageInfo imageInfo)
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            DrawRegions(imageInfo);
            txtMessage.Text = imageInfo.TextMessage;
        }

        /// <summary>Draw the image and the carrier regions</summary>
        /// <param name="imageInfo">Image and regions</param>
        private void DrawRegions(ImageInfo imageInfo)
        {
            picImage.Image = (Image)imageInfo.Image;
            picImage.Size = picImage.Image.Size;
            Graphics graphics = Graphics.FromImage(picImage.Image);
            
            foreach (RegionInfo info in imageInfo.RegionInfo)
            {
                graphics.FillRegion(new SolidBrush(Color.Red), info.Region);
            }

            graphics.Dispose();
            picImage.Invalidate();
        }

        private void btnClose_Click(object sender, System.EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }
    }
}