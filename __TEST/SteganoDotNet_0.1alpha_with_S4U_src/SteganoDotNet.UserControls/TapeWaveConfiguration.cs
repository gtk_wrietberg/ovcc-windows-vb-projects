/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.UserControls {

	/// <summary>Configuration UI for SteganoDotNet.Action.TapeWaveFileUtility.</summary>
	public partial class TapeWaveConfiguration : CarrierConfiguration, ITapeWaveConfiguration
	{

		/// <summary>Called whenever the applicatiohn should open its tape configuration dialog.</summary>
		public event EventHandler OpenTapeForm;

		/// <summary>Percentage of the message that will be hidden in the current file.</summary>
		public float CountBytesToHidePercent
		{
			get
			{
				return (float)numPercentOfMessage.Value;
			}
			set
			{
				numPercentOfMessage.Value = (decimal)value;
			}
		}

		/// <summary>Carrier frequency.</summary>
		public decimal Frequency
		{
			get
			{
				return numFrequency.Value;
			}
			set
			{
				numFrequency.Value = value;
			}
		}

		/// <summary>Carrier volume.</summary>
		public decimal Volume
		{
			get
			{
				return numVolume.Value;
			}
			set
			{
				numVolume.Value = value;
			}
		}

		/// <summary>Lenght of the inserted beeps ind 1/seconds.</summary>
		public int BeepLength
		{
			get
			{
				return (int)numBeepLength.Value;
			}
			set
			{
				numBeepLength.Value = value;
			}
		}

		/// <summary>Offset of the start marker in seconds.</summary>
		public int StartTagOffset
		{
			get
			{
				return (int)numStartTagOffset.Value;
			}
			set
			{
				numStartTagOffset.Value = value;
			}
		}

		/// <summary>Constructor.</summary>
		public TapeWaveConfiguration()
		{
			InitializeComponent();
		}

		/// <summary>Accepts the settings.</summary>
		public override void Accept(CancelEventArgs eventArgs)
		{
			
		}

		private void TapeWaveConfiguration_ParentChanged(object sender, EventArgs e)
		{

			if (this.Parent != null)
			{
				if (OpenTapeForm != null)
				{
					this.Cursor = Cursors.WaitCursor;
					try
					{
						OpenTapeForm(this, EventArgs.Empty);
					}
					finally
					{
						this.Cursor = Cursors.Default;
					}
				}
			}
		}
	}
}
