/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SteganoDotNet.UserControls {

    /// <summary>Combines a <see cref="System.Windows.Forms.Label">label</see>, <see cref="System.Windows.Forms.TextBox">textbox</see> and a browse <see cref="System.Windows.Forms.Button">button</see>.</summary>
	public partial class FileSelectBox : UserControl {

		/// <summary>Supported kinds of file dialogs.</summary>
		public enum FileDialogType
		{
			/// <summary>Open file for reading.</summary>
			Open,
			/// <summary>Select file name for writing.</summary>
			Save
		}

		private FileDialogType dialogType;
		private string filter = String.Empty;
		
		/// <summary>The name of the selected file has changed.</summary>
		public event EventHandler FileNameChanged;

		/// <summary>Returns or sets the kind of file dialog displayed by the FileSelectBox.</summary>
		public FileDialogType DialogType
		{
			get { return dialogType; }
			set { dialogType = value; }
		}

        /// <summary>Returns or sets the filter for an <see cref="System.Windows.Forms.OpenFileDialog"/>.</summary>
		public string Filter
		{
			get { return filter; }
			set { filter = value; }
		}

        /// <summary>Returns or sets the label text.</summary>
		public string LabelText
		{
			get { return lblFileName.Text; }
			set { lblFileName.Text = value; }
		}

        /// <summary>Returns or sets the selected file's full name.</summary>
		public string FileName
		{
			get { return txtFileName.Text; }
			set { txtFileName.Text = value; }
		}

		/// <summary>Returns or sets the enabled state of the TextBox.</summary>
		public bool TextBoxEnabled
		{
			get { return txtFileName.Enabled; }
			set { txtFileName.Enabled = value; }
		}

		/// <summary>Returns or sets the enabled state of the Button.</summary>
		public bool BrowseButtonEnabled
		{
			get { return btnBrowse.Enabled; }
			set { btnBrowse.Enabled = value; }
		}

        /// <summary>Constructor.</summary>
		public FileSelectBox()
		{
			InitializeComponent();
		}

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			FileDialog dlg;
			if (dialogType == FileDialogType.Open)
			{
				dlg = new OpenFileDialog();
			}
			else
			{
				dlg = new SaveFileDialog();
			}

			dlg.Filter = filter;
			if (dlg.ShowDialog() == DialogResult.OK) {
				txtFileName.Text = dlg.FileName;
			}
		}

		private void txtFileName_TextChanged(object sender, EventArgs e)
		{
			if (FileNameChanged != null) {
				FileNameChanged(this, e);
			}
		}
	}
}
