/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace SteganoDotNet.UserControls {
	/// <summary>Combination of a file browser and a TextBox.</summary>
	public partial class StreamBox : UserControl {

		/// <summary>Informs other objects, if the file or text has changed.</summary>
		public event EventHandler StreamChanged;

		/// <summary>Checks the availability of a file or text.</summary>
		public bool HasStream
		{
			get
			{
				return ((txtMessageText.Text.Length > 0) || (fsbMessageFileName.FileName.Length > 0));
			}
		}

		/// <summary>Returns or sets the kind of file dialog displayed by the FileSelectBox.</summary>
		public FileSelectBox.FileDialogType DialogType
		{
			get { return fsbMessageFileName.DialogType; }
			set { fsbMessageFileName.DialogType = value; }
		}

		/// <summary>Returns or sets the name of the selected file.</summary>
		public string FileName
		{
			get { return fsbMessageFileName.FileName; }
			set { fsbMessageFileName.FileName = value; }
		}
		
		/// <summary>Returns or sets the text in the TextBox.</summary>
		public string MessageText
		{
			get { return txtMessageText.Text; }
			set { txtMessageText.Text = value; }
		}

		/// <summary>Returns or sets the label text for the file box.</summary>
		public string LabelFile
		{
			get { return fsbMessageFileName.LabelText; }
			set { fsbMessageFileName.LabelText = value; }
		}

		/// <summary>Returns or sets the label text for the text box.</summary>
		public string LabelText
		{
			get { return lblMessageText.Text; }
			set { lblMessageText.Text = value; }
		}

		/// <summary>Contstructor.</summary>
		public StreamBox()
		{
			InitializeComponent();
		}

		/// <summary>Returns a stream with the file's content or the entered text.</summary>
		public Stream Stream
		{
			get { return GetStream(); }
		}

		private Stream GetStream()
		{
			BinaryWriter writer = new BinaryWriter(new MemoryStream());
			if (txtMessageText.Text.Length > 0) {
				writer.Write(Encoding.Default.GetBytes(txtMessageText.Text));
			}
			else if (fsbMessageFileName.FileName.Length > 0)
			{
				FileStream fs = new FileStream(fsbMessageFileName.FileName, FileMode.Open, FileAccess.Read);
				byte[] buffer = new byte[fs.Length];
				fs.Read(buffer, 0, (int)fs.Length);
				writer.Write(buffer);
				fs.Close();
			}
			
			writer.Seek(0, SeekOrigin.Begin);
			return writer.BaseStream;
		}

		private void OnStreamChanged()
		{
			if (StreamChanged != null)
			{
				StreamChanged(this, EventArgs.Empty);
			}
		}

		private void fsbMessageFileName_FileNameChanged(object sender, EventArgs e)
		{
			if (fsbMessageFileName.FileName.Length > 0) {
				txtMessageText.Enabled = false;
				txtMessageText.Text = string.Empty;
			} else {
				txtMessageText.Enabled = true;
			}
			OnStreamChanged();
		}

		private void txtMessageText_TextChanged(object sender, EventArgs e)
		{
			if (txtMessageText.Text.Length > 0) {
				fsbMessageFileName.TextBoxEnabled = false;
			} else {
				fsbMessageFileName.TextBoxEnabled = true;
			}
			OnStreamChanged();
		}
	}
}
