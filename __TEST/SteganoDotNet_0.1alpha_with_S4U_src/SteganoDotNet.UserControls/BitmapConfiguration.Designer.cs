namespace SteganoDotNet.UserControls {
	partial class BitmapConfiguration {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label3 = new System.Windows.Forms.Label();
			this.lblNoisePercent = new System.Windows.Forms.Label();
			this.tbNoisePercent = new System.Windows.Forms.TrackBar();
			this.numNoisePercent = new System.Windows.Forms.NumericUpDown();
			this.lblPercentOfMessage2 = new System.Windows.Forms.Label();
			this.numPercentOfMessage = new System.Windows.Forms.NumericUpDown();
			this.btnRegions = new System.Windows.Forms.Button();
			this.lblPercentOfMessage1 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbNoisePercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numNoisePercent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).BeginInit();
			this.SuspendLayout();
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Location = new System.Drawing.Point(454, 133);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(66, 34);
			this.label3.TabIndex = 19;
			this.label3.Text = "% of the file";
			// 
			// lblNoisePercent
			// 
			this.lblNoisePercent.Location = new System.Drawing.Point(10, 133);
			this.lblNoisePercent.Name = "lblNoisePercent";
			this.lblNoisePercent.Size = new System.Drawing.Size(97, 34);
			this.lblNoisePercent.TabIndex = 18;
			this.lblNoisePercent.Text = "Add random noise";
			this.lblNoisePercent.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// tbNoisePercent
			// 
			this.tbNoisePercent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbNoisePercent.Location = new System.Drawing.Point(102, 125);
			this.tbNoisePercent.Maximum = 100;
			this.tbNoisePercent.Name = "tbNoisePercent";
			this.tbNoisePercent.Size = new System.Drawing.Size(297, 42);
			this.tbNoisePercent.TabIndex = 16;
			this.tbNoisePercent.ValueChanged += new System.EventHandler(this.tbNoisePercent_ValueChanged);
			// 
			// numNoisePercent
			// 
			this.numNoisePercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.numNoisePercent.DecimalPlaces = 1;
			this.numNoisePercent.Location = new System.Drawing.Point(398, 133);
			this.numNoisePercent.Name = "numNoisePercent";
			this.numNoisePercent.Size = new System.Drawing.Size(56, 20);
			this.numNoisePercent.TabIndex = 17;
			this.numNoisePercent.ValueChanged += new System.EventHandler(this.numNoisePercent_ValueChanged);
			// 
			// lblPercentOfMessage2
			// 
			this.lblPercentOfMessage2.Location = new System.Drawing.Point(101, 38);
			this.lblPercentOfMessage2.Name = "lblPercentOfMessage2";
			this.lblPercentOfMessage2.Size = new System.Drawing.Size(159, 16);
			this.lblPercentOfMessage2.TabIndex = 21;
			this.lblPercentOfMessage2.Text = "% of the message in this carrier";
			// 
			// numPercentOfMessage
			// 
			this.numPercentOfMessage.DecimalPlaces = 1;
			this.numPercentOfMessage.Value = 100;
			this.numPercentOfMessage.Location = new System.Drawing.Point(45, 34);
			this.numPercentOfMessage.Name = "numPercentOfMessage";
			this.numPercentOfMessage.Size = new System.Drawing.Size(50, 20);
			this.numPercentOfMessage.TabIndex = 20;
			// 
			// btnRegions
			// 
			this.btnRegions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.btnRegions.BackColor = System.Drawing.SystemColors.ButtonFace;
			this.btnRegions.Location = new System.Drawing.Point(13, 79);
			this.btnRegions.Name = "btnRegions";
			this.btnRegions.Size = new System.Drawing.Size(247, 23);
			this.btnRegions.TabIndex = 23;
			this.btnRegions.Text = "Configure regions ...";
			this.btnRegions.UseVisualStyleBackColor = false;
			this.btnRegions.Click += new System.EventHandler(this.btnRegions_Click);
			// 
			// lblPercentOfMessage1
			// 
			this.lblPercentOfMessage1.AutoSize = true;
			this.lblPercentOfMessage1.Location = new System.Drawing.Point(10, 38);
			this.lblPercentOfMessage1.Name = "lblPercentOfMessage1";
			this.lblPercentOfMessage1.Size = new System.Drawing.Size(29, 13);
			this.lblPercentOfMessage1.TabIndex = 24;
			this.lblPercentOfMessage1.Text = "Hide";
			// 
			// BitmapConfiguration
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.lblPercentOfMessage1);
			this.Controls.Add(this.lblPercentOfMessage2);
			this.Controls.Add(this.btnRegions);
			this.Controls.Add(this.numPercentOfMessage);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.lblNoisePercent);
			this.Controls.Add(this.tbNoisePercent);
			this.Controls.Add(this.numNoisePercent);
			this.Name = "BitmapConfiguration";
			this.Size = new System.Drawing.Size(569, 176);
			this.Controls.SetChildIndex(this.numNoisePercent, 0);
			this.Controls.SetChildIndex(this.tbNoisePercent, 0);
			this.Controls.SetChildIndex(this.lblNoisePercent, 0);
			this.Controls.SetChildIndex(this.label3, 0);
			this.Controls.SetChildIndex(this.numPercentOfMessage, 0);
			this.Controls.SetChildIndex(this.btnRegions, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage2, 0);
			this.Controls.SetChildIndex(this.lblPercentOfMessage1, 0);
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbNoisePercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numNoisePercent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPercentOfMessage)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label lblNoisePercent;
		private System.Windows.Forms.TrackBar tbNoisePercent;
		private System.Windows.Forms.NumericUpDown numNoisePercent;
		private System.Windows.Forms.Label lblPercentOfMessage2;
		private System.Windows.Forms.NumericUpDown numPercentOfMessage;
		private System.Windows.Forms.Button btnRegions;
		private System.Windows.Forms.Label lblPercentOfMessage1;
	}
}
