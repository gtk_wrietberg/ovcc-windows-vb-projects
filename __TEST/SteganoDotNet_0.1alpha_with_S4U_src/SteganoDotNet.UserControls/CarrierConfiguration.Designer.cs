namespace SteganoDotNet.UserControls {
	partial class CarrierConfiguration {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CarrierConfiguration));
			this.mainToolStrip = new System.Windows.Forms.ToolStrip();
			this.tsbAccept = new System.Windows.Forms.ToolStripButton();
			this.tsbCancel = new System.Windows.Forms.ToolStripButton();
			this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.mainToolStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
			this.SuspendLayout();
			// 
			// mainToolStrip
			// 
			this.mainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAccept,
            this.tsbCancel});
			this.mainToolStrip.Location = new System.Drawing.Point(0, 0);
			this.mainToolStrip.Name = "mainToolStrip";
			this.mainToolStrip.Size = new System.Drawing.Size(516, 25);
			this.mainToolStrip.TabIndex = 0;
			// 
			// tsbAccept
			// 
			this.tsbAccept.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbAccept.Image = ((System.Drawing.Image)(resources.GetObject("tsbAccept.Image")));
			this.tsbAccept.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbAccept.Name = "tsbAccept";
			this.tsbAccept.Size = new System.Drawing.Size(23, 22);
			this.tsbAccept.ToolTipText = "Einstellungen �bernehmen und zur�ck zur Liste";
			this.tsbAccept.Click += new System.EventHandler(this.tsbAccept_Click);
			// 
			// tsbCancel
			// 
			this.tsbCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.tsbCancel.Image = ((System.Drawing.Image)(resources.GetObject("tsbCancel.Image")));
			this.tsbCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.tsbCancel.Name = "tsbCancel";
			this.tsbCancel.Size = new System.Drawing.Size(23, 22);
			this.tsbCancel.ToolTipText = "Zur�ck zur Liste";
			this.tsbCancel.Click += new System.EventHandler(this.tsbCancel_Click);
			// 
			// errorProvider
			// 
			this.errorProvider.ContainerControl = this;
			// 
			// CarrierConfiguration
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.mainToolStrip);
			this.Name = "CarrierConfiguration";
			this.Size = new System.Drawing.Size(516, 284);
			this.mainToolStrip.ResumeLayout(false);
			this.mainToolStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip mainToolStrip;
		private System.Windows.Forms.ToolStripButton tsbAccept;
		private System.Windows.Forms.ToolStripButton tsbCancel;
		/// <summary>General error marker.</summary>
		protected System.Windows.Forms.ErrorProvider errorProvider;

	}
}
