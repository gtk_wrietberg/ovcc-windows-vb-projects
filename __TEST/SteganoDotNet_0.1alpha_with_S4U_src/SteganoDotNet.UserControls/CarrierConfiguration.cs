/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.UserControls {

    /// <summary>Base class for all configuration UI classes.</summary>
    /// <remarks>Cannot be abstract due to Visual Studio Designer issues.</remarks>
	public partial class CarrierConfiguration : UserControl, ICarrierConfiguration {

		/// <summary>Filter string for file dialogs.</summary>
		protected string fileExtensionFilter;

		/// <summary>Expected length of the message part.</summary>
		protected long expectedMessageLength;

		/// <summary>Amount of data that will be hidden in the carrier file, in percent of the message.</summary>
		protected float percentOfMessage;

        /// <summary>Raised when the user submits his changes.</summary>
		public event AcceptSettingsHandler AcceptSettings;
		
        /// <summary>Raised when the user cancels the configuration.</summary>
        public event EventHandler CancelSettings;

        /// <summary>Returns or sets the filter for output file name selection.</summary>
		public string FileExtensionFilter
		{
			get { return fileExtensionFilter; }
			set { fileExtensionFilter = value; }
		}

        /// <summary>Returns or sets the length of the entire message that will be hidden in the image.</summary>
		public long ExpectedMessageLength
		{
			get
			{
				return expectedMessageLength;
			}
			set
			{
				expectedMessageLength = value;
			}
		}

		/// <summary>Returns or sets the length of the entire message that will be hidden in the image.</summary>
		public virtual float PercentOfMessage
		{
			get
			{
				return percentOfMessage;
			}
			set
			{
				percentOfMessage = value;
			}
		}

        /// <summary>Constructor.</summary>
        public CarrierConfiguration()
		{
			InitializeComponent();
		}

        /// <summary>Can be overwritten by concrete controls to validate the new settings before they are accepted.</summary>
        public virtual void Accept(CancelEventArgs eventArgs) {
			//do nothing
		}

        /// <summary>Removes the control from the container.</summary>
		public void Cancel()
		{
			this.Parent.Controls.Remove(this);
			this.Dispose();
		}

		private void tsbAccept_Click(object sender, EventArgs e)
		{
			CancelEventArgs eventArgs = new CancelEventArgs(false);
			Accept(eventArgs);
			if (!eventArgs.Cancel) {
				OnAccept();
			}
		}

		private void tsbCancel_Click(object sender, EventArgs e)
		{
			Cancel();
			OnCancelSettings();
		}

        /// <summary>Raises the AcceptSettings event.</summary>
		public void OnAccept()
		{
			if (AcceptSettings != null) {
				AcceptSettings(this, EventArgs.Empty);
			}
		}

        /// <summary>Raises the CancelSettings event.</summary>
        private void OnCancelSettings()
		{
			if (CancelSettings  != null) {
				CancelSettings(this, EventArgs.Empty);
			}
		}
	}
}
