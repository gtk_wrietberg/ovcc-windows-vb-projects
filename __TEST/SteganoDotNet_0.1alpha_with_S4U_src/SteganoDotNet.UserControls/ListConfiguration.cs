/*
 Created: 29.07.2006
 Author: Corinna John

Copyright (C) 2006 SteganoDotNet Team

http://sourceforge.net/projects/steganodotnet

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
(current version) as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using SteganoDotNet.Interfaces;

namespace SteganoDotNet.UserControls {

	/// <summary>Configuration UI for SteganoDotNet.Action.ListFileUtility.</summary>
	public partial class ListConfiguration : CarrierConfiguration, IListConfiguration
	{

		/// <summary>Returns or sets the amount of data that will be hidden in the carrier file, in percent of the message.</summary>
		public float PercentOfMessage
		{
			get
			{
				return (float)numPercentOfMessage.Value;
			}
			set
			{
				numPercentOfMessage.Value = (decimal)value;
			}
		}

		/// <summary>File name and path of the alphabet for sorting the list.</summary>
		public string AlphabetFileName
		{
			get
			{
				return txtAlphabetFile.Text;
			}
		}

		/// <summary>Constructor.</summary>
		public ListConfiguration()
		{
			InitializeComponent();
		}

		/// <summary>Validates the configuration.</summary>
		public override void Accept(CancelEventArgs eventArgs)
		{
			base.Accept(eventArgs);

			errorProvider.SetError(lblPercentOfMessage2, string.Empty);
			if (numPercentOfMessage.Value == 0)
			{
				errorProvider.SetError(lblPercentOfMessage2, "Die Tr�ger-Datei ist nutzlos, wenn sie keinen Teil der Nachricht versteckt.");
				eventArgs.Cancel = true;
			}
		}

		private void btnAlphabetFile_Click(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				txtAlphabetFile.Text = dlg.FileName;
			}
		}

	}
}
