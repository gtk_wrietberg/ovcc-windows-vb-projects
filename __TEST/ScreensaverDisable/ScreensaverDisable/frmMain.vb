Imports System.Runtime.InteropServices

Public Class frmMain
    <DllImport("user32", CharSet:=CharSet.Auto)> _
    Public Shared Function SystemParametersInfo( _
                ByVal intAction As Integer, _
                ByVal intParam As Integer, _
                ByRef strParam As String, _
                ByVal intWinIniFlag As Integer) As Integer
    End Function

    Private Const SPI_GETSCREENSAVETIMEOUT As Integer = &HE

    Private Sub btnToggle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnToggle.Click
        Dim retval As Integer
        Dim sScreensaveTimeout As String = "POEP"

        Try
            retval = SystemParametersInfo(SPI_GETSCREENSAVETIMEOUT, 255, sScreensaveTimeout, 0)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try


        MsgBox(retval)
        MsgBox(sScreensaveTimeout)
    End Sub
End Class
