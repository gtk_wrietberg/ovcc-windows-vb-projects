﻿Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement

Module Module1
    Private Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000

    Private Const SID_USERS As String = "S-1-5-32-545"
    Private Const SID_ADMINISTRATORS As String = "S-1-5-32-544"

    Public Sub creaetuser()
        'Try
        Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")


        Dim NewUser As DirectoryEntry = AD.Children.Add("TestUser1", "user")
        NewUser.Invoke("SetPassword", New Object() {"#12345Abc"})
        NewUser.Invoke("Put", New Object() {"Description", "Test User from .NET"})
        NewUser.CommitChanges()

        NewUser.Properties("UserFlags").Value = ADS_UF_DONT_EXPIRE_PASSWD
        NewUser.CommitChanges()

        Dim grp As DirectoryEntry

        grp = AD.Children.Find("Administrators", "group")
        If grp.Name <> "" Then
            grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
        End If
        Console.WriteLine("Account Created Successfully")
        Console.ReadLine()

        'Catch ex As Exception
        '    MsgBox(ex.Message)
        '    Console.WriteLine(ex.Message)
        '    Console.ReadLine()
        'End Try
    End Sub

    Public Function AddUser(ByVal sFullName As String, ByVal sUsername As String, ByVal sPassWord As String, Optional ByVal sGroup As String = "") As String
        Try
            Dim oSystem As Object, oUser As Object, oGroup As Object
            Dim sComputerName As String

            sComputerName = Environment.MachineName

            oSystem = GetObject("WinNT://" & sComputerName)
            oUser = oSystem.Create("user", sUsername)
            oUser.FullName = sFullName
            oUser.SetPassword(sPassWord)
            oUser.SetInfo()

            If sGroup = "" Then
                sGroup = GetUsersGroupName()
            End If
            oGroup = GetObject("WinNT://" & sComputerName & "/" & sGroup)
            oGroup.Add("WinNT://" & sComputerName & "/" & sUsername)

            oGroup = Nothing
            oUser = Nothing
            oSystem = Nothing

            Return "ok"

        Catch ex As Exception

            'How's that for exception handling
            Return ex.Message
        End Try
    End Function


    Public Function GetAdministratorsGroupName() As String
        Return GetGroupNameFromSid(SID_ADMINISTRATORS)
    End Function

    Public Function GetUsersGroupName() As String
        Return GetGroupNameFromSid(SID_USERS)
    End Function

    Private Function GetGroupNameFromSid(ByVal _sid As String) As String
        Dim context As PrincipalContext, group As GroupPrincipal

        context = New PrincipalContext(ContextType.Machine)
        group = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, _sid)

        Return group.SamAccountName
    End Function
End Module