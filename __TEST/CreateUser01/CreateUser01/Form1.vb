﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        creaetuser()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MsgBox(GetAdministratorsGroupName)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        AddUser("Test User from .NET", "guesttek", "rancidkipper")
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        MsgBox(cWindowsUser.DoesUserExist("testUser1"))
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        MsgBox(cWindowsUser.IsUserAdmin("testuser1"))
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim cADMIN_Username As String = "guesttek"
        Dim cADMIN_Password As String = ""
        If CheckBox1.Checked Then
            cADMIN_Password = "Rancidk1pper"
        Else
            cADMIN_Password = "rancidkipper"
        End If

        Dim sRet As String = ""
        MsgBox("Does user '" & cADMIN_Username & "' exist?")
        If Not cWindowsUser.DoesUserExist(cADMIN_Username) Then
            MsgBox("nope")
            MsgBox("creating")

            sRet = cWindowsUser.AddAdminUser(cADMIN_Username, cADMIN_Password)
            If sRet = "ok" Then
                MsgBox("ok")
            Else
                MsgBox("error:")
                MsgBox(sRet)
            End If
        Else
            MsgBox("yep")

            MsgBox("But is he an admin?")
            If cWindowsUser.IsUserAdmin(cADMIN_Username) Then
                MsgBox("yes!")
            Else
                MsgBox("nope")
                MsgBox("adding to admin group")

                sRet = cWindowsUser.PromoteUserToAdmin(cADMIN_Username)
                If sRet = "ok" Then
                    MsgBox("ok")
                Else
                    MsgBox("error:")
                    MsgBox(sRet)
                End If
            End If

            MsgBox("updating the password")
            MsgBox("setting password to default")
            sRet = cWindowsUser.ChangePassword(cADMIN_Username, cADMIN_Password)
            If sRet = "ok" Then
                MsgBox("ok")
            Else
                MsgBox("error:")
                MsgBox(sRet)
            End If
        End If

        MsgBox("remove password expiry")
        sRet = cWindowsUser.RemovePasswordExpiry(cADMIN_Username)
        If sRet = "ok" Then
            MsgBox("ok")
        Else
            MsgBox("error:")
            MsgBox(sRet)
        End If
    End Sub
End Class
