﻿Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement

Public Class cWindowsUser
    Private Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000

    Private Const SID_USERS As String = "S-1-5-32-545"
    Private Const SID_ADMINISTRATORS As String = "S-1-5-32-544"

    Public Shared Function DoesUserExist(sUsername As String) As Boolean
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                Return True
            End If
        Catch ex As Exception

        End Try

        Return False
    End Function

    Public Shared Function IsUserAdmin(sUsername As String) As Boolean
        Try
            Dim DC = New PrincipalContext(ContextType.Machine)

            Dim user = UserPrincipal.FindByIdentity(DC, sUsername)
            Dim groups = user.GetGroups()
            Dim adminGroup As String = GetAdministratorsGroupName()

            For Each group As Principal In groups
                If group.Name = adminGroup Then
                    Return True
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return False
    End Function

    Public Shared Function AddAdminUser(ByVal sUsername As String, ByVal sPassWord As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim NewUser As DirectoryEntry = AD.Children.Add(sUsername, "user")
            NewUser.Invoke("SetPassword", New Object() {sPassWord})
            NewUser.Invoke("Put", New Object() {"Description", "GuestTek admin user"})
            NewUser.CommitChanges()

            Dim grp As DirectoryEntry, sGrp As String = GetAdministratorsGroupName()

            If sGrp = "" Then
                sGrp = "Administrators"
            End If

            grp = AD.Children.Find(sGrp, "group")
            If grp.Name <> "" Then
                grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
            Else
                Return "group failed"
            End If

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function AddUser(ByVal sUsername As String, ByVal sPassWord As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim NewUser As DirectoryEntry = AD.Children.Add(sUsername, "user")
            NewUser.Invoke("SetPassword", New Object() {sPassWord})
            NewUser.Invoke("Put", New Object() {"Description", "GuestTek admin user"})
            NewUser.CommitChanges()

            Dim grp As DirectoryEntry, sGrp As String = GetUsersGroupName()

            If sGrp = "" Then
                sGrp = "Users"
            End If

            grp = AD.Children.Find(sGrp, "group")
            If grp.Name <> "" Then
                grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
            End If

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function ChangePassword(ByVal sUsername As String, ByVal sPassWord As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                usr.Invoke("SetPassword", New Object() {sPassWord})
                usr.CommitChanges()

                Return "ok"
            Else
                Return "not found"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function RemovePasswordExpiry(ByVal sUsername As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                usr.Properties("UserFlags").Value = ADS_UF_DONT_EXPIRE_PASSWD
                usr.CommitChanges()

                Return "ok"
            Else
                Return "not found"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function PromoteUserToAdmin(sUsername As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                Dim grp As DirectoryEntry, sGrp As String = GetAdministratorsGroupName()

                If sGrp = "" Then
                    sGrp = "Administrators"
                End If

                grp = AD.Children.Find(sGrp, "group")
                If grp.Name <> "" Then
                    grp.Invoke("Add", New Object() {usr.Path.ToString()})
                End If
            End If

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function GetAdministratorsGroupName() As String
        Return GetGroupNameFromSid(SID_ADMINISTRATORS)
    End Function

    Public Shared Function GetUsersGroupName() As String
        Return GetGroupNameFromSid(SID_USERS)
    End Function

    Private Shared Function GetGroupNameFromSid(ByVal _sid As String) As String
        Try
            Dim context As PrincipalContext, group As GroupPrincipal

            context = New PrincipalContext(ContextType.Machine)
            group = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, _sid)

            Return group.SamAccountName
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function HideUserInLogonScreen(ByVal sUsername As String) As Boolean
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", sUsername, 0)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
