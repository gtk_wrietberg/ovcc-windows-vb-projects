﻿Public Class PerPixelAlphaLayeredForm
    Public Sub New()
        Me.New(Nothing)
    End Sub

    Public Sub New(bitmap As Bitmap)
        InitializeComponent()
        Me.LayerBitmap = bitmap
    End Sub

    Private ReadOnly Property LayerBitmap As Bitmap

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        If Me.LayerBitmap IsNot Nothing Then
            Me.ClientSize = Me.LayerBitmap.Size
            'Dim screenSize = Screen.FromHandle(Me.Handle).Bounds.Size
            ' Me.Location = New Point((screenSize.Width - Me.Width) \ 2, (screenSize.Height - Me.Height) \ 2)
            SelectBitmap(Me.LayerBitmap)
        End If
        Me.TopMost = True
    End Sub

    Protected Overrides Sub OnFormClosed(e As FormClosedEventArgs)
        Me.LayerBitmap?.Dispose()
        MyBase.OnFormClosed(e)
    End Sub

    Private Sub SelectBitmap(bitmap As Bitmap)
        NativeMethods.SelectBitmapToControl(Me.PictureBox1, bitmap, 255)
    End Sub

    Protected Overrides ReadOnly Property CreateParams As CreateParams
        Get
            Dim parms As CreateParams = MyBase.CreateParams
            If Not DesignMode Then parms.ExStyle = parms.ExStyle Or NativeMethods.WS_EX_LAYERED
            Return parms
        End Get
    End Property

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = NativeMethods.WM_NCHITTEST Then
            m.Result = New IntPtr(NativeMethods.HTCAPTION)
        Else
            MyBase.WndProc(m)
        End If
    End Sub
End Class