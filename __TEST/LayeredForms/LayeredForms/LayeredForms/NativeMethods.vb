﻿Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices

Friend Class NativeMethods
    Public Const HTCAPTION As Integer = &H2
    Public Const WM_PAINT = &HF
    Public Const WM_NCHITTEST As Integer = &H84
    Public Const WS_EX_LAYERED As Integer = &H80000

    Public Const AC_SRC_OVER As Byte = 0
    Public Const AC_SRC_ALPHA As Byte = 1

    <Flags>
    Friend Enum ULWFlags
        ULW_COLORKEY = &H1
        ULW_ALPHA = &H2
        ULW_OPAQUE = &H4
        ULW_EX_NORESIZE = &H8
    End Enum

    <StructLayout(LayoutKind.Sequential)>
    Friend Structure POINT
        Public x As Integer
        Public y As Integer
        Public Sub New(X As Integer, Y As Integer)
            Me.x = X
            Me.y = Y
        End Sub
    End Structure

    <StructLayout(LayoutKind.Sequential)>
    Friend Structure SIZE
        Public cx As Integer
        Public cy As Integer
        Public Sub New(cX As Integer, cY As Integer)
            Me.cx = cX
            Me.cy = cY
        End Sub
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)>
    Friend Structure ARGB
        Public Blue As Byte
        Public Green As Byte
        Public Red As Byte
        Public Alpha As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)>
    Friend Structure BLENDFUNCTION
        Public BlendOp As Byte
        Public BlendFlags As Byte
        Public SourceConstantAlpha As Byte
        Public AlphaFormat As Byte
    End Structure

    <DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Friend Shared Function UpdateLayeredWindow(hWnd As IntPtr, hdcDst As IntPtr, ByRef pptDst As POINT,
        ByRef psize As SIZE, hdcSrc As IntPtr, ByRef pprSrc As POINT, crKey As Integer,
        ByRef pblend As BLENDFUNCTION, dwFlags As ULWFlags) As Boolean
    End Function

    <DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Friend Shared Function SetLayeredWindowAttributes(hWnd As IntPtr, crKey As Integer,
        bAlpha As Byte, dwFlags As ULWFlags) As Boolean
    End Function

    <DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Friend Shared Function ReleaseDC(hWnd As IntPtr, hDC As IntPtr) As Integer
    End Function

    <DllImport("gdi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Friend Shared Function CreateCompatibleDC(hDC As IntPtr) As IntPtr
    End Function

    <DllImport("user32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Friend Shared Function GetDC(hWnd As IntPtr) As IntPtr
    End Function

    <DllImport("gdi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Friend Shared Function DeleteDC(hdc As IntPtr) As Boolean
    End Function

    <DllImport("gdi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Friend Shared Function SelectObject(hDC As IntPtr, hObject As IntPtr) As IntPtr
    End Function

    <DllImport("gdi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)>
    Friend Shared Function DeleteObject(hObject As IntPtr) As Boolean
    End Function

    Public Shared Sub SelectBitmapToControl(control As Control, bitmap As Bitmap, opacity As Integer)

        If bitmap.PixelFormat <> PixelFormat.Format32bppArgb Then
            Throw New ApplicationException("The bitmap must be 32bpp with alpha-channel.")
        End If

        Dim screenDc As IntPtr = GetDC(IntPtr.Zero)
        Dim sourceDc As IntPtr = CreateCompatibleDC(screenDc)
        Dim hBitmap As IntPtr = IntPtr.Zero
        Dim hOldBitmap As IntPtr = IntPtr.Zero

        Try
            ' Get handle to the New bitmap and select it into the current device context.
            hBitmap = bitmap.GetHbitmap(Color.FromArgb(0))
            hOldBitmap = SelectObject(sourceDc, hBitmap)

            Dim windowLocation As New POINT(control.Left, control.Top)
            Dim windowSize As New SIZE(bitmap.Width, bitmap.Height)
            Dim sourceLocation As New POINT(0, 0)
            Dim blend As New BLENDFUNCTION() With {
                .BlendOp = AC_SRC_OVER,
                .BlendFlags = 0,
                .SourceConstantAlpha = CType(opacity, Byte),
                .AlphaFormat = AC_SRC_ALPHA
            }

            ' Update the window.
            ' Handle =>         Handle to the layered window
            ' screenDc =>       Handle to the screen DC
            ' windowLocation => Screen position of the layered window
            ' windowSize =>     SIZE of the layered window
            ' sourceDc =>       Handle to the layered window surface DC
            ' sourceLocation => Location of the layer in the DC
            ' 0 =>              Color key of the layered window
            ' blend =>          Transparency of the layered window
            ' ULW_ALPHA =>      Use blend as the blend function
            UpdateLayeredWindow(control.Handle, screenDc, windowLocation, windowSize,
                                sourceDc, sourceLocation, 0, blend, ULWFlags.ULW_ALPHA)
        Finally
            ' Release device context.
            ReleaseDC(IntPtr.Zero, screenDc)
            If hBitmap <> IntPtr.Zero Then
                SelectObject(sourceDc, hOldBitmap)
                DeleteObject(hBitmap)
            End If
            DeleteDC(sourceDc)
        End Try
    End Sub
End Class