Public Class Form1

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim w As Window

        w = GetWindowText

        TextBox1.Clear()

        TextBox1.AppendText(w.Handle.ToInt32.ToString() & vbCrLf)
        TextBox1.AppendText(w.Text & vbCrLf)
        TextBox1.AppendText(w.ClassName & vbCrLf)
        TextBox1.Update()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Button1.Enabled = False
        Button2.Enabled = True
        Timer1.Enabled = True
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Button1.Enabled = True
        Button2.Enabled = False
        Timer1.Enabled = False
    End Sub
End Class
