Module Module1
    ' Imports
    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Function GetForegroundWindow() As IntPtr
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Function GetWindowText(ByVal hWnd As IntPtr, ByVal lpWindowText As System.Text.StringBuilder, _
    ByVal nMaxCount As Integer) As Integer
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Function GetClassName(ByVal hWnd As IntPtr, ByVal lpWindowText As System.Text.StringBuilder, _
    ByVal nMaxCount As Integer) As Integer
    End Function

    ' /Imports

    ''' <summary>
    ''' Get's the handle and text of the foreground window
    ''' </summary>
    Public Function GetWindowText() As Window
        Dim classname As New System.Text.StringBuilder(255)
        Dim classnameLength As Integer = GetClassName(GetForegroundWindow(), classname, classname.Capacity + 1)
        classname.Length = classnameLength

        Dim title As New System.Text.StringBuilder(255)
        Dim titleLength As Integer = GetWindowText(GetForegroundWindow(), title, title.Capacity + 1)
        title.Length = titleLength

        Dim w As New Window
        w.Text = title.ToString()
        w.ClassName = classname.ToString
        w.Handle = GetForegroundWindow()
        Return w
    End Function

    ''' <summary>
    ''' Represents an open, visible window
    ''' </summary>
    Public Structure Window
        Public Handle As IntPtr
        Public Text As String
        Public ClassName As String
    End Structure
End Module
