﻿Imports System.Runtime.InteropServices
Imports System.Text

Public Class GetApplicationForExtension
    <DllImport("Shlwapi.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Private Shared Function AssocQueryString(ByVal flags As UInteger, ByVal str As UInteger, ByVal pszAssoc As String, ByVal pszExtra As String, ByVal pszOut As StringBuilder, ByRef pcchOut As UInteger) As UInteger
    End Function

    Enum AssocF As UInteger
        None = 0
        Init_NoRemapCLSID = &H1
        Init_ByExeName = &H2
        Open_ByExeName = &H2
        Init_DefaultToStar = &H4
        Init_DefaultToFolder = &H8
        NoUserSettings = &H10
        NoTruncate = &H20
        Verify = &H40
        RemapRunDll = &H80
        NoFixUps = &H100
        IgnoreBaseClass = &H200
        Init_IgnoreUnknown = &H400
        Init_FixedProgId = &H800
        IsProtocol = &H1000
        InitForFile = &H2000
    End Enum

    Enum AssocStr
        Command = 1
        Executable
        FriendlyDocName
        FriendlyAppName
        NoOpen
        ShellNewValue
        DDECommand
        DDEIfExec
        DDEApplication
        DDETopic
        InfoTip
        QuickTip
        TileInfo
        ContentType
        DefaultIcon
        ShellExtension
        DropTarget
        DelegateExecute
        SupportedUriProtocols
        Max
    End Enum

    Private Shared Function AssocQueryString(association As AssocStr, extension As String) As String
        Const S_OK As Integer = 0
        Const S_FALSE As Integer = 1

        Dim length As UInteger = 0
        Dim ret As UInteger = AssocQueryString(AssocF.None, association, extension, Nothing, Nothing, length)
        If ret <> S_FALSE Then
            Throw New InvalidOperationException("Could not determine associated string (1 ; {" & ret.ToString() & "})")
        End If

        Dim sb = New StringBuilder(CInt(length))
        ' (length-1) will probably work too as the marshaller adds null termination
        ret = AssocQueryString(AssocF.None, association, extension, Nothing, sb, length)
        If ret <> S_OK Then
            Throw New InvalidOperationException("Could not determine associated string (2 ; {" & ret.ToString() & "})")
        End If

        Return sb.ToString()
    End Function

    Public Shared Function GetExecutable(extension As String, ByRef application As String) As Boolean
        Try
            application = AssocQueryString(AssocStr.Executable, extension)

            Return True
        Catch ex As Exception
            application = ex.Message
        End Try

        Return False
    End Function

    Public Shared Function GetName(extension As String, ByRef name As String) As Boolean
        Try
            name = AssocQueryString(AssocStr.FriendlyAppName, extension)

            Return True
        Catch ex As Exception
            name = ex.Message
        End Try

        Return False
    End Function

End Class
