﻿Imports Microsoft.Win32
Imports System.IO

Public Class OfficeDetection
    ''' Possible Office apps.
    Enum MSOfficeApp
        Access_Application
        Excel_Application
        Outlook_Application
        PowerPoint_Application
        Word_Application
        FrontPage_Application
    End Enum

    ''' Possible versions
    Enum Version
        Version2007 = 12
        Version2003 = 11
        Version2002 = 10
        Version2000 = 9
        Version97 = 8
        Version95 = 7
    End Enum


    Public Shared Sub ListAllOfficeVersions()
        Dim strApp As String = Nothing

        For Each s As String In [Enum].GetNames(GetType(MSOfficeApp))
            If Not IsNothing(GetComponentPath(CType([Enum].Parse(GetType(MSOfficeApp), s), MSOfficeApp), True)) Then
                strApp = GetComponentPath(CType([Enum].Parse(GetType(MSOfficeApp), s), MSOfficeApp), True).ToString
            Else
                strApp = "Not Found Err."
            End If

            If File.Exists((strApp).ToUpper) Then
                Dim _fileVersion As FileVersionInfo = FileVersionInfo.GetVersionInfo(strApp.ToUpper)
                Debug.Print(s & vbTab & GetVersionsString(CType([Enum].Parse(GetType(MSOfficeApp), s), MSOfficeApp)))
                Debug.Print("App Path Full = " & (strApp).ToUpper)
                Debug.Print("App Exists: " & _fileVersion.ToString)
            Else
                Debug.Print(s & vbTab & GetVersionsString(CType([Enum].Parse(GetType(MSOfficeApp), s), MSOfficeApp)))
            End If

            Debug.Print(vbCrLf & "=================" & vbCrLf)

        Next
    End Sub

    Public Shared Sub ListSpecificOfficeVersion(ByVal MsApplication As MSOfficeApp)
        Debug.Print(GetComponentPath(MsApplication))
        Dim strApp As String = Nothing
        If Not IsNothing(GetComponentPath(MsApplication, True)) Then
            strApp = GetComponentPath(MsApplication, True).ToString
        Else
            strApp = "Not Found Err."
        End If


        If File.Exists((strApp).ToUpper) Then
            Dim _fileVersion As FileVersionInfo = FileVersionInfo.GetVersionInfo(strApp.ToUpper)
            Debug.Print(vbTab & GetVersionsString(MsApplication))
            Debug.Print("App Path Full = " & (strApp).ToUpper)
            Debug.Print("App Exists: " & _fileVersion.ToString)
        Else
            Debug.Print(vbTab & GetVersionsString(MsApplication))
        End If

        Debug.Print(vbCrLf & "=================" & vbCrLf)

    End Sub


    ''' Returns version number as integer
    ''' Value is 0 if no version could be detected
    Public Shared Function GetVersionsID(ByVal app As MSOfficeApp) As Integer
        Dim strProgID As String = [Enum].GetName(GetType(MSOfficeApp), app)
        strProgID = Replace(strProgID, "_", ".")
        Dim regKey As RegistryKey
        regKey = Registry.LocalMachine.OpenSubKey("Software\Classes\" & strProgID & "\CurVer", False)
        If IsNothing(regKey) Then Return 0
        Dim strV As String = CStr(regKey.GetValue("", Nothing, RegistryValueOptions.None))
        Debug.Print(strV)
        regKey.Close()

        strV = Replace(Replace(strV, strProgID, ""), ".", "")
        Return CInt(strV)
    End Function


    ''' Returns the version string
    Public Shared Function GetVersionsString(ByVal app As MSOfficeApp) As String
        Dim strProgID As String = [Enum].GetName(GetType(MSOfficeApp), app)
        strProgID = Replace(strProgID, "_", ".")
        Dim regKey As RegistryKey
        regKey = Registry.LocalMachine.OpenSubKey("Software\Classes\" & strProgID & "\CurVer", False)
        'Debug.Print(regKey.ToString)
        If IsNothing(regKey) Then Return "No version detected."
        Dim strV As String = CStr(regKey.GetValue("", Nothing, RegistryValueOptions.None))
        'Debug.Print(strV)
        regKey.Close()

        strV = Replace(Replace(strV, strProgID, ""), ".", "")
        Return [Enum].GetName(GetType(Version), CInt(strV))
    End Function


    Private Shared Function GetComponentPath(ByVal _component As MSOfficeApp, Optional ByVal blnFullPath As Boolean = False) As String
        'code in c# by Niskov from codeproject.com
        'http://www.codeproject.com/script/Membership/View.aspx?mid=4840737
        '
        'translated and modified to vb.net by aalvarez of Dane-Elec
        '=======================================================

        Const RegKey As String = "Software\Microsoft\Windows\CurrentVersion\App Paths"
        Dim toReturn As String = Nothing
        Dim _key As String = Nothing

        Select Case _component
            Case MSOfficeApp.Word_Application
                _key = "winword.exe"

            Case MSOfficeApp.Excel_Application
                _key = "excel.exe"

            Case MSOfficeApp.PowerPoint_Application
                _key = "powerpnt.exe"

            Case MSOfficeApp.Outlook_Application
                _key = "outlook.exe"

            Case MSOfficeApp.Access_Application
                _key = "MSACCESS.exe"

            Case MSOfficeApp.FrontPage_Application
                _key = "FrontPg.exe"

        End Select

        _key = _key.ToUpper

        'looks inside CURRENT_USER:
        Dim _mainKey As RegistryKey = Registry.CurrentUser

        Try
            _mainKey = _mainKey.OpenSubKey(RegKey & "\" & _key, False)
            If _mainKey IsNot Nothing Then
                toReturn = _mainKey.GetValue(String.Empty).ToString()
                If blnFullPath Then toReturn = toReturn & _key
            End If
        Catch
        End Try

        'if not found, looks inside LOCAL_MACHINE:
        _mainKey = Registry.LocalMachine
        If String.IsNullOrEmpty(toReturn) Then
            Try
                _mainKey = _mainKey.OpenSubKey(RegKey & "\" & _key, False)
                If _mainKey IsNot Nothing Then
                    toReturn = _mainKey.GetValue("Path").ToString()
                    If blnFullPath Then toReturn = toReturn & _key
                End If
            Catch
            End Try
        End If

        'closing the handle:
        If _mainKey IsNot Nothing Then
            _mainKey.Close()
        End If

        Return toReturn

    End Function
End Class
