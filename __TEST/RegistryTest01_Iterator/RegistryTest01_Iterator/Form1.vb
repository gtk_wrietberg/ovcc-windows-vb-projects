Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Function GetUninstallCommand(ByVal sSearchForApplication As String) As String
        Dim KeyName As String
        Dim Value_DisplayName As String
        Dim Value_UninstallString As String = ""
        Dim UninstallRegKey As Microsoft.Win32.RegistryKey
        Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

        UninstallRegKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", False)

        If sSearchForApplication = "" Then
            'Yes that's right
            sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
        End If

        sSearchForApplication = sSearchForApplication.ToLower

        For Each KeyName In UninstallRegKey.GetSubKeyNames
            ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

            Try
                Value_DisplayName = ApplicationRegKey.GetValue("DisplayName", "").ToString.ToLower
            Catch ex As Exception
                Value_DisplayName = ""
            End Try

            If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                Try
                    Value_UninstallString = ApplicationRegKey.GetValue("UninstallString", "").ToString
                Catch ex As Exception
                    Value_UninstallString = ""
                End Try

                Value_UninstallString = System.Text.RegularExpressions.Regex.Replace(Value_UninstallString, ".*\{(.+)\}.*", "$1")

                Exit For
            End If
        Next

        Return Value_UninstallString
    End Function

    Private Function GetUninstallCommand2(ByVal sSearchForApplication As String) As String
        Dim KeyName As String
        Dim Value_DisplayName As String
        Dim Value_UninstallString As String = ""
        Dim UninstallRegKey As Microsoft.Win32.RegistryKey
        Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

        UninstallRegKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Installer\Products", False)

        If sSearchForApplication = "" Then
            'Yes that's right
            sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
        End If

        sSearchForApplication = sSearchForApplication.ToLower

        For Each KeyName In UninstallRegKey.GetSubKeyNames
            ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

            Try
                Value_DisplayName = ApplicationRegKey.GetValue("ProductName", "").ToString.ToLower
            Catch ex As Exception
                Value_DisplayName = ""
            End Try

            If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                Try
                    Value_UninstallString = KeyName
                Catch ex As Exception
                    Value_UninstallString = ""
                End Try

                Exit For
            End If
        Next

        Return Value_UninstallString
    End Function


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'MsgBox("_" & GetUninstallCommand(TextBox1.Text) & "-")
        TextBox2.Text = GetUninstallCommand(TextBox1.Text)
        TextBox3.Text = GetUninstallCommand2(TextBox1.Text)
    End Sub
End Class
