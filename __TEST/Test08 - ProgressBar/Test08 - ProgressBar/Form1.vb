Public Class Form1
    Private dFakeProgressCount As Double
    Private dFakeProgressCountStep As Double
    Private dFakeProgress As Double

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dFakeProgressCount = 0
        dFakeProgressCountStep = 1
    End Sub

    Private Sub tmrFakeProgressBar_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrFakeProgressBar.Tick
        dFakeProgressCount += dFakeProgressCountStep

        dFakeProgress = (dFakeProgressCount ^ 2 + dFakeProgressCount) / (dFakeProgressCount ^ 2 + 5 * dFakeProgressCount + 10000)

        dFakeProgress = dFakeProgress * 100

        TextBox1.Text = CStr(dFakeProgressCount)
        TextBox2.Text = CStr(dFakeProgress)

        If dFakeProgress < 0 Then
            ProgressBar1.Value = 0
        ElseIf dFakeProgress > 100 Then
            ProgressBar1.Value = 100
        Else
            ProgressBar1.Value = dFakeProgress
        End If
    End Sub

    '600-600/lfakeprogresscount

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If tmrFakeProgressBar.Enabled Then
            tmrFakeProgressBar.Enabled = False
        Else
            dFakeProgressCount = 0

            tmrFakeProgressBar.Enabled = True
        End If
    End Sub
End Class
