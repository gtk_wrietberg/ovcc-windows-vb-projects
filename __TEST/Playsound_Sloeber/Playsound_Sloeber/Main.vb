﻿Imports System.Threading

Module Main
    Private mDebug As Boolean = False
    Private mThread_Playsound As Thread

    Private ReadOnly c_PARAM__NAME As String = "--name="
    Private ReadOnly c_PARAM__COUNT As String = "--count="
    Private ReadOnly c_PARAM__PAUSE As String = "--pause="
    Private ReadOnly c_PARAM__DEBUG As String = "--debug"


    Public Sub Main()
        Dim sSound As String = "", iCount As Integer = 1, iPause As Integer = 0


        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals(c_PARAM__DEBUG) Then
                mDebug = True
                _debug("debugging on")
            End If

            If arg.StartsWith(c_PARAM__NAME) Then
                sSound = arg.Replace(c_PARAM__NAME, "")
            End If

            If arg.StartsWith(c_PARAM__COUNT) Then
                If Not Integer.TryParse(arg.Replace(c_PARAM__COUNT, ""), iCount) Then
                    iCount = 1
                End If
            End If

            If arg.StartsWith(c_PARAM__PAUSE) Then
                If Not Integer.TryParse(arg.Replace(c_PARAM__PAUSE, ""), iPause) Then
                    iPause = 0
                End If
            End If
        Next

        If My.Resources.ResourceManager.GetObject(sSound) Is Nothing Then
            'resource not found, just leave
            _debug("resource '" & sSound & "' not found!")
        Else
            _debug("starting thread")

            mThread_Playsound = New Thread(Sub() _PlaySound(sSound, iCount, iPause))
            mThread_Playsound.Start()
        End If
    End Sub

    Private Sub _PlaySound(sName As String, iCount As Integer, iPause As Integer)
        _debug("thread started")

        _debug("name : " & sName)
        _debug("count: " & iCount.ToString)
        _debug("pause: " & iPause.ToString)


        If iCount > 10 Then
            _debug("count too high, set to 10")
            iCount = 10 'yeah, just to be sure
        End If
        If iCount < 1 Then
            _debug("count less than 1, set to 1")
            iCount = 1
        End If
        If iPause > 5000 Then
            _debug("pause too high, set to 5000")
            iPause = 5000
        End If
        If iPause < 0 Then
            _debug("pause less than 0, set to 0")
            iPause = 0
        End If

        Try
            For i As Integer = 1 To iCount
                _debug("Playing '" & sName & "' (" & i.ToString & "/" & iCount.ToString & ")")
                My.Computer.Audio.Play(My.Resources.ResourceManager.GetStream(sName), AudioPlayMode.WaitToComplete)

                If iPause > 0 And i < iCount Then
                    _debug("Pausing for " & iPause & "ms (" & i.ToString & "/" & iCount.ToString & ")")

                    Thread.Sleep(iPause)
                Else
                    _debug("Pausing skipped (" & i.ToString & "/" & iCount.ToString & ")")
                End If
            Next
        Catch ex As Exception
            _debug("exception: " & ex.Message)
        End Try

        _debug("thread ended")
    End Sub

    Private Sub _debug(s As String)
        If mDebug Then
            Console.WriteLine(_timestamp().ToString & " - " & s)
        End If
    End Sub

    Private ReadOnly c_ONEJANUARYNINETEENSEVENTY As DateTime = New DateTime(1970, 1, 1)
    Private Function _timestamp() As Long
        Return (DateTime.Now - c_ONEJANUARYNINETEENSEVENTY).TotalMilliseconds
    End Function
End Module
