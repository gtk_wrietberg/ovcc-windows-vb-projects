using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using PluginBaseLib;
using SiteKioskRuntimeLib;

namespace SiteKioskController
{
	public partial class k_Form : Form
	{
		k_SiteKioskController mk_Controller;

		public k_Form()
		{
			InitializeComponent();
		}

		private void StatusTmr_Tick(object sender, EventArgs e)
		{
			try
			{
				UpdateStats();
			}
			catch (Exception)
			{}
		}

        private void UpdateSKStatus()
        {
            StringBuilder ls_skStatus = new StringBuilder();

            mk_Controller = new k_SiteKioskController();

            if (mk_Controller.SiteKiosk != null)
                ls_skStatus.Append("SiteKiosk is running!");
            else
                ls_skStatus.Append("SiteKiosk is not running!");

            skstatuslbl.Text = ls_skStatus.ToString();

            UpdateStats();
        }

		private void UpdateStats()
		{
			StringBuilder ls_Status = new StringBuilder();
			if (mk_Controller.SiteCash == null || !(mk_Controller.SiteCash as IPluginBase).Enabled)
			{
				ls_Status.Append("SiteCash needs to be installed and enabled!");
			}
			else
			{
				ls_Status.Append("Balance: ");
				ls_Status.Append(mk_Controller.SiteCash.CurrentBalance);
			}

			StatusLbl.Text = ls_Status.ToString();
		}

		private void CreditEdt_Click(object sender, EventArgs e)
		{
			try
            {
                decimal ld_Amount = 0.0M;
			    if (decimal.TryParse(CreditDebitEdt.Text, out ld_Amount))
				    mk_Controller.SiteCash.Credit(ld_Amount);

			    UpdateStats();
            }
            catch (Exception)
            {
                MessageBox.Show("Error! Most likely SiteKiosk is not running or the connection has not been initialized! Please click on the Check button.");
            }
		}

		private void DebitEdt_Click(object sender, EventArgs e)
		{
			try
            {
                decimal ld_Amount = 0.0M;
			    if (decimal.TryParse(CreditDebitEdt.Text, out ld_Amount))
				    mk_Controller.SiteCash.Debit(ld_Amount);

			    UpdateStats();
            }
            catch (Exception)
            {
                MessageBox.Show("Error! Most likely SiteKiosk is not running or the connection has not been initialized! Please click on the Check button.");
            }
		}

		private void NavBtn_Click(object sender, EventArgs e)
		{
            try
            {
                IWindowInfo lk_Window = mk_Controller.WindowList.MainWindow as IWindowInfo;
                ISiteKioskWindow lk_SKWindow = lk_Window.SiteKioskWindow as ISiteKioskWindow;
                (lk_SKWindow.SiteKioskWebBrowser as ISiteKioskWebBrowser).Navigate(NavEdt.Text, true);
            }
            catch (Exception)
            {
                MessageBox.Show("Error! Most likely SiteKiosk is not running or you are using a SiteKiosk IE-based browser or the connection has not been initialized! Please click on the Check button.");
            }
		}

        private void CheckSKBtn_Click(object sender, EventArgs e)
        {
            UpdateSKStatus();
        }

        private void WriteToSKLog_Click(object sender, EventArgs e)
        {
            try
            {
                dynamic lk_SKLog = mk_Controller.SiteKiosk.Logfile;
                lk_SKLog.Write(0000, 20, "TEST", "A test message from the SiteKioskController C# example!");
            }
            catch (Exception)
            {
                MessageBox.Show("Error! Most likely SiteKiosk is not running or the connection has not been initialized! Please click on the Check button.");
            }
        }
	}
}