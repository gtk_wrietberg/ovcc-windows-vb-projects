using System.Runtime.InteropServices;
using SiteKioskRuntimeLib;

namespace SiteKioskController
{
	partial class k_Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.StatusTxtLbl = new System.Windows.Forms.Label();
            this.StatusLbl = new System.Windows.Forms.Label();
            this.StatusTmr = new System.Windows.Forms.Timer(this.components);
            this.CreditDebitEdt = new System.Windows.Forms.TextBox();
            this.CreditEdt = new System.Windows.Forms.Button();
            this.DebitEdt = new System.Windows.Forms.Button();
            this.NavEdt = new System.Windows.Forms.TextBox();
            this.NavBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.skstatuslbl = new System.Windows.Forms.Label();
            this.CheckSKBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // StatusTxtLbl
            // 
            this.StatusTxtLbl.AutoSize = true;
            this.StatusTxtLbl.Location = new System.Drawing.Point(12, 10);
            this.StatusTxtLbl.Name = "StatusTxtLbl";
            this.StatusTxtLbl.Size = new System.Drawing.Size(87, 13);
            this.StatusTxtLbl.TabIndex = 1;
            this.StatusTxtLbl.Text = "SiteKiosk Status:";
            // 
            // StatusLbl
            // 
            this.StatusLbl.AutoSize = true;
            this.StatusLbl.Location = new System.Drawing.Point(12, 113);
            this.StatusLbl.Name = "StatusLbl";
            this.StatusLbl.Size = new System.Drawing.Size(73, 13);
            this.StatusLbl.TabIndex = 2;
            this.StatusLbl.Text = "Balance: 0.00";
            // 
            // StatusTmr
            // 
            this.StatusTmr.Enabled = true;
            this.StatusTmr.Interval = 1000;
            this.StatusTmr.Tick += new System.EventHandler(this.StatusTmr_Tick);
            // 
            // CreditDebitEdt
            // 
            this.CreditDebitEdt.Location = new System.Drawing.Point(15, 136);
            this.CreditDebitEdt.Name = "CreditDebitEdt";
            this.CreditDebitEdt.Size = new System.Drawing.Size(130, 20);
            this.CreditDebitEdt.TabIndex = 3;
            this.CreditDebitEdt.Text = "0.00";
            // 
            // CreditEdt
            // 
            this.CreditEdt.Location = new System.Drawing.Point(151, 135);
            this.CreditEdt.Name = "CreditEdt";
            this.CreditEdt.Size = new System.Drawing.Size(47, 23);
            this.CreditEdt.TabIndex = 4;
            this.CreditEdt.Text = "Credit";
            this.CreditEdt.UseVisualStyleBackColor = true;
            this.CreditEdt.Click += new System.EventHandler(this.CreditEdt_Click);
            // 
            // DebitEdt
            // 
            this.DebitEdt.Location = new System.Drawing.Point(204, 135);
            this.DebitEdt.Name = "DebitEdt";
            this.DebitEdt.Size = new System.Drawing.Size(75, 23);
            this.DebitEdt.TabIndex = 5;
            this.DebitEdt.Text = "Debit";
            this.DebitEdt.UseVisualStyleBackColor = true;
            this.DebitEdt.Click += new System.EventHandler(this.DebitEdt_Click);
            // 
            // NavEdt
            // 
            this.NavEdt.Location = new System.Drawing.Point(15, 205);
            this.NavEdt.Name = "NavEdt";
            this.NavEdt.Size = new System.Drawing.Size(265, 20);
            this.NavEdt.TabIndex = 6;
            this.NavEdt.Text = "http://";
            // 
            // NavBtn
            // 
            this.NavBtn.Location = new System.Drawing.Point(209, 231);
            this.NavBtn.Name = "NavBtn";
            this.NavBtn.Size = new System.Drawing.Size(71, 22);
            this.NavBtn.TabIndex = 7;
            this.NavBtn.Text = "Navigate";
            this.NavBtn.UseVisualStyleBackColor = true;
            this.NavBtn.Click += new System.EventHandler(this.NavBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 186);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Navigate requires a SiteKiosk IE browser:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(231, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Requires enabled payment options in SiteKiosk:";
            // 
            // skstatuslbl
            // 
            this.skstatuslbl.AutoSize = true;
            this.skstatuslbl.Location = new System.Drawing.Point(103, 10);
            this.skstatuslbl.Name = "skstatuslbl";
            this.skstatuslbl.Size = new System.Drawing.Size(16, 13);
            this.skstatuslbl.TabIndex = 11;
            this.skstatuslbl.Text = "...";
            // 
            // CheckSKBtn
            // 
            this.CheckSKBtn.Location = new System.Drawing.Point(227, 5);
            this.CheckSKBtn.Name = "CheckSKBtn";
            this.CheckSKBtn.Size = new System.Drawing.Size(54, 23);
            this.CheckSKBtn.TabIndex = 12;
            this.CheckSKBtn.Text = "Check";
            this.CheckSKBtn.UseVisualStyleBackColor = true;
            this.CheckSKBtn.Click += new System.EventHandler(this.CheckSKBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Write test message to SiteKiosk logs:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(201, 50);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Write to log";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.WriteToSKLog_Click);
            // 
            // label4
            // 
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(14, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(266, 2);
            this.label4.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label5.Location = new System.Drawing.Point(13, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(266, 2);
            this.label5.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(13, 176);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(266, 2);
            this.label6.TabIndex = 17;
            // 
            // k_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CheckSKBtn);
            this.Controls.Add(this.skstatuslbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.NavBtn);
            this.Controls.Add(this.NavEdt);
            this.Controls.Add(this.DebitEdt);
            this.Controls.Add(this.CreditEdt);
            this.Controls.Add(this.CreditDebitEdt);
            this.Controls.Add(this.StatusLbl);
            this.Controls.Add(this.StatusTxtLbl);
            this.Name = "k_Form";
            this.Text = "SiteKiosk Controller";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label StatusTxtLbl;
		private System.Windows.Forms.Label StatusLbl;
		private System.Windows.Forms.Timer StatusTmr;
		private System.Windows.Forms.TextBox CreditDebitEdt;
		private System.Windows.Forms.Button CreditEdt;
		private System.Windows.Forms.Button DebitEdt;
		private System.Windows.Forms.TextBox NavEdt;
		private System.Windows.Forms.Button NavBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label skstatuslbl;
        private System.Windows.Forms.Button CheckSKBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
	}
}

