Module modParseTermsAndConditions
    Public Sub ParseTermsAndConditions(ByRef rtbTC As RichTextBox)
        Dim iStart As Integer, iEnd As Integer, iOffset As Integer
        Dim s As String = ""

        iOffset = 0
        iStart = -1

        s = rtbTC.Text
        s = s.Replace("&trade;", Chr(153))
        rtbTC.Text = s

        Do
            Try

                iStart = rtbTC.Text.IndexOf("<b>", iOffset)
                If iStart > -1 Then
                    iEnd = rtbTC.Text.IndexOf("</b>", iStart)
                    iOffset = iStart + 3

                    rtbTC.Select(iStart, (iEnd - iStart + 4))

                    s = rtbTC.SelectedText
                    s = s.Replace("<b>", "")
                    s = s.Replace("</b>", "")

                    rtbTC.SelectionFont = New Font(rtbTC.SelectionFont, FontStyle.Bold)
                    rtbTC.SelectedText = s
                End If

                iStart = rtbTC.Text.IndexOf("<c>", iOffset)
                If iStart > -1 Then
                    iEnd = rtbTC.Text.IndexOf("</c>", iStart)
                    iOffset = iStart + 3

                    rtbTC.Select(iStart, (iEnd - iStart + 4))

                    s = rtbTC.SelectedText
                    s = s.Replace("<c>", "")
                    s = s.Replace("</c>", "")

                    rtbTC.SelectionAlignment = HorizontalAlignment.Center
                    rtbTC.SelectedText = s
                End If

                iStart = rtbTC.Text.IndexOf("<cb>", iOffset)
                If iStart > -1 Then
                    iEnd = rtbTC.Text.IndexOf("</cb>", iStart)
                    iOffset = iStart + 4

                    rtbTC.Select(iStart, (iEnd - iStart + 5))

                    s = rtbTC.SelectedText
                    s = s.Replace("<cb>", "")
                    s = s.Replace("</cb>", "")

                    rtbTC.SelectionFont = New Font(rtbTC.SelectionFont, FontStyle.Bold)
                    rtbTC.SelectionAlignment = HorizontalAlignment.Center
                    rtbTC.SelectedText = s
                End If

            Catch ex As Exception

            End Try

        Loop While iStart > -1
    End Sub

End Module
