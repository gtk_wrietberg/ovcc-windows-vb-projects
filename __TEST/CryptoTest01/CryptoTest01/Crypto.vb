﻿Public Class Crypto
    Public Class SHA512
        Private Shared ReadOnly _sha1 As Security.Cryptography.SHA512 = Security.Cryptography.SHA512.Create()

        Public Shared Function GetHash(source As String) As String
            Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
            Dim sb As New System.Text.StringBuilder()

            Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

            Return sb.ToString()
        End Function

        Public Shared Function VerifyHash(source As String, hash As String) As Boolean
            Dim sourceHash = GetHash(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function

        Public Shared Function GetHashBase64(source As String) As String
            Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

            Return Convert.ToBase64String(data)
        End Function

        Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
            Dim sourceHash = GetHashBase64(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function
    End Class

    Public Class MD5
        Private Shared ReadOnly _md5 As Security.Cryptography.MD5 = Security.Cryptography.MD5.Create()

        Public Shared Function GetHash(source As String) As String
            Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
            Dim sb As New System.Text.StringBuilder()

            Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

            Return sb.ToString()
        End Function

        Public Shared Function VerifyHash(source As String, hash As String) As Boolean
            Dim sourceHash = GetHash(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function

        Public Shared Function GetHashBase64(source As String) As String
            Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

            Return Convert.ToBase64String(data)
        End Function

        Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
            Dim sourceHash = GetHashBase64(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function
    End Class
End Class
