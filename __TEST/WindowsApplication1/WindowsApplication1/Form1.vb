﻿Public Class Form1
    Public ReadOnly cParams__keep_this_java As String = "--keep:"
    Public ReadOnly cParams__go_for_it As String = "--go-for-it"

    Private mValue__uninstall_app As String

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        For Each sArg In My.Application.CommandLineArgs
            sArg = sArg.ToLower

            If InStr(sArg, cParams__keep_this_java) > 0 Then
                'mValue__uninstall_app = LettersAndDigitsOnly(sArg.Replace(cParams__keep_this_java, ""))
                mValue__uninstall_app = sArg.Replace(cParams__keep_this_java, "")

                MsgBox("Uninstall app: " & mValue__uninstall_app, , 2)
            End If

        Next
    End Sub

    Private Function LettersAndDigitsOnly(ByVal sString As String) As String
        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In sString
            If Char.IsLetterOrDigit(ch) Then
                sb.Append(ch)
            End If
        Next

        Return sb.ToString()
    End Function

End Class
