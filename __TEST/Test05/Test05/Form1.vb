Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim s As String
        Dim d As Decimal
        Dim dd As Double
        Dim sgncn As String

        s = TextBox1.Text

        sgncn = System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator

        If sgncn.Equals(".") Then
            s = s.Replace(",", ".")
        Else
            s = s.Replace(".", ",")
        End If

        MsgBox("1")
        Try
            dd = Double.Parse(s)
            d = New Decimal(dd)

            MsgBox("s='" & s & "' ; d=" & CStr(d))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        MsgBox("2")
        Try
            d = Decimal.Parse(s)

            MsgBox("s='" & s & "' ; d=" & CStr(d))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MsgBox(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim d As Date = Now

        MsgBox(d.ToString("yyyy\\MM\\dd"))
    End Sub
End Class
