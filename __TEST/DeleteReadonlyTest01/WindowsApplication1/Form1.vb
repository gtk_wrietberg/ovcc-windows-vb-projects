Public Class Form1
    Private ReadOnly cFolder As String = "C:\Documents and Settings\iBAHN"

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            MsgBox("delete")
            System.IO.Directory.Delete(cFolder, True)
            MsgBox("ok")
        Catch ex As Exception
            MsgBox("fail")
            Button2.Enabled = True
        End Try
    End Sub

    Private Sub ClearAttributes(ByVal sFolder As String)
        If System.IO.Directory.Exists(sFolder) Then
            Dim sSubFolders() As String = System.IO.Directory.GetDirectories(sFolder)
            Dim sSubFolder As String

            For Each sSubFolder In sSubFolders
                System.IO.File.SetAttributes(sSubFolder, IO.FileAttributes.Normal)
                ClearAttributes(sSubFolder)
            Next

            Dim sFiles() As String = System.IO.Directory.GetFiles(sFolder)
            Dim sFile As String

            For Each sFile In sFiles
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
            Next
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        ClearAttributes(cFolder)
    End Sub
End Class
