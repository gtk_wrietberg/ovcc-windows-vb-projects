﻿Imports System.Management

Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MsgBox("version: " & GetOSVersionAndCaption.Key & " _ " & GetOSVersionAndCaption.Value)
    End Sub


    Public Shared Function GetOSVersionAndCaption() As KeyValuePair(Of String, String)
        Dim kvpOSSpecs As New KeyValuePair(Of String, String)()
        Dim searcher As New ManagementObjectSearcher("SELECT Caption, Version FROM Win32_OperatingSystem")
        Try
            For Each os As Object In searcher.Get()
                Dim version = os("Version").ToString()
                Dim productName = os("Caption").ToString()
                kvpOSSpecs = New KeyValuePair(Of String, String)(productName, version)
            Next
        Catch
        End Try

        Return kvpOSSpecs
    End Function

    '=======================================================
    'Service provided by Telerik (www.telerik.com)
    'Conversion powered by NRefactory.
    'Twitter: @telerik
    'Facebook: facebook.com/telerik
    '=======================================================

End Class
