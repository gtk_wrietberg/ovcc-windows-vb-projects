Public Class ConnectState

    Private mConnectionTypeString As String

    Private Declare Function InternetGetConnectedState Lib "wininet.dll" (ByRef lpSFlags As Int32, ByVal dwReserved As Int32) As Boolean

    Public Enum InetConnState
        MODEM = &H1
        LAN = &H2
        PROXY = &H4
        RAS = &H10
        OFFLINE = &H20
        CONFIGURED = &H40
    End Enum

    Public ReadOnly Property ConnectionType() As String
        Get
            Return mConnectionTypeString
        End Get
    End Property

    Public Function CheckInetConnection() As Boolean

        Dim lngFlags As Long

        If InternetGetConnectedState(lngFlags, 0) Then
            If lngFlags And InetConnState.LAN Then
                mConnectionTypeString = "LAN"
                Return True
            ElseIf lngFlags And InetConnState.MODEM Then
                mConnectionTypeString = "MODEM"
                Return True
            ElseIf lngFlags And InetConnState.CONFIGURED Then
                mConnectionTypeString = "CONFIGURED"
                Return True
            ElseIf lngFlags And InetConnState.PROXY Then
                mConnectionTypeString = "PROXY"
                Return True
            ElseIf lngFlags And InetConnState.RAS Then
                mConnectionTypeString = "RAS"
                Return True
            ElseIf lngFlags And InetConnState.OFFLINE Then
                mConnectionTypeString = "OFFLINE"
                Return False
            End If
        Else
            mConnectionTypeString = "MODEM"
            Return False
        End If

    End Function

End Class
