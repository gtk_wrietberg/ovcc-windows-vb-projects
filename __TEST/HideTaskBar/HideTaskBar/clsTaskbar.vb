Imports System.Runtime.InteropServices

Public Class clsTaskBar
#Region "WIN API FUNCTIONS"
    <DllImport("user32.dll")> Private Shared Function FindWindow(ByVal className As String, ByVal windowText As String) As Integer
    End Function

    <DllImport("user32.dll")> Private Shared Function FindWindowEx(ByVal HWnd1 As IntPtr, ByVal HWnd2 As IntPtr, ByVal lpsz1 As IntPtr, ByVal lpsz2 As String) As Integer
    End Function

    <DllImport("user32.dll")> Private Shared Function ShowWindow(ByVal HWnd As Integer, ByVal Command As Integer) As Integer
    End Function
#End Region

    Private ReadOnly SW_HIDE As Integer = 0
    Private ReadOnly SW_SHOW As Integer = 1
    Private ReadOnly MAGIC_NUMBER_FOR_START_BUTTON As Long = 49175

    Public Sub New()

    End Sub

    Public Function GetHWnd_Taskbar() As Integer
        Return FindWindow("Shell_TrayWnd", "")
    End Function

    Public Function GetHWnd_StartButton() As Integer
        Dim ptr1 = New IntPtr(MAGIC_NUMBER_FOR_START_BUTTON)

        Return FindWindowEx(IntPtr.Zero, IntPtr.Zero, ptr1, "Start")
    End Function

    Public Function GetHWnd_Desktop() As Integer
        Return FindWindow("Progman", "Program Manager")
    End Function

    Public Sub HideTaskbar()
        _Hide(GetHWnd_Taskbar())
    End Sub

    Public Sub HideStartButton()
        _Hide(GetHWnd_StartButton())
    End Sub

    Public Sub HideDesktop()
        _Hide(GetHWnd_Desktop())
    End Sub

    Public Sub HideAll()
        _Hide(GetHWnd_Taskbar())
        _Hide(GetHWnd_StartButton())
        _Hide(GetHWnd_Desktop())
    End Sub

    Public Sub ShowTaskbar()
        _Show(GetHWnd_Taskbar())
    End Sub

    Public Sub ShowStartButton()
        _Show(GetHWnd_StartButton())
    End Sub

    Public Sub ShowDesktop()
        _Show(GetHWnd_Desktop())
    End Sub

    Public Sub ShowAll()
        _Show(GetHWnd_Taskbar())
        _Show(GetHWnd_StartButton())
        _Show(GetHWnd_Desktop())
    End Sub

    Private Sub _Hide(ByVal hWnd As Integer)
        ShowWindow(hWnd, SW_HIDE)
    End Sub

    Private Sub _Show(ByVal hWnd As Integer)
        ShowWindow(hWnd, SW_SHOW)
    End Sub
End Class
