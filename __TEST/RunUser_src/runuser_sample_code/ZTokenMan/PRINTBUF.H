/******************************************************************************
Module:  PrintBuf.h
Notices: Copyright (c) 2000 Jeffrey Richter
Purpose: This class wraps allows sprintf-like operations while automatically
         growing the resulting data buffer.
         See Appendix B.
******************************************************************************/


#pragma once   // Include this header file once per compilation unit


///////////////////////////////////////////////////////////////////////////////


#include "CmnHdr.h"                 // See Appendix A.
#include <StdIO.h>                     // For _vstprintf


///////////////////////////////////////////////////////////////////////////////


class CPrintBuf {
public:
   CPrintBuf(SIZE_T nMaxSizeInBytes = 64 * 1024); // 64KB is default
   virtual ~CPrintBuf();

   BOOL Print(PCTSTR pszFmt, ...);
   BOOL PrintError(DWORD dwError = GetLastError());
   operator PCTSTR() { return(m_pszBuffer); }
   void Clear();

private:
   LONG Filter(EXCEPTION_POINTERS* pep);

private:
   int   m_nMaxSizeInBytes;
   int   m_nCurSize;    // In number of characters
   PTSTR m_pszBuffer;
};


///////////////////////////////////////////////////////////////////////////////


//#ifdef PRINTBUF_IMPL


///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////


//#endif   // PRINTBUF_IMPL


///////////////////////////////// End of File /////////////////////////////////
