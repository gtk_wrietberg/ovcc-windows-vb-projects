#include "AutoBuf.h"
#include "StdAfx.h"

void CAutoBufBase::Reconstruct(BOOL fFirstTime) {

   if (!fFirstTime) {
      if (*m_ppbBuffer != NULL)
         HeapFree(GetProcessHeap(), 0, *m_ppbBuffer);
   }

   *m_ppbBuffer = NULL; // Derived class doesn't point to a data buffer
   m_uNewSize = 0;      // Initially, buffer has no bytes in it
   m_uCurrentSize = 0;  // Initially, buffer has no bytes in it
}


///////////////////////////////////////////////////////////////////////////////


UINT CAutoBufBase::Size(UINT uSize) {

   // Set buffer to desired number of m_nMult bytes.
   if (uSize == 0) {
      Reconstruct();
   } else {
      m_uNewSize = uSize;
      AdjustBuffer();      
   }
   return(m_uNewSize);
}


///////////////////////////////////////////////////////////////////////////////


void CAutoBufBase::AdjustBuffer() {

   if (m_uCurrentSize < m_uNewSize) {

      // We're growing the buffer
      HANDLE hHeap = GetProcessHeap();

      if (*m_ppbBuffer != NULL) {
         // We already have a buffer, re-size it
         PBYTE pNew = (PBYTE) 
            HeapReAlloc(hHeap, 0, *m_ppbBuffer, m_uNewSize * m_nMult);
         if (pNew != NULL) {
            m_uCurrentSize = m_uNewSize;
            *m_ppbBuffer = pNew;
         } 
      } else {
         // We don't have a buffer, create new one.
         *m_ppbBuffer = (PBYTE) HeapAlloc(hHeap, 0, m_uNewSize * m_nMult);
         if (*m_ppbBuffer != NULL) 
            m_uCurrentSize = m_uNewSize;
      }
   }
}
