
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// ZTokenManDoc.cpp : CZTokenManDoc クラスの動作の定義を行います。
//

#include "stdafx.h"
#include "ZTokenMan.h"

#include "ZTokenManDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CZTokenManDoc

IMPLEMENT_DYNCREATE(CZTokenManDoc, CDocument)

BEGIN_MESSAGE_MAP(CZTokenManDoc, CDocument)
	//{{AFX_MSG_MAP(CZTokenManDoc)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
		//        この位置に生成されるコードを編集しないでください。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZTokenManDoc クラスの構築/消滅

CZTokenManDoc::CZTokenManDoc()
{
	// TODO: この位置に１度だけ呼ばれる構築用のコードを追加してください。

}

CZTokenManDoc::~CZTokenManDoc()
{
}

BOOL CZTokenManDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: この位置に再初期化処理を追加してください。
	// (SDI ドキュメントはこのドキュメントを再利用します。)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CZTokenManDoc シリアライゼーション

void CZTokenManDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: この位置に保存用のコードを追加してください。
	}
	else
	{
		// TODO: この位置に読み込み用のコードを追加してください。
	}
}

/////////////////////////////////////////////////////////////////////////////
// CZTokenManDoc クラスの診断

#ifdef _DEBUG
void CZTokenManDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CZTokenManDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CZTokenManDoc コマンド
