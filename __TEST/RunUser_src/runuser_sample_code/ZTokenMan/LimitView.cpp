
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// LimitView.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "ZTokenMan.h"
#include "SetView.h"
#include "MainFrm.h"
#include "InitView.h"
#include "ReportView.h"
#include "LimitView.h"
#include "GlobalVar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLimitView

IMPLEMENT_DYNCREATE(CLimitView, CFormView)

CLimitView::CLimitView()
	: CFormView(CLimitView::IDD)
{
	//{{AFX_DATA_INIT(CLimitView)
		// メモ: ClassWizard はこの位置にメンバの初期化処理を追加します
	//}}AFX_DATA_INIT
	m_pW = NULL;
	m_bExpand = TRUE;
}

CLimitView::~CLimitView()
{
}

void CLimitView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLimitView)
	DDX_Control(pDX, IDC_BUTTON_V, m_btnV);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLimitView, CFormView)
	//{{AFX_MSG_MAP(CLimitView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_V, OnButtonV)
	ON_BN_CLICKED(IDB_CREATERESTRICTEDTOKEN, OnCreateRestrictedToken)
	ON_BN_CLICKED(IDB_ADDRESTRICTED, OnAddRestricted)
	ON_BN_CLICKED(IDB_REMOVERESTRICTED, OnRemoveRestricted)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLimitView 診断

#ifdef _DEBUG
void CLimitView::AssertValid() const
{
	CFormView::AssertValid();
}

void CLimitView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CLimitView メッセージ ハンドラ

void CLimitView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	g_hwndDeletedPrivileges = ::GetDlgItem(m_hWnd, IDL_DELETEDPRIVILEGES);
    g_hwndDisabledSids = ::GetDlgItem(m_hWnd, IDL_DISABLEDSIDS);
    g_hwndRestrictedSids = ::GetDlgItem(m_hWnd, IDL_RESTRICTEDSIDS);
	if(!m_pW)
	{
		m_pW = (CMainFrame*)AfxGetMainWnd();
		m_pW->m_pLimit = this;
	}
	ArrangeLayout(1,1);
	m_btnV.SetColor(BLACK, CLOUDBLUE);
    m_btnV.SetWindowText(_T("--------------------------"));
}

void CLimitView::OnSize(UINT nType, int cx, int cy) 
{
	SetScrollSizes(MM_TEXT, CSize(cx, cy));

	CFormView::OnSize(nType, cx, cy);
	ArrangeLayout(cx, cy);
	Invalidate();
}

void CLimitView::ArrangeLayout(int cx, int cy)
{
	if(!m_bExpand) 
	{
		CRect rect;
		m_btnV.GetClientRect(&rect);
		m_pW->m_wndSplitter.SetRowInfo(0, 
				rect.Height() ,  rect.Height());
	    m_pW->m_wndSplitter.RecalcLayout();
		CRect rectClient;
		this->GetClientRect(&rectClient);
		rect.right = rectClient.right;
		m_btnV.MoveWindow(&rect);
		m_btnV.SetColor(WHITE, RED);//RGB(255,255,128));
		return;
	}

	BOOL static first = TRUE;
	if(first)
	{
		first = FALSE;
	}
	else
	{
		if(m_pW && m_pW->GetSafeHwnd())
		{
            HRSRC  hRsrc = ::FindResource(NULL, MAKEINTRESOURCE(IDD_LIMIT), RT_DIALOG);
	        ASSERT(hRsrc);
            HGLOBAL hGlobal = LoadResource(NULL, hRsrc);
            ASSERT(hGlobal);
            //DWORD dwSize = GlobalSize(hGlobal);
            DLGTEMPLATE* lpStr = (DLGTEMPLATE*)hGlobal;
	        int cx1 = lpStr->cx;
	        int cy1 = lpStr->cy;
	        CRect rect;
	        rect.SetRect(0,0,cx1,cy1);
	        ::MapDialogRect(GetSafeHwnd(), &rect);
            
			if(cx > 0 && cx < 2000)
			{
				if(cy >= rect.Height())
				{
					//rect.bottom = rect.top + cy;
				}
			}
            m_pW->m_wndSplitter3.SetRowInfo(0, 
				rect.Height() ,  rect.Height());
	        m_pW->m_wndSplitter3.RecalcLayout();

			m_btnV.GetClientRect(&rect);
			CRect rectClient;
		    this->GetClientRect(&rectClient);
		    rect.right = rectClient.right;
		    m_btnV.MoveWindow(&rect);
			m_btnV.SetColor(BLACK, CLOUDBLUE);
		}
	}
}

void CLimitView::OnButtonV() 
{
	if(m_bExpand)
	{
		m_bExpand = !m_bExpand;
		CRect rect;
		m_btnV.GetClientRect(&rect);
		m_pW->m_wndSplitter3.SetRowInfo(0, 
				rect.Height() ,  rect.Height());
        m_pW->m_wndSplitter3.RecalcLayout();
		::SetFocus(::g_hwndToken);
	}
	else
	{
		m_bExpand = !m_bExpand;
        ArrangeLayout(0,0);
	}	
}

void CLimitView::OnAddRestricted() 
{
	// Add the new name to the restricted sids list control
    TCHAR szName[1024];
    if (g_hToken && GetAccountName(m_hWnd, szName, chDIMOF(szName), TRUE, TRUE))
	   ::SendMessage(g_hwndRestrictedSids, LB_ADDSTRING, 0, (LPARAM) szName);
}

void CLimitView::OnRemoveRestricted() 
{
	LRESULT nIndex = ::SendMessage(g_hwndRestrictedSids, LB_GETCURSEL, 0, 0);
    if (nIndex != LB_ERR)
	   ::SendMessage(g_hwndRestrictedSids, LB_DELETESTRING, nIndex, 0);
}

void CLimitView::OnCreateRestrictedToken() 
{
	PSID_AND_ATTRIBUTES  psidToDisableAttrib  = NULL;
    PLUID_AND_ATTRIBUTES pluidPrivAttribs     = NULL;
    PSID_AND_ATTRIBUTES  psidToRestrictAttrib = NULL;

    DWORD dwIndex;
    DWORD dwDisableSize  = 0;
    DWORD dwRestrictSize = 0;

   PTSTR szStatus = TEXT("Restricted Token Created");

   try {{

      // No Token?  Gotta run...
      if (g_hToken == NULL) {
         szStatus = TEXT("No Token");
         SetLastError(0);
         goto leave;
      }

      // How big of a structure do I allocate for restricted sids
      dwRestrictSize = ::SendMessage(g_hwndRestrictedSids, LB_GETCOUNT, 0 , 0);
      psidToRestrictAttrib = (PSID_AND_ATTRIBUTES) LocalAlloc(LPTR,
            dwRestrictSize * sizeof(SID_AND_ATTRIBUTES));
      if (psidToRestrictAttrib == NULL) {
         szStatus = TEXT("LocalAlloc");
         goto leave;
      }

      ZeroMemory(psidToRestrictAttrib, dwRestrictSize
            * sizeof(SID_AND_ATTRIBUTES));

      DWORD dwData;
      DWORD dwSidSize;
      DWORD dwDomainSize;
      TCHAR szBuffer[1024];
      TCHAR szDomain[1024];
      PSID  pSid;

      SID_NAME_USE sidNameUse;

      // For each sid, we find the sid and add it to our array
      for (dwIndex = 0; dwIndex < dwRestrictSize; dwIndex++) {

         dwData = ::SendMessage(g_hwndRestrictedSids, LB_GETTEXT, dwIndex,
               (LPARAM) szBuffer);
         if (dwData == LB_ERR) {
            dwIndex--;
            break;
         }

         dwSidSize = 0;
         dwDomainSize = chDIMOF(szDomain);

         // Size of SID?
         LookupAccountName(NULL, szBuffer, NULL, &dwSidSize, szDomain,
               &dwDomainSize, &sidNameUse);
         pSid = LocalAlloc(LPTR, dwSidSize);
         if (pSid == NULL) {

            szStatus = TEXT("LocalAlloc");
            goto leave;
         }

         // Get the SID
         if (!LookupAccountName(NULL, szBuffer, pSid, &dwSidSize, szDomain,
                        &dwDomainSize, &sidNameUse)) {

            szStatus = TEXT("LookupAccountName");
            goto leave;
         }

         psidToRestrictAttrib[dwIndex].Sid = pSid;
         psidToRestrictAttrib[dwIndex].Attributes = 0;
      }
      dwRestrictSize = dwIndex;

      // How much memory do we need for our disabled SIDS?
      dwDisableSize = ::SendMessage(g_hwndDisabledSids, LB_GETCOUNT, 0 , 0);
      psidToDisableAttrib = (PSID_AND_ATTRIBUTES)
            LocalAlloc(LPTR, dwDisableSize * sizeof(SID_AND_ATTRIBUTES));     
      if (psidToDisableAttrib == NULL) {

         szStatus = TEXT("LocalAlloc");
         goto leave;
      }
      ZeroMemory(psidToDisableAttrib,
         dwDisableSize * sizeof(SID_AND_ATTRIBUTES));

      DWORD dwEnd = dwDisableSize;
      dwDisableSize = 0;

      // For each one, add it to our array
      for (dwIndex = 0; dwIndex < dwEnd; dwIndex++) {


         if (::SendMessage(g_hwndDisabledSids, LB_GETSEL, dwIndex, 0)) {

            dwData = ::SendMessage(g_hwndDisabledSids, LB_GETTEXT,
                              dwIndex, (LPARAM) szBuffer);
            if (dwData == LB_ERR) {

               dwIndex--;
               break;
            }

            dwSidSize = 0;
            dwDomainSize = chDIMOF(szDomain);

            // SID size?
            LookupAccountName(NULL, szBuffer, NULL, &dwSidSize, szDomain,
                           &dwDomainSize, &sidNameUse);
            pSid = LocalAlloc(LPTR, dwSidSize);
            if (pSid == NULL) {

               szStatus = TEXT("LocalAlloc");
               goto leave;
            }
            // Get the SID
            if (!LookupAccountName(NULL, szBuffer, pSid, &dwSidSize,
                  szDomain, &dwDomainSize, &sidNameUse)) {

               szStatus = TEXT("LookupAccountName");
               goto leave;
            }

            psidToDisableAttrib[dwDisableSize].Sid = pSid;
            psidToDisableAttrib[dwDisableSize].Attributes = 0;
            dwDisableSize++;
         }
      }

      // Now we find out how much memory we need for our privileges
      DWORD dwPrivSize = ::SendMessage(g_hwndDeletedPrivileges, LB_GETCOUNT,
            0, 0);
      pluidPrivAttribs = (PLUID_AND_ATTRIBUTES) LocalAlloc(LPTR, dwPrivSize
            * sizeof(LUID_AND_ATTRIBUTES));
      if (pluidPrivAttribs == NULL) {
         szStatus = TEXT("LocalAlloc");
         goto leave;
      }
      ZeroMemory(pluidPrivAttribs, dwPrivSize * sizeof(LUID_AND_ATTRIBUTES));

      // Add the privileges that are to be removed from the token to the list
      dwEnd = dwPrivSize;
      dwPrivSize = 0;
      for (dwIndex = 0; dwIndex < dwEnd; dwIndex++) {

         if (::SendMessage(g_hwndDeletedPrivileges, LB_GETSEL, dwIndex, 0)) {

            dwData = ::SendMessage(g_hwndDeletedPrivileges, LB_GETTEXT, dwIndex,
                  (LPARAM) szBuffer);
            if (dwData == LB_ERR) {
               dwIndex--;
               break;
            }

            LUID luid;
            if (!LookupPrivilegeValue(NULL, szBuffer, &luid)) {
               szStatus = TEXT("LookupPrivilegeValue");
               goto leave;
            }

            pluidPrivAttribs[dwPrivSize].Luid = luid;
            pluidPrivAttribs[dwPrivSize].Attributes = 0;
            dwPrivSize++;
         }
      }

      // Attempt to create restricted token with the structures we built
      HANDLE hNewToken;
      if (!CreateRestrictedToken(g_hToken, 0, dwDisableSize,
            psidToDisableAttrib, dwPrivSize, pluidPrivAttribs, dwRestrictSize,
            psidToRestrictAttrib, &hNewToken)) {
         szStatus = TEXT("CreateRestrictedToken");
         goto leave;
      }

      // Close out our old token
      CloseHandle(g_hToken);

      // This is our new token
      g_hToken = hNewToken;

      // Display it
      DumpToken();

      SetLastError(0);

   } leave:;
   } catch(...) {}

   Status(szStatus, GetLastError());

   // We have to loop to remove all of those sids we allocated
   if (psidToDisableAttrib != NULL) {
      for (dwIndex = 0; dwIndex < dwDisableSize; dwIndex++)
         if (psidToDisableAttrib[dwIndex].Sid != NULL)
            LocalFree(psidToDisableAttrib[dwIndex].Sid);
      LocalFree(psidToDisableAttrib);
   }

   // The privileges we can just free
   if (pluidPrivAttribs != NULL)
      LocalFree(pluidPrivAttribs);

   // More looping to free up sids we allocated
   if (psidToRestrictAttrib != NULL) {
      for (dwIndex = 0; dwIndex < dwRestrictSize; dwIndex++)
         if (psidToRestrictAttrib[dwIndex].Sid != NULL)
            LocalFree(psidToRestrictAttrib[dwIndex].Sid);
      LocalFree(psidToRestrictAttrib);
   }
}

