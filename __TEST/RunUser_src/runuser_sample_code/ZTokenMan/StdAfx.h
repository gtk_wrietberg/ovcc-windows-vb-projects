
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// stdafx.h : 標準のシステム インクルード ファイル、
//            または参照回数が多く、かつあまり変更されない
//            プロジェクト専用のインクルード ファイルを記述します。
//

#if !defined(AFX_STDAFX_H__F8A903EA_484F_4460_87D3_AD25510FC91D__INCLUDED_)
#define AFX_STDAFX_H__F8A903EA_484F_4460_87D3_AD25510FC91D__INCLUDED_

#define WINVER  0x0500  
#define _WIN32_WINNT  0x0500 //Usefull!!!!!
#define _WIN32_IE 0x0600

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Windows ヘッダーから殆ど使用されないスタッフを除外します。

#include <afxwin.h>         // MFC のコアおよび標準コンポーネント
#include <afxext.h>         // MFC の拡張部分
#include <afxdisp.h>        // MFC のオートメーション クラス
#include <afxdtctl.h>		// MFC の Internet Explorer 4 コモン コントロール サポート
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC の Windows コモン コントロール サポート
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <AclAPI.h>
#include <AclUI.h>
#include <ObjBase.h>
#include <ObjSel.h>
#include <TlHelp32.h>

#define AUTOBUF_IMPL
#include "AutoBuf.h"       // See Appendix B.

#define PRINTBUF_IMPL
#include "PrintBuf.h"      // See Appendix B.

//#define SECINFO_IMPL
#include "SecInfo.h"      // See Appendix B.

#define DIVIDERL  TEXT("****************************************") \
                  TEXT("****************************************\r\n")

#define DIVIDERS  TEXT("----------------------------------------") \
                  TEXT("----------------------------------------\r\n")

#include "z.h"
#pragma comment(lib, "Netapi32") //to use account list dialog
#define WM_ROTATE WM_USER + 313
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_STDAFX_H__F8A903EA_484F_4460_87D3_AD25510FC91D__INCLUDED_)
