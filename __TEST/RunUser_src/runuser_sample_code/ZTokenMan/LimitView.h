
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#if !defined(AFX_LIMITVIEW_H__BE7BEFCA_2C91_4DB7_B1B6_AD8706D15B8D__INCLUDED_)
#define AFX_LIMITVIEW_H__BE7BEFCA_2C91_4DB7_B1B6_AD8706D15B8D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LimitView.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CLimitView フォーム ビュー

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif
class CMainFrame;
#include "clrButton.h"
class CLimitView : public CFormView
{
protected:
	CLimitView();           // 動的生成に使用されるプロテクト コンストラクタ。
	DECLARE_DYNCREATE(CLimitView)

// フォーム データ
public:
	//{{AFX_DATA(CLimitView)
	enum { IDD = IDD_LIMIT };
	CClrButton	m_btnV;
	//}}AFX_DATA
	BOOL m_bExpand;

// アトリビュート
public:
    CMainFrame* m_pW;
	void ArrangeLayout(int cx = 0, int cy = 0);
// オペレーション
public:

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CLimitView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	virtual ~CLimitView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CLimitView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonV();
	afx_msg void OnCreateRestrictedToken();
	afx_msg void OnAddRestricted();
	afx_msg void OnRemoveRestricted();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_LIMITVIEW_H__BE7BEFCA_2C91_4DB7_B1B6_AD8706D15B8D__INCLUDED_)
