//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ZTokenMan.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_ZTOKENMAN_FORM              101
#define IDD_INIT                        101
#define IDD_REPORT                      102
#define IDD_SET                         103
#define IDD_LIMIT                       104
#define IDD_ACCOUNT_LIST                105
#define IDR_MAINFRAME                   128
#define IDR_ZTOKENTYPE                  129
#define IDB_ACCOUNT                     149
#define IDB_DUMPTOKEN                   1000
#define IDB_LOGONUSER                   1001
#define IDC_PROCESSES                   1002
#define IDC_THREADS                     1003
#define IDE_TOKEN                       1004
#define IDC_BUTTON_V                    1004
#define IDB_DUPLICATE                   1005
#define IDL_DELETEDPRIVILEGES           1006
#define IDB_ADJUSTTOKENPRIVILEGES       1007
#define IDL_DISABLEDSIDS                1008
#define IDC_CHECK_COPY_TOKEN            1008
#define IDL_RESTRICTEDSIDS              1009
#define IDC_BUTTON_ENUM_USER            1009
#define IDB_ADDRESTRICTED               1010
#define IDC_LIST                        1010
#define IDS_STATUS                      1010
#define IDC_BUTTON1                     1011
#define IDB_REMOVERESTRICTED            1012
#define IDC_BTN_REFRESH                 1012
#define IDB_CREATERESTRICTEDTOKEN       1013
#define IDE_FILENAME                    1014
#define IDB_BROWSE                      1015
#define IDB_CREATEPROCESS               1016
#define IDL_ENABLEGROUPS                1017
#define IDB_ADJUSTTOKENGROUPS           1018
#define IDL_ENABLEPRIVILEGES            1019
#define IDB_SETOWNER                    1020
#define IDB_SETGROUP                    1021
#define IDB_SETDACL                     1022
#define IDC_LINK_TDU                    1022
#define IDS_RETRIEVE                    1023
#define IDC_LINK_LAB                    1023
#define IDS_DELETEDPRIVILEGES           1024
#define IDC_LINK_CODEGURU               1024
#define IDE_USERNAME                    1025
#define IDC_DISK_INFO                   1025
#define IDS_DISABLEDSIDS                1026
#define IDS_SETTOKENINFORMATION         1027
#define IDS_ADJUSTPRIV                  1028
#define IDS_ADJUSTGROUPS                1029
#define IDS_CREATEPROCESS               1030
#define IDS_PROCESSES                   1031
#define IDS_THREADS                     1032
#define IDS_USERNAME                    1033
#define IDS_TOKENINFORMATION            1034
#define IDS_PASSWORD                    1035
#define IDS_CREATEARESTRICTEDTOKEN      1036
#define IDE_PASSWORD                    1037
#define IDS_ADDREMOVERESTRICTED         1038
#define IDE_STATUS                      1039
#define IDS_LOGONTYPE                   1040
#define IDS_IMPLEVEL                    1041
#define IDS_LOGONPROVIDER               1042
#define IDS_TYPE                        1043
#define IDC_LOGONTYPE                   1044
#define IDC_LOGONPROVIDER               1045
#define IDC_IMPERSONATIONLEVEL          1046
#define IDC_TOKENTYPE                   1047
#define ID_TTT                          57608

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
