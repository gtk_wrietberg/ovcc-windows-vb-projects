
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#ifndef GLOBAL_Z_VAR
#define GLOBAL_Z_VAR

extern HANDLE g_hSnapShot;
extern HANDLE g_hToken;

extern HWND g_hwndProcessCombo;
extern HWND g_hwndThreadCombo;
extern HWND g_hwndToken;
extern HWND g_hwndStatus;
extern HWND g_hwndEnablePrivileges;
extern HWND g_hwndEnableGroups;
extern HWND g_hwndDeletedPrivileges;
extern HWND g_hwndDisabledSids;
extern HWND g_hwndRestrictedSids;
extern HWND g_hwndLogonTypes;
extern HWND g_hwndLogonProviders;
extern HWND g_hwndImpersonationLevels;
extern HWND g_hwndTokenTypes;

extern HINSTANCE g_hInst;

DWORD WINAPI DumpTokenThread(LPVOID);

void GetToken(DWORD dwProcessID, DWORD dwThreadID, BOOL bCopy = TRUE);
void Status(PTSTR szStatus, DWORD dwLastError);
void DumpToken();
BOOL DumpTokenRestrictedSids(HANDLE hToken, CPrintBuf* pbufToken);
void DumpTokenType(HANDLE hToken, CPrintBuf* pbufToken);
void DumpTokenImpersonationLevel(HANDLE hToken, CPrintBuf* pbufToken);
void DumpTokenSource(HANDLE hToken, CPrintBuf* pbufToken);
void DumpTokenDefaultDacl(HANDLE hToken, CPrintBuf* pbufToken);
PVOID AllocateTokenInfo(HANDLE hToken, TOKEN_INFORMATION_CLASS tokenClass);
void DumpSIDAttributes(DWORD dwAttrib, CPrintBuf* pbufToken);
void DumpSID(PSID psid, CPrintBuf* pbufToken);
BOOL GetTextualSid(PSID pSid, PTSTR TextualSid, PDWORD pdwBufferLen);
void DumpTokenPrimaryGroup(HANDLE hToken, CPrintBuf* pbufToken); 
void DumpTokenOwner(HANDLE hToken, CPrintBuf* pbufToken);
void DumpTokenPrivileges(HANDLE hToken, CPrintBuf* pbufToken);
BOOL DumpTokenUser(HANDLE hToken, CPrintBuf* pbufToken);
BOOL DumpTokenGroups(HANDLE hToken, CPrintBuf* pbufToken);
BOOL DumpTokenStatistics(HANDLE hToken, CPrintBuf* pbufToken);

BOOL GetUserSID(PSID psid, BOOL fAllowImpersonate, PDWORD pdwSize);
BOOL TryRelaunch(); 
BOOL RunAsUser(PTSTR pszEXE, PTSTR pszUserName, PTSTR pszDomainName,
			   PTSTR pszPassword, PTSTR pszDesktop);
HANDLE GetLSAToken();
BOOL ModifySecurity(HANDLE hProc, DWORD dwAccess);
HANDLE OpenSystemProcess();
BOOL EnablePrivilege(PTSTR szPriv, BOOL fEnabled);
BOOL GetAccountName(HWND hwnd, PTSTR szBuf, DWORD dwSize, BOOL fAllowGroups,
      BOOL fAllowUsers);
void UpdatePrivileges();
void UpdateGroups();
#endif
