
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// ReportView.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "ZTokenMan.h"
#include "SetView.h"
#include "MainFrm.h"
#include "InitView.h"
#include "ReportView.h"
#include "GlobalVar.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CReportView

IMPLEMENT_DYNCREATE(CReportView, CFormView)

CReportView::CReportView()
	: CFormView(CReportView::IDD)
{
	//{{AFX_DATA_INIT(CReportView)
		// メモ: ClassWizard はこの位置にメンバの初期化処理を追加します
	//}}AFX_DATA_INIT
	m_pW = NULL;
}

CReportView::~CReportView()
{
}

void CReportView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CReportView)
	DDX_Control(pDX, IDS_STATUS, m_staticStatus);
	DDX_Control(pDX, IDS_TOKENINFORMATION, m_staticToken);
	DDX_Control(pDX, IDE_TOKEN, m_editToken);
	DDX_Control(pDX, IDE_STATUS, m_editStatus);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CReportView, CFormView)
	//{{AFX_MSG_MAP(CReportView)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CReportView 診断

#ifdef _DEBUG
void CReportView::AssertValid() const
{
	CFormView::AssertValid();
}

void CReportView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CReportView メッセージ ハンドラ

void CReportView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	::g_hwndToken = ::GetDlgItem(m_hWnd, IDE_TOKEN);
    ::g_hwndStatus = ::GetDlgItem(m_hWnd, IDE_STATUS);
	if(!m_pW)
	{
		m_pW = (CMainFrame*)AfxGetMainWnd();
		m_pW->m_pReport = this;
	}
	ArrangeLayout(0,0);
}

void CReportView::OnSize(UINT nType, int cx, int cy) 
{
	SetScrollSizes(MM_TEXT, CSize(cx, cy));
	CFormView::OnSize(nType, cx, cy);
	if(cx > 0 && cx < 2000 && m_editStatus.GetSafeHwnd())
	{
		ArrangeLayout(cx,cy); 
	}
}

void CReportView::ArrangeLayout(int cx, int cy)
{
	CRect rect;
	m_staticStatus.GetWindowRect(&rect);
	this->ScreenToClient(&rect);
    rect.OffsetRect(-rect.left + 1, -rect.top + 1);
	m_staticStatus.MoveWindow(rect);
    int n = rect.bottom;
	n += 3;
    CRect rectClient;
	this->GetClientRect(&rectClient);
    
	CDC* pDC = m_editStatus.GetDC();
    CString strText;
    m_editStatus.GetWindowText(strText);
	if(strText.IsEmpty())
		strText = _T(" ");
	CSize size = pDC->GetTextExtent(strText);
	pDC->LPtoDP(&size);
	size.cx += 10;
	int line = size.cx / rect.Width();
    line++;
	if(line > 3) line = 3;
	m_editStatus.ReleaseDC(pDC);
	rect.top = n;
	rect.left = -1;
	rect.bottom = rect.top + line*size.cy;
	rect.right = rectClient.right;
    m_editStatus.MoveWindow(&rect);

	n = rect.bottom;
	n += 3;
	m_staticToken.GetWindowRect(&rect);
	this->ScreenToClient(&rect);
    rect.OffsetRect(-rect.left + 1, -rect.top + n);
	m_staticToken.MoveWindow(rect);

    n = rect.bottom;
	n += 3;
    
	rect.SetRect(-1, n, rectClient.right, rectClient.bottom);
	m_editToken.MoveWindow(&rect);

}
