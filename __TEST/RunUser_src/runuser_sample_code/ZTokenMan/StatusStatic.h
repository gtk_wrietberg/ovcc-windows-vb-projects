
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// StatusStatic.h : header file
//
#ifndef StatusStatic_WND
#define StatusStatic_WND
/////////////////////////////////////////////////////////////////////////////
// CStatusStatic window
//class CStatusControl;
#include "StatusControl.h"
class CStatusStatic : public CStatic
{
// Construction
public:
	CStatusStatic();
	BOOL Create(CStatusBar * parent, UINT id, DWORD style);
	void Rotate(BOOL bStart = TRUE);
// Attributes
public:
	static COLORREF m_color[7];

// Operations
public:
	__inline void Reposition() { CStatusControl::reposition(this); }
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStatusStatic)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CStatusStatic();

	// Generated message map functions
protected:
	//{{AFX_MSG(CStatusStatic)
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#endif
/////////////////////////////////////////////////////////////////////////////
