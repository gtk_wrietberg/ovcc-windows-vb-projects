
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#if !defined(AFX_SETVIEW_H__B909F88B_DD9E_4C7A_AFB1_8C8F4F8CA5AF__INCLUDED_)
#define AFX_SETVIEW_H__B909F88B_DD9E_4C7A_AFB1_8C8F4F8CA5AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SetView.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CSetView フォーム ビュー

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif
class CMainFrame;
#include "clrButton.h"
class CSetView : public CFormView
{
protected:
	CSetView();           // 動的生成に使用されるプロテクト コンストラクタ。
	DECLARE_DYNCREATE(CSetView)

// フォーム データ
public:
	//{{AFX_DATA(CSetView)
	enum { IDD = IDD_SET };
	CEdit	m_editFileName;
	CListBox	m_listEnablePrivilege;
	CListBox	m_listEnableGroup;
	CClrButton	m_btnV;
	//}}AFX_DATA
	BOOL m_bExpand;

// アトリビュート
public:
	CMainFrame* m_pW;

// オペレーション
public:

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CSetView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL
    void ArrangeLayout(int cx = 0, int cy = 0);
// インプリメンテーション
protected:
	virtual ~CSetView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CSetView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonV();
	afx_msg void OnBrowseExe();
	afx_msg void OnCreateProcessAsUser();
	afx_msg void OnAdjustTokenGroups();
	afx_msg void OnAdjustTokenPrivileges();
	afx_msg void OnSetOwner();
	afx_msg void OnSetGroup();
	afx_msg void OnSetDacl();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_SETVIEW_H__B909F88B_DD9E_4C7A_AFB1_8C8F4F8CA5AF__INCLUDED_)
