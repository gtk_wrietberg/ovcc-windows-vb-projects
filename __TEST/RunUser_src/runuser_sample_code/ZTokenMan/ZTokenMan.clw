; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CInitView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "ztokenman.h"
LastPage=0

ClassCount=12
Class1=CAccountListDlg
Class2=CClrButton
Class3=CInitView
Class4=CLimitView
Class5=CMainFrame
Class6=CReportView
Class7=CSetView
Class8=CStatusControl
Class9=CStatusStatic
Class10=CZTokenManApp
Class11=CAboutDlg
Class12=CZTokenManDoc

ResourceCount=14
Resource1=IDR_MAINFRAME (�p�� (��ض))
Resource2=IDD_SET (�p�� (��ض))
Resource3=IDD_LIMIT (�p�� (��ض))
Resource4=IDD_INIT (�p�� (��ض))
Resource5=IDD_ACCOUNT_LIST (�p�� (��ض))
Resource6=IDD_REPORT (�p�� (��ض))
Resource7=IDD_ABOUTBOX (�p�� (��ض))
Resource8=IDR_MAINFRAME
Resource9=IDD_SET
Resource10=IDD_LIMIT
Resource11=IDD_REPORT
Resource12=IDD_ACCOUNT_LIST
Resource13=IDD_INIT
Resource14=IDD_ABOUTBOX

[CLS:CAccountListDlg]
Type=0
BaseClass=CDialog
HeaderFile=AccountListDlg.h
ImplementationFile=AccountListDlg.cpp

[CLS:CClrButton]
Type=0
BaseClass=CButton
HeaderFile=ClrButton.h
ImplementationFile=ClrButton.cpp

[CLS:CInitView]
Type=0
BaseClass=CFormView
HeaderFile=InitView.h
ImplementationFile=InitView.cpp
Filter=D
VirtualFilter=VWC
LastObject=IDC_PROCESSES

[CLS:CLimitView]
Type=0
BaseClass=CFormView
HeaderFile=LimitView.h
ImplementationFile=LimitView.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp

[CLS:CReportView]
Type=0
BaseClass=CFormView
HeaderFile=ReportView.h
ImplementationFile=ReportView.cpp

[CLS:CSetView]
Type=0
BaseClass=CFormView
HeaderFile=SetView.h
ImplementationFile=SetView.cpp

[CLS:CStatusControl]
Type=0
BaseClass=CWnd
HeaderFile=StatusControl.h
ImplementationFile=StatusControl.cpp

[CLS:CStatusStatic]
Type=0
BaseClass=CStatic
HeaderFile=StatusStatic.h
ImplementationFile=StatusStatic.cpp

[CLS:CZTokenManApp]
Type=0
BaseClass=CWinApp
HeaderFile=ZTokenMan.h
ImplementationFile=ZTokenMan.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=ZTokenMan.cpp
ImplementationFile=ZTokenMan.cpp
LastObject=CAboutDlg

[CLS:CZTokenManDoc]
Type=0
BaseClass=CDocument
HeaderFile=ZTokenManDoc.h
ImplementationFile=ZTokenManDoc.cpp

[DLG:IDD_ACCOUNT_LIST]
Type=1
Class=CAccountListDlg
ControlCount=1
Control1=IDC_LIST,SysListView32,1350631437

[DLG:IDD_INIT]
Type=1
Class=CInitView
ControlCount=25
Control1=IDC_PROCESSES,combobox,1344339971
Control2=IDC_THREADS,combobox,1344339971
Control3=IDB_DUMPTOKEN,button,1342242816
Control4=IDC_IMPERSONATIONLEVEL,combobox,1344340227
Control5=IDC_TOKENTYPE,combobox,1344340227
Control6=IDB_DUPLICATE,button,1342242816
Control7=IDE_USERNAME,edit,1350631552
Control8=IDE_PASSWORD,edit,1350631584
Control9=IDC_LOGONTYPE,combobox,1344340227
Control10=IDC_LOGONPROVIDER,combobox,1344340227
Control11=IDB_LOGONUSER,button,1342242816
Control12=IDS_PROCESSES,static,1342308352
Control13=IDS_THREADS,static,1342308352
Control14=IDS_IMPLEVEL,static,1342308352
Control15=IDS_TYPE,static,1342308352
Control16=IDS_USERNAME,static,1342308352
Control17=IDS_PASSWORD,static,1342308352
Control18=IDS_LOGONTYPE,static,1342308352
Control19=IDS_LOGONPROVIDER,static,1342308352
Control20=IDS_RETRIEVE,button,1342177287
Control21=IDC_BUTTON_V,button,1342242827
Control22=IDC_CHECK_COPY_TOKEN,button,1342242819
Control23=IDC_BUTTON_ENUM_USER,button,1342242816
Control24=IDC_BUTTON1,button,1342242816
Control25=IDC_BTN_REFRESH,button,1342242816

[DLG:IDD_LIMIT]
Type=1
Class=CLimitView
ControlCount=11
Control1=IDL_DELETEDPRIVILEGES,listbox,1353777419
Control2=IDL_RESTRICTEDSIDS,listbox,1353777411
Control3=IDB_ADDRESTRICTED,button,1342242816
Control4=IDB_REMOVERESTRICTED,button,1342242816
Control5=IDL_DISABLEDSIDS,listbox,1353777419
Control6=IDB_CREATERESTRICTEDTOKEN,button,1342242816
Control7=IDS_DELETEDPRIVILEGES,static,1342308363
Control8=IDS_ADDREMOVERESTRICTED,button,1342177287
Control9=IDS_DISABLEDSIDS,static,1342308363
Control10=IDS_CREATEARESTRICTEDTOKEN,button,1342177287
Control11=IDC_BUTTON_V,button,1342242827

[DLG:IDD_REPORT]
Type=1
Class=CReportView
ControlCount=4
Control1=IDS_TOKENINFORMATION,static,1342308352
Control2=IDE_TOKEN,edit,1353783428
Control3=IDS_STATUS,static,1342308352
Control4=IDE_STATUS,edit,1350633472

[DLG:IDD_SET]
Type=1
Class=CSetView
ControlCount=15
Control1=IDE_FILENAME,edit,1350631552
Control2=IDB_BROWSE,button,1342242816
Control3=IDB_CREATEPROCESS,button,1342242816
Control4=IDL_ENABLEGROUPS,listbox,1353777417
Control5=IDB_ADJUSTTOKENGROUPS,button,1342242816
Control6=IDL_ENABLEPRIVILEGES,listbox,1353777419
Control7=IDB_ADJUSTTOKENPRIVILEGES,button,1342242816
Control8=IDB_SETOWNER,button,1342242816
Control9=IDB_SETGROUP,button,1342242816
Control10=IDB_SETDACL,button,1342242816
Control11=IDS_CREATEPROCESS,button,1342177287
Control12=IDS_ADJUSTGROUPS,button,1342177287
Control13=IDS_ADJUSTPRIV,button,1342177287
Control14=IDS_SETTOKENINFORMATION,button,1342177287
Control15=IDC_BUTTON_V,button,1342242827

[TB:IDR_MAINFRAME (�p�� (��ض))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[MNU:IDR_MAINFRAME (�p�� (��ض))]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_FILE_SAVE_AS
Command4=ID_FILE_PRINT
Command5=ID_FILE_PRINT_PREVIEW
Command6=ID_FILE_PRINT_SETUP
Command7=ID_FILE_MRU_FILE1
Command8=ID_APP_EXIT
Command9=ID_VIEW_TOOLBAR
Command10=ID_VIEW_STATUS_BAR
Command11=ID_APP_ABOUT
CommandCount=11

[ACL:IDR_MAINFRAME (�p�� (��ض))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_ABOUTBOX (�p�� (��ض))]
Type=1
Class=?
ControlCount=7
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDC_LINK_TDU,static,1342308352
Control5=IDC_LINK_LAB,static,1342308352
Control6=IDOK,button,1342373889
Control7=IDC_LINK_CODEGURU,static,1342308352

[DLG:IDD_INIT (�p�� (��ض))]
Type=1
Class=?
ControlCount=24
Control1=IDC_PROCESSES,combobox,1344339971
Control2=IDC_THREADS,combobox,1344339971
Control3=IDB_DUMPTOKEN,button,1342242816
Control4=IDC_IMPERSONATIONLEVEL,combobox,1344340227
Control5=IDC_TOKENTYPE,combobox,1344340227
Control6=IDB_DUPLICATE,button,1342242816
Control7=IDE_USERNAME,edit,1350631552
Control8=IDE_PASSWORD,edit,1350631584
Control9=IDC_LOGONTYPE,combobox,1344340227
Control10=IDC_LOGONPROVIDER,combobox,1344340227
Control11=IDB_LOGONUSER,button,1342242816
Control12=IDS_PROCESSES,static,1342308352
Control13=IDS_THREADS,static,1342308352
Control14=IDS_IMPLEVEL,static,1342308352
Control15=IDS_TYPE,static,1342308352
Control16=IDS_USERNAME,static,1342308352
Control17=IDS_PASSWORD,static,1342308352
Control18=IDS_LOGONTYPE,static,1342308352
Control19=IDS_LOGONPROVIDER,static,1342308352
Control20=IDS_RETRIEVE,button,1342177287
Control21=IDC_BUTTON_V,button,1342242827
Control22=IDC_CHECK_COPY_TOKEN,button,1342242819
Control23=IDC_BUTTON_ENUM_USER,button,1342242816
Control24=IDC_BUTTON1,button,1342242816

[DLG:IDD_REPORT (�p�� (��ض))]
Type=1
Class=?
ControlCount=4
Control1=IDS_TOKENINFORMATION,static,1342308352
Control2=IDE_TOKEN,edit,1353783428
Control3=IDS_STATUS,static,1342308352
Control4=IDE_STATUS,edit,1350633472

[DLG:IDD_SET (�p�� (��ض))]
Type=1
Class=?
ControlCount=15
Control1=IDE_FILENAME,edit,1350631552
Control2=IDB_BROWSE,button,1342242816
Control3=IDB_CREATEPROCESS,button,1342242816
Control4=IDL_ENABLEGROUPS,listbox,1353777417
Control5=IDB_ADJUSTTOKENGROUPS,button,1342242816
Control6=IDL_ENABLEPRIVILEGES,listbox,1353777419
Control7=IDB_ADJUSTTOKENPRIVILEGES,button,1342242816
Control8=IDB_SETOWNER,button,1342242816
Control9=IDB_SETGROUP,button,1342242816
Control10=IDB_SETDACL,button,1342242816
Control11=IDS_CREATEPROCESS,button,1342177287
Control12=IDS_ADJUSTGROUPS,button,1342177287
Control13=IDS_ADJUSTPRIV,button,1342177287
Control14=IDS_SETTOKENINFORMATION,button,1342177287
Control15=IDC_BUTTON_V,button,1342242827

[DLG:IDD_LIMIT (�p�� (��ض))]
Type=1
Class=?
ControlCount=11
Control1=IDL_DELETEDPRIVILEGES,listbox,1353777419
Control2=IDL_RESTRICTEDSIDS,listbox,1353777411
Control3=IDB_ADDRESTRICTED,button,1342242816
Control4=IDB_REMOVERESTRICTED,button,1342242816
Control5=IDL_DISABLEDSIDS,listbox,1353777419
Control6=IDB_CREATERESTRICTEDTOKEN,button,1342242816
Control7=IDS_DELETEDPRIVILEGES,static,1342308363
Control8=IDS_ADDREMOVERESTRICTED,button,1342177287
Control9=IDS_DISABLEDSIDS,static,1342308363
Control10=IDS_CREATEARESTRICTEDTOKEN,button,1342177287
Control11=IDC_BUTTON_V,button,1342242827

[DLG:IDD_ACCOUNT_LIST (�p�� (��ض))]
Type=1
Class=?
ControlCount=1
Control1=IDC_LIST,SysListView32,1350631437

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_CUT
Command5=ID_EDIT_COPY
Command6=ID_EDIT_PASTE
Command7=ID_FILE_PRINT
Command8=ID_APP_ABOUT
CommandCount=8

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_FILE_SAVE_AS
Command4=ID_FILE_PRINT
Command5=ID_FILE_PRINT_PREVIEW
Command6=ID_FILE_PRINT_SETUP
Command7=ID_FILE_MRU_FILE1
Command8=ID_APP_EXIT
Command9=ID_VIEW_TOOLBAR
Command10=ID_VIEW_STATUS_BAR
Command11=ID_APP_ABOUT
CommandCount=11

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE
CommandCount=14

[DLG:IDD_ABOUTBOX]
Type=1
Class=?
ControlCount=5
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_LINK_CODEGURU,static,1342308352

