
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// InitView.h : CInitView クラスの宣言およびインターフェイスの定義をします。
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_INITVIEW_H__D005CE48_7DCB_4BE1_BF9A_28B704E397AB__INCLUDED_)
#define AFX_INITVIEW_H__D005CE48_7DCB_4BE1_BF9A_28B704E397AB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
class CMainFrame;
class CZTokenManDoc;
#include "clrButton.h"
class CInitView : public CFormView
{
	friend class CMainFrame;
protected: // シリアライズ機能のみから作成します。
	CInitView();
	DECLARE_DYNCREATE(CInitView)

public:
	//{{AFX_DATA(CInitView)
	enum { IDD = IDD_INIT };
	CButton	m_btnLogonUser;
	CButton	m_btnDuplicate;
	CButton	m_btnDumpToken;
	CComboBox	m_comboTokenType;
	CComboBox	m_comboThread;
	CComboBox	m_comboProcess;
	CComboBox	m_comboLogonType;
	CComboBox	m_comboProvider;
	CComboBox	m_comboImpersonate;
	CClrButton	m_btnV;
	CString	m_strUserName;
	CString	m_strPassword;
	//}}AFX_DATA
	BOOL m_bExpand;
	

// アトリビュート
public:
	CZTokenManDoc* GetDocument();
    CMainFrame* m_pW;
	void ArrangeLayout(int cx = 0, int cy = 0);
	void PopulateProcessCombo();
	void PopulateThreadCombo();
	void PopulateStaticCombos();
// オペレーション
public:

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CInitView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV のサポート
	virtual void OnInitialUpdate(); // 構築後の最初の１度だけ呼び出されます。
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL
    
// インプリメンテーション
public:
	virtual ~CInitView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(CInitView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnButtonV();
	afx_msg void OnSelchangeProcesses();
	afx_msg void OnDumpToken();
	afx_msg void OnDuplicateTokenEx();
	afx_msg void OnLogonUser();
	afx_msg void OnButtonEnumUser();
	afx_msg void OnButtonLaunchExeWithUser();
	afx_msg void OnBtnRefresh();
	afx_msg void OnDropdownProcesses();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // InitView.cpp ファイルがデバッグ環境の時使用されます。
inline CZTokenManDoc* CInitView::GetDocument()
   { return (CZTokenManDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_INITVIEW_H__D005CE48_7DCB_4BE1_BF9A_28B704E397AB__INCLUDED_)
