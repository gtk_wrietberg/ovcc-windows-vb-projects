
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#if !defined(AFX_REPORTVIEW_H__90139ECB_E9E6_4A2A_A3E5_2EFF02409E5A__INCLUDED_)
#define AFX_REPORTVIEW_H__90139ECB_E9E6_4A2A_A3E5_2EFF02409E5A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ReportView.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CReportView フォーム ビュー

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif
class CMainFrame;
class CReportView : public CFormView
{
protected:
	CReportView();           // 動的生成に使用されるプロテクト コンストラクタ。
	DECLARE_DYNCREATE(CReportView)

// フォーム データ
public:
	//{{AFX_DATA(CReportView)
	enum { IDD = IDD_REPORT };
	CStatic	m_staticStatus;
	CStatic	m_staticToken;
	CEdit	m_editToken;
	CEdit	m_editStatus;
	//}}AFX_DATA

// アトリビュート
public:
	CMainFrame* m_pW;
	void ArrangeLayout(int cx = 0, int cy = 0);
// オペレーション
public:

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CReportView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	virtual ~CReportView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CReportView)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_REPORTVIEW_H__90139ECB_E9E6_4A2A_A3E5_2EFF02409E5A__INCLUDED_)
