
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// SetView.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "ZTokenMan.h"
#include "SetView.h"
#include "MainFrm.h"
#include "InitView.h"
#include "ReportView.h"
#include "GlobalVar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSetView

IMPLEMENT_DYNCREATE(CSetView, CFormView)

CSetView::CSetView()
	: CFormView(CSetView::IDD)
{
	//{{AFX_DATA_INIT(CSetView)
		// メモ: ClassWizard はこの位置にメンバの初期化処理を追加します
	//}}AFX_DATA_INIT
	m_pW = NULL;
	m_bExpand = TRUE;
}

CSetView::~CSetView()
{
}

void CSetView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSetView)
	DDX_Control(pDX, IDE_FILENAME, m_editFileName);
	DDX_Control(pDX, IDL_ENABLEPRIVILEGES, m_listEnablePrivilege);
	DDX_Control(pDX, IDL_ENABLEGROUPS, m_listEnableGroup);
	DDX_Control(pDX, IDC_BUTTON_V, m_btnV);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSetView, CFormView)
	//{{AFX_MSG_MAP(CSetView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_V, OnButtonV)
	ON_BN_CLICKED(IDB_BROWSE, OnBrowseExe)
	ON_BN_CLICKED(IDB_CREATEPROCESS, OnCreateProcessAsUser)
	ON_BN_CLICKED(IDB_ADJUSTTOKENGROUPS, OnAdjustTokenGroups)
	ON_BN_CLICKED(IDB_ADJUSTTOKENPRIVILEGES, OnAdjustTokenPrivileges)
	ON_BN_CLICKED(IDB_SETOWNER, OnSetOwner)
	ON_BN_CLICKED(IDB_SETGROUP, OnSetGroup)
	ON_BN_CLICKED(IDB_SETDACL, OnSetDacl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSetView 診断

#ifdef _DEBUG
void CSetView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSetView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CSetView メッセージ ハンドラ

void CSetView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	g_hwndEnablePrivileges = ::GetDlgItem(m_hWnd, IDL_ENABLEPRIVILEGES);
    g_hwndEnableGroups = ::GetDlgItem(m_hWnd, IDL_ENABLEGROUPS);
	if(!m_pW)
	{
		m_pW = (CMainFrame*)AfxGetMainWnd();
		m_pW->m_pSet = this;
	}
	ArrangeLayout(1,1);
	m_btnV.SetColor(RGB(255,255,255), BLUE);
    m_btnV.SetWindowText(_T(""));
}

void CSetView::OnSize(UINT nType, int cx, int cy) 
{
	SetScrollSizes(MM_TEXT, CSize(cx, cy));
	CFormView::OnSize(nType, cx, cy);

	ArrangeLayout(cx, cy);
	Invalidate();
}

void CSetView::ArrangeLayout(int cx, int cy)
{
	if(!m_bExpand) 
	{
		CRect rect;
		m_btnV.GetClientRect(&rect);
		m_pW->m_wndSplitter2.SetColumnInfo(0, 
				rect.Width() ,  rect.Width());
	    m_pW->m_wndSplitter2.RecalcLayout();
		CRect rectClient;
		this->GetClientRect(&rectClient);
		rect.bottom = rectClient.bottom;
		m_btnV.MoveWindow(&rect);
		m_btnV.SetColor(BLACK, BLUE);//RGB(255,255,128));
		return;
	}

	BOOL static first = TRUE;
	if(first)
	{
		first = FALSE;
	}
	else
	{
		if(m_pW && m_pW->GetSafeHwnd())
		{
            HRSRC  hRsrc = ::FindResource(NULL, MAKEINTRESOURCE(IDD_SET), RT_DIALOG);
	        ASSERT(hRsrc);
            HGLOBAL hGlobal = LoadResource(NULL, hRsrc);
            ASSERT(hGlobal);
            //DWORD dwSize = GlobalSize(hGlobal);
            DLGTEMPLATE* lpStr = (DLGTEMPLATE*)hGlobal;
	        int cx1 = lpStr->cx;
	        int cy1 = lpStr->cy;
	        CRect rect;
	        rect.SetRect(0,0,cx1,cy1);
	        ::MapDialogRect(GetSafeHwnd(), &rect);
            
			if(cx > 0 && cx < 2000)
			{
//				if(cx >= rect.Width())
//				{
//					rect.right = rect.left + cx;
//				}
				//Move Ret Edit
			}
//			CRect rectTotal;
//			m_pW->GetClientRect(&rectTotal);
//	        int hScrollBarWidth = GetSystemMetrics(SM_CXHSCROLL);
//	        rect.right += hScrollBarWidth;
	        //void SetRowInfo( int row, int cyIdeal, int cyMin );
            m_pW->m_wndSplitter2.SetColumnInfo(0, 
				rect.Width() ,  rect.Width());
	        m_pW->m_wndSplitter2.RecalcLayout();

			m_btnV.GetClientRect(&rect);
			CRect rectClient;
		    this->GetClientRect(&rectClient);
		    rect.bottom = rectClient.bottom;
		    m_btnV.MoveWindow(&rect);
			m_btnV.SetColor(BLACK, MAGENTA);
		}
	}
}

void CSetView::OnButtonV() 
{
	if(m_bExpand)
	{
		m_bExpand = !m_bExpand;
		CRect rect;
		m_btnV.GetClientRect(&rect);
		m_pW->m_wndSplitter2.SetColumnInfo(0, 
				rect.Width(),  rect.Width());
        m_pW->m_wndSplitter2.RecalcLayout();
		::SetFocus(::g_hwndToken);
	}
	else
	{
		m_bExpand = !m_bExpand;
        ArrangeLayout(0,0);
		m_editFileName.SetFocus();
	}
}

void CSetView::OnBrowseExe() 
{
	CString strIniPath = _T("");
	//int nRet = ::GetSystemDirectory(strIniPath.GetBuffer(MAX_PATH), MAX_PATH);
	//strIniPath.ReleaseBuffer(nRet);
	//static TCHAR BASED_CODE szFilter[] = _T("Exe Files (*.exe)|All Files (*.*)|*.*||");
    
    //CFileDialog dlg(TRUE,_T("EXE"), strIniPath + _T("\\*.exe"), 
	//	OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, NULL); //szFilter);
	//if(dlg.DoModal() != IDOK) return;
	//this->m_strPath = dlg.GetPathName();

    //You can not run the above code for GP error 
	static TCHAR szFilter[] = _T("Exe Files (*.exe)|All Files (*.*)|*.*||");
	CFileDialog* pDlg = new CFileDialog(TRUE, _T("EXE"),
		  strIniPath + _T("\\*.exe"),
		  OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter);
	if(pDlg->DoModal() != IDOK) { delete pDlg; return;}
	CString strPath = pDlg->GetPathName(); delete pDlg;
    ::SetDlgItemText(this->GetSafeHwnd(), IDE_FILENAME, (LPCTSTR)strPath);
}

void CSetView::OnCreateProcessAsUser() 
{
	try {{

      // No token?  Adios!
      if (g_hToken == NULL) {
         Status(TEXT("No Token"), 0);
         goto leave;
      }

      // Get current filename text
      TCHAR szFileBuf[MAX_PATH];
      *szFileBuf = 0;
      ::GetDlgItemText(m_hWnd, IDE_FILENAME, szFileBuf, chDIMOF(szFileBuf));

      STARTUPINFO si;
      ZeroMemory(&si, sizeof(si));
      si.cb = sizeof(si);

      // Create a new process with our current token
      PROCESS_INFORMATION pi;
      if (CreateProcessAsUser(g_hToken, NULL, szFileBuf, NULL, NULL, FALSE, 0,
            NULL, NULL, &si, &pi)) {
         CloseHandle(pi.hProcess);
         CloseHandle(pi.hThread);
      } else
         Status(TEXT("CreateProcessAsUser"), GetLastError());

   } leave:;
   } catch(...) {}
}

void CSetView::OnAdjustTokenGroups() 
{
	PTOKEN_GROUPS ptgGroups = NULL;

    try {{
		// No token?  Give up
        if(g_hToken == NULL) 
		{
			Status(TEXT("No Token"), 0);
            goto leave;
		} 

        // Allocate a buffer with the token information
        ptgGroups = (PTOKEN_GROUPS) AllocateTokenInfo(g_hToken, TokenGroups);
        if (ptgGroups == NULL)
			goto leave;

        // Enumerate through the list box and find groups to enable in the token
        DWORD dwItem = ::SendMessage(g_hwndEnableGroups, LB_GETCOUNT, 0, 0);
        while (dwItem-- != 0) 
		{
			DWORD dwIndex = ::SendMessage(g_hwndEnableGroups, LB_GETITEMDATA,
               dwItem, 0);
		    BOOL fSel = ::SendMessage(g_hwndEnableGroups, LB_GETSEL, dwItem, 0);
            if(fSel)
				ptgGroups->Groups[dwIndex].Attributes |= SE_GROUP_ENABLED;
            else
                ptgGroups->Groups[dwIndex].Attributes &= (~SE_GROUP_ENABLED);
		}

        // Actually adjust the token groups for the token
        if (!AdjustTokenGroups(g_hToken, FALSE, ptgGroups, 0, NULL, NULL))
			Status(TEXT("AdjustTokenGroups"), GetLastError());
        else
            DumpToken();   // Display the new token

    } leave:;
    } catch(...) {}

    // Cleanup
    if (ptgGroups != NULL)
		LocalFree(ptgGroups);
}

void CSetView::OnAdjustTokenPrivileges() 
{
	PTOKEN_PRIVILEGES ptpPrivileges = NULL;

   try {{

      // No token?  Buh-Bye
      if (g_hToken == NULL) {
         Status(TEXT("No Token"), 0);
         goto leave;
      }

      // Get the token information for privileges
      ptpPrivileges = (PTOKEN_PRIVILEGES) AllocateTokenInfo(g_hToken,
            TokenPrivileges);
      if (ptpPrivileges == NULL)
         goto leave;

      // Enumerate privileges to enable
      DWORD dwItem = ::SendMessage(g_hwndEnablePrivileges, LB_GETCOUNT, 0, 0);
      while (dwItem-- != 0) {

		  DWORD dwIndex = ::SendMessage(g_hwndEnablePrivileges, LB_GETITEMDATA,
               dwItem, 0);
         BOOL fSel = ::SendMessage(g_hwndEnablePrivileges, LB_GETSEL, dwItem, 0);
         if (fSel)
            ptpPrivileges->Privileges[dwIndex].Attributes |=
                  SE_PRIVILEGE_ENABLED;
         else
            ptpPrivileges->Privileges[dwIndex].Attributes &=
                  ~SE_PRIVILEGE_ENABLED;
      }

      // Adjust the privileges for the token
      if (!AdjustTokenPrivileges(g_hToken, FALSE, ptpPrivileges, 0, NULL,
            NULL))
         Status(TEXT("AdjustTokenPrivileges"), GetLastError());
      else
         DumpToken();   // Display the new token

   } leave:;
   } catch(...) {}

   // Cleanup
   if (ptpPrivileges != NULL)
      LocalFree(ptpPrivileges);
}

void CSetView::OnSetOwner() 
{
	BOOL fOwner = TRUE;
	PTSTR szStatus = TEXT("Item Set");
    PSID  psid     = NULL;

    try {{
		// No token?  Scram!
        if (g_hToken == NULL) 
		{
			szStatus = TEXT("No Token");
            SetLastError(0);
            goto leave;
		}

        // Get an account name
        TCHAR szName[1024];
        if (!GetAccountName(m_hWnd, szName, chDIMOF(szName), TRUE, TRUE))  
		{
			szStatus = TEXT("User Cancelled Item Selection");
            SetLastError(0);
            goto leave;
		}

        // Get the Sid for the name
        DWORD dwSize = 0;
        TCHAR szDomainName[1024];
        DWORD dwDomSize = chDIMOF(szDomainName);
        SID_NAME_USE sidUse;
        LookupAccountName(NULL, szName, NULL, &dwSize, szDomainName, &dwDomSize,
            &sidUse);

        psid = (PSID) GlobalAlloc(GPTR, dwSize);
        if (psid == NULL) 
		{
			szStatus = TEXT("GlobalAlloc");
            goto leave;
		}

        if (!LookupAccountName(NULL, szName, psid, &dwSize, szDomainName,
            &dwDomSize, &sidUse)) 
		{
			szStatus = TEXT("LookupAccountName");
            goto leave;
		}

        // Set the token information using the TOKEN_OWNER structure,
        TOKEN_OWNER to;
        to.Owner = psid;
        if (!SetTokenInformation(g_hToken, fOwner ? TokenOwner :
            TokenPrimaryGroup, &to, sizeof(to))) 
		{
			szStatus = TEXT("SetTokenInformation");
            goto leave;
		}

        DumpToken();

        SetLastError(0);

    } leave:;
    } catch(...) {}

    // Cleanup
    Status(szStatus, GetLastError());

    if (psid != NULL)
		GlobalFree(psid);
}

void CSetView::OnSetGroup() 
{
	BOOL fOwner = FALSE;
	PTSTR szStatus = TEXT("Item Set");
    PSID  psid     = NULL;

    try {{
		// No token?  Scram!
        if (g_hToken == NULL) 
		{
			szStatus = TEXT("No Token");
            SetLastError(0);
            goto leave;
		}

        // Get an account name
        TCHAR szName[1024];
        if (!GetAccountName(m_hWnd, szName, chDIMOF(szName), TRUE, TRUE))  
		{
			szStatus = TEXT("User Cancelled Item Selection");
            SetLastError(0);
            goto leave;
		}

        // Get the Sid for the name
        DWORD dwSize = 0;
        TCHAR szDomainName[1024];
        DWORD dwDomSize = chDIMOF(szDomainName);
        SID_NAME_USE sidUse;
        LookupAccountName(NULL, szName, NULL, &dwSize, szDomainName, &dwDomSize,
            &sidUse);

        psid = (PSID) GlobalAlloc(GPTR, dwSize);
        if (psid == NULL) 
		{
			szStatus = TEXT("GlobalAlloc");
            goto leave;
		}

        if (!LookupAccountName(NULL, szName, psid, &dwSize, szDomainName,
            &dwDomSize, &sidUse)) 
		{
			szStatus = TEXT("LookupAccountName");
            goto leave;
		}

        // Set the token information using the TOKEN_OWNER structure,
        TOKEN_OWNER to;
        to.Owner = psid;
        if (!SetTokenInformation(g_hToken, fOwner ? TokenOwner :
            TokenPrimaryGroup, &to, sizeof(to))) 
		{
			szStatus = TEXT("SetTokenInformation");
            goto leave;
		}

        DumpToken();

        SetLastError(0);

    } leave:;
    } catch(...) {}

    // Cleanup
    Status(szStatus, GetLastError());

    if (psid != NULL)
		GlobalFree(psid);
}

#include "SecurityPage.h"
//#include "SecInfo.h"
void CSetView::OnSetDacl() 
{
	PSECURITY_DESCRIPTOR pSD  = NULL;
    CSecurityPage*       pSec = NULL;

    PTSTR szStatus = TEXT("Default DACL Set");

    try {{
		// No token?  Outta here
        if(g_hToken == NULL) 
		{
			szStatus = TEXT("No Token");
            goto leave;
		}

        // Get the default dacl for the token
        PTOKEN_DEFAULT_DACL ptdDacl = (PTOKEN_DEFAULT_DACL) AllocateTokenInfo(
            g_hToken, TokenDefaultDacl);
        if(ptdDacl == NULL) 
		{
			szStatus = TEXT("Unable to get default DACL");
            SetLastError(0);
            goto leave;
		}

        // Allocate memory for an SD
        pSD = (PSECURITY_DESCRIPTOR) GlobalAlloc(GPTR,
            SECURITY_DESCRIPTOR_MIN_LENGTH);
        if(pSD == NULL) 
		{
			szStatus = TEXT("GlobalAlloc");
            goto leave;
		}

        // Initialize it
        if(!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION)) 
		{
			szStatus = TEXT("InitializeSecurityDescriptor");
            goto leave;
		}

        // Set the security descriptor DACL to the default DACL of the token
        if (!SetSecurityDescriptorDacl(pSD, TRUE, ptdDacl->DefaultDacl, FALSE)) 
		{
			szStatus = TEXT("SetSecurityDescriptorDacl");
            goto leave;
		}

        // Create an instance of the CSecurityPage object with our SD.
        // This is derived from the ISecurityInformation interface defined by
        // the SDK.  It will be passed to the EditSecurity function to produce
        // the common ACL editor Dialog Box.
        pSec = new CSecurityPage(pSD);
        if (pSec == NULL)
			goto leave;

        // Common dialog box for ACL editing
        if (EditSecurity(m_hWnd, pSec) && pSec->IsModified())
			DumpToken(); // If success, then redisplay

        SetLastError(0);

    } leave:;
    } catch(...) {}

    // Cleanup
    Status(szStatus, GetLastError());

    if (pSD != NULL)
		GlobalFree(pSD);

    if (pSec != NULL)
        pSec->Release();
}
