
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// MainFrm.cpp : CMainFrame クラスの動作の定義を行います。
//

#include "stdafx.h"
#include "ZTokenMan.h"

#include "MainFrm.h"
#include "LimitView.h"
#include "SetView.h"
#include "ReportView.h"
#include "InitView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_ROTATE, OnRotate)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // ステータス ライン インジケータ
	ID_TTT, 
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame クラスの構築/消滅

CMainFrame::CMainFrame()
{
	m_pInit = NULL;
    m_pSet = NULL;
    m_pReport = NULL;
    m_pLimit = NULL;
	m_bDumping = FALSE;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // 作成に失敗
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // 作成に失敗
	}

	// TODO: ツール バーをドッキング可能にしない場合は以下の３行を削除
	//       してください。
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	m_wndStatusBar.SetPaneStyle(1,SBPS_STRETCH); 

	if(m_StatusRainbow.m_hWnd == NULL)
    {
		// create it 
        m_StatusRainbow.Create(&m_wndStatusBar, ID_TTT, WS_VISIBLE | SS_ICON | SS_CENTERIMAGE);
	}
	m_StatusRainbow.SetWindowText(_T("abc"));

	TCHAR szTitle[1024];
    lstrcpy(szTitle, TEXT("ZTokenMan is running as \""));
    ULONG lSize = chDIMOF(szTitle) - lstrlen(szTitle);
    GetUserName(szTitle+lstrlen(szTitle),&lSize);
    lstrcat(szTitle, TEXT("\""));
    ::SetWindowText(this->m_hWnd, szTitle);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~FWS_ADDTOTITLE;
	cs.cx = 900;
	cs.cy = 600;
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: この位置で CREATESTRUCT cs を修正して、Window クラスやスタイルを
	//       修正してください。

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame クラスの診断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame メッセージ ハンドラ


BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
	if (!m_wndSplitter.CreateStatic(this, 2, 1)) 
		return FALSE;
	
	if (!m_wndSplitter2.CreateStatic(&m_wndSplitter, 1, 2,
            WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter.IdFromRowCol (1, 0)))
		return FALSE;

	if (!m_wndSplitter3.CreateStatic(&m_wndSplitter2, 2, 1,
            WS_CHILD | WS_VISIBLE | WS_BORDER, m_wndSplitter2.IdFromRowCol (0, 1)))
		return FALSE;

	if (!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CInitView),
		  CSize(10, 10), pContext))
		return FALSE;

    if (!m_wndSplitter2.CreateView(0, 0, RUNTIME_CLASS(CSetView), 
		    CSize(0, 100), pContext))
		return FALSE;
	
	if (!m_wndSplitter3.CreateView(1, 0, RUNTIME_CLASS(CReportView), 
		    CSize(200, 100), pContext))
	    return FALSE;

	if (!m_wndSplitter3.CreateView(0, 0, RUNTIME_CLASS(CLimitView), 
		    CSize(200, 100), pContext))
	    return FALSE;

	return TRUE;
	//return CFrameWnd::OnCreateClient(lpcs, pContext);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy) 
{
	CFrameWnd::OnSize(nType, cx, cy);
	
	m_StatusRainbow.Reposition();
}

LRESULT CMainFrame::OnRotate(WPARAM wParam, LPARAM lParam)
{
	if(wParam == 0) //Dump Over
	{
		m_bDumping = FALSE;
		m_pInit->m_btnLogonUser.EnableWindow(TRUE);
	    m_pInit->m_btnDuplicate.EnableWindow(TRUE);
	    m_pInit->m_btnDumpToken.EnableWindow(TRUE);
		m_StatusRainbow.Rotate(FALSE);
	}
	else
	{
		m_bDumping = TRUE;
		m_pInit->m_btnLogonUser.EnableWindow(FALSE);
	    m_pInit->m_btnDuplicate.EnableWindow(FALSE);
	    m_pInit->m_btnDumpToken.EnableWindow(FALSE);
		m_StatusRainbow.Rotate(TRUE);
	}
	return 0;
}
