
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#ifndef ZZZ
#define ZZZ
//Last Revised : 2002/2/26
CString BrowsePrinter(CString strDescription);
CString BrowsePath(CString strDescription = _T("Choose Path"), UINT uFlag = 0);

//Always Convert Whole String
LPWSTR myA2W(LPSTR szMultiByte, DWORD& dwSize);

//Able Convert Part Str->WStr
//return in WCHARs
LPWSTR _A2W(LPSTR szMultiByte, DWORD dwSize, DWORD& dwRetSize);

//return BYTEs, 
//dwSize in WCHAR
LPSTR _W2A(LPWSTR szUnicode, DWORD dwSize, DWORD& dwRetSize);

//2002.2.21
//Able Convert Part Str->WStr //return in WCHARs
//Can handle any size string, but user must VirtualFree the
//return pointer
LPWSTR _A2Wex(LPSTR szMultiByte, DWORD dwSize, DWORD& dwRetSize);

//return BYTEs, 
//dwSize in WCHAR
//Can handle any size string, but user must VirtualFree the
//return pointer
LPSTR _W2Aex(LPWSTR szUnicode, DWORD dwSize, DWORD& dwRetSize);
CString GetClipText();
void SetClipText(CString str);
LONG StringToHex(LPCTSTR strHex);


//2002.1.30
//Full Path is limited 248 long 
BOOL ZMakeSureDirectoryPathExistsA(LPCSTR szFullPath);
//Full Path is limited 248 long 
BOOL ZMakeSureDirectoryPathExistsW(LPCWSTR szFullPath);

#ifdef _UNICODE
#define ZMakeSureDirectoryPathExists ZMakeSureDirectoryPathExistsW
#else
#define ZMakeSureDirectoryPathExists ZMakeSureDirectoryPathExistsA
#endif

#include "CmnHdr.h"

//------------Log 
//Log Process & Thread Info
BOOL WriteProcessLog(LPCTSTR szFilename, LPCTSTR szRoutine, DWORD dwPID, DWORD dwTID);
//Thread Safe Version of WriteLineLog
BOOL WriteLineLogMT(LPCTSTR szFilename, LPCTSTR szMessage);
BOOL WriteLineLog(LPCTSTR szFilename, LPCTSTR szMessage);

//------------Security Setting
BOOL SetCurrentPrivilege(
       LPWSTR TargetComputer,  // target of privilege operation
       LPCWSTR Privilege,      // Privilege to enable/disable
       BOOL bEnablePrivilege   // to enable or disable privilege
       );
void ShutDown();

void PopMsg(LPCTSTR pszFormat, ...) ;
void ReportErr(LPCTSTR str);
void ReportErrEx(LPCTSTR pszFormat, ...) ;
#include <Winnetwk.h>
void ReportWNetErr(DWORD dwErr, LPCTSTR str);
#pragma comment(lib, "Mpr")

LONG StringToHex(LPCTSTR strHex);

class CZCriticalSection
{
public:
	CZCriticalSection(BOOL fInitiallyUp = TRUE, PCTSTR pszName = NULL)
    {
		//The following may fail
		::InitializeCriticalSection(&m_cs);
		//If need Info about if he Critical Section allocated or not, use
		//InitializeCriticalSectionAndSpinCount(&m_cs, 0);
	}

    ~CZCriticalSection()
	{ 
		::DeleteCriticalSection(&m_cs); 
    }

	//Win2k Only, Win98 return FALSE always
	BOOL TryEnterCriticalSection()
	{
		return ::TryEnterCriticalSection(&m_cs);
	}

	void EnterCriticalSection()
	{
		::EnterCriticalSection(&m_cs);
	}

	void LeaveCriticalSection()
	{
		::LeaveCriticalSection(&m_cs);
	}
private:
    CRITICAL_SECTION  m_cs;
};

#endif