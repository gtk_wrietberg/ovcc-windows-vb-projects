
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// InitView.cpp : CInitView クラスの動作の定義を行います。
//

#include "stdafx.h"
#include "ZTokenMan.h"

#include "ZTokenManDoc.h"
#include "SetView.h"
#include "MainFrm.h"
#include "InitView.h"
#include "ReportView.h"
#include "GlobalVar.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInitView

IMPLEMENT_DYNCREATE(CInitView, CFormView)

BEGIN_MESSAGE_MAP(CInitView, CFormView)
	//{{AFX_MSG_MAP(CInitView)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_V, OnButtonV)
	ON_CBN_SELCHANGE(IDC_PROCESSES, OnSelchangeProcesses)
	ON_BN_CLICKED(IDB_DUMPTOKEN, OnDumpToken)
	ON_BN_CLICKED(IDB_DUPLICATE, OnDuplicateTokenEx)
	ON_BN_CLICKED(IDB_LOGONUSER, OnLogonUser)
	ON_BN_CLICKED(IDC_BUTTON_ENUM_USER, OnButtonEnumUser)
	ON_BN_CLICKED(IDC_BUTTON1, OnButtonLaunchExeWithUser)
	ON_BN_CLICKED(IDC_BTN_REFRESH, OnBtnRefresh)
	ON_CBN_DROPDOWN(IDC_PROCESSES, OnDropdownProcesses)
	//}}AFX_MSG_MAP
	// 標準印刷コマンド
	ON_COMMAND(ID_FILE_PRINT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CFormView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CFormView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInitView クラスの構築/消滅

CInitView::CInitView()
	: CFormView(CInitView::IDD)
{
	//{{AFX_DATA_INIT(CInitView)
	m_strUserName = _T("");
	m_strPassword = _T("");
	//}}AFX_DATA_INIT
	// TODO: この場所に構築用のコードを追加してください。
    m_pW = NULL;
	m_bExpand = TRUE;
}

CInitView::~CInitView()
{
}

void CInitView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInitView)
	DDX_Control(pDX, IDB_LOGONUSER, m_btnLogonUser);
	DDX_Control(pDX, IDB_DUPLICATE, m_btnDuplicate);
	DDX_Control(pDX, IDB_DUMPTOKEN, m_btnDumpToken);
	DDX_Control(pDX, IDC_TOKENTYPE, m_comboTokenType);
	DDX_Control(pDX, IDC_THREADS, m_comboThread);
	DDX_Control(pDX, IDC_PROCESSES, m_comboProcess);
	DDX_Control(pDX, IDC_LOGONTYPE, m_comboLogonType);
	DDX_Control(pDX, IDC_LOGONPROVIDER, m_comboProvider);
	DDX_Control(pDX, IDC_IMPERSONATIONLEVEL, m_comboImpersonate);
	DDX_Control(pDX, IDC_BUTTON_V, m_btnV);
	DDX_Text(pDX, IDE_USERNAME, m_strUserName);
	DDX_Text(pDX, IDE_PASSWORD, m_strPassword);
	//}}AFX_DATA_MAP
}

BOOL CInitView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: この位置で CREATESTRUCT cs を修正して Window クラスまたはスタイルを
	//  修正してください。
	return CFormView::PreCreateWindow(cs);
}

void CInitView::OnInitialUpdate()
{
	if(!m_pW)
	{
		m_pW = (CMainFrame*)AfxGetMainWnd();
		m_pW->m_pInit = this;
	}
	CFormView::OnInitialUpdate();
	g_hwndProcessCombo = ::GetDlgItem(m_hWnd, IDC_PROCESSES);
    g_hwndThreadCombo = ::GetDlgItem(m_hWnd, IDC_THREADS);
	g_hwndLogonTypes = ::GetDlgItem(m_hWnd, IDC_LOGONTYPE);
    g_hwndLogonProviders = ::GetDlgItem(m_hWnd, IDC_LOGONPROVIDER);
    g_hwndImpersonationLevels = ::GetDlgItem(m_hWnd, IDC_IMPERSONATIONLEVEL);
    g_hwndTokenTypes = ::GetDlgItem(m_hWnd, IDC_TOKENTYPE);

	::SendMessage(::GetDlgItem(m_hWnd, IDC_CHECK_COPY_TOKEN), 
		BM_SETCHECK,BST_CHECKED,0);
	//	GetParentFrame()->RecalcLayout();
    //	ResizeParentToFit();
    ArrangeLayout(1,1);
	m_btnV.SetColor(BLACK, GREEN);
    m_btnV.SetWindowText(_T("--------------------------"));

	// If we already have one, close it
    if (g_hSnapShot != NULL)
      CloseHandle(g_hSnapShot);

    g_hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS
         | TH32CS_SNAPTHREAD, 0);
	PopulateProcessCombo();
	PopulateThreadCombo();
	PopulateStaticCombos();
}

/////////////////////////////////////////////////////////////////////////////
// CInitView クラスの印刷

BOOL CInitView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// デフォルトの印刷準備
	return DoPreparePrinting(pInfo);
}

void CInitView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷前の特別な初期化処理を追加してください。
}

void CInitView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷後の後処理を追加してください。
}

void CInitView::OnPrint(CDC* pDC, CPrintInfo* /*pInfo*/)
{
	// TODO: 印刷用のコードをここに追加してください。
}

/////////////////////////////////////////////////////////////////////////////
// CInitView クラスの診断

#ifdef _DEBUG
void CInitView::AssertValid() const
{
	CFormView::AssertValid();
}

void CInitView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CZTokenManDoc* CInitView::GetDocument() // 非デバッグ バージョンはインラインです。
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CZTokenManDoc)));
	return (CZTokenManDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CInitView クラスのメッセージ ハンドラ

void CInitView::OnSize(UINT nType, int cx, int cy) 
{
	
	SetScrollSizes(MM_TEXT, CSize(cx, cy));

	CFormView::OnSize(nType, cx, cy);
	ArrangeLayout(cx, cy);
	Invalidate();
}

void CInitView::ArrangeLayout(int cx, int cy)
{
	if(!m_bExpand) 
	{
		CRect rect;
		m_btnV.GetClientRect(&rect);
		m_pW->m_wndSplitter.SetRowInfo(0, 
				rect.Height() ,  rect.Height());
	    m_pW->m_wndSplitter.RecalcLayout();
		CRect rectClient;
		this->GetClientRect(&rectClient);
		rect.right = rectClient.right;
		m_btnV.MoveWindow(&rect);
		m_btnV.SetColor(WHITE, YELLOW);//RGB(255,255,128));
		return;
	}

	BOOL static first = TRUE;
	if(first)
	{
		first = FALSE;
	}
	else
	{
		if(m_pW && m_pW->GetSafeHwnd())
		{
            HRSRC  hRsrc = ::FindResource(NULL, MAKEINTRESOURCE(IDD_INIT), RT_DIALOG);
	        ASSERT(hRsrc);
            HGLOBAL hGlobal = LoadResource(NULL, hRsrc);
            ASSERT(hGlobal);
            //DWORD dwSize = GlobalSize(hGlobal);
            DLGTEMPLATE* lpStr = (DLGTEMPLATE*)hGlobal;
	        int cx1 = lpStr->cx;
	        int cy1 = lpStr->cy;
	        CRect rect;
	        rect.SetRect(0,0,cx1,cy1);
	        ::MapDialogRect(GetSafeHwnd(), &rect);
            
			if(cx > 0 && cx < 2000)
			{
				if(cy >= rect.Height())
				{
					//rect.bottom = rect.top + cy;
				}
			}
            m_pW->m_wndSplitter.SetRowInfo(0, 
				rect.Height() ,  rect.Height());
	        m_pW->m_wndSplitter.RecalcLayout();

			m_btnV.GetClientRect(&rect);
			CRect rectClient;
		    this->GetClientRect(&rectClient);
		    rect.right = rectClient.right;
		    m_btnV.MoveWindow(&rect);
			m_btnV.SetColor(BLACK, GREEN);
		}
	}
}

void CInitView::OnButtonV() 
{
	if(m_bExpand)
	{
		m_bExpand = !m_bExpand;
		CRect rect;
		m_btnV.GetClientRect(&rect);
		m_pW->m_wndSplitter.SetRowInfo(0, 
				rect.Height() ,  rect.Height());
        m_pW->m_wndSplitter.RecalcLayout();
		::SetFocus(::g_hwndToken);
	}
	else
	{
		m_bExpand = !m_bExpand;
        ArrangeLayout(0,0);
		m_comboProcess.SetFocus();
	}	
}

void CInitView::PopulateProcessCombo()
{
	m_comboProcess.ResetContent();
	// No snapshot means we empty the combo
   if (g_hSnapShot != NULL) 
   {
	   // Iterate through the process list adding them to the combo
       PROCESSENTRY32 pentry;
       pentry.dwSize = sizeof(pentry);
       BOOL fIsProcess = Process32First(g_hSnapShot, &pentry);
	   int index;
       while (fIsProcess)
	   {
		   if (pentry.th32ProcessID != 0)
		   {
			   CString strInfo; 
               strInfo.Format(_T("%s   PID 0x%x-%d"), (LPCTSTR)pentry.szExeFile,
				   pentry.th32ProcessID, pentry.th32ProcessID);
			   index = m_comboProcess.AddString(strInfo);
		   }
		   else
		   {
		       // Special Case... The Idle Process has a zero ID
               index = m_comboProcess.AddString(TEXT("[System Idle Process]"));
               m_comboProcess.SetCurSel(index);
		   }

           // Set the item data to the processes ID
           m_comboProcess.SetItemData(index, pentry.th32ProcessID);

           // If the process ID matches the last one, we found it
           //if (pentry.th32ProcessID == dwLastID)
		   //   SendMessage(g_hwndProcessCombo, CB_SETCURSEL, lItem, 0);

           fIsProcess = Process32Next(g_hSnapShot, &pentry);
	   }
   }
}

void CInitView::OnSelchangeProcesses() 
{
	PopulateThreadCombo();
}

void CInitView::PopulateThreadCombo() 
{
	// Get process id
    int index = m_comboProcess.GetCurSel();
    DWORD dwPID = m_comboProcess.GetItemData(index);

    // We want the selected thread to stick, if possible
//   lIndex = SendMessage(g_hwndThreadCombo, CB_GETCURSEL, 0, 0);
//   DWORD dwLastThreadID = SendMessage(g_hwndThreadCombo, CB_GETITEMDATA,
//         lIndex, 0);

    m_comboThread.ResetContent();

    // Add that "No Thread" option
    index = m_comboThread.AddString(TEXT("[No Thread]"));
    m_comboThread.SetItemData(index, 0);
    m_comboThread.SetCurSel(index);

	if(g_hSnapShot != NULL) 
	{
		// Iterate through the threads adding them
        TCHAR szBuf[256];
        THREADENTRY32 entry;
        entry.dwSize = sizeof(entry);
        BOOL fIsThread = Thread32First(g_hSnapShot, &entry);
        while (fIsThread) 
		{
			if (entry.th32OwnerProcessID == dwPID)
			{
				wsprintf(szBuf, TEXT("ID = %d"), entry.th32ThreadID);
                index = m_comboThread.AddString((LPCTSTR)szBuf);
                m_comboThread.SetItemData(index, entry.th32ThreadID);

                // Last thread selected?  If so reselect
//                if (entry.th32ThreadID == dwLastThreadID)
//                    SendMessage(g_hwndThreadCombo, CB_SETCURSEL, lIndex, 0);
         }
         fIsThread = Thread32Next(g_hSnapShot, &entry);
      }
   }
}

void CInitView::PopulateStaticCombos() 
{
	int nIndex = ::SendMessage(g_hwndLogonTypes, CB_ADDSTRING, 0,
         (LPARAM) L"Batch");
	::SendMessage(g_hwndLogonTypes, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_BATCH);

	nIndex = ::SendMessage(g_hwndLogonTypes, CB_ADDSTRING, 0,
         (LPARAM) L"Network");
	::SendMessage(g_hwndLogonTypes, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_NETWORK);

	nIndex = ::SendMessage(g_hwndLogonTypes, CB_ADDSTRING, 0,
         (LPARAM) L"Network Cleartext");
	::SendMessage(g_hwndLogonTypes, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_NETWORK_CLEARTEXT);

    nIndex = ::SendMessage(g_hwndLogonTypes, CB_ADDSTRING, 0,
         (LPARAM) L"New Credentials");
    ::SendMessage(g_hwndLogonTypes, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_NEW_CREDENTIALS);

    nIndex = ::SendMessage(g_hwndLogonTypes, CB_ADDSTRING, 0,
         (LPARAM) L"Service");
    ::SendMessage(g_hwndLogonTypes, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_SERVICE);

    nIndex = ::SendMessage(g_hwndLogonTypes, CB_ADDSTRING, 0,
         (LPARAM) L"Unlock");
    ::SendMessage(g_hwndLogonTypes, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_UNLOCK);

    nIndex = ::SendMessage(g_hwndLogonTypes, CB_ADDSTRING, 0,
         (LPARAM) L"Interactive");
    ::SendMessage(g_hwndLogonTypes, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_INTERACTIVE);
    ::SendMessage(g_hwndLogonTypes, CB_SETCURSEL, nIndex, 0);


    nIndex = ::SendMessage(g_hwndLogonProviders, CB_ADDSTRING, 0,
         (LPARAM) L"Windows 2000");
    ::SendMessage(g_hwndLogonProviders, CB_SETITEMDATA, nIndex,
         LOGON32_PROVIDER_WINNT50);

    nIndex = ::SendMessage(g_hwndLogonProviders, CB_ADDSTRING, 0,
         (LPARAM) L"Windows NT 4.0");
    ::SendMessage(g_hwndLogonProviders, CB_SETITEMDATA, nIndex,
         LOGON32_PROVIDER_WINNT40);

    nIndex = ::SendMessage(g_hwndLogonProviders, CB_ADDSTRING, 0,
         (LPARAM) L"Windows NT 3.5");
    ::SendMessage(g_hwndLogonProviders, CB_SETITEMDATA, nIndex,
         LOGON32_PROVIDER_WINNT35);

    nIndex = ::SendMessage(g_hwndLogonProviders, CB_ADDSTRING, 0,
         (LPARAM) L"Default");
    ::SendMessage(g_hwndLogonProviders, CB_SETITEMDATA, nIndex,
         LOGON32_PROVIDER_DEFAULT);
    ::SendMessage(g_hwndLogonProviders, CB_SETCURSEL, nIndex, 0);

    nIndex = ::SendMessage(g_hwndImpersonationLevels, CB_ADDSTRING, 0,
         (LPARAM) L"SecurityAnonymous");
    ::SendMessage(g_hwndImpersonationLevels, CB_SETITEMDATA, nIndex,
         SecurityAnonymous);

    nIndex = ::SendMessage(g_hwndImpersonationLevels, CB_ADDSTRING, 0,
         (LPARAM) L"SecurityIdentification");
    ::SendMessage(g_hwndImpersonationLevels, CB_SETITEMDATA, nIndex,
         SecurityIdentification);

    nIndex = ::SendMessage(g_hwndImpersonationLevels, CB_ADDSTRING, 0,
         (LPARAM) L"SecurityDelegation");
    ::SendMessage(g_hwndImpersonationLevels, CB_SETITEMDATA, nIndex,
         SecurityDelegation);

    nIndex = ::SendMessage(g_hwndImpersonationLevels, CB_ADDSTRING, 0,
         (LPARAM) L"SecurityImpersonation");
    ::SendMessage(g_hwndImpersonationLevels, CB_SETITEMDATA, nIndex,
         SecurityImpersonation);
    ::SendMessage(g_hwndImpersonationLevels, CB_SETCURSEL, nIndex, 0);

    nIndex = ::SendMessage(g_hwndTokenTypes, CB_ADDSTRING, 0,
         (LPARAM) L"Impersonation");
    ::SendMessage(g_hwndTokenTypes, CB_SETITEMDATA, nIndex,
         TokenImpersonation);

    nIndex = ::SendMessage(g_hwndTokenTypes, CB_ADDSTRING, 0,
         (LPARAM) L"Primary");
    ::SendMessage(g_hwndTokenTypes, CB_SETITEMDATA, nIndex, TokenPrimary);
    ::SendMessage(g_hwndTokenTypes, CB_SETCURSEL, nIndex, 0);
}

void CInitView::OnDumpToken() 
{
	// Find the process ID
    LRESULT lIndex = ::SendMessage(g_hwndProcessCombo, CB_GETCURSEL, 0, 0);
    DWORD dwProcessID = ::SendMessage(g_hwndProcessCombo, CB_GETITEMDATA,
          lIndex, 0);

    // Get the thread ID
    lIndex = ::SendMessage(g_hwndThreadCombo, CB_GETCURSEL, 0, 0);
    DWORD dwThreadID = ::SendMessage(g_hwndThreadCombo, CB_GETITEMDATA,
            lIndex, 0);

	//If copy the token 
	BOOL bCopy = ::SendMessage(::GetDlgItem(m_hWnd, IDC_CHECK_COPY_TOKEN), BM_GETSTATE,0,0) == BST_CHECKED;
	// Get the token for the process
	BeginWaitCursor();
    GetToken(dwProcessID, dwThreadID, bCopy);
	EndWaitCursor();
    if (g_hToken != NULL) 
	{
		DWORD dwThreadID;
		//HWND h = m_pW->GetSafeHwnd(); //Local Var can not be passed to thread
		HANDLE hThread = ::CreateThread(NULL, 0, DumpTokenThread, (LPVOID)&(m_pW->m_hWnd),
			0, &dwThreadID);
		if(hThread == NULL)
		{
			::ReportErr(_T("Launching Dump Thread Failure"));
		}
		::CloseHandle(hThread);
		// Success? Display it
        //DumpToken();
    }
	else
	{
		// No?   Then clear out the dialog box
        //SetDlgItemText(hwnd, IDE_TOKEN, TEXT(""));
		::SendMessage(g_hwndToken, WM_SETTEXT, 0, (LPARAM)(LPCTSTR)_T(""));
        ::SendMessage(g_hwndEnablePrivileges, LB_RESETCONTENT, 0, 0);
        ::SendMessage(g_hwndEnableGroups, LB_RESETCONTENT, 0, 0);
        ::SendMessage(g_hwndDeletedPrivileges, LB_RESETCONTENT, 0, 0);
        ::SendMessage(g_hwndDisabledSids, LB_RESETCONTENT, 0, 0);
        ::SendMessage(g_hwndRestrictedSids, LB_RESETCONTENT, 0, 0);
   }
}

void CInitView::OnDuplicateTokenEx() 
{
	DWORD dwStatus = 0;
    PTSTR szStatus = TEXT("Token Duplicated, Dumped Token");

    // Do we have a token?  If not, fail and complain
    if(g_hToken == NULL) 
	{
		PopMsg(_T("No Token.  A starting token is required"));
		szStatus = TEXT("No Token.  A starting token is required");
    }
	else
	{
		HANDLE hOldToken = g_hToken;
        g_hToken = NULL;
  
		int nIndex = ::SendMessage(g_hwndImpersonationLevels, CB_GETCURSEL, 0, 0);
        SECURITY_IMPERSONATION_LEVEL nLevel = (SECURITY_IMPERSONATION_LEVEL)
            ::SendMessage( g_hwndImpersonationLevels, CB_GETITEMDATA, nIndex, 0);

        nIndex = ::SendMessage(g_hwndTokenTypes, CB_GETCURSEL, 0, 0);
        TOKEN_TYPE nType = (TOKEN_TYPE)::SendMessage(g_hwndTokenTypes,
           CB_GETITEMDATA, nIndex, 0);

        // Copy our token
        HANDLE hNewToken;
        if(DuplicateTokenEx(hOldToken, TOKEN_ALL_ACCESS, NULL, nLevel, nType,
            &hNewToken) == TRUE)
		{
			g_hToken = hNewToken;
            DumpToken();
		}
		else
		{
			// Failure?  Clear the dialog box
            dwStatus = GetLastError();
            szStatus = TEXT("DuplicateTokenEx");
            ::SetWindowText(g_hwndToken, TEXT(""));
            ::SendMessage(g_hwndEnablePrivileges, LB_RESETCONTENT, 0, 0);
            ::SendMessage(g_hwndEnableGroups, LB_RESETCONTENT, 0, 0);
            ::SendMessage(g_hwndDeletedPrivileges, LB_RESETCONTENT, 0, 0);
            ::SendMessage(g_hwndDisabledSids, LB_RESETCONTENT, 0, 0);
            ::SendMessage(g_hwndRestrictedSids, LB_RESETCONTENT, 0, 0);
		}

        // No matter what, we kill the last token and take on the new
        CloseHandle(hOldToken);
    }
    // Display the status either way
    Status(szStatus, dwStatus);
}

void CInitView::OnLogonUser() 
{
	// Do we have a token?  If so, kill it.
    if(g_hToken != NULL) 
	{
		CloseHandle(g_hToken);
        g_hToken = NULL;
    }

    // Get the credentials
    TCHAR szName[1024];
    ::GetDlgItemText(m_hWnd, IDE_USERNAME, szName, chDIMOF(szName));

    TCHAR szPassword[1024];
    ::GetDlgItemText(m_hWnd, IDE_PASSWORD, szPassword, chDIMOF(szPassword));

    int nLogonType = ::SendMessage(g_hwndLogonTypes, CB_GETCURSEL, 0, 0);
    nLogonType = ::SendMessage(g_hwndLogonTypes, CB_GETITEMDATA, nLogonType, 0);

    int nLogonProvider = ::SendMessage(g_hwndLogonProviders, CB_GETCURSEL, 0, 0);
    nLogonProvider = ::SendMessage(g_hwndLogonProviders, CB_GETITEMDATA,
         nLogonProvider, 0);

    DWORD dwStatus = 0;
    PTSTR szStatus = TEXT("User Logon Succeeded, Dumped Token");

    // Get a token for the credentials.
    if(LogonUser(szName, NULL, szPassword, nLogonType, nLogonProvider,
         &g_hToken))      
	{
		DWORD dwThreadID;
		//HWND h = m_pW->GetSafeHwnd(); //Local Var can not be passed to thread
		HANDLE hThread = ::CreateThread(NULL, 0, DumpTokenThread, (LPVOID)&(m_pW->m_hWnd),
			0, &dwThreadID);
		if(hThread == NULL)
		{
			::ReportErr(_T("Launching Dump Thread Failure"));
		}
		::CloseHandle(hThread);
		// Success?  Display it
        //DumpToken();
	}
	else
	{
		// Failure?  Clear the dialog box
        dwStatus = GetLastError();
        szStatus = TEXT("LogonUser");
        g_hToken = NULL;
        ::SetDlgItemText(m_hWnd, IDE_TOKEN, TEXT(""));
        ::SendMessage(g_hwndEnablePrivileges, LB_RESETCONTENT, 0, 0);
        ::SendMessage(g_hwndEnableGroups, LB_RESETCONTENT, 0, 0);
        ::SendMessage(g_hwndDeletedPrivileges, LB_RESETCONTENT, 0, 0);
        ::SendMessage(g_hwndDisabledSids, LB_RESETCONTENT, 0, 0);
        ::SendMessage(g_hwndRestrictedSids, LB_RESETCONTENT, 0, 0);
    }
    // Display the status either way
    Status(szStatus, dwStatus);
}

#include "accountListDlg.h"
void CInitView::OnButtonEnumUser() 
{
	CAccountListDlg dlg;
	dlg.DoModal();
	CString str = dlg.m_strRetAccountName;
	if(!str.IsEmpty())
	{
		::SetWindowText(::GetDlgItem(m_hWnd, IDE_USERNAME), str);
	}
}

//Launch an exe with the name of the user on the left editbox
void CInitView::OnButtonLaunchExeWithUser() 
{
	// Get the credentials
    TCHAR szName[1024];
    ::GetDlgItemText(m_hWnd, IDE_USERNAME, szName, chDIMOF(szName));

    TCHAR szPassword[1024];
    ::GetDlgItemText(m_hWnd, IDE_PASSWORD, szPassword, chDIMOF(szPassword));

    int nLogonType = ::SendMessage(g_hwndLogonTypes, CB_GETCURSEL, 0, 0);
    nLogonType = ::SendMessage(g_hwndLogonTypes, CB_GETITEMDATA, nLogonType, 0);

    int nLogonProvider = ::SendMessage(g_hwndLogonProviders, CB_GETCURSEL, 0, 0);
    nLogonProvider = ::SendMessage(g_hwndLogonProviders, CB_GETITEMDATA,
         nLogonProvider, 0);
    
	CString strIniPath = _T("");
	static TCHAR szFilter[] = _T("Exe Files (*.exe)|All Files (*.*)|*.*||");
	CFileDialog* pDlg = new CFileDialog(TRUE, _T("EXE"),
		  strIniPath + _T("\\*.exe"),
		  OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter);
	if(pDlg->DoModal() != IDOK) { delete pDlg; return;}
	CString strPath = pDlg->GetPathName(); delete pDlg;

	EnablePrivilege(SE_TCB_NAME, TRUE);
    //::RunAsUser((LPTSTR)(LPCTSTR)strPath, szName, NULL, szPassword, TEXT("Winsta0\\Default"));		
	HANDLE hToken   = NULL;
    BOOL   fProcess = FALSE;
    BOOL   fSuccess = FALSE;

    PROCESS_INFORMATION pi = {NULL, NULL, 0, 0};
    //Default 
    nLogonType = LOGON32_LOGON_INTERACTIVE;
	nLogonProvider = LOGON32_PROVIDER_DEFAULT;
    try {{
		if (szName == NULL) 
		{
			hToken = GetLSAToken();
            if(hToken == NULL)
				goto leave;
		}
		else
		{
			if (!LogonUser(szName, NULL, szPassword,
               nLogonType, nLogonProvider, &hToken))
			   goto leave;
		}

        STARTUPINFO si;
        si.cb          = sizeof(si);
        si.lpDesktop   = TEXT("Winsta0\\Default");
        si.lpTitle     = NULL;
        si.dwFlags     = 0;
        si.cbReserved2 = 0;
        si.lpReserved  = NULL;
        si.lpReserved2 = NULL;

        fProcess = CreateProcessAsUser(hToken, NULL, 
			(LPTSTR)(LPCTSTR)strPath, NULL, NULL, FALSE,
            0, NULL, NULL, &si, &pi);
        if(!fProcess)
			goto leave;

        fSuccess = TRUE;

    } leave:;
    } catch(...) {}

    if (hToken != NULL)
		CloseHandle(hToken);

    if (fProcess) 
	{
		CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }
	if(!fSuccess)
		::ReportErr(_T("Run Exe As User Failure"));
}

void CInitView::OnBtnRefresh() 
{
	// If we already have one, close it
    if (g_hSnapShot != NULL)
      CloseHandle(g_hSnapShot);

    g_hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS
         | TH32CS_SNAPTHREAD, 0);
	PopulateProcessCombo();
	PopulateThreadCombo();
	PopulateStaticCombos();
}

void CInitView::OnDropdownProcesses() 
{
	CString str;
    CSize   sz;
    int     dx=0;
    CDC*    pDC = this->m_comboProcess.GetDC();
    for (int i=0;i < m_comboProcess.GetCount();i++)
	{
		m_comboProcess.GetLBText( i, str );
        sz = pDC->GetTextExtent(str);

        if (sz.cx > dx)
           dx = sz.cx;
	}
    m_comboProcess.ReleaseDC(pDC);

    // Adjust the width for the vertical scroll bar and the left and right border.
    dx += ::GetSystemMetrics(SM_CXVSCROLL) ;//+ 2*::GetSystemMetrics(SM_CXEDGE);

    // Set the width of the list box so that every item is completely visible.
    m_comboProcess.SetDroppedWidth(dx);	
}
