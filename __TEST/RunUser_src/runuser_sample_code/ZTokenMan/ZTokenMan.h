
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// ZTokenMan.h : ZTOKENMAN アプリケーションのメイン ヘッダー ファイル
//

#if !defined(AFX_ZTOKENMAN_H__EBEF1DC6_2A65_4775_885B_0AD425DC642F__INCLUDED_)
#define AFX_ZTOKENMAN_H__EBEF1DC6_2A65_4775_885B_0AD425DC642F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // メイン シンボル

/////////////////////////////////////////////////////////////////////////////
// CZTokenManApp:
// このクラスの動作の定義に関しては ZTokenMan.cpp ファイルを参照してください。
//

class CZTokenManApp : public CWinApp
{
public:
	CZTokenManApp();

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CZTokenManApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// インプリメンテーション
	//{{AFX_MSG(CZTokenManApp)
	afx_msg void OnAppAbout();
		// メモ - ClassWizard はこの位置にメンバ関数を追加または削除します。
		//        この位置に生成されるコードを編集しないでください。
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CZTokenManApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_ZTOKENMAN_H__EBEF1DC6_2A65_4775_885B_0AD425DC642F__INCLUDED_)
