
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// StatusStatic.cpp : implementation file
//

#include "stdafx.h"
#include "StatusControl.h"
#include "StatusStatic.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#include "ClrButton.h"
/////////////////////////////////////////////////////////////////////////////
// CStatusStatic
COLORREF CStatusStatic::m_color[7] = 
{
	RED, YELLOW, GREEN, CYAN, BLUE, MAGENTA, RED
};

CStatusStatic::CStatusStatic()
{
}

CStatusStatic::~CStatusStatic()
{
}


BEGIN_MESSAGE_MAP(CStatusStatic, CStatic)
	//{{AFX_MSG_MAP(CStatusStatic)
	ON_WM_PAINT()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStatusStatic message handlers

/****************************************************************************
*                              CStatusStatic::Create
* Inputs:
*	CStatusBar * parent: Parent window, the status bar
*	UINT id: Control id, also pane id
*	DWORD style: Style flags
* Result: BOOL
*       TRUE if success
*	FALSE if error
* Effect:
*       Creates a static control. Sets the pane text to the empty string.
****************************************************************************/

BOOL CStatusStatic::Create(CStatusBar * parent, UINT id, DWORD style)
{
     CRect r;

     CStatusControl::setup(parent, id, r);

     BOOL result = CStatic::Create(NULL, style | WS_CHILD, r, parent, id);
     if(!result)
	return FALSE;
     CFont * f = parent->GetFont();
     SetFont(f);
     return TRUE;
}

#pragma comment(lib, "Msimg32")

void CStatusStatic::OnPaint() 
{
	CPaintDC dc(this); // 描画用のデバイス コンテキスト
	CRect rect;
	this->GetClientRect(&rect);
//	TRIVERTEX        vert[7];
//	GRADIENT_RECT    gRect;
//	for(int i = 0; i < 7; i++)
//	{
//		vert[i].x = (int)i*1.0*rect.right/6;
//		vert[i].y = (int)i*1.0*rect.bottom/6;
//		vert[i].Red    = GetRValue(m_color[i]);
//      vert[i].Green  = GetGValue(m_color[i]);
//      vert[i].Blue   = GetBValue(m_color[i]);
//      vert[i].Alpha  = 0x0000;
//	}
//	gRect.UpperLeft  = 0;
//    gRect.LowerRight = 6;
	//need Msimg32.dll
//    GradientFill(dc,vert,6,&gRect,1,GRADIENT_FILL_RECT_H);
/*	TRIVERTEX        vert[2] ;
    GRADIENT_RECT    gRect;
    vert [0] .x      = 0;
    vert [0] .y      = 0;
    vert [0] .Red    = 0x0000;
    vert [0] .Green  = 0x0000;
    vert [0] .Blue   = 0x0000;
    vert [0] .Alpha  = 0x0000;

    vert [1] .x      = rect.right;
    vert [1] .y      = rect.bottom; 
    vert [1] .Red    = 0x0000;
    vert [1] .Green  = 0x0000;
    vert [1] .Blue   = 0xff00;
    vert [1] .Alpha  = 0x0000;

    gRect.UpperLeft  = 0;
    gRect.LowerRight = 1;
	//need Msimg32.dll
    GradientFill(dc,vert,2,&gRect,1,GRADIENT_FILL_RECT_H);
*/
	int dim = sizeof(m_color)/sizeof(m_color[0]);
    //dim = 2;
	for(int i = 0; i < dim - 1; i++)
	{
		TRIVERTEX        vert[2] ;
        GRADIENT_RECT    gRect;
        vert[0].x      = (long)(i*1.0*rect.right/(dim-1));
        vert[0].y       = 0; //(long)(i*1.0*rect.bottom/(dim-1));
        vert[0].Red    = (WORD)(256*GetRValue(m_color[i]));
        vert[0].Green  = (WORD)(256*GetGValue(m_color[i]));
        vert[0].Blue   = (WORD)(256*GetBValue(m_color[i]));
		vert[0].Alpha  = 0x0000;

        vert[1].x      =  (long)((i+1)*1.0*rect.right/(dim-1));
        vert[1].y      =  rect.bottom;//(long)((i+1)*1.0*rect.bottom/(dim-1));
        vert[1].Red    = (WORD)(256*GetRValue(m_color[i+1]));
        vert[1].Green  = (WORD)(256*GetGValue(m_color[i+1]));
        vert[1].Blue   = (WORD)(256*GetBValue(m_color[i+1]));
		vert[1].Alpha  = 0x0000;

        gRect.UpperLeft  = 0;
        gRect.LowerRight = 1;
	    //need Msimg32.dll
        GradientFill(dc,vert,2,&gRect,1,GRADIENT_FILL_RECT_H);
	}
	CString strText;
	this->GetWindowText(strText);
	strText = _T("~o~");
	dc.SetBkMode(TRANSPARENT);
	dc.DrawText(strText, strText.GetLength(), &rect, DT_CENTER);
}

void CStatusStatic::OnTimer(UINT nIDEvent) 
{
	//Rotation of the Rainbow
	int dim = sizeof(m_color)/sizeof(m_color[0]);
	for(int i = dim - 1; i >= 1; i--)
	{
		m_color[i] = m_color[i-1];
	}
	m_color[0] = m_color[dim-1];
	Invalidate();
	CStatic::OnTimer(nIDEvent);
}

void CStatusStatic::Rotate(BOOL bStart)
{
	if(bStart)
	{
		::SetTimer(this->m_hWnd, 101, 50, NULL);
		Invalidate();
	}
	else
	{
		::KillTimer(this->m_hWnd, 101);
		Invalidate();
	}
}