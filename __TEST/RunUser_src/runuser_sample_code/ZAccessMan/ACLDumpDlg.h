#if !defined(AFX_ACLDUMPDLG_H__EF61E85D_B408_4D31_8EE3_044465161286__INCLUDED_)
#define AFX_ACLDUMPDLG_H__EF61E85D_B408_4D31_8EE3_044465161286__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ACLDumpDlg.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CACLDumpDlg ダイアログ
#include "AccessData.h"

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#include "LISTCTRLEX.h"

typedef struct tagListCtrlItemData
{
	DWORD dwAceType;
	DWORD dwAceFlag;
	DWORD dwAceMask;
} ListCtrlItemData;

#define WM_MASK_LIST_CHANGE WM_USER + 123
#include "ZToolTipCtrl.h"
class CACLDumpDlg : public CDialog
{
// コンストラクション
public:
	CACLDumpDlg(CWnd* pParent = NULL);   // 標準のコンストラクタ
    CACLDumpDlg(ObjInf* pObjInf);   // Overload コンストラクタ
// ダイアログ データ
	//{{AFX_DATA(CACLDumpDlg)
	enum { IDD = IDD_ACL_DUMP };
	CComboBox	m_comboNewAceType;
	CCheckListBox	m_listNewAceFlag;
	CCheckListBox	m_listNewAceMask1;
	CCheckListBox   m_listNewAceMask2;
	CBitmapButton	m_btnAceAdd;
	CBitmapButton	m_btnAceDelete;
	CBitmapButton	m_btnAcePeekAccount;
	CBitmapButton	m_btnRefresh;
	CBitmapButton	m_btnMask2Mask;
	CListCtrlEx	m_listSACL;
	CListCtrlEx	m_listDACL;
	CComboBox	m_comboGroupSIDType;
	CComboBox	m_comboOwnerSIDType;
	CString	m_strOwnerName;
	CString	m_strOwnerSID;
	CString	m_strName;
	CString	m_strHandle;
	CString	m_strGroupName;
	CString	m_strGroupSID;
	CString	m_strType;
	CString	m_strNewAceComputerName;
	CString	m_strNewAceUserName;
	CString	m_strNewAceUserSID;
	//}}AFX_DATA
private:
	CZToolTipCtrl m_tt;
private:
	ObjInf* m_pObjInf;
	void Refresh();
	void ZoomAceFromList(CString strComputerName, CString strUserName,
		PSID pUserSID, ListCtrlItemData* pItemData);

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CACLDumpDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CACLDumpDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRadioDacl();
	afx_msg void OnRadioSacl();
	afx_msg void OnItemchangedListSacl(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedListDacl(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnAceDelete();
	afx_msg void OnAceAdd();
	afx_msg void OnChangeGroup();
	afx_msg void OnChangeGroupSid();
	afx_msg void OnChangeOwnerName();
	afx_msg void OnChangeOwnerSid();
	afx_msg void OnNewAceNewOwner();
	afx_msg void OnNewAceNewGroup();
	afx_msg void OnRefresh();
	afx_msg void OnMask2Mask1();
	afx_msg void OnAcePeekAccount();
	//}}AFX_MSG
	afx_msg LRESULT OnMaskListChange(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_ACLDUMPDLG_H__EF61E85D_B408_4D31_8EE3_044465161286__INCLUDED_)
