// ZAccessManDlg.h : ヘッダー ファイル
//

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#if !defined(AFX_ZACCESSMANDLG_H__77027850_9C5C_42C2_843E_91B9FE589349__INCLUDED_)
#define AFX_ZACCESSMANDLG_H__77027850_9C5C_42C2_843E_91B9FE589349__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CZAccessManDlg ダイアログ
#include "AccessData.h"

//#define SCMCTRL_IMPL
#include "SCMCtrl.h"

//#define SERVICECTRL_IMPL
#include "ServiceCtrl.h"

#include "listctrlEx.h"
#include "ztreectrl.h"

typedef struct tagRootReg
{
	LPCTSTR szName;
	HKEY    hKey;
} RootReg;

class CZAccessManDlg : public CDialog
{
// 構築
public:
	CZAccessManDlg(CWnd* pParent = NULL);	// 標準のコンストラクタ

	void UpdateObjDependentCtrls();
	BOOL FillInfo(ObjInf* pInfo) ;

public:
	//To Show Service Preview
	CSCMCtrl m_scm;
	void ShowServicePreview();
	void ShowProcessThreadPreview();
	void ShowWindowStationDesktopPreview();

	static RootReg m_root[7];
	void ShowRegistryPreview();
	void ShowNode(CString strPath);
	void RecursiveShowNode(HTREEITEM hParent, CString strPath);
	CString GetSelectedPath();
	CString DeleteTimePart(CString strPath);
	BOOL FillNode(HTREEITEM hItem);
	BOOL IsFilledNode(HTREEITEM hItem);

// ダイアログ データ
	//{{AFX_DATA(CZAccessManDlg)
	enum { IDD = IDD_ZACCESSMAN_DIALOG };
	CStatic	m_bannar;
	CZTreeCtrl	m_tree;
	CListCtrlEx	m_list;
	//}}AFX_DATA

	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CZAccessManDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV のサポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	HICON m_hIcon;

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CZAccessManDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangeType();
	afx_msg void OnEdit();
	afx_msg void OnButtonOpen();
	afx_msg void OnClickList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemexpandedTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonX();
	afx_msg void OnButtonLog();
	afx_msg void OnChangeUser();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_ZACCESSMANDLG_H__77027850_9C5C_42C2_843E_91B9FE589349__INCLUDED_)
