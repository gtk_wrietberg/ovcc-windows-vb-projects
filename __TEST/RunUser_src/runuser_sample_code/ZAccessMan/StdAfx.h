
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// stdafx.h : 標準のシステム インクルード ファイル、
//            または参照回数が多く、かつあまり変更されない
//            プロジェクト専用のインクルード ファイルを記述します。
//

#if !defined(AFX_STDAFX_H__717B381C_C2BD_4BE9_8759_1CC2B9AC922F__INCLUDED_)
#define AFX_STDAFX_H__717B381C_C2BD_4BE9_8759_1CC2B9AC922F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WINVER  0x0500  
#define _WIN32_WINNT  0x0500 //Usefull!!!!!
#define _WIN32_IE 0x0600


#define VC_EXTRALEAN		// Windows ヘッダーから殆ど使用されないスタッフを除外します。

#include <afxwin.h>         // MFC のコアおよび標準コンポーネント
#include <afxext.h>         // MFC の拡張部分
#include <afxdisp.h>        // MFC のオートメーション クラス
#include <afxdtctl.h>		// MFC の Internet Explorer 4 コモン コントロール サポート
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC の Windows コモン コントロール サポート
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <ACLAPI.h>
#include <ACLUI.h>

// Force linking against the ACLUI library
#pragma comment(lib, "ACLUI.lib")   
#pragma comment(lib, "NetAPI32")
//#define AUTOBUF_IMPL
#include "AutoBuf.h"       // See Appendix B.


//#define PRINTBUF_IMPL
//#include "PrintBuf.h"

//#include "AccessData.h"

// Import library for NTDLL.DLL is available in NT DDK. If you don't have
// the DDK, or can't link with NTDLL implicitly for some reason, you need
// to find the address of ZwQuerySystemInformation at runtime using
// LoadLibrary and GetProcAddress.

//#pragma comment(lib, "ntdll.lib")

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_STDAFX_H__717B381C_C2BD_4BE9_8759_1CC2B9AC922F__INCLUDED_)
