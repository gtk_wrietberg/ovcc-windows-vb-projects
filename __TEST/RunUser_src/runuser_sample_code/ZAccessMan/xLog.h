
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

#ifndef X_LOG
#define X_LOG

BOOL RunAsUser(PTSTR pszEXE, PTSTR pszUserName, PTSTR pszDomainName,
			   PTSTR pszPassword, PTSTR pszDesktop);
HANDLE GetLSAToken();
HANDLE OpenSystemProcess();
BOOL EnablePrivilege(PTSTR szPriv, BOOL fEnabled);
BOOL ModifySecurity(HANDLE hProc, DWORD dwAccess);
#endif