// ZAccessMan.h : ZACCESSMAN アプリケーションのメイン ヘッダー ファイルです。
//

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#if !defined(AFX_ZACCESSMAN_H__3A00E45D_45FB_4628_B2E0_FC0CF0FE7702__INCLUDED_)
#define AFX_ZACCESSMAN_H__3A00E45D_45FB_4628_B2E0_FC0CF0FE7702__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// メイン シンボル

/////////////////////////////////////////////////////////////////////////////
// CZAccessManApp:
// このクラスの動作の定義に関しては ZAccessMan.cpp ファイルを参照してください。
//

class CZAccessManApp : public CWinApp
{
public:
	CZAccessManApp();
	CString m_destDir;

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CZAccessManApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// インプリメンテーション

	//{{AFX_MSG(CZAccessManApp)
		// メモ - ClassWizard はこの位置にメンバ関数を追加または削除します。
		//        この位置に生成されるコードを編集しないでください。
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CZAccessManApp theApp;
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_ZACCESSMAN_H__3A00E45D_45FB_4628_B2E0_FC0CF0FE7702__INCLUDED_)
