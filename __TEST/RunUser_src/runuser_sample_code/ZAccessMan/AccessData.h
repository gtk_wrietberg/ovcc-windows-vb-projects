/******************************************************************************
Module:  AccessData.h
Notices: Copyright (c) 2000 Jeffrey Richter
******************************************************************************/


#include "CmnHdr.h"              /* See Appendix A. */
#include <ACLAPI.h>
#include <ACLUI.h>
#include <lmshare.h>
#include <Winsvc.h> //for service
#include <Winspool.h> //for printer

#ifndef ACCESS_DATA_SEC
#define ACCESS_DATA_SEC
///////////////////////////////////////////////////////////////////////////////


#ifndef UNICODE
#error This module must be compiled natively using Unicode.
#endif


///////////////////////////////////////////////////////////////////////////////

typedef enum AM_SECURETYPE{
   AM_FILE = 0, AM_DIR, AM_SERVICE, 
   AM_PRINTER, AM_REGISTRY, AM_SHARE,   
   AM_PROCESS, AM_THREAD, AM_JOB,     
   AM_SEMAPHORE, AM_EVENT, AM_MUTEX,    
   AM_MAPPING, AM_TIMER, AM_TOKEN,    
   AM_NAMEDPIPE, AM_ANONPIPE, 
   AM_WINDOWSTATION, AM_DESKTOP       
};


typedef struct _ObjEntry {
   AM_SECURETYPE  m_nSpecificType;
   SE_OBJECT_TYPE m_objType;
   PTSTR          m_pszComboText;
   PTSTR          m_pszUsageText;
   BOOL           m_fUseName;
   BOOL           m_fUseHandle;
   BOOL           m_fUsePID;
   BOOL           m_fIsChild;
   BOOL           m_fIsContainer;
} ObjEntry;

typedef struct _ObjInf {
   ObjEntry*   m_pEntry;
   HANDLE      m_hHandle;
   TCHAR       m_szName[1024];
   TCHAR       m_szObjectName[2048];
} ObjInf;


extern ObjEntry g_objMap[19];

///////////////////////////////////////////////////////////////////////////////


class CSecurityInformation: public ISecurityInformation {

public:
   CSecurityInformation(ObjInf* pInfo, BOOL fBinary);
// { 
//     m_pInfo = pInfo; 
//     m_nRef = 1; 
//     m_fBinary = fBinary; 
// }

//Header: Declared in Aclui.h. Win2k & XP Only
//typedef struct _SI_ACCESS {
//  const GUID *pguid;
//  ACCESS_MASK mask;
//  LPCWSTR     pszName;
//  DWORD       dwFlags;
//} SI_ACCESS, *PSI_ACCESS;

//Flag SI_ACCESS_SPECIFIC SI_ACCESS_GENERAL SI_ACCESS_CONTAINER
//     SI_ACCESS_PROPERTY CONTAINER_INHERIT_ACE INHERIT_ONLY_ACE
//     OBJECT_INHERIT_ACE
private:
   static GUID m_guidNULL;
   static SI_ACCESS m_siAccessAllRights[][19];
   static SI_ACCESS m_siAccessBinaryRights[32];
   static SI_INHERIT_TYPE m_siInheritType[];

   ULONG    m_nRef;
   ObjInf*  m_pInfo;

   BOOL     m_fBinary;

public:
   HRESULT WINAPI QueryInterface(REFIID riid, PVOID* ppvObj);
   ULONG WINAPI AddRef();
   ULONG WINAPI Release();

private:
   HRESULT WINAPI GetObjectInformation(PSI_OBJECT_INFO pObjectInfo);
   HRESULT WINAPI GetSecurity(SECURITY_INFORMATION RequestedInformation,
      PSECURITY_DESCRIPTOR* ppSecurityDescriptor, BOOL fDefault);
   HRESULT WINAPI SetSecurity(SECURITY_INFORMATION SecurityInformation,
      PSECURITY_DESCRIPTOR pSecurityDescriptor);
   HRESULT WINAPI GetAccessRights(const GUID* pguidObjectType,
      DWORD dwFlags, // si_edit_audits, si_edit_properties
      PSI_ACCESS* ppAccess, ULONG* pcAccesses, ULONG* piDefaultAccess);
   HRESULT WINAPI MapGeneric(const GUID* pguidObjectType,
      UCHAR* pAceFlags, ACCESS_MASK* pMask);
   HRESULT WINAPI GetInheritTypes(PSI_INHERIT_TYPE* ppInheritTypes, 
         ULONG* pcInheritTypes);
   HRESULT WINAPI PropertySheetPageCallback(HWND hwnd, UINT uMsg, 
         SI_PAGE_TYPE uPage);
};

#endif


///////////////////////////////// End of File /////////////////////////////////