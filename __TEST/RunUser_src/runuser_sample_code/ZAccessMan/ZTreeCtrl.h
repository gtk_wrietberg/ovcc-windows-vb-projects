#if !defined(AFX_JXTREECTRL_H__F120DA48_BDC7_4252_B1FA_2C58FB423982__INCLUDED_)

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

#define AFX_JXTREECTRL_H__F120DA48_BDC7_4252_B1FA_2C58FB423982__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// JXTreeCtrl.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CZTreeCtrl ウィンドウ

class CZTreeCtrl : public CTreeCtrl
{
// コンストラクション
public:
	CZTreeCtrl();
    CImageList m_imageList;
// アトリビュート
public:

// オペレーション
public:

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CZTreeCtrl)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	CString GetFullPath(HTREEITEM hItem, TCHAR chDelimit = _T(','));
	DWORD GetParam(HTREEITEM hItem);
	HTREEITEM IsParamAvailable(DWORD dwParam);
	HTREEITEM RecursiveIsParamAvailable(HTREEITEM hParent,
						DWORD dwParam);
	//when user uncheck an item, uncheck all the child item
    //when user check an item, uncheck all the child item
	void CheckCheck(HTREEITEM hItem);
    void RecursiveCheckCheck(HTREEITEM hItem);
	BOOL RecursiveLoadIniFile(CString szIniFile, CString szSectionName,
              HTREEITEM hParent, int* pLevel, int levelNum,
			  BOOL loadCheckedOnly = FALSE);
	BOOL LoadIniFile(CString strFilename, BOOL loadCheckedOnly = FALSE,
		             CString strSectionName = _T(""));
	BOOL RecursiveSaveIniFile(CString szIniFile, CString szSectionName,
              HTREEITEM hParent, int* pLevel, int levelNum);
	BOOL SaveIniFile(CString strFilename,  CString strSectionName = _T(""));
	BOOL IsChecked(CString strPath, TCHAR chDelimit = _T(','));
	HTREEITEM IsAvailable(CString strPath, TCHAR chDelimit = _T(','));
	HTREEITEM RecursiveIsAvailable(HTREEITEM hParent,CString strPath, TCHAR chDelimit = _T(','));
	BOOL RecursiveIsChecked(HTREEITEM hParent,CString strPath, TCHAR chDelimit = _T(','));
	CWnd* m_pClient;
	void SetAdviseSink(CWnd* pClient);
	BOOL m_bCheckBox;
	BOOL m_bHasButton;
	BOOL m_bHasLine;
	void Clear(BOOL bClearItem = TRUE, BOOL bClearImgList = TRUE);
	void SetStyle(BOOL bHasLine, BOOL bHasButton, BOOL bCheckBox);
	//strPath's example --- root,layer1,layer2 
	HTREEITEM AddItem(CString strPath,
		TCHAR chDelimit = _T(','),  BOOL bCreate = TRUE, 
		UINT imgIndex = 0, UINT focusIndex = (UINT)-1,
		DWORD dwID = (UINT)-1);
	void SetColor(COLORREF colorBack = RGB(255,255,255),
		COLORREF colorText = RGB(0,0,0));
	
	
	BOOL AddImageBitmap(HBITMAP *pBitmap, int nNum, 
		int cx, int cy, COLORREF backColor = RGB(255,255,255));
	BOOL AddImageIcon(HICON* pIcon, UINT nNum, int cx, int cy);
	virtual ~CZTreeCtrl();

	// 生成されたメッセージ マップ関数
protected:
	HTREEITEM RecursiveAddNode(HTREEITEM hParent, 
		CString strPath, TCHAR chDelimit, BOOL bCreate,
		UINT imgIndex, UINT focusIndex,  DWORD dwID = (DWORD)(UINT)-1);
	//{{AFX_MSG(CZTreeCtrl)
	afx_msg void OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_JXTREECTRL_H__F120DA48_BDC7_4252_B1FA_2C58FB423982__INCLUDED_)
