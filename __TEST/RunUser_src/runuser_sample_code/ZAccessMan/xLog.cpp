
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#include "xLog.h"
#include "StdAfx.h"
#include <Tlhelp32.h>

#pragma comment(lib, "Advapi32") //LogonUser
///////////////////////////////////////////////////////////////////
BOOL EnablePrivilege(PTSTR szPriv, BOOL fEnabled) 
{
	HANDLE hToken   = NULL;
    BOOL   fSuccess = FALSE;

    try {{
		// First lookup the system unique luid for the privilege
        LUID luid;
        if(!LookupPrivilegeValue(NULL, szPriv, &luid)) 
		{
			// If the name is bogus...
            goto leave;
		}

        // Then get the processes token
        if(!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES,
            &hToken)) 
		{
			goto leave;
		}

        // Set up our token privileges "array" (in our case an array of one)
        TOKEN_PRIVILEGES tp;
        tp.PrivilegeCount             = 1;
        tp.Privileges[0].Luid         = luid;
        tp.Privileges[0].Attributes = fEnabled ? SE_PRIVILEGE_ENABLED : 0;

        // Adjust our token privileges by enabling or disabling this one
        if(!AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES),
            NULL, NULL)) 
		{
			goto leave;
		}

        fSuccess = TRUE;

    } leave:;
    } catch(...) {}

    // Cleanup
    if (hToken != NULL)
		CloseHandle(hToken);

    return(fSuccess);
}
///////////////////////////////////////////////////////////////////////////////
BOOL ModifySecurity(HANDLE hProc, DWORD dwAccess) 
{
	PACL pAcl        = NULL;
    PACL pNewAcl     = NULL;
    PACL pSacl       = NULL;
    PSID pSidOwner   = NULL;
    PSID pSidPrimary = NULL;
    BOOL fSuccess    = TRUE;

    PSECURITY_DESCRIPTOR pSD = NULL;

    try {{
		// Find the length of the security object for the kernel object
        DWORD dwSDLength;
        if (GetKernelObjectSecurity(hProc, DACL_SECURITY_INFORMATION, pSD, 0,
            &dwSDLength) || (GetLastError() != ERROR_INSUFFICIENT_BUFFER))
			goto leave;

        // Allocate a buffer of that length
        pSD = LocalAlloc(LPTR, dwSDLength);
        if (pSD == NULL)
			goto leave;

        // Retrieve the kernel object
        if (!GetKernelObjectSecurity(hProc, DACL_SECURITY_INFORMATION, pSD,
            dwSDLength, &dwSDLength))
            goto leave;

        // Get a pointer to the DACL of the SD
        BOOL fDaclPresent;
        BOOL fDaclDefaulted;
        if (!GetSecurityDescriptorDacl(pSD, &fDaclPresent, &pAcl,
            &fDaclDefaulted))
            goto leave;

        // Get the current user's name
        TCHAR szName[1024];
        DWORD dwLen = chDIMOF(szName);
        if (!GetUserName(szName, &dwLen))
           goto leave;
 
        // Build an EXPLICIT_ACCESS structure for the ace we wish to add.
        EXPLICIT_ACCESS ea;
        BuildExplicitAccessWithName(&ea, szName, dwAccess, GRANT_ACCESS, 0);
        ea.Trustee.TrusteeType = TRUSTEE_IS_USER;

        // We are allocating a new ACL with a new ace inserted.  The new
        // ACL must be LocalFree'd
        if(ERROR_SUCCESS != SetEntriesInAcl(1, &ea, pAcl, &pNewAcl)) 
		{
			pNewAcl = NULL;
            goto leave;
		}

        // Find the buffer sizes we would need to make our SD absolute
        pAcl               = NULL;
        dwSDLength         = 0;
        DWORD dwAclSize    = 0;
        DWORD dwSaclSize   = 0;
        DWORD dwSidOwnLen  = 0;
        DWORD dwSidPrimLen = 0;
        PSECURITY_DESCRIPTOR pAbsSD = NULL;
        if(MakeAbsoluteSD(pSD, pAbsSD, &dwSDLength, pAcl, &dwAclSize, pSacl,
            &dwSaclSize, pSidOwner, &dwSidOwnLen, pSidPrimary, &dwSidPrimLen)
			|| (GetLastError() != ERROR_INSUFFICIENT_BUFFER))
            goto leave;

        // Allocate the buffers
        pAcl = (PACL) LocalAlloc(LPTR, dwAclSize);
        pSacl = (PACL) LocalAlloc(LPTR, dwSaclSize);
        pSidOwner = (PSID) LocalAlloc(LPTR, dwSidOwnLen);
        pSidPrimary = (PSID) LocalAlloc(LPTR, dwSidPrimLen);
        pAbsSD = (PSECURITY_DESCRIPTOR) LocalAlloc(LPTR, dwSDLength);
        if(!(pAcl && pSacl && pSidOwner && pSidPrimary && pAbsSD))
            goto leave;

        // And actually make our SD absolute
        if(!MakeAbsoluteSD(pSD, pAbsSD, &dwSDLength, pAcl, &dwAclSize, pSacl,
            &dwSaclSize, pSidOwner, &dwSidOwnLen, pSidPrimary, &dwSidPrimLen))
            goto leave;

        // Now set the security descriptor DACL
        if(!SetSecurityDescriptorDacl(pAbsSD, fDaclPresent, pNewAcl,
            fDaclDefaulted))
            goto leave;

        // And set the security for the object
        if(!SetKernelObjectSecurity(hProc, DACL_SECURITY_INFORMATION, pAbsSD))
           goto leave;

        fSuccess = TRUE;

    } leave:;
    } catch(...) {}

    // Cleanup
    if (pNewAcl == NULL)
		LocalFree(pNewAcl);

    if (pSD == NULL)
        LocalFree(pSD);

    if (pAcl == NULL)
        LocalFree(pAcl);

    if (pSacl == NULL)
        LocalFree(pSacl);

    if (pSidOwner == NULL)
        LocalFree(pSidOwner);

    if (pSidPrimary == NULL)
        LocalFree(pSidPrimary);

    return(fSuccess);
}
///////////////////////////////////////////////////////////////////////////////
HANDLE OpenSystemProcess() 
{
	HANDLE hSnapshot = NULL;
    HANDLE hProc     = NULL;

    try {{
		// Get a snapshot of the processes in the system
        hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        if (hSnapshot == NULL)
			goto leave;

        PROCESSENTRY32 pe32;
        pe32.dwSize = sizeof(pe32);

        // Find the "System" process
        BOOL fProcess = Process32First(hSnapshot, &pe32);
        while (fProcess && (lstrcmpi(pe32.szExeFile, TEXT("SYSTEM")) != 0))
			fProcess = Process32Next(hSnapshot, &pe32);
        if (!fProcess)
            goto leave;    // Didn't find "System" process

        // Open the process with PROCESS_QUERY_INFORMATION access
        hProc = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE,
            pe32.th32ProcessID);
        if (hProc == NULL)
			goto leave;

    } leave:;
    } catch(...) {}

    // Cleanup the snapshot
    if (hSnapshot != NULL)
		CloseHandle(hSnapshot);

    return(hProc);
}
///////////////////////////////////////////////////////////////////////////////
HANDLE GetLSAToken() 
{
	HANDLE hProc  = NULL;
    HANDLE hToken = NULL;

    try {{
		// Enable the SE_DEBUG_NAME privilege in our process token
        if (!EnablePrivilege(SE_DEBUG_NAME, TRUE))
         goto leave;

        // Retrieve a handle to the "System" process
        hProc = OpenSystemProcess();
        if(hProc == NULL)
			goto leave;

        // Open the process token with READ_CONTROL and WRITE_DAC access.  We
        // will use this access to modify the security of the token so that we
        // retrieve it again with a more complete set of rights.
        BOOL fResult = OpenProcessToken(hProc, READ_CONTROL | WRITE_DAC,
            &hToken);
        if(FALSE == fResult)  
		{
			hToken = NULL;
            goto leave;
		}

        // Add an ace for the current user for the token.  This ace will add
        // TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY | TOKEN_QUERY rights.
        if (!ModifySecurity(hToken, TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY
            | TOKEN_QUERY)) 
		{
			CloseHandle(hToken);
            hToken = NULL;
            goto leave;
		}

        // Reopen the process token now that we have added the rights to
        // query the token, duplicate it, and assign it.
        fResult = OpenProcessToken(hProc, TOKEN_QUERY | TOKEN_DUPLICATE
            | TOKEN_ASSIGN_PRIMARY, &hToken);
        if (FALSE == fResult)  
		{
			hToken = NULL;
            goto leave;
		}

    } leave:;
    } catch(...) {}

    // Close the System process handle
    if (hProc != NULL)
		CloseHandle(hProc);

    return(hToken);
}
///////////////////////////////////////////////////////////////////////////////
BOOL RunAsUser(PTSTR pszEXE, PTSTR pszUserName, PTSTR pszDomainName,
			   PTSTR pszPassword, PTSTR pszDesktop) 
{
	HANDLE hToken   = NULL;
    BOOL   fProcess = FALSE;
    BOOL   fSuccess = FALSE;

    PROCESS_INFORMATION pi = {NULL, NULL, 0, 0};

    try {{
		if (pszUserName == NULL) 
		{
			hToken = GetLSAToken();
            if(hToken == NULL)
				goto leave;
		}
		else
		{
			if (!LogonUser(pszUserName, pszDomainName, pszPassword,
               LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, &hToken))
			   goto leave;
		}

        STARTUPINFO si;
        si.cb          = sizeof(si);
        si.lpDesktop   = pszDesktop;
        si.lpTitle     = NULL;
        si.dwFlags     = 0;
        si.cbReserved2 = 0;
        si.lpReserved  = NULL;
        si.lpReserved2 = NULL;

        fProcess = CreateProcessAsUser(hToken, NULL, pszEXE, NULL, NULL, FALSE,
            0, NULL, NULL, &si, &pi);
        if(!fProcess)
			goto leave;

        fSuccess = TRUE;

    } leave:;
    } catch(...) {}

    if (hToken != NULL)
		CloseHandle(hToken);

    if (fProcess) 
	{
		CloseHandle(pi.hProcess);
        CloseHandle(pi.hThread);
    }

    return(fSuccess);
}