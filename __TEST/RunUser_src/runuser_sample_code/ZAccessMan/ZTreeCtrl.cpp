
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// JXTreeCtrl.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "ZAccessMan.h"
#include "ZTreeCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
#define MAX_NODE_NAME_LEN  50
/////////////////////////////////////////////////////////////////////////////
// CZTreeCtrl

CZTreeCtrl::CZTreeCtrl()
{
	m_bCheckBox = FALSE;
	m_bHasButton = m_bHasLine = TRUE;
	m_pClient = NULL;
}

CZTreeCtrl::~CZTreeCtrl()
{
}


BEGIN_MESSAGE_MAP(CZTreeCtrl, CTreeCtrl)
	//{{AFX_MSG_MAP(CZTreeCtrl)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelchanged)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZTreeCtrl メッセージ ハンドラ

BOOL CZTreeCtrl::AddImageIcon(HICON* pIcon, UINT nNum, int cx, int cy)
{
	m_imageList.Create(cx, cy, 0, nNum, nNum);
	// 32, 32 for large icons
	
	for (int n = 0; n < (int)nNum; n++) 
	{
		m_imageList.Add(pIcon[n]);
	}
	SetImageList(&m_imageList, TVSIL_NORMAL);
	return TRUE;
}

BOOL CZTreeCtrl::AddImageBitmap(HBITMAP *pBitmap, int nNum, 
								 int cx, int cy, COLORREF backColor)
{
	m_imageList.Create(cx, cy, 0, nNum, nNum);
    for (int n = 0; n < (int)nNum; n++) 
	{
		CBitmap* pBitmap = new CBitmap;
		pBitmap->Attach((HBITMAP)(pBitmap[n]));
		m_imageList.Add(pBitmap, backColor);
		pBitmap->Detach();
		delete pBitmap;
	}
	SetImageList(&m_imageList, TVSIL_NORMAL);
	return TRUE;
}

void CZTreeCtrl::SetColor(COLORREF colorBack, COLORREF colorText)
{
	SetBkColor(colorBack);
	SetTextColor(colorText);
}

HTREEITEM CZTreeCtrl::AddItem(CString strPath, TCHAR chDelimit, BOOL bCreate, 
						  UINT imgIndex, UINT focusIndex, 
						  DWORD dwID)
{
	//disasemble the strPath
	//i.e.  root,layer1,layer2 --> root,  layer1, layer2
	int nMaxArraySize = 10;
    CStringArray arrayPath;
	arrayPath.SetSize(nMaxArraySize);
    int nKeyNum = 0;

	int pos1,pos2;
	pos1 = 0;
	pos2 = strPath.Find(chDelimit);
	if(pos2 == -1) //only one
	{
		CString str = strPath;
		str.TrimLeft(); str.TrimRight();
        arrayPath.SetAt(nKeyNum, str); 
	    nKeyNum++;	 
		if(nKeyNum%nMaxArraySize == 0)
          arrayPath.SetSize(nKeyNum + nMaxArraySize);
	}
	else
	{
		CString str = strPath.Mid(pos1, pos2);
		str.TrimLeft(); str.TrimRight();
        arrayPath.SetAt(nKeyNum, str); 
	    nKeyNum++;	    
        if(nKeyNum%nMaxArraySize == 0)
          arrayPath.SetSize(nKeyNum + nMaxArraySize);
		pos1 = pos2 + 1;
		while(pos1 < strPath.GetLength())
		{
			pos2 = strPath.Find(chDelimit, pos1);
			if(pos2 == -1)
			{
				str = strPath.Mid(pos1);
				str.TrimLeft(); str.TrimRight();
				arrayPath.SetAt(nKeyNum, str); 
	            nKeyNum++;
				if(nKeyNum%nMaxArraySize == 0)
                   arrayPath.SetSize(nKeyNum + nMaxArraySize);
				pos1 = strPath.GetLength();
			}
			else
			{
				str = strPath.Mid(pos1, pos2-pos1);
				str.TrimLeft(); str.TrimRight();
                arrayPath.SetAt(nKeyNum, str); 
	            nKeyNum++;	
				if(nKeyNum%nMaxArraySize == 0)
                    arrayPath.SetSize(nKeyNum + nMaxArraySize);
				pos1 = pos2 + 1;
			}
		}
	}
    
    //add the node now
    HTREEITEM hRoot = GetRootItem( );
	if(hRoot)
	{
		CString strCur;
		int pos = strPath.Find(chDelimit);
		if(pos == -1) 
		{
			strCur = strPath;
			strPath = _T("");
		}
		else 
		{
			strCur = strPath.Left(pos);
			strPath = strPath.Right(strPath.GetLength() - pos - 1);
		}

		while(hRoot)
		{
			CString str = GetItemText(hRoot);
			if(strCur == str) break;
            HTREEITEM hSibling = GetNextItem(hRoot, TVGN_NEXT);
		    hRoot = hSibling;
		}
		
		if(hRoot) //found in this root level 
		{
			//go to the next level
			if(strPath != _T(""))
			{
				HTREEITEM hRet = RecursiveAddNode(hRoot, strPath, 
			      chDelimit, bCreate,
			      imgIndex, focusIndex, dwID);
				return hRet;
			}
			else //the node alread exist
			{
				if(!bCreate) return hRoot;
				else  //create anyway
				{
					TV_INSERTSTRUCT tvinsert;
	                tvinsert.hParent = this->GetParentItem(hRoot);
	                tvinsert.hInsertAfter = TVI_LAST;
	                tvinsert.item.mask = 
	                   TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
                    //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
	                tvinsert.item.hItem = NULL; 
	                tvinsert.item.state = 0;
	                tvinsert.item.stateMask = 0;
	                tvinsert.item.cchTextMax = MAX_NODE_NAME_LEN; //max text length
	                //tvinsert.item.iSelectedImage = ;
	                tvinsert.item.cChildren = 0;
	                tvinsert.item.lParam = (LPARAM)dwID;
	   			
					strCur = GetItemText(hRoot);
	                tvinsert.item.pszText = (LPTSTR)(LPCTSTR)strCur;
	                tvinsert.item.iImage = imgIndex;
			        if(focusIndex == (UINT)-1)
				        tvinsert.item.iSelectedImage = imgIndex;
			        else
 	                    tvinsert.item.iSelectedImage = focusIndex;
			
					tvinsert.item.lParam = (LPARAM)dwID;
		            HTREEITEM hItem = InsertItem(&tvinsert);
					return hItem;
				}
			}
		}
		else //create the node in this level
		{
			//if(!bCreate) return NULL;
			//2002.1.16
			//create it then
			TV_INSERTSTRUCT tvinsert;
	        tvinsert.hParent = NULL;
	        tvinsert.hInsertAfter = TVI_LAST;
	        tvinsert.item.mask = 
	           TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
            //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
	        tvinsert.item.hItem = NULL; 
	        tvinsert.item.state = 0;
	        tvinsert.item.stateMask = 0;
	        tvinsert.item.cchTextMax = MAX_NODE_NAME_LEN; //max text length
	        //tvinsert.item.iSelectedImage = ;
	        tvinsert.item.cChildren = 0;
	        tvinsert.item.lParam = (LPARAM)dwID;
	   			
		    tvinsert.item.pszText = (LPTSTR)(LPCTSTR)strCur;
	        tvinsert.item.iImage = imgIndex;
			if(focusIndex == (UINT)-1)
				tvinsert.item.iSelectedImage = imgIndex;
			else
 	            tvinsert.item.iSelectedImage = focusIndex;
			
	        tvinsert.item.lParam = (LPARAM)dwID;
		    HTREEITEM hItem = InsertItem(&tvinsert);
			
			if(strPath != _T(""))
			{
				HTREEITEM hRet = RecursiveAddNode(hItem, strPath, 
			      chDelimit, bCreate,
			      imgIndex, focusIndex, dwID);
				return hRet;
			}
			else return hItem;
		}
	}
	else //root do not exist at all
	{
		//if(!bCreate) return NULL;
		//2002.1.16
		CString strCur;
		int pos = strPath.Find(chDelimit);
		if(pos == -1) 
		{
			strCur = strPath;
			strPath = _T("");
		}
		else 
		{
			strCur = strPath.Left(pos);
			strPath = strPath.Right(strPath.GetLength() - pos - 1);
		}

		ASSERT(strCur != _T(""));
		//create it then
        TV_INSERTSTRUCT tvinsert;
        tvinsert.hParent = NULL;
        tvinsert.hInsertAfter = TVI_ROOT; //root item
        tvinsert.item.mask = 
           TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
        //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
        tvinsert.item.hItem = NULL; 
        tvinsert.item.state = 0;
        tvinsert.item.stateMask = 0;
        tvinsert.item.cchTextMax = MAX_NODE_NAME_LEN; //max text length
        //tvinsert.item.iSelectedImage = ;
        tvinsert.item.cChildren = 0;
        tvinsert.item.lParam = (LPARAM)dwID;
               
        tvinsert.item.pszText = (LPTSTR)(LPCTSTR)strCur;
        tvinsert.item.iImage = imgIndex;
        if(focusIndex == (UINT)-1)
            tvinsert.item.iSelectedImage = imgIndex;
        else
             tvinsert.item.iSelectedImage = focusIndex;
        tvinsert.item.lParam = (LPARAM)dwID;
        HTREEITEM hItem = InsertItem(&tvinsert);
		
		if(strPath != _T(""))
        {
            HTREEITEM hRet = RecursiveAddNode(hItem, strPath, 
              chDelimit, bCreate,
              imgIndex, focusIndex, dwID);
            return hRet;
        }
		else return hItem;
	}
	return NULL;
}

HTREEITEM CZTreeCtrl::RecursiveAddNode(HTREEITEM hParent, 
			CString strPath, TCHAR chDelimit, BOOL bCreate,
			UINT imgIndex, UINT focusIndex,  DWORD dwID)
{
	ASSERT(strPath != _T(""));
    HTREEITEM hChild = GetChildItem(hParent);
	if(hChild)
	{
		CString strCur;
		int pos = strPath.Find(chDelimit);
		if(pos == -1) 
		{
			strCur = strPath;
			strPath = _T("");
		}
		else 
		{
			strCur = strPath.Left(pos);
			strPath = strPath.Right(strPath.GetLength() - pos - 1);
		}

		while(hChild)
		{
			CString str = GetItemText(hChild);
			if(strCur == str) break;
            HTREEITEM hSibling = GetNextSiblingItem(hChild);
		    hChild = hSibling;
		}
		
		if(hChild) //found in this root level 
		{
			//go to the next level
			if(strPath != _T(""))
			{
				HTREEITEM hRet = RecursiveAddNode(hChild, strPath, 
			      chDelimit, bCreate,
			      imgIndex, focusIndex, dwID);
				return hRet;
			}
			else //the node alread exist
			{
				if(!bCreate) return hChild;
				else //create anyway
				{
					TV_INSERTSTRUCT tvinsert;
	                tvinsert.hParent = this->GetParentItem(hChild);
	                tvinsert.hInsertAfter = TVI_LAST;
	                tvinsert.item.mask = 
	                   TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
                    //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
	                tvinsert.item.hItem = NULL; 
	                tvinsert.item.state = 0;
	                tvinsert.item.stateMask = 0;
	                tvinsert.item.cchTextMax = MAX_NODE_NAME_LEN; //max text length
	                //tvinsert.item.iSelectedImage = ;
	                tvinsert.item.cChildren = 0;
	                tvinsert.item.lParam = (LPARAM)dwID;
	   			
					strCur = GetItemText(hChild);
	                tvinsert.item.pszText = (LPTSTR)(LPCTSTR)strCur;
	                tvinsert.item.iImage = imgIndex;
			        if(focusIndex == (UINT)-1)
				        tvinsert.item.iSelectedImage = imgIndex;
			        else
 	                    tvinsert.item.iSelectedImage = focusIndex;
			
					tvinsert.item.lParam = (LPARAM)dwID;
		            
		            HTREEITEM hItem;
	                hItem = InsertItem(&tvinsert);
					return hItem;
				}
			}
		}
		else //create the node in this level
		{
			//if(!bCreate) return NULL;
			//2002.1.16
			//create it then
			TV_INSERTSTRUCT tvinsert;
	        tvinsert.hParent = hParent;
	        tvinsert.hInsertAfter = TVI_LAST;
	        tvinsert.item.mask = 
	           TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
            //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
	        tvinsert.item.hItem = NULL; 
	        tvinsert.item.state = 0;
	        tvinsert.item.stateMask = 0;
	        tvinsert.item.cchTextMax = MAX_NODE_NAME_LEN; //max text length
	        //tvinsert.item.iSelectedImage = ;
	        tvinsert.item.cChildren = 0;
	        tvinsert.item.lParam = (LPARAM)dwID;
	   			
		    tvinsert.item.pszText = (LPTSTR)(LPCTSTR)strCur;
	        tvinsert.item.iImage = imgIndex;
			if(focusIndex == (UINT)-1)
				tvinsert.item.iSelectedImage = imgIndex;
			else
 	            tvinsert.item.iSelectedImage = focusIndex;
	        tvinsert.item.lParam = (LPARAM)dwID;
		    
		    HTREEITEM hItem;
	        hItem = InsertItem(&tvinsert);
			if(strPath != _T(""))
			{
				HTREEITEM hRet = RecursiveAddNode(hItem, strPath, 
			      chDelimit, bCreate,
			      imgIndex, focusIndex, dwID);
				return hRet;
			}
			else return hItem;
		}
	}
	else //children do not exist 
	{
		//if(!bCreate) return NULL;
		//2002.1.16
		CString strCur;
		int pos = strPath.Find(chDelimit);
		if(pos == -1) 
		{
			strCur = strPath;
			strPath = _T("");
		}
		else 
		{
			strCur = strPath.Left(pos);
			strPath = strPath.Right(strPath.GetLength() - pos - 1);
		}

		ASSERT(strCur != _T(""));
		//create it then
        TV_INSERTSTRUCT tvinsert;
        tvinsert.hParent = hParent;
        tvinsert.hInsertAfter = TVI_LAST; //last item
        tvinsert.item.mask = 
           TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
        //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
        tvinsert.item.hItem = NULL; 
        tvinsert.item.state = 0;
        tvinsert.item.stateMask = 0;
        tvinsert.item.cchTextMax = MAX_NODE_NAME_LEN; //max text length
        //tvinsert.item.iSelectedImage = ;
        tvinsert.item.cChildren = 0;
        tvinsert.item.lParam = (LPARAM)dwID;
               
        tvinsert.item.pszText = (LPTSTR)(LPCTSTR)strCur;
        tvinsert.item.iImage = imgIndex;
        if(focusIndex == (UINT)-1)
            tvinsert.item.iSelectedImage = imgIndex;
        else
             tvinsert.item.iSelectedImage = focusIndex;
        tvinsert.item.lParam = (LPARAM)dwID;
        
        HTREEITEM hItem;
        hItem = InsertItem(&tvinsert);
        if(strPath != _T(""))
        {
            HTREEITEM hRet = RecursiveAddNode(hItem, strPath, 
              chDelimit, bCreate,
              imgIndex, focusIndex, dwID);
            return hRet;
        }
		else return hItem;
	}
	return NULL;
}


void CZTreeCtrl::SetStyle(BOOL bHasLine, BOOL bHasButton, BOOL bCheckBox)
{
	if(!bHasLine)
	    ModifyStyle(TVS_HASLINES, 0);
	else
		ModifyStyle(0, TVS_HASLINES);
    m_bHasLine = bHasLine;
	if(!bHasButton)
	    ModifyStyle(TVS_HASBUTTONS, 0);
	else
		ModifyStyle(0, TVS_HASBUTTONS);
    m_bHasButton = bHasButton;
	if(!bCheckBox)
	    ModifyStyle(TVS_CHECKBOXES , 0);
	else
		ModifyStyle(0, TVS_CHECKBOXES);
	m_bCheckBox = bCheckBox;
}

void CZTreeCtrl::Clear(BOOL bClearItem, BOOL bClearImgList)
{
	if(bClearItem) DeleteAllItems();
    if(bClearImgList) 
	{
		m_imageList.DeleteImageList();
	}
}

BOOL CZTreeCtrl::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	if(m_bHasLine) dwStyle |= TVS_HASLINES;
	else dwStyle &= ~TVS_HASLINES;
	if(m_bHasButton) dwStyle |= TVS_HASBUTTONS;
	else dwStyle &= ~TVS_HASBUTTONS;
	if(m_bCheckBox) dwStyle |= TVS_CHECKBOXES;
	else dwStyle &= ~TVS_CHECKBOXES;

	return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

BOOL CZTreeCtrl::PreCreateWindow(CREATESTRUCT& cs) 
{
	if(m_bHasLine) cs.style |= TVS_HASLINES;
	else cs.style &= ~TVS_HASLINES;
	if(m_bHasButton) cs.style |= TVS_HASBUTTONS;
	else cs.style &= ~TVS_HASBUTTONS;
	if(m_bCheckBox) cs.style |= TVS_CHECKBOXES;
	else cs.style &= ~TVS_CHECKBOXES;

	return CTreeCtrl::PreCreateWindow(cs);
}

void CZTreeCtrl::OnSelchanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	pNMTreeView;
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
	HTREEITEM  hItem = GetSelectedItem();
	if(m_pClient != NULL && hItem) 
	{
		//decide the level the selected item
		int level = 0; //the root level
        HTREEITEM hParent = GetNextItem(hItem,TVGN_PARENT);
		while(hParent != NULL)
		{
			level++;
			hParent = GetNextItem(hParent,TVGN_PARENT);
		}
		//level value decided;
		WPARAM wParam = level;
		TCHAR  **szItemArray = NULL;
		szItemArray = (TCHAR**)GlobalAlloc(GMEM_FIXED, 
                sizeof(TCHAR*) * (level+1));
		//even the level 0 need 1 place to put the item name
		for(int i = 0; i <= level; i++)
           szItemArray[i] = (TCHAR*)GlobalAlloc(GMEM_FIXED, 
                    MAX_NODE_NAME_LEN);
        for(i = level; i >= 0; i--)
		{
			CString strItem = GetItemText(hItem);
			//copy strItem to the item array
            int len = strItem.GetLength();
			::lstrcpyn(szItemArray[i], strItem.GetBuffer(len), len + 1);
			strItem.ReleaseBuffer(len);
			hItem = GetNextItem(hItem,TVGN_PARENT);
		}
		//send message
		m_pClient->PostMessage(WM_USER + 1, wParam, (LPARAM)szItemArray);	
		//free the space
		for(i = 0; i <= level; i++)
			::GlobalFree(szItemArray[i]);
		::GlobalFree(szItemArray);
	}
	*pResult = 0;
}

//set the cleint when selchanged event occur
void CZTreeCtrl::SetAdviseSink(CWnd *pClient)
{
	ASSERT(pClient->GetSafeHwnd() != NULL);
	m_pClient = pClient;
}

HTREEITEM CZTreeCtrl::IsAvailable(CString strPath, TCHAR chDelimit)
{
	HTREEITEM hRoot = GetRootItem( );
	if(hRoot)
	{
		CString strCur;
		int pos = strPath.Find(chDelimit);
		if(pos == -1) 
		{
			strCur = strPath;
			strPath = _T("");
		}
		else 
		{
			strCur = strPath.Left(pos);
			strPath = strPath.Right(strPath.GetLength() - pos - 1);
		}

		while(hRoot)
		{
			CString str = GetItemText(hRoot);
			if(strCur == str) break;
            HTREEITEM hSibling = GetNextItem(hRoot, TVGN_NEXT);
		    hRoot = hSibling;
		}
		
		if(hRoot) //found in this root level 
		{
			//check sub level
			if(!strPath.IsEmpty())
				return RecursiveIsAvailable(hRoot, strPath, chDelimit);
			else
				return hRoot; //found
		}
		else return NULL;
	}
	else return NULL;
}

HTREEITEM CZTreeCtrl::RecursiveIsAvailable(HTREEITEM hParent, CString strPath, TCHAR chDelimit)
{
	ASSERT(strPath != _T(""));
    HTREEITEM hChild = GetChildItem(hParent);
	if(hChild)
	{
		CString strCur;
		int pos = strPath.Find(chDelimit);
		if(pos == -1) 
		{
			strCur = strPath;
			strPath = _T("");
		}
		else 
		{
			strCur = strPath.Left(pos);
			strPath = strPath.Right(strPath.GetLength() - pos - 1);
		}

		while(hChild)
		{
			CString str = GetItemText(hChild);
			if(strCur == str) break;
            HTREEITEM hSibling = GetNextSiblingItem(hChild);
		    hChild = hSibling;
		}
		
		if(hChild) //found in this root level 
		{
			//go to the next level
			if(strPath != _T(""))
			{
				return RecursiveIsAvailable(hChild, strPath, chDelimit);
			}
			else return hChild; //found
		}
		else return NULL;
	}
	else return NULL;
}

BOOL CZTreeCtrl::IsChecked(CString strPath, TCHAR chDelimit)
{
    HTREEITEM hRoot = GetRootItem( );
	if(hRoot)
	{
		CString strCur;
		int pos = strPath.Find(chDelimit);
		if(pos == -1) 
		{
			strCur = strPath;
			strPath = _T("");
		}
		else 
		{
			strCur = strPath.Left(pos);
			strPath = strPath.Right(strPath.GetLength() - pos - 1);
		}

		while(hRoot)
		{
			CString str = GetItemText(hRoot);
			if(strCur == str) break;
            HTREEITEM hSibling = GetNextItem(hRoot, TVGN_NEXT);
		    hRoot = hSibling;
		}
		
		if(hRoot) //found in this root level 
		{
			//check sub level
			if(!strPath.IsEmpty())
				return RecursiveIsChecked(hRoot, strPath, chDelimit);
			else
				return GetCheck(hRoot); //found
		}
		else return FALSE;
	}
	else return FALSE;	
}

BOOL CZTreeCtrl::RecursiveIsChecked(HTREEITEM hParent, CString strPath, TCHAR chDelimit)
{
	ASSERT(strPath != _T(""));
    HTREEITEM hChild = GetChildItem(hParent);
	if(hChild)
	{
		CString strCur;
		int pos = strPath.Find(chDelimit);
		if(pos == -1) 
		{
			strCur = strPath;
			strPath = _T("");
		}
		else 
		{
			strCur = strPath.Left(pos);
			strPath = strPath.Right(strPath.GetLength() - pos - 1);
		}

		while(hChild)
		{
			CString str = GetItemText(hChild);
			if(strCur == str) break;
            HTREEITEM hSibling = GetNextSiblingItem(hChild);
		    hChild = hSibling;
		}
		
		if(hChild) //found in this root level 
		{
			//go to the next level
			if(strPath != _T(""))
			{
				return RecursiveIsChecked(hChild, strPath, chDelimit);
			}
			else return GetCheck(hChild); //found
		}
		else return FALSE;
	}
	else return FALSE;
}


//６月
BOOL CZTreeCtrl::SaveIniFile(CString strFilename,  CString strSectionName)
{
	CString szIniFile;
	if(strFilename.IsEmpty())
	{
		szIniFile = theApp.m_destDir + "\\cfg.ini";
	}
	else
		szIniFile = strFilename;
    //get section name  =  parent window id
	CWnd* pParent = this->GetParent();
	DWORD dwParentID = ::GetWindowLong(pParent->GetSafeHwnd(), GWL_ID);
	CString szSectionName;
	DWORD dwSelfID = ::GetWindowLong(this->GetSafeHwnd(), GWL_ID);
	if(strSectionName.IsEmpty())
		szSectionName.Format(_T("%d-%d"), dwParentID, dwSelfID);
	else 
		szSectionName =  strSectionName;
	//get self Window ID イメージ
  
	//sample of ini file contents
	//menu number = 3
	//menu0=cap,desc,TRUE, imageIndex, focusIndex
	//menu0 number = 2
	//menu0-0=cap,desc,TRUE, imageIndex, focusIndex
	//menu0-1=cap.desc,TRUE, imageIndex, focusIndex

	//menu1=cap,desc,TRUE, imageIndex, focusIndex
	//menu1 number = 0
	HTREEITEM hRoot = GetRootItem( );
	int index = 0;
	CString strKey, str;
	while(hRoot)
	{
		int nImage, nSelectedImage;
		this->GetItemImage(hRoot, nImage, nSelectedImage);

		int nID;
		TVITEM tv;
		tv.mask = TVIF_PARAM | TVIF_IMAGE | TVIF_HANDLE;
		tv.hItem  = hRoot;
		this->GetItem(&tv);
		ASSERT(nImage == tv.iImage);
		nID = (int)tv.lParam;
		BOOL bCheck = this->GetCheck(hRoot);
		CString strName = this->GetItemText(hRoot);
        if(bCheck)
			str.Format(_T("%s, %s, TRUE, %d, %d, %d"), strName, strName,
			                                       nImage, nSelectedImage, nID);
		else
			str.Format(_T("%s, %s, FALSE, %d, %d, %d"), strName, strName,
			                                       nImage, nSelectedImage, nID);
		strKey.Format(_T("menu%d"), index);
		WritePrivateProfileString(
          szSectionName,  // section name
          strKey,  // key name
          str,   // string to add
          szIniFile  // initialization file
        );
        int *pLevel = new int[1];
        pLevel[0] = index;
        RecursiveSaveIniFile(szIniFile, szSectionName,
                             hRoot, pLevel, 1);
		delete pLevel;
		HTREEITEM hSibling = GetNextItem(hRoot, TVGN_NEXT);
		hRoot = hSibling;
		index++;
	}
	
	strKey.Format(_T("menu number"));
    str.Format(_T("%d"), index);
	WritePrivateProfileString(
          szSectionName,  // section name
          strKey,  // key name
          str,   // stri
		  szIniFile  // initialization file
        );
	return TRUE;
}

BOOL CZTreeCtrl::RecursiveSaveIniFile(CString szIniFile,
									   CString szSectionName,
                                       HTREEITEM hParent,
									   int* pLevel, int levelNum)
{
	HTREEITEM hChild = GetChildItem(hParent);
	int index = 0;
	CString strKey, str;
	//make a prefix string 
	CString strPrefix = _T("menu");
	for(int i = 0; i < levelNum; i++)
	{
		CString strTemp;
        strTemp.Format(_T("%d-"), pLevel[i]);
		strPrefix += strTemp;
	}
	strPrefix = strPrefix.Left(strPrefix.GetLength() - 1);
	while(hChild)
	{
		int nImage, nSelectedImage;
		this->GetItemImage(hChild, nImage, nSelectedImage);

		int nID;
		TVITEM tv;
		tv.mask = TVIF_PARAM | TVIF_IMAGE | TVIF_HANDLE;
		tv.hItem  = hChild;
		this->GetItem(&tv);
		ASSERT(nImage == tv.iImage);
		nID = (int)tv.lParam;

		BOOL bCheck = this->GetCheck(hChild);
		CString strName = this->GetItemText(hChild);
        if(bCheck)
			str.Format(_T("%s, %s, TRUE, %d, %d, %d"), strName, strName,
			                                       nImage, nSelectedImage, nID);
		else
			str.Format(_T("%s, %s, FALSE, %d, %d, %d"), strName, strName,
			                                       nImage, nSelectedImage, nID);
		strKey.Format(_T("-%d"), index);
		strKey = strPrefix + strKey;
		WritePrivateProfileString(
          szSectionName,  // section name
          strKey,  // key name
          str,   // string to add
          szIniFile  // initialization file
        );
		int *pLevel2 = new int[levelNum+1];
		for(int i = 0; i < levelNum; i++)
			pLevel2[i] = pLevel[i];
		pLevel2[levelNum] = index;
        RecursiveSaveIniFile(szIniFile, szSectionName,
              hChild, pLevel2, levelNum+1);
		delete pLevel2;
		HTREEITEM hSibling = GetNextItem(hChild, TVGN_NEXT);
		hChild = hSibling;
		index++;
	}
	
	//strKey.Format(_T("%s number"), strPrefix);
	strKey = strPrefix + _T(" number");
    str.Format(_T("%d"), index);
	WritePrivateProfileString(
          szSectionName,  // section name
          strKey,  // key name
          str,   // stri
		  szIniFile  // initialization file
        );
	return TRUE;
}

BOOL CZTreeCtrl::LoadIniFile(CString strFilename, BOOL loadCheckedOnly,
							  CString strSectionName)
{
	CString szIniFile;
	if(strFilename.IsEmpty())
	{
		szIniFile = theApp.m_destDir + "\\cfg.ini";
	}
	else
		szIniFile = strFilename;
    //get section name  =  parent window id
	CWnd* pParent = this->GetParent();
	DWORD dwParentID = ::GetWindowLong(pParent->GetSafeHwnd(), GWL_ID);
	CString szSectionName;
	DWORD dwSelfID = ::GetWindowLong(this->GetSafeHwnd(), GWL_ID);
	if(strSectionName.IsEmpty())
		szSectionName.Format(_T("%d-%d"), dwParentID, dwSelfID);
	else
		szSectionName = strSectionName;
 	//get self Window ID イメージ
    
	//get the first level number
	CString strKey, str;
	strKey.Format(_T("menu number"));
	int nRet = GetPrivateProfileString(
       szSectionName,  // section name
       strKey,  // key name
	   _T(""),  //default value
       str.GetBuffer(20),   // string to add
	   20,
       szIniFile  // initialization file
    );
	str.ReleaseBuffer(nRet);
	str.TrimRight();
	if(str != _T(""))
	{
		int num = _ttoi((LPCTSTR)str);
		for(int i = 0; i < num; i++)
		{
			strKey.Format(_T("menu%d"), i);
			int nRet = GetPrivateProfileString(
              szSectionName,  // section name
              strKey,  // key name
	          _T(""),  //default value
              str.GetBuffer(50),   // string to add
	          50,
              szIniFile  // initialization file
            );
	        str.ReleaseBuffer(nRet);
	        str.TrimRight();
			ASSERT(str != _T(""));
			// str = root, root, TRUE, 0, 0
            CString strName, strBool;
			int nImage, nSelectedImage, nID;
			int pos1 = str.Find(_T(","));
			strName = str.Left(pos1);

			pos1 = str.Find( _T(","),pos1+1);
            int pos2 = str.Find( _T(","),pos1+1);
			strBool = str.Mid(pos1+1, pos2 - pos1 -1);
			strBool.TrimLeft(); strBool.TrimRight();

			pos1 = str.Find( _T(","),pos2+1);
			CString strN = str.Mid(pos2+1, pos1 - pos2 -1);
            nImage = _ttoi((LPCTSTR)strN);

			pos2 = str.Find( _T(","),pos1+1);
            strN = str.Mid(pos1+1, pos2 - pos1 -1);
			nSelectedImage = _ttoi((LPCTSTR)strN);

            strN = str.Mid(pos2+1);
			nID = _ttoi((LPCTSTR)strN);

			BOOL bCheck = strBool == _T("TRUE") ? TRUE : FALSE;
			if(loadCheckedOnly && !bCheck) continue;
			//insert item
			TV_INSERTSTRUCT tvinsert;
            tvinsert.hParent = NULL;
            tvinsert.hInsertAfter = TVI_LAST; //last item
            tvinsert.item.mask = 
               TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
            //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
            tvinsert.item.hItem = NULL; 
            tvinsert.item.state = 0;
            tvinsert.item.stateMask = 0;
            tvinsert.item.cchTextMax = MAX_NODE_NAME_LEN; //max text length
            //tvinsert.item.iSelectedImage = ;
            tvinsert.item.cChildren = 0;
            tvinsert.item.lParam = (LPARAM)nID;
                   
            tvinsert.item.pszText = (LPTSTR)(LPCTSTR)strName;
            tvinsert.item.iImage = nImage;
            tvinsert.item.iSelectedImage = nSelectedImage;
            tvinsert.item.lParam = (LPARAM)nID;
            
            HTREEITEM hItem;
            hItem = InsertItem(&tvinsert);
			SetCheck(hItem, bCheck);
			int *pLevel = new int[1];
			pLevel[0] = i;
			RecursiveLoadIniFile(szIniFile, szSectionName,
               hItem, pLevel, 1, loadCheckedOnly);
			delete pLevel;
		}
	}
 	return TRUE;
}


BOOL CZTreeCtrl::RecursiveLoadIniFile(CString szIniFile,
									   CString szSectionName,
                                       HTREEITEM hParent,
									   int* pLevel, int levelNum,
									    BOOL loadCheckedOnly)
{
	CString strKey, str, strPrefix;
	for(int i = 0; i < levelNum; i++)
	{
		CString strTemp;
        strTemp.Format(_T("%d-"), pLevel[i]);
		strPrefix += strTemp;
	}
	strPrefix = strPrefix.Left(strPrefix.GetLength() - 1);

	strKey.Format(_T("menu%s number"), strPrefix);
	int nRet = GetPrivateProfileString(
       szSectionName,  // section name
       strKey,  // key name
	   _T(""),  //default value
       str.GetBuffer(20),   // string to add
	   20,
       szIniFile  // initialization file
    );
	str.ReleaseBuffer(nRet);
	str.TrimRight();
	if(str != _T("") && str != _T("0")) //there is sub item 
	{
		int num = _ttoi((LPCTSTR)str);
		for(int i = 0; i < num; i++) //menu1-0=child1, child1, TRUE, 0, 0
		{
			strKey.Format(_T("menu%s-%d"), strPrefix, i);
			int nRet = GetPrivateProfileString(
              szSectionName,  // section name
              strKey,  // key name
	          _T(""),  //default value
              str.GetBuffer(50),   // string to add
	          50,
              szIniFile  // initialization file
            );
	        str.ReleaseBuffer(nRet);
	        str.TrimRight();
			ASSERT(str != _T(""));
			// str = root, root, TRUE, 0, 0
            CString strName, strBool;
			int nImage, nSelectedImage, nID;
			int pos1 = str.Find(_T(","));
			strName = str.Left(pos1);

			pos1 = str.Find( _T(","),pos1+1);
            int pos2 = str.Find( _T(","),pos1+1);
			strBool = str.Mid(pos1+1, pos2 - pos1 -1);
			strBool.TrimLeft(); strBool.TrimRight();

			pos1 = str.Find( _T(","),pos2+1);
			CString strN = str.Mid(pos2+1, pos1 - pos2 -1);
            nImage = _ttoi((LPCTSTR)strN);

            pos2 = str.Find( _T(","),pos1+1);
            strN = str.Mid(pos1+1, pos2 - pos1 -1);
			nSelectedImage = _ttoi((LPCTSTR)strN);

            strN = str.Mid(pos2+1);
			nID = _ttoi((LPCTSTR)strN);

			BOOL bCheck = strBool == _T("TRUE") ? TRUE : FALSE;
			if(loadCheckedOnly && !bCheck) continue;
			//insert item
			TV_INSERTSTRUCT tvinsert;
            tvinsert.hParent = hParent;
            tvinsert.hInsertAfter = TVI_LAST; //last item
            tvinsert.item.mask = 
               TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
            //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
            tvinsert.item.hItem = NULL; 
            tvinsert.item.state = 0;
            tvinsert.item.stateMask = 0;
            tvinsert.item.cchTextMax = MAX_NODE_NAME_LEN; //max text length
            //tvinsert.item.iSelectedImage = ;
            tvinsert.item.cChildren = 0;
            tvinsert.item.lParam = (LPARAM)nID;
                   
            tvinsert.item.pszText = (LPTSTR)(LPCTSTR)strName;
            tvinsert.item.iImage = nImage;
            tvinsert.item.iSelectedImage = nSelectedImage;
            tvinsert.item.lParam = (LPARAM)nID;
            
            HTREEITEM hItem;
            hItem = InsertItem(&tvinsert);
			SetCheck(hItem, bCheck);
			int *pLevel2 = new int[levelNum+1];
			for(int j = 0; j < levelNum; j++)
				pLevel2[j] = pLevel[j];
			pLevel2[levelNum] = i;
			RecursiveLoadIniFile(szIniFile, szSectionName,
                hItem, pLevel2, levelNum+1,loadCheckedOnly);
			delete pLevel2;
		}
	}
	return TRUE;
}

//when user uncheck an item, uncheck all the child item
//when user check an item, uncheck all the child item
void CZTreeCtrl::CheckCheck(HTREEITEM hItem)
{
    HTREEITEM hChild = GetChildItem(hItem);
	while(hChild)
	{
        SetCheck(hChild, FALSE);
        RecursiveCheckCheck(hChild);
		HTREEITEM hSibling = GetNextSiblingItem(hChild);
        hChild = hSibling;
	}
}

void CZTreeCtrl::RecursiveCheckCheck(HTREEITEM hItem)
{
	HTREEITEM hChild = GetChildItem(hItem);
	while(hChild)
	{
        SetCheck(hChild, FALSE);
        RecursiveCheckCheck(hChild);
		HTREEITEM hSibling = GetNextSiblingItem(hChild);
        hChild = hSibling;
	}
}

HTREEITEM CZTreeCtrl::IsParamAvailable(DWORD dwParam)
{
	HTREEITEM hRoot = GetRootItem( );
	if(hRoot)
	{
		while(hRoot)
		{
            DWORD dw;
		    TVITEM tv;
		    tv.mask = TVIF_PARAM | TVIF_HANDLE;
		    tv.hItem  = hRoot;
		    GetItem(&tv);
		    dw = (DWORD)tv.lParam;
			if(dw == dwParam) return hRoot;
			HTREEITEM h;
			if((h = RecursiveIsParamAvailable(hRoot, dwParam)) != NULL)
				return h;
            HTREEITEM hSibling = GetNextItem(hRoot, TVGN_NEXT);
		    hRoot = hSibling;
		}
		return NULL;
	}
	else return NULL;
}

HTREEITEM CZTreeCtrl::RecursiveIsParamAvailable(HTREEITEM hParent,
						DWORD dwParam)
{
	HTREEITEM hChild = GetChildItem(hParent);
	if(hChild)
	{
		while(hChild)
		{
			DWORD dw;
		    TVITEM tv;
		    tv.mask = TVIF_PARAM | TVIF_HANDLE;
		    tv.hItem  = hChild;
		    GetItem(&tv);
		    dw = (DWORD)tv.lParam;
			if(dw == dwParam) return hChild;
			HTREEITEM h;
			if((h = RecursiveIsParamAvailable(hChild, dwParam)) != NULL)
				return h;
            HTREEITEM hSibling = GetNextSiblingItem(hChild);
		    hChild = hSibling;
		}
		return NULL;
	}
	else return NULL;
}

DWORD CZTreeCtrl::GetParam(HTREEITEM hItem)
{
	DWORD dw;
	TVITEM tv;
	tv.mask = TVIF_PARAM | TVIF_HANDLE;
	tv.hItem  = hItem;
	GetItem(&tv);
	dw = (DWORD)tv.lParam;
    return dw;
}

CString CZTreeCtrl::GetFullPath(HTREEITEM hItem, TCHAR chDelimit)
{
    CString strRet = _T("");
    CString strDelimit = chDelimit;
	while(hItem)
	{
		CString strName = GetItemText(hItem);
		strRet = strName + strDelimit + strRet;
		hItem = GetParentItem(hItem);
	}
    int len = strDelimit.GetLength();
	strRet = strRet.Left(strRet.GetLength() - len);
	return strRet;
}
