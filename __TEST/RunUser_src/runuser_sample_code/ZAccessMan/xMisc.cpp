
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#include "StdAfx.h"
#include "XMisc.h"
#include <malloc.h> //for _alloc


void DumpACL( PACL pACL ){
   __try{
      if (pACL == NULL){
         _tprintf(TEXT("NULL DACL\n"));
         __leave;
      }

      ACL_SIZE_INFORMATION aclSize = {0};
      if (!GetAclInformation(pACL, &aclSize, sizeof(aclSize),
         AclSizeInformation))
         __leave;
      _tprintf(TEXT("ACL ACE count: %d\n"), aclSize.AceCount);
      
      struct{
         BYTE  lACEType;
         PTSTR pszTypeName;
      }aceTypes[6] = {
         {ACCESS_ALLOWED_ACE_TYPE, TEXT("ACCESS_ALLOWED_ACE_TYPE")},
         {ACCESS_DENIED_ACE_TYPE, TEXT("ACCESS_DENIED_ACE_TYPE")},
         {SYSTEM_AUDIT_ACE_TYPE, TEXT("SYSTEM_AUDIT_ACE_TYPE")},
         {ACCESS_ALLOWED_OBJECT_ACE_TYPE,
            TEXT("ACCESS_ALLOWED_OBJECT_ACE_TYPE")},
         {ACCESS_DENIED_OBJECT_ACE_TYPE,
            TEXT("ACCESS_DENIED_OBJECT_ACE_TYPE")},
         {SYSTEM_AUDIT_OBJECT_ACE_TYPE,
            TEXT("SYSTEM_AUDIT_OBJECT_ACE_TYPE")}};

      struct{
         ULONG lACEFlag;
         PTSTR pszFlagName;
      }aceFlags[7] = {
         {INHERITED_ACE, TEXT("INHERITED_ACE")},
         {CONTAINER_INHERIT_ACE, TEXT("CONTAINER_INHERIT_ACE")},
         {OBJECT_INHERIT_ACE, TEXT("OBJECT_INHERIT_ACE")},
         {INHERIT_ONLY_ACE, TEXT("INHERIT_ONLY_ACE")},
         {NO_PROPAGATE_INHERIT_ACE, TEXT("NO_PROPAGATE_INHERIT_ACE")},
         {FAILED_ACCESS_ACE_FLAG, TEXT("FAILED_ACCESS_ACE_FLAG")},
         {SUCCESSFUL_ACCESS_ACE_FLAG, 
            TEXT("SUCCESSFUL_ACCESS_ACE_FLAG")}};

      for (ULONG lIndex = 0;lIndex < aclSize.AceCount;lIndex++){
         ACCESS_ALLOWED_ACE* pACE;
         if (!GetAce(pACL, lIndex, (PVOID*)&pACE))
            __leave;

         _tprintf(TEXT("\nACE #%d\n"), lIndex);
         
         ULONG lIndex2 = 6;
         PTSTR pszString = TEXT("Unknown ACE Type");
         while (lIndex2--){
            if(pACE->Header.AceType == aceTypes[lIndex2].lACEType){
               pszString = aceTypes[lIndex2].pszTypeName;
            }
         }
         _tprintf(TEXT("  ACE Type =\n  \t%s\n"), pszString);

         _tprintf(TEXT("  ACE Flags = \n"));
         lIndex2 = 7;
         while (lIndex2--){
            if ((pACE->Header.AceFlags & aceFlags[lIndex2].lACEFlag) 
               != 0)
               _tprintf(TEXT("  \t%s\n"), 
                  aceFlags[lIndex2].pszFlagName);
         }

         _tprintf(TEXT("  ACE Mask (32->0) =\n  \t"));
         lIndex2 = (ULONG)1<<31;
         while (lIndex2){
            _tprintf(((pACE->Mask & lIndex2) != 0)?TEXT("1"):TEXT("0"));
            lIndex2>>=1;
         }

         TCHAR szName[1024];
         TCHAR szDom[1024];
         PSID pSID = PSIDFromPACE(pACE);
         SID_NAME_USE sidUse;         
         ULONG lLen1 = 1024, lLen2 = 1024;
         if (!LookupAccountSid(NULL, pSID, 
            szName, &lLen1, szDom, &lLen2, &sidUse))
            lstrcpy(szName, TEXT("Unknown"));
         PTSTR pszSID;
         if (!ConvertSidToStringSid(pSID, &pszSID))
            __leave;
         _tprintf(TEXT("\n  ACE SID =\n  \t%s (%s)\n"), pszSID, szName);
         LocalFree(pszSID);
      }
   }__finally{}
}

ULONG CalculateACLSize( PACL pACLOld, PSID* ppSidArray, int nNumSids, 
   PACE_UNION* ppACEs, int nNumACEs ){
   ULONG lACLSize = 0;
   
   try{
      // If we are including an existing ACL, then find its size
      if (pACLOld != NULL){
         ACL_SIZE_INFORMATION aclSize;
         if(!GetAclInformation(pACLOld, &aclSize, sizeof(aclSize), 
            AclSizeInformation)){            
            goto leave;
         }
         lACLSize = aclSize.AclBytesInUse;
      }

      if (ppSidArray != NULL){
         // Step through each SID
         while (nNumSids--){
            // If a SID isn't valid, then we bail
            if (!IsValidSid(ppSidArray[nNumSids])){
               lACLSize = 0;
               goto leave;
            }
            // Get the SID's length
            lACLSize += GetLengthSid(ppSidArray[nNumSids]);
            // Add the ACE structure size, minus the 
            // size of the SidStart member
            lACLSize += sizeof(ACCESS_ALLOWED_ACE) - 
               sizeof(((ACCESS_ALLOWED_ACE*)0)->SidStart);         
         }      
      }

      if (ppACEs != NULL){
         // Step through each ACE
         while (nNumACEs--){            
            // Get the SIDs length
            lACLSize += ppACEs[nNumACEs]->aceHeader.AceSize;
         }      
      }         
      // Add in the ACL structure itself
      lACLSize += sizeof(ACL);

   leave:;
   }catch(...){
      // An exception means we fail the function
      lACLSize = 0;      
   }
   return (lACLSize);
}

PACE_UNION AllocateACE( ULONG bACEType, ULONG bACEFlags, 
   ULONG lAccessMask, PSID pSID ){
   PACE_UNION pReturnACE = NULL;
   PBYTE pbBuffer = NULL;
   try{
      // Get the offset of the SID in the ACE
      ULONG lSIDOffset = (ULONG)(&((ACCESS_ALLOWED_ACE*)0)->SidStart);
      // Get the size of the ACE without the SID
      ULONG lACEStructSize = sizeof(ACCESS_ALLOWED_ACE) - 
         sizeof(((ACCESS_ALLOWED_ACE*)0)->SidStart);
      // Get the length of the SID
      ULONG lSIDSize = GetLengthSid(pSID);      

      // Allocate a buffer for the ACE
      pbBuffer = (PBYTE)LocalAlloc(LPTR, lACEStructSize + lSIDSize);
      if (pbBuffer == NULL)
         goto leave;

      // Copy the SID into the ACE
      if(!CopySid(lSIDSize, (PSID)(pbBuffer+lSIDOffset), pSID)){
         goto leave;
      }      
      pReturnACE = (PACE_UNION) pbBuffer;
      pReturnACE->aceHeader.AceSize = (USHORT)(lACEStructSize + lSIDSize);
      pReturnACE->aceHeader.AceType = (BYTE)bACEType;
      pReturnACE->aceHeader.AceFlags = (BYTE)bACEFlags;
      pReturnACE->aceAllowed.Mask = lAccessMask;
   leave:;
   }catch(...){}
   // Free the buffer in an error case
   if (pbBuffer != (PBYTE)pReturnACE){
      LocalFree(pbBuffer);
   }
   return (pReturnACE);    
}

BOOL IsEqualACE( PACE_UNION pACE1, PACE_UNION pACE2 ){
   BOOL fReturn = FALSE;
   
   try{{
      if(pACE1->aceHeader.AceType != pACE2->aceHeader.AceType)
         goto leave;

      // Get the offset of the SID in the ACE
      ULONG lSIDOffset = (ULONG)(&((ACCESS_ALLOWED_ACE*)0)->SidStart);
      // Get the size of the ACE without the SID
      ULONG lACEStructSize = sizeof(ACCESS_ALLOWED_ACE) - 
         sizeof(((ACCESS_ALLOWED_ACE*)0)->SidStart);

      PBYTE pbACE1 = (PBYTE)pACE1;
      PBYTE pbACE2 = (PBYTE)pACE2;
      fReturn = TRUE;
      while(lACEStructSize--)
         fReturn = (fReturn && ((pbACE1[lACEStructSize] ==
            pbACE2[lACEStructSize])));
      // Check SIDs
      fReturn = fReturn && EqualSid((PSID)(pbACE1+lSIDOffset),
         (PSID)(pbACE2+lSIDOffset));      
   }leave:;
   }catch(...){
   }

   return (fReturn);
}

int FindACEInACL( PACL pACL, PACE_UNION pACE ){
   int nACEIndex = -1;

   try{{
      ACL_SIZE_INFORMATION aclSize;
      if (!GetAclInformation(pACL, &aclSize, sizeof(aclSize), 
         AclSizeInformation)){
         goto leave;
      }

      while (aclSize.AceCount--){
         PACE_UNION pACETemp;
         if(!GetAce(pACL, aclSize.AceCount, (PVOID *)&pACETemp))
            goto leave;

         if(IsEqualACE(pACETemp, pACE)){
            nACEIndex = (int)aclSize.AceCount;
            break;
         }
      }
   }leave:;
   }catch(...){
   }
   return (nACEIndex);
}

BOOL CopyACL( PACL pACLDestination, PACL pACLSource ){
   BOOL fReturn = FALSE;
   try{{
      // Get the number of ACEs in the source ACL
      ACL_SIZE_INFORMATION aclSize;
      if (!GetAclInformation(pACLSource, &aclSize, 
         sizeof(aclSize), AclSizeInformation)){
         goto leave;
      }

      // Use GetAce and AddAce to copy the ACEs
      for(ULONG lIndex=0;lIndex < aclSize.AceCount;lIndex++){
         ACE_HEADER* pACE;
         if(!GetAce(pACLSource, lIndex, (PVOID*)&pACE))
		 {
			 //ReportErr(_T("GetAce in CopyACL"));
             goto leave;
		 }
         if(!AddAce(pACLDestination, ACL_REVISION, MAXDWORD, 
            (PVOID*)pACE, pACE->AceSize))
		 {
			 //ReportErr(_T("AddAce in CopyACL"));
			 goto leave;
		 }
      }      
      fReturn = TRUE;
   }leave:;
   }catch(...){
   }
   return (fReturn);
}

ULONG GetACEInsertionIndex( PACL pDACL, PACE_UNION pACENew){
   ULONG lIndex = (ULONG) -1;

   try{{
      // ACE types by ACL order
      ULONG lFilterType[] = { ACCESS_DENIED_ACE_TYPE, 
                              ACCESS_DENIED_OBJECT_ACE_TYPE, 
                              ACCESS_ALLOWED_ACE_TYPE, 
                              ACCESS_ALLOWED_OBJECT_ACE_TYPE};

      // Determine which group the new ACE should belong to
      ULONG lNewAceGroup;
      for(lNewAceGroup = 0; lNewAceGroup<4 ; lNewAceGroup++){
         if(pACENew->aceHeader.AceType == lFilterType[lNewAceGroup])
            break;
      }
      // If group == 4, the ACE type is no good
      if(lNewAceGroup==4)
         goto leave;
      // If new ACE is an inherited ACE, then it goes after other ACEs
      if((pACENew->aceHeader.AceFlags & INHERITED_ACE) != 0)
         lNewAceGroup+=4;

      // Get ACE count
      ACL_SIZE_INFORMATION aclSize;
      if (!GetAclInformation(pDACL, &aclSize, 
         sizeof(aclSize), AclSizeInformation)){
         goto leave;
      }      

      // Iterate through ACEs
      lIndex = 0;
      for(lIndex = 0;lIndex < aclSize.AceCount;lIndex++){
         ACE_HEADER* pACE;
         if(!GetAce(pDACL, lIndex, (PVOID*)&pACE))
            goto leave;

         // Get the group of the ACL ACE
         ULONG lAceGroup;
         for(lAceGroup = 0; lAceGroup<4 ; lAceGroup++){
            if(pACE->AceType == lFilterType[lAceGroup])
               break;
         }
         // Test for bad ACE
         if(lAceGroup==4){
            lIndex = (ULONG) -1;
            goto leave;
         }
         // Inherited adjustment
         if((pACE->AceFlags & INHERITED_ACE) != 0)
            lAceGroup+=4;

         // If this is the same group, then insertion point found
         if(lAceGroup>=lNewAceGroup)
            break;
      }       

   }leave:;
   }catch(...){
   }   
   return (lIndex);
}

BOOL OrderDACL( PACL pDACL ){
   BOOL fReturn = FALSE;

   try{{
      // Get ACL size and ACE count
      ACL_SIZE_INFORMATION aclSize;
      if (!GetAclInformation(pDACL, &aclSize, 
         sizeof(aclSize), AclSizeInformation)){
         goto leave;
      }
      // Get memory for temporary ACL
      PACL pTempDACL = (PACL) _alloca(aclSize.AclBytesInUse);
      if (pTempDACL==NULL)
         goto leave;
      // Initialize temporary ACL
      if (!InitializeAcl(pTempDACL, aclSize.AclBytesInUse, 
         ACL_REVISION))
         goto leave;
      
      // Iterate through ACEs
      for (ULONG lAceIndex = 0; 
         lAceIndex < aclSize.AceCount ; lAceIndex++){
         // Get ACE
         PACE_UNION pACE;
         if (!GetAce(pDACL, lAceIndex, (PVOID*)&pACE))
            goto leave;
         // Find location, and add ACE to temp DACL
         ULONG lWhere = GetACEInsertionIndex(pTempDACL, pACE);
         if (!AddAce(pTempDACL, ACL_REVISION, 
            lWhere, pACE, ((ACE_HEADER*)pACE)->AceSize))
            goto leave;
      }
      // Copy temp DACL to original
      CopyMemory(pDACL, pTempDACL, aclSize.AclBytesInUse);
      fReturn = TRUE;
   }
   leave:;
   }catch(...){
   }
   return (fReturn);
}

BOOL AllowAccessToWinSta( PSID psidTrustee, HWINSTA hWinSta ){
   BOOL fReturn = FALSE;     
   PSECURITY_DESCRIPTOR psdWinSta = NULL;
   PACE_UNION pACENew = NULL;

   try{{    
      // Get the DACL for the window station
      PACL pDACLWinSta;
      if(GetSecurityInfo(hWinSta, SE_WINDOW_OBJECT, 
         DACL_SECURITY_INFORMATION, NULL, NULL, &pDACLWinSta, 
         NULL, &psdWinSta) != ERROR_SUCCESS)
         goto leave;
      
      // Allocate our new ACE
      // This is the access awarded to a user who logged on interactively
      PACE_UNION pACENew = AllocateACE(ACCESS_ALLOWED_ACE_TYPE, 0, 
         DELETE|WRITE_OWNER|WRITE_DAC|READ_CONTROL|
         WINSTA_ENUMDESKTOPS|WINSTA_READATTRIBUTES|
         WINSTA_ACCESSCLIPBOARD|WINSTA_CREATEDESKTOP|
         WINSTA_WRITEATTRIBUTES|WINSTA_ACCESSGLOBALATOMS|
         WINSTA_EXITWINDOWS|WINSTA_ENUMERATE|WINSTA_READSCREEN, 
         psidTrustee);      
      
      // Is the ACE already in the DACL?
      if (FindACEInACL(pDACLWinSta, pACENew) == -1){
         // If not, calculate new DACL size
         ULONG lNewACL = CalculateACLSize( pDACLWinSta, NULL, 0, 
            &pACENew, 1 );

         // Allocate memory for the new DACL
         PACL pNewDACL = (PACL)_alloca(lNewACL);
         if (pNewDACL == NULL)
            goto leave;

         // Initialize the ACL
         if (!InitializeAcl(pNewDACL, lNewACL, ACL_REVISION))
            goto leave;

         // Copy the ACL
         if (!CopyACL(pNewDACL, pDACLWinSta))
            goto leave;

         // Get location for new ACE
         ULONG lIndex = GetACEInsertionIndex(pNewDACL, pACENew);

         // Add the new ACE
         if (!AddAce(pNewDACL, ACL_REVISION, lIndex, 
            pACENew, pACENew->aceHeader.AceSize))
            goto leave;

         // Set the DACL back to the window station
         if (SetSecurityInfo(hWinSta, SE_WINDOW_OBJECT, 
            DACL_SECURITY_INFORMATION, NULL, NULL, 
            pNewDACL, NULL)!=ERROR_SUCCESS)
            goto leave;
      }
      fReturn = TRUE;        
   }leave:;
   }catch(...){
   }
   // Clean up
   if(pACENew != NULL)
      LocalFree(pACENew);
   if(psdWinSta != NULL)
      LocalFree(psdWinSta);   
   return (fReturn);   
}

BOOL AllowAccessToDesktop( PSID psidTrustee, HDESK hDesk ){
   BOOL fReturn = FALSE;     
   PSECURITY_DESCRIPTOR psdDesk = NULL;
   PACE_UNION pACENew = NULL;

   try{{    
      // Get the DACL for the desktop
      PACL pDACLDesk;
      if(GetSecurityInfo(hDesk, SE_WINDOW_OBJECT, 
         DACL_SECURITY_INFORMATION, NULL, NULL, &pDACLDesk, 
         NULL, &psdDesk) != ERROR_SUCCESS)
         goto leave;
      
      // Allocate our new ACE
      // This is the access awarded to a user who logged on interactively
      PACE_UNION pACENew = AllocateACE(ACCESS_ALLOWED_ACE_TYPE, 0, 
         DELETE|WRITE_OWNER|WRITE_DAC|READ_CONTROL|
         DESKTOP_READOBJECTS|DESKTOP_CREATEWINDOW|
         DESKTOP_CREATEMENU|DESKTOP_HOOKCONTROL|
         DESKTOP_JOURNALRECORD|DESKTOP_JOURNALPLAYBACK|
         DESKTOP_ENUMERATE|DESKTOP_WRITEOBJECTS|DESKTOP_SWITCHDESKTOP,
         psidTrustee);      
      
      // Is the ACE already in the DACL?
      if (FindACEInACL(pDACLDesk, pACENew) == -1){
         // If not, calculate new DACL size
         ULONG lNewACL = CalculateACLSize( pDACLDesk, NULL, 0, 
            &pACENew, 1 );

         // Allocate memory for the new DACL
         PACL pNewDACL = (PACL)_alloca(lNewACL);
         if (pNewDACL == NULL)
            goto leave;

         // Initialize the ACL
         if (!InitializeAcl(pNewDACL, lNewACL, ACL_REVISION))
            goto leave;

         // Copy the ACL
         if (!CopyACL(pNewDACL, pDACLDesk))
            goto leave;

         // Get location for new ACE
         ULONG lIndex = GetACEInsertionIndex(pNewDACL, pACENew);

         // Add the new ACE
         if (!AddAce(pNewDACL, ACL_REVISION, lIndex, 
            pACENew, pACENew->aceHeader.AceSize))
            goto leave;

         // Set the DACL back to the window station
         if (SetSecurityInfo(hDesk, SE_WINDOW_OBJECT, 
            DACL_SECURITY_INFORMATION, NULL, NULL, 
            pNewDACL, NULL)!=ERROR_SUCCESS)
            goto leave;
      }
      fReturn = TRUE;        
   }leave:;
   }catch(...){
   }
   // Clean up
   if(pACENew != NULL)
      LocalFree(pACENew);
   if(psdDesk != NULL)
      LocalFree(psdDesk);   
   return (fReturn);   
}

//Sample
//PSID psidEveryone; 
//
// Create a SID for the built-in "Everyone" group
//SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_WORLD_SID_AUTHORITY;
//if (!AllocateAndInitializeSid( &sidAuth, 1, SECURITY_WORLD_RID, 
//   0, 0, 0, 0, 0, 0, 0, &psidEveryone )){
//   // Error
//}
//
//HWINSTA hWinSta = GetProcessWindowStation();
//if (hWinSta == NULL){
//   // Error
//}
//AllowAccessToWinSta(psidEveryone, hWinSta);
//
//HDESK hDesk = GetThreadDesktop(GetCurrentThreadId());
//if (hDesk == NULL){
//   // Error
//}
//AllowAccessToDesktop(psidEveryone, hDesk);






