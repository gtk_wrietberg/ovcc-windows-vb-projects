// ZAccessManDlg.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "ZAccessMan.h"
#include "ZAccessManDlg.h"
#include "HyperLink.h"
#include "Z.h"
#include "AccessData.h"
#include "AclDumpDlg.h"
#include <windows.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MAX_KEY_LEN 1024

BOOL HasSubKey(HKEY hKey)
{
	TCHAR keyName[MAX_KEY_LEN];
	DWORD dwKeyLen = MAX_KEY_LEN;
	FILETIME tm;
	int index = 0;

	LONG dwRet = ::RegEnumKeyEx(hKey, index, keyName, &dwKeyLen,
		    NULL, NULL, 0, &tm);
	if(dwRet != ERROR_SUCCESS)
	{
		return FALSE;
	}
	while(dwRet != ERROR_NO_MORE_ITEMS && index < 1)
	{
		index++;
	    dwKeyLen = MAX_KEY_LEN;
        dwRet = ::RegEnumKeyEx(hKey, index, keyName, &dwKeyLen,
		      NULL, NULL, 0, &tm);
	}
	return TRUE;
}

//HKEY_CLASSES_ROOT
//HKEY_CURRENT_CONFIG
//HKEY_CURRENT_USER
//HKEY_LOCAL_MACHINE
//HKEY_USERS
//Windows NT/2000/XP: HKEY_PERFORMANCE_DATA 
//Windows 95/98/Me: HKEY_DYN_DATA 

RootReg CZAccessManDlg::m_root[7] = 
{
	{ _T("HKEY_CLASSES_ROOT"), HKEY_CLASSES_ROOT},
	{ _T("HKEY_CURRENT_CONFIG"), HKEY_CURRENT_CONFIG},
	{ _T("HKEY_CURRENT_USER"), HKEY_CURRENT_USER},
	{ _T("HKEY_LOCAL_MACHINE"), HKEY_LOCAL_MACHINE},
	{ _T("HKEY_USERS"), HKEY_USERS},
	{ _T("HKEY_PERFORMANCE_DATA"), HKEY_PERFORMANCE_DATA},
	{ _T("HKEY_DYN_DATA"), HKEY_DYN_DATA},
};

/////////////////////////////////////////////////////////////////////////////
// アプリケーションのバージョン情報で使われている CAboutDlg ダイアログ

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ダイアログ データ
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CStatic	m_diskinfo;
	CHyperLink	m_linkCodeGuru;
	//}}AFX_DATA

	// ClassWizard 仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV のサポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_DISK_INFO, m_diskinfo);
	DDX_Control(pDX, IDC_LINK_CODEGURU, m_linkCodeGuru);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog() 
{
	m_linkCodeGuru.SetURL(_T("http://www.codeguru.com/misc/RunUser.html"));
	m_linkCodeGuru.ModifyLinkStyle(0, CHyperLink::StyleUseHover);
	CDialog::OnInitDialog();
	m_linkCodeGuru.SetWindowText(_T("http://www.codeguru.com/misc/RunUser.html"));
    
	DWORD SectorsPerCluster, BytesPerSector, 
		  NumberOfFreeClusters, TotalNumberOfClusters;
	GetDiskFreeSpace(
  NULL,          // root path
  &SectorsPerCluster,     // sectors per cluster
  &BytesPerSector,        // bytes per sector
  &NumberOfFreeClusters,  // free clusters
  &TotalNumberOfClusters  // total clusters
    );
	CString str;
	str.Format(_T("Your Disk: %d Sector/Cluster, %d Bytes/Sector, %d/%d Cluster Free"),
		SectorsPerCluster, BytesPerSector, NumberOfFreeClusters,
		TotalNumberOfClusters);
	m_diskinfo.SetWindowText(str);
	return TRUE;  // コントロールにフォーカスを設定しないとき、戻り値は TRUE となります
	              // 例外: OCX プロパティ ページの戻り値は FALSE となります
}


/////////////////////////////////////////////////////////////////////////////
// CZAccessManDlg ダイアログ

CZAccessManDlg::CZAccessManDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CZAccessManDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CZAccessManDlg)
		// メモ: この位置に ClassWizard によってメンバの初期化が追加されます。
	//}}AFX_DATA_INIT
	// メモ: LoadIcon は Win32 の DestroyIcon のサブシーケンスを要求しません。
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CZAccessManDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CZAccessManDlg)
	DDX_Control(pDX, IDC_BANNER, m_bannar);
	DDX_Control(pDX, IDC_TREE, m_tree);
	DDX_Control(pDX, IDC_LIST, m_list);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CZAccessManDlg, CDialog)
	//{{AFX_MSG_MAP(CZAccessManDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CBN_SELCHANGE(IDC_TYPE, OnSelchangeType)
	ON_BN_CLICKED(IDB_EDIT, OnEdit)
	ON_BN_CLICKED(IDC_BUTTON_OPEN, OnButtonOpen)
	ON_NOTIFY(NM_CLICK, IDC_LIST, OnClickList)
	ON_NOTIFY(TVN_ITEMEXPANDED, IDC_TREE, OnItemexpandedTree)
	ON_NOTIFY(NM_CLICK, IDC_TREE, OnClickTree)
	ON_BN_CLICKED(IDC_BUTTON_X, OnButtonX)
	ON_BN_CLICKED(IDC_BUTTON_LOG, OnButtonLog)
	ON_EN_CHANGE(IDC_USER, OnChangeUser)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZAccessManDlg メッセージ ハンドラ

BOOL CZAccessManDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// "バージョン情報..." メニュー項目をシステム メニューへ追加します。

	// IDM_ABOUTBOX はコマンド メニューの範囲でなければなりません。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// このダイアログ用のアイコンを設定します。フレームワークはアプリケーションのメイン
	// ウィンドウがダイアログでない時は自動的に設定しません。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンを設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンを設定
	
	TCHAR szTitle[1024];
    lstrcpy(szTitle, TEXT("AccessMaster is running as \""));
    ULONG lSize = chDIMOF(szTitle)-lstrlen(szTitle);
    GetUserName(szTitle+lstrlen(szTitle),&lSize);
    lstrcat(szTitle, TEXT("\""));
    ::SetWindowText(m_hWnd, szTitle);
	lSize = 1024;
	GetUserName(szTitle,&lSize);
	::SetDlgItemText(m_hWnd, IDC_USER, szTitle);

	::EnableWindow(::GetDlgItem(m_hWnd, IDC_BUTTON_LOG), FALSE);
    
//	CBitmap* pBitmap = new CBitmap;
//	pBitmap->LoadOEMBitmap(OBM_CHECK);
//	this->m_staticLogState.SetBitmap(*pBitmap);
//	pBitmap->Detach();
//	delete pBitmap;
    // Set-up the object type combo
    int nIndex = sizeof(g_objMap)/sizeof(g_objMap[0]);//chDIMOF(g_objMap);
    CComboBox* pType = (CComboBox*)GetDlgItem(IDC_TYPE);
    while (nIndex-- != 0) {
      pType->InsertString(0, g_objMap[nIndex].m_pszComboText);
    }

    pType->SetCurSel(0);
    UpdateObjDependentCtrls();

    CImageList image;
	image.Create(MAKEINTRESOURCE(IDB_IMAGE), 16, 1, RGB(255, 0, 255));
	m_list.SetImageList(&image, LVSIL_SMALL);
	image.Detach();
	ListView_SetExtendedListViewStyle(m_list.GetSafeHwnd(),
		LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES);
    
	CImageList img;
	img.Create(IDB_REG, 16, 0, RGB(255, 255, 255));
	m_tree.SetImageList(&img, TVSIL_NORMAL);
	img.Detach();

	m_bannar.ShowWindow(SW_SHOW);
	m_list.ShowWindow(SW_HIDE);
	m_tree.ShowWindow(SW_HIDE);
	return TRUE;  // TRUE を返すとコントロールに設定したフォーカスは失われません。
}

void CZAccessManDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// もしダイアログボックスに最小化ボタンを追加するならば、アイコンを描画する
// コードを以下に記述する必要があります。MFC アプリケーションは document/view
// モデルを使っているので、この処理はフレームワークにより自動的に処理されます。

void CZAccessManDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画用のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// クライアントの矩形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンを描画します。
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// システムは、ユーザーが最小化ウィンドウをドラッグしている間、
// カーソルを表示するためにここを呼び出します。
HCURSOR CZAccessManDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CZAccessManDlg::UpdateObjDependentCtrls()
{
	HWND hwnd = this->GetSafeHwnd();
	// Setup controls for selected object type
	HWND hwndCtrl = ::GetDlgItem(hwnd, IDC_TYPE);
   int nIndex = ((CComboBox*)GetDlgItem(IDC_TYPE))->GetCurSel();

   ::SetDlgItemText(hwnd, IDE_USAGE, g_objMap[nIndex].m_pszUsageText);

   hwndCtrl = ::GetDlgItem(hwnd, IDE_NAME);
   ::EnableWindow(hwndCtrl, g_objMap[nIndex].m_fUseName);

   hwndCtrl = ::GetDlgItem(hwnd, IDE_HANDLE);
   ::EnableWindow(hwndCtrl, g_objMap[nIndex].m_fUseHandle);

   hwndCtrl = ::GetDlgItem(hwnd, IDE_PID);
   ::EnableWindow(hwndCtrl, g_objMap[nIndex].m_fUsePID);

   if (g_objMap[nIndex].m_fUsePID || g_objMap[nIndex].m_fUseHandle) 
   {
	   hwndCtrl = ::GetDlgItem(hwnd, IDR_HANDLE);
       ::EnableWindow(hwndCtrl, TRUE);
	  
   } 
   else
   {
	   hwndCtrl = ::GetDlgItem(hwnd, IDR_HANDLE);
      ::EnableWindow(hwndCtrl, FALSE);
      ::CheckRadioButton(hwnd, IDR_NAME, IDR_HANDLE, IDR_NAME);
   }

   if (g_objMap[nIndex].m_fUseName) 
   {
	   hwndCtrl = ::GetDlgItem(hwnd, IDR_NAME);
       ::EnableWindow(hwndCtrl, TRUE);
	   hwndCtrl = ::GetDlgItem(hwnd, IDC_BUTTON_OPEN);
       ::EnableWindow(hwndCtrl, TRUE);
   }
   else
   {
	   hwndCtrl = ::GetDlgItem(hwnd, IDR_NAME);
       ::EnableWindow(hwndCtrl, FALSE);
       ::CheckRadioButton(hwnd, IDR_NAME, IDR_HANDLE, IDR_HANDLE);
	   hwndCtrl = ::GetDlgItem(hwnd, IDC_BUTTON_OPEN);
       ::EnableWindow(hwndCtrl, FALSE);
   }
}

void CZAccessManDlg::OnSelchangeType() 
{
	UpdateObjDependentCtrls();
	CComboBox* pType = (CComboBox*)GetDlgItem(IDC_TYPE);
	int sel = pType->GetCurSel();
	m_bannar.ShowWindow(SW_HIDE);
	m_list.ShowWindow(SW_HIDE);
	m_tree.ShowWindow(SW_HIDE);
	if(::g_objMap[sel].m_nSpecificType == AM_REGISTRY)
	{		
		m_tree.ShowWindow(SW_SHOW);
	}
    else if(::g_objMap[sel].m_nSpecificType == AM_PROCESS ||
		::g_objMap[sel].m_nSpecificType == AM_THREAD ||
		::g_objMap[sel].m_nSpecificType == AM_DESKTOP ||
		::g_objMap[sel].m_nSpecificType == AM_WINDOWSTATION ||
		::g_objMap[sel].m_nSpecificType == AM_SERVICE)
	{		
		m_list.ShowWindow(SW_SHOW);
	}
	else
	{
		m_bannar.ShowWindow(SW_SHOW);
	}

    if(::g_objMap[sel].m_nSpecificType == AM_SERVICE)
	{
		this->ShowServicePreview();
	}
	else if(::g_objMap[sel].m_nSpecificType == AM_PROCESS ||
		::g_objMap[sel].m_nSpecificType == AM_THREAD)
	{
		this->ShowProcessThreadPreview();
	}
	else if(::g_objMap[sel].m_nSpecificType == AM_WINDOWSTATION ||
		::g_objMap[sel].m_nSpecificType == AM_DESKTOP)
	{
		this->ShowWindowStationDesktopPreview();
	}
	else if(::g_objMap[sel].m_nSpecificType == AM_REGISTRY)
	{
		this->ShowRegistryPreview();
	}
	else
	{
		m_list.ShowWindow(SW_HIDE);
	    m_tree.ShowWindow(SW_HIDE);
	}
}

void CZAccessManDlg::OnEdit() 
{
	HWND hwnd = this->GetSafeHwnd();
	// Maintains information about the object whose security we are editing
   ObjInf info = { 0 };

   // Fill the info structure with info from the UI
   if (FillInfo(&info)) 
   {
      // Create instance of class derived from interface ISecurityInformation 
      CSecurityInformation* pSec = new CSecurityInformation(&info, 
		  ::IsDlgButtonChecked(hwnd, IDC_BINARY) == BST_CHECKED);

      // Common dialog box for ACL editing
      EditSecurity(hwnd, pSec);
      if (pSec != NULL)
            pSec->Release();

      // Cleanup if we had opened a handle before
      if (info.m_szName[0] == 0) {

         switch (info.m_pEntry->m_nSpecificType) {

            case AM_FILE:
            case AM_PROCESS:
            case AM_THREAD:
            case AM_JOB:
            case AM_SEMAPHORE:
            case AM_EVENT:
            case AM_MUTEX:
            case AM_MAPPING:
            case AM_TIMER:
            case AM_TOKEN:
            case AM_NAMEDPIPE:
            case AM_ANONPIPE:
               CloseHandle(info.m_hHandle);
               break;
            
            case AM_REGISTRY:
               RegCloseKey((HKEY) info.m_hHandle);
               break;
            
            case AM_WINDOWSTATION:
               CloseWindowStation((HWINSTA) info.m_hHandle);
               break;
            
            case AM_DESKTOP:
               CloseDesktop((HDESK) info.m_hHandle);
               break;
         }
      }
   }
}

BOOL CZAccessManDlg::FillInfo(ObjInf* pInfo) 
{
   HWND hwnd = this->GetSafeHwnd();
   BOOL fReturn = FALSE;

   // Map object type to data block in the object map
   //HWND hwndCtrl = ::GetDlgItem(hwnd, IDC_TYPE);
   int nIndex = ((CComboBox*)GetDlgItem(IDC_TYPE))->GetCurSel();
	   //ComboBox_GetCurSel(hwndCtrl);
   pInfo->m_pEntry = g_objMap + nIndex;

   // Copy the object's name into the info block for building the title text
   lstrcpy(pInfo->m_szObjectName, pInfo->m_pEntry->m_pszComboText);

   // Is it a named item?
   if (::IsDlgButtonChecked(hwnd, IDR_NAME)) {
      switch (pInfo->m_pEntry->m_nSpecificType) {
         
         case AM_WINDOWSTATION:
            { // If windowstation, we must translate the name to a handle
               HWINSTA hwinsta = NULL;
               ::GetDlgItemText(hwnd, IDE_NAME, pInfo->m_szName, 
                     chDIMOF(pInfo->m_szName));
               // Get the maximum possible access
               hwinsta = OpenWindowStation(pInfo->m_szName, FALSE, 
                        MAXIMUM_ALLOWED);

               if (hwinsta == NULL) // Still failed?
                  ReportErrEx(TEXT("OpenWindowStation %d"), GetLastError());
               else { // Otherwise finish title text
                  fReturn = TRUE;
                  pInfo->m_hHandle = (HANDLE) hwinsta;
                  lstrcat(pInfo->m_szObjectName, TEXT("-"));
                  lstrcat(pInfo->m_szObjectName, pInfo->m_szName); 
                  pInfo->m_szName[0] = 0;
               }
            }
            break;

         case AM_DESKTOP:
            { // If desktop, we must translate the name to a handle
               HWINSTA hwinstaOld;
               HWINSTA hwinstaTemp;
               HDESK hdesk=NULL;
               PTSTR pszWinSta;
               PTSTR pszDesktop;
               int nIndex;

               ::GetDlgItemText(hwnd, IDE_NAME, pInfo->m_szName, 
                     chDIMOF(pInfo->m_szName));
               pszWinSta = pInfo->m_szName;
               nIndex = lstrlen(pInfo->m_szName);
               
               // Parse the text for windowstation and desktop
               while (nIndex-- != 0) {

                  if (pInfo->m_szName[nIndex] == TEXT('\\') 
                        || pInfo->m_szName[nIndex] == TEXT('/')) {

                     pInfo->m_szName[nIndex] = 0;
                     break;
                  }
               }
            
               // Desktop string
               nIndex++;
               pszDesktop = pInfo->m_szName + nIndex;
               // Open the windowstation
               hwinstaTemp = OpenWindowStation(pszWinSta, FALSE, 
                     DESKTOP_ENUMERATE);
               if (hwinstaTemp != NULL) {
                  // Save the last one
                  hwinstaOld = GetProcessWindowStation();
                  SetProcessWindowStation(hwinstaTemp);
                  // Get maximum access to the desktop
                  hdesk = OpenDesktop(pszDesktop, 0, FALSE, 
                           MAXIMUM_ALLOWED);
                  if (hdesk == NULL)// failed?
                     ReportErrEx(TEXT("OpenDesktop %d"), GetLastError());
                  else { // build title
                     fReturn = TRUE; 
                     pInfo->m_hHandle = (HANDLE) hdesk;
                     lstrcat(pInfo->m_szObjectName, TEXT("-"));
                     lstrcat(pInfo->m_szObjectName, pszDesktop);
                     pInfo->m_szName[0] = 0;
                  }
                  
                  // Close and reset window stations for the process
                  CloseWindowStation(hwinstaTemp);
                  SetProcessWindowStation(hwinstaOld);
            
               } else // Failed open winsta
                  ReportErrEx(TEXT("OpenWindowStation %d"), GetLastError());
            }
            break;

         default: // The rest of named objects work with GetNamedSecurity...
			 ::GetDlgItemText(hwnd, IDE_NAME, pInfo->m_szName, 
                  chDIMOF(pInfo->m_szName));
            lstrcat(pInfo->m_szObjectName, TEXT("-"));
            lstrcat(pInfo->m_szObjectName, pInfo->m_szName);
            fReturn = TRUE;
            break;
      }
   
   } else { // Is it a handle and or process id we are dealing with?

      //BOOL fTrans;
      // Get the actual numbers
//      ULONG lPid = ::GetDlgItemInt(hwnd, IDE_PID, &fTrans, FALSE);
//      HANDLE hHandle = (HANDLE) ::GetDlgItemInt(hwnd, IDE_HANDLE, &fTrans, 
//            FALSE);
	  CString strPID, strHandle;
	  int nRet = ::GetDlgItemText(hwnd, IDE_PID, strPID.GetBuffer(15), 15);
      strPID.ReleaseBuffer(nRet);
	  strPID.MakeLower();
      nRet = ::GetDlgItemText(hwnd, IDE_HANDLE, strHandle.GetBuffer(15), 15);
      strHandle.ReleaseBuffer(nRet);
      strHandle.MakeLower();

      ULONG lPid;
	  if(strPID.Left(2) == _T("0x"))
		  lPid = ::StringToHex(strPID);
	  else
		  lPid = _ttol((LPCTSTR)strPID);

	  HANDLE hHandle;
	  if(strHandle.Left(2) == _T("0x"))
		  hHandle = (HANDLE)::StringToHex(strHandle);
	  else
		  hHandle = (HANDLE)_ttol((LPCTSTR)strHandle);

      HANDLE hObj = NULL;

      switch (pInfo->m_pEntry->m_nSpecificType) {

         case AM_THREAD: // Maximum access to the thread
            hObj = OpenThread(MAXIMUM_ALLOWED, FALSE, lPid);
            if (hObj == NULL) // None == failure
               ReportErrEx(TEXT("OpenThread %d"), GetLastError());
            break;

         case AM_PROCESS: // Get maximum access to the process
            hObj = OpenProcess(MAXIMUM_ALLOWED, FALSE, lPid);
            if (hObj == NULL) // None == failure
               ReportErrEx(TEXT("OpenProcess %d"), GetLastError());
            break;

         default: // The rest work with duplicate handle
            {
               HANDLE hProcess = OpenProcess(PROCESS_DUP_HANDLE, FALSE, lPid);
               if (hProcess != NULL) {
                  // Get as much access as possible
                  if (!DuplicateHandle(hProcess, hHandle, GetCurrentProcess(),
                     &hObj, MAXIMUM_ALLOWED, FALSE, 0))
                     ReportErrEx(TEXT("DuplicateHandle %d"), GetLastError());
               } else
                  ReportErrEx(TEXT("OpenProcess %d"), GetLastError());
            }
            break;
      }

      if (hObj != NULL) {
         pInfo->m_hHandle = hObj;
         fReturn = TRUE;
      }
   }

   // Test object availability 
   if (fReturn) {
      ULONG lErr;
      PSECURITY_DESCRIPTOR pSD = NULL;
      if (pInfo->m_szName[0] != 0) // Is it named
         lErr = GetNamedSecurityInfo(pInfo->m_szName, 
                pInfo->m_pEntry->m_objType, DACL_SECURITY_INFORMATION, 
                NULL, NULL, NULL, NULL, &pSD);
      else // Is it a handle case
         lErr = GetSecurityInfo(pInfo->m_hHandle, pInfo->m_pEntry->m_objType,
                DACL_SECURITY_INFORMATION, NULL, NULL, NULL, NULL, &pSD);

      if ((lErr != ERROR_ACCESS_DENIED) && (lErr != ERROR_SUCCESS)){
         ReportErrEx(TEXT("Get[Named]SecurityInfo %d"), lErr);
         fReturn = FALSE;
      }
      else {
         LocalFree(pSD);
      }
   }

   return(fReturn);
}

void CZAccessManDlg::ShowServicePreview()
{
	ListView_SetExtendedListViewStyle(m_list.GetSafeHwnd(),
		LVS_EX_FULLROWSELECT);
	ListView_SetExtendedListViewStyle(m_list.GetSafeHwnd(),
		LVS_EX_CHECKBOXES);
	m_list.DeleteAllItems();
	//CHeaderCtrl* pHead = m_list.GetHeaderCtrl();
	//while(pHead->DeleteItem(0));
	while(m_list.DeleteColumn(0));
	
    int count = 0;
	m_list.AddColumn(_T("Internal Name"), count++);
	m_list.AddColumn(_T("Display Name"), count++);
	m_list.AddColumn(_T("Description "), count++);
	m_list.AddColumn(_T("Full Path"), count++);
	m_scm.Open(SC_MANAGER_CONNECT | SC_MANAGER_CREATE_SERVICE | 
      SC_MANAGER_ENUMERATE_SERVICE | SC_MANAGER_LOCK | 
      SC_MANAGER_QUERY_LOCK_STATUS, NULL);
   if (!m_scm.OpenOK()) 
   {
	   return;
   }

   m_scm.CreateStatusSnapshot(SERVICE_STATE_ALL);
   for (int nIndex = 0; nIndex < m_scm.GetStatusSnapshotNum(); nIndex++) 
   {
      const ENUM_SERVICE_STATUS* pService = 
         m_scm.GetStatusSnapshotEntry(nIndex);
	  
      m_list.AddItem(nIndex, 0, pService->lpServiceName);
	  m_list.AddItem(nIndex, 1, pService->lpDisplayName);

	  CServiceCtrl sc(m_scm, TRUE, pService->lpServiceName, SERVICE_QUERY_CONFIG);
      BOOL fOk = sc.OpenOK();
      if(!fOk) continue;

	  const QUERY_SERVICE_CONFIG* pConfig = sc.QueryConfig();
	  chASSERT(pConfig != NULL);
      m_list.AddItem(nIndex, 2, sc.QueryDescription());
      m_list.AddItem(nIndex, 3, pConfig->lpBinaryPathName);
	  
	  //Current State
	  CServiceCtrl sc2(m_scm, TRUE, pService->lpServiceName, SERVICE_QUERY_STATUS);
      SERVICE_STATUS ss = { 0 };
      fOk = (sc2.OpenOK() && sc2.QueryStatus(&ss));
      if (!fOk) 
	  {
		  //m_list.AddItem(nIndex, 8, _T("Unknown"));
	  }
	  else
	  {
		  LVITEM lv;
		  lv.iItem = nIndex;
		  lv.mask = LVIF_IMAGE;
		  switch (ss.dwCurrentState) {
			  case SERVICE_STOPPED:          
				  //m_list.AddItem(nIndex, 8, _T("Stopped")); 
				  lv.iImage = 0;
                  break;
              case SERVICE_START_PENDING:    
                  //m_list.AddItem(nIndex, 8, _T("Start pending")); 
				  lv.iImage = 1;
                  break;
              case SERVICE_STOP_PENDING:     
                  //m_list.AddItem(nIndex, 8, _T("Stop pending")); 
				  lv.iImage = 0;
                  break;
              case SERVICE_RUNNING:          
                  //m_list.AddItem(nIndex, 8, _T("Running")); 
				  lv.iImage = 1;
                  break;
              case SERVICE_CONTINUE_PENDING: 
                  //m_list.AddItem(nIndex, 8, _T("Continue pending")); 
				  lv.iImage = 1;
                  break;
              case SERVICE_PAUSE_PENDING:    
                  //m_list.AddItem(nIndex, 8, _T("Pause pending")); 
				  lv.iImage = 1;
                  break;
              case SERVICE_PAUSED:           
                  //m_list.AddItem(nIndex, 8, _T("Paused")); 
				  lv.iImage = 0;
                  break;
          }
		  lv.iImage += 2;
		  ListView_SetItem(m_list.GetSafeHwnd(), &lv);
	  }

	  if(pConfig->dwServiceType & SERVICE_INTERACTIVE_PROCESS)
	  {
		  //m_list.AddItem(nIndex, 9, _T("Y"));
		  ListView_SetItemState (m_list.GetSafeHwnd(), nIndex, 
		      UINT((1 + 1) << 12), LVIS_STATEIMAGEMASK);

	  }
	  else
		  ListView_SetItemState (m_list.GetSafeHwnd(), nIndex, 
		      UINT((1) << 12), LVIS_STATEIMAGEMASK);
   }
}


#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

#define NTAPI	__stdcall

typedef LONG	NTSTATUS;
typedef LONG	KPRIORITY;

#define NT_SUCCESS(Status) ((NTSTATUS)(Status) >= 0)

#define STATUS_INFO_LENGTH_MISMATCH      ((NTSTATUS)0xC0000004L)

#define SystemProcessesAndThreadsInformation	5

extern "C"
NTSYSAPI
NTSTATUS
NTAPI
ZwQuerySystemInformation(
    IN	    UINT SystemInformationClass,
    IN OUT  PVOID SystemInformation,
    IN	    ULONG SystemInformationLength,
    OUT	    PULONG ReturnLength OPTIONAL
    );

typedef struct _CLIENT_ID {
    DWORD	    UniqueProcess;
    DWORD	    UniqueThread;
} CLIENT_ID;

typedef struct _UNICODE_STRING {
    USHORT	    Length;
    USHORT	    MaximumLength;
    PWSTR	    Buffer;
} UNICODE_STRING;

typedef struct _VM_COUNTERS {
    SIZE_T	    PeakVirtualSize;
    SIZE_T	    VirtualSize;
    ULONG	    PageFaultCount;
    SIZE_T	    PeakWorkingSetSize;
    SIZE_T	    WorkingSetSize;
    SIZE_T	    QuotaPeakPagedPoolUsage;
    SIZE_T	    QuotaPagedPoolUsage;
    SIZE_T	    QuotaPeakNonPagedPoolUsage;
    SIZE_T	    QuotaNonPagedPoolUsage;
    SIZE_T	    PagefileUsage;
    SIZE_T	    PeakPagefileUsage;
} VM_COUNTERS;

typedef struct _SYSTEM_THREADS {
    LARGE_INTEGER   KernelTime;
    LARGE_INTEGER   UserTime;
    LARGE_INTEGER   CreateTime;
    ULONG			WaitTime;
    PVOID			StartAddress;
    CLIENT_ID	    ClientId;
    KPRIORITY	    Priority;
    KPRIORITY	    BasePriority;
    ULONG			ContextSwitchCount;
    LONG			State;
    LONG			WaitReason;
} SYSTEM_THREADS, * PSYSTEM_THREADS;

// Note, that the size of the SYSTEM_PROCESSES structure is different on
// NT 4 and Win2K.

typedef struct _SYSTEM_PROCESSES_NT4 {
    ULONG			NextEntryDelta;
    ULONG			ThreadCount;
    ULONG			Reserved1[6];
    LARGE_INTEGER   CreateTime;
    LARGE_INTEGER   UserTime;
    LARGE_INTEGER   KernelTime;
    UNICODE_STRING  ProcessName;
    KPRIORITY	    BasePriority;
    ULONG			ProcessId;
    ULONG			InheritedFromProcessId;
    ULONG			HandleCount;
    ULONG			Reserved2[2];
    VM_COUNTERS	    VmCounters;
    SYSTEM_THREADS  Threads[1];
} SYSTEM_PROCESSES_NT4, * PSYSTEM_PROCESSES_NT4;

typedef struct _SYSTEM_PROCESSES {
    ULONG			NextEntryDelta;
    ULONG			ThreadCount;
    ULONG			Reserved1[6];
    LARGE_INTEGER   CreateTime;
    LARGE_INTEGER   UserTime;
    LARGE_INTEGER   KernelTime;
    UNICODE_STRING  ProcessName;
    KPRIORITY	    BasePriority;
    ULONG			ProcessId;
    ULONG			InheritedFromProcessId;
    ULONG			HandleCount;
    ULONG			Reserved2[2];
    VM_COUNTERS	    VmCounters;
    IO_COUNTERS	    IoCounters;
    SYSTEM_THREADS  Threads[1];
} SYSTEM_PROCESSES, * PSYSTEM_PROCESSES;


void CZAccessManDlg::ShowProcessThreadPreview()
{
	ListView_SetExtendedListViewStyle(m_list.GetSafeHwnd(),
		LVS_EX_FULLROWSELECT);
	//ListView_SetExtendedListViewStyle(m_list.GetSafeHwnd(),
	//	LVS_EX_CHECKBOXES);
	//m_list.ModifyStyleEx(LVS_EX_CHECKBOXES, 0, SWP_SHOWWINDOW);
	m_list.DeleteAllItems();
	
	//CHeaderCtrl* pHead = m_list.GetHeaderCtrl();
	//while(pHead->DeleteItem(0));
	while(m_list.DeleteColumn(0));
	
    int count = 0;
	m_list.AddColumn(_T("PID(TID)      "), count++);
	m_list.AddColumn(_T("Name      "), count++);
	m_list.AddColumn(_T("Exe Path "), count++);

	// determine operating system version
    OSVERSIONINFO osvi;
    osvi.dwOSVersionInfoSize = sizeof(osvi);
    GetVersionEx(&osvi);

    ULONG cbBuffer = 0x8000;
    LPVOID pBuffer = NULL;
    NTSTATUS Status;
    
	HMODULE hNtDll = ::LoadLibrary(_T("ntdll.dll"));
	if(hNtDll == NULL) return;

typedef UINT (WINAPI *MyZwQuerySystemInformation)(UINT, PVOID, ULONG, PULONG);
//extern "C"
MyZwQuerySystemInformation     ZwQuerySystemInformation; 
//ZwQuerySystemInformation(
//    IN	    UINT SystemInformationClass,
//    IN OUT  PVOID SystemInformation,
//    IN	    ULONG SystemInformationLength,
//    OUT	    PULONG ReturnLength OPTIONAL
//    );
    ZwQuerySystemInformation =(MyZwQuerySystemInformation)GetProcAddress(hNtDll, "ZwQuerySystemInformation");
  	// it is difficult to say a priory which size of the buffer 
    // will be enough to retrieve all information, so we start
    // with 32K buffer and increase its size until we get the
    // information successfully
    do
    {
		pBuffer = malloc(cbBuffer);
		if (pBuffer == NULL)
		{
			//_tprintf(_T("Not enough memory\n"));
			return ;
		}

		Status = ZwQuerySystemInformation(
					SystemProcessesAndThreadsInformation,
					pBuffer, cbBuffer, NULL);

		if (Status == STATUS_INFO_LENGTH_MISMATCH)
		{
			free(pBuffer);
			cbBuffer *= 2;
		}
		else if (!NT_SUCCESS(Status))
		{
			//_tprintf(_T("ZwQuerySystemInformation failed with")
			//		 _T("status 0x%08X\n"), Status);

			free(pBuffer);
			return;
		}
    }
    while (Status == STATUS_INFO_LENGTH_MISMATCH);

    PSYSTEM_PROCESSES pProcesses = (PSYSTEM_PROCESSES)pBuffer;

    for (;;)
    {
		PCWSTR pszProcessName = pProcesses->ProcessName.Buffer;
		if (pszProcessName == NULL)
			pszProcessName = L"Idle";

		//_tprintf(_T("ProcessID: %d (%ls)\n"), pProcesses->ProcessId,
		//		 pszProcessName);
		int count = m_list.GetItemCount();
		CString str; 
		str.Format(_T("0x%X"), pProcesses->ProcessId);
		m_list.AddItem(count, 0, str, 4);
		str.Format(_T("%s"), pszProcessName);
		m_list.AddItem(count, 1, str);
		str.Format(_T("%s"), pszProcessName);
		m_list.AddItem(count, 2, str);

//        LVITEM lv;
//		lv.iItem = count;
//		lv.mask = LVIF_IMAGE;
//		lv.iImage = 4;
//		ListView_SetItem(m_list.GetSafeHwnd(), &lv);

    		if (osvi.dwMajorVersion >= 5)
		{
			for (ULONG i = 0; i < pProcesses->ThreadCount; i++)
			{
				//_tprintf(_T("\tThreadID: %d\n"), 
				//	 pProcesses->Threads[i].ClientId.UniqueThread);
				int count = m_list.GetItemCount();
		        CString str; 
		        str.Format(_T("0x%X"), pProcesses->Threads[i].ClientId.UniqueThread);
		        m_list.AddItem(count, 0, str, 5);
		        //str.Format(_T("%s"), pszProcessName);
		        //m_list.AddItem(count, 1, str);
		        //str.Format(_T("%s"), pProcesses->ProcessName);
		        //m_list.AddItem(count, 2, str);

//                LVITEM lv;
//		        lv.iItem = count;
//		        lv.mask = LVIF_IMAGE;
//		        lv.iImage = 5;
//		        ListView_SetItem(m_list.GetSafeHwnd(), &lv);

			}
		}
		else
		{
			PSYSTEM_PROCESSES_NT4 pProcessesNT4 = 
				(PSYSTEM_PROCESSES_NT4)pProcesses;

			for (ULONG i = 0; i < pProcessesNT4->ThreadCount; i++)
			{
				//_tprintf(_T("\tThreadID: %d\n"), 
				//	 pProcessesNT4->Threads[i].ClientId.UniqueThread);
			}
		}

		if (pProcesses->NextEntryDelta == 0)
			break;

		// find the address of the next process structure
		pProcesses = (PSYSTEM_PROCESSES)(((LPBYTE)pProcesses)
						+ pProcesses->NextEntryDelta);
    }

    free(pBuffer);
}

BOOL CALLBACK myEnumDesktopProc(
  LPTSTR lpszDesktop,  // desktop name
  LPARAM lParam        // user-defined value
)
{
	CListCtrlEx* pList = (CListCtrlEx*)lParam;
	int count = pList->GetItemCount();
	pList->AddItem(count, 0, (LPCTSTR)lpszDesktop, 7);
//	LVITEM lv;
//	lv.iItem = count;
//	lv.mask = LVIF_IMAGE;
//	lv.iImage = 7;
//	ListView_SetItem(pList->GetSafeHwnd(), &lv);
	return TRUE;
}


BOOL CALLBACK myEnumWindowStationProc(
  LPTSTR lpszWindowStation,  // window station name
  LPARAM lParam             // user-defined value
)
{
	//CListCtrlEx* pList = new CListCtrlEx;
	//pList->Attach((HWND)lParam);
    CListCtrlEx* pList = (CListCtrlEx*)lParam;
	int count = pList->GetItemCount();
	pList->AddItem(count, 0, (LPCTSTR)lpszWindowStation, 6);
	
//	LVITEM lv;
//	lv.iItem = count;
//	lv.mask = LVIF_IMAGE;
//	lv.iImage = 6;
//	ListView_SetItem(pList->GetSafeHwnd(), &lv);

	//pList->Detach();
	//delete pList;
	HWINSTA hwinsta = ::OpenWindowStation(lpszWindowStation,
		FALSE, WINSTA_ENUMDESKTOPS);
	if(hwinsta != NULL)
	{
		::EnumDesktops(hwinsta, myEnumDesktopProc ,lParam);
	}
	return TRUE;
}

void CZAccessManDlg::ShowWindowStationDesktopPreview()
{
	ListView_SetExtendedListViewStyle(m_list.GetSafeHwnd(),
		LVS_EX_FULLROWSELECT);
	//m_list.ModifyStyleEx(LVS_EX_CHECKBOXES, 0, SWP_SHOWWINDOW);
	m_list.DeleteAllItems();
	//CHeaderCtrl* pHead = m_list.GetHeaderCtrl();
	//while(pHead->DeleteItem(0));
	while(m_list.DeleteColumn(0));
	
    int count = 0;
	m_list.AddColumn(_T("Name      "), count++);

	//::EnumWindowStations(myEnumWindowStationProc, (LPARAM)m_list.GetSafeHwnd());
	::EnumWindowStations(myEnumWindowStationProc, (LPARAM)&m_list);
}


void CZAccessManDlg::OnButtonOpen() 
{
	CComboBox* pType = (CComboBox*)GetDlgItem(IDC_TYPE);
	int sel = pType->GetCurSel();
    if(::g_objMap[sel].m_nSpecificType == AM_FILE)
	{
		static TCHAR szFilter[] = _T("All Files (*.*)|*.*||");
	    CFileDialog* pDlg = new CFileDialog(TRUE, _T(""),
		  _T("\\*.*"),
		  OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, szFilter);
	    if(pDlg->DoModal() != IDOK) { delete pDlg;  return;}		
        CEdit* pEdit = (CEdit*)GetDlgItem(IDE_NAME);
		pEdit->SetWindowText(pDlg->GetPathName());
		delete pDlg;
	}
	else if(::g_objMap[sel].m_nSpecificType == AM_DIR)
	{
		CString strDir = ::BrowsePath(_T("Choose Direcoty"));
	    CEdit* pEdit = (CEdit*)GetDlgItem(IDE_NAME);
		pEdit->SetWindowText(strDir);		
	}
	else if(::g_objMap[sel].m_nSpecificType == AM_PRINTER)
	{
		CString strDir = ::BrowsePrinter(_T("Choose Printer"));
	    CEdit* pEdit = (CEdit*)GetDlgItem(IDE_NAME);
		pEdit->SetWindowText(strDir);		
	}
	else if(::g_objMap[sel].m_nSpecificType == AM_SHARE)
	{
		CString strDir = ::BrowsePath(_T("Choose Share"), BIF_SHAREABLE | BIF_USENEWUI);
	    CEdit* pEdit = (CEdit*)GetDlgItem(IDE_NAME);
		pEdit->SetWindowText(strDir);		
	}
	HWND hwnd = this->GetSafeHwnd();
	::CheckRadioButton(hwnd, IDR_NAME, IDR_HANDLE, IDR_NAME);	
}

void CZAccessManDlg::OnClickList(NMHDR* pNMHDR, LRESULT* pResult) 
{ 
	//*pResult = 0;
    //return;

	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
    LVHITTESTINFO lvhti;
	lvhti.pt = pNMListView->ptAction;
	int nItem = m_list.SubItemHitTest(&lvhti);

	if(nItem != -1)
	{

	}
	CComboBox* pType = (CComboBox*)GetDlgItem(IDC_TYPE);
	int sel = pType->GetCurSel();
    //if (lvhti.flags & LVHT_ONITEMLABEL)
	if (lvhti.flags & LVHT_ONITEM)
    {
		m_list.SetItem(nItem, 0, LVIF_STATE, NULL, 0, LVIS_SELECTED, 
			LVIS_SELECTED, 0);
		CString strName = m_list.GetItemText(nItem, 0);
        if(::g_objMap[sel].m_nSpecificType == AM_SERVICE)
		{
			CEdit* pEdit = (CEdit*)GetDlgItem(IDE_NAME);
			pEdit->SetWindowText(strName);
			HWND hwnd = this->GetSafeHwnd();
			::CheckRadioButton(hwnd, IDR_NAME, IDR_HANDLE, IDR_NAME);
		} 
		else if(::g_objMap[sel].m_nSpecificType == AM_PROCESS ||
		::g_objMap[sel].m_nSpecificType == AM_THREAD)
		{
			DWORD dw = ::StringToHex(strName);
			strName.Format(_T("%d"), dw);
			CEdit* pEdit = (CEdit*)GetDlgItem(IDE_PID);
			pEdit->SetWindowText(strName);  
			HWND hwnd = this->GetSafeHwnd();
			::CheckRadioButton(hwnd, IDR_NAME, IDR_HANDLE, IDR_HANDLE);
		}
		else if(::g_objMap[sel].m_nSpecificType == AM_WINDOWSTATION)
		{
			CEdit* pEdit = (CEdit*)GetDlgItem(IDE_NAME);
			pEdit->SetWindowText(strName);
			HWND hwnd = this->GetSafeHwnd();
			::CheckRadioButton(hwnd, IDR_NAME, IDR_HANDLE, IDR_NAME);
		}
		else if(::g_objMap[sel].m_nSpecificType == AM_DESKTOP)
		{
			//Desktop Name must be proceeded with WinStation Name
            CString strStation = _T("");
            for(int i = nItem; i >= 0; i--)
			{
				LVITEM lv;
		        lv.iItem = i; 
				lv.iSubItem = 0;
		        lv.mask = LVIF_IMAGE;
                ListView_GetItem(m_list.GetSafeHwnd(), &lv);
				if(lv.iImage == 6)
				{
					strStation = m_list.GetItemText(i, 0);
					break;
				}
			}
			ASSERT(!strStation.IsEmpty());
			strName = strStation + _T("\\") + strName;
			CEdit* pEdit = (CEdit*)GetDlgItem(IDE_NAME);
			pEdit->SetWindowText(strName);
			HWND hwnd = this->GetSafeHwnd();
			::CheckRadioButton(hwnd, IDR_NAME, IDR_HANDLE, IDR_NAME);
		}
	}
	*pResult = 0;
}

void CZAccessManDlg::ShowRegistryPreview()
{
	static BOOL bFirst = TRUE;
	if(!bFirst) return;
	bFirst = FALSE;

	m_tree.DeleteAllItems();
	//Add My Computer
	CString szComputerName;
	DWORD dwSize = MAX_COMPUTERNAME_LENGTH + 1; //defed in WinBase = 15
    GetComputerName(szComputerName.GetBuffer(MAX_COMPUTERNAME_LENGTH + 1), &dwSize);
    szComputerName.ReleaseBuffer(-1);
	HTREEITEM hRoot = m_tree.AddItem(szComputerName, _T(','), FALSE, 0, 0, 0);
    //Add Root Reg
	int dim = sizeof(m_root)/sizeof(RootReg);
	for(int i = 0; i < dim; i++)
	{
		CString str = szComputerName;
		str += _T(",");
		str += m_root[i].szName;
		HTREEITEM hItem = m_tree.AddItem(str, _T(','),
			FALSE, 9, 9, (DWORD)m_root[i].hKey);
		if(::lstrcmp(m_root[i].szName, _T("HKEY_CLASSES_ROOT")) == 0)
		{
			m_tree.InsertItem(_T("***"), hItem);
			FillNode(hItem);
			//m_tree.Expand(hItem, TVE_EXPAND);
			//Try to Expand "CLSID" subItem
/*			HTREEITEM hSubItem = m_tree.GetNextItem(hItem, TVGN_CHILD);
			while(hSubItem)
			{
				CString str = m_tree.GetItemText(hSubItem);
				if(str.Left(5) == _T("CLSID"))
				{
                    m_tree.InsertItem(_T("***"), hSubItem);
                    FillNode(hSubItem);
                    m_tree.Expand(hSubItem, TVE_EXPAND);
					break;
				}
				hSubItem = m_tree.GetNextItem(hSubItem, TVGN_NEXT); //next sibling
			}
*/
		}
		else if(::lstrcmp(m_root[i].szName, _T("HKEY_PERFORMANCE_DATA")) == 0)
		{
			if(HasSubKey(HKEY_PERFORMANCE_DATA))
				m_tree.InsertItem(_T("***"), hItem);
		}
	    else if(::lstrcmp(m_root[i].szName, _T("HKEY_DYN_DATA")) == 0)
		{
			if(HasSubKey(HKEY_PERFORMANCE_DATA))
				m_tree.InsertItem(_T("***"), hItem);
		}
		else
			m_tree.InsertItem(_T("***"), hItem); //Fake Item To Give + box         
	}
	m_tree.Expand(hRoot, TVE_EXPAND);
}

//If only one child, and the child's text is ***, FALSE
BOOL CZAccessManDlg::IsFilledNode(HTREEITEM hItem)
{
	HTREEITEM hChild = m_tree.GetNextItem(hItem, TVGN_CHILD);
	if(!hChild) return TRUE;
	CString str = m_tree.GetItemText(hChild);
	if(str == _T("***")) return FALSE;
	return TRUE;
}

BOOL CZAccessManDlg::FillNode(HTREEITEM hItem)
{
	//delete fake child
	HTREEITEM hChild = m_tree.GetNextItem(hItem, TVGN_CHILD);
	if(hChild) m_tree.DeleteItem(hChild);
	CString strPath = DeleteTimePart(m_tree.GetFullPath(hItem, TCHAR('\\')));
    //MyComputer\HKEY_CLASS_ROOT\STH
	int pos = strPath.Find(TCHAR('\\'));
	ASSERT(pos != -1);
	pos = strPath.Find(TCHAR('\\'), pos + 1);
	HKEY hRootKey, hKey;
	TCHAR keyName[MAX_KEY_LEN];
	DWORD dwKeyLen = MAX_KEY_LEN;
	FILETIME tm;
	SYSTEMTIME now;
	::GetLocalTime(&now);
    CString strTime;
	int index = 0;
	if(pos == -1) //Expand Root Reg Item
	{
		hRootKey = (HKEY)m_tree.GetParam(hItem);
		hKey = hRootKey;
	}
	else
	{
		CString strRootItem = strPath.Left(pos);
		HTREEITEM hRoot = m_tree.IsAvailable(strRootItem, _T('\\'));
		if(!hRoot) return FALSE;
		CString strItemPath = strPath.Mid(pos+1);
		hRootKey = (HKEY)m_tree.GetParam(hRoot);
		DWORD dwRet = RegOpenKeyEx(hRootKey, strItemPath, 0,
     			KEY_READ, &hKey);
		if(ERROR_SUCCESS != dwRet) return FALSE;
	}

	LONG dwRet = ::RegEnumKeyEx(hKey, index, keyName, &dwKeyLen,
		    NULL, NULL, 0, &tm);
	if(dwRet != ERROR_SUCCESS)
	{
		return FALSE;
	}
	while(dwRet != ERROR_NO_MORE_ITEMS)
	{
		FILETIME lt;
		SYSTEMTIME st;
		FileTimeToLocalFileTime(&tm, &lt);
        ::FileTimeToSystemTime(&lt, &st);
        
		if(now.wYear == st.wYear)
		{
			strTime.Format(_T("(%d/%d %d:%d:%d)"), st.wMonth, st.wDay,
				st.wHour, st.wMinute, st.wSecond);
		}
		else
		{
			strTime.Format(_T("(%d/%d/%d %d:%d:%d)"), st.wYear, st.wMonth, st.wDay,
				st.wHour, st.wMinute, st.wSecond);
		}
		HKEY hSubKey;
        dwRet = RegOpenKeyEx(hKey, keyName, 0,
    			KEY_ALL_ACCESS, &hSubKey);
		//try all access right
        if(ERROR_SUCCESS == dwRet)
		{
			//ok all permission
			CString strItem = strPath;
			strItem += _T("\\");
			strItem += keyName;
			//I have to, for class root contains too many items
			//m_tree.AddItem(strItem, _T('\\'), FALSE, 2,2,0);
			HTREEITEM hCur = m_tree.InsertItem(keyName+strTime, 2, 2, hItem);
		    //Have Child ? 
			if(HasSubKey(hSubKey)) 
				m_tree.InsertItem(_T("***"), hCur);
            RegCloseKey(hSubKey);
		}
	    else //try read permission
		{
			dwRet = RegOpenKeyEx(hKey, keyName, 0,
		        KEY_READ, &hSubKey);
		    if(ERROR_SUCCESS == dwRet) //read only
			{
				CString strItem = strPath;
			    strItem += _T("\\");
			    strItem += keyName;
				//m_tree.AddItem(strItem, _T('\\'), FALSE, 2,2,0);
			    HTREEITEM hCur = m_tree.InsertItem(keyName+strTime, 7, 7, hItem);
				//have child? 
				if(HasSubKey(hSubKey)) 
				    m_tree.InsertItem(_T("***"), hCur);
                RegCloseKey(hSubKey);
			}
	        else //error
			{
				CString strItem = strPath;
		        strItem += _T("\\");
		        strItem += keyName;
			    //m_tree.AddItem(strItem, _T('\\'), FALSE, 2,2,0);
		        m_tree.InsertItem(keyName+strTime, 6, 6, hItem);
			}
		}
		index++;
	    dwKeyLen = 1024;
        dwRet = ::RegEnumKeyEx(hKey, index, keyName, &dwKeyLen,
		      NULL, NULL, 0, &tm);
	} 
	::RegCloseKey(hKey);
	return TRUE;
}

//Versatile Parser
void CZAccessManDlg::ShowNode(CString strPath)
{
	CString str = strPath;
	int dim = sizeof(m_root)/sizeof(m_root[0]);
	for(int i = 0; i < dim; i++)
	{
		int pos = str.Find(m_root[i].szName);
		if(pos != -1) 
		{
			str = str.Mid(pos);
			break;
		}
	}
	if(i == dim) return;
	if(str.Right(1) != _T("\\")) str += _T("\\");
    HTREEITEM hRoot = m_tree.GetRootItem();
    RecursiveShowNode(hRoot, str);
}

void CZAccessManDlg::RecursiveShowNode(HTREEITEM hParent, CString strPath)
{
	int pos = strPath.Find(_T("\\"));
	CString strCur = strPath.Left(pos);
    HTREEITEM hChild = m_tree.GetNextItem(hParent, TVGN_CHILD);
	while(hChild)
	{
		CString strNodeText = DeleteTimePart(m_tree.GetItemText(hChild));
        if(strNodeText == strCur)
		{
			if(!this->IsFilledNode(hChild))
			{
				this->FillNode(hChild);
			}
			if(pos == strPath.GetLength() - 1)
			{
				m_tree.SelectItem(hChild);
				m_tree.EnsureVisible(hChild);
				return;
			}
			else
				this->RecursiveShowNode(hChild, strPath.Mid(pos + 1));
			break;
		}
		hChild = m_tree.GetNextItem(hChild, TVGN_NEXT);
	}
}

CString CZAccessManDlg::DeleteTimePart(CString strPath)
{
	int pos1, pos2;
	CString strRet = strPath;
	pos1 = strPath.Find(_T("("));
	while(pos1 != -1)
	{
		pos2 = strRet.Find(_T(")"));
		if(pos2 != strRet.GetLength()-1)
			strRet = strRet.Left(pos1) + strRet.Mid(pos2+1);
		else
			strRet = strRet.Left(pos1);
        pos1 = strRet.Find(_T("("));
	}
	return strRet;
}

CString CZAccessManDlg::GetSelectedPath()
{
	HTREEITEM hItem = m_tree.GetSelectedItem();
	if(!hItem) //Root Item
	{
		hItem = m_tree.GetRootItem();
	}
	CString strPath = m_tree.GetFullPath(hItem, _T('\\'));
	return DeleteTimePart(strPath);
}

void CZAccessManDlg::OnItemexpandedTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
    HTREEITEM hItem = pNMTreeView->itemNew.hItem;
	if(hItem)
	{
		int nImage, nSelectedImage;
		m_tree.GetItemImage(hItem, nImage, nSelectedImage);
		if(nImage == 2)
		{
			nImage = 3; nSelectedImage = 3;
			m_tree.SetItemImage(hItem, nImage, nSelectedImage);
		}
		else if(nImage == 3)
		{
			nImage = 2; nSelectedImage = 2;
			m_tree.SetItemImage(hItem, nImage, nSelectedImage);
		}
		else if(nImage == 7)
		{
			nImage = 8; nSelectedImage = 8;
			m_tree.SetItemImage(hItem, nImage, nSelectedImage);
		}
		else if(nImage == 8)
		{
			nImage = 7; nSelectedImage = 7;
			m_tree.SetItemImage(hItem, nImage, nSelectedImage);
		}
		else if(nImage == 9)
		{
			nImage = 10; nSelectedImage = 10;
			m_tree.SetItemImage(hItem, nImage, nSelectedImage);
		}
		else if(nImage == 10)
		{
			nImage = 9; nSelectedImage = 9;
			m_tree.SetItemImage(hItem, nImage, nSelectedImage);
		}
		else
		{
			*pResult = 0;
			return;
		}
		if(!IsFilledNode(hItem))
			FillNode(hItem);
		
		//CString strText = m_tree.GetItemText(hItem);
		//PopMsg(strText);
	}
	*pResult = 0;
}

void CZAccessManDlg::OnClickTree(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CString strPath = this->GetSelectedPath();
	//add \\ before Machine Name
	strPath = _T("\\\\") + strPath;

	//Delete HKEY_CLASS_ROOT --> CLASS_ROOT, otherwise error 87
	int pos = strPath.Find(_T("\\HKEY_"));
	if(pos == -1) return;
	ASSERT(pos != -1);
	strPath = /*strPath.Left(pos+1) + */strPath.Mid(pos+6);
	CEdit* pEdit = (CEdit*)GetDlgItem(IDE_NAME);
	pEdit->SetWindowText(strPath);
	HWND hwnd = this->GetSafeHwnd();
	::CheckRadioButton(hwnd, IDR_NAME, IDR_HANDLE, IDR_NAME);
	*pResult = 0;
}

void CZAccessManDlg::OnButtonX() 
{

	// Maintains information about the object whose security we are editing
    ObjInf info = { 0 };

    // Fill the info structure with info from the UI
    if (FillInfo(&info)) 
    {
		CACLDumpDlg* pDlg = new CACLDumpDlg(&info);
	    //Set the name if applicable
        if (::IsDlgButtonChecked(this->m_hWnd, IDR_NAME)) 
		{
			if(info.m_pEntry->m_nSpecificType == AM_WINDOWSTATION ||
				info.m_pEntry->m_nSpecificType == AM_DESKTOP)	
			{
				int nRet = ::GetDlgItemText(this->m_hWnd, IDE_NAME, pDlg->m_strName.GetBuffer(2048), 
                     2048);
                pDlg->m_strName.ReleaseBuffer(nRet);			
			}
		}
	    pDlg->DoModal();
	    delete pDlg;
	   
        // Cleanup if we had opened a handle before
        if(info.m_szName[0] == 0)
		{
			switch (info.m_pEntry->m_nSpecificType) 
			{
            case AM_FILE:
            case AM_PROCESS:
            case AM_THREAD:
            case AM_JOB:
            case AM_SEMAPHORE:
            case AM_EVENT:
            case AM_MUTEX:
            case AM_MAPPING:
            case AM_TIMER:
            case AM_TOKEN:
            case AM_NAMEDPIPE:
            case AM_ANONPIPE:
               CloseHandle(info.m_hHandle);
               break;
            
            case AM_REGISTRY:
               RegCloseKey((HKEY) info.m_hHandle);
               break;
            
            case AM_WINDOWSTATION:
               CloseWindowStation((HWINSTA) info.m_hHandle);
               break;
            
            case AM_DESKTOP:
               CloseDesktop((HDESK) info.m_hHandle);
               break;
         }
      }
   }
}

#include "xLog.h"
void CZAccessManDlg::OnButtonLog() 
{
	CString strUser, strPwd;
	int nRet = ::GetDlgItemText(m_hWnd, IDC_USER, strUser.GetBuffer(MAX_PATH), MAX_PATH);
	strUser.ReleaseBuffer(nRet);
	nRet = ::GetDlgItemText(m_hWnd, IDC_USER, strPwd.GetBuffer(MAX_PATH), MAX_PATH);
	strPwd.ReleaseBuffer(nRet);

	CString strExe;
    nRet = ::GetModuleFileName(NULL, strExe.GetBuffer(4096), 4096);
	strExe.ReleaseBuffer(nRet);
    if(strExe.IsEmpty()) 
	{
		PopMsg(_T("Invalid Exe Path -- %s"), strExe);
		return;
	}
	BOOL bRet = FALSE;
	if(strUser.IsEmpty()) //try to log on as system
	{
		bRet = ::RunAsUser((LPTSTR)(LPCTSTR)strExe, NULL,
		    NULL, NULL, TEXT("Winsta0\\Default"));
	}
	else if(strPwd.IsEmpty())
	{
		bRet = ::RunAsUser((LPTSTR)(LPCTSTR)strExe, (LPTSTR)(LPCTSTR)strUser,
		    NULL, NULL, TEXT("Winsta0\\Default"));
	}
	else
	{
		bRet = ::RunAsUser((LPTSTR)(LPCTSTR)strExe, (LPTSTR)(LPCTSTR)strUser,
		    NULL, (LPTSTR)(LPCTSTR)strPwd, TEXT("Winsta0\\Default"));
	}
	if(bRet)
	{
		if(BST_CHECKED == ::SendMessage(::GetDlgItem(m_hWnd, IDC_CHECK_QUIT_AFTER_LOG), BM_GETSTATE, 0, 0))
			PostQuitMessage(0);
	}	
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_BUTTON_LOG), FALSE);
	TCHAR szTitle[1024];
    DWORD lSize = 1024;
	GetUserName(szTitle,&lSize);
	::SetDlgItemText(m_hWnd, IDC_USER, szTitle);
	szTitle[0] = TCHAR('\0');
	::SetDlgItemText(m_hWnd, IDC_PASSWORD,  szTitle);
}

void CZAccessManDlg::OnChangeUser() 
{
	::EnableWindow(::GetDlgItem(m_hWnd, IDC_BUTTON_LOG), TRUE);
}
