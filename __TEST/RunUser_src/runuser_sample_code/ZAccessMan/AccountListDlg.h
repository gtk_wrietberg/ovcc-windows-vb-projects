
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#if !defined(AFX_ACCOUNTLISTDLG_H__59898718_D7AF_4CCF_90FD_6541D24C23AC__INCLUDED_)
#define AFX_ACCOUNTLISTDLG_H__59898718_D7AF_4CCF_90FD_6541D24C23AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AccountListDlg.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CAccountListDlg ダイアログ
#include "LISTCTRLEX.h"

class CAccountListDlg : public CDialog
{
// コンストラクション
public:
	CAccountListDlg(CWnd* pParent = NULL);   // 標準のコンストラクタ

// ダイアログ データ
	//{{AFX_DATA(CAccountListDlg)
	enum { IDD = IDD_ACCOUNT_LIST };
	CListCtrlEx	m_list;
	//}}AFX_DATA

public:
	CString m_strRetAccountName;
	void PopulateTrusteeList(LPCTSTR szComputerName);
	int AddTrusteeToList(LPCTSTR szComputerName, LPCTSTR szTrustName, BOOL fGroup);
	void QueryTrusteeDetail(LPCTSTR szComputerName, LPCTSTR szTrustName, BOOL fGroup, int nIndex);
// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CAccountListDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CAccountListDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_ACCOUNTLISTDLG_H__59898718_D7AF_4CCF_90FD_6541D24C23AC__INCLUDED_)
