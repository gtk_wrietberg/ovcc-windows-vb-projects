# Microsoft Developer Studio Project File - Name="ZAccessMan" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=ZAccessMan - Win32 UnicodeDebug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ZAccessMan.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ZAccessMan.mak" CFG="ZAccessMan - Win32 UnicodeDebug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ZAccessMan - Win32 UnicodeDebug" (based on "Win32 (x86) Application")
!MESSAGE "ZAccessMan - Win32 UnicodeRelease" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ZAccessMan - Win32 UnicodeDebug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "UnicodeDebug"
# PROP BASE Intermediate_Dir "UnicodeDebug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "UnicodeDebug"
# PROP Intermediate_Dir "UnicodeDebug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_UNICODE" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x411 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "ZAccessMan - Win32 UnicodeRelease"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "UnicodeRelease"
# PROP BASE Intermediate_Dir "UnicodeRelease"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "UnicodeRelease"
# PROP Intermediate_Dir "UnicodeRelease"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_UNICODE" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x411 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x411 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /entry:"wWinMainCRTStartup" /subsystem:windows /machine:I386

!ENDIF 

# Begin Target

# Name "ZAccessMan - Win32 UnicodeDebug"
# Name "ZAccessMan - Win32 UnicodeRelease"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AccessData.cpp
# End Source File
# Begin Source File

SOURCE=.\AccountListDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ACLDumpDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\AUTOBUF.CPP
# End Source File
# Begin Source File

SOURCE=.\HyperLink.cpp
# End Source File
# Begin Source File

SOURCE=.\LISTCTRLEX.CPP
# End Source File
# Begin Source File

SOURCE=.\SCMCTRL.cpp
# End Source File
# Begin Source File

SOURCE=.\SECINFO.cpp
# End Source File
# Begin Source File

SOURCE=.\SERVICECTRL.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD BASE CPP /Yc"stdafx.h"
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\xLog.cpp
# End Source File
# Begin Source File

SOURCE=.\xMisc.cpp
# End Source File
# Begin Source File

SOURCE=.\z.cpp
# End Source File
# Begin Source File

SOURCE=.\ZAccessMan.cpp
# End Source File
# Begin Source File

SOURCE=.\ZAccessMan.rc
# End Source File
# Begin Source File

SOURCE=.\ZAccessManDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\zToolTipCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ZTreeCtrl.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AccessData.h
# End Source File
# Begin Source File

SOURCE=.\AccountListDlg.h
# End Source File
# Begin Source File

SOURCE=.\ACLDumpDlg.h
# End Source File
# Begin Source File

SOURCE=.\AUTOBUF.H
# End Source File
# Begin Source File

SOURCE=.\CMNHDR.H
# End Source File
# Begin Source File

SOURCE=.\CTRLEXT.INL
# End Source File
# Begin Source File

SOURCE=.\HyperLink.h
# End Source File
# Begin Source File

SOURCE=.\LISTCTRLEX.H
# End Source File
# Begin Source File

SOURCE=.\PRINTBUF.H
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SCMCTRL.H
# End Source File
# Begin Source File

SOURCE=.\SECINFO.H
# End Source File
# Begin Source File

SOURCE=.\SERVICECTRL.H
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\xLog.h
# End Source File
# Begin Source File

SOURCE=.\xMisc.h
# End Source File
# Begin Source File

SOURCE=.\z.h
# End Source File
# Begin Source File

SOURCE=.\ZAccessMan.h
# End Source File
# Begin Source File

SOURCE=.\ZAccessManDlg.h
# End Source File
# Begin Source File

SOURCE=.\zToolTipCtrl.h
# End Source File
# Begin Source File

SOURCE=.\ZTreeCtrl.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\account.bmp
# End Source File
# Begin Source File

SOURCE=.\res\acl.bmp
# End Source File
# Begin Source File

SOURCE=.\res\add.ico
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\delete.ico
# End Source File
# Begin Source File

SOURCE=.\res\mask2mas.ico
# End Source File
# Begin Source File

SOURCE=.\res\peek.ico
# End Source File
# Begin Source File

SOURCE=.\res\refresh.ico
# End Source File
# Begin Source File

SOURCE=.\res\reg.bmp
# End Source File
# Begin Source File

SOURCE=.\res\reg1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ZAccessMan.ico
# End Source File
# Begin Source File

SOURCE=.\res\ZAccessMan.rc2
# End Source File
# End Group
# End Target
# End Project
