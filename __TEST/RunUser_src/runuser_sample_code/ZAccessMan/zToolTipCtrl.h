
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#ifndef _INSIDE_VISUAL_CPP_ZTOOLTIP
#define _INSIDE_VISUAL_CPP_ZTOOLTIP  
//if you not define, you will have trouble

class CZToolTipCtrl : public CToolTipCtrl
{
public:
    BOOL AddWindowTool (CWnd*, LPCTSTR);
	BOOL DelWindowTool (CWnd*);
    BOOL AddRectTool (CWnd*, LPCTSTR, LPCRECT, UINT);
};

#endif // _INSIDE_VISUAL_CPP_ZTOOLTIP