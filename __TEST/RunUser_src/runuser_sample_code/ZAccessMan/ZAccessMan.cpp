
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
// ZAccessMan.cpp : アプリケーション用クラスの定義を行います。
//

#include "stdafx.h"
#include "ZAccessMan.h"
#include "ZAccessManDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CZAccessManApp

BEGIN_MESSAGE_MAP(CZAccessManApp, CWinApp)
	//{{AFX_MSG_MAP(CZAccessManApp)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
		//        この位置に生成されるコードを編集しないでください。
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CZAccessManApp クラスの構築

CZAccessManApp::CZAccessManApp()
{
	// TODO: この位置に構築用のコードを追加してください。
	// ここに InitInstance 中の重要な初期化処理をすべて記述してください。
}

/////////////////////////////////////////////////////////////////////////////
// 唯一の CZAccessManApp オブジェクト

CZAccessManApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CZAccessManApp クラスの初期化

BOOL CZAccessManApp::InitInstance()
{
	LPTSTR CurFilename = new TCHAR[MAX_PATH];
	DWORD nSize = 1024;
	::GetModuleFileName(NULL,CurFilename,nSize);
	m_destDir = CurFilename;
	int pos;
	pos = m_destDir.ReverseFind(TCHAR('\\'));
	CString leftPart; 
	leftPart = m_destDir.Left(pos);
	m_destDir = leftPart;
	delete CurFilename;

	AfxEnableControlContainer();

	// 標準的な初期化処理
	// もしこれらの機能を使用せず、実行ファイルのサイズを小さくしたけ
	//  れば以下の特定の初期化ルーチンの中から不必要なものを削除して
	//  ください。

#ifdef _AFXDLL
	Enable3dControls();			// 共有 DLL 内で MFC を使う場合はここをコールしてください。
#else
	Enable3dControlsStatic();	// MFC と静的にリンクする場合はここをコールしてください。
#endif

	CZAccessManDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: ダイアログが <OK> で消された時のコードを
		//       記述してください。
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: ダイアログが <ｷｬﾝｾﾙ> で消された時のコードを
		//       記述してください。
	}

	// ダイアログが閉じられてからアプリケーションのメッセージ ポンプを開始するよりは、
	// アプリケーションを終了するために FALSE を返してください。
	return FALSE;
}
