//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ZAccessMan.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_ZACCESSMAN_DIALOG           102
#define IDD_ACL_DUMP                    103
#define IDD_ACCOUNT_LIST                104
#define IDR_MAINFRAME                   128
#define IDI_DELETE                      129
#define IDI_ADD                         130
#define IDB_REG                         131
#define IDI_PEEK                        131
#define IDI_REFRESH                     132
#define IDI_MASK2MASK                   133
#define IDB_IMAGE                       147
#define IDB_ACL                         148
#define IDB_ACCOUNT                     149
#define IDC_TYPE                        1000
#define IDR_NAME                        1001
#define IDC_COMBO_OWNER_SID_TYPE        1001
#define IDR_HANDLE                      1002
#define IDC_COMBO_GROUP_SID_TYPE        1002
#define IDE_NAME                        1003
#define IDC_COMBO_NEW_ACE_TYPE          1003
#define IDE_PID                         1004
#define IDE_HANDLE                      1005
#define IDB_EDIT                        1006
#define IDE_USAGE                       1007
#define IDC_BINARY                      1008
#define IDC_BUTTON_OPEN                 1009
#define IDC_LIST                        1010
#define IDC_TREE                        1011
#define IDC_BUTTON_X                    1012
#define IDC_NAME                        1013
#define IDC_HANDLE                      1014
#define IDC_OWNER_SID                   1015
#define IDC_OWNER_NAME                  1016
#define IDC_GROUP                       1017
#define IDC_GROUP_NAME                  1017
#define IDC_GROUP_SID                   1018
#define IDC_EDIT_NEW_ACE_USER_NAME      1019
#define IDC_LIST_DACL                   1020
#define IDC_LIST_SACL                   1021
#define IDC_LINK_TDU                    1022
#define IDC_RADIO_DACL                  1022
#define IDC_LINK_LAB                    1023
#define IDC_RADIO_SACL                  1023
#define IDC_BANNER                      1023
#define IDC_LINK_CODEGURU               1024
#define IDC_ACE_ADD                     1024
#define IDC_DISK_INFO                   1025
#define IDC_ACE_DELETE                  1025
#define IDC_EDIT_NEW_ACE_DOMAIN_NAME    1026
#define IDC_EDIT_NEW_ACE_COMPUTER_NAME  1026
#define IDC_ACE_PEEK_ACCOUNT            1027
#define IDC_EDIT_NEW_ACE_USER_SID       1028
#define IDC_LIST_NEW_ACE_MASK1          1029
#define IDC_LIST_NEW_ACE_MASK2          1030
#define IDC_LIST_NEW_ACE_FLAG           1031
#define IDC_NEW_ACE_NEW_OWNER           1032
#define IDC_NEW_ACE_NEW_GROUP           1033
#define IDC_REFRESH                     1034
#define IDC_MASK2_MASK1                 1035
#define IDC_USER                        1035
#define IDC_PASSWORD                    1036
#define IDC_BUTTON_LOG                  1037
#define IDC_CHECK_QUIT_AFTER_LOG        1038

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1039
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
