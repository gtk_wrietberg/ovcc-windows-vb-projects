
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

#include "StdAfx.h"
#include "CoreCode.h"
#include "RunAsEx.h"

//#include <userenv.h> //Use dynamic link

#include "ntdll.h" //for ZwCreateToken

#define EvtLogName _T("RunAsEx")
#define NoMessageBox

//handler for err reporting
void err_handler(LPCTSTR szLocation, LPCTSTR szInfo)
{
//0: No Log
//1: Log to a file
//2: Log to EventLog
#ifdef NoMessageBox
	if(::g_dwLogOption == 1)
	{
		::ReportErrExToFile(szLocation, szInfo);
	}
	else if(::g_dwLogOption == 2)
	{
		::ReportErrExToEvtLog(szLocation, szInfo);
	}
#else
    ::ReportErrEx(szInfo);
#endif	
}

//Caller LocalFree
LPVOID GetFromToken(HANDLE hToken, TOKEN_INFORMATION_CLASS tic)
{
	DWORD dw;
	BOOL bRet = FALSE;
	LPVOID lpData = NULL;
	__try
	{
		bRet = GetTokenInformation(hToken, tic, 0, 0, &dw);
        if(bRet == FALSE && GetLastError() != ERROR_INSUFFICIENT_BUFFER) 
		{
			err_handler(EvtLogName, _T("GetFromToken GetTokenInformation Size Failed"));
			return NULL;
		}
	
		lpData = (LPVOID)LocalAlloc(LPTR, dw);
	    bRet = GetTokenInformation(hToken, tic, lpData, dw, &dw);
		if(!bRet)
		{
			err_handler(EvtLogName, _T("GetFromToken GetTokenInformation Failed"));
		}
	}
	__finally
	{
		if(!bRet)
		{
			if(lpData) ::LocalFree(lpData);
			lpData = NULL;
		}
		return lpData;
	}
}

//Caller FreeSID!!!
PSID Name2SID(LPCTSTR pszUserName, LPCTSTR pszDomainName)
{
    SID_NAME_USE sidUsage;
    DWORD cbSid = 0;
	DWORD cchReferencedDomainName = MAX_PATH;
	TCHAR ReferencedDomainName[MAX_PATH];
	BOOL bRet = ::LookupAccountName(pszDomainName, pszUserName, NULL, &cbSid,
		ReferencedDomainName, &cchReferencedDomainName, &sidUsage);
    if(!bRet && (GetLastError() != ERROR_INSUFFICIENT_BUFFER))
	{
		err_handler(EvtLogName, _T("Name2SID LookupAccountName Size Failed"));
		return NULL;
	}
	LPVOID lpSID = ::LocalAlloc(LPTR, cbSid);
	bRet = ::LookupAccountName(pszDomainName, pszUserName, lpSID, &cbSid,
		ReferencedDomainName, &cchReferencedDomainName, &sidUsage);
	if(!bRet)
	{
        err_handler(EvtLogName, _T("Name2SID LookupAccountName Failed"));
		::LocalFree(lpSID);
		return NULL;
	}
	return lpSID;
}

//Call ZwCreateToken Direcly
//When bCopyFromCallerToken is false, it may be very time consuming to check all 
//domain and local user group to decide which one the user belowngs to ... In some
//adverse cases, it takes minutes where bCopyFromCallerToken = TRUE come for a resue
//      
BOOL CreateTokenFromCaller(HANDLE &hToken, LPCTSTR pszUserName, LPCTSTR pszDomainName, 
						 char* szTokenSource) 
{
	hToken = NULL;
	BOOL bRet = FALSE;

    if(!EnablePrivilege(SE_CREATE_TOKEN_NAME, TRUE))
	//if(!EnablePrivilege(L"SeCreateTokenPrivilege", TRUE))
	{
		err_handler(EvtLogName, _T("CreateTokenFromCaller EnablePrivilege SE_CREATE_TOKEN_NAME Failed"));
		return FALSE;
	}

	//open caller's process token
    hToken = NULL;
    PSID lpSidOwner = NULL;
    //TOKEN_USER userToken;
	LUID luid;
	//TOKEN_SOURCE sourceToken;
	int len = 0;
	//LUID authid;
	PTOKEN_STATISTICS lpStatsToken;
    //NT::SECURITY_QUALITY_OF_SERVICE sqos;
	//NT::OBJECT_ATTRIBUTES oa;
	NTSTATUS ntStatus;
	HANDLE hTokenCaller = NULL;

	PTOKEN_GROUPS lpGroupToken = NULL;           //(GetFromToken(hTokenCaller, TokenGroups)),
    PTOKEN_PRIVILEGES lpPrivToken = NULL; //(GetFromToken(hTokenCaller, TokenPrivileges)),
    PTOKEN_OWNER lpOwnerToken = NULL;     //(GetFromToken(hTokenCaller, TokenOwner)),
    PTOKEN_PRIMARY_GROUP lpPrimGroupToken = NULL; //(GetFromToken(hTokenCaller, TokenPrimaryGroup)), 
    PTOKEN_DEFAULT_DACL  lpDaclToken = NULL;      //(GetFromToken(hTokenCaller, TokenDefaultDacl)),
	__try
	{
		bRet = OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY | TOKEN_QUERY_SOURCE,
                     &hTokenCaller);
	    if(!bRet) 
		{
		    err_handler(EvtLogName, _T("CreateTokenFromCaller OpenProcessToken Failed"));
		    __leave;
		}

	    //Create Token Owner
	    //PSID lpSidOwner;
		if(pszUserName == NULL || ::lstrlen(pszUserName) == 0) //LocalSystem
		{
			SID_IDENTIFIER_AUTHORITY nt = SECURITY_NT_AUTHORITY;
            bRet = AllocateAndInitializeSid(&nt, 1, SECURITY_LOCAL_SYSTEM_RID,
                             0, 0, 0, 0, 0, 0, 0, &lpSidOwner);
		    if(!bRet)
			{
                err_handler(EvtLogName, _T("CreateTokenFromCaller AllocateAndInitializeSid Failed"));
				__leave;
        	}
		}
		else
		{
			lpSidOwner = Name2SID(pszUserName, 
				((pszDomainName == NULL) || ::lstrlen(pszDomainName)) ? NULL : pszDomainName);
			if(!lpSidOwner)
			{
                err_handler(EvtLogName, _T("CreateTokenFromCaller Name2SID Failed"));
				__leave;
			}
		}

        TOKEN_USER userToken = {{lpSidOwner, 0}};
    
        bRet = AllocateLocallyUniqueId(&luid); //this luid only unique in this session
	    if(!bRet)
		{
            err_handler(EvtLogName, _T("CreateTokenFromCaller AllocateLocallyUniqueId Failed"));
			__leave;
		}

        TOKEN_SOURCE sourceToken = {{'*', '*', '*', '*', '*', '*', '*', '*'},
                           {luid.LowPart, luid.HighPart}};

		len = ::strlen(szTokenSource);
		if(len > 8) len = 8;
		if(len > 0)
			::CopyMemory((LPBYTE)sourceToken.SourceName, (LPBYTE)szTokenSource, len); 
        
	//
    // Allocate the System Luid.  The first 1000 LUIDs are reserved.
    // Use #999 here (0x3E7 = 999)
    //
    //#define SYSTEM_LUID                     { 0x3E7, 0x0 }
    //#define ANONYMOUS_LOGON_LUID            { 0x3e6, 0x0 }
    //#define LOCALSERVICE_LUID               { 0x3e5, 0x0 }
    //#define NETWORKSERVICE_LUID             { 0x3e4, 0x0 }

		LUID authid = SYSTEM_LUID; //???
	//typedef struct _TOKEN_STATISTICS {
	//	LUID TokenId;
	//	LUID AuthenticationId;  
	//	LARGE_INTEGER ExpirationTime;
	//	TOKEN_TYPE TokenType; 
	//	SECURITY_IMPERSONATION_LEVEL ImpersonationLevel; 
	//	DWORD DynamicCharged;
	//	DWORD DynamicAvailable;
	//	DWORD GroupCount;
	//	DWORD PrivilegeCount;
	//	LUID ModifiedId;
	//} TOKEN_STATISTICS, *PTOKEN_STATISTICS;

        lpStatsToken = PTOKEN_STATISTICS(GetFromToken(hTokenCaller, TokenStatistics));

	//typedef enum _SECURITY_IMPERSONATION_LEVEL {
    //SecurityAnonymous,
    //SecurityIdentification,
    //SecurityImpersonation,
    //SecurityDelegation
    //} SECURITY_IMPERSONATION_LEVEL, * PSECURITY_IMPERSONATION_LEVEL;

	//typedef struct _SECURITY_QUALITY_OF_SERVICE {
    //DWORD Length;
    //SECURITY_IMPERSONATION_LEVEL ImpersonationLevel;
    //SECURITY_CONTEXT_TRACKING_MODE ContextTrackingMode;
    //BOOLEAN EffectiveOnly;
    //} SECURITY_QUALITY_OF_SERVICE, * PSECURITY_QUALITY_OF_SERVICE;

	//#define SECURITY_DYNAMIC_TRACKING      (TRUE)
    //#define SECURITY_STATIC_TRACKING       (FALSE)

    //typedef BOOLEAN SECURITY_CONTEXT_TRACKING_MODE,
    //              * PSECURITY_CONTEXT_TRACKING_MODE;

        NT::SECURITY_QUALITY_OF_SERVICE sqos = {sizeof(sqos), NT::SecurityAnonymous, SECURITY_STATIC_TRACKING, FALSE};

        NT::OBJECT_ATTRIBUTES oa = {sizeof(oa), 0, 0, 0, 0, &sqos};

		lpGroupToken = (PTOKEN_GROUPS)(GetFromToken(hTokenCaller, TokenGroups));
        lpPrivToken = (PTOKEN_PRIVILEGES)(GetFromToken(hTokenCaller, TokenPrivileges));

        //this only works in Win2k in WinXP+ it is GP error
		//lpOwnerToken = (PTOKEN_OWNER)(GetFromToken(hTokenCaller, TokenOwner));
        
		//this works in WinXP
        lpOwnerToken = (PTOKEN_OWNER)LocalAlloc(LPTR, sizeof(PSID));
		if(pszUserName == NULL || ::lstrlen(pszUserName) == 0)
		{
			lpOwnerToken->Owner = ::GetLocalSystemSID();
		}
		else
		{
			lpOwnerToken->Owner = ::Name2SID(pszUserName, 
				((pszDomainName == NULL) || ::lstrlen(pszDomainName) == 0) ? NULL : pszDomainName);
		}
               
		lpPrimGroupToken = (PTOKEN_PRIMARY_GROUP)(GetFromToken(hTokenCaller, TokenPrimaryGroup)); 
        lpDaclToken = (PTOKEN_DEFAULT_DACL)(GetFromToken(hTokenCaller, TokenDefaultDacl));

        ntStatus = NT::ZwCreateToken(&hToken, TOKEN_ALL_ACCESS, &oa, TokenPrimary, 
                NT::PLUID(&authid), // NT::PLUID(&stats->AuthenticationId),
                NT::PLARGE_INTEGER(&lpStatsToken->ExpirationTime),
                &userToken,
                lpGroupToken, lpPrivToken, lpOwnerToken, lpPrimGroupToken, lpDaclToken,
//                PTOKEN_GROUPS(GetFromToken(hTokenCaller, TokenGroups)),
//                PTOKEN_PRIVILEGES(GetFromToken(hTokenCaller, TokenPrivileges)),
//                PTOKEN_OWNER(GetFromToken(hTokenCaller, TokenOwner)),
//                PTOKEN_PRIMARY_GROUP(GetFromToken(hTokenCaller, TokenPrimaryGroup)), 
//                PTOKEN_DEFAULT_DACL(GetFromToken(hTokenCaller, TokenDefaultDacl)),
                &sourceToken);
		//You may get 0xC0000061 which is STATUS_PRIVILEDGE_NOT_HELD, 
		//If so, try to GRANT SeCreateTokenPrivilege and relog once...
	    //STATUS_PRIVILEDGE_NOT_HELD; 
	    //;
		//0xc000005a invalid owner
		if(ntStatus == STATUS_SUCCESS)
			bRet = TRUE;
		else
		{
			err_handler(EvtLogName, _T("CreateTokenFromCaller ZwCreateToken Failed"));
		}
	}
	__finally
	{
		if(hTokenCaller)
			::CloseHandle(hTokenCaller);
		if(lpSidOwner)
			::FreeSid(lpSidOwner);

        if(lpStatsToken) ::LocalFree(lpStatsToken);
		if(lpGroupToken) ::LocalFree(lpGroupToken);
		if(lpPrivToken) ::LocalFree(lpPrivToken);
		if(lpOwnerToken)
		{
			if(lpOwnerToken->Owner)
				::FreeSid(lpOwnerToken->Owner);
			::LocalFree(lpOwnerToken);
		}
		if(lpPrimGroupToken) ::LocalFree(lpPrimGroupToken);
		if(lpDaclToken) ::LocalFree(lpDaclToken);

		if(!bRet)
		{
			err_handler(EvtLogName, _T("CreateTokenFromCaller expection caught in __finally"));
		}
    	return bRet;
	}
}

//Caller FreeSID
//SID:		S-1-5-18
//Use:		User SID
//Name:		SYSTEM
//Domain Name:	NT AUTHORITY
PSID GetLocalSystemSID()
{
	SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_NT_AUTHORITY;
	PSID psid = NULL;
	BOOL bRet = AllocateAndInitializeSid( &sidAuth, 1,
			SECURITY_LOCAL_SYSTEM_RID,
			0, 0, 0, 0, 0, 0, 0, &psid );
	if(!bRet) 
		return NULL;
	return psid;
}

//Caller FreeSID
//SID:		S-1-5-32-544
//Use:		Alias SID
//Name:		Administrators
//Domain Name:	BUILTIN
PSID GetAliasAdministratorsSID()
{	
	SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_NT_AUTHORITY;
	PSID psid = NULL;
	BOOL bRet = AllocateAndInitializeSid( &sidAuth, 2,
			SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS,
			0, 0, 0, 0, 0, 0, &psid );
	if(!bRet) 
		return NULL;
	return psid;
}


//Caller FreeSID
//SID:		S-1-1-0
//Use:		Well-Known Group SID
//Name:		Everyone
//Domain Name:	
PSID GetEveryoneSID()
{
	SID_IDENTIFIER_AUTHORITY ntauth = SECURITY_WORLD_SID_AUTHORITY;
	PSID psid = NULL;
	BOOL bRet = AllocateAndInitializeSid( &ntauth, 1,
			0,
			0, 0, 0, 0, 0, 0, 0, &psid );
	if(!bRet) 
		return NULL;
	return psid;
}

//Caller FreeSID
//SID:		S-1-5-11
//Use:		Well-Known Group SID
//Name:		Authenticated Users
//Domain Name:	NT AUTHORITY
PSID GetAuthenticatedUsersSID()
{
	SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_NT_AUTHORITY;
	PSID psid = NULL;
	BOOL bRet = AllocateAndInitializeSid( &sidAuth, 1,
			SECURITY_AUTHENTICATED_USER_RID,
			0, 0, 0, 0, 0, 0, 0, &psid );
	if(!bRet) 
		return NULL;
	return psid;
}

//Caller FreeSID
//SID:		S-1-5-4
//Use:		Well-Known Group SID
//Name:		INTERACTIVE
//Domain Name:	NT AUTHORITY
PSID GetInteractiveSID()
{
	SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_NT_AUTHORITY;
	PSID psid = NULL;
	BOOL bRet = AllocateAndInitializeSid( &sidAuth, 1,
			SECURITY_INTERACTIVE_RID,
			0, 0, 0, 0, 0, 0, 0, &psid );
	if(!bRet) 
		return NULL;
	return psid;
}

//Caller FreeSID
//SID:		S-1-2-0
//Use:		Well-Known Group SID
//Name:		LOCAL
//Domain Name:	
PSID GetLocalSID()
{
	SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_LOCAL_SID_AUTHORITY;
	PSID psid = NULL;
	BOOL bRet = AllocateAndInitializeSid( &sidAuth, 1,
			0,
			0, 0, 0, 0, 0, 0, 0, &psid );
	if(!bRet) 
		return NULL;
	return psid;
}

//Caller FreeSID
//SID:		S-1-5-6
//Use:		Well-Known Group SID
//Name:		SERVICE
//Domain Name:	NT AUTHORITY
PSID GetServiceSID()
{
	SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_NT_AUTHORITY;
	PSID psid = NULL;
	BOOL bRet = AllocateAndInitializeSid( &sidAuth, 1,
			SECURITY_SERVICE_RID,
			0, 0, 0, 0, 0, 0, 0, &psid );
	if(!bRet) 
		return NULL;
	return psid;
}

//Caller FreeSID
//SID:		S-1-5-3
//Use:		Well-Known Group SID
//Name:		BATCH
//Domain Name:	NT AUTHORITY
PSID GetBatchSID()
{
	SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_NT_AUTHORITY;
	PSID psid = NULL;
	BOOL bRet = AllocateAndInitializeSid( &sidAuth, 1,
			SECURITY_BATCH_RID,
			0, 0, 0, 0, 0, 0, 0, &psid );
	if(!bRet) 
		return NULL;
	return psid;
}

PSID* QueryLocalGroupSIDs(LPCTSTR pszUserName)
{
	//Suppose Local Groups' Max Number 64
#define MAX_LOCAL_GROUP_NUMBER 64
	LPVOID lpData = (LPVOID)LocalAlloc(LPTR, sizeof(PSID) * MAX_LOCAL_GROUP_NUMBER);
	LPVOID lpData2 = NULL;

    NET_API_STATUS netStatus;
    LOCALGROUP_USERS_INFO_0* pinfoGroups = NULL;
	ULONG  lTotal = 0;
    ULONG  lReturned = 0;
    ULONG  lIndex = 0;
	DWORD  dwSID = 0; //total sid fetched
	BOOL   bSuccess = FALSE;
	__try
	{
		netStatus = NetUserGetLocalGroups(NULL, pszUserName,
				0, LG_INCLUDE_INDIRECT, (PBYTE*)&pinfoGroups, 
                MAX_PREFERRED_LENGTH,&lReturned, &lTotal);
        if(netStatus == NERR_Success)
		{
			for (lIndex = 0; lIndex < lReturned; lIndex++) 
			{
				//pinfoGroups[lIndex].lgrui0_name;
				PSID lpsid = ::Name2SID(pinfoGroups[lIndex].lgrui0_name, NULL);
				if(lpsid == NULL) continue;
				PSID* pSIDArray = (PSID*)lpData;
				pSIDArray[dwSID] = lpsid;
				dwSID++;
            }
			NetApiBufferFree(pinfoGroups);
			pinfoGroups = NULL;
		}
		else if(netStatus == ERROR_MORE_DATA)
		{
			NetApiBufferFree(pinfoGroups);
			pinfoGroups = NULL;
			err_handler(EvtLogName, _T("QueryLocalGroupSIDs NetUserGetLocalGroups ERROR_MORE_DATA"));
			__leave;
		}
		else
		{
			err_handler(EvtLogName, _T("QueryLocalGroupSIDs NetUserGetLocalGroups Unknown Error"));
			__leave;
		}
		if(dwSID == 0) __leave;
		lpData2 = (LPVOID)LocalAlloc(LPTR, sizeof(PSID) * (dwSID+1));
		if(lpData2 == NULL) __leave;
		ZeroMemory(lpData2, sizeof(PSID) * (dwSID + 1));
		CopyMemory(lpData2, lpData, sizeof(PSID) * dwSID);
		bSuccess = TRUE;
	}
	__finally
	{
		if(pinfoGroups) NetApiBufferFree(pinfoGroups);
		if(lpData) LocalFree(lpData);
		if(!bSuccess && lpData2) LocalFree(lpData2);
        if(!bSuccess)
		{
			err_handler(EvtLogName, _T("QueryLocalGroupSIDs exception caught in __finally"));
			return NULL;
		}
		else
			return (PSID*)lpData2;
	}
}

PSID* QueryNetGroupSIDs(LPCTSTR pszUserName, LPCTSTR pszDomainName)
{
	//Suppose Local Groups' Max Number 64
#define MAX_NET_GROUP_NUMBER 64
	LPVOID lpData = (LPVOID)LocalAlloc(LPTR, sizeof(PSID) * MAX_LOCAL_GROUP_NUMBER);
	LPVOID lpData2 = NULL;

    NET_API_STATUS netStatus;
    GROUP_USERS_INFO_0* pinfoGroups = NULL;
	ULONG  lTotal = 0;
    ULONG  lReturned = 0;
    ULONG  lIndex = 0;
	DWORD  dwSID = 0; //total sid fetched
	BOOL   bSuccess = FALSE;
	__try
	{
		netStatus = NetUserGetGroups(pszDomainName, pszUserName,
			0, (PBYTE*)&pinfoGroups, 
            MAX_PREFERRED_LENGTH,&lReturned, &lTotal);
        if(netStatus == NERR_Success)
		{
			for (lIndex = 0; lIndex < lReturned; lIndex++) 
			{
				//pinfoGroups[lIndex].lgrui0_name;
				PSID lpsid = ::Name2SID(pinfoGroups[lIndex].grui0_name, pszDomainName);
				if(lpsid == NULL) continue;
				PSID* pSIDArray = (PSID*)lpData;
				pSIDArray[dwSID] = lpsid;
				dwSID++;
            }
			NetApiBufferFree(pinfoGroups);
			pinfoGroups = NULL;
		}
		else if(netStatus == ERROR_MORE_DATA)
		{
			NetApiBufferFree(pinfoGroups);
			pinfoGroups = NULL;
			err_handler(EvtLogName, _T("QueryNetGroupSIDs NetUserGetGroups ERROR_MORE_DATA"));
			__leave;
		}
		else
		{
			err_handler(EvtLogName, _T("QueryNetGroupSIDs NetUserGetGroups Unknown error"));
			__leave;
		}
		if(dwSID == 0) __leave;
		lpData2 = (LPVOID)LocalAlloc(LPTR, sizeof(PSID) * (dwSID + 1));
		if(lpData2 == NULL) __leave;
		ZeroMemory(lpData2, sizeof(PSID) * (dwSID + 1));
		CopyMemory(lpData2, lpData, sizeof(PSID) * dwSID);		
		bSuccess = TRUE;
	}
	__finally
	{
		if(pinfoGroups) NetApiBufferFree(pinfoGroups);
		if(lpData) LocalFree(lpData);
		if(!bSuccess && lpData2) LocalFree(lpData2);
        if(!bSuccess)
		{
			err_handler(EvtLogName, _T("QueryNetGroupSIDs exception caught in __finally"));
			return NULL;
		}
		else
			return (PSID*)lpData2;
	}
}

////////////////////////////////////////////////////////////////////////
//                                                                    //
//               NT Defined Privileges  WinNT.h                       //
//                                                                    //
////////////////////////////////////////////////////////////////////////
//#define SE_CREATE_TOKEN_NAME              TEXT("SeCreateTokenPrivilege")
//#define SE_ASSIGNPRIMARYTOKEN_NAME        TEXT("SeAssignPrimaryTokenPrivilege")
//#define SE_LOCK_MEMORY_NAME               TEXT("SeLockMemoryPrivilege")
//#define SE_INCREASE_QUOTA_NAME            TEXT("SeIncreaseQuotaPrivilege")
//#define SE_UNSOLICITED_INPUT_NAME         TEXT("SeUnsolicitedInputPrivilege")
//#define SE_MACHINE_ACCOUNT_NAME           TEXT("SeMachineAccountPrivilege")
//#define SE_TCB_NAME                       TEXT("SeTcbPrivilege")
//#define SE_SECURITY_NAME                  TEXT("SeSecurityPrivilege")
//#define SE_TAKE_OWNERSHIP_NAME            TEXT("SeTakeOwnershipPrivilege")
//#define SE_LOAD_DRIVER_NAME               TEXT("SeLoadDriverPrivilege")
//#define SE_SYSTEM_PROFILE_NAME            TEXT("SeSystemProfilePrivilege")
//#define SE_SYSTEMTIME_NAME                TEXT("SeSystemtimePrivilege")
//#define SE_PROF_SINGLE_PROCESS_NAME       TEXT("SeProfileSingleProcessPrivilege")
//#define SE_INC_BASE_PRIORITY_NAME         TEXT("SeIncreaseBasePriorityPrivilege")
//#define SE_CREATE_PAGEFILE_NAME           TEXT("SeCreatePagefilePrivilege")
//#define SE_CREATE_PERMANENT_NAME          TEXT("SeCreatePermanentPrivilege")
//#define SE_BACKUP_NAME                    TEXT("SeBackupPrivilege")
//#define SE_RESTORE_NAME                   TEXT("SeRestorePrivilege")
//#define SE_SHUTDOWN_NAME                  TEXT("SeShutdownPrivilege")
//#define SE_DEBUG_NAME                     TEXT("SeDebugPrivilege")
//#define SE_AUDIT_NAME                     TEXT("SeAuditPrivilege")
//#define SE_SYSTEM_ENVIRONMENT_NAME        TEXT("SeSystemEnvironmentPrivilege")
//#define SE_CHANGE_NOTIFY_NAME             TEXT("SeChangeNotifyPrivilege")
//#define SE_REMOTE_SHUTDOWN_NAME           TEXT("SeRemoteShutdownPrivilege")
//#define SE_UNDOCK_NAME                    TEXT("SeUndockPrivilege")
//#define SE_SYNC_AGENT_NAME                TEXT("SeSyncAgentPrivilege")
//#define SE_ENABLE_DELEGATION_NAME         TEXT("SeEnableDelegationPrivilege")
//#define SE_MANAGE_VOLUME_NAME             TEXT("SeManageVolumePrivilege")

//SeBatchLogonRight
//SE_BATCH_LOGON_NAME Log on as a batch job NTSecAPI.h 
//SeDenyBatchLogonRight
//SE_DENY_BATCH_LOGON_NAME Deny logon as a batch job NTSecAPI.h 
//SeDenyInteractiveLogonRight
//SE_DENY_INTERACTIVE_LOGON_NAME Deny logon locally NTSecAPI.h 
//SeDenyNetworkLogonRight
//SE_DENY_NETWORK_LOGON_NAME Deny access to this computer from the network NTSecAPI.h 
//SeDenyServiceLogonRight
//SE_DENY_SERVICE_LOGON_NAME Deny logon as a service NTSecAPI.h 
//SeInteractiveLogonRight
//SE_INTERACTIVE_LOGON_NAME Log on locally NTSecAPI.h 
//SeNetworkLogonRight
//SE_NETWORK_LOGON_NAME Access this computer from the network NTSecAPI.h 
//SeServiceLogonRight
//SE_SERVICE_LOGON_NAME Log on as a service NTSecAPI.h 

//Caller LocalFree Returned Memory
//bGrantEnableAll : TRUE All Priv and NT Rights are granted and enabled
//lpszPriv must have a ending NULL
PTOKEN_PRIVILEGES CreateTokenPriv(DWORD& dwPrivGranted, LPCTSTR* lpszPriv, BOOL bGrantEnableAll)
{
    LPCTSTR _lpszPriv[] = { 
		                    //////////////     Priviledge      /////////////////
		                    SE_CREATE_TOKEN_NAME, SE_ASSIGNPRIMARYTOKEN_NAME,
	                        SE_LOCK_MEMORY_NAME, SE_CREATE_TOKEN_NAME,
							SE_ASSIGNPRIMARYTOKEN_NAME, SE_LOCK_MEMORY_NAME,
							SE_INCREASE_QUOTA_NAME,       
							//SE_UNSOLICITED_INPUT_NAME, //failure to found in Win2k, XP
							//SE_MACHINE_ACCOUNT_NAME,  //leading to Zw failure in WinXP
							SE_TCB_NAME,
							SE_SECURITY_NAME, SE_TAKE_OWNERSHIP_NAME,
							SE_LOAD_DRIVER_NAME, SE_SYSTEM_PROFILE_NAME,
							SE_SYSTEMTIME_NAME, SE_PROF_SINGLE_PROCESS_NAME,
							SE_INC_BASE_PRIORITY_NAME, SE_CREATE_PAGEFILE_NAME,
							SE_CREATE_PERMANENT_NAME, SE_BACKUP_NAME,
							SE_RESTORE_NAME, SE_SHUTDOWN_NAME,
							SE_DEBUG_NAME, SE_AUDIT_NAME, 
							SE_SYSTEM_ENVIRONMENT_NAME, SE_CHANGE_NOTIFY_NAME,
							SE_REMOTE_SHUTDOWN_NAME, SE_UNDOCK_NAME,
							SE_SYNC_AGENT_NAME, //
							SE_ENABLE_DELEGATION_NAME, //
							//SE_MANAGE_VOLUME_NAME,  //failure to find on Win2k but ok when Xp+
							NULL
							////////////////     NT Rights      ////////////////
						    //SE_BATCH_LOGON_NAME, SE_DENY_BATCH_LOGON_NAME,
							//SE_DENY_INTERACTIVE_LOGON_NAME, SE_DENY_NETWORK_LOGON_NAME,
							//SE_DENY_SERVICE_LOGON_NAME, SE_INTERACTIVE_LOGON_NAME ,
							//SE_NETWORK_LOGON_NAME, SE_SERVICE_LOGON_NAME, NULL
	};
	LPCTSTR* pPriv = NULL;
	int dim = 0;
	if(bGrantEnableAll)
	{
		pPriv = _lpszPriv;
		while(pPriv[dim] != NULL)  dim++;
	}
	else
	{
		pPriv = lpszPriv;
		if(lpszPriv) while(pPriv[dim] != NULL)  dim++;
	}

	
	//int dim = sizeof(pPriv)/sizeof(pPriv[0]);
	//typedef struct _TOKEN_PRIVILEGES { 
	//	DWORD PrivilegeCount;
	//	LUID_AND_ATTRIBUTES Privileges[ANYSIZE_ARRAY];
	//} TOKEN_PRIVILEGES, *PTOKEN_PRIVILEGES;
	//typedef struct _LUID_AND_ATTRIBUTES {
	//	LUID Luid;  
	//	DWORD Attributes;
	//} LUID_AND_ATTRIBUTES, *PLUID_AND_ATTRIBUTES;
	DWORD dwSize = sizeof(DWORD) + dim * sizeof(LUID_AND_ATTRIBUTES);
	TOKEN_PRIVILEGES* lpPrivToken = NULL;
    BOOL bSuccess = FALSE;
	LPVOID lpData = NULL;
    LPVOID lpData2 = NULL;
	DWORD  dwIndex = 0; //real number of Priv
	__try
	{
		lpData = (LPVOID)LocalAlloc(LPTR, dwSize);
        lpPrivToken = (TOKEN_PRIVILEGES*)(lpData);
        lpPrivToken->PrivilegeCount = dim;

	    for(int i = 0; i < dim; i++)
		{
			LPCTSTR szPrivAdd = pPriv[i];
			LUID luid;
            if(!LookupPrivilegeValue(NULL, szPrivAdd, &luid)) 
			{
				// If the name is bogus...SE_UNSOLICITED_INPUT_NAME
                //__leave;
				continue;
				//::ReportErrEx(_T("%s"), pPriv[i]);
			}
			lpPrivToken->Privileges[dwIndex].Luid.HighPart = luid.HighPart;
			lpPrivToken->Privileges[dwIndex].Luid.LowPart  = luid.LowPart;
			lpPrivToken->Privileges[dwIndex].Attributes = SE_PRIVILEGE_ENABLED | SE_PRIVILEGE_ENABLED_BY_DEFAULT;
			dwIndex++;
		}
		if(dwIndex == (DWORD)dim)
		{
			dwPrivGranted = dim;
		    lpData2 = lpData;
			bSuccess = TRUE;
		}
		else
		{
			dwPrivGranted = dwIndex;
		    dwSize = sizeof(DWORD) + dwIndex * sizeof(LUID_AND_ATTRIBUTES);
			lpData2 = (LPVOID)LocalAlloc(LPTR, dwSize);
			if(!lpData2) __leave;
            CopyMemory(lpData2, lpData, dwSize);
            lpPrivToken = (TOKEN_PRIVILEGES*)(lpData2);
            lpPrivToken->PrivilegeCount = dwIndex;
			bSuccess = TRUE;
		}
	}
	__finally
	{		
		if(!bSuccess)
		{
			if(lpData) ::LocalFree(lpData);
			err_handler(EvtLogName, _T("CreateTokenPriv exception caught in __finally"));
			return NULL;
		}
		else
		{
			if(lpData != lpData2 && lpData)
				::LocalFree(lpData);
			return (TOKEN_PRIVILEGES*)lpData2;
		}
	}
}

PTOKEN_PRIVILEGES CreateTokenPrivFromUser(DWORD& dwPrivGranted, LPCTSTR pszUserName, LPCTSTR pszDomainName)
{
	dwPrivGranted = 0;
	if(pszUserName == NULL || ::lstrlen(pszUserName) == 0)
		return CreateTokenPriv(dwPrivGranted, NULL, TRUE);
	//check user's current holding priviledge
	PSID pSIDUser = NULL;
	BOOL bSuccess = FALSE;
	NTSTATUS ntStatus = 0;
	LSA_HANDLE hPolicy = NULL;

	//typedef struct _LSA_UNICODE_STRING {
    //   USHORT Length;
    //   USHORT MaximumLength;
    //   PWSTR  Buffer;
	//} LSA_UNICODE_STRING, *PLSA_UNICODE_STRING;

	//here Unicode only, but I am addicted to TCHAR already :=)
	LSA_UNICODE_STRING lsastrComputer;
	lsastrComputer.Buffer = NULL;
	LSA_OBJECT_ATTRIBUTES lsaOA = { 0 };
    lsaOA.Length = sizeof(lsaOA);
	PLSA_UNICODE_STRING  pustrPrivileges = NULL;
    ULONG                ulCount = 0;	
	LPVOID lpData = NULL; //will be cast to LPCTSTR*
	PTOKEN_PRIVILEGES lpRetPriv = NULL;
	__try
	{
#define MAX_POSSIBLE_PRIV_NUMBER 64
#define MAX_PRIV_NAME_LEN        40
		//max 64 Priv (in fact max only around 35), each with 40 char
        lpData = ::LocalAlloc(LPTR, sizeof(LPCTSTR) * MAX_POSSIBLE_PRIV_NUMBER + MAX_POSSIBLE_PRIV_NUMBER * MAX_PRIV_NAME_LEN * sizeof(TCHAR));
		if(!lpData) __leave;

		pSIDUser = ::Name2SID(pszUserName, pszDomainName);
		lsastrComputer.Buffer = (PWSTR)::LocalAlloc(LPTR, MAX_PATH * sizeof(TCHAR));
		if(!lsastrComputer.Buffer)
			__leave;
		ZeroMemory(lsastrComputer.Buffer, MAX_PATH * sizeof(TCHAR));
		lsastrComputer.MaximumLength = MAX_PATH * sizeof(TCHAR);
		if(pszDomainName && ::lstrlen(pszDomainName) > 0)
		{
			::lstrcpy((LPTSTR)lsastrComputer.Buffer, pszDomainName);
			lsastrComputer.Length = ::lstrlen(pszDomainName) * sizeof(TCHAR);
		}
		else
		{
			lsastrComputer.Length = 0;
		}
		        
        ntStatus = LsaOpenPolicy(
			(pszDomainName && ::lstrlen(pszDomainName) > 0) ? &lsastrComputer : NULL,
			&lsaOA,
			POLICY_VIEW_LOCAL_INFORMATION | POLICY_LOOKUP_NAMES, &hPolicy);
        ULONG lErr = LsaNtStatusToWinError(ntStatus);
		if (lErr != ERROR_SUCCESS) 
		{
			err_handler(EvtLogName, _T("CreateTokenPrivFromUser LsaOpenPolicy Failed"));
			__leave;		
		}

		ntStatus = LsaEnumerateAccountRights(hPolicy, pSIDUser,
           &pustrPrivileges, &ulCount);
        lErr = LsaNtStatusToWinError(ntStatus);
        if(ERROR_FILE_NOT_FOUND == lErr)
		{
			//not error here! just no priv
			lpRetPriv = ::CreateTokenPriv(dwPrivGranted, NULL, FALSE);
			if(!lpRetPriv)
			{
				err_handler(EvtLogName, _T("CreateTokenPrivFromUser LsaEnumerateAccountRights 0 Priv - CreateTokenPriv Failed"));
				__leave;
			}
		}
		else
		{
			if(ERROR_SUCCESS != lErr) 
			{
				err_handler(EvtLogName, _T("CreateTokenPrivFromUser LsaEnumerateAccountRights Failed"));
				__leave;
			}
    
		    ULONG nIndex2 = ulCount;
		    LPCTSTR* lpBase1 = (LPCTSTR*)lpData;
		    LPBYTE lpBase2 = (LPBYTE)lpData;
		    lpBase2 += sizeof(LPCTSTR) * MAX_POSSIBLE_PRIV_NUMBER;
		    LPTSTR lpBase3 = (LPTSTR)lpBase2;
            for(int i = 0; i < (int)nIndex2; i++)
			{
			    lpBase1[i] = lpBase3;
			    ::lstrcpyn(lpBase3, pustrPrivileges[i].Buffer, 
    			pustrPrivileges[i].Length + 1);
	    		lpBase3 += pustrPrivileges[i].Length + 1;
			}
			lpRetPriv = ::CreateTokenPriv(dwPrivGranted, (LPCTSTR*)lpData, FALSE);
			if(!lpRetPriv)
			{
				err_handler(EvtLogName, _T("CreateTokenPrivFromUser LsaEnumerateAccountRights x Priv - CreateTokenPriv Failed"));
				__leave;
			}

		}
		bSuccess = TRUE;
	}
	__finally
	{
		if(pSIDUser) ::FreeSid(pSIDUser);
		if(lsastrComputer.Buffer)
		{
			::LocalFree(lsastrComputer.Buffer);
			lsastrComputer.Buffer = NULL;
		}
		if(pustrPrivileges) LsaFreeMemory(pustrPrivileges);
		if(hPolicy != NULL) ::LsaClose(hPolicy);
		if(lpData) 	::LocalFree(lpData);
		if(bSuccess)
		{
			return (PTOKEN_PRIVILEGES)lpRetPriv;
		}
		else
		{
			err_handler(EvtLogName, _T("CreateTokenPrivFromUser exception caught in __finally"));
			return NULL;
		}
	}
}

//Caller LocalFree
PTOKEN_STATISTICS CreateTokenStatistics(
	LUID* lpTokenId,  //Optional        
	LUID* lpAuthenticationId, //Optional 
	LARGE_INTEGER* lpExpirationTime,  //Optional
	TOKEN_TYPE* lpTokenType,  //Primary
	SECURITY_IMPERSONATION_LEVEL* lpImpersonationLevel, //Only When ImpersonteToken
	DWORD* lpDynamicCharged, //Optional
	DWORD* lpDynamicAvailable, //Optional
	DWORD* lpGroupCount, //Mandatory
	DWORD* lpPrivilegeCount, //Mandatory
	LUID* lpModifiedId //Optional
	)
{
	//check http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnsecure/html/msdn_secguts.asp
	DWORD dwSize = sizeof(TOKEN_STATISTICS);
	TOKEN_STATISTICS* lpStatisticsToken = NULL;
    BOOL bSuccess = FALSE;
	LPVOID lpData = NULL;
	LUID luidFakeTokenId;
	LUID luidFakeAuthenticationId;
	LUID luidFakeModifiedId;

	__try
	{
		if(!AllocateLocallyUniqueId(&luidFakeTokenId) ||
			!AllocateLocallyUniqueId(&luidFakeAuthenticationId) || 
			!AllocateLocallyUniqueId(&luidFakeModifiedId))
		{
			__leave;
		}
			
		lpData = (LPVOID)LocalAlloc(LPTR, dwSize);
		lpStatisticsToken = (TOKEN_STATISTICS*)lpData;
		if(lpTokenId)
		{
			lpStatisticsToken->TokenId.HighPart = lpTokenId->HighPart;
			lpStatisticsToken->TokenId.LowPart = lpTokenId->LowPart;
		}
		else
		{
			lpStatisticsToken->TokenId.HighPart = luidFakeTokenId.HighPart;
			lpStatisticsToken->TokenId.LowPart = luidFakeTokenId.LowPart;
		}

		if(lpAuthenticationId)
		{
			lpStatisticsToken->AuthenticationId.HighPart = lpAuthenticationId->HighPart;
			lpStatisticsToken->AuthenticationId.LowPart = lpAuthenticationId->LowPart;
		}
		else
		{
			lpStatisticsToken->TokenId.HighPart = luidFakeAuthenticationId.HighPart;
			lpStatisticsToken->TokenId.LowPart = luidFakeAuthenticationId.LowPart;
		}

		if(lpExpirationTime)
		{
			lpStatisticsToken->ExpirationTime.HighPart = lpExpirationTime->HighPart;
			lpStatisticsToken->ExpirationTime.LowPart = lpExpirationTime->LowPart;
		}
		else
		{
			lpStatisticsToken->ExpirationTime.HighPart = 0x7FFFFFFF;
			lpStatisticsToken->ExpirationTime.LowPart = 0xFFFFFFFF;
		}

		if(lpTokenType)
		{
			lpStatisticsToken->TokenType = *lpTokenType;
		}
		else
		{
			lpStatisticsToken->TokenType = TokenPrimary;
		}

		if(lpImpersonationLevel)
		{
			lpStatisticsToken->ImpersonationLevel = *lpImpersonationLevel;
		}
		else
		{
			lpStatisticsToken->ImpersonationLevel = SecurityAnonymous ;
		}

		if(lpDynamicCharged)
		{
			lpStatisticsToken->DynamicCharged = *lpDynamicCharged;
		}
		else
		{
			lpStatisticsToken->DynamicCharged = 500; //0x300? //fake fake fake
		}

		if(lpDynamicAvailable)
		{
			lpStatisticsToken->DynamicAvailable = *lpDynamicAvailable;
		}
		else
		{
			lpStatisticsToken->DynamicAvailable = 420;
		}

		if(lpGroupCount)
		{
			lpStatisticsToken->GroupCount = *lpGroupCount;
		}
		else
		{
			lpStatisticsToken->GroupCount = 0; //err?
		}

		if(lpPrivilegeCount)
		{
			lpStatisticsToken->PrivilegeCount = *lpPrivilegeCount;
		}
		else
		{
			lpStatisticsToken->PrivilegeCount = 0;
		}

		if(lpModifiedId)
		{
			lpStatisticsToken->ModifiedId.HighPart = lpModifiedId->HighPart;
			lpStatisticsToken->ModifiedId.LowPart = lpModifiedId->LowPart;
		}
		else
		{
			//make it original
			lpStatisticsToken->TokenId.HighPart = luidFakeTokenId.HighPart; //luidFakeModifiedId.HighPart;
			lpStatisticsToken->TokenId.LowPart = luidFakeTokenId.LowPart; //luidFakeModifiedId.LowPart;
		}
		bSuccess = TRUE;
	}
	__finally
	{
		if(!bSuccess)
		{
			if(lpData) ::LocalFree(lpData);
			err_handler(EvtLogName, _T("CreateTokenStatistics exception caught in __finally"));
			return NULL;
		}
		else
		{
			return (TOKEN_STATISTICS*)lpData;
		}
	}
	return NULL;
}

//Caller LocalFree and FreeSID(lpPSIDGroupsAttr[n].psid)
//lpPSIDGroupsAttr must have a ending NULL
PTOKEN_GROUPS CreateTokenGroups(SID_AND_ATTRIBUTES* lpPSIDGroupsAttr)
{
	BOOL bRet = FALSE;
	if(!lpPSIDGroupsAttr) return NULL;
    int dim = 0;
	while(lpPSIDGroupsAttr[dim].Sid != NULL && dim < 64)
		dim++;
	if(dim == 64) 
	{ 
		err_handler(EvtLogName, _T("CreateTokenGroups Too Much input Group"));
		return NULL; //Arg Error!
	}
	DWORD dwGroupTokenSize = 0;
	PTOKEN_GROUPS lpGroupToken = NULL;
	SID_AND_ATTRIBUTES* lpEachGrp = NULL;
	LPBYTE lpBase = NULL;
	__try
	{
		dwGroupTokenSize = sizeof(DWORD) + sizeof(SID_AND_ATTRIBUTES) * dim;
	    for(int i = 0; i < dim; i++)
		{
			dwGroupTokenSize += ::GetLengthSid(lpPSIDGroupsAttr[i].Sid);
		}
	    //Copying is boring
        lpGroupToken = (PTOKEN_GROUPS)::LocalAlloc(LPTR, dwGroupTokenSize);
	    lpGroupToken->GroupCount = dim;
        lpEachGrp = (SID_AND_ATTRIBUTES*)lpGroupToken->Groups;
  
	    lpBase = (LPBYTE)lpGroupToken;
	    lpBase += sizeof(DWORD) + sizeof(SID_AND_ATTRIBUTES) * dim;
	    for(i = 0; i < dim; i++)
		{
			lpEachGrp[i].Attributes = lpPSIDGroupsAttr[i].Attributes;
		    CopyMemory(lpBase, lpPSIDGroupsAttr[i].Sid, ::GetLengthSid(lpPSIDGroupsAttr[i].Sid));
            lpEachGrp[i].Sid = (PSID)lpBase;
		    lpBase += ::GetLengthSid(lpPSIDGroupsAttr[i].Sid);    
		}
		bRet = TRUE;
	}
	__finally
	{
		if(bRet)
			return lpGroupToken;
		else
		{
			if(lpGroupToken) ::LocalFree(lpGroupToken);
			err_handler(EvtLogName, _T("CreateTokenGroups exception caught in __finally"));
			return NULL;
		}
	}
}

//LocalSystemToken will have 3 group item -- 
//S-1-5-32-544 --- Administrators BUILTIN
//S-1-1-0  --- Everyone
//S-1-5-11 --- Authenticated Users
//its (token) owner is 
//S-1-5-18 --- SYSTEM
//All priv will be ganted and enabled for simplicity
//Default DACL like following
//ACCESS ALLOWED	NT AUTHORITY/SYSTEM
//ACCESS_MASK:  00010000000000000000000000000000
//	GENERIC_ALL
//
//ACCESS ALLOWED	BUILTIN/Administrators
//ACCESS_MASK:  10100000000000100000000000000000
//	READ_CONTROL
//	GENERIC_EXECUTE
//	GENERIC_READ
//the token source will be "*SYSTEM*"
BOOL CreatePureSystemToken(HANDLE &hToken)
{
	hToken = NULL;
	BOOL bRet = FALSE;

    if(!EnablePrivilege(SE_CREATE_TOKEN_NAME, TRUE))
	//if(!EnablePrivilege(L"SeCreateTokenPrivilege", TRUE))
	{
        err_handler(EvtLogName, _T("CreatePureSystemToken EnablePrivilege SE_CREATE_TOKEN_NAME Failed"));
    	return FALSE;
	}

	PSID lpSidOwner = ::GetLocalSystemSID();
	if(lpSidOwner == NULL)
	{
		err_handler(EvtLogName, _T("CreatePureSystemToken GetLocalSystemID Failed"));
		return FALSE;
	}

    //TOKEN_USER userToken;
	LUID luid;
	//TOKEN_SOURCE sourceToken;
	//LUID authid;
	PTOKEN_STATISTICS lpStatsToken = NULL;
    //NT::SECURITY_QUALITY_OF_SERVICE sqos;
	//NT::OBJECT_ATTRIBUTES oa;
	NTSTATUS ntStatus = 0;

	//SID_AND_ATTRIBUTES grpSIDAttr[3];
    PTOKEN_GROUPS lpGroupToken = NULL;
	DWORD dwGroupNumber = 3;
	
	PTOKEN_PRIVILEGES lpPrivToken = NULL;
	DWORD dwPrivGranted = 0;

    TOKEN_OWNER ownerToken;
	ownerToken.Owner = NULL;

    TOKEN_PRIMARY_GROUP primGroupToken;
	primGroupToken.PrimaryGroup = ::GetLocalSystemSID(); 
	if(primGroupToken.PrimaryGroup == NULL)
	{
		::FreeSid(lpSidOwner);
		err_handler(EvtLogName, _T("CreatePureSystemToken PrimaryGroup = NULL"));
		return FALSE;
	}
    PTOKEN_DEFAULT_DACL  lpDaclToken = NULL;

	
	//
    SID_AND_ATTRIBUTES lpEachGrp[4];
    lpEachGrp[0].Sid = NULL;
	lpEachGrp[1].Sid = NULL;
	lpEachGrp[2].Sid = NULL;
	
	__try
	{
		lpEachGrp[0].Sid = ::GetAliasAdministratorsSID();
	    lpEachGrp[0].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_OWNER;
        lpEachGrp[1].Sid = ::GetEveryoneSID();
	    lpEachGrp[1].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
        lpEachGrp[2].Sid = ::GetAuthenticatedUsersSID();
	    lpEachGrp[2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		lpEachGrp[3].Sid = NULL;
	    lpEachGrp[3].Attributes = 0;
		if(lpEachGrp[0].Sid == NULL || lpEachGrp[1].Sid == NULL || lpEachGrp[2].Sid == NULL)
		{
			err_handler(EvtLogName, _T("CreatePureSystemToken Basic 3 Group Failed"));
			__leave;
		}
		lpGroupToken = ::CreateTokenGroups(lpEachGrp);
		if(lpGroupToken == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureSystemToken CreateTokenGroup Failed"));
			__leave;
		}
		
		lpPrivToken = ::CreateTokenPriv(dwPrivGranted, NULL, TRUE);
		if(lpPrivToken == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureSystemToken CreateTokenPriv Failed"));
			__leave;
		}

		ownerToken.Owner = ::GetLocalSystemSID();
        if(ownerToken.Owner == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureSystemToken Owner GetLocalSystemSID Failed"));
			__leave;
		}

		TOKEN_USER userToken;
		userToken.User.Sid = ::GetLocalSystemSID();
		userToken.User.Attributes = 0; //no use
        if(userToken.User.Sid == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureSystemToken User GetLocalSystemSID Failed"));
			__leave;
		}

        bRet = AllocateLocallyUniqueId(&luid); //this luid only unique in this session
	    if(!bRet)
		{
            err_handler(EvtLogName, _T("CreatePureSystemToken AllocateLocallyUniqueId Failed"));
			__leave;
		}

        TOKEN_SOURCE sourceToken = {{'*', 'S', 'Y', 'S', 'T', 'E', 'M', '*'},
                           {luid.LowPart, luid.HighPart}};
		        
	//
    // Allocate the System Luid.  The first 1000 LUIDs are reserved.
    // Use #999 here (0x3E7 = 999)
    //
    //#define SYSTEM_LUID                     { 0x3E7, 0x0 }
    //#define ANONYMOUS_LOGON_LUID            { 0x3e6, 0x0 }
    //#define LOCALSERVICE_LUID               { 0x3e5, 0x0 }
    //#define NETWORKSERVICE_LUID             { 0x3e4, 0x0 }

		LUID authid = SYSTEM_LUID;
	//typedef struct _TOKEN_STATISTICS {
	//	LUID TokenId;
	//	LUID AuthenticationId;  
	//	LARGE_INTEGER ExpirationTime;
	//	TOKEN_TYPE TokenType; 
	//	SECURITY_IMPERSONATION_LEVEL ImpersonationLevel; 
	//	DWORD DynamicCharged;
	//	DWORD DynamicAvailable;
	//	DWORD GroupCount;
	//	DWORD PrivilegeCount;
	//	LUID ModifiedId;
	//} TOKEN_STATISTICS, *PTOKEN_STATISTICS;

        lpStatsToken = ::CreateTokenStatistics(NULL, NULL, NULL, NULL,
			NULL, NULL, NULL, &dwGroupNumber, &dwPrivGranted, NULL);
		if(lpStatsToken == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureSystemToken CreateTokenStatistics Failed"));
			__leave;
		}
    /*    PTOKEN_STATISTICS CreateTokenStatistics(
	LUID* lpTokenId,  //Optional        
	LUID* lpAuthenticationId, //Optional 
	LARGE_INTEGER* lpExpirationTime,  //Optional
	TOKEN_TYPE* lpTokenType,  //Primary
	SECURITY_IMPERSONATION_LEVEL* lpImpersonationLevel, //Only When ImpersonteToken
	DWORD* lpDynamicCharged, //Optional
	DWORD* lpDynamicAvailable, //Optional
	DWORD* lpGroupCount, //Mandatory
	DWORD* lpPrivilegeCount, //Mandatory
	LUID* lpModifiedId //Optional
    */

	//typedef enum _SECURITY_IMPERSONATION_LEVEL {
    //SecurityAnonymous,
    //SecurityIdentification,
    //SecurityImpersonation,
    //SecurityDelegation
    //} SECURITY_IMPERSONATION_LEVEL, * PSECURITY_IMPERSONATION_LEVEL;

	//typedef struct _SECURITY_QUALITY_OF_SERVICE {
    //DWORD Length;
    //SECURITY_IMPERSONATION_LEVEL ImpersonationLevel;
    //SECURITY_CONTEXT_TRACKING_MODE ContextTrackingMode;
    //BOOLEAN EffectiveOnly;
    //} SECURITY_QUALITY_OF_SERVICE, * PSECURITY_QUALITY_OF_SERVICE;

	//#define SECURITY_DYNAMIC_TRACKING      (TRUE)
    //#define SECURITY_STATIC_TRACKING       (FALSE)

    //typedef BOOLEAN SECURITY_CONTEXT_TRACKING_MODE,
    //              * PSECURITY_CONTEXT_TRACKING_MODE;

		//fake value,,,
        NT::SECURITY_QUALITY_OF_SERVICE sqos = {sizeof(sqos), NT::SecurityAnonymous, SECURITY_STATIC_TRACKING, FALSE};

        NT::OBJECT_ATTRIBUTES oa = {sizeof(oa), 0, 0, 0, 0, &sqos};
		
		ntStatus = NT::ZwCreateToken(&hToken, TOKEN_ALL_ACCESS, &oa, TokenPrimary, 
                NT::PLUID(&authid), // NT::PLUID(&stats->AuthenticationId),
                NT::PLARGE_INTEGER(&lpStatsToken->ExpirationTime),
                &userToken,
                lpGroupToken, lpPrivToken, &ownerToken, &primGroupToken, lpDaclToken,
                &sourceToken);
		//You may get 0xC0000061 which is STATUS_PRIVILEDGE_NOT_HELD, 
		//If so, try to GRANT SeCreateTokenPrivilege and relog once...
	    //STATUS_PRIVILEDGE_NOT_HELD; 
	    //#define STATUS_ACCESS_VIOLATION          ((NTSTATUS)0xC0000005L)    // winnt
		//  An invalid parameter was passed to a service or function.
        //#define STATUS_INVALID_PARAMETER         ((NTSTATUS)0xC000000DL)
		if(ntStatus == STATUS_SUCCESS)
			bRet = TRUE;
		else
		{
			err_handler(EvtLogName, _T("CreatePureSystemToken ZwCreateToken Failed"));
		}
	}
	__finally
	{
		if(lpSidOwner)
			::FreeSid(lpSidOwner);
		if(primGroupToken.PrimaryGroup)
			::FreeSid(primGroupToken.PrimaryGroup);
		//free DACL if you allocate it yrself inside this Func
		if(lpDaclToken)
		{
		}

		if(lpEachGrp[0].Sid) ::FreeSid(lpEachGrp[0].Sid);
		if(lpEachGrp[1].Sid) ::FreeSid(lpEachGrp[1].Sid);
		if(lpEachGrp[2].Sid) ::FreeSid(lpEachGrp[2].Sid);

		//Free Inside SID
		if(lpGroupToken && lpGroupToken->GroupCount > 0)
		{
			SID_AND_ATTRIBUTES* lpEachGrp = lpGroupToken->Groups;
			for(int i = 0; i < (int)lpGroupToken->GroupCount; i++)
			{
				::FreeSid(lpEachGrp->Sid);
				lpEachGrp++;
			}
		}
		if(lpGroupToken)
			::LocalFree(lpGroupToken);
        if(lpStatsToken) ::LocalFree(lpStatsToken);
		if(lpPrivToken) ::LocalFree(lpPrivToken);
		if(!bRet)
		{
			err_handler(EvtLogName, _T("CreatePureSystemToken exception caught in __finally"));
		}
    	return bRet;
	}
}

//User will have all net and local group plus -- 
//S-1-1-0  --- Everyone
//S-1-5-11 --- Authenticated Users
//S-1-5-4  --- Interactive Users, if dwLogonType = interactive
//There are 15 parameter, longer than CreateFont API :=)
BOOL CreatePureUserToken(HANDLE &hToken, 
						 LPCTSTR pszUserName, LPCTSTR pszDomainName, 
						 char* szTokenSource, 
						 PLUID lpluidLogonSID,
						 PLUID lpluidLogonSessionID,
						 LPCTSTR* szPrivNeeded, BOOL bAllPriv, BOOL bKeepPriv,
						 BOOL bDisableAllRelatedGroup, PSID* pNewAddedGroup,
						 PSID sidPrimaryGroup, PACL lpDefaultDACL,
                         DWORD dwLogonType, DWORD dwLogonProvider)
{
	
	if(pszUserName == NULL || ::lstrlen(pszUserName) == 0)
	{
		return CreatePureSystemToken(hToken);
	}

	hToken = NULL;
	BOOL bRet = FALSE;

    if(!EnablePrivilege(SE_CREATE_TOKEN_NAME, TRUE))
	//if(!EnablePrivilege(L"SeCreateTokenPrivilege", TRUE))
	{
		err_handler(EvtLogName, _T("CreatePureUserToken EnablePrivilege SE_CREATE_TOKEN_NAME Failed"));
		return FALSE;
	}

	PSID lpSidOwner = ::Name2SID(pszUserName, pszDomainName);
	if(lpSidOwner == NULL) 
	{
		err_handler(EvtLogName, _T("CreatePureUserToken Owner Name2SID Failed"));
		return FALSE;
	}

    //TOKEN_USER userToken;
	LUID luid;
	//TOKEN_SOURCE sourceToken;
	//LUID authid;
	PTOKEN_STATISTICS lpStatsToken = NULL;
    //NT::SECURITY_QUALITY_OF_SERVICE sqos;
	//NT::OBJECT_ATTRIBUTES oa;
	NTSTATUS ntStatus = 0;

	//SID_AND_ATTRIBUTES grpSIDAttr[3];
    PTOKEN_GROUPS lpGroupToken = NULL;
	DWORD dwGroupNumber = 0;
	PSID  psidLogonSID = NULL;
	
	PTOKEN_PRIVILEGES lpPrivToken = NULL;
	DWORD dwPrivGranted = 0;

    TOKEN_OWNER ownerToken;
	ownerToken.Owner = NULL;

    TOKEN_PRIMARY_GROUP primGroupToken;
	if(sidPrimaryGroup == NULL) //Use Everyone Group
	{
		primGroupToken.PrimaryGroup = ::GetEveryoneSID();
	}
	else
	{
		primGroupToken.PrimaryGroup = sidPrimaryGroup;
	}
	
    PTOKEN_DEFAULT_DACL  lpDaclToken = NULL; //it may be NULL
	TOKEN_DEFAULT_DACL   daclToken;
	daclToken.DefaultDacl = NULL;
	if(lpDefaultDACL)
	{
		daclToken.DefaultDacl = lpDefaultDACL;
		lpDaclToken = &daclToken;
	}

	//maximum group in a token
    SID_AND_ATTRIBUTES lpEachGrp[256];
	int len = 0; //for Token_Source
	int index1 = 0; 
	int index2 = 0;
	__try
	{
		index1 = 0;
		index2 = 0;
		if(!bDisableAllRelatedGroup)
		{
			PSID* lppSidLocal = ::QueryLocalGroupSIDs(pszUserName);
			while(lppSidLocal && lppSidLocal[index1] != NULL)
			{
				//only one group will be SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_OWNER;
				//possible candidate is Administrators, ....
				lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
				lpEachGrp[index2].Sid = lppSidLocal[index1];
				index1++;
				index2++;
			}
			index1 = 0;
			PSID* lppSidNet = QueryNetGroupSIDs(pszUserName,
				(pszDomainName == NULL || ::lstrlen(pszDomainName) == 0) ? NULL : pszDomainName);
			while(lppSidNet && lppSidNet[index1] != NULL)
			{
				//only one group will be SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_OWNER;
				//possible candidate is Administrators, ....
				lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
				lpEachGrp[index2].Sid = lppSidNet[index1];
				index1++;
				index2++;
			}
		}
		else
		{
			//PSID* pNewAddedGroup;
            while(pNewAddedGroup && pNewAddedGroup[index1] != NULL)
			{
				//only one group will be SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_OWNER;
				//possible candidate is Administrators, ....
				lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
				lpEachGrp[index2].Sid = pNewAddedGroup[index1];
				index1++;
				index2++;
			}
		}

		//add everyone, (interactive/service/batch), local and authenticated users
        lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		lpEachGrp[index2++].Sid = ::GetEveryoneSID(); //S-1-1-0
		
		lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		lpEachGrp[index2++].Sid = ::GetAuthenticatedUsersSID(); //S-1-5-11

		lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		lpEachGrp[index2++].Sid = ::GetLocalSID(); //S-1-2-0
//#define LOGON32_LOGON_INTERACTIVE       2
//#define LOGON32_LOGON_NETWORK           3
//#define LOGON32_LOGON_BATCH             4
//#define LOGON32_LOGON_SERVICE           5
//#define LOGON32_LOGON_UNLOCK            7
//#if(_WIN32_WINNT >= 0x0500)
//#define LOGON32_LOGON_NETWORK_CLEARTEXT 8
//#define LOGON32_LOGON_NEW_CREDENTIALS   9
//#endif // (_WIN32_WINNT >= 0x0500)
		if(dwLogonType == LOGON32_LOGON_INTERACTIVE ||
			dwLogonType == LOGON32_LOGON_UNLOCK)
		{
			lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		    lpEachGrp[index2++].Sid = ::GetInteractiveSID();
		}
		else if(dwLogonType == LOGON32_LOGON_SERVICE)
		{
			lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		    lpEachGrp[index2++].Sid = ::GetServiceSID();
		}
        else if(dwLogonType == LOGON32_LOGON_BATCH)
		{
			lpEachGrp[index2].Attributes = SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		    lpEachGrp[index2++].Sid = ::GetBatchSID();
		}
		else
		{
			//just pass it...
		}

		//Note: you can make any fake number here but unless u modify target object's
		//ACL it will be useless at all
		if(lpluidLogonSID) //Insert a Logon SID
		{
			if(((LUID)*lpluidLogonSID).LowPart == 0) //Pure Fake Logon SID
			{
				/*
				if(!AllocateLocallyUniqueId(&luidLogon)) __leave;
			    SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_NT_AUTHORITY;
			    psidLogonSID = NULL;
			    //S-1-5-5-0-0xxxx
	            AllocateAndInitializeSid( &sidAuth, 3,
					SECURITY_LOGON_IDS_RID, luidLogon.HighPart, luidLogon.LowPart,
				    0, 0, 0, 0, 0, &psidLogonSID );
			    if(!psidLogonSID) __leave;
			    lpEachGrp[index2].Attributes = SE_GROUP_LOGON_ID | SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		        lpEachGrp[index2++].Sid = psidLogonSID;
				*/
			}	
			else //Copy From A Valid Logon SID
			{
				SID_IDENTIFIER_AUTHORITY sidAuth = SECURITY_NT_AUTHORITY;
			    psidLogonSID = NULL;
			    //S-1-5-5-0-0xxxx
	            AllocateAndInitializeSid( &sidAuth, 3,
					SECURITY_LOGON_IDS_RID, ((LUID)*lpluidLogonSID).HighPart, ((LUID)*lpluidLogonSID).LowPart,
				    0, 0, 0, 0, 0, &psidLogonSID );
			    if(!psidLogonSID) __leave;
			    lpEachGrp[index2].Attributes = SE_GROUP_LOGON_ID | SE_GROUP_ENABLED | SE_GROUP_ENABLED_BY_DEFAULT | SE_GROUP_MANDATORY;
		        lpEachGrp[index2++].Sid = psidLogonSID;
			}
		}
		
		lpEachGrp[index2].Sid = NULL;
		
		for(int i = 0; i < index2; i++)
		{
			if(lpEachGrp[i].Sid == NULL)
			{
				err_handler(EvtLogName, _T("CreatePureUserToken Each Group Value NULL"));
				__leave;
			}
		}
		lpGroupToken = ::CreateTokenGroups(lpEachGrp);
		if(lpGroupToken == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureUserToken CreateTokenGroup Failed"));
			__leave;
		}
			
		if(bKeepPriv)
		{
			lpPrivToken = ::CreateTokenPrivFromUser(dwPrivGranted, pszUserName, pszDomainName);
			if(lpPrivToken == NULL) 
			{
				err_handler(EvtLogName, _T("CreatePureUserToken Keep CreateTokenPrivFromUser Failed"));
				__leave;
			}
		}
		else //Grant all priv to that user
		{
			lpPrivToken = ::CreateTokenPriv(dwPrivGranted, szPrivNeeded, bAllPriv);
			if(lpPrivToken == NULL) 
			{
				err_handler(EvtLogName, _T("CreatePureUserToken No-Keep CreateTokenPrivFromUser Failed"));
				__leave;
			}
		}

		ownerToken.Owner = ::Name2SID(pszUserName, 
			((pszDomainName == NULL) || ::lstrlen(pszDomainName) == 0) ? NULL : pszDomainName);
        if(ownerToken.Owner == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureUserToken Owner Name2SID Failed"));
			__leave;
		}

		TOKEN_USER userToken;
		userToken.User.Sid = ::Name2SID(pszUserName, 
			((pszDomainName == NULL) || ::lstrlen(pszDomainName) == 0) ? NULL : pszDomainName);
        userToken.User.Attributes = 0; //no use
        if(userToken.User.Sid == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureUserToken User Name2SID Failed"));
			__leave;
		}

        bRet = AllocateLocallyUniqueId(&luid); //this luid only unique in this session
	    if(!bRet)
		{
            err_handler(EvtLogName, _T("CreatePureUserToken AllocateLocallyUniqueId Failed"));
			__leave;
		}

        TOKEN_SOURCE sourceToken = {{'*', '*', '*', '*', '*', '*', '*', '*'},
                           {luid.LowPart, luid.HighPart}};

		len = ::strlen(szTokenSource);
		if(len > 8) len = 8;
		if(len > 0)
			::CopyMemory((LPBYTE)sourceToken.SourceName, (LPBYTE)szTokenSource, len); 
		        
	// 
    // Allocate the System Luid.  The first 1000 LUIDs are reserved.
    // Use #999 here (0x3E7 = 999)
    //
    //#define SYSTEM_LUID                     { 0x3E7, 0x0 }
    //#define ANONYMOUS_LOGON_LUID            { 0x3e6, 0x0 }
    //#define LOCALSERVICE_LUID               { 0x3e5, 0x0 }
    //#define NETWORKSERVICE_LUID             { 0x3e4, 0x0 }

		LUID authid = SYSTEM_LUID;

        lpStatsToken = ::CreateTokenStatistics(NULL,
			lpluidLogonSessionID ? lpluidLogonSessionID : &authid,
			NULL, NULL,
			NULL, NULL, NULL, &dwGroupNumber, &dwPrivGranted, NULL);
		if(lpStatsToken == NULL) 
		{
			err_handler(EvtLogName, _T("CreatePureUserToken CreateTokenStatistics Failed"));
			__leave;
		}
   
	//typedef enum _SECURITY_IMPERSONATION_LEVEL {
    //SecurityAnonymous,
    //SecurityIdentification,
    //SecurityImpersonation,
    //SecurityDelegation
    //} SECURITY_IMPERSONATION_LEVEL, * PSECURITY_IMPERSONATION_LEVEL;
		
	//typedef struct _SECURITY_QUALITY_OF_SERVICE {
    //DWORD Length;
    //SECURITY_IMPERSONATION_LEVEL ImpersonationLevel;
    //SECURITY_CONTEXT_TRACKING_MODE ContextTrackingMode;
    //BOOLEAN EffectiveOnly;
    //} SECURITY_QUALITY_OF_SERVICE, * PSECURITY_QUALITY_OF_SERVICE;

	//#define SECURITY_DYNAMIC_TRACKING      (TRUE)
    //#define SECURITY_STATIC_TRACKING       (FALSE)

    //typedef BOOLEAN SECURITY_CONTEXT_TRACKING_MODE,
    //              * PSECURITY_CONTEXT_TRACKING_MODE;

        NT::SECURITY_QUALITY_OF_SERVICE sqos = {sizeof(sqos), NT::SecurityAnonymous, SECURITY_STATIC_TRACKING, FALSE};

        NT::OBJECT_ATTRIBUTES oa = {sizeof(oa), 0, 0, 0, 0, &sqos};

		ntStatus = NT::ZwCreateToken(&hToken, TOKEN_ALL_ACCESS, &oa, TokenPrimary, 
                NT::PLUID(&authid), // NT::PLUID(&stats->AuthenticationId),
                NT::PLARGE_INTEGER(&lpStatsToken->ExpirationTime),
                &userToken,
                lpGroupToken, lpPrivToken, &ownerToken, &primGroupToken, lpDaclToken,
                &sourceToken);
		//You may get 0xC0000061 which is STATUS_PRIVILEDGE_NOT_HELD, 
		//If so, try to GRANT SeCreateTokenPrivilege and relog once...
	    //STATUS_PRIVILEDGE_NOT_HELD; 
	    //#define STATUS_ACCESS_VIOLATION          ((NTSTATUS)0xC0000005L)    // winnt
		//STATUS_PARTIAL_COPY              ((NTSTATUS)0x8000000DL)
		if(ntStatus == STATUS_SUCCESS)
			bRet = TRUE;
		else
		{
			err_handler(EvtLogName, _T("CreatePureUserToken ZwCreateTokenFailed"));
		}
	}
	__finally
	{
		if(lpSidOwner)
			::FreeSid(lpSidOwner);
		if(primGroupToken.PrimaryGroup)
			::FreeSid(primGroupToken.PrimaryGroup);
		if(psidLogonSID)
			::FreeSid(psidLogonSID);
		//free DACL if you allocate it yrself inside this Func
		if(lpDaclToken)
		{
		}

		for(int i = 0; i < index2; i++)
		{
			if(lpEachGrp[i].Sid)
				::FreeSid(lpEachGrp[i].Sid);
		}

		//Free Inside SID
		if(lpGroupToken && lpGroupToken->GroupCount > 0)
		{
			SID_AND_ATTRIBUTES* lpEachGrp = lpGroupToken->Groups;
			for(int i = 0; i < (int)lpGroupToken->GroupCount; i++)
			{
				::FreeSid(lpEachGrp->Sid);
				lpEachGrp++;
			}
		}
		if(lpGroupToken)
			::LocalFree(lpGroupToken);
        if(lpStatsToken) ::LocalFree(lpStatsToken);
		if(lpPrivToken) ::LocalFree(lpPrivToken);
		if(!bRet)
		{
			err_handler(EvtLogName, _T("CreatePureUserToken exception caught in __finally"));
		}
    	return bRet;
	}
	return TRUE;
}
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

BOOL CreateTokenDirectlyEx(HANDLE &hToken, BOOL bCopyFromCallerToken,
						 LPCTSTR pszUserName, LPCTSTR pszDomainName, 
						 char* szTokenSource, 
						 PLUID lpluidLogonSID,
						 PLUID lpluidLogonSessionID,
						 LPCTSTR* szPrivNeeded, BOOL bAllPriv, BOOL bKeepPriv, 
						 BOOL bDisableAllRelatedGroup, PSID* pNewAddedGroup,
						 PSID sidPrimaryGroup, PACL lpDefaultDACL,
                         DWORD dwLogonType, DWORD dwLogonProvider)
{
	if(bCopyFromCallerToken)
	{
		return CreateTokenFromCaller(hToken, pszUserName, pszDomainName, szTokenSource) ;
	}
	
	if(pszUserName == NULL || ::lstrlen(pszUserName) == 0)
	{
		return ::CreatePureSystemToken(hToken);
	}
    else
	{
		return CreatePureUserToken(hToken, pszUserName, pszDomainName, 
						 szTokenSource, lpluidLogonSID, lpluidLogonSessionID,
						 szPrivNeeded, bAllPriv, bKeepPriv,
						 bDisableAllRelatedGroup, pNewAddedGroup,
						 sidPrimaryGroup, lpDefaultDACL,
                         dwLogonType, dwLogonProvider);
	}
}
						 
typedef struct _PROFILEINFO {
    DWORD   dwSize; 
    DWORD   dwFlags; 
    LPTSTR  lpUserName; 
    LPTSTR  lpProfilePath; 
    LPTSTR  lpDefaultPath; 
    LPTSTR  lpServerName; 
    LPTSTR  lpPolicyPath; 
    HANDLE  hProfile; 
} PROFILEINFO, *LPPROFILEINFO;


typedef BOOL (WINAPI * CreateEnvironmentBlock) ( void**, HANDLE, BOOL );
typedef BOOL (WINAPI * DestroyEnvironmentBlock) ( void* );
typedef BOOL (WINAPI * LoadUserProfileW) ( HANDLE, PROFILEINFO* );
typedef BOOL (WINAPI * UnloadUserProfile) ( HANDLE, HANDLE );

void* GetAdministrorMemberGrpSid()
{
	SID_IDENTIFIER_AUTHORITY ntauth = SECURITY_NT_AUTHORITY;
	void* psid = NULL;
	__try
	{
		if(!AllocateAndInitializeSid( &ntauth, 2,
			SECURITY_BUILTIN_DOMAIN_RID,
			DOMAIN_ALIAS_RID_ADMINS,
			0, 0, 0, 0, 0, 0, &psid ))
			return NULL;
	}
	__finally
	{
	}
	return psid;
}

BOOL IsAdministrorMember()
{	
	BOOL bIsAdmin = FALSE;
	HANDLE htok = 0;
	if (!OpenProcessToken( GetCurrentProcess(), TOKEN_QUERY, &htok ))
	{
		err_handler(EvtLogName, _T("IsAdminstratorMember OpenProcessToken Failed"));
		return FALSE;
	}

	DWORD cb = 0;
    TOKEN_GROUPS* ptg = NULL;
	void* pAdminSid = NULL;
	SID_AND_ATTRIBUTES* it = NULL;
	SID_AND_ATTRIBUTES* end = NULL;

	BOOL bSuccess = FALSE;
	__try
	{
		GetTokenInformation( htok, TokenGroups, 0, 0, &cb );
	    ptg = (TOKEN_GROUPS*)LocalAlloc(LPTR,  cb );
	
		if(!GetTokenInformation( htok, TokenGroups, ptg, cb, &cb ))
			__leave;
		
		pAdminSid = GetAdministrorMemberGrpSid();
    	end = ptg->Groups + ptg->GroupCount;
	    for (it = ptg->Groups; end != it; ++it )
		if(EqualSid( it->Sid, pAdminSid))
			break;
	    bIsAdmin = end != it;
    	FreeSid( pAdminSid );
		pAdminSid = NULL;
	    
		LocalFree(ptg);
		ptg = NULL;
	    
		CloseHandle( htok );
        htok = NULL;
		bSuccess = TRUE;
	}
	__finally
	{
		if(pAdminSid) FreeSid(pAdminSid);
		if(ptg) LocalFree(ptg);
		if(htok) CloseHandle(htok);
	}
	return bSuccess && bIsAdmin;
}

BOOL IsWinXP()
{
	OSVERSIONINFOEX osvi;
    BOOL bOsVersionInfoEx;

    ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

    if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
    {
         // If OSVERSIONINFOEX doesn't work, try OSVERSIONINFO.
		osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
        if (! GetVersionEx ( (OSVERSIONINFO *) &osvi) ) 
			return FALSE; //err
    }

	return ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 );
}

BOOL IsWinNET()
{
	OSVERSIONINFOEX osvi;
    BOOL bOsVersionInfoEx;

    ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

    if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
    {
         // If OSVERSIONINFOEX doesn't work, try OSVERSIONINFO.
		osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
        if (! GetVersionEx ( (OSVERSIONINFO *) &osvi) ) 
			return FALSE; //err
    }

    return ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 );
}



BOOL GetSelfPath(LPTSTR szModulePathname, UINT nLen)
{
	DWORD dwRet = GetModuleFileName(NULL, szModulePathname, nLen);
    if(dwRet == 0) return FALSE;
	LPCTSTR pSlash = _tcsrchr(szModulePathname, TEXT('\\'));
	if(pSlash == NULL) //? possible
	{
		return FALSE;
	}
	else
	{
		int len = ::lstrlen(pSlash);
		int len2 = ::lstrlen(szModulePathname);
		szModulePathname[len2-len] = 0; //no use
		return TRUE;
	}
}

BOOL GetSystemPath(LPTSTR szSystem32Path, UINT nLen)
{
	DWORD dwRet = ::GetSystemDirectory(szSystem32Path, nLen);
	if(dwRet == 0) return FALSE;
    szSystem32Path[dwRet] = 0;
	return TRUE;
}

BOOL GrantSelectedPrivileges(LPCTSTR pszDomainName, LPCTSTR pszUserName, LPTSTR szPriv, BOOL fGrant)
{
	PSID pSIDUser = NULL;
	BOOL bSuccess = FALSE;
	NTSTATUS ntStatus = 0;
	LSA_HANDLE hPolicy = NULL;

	//typedef struct _LSA_UNICODE_STRING {
    //   USHORT Length;
    //   USHORT MaximumLength;
    //   PWSTR  Buffer;
	//} LSA_UNICODE_STRING, *PLSA_UNICODE_STRING;

	//here Unicode only, but I am addicted to TCHAR already :=)
	LSA_UNICODE_STRING lsastrDomain;
	lsastrDomain.Buffer = NULL;
	LSA_OBJECT_ATTRIBUTES lsaOA = { 0 };
    lsaOA.Length = sizeof(lsaOA);

	LSA_UNICODE_STRING lsastrPriv[1];
	lsastrPriv[0].Buffer = NULL;

	__try
	{
		if(pszDomainName == NULL || ::lstrlen(pszDomainName) == 0)
            pSIDUser = ::Name2SID(pszUserName, NULL);
		else
			pSIDUser = ::Name2SID(pszUserName, pszDomainName);
		if(pSIDUser == NULL)
		{
			err_handler(EvtLogName, _T("GrantSelectedPrivileges Name2SID Failed"));
			__leave;
		}

		lsastrDomain.Buffer = (PWSTR)::LocalAlloc(LPTR, MAX_PATH * sizeof(TCHAR));
		if(!lsastrDomain.Buffer)
		{
            err_handler(EvtLogName, _T("GrantSelectedPrivileges LocalAlloc Failed"));
			__leave;
		}
		ZeroMemory(lsastrDomain.Buffer, MAX_PATH * sizeof(TCHAR));
		lsastrDomain.MaximumLength = MAX_PATH * sizeof(TCHAR);
		if(pszDomainName && ::lstrlen(pszDomainName) > 0)
		{
			::lstrcpy((LPTSTR)lsastrDomain.Buffer, pszDomainName);
			lsastrDomain.Length = ::lstrlen(pszDomainName) * sizeof(TCHAR);
		}
		else
		{
			lsastrDomain.Length = 0;
		}

		lsastrPriv[0].Buffer = (PWSTR)::LocalAlloc(LPTR, MAX_PATH * sizeof(TCHAR));
		if(!lsastrPriv[0].Buffer)
		{
            err_handler(EvtLogName, _T("GrantSelectedPrivileges LocalAlloc Failed"));
			__leave;
		}
		ZeroMemory(lsastrPriv[0].Buffer, MAX_PATH * sizeof(TCHAR));
		lsastrPriv[0].MaximumLength = MAX_PATH * sizeof(TCHAR);
		if(szPriv && ::lstrlen(szPriv) > 0)
		{
			::lstrcpy((LPTSTR)lsastrPriv[0].Buffer, szPriv);
			lsastrPriv[0].Length = ::lstrlen(szPriv) * sizeof(TCHAR);
		}
		else
		{
			lsastrPriv[0].Length = 0;
		}
		        
        ntStatus = LsaOpenPolicy(
			(pszDomainName && ::lstrlen(pszDomainName) > 0) ? &lsastrDomain : NULL,
			&lsaOA,
			POLICY_VIEW_LOCAL_INFORMATION | POLICY_LOOKUP_NAMES
            | POLICY_CREATE_ACCOUNT, &hPolicy);
        ULONG lErr = LsaNtStatusToWinError(ntStatus);
		if (lErr != ERROR_SUCCESS) 
		{
			err_handler(EvtLogName, _T("GrantSelectedPrivileges LsaOpenPolicy Failed"));
			__leave;		
		}

		if (fGrant)
			ntStatus = LsaAddAccountRights(hPolicy, pSIDUser, lsastrPriv,
			      1);
        else
            ntStatus = LsaRemoveAccountRights(hPolicy, pSIDUser, FALSE,
                  lsastrPriv, 1);

        // Check errors
        lErr = LsaNtStatusToWinError(ntStatus);
        if(lErr == ERROR_SUCCESS)
		{}
        else
        {
			err_handler(EvtLogName, _T("GrantSelectedPrivileges LsaAddAccountRights/LsaRemoveAccountRights Failed"));
			__leave;
		}		
	}
	__finally
	{
		if(pSIDUser) ::FreeSid(pSIDUser);
		if(lsastrDomain.Buffer)
		{
			::LocalFree(lsastrDomain.Buffer);
			lsastrDomain.Buffer = NULL;
		}
		if(lsastrPriv[0].Buffer)
		{
			::LocalFree(lsastrPriv[0].Buffer);
			lsastrPriv[0].Buffer = NULL;
		}
		if(hPolicy != NULL) ::LsaClose(hPolicy);
		if(!bSuccess)
		{
			err_handler(EvtLogName, _T("GrantSelectedPrivileges exception caught in __finally"));
		}
		return bSuccess;		
	}    
}

BOOL EnablePrivilege(PTSTR szPriv, BOOL fEnabled) 
{
	HANDLE hToken   = NULL;
    BOOL   fSuccess = FALSE;

    __try
	{
		// First lookup the system unique luid for the privilege
        LUID luid;
        if(!LookupPrivilegeValue(NULL, szPriv, &luid)) 
		{
			// If the name is bogus...
            __leave;
		}

        // Then get the processes token
        if(!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES,
            &hToken)) 
		{
			__leave;
		}

        // Set up our token privileges "array" (in our case an array of one)
        TOKEN_PRIVILEGES tp;
        tp.PrivilegeCount             = 1;
        tp.Privileges[0].Luid         = luid;
        tp.Privileges[0].Attributes = fEnabled ? SE_PRIVILEGE_ENABLED : 0;

        // Adjust our token privileges by enabling or disabling this one
        if(!AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES),
            NULL, NULL)) 
		{
			__leave;
		}
        fSuccess = TRUE;
    }
    __finally
	{
		// Cleanup
        if (hToken != NULL)
			CloseHandle(hToken);
        return(fSuccess);
	}
}



//#define NoMessageBox

//Note1 : All parameter can be null except pszExe and pszDesktop
//Note2 : The caller must NOT have window/hook on current desktop when 
//        the launched program is running an non-existing desktop 
//Note3:  Caller need take_ownership priv when approaching certain WinStat and Desk
//Note4:  Though you can include "\\" in the name of WinStation or Desktop
//        here "\\" internally used as the delimit, so do not misuse "\\" !
//Note5:  Caller need other 4 priv to call LogOnUser API when bCreateTokenDirectly = FALSE,
//        INCREASE_QUOTA, ASSIGN_PRIMARY_TOKEN, CHANGE_NOTIFY, and TCB.
//        It must be Granted before the caller user log on Windows.
//        Take care here: EnablePriv just enable/disable 
//        an existing priv while GrantPriv will take effect when the user relog
BOOL RunAsUser(LPTSTR pszEXE, LPTSTR pszCmdLine, 
			LPTSTR pszDomainName, LPTSTR pszUserName, LPTSTR pszPassword,
			LPTSTR pszDesktop, BOOL bCreateTokenDirectly, DWORD dwSession,
			BOOL bLoadProfile, BOOL bCopyTokenPropFromCaller, BOOL bKeepPriv, 
			DWORD dwLogonType, DWORD dwLogonProvider) 
{
	//LocalSystem no profile
	if(pszUserName == NULL && bLoadProfile) return FALSE;

	TCHAR szSrcWinStat[MAX_PATH];
	TCHAR szSrcDesktop[MAX_PATH];
	TCHAR szWinStat[MAX_PATH];
	TCHAR szDesktop[MAX_PATH];
	
	HWINSTA hSrcWinStat = ::GetProcessWindowStation();
	HDESK   hSrcDesktop = ::GetThreadDesktop(::GetCurrentThreadId());
	DWORD dwFakeLen;

	//Get Target User Token --> his.her SID
	HANDLE hToken   = NULL;

    BOOL   fProcess = FALSE;
    BOOL   fSuccess = FALSE;
    PROCESS_INFORMATION pi = {NULL, NULL, 0, 0};
    STARTUPINFO si;

	PSECURITY_DESCRIPTOR pSD = NULL; 
	BOOL bRet = FALSE;

	//save current win station and desktop to make return journey smoothly if ...
	HWINSTA hwinstaOld = NULL; 
	HDESK hdeskOld = NULL; 
	HWINSTA hwinstaNew = NULL; 
	HDESK hdeskNew = NULL; 

	BOOL  bWinStatCreated = FALSE;
	BOOL  bDeskCreated = FALSE;

	BOOL bSameWinStat = FALSE;
	BOOL bSameDesktop = FALSE;

	HANDLE hTokenSelf = NULL;
	if(!OpenProcessToken( GetCurrentProcess(), TOKEN_QUERY, &hTokenSelf)) 
	{
		err_handler(EvtLogName, _T("RunAsUser OpenProcessToken On Self Failed"));
		return FALSE;
	}

	PROFILEINFO profInfo = { sizeof(profInfo), 0, pszUserName };
	void* pEnvBlock = NULL;

	CreateEnvironmentBlock  _CreateEnvironmentBlock;
    DestroyEnvironmentBlock _DestroyEnvironmentBlock;
    LoadUserProfileW        _LoadUserProfileW;
    UnloadUserProfile       _UnloadUserProfile;
	HMODULE hEvnModule = NULL;
	if (bLoadProfile && (hEvnModule = LoadLibrary(_T("userenv.dll"))) == NULL)
	{
        err_handler(EvtLogName, _T("RunAsUser LoadLibrary UserEnv.DLL Failed"));
		return FALSE;
	}
	
	if(hEvnModule)
	{
		//this unlikely fails
		_CreateEnvironmentBlock = reinterpret_cast<CreateEnvironmentBlock>
			(GetProcAddress(hEvnModule, "CreateEnvironmentBlock"));
		_DestroyEnvironmentBlock = reinterpret_cast<DestroyEnvironmentBlock>
			(GetProcAddress(hEvnModule, "DestroyEnvironmentBlock"));
		_LoadUserProfileW = reinterpret_cast<LoadUserProfileW>
			(GetProcAddress(hEvnModule, "LoadUserProfileW"));
		_UnloadUserProfile = reinterpret_cast<UnloadUserProfile>
			(GetProcAddress(hEvnModule, "UnloadUserProfile"));	
		if(!_CreateEnvironmentBlock || !_DestroyEnvironmentBlock ||
			!_LoadUserProfileW || !_UnloadUserProfile)
		{
			err_handler(EvtLogName, _T("RunAsUser GetProcAddress UserEnv.DLL Failed"));
			return FALSE;
		}
	}

	BOOL bNullDesktop = FALSE;
	BOOL bEmptyDesktop = FALSE;
	//a long try_finally block
	__try
	{
		if(!IsAdministrorMember())
		{
            err_handler(EvtLogName, _T("RunAsUser IsAdministrorMember Failed"));
			__leave;
		}

		//Though Not all the time SeTakeOwnershipPrivilege is needed, to be safe, and since
		//only Admin member come here, it will pass anyway...
		if(!::EnablePrivilege(L"SeTakeOwnershipPrivilege", TRUE))
		{
            err_handler(EvtLogName, _T("RunAsUser EnablePrivilege SeTakeOwnershipPrivilege Failed"));
			__leave;
		}

		EnablePrivilege(L"SeTcbPrivilege", TRUE);
		EnablePrivilege(L"SeChangeNotifyPrivilege", TRUE);
		EnablePrivilege(L"SeIncreaseQuotaPrivilege", TRUE);
		EnablePrivilege(L"SeAssignPrimaryTokenPrivilege", TRUE);
		EnablePrivilege(L"SeCreateTokenPrivilege", TRUE);
		
		//get caller thread's WinStat and Desktop, used to decide if SetProcessWinStat is needed
		bRet = GetUserObjectInformation(
			hSrcWinStat,            // handle to object
			UOI_NAME,             // type of information to retrieve
			(LPVOID)szSrcWinStat,           // information buffer
			MAX_PATH * sizeof(TCHAR),          // size of the buffer
			&dwFakeLen // receives required buffer size
		);
		if(!bRet || dwFakeLen > MAX_PATH * sizeof(TCHAR))
		{
            err_handler(EvtLogName, _T("RunAsUser GetUserObjectInformation On Current WinStat Failed"));
			__leave;
		}

		bRet = GetUserObjectInformation(
			hSrcDesktop,            // handle to object
			UOI_NAME,             // type of information to retrieve
			(LPVOID)szSrcDesktop,           // information buffer
			MAX_PATH * sizeof(TCHAR),          // size of the buffer
			&dwFakeLen // receives required buffer size
		);
		if(!bRet || dwFakeLen > MAX_PATH * sizeof(TCHAR)) 
		{
            err_handler(EvtLogName, _T("RunAsUser GetUserObjectInformation On Current Desktop Failed"));
			__leave;	
		}
	    
		if(pszDesktop == NULL) //|| _tcsstr(pszDesktop, _T("\\")) == NULL)
		{
			::lstrcpy(szWinStat, szSrcWinStat);
			::lstrcpy(szDesktop, szSrcDesktop);
		}
		else if(::lstrcmpi(pszDesktop, _T("NULL")) == 0)
		{
			bNullDesktop = TRUE;
		}
        else if(::lstrcmpi(pszDesktop, _T("EMPTY")) == 0)
		{
			bEmptyDesktop = TRUE;
		}
		else
		{
			//check the integrity of the pszDesktop
			TCHAR* pSlash1 = _tcsstr(pszDesktop, _T("\\"));
			TCHAR* pSlash2 = _tcsrchr(pszDesktop, TCHAR('\\')); 
			if(pSlash1 != pSlash2) return FALSE;
		
			TCHAR* psz = (TCHAR*)pszDesktop;
			::lstrcpyn(szWinStat, (LPCTSTR)pszDesktop, pSlash1 - psz + 1);
			::lstrcpy(szDesktop, pSlash1 + 1);
		}

		if(!bNullDesktop && !bEmptyDesktop)
		{
			if(::lstrcmp(szWinStat, szSrcWinStat) == 0) //same winstat
			{
				bSameWinStat = TRUE;
				if(::lstrcmp(szDesktop, szSrcDesktop) == 0) //same desktop
					bSameDesktop = TRUE;
				else
					bSameDesktop = FALSE;
			}
			else
			{
				bSameWinStat = FALSE;
				bSameDesktop = FALSE;
			}
			
			if(!bSameDesktop || !bSameWinStat)
			{
				//for quick revertion
				hwinstaOld = GetProcessWindowStation(); 
				hdeskOld = GetThreadDesktop(GetCurrentThreadId()); 
			}
		}

    	if(!bNullDesktop && !bEmptyDesktop && !bSameWinStat)
		{
			//To Test The existing of a WinStat, you can
			//1. EnumWindowStations and compare the string returned
			//2. Call OpenWindowStation with WINSTA_ENUMERATE and test the handle

			
			//Way 2: 
			//Since the caller func is from a Admin Grp Member, this call is always OK unless WinStat not exists
			::SetLastError(ERROR_SUCCESS);
			hwinstaNew = ::OpenWindowStation(szWinStat, FALSE, WINSTA_ENUMERATE); 
			if(!hwinstaNew) 
			{
				//winstat not existing
				::CloseWindowStation(hwinstaNew);
				bWinStatCreated = TRUE;
			}
			else if(::GetLastError() == ERROR_ACCESS_DENIED)
			{
                err_handler(EvtLogName, _T("RunAsUser OpenWindowStation with WINSTA_ENUMERATE Failed"));
				__leave;
			}
			else
			{
				bWinStatCreated = FALSE;
			}
		}
		

		if(pszUserName == NULL || ::lstrlen(pszUserName) == 0 ) //LogOn as LocalSystem
		{
            if(!bCreateTokenDirectly)
			{
				hToken = GetLSAToken();
                if(hToken == NULL) 
				{
                   err_handler(EvtLogName, _T("RunAsUser Get System Token GetLSAToken Failed"));
				    __leave;
				}
			}
			else
			{
				if(!CreateTokenDirectlyEx(hToken, bCopyTokenPropFromCaller, pszUserName, pszDomainName, 
						 "RunAsEx+", NULL, NULL, NULL, TRUE, bKeepPriv, FALSE, NULL, NULL, NULL, dwLogonType, dwLogonProvider) ||
						 hToken == NULL)
				{
                    err_handler(EvtLogName, _T("RunAsUser Get System Token CreateTokenDirectlyEx Failed"));
				    __leave;
				}
			}
		}
	    else
		{
			if(!bCreateTokenDirectly && !LogonUser(pszUserName, pszDomainName, pszPassword,
               dwLogonType, dwLogonProvider, &hToken))
			{
                err_handler(EvtLogName, _T("RunAsUser LogonUser Failed"));
				__leave;
			}
			else if(bCreateTokenDirectly && !CreateTokenDirectlyEx(hToken, bCopyTokenPropFromCaller, pszUserName, pszDomainName, 
						 "RunAsEx+", NULL, NULL, NULL, TRUE, bKeepPriv, FALSE, NULL, NULL, NULL, dwLogonType, dwLogonProvider))
			{
                err_handler(EvtLogName, _T("RunAsUser CreateTokenDirectly Failed"));
				__leave;
			}
		}

		
		//Set Token Seesion ID
		if(dwSession != (DWORD)-1)
		{
			//need to set?
			DWORD dwSelfSession;
			if(ProcessIdToSessionId(::GetCurrentProcessId(), &dwSelfSession))
			{
				if(dwSelfSession != dwSession)
				{
					if(!SetTokenInformation(hToken, TokenSessionId, &dwSession, sizeof(DWORD)))
					{
						if(GetLastError() == ERROR_ACCESS_DENIED)
						{
							//try again
                            if (!ModifySecurity(hToken, TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY
                                   | TOKEN_QUERY | TOKEN_ADJUST_SESSIONID)) 
							{
								err_handler(EvtLogName, _T("RunAsUser ModifySecurity Failed"));
			                    __leave;
							}
							if(!SetTokenInformation(hToken, TokenSessionId, &dwSession, sizeof(DWORD)))
							{
								err_handler(EvtLogName, _T("RunAsUser SetTokenInformation TokenSessionId 2 Failed"));
						        __leave;
							}
						}
						else
						{
							err_handler(EvtLogName, _T("RunAsUser SetTokenInformation TokenSessionId 1 Failed"));
						    __leave;
						}
					}
				}
			}
		}
	
		pSD = HeapAlloc(GetProcessHeap(), 0, SECURITY_DESCRIPTOR_MIN_LENGTH);
        if(pSD == NULL) 
		{
			err_handler(EvtLogName, _T("RunAsUser HeapAlloc PSECURITY_DESCRIPTOR Failed"));
			__leave;
		}
		
		// We now have an empty security descriptor
        if (!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION))
		{
             err_handler(EvtLogName, _T("RunAsUser InitializeSecurityDescriptor Failed"));
		     __leave;
		}

	    if(!SetSecurityDescriptorDacl(pSD, TRUE, NULL, FALSE))
		{
             err_handler(EvtLogName, _T("RunAsUser SetSecurityDescriptorDacl Failed"));
		     __leave;
		}

        // Then we point to our SD from a SECURITY_ATTRIBUTES structure
        SECURITY_ATTRIBUTES sa = {0};
        sa.nLength = sizeof(sa);
        sa.lpSecurityDescriptor = pSD;
		
		if(!bNullDesktop && !bEmptyDesktop && bWinStatCreated)
		{
			//Create a WinStat and naturally a new desktop
		    //First make it a NULL DACL :=) 
		    hwinstaNew = CreateWindowStation(szWinStat, 0, MAXIMUM_ALLOWED, &sa);
		    //using default security is good here since we are the owner
            if(!hwinstaNew) __leave;
		    //We must SetProcessWindowStation when new desktop needs created on a different WinStat
            if(!SetProcessWindowStation(hwinstaNew))
			{
				::CloseWindowStation(hwinstaNew);
				hwinstaNew = NULL;
                err_handler(EvtLogName, _T("RunAsUser SetProcessWindowStation to hwinstaNew Failed"));
			    __leave;
			}
		    hdeskNew = ::CreateDesktop(szDesktop, NULL, NULL, 0, //or DF_ALLOWOTHERACCOUNTHOOK
				MAXIMUM_ALLOWED, &sa);
		    if(hdeskNew == NULL)
			{
				SetProcessWindowStation(hwinstaOld);
                ::CloseWindowStation(hwinstaNew);
				hwinstaNew = NULL;
                err_handler(EvtLogName, _T("RunAsUser CreateDesktop New Failed"));
				__leave;
			}

			if(!AllowTokenFullAccessToObject(hTokenSelf, hwinstaNew, SE_WINDOW_OBJECT, _T("WinStat")))
			{
                err_handler(EvtLogName, _T("RunAsUser AllowTokenFullAccessToObject for Self Token on hwinstaNew Failed"));
				__leave;
			}

			if(!AllowTokenFullAccessToObject(hTokenSelf, hdeskNew, SE_WINDOW_OBJECT, _T("Desktop")))
			{
                err_handler(EvtLogName, _T("RunAsUser AllowTokenFullAccessToObject for Self Token on hdeskNew Failed"));
				__leave;
			}

			if(!AllowTokenFullAccessToObject(hToken, hwinstaNew, SE_WINDOW_OBJECT, _T("WinStat")))
			{
                err_handler(EvtLogName, _T("RunAsUser AllowTokenFullAccessToObject for User Token on hwinstaNew Failed"));
				__leave;
			}

			if(!AllowTokenFullAccessToObject(hToken, hdeskNew, SE_WINDOW_OBJECT, _T("Desktop")))
			{
                err_handler(EvtLogName, _T("RunAsUser AllowTokenFullAccessToObject for User Token on hdeskNew Failed"));
				__leave;
			}
			//modify DACL of the winStat
			//modify DACL of the desktop
		}
	    else if(!bNullDesktop && !bEmptyDesktop)//the WinStat exists
		{
			hwinstaNew = OpenWindowStation(szWinStat, FALSE, READ_CONTROL | WRITE_DAC);
			if(hwinstaNew == NULL)
			{
                err_handler(EvtLogName, _T("RunAsUser Open WinStat With WRITE_DAC Failed"));
				__leave;
			}
            //give self such rights -- WINSTA_CREATEDESKTOP WINSTA_ENUMDESKTOPS
            //code here
			if(!bSameWinStat && !SetProcessWindowStation(hwinstaNew))
			{
                err_handler(EvtLogName, _T("RunAsUser SetProcessWindowStation to hwinstaNew Failed"));
				::CloseWindowStation(hwinstaNew);
				hwinstaNew = NULL;
			    __leave;
			}
			//if bSameWinStat --
			//if not          -- SetProcessWindowStation called

			//check the desk
			//Does the desktop exists?
			::SetLastError(ERROR_SUCCESS);
            hdeskNew = OpenDesktop(szDesktop, 0, //DF_ALLOWOTHERACCOUNTHOOK,
					FALSE, READ_CONTROL | WRITE_DAC);
			if(hdeskNew == NULL)
			{
				if(::GetLastError() == ERROR_ACCESS_DENIED)
				{
                    err_handler(EvtLogName, _T("RunAsUser Open Desktop With WRITE_DAC Failed"));
					__leave;
				}
				else //not existing
				{
					bDeskCreated = TRUE;
					hdeskNew = ::CreateDesktop(szDesktop, NULL, NULL, 0, //or DF_ALLOWOTHERACCOUNTHOOK
			         	MAXIMUM_ALLOWED, &sa);
		            if(hdeskNew == NULL)
					{
                        err_handler(EvtLogName, _T("RunAsUser CreateDesktop with MAXIMUM_ALLOWED Failed"));
    					__leave;
					}
				}
			}
			else
				bDeskCreated = FALSE;
			//modify DACL of the desk
			if(!AllowTokenFullAccessToObject(hTokenSelf, hwinstaNew, SE_WINDOW_OBJECT, _T("WinStat")))
			{
                err_handler(EvtLogName, _T("RunAsUser AllowTokenFullAccessToObject for Self Token on hwinstaNew Failed"));
				__leave;
			}
			if(!AllowTokenFullAccessToObject(hTokenSelf, hdeskNew, SE_WINDOW_OBJECT, _T("Desktop")))
			{
                err_handler(EvtLogName, _T("RunAsUser AllowTokenFullAccessToObject for Self Token on hdeskNew Failed"));
				__leave;
			}

			if(!AllowTokenFullAccessToObject(hToken, hwinstaNew, SE_WINDOW_OBJECT, _T("WinStat")))
			{
                err_handler(EvtLogName, _T("RunAsUser AllowTokenFullAccessToObject for User Token on hwinstaNew Failed"));
				__leave;
			}
			if(!AllowTokenFullAccessToObject(hToken, hdeskNew, SE_WINDOW_OBJECT, _T("Desktop")))
			{
                err_handler(EvtLogName, _T("RunAsUser AllowTokenFullAccessToObject for User Token on hdeskNew Failed"));
				__leave;
			}
		}
      
		if(pszUserName == NULL && bLoadProfile) __leave;
		
		//ready to launch
		if(bLoadProfile)
		{
			// load the user profile
	        //PROFILEINFO profInfo = { sizeof(profInfo), 0, pszUserName };
			if (!_LoadUserProfileW( hToken, &profInfo))
			{
                err_handler(EvtLogName, _T("RunAsUser LoadUserProfile Failed"));
				__leave;
			}
		
			// set up an environment block
	        //void* pEnvBlock = NULL;
	        if(!_CreateEnvironmentBlock( &pEnvBlock, hToken, FALSE))
			{
                err_handler(EvtLogName, _T("RunAsUser CreateEnvironmentBlock Failed"));
				__leave;
			}
		}

        si.cb          = sizeof(si);
		//Desktop or WinStat\Desktop //MSDN Error Here! Note: to be 100% safe use the latter!!!
		TCHAR szFullDesktop[MAX_PATH];
		::lstrcpy(szFullDesktop, szWinStat);
		::lstrcat(szFullDesktop, _T("\\"));
		::lstrcat(szFullDesktop, szDesktop);
		
		if(!bNullDesktop && !bEmptyDesktop)
		{
			si.lpDesktop   = szFullDesktop;
		}
		else if(bNullDesktop)
		{
			si.lpDesktop   = NULL;
		}
		else //if bEmptyDesktop
		{
			si.lpDesktop = _T("");
		}

        si.lpTitle     = NULL;
        si.dwFlags     = 0;
        si.cbReserved2 = 0;
        si.lpReserved  = NULL;
        si.lpReserved2 = NULL;

		TCHAR szLocalCmdLine[2 * MAX_PATH];
		::SetLastError(0);
		
		::lstrcpy(szLocalCmdLine, _T("\""));
		::lstrcat(szLocalCmdLine, pszEXE);
		::lstrcat(szLocalCmdLine, _T("\""));
		::lstrcat(szLocalCmdLine, _T(" "));
		if(pszCmdLine)
			::lstrcat(szLocalCmdLine, pszCmdLine);
		//LPCTSTR-->LPTSTR
	    fProcess = CreateProcessAsUser(hToken, NULL, (LPTSTR)szLocalCmdLine, 
				&sa, &sa, //lpProcessAttributes , lpThreadAttributes 
				FALSE, 
				bLoadProfile ? CREATE_UNICODE_ENVIRONMENT : 0,
                bLoadProfile ? pEnvBlock : NULL,
				NULL, &si, &pi);
		
        if(!fProcess)
		{
            err_handler(EvtLogName, _T("RunAsUser CreateProcessAsUser Failed"));
			__leave;
		}
		fSuccess = TRUE;
	}
    __finally
	{
		if(bLoadProfile && pEnvBlock)
			// free the environment block
	        _DestroyEnvironmentBlock(pEnvBlock);

		if(bLoadProfile && hToken && profInfo.hProfile)
			// unload the user profile
	        _UnloadUserProfile( hToken, profInfo.hProfile );

		if(pSD) HeapFree(GetProcessHeap(), 0, pSD);
		if(hToken) CloseHandle(hToken);

		//if(!bSameWinStat && hwinstaNew) ::SetProcessWindowStation(hwinstaOld);
		if(hwinstaOld) ::SetProcessWindowStation(hwinstaOld);
		if(hdeskOld) ::SetThreadDesktop(hdeskOld);
			
		//if(hdeskNew) ::CloseDesktop(hdeskNew);
		//if(hwinstaNew) ::CloseWindowStation(hwinstaNew);
				
        if (fProcess) 
		{
			CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
		}
		
	    if(hdeskOld) ::CloseDesktop(hdeskOld);
	    if(hwinstaOld) ::CloseWindowStation(hwinstaOld); 

		if(!fSuccess)
		{
			err_handler(EvtLogName, _T("RunAsUser exception caught in __finally"));
		}
	}

    return(fSuccess);
}

///////////////////////////////////////////////////////////////////////////////
HANDLE OpenSystemProcess() 
{
	HANDLE hSnapshot = NULL;
    HANDLE hProc     = NULL;

    __try
	{
		// Get a snapshot of the processes in the system
        hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        if (hSnapshot == NULL)
		{
            err_handler(EvtLogName, _T("OpenSystemProcess CreateToolhelp32Snapshot Failed"));
			__leave;
		}

        PROCESSENTRY32 pe32;
        pe32.dwSize = sizeof(pe32);

        // Find the "System" process
        BOOL fProcess = Process32First(hSnapshot, &pe32);
        while (fProcess && (lstrcmpi(pe32.szExeFile, TEXT("SYSTEM")) != 0))
			fProcess = Process32Next(hSnapshot, &pe32);
        if (!fProcess)
		{
			err_handler(EvtLogName, _T("OpenSystemProcess Not Found SYSTEM"));
            __leave;    // Didn't find "System" process
		}

        // Open the process with PROCESS_QUERY_INFORMATION access
        hProc = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE,
            pe32.th32ProcessID);
        if (hProc == NULL)
		{
			err_handler(EvtLogName, _T("OpenSystemProcess OpenProcess Failed"));
			__leave;
		}
    } 
    __finally
	{
		// Cleanup the snapshot
       if (hSnapshot != NULL)
		   CloseHandle(hSnapshot);
       return(hProc);
    }
}
///////////////////////////////////////////////////////////////////////////////
HANDLE GetLSAToken() 
{
	HANDLE hProc  = NULL;
    HANDLE hToken = NULL;
    BOOL bSuccess = FALSE;
    __try
	{
		// Enable the SE_DEBUG_NAME privilege in our process token
        if (!EnablePrivilege(SE_DEBUG_NAME, TRUE)) 
		{
			err_handler(EvtLogName, _T("GetLSAToken EnablePrivilege Failed"));
			__leave;
		}

        // Retrieve a handle to the "System" process
        hProc = OpenSystemProcess();
        if(hProc == NULL) 
		{
			err_handler(EvtLogName, _T("GetLSAToken OpenSystemProcess Failed"));
			__leave;
		}

        // Open the process token with READ_CONTROL and WRITE_DAC access.  We
        // will use this access to modify the security of the token so that we
        // retrieve it again with a more complete set of rights.
        BOOL fResult = OpenProcessToken(hProc, READ_CONTROL | WRITE_DAC,
            &hToken);
        if(FALSE == fResult)  
		{
			err_handler(EvtLogName, _T("GetLSAToken OpenProcessToken Failed"));
			__leave;
		}

        // Add an ace for the current user for the token.  This ace will add
        // TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY | TOKEN_QUERY rights.
        if (!ModifySecurity(hToken, TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY
            | TOKEN_QUERY | TOKEN_ADJUST_SESSIONID)) 
		{
			err_handler(EvtLogName, _T("GetLSAToken ModifySecurity Failed"));
			__leave;
		}
		

        // Reopen the process token now that we have added the rights to
        // query the token, duplicate it, and assign it.
        fResult = OpenProcessToken(hProc, TOKEN_QUERY | TOKEN_DUPLICATE
            | TOKEN_ASSIGN_PRIMARY | READ_CONTROL | WRITE_DAC, &hToken);
        if (FALSE == fResult)  
		{
			err_handler(EvtLogName, _T("GetLSAToken OpenProcessToken Failed"));
			__leave;
		}
		bSuccess = TRUE;
    } 
	__finally
	{
		// Close the System process handle
        if (hProc != NULL)	CloseHandle(hProc);
		if(bSuccess)
			return hToken;
		else
		{
			::CloseHandle(hToken);
			return NULL;
		}
	}
}

BOOL ModifySecurity(HANDLE hProc, DWORD dwAccess) 
{
	PACL pAcl        = NULL;
    PACL pNewAcl     = NULL;
    PACL pSacl       = NULL;
    PSID pSidOwner   = NULL;
    PSID pSidPrimary = NULL;
    BOOL fSuccess    = TRUE;

    PSECURITY_DESCRIPTOR pSD = NULL;

    __try 
	{
		// Find the length of the security object for the kernel object
        DWORD dwSDLength;
        if (GetKernelObjectSecurity(hProc, DACL_SECURITY_INFORMATION, pSD, 0,
            &dwSDLength) || (GetLastError() != ERROR_INSUFFICIENT_BUFFER))
		{
            err_handler(EvtLogName, _T("ModifySecurity GetKernelObjectSecurity Size Failed"));
			__leave;
		}

        // Allocate a buffer of that length
        pSD = LocalAlloc(LPTR, dwSDLength);
        if (pSD == NULL)
		{
			err_handler(EvtLogName, _T("ModifySecurity LocalAlloc Failed"));
			__leave;
		}

        // Retrieve the kernel object
        if (!GetKernelObjectSecurity(hProc, DACL_SECURITY_INFORMATION, pSD,
            dwSDLength, &dwSDLength))
		{
			err_handler(EvtLogName, _T("ModifySecurity GetKernelObjectSecurity Failed"));
            __leave;
		}

        // Get a pointer to the DACL of the SD
        BOOL fDaclPresent;
        BOOL fDaclDefaulted;
        if (!GetSecurityDescriptorDacl(pSD, &fDaclPresent, &pAcl,
            &fDaclDefaulted))
		{
			err_handler(EvtLogName, _T("ModifySecurity GetSecurityDescriptorDacl Failed"));
            __leave;
		}

        // Get the current user's name
        TCHAR szName[1024];
        DWORD dwLen = chDIMOF(szName);
        if (!GetUserName(szName, &dwLen))
		{
			err_handler(EvtLogName, _T("ModifySecurity GetUserName Failed"));
			__leave;
		}
 
        // Build an EXPLICIT_ACCESS structure for the ace we wish to add.
        EXPLICIT_ACCESS ea;
        BuildExplicitAccessWithName(&ea, szName, dwAccess, GRANT_ACCESS, 0);
        ea.Trustee.TrusteeType = TRUSTEE_IS_USER;

        // We are allocating a new ACL with a new ace inserted.  The new
        // ACL must be LocalFree'd
        if(ERROR_SUCCESS != SetEntriesInAcl(1, &ea, pAcl, &pNewAcl)) 
		{
			err_handler(EvtLogName, _T("ModifySecurity SetEntriesInAcl Failed"));
			pNewAcl = NULL;
            __leave;
		}

        // Find the buffer sizes we would need to make our SD absolute
        pAcl               = NULL;
        dwSDLength         = 0;
        DWORD dwAclSize    = 0;
        DWORD dwSaclSize   = 0;
        DWORD dwSidOwnLen  = 0;
        DWORD dwSidPrimLen = 0;
        PSECURITY_DESCRIPTOR pAbsSD = NULL;
        if(MakeAbsoluteSD(pSD, pAbsSD, &dwSDLength, pAcl, &dwAclSize, pSacl,
            &dwSaclSize, pSidOwner, &dwSidOwnLen, pSidPrimary, &dwSidPrimLen)
			|| (GetLastError() != ERROR_INSUFFICIENT_BUFFER))
		{
			err_handler(EvtLogName, _T("ModifySecurity MakeAbsoluteSD Size Failed"));
			__leave;
		}

        // Allocate the buffers
        pAcl = (PACL) LocalAlloc(LPTR, dwAclSize);
        pSacl = (PACL) LocalAlloc(LPTR, dwSaclSize);
        pSidOwner = (PSID) LocalAlloc(LPTR, dwSidOwnLen);
        pSidPrimary = (PSID) LocalAlloc(LPTR, dwSidPrimLen);
        pAbsSD = (PSECURITY_DESCRIPTOR) LocalAlloc(LPTR, dwSDLength);
        if(!(pAcl && pSacl && pSidOwner && pSidPrimary && pAbsSD))
		{
			err_handler(EvtLogName, _T("ModifySecurity Invalid SID Found"));
            __leave;
		}

        // And actually make our SD absolute
        if(!MakeAbsoluteSD(pSD, pAbsSD, &dwSDLength, pAcl, &dwAclSize, pSacl,
            &dwSaclSize, pSidOwner, &dwSidOwnLen, pSidPrimary, &dwSidPrimLen))
		{
			err_handler(EvtLogName, _T("ModifySecurity MakeAbsoluteSD Failed"));
            __leave;
		}

        // Now set the security descriptor DACL
        if(!SetSecurityDescriptorDacl(pAbsSD, fDaclPresent, pNewAcl,
            fDaclDefaulted))
		{
			err_handler(EvtLogName, _T("ModifySecurity SetSecurityDescriptorDacl Failed"));
            __leave;
		}

        // And set the security for the object
        if(!SetKernelObjectSecurity(hProc, DACL_SECURITY_INFORMATION, pAbsSD))
		{
			err_handler(EvtLogName, _T("ModifySecurity SetKernelObjectSecurity Failed"));
            __leave;
		}

        fSuccess = TRUE;

    } 
	__finally
	{
		// Cleanup
        if (pNewAcl == NULL)
			LocalFree(pNewAcl);

        if (pSD == NULL)
			LocalFree(pSD);

        if (pAcl == NULL)
			LocalFree(pAcl);

        if (pSacl == NULL)
			LocalFree(pSacl);

        if (pSidOwner == NULL)
			LocalFree(pSidOwner);

        if (pSidPrimary == NULL)
			LocalFree(pSidPrimary);

		if(!fSuccess)
		{
			err_handler(EvtLogName, _T("ModifySecurity exception caught in __finally"));
		}

        return(fSuccess);
	}
}

void DumpACL( PACL pACL ){
   __try{
      if (pACL == NULL){
         _tprintf(TEXT("NULL DACL\n"));
         __leave;
      }

      ACL_SIZE_INFORMATION aclSize = {0};
      if (!GetAclInformation(pACL, &aclSize, sizeof(aclSize),
         AclSizeInformation))
         __leave;
      _tprintf(TEXT("ACL ACE count: %d\n"), aclSize.AceCount);
      
      struct{
         BYTE  lACEType;
         PTSTR pszTypeName;
      }aceTypes[6] = {
         {ACCESS_ALLOWED_ACE_TYPE, TEXT("ACCESS_ALLOWED_ACE_TYPE")},
         {ACCESS_DENIED_ACE_TYPE, TEXT("ACCESS_DENIED_ACE_TYPE")},
         {SYSTEM_AUDIT_ACE_TYPE, TEXT("SYSTEM_AUDIT_ACE_TYPE")},
         {ACCESS_ALLOWED_OBJECT_ACE_TYPE,
            TEXT("ACCESS_ALLOWED_OBJECT_ACE_TYPE")},
         {ACCESS_DENIED_OBJECT_ACE_TYPE,
            TEXT("ACCESS_DENIED_OBJECT_ACE_TYPE")},
         {SYSTEM_AUDIT_OBJECT_ACE_TYPE,
            TEXT("SYSTEM_AUDIT_OBJECT_ACE_TYPE")}};

      struct{
         ULONG lACEFlag;
         PTSTR pszFlagName;
      }aceFlags[7] = {
         {INHERITED_ACE, TEXT("INHERITED_ACE")},
         {CONTAINER_INHERIT_ACE, TEXT("CONTAINER_INHERIT_ACE")},
         {OBJECT_INHERIT_ACE, TEXT("OBJECT_INHERIT_ACE")},
         {INHERIT_ONLY_ACE, TEXT("INHERIT_ONLY_ACE")},
         {NO_PROPAGATE_INHERIT_ACE, TEXT("NO_PROPAGATE_INHERIT_ACE")},
         {FAILED_ACCESS_ACE_FLAG, TEXT("FAILED_ACCESS_ACE_FLAG")},
         {SUCCESSFUL_ACCESS_ACE_FLAG, 
            TEXT("SUCCESSFUL_ACCESS_ACE_FLAG")}};

      for (ULONG lIndex = 0;lIndex < aclSize.AceCount;lIndex++){
         ACCESS_ALLOWED_ACE* pACE;
         if (!GetAce(pACL, lIndex, (PVOID*)&pACE))
            __leave;

         _tprintf(TEXT("\nACE #%d\n"), lIndex);
         
         ULONG lIndex2 = 6;
         PTSTR pszString = TEXT("Unknown ACE Type");
         while (lIndex2--){
            if(pACE->Header.AceType == aceTypes[lIndex2].lACEType){
               pszString = aceTypes[lIndex2].pszTypeName;
            }
         }
         _tprintf(TEXT("  ACE Type =\n  \t%s\n"), pszString);

         _tprintf(TEXT("  ACE Flags = \n"));
         lIndex2 = 7;
         while (lIndex2--){
            if ((pACE->Header.AceFlags & aceFlags[lIndex2].lACEFlag) 
               != 0)
               _tprintf(TEXT("  \t%s\n"), 
                  aceFlags[lIndex2].pszFlagName);
         }

         _tprintf(TEXT("  ACE Mask (32->0) =\n  \t"));
         lIndex2 = (ULONG)1<<31;
         while (lIndex2){
            _tprintf(((pACE->Mask & lIndex2) != 0)?TEXT("1"):TEXT("0"));
            lIndex2>>=1;
         }

         TCHAR szName[1024];
         TCHAR szDom[1024];
         PSID pSID = PSIDFromPACE(pACE);
         SID_NAME_USE sidUse;         
         ULONG lLen1 = 1024, lLen2 = 1024;
         if (!LookupAccountSid(NULL, pSID, 
            szName, &lLen1, szDom, &lLen2, &sidUse))
            lstrcpy(szName, TEXT("Unknown"));
         PTSTR pszSID;
         if (!ConvertSidToStringSid(pSID, &pszSID))
            __leave;
         _tprintf(TEXT("\n  ACE SID =\n  \t%s (%s)\n"), pszSID, szName);
         LocalFree(pszSID);
      }
   }__finally{}
}

ULONG CalculateACLSize( PACL pACLOld, PSID* ppSidArray, int nNumSids, 
   PACE_UNION* ppACEs, int nNumACEs )
{
   ULONG lACLSize = 0;
   BOOL bSuccess = FALSE;
   __try
   {
      // If we are including an existing ACL, then find its size
      if (pACLOld != NULL){
         ACL_SIZE_INFORMATION aclSize;
         if(!GetAclInformation(pACLOld, &aclSize, sizeof(aclSize), 
            AclSizeInformation))
		 {
			 err_handler(EvtLogName, _T("CalculateACLSize GetAclInformation Failed"));
			 __leave;
         }
         lACLSize = aclSize.AclBytesInUse;
      }

      if (ppSidArray != NULL){
         // Step through each SID
         while (nNumSids--){
            // If a SID isn't valid, then we bail
            if (!IsValidSid(ppSidArray[nNumSids]))
			{
               lACLSize = 0;
               err_handler(EvtLogName, _T("CalculateACLSize IsValidSid Failed"));
			   __leave;
            }
            // Get the SID's length
            lACLSize += GetLengthSid(ppSidArray[nNumSids]);
            // Add the ACE structure size, minus the 
            // size of the SidStart member
            lACLSize += sizeof(ACCESS_ALLOWED_ACE) - 
               sizeof(((ACCESS_ALLOWED_ACE*)0)->SidStart);         
         }      
      }

      if (ppACEs != NULL){
         // Step through each ACE
         while (nNumACEs--){            
            // Get the SIDs length
            lACLSize += ppACEs[nNumACEs]->aceHeader.AceSize;
         }      
      }         
      // Add in the ACL structure itself
      lACLSize += sizeof(ACL);
	  bSuccess = TRUE;
   }
   __finally
   {
	   if(bSuccess)
		   return lACLSize;
	   else
		   return 0;
   }
}

PACE_UNION AllocateACE( ULONG bACEType, ULONG bACEFlags, 
   ULONG lAccessMask, PSID pSID )
{
   PACE_UNION pReturnACE = NULL;
   PBYTE pbBuffer = NULL;
   __try
   {
      // Get the offset of the SID in the ACE
      ULONG lSIDOffset = (ULONG)(&((ACCESS_ALLOWED_ACE*)0)->SidStart);
      // Get the size of the ACE without the SID
      ULONG lACEStructSize = sizeof(ACCESS_ALLOWED_ACE) - 
         sizeof(((ACCESS_ALLOWED_ACE*)0)->SidStart);
      // Get the length of the SID
      ULONG lSIDSize = GetLengthSid(pSID);      

      // Allocate a buffer for the ACE
      pbBuffer = (PBYTE)LocalAlloc(LPTR, lACEStructSize + lSIDSize);
      if (pbBuffer == NULL)
	  {
		  err_handler(EvtLogName, _T("AllocateACE LocalAlloc Failed"));
          __leave;
	  }

      // Copy the SID into the ACE
      if(!CopySid(lSIDSize, (PSID)(pbBuffer+lSIDOffset), pSID))
	  {
		  err_handler(EvtLogName, _T("AllocateACE CopySid Failed"));
		  __leave;
      }      
      pReturnACE = (PACE_UNION) pbBuffer;
      pReturnACE->aceHeader.AceSize = (USHORT)(lACEStructSize + lSIDSize);
      pReturnACE->aceHeader.AceType = (BYTE)bACEType;
      pReturnACE->aceHeader.AceFlags = (BYTE)bACEFlags;
      pReturnACE->aceAllowed.Mask = lAccessMask;
   }
   __finally
   {
	   // Free the buffer in an error case
       if (pbBuffer != (PBYTE)pReturnACE)
		   LocalFree(pbBuffer);
       return (pReturnACE);    
   }
}

BOOL IsEqualACE( PACE_UNION pACE1, PACE_UNION pACE2 )
{
   BOOL fReturn = FALSE;
  
   __try
   {
      if(pACE1->aceHeader.AceType != pACE2->aceHeader.AceType)
		  __leave;

      // Get the offset of the SID in the ACE
      ULONG lSIDOffset = (ULONG)(&((ACCESS_ALLOWED_ACE*)0)->SidStart);
      // Get the size of the ACE without the SID
      ULONG lACEStructSize = sizeof(ACCESS_ALLOWED_ACE) - 
         sizeof(((ACCESS_ALLOWED_ACE*)0)->SidStart);

      PBYTE pbACE1 = (PBYTE)pACE1;
      PBYTE pbACE2 = (PBYTE)pACE2;
      fReturn = TRUE;
      while(lACEStructSize--)
         fReturn = (fReturn && ((pbACE1[lACEStructSize] ==
            pbACE2[lACEStructSize])));
      // Check SIDs
      fReturn = fReturn && EqualSid((PSID)(pbACE1+lSIDOffset),
         (PSID)(pbACE2+lSIDOffset));      
   }
   __finally
   {
      return (fReturn);
   }
}

int FindACEInACL( PACL pACL, PACE_UNION pACE )
{
   int nACEIndex = -1;
   __try
   {
      ACL_SIZE_INFORMATION aclSize;
      if (!GetAclInformation(pACL, &aclSize, sizeof(aclSize), AclSizeInformation))
	  {
		  err_handler(EvtLogName, _T("FindACEInACL GetAclInformation Failed"));
          __leave;
      }

      while (aclSize.AceCount--)
	  {
         PACE_UNION pACETemp;
         if(!GetAce(pACL, aclSize.AceCount, (PVOID *)&pACETemp))
		 {
			 err_handler(EvtLogName, _T("FindACEInACL GetAce Failed"));
             __leave;
		 }

         if(IsEqualACE(pACETemp, pACE)){
            nACEIndex = (int)aclSize.AceCount;
            break;
         }
      }
   
   }
   __finally
   {
	   return (nACEIndex);
   }
}

BOOL CopyACL( PACL pACLDestination, PACL pACLSource )
{
   BOOL fReturn = FALSE;
   __try
   {
      // Get the number of ACEs in the source ACL
      ACL_SIZE_INFORMATION aclSize;
      if (!GetAclInformation(pACLSource, &aclSize, 
         sizeof(aclSize), AclSizeInformation))
	  {
          err_handler(EvtLogName, _T("CopyACL GetAclInformation Failed"));
          __leave;
      }

      // Use GetAce and AddAce to copy the ACEs
      for(ULONG lIndex=0;lIndex < aclSize.AceCount;lIndex++){
         ACE_HEADER* pACE;
         if(!GetAce(pACLSource, lIndex, (PVOID*)&pACE))
		 {
			 //ReportErr(_T("GetAce in CopyACL"));
			 err_handler(EvtLogName, _T("CopyACL GetAce Failed"));
             __leave;
		 }
         if(!AddAce(pACLDestination, ACL_REVISION, MAXDWORD, 
            (PVOID*)pACE, pACE->AceSize))
		 {
			 //ReportErr(_T("AddAce in CopyACL"));
			 err_handler(EvtLogName, _T("CopyACL AddAce Failed"));
			 __leave;
		 }
      }      
      fReturn = TRUE;
   }
   __finally
   {
	   return (fReturn);
   }
}

ULONG GetACEInsertionIndex( PACL pDACL, PACE_UNION pACENew)
{
   ULONG lIndex = (ULONG) -1;

   __try
   {
      // ACE types by ACL order
      ULONG lFilterType[] = { ACCESS_DENIED_ACE_TYPE, 
                              ACCESS_DENIED_OBJECT_ACE_TYPE, 
                              ACCESS_ALLOWED_ACE_TYPE, 
                              ACCESS_ALLOWED_OBJECT_ACE_TYPE};

      // Determine which group the new ACE should belong to
      ULONG lNewAceGroup;
      for(lNewAceGroup = 0; lNewAceGroup<4 ; lNewAceGroup++){
         if(pACENew->aceHeader.AceType == lFilterType[lNewAceGroup])
            break;
      }
      // If group == 4, the ACE type is no good
      if(lNewAceGroup == 4)
	  {
		  err_handler(EvtLogName, _T("GetACEInsertionIndex Bad Ace Type"));
          __leave;
	  }
      // If new ACE is an inherited ACE, then it goes after other ACEs
      if((pACENew->aceHeader.AceFlags & INHERITED_ACE) != 0)
         lNewAceGroup+=4;

      // Get ACE count
      ACL_SIZE_INFORMATION aclSize;
      if (!GetAclInformation(pDACL, &aclSize, 
         sizeof(aclSize), AclSizeInformation))
	  {
          err_handler(EvtLogName, _T("GetACEInsertionIndex GetAclInformation Failed"));
          __leave;
      }      

      // Iterate through ACEs
      lIndex = 0;
      for(lIndex = 0;lIndex < aclSize.AceCount;lIndex++)
	  {
         ACE_HEADER* pACE;
         if(!GetAce(pDACL, lIndex, (PVOID*)&pACE))
		 {
             err_handler(EvtLogName, _T("GetACEInsertionIndex GetAce Failed"));
             __leave;
		 }

         // Get the group of the ACL ACE
         ULONG lAceGroup;
         for(lAceGroup = 0; lAceGroup<4 ; lAceGroup++){
            if(pACE->AceType == lFilterType[lAceGroup])
               break;
         }
         // Test for bad ACE
         if(lAceGroup==4)
		 {
            lIndex = (ULONG) -1;
            err_handler(EvtLogName, _T("GetACEInsertionIndex Bad Src Ace Type"));
            __leave;
         }
         // Inherited adjustment
         if((pACE->AceFlags & INHERITED_ACE) != 0)
            lAceGroup+=4;

         // If this is the same group, then insertion point found
         if(lAceGroup>=lNewAceGroup)
            break;
      } 
   }
   __finally
   {
	   return (lIndex);
   }
}

BOOL OrderDACL( PACL pDACL )
{
   BOOL fReturn = FALSE;
   __try
   {
      // Get ACL size and ACE count
      ACL_SIZE_INFORMATION aclSize;
      if (!GetAclInformation(pDACL, &aclSize, 
         sizeof(aclSize), AclSizeInformation))
	  {
		  err_handler(EvtLogName, _T("OrderDACL GetAclInformation Failed"));
          __leave;
      }
      // Get memory for temporary ACL
      PACL pTempDACL = (PACL) _alloca(aclSize.AclBytesInUse);
      if (pTempDACL == NULL)
	  {
		  err_handler(EvtLogName, _T("OrderDACL _alloca Failed"));
          __leave;
	  }
      // Initialize temporary ACL
      if (!InitializeAcl(pTempDACL, aclSize.AclBytesInUse, 
         ACL_REVISION))
	  {
		  err_handler(EvtLogName, _T("OrderDACL InitializeAcl Failed"));
          __leave;
	  }
      
      // Iterate through ACEs
      for (ULONG lAceIndex = 0; lAceIndex < aclSize.AceCount ; lAceIndex++)
	  {
         // Get ACE
         PACE_UNION pACE;
         if (!GetAce(pDACL, lAceIndex, (PVOID*)&pACE))
		 {
			 err_handler(EvtLogName, _T("OrderDACL GetAce Failed"));
             __leave;
		 }
         // Find location, and add ACE to temp DACL
         ULONG lWhere = GetACEInsertionIndex(pTempDACL, pACE);
         if (!AddAce(pTempDACL, ACL_REVISION, 
            lWhere, pACE, ((ACE_HEADER*)pACE)->AceSize))
		 {
			 err_handler(EvtLogName, _T("OrderDACL AddAce Failed"));
             __leave;
		 }
      }
      // Copy temp DACL to original
      CopyMemory(pDACL, pTempDACL, aclSize.AclBytesInUse);
      fReturn = TRUE;
   }
   __finally
   {
	   if(!fReturn)
	   {
		   err_handler(EvtLogName, _T("OrderDACL exception caught in __finally"));
	   }
	   return (fReturn);
   }
}

BOOL AllowAccessToWinSta( PSID psidTrustee, HWINSTA hWinSta ){
   BOOL fReturn = FALSE;     
   PSECURITY_DESCRIPTOR psdWinSta = NULL;
   PACE_UNION pACENew = NULL;

   __try
   {    
      // Get the DACL for the window station
      PACL pDACLWinSta;
      if(GetSecurityInfo(hWinSta, SE_WINDOW_OBJECT, 
         DACL_SECURITY_INFORMATION, NULL, NULL, &pDACLWinSta, 
         NULL, &psdWinSta) != ERROR_SUCCESS)
	  {
         err_handler(EvtLogName, _T("AllowAccessToWinSta GetSecurityInfo Failed"));
         __leave;
	  }

      // Allocate our new ACE
      // This is the access awarded to a user who logged on interactively
      PACE_UNION pACENew = AllocateACE(ACCESS_ALLOWED_ACE_TYPE, 0, 
         DELETE|WRITE_OWNER|WRITE_DAC|READ_CONTROL|
         WINSTA_ENUMDESKTOPS|WINSTA_READATTRIBUTES|
         WINSTA_ACCESSCLIPBOARD|WINSTA_CREATEDESKTOP|
         WINSTA_WRITEATTRIBUTES|WINSTA_ACCESSGLOBALATOMS|
         WINSTA_EXITWINDOWS|WINSTA_ENUMERATE|WINSTA_READSCREEN, 
         psidTrustee);      
      
      // Is the ACE already in the DACL?
      if (pDACLWinSta && FindACEInACL(pDACLWinSta, pACENew) == -1)
	  {
         // If not, calculate new DACL size
         ULONG lNewACL = CalculateACLSize( pDACLWinSta, NULL, 0, 
            &pACENew, 1 );

         // Allocate memory for the new DACL
         PACL pNewDACL = (PACL)_alloca(lNewACL);
         if (pNewDACL == NULL)
		 {
            err_handler(EvtLogName, _T("AllowAccessToWinSta _alloca Failed"));
            __leave;
		 }
         // Initialize the ACL
         if (!InitializeAcl(pNewDACL, lNewACL, ACL_REVISION))
		 {
            err_handler(EvtLogName, _T("AllowAccessToWinSta InitializeAcl Failed"));
            __leave;
		 }
         // Copy the ACL
         if (pDACLWinSta && !CopyACL(pNewDACL, pDACLWinSta))
		 {
            err_handler(EvtLogName, _T("AllowAccessToWinSta CopyACL Failed"));
            __leave;
		 }
         // Get location for new ACE
         ULONG lIndex = GetACEInsertionIndex(pNewDACL, pACENew);

         // Add the new ACE
         if (!AddAce(pNewDACL, ACL_REVISION, lIndex, 
            pACENew, pACENew->aceHeader.AceSize))
		 {
            err_handler(EvtLogName, _T("AllowAccessToWinSta AddAce Failed"));
            __leave;
		 }
         // Set the DACL back to the window station
         if (SetSecurityInfo(hWinSta, SE_WINDOW_OBJECT, 
            DACL_SECURITY_INFORMATION, NULL, NULL, 
            pNewDACL, NULL)!=ERROR_SUCCESS)
		 {
              err_handler(EvtLogName, _T("AllowAccessToWinSta SetSecurityInfo Failed"));
              __leave;
		 }
      }
      fReturn = TRUE;        
   }
   __finally
   {
	   // Clean up
       if(pACENew != NULL)
		   LocalFree(pACENew);
       if(psdWinSta != NULL)
		   LocalFree(psdWinSta);   
	   if(!fReturn)
	   {
		   err_handler(EvtLogName, _T("AllowAccessToWinSta exception caught in __finally"));
	   }
       return (fReturn);   
   }
}

BOOL AllowTokenFullAccessToObject(HANDLE hToken, HANDLE hObject, SE_OBJECT_TYPE ObjectType, LPCTSTR szDetailedName)
{
	if(hToken == NULL || hObject == NULL) return FALSE;

	DWORD cb = 0;
    TOKEN_GROUPS* ptg = NULL;
	TOKEN_USER*   ptu = NULL;

	//void* pAdminSid = NULL;
	SID_AND_ATTRIBUTES* it = NULL;
	SID_AND_ATTRIBUTES* end = NULL;
    BOOL bSuccess = FALSE;
	TCHAR strTempNameString[MAX_PATH];
	::lstrcpy(strTempNameString, _T(""));
	TCHAR* upperString = NULL;
	if(szDetailedName != NULL)
	{
		::lstrcpy(strTempNameString, szDetailedName);
		upperString = _tcslwr((LPTSTR)strTempNameString);
	}

    __try
	{
        GetTokenInformation( hToken, TokenUser, 0, 0, &cb );
	    ptu = (TOKEN_USER*)LocalAlloc(LPTR,  cb );
		if(!ptu)
		{
			err_handler(EvtLogName, _T("AllowTokenFullAccessToObject LocalAlloc Failed"));
			__leave;
		}
		if(!GetTokenInformation( hToken, TokenUser, ptu, cb, &cb ))
		{
			err_handler(EvtLogName, _T("AllowTokenFullAccessToObject GetTokenInformation User Failed"));
			__leave;
		}
        if(!DeleteDenyACEofObject(ptu->User.Sid, hObject, ObjectType))
		{
			err_handler(EvtLogName, _T("AllowTokenFullAccessToObject DeleteDenyACEofObject User Failed"));
			__leave;
		}
		GetTokenInformation( hToken, TokenGroups, 0, 0, &cb );
	    ptg = (TOKEN_GROUPS*)LocalAlloc(LPTR,  cb );
		if(!ptg)
		{
			err_handler(EvtLogName, _T("AllowTokenFullAccessToObject LocalAlloc Failed"));
			__leave;
		}
	
		if(!GetTokenInformation( hToken, TokenGroups, ptg, cb, &cb ))
		{
            err_handler(EvtLogName, _T("AllowTokenFullAccessToObject GetTokenInformation Group Failed"));
            __leave;
		}
		//
		//pAdminSid = GetAdministrorMemberGrpSid();
    	end = ptg->Groups + ptg->GroupCount;
	    for (it = ptg->Groups; end != it; ++it )
		{
			if(!DeleteDenyACEofObject(it->Sid, hObject, ObjectType))
			{
				err_handler(EvtLogName, _T("AllowTokenFullAccessToObject DeleteDenyACEofObject Groups Failed"));
                __leave;
			}			
		}

		if(ObjectType == SE_WINDOW_OBJECT)
		{
			//key word like WinStat, Desktop ...
			if(_tcsstr(upperString, _T("desk")) != NULL) 
			{
				//desktop
				if(!AllowAccessToDesktop(ptu->User.Sid, (HDESK)hObject)) 
				{
                    err_handler(EvtLogName, _T("AllowTokenFullAccessToObject AllowAccessToDesktop Failed"));
					__leave;
				}
			}
			else
			{
				//winstat
				if(!AllowAccessToWinSta(ptu->User.Sid, (HWINSTA)hObject)) 
				{
                     err_handler(EvtLogName, _T("AllowTokenFullAccessToObject AllowAccessToWinSta Failed"));
			         __leave;
				}
			}			
		} 
		else //... omitted intentionally
		{
            err_handler(EvtLogName, _T("AllowTokenFullAccessToObject Wrong Object"));
			__leave;
		}
        bSuccess = TRUE;
    }
	__finally
	{
		if(ptu) LocalFree(ptu);
		if(ptg) LocalFree(ptg);
		if(!bSuccess)
		{
			err_handler(EvtLogName, _T("AllowTokenFullAccessToObject exception caught in __finally"));
		}
		return bSuccess;
	}
}

//Delete All Deny ACE if it is for psidTrustee
BOOL DeleteDenyACEofObject(PSID psidTrustee, HANDLE hObject, SE_OBJECT_TYPE ObjectType)
{
	BOOL fReturn = FALSE;     
    PSECURITY_DESCRIPTOR psdObj = NULL;
    PACE_UNION  pACE = NULL;
    
    ULONG lAceIndex = 0;
	ACL_SIZE_INFORMATION aclSize;
	PBYTE pbACE = NULL;
	BOOL bRet = FALSE;
    __try
	{    
       // Get the DACL for the desktop
       PACL pDACL;
       if(GetSecurityInfo(hObject, ObjectType, 
          DACL_SECURITY_INFORMATION, NULL, NULL, &pDACL, 
          NULL, &psdObj) != ERROR_SUCCESS)
	   {
          err_handler(EvtLogName, _T("DeleteDenyACEofObject GetSecurityInfo Failed"));
          __leave;
	   }

	   if(pDACL == NULL) 
	   {
		   //err_handler(EvtLogName, _T("DeleteDenyACEofObject NULL DACL");
		   fReturn = TRUE;
		   __leave;
	   }

       
       if(!GetAclInformation(pDACL, &aclSize, 
          sizeof(aclSize), AclSizeInformation))
	   {
          err_handler(EvtLogName, _T("DeleteDenyACEofObject GetAclInformation Failed"));
    	  __leave;
	   }

	   // Iterate through ACEs
       for (lAceIndex = aclSize.AceCount - 1; lAceIndex > 0; lAceIndex--)
	   {
		   // Get ACE
           if (!GetAce(pDACL, lAceIndex, (PVOID*)&pACE))
		   {
               err_handler(EvtLogName, _T("DeleteDenyACEofObject GetAce Failed"));
			   __leave;
		   }
		   if(pACE->aceHeader.AceType == ACCESS_DENIED_ACE_TYPE ||
			   pACE->aceHeader.AceType == ACCESS_DENIED_OBJECT_ACE_TYPE)
		   {
			   // Get the offset of the SID in the ACE
               ULONG lSIDOffset = (ULONG)(&((ACCESS_ALLOWED_ACE*)0)->SidStart);
               pbACE = (PBYTE)pACE;
               //Compare SID
               bRet = EqualSid((PSID)(pbACE+lSIDOffset), psidTrustee);
			   //Found and delete it
			   if(bRet)
			   {
				   DeleteAce (pDACL, lAceIndex);
			   }
		   }
	   }

	   // Set the DACL back to the window station
       if(SetSecurityInfo(hObject, ObjectType, 
            DACL_SECURITY_INFORMATION, NULL, NULL, 
            pDACL, NULL)!=ERROR_SUCCESS)
	   {
            err_handler(EvtLogName, _T("DeleteDenyACEofObject SetSecurityInfo GetAce Failed"));
            __leave;
	   }
	   fReturn = TRUE;
    }
	__finally
	{
		// Clean up
        if(psdObj != NULL)
			LocalFree(psdObj); 
		if(!fReturn)
		{
			err_handler(EvtLogName, _T("DeleteDenyACEofObject exception caught in __finally"));
		}
        return (fReturn); 
	}
}

BOOL AllowAccessToDesktop( PSID psidTrustee, HDESK hDesk ){
   BOOL fReturn = FALSE;     
   PSECURITY_DESCRIPTOR psdDesk = NULL;
   PACE_UNION pACENew = NULL;

   __try
   {    
      // Get the DACL for the desktop
      PACL pDACLDesk;
      if(GetSecurityInfo(hDesk, SE_WINDOW_OBJECT, 
         DACL_SECURITY_INFORMATION, NULL, NULL, &pDACLDesk, 
         NULL, &psdDesk) != ERROR_SUCCESS)
	  {
		  err_handler(EvtLogName, _T("AllowAccessToDesktop GetSecurityInfo Failed"));
		  __leave;
	  }
      
      // Allocate our new ACE
      // This is the access awarded to a user who logged on interactively
      PACE_UNION pACENew = AllocateACE(ACCESS_ALLOWED_ACE_TYPE, 0, 
         DELETE|WRITE_OWNER|WRITE_DAC|READ_CONTROL|
         DESKTOP_READOBJECTS|DESKTOP_CREATEWINDOW|
         DESKTOP_CREATEMENU|DESKTOP_HOOKCONTROL|
         DESKTOP_JOURNALRECORD|DESKTOP_JOURNALPLAYBACK|
         DESKTOP_ENUMERATE|DESKTOP_WRITEOBJECTS|DESKTOP_SWITCHDESKTOP,
         psidTrustee);      
      
      // Is the ACE already in the DACL?
      if (pDACLDesk && FindACEInACL(pDACLDesk, pACENew) == -1)
	  {
         // If not, calculate new DACL size
         ULONG lNewACL = CalculateACLSize( pDACLDesk, NULL, 0, 
            &pACENew, 1 );

         // Allocate memory for the new DACL
         PACL pNewDACL = (PACL)_alloca(lNewACL);
         if (pNewDACL == NULL)
		 {
			 err_handler(EvtLogName, _T("AllowAccessToDesktop _alloc Failed"));
			 __leave;
		 }

         // Initialize the ACL
         if (!InitializeAcl(pNewDACL, lNewACL, ACL_REVISION))
		 {
			 err_handler(EvtLogName, _T("AllowAccessToDesktop Init ACL Failed"));
			 __leave;
		 }

         // Copy the ACL
         if (pDACLDesk && !CopyACL(pNewDACL, pDACLDesk))
		 {
			 err_handler(EvtLogName, _T("AllowAccessToDesktop CopyACL Failed"));
			 __leave;
		 }

         // Get location for new ACE
         ULONG lIndex = GetACEInsertionIndex(pNewDACL, pACENew);

         // Add the new ACE
         if (!AddAce(pNewDACL, ACL_REVISION, lIndex, 
            pACENew, pACENew->aceHeader.AceSize))
		 {
			 err_handler(EvtLogName, _T("AllowAccessToDesktop AddAce Failed"));
			 __leave;
		 }

         // Set the DACL back to the window station
         if (SetSecurityInfo(hDesk, SE_WINDOW_OBJECT, 
            DACL_SECURITY_INFORMATION, NULL, NULL, 
            pNewDACL, NULL)!=ERROR_SUCCESS)
		 {
			 err_handler(EvtLogName, _T("AllowAccessToDesktop SetSecurityInfo Failed"));
			 __leave;
		 }
      }
      fReturn = TRUE;        
   }
   __finally
   {
	   // Clean up
       if(pACENew != NULL)
		   LocalFree(pACENew);
       if(psdDesk != NULL)
		   LocalFree(psdDesk);  
	   if(!fReturn)
	   {
		   err_handler(EvtLogName, _T("AllowAccessToDesktop exception caught in __finally"));
	   }
       return (fReturn);   
   }
}