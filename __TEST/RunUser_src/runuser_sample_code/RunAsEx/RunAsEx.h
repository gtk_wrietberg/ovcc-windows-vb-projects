
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

#if !defined(AFX_RUNASEX_H__7A0FFFB9_9E76_4A6B_AFBF_E1DA01AAD9E3__INCLUDED_)
#define AFX_RUNASEX_H__7A0FFFB9_9E76_4A6B_AFBF_E1DA01AAD9E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"

extern HINSTANCE hInst;
extern BOOL  g_bService;
extern TCHAR g_szDomain[MAX_PATH];
extern TCHAR g_szUser[MAX_PATH];
extern TCHAR g_szPassword[MAX_PATH];
extern DWORD g_dwLogOnType;
extern DWORD g_dwLogOnProvider;
extern TCHAR g_szExeName[4 * MAX_PATH];
extern TCHAR g_szCmdLine[4 * MAX_PATH];
extern TCHAR g_szDesktop[MAX_PATH]; //Including win station

extern BOOL  g_bLoadProfile;
extern BOOL  g_bDirectLaunch; //When True : LogOnUser, ZwCreateToken --> CreateProcessAsUser
                     //     FALSE : CreateService, in Service --> LogOn, Zw --> CreateProcessAsUser
extern BOOL  g_LogOnUser_Zw;  //When True: LogOnUser
                     //     FALSE: ZwCreateToken //no password needed
extern BOOL  g_bReStart;      //For Switch WinStat require no Window left on the old WinStat
                       //I close the dialog temporarily and make switch, after all done
                       //switch back to old WinStat and restore the dialog
extern RECT  g_rectDialog; 
extern HWND  g_hwndAfter;
extern BOOL  g_bCopyTokenPropFromCaller; //Only meaningful when ZwCreateToekn g_LogOnUser_Zw = FALSE
extern BOOL  g_bKeepPriv; //Only meaningful when ZwCreateToekn g_LogOnUser_Zw = FALSE
extern DWORD g_dwSession; 

//0: No Log
//1: Log to a file : default
//2: Log to EventLog
extern DWORD g_dwLogOption;

BOOL FinalizeStartingService();
BOOL RemoveService();
BOOL InstallService(LPCTSTR szPath, LPCTSTR szCmdLine, BOOL bStart = TRUE);
void WINAPI myServiceMain(DWORD dwArgc, PTSTR* pszArgv) ;
DWORD WINAPI myHandlerEx(DWORD dwControl, DWORD dwEventType, 
   PVOID pvEventData, PVOID pvContext);

///////////////////////////////////////////////////////////////////////
typedef enum 
{
	x_service,
	x_install,
	x_remove,
	x_gui,
	x_console,
	//x_help
	x_error
} enumStartType;
enumStartType IsService(LPSTR lpCmdLine);
int LaunchGUI(HINSTANCE hInstance, HINSTANCE hPrevInstance, 
			  LPSTR     lpCmdLine, int       nCmdShow);
int LaunchService(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine, int       nCmdShow);
int LaunchConsole(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine, int       nCmdShow);
////////////////////////////////////////////////////////////////////////
//------------Service Info----------------------------------------------
////////////////////////////////////////////////////////////////////////
BOOL IsSvrRunning(LPCTSTR szInternalName, DWORD& dwStatus);
BOOL InterStartService(LPCTSTR szInternalName);
BOOL InterStopService(LPCTSTR szInternalName);
BOOL InterServiceRemove(LPCTSTR szInternalName);
BOOL InterAutoService(LPCTSTR szInternalName);

#endif // !defined(AFX_RUNASEX_H__7A0FFFB9_9E76_4A6B_AFBF_E1DA01AAD9E3__INCLUDED_)
