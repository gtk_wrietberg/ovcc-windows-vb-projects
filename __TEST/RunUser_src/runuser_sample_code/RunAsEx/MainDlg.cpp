
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

#include "StdAfx.h"
#include "MainDlg.h"
#include "resource.h"
#include "RunAsEx.h"
#include "RunAsHelper.h"
#include "CoreCode.h"

#include <Shellapi.h>

HWND hEdtDomain, hEdtUser, hEdtPassword;
//HWND hBtnBrowseDomain, hBtnBrowseUser, hBtnTestPassword;
HWND hComboLogOnType, hComboLogOnProvider;
HWND hComboDesktop, hEdtExe, hEdtCommandLine;
//hWND hBtnBrowseDesktop, hBtnBrowseExe;
HWND hChkLoadUserProfile, hChkCopyTokenPropFromCaller, hChkKeepPriv;
HWND hListExe;
HWND hChkSession, hComboSession;

HWND hTT; 


INT_PTR CALLBACK DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
		case WM_INITDIALOG:
			return OnInitDialog(hWnd, wParam, lParam); 
		case WM_COMMAND: 
		    return OnCommand(hWnd, wParam, lParam); 
		case WM_NOTIFY:
			return OnNotify(hWnd, wParam, lParam);
		case WM_DROPFILES:
			return OnDropFile(hWnd, wParam, lParam);
		case WM_SYSCOMMAND:
			return OnSysCommand(hWnd, wParam, lParam);
	} 
    return FALSE ;
}

LRESULT OnInitAboutDialog(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	HWND hRadio = NULL;
	if(::g_dwLogOption == 0)
	{
		hRadio = ::GetDlgItem(hWnd, IDC_RADIO_ERROR_LOG_NO);
		::SendMessage(hRadio, BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)0);
	}
	else if(::g_dwLogOption == 2)
	{
		hRadio = ::GetDlgItem(hWnd, IDC_RADIO_ERROR_LOG_EVENTLOG);
		::SendMessage(hRadio, BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)0);
	}
	else 
	{
		hRadio = ::GetDlgItem(hWnd, IDC_RADIO_ERROR_LOG_DISKFILE);
		::SendMessage(hRadio, BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)0);
	}

	return TRUE;
}

LRESULT OnAboutCommand(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(wParam)) 
	{
		case IDOK:
			EndDialog(hWnd, IDOK);
            return TRUE;
        case IDCANCEL:
            EndDialog(hWnd, IDCANCEL);
            return TRUE;
		case IDC_GO_CODEGURU:
			::ShellExecute(NULL, _T("open"),
				_T("http://www.codeproject.com/system/RunUser.asp"), NULL, NULL, 0);
			::ShellExecute(NULL, _T("open"),
				_T("http://www.codeguru.com/misc/RunUser.html"), NULL, NULL, 0);
			return TRUE;
	}	
   	return FALSE;
}

INT_PTR CALLBACK DialogAboutProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
		case WM_INITDIALOG:
			return OnInitAboutDialog(hWnd, wParam, lParam); 
		case WM_COMMAND: 
		    return OnAboutCommand(hWnd, wParam, lParam); 
		
	} 
    return FALSE ;
}

LRESULT OnSysCommand(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	if ((wParam & 0xFFF0) == IDM_ABOUT)
	{
		DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), NULL, DialogAboutProc, 0);
		return 0;
	}
	else
		return DefWindowProc(hWnd, WM_SYSCOMMAND, wParam, lParam);
}

#include <shobjidl.h>
#include <ShlGuid.h>

//Some lnk file like "%SystemRoot%\System32\Calc.exe !! I have no interest parsing more!
BOOL OnDropFileCheck(TCHAR* szFile)
{
	//-->lowser case
	szFile = ::CharLower(szFile); //change in place
	if(::lstrlen(szFile) < 7) return FALSE;
	TCHAR* psz = szFile;
	psz += ::lstrlen(szFile) - 4;
	if(::_tcscmp(psz, _T(".exe")) == 0 || ::_tcscmp(psz, _T(".bat")) == 0
		|| ::_tcscmp(psz, _T(".com")) == 0)
	{
		return TRUE;
	}
	else if(::_tcscmp(psz, _T(".lnk")) == 0)
	{
		//parse it, emmm, to keep module size, omit it ????
		::CoInitialize(NULL);
	    IShellLink *pisl;  
	    HRESULT hr = ::CoCreateInstance(CLSID_ShellLink,
			    NULL, CLSCTX_INPROC_SERVER,
			    IID_IShellLink, (void **)&pisl); 
	    if(!SUCCEEDED(hr)) 
		{
			::CoUninitialize();
			return FALSE; 
		}

		IPersistFile *pipf; 
	    hr = pisl->QueryInterface(IID_IPersistFile, (void **)&pipf);
		if(!SUCCEEDED(hr))
		{
			pisl->Release();
			::CoUninitialize();
			return FALSE;
		}
		hr = pipf->Load(szFile, STGM_READ);
        if(!SUCCEEDED(hr))
		{
			pipf->Release();
			pisl->Release();
			::CoUninitialize();
			return FALSE;
		}
		WIN32_FIND_DATA fdInteral;
		//hope 4096 is enough
		TCHAR pszFile[4096];
        hr = pisl->GetPath(pszFile, 4096, &fdInteral, SLGP_RAWPATH);
		if(hr == S_OK) ::lstrcpy(szFile, pszFile);
        pisl->Release();
		::CoUninitialize();		
		return hr == S_OK;//TRUE;
	}
	else
		return FALSE;
}

LRESULT OnDropFile(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	HDROP hDropInfo = (HDROP)wParam;
	//
	// Find out how many files were dropped.
	//
    int nCount = ::DragQueryFile (hDropInfo, (UINT) -1, NULL, 0);
    if (nCount == 1)
	{
		// One file at a time, please
        TCHAR szFile[4 * MAX_PATH];
        ::DragQueryFile (hDropInfo, 0, szFile, sizeof (szFile));
		
        if(::lstrlen(szFile) > 0 && OnDropFileCheck(szFile))
		{
			::SendMessage(hEdtExe, WM_SETTEXT, 0,  (LPARAM)szFile);
		}
    }
    ::DragFinish (hDropInfo);
	return 0;
}

LRESULT OnInitDialog(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	TCHAR szTitle[1024];
    lstrcpy(szTitle, TEXT("RunAsEx is running as \""));
    ULONG lSize = chDIMOF(szTitle) - lstrlen(szTitle);
    GetUserName(szTitle+lstrlen(szTitle),&lSize);
    lstrcat(szTitle, TEXT("\""));
    ::SetWindowText(hWnd, szTitle);
	
	hEdtDomain = ::GetDlgItem(hWnd, IDC_EDT_DOMAIN);
	hEdtUser = ::GetDlgItem(hWnd, IDC_EDT_USER);;
	hEdtPassword= ::GetDlgItem(hWnd, IDC_EDT_PWD);
    hComboLogOnType = ::GetDlgItem(hWnd, IDC_COMBO_LOGONTYPE);
	hComboLogOnProvider = ::GetDlgItem(hWnd, IDC_COMBO_LOGONPROVIDER);
    
	hComboDesktop =::GetDlgItem(hWnd, IDC_COMBO_DESKTOP);
	hEdtExe = ::GetDlgItem(hWnd, IDC_EDT_EXE);
	hEdtCommandLine = ::GetDlgItem(hWnd, IDC_EDT_COMMANDLINE);

    hChkLoadUserProfile = ::GetDlgItem(hWnd, IDC_CHK_PROFILE);
	hChkCopyTokenPropFromCaller = ::GetDlgItem(hWnd, IDC_CHK_COPYCALLER);
	hChkKeepPriv = ::GetDlgItem(hWnd, IDC_CHK_KEEP_PRIV);
	hListExe = ::GetDlgItem(hWnd, IDC_LIST);

	hChkSession = ::GetDlgItem(hWnd, IDC_CHK_SESSION);
	hComboSession = ::GetDlgItem(hWnd, IDC_COMBO_SESSION);

	TCHAR szComputerName[MAX_PATH];
	DWORD dwNameLen = MAX_PATH;
	if(::GetComputerName(szComputerName, &dwNameLen))
	{
		HANDLE hWTS = WTSOpenServer(szComputerName);
		if(hWTS)
		{
			::EnableWindow(hChkSession, TRUE);
            ::EnableWindow(hComboSession, TRUE);

			PWTS_SESSION_INFO pInfo = NULL;
			DWORD dwCount = 0;
			if(::WTSEnumerateSessions(hWTS, 0, 1, &pInfo, &dwCount))
			{
				for(int k = 0; k < (int)dwCount; k++)
				{
					//add to combo session
					TCHAR szSession[32];
					wsprintf(szSession, _T("%d"), pInfo[k].SessionId);
					::SendMessage(hComboSession, CB_ADDSTRING, 0, (LPARAM)szSession);										                  
				}
				if(::g_dwSession == -1)
				{
					::SendMessage(hChkSession, BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)0);
                    TCHAR szSession[32];
                    DWORD dwSessionID;
					if(ProcessIdToSessionId(GetCurrentProcessId(), &dwSessionID))
					{
						wsprintf(szSession, _T("%d"), dwSessionID);
					    int selIndex = ::SendMessage(hComboSession, CB_FINDSTRING,
						   (WPARAM)-1, (LPARAM)(LPCTSTR)szSession);
						if(selIndex != CB_ERR)
						{
							::SendMessage(hComboSession, CB_SETCURSEL, selIndex, 0);
						}
					}
				}
				else
				{
					::SendMessage(hChkSession, BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)0);
                    TCHAR szSession[32];
                    wsprintf(szSession, _T("%d"), g_dwSession); 
					int selIndex = ::SendMessage(hComboSession, CB_FINDSTRING,
					  (WPARAM)-1, (LPARAM)(LPCTSTR)szSession);
					if(selIndex != CB_ERR)
					{
						::SendMessage(hComboSession, CB_SETCURSEL, selIndex, 0);
					}
				}
			}
			if(pInfo)
				::WTSFreeMemory(pInfo);
			WTSCloseServer(hWTS);
		}
	}


	HICON hIcon = ::LoadIcon(hInst, MAKEINTRESOURCE(IDI_MAIN));
    ::SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
	::SendMessage(hWnd, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);

	//Fill Combo LogOn Type
	int nIndex = ::SendMessage(hComboLogOnType, CB_ADDSTRING, 0,
         (LPARAM) L"Batch");
	::SendMessage(hComboLogOnType, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_BATCH);

	nIndex = ::SendMessage(hComboLogOnType, CB_ADDSTRING, 0,
         (LPARAM) L"Network");
	::SendMessage(hComboLogOnType, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_NETWORK);

	nIndex = ::SendMessage(hComboLogOnType, CB_ADDSTRING, 0,
         (LPARAM) L"Network Cleartext");
	::SendMessage(hComboLogOnType, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_NETWORK_CLEARTEXT);

    nIndex = ::SendMessage(hComboLogOnType, CB_ADDSTRING, 0,
         (LPARAM) L"New Credentials");
    ::SendMessage(hComboLogOnType, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_NEW_CREDENTIALS);

    nIndex = ::SendMessage(hComboLogOnType, CB_ADDSTRING, 0,
         (LPARAM) L"Service");
    ::SendMessage(hComboLogOnType, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_SERVICE);

    nIndex = ::SendMessage(hComboLogOnType, CB_ADDSTRING, 0,
         (LPARAM) L"Unlock");
    ::SendMessage(hComboLogOnType, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_UNLOCK);

    nIndex = ::SendMessage(hComboLogOnType, CB_ADDSTRING, 0,
         (LPARAM) L"Interactive");
    ::SendMessage(hComboLogOnType, CB_SETITEMDATA, nIndex,
         LOGON32_LOGON_INTERACTIVE);
    ::SendMessage(hComboLogOnType, CB_SETCURSEL, nIndex, 0);

	//Fill Combo LogOn Provider

    nIndex = ::SendMessage(hComboLogOnProvider, CB_ADDSTRING, 0,
         (LPARAM) L"Windows NT5");
    ::SendMessage(hComboLogOnProvider, CB_SETITEMDATA, nIndex,
         LOGON32_PROVIDER_WINNT50);

    nIndex = ::SendMessage(hComboLogOnProvider, CB_ADDSTRING, 0,
         (LPARAM) L"Windows NT 4.0");
    ::SendMessage(hComboLogOnProvider, CB_SETITEMDATA, nIndex,
         LOGON32_PROVIDER_WINNT40);

    nIndex = ::SendMessage(hComboLogOnProvider, CB_ADDSTRING, 0,
         (LPARAM) L"Windows NT 3.5");
    ::SendMessage(hComboLogOnProvider, CB_SETITEMDATA, nIndex,
         LOGON32_PROVIDER_WINNT35);

    nIndex = ::SendMessage(hComboLogOnProvider, CB_ADDSTRING, 0,
         (LPARAM) L"Default");

	//Default
    ::SendMessage(hComboLogOnProvider, CB_SETITEMDATA, nIndex,
         LOGON32_PROVIDER_DEFAULT);
    ::SendMessage(hComboLogOnProvider, CB_SETCURSEL, nIndex, 0);

	//Fill ShortCut List
	ListView_SetExtendedListViewStyle(hListExe, LVS_EX_FULLROWSELECT /*| LVS_EX_CHECKBOXES*/ );
	
	HIMAGELIST hImage;   // image list  
     // Create the full-sized icon image lists. 
    hImage = ImageList_Create(GetSystemMetrics(SM_CXSMICON), 
        GetSystemMetrics(SM_CYSMICON), ILC_COLOR24, 0, 1);
    ListView_SetImageList(hListExe, hImage, LVSIL_SMALL); 

	//char szText[256];     // temporary buffer 
    LVCOLUMN lvc; 
    int iCol; 
 
    // Initialize the LVCOLUMN structure.
    // The mask specifies that the format, width, text, and subitem
    // members of the structure are valid. 
    lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM; 
     
    // Add the 2 columns -- Exe name and Full Path (including exe) 
    for (iCol = 0; iCol < 2; iCol++) 
	{ 
        lvc.iSubItem = iCol;
        lvc.pszText = iCol == 0 ? _T("Name") : _T("Path");	
        lvc.cx = iCol == 0 ? 70 : 80;    // width of column in pixels
        
		lvc.fmt = iCol == 0 ? LVCFMT_LEFT : LVCFMT_LEFT;  // left-right-aligned column
        ListView_InsertColumn(hListExe, iCol, &lvc); 
    } 

	//Add Several Common Exe into Program Combo 
	//Cmd TaskMgt RegEdit RegEdt32 IE Explorer Spy++ VC6 VSNET NotePad MMC, RunDll32 MSN IM5. IM 6 
    
	HICON hIcon2 = ::LoadIcon(hInst, MAKEINTRESOURCE(IDI_EMPTY));
	ImageList_AddIcon(hImage, hIcon2);
	::DestroyIcon(hIcon2);

#define Z_MAX_PATH (4 * MAX_PATH)
	TCHAR szSys[Z_MAX_PATH];
	GetSystemPath(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\Cmd.exe"));
	AddQuickLaunch(hListExe, szSys);

	GetSystemPath(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\Taskmgr.exe"));
	AddQuickLaunch(hListExe, szSys);

	GetWindowsDirectory(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\Regedit.exe"));
	AddQuickLaunch(hListExe, szSys);

	GetSystemPath(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\Regedt32.exe"));
	AddQuickLaunch(hListExe, szSys);
	
	//Following is hard-code
	GetSystemPath(szSys, Z_MAX_PATH);
	szSys[1] = TCHAR('\0');
	::lstrcat(szSys, _T(":\\Program Files\\Internet Explorer\\IExplore.Exe"));
	AddQuickLaunch(hListExe, szSys);

    GetWindowsDirectory(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\Explorer.exe"));
	AddQuickLaunch(hListExe, szSys);

	//Spy++6
	GetSystemPath(szSys, Z_MAX_PATH);
	szSys[1] = TCHAR('\0');
	::lstrcat(szSys, _T(":\\Program Files\\Microsoft Visual Studio\\Common\\Tools\\WinNT\\Tools\\Spyxx.Exe"));
	AddQuickLaunch(hListExe, szSys);

	//Spy++7
	GetSystemPath(szSys, Z_MAX_PATH);
	szSys[1] = TCHAR('\0');
	::lstrcat(szSys, _T(":\\Program Files\\Microsoft Visual Studio .NET\\Common7\\Tools\\Spyxx.exe"));
	AddQuickLaunch(hListExe, szSys);

	//VC++6
	GetSystemPath(szSys, Z_MAX_PATH);
	szSys[1] = TCHAR('\0');
	::lstrcat(szSys, _T(":\\Program Files\\Microsoft Visual Studio\\Common\\MSDev98\\Bin\\Msdev.Exe"));
	AddQuickLaunch(hListExe, szSys);

	//VS Net 2002
    GetSystemPath(szSys, Z_MAX_PATH);
	szSys[1] = TCHAR('\0');
	::lstrcat(szSys, _T(":\\Program Files\\Microsoft Visual Studio .NET\\Common7\\IDE\\Devenv.Exe"));
	AddQuickLaunch(hListExe, szSys);

	//VS Net2003

	//NotePad
	GetWindowsDirectory(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\Notepad.exe"));
	AddQuickLaunch(hListExe, szSys);

	//mmc
	GetSystemPath(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\Mmc.exe"));
	AddQuickLaunch(hListExe, szSys);

    //rundll32
	GetSystemPath(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\Rundll32.exe"));
	AddQuickLaunch(hListExe, szSys);

	//MSN IM5
    GetSystemPath(szSys, Z_MAX_PATH);
	szSys[1] = TCHAR('\0');
	::lstrcat(szSys, _T(":\\Program Files\\Messenger\\Msnmsgr.exe"));
	AddQuickLaunch(hListExe, szSys);

	//MSN IM6
	GetSystemPath(szSys, Z_MAX_PATH);
	szSys[1] = TCHAR('\0');
	::lstrcat(szSys, _T(":\\Program Files\\MSN Messenger\\Msnmsgr.exe"));
	AddQuickLaunch(hListExe, szSys);

	//self --- RunAsEx
	GetSelfPath(szSys, Z_MAX_PATH);
	::lstrcat(szSys, _T("\\RunAsEx.exe"));
	AddQuickLaunch(hListExe, szSys);
	///////////////////////////

	OnBrowseDesktop(hWnd, hComboDesktop);
	//Add NULL Desktop and "" Desktop
	COMBOBOXEXITEM cbei;
	cbei.mask = CBEIF_IMAGE | CBEIF_INDENT |
		CBEIF_SELECTEDIMAGE | CBEIF_TEXT;
	cbei.iItem = -1; //count-1;
    cbei.pszText = (LPTSTR)((LPCTSTR)_T("NULL"));
    cbei.cchTextMax = ::lstrlen(_T("NULL"));
    cbei.iImage = 7;  cbei.iSelectedImage = 7;   cbei.iIndent = 0;   
    ::SendMessage(hComboDesktop, CBEM_INSERTITEM, 0, (LPARAM)&cbei);

	cbei.pszText = (LPTSTR)((LPCTSTR)_T("EMPTY"));
    cbei.cchTextMax = ::lstrlen(_T("EMPTY"));
	::SendMessage(hComboDesktop, CBEM_INSERTITEM, 0, (LPARAM)&cbei);

	//set default setting, combo choose winstata0\default, kepp Priv --> true 
    int nDefaultDesktopIndex = (int)::SendMessage(
		hComboDesktop, CB_FINDSTRINGEXACT, (WPARAM)-1, (LPARAM)(LPCTSTR)_T("WinSta0\\Default"));
	if(nDefaultDesktopIndex != CB_ERR)
	{
		::SendMessage(hComboDesktop, CB_SETCURSEL, nDefaultDesktopIndex, 0);
	}
	::SendMessage(hChkKeepPriv, BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)0);
	
	//if(::IsWin2K() && !::IsWinXP() && !::IsWinNET())
	//{
	//	::EnableWindow(::GetDlgItem(hWnd, IDC_BTN_WHO), TRUE);
	//}
	//else
	//{
	//	::EnableWindow(::GetDlgItem(hWnd, IDC_BTN_WHO), FALSE);
	//}

	if(::g_bReStart)
	{
		::MoveWindow(hWnd, ::g_rectDialog.left, g_rectDialog.top,
			g_rectDialog.right - g_rectDialog.left, g_rectDialog.bottom - g_rectDialog.top,
			TRUE);
		UpdateData(hWnd, FALSE);
		//Move Z-order
		if(g_hwndAfter)
		{
			HWND hTemp = ::GetWindow(hWnd, GW_HWNDPREV);
			::SetWindowPos(hWnd, hTemp, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
		}
		g_bReStart = FALSE;
	}
    //Add Drag-Drop
	DragAcceptFiles(hWnd, TRUE);
	//Add About Menu
	HMENU hSysMenu = ::GetSystemMenu(hWnd, FALSE);
	if (hSysMenu != NULL)
	{
		AppendMenu(hSysMenu, MF_STRING, MF_SEPARATOR, NULL); 
		AppendMenu(hSysMenu, MF_STRING, IDM_ABOUT, _T("About")); 
	}

	//ToolTip
	hTT = CreateWindowEx(NULL, TOOLTIPS_CLASS, NULL,
                            WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,
                            CW_USEDEFAULT, CW_USEDEFAULT,
                            CW_USEDEFAULT, CW_USEDEFAULT,
                            hWnd, NULL, hInst,NULL);

    //::SetWindowPos(hTT, HWND_TOPMOST,0, 0, 0, 0,
    //         SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
 
	::SendMessage(hTT, TTM_SETDELAYTIME, TTDT_INITIAL, (LPARAM) MAKELONG(10, 0));
	::SendMessage(hTT, TTM_SETDELAYTIME, TTDT_AUTOPOP, (LPARAM) MAKELONG(10000, 0));


    TOOLINFO ti;
    ti.cbSize = sizeof (TOOLINFO);
    ti.uFlags = TTF_IDISHWND | TTF_SUBCLASS;
    ti.hwnd = hWnd;
	ti.hinst = 0;//hInst;

    ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_CMD_LINE);
    ti.lpszText = _T("Copy Current Setting to Clipboard As Text");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);  

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_CHK_COPYCALLER);
    ti.lpszText = _T("Zw-ed Token's User Group, Privilege, Statistics are the SAME as Caller Process (RunAsEx) (N/A when LogonUser)");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_CHK_KEEP_PRIV);
    ti.lpszText = _T("Check LSA to Keep Correct User Privilege (N/A when LogonUser)");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_CHK_PROFILE);
    ti.lpszText = _T("Load User Profile (Even User Just Created Without Profile)");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_CHK_SESSION);
    ti.lpszText = _T("When Run Program in the SAME session of Directly Caller Process (Unchecked Recommended)");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_COMBO_SESSION);
    ti.lpszText = _T("List of Existing Session");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_DOMAIN);
    ti.lpszText = _T("Browse Domain Names (WArning: Do NOt Use This For Long Query Time Needed Unless... You Do not Care)");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_USER);
    ti.lpszText = _T("List of Existing User om Domain/Machine. Input Domain/Machine Name in Edit box on the Dialog. Empty Means LocalMachine");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BUTTON_XWD);
    ti.lpszText = _T("Test Usr-Pwd Match, Certain Privilege Needed Due To LogonUser");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_WHO);
    ti.lpszText = _T("Current Domain/User; In Win2K Password is also Shown");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_PRIVILEGE);
    ti.lpszText = _T("Check Your Privilege Status");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

    ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_LAUNCH_LOGON);
    ti.lpszText = _T("LogonUser + CreateProcessAsUser");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_LAUNCH_ZW);
    ti.lpszText = _T("ZwCreateToken + CreateProcessAsUser");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_SVR_LOGON);
    ti.lpszText = _T("InstallService + LogonUser + CreateProcessAsUser");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_BTN_SVR_ZW);
    ti.lpszText = _T("InstallService + ZwCreateToken + CreateProcessAsUser");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	ti.uId = (DWORD)::GetDlgItem(hWnd, IDC_LIST);
    ti.lpszText = _T("No Icon Program is Non-existing on this machine");
    ::SendMessage(hTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&ti);

	//SendMessage(hTT,TTM_TRACKACTIVATE,(WPARAM)TRUE,(LPARAM)&ti);
    SendMessage(hTT, TTM_ACTIVATE , (WPARAM)TRUE, 0);
    return TRUE ;
}

void AddQuickLaunch(HWND hListExe, LPCTSTR szPath)
{
	HIMAGELIST hImage = ListView_GetImageList(hListExe, LVSIL_SMALL); 
    
	HICON hSmall[1];
    UINT nRet = ::ExtractIconEx(
         szPath,        // file name
         0,          // icon index
         NULL,      // large icon array
         hSmall,      // small icon array
         1              // number of icons to extract
    );
	BOOL bHasIcon = nRet == 1;

	//if(!bHasIcon) return;

	int indexImg = -1;
	if(bHasIcon)
	{
		indexImg = ImageList_AddIcon(hImage, hSmall[0]); 
    }
	else
	{
		indexImg = 0; 
	}

	LVITEM lv;
	lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE;
    lv.state = 0; 
    lv.stateMask = 0; 

	int index = ListView_GetItemCount(hListExe);
    
	TCHAR sz[MAX_PATH];
	TCHAR sz2[2 * MAX_PATH];

	::lstrcpy(sz2, szPath);
	
	TCHAR* psz = _tcsrchr(szPath, TCHAR('\\'));
	::lstrcpy(sz, ++psz);
    

   	lv.iItem = index;
	lv.iImage = indexImg;
	lv.iSubItem = 0;
	lv.lParam = (LPARAM)hSmall;
	lv.cchTextMax = MAX_PATH;
	lv.pszText = sz; 

	ListView_InsertItem(hListExe, &lv);

    ListView_SetItemText(hListExe, index, 1, (LPTSTR)sz2); 
}

void OnTestPassword()
{
	::SendMessage(hEdtDomain, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)g_szDomain);
    ::SendMessage(hEdtUser, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)g_szUser);
    ::SendMessage(hEdtPassword, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)g_szPassword);

	int nLogonType = ::SendMessage(hComboLogOnType, CB_GETCURSEL, 0, 0);
        g_dwLogOnType = (DWORD)::SendMessage(hComboLogOnType, CB_GETITEMDATA, nLogonType, 0);

    int nLogonProvider = ::SendMessage(hComboLogOnProvider, CB_GETCURSEL, 0, 0);
        g_dwLogOnProvider = (DWORD)::SendMessage(hComboLogOnProvider, CB_GETITEMDATA,
           nLogonProvider, 0);

    if(g_szUser == NULL || ::lstrlen(g_szUser) == 0 || g_szPassword == NULL || ::lstrlen(g_szPassword) == 0)
	{
		::PopMsg(_T("Input User Name And Password To Match Test, No NULL Text"));
	}
	else
	{
		HANDLE hFake = NULL;
		BOOL bRet = ::LogonUser(g_szUser, g_szDomain, g_szPassword, g_dwLogOnType, g_dwLogOnProvider, &hFake);
        
		if(bRet)
		{
			::PopMsg(_T("Usr/Pwd Match"));
			::CloseHandle(hFake);
		}
		else
		{
			::ReportErr(_T("Due to Various Reason, Usr/Pwd Match Failed"));
		}
	}
}

void OnMakeCmdText(HWND hDlg)
{
	if(!::UpdateData(hDlg, TRUE)) return;

	TCHAR szCmdLine[8 * MAX_PATH]; //do not tell me u want to overflow here
	if(g_szExeName == NULL || ::lstrlen(g_szExeName) == 0) return;
	if(g_szDesktop == NULL || ::lstrlen(g_szDesktop) == 0) return;

    int nProfile = g_bLoadProfile ? 1 : 0;
	int nDirect = g_LogOnUser_Zw ? 1 : 0;
	int nCopyTokenPropFromCaller = g_bCopyTokenPropFromCaller ? 1 : 0;
	int nKeepPriv = g_bKeepPriv ? 1 : 0;
	wsprintf(szCmdLine, _T("\"Domain:%s\" \"User:%s\" \"Password:%s\" \"Exe:%s\" \"Cmd:%s\" \"LogonType:%d\" \"LogOnProvider:%d\" \"Desktop:%s\" \"LoadProfile:%d\" \"LogonDirectly:%d\" \"CopyTokenProp:%d\" \"KeepPriv:%d\" \"Session:%d\" "), 
        g_szDomain, g_szUser, g_szPassword, g_szExeName, g_szCmdLine, 
		g_dwLogOnType, g_dwLogOnProvider, g_szDesktop, nProfile, nDirect, nCopyTokenPropFromCaller, nKeepPriv, g_dwSession);
	//Copy To Clipboard, Oh, man; I know it SHOULD be done with OLE Drop and Drop,
	//but, but I want to keep this exe size compact and dependency min!
	HGLOBAL     hGMem;
    LPTSTR        pGMem;
	if(!::OpenClipboard (hDlg))
	{
		::ReportErr(_T("Open Clipboard Failed !?!"));
		return;
	}
    //unlikely it will fail in following call...
	hGMem = ::GlobalAlloc(GPTR, (::lstrlen(szCmdLine) + 1)*sizeof(TCHAR));
	pGMem = (LPTSTR)(LPVOID )::GlobalLock (hGMem) ;
    ::lstrcpyn(pGMem, szCmdLine, ::lstrlen(szCmdLine) + 1);
	
	::SetClipboardData(CF_UNICODETEXT, hGMem);
	::GlobalUnlock (hGMem);
	::CloseClipboard();
}

LRESULT OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	TCHAR sz[MAX_PATH];
	HWND hFocus = NULL;
	switch (LOWORD(wParam)) 
	{
		case IDOK:
			hFocus = ::GetFocus();
            if(hFocus == ::hEdtCommandLine || hFocus == ::hEdtExe ||
				hFocus == ::hEdtDomain || hFocus == ::hEdtUser || hFocus == ::hEdtPassword ||
				hFocus == ::hChkCopyTokenPropFromCaller || hFocus == ::hChkKeepPriv ||
				hFocus == ::hChkLoadUserProfile || hFocus == ::hChkSession ||
				hFocus == ::hComboDesktop || ::GetParent(hFocus) == ::hComboDesktop || ::GetParent(::GetParent(hFocus)) == ::hComboDesktop ||
                hFocus == ::hComboLogOnProvider || ::GetParent(hFocus) == ::hComboLogOnProvider ||
				hFocus == ::hComboLogOnType || ::GetParent(hFocus) == ::hComboLogOnType  ||
				hFocus == ::hComboSession || ::GetParent(hFocus) == ::hComboSession || 
				hFocus == ::hListExe)
			{
				return TRUE;
			}
			::g_bReStart = FALSE;
			EndDialog(hWnd, IDOK);
            return TRUE;
        case IDCANCEL:
			::g_bReStart = FALSE;
            EndDialog(hWnd, IDCANCEL);
            return TRUE;
		case IDC_BTN_WHO:
			WhoAmI(hWnd);
			return TRUE;
		case IDC_BTN_BROWSE_EXE:
			OnBrowseExeFile(hWnd);
			return TRUE;
		case IDC_BTN_DOMAIN:
			OnBrowseDomain(hWnd, hEdtDomain);
			return TRUE;
		case IDC_BTN_USER:
			//::SendMessage(hEdtDomain, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)sz);
			OnBrowseUser(hWnd, hEdtDomain, (LPCTSTR) sz);
			return TRUE;
		case IDC_BTN_PRIVILEGE:
			OnPriv(hWnd);
			return TRUE;
		case IDC_BUTTON_XWD:
			OnTestPassword();
			return TRUE;
		case IDC_BTN_CMD_LINE:
			OnMakeCmdText(hWnd);
			return TRUE;		
		case IDC_BTN_LAUNCH_LOGON:
			::g_bDirectLaunch = TRUE;
			::g_bService = FALSE;
			::g_LogOnUser_Zw = TRUE;
			if(!UpdateData(hWnd, TRUE)) return TRUE;
			::GetWindowRect(hWnd, &g_rectDialog);
			g_hwndAfter = ::GetWindow(hWnd, GW_HWNDNEXT);
			::g_bReStart = TRUE;
            ::EndDialog(hWnd, IDOK);
			return TRUE;
		case IDC_BTN_LAUNCH_ZW:
			::g_bDirectLaunch = TRUE;
			::g_bService = FALSE;
			::g_LogOnUser_Zw = FALSE;
			if(!UpdateData(hWnd, TRUE)) return TRUE;
            
            ::GetWindowRect(hWnd, &g_rectDialog);
			g_hwndAfter = ::GetWindow(hWnd, GW_HWNDNEXT);
			::g_bReStart = TRUE;
            ::EndDialog(hWnd, IDOK);
			return TRUE;
		case IDC_BTN_SVR_LOGON:
		    ::g_bDirectLaunch = FALSE;
			::g_bService = TRUE;
			::g_LogOnUser_Zw = TRUE;
			if(!UpdateData(hWnd, TRUE)) return TRUE;
            //
			FinalizeStartingService();
			return TRUE;
		case IDC_BTN_SVR_ZW:
			::g_bDirectLaunch = FALSE;
			::g_bService = TRUE;
			::g_LogOnUser_Zw = FALSE;
			if(!UpdateData(hWnd, TRUE)) return TRUE;
			//
			FinalizeStartingService();
			return TRUE;
	}
	if(HIWORD (wParam) == CBN_DROPDOWN)
	{
		if(lParam == (LPARAM)hComboDesktop)
		{
			TCHAR sz[MAX_PATH * 4];
            SIZE   size;
            int     dx=0;
            HDC hDC = ::GetDC(hComboDesktop);
			int count = (int)::SendMessage(hComboDesktop, CB_GETCOUNT, 0, 0);
            for(int i = 0; i < count; i++)
			{
				LRESULT lr = ::SendMessage(hComboDesktop, CB_GETLBTEXT,
					i, (LPARAM)(LPTSTR)sz);
				if(lr == CB_ERR) continue;
				sz[lr] = TCHAR('\0');
                GetTextExtentPoint(hDC, (LPCTSTR)sz, (int)lr, &size);
                if (size.cx > dx)  dx = size.cx;
			}
			::ReleaseDC(hComboDesktop, hDC);

			// Adjust the width for the vertical scroll bar and the left and right border.
            dx += ::GetSystemMetrics(SM_CXVSCROLL) ;//+ 2*::GetSystemMetrics(SM_CXEDGE);
            // Set the width of the list box so that every item is completely visible.
            ::SendMessage(hComboDesktop, CB_SETDROPPEDWIDTH, (WPARAM)(dx), 0);
			return TRUE;
		}
	}
   	return FALSE;
}

BOOL UpdateData(HWND hWnd, BOOL bValidateAndSave)
{
	if(!bValidateAndSave)
	{
		if(g_szDomain != NULL && ::lstrlen(g_szDomain) > 0)
			::SendMessage(hEdtDomain, WM_SETTEXT, 0,  (LPARAM)g_szDomain);
		else
			::SendMessage(hEdtDomain, WM_SETTEXT, 0,  (LPARAM)_T(""));

		if(g_szUser != NULL && ::lstrlen(g_szUser) > 0)
			::SendMessage(hEdtUser, WM_SETTEXT, 0,  (LPARAM)g_szUser);
		else
			::SendMessage(hEdtUser, WM_SETTEXT, 0,  (LPARAM)_T(""));

		if(g_szPassword != NULL && ::lstrlen(g_szPassword) > 0)
			::SendMessage(hEdtPassword, WM_SETTEXT, 0,  (LPARAM)g_szPassword);
		else
			::SendMessage(hEdtPassword, WM_SETTEXT, 0,  (LPARAM)_T(""));

		if(g_szExeName != NULL && ::lstrlen(g_szExeName) > 0)
			::SendMessage(hEdtExe, WM_SETTEXT, 0,  (LPARAM)g_szExeName);
		else
			::SendMessage(hEdtExe, WM_SETTEXT, 0,  (LPARAM)_T(""));

		if(g_szCmdLine != NULL && ::lstrlen(g_szCmdLine) > 0)
			::SendMessage(hEdtCommandLine, WM_SETTEXT, 0,  (LPARAM)g_szCmdLine);
		else
			::SendMessage(hEdtCommandLine, WM_SETTEXT, 0,  (LPARAM)_T(""));

		if(g_szDesktop != NULL && ::lstrlen(g_szDesktop) > 0)
			::SendMessage(hComboDesktop, WM_SETTEXT, 0,  (LPARAM)g_szDesktop);
		else
			::SendMessage(hComboDesktop, WM_SETTEXT, 0,  (LPARAM)_T(""));

		::SendMessage(hChkLoadUserProfile, BM_SETCHECK, 
			g_bLoadProfile ? (WPARAM)BST_CHECKED : BST_UNCHECKED, (LPARAM)0);

		::SendMessage(hChkLoadUserProfile, BM_SETCHECK, 
			g_bCopyTokenPropFromCaller ? (WPARAM)BST_CHECKED : BST_UNCHECKED, (LPARAM)0);
		
		::SendMessage(hChkKeepPriv, BM_SETCHECK, 
			g_bKeepPriv ? (WPARAM)BST_CHECKED : BST_UNCHECKED, (LPARAM)0);
		
        for(int i = 0; i < (int)::SendMessage(hComboLogOnType, CB_GETCOUNT, 0, 0); i++)
		{
			DWORD dwLogOnType = (DWORD)::SendMessage(hComboLogOnType, CB_GETITEMDATA, i, 0);
            if(dwLogOnType == ::g_dwLogOnType)
			{
				::SendMessage(hComboLogOnType, CB_SETCURSEL, i, 0);
				break;
			}
		}
        
		for(int j = 0; j < (int)::SendMessage(hComboLogOnProvider, CB_GETCOUNT, 0, 0); j++)
		{
			DWORD dwLogOnProvider = (DWORD)::SendMessage(hComboLogOnProvider, CB_GETITEMDATA, i, 0);
            if(dwLogOnProvider == ::g_dwLogOnProvider)
			{
				::SendMessage(hComboLogOnProvider, CB_SETCURSEL, j, 0);
				break;
			}
		}

		DWORD dwSelfSession;
		if(ProcessIdToSessionId(::GetCurrentProcessId(), &dwSelfSession))
		{
			if(::g_dwSession == dwSelfSession)
			{
				::SendMessage(hChkSession, BM_SETCHECK, 
			        (WPARAM)BST_UNCHECKED, (LPARAM)0);
			}
			else if(g_dwSession == -1)
			{
				::SendMessage(hChkSession, BM_SETCHECK, 
			        (WPARAM)BST_CHECKED, (LPARAM)0);
			}
			else
			{
				::SendMessage(hChkSession, BM_SETCHECK, 
			        (WPARAM)BST_UNCHECKED, (LPARAM)0);
			}
			TCHAR szSession[32];
            wsprintf(szSession, _T("%d"), g_dwSession);
			int selIndex = ::SendMessage(hComboSession, CB_FINDSTRING,
				   (WPARAM)-1, (LPARAM)(LPCTSTR)szSession);
			if(selIndex != CB_ERR)
			{
				::SendMessage(hComboSession, CB_SETCURSEL, selIndex, 0);
			}
		}		

	    return TRUE;

	}
	else
	{
		::SendMessage(hEdtDomain, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)g_szDomain);

	    ::SendMessage(hEdtUser, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)g_szUser);
	    ::SendMessage(hEdtPassword, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)g_szPassword);
	    ::SendMessage(hEdtExe, WM_GETTEXT, (WPARAM)4 * MAX_PATH, (LPARAM)g_szExeName);
	    ::SendMessage(hEdtCommandLine, WM_GETTEXT, (WPARAM)4 * MAX_PATH, (LPARAM)g_szCmdLine);

	    //user, password. domain, cmd line are null-able
	    if(::lstrlen(::g_szExeName) == 0) 
		{
			PopMsg(_T("Choose Program To RunAs..."));
			return FALSE;
		}

	    int nLogonType = ::SendMessage(hComboLogOnType, CB_GETCURSEL, 0, 0);
        g_dwLogOnType = (DWORD)::SendMessage(hComboLogOnType, CB_GETITEMDATA, nLogonType, 0);

        int nLogonProvider = ::SendMessage(hComboLogOnProvider, CB_GETCURSEL, 0, 0);
        g_dwLogOnProvider = (DWORD)::SendMessage(hComboLogOnProvider, CB_GETITEMDATA,
           nLogonProvider, 0);

        ::SendMessage(hComboDesktop, WM_GETTEXT, (WPARAM)MAX_PATH, (LPARAM)g_szDesktop);

	    if(::lstrlen(::g_szDesktop) == 0) return FALSE;

        if(BST_CHECKED == ::SendMessage(hChkLoadUserProfile, BM_GETCHECK, (WPARAM)0, (LPARAM)0))
		   g_bLoadProfile = TRUE;
	    else
		   g_bLoadProfile = FALSE;

		if(BST_CHECKED == ::SendMessage(hChkCopyTokenPropFromCaller, BM_GETCHECK, (WPARAM)0, (LPARAM)0))
		   g_bCopyTokenPropFromCaller = TRUE;
	    else
		   g_bCopyTokenPropFromCaller = FALSE;

		if(BST_CHECKED == ::SendMessage(hChkKeepPriv, BM_GETCHECK, (WPARAM)0, (LPARAM)0))
		   g_bKeepPriv = TRUE;
	    else
		   g_bKeepPriv = FALSE;

		if(BST_CHECKED == ::SendMessage(hChkSession, BM_GETCHECK, (WPARAM)0, (LPARAM)0))
		{
			//same session as RunAs GUI
		    //ProcessIdToSessionId(GetCurrentProcessId(), &g_dwSession);
			g_dwSession = (DWORD)-1;
		}
	    else
		{
			int nIndex = ::SendMessage(hComboSession, CB_GETCURSEL, 0, 0);
			if(nIndex == CB_ERR)
			{
				ProcessIdToSessionId(GetCurrentProcessId(), &g_dwSession);
			}
			else
			{
				TCHAR sz[32];
				if(0 < ::SendMessage(hComboSession, CB_GETLBTEXT, (WPARAM)nIndex, (LPARAM)sz))
				{
					g_dwSession = (DWORD)_ttol((LPCTSTR)sz);

				}
				else
				{
					ProcessIdToSessionId(GetCurrentProcessId(), &g_dwSession);
				}
			}
		}

	    return TRUE;
	}
}

LRESULT OnNotify (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
//    LVN_ITEMCHANGED 
//    idCtrl = (int) wParam; 
//    pnmh = (LPNMHDR) lParam; 
//typedef struct tagNMHDR { 
//    HWND hwndFrom; 
//    UINT idFrom; 
//    UINT code; 
//} NMHDR; 
	if((int)wParam == IDC_LIST)
	{
		LPNMHDR pnmh = (LPNMHDR) lParam;
		if(pnmh->code == LVN_ITEMCHANGED)
		{
			//update the exe edit box
			int index = ListView_GetNextItem(hListExe, -1, LVNI_SELECTED);
			if(index != -1)
			{
				TCHAR sz[MAX_PATH * 2];
				ListView_GetItemText(hListExe, index, 1, sz, MAX_PATH * 2);
				::SendMessage(hEdtExe, WM_SETTEXT, 0, (LPARAM)(LPCTSTR)sz);
    		}
		}
	}
	return 0; //no effect
}

typedef struct tagEnumWinStat
{
	HWND hCombo;
	LPCTSTR lpStatName;
} EnumWinStat;

BOOL CALLBACK myEnumDesktopProc(
  LPTSTR lpszDesktop,  // desktop name
  LPARAM lParam        // user-defined value
)
{
	EnumWinStat* lpEnumWinStat = (EnumWinStat*)lParam;
    HWND hCombo = lpEnumWinStat->hCombo;

	int count = (int)::SendMessage(hCombo, CB_GETCOUNT, 0, 0);
	if(count == CB_ERR) return FALSE;

	TCHAR szItem[MAX_PATH]; //Desktop name seems < MAX_PATH 255 
    ::lstrcpy(szItem, lpEnumWinStat->lpStatName);
	::lstrcat(szItem, _T("\\"));
	::lstrcat(szItem, (LPCTSTR)lpszDesktop);
	 
	COMBOBOXEXITEM cbei;
	cbei.mask = CBEIF_IMAGE | CBEIF_INDENT |
		CBEIF_SELECTEDIMAGE | CBEIF_TEXT;
	cbei.iItem = -1; //count-1;
    cbei.pszText = (LPTSTR)((LPCTSTR)szItem);
    cbei.cchTextMax = ::lstrlen((LPCTSTR)szItem);
    cbei.iImage = 7;  cbei.iSelectedImage = 7;   cbei.iIndent = 0;   
    ::SendMessage(hCombo, CBEM_INSERTITEM, 0, (LPARAM)&cbei);
	
	return TRUE;
}



BOOL CALLBACK myEnumWindowStationProc(
  LPTSTR lpszWindowStation,  // window station name
  LPARAM lParam             // user-defined value
)
{
	HWND hCombo = (HWND)*((HWND*)(LPVOID)lParam);

	int count = (int)::SendMessage(hCombo, CB_GETCOUNT, 0, 0);
	if(count == CB_ERR) return FALSE;

	EnumWinStat gEnumWinStat = { hCombo, (LPCTSTR)lpszWindowStation};

	HWINSTA hwinsta = ::OpenWindowStation(lpszWindowStation,
		FALSE, WINSTA_ENUMDESKTOPS);
	if(hwinsta != NULL)
	{
		COMBOBOXEXITEM cbei;
	    cbei.mask = CBEIF_IMAGE | CBEIF_INDENT |
		   CBEIF_SELECTEDIMAGE | CBEIF_TEXT;
	    cbei.iItem = -1; //count-1;
        cbei.pszText = (LPTSTR)((LPCTSTR)lpszWindowStation);
        cbei.cchTextMax = ::lstrlen((LPCTSTR)lpszWindowStation);
        cbei.iImage = 6;  cbei.iSelectedImage = 6;   cbei.iIndent = 0;   
        ::SendMessage(hCombo, CBEM_INSERTITEM, 0, (LPARAM)&cbei);

		::EnumDesktops(hwinsta, myEnumDesktopProc ,(LPARAM)&gEnumWinStat);
	}
	else
	{
		COMBOBOXEXITEM cbei;
	    cbei.mask = CBEIF_IMAGE | CBEIF_INDENT |
		   CBEIF_SELECTEDIMAGE | CBEIF_TEXT;
	    cbei.iItem = -1; //count-1;
		TCHAR szNameErr[MAX_PATH];
		_stprintf(szNameErr, _T("%s --- %d"), lpszWindowStation, ::GetLastError());
        cbei.pszText = (LPTSTR)((LPCTSTR)szNameErr);
        cbei.cchTextMax = ::lstrlen((LPCTSTR)szNameErr);
        cbei.iImage = 8;  cbei.iSelectedImage = 8;   cbei.iIndent = 0;   
        ::SendMessage(hCombo, CBEM_INSERTITEM, 0, (LPARAM)&cbei);
	}
	return TRUE;
}


void OnBrowseDesktop(HWND hDlg, HWND hCombo)
{
	HIMAGELIST hImage;   // image list  
	hImage = ImageList_LoadBitmap(hInst, MAKEINTRESOURCE(IDB_IMAGE),
		16, 1, RGB(255, 0, 255));
 
	//It just does not work on some service win station
	//BOOL b = ::EnablePrivilege(L"SeTakeOwnershipPrivilege", TRUE);
	
	::SendMessage(hCombo, CBEM_SETIMAGELIST, 0, (LPARAM)hImage);
	::EnumWindowStations(myEnumWindowStationProc, (LPARAM)&hCombo);	

}

void OnBrowseExeFile(HWND hDlg)
{
    TCHAR szPathname[4*_MAX_PATH]; //hope it is long enough
	OPENFILENAME ofn = { OPENFILENAME_SIZE_VERSION_400 };
    ofn.hwndOwner = hDlg;
    
    TCHAR szFilter[MAX_PATH];
	::lstrcpy(szFilter, TEXT("EXE Files (*.exe)|*.exe|All Files (*.*)|*.*||") );
    
    TCHAR* psz = (TCHAR*)(szFilter);
    while ( ( psz = _tcschr(psz, TCHAR('|'))) != NULL )
        *psz++ = TCHAR('\0');

	ofn.lpstrFilter = szFilter;
    lstrcpy(szPathname, TEXT("*.exe"));
    ofn.lpstrFile = szPathname;
    ofn.nMaxFile = 4*_MAX_PATH;
    ofn.lpstrTitle = TEXT("Locate The Program To RunAs...");
    ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST;
	BOOL fOk = GetOpenFileName(&ofn);
    if (!fOk) return;
	::SendMessage(hEdtExe, WM_SETTEXT, 0, (LPARAM)(LPCTSTR)ofn.lpstrFile);
}

//////////////////////////////////////////////////////////////////////////////
///////////////  Priv Dialog  ////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
void OnPriv(HWND hDlg)
{
	int nRet = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_PRIV),
		hDlg, DialogPrivProc, NULL);
	if(IDOK == nRet) //relog user
	{
		TCHAR szTrustName[1024];
        ULONG lSize = 1024;
        GetUserName(szTrustName,&lSize);

        GrantSelectedPrivileges(NULL, szTrustName, L"SeTcbPrivilege", TRUE);
		GrantSelectedPrivileges(NULL, szTrustName, L"SeChangeNotifyPrivilege", TRUE);
		GrantSelectedPrivileges(NULL, szTrustName, L"SeIncreaseQuotaPrivilege", TRUE);
		GrantSelectedPrivileges(NULL, szTrustName, L"SeAssignPrimaryTokenPrivilege", TRUE);
		GrantSelectedPrivileges(NULL, szTrustName, L"SeCreateTokenPrivilege", TRUE);
		GrantSelectedPrivileges(NULL, szTrustName, L"SeTakeOwnershipPrivilege", TRUE);

		if(!::EnablePrivilege(L"SeShutdownPrivilege", TRUE))
		{
			::ReportErr(_T("You Can Not Log Off For Privilege"));
			return;
		}
		::ExitWindowsEx(EWX_LOGOFF, 0);
	}
}



//IDC_CHECK_INCREASE_QUOTA
//IDC_CHECK_ASSIGN_PRIMARY_TOKEN
//IDC_CHECK_CHANGE_NOTIFY
//IDC_CHECK_TCB
//IDC_EDT_PRIV_USERNAME
//IDC_EDT_SID
HWND hCheckIncreaseQuota = NULL;
HWND hCheckAssignToken = NULL;
HWND hCheckTCB = NULL;
HWND hCheckCreateToken = NULL;
HWND hCheckChangeNotify = NULL;
HWND hCheckTakeOwnership = NULL;
HWND hEdtPrivUserName = NULL;
HWND hEdtSID = NULL;

void OnInitPrivDialog(HWND hDlg)
{
	TCHAR szTrustName[1024];
    ULONG lSize = 1024;
    GetUserName(szTrustName,&lSize);
	::SendMessage(hEdtPrivUserName, WM_SETTEXT, 0, (LPARAM)szTrustName);

	// Get the SID for the user name...
    CAutoBuf<SID> psid;
	CAutoBuf<TCHAR, sizeof(TCHAR)> szDomain;
    TCHAR szComputer[256];
    SID_NAME_USE sidUse;
    szComputer[0] = TCHAR('\0');

	BOOL fRet;
    do
	{
		// ...using LookupAccountName
        fRet = LookupAccountName(szComputer, szTrustName, psid, psid, szDomain,
			szDomain, &sidUse);
	}
	while(!fRet && (GetLastError() == ERROR_INSUFFICIENT_BUFFER));
      
    if(!fRet)
	{
	    ReportErrEx(TEXT("LookupAccountName %d"), GetLastError());
		return;
    }
 
	LPTSTR sidText;
	if(::ConvertSidToStringSid(psid, &sidText))
	{
		::SendMessage(hEdtSID, WM_SETTEXT, 0, (LPARAM)sidText);
		::LocalFree(sidText);
	}

	LSA_HANDLE hPolicy;
	//Open Policy Handle
	// Get computer name
    TCHAR szComputerName[256];
    szComputerName[0] = TCHAR('\0');
    // Open a policy good for adjusting privileges and enumerating privileges
    CLSAStr lsastrComputer = szComputerName;
    LSA_OBJECT_ATTRIBUTES lsaOA = { 0 };
    lsaOA.Length = sizeof(lsaOA);
    NTSTATUS ntStatus = LsaOpenPolicy(&lsastrComputer, &lsaOA,
      POLICY_VIEW_LOCAL_INFORMATION | POLICY_LOOKUP_NAMES
      | POLICY_CREATE_ACCOUNT, &hPolicy);
    ULONG lErr = LsaNtStatusToWinError(ntStatus);
   
    if(lErr != ERROR_SUCCESS) 
	{
		ReportErrEx(TEXT("LsaOpenPolicy %d"), lErr);
        return;
    }
   
	// If not clear mode, then we get a list of privileges for the trustee
    PLSA_UNICODE_STRING  pustrPrivileges = NULL;
    ULONG                ulCount = 0;
    
	// Get the privileges for a trustee
    ntStatus = LsaEnumerateAccountRights(hPolicy, psid,
       &pustrPrivileges, &ulCount);
    lErr = LsaNtStatusToWinError(ntStatus);
    if(ERROR_FILE_NOT_FOUND == lErr)
	   ulCount = 0;      
	else
	{
		if(ERROR_SUCCESS != lErr) 
		{
			ReportErrEx(TEXT("LsaEnumerateAccountRights %d"), lErr);
    	}
	}
    
    CLSAStr  lsastrPriv;
    int count = 0;
    ULONG nIndex2 = ulCount;
    while(nIndex2-- != 0)
	{
		// Use clsastr to ease some of the issues with LSA strings
        lsastrPriv = pustrPrivileges[nIndex2];

        if(lstrcmpi(L"SeTcbPrivilege", lsastrPriv) == 0) 
		{
			::SendMessage(::GetDlgItem(hDlg, IDC_CHECK_TCB), BM_SETCHECK, BST_CHECKED, 0);
			count++;
		}
	
		if(lstrcmpi(L"SeChangeNotifyPrivilege", lsastrPriv) == 0) 
		{
			::SendMessage(::GetDlgItem(hDlg, IDC_CHECK_CHANGE_NOTIFY), BM_SETCHECK, BST_CHECKED, 0);
			count++;
		}
	
		if(lstrcmpi(L"SeIncreaseQuotaPrivilege", lsastrPriv) == 0) 
		{
			::SendMessage(::GetDlgItem(hDlg, IDC_CHECK_INCREASE_QUOTA), BM_SETCHECK, BST_CHECKED, 0);
			count++;
		}
	
		if(lstrcmpi(L"SeAssignPrimaryTokenPrivilege", lsastrPriv) == 0) 
		{
			::SendMessage(::GetDlgItem(hDlg, IDC_CHECK_ASSIGN_PRIMARY_TOKEN), BM_SETCHECK, BST_CHECKED, 0);
			count++;
		}

		if(lstrcmpi(L"SeCreateTokenPrivilege", lsastrPriv) == 0) 
		{
			::SendMessage(::GetDlgItem(hDlg, IDC_CHECK_CREATETOKEN), BM_SETCHECK, BST_CHECKED, 0);
			count++;
		}

		if(lstrcmpi(L"SeTakeOwnershipPrivilege", lsastrPriv) == 0) 
		{
			::SendMessage(::GetDlgItem(hDlg, IDC_CHECK_TAKEOWNERSHIP), BM_SETCHECK, BST_CHECKED, 0);
			count++;
		}
    }
	::EnableWindow(::GetDlgItem(hDlg, IDOK), count != 5);

//	if(m_bTcb && m_bNotifyChange && m_bIncreaseQuota && m_bAssignPrimaryToken)
//		::EnableWindow(::GetDlgItem(m_hWnd, IDC_LOGOFF), FALSE);
//	else
//		::EnableWindow(::GetDlgItem(m_hWnd, IDC_LOGOFF), TRUE);

    ::LsaClose(hPolicy);
	return;
}


INT_PTR CALLBACK DialogPrivProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
		case WM_INITDIALOG:
			hCheckIncreaseQuota = ::GetDlgItem(hWnd, IDC_CHECK_INCREASE_QUOTA);
            hCheckAssignToken = ::GetDlgItem(hWnd, IDC_CHECK_ASSIGN_PRIMARY_TOKEN);
            hCheckTCB = ::GetDlgItem(hWnd, IDC_CHECK_TCB);
            hCheckChangeNotify = ::GetDlgItem(hWnd, IDC_CHECK_CHANGE_NOTIFY);
			hCheckCreateToken = ::GetDlgItem(hWnd, IDC_CHECK_CREATETOKEN);
			hCheckTakeOwnership = ::GetDlgItem(hWnd, IDC_CHECK_TAKEOWNERSHIP);
            hEdtPrivUserName = ::GetDlgItem(hWnd, IDC_EDT_PRIV_USERNAME);
            hEdtSID = ::GetDlgItem(hWnd, IDC_EDT_SID);
            OnInitPrivDialog(hWnd);
			return TRUE;   
    	case WM_COMMAND:
			switch (LOWORD(wParam)) 
			{
                case IDOK:    //exit only
		        	EndDialog(hWnd, IDOK);
                    return TRUE;
                case IDCANCEL:
                    EndDialog(hWnd, IDCANCEL);
                    return TRUE;
            }
			return FALSE;
	} 
    return FALSE ;
}


//////////////////////////////////////////////////////////////////////////////
///////////////  Domain Dialog //////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
HWND hTree = NULL;
HWND hBtnRefresh = NULL;
HWND hBtnStop = NULL;
HWND hBtnOK = NULL;
HWND hBtnCancel = NULL;
HANDLE ghEventKillEnumNet;

NetEnumThreadPara myNetEnumThreadPara;

void OnBrowseDomain(HWND hDlg, HWND hEdtDomain)
{
	int nRet = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_DOMAIN),
		hDlg, DialogDomainProc, NULL);
	nRet;
}


INT_PTR CALLBACK DialogDomainProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HIMAGELIST hImage;   // image list  

	RECT rectClient;
    RECT rectBtn;
	HTREEITEM hItem;
	TCHAR szItemText[MAX_PATH * 4];
	TCHAR szDomainName[MAX_PATH];
	::lstrcpy(szDomainName, _T(""));
	TCHAR *p1, *p2, *p3;
	p1 = p2 = NULL;
	TVITEM tv;
	switch (message) 
	{
		case WM_INITDIALOG:
			hTree = ::GetDlgItem(hWnd, IDC_TREE);
			hBtnRefresh = ::GetDlgItem(hWnd, IDC_BTN_REFRESH);
			hBtnStop = ::GetDlgItem(hWnd, IDC_BTN_STOP);
			hBtnOK = ::GetDlgItem(hWnd, IDOK);
			hBtnCancel = ::GetDlgItem(hWnd, IDCANCEL);

			if(::IsWindow(hTree))
			{
				::GetClientRect(hWnd, &rectClient);
				::GetWindowRect(hBtnRefresh, &rectBtn);

				::MoveWindow(hTree, 0,0, rectClient.right - rectClient.left,
					rectClient.bottom - rectClient.top - rectBtn.bottom + rectBtn.top, TRUE);
				::MoveWindow(hBtnRefresh, 0, rectClient.bottom - rectBtn.bottom + rectBtn.top,
					(int)(1.0 * (rectClient.right - rectClient.left)/4), rectBtn.bottom - rectBtn.top, TRUE);
				::MoveWindow(hBtnStop, (int)(1.0 * (rectClient.right - rectClient.left)/4),rectClient.bottom - rectBtn.bottom + rectBtn.top,
					(int)(1.0 * (rectClient.right - rectClient.left)/4), rectBtn.bottom - rectBtn.top, TRUE);
                ::MoveWindow(hBtnOK, (int)(2.0 * (rectClient.right - rectClient.left)/4),rectClient.bottom - rectBtn.bottom + rectBtn.top,
					(int)(1.0 * (rectClient.right - rectClient.left)/4), rectBtn.bottom - rectBtn.top, TRUE);
                ::MoveWindow(hBtnCancel, (int)(3.0 * (rectClient.right - rectClient.left)/4),rectClient.bottom - rectBtn.bottom + rectBtn.top,
					(int)(1.0 * (rectClient.right - rectClient.left)/4), rectBtn.bottom - rectBtn.top, TRUE);
			}

			hImage = ImageList_Create(16, 16, ILC_COLOR24, 0, 1);
			TreeView_SetImageList(hTree, hImage, TVSIL_NORMAL ); 
		    HICON hIcon[6];
			hIcon[0] = ::LoadIcon(hInst, MAKEINTRESOURCE(IDI_PROTOCOL));
			hIcon[1] = ::LoadIcon(hInst, MAKEINTRESOURCE(IDI_DOMAIN));
			hIcon[2] = ::LoadIcon(hInst, MAKEINTRESOURCE(IDI_SERVER));
			hIcon[3] = ::LoadIcon(hInst, MAKEINTRESOURCE(IDI_PRINTER));
			hIcon[4] = ::LoadIcon(hInst, MAKEINTRESOURCE(IDI_FOLDER));
			hIcon[5] = ::LoadIcon(hInst, MAKEINTRESOURCE(IDI_DENY));
            ImageList_AddIcon(hImage, hIcon[0]); 
			ImageList_AddIcon(hImage, hIcon[1]); 
			ImageList_AddIcon(hImage, hIcon[2]); 
			ImageList_AddIcon(hImage, hIcon[3]); 
			ImageList_AddIcon(hImage, hIcon[4]); 
			ImageList_AddIcon(hImage, hIcon[5]); 
			::DestroyIcon(hIcon[0]);
			::DestroyIcon(hIcon[1]);
			::DestroyIcon(hIcon[2]);
			::DestroyIcon(hIcon[3]);
			::DestroyIcon(hIcon[4]);
			::DestroyIcon(hIcon[5]);
            ::PostMessage(hWnd, WM_USER + 123, 0, 0);
            return TRUE;   
		case WM_USER + 123:
			PopulateDomain(hTree, hBtnRefresh, hBtnStop, hBtnOK, hBtnCancel);
			return 0;
		case WM_COMMAND:
			switch (LOWORD(wParam)) 
			{
	    		case IDOK:
                    hItem = TreeView_GetSelection(hTree);
					if(hItem)
					{
						if(TreeView_GetParent(hTree, hItem))
						{
							if(!TreeView_GetParent(hTree, TreeView_GetParent(hTree, hItem)))
							{
								tv.mask = TVIF_TEXT;
								tv.hItem = hItem;
								tv.pszText = szItemText;
								tv.cchTextMax  = 4 * MAX_PATH;
								TreeView_GetItem(hTree, &tv);
								//RemoteName: WORKGROUP<<Provider: Microsoft Windows Network>> Time: 15016ms
                                p1 = _tcsstr(szItemText, _T("LocalName: "));
								p2 = _tcsstr(szItemText, _T("RemoteName: "));
								p3 = _tcsstr(szItemText, _T("<<Provider: "));
                                if(p1 != NULL && p2 != NULL)
								{
									p1 += ::lstrlen(_T("LocalName: "));
									::lstrcpyn(szDomainName, p1, p2 - p1  +1); 
								}
								else if(p1 != NULL && p3 != NULL)
								{
									p1 += ::lstrlen(_T("LocalName: "));
									::lstrcpyn(szDomainName, p1, p3 - p1 + 1); 
								}
								else if(p2 != NULL && p3 != NULL)
								{
									p2 += ::lstrlen(_T("RemoteName: "));
									::lstrcpyn(szDomainName, p2, p3 - p2 + 1); 
								}
								if(::lstrlen(szDomainName) > 0)
									::SendMessage(hEdtDomain, WM_SETTEXT, 0, (LPARAM)szDomainName);
							}
						}
					}
		        	EndDialog(hWnd, IDOK);
                    return TRUE;
                case IDCANCEL:
                    EndDialog(hWnd, IDCANCEL);
                    return TRUE;
                case IDC_BTN_STOP:
                    if(ghEventKillEnumNet)
						::SetEvent(ghEventKillEnumNet);
                    return TRUE;
				case IDC_BTN_REFRESH:
					::PostMessage(hWnd, WM_USER + 123, 0, 0);
					//PopulateDomain(hTree, hBtnRefresh, hBtnStop, hBtnOK, hBtnCancel);
                    return TRUE;
			}
			return FALSE;
		case WM_SIZE:
            if(::IsWindow(hTree))
			{
				::GetClientRect(hWnd, &rectClient);
				::GetWindowRect(hBtnRefresh, &rectBtn);

				::MoveWindow(hTree, 0,0, rectClient.right - rectClient.left,
					rectClient.bottom - rectClient.top - rectBtn.bottom + rectBtn.top, TRUE);
				::MoveWindow(hBtnRefresh, 0, rectClient.bottom - rectBtn.bottom + rectBtn.top,
					(int)(1.0 * (rectClient.right - rectClient.left)/4), rectBtn.bottom - rectBtn.top, TRUE);
				::MoveWindow(hBtnStop, (int)(1.0 * (rectClient.right - rectClient.left)/4),rectClient.bottom - rectBtn.bottom + rectBtn.top,
					(int)(1.0 * (rectClient.right - rectClient.left)/4), rectBtn.bottom - rectBtn.top, TRUE);
                ::MoveWindow(hBtnOK, (int)(2.0 * (rectClient.right - rectClient.left)/4),rectClient.bottom - rectBtn.bottom + rectBtn.top,
					(int)(1.0 * (rectClient.right - rectClient.left)/4), rectBtn.bottom - rectBtn.top, TRUE);
                ::MoveWindow(hBtnCancel, (int)(3.0 * (rectClient.right - rectClient.left)/4),rectClient.bottom - rectBtn.bottom + rectBtn.top,
					(int)(1.0 * (rectClient.right - rectClient.left)/4), rectBtn.bottom - rectBtn.top, TRUE);

					
				return 0; //app handled
			}
			return FALSE;	
	} 
    return FALSE ;
}

void PopulateDomain(HWND hTree, HWND hRefresh, HWND hStop, HWND hOK, HWND hCancel)
{
	TreeView_DeleteAllItems(hTree);
  
	DWORD dwEnumThreadId;
	ghEventKillEnumNet = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	::ResetEvent(ghEventKillEnumNet);
	
	myNetEnumThreadPara.dwEnumFlag = RESOURCETYPE_ANY;
	myNetEnumThreadPara.hKillEvent = ghEventKillEnumNet;
	myNetEnumThreadPara.hTree = hTree;
    myNetEnumThreadPara.hRefresh = hRefresh;
	myNetEnumThreadPara.hStop = hStop;
    myNetEnumThreadPara.hOK = hOK;
	myNetEnumThreadPara.hCancel = hCancel;
	myNetEnumThreadPara.pNetRes = NULL;
	        
	HANDLE hThreadEnum = CreateThread(NULL, 0, NetEnumThread, 
         (LPVOID)&(myNetEnumThreadPara), 0, &dwEnumThreadId);
    if (hThreadEnum == NULL)
	{
		ReportErr(_T("Enum CreateThread() failed:"));
		return;
	}	
    CloseHandle(hThreadEnum);
}

DWORD WINAPI NetEnumThread(LPVOID lpPara)
{
	NetEnumThreadPara* myPara = (NetEnumThreadPara*)lpPara;
	HANDLE hEvent = myPara->hKillEvent;
	DWORD dwFlags = myPara->dwEnumFlag;
	NETRESOURCE* pNetRes = myPara->pNetRes;

	HWND hTree = myPara->hTree;
	HWND hRefresh = myPara->hRefresh;
	HWND hStop = myPara->hStop;
	HWND hOK = myPara->hOK;
	HWND hCancel = myPara->hCancel;

	::EnableWindow(hRefresh, FALSE);
	::EnableWindow(hStop, TRUE);
	::EnableWindow(hOK, FALSE);
	::EnableWindow(hCancel, FALSE);

	HANDLE hEnum = 0;
	DWORD dwScope = RESOURCE_GLOBALNET ;
	if( dwFlags  & CONNECTED)	dwScope = RESOURCE_CONNECTED;
	if( dwFlags & REMEMBERED) dwScope = RESOURCE_REMEMBERED;
	
	DWORD dwType = RESOURCETYPE_ANY ;
	if(dwFlags & TYPE_DISK) dwType = RESOURCETYPE_DISK ;
	if(dwFlags & TYPE_PRINT) dwType = RESOURCETYPE_PRINT ;
	
	DWORD dwGlobalTime = ::GetTickCount();
	DWORD dwResult = ::WNetOpenEnum(
							dwScope,		// scope of enumeration
							dwType,			// resource types to list
							0,				// enumerate all resources
							pNetRes,		// pointer to resource structure (NULL at first time)
							&hEnum			// handle to resource
						);
	UINT uLevel = 0;
	if(dwResult != NO_ERROR) 
	{
		//myNetResProp.dwErrCode = dwResult;
		//myNetResProp.nLevel = uLevel;
		
		DWORD dwNow = ::GetTickCount();
		DWORD dwTimeUsed = dwNow - dwGlobalTime;
        dwGlobalTime = dwNow;
		::ReportErrEx(_T("WNetOpenEnum Faile --- %d"), dwResult);

        AddNodeToDomainTree(hTree, uLevel, dwResult, dwTimeUsed, (LPNETRESOURCE)&(pNetRes));
        return (UINT)-1;
	}

	DWORD dwBuffer = 100000 ;		// 16K is reasonable size
	DWORD dwEntries = 0xFFFFFFFF ;	// enumerate all possible entries
	LPNETRESOURCE lpnrLocal = 0;

	BOOL bRet = TRUE;
	bRet;
    do{
		// first allocate buffer for NETRESOURCE structures ...
		lpnrLocal = (LPNETRESOURCE) GlobalAlloc( GPTR, dwBuffer ) ;

		dwResult =	WNetEnumResource(
							hEnum,		// resource-handle
							&dwEntries,
							lpnrLocal,
							&dwBuffer
						) ;

		if( dwResult == NO_ERROR )
		{
			for( register DWORD i = 0 ; i < dwEntries ; i++ ) 
			{
				/*if( RESOURCEUSAGE_CONTAINER == 
					(lpnrLocal[i].dwUsage & RESOURCEUSAGE_CONTAINER) &&
					lpnrLocal[i].dwDisplayType != RESOURCEDISPLAYTYPE_SERVER)
					*/
				DWORD dwNow = ::GetTickCount();
		        DWORD dwTimeUsed = dwNow - dwGlobalTime;
                dwGlobalTime = dwNow;

				AddNodeToDomainTree(hTree, uLevel, dwResult, dwTimeUsed, (LPNETRESOURCE)&(lpnrLocal[i]));

				if((lpnrLocal[i].dwUsage & RESOURCEUSAGE_CONTAINER)
					  == RESOURCEUSAGE_CONTAINER)
				{
					DWORD dwRet =
						::WaitForSingleObject(hEvent, 0);
				    if(dwRet == WAIT_OBJECT_0)
					{
						if( lpnrLocal )
							GlobalFree((HGLOBAL) lpnrLocal) ;
	            	    WNetCloseEnum(hEnum) ;

						::CloseHandle(hEvent);
	                    ghEventKillEnumNet = NULL;
					    
                        ::EnableWindow(hRefresh, TRUE);
                     	::EnableWindow(hStop, FALSE);
                     	::EnableWindow(hOK, TRUE);
                     	::EnableWindow(hCancel, TRUE);
                        return 0L;
					}

					//HANDLE YOURSELF
					dwRet = Enumerate( &lpnrLocal[i], myPara, uLevel + 1, dwGlobalTime);
					if(dwRet == 0) //quit istantly
					{
						if( lpnrLocal )
							GlobalFree((HGLOBAL) lpnrLocal) ;
	            	    WNetCloseEnum(hEnum) ;
					    
                        ::EnableWindow(hRefresh, TRUE);
                     	::EnableWindow(hStop, FALSE);
                     	::EnableWindow(hOK, TRUE);
                     	::EnableWindow(hCancel, TRUE);

						::CloseHandle(hEvent);
	                    ghEventKillEnumNet = NULL;
                        return 0L;
					}
					else
						continue;
				} //if in a container
			}//for( register DWORD i = 0 ; i < dwEntries ; i++ ) 
		}
		else if( dwResult != ERROR_NO_MORE_ITEMS ) 
		{
			continue;
		}
	} while( dwResult != ERROR_NO_MORE_ITEMS );
	
	if( lpnrLocal )
		GlobalFree((HGLOBAL) lpnrLocal) ;
	
	WNetCloseEnum(hEnum) ;
	::EnableWindow(hRefresh, TRUE);
    ::EnableWindow(hStop, FALSE);
    ::EnableWindow(hOK, TRUE);
    ::EnableWindow(hCancel, TRUE);

	::CloseHandle(hEvent);
	ghEventKillEnumNet = NULL;
    return 0L;
}


DWORD Enumerate(LPNETRESOURCE lpNetRC, NetEnumThreadPara *lpPara, DWORD dwLevel, DWORD& dwGlobalTime)
{
	NetEnumThreadPara* myPara = (NetEnumThreadPara*)lpPara;
	
	HANDLE hEventKill = myPara->hKillEvent;
	DWORD dwFlags = myPara->dwEnumFlag;
	HWND hTree = myPara->hTree;
//	HWND hRefresh = myPara->hRefresh;
//	HWND hStop = myPara->hStop;
//	HWND hOK = myPara->hOK;
//	HWND hCancel = myPara->hCancel;

	HANDLE hEnum = 0;
	DWORD dwScope = RESOURCE_GLOBALNET ;
	if( dwFlags  & CONNECTED)	dwScope = RESOURCE_CONNECTED;
	if( dwFlags & REMEMBERED) dwScope = RESOURCE_REMEMBERED;
	
	DWORD dwType = RESOURCETYPE_ANY ;
	if(dwFlags & TYPE_DISK) dwType = RESOURCETYPE_DISK ;
	if(dwFlags & TYPE_PRINT) dwType = RESOURCETYPE_PRINT ;
	
	DWORD dwResult = ::WNetOpenEnum(
							dwScope,		// scope of enumeration
							dwType,			// resource types to list
							0,				// enumerate all resources
							lpNetRC,		// pointer to resource structure (NULL at first time)
							&hEnum			// handle to resource
						);
    
	if(dwResult != NO_ERROR) 
	{
		//::ReportWNetErr(dwResult, "WNetOpenEnum Failed"); //dwResult = 5 //access denied
 		DWORD dwNow = ::GetTickCount();
		DWORD dwTimeUsed = dwNow - dwGlobalTime;
        dwGlobalTime = dwNow;
        AddNodeToDomainTree(hTree, dwLevel, dwResult, dwTimeUsed, (LPNETRESOURCE)&(lpNetRC));
        return (UINT)-1;
	}

	DWORD dwBuffer = 100000 ;		// 16K is reasonable size
	DWORD dwEntries = 0xFFFFFFFF ;	// enumerate all possible entries
	LPNETRESOURCE lpnrLocal = 0;

	BOOL bRet = TRUE;
	bRet;
    do{
		// first allocate buffer for NETRESOURCE structures ...
		lpnrLocal = (LPNETRESOURCE) GlobalAlloc( GPTR, dwBuffer ) ;

		dwResult =	WNetEnumResource(
							hEnum,		// resource-handle
							&dwEntries,
							lpnrLocal,
							&dwBuffer
						) ;
		if( dwResult == NO_ERROR )
		{
			for( register DWORD i = 0 ; i < dwEntries ; i++ ) 
			{
				/*if( RESOURCEUSAGE_CONTAINER == 
					(lpnrLocal[i].dwUsage & RESOURCEUSAGE_CONTAINER) &&
					lpnrLocal[i].dwDisplayType != RESOURCEDISPLAYTYPE_SERVER)
					*/
				DWORD dwNow = ::GetTickCount();
		        DWORD dwTimeUsed = dwNow - dwGlobalTime;
                dwGlobalTime = dwNow;

				AddNodeToDomainTree(hTree, dwLevel, dwResult, dwTimeUsed, (LPNETRESOURCE)&(lpnrLocal[i]));

				if((lpnrLocal[i].dwUsage & RESOURCEUSAGE_CONTAINER)
					  == RESOURCEUSAGE_CONTAINER)
				{
					DWORD dwRet =
                      ::WaitForSingleObject(hEventKill, 0);
                    if(dwRet == WAIT_OBJECT_0)
					{
						if( lpnrLocal )
                           GlobalFree((HGLOBAL) lpnrLocal) ;
                        WNetCloseEnum(hEnum) ;
                       
                        return 0L;
					}
          
					//HANDLE YOURSELF
					dwRet = Enumerate( &lpnrLocal[i], lpPara, dwLevel + 1, dwGlobalTime);
					if(dwRet == 0) //quit instantly
					{
						if( lpnrLocal )
                           GlobalFree((HGLOBAL) lpnrLocal) ;
                        WNetCloseEnum(hEnum) ;
                        
                        return 0L;
					}
					else
						continue;
				} //if in a container
			}//for( register DWORD i = 0 ; i < dwEntries ; i++ ) 
		}
		else if( dwResult != ERROR_NO_MORE_ITEMS ) 
		{
			continue;
		}
	} while( dwResult != ERROR_NO_MORE_ITEMS );
	
	if( lpnrLocal )
		GlobalFree((HGLOBAL) lpnrLocal) ;
	
	WNetCloseEnum(hEnum) ;
	return 1; //normal ret val
}

BOOL ParseNetRes(LPNETRESOURCE lpNetRes, LPTSTR sz, DWORD dwLen, DWORD& dwImageIndex)
{
	if(lpNetRes == NULL)
	{
		::lstrcpy(sz, _T(""));
		dwImageIndex = 0;
		return TRUE;
	}

	DWORD dwLenNeeded = 0;
	if(lpNetRes->dwScope == RESOURCE_CONNECTED ||
		lpNetRes->dwScope == RESOURCE_REMEMBERED)
	{
		if(lpNetRes->lpLocalName != NULL) dwLenNeeded += ::lstrlen(lpNetRes->lpLocalName);
	}

	dwLenNeeded += ::lstrlen(lpNetRes->lpRemoteName);
	dwLenNeeded += ::lstrlen(lpNetRes->lpComment);
	dwLenNeeded += ::lstrlen(lpNetRes->lpProvider);

	if(dwLenNeeded > dwLen - 16) return FALSE;

	if(lpNetRes->dwScope == RESOURCE_CONNECTED ||
		lpNetRes->dwScope == RESOURCE_REMEMBERED)
	{
		if(lpNetRes->lpLocalName != NULL) ::lstrcpy(sz, lpNetRes->lpLocalName);
	}

	if(::lstrlen(lpNetRes->lpRemoteName) > 0)
	{
		::lstrcat(sz, _T(" RemoteName: "));
	    ::lstrcat(sz, lpNetRes->lpRemoteName);
		::lstrcat(sz, _T(""));
	}

	if(::lstrlen(lpNetRes->lpComment) > 0)
	{
	    ::lstrcat(sz, _T("<<Comment: "));
	    ::lstrcat(sz, lpNetRes->lpComment);
		::lstrcat(sz, _T(">>"));
	}

	if(::lstrlen(lpNetRes->lpProvider) > 0)
	{
	    ::lstrcat(sz, _T("<<Provider: "));
	    ::lstrcat(sz, lpNetRes->lpProvider);
		::lstrcat(sz, _T(">>"));
	}

	if(lpNetRes == NULL)
	{
		dwImageIndex = 0;
	}
	else if(lpNetRes->dwDisplayType == RESOURCEDISPLAYTYPE_DOMAIN)
	{
		dwImageIndex = 1;
	}
	else if(lpNetRes->dwDisplayType == RESOURCEDISPLAYTYPE_SERVER)
	{
		dwImageIndex = 2;
	}
	else if(lpNetRes->dwDisplayType == RESOURCEDISPLAYTYPE_SHARE)
	{
		if(lpNetRes->dwType == RESOURCETYPE_DISK)
		{
			dwImageIndex = 4;
		}
		else if(lpNetRes->dwType == RESOURCETYPE_PRINT)
		{
			dwImageIndex = 3;
		}
	}
	else
		dwImageIndex = 0;
    return TRUE;
}

BOOL WNetErr2Msg(DWORD dwErr, LPTSTR sz, DWORD dwLen)
{
	if(dwErr != ERROR_EXTENDED_ERROR)
	{
		LPVOID lpMsgBuf;
        FormatMessage( 
           FORMAT_MESSAGE_ALLOCATE_BUFFER | 
           FORMAT_MESSAGE_FROM_SYSTEM | 
           FORMAT_MESSAGE_IGNORE_INSERTS,
           NULL,
           dwErr,
           MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), // Default language
           (LPTSTR) &lpMsgBuf,
           0,
           NULL 
       );
	   
	   if(::lstrlen((LPCTSTR)lpMsgBuf) < (int)dwLen)
	   {
		   //deleting ending \r\n
		   ::lstrcpyn(sz, (LPCTSTR)lpMsgBuf, ::lstrlen((LPCTSTR)lpMsgBuf) - 1);
		   
	   	   // Free the buffer.
           LocalFree( lpMsgBuf );
	       return TRUE;
	   }
	   LocalFree( lpMsgBuf );
	}
	else
	{
		// The following code performs error-handling when the
		// ERROR_EXTENDED_ERROR return value indicates that WNetGetLastError
		// can retrieve additional information.
		DWORD dwLastError;
		TCHAR szDescription[MAX_PATH * 4];
		TCHAR szProvider[MAX_PATH * 4];
		TCHAR pszError[MAX_PATH * 4];
		DWORD dwResult =	WNetGetLastError(
								&dwLastError,
								szDescription,  // buffer for error description
								sizeof(szDescription), 
								szProvider,     // buffer for provider name
								sizeof(szProvider)
							);

		if(dwResult != NO_ERROR) {
			wsprintf(pszError,
				TEXT("WNetGetLastError failed; error %ld"), dwResult);
		} else {
	        szDescription[_tcsclen(szDescription)] = TEXT('\0');  //-1 remove cr/nl characters
			wsprintf(pszError,
				TEXT("%s failed with code %ld (\"%s\")"),
				szProvider, dwLastError, szDescription);
		}	

		if(::lstrlen(pszError) < (int)dwLen)
		{
		   ::lstrcpy(sz, pszError);
		   return TRUE;
		}
	}
	return FALSE;
}

void AddNodeToDomainTree(HWND hTree, DWORD dwLevel, DWORD dwResult, DWORD dwTick, LPNETRESOURCE lpNetRes)
{
	LPNETRESOURCE pEntry = (LPNETRESOURCE)lpNetRes;
    /*
	typedef struct _NETRESOURCE { 
    DWORD  dwScope; 
    DWORD  dwType; 
    DWORD  dwDisplayType; 
    DWORD  dwUsage; 
    LPTSTR lpLocalName; 
    LPTSTR lpRemoteName; 
    LPTSTR lpComment; 
    LPTSTR lpProvider; 
    } NETRESOURCE;
	*/
   
	int level = (int)dwLevel;
	
	static int curLevel = 0;
	static HTREEITEM curItem = NULL;
	if(TreeView_GetRoot(hTree) == NULL)
		curItem = NULL;

	TCHAR szItem[4 * MAX_PATH];
	::lstrcpy(szItem, _T(""));
	TCHAR szErr[5 * MAX_PATH];
	DWORD dwImageIndex = 0;
	if(!ParseNetRes(pEntry, szItem, 4 * MAX_PATH, dwImageIndex)) return;

	if(dwLevel == 0)
		dwImageIndex  = 0;

	TCHAR szTime[MAX_PATH];
	_stprintf(szTime, _T(" Time: %dms"), dwTick);
	if(dwResult != 0)
	{
        if(!WNetErr2Msg(dwResult, szErr, 5 * MAX_PATH)) return;
		dwImageIndex = 5;
        ::lstrcpy(szItem, szErr);
		::lstrcat(szItem, szTime);
	}
	else
	{
        ::lstrcat(szItem, szTime);
	}

	if(curItem != NULL)
	{
		HTREEITEM hItem;
		TV_INSERTSTRUCT tvinsert;
		
        tvinsert.item.mask = 
           TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
        //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
        tvinsert.item.hItem = NULL; 
        tvinsert.item.state = 0;
        tvinsert.item.stateMask = 0;
        tvinsert.item.cchTextMax = ::lstrlen(szItem)+1; //max text length
        tvinsert.item.cChildren = 0;
        tvinsert.item.lParam = 0;//(LPARAM)dwTimeUsed;
               
        tvinsert.item.pszText = szItem;
        tvinsert.item.iImage = dwImageIndex;
        tvinsert.item.iSelectedImage = dwImageIndex;
        tvinsert.item.lParam = (LPARAM)0;
        
		if(curLevel == level) //add next
		{
            hItem = TreeView_GetParent(hTree, curItem);
			tvinsert.hParent = hItem; //may be NULL
            tvinsert.hInsertAfter = TVI_LAST; //last item
		    curItem = TreeView_InsertItem(hTree, &tvinsert);  
			if(TreeView_GetParent(hTree, curItem) != NULL)
				TreeView_Expand(hTree, TreeView_GetParent(hTree, curItem),
				  TVE_EXPAND);
		}
		else if(curLevel < level) //add child
		{
			//ASSERT(curLevel + 1 == level);
			tvinsert.hParent = curItem;
            tvinsert.hInsertAfter = TVI_LAST; //last child item
			curItem = TreeView_InsertItem(hTree, &tvinsert);      
            curLevel = level;
			TreeView_Expand(hTree, TreeView_GetParent(hTree, curItem),
				TVE_EXPAND);

		}
		else if(curLevel > level) //return to add 
		{
			for(int i = 0; i < curLevel - level && curItem != NULL; i++)
			{
				curItem = TreeView_GetParent(hTree, curItem);
			}
			if(curItem != NULL)
				hItem = TreeView_GetParent(hTree, curItem);
			else
				hItem = NULL;

			tvinsert.hParent = hItem;
            tvinsert.hInsertAfter = TVI_LAST; //last child item
			curItem = TreeView_InsertItem(hTree, &tvinsert);      
            curLevel = level;
			if(hItem)
				TreeView_Expand(hTree, hItem,TVE_EXPAND);
		}
		//m_treeNetBoy.Invalidate();
	}
	else //no root at all
	{
		//ASSERT(level == 0);
        TV_INSERTSTRUCT tvinsert;
        tvinsert.hParent = NULL;
        tvinsert.hInsertAfter = TVI_LAST; //root item
        tvinsert.item.mask = 
           TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_TEXT  | TVIF_PARAM ;
        //TVIF_TEXT | TVIF_PARAM | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_CHILDREN ;
        tvinsert.item.hItem = NULL; 
        tvinsert.item.state = 0;
        tvinsert.item.stateMask = 0;
        tvinsert.item.cchTextMax = ::lstrlen(szItem)+1;; //max text length
        tvinsert.item.cChildren = 0;
        tvinsert.item.lParam = (LPARAM)0;
               
        tvinsert.item.pszText = szItem;
        tvinsert.item.iImage = dwImageIndex;
        tvinsert.item.iSelectedImage = dwImageIndex;
        tvinsert.item.lParam = (LPARAM)0;
        
        curItem = TreeView_InsertItem(hTree, &tvinsert);        
	}
	
	return;
}


//////////////////////////////////////////////////////////////////////////////
/////////////// User Dislog //////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
void OnBrowseUser(HWND hDlg, HWND hEdtDomain, LPCTSTR szDomain)
{
	int nRet = DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_USER),
		hDlg, DialogUserProc, NULL);
	nRet;
}

HWND hListUser = NULL;
//HWND hBtnRefresh = NULL;
HWND hEdtMachine = NULL;
INT_PTR CALLBACK DialogUserProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HIMAGELIST hImage;   // image list  
    LVCOLUMN lvc; 
    int iCol; 
	RECT rectClient;
    RECT rectEdt;
	LPCTSTR szName[8] = 
	{
		_T("Name  "), _T("Pwd Age"), _T("Priv  "), _T("Comment"),
		_T("Full Name"), _T("User Comment"), _T("UserID "), _T("GrpID ")
	}; 
	TCHAR szMachine[MAX_PATH];

	switch (message) 
	{
		case WM_INITDIALOG:
			hListUser = ::GetDlgItem(hWnd, IDC_LIST);
			hBtnRefresh = ::GetDlgItem(hWnd, IDC_BTN_REFRESH);
            hEdtMachine = ::GetDlgItem(hWnd, IDC_EDT_MACHINE);

			ListView_SetExtendedListViewStyle(hListUser, LVS_EX_FULLROWSELECT /*| LVS_EX_CHECKBOXES*/ );
	        // Create the full-sized icon image lists. 
            hImage = ImageList_LoadBitmap(hInst, MAKEINTRESOURCE(IDB_ACCOUNT),
		         16, 1, RGB(255, 0, 255));
            ListView_SetImageList(hListUser, hImage, LVSIL_SMALL); 
 
            // Initialize the LVCOLUMN structure.
            // The mask specifies that the format, width, text, and subitem
            // members of the structure are valid. 
            lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM; 
     
            // Add the 2 columns -- Exe name and Full Path (including exe) 
			
            for (iCol = 0; iCol < 8; iCol++) 
			{
				lvc.iSubItem = iCol;
                lvc.pszText = (LPTSTR)szName[iCol];
//				switch(iCol)
//				{
//				case 0: lvc.pszText = _T("Name  ");	break;
//				case 1: lvc.pszText = _T("Pwd Age");	break;
//				case 2: lvc.pszText = _T("Priv  ");	break;
//                case 3: lvc.pszText = _T("Comment");	break;
//				case 4: lvc.pszText = _T("Full Name");	break;
//				case 5: lvc.pszText = _T("User Comment");	break;
//				case 6: lvc.pszText = _T("UserID ");	break;
//				case 7: lvc.pszText = _T("GrpID ");	break;
//				}
                lvc.cx =  70;    // width of column in pixels
        		lvc.fmt = LVCFMT_LEFT;  // left-right-aligned column
                ListView_InsertColumn(hListUser, iCol, &lvc); 
			}    
			if(::IsWindow(hListUser))
			{
				::GetClientRect(hWnd, &rectClient);
				::GetWindowRect(hEdtMachine, &rectEdt);

				::MoveWindow(hListUser, 0,0, rectClient.right - rectClient.left,
					rectClient.bottom - rectClient.top - rectEdt.bottom + rectEdt.top, TRUE);
				::MoveWindow(hEdtMachine, 0, rectClient.bottom - rectEdt.bottom + rectEdt.top,
					(int)(2.0 * (rectClient.right - rectClient.left)/3), rectEdt.bottom - rectEdt.top, TRUE);
				::MoveWindow(hBtnRefresh, (int)(2.0 * (rectClient.right - rectClient.left)/3), rectClient.bottom - rectEdt.bottom + rectEdt.top,
                    (int)(1.0 * (rectClient.right - rectClient.left)/3), rectEdt.bottom - rectEdt.top, TRUE);
			}
			PopulateTrusteeList(hListUser, NULL); //local machine
            return FALSE;          			 
		case WM_COMMAND:
			switch (LOWORD(wParam)) 
			{
	    		case IDOK:
		        	EndDialog(hWnd, IDOK);
                    return TRUE;
                case IDCANCEL:
                    EndDialog(hWnd, IDCANCEL);
                    return TRUE;
				case IDC_BTN_REFRESH:
					::SendMessage(hEdtMachine, WM_GETTEXT, MAX_PATH, (LPARAM)szMachine);
					if(::lstrlen(szMachine) == 0)
						PopulateTrusteeList(hListUser, NULL);
					else
                        PopulateTrusteeList(hListUser, szMachine);
                    return TRUE;

			}
			return FALSE;
		case WM_SIZE:
            if(::IsWindow(hListUser))
			{
				::GetClientRect(hWnd, &rectClient);
				::GetWindowRect(hEdtMachine, &rectEdt);

				::MoveWindow(hListUser, 0,0, rectClient.right - rectClient.left,
					rectClient.bottom - rectClient.top - rectEdt.bottom + rectEdt.top, TRUE);
				::MoveWindow(hEdtMachine, 0, rectClient.bottom - rectEdt.bottom + rectEdt.top,
					(int)(2.0 * (rectClient.right - rectClient.left)/3), rectEdt.bottom - rectEdt.top, TRUE);
				::MoveWindow(hBtnRefresh, (int)(2.0 * (rectClient.right - rectClient.left)/3), rectClient.bottom - rectEdt.bottom + rectEdt.top,
                    (int)(1.0 * (rectClient.right - rectClient.left)/3), rectEdt.bottom - rectEdt.top, TRUE);
				return 0; //app handled
			}
			return FALSE;	
		case WM_NOTIFY:
			if((int)wParam == IDC_LIST)
			{
				LPNMHDR pnmh = (LPNMHDR) lParam;
		        if(pnmh->code == NM_DBLCLK)
				{
					//update the exe edit box
			        int index = ListView_GetNextItem(hListUser, -1, LVNI_SELECTED);
			        if(index != -1)
					{
						TCHAR sz[MAX_PATH * 2];
				        ListView_GetItemText(hListUser, index, 0, sz, MAX_PATH * 2);
				        ::SendMessage(hEdtUser, WM_SETTEXT, 0, (LPARAM)(LPCTSTR)sz);
						EndDialog(hWnd, IDOK);
					}
				}
			}
			return 123;
	} 
    return FALSE ;
}

#include <Lm.h>
#include <Sddl.h>

void PopulateTrusteeList(HWND hList, LPCTSTR szComputerName)
{
	NET_API_STATUS netStatus;
	//LPCTSTR szComputerName = NULL;

    ListView_DeleteAllItems(hList);

    // Enumerate local groups of the system, and add to the trustee list
    ULONG lIndex2 = 0;
    ULONG lRetEntries, lTotalEntries;
    ULONG_PTR ulPtr = 0;
    LOCALGROUP_INFO_0* pinfoGroups;
   
    do
	{
		netStatus = NetLocalGroupEnum(szComputerName, 0, (PBYTE*) &pinfoGroups,
           1000, &lRetEntries, &lTotalEntries, &ulPtr);
        if ((netStatus != ERROR_MORE_DATA) && (netStatus != NERR_Success)) 
		{
            ReportErrEx(TEXT("NetLocalGroupEnum %d"), netStatus);
            break;
		}

        if (lRetEntries != 0) 
		{
			for (lIndex2 = 0; lIndex2 < lRetEntries; lIndex2++) 
			{
				AddTrusteeToList(hList, szComputerName, pinfoGroups[lIndex2].lgrpi0_name, TRUE);
			}
		}
      
        // Free the buffer containing the local groups
        NetApiBufferFree(pinfoGroups);

   } while (netStatus == ERROR_MORE_DATA);

   // Enumerate users of the system and add to the trustee list
   ULONG lIndex = 0;
   NET_DISPLAY_USER* pnetUsers;
   do 
   {
      // Because of the potentially many users on a system, this function
      // is more appropriate than NetUserEnum for UI programs.
      // We will return no more than 20000 users with this call in 1 k chunks
      netStatus = NetQueryDisplayInformation(szComputerName, 1, lIndex, 20000,
         1024, &lRetEntries, (PVOID*) &pnetUsers);
      if ((netStatus != ERROR_MORE_DATA) && (netStatus != NERR_Success)) 
	  {
		  ReportErrEx(TEXT("NetQueryDisplayInformation %d"), netStatus);
          break;
      }

      for (lIndex2 = 0; lIndex2 < lRetEntries; lIndex2++) 
	  {
         AddTrusteeToList(hList, szComputerName, pnetUsers[lIndex2].usri1_name, FALSE);
      }
      
      // Start enumeration where we left off
      lIndex = pnetUsers[lIndex2 - 1].usri1_next_index;
      
      // Free the buffer
      NetApiBufferFree(pnetUsers);

   } while (netStatus == ERROR_MORE_DATA);
	return;
}

int AddTrusteeToList(HWND hList, LPCTSTR szComputerName, LPCTSTR szTrustName, BOOL fGroup)
{
	LVITEM item = { 0 };
    item.mask = LVIF_TEXT | LVIF_IMAGE;
    item.iItem = 0;
    item.iImage = fGroup ? 0 : 1;
    item.pszText =  (LPTSTR)szTrustName;
    int nIndex = ListView_InsertItem(hList, &item);
    ListView_SetItemText(hList, nIndex, 1, 
      fGroup ? TEXT("Group") : TEXT("User"));
    QueryTrusteeDetail(hList, szComputerName, szTrustName, fGroup, nIndex);
	
	return(nIndex);
}

void QueryTrusteeDetail(HWND hList, LPCTSTR szComputerName, LPCTSTR szTrustName, BOOL fGroup, int nIndex)
{
	TCHAR strPwdAge[64], strPriv[16], *strComment;
	TCHAR *strFullName, *strUserComment;
	TCHAR *strUserID, strGrpID[64];

	if(!fGroup)
	{
		USER_INFO_3* lpUserInfo = {0};
		NET_API_STATUS netStatus = NetUserGetInfo(szComputerName,  
                 szTrustName, 3, (PBYTE*)&lpUserInfo);
		if (netStatus == NERR_Success) 
		{
			DWORD dw = lpUserInfo->usri3_password_age;
			_stprintf(strPwdAge, _T("%d:%d:%d"), dw/3600, (dw%3600)/60, dw%60);
			switch(lpUserInfo->usri3_priv)
			{
			case USER_PRIV_GUEST:
				_stprintf(strPriv, _T("Guest"));
				break;
			case USER_PRIV_USER:
				_stprintf(strPriv, _T("User"));
				break;
			case USER_PRIV_ADMIN:
				_stprintf(strPriv, _T("Admin"));
				break;
			default:
				_stprintf(strPriv, _T("???"));
				break;
			}
			strComment = lpUserInfo->usri3_comment;
			//if(lpUserInfo->usri3_comment == NULL || ::lstrlen(lpUserInfo->usri3_comment) == 0)
			//	strComment = NULL;
			
            strFullName = lpUserInfo->usri3_full_name;
            strUserComment = lpUserInfo->usri3_usr_comment;
			
			LPTSTR UsrSIDString;
			if(ConvertSidToStringSid((PSID)(lpUserInfo->usri3_user_id), &UsrSIDString))
			{
				strUserID = UsrSIDString;
			}
			else
			{
				strUserID = NULL;

			}
			::LocalFree(UsrSIDString);

			if(::lstrlen(strUserID))
			{
				BYTE sid[50];
				DWORD dwSizeSid = 50;
				TCHAR szDomain[MAX_PATH];
				DWORD dwSizeDomain = MAX_PATH;
				SID_NAME_USE use;
				BOOL fRet = LookupAccountName(szComputerName, szTrustName,
                   &sid, &dwSizeSid, szDomain, &dwSizeDomain, &use);
				if(fRet)
				{
					if(ConvertSidToStringSid((PSID)sid, &UsrSIDString))
					{
						strUserID = UsrSIDString;
					}
			        ::LocalFree(UsrSIDString);
				}
			}
			_stprintf(strGrpID, _T("%d"), lpUserInfo->usri3_primary_group_id);

			ListView_SetItemText(hList, nIndex, 1, (LPTSTR)strPwdAge); 
        	ListView_SetItemText(hList, nIndex, 2, (LPTSTR)strPriv); 
			if(strComment)
				ListView_SetItemText(hList, nIndex, 3, (LPTSTR)strComment); 
			if(strFullName)
				ListView_SetItemText(hList, nIndex, 4, (LPTSTR)strFullName); 
	        if(strUserComment)
				ListView_SetItemText(hList, nIndex, 5, (LPTSTR)strUserComment); 
	        if(strUserID)
				ListView_SetItemText(hList, nIndex, 6, (LPTSTR)strUserID); 
			if(strGrpID)
				ListView_SetItemText(hList, nIndex, 7, (LPTSTR)strGrpID); 

		}
		else 
			ReportErr(_T("GetUserInfo Failure"));
		NetApiBufferFree(lpUserInfo);
	}
	else //Group
	{
		GROUP_INFO_2* lpGrpInfo = {0};
		NET_API_STATUS netStatus = ::NetGroupGetInfo(szComputerName,  
                 szTrustName, 2, (PBYTE*)&lpGrpInfo);
		if (netStatus == NERR_Success) 
		{
			strComment = lpGrpInfo->grpi2_comment;
			strFullName = lpGrpInfo->grpi2_name;
			_stprintf(strGrpID, _T("%d"), lpGrpInfo->grpi2_group_id);

			ListView_SetItemText(hList, nIndex, 1, (LPTSTR)strPwdAge); 
        	ListView_SetItemText(hList, nIndex, 2, (LPTSTR)strPriv); 
			if(strComment)
				ListView_SetItemText(hList, nIndex, 3, (LPTSTR)strComment); 
			if(strFullName)
				ListView_SetItemText(hList, nIndex, 4, (LPTSTR)strFullName); 
	        if(strGrpID)
				ListView_SetItemText(hList, nIndex, 7, (LPTSTR)strGrpID); 

		}
		else if(netStatus == ERROR_ACCESS_DENIED) 
		{
			ReportErr(_T("GetGroupInfo Failure -- ERROR_ACCESS_DENIED"));
		}
		else if(netStatus == NERR_InvalidComputer) 
		{
			ReportErr(_T("GetGroupInfo Failure -- NERR_InvalidComputer"));
		}
	    else if(netStatus == NERR_GroupNotFound ) 
		{
			//ReportErr(_T("GetGroupInfo Failure -- NERR_GroupNotFound"));
		}
		else
			ReportErr(_T("GetGroupInfo Failure"));
		NetApiBufferFree(lpGrpInfo);
		
		::lstrcpy(strPwdAge, _T(""));
		::lstrcpy(strPriv, _T(""));
		::lstrcpy(strGrpID, _T(""));
	}
}
///////////////////////////////////////////////////////////////////////////////
///////////         WhoAmI Dialog /////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void WhoAmI(HWND hWnd)
{
	DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_WHO_AM_I), NULL, DialogWhoAmIProc, 0);
}


HWND hWhoDomainEdit, hWhoUserEdit, hWhoPasswordEdit;
TCHAR szWhoDomain[MAX_PATH], szWhoUser[MAX_PATH], szWhoPassword[MAX_PATH];
INT_PTR CALLBACK DialogWhoAmIProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) 
	{
		case WM_INITDIALOG:
			hWhoDomainEdit = ::GetDlgItem(hWnd, IDC_EDT_DOMAIN);
			hWhoUserEdit = ::GetDlgItem(hWnd, IDC_EDT_USER);
			hWhoPasswordEdit = ::GetDlgItem(hWnd, IDC_EDT_PWD);
			::ZeroMemory(szWhoDomain, sizeof(TCHAR) * MAX_PATH);
			::ZeroMemory(szWhoUser, sizeof(TCHAR) * MAX_PATH);
			::ZeroMemory(szWhoPassword, sizeof(TCHAR) * MAX_PATH);
            if(FetchWhoAmI(szWhoDomain, szWhoUser, szWhoPassword, MAX_PATH))
			{
				//When User Running in WinNT4/2000 We can get the password
				::SetDlgItemText(hWnd, IDC_EDT_DOMAIN, (LPCTSTR)szWhoDomain);
			    ::SetDlgItemText(hWnd, IDC_EDT_USER, (LPCTSTR)szWhoUser);
			    ::SetDlgItemText(hWnd, IDC_EDT_PWD, (LPCTSTR)szWhoPassword);
			}
			else
			{
				//When User Running in WinXP/2003, get Domain and User Name Only
				::SetDlgItemText(hWnd, IDC_EDT_DOMAIN, (LPCTSTR)szWhoDomain);
			    ::SetDlgItemText(hWnd, IDC_EDT_USER, (LPCTSTR)szWhoUser);
			    ::SetDlgItemText(hWnd, IDC_EDT_PWD, (LPCTSTR)szWhoPassword);												
			}
            return FALSE;          			 
		case WM_COMMAND:
			switch (LOWORD(wParam)) 
			{
	    		case IDOK:
		        	EndDialog(hWnd, IDOK);
                    return TRUE;
                case IDCANCEL:
                    EndDialog(hWnd, IDCANCEL);
                    return TRUE;
			}
			return FALSE;		
	} 
    return FALSE ;
}