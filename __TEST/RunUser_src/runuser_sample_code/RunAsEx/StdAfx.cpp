// stdafx.cpp : source file that includes just the standard includes
//	RunAsEx.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

#include "stdafx.h"

// TODO: reference any additional headers you need in STDAFX.H
// and not in this file
void PopMsg(LPCTSTR pszFormat, ...) 
{
   va_list argList;
   va_start(argList, pszFormat);

   TCHAR sz[1024];
//#ifdef _UNICODE
//   vswprintf(sz, pszFormat, argList);
//#else
//   vsprintf(sz, pszFormat, argList);
//#endif
   wvsprintf(sz, pszFormat, argList);
   va_end(argList);
   ::MessageBox(NULL, sz, _T("Pop Msg"), MB_OK);
}

void ReportErr(LPCTSTR str)
{
	    LPVOID lpMsgBuf;
        FormatMessage( 
           FORMAT_MESSAGE_ALLOCATE_BUFFER | 
           FORMAT_MESSAGE_FROM_SYSTEM | 
           FORMAT_MESSAGE_IGNORE_INSERTS,
           NULL,
           ::GetLastError(),
           MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), // Default language
           (LPTSTR) &lpMsgBuf,
           0,
           NULL 
       );
		::MessageBox( NULL, (LPCTSTR)lpMsgBuf, 
		   str, MB_OK | MB_ICONINFORMATION );
       // Free the buffer.
       LocalFree( lpMsgBuf );
}

void ReportErrEx(LPCTSTR pszFormat, ...) 
{
   va_list argList;
   va_start(argList, pszFormat);

   TCHAR sz[1024];
   wvsprintf(sz, pszFormat, argList);
   va_end(argList);
   
   LPVOID lpMsgBuf;
   FormatMessage( 
         FORMAT_MESSAGE_ALLOCATE_BUFFER | 
         FORMAT_MESSAGE_FROM_SYSTEM | 
         FORMAT_MESSAGE_IGNORE_INSERTS,
         NULL,
         ::GetLastError(),
         MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), // Default language
         (LPTSTR) &lpMsgBuf,
         0,
         NULL 
    );
	::MessageBox( NULL, (LPCTSTR)lpMsgBuf, 
		   sz, MB_OK | MB_ICONINFORMATION );
    // Free the buffer.
    LocalFree( lpMsgBuf );
}

void ReportErrExToFile(LPCTSTR szEvtName, LPCTSTR pszFormat, ...)
{
	va_list argList;
    va_start(argList, pszFormat);

    TCHAR sz[1024]; //enough ??? 
    wvsprintf(sz, pszFormat, argList);
    va_end(argList);
   
    LPTSTR lpMsgBuf;
    FormatMessage( 
         FORMAT_MESSAGE_ALLOCATE_BUFFER | 
         FORMAT_MESSAGE_FROM_SYSTEM | 
         FORMAT_MESSAGE_IGNORE_INSERTS,
         NULL,
         ::GetLastError(),
         MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), // Default language
         (LPTSTR) &lpMsgBuf,
         0,
         NULL 
     );
	     
	LPCTSTR lpMsgBuf2 = (LPCTSTR)lpMsgBuf;
	WriteLineLog(szEvtName, sz);
    WriteLineLog(szEvtName, lpMsgBuf2);
	
	// Free the buffer.
    LocalFree( lpMsgBuf );
}

//pszFormat << 1024 plz!!!
void ReportErrExToEvtLog(LPCTSTR szEvtName, LPCTSTR pszFormat, ...)
{
	va_list argList;
    va_start(argList, pszFormat);

    TCHAR sz[1024]; //enough ??? 
    wvsprintf(sz, pszFormat, argList);
    va_end(argList);
   
    LPTSTR lpMsgBuf;
    FormatMessage( 
         FORMAT_MESSAGE_ALLOCATE_BUFFER | 
         FORMAT_MESSAGE_FROM_SYSTEM | 
         FORMAT_MESSAGE_IGNORE_INSERTS,
         NULL,
         ::GetLastError(),
         MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), // Default language
         (LPTSTR) &lpMsgBuf,
         0,
         NULL 
     );
	     
	HANDLE h = RegisterEventSource( 0, szEvtName);
	if(h)
	{
		LPCTSTR sz2 = (LPCTSTR)sz;
		LPCTSTR lpMsgBuf2 = (LPCTSTR)lpMsgBuf;
		BOOL bRet = ReportEvent( h, EVENTLOG_ERROR_TYPE, 0, 0, 0, 1, 0, &(sz2), 0 );
		bRet = ReportEvent( h, EVENTLOG_ERROR_TYPE, 0, 0, 0, 1, 0, &lpMsgBuf2, 0 );
		//::ReportErr(_T(""));
	    DeregisterEventSource( h );
	}
	
	// Free the buffer.
    LocalFree( lpMsgBuf );
}

void SvrPopMsg(LPCTSTR pszFormat, ...) 
{
   va_list argList;
   va_start(argList, pszFormat);

   TCHAR sz[1024];
//#ifdef _UNICODE
//   vswprintf(sz, pszFormat, argList);
//#else
//   vsprintf(sz, pszFormat, argList);
//#endif
   wvsprintf(sz, pszFormat, argList);
   va_end(argList);
   ::MessageBox(NULL, sz, _T("Pop Msg"), MB_OK | MB_SERVICE_NOTIFICATION);
}

void SvrReportErr(LPCTSTR str)
{
	    LPVOID lpMsgBuf;
        FormatMessage( 
           FORMAT_MESSAGE_ALLOCATE_BUFFER | 
           FORMAT_MESSAGE_FROM_SYSTEM | 
           FORMAT_MESSAGE_IGNORE_INSERTS,
           NULL,
           ::GetLastError(),
           MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), // Default language
           (LPTSTR) &lpMsgBuf,
           0,
           NULL 
       );
		::MessageBox( NULL, (LPCTSTR)lpMsgBuf, 
		   str, MB_OK | MB_ICONINFORMATION | MB_SERVICE_NOTIFICATION);
       // Free the buffer.
       LocalFree( lpMsgBuf );
}

BOOL WriteLineLog(LPCTSTR szFilename, LPCTSTR szMessage)
{
	TCHAR chFilename[_MAX_PATH];
	wsprintf(chFilename, _T("c:\\%s.log"), szFilename);
	HANDLE hFile = ::CreateFile(
                chFilename, GENERIC_WRITE,
				FILE_SHARE_READ, //not shared
				NULL, // SD
                OPEN_ALWAYS,  // how to create
                FILE_ATTRIBUTE_NORMAL, // file attributes
                NULL // handle to template file
               );
	if(INVALID_HANDLE_VALUE == hFile)
	{
#ifdef _DEBUG
		//::ReportErr(_T("Log File Open Failed"));
#endif
		return FALSE;
	}
	
	LONG disp1, disp2;
	disp1 = disp2 = 0;
    SetFilePointer(hFile, disp1, &disp2, FILE_END);
	TCHAR chLine[1024];
    wsprintf(chLine, _T("%s\r\n"), szMessage); 

	int len = lstrlen(chLine)*sizeof(TCHAR);//lstrlen(szMessage) + 2;
	DWORD dwWritten;
	BOOL bRet = ::WriteFile(hFile, // handle to file
                   chLine, // data buffer
                   len, // number of bytes to write
				   &dwWritten,
                   NULL   // offset
                   );
	bRet;
	::CloseHandle(hFile);
	return TRUE;
}