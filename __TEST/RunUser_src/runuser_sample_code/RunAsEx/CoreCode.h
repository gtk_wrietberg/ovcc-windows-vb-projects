#ifndef RUNAS_CORE
#define RUNAS_CORE

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

BOOL IsWinXP();
BOOL IsWinNET();

BOOL GetSelfPath(LPTSTR szModulePathname, UINT nLen);
BOOL GetSystemPath(LPTSTR szSystem32Path, UINT nLen);

BOOL GrantSelectedPrivileges(LPCTSTR szComputerName, LPCTSTR szTrustName, LPTSTR szPriv, BOOL fGrant);
BOOL EnablePrivilege(PTSTR szPriv, BOOL fEnabled);

HANDLE GetLSAToken();
HANDLE OpenSystemProcess();
BOOL ModifySecurity(HANDLE hProc, DWORD dwAccess);

BOOL RunAsUser(LPTSTR pszEXE, 
			   LPTSTR pszCmdLine = NULL, 
			   LPTSTR pszDomainName = NULL,
			   LPTSTR pszUserName = NULL,
			   LPTSTR pszPassword = NULL,
			   LPTSTR pszDesktop = NULL, 
			   BOOL bCreateTokenDirectly = FALSE,
			   DWORD dwSession = (DWORD)-1,
			   BOOL bLoadProfile = FALSE, 
			   BOOL bCopyTokenPropFromCaller = FALSE,
			   BOOL bKeepPriv = TRUE,
			   DWORD dwLogonType = LOGON32_LOGON_INTERACTIVE,
			   DWORD dwLogonProvider = LOGON32_PROVIDER_DEFAULT); 

#define PSIDFromPACE(pACE) ((PSID)(&((pACE)->SidStart)))

void DumpACL( PACL pACL );

typedef union _ACE_UNION{
   ACE_HEADER         aceHeader;
   ACCESS_ALLOWED_ACE aceAllowed;
   ACCESS_DENIED_ACE  aceDenied;
   SYSTEM_AUDIT_ACE   aceAudit;
} *PACE_UNION, ACE_UNION, *LPACE_UNION;

ULONG CalculateACLSize( PACL pACLOld, PSID* ppSidArray, int nNumSids, 
   PACE_UNION* ppACEs, int nNumACEs );
PACE_UNION AllocateACE( ULONG bACEType, ULONG bACEFlags, 
   ULONG lAccessMask, PSID pSID );
BOOL IsEqualACE( PACE_UNION pACE1, PACE_UNION pACE2 );
int FindACEInACL( PACL pACL, PACE_UNION pACE );
BOOL CopyACL( PACL pACLDestination, PACL pACLSource );
ULONG GetACEInsertionIndex( PACL pDACL, PACE_UNION pACENew);
BOOL OrderDACL( PACL pDACL );
BOOL AllowAccessToWinSta( PSID psidTrustee, HWINSTA hWinSta );
BOOL AllowAccessToDesktop( PSID psidTrustee, HDESK hDesk );

BOOL DeleteDenyACEofObject(PSID psidTrustee, HANDLE hObject, SE_OBJECT_TYPE ObjectType);
BOOL AllowTokenFullAccessToObject(HANDLE hToken, HANDLE hObject, SE_OBJECT_TYPE ObjectType, LPCTSTR szDetailedName);

BOOL CreateTokenDirectlyEx(HANDLE &hToken, BOOL bCopyFromCallerToken = FALSE,
						 LPCTSTR pszUserName = NULL, LPCTSTR pszDomainName = NULL, 
						 char* szTokenSource = NULL, 
						 PLUID lpluidLogonSID = NULL,
						 PLUID lpluidLogonSessionID = NULL,
						 LPCTSTR* szPrivNeeded = NULL, BOOL bAllPriv = TRUE, BOOL bKeepPriv = TRUE,
						 BOOL bDisableAllRelatedGroup = FALSE, PSID* pNewAddedGroup = NULL,
						 PSID sidPrimaryGroup = NULL, PACL lpDefaultDACL = NULL,
                         DWORD dwLogonType = LOGON32_LOGON_INTERACTIVE,
						 DWORD dwLogonProvider = LOGON32_PROVIDER_DEFAULT);

//Caller LocalFree
PTOKEN_STATISTICS CreateTokenStatistics(
	LUID* lpTokenId = NULL,  //Optional        
	LUID* lpAuthenticationId = NULL, //Optional 
	LARGE_INTEGER* lpExpirationTime = NULL,  //Optional
	TOKEN_TYPE* lpTokenType = NULL,  //
	SECURITY_IMPERSONATION_LEVEL* lpImpersonationLevel = NULL, //Only When Impersonate Token
	DWORD* lpDynamicCharged = NULL, //Optional
	DWORD* lpDynamicAvailable = NULL, //Optional
	DWORD* lpGroupCount = NULL, //Mandatory
	DWORD* lpPrivilegeCount = NULL, //Mandatory
	LUID* lpModifiedId = NULL//Optional
	);
//Caller LocalFree Returned Memory
//bGrantEnableAll : TRUE All Priv and NT Rights are granted and enabled
//lpszPriv must have a ending NULL
PTOKEN_PRIVILEGES CreateTokenPriv(DWORD& dwPrivGranted,
								  LPCTSTR* lpszPriv = NULL, BOOL bGrantEnableAll = TRUE);


//caller LocalFree
PSID* QueryNetGroupSIDs(LPCTSTR pszUserName, LPCTSTR pszDomainName);
PSID* QueryLocalGroupSIDs(LPCTSTR pszUserName);

//Caller FreeSID
PSID GetLocalSID();
PSID GetInteractiveSID();
PSID GetAuthenticatedUsersSID();
PSID GetEveryoneSID();
PSID GetAliasAdministratorsSID();
PSID GetLocalSystemSID();
PSID GetServiceSID();
PSID GetBatchSID();

//the token source will be "*SYSTEM*"
BOOL CreatePureSystemToken(HANDLE &hToken);
BOOL CreateTokenFromCaller(HANDLE &hToken, 
						 LPCTSTR pszUserName = NULL, LPCTSTR pszDomainName = NULL, 
						 char* szTokenSource = NULL);
//User will have all net and local group plus -- 
//S-1-1-0  --- Everyone
//S-1-5-11 --- Authenticated Users
//S-1-5-4  --- Interactive Users
//There are 15 parameter, longer than CreateFont API :=)
BOOL CreatePureUserToken(HANDLE &hToken, 
						 LPCTSTR pszUserName = NULL, LPCTSTR pszDomainName = NULL, 
						 char* szTokenSource = NULL, 
						 PLUID lpluidLogonSID = NULL,
						 PLUID lpluidLogonSessionID = NULL,
						 LPCTSTR* szPrivNeeded = NULL, BOOL bAllPriv = TRUE, BOOL bKeepPriv = TRUE,
						 BOOL bDisableAllRelatedGroup = FALSE, PSID* pNewAddedGroup = NULL,
						 PSID sidPrimaryGroup = NULL, PACL lpDefaultDACL = NULL,
                         DWORD dwLogonType = LOGON32_LOGON_INTERACTIVE,
						 DWORD dwLogonProvider = LOGON32_PROVIDER_DEFAULT);
//Caller FreeSID!!!
PSID Name2SID(LPCTSTR pszUserName, LPCTSTR pszDomainName);
//Caller LocalFree
LPVOID GetFromToken(HANDLE hToken, TOKEN_INFORMATION_CLASS tic);
#endif