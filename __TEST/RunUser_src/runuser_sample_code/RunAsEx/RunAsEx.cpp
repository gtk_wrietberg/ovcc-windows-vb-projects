// RunAsEx.cpp : Defines the entry point for the application.
//

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

#include "stdafx.h"
#include "resource.h"
#include "MainDlg.h"
#include "CoreCode.h"
#include "RunAsEx.h"
#include "RunAsHelper.h"

#include <stdlib.h>

#include "CmnHdr.h"                
#include "EnsureCleanup.h" 

#define SERVICESTATUS_IMPL
#include "ServiceStatus.h"

//#define SCMCTRL_IMPL
#include "SCMCtrl.h"

//#define SERVICECTRL_IMPL
#include "ServiceCtrl.h"

TCHAR g_szServiceName[] = TEXT("RunAsEx Temporary Service");
CServiceStatus g_ss;

HANDLE g_hReadEvent;
HANDLE g_hWriteEvent;
DWORD  g_dwControl;

#define DEFAULT_CMD_SERVICE TEXT("zservice")
#define DEFAULT_CMD_INSTALL TEXT("zinstall")
#define DEFAULT_CMD_REMOVE TEXT("zremove")
#define DEFAULT_CMD_DEBUG TEXT("zdebug")

// Global Variables:
HINSTANCE hInst;								// current instance

BOOL g_bService; 
TCHAR g_szDomain[MAX_PATH];
TCHAR g_szUser[MAX_PATH];
TCHAR g_szPassword[MAX_PATH];
DWORD g_dwLogOnType;
//#define LOGON32_LOGON_INTERACTIVE       2
//#define LOGON32_LOGON_NETWORK           3
//#define LOGON32_LOGON_BATCH             4
//#define LOGON32_LOGON_SERVICE           5
//#define LOGON32_LOGON_UNLOCK            7
//#if(_WIN32_WINNT >= 0x0500)
//#define LOGON32_LOGON_NETWORK_CLEARTEXT 8
//#define LOGON32_LOGON_NEW_CREDENTIALS   9
//#endif // (_WIN32_WINNT >= 0x0500)
DWORD g_dwLogOnProvider;
//#define LOGON32_PROVIDER_DEFAULT    0
//#define LOGON32_PROVIDER_WINNT35    1
//#if(_WIN32_WINNT >= 0x0400)
//#define LOGON32_PROVIDER_WINNT40    2
//#endif /* _WIN32_WINNT >= 0x0400 */
//#if(_WIN32_WINNT >= 0x0500)
//#define LOGON32_PROVIDER_WINNT50    3
//#endif // (_WIN32_WINNT >= 0x0500)
TCHAR g_szExeName[4 * MAX_PATH];
TCHAR g_szCmdLine[4 * MAX_PATH];
TCHAR g_szDesktop[MAX_PATH]; //Including win station, seperated by "\\"

BOOL  g_bLoadProfile;  //Only Have sense logging as a physical user not SYSTEM 
BOOL  g_bDirectLaunch; //When True : LogOnUser, ZwCreateToken --> CreateProcessAsUser
                       //     FALSE : CreateService, in Service --> LogOn, Zw --> CreateProcessAsUser
BOOL  g_LogOnUser_Zw;  //When True : LogOnUser
                       //     FALSE: ZwCreateToken //no password needed
BOOL  g_bReStart;      //For Switch WinStat require no Window left on the old WinStat
                       //I close the dialog temporarily and make switch, after all done
                       //switch back to old WinStat and restore the dialog
RECT  g_rectDialog;    
HWND  g_hwndAfter;     //Z-oeder window just below RunAsEx

BOOL  g_bCopyTokenPropFromCaller; //Only meaningful when ZwCreateToekn g_LogOnUser_Zw = FALSE
BOOL  g_bKeepPriv;
DWORD g_dwSession;

//0: No Log
//1: Log to a file
//2: Log to EventLog
DWORD g_dwLogOption;



//Home Made Comparison Func =)
BOOL CheckHeadingString(PCTSTR szArgv, LPCTSTR szKey)
{
	if(::lstrlen(szArgv) < ::lstrlen(szKey)) return FALSE;
	for(int i = 0; i < (int)(sizeof(TCHAR) * ::lstrlen(szKey)); i++)
	{
		LPBYTE b1 = (LPBYTE)szArgv;
		LPBYTE b2 = (LPBYTE)szKey;
		b1 += i; b2 += i;
		if(*b1 != *b2) return FALSE;
	}
	return TRUE;
}

BOOL ExtractArg(/*in*/ PCTSTR szArgv, /*in*/LPCTSTR szKey, 
				/*in,out*/LPTSTR szBuffer, /*in  szBuffer's length, in TCHAR!!! */int nLength)
{
	if(szArgv == NULL || szBuffer == NULL) return FALSE;
	if(::lstrlen(szArgv) - ::lstrlen(szKey) - 1 > nLength) return FALSE;

	//e.g. "Cmd: /k" "Cmd --> /k
	TCHAR* lpTemp = (TCHAR*)szArgv;
	lpTemp += ::lstrlen(szKey);
	::lstrcpyn(szBuffer, lpTemp, ::lstrlen(lpTemp)); //Note: here we copied ::lstrlen(lpTemp)-1 tchars
	return TRUE;
}

BOOL ExtractArg2(/*in*/ PCTSTR szArgv, /*in*/LPCTSTR szKey, 
				/*in,out*/LPTSTR szBuffer, /*in  szBuffer's length, in TCHAR!!! */int nLength)
{
	if(szArgv == NULL || szBuffer == NULL) return FALSE;
	if(::lstrlen(szArgv) - ::lstrlen(szKey) - 1 > nLength) return FALSE;

	if(::lstrlen(szArgv) == ::lstrlen(szKey))
	{
		::lstrcpy(szBuffer, _T(""));
		return TRUE;
	}
	//e.g. "Cmd: /k" "Cmd --> /k
	TCHAR* lpTemp = (TCHAR*)szArgv;
	lpTemp += ::lstrlen(szKey);
	::lstrcpyn(szBuffer, lpTemp, ::lstrlen(lpTemp) + 1); //Note: here we copied ::lstrlen(lpTemp)-1 tchars
	return TRUE;
}
//Command Line
//RunAsEx "Domain:DomainName" "User:UserName" "Password:Password" 
//        "Exe:c:\\winnt\\system32\\cmd.exe" "Cmd: /k" "Session:0"
//        "LogonType:2" "LogOnProvider:0" "Desktop:WinSta0\\Winlogon"
//        "LoadProfile:1" "LogonDirectly:1" "CopyTokenProp:0"
//===============================================================
//        /NoGUI   //when start from command line or batch file
//        /Svr     //NT Service
//        /Install //Install Service
//        /Remove  //Remove Service
//        These 4 Command Line are Exlusive!!!
enumStartType IsService(LPSTR lpCmdLine)
{
    TCHAR szTemp[MAX_PATH];
	int nArgc = __argc;
	if(nArgc < 2) return x_gui; //lauch GUI by default
    enumStartType retType = x_error;
#ifdef UNICODE
    PCTSTR *ppArgv = (PCTSTR*) CommandLineToArgvW(GetCommandLine(), &nArgc);
#else
    PCTSTR *ppArgv = (PCTSTR*) __argv;
#endif
    	
    __try
	{
		if (nArgc < 4) 
		{
			return x_error;
		} 
        else
		{
			for (int i = 1; i < nArgc; i++) 
			{
				if(CheckHeadingString(ppArgv[i], _T("\"Domain:")) ||
					CheckHeadingString(ppArgv[i], _T("\"domain:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"Domain"), g_szDomain, MAX_PATH))
						__leave;
				}
				if(CheckHeadingString(ppArgv[i], _T("Domain:")) ||
					CheckHeadingString(ppArgv[i], _T("domain:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("Domain:"), g_szDomain, MAX_PATH))
						__leave;
				}	

				if(CheckHeadingString(ppArgv[i], _T("\"User:")) ||
					CheckHeadingString(ppArgv[i], _T("\"user:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"User:"), g_szUser, MAX_PATH))
						__leave;
				}
				if(CheckHeadingString(ppArgv[i], _T("User:")) ||
					CheckHeadingString(ppArgv[i], _T("user:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("User:"), g_szUser, MAX_PATH))
						__leave;
				}	

				if(CheckHeadingString(ppArgv[i], _T("\"Password:")) ||
					CheckHeadingString(ppArgv[i], _T("\"password:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"Password:"), ::g_szPassword, MAX_PATH))
						__leave;
				}
				if(CheckHeadingString(ppArgv[i], _T("Password:")) ||
					CheckHeadingString(ppArgv[i], _T("password:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("Password:"), g_szPassword, MAX_PATH))
						__leave;
				}	

				if(CheckHeadingString(ppArgv[i], _T("\"Exe:")) ||
					CheckHeadingString(ppArgv[i], _T("\"exe:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"exe:"), g_szExeName, MAX_PATH))
						__leave;
				}
				if(CheckHeadingString(ppArgv[i], _T("Exe:")) ||
					CheckHeadingString(ppArgv[i], _T("exe:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("exe:"), g_szExeName, MAX_PATH))
						__leave;
				}


				if(CheckHeadingString(ppArgv[i], _T("\"Cmd:")) ||
					CheckHeadingString(ppArgv[i], _T("\"cmd:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"Cmd:"), g_szCmdLine, MAX_PATH))
						__leave;
				}	
				if(CheckHeadingString(ppArgv[i], _T("Cmd:")) ||
					CheckHeadingString(ppArgv[i], _T("cmd:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("Cmd:"), g_szCmdLine, MAX_PATH))
						__leave;
				}

				if(CheckHeadingString(ppArgv[i], _T("\"LogonType:")) ||
					CheckHeadingString(ppArgv[i], _T("\"logontype:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"LogonType:"), szTemp, MAX_PATH))
						__leave;
					::g_dwLogOnType = (DWORD)::_ttol(szTemp);
				}	
				if(CheckHeadingString(ppArgv[i], _T("LogonType:")) ||
					CheckHeadingString(ppArgv[i], _T("logontype:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("LogonType:"), szTemp, MAX_PATH))
						__leave;
					::g_dwLogOnType = (DWORD)::_ttol(szTemp);
				}	

				if(CheckHeadingString(ppArgv[i], _T("\"LogOnProvider:")) ||
					CheckHeadingString(ppArgv[i], _T("\"logonprovider:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"LogOnProvider:"), szTemp, MAX_PATH))
						__leave;
					::g_dwLogOnProvider = (DWORD)::_ttol(szTemp);
				}	
				if(CheckHeadingString(ppArgv[i], _T("LogOnProvider:")) ||
					CheckHeadingString(ppArgv[i], _T("logonprovider:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("LogOnProvider:"), szTemp, MAX_PATH))
						__leave;
					::g_dwLogOnProvider = (DWORD)::_ttol(szTemp);
				}

				if(CheckHeadingString(ppArgv[i], _T("\"Desktop:")) ||
					CheckHeadingString(ppArgv[i], _T("\"desktop:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"Desktop:"), g_szDesktop, MAX_PATH))
						__leave;
				}
				if(CheckHeadingString(ppArgv[i], _T("Desktop:")) ||
					CheckHeadingString(ppArgv[i], _T("desktop:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("Desktop:"), g_szDesktop, MAX_PATH))
						__leave;
				}

				if(CheckHeadingString(ppArgv[i], _T("\"LoadProfile:")) ||
					CheckHeadingString(ppArgv[i], _T("\"LoadProfile:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"LoadProfile:"), szTemp, MAX_PATH))
						__leave;
					::g_bLoadProfile = (BOOL)::_ttol(szTemp);
				}
				if(CheckHeadingString(ppArgv[i], _T("LoadProfile:")) ||
					CheckHeadingString(ppArgv[i], _T("LoadProfile:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("LoadProfile:"), szTemp, MAX_PATH))
						__leave;
					::g_bLoadProfile = (BOOL)::_ttol(szTemp);
				}

				if(CheckHeadingString(ppArgv[i], _T("\"LogonDirectly:")) ||
					CheckHeadingString(ppArgv[i], _T("\"logondirectly:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"LogonDirectly:"), szTemp, MAX_PATH))
						__leave;
					::g_LogOnUser_Zw = (BOOL)::_ttol(szTemp);
					g_LogOnUser_Zw = !g_LogOnUser_Zw;
				}
				if(CheckHeadingString(ppArgv[i], _T("LogonDirectly:")) ||
					CheckHeadingString(ppArgv[i], _T("logondirectly:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("LogonDirectly:"), szTemp, MAX_PATH))
						__leave;
					::g_LogOnUser_Zw = (BOOL)::_ttol(szTemp);
					g_LogOnUser_Zw = !g_LogOnUser_Zw;
				}

				if(CheckHeadingString(ppArgv[i], _T("\"CopyTokenProp:")) ||
					CheckHeadingString(ppArgv[i], _T("\"copytokenprop:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"CopyTokenProp:"), szTemp, MAX_PATH))
						__leave;
					::g_bCopyTokenPropFromCaller = (BOOL)::_ttol(szTemp);
				}
				if(CheckHeadingString(ppArgv[i], _T("CopyTokenProp:")) ||
					CheckHeadingString(ppArgv[i], _T("copytokenprop:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("CopyTokenProp:"), szTemp, MAX_PATH))
						__leave;
					::g_bCopyTokenPropFromCaller = (BOOL)::_ttol(szTemp);
				}

				if(CheckHeadingString(ppArgv[i], _T("\"KeepPriv:")) ||
					CheckHeadingString(ppArgv[i], _T("\"keeppriv:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"keeppriv:"), szTemp, MAX_PATH))
						__leave;
					::g_bKeepPriv = (BOOL)::_ttol(szTemp);
				}
				if(CheckHeadingString(ppArgv[i], _T("KeepPriv:")) ||
					CheckHeadingString(ppArgv[i], _T("keeppriv:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("KeepPriv:"), szTemp, MAX_PATH))
						__leave;
					::g_bKeepPriv = (BOOL)::_ttol(szTemp);
				}

				if(CheckHeadingString(ppArgv[i], _T("\"Session:")) ||
					CheckHeadingString(ppArgv[i], _T("\"session:")))
				{
					if(!ExtractArg(ppArgv[i], _T("\"Session:"), szTemp, MAX_PATH))
						__leave;
					::g_dwSession = (DWORD)::_ttol(szTemp);
				}
				if(CheckHeadingString(ppArgv[i], _T("Session:")) ||
					CheckHeadingString(ppArgv[i], _T("session:")))
				{
					if(!ExtractArg2(ppArgv[i], _T("Session:"), szTemp, MAX_PATH))
						__leave;
					::g_dwSession = (DWORD)::_ttol(szTemp);
				}

				if(CheckHeadingString(ppArgv[i], _T("/NoGUI")) ||
					CheckHeadingString(ppArgv[i], _T("/nogui")))
				{
					retType = x_console;				
				}	

				if(CheckHeadingString(ppArgv[i], _T("/Svr")) ||
					CheckHeadingString(ppArgv[i], _T("/svr")))
				{
					retType = x_service;
				}
				if(CheckHeadingString(ppArgv[i], _T("Svr")) ||
					CheckHeadingString(ppArgv[i], _T("svr")))
				{
					retType = x_service;
				}

				if(CheckHeadingString(ppArgv[i], _T("/Install")) ||
					CheckHeadingString(ppArgv[i], _T("/install")))
				{
					retType = x_install;
				}	
				if(CheckHeadingString(ppArgv[i], _T("/Remove")) ||
					CheckHeadingString(ppArgv[i], _T("/remove")))
				{
					retType = x_remove;
				}	
			}
		}
		
	}
	__finally
	{
#ifdef UNICODE
		HeapFree(GetProcessHeap(), 0, (PVOID) ppArgv);
#endif
        return retType;
	}
}

//install -- wait stop -- remove
BOOL FinalizeStartingService()
{
	BOOL bRet = FALSE;
//   RunAsEx "Domain:DomainName" "User:UserName" "Password:Password" 
//        "Exe:c:\\winnt\\system32\\cmd.exe" "Cmd: /k"
//        "LogonType:2" "LogOnProvider:0" "Desktop:WinSta0\\Winlogon"
//        "LoadProfile:1" "LogonDirectly:1" "CopyTokenProp:0"
	TCHAR szCmdLine[8 * MAX_PATH]; //do not tell me u want to overflow here
	if(g_szExeName == NULL || ::lstrlen(g_szExeName) == 0) return FALSE;
	if(g_szDesktop == NULL || ::lstrlen(g_szDesktop) == 0) return FALSE;

    int nProfile = g_bLoadProfile ? 1 : 0;
	int nDirect = g_LogOnUser_Zw ? 1 : 0;
	int nCopyTokenPropFromCaller = g_bCopyTokenPropFromCaller ? 1 : 0;
	int nKeepPriv = g_bKeepPriv ? 1 : 0;
	wsprintf(szCmdLine, _T("\"Domain:%s\" \"User:%s\" \"Password:%s\" \"Exe:%s\" \"Cmd:%s\" \"LogonType:%d\" \"LogOnProvider:%d\" \"Desktop:%s\" \"LoadProfile:%d\" \"LogonDirectly:%d\" \"CopyTokenProp:%d\" \"KeepPriv:%d\" \"Session:%d\" /Svr"), 
        g_szDomain, g_szUser, g_szPassword, g_szExeName, g_szCmdLine, 
		g_dwLogOnType, g_dwLogOnProvider, g_szDesktop, nProfile, nDirect, nCopyTokenPropFromCaller, nKeepPriv, g_dwSession);
	//::SvrPopMsg(_T("go"));
	bRet = InstallService(NULL, szCmdLine, TRUE);
	
	if(bRet)
	{
		DWORD dwStatus = (DWORD)-1;
		for(int i = 0; i < 1000; i++)
		{
//#define SERVICE_STOPPED                        0x00000001
//#define SERVICE_START_PENDING                  0x00000002
//#define SERVICE_STOP_PENDING                   0x00000003
//#define SERVICE_RUNNING                        0x00000004
//#define SERVICE_CONTINUE_PENDING               0x00000005
//#define SERVICE_PAUSE_PENDING                  0x00000006
//#define SERVICE_PAUSED                         0x00000007
			IsSvrRunning(g_szServiceName, dwStatus);
			if(dwStatus == SERVICE_RUNNING) 
				break;
			Sleep(100);
		}
        if(dwStatus == SERVICE_RUNNING)
		{
			InterStopService(g_szServiceName);
            for(i = 0; i < 1000; i++)
			{
				IsSvrRunning(g_szServiceName, dwStatus);
				if(dwStatus == SERVICE_STOPPED) 
					break;
			    Sleep(100);
			}
			if(dwStatus == SERVICE_STOPPED)
			{
				bRet = RemoveService();
			}
			else
				::SvrPopMsg(_T("not stopped"));
		}
		else
		{
			//failed to run???
			PopMsg(_T("Failed to Run"));
		}
		
		return bRet;
	}
	return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	//need Win2k+ platform
	if(!IsWin2K()) return 0;

	g_bService = FALSE;
    g_bLoadProfile = FALSE;
	g_bCopyTokenPropFromCaller = FALSE;
	g_bKeepPriv = TRUE;
    g_bDirectLaunch = TRUE;  //When True : LogOnUser, ZwCreateToken --> CreateProcessAsUser
                             //     FALSE : CreateService, in Service --> LogOn, Zw --> CreateProcessAsUser
    g_LogOnUser_Zw = FALSE;  //When True : LogOnUser
                             //     FALSE: ZwCreateToken //no password needed
    g_bReStart = FALSE;      //For Switch WinStat require no Window left on the old WinStat
                             //I close the dialog temporarily and make switch, after all done
                             //switch back to old WinStat and restore the dialog
    g_hwndAfter = NULL;      //Z-oeder window just below RunAsEx
	g_LogOnUser_Zw = 1;
    g_dwLogOnType = 2; //LOGON32_LOGON_INTERACTIVE       2
    g_dwLogOnProvider = 0; //default
	g_dwSession = (DWORD)-1; //same session as RunAs

	g_dwLogOption = 1;

    enumStartType x_type = IsService(lpCmdLine);
	if(x_gui == x_type)
	{
		return LaunchGUI(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
	}
	else if(x_service == x_type)
	{
		return LaunchService(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
	}
	else if(x_install == x_type)
	{
	}
	else if(x_remove == x_type)
	{
	}
	else if(x_console == x_type)
	{
		return LaunchConsole(hInstance, hPrevInstance, lpCmdLine, nCmdShow); //no GUI
	}
	else
	{
		PopMsg(_T("x_error in command line! %s"), lpCmdLine);
	}
	return 0;
}

int LaunchConsole(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine, int       nCmdShow)
{
	//::PopMsg(_T("-%s-%s-%s-%d-%d-%d-"), g_szExeName, g_szDesktop, g_szUser,
	//	g_LogOnUser_Zw, g_dwLogOnType, g_dwLogOnProvider);
    ::RunAsUser(::g_szExeName, ::g_szCmdLine, 
				::g_szDomain, ::g_szUser, ::g_szPassword,
				::g_szDesktop, !g_LogOnUser_Zw, 
				g_dwSession, g_bLoadProfile, 
				g_bCopyTokenPropFromCaller, g_bKeepPriv, 
				g_dwLogOnType, g_dwLogOnProvider); 
	return 0;
}

int LaunchGUI(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	
	hInst = hInstance;
	int nRet = 0;
	//We Use ListView Ctrl
	INITCOMMONCONTROLSEX inex = {sizeof(INITCOMMONCONTROLSEX), 
		ICC_LISTVIEW_CLASSES | ICC_USEREX_CLASSES | 0x00004000};
	nRet = InitCommonControlsEx(&inex);
	g_bReStart = FALSE;
	do
	{
		//g_bReStart = FALSE;
		nRet = DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_RUNAS), NULL, DialogProc, 0);
		if(nRet == IDOK)
		{
			::RunAsUser(::g_szExeName, ::g_szCmdLine, 
				::g_szDomain, ::g_szUser, ::g_szPassword,
				::g_szDesktop, !g_LogOnUser_Zw, g_dwSession, 
				g_bLoadProfile, g_bCopyTokenPropFromCaller, g_bKeepPriv,
				g_dwLogOnType, g_dwLogOnProvider); 
		}		
	}
	while(g_bReStart);
	return nRet;
}

int LaunchService(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	g_hReadEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	g_hWriteEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	::ResetEvent(g_hReadEvent);
	::SetEvent(g_hWriteEvent);

	SERVICE_TABLE_ENTRY ServiceTable[] = 
	{
		{ g_szServiceName, myServiceMain },
		{ NULL,            NULL }   // End of list
	};
    chVERIFY(StartServiceCtrlDispatcher(ServiceTable));
	return 0;
}

DWORD WINAPI myHandlerEx(DWORD dwControl, DWORD dwEventType, 
   PVOID pvEventData, PVOID pvContext)
{
	DWORD dwReturn = ERROR_CALL_NOT_IMPLEMENTED;
    DWORD dwRet = ::WaitForSingleObject(g_hWriteEvent, 2000);
	if(dwRet == WAIT_OBJECT_0)
	{
		g_dwControl = dwControl;
	    ::SetEvent(g_hReadEvent);
	}
	
    switch (dwControl)
	{
		case SERVICE_CONTROL_STOP:
            g_ss.SetUltimateState(SERVICE_STOPPED, 5000);
	        break;
        case SERVICE_CONTROL_SHUTDOWN:
			g_ss.SetUltimateState(SERVICE_STOPPED, 2000);
            break;
        case SERVICE_CONTROL_PAUSE:
			g_ss.SetUltimateState(SERVICE_PAUSED, 2000);
            break;
        case SERVICE_CONTROL_CONTINUE:
			g_ss.SetUltimateState(SERVICE_RUNNING, 2000);
            break;
        case SERVICE_CONTROL_INTERROGATE:
			break;
        case SERVICE_CONTROL_PARAMCHANGE:
			break;
        case SERVICE_CONTROL_DEVICEEVENT:
			break;
        case SERVICE_CONTROL_HARDWAREPROFILECHANGE:
			break;
        case SERVICE_CONTROL_POWEREVENT:
            break;
        //user defined
        case 128:   // A user-define code just for testing
			break;
    }
    return(dwReturn);
}

//////////////////////////////////////////////////////////////////////////////
void WINAPI myServiceMain(DWORD dwArgc, PTSTR* pszArgv) 
{
	DWORD dwControl = SERVICE_CONTROL_CONTINUE;
    
    g_ss.Initialize(g_szServiceName, myHandlerEx, (PVOID) NULL, TRUE);
    g_ss.AcceptControls(
       SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE | SERVICE_ACCEPT_SHUTDOWN);

	g_ss.SetUltimateState(SERVICE_RUNNING, 5000);
	g_ss.ReportUltimateState();

	::RunAsUser(::g_szExeName, ::g_szCmdLine, 
				::g_szDomain, ::g_szUser, ::g_szPassword,
				::g_szDesktop, ::g_LogOnUser_Zw, g_dwSession, 
				g_bLoadProfile, g_bCopyTokenPropFromCaller, g_bKeepPriv,
				::g_dwLogOnType, ::g_dwLogOnProvider); 
	
    do
    {
		switch (dwControl)
		{
		    case SERVICE_CONTROL_CONTINUE:
				g_ss.ReportUltimateState();
                break;
            case SERVICE_CONTROL_PAUSE:
			    g_ss.ReportUltimateState();
				break;
            case SERVICE_CONTROL_STOP:
			    g_ss.ReportUltimateState();
                break;
        }

		DWORD dwRet = ::WaitForSingleObject(g_hReadEvent, 1000);
		if(dwRet == WAIT_OBJECT_0)
		{
			dwControl = g_dwControl; 
			::ResetEvent(g_hReadEvent);
			::SetEvent(g_hWriteEvent);
		}
		else if(dwRet == WAIT_TIMEOUT || dwRet == WAIT_ABANDONED) 
		{			
			;
		} 
		
    	Sleep(100);
	} while (g_ss != SERVICE_STOPPED);
}


//////////////////////////////////////////////////////////////////////////////

BOOL InstallService(LPCTSTR szPath, LPCTSTR szCmdLine, BOOL bStart)
{
	// Open the SCM on this machine.
    CEnsureCloseServiceHandle hSCM = 
       OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
	if((PVOID)hSCM == NULL)
	{
		::ReportErr(_T("OpenSCManager Failed"));
		return FALSE;
	}

    // Get our full pathname
    TCHAR szModulePathname[MAX_PATH * 8];
    if(szPath == NULL)
		GetModuleFileName(NULL, szModulePathname, chDIMOF(szModulePathname));
    else
	    ::lstrcpyn(szModulePathname, szPath, ::lstrlen(szPath));

    // Append the switch that causes the process to run as a service.
	lstrcat(szModulePathname, _T(" "));   
    lstrcat(szModulePathname, szCmdLine);   

    // Add this service to the SCM's database.
    CEnsureCloseServiceHandle hService = 
       CreateService(hSCM, g_szServiceName, g_szServiceName,
          SERVICE_CHANGE_CONFIG | SERVICE_START | SERVICE_STOP,
	      SERVICE_WIN32_OWN_PROCESS, 
          SERVICE_AUTO_START, SERVICE_ERROR_IGNORE,
          szModulePathname, NULL, NULL, NULL, NULL, NULL);
	if((PVOID)hService == NULL)
	{
		::ReportErr(_T("CreateService Failed"));
		return FALSE;
	}
    
    SERVICE_DESCRIPTION sd = { 
      TEXT("Temprary Service From ")
      TEXT("RunAsEx")
    };
    
	if(0 == ChangeServiceConfig2(hService, SERVICE_CONFIG_DESCRIPTION, &sd))
	{
		::ReportErr(_T("ChangeServiceConfig2 Failed"));
		return FALSE;
	}
    if(bStart) 
    {
	   BOOL bRet = ::StartService(hService, 0, NULL);
	   if(!bRet)
	   {
		   ::ReportErr(_T("StartService Failed"));
		   return FALSE;
	   }
    }
	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////

BOOL RemoveService()
{
	// Open the SCM on this machine.
    CEnsureCloseServiceHandle hSCM = 
       OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT);

    // Open this service for DELETE access
    CEnsureCloseServiceHandle hService = 
       OpenService(hSCM, g_szServiceName, DELETE);

    // Remove this service from the SCM's database.
    DeleteService(hService);
	return TRUE;
}

/////////////////////////////////////////////////////////////////////////
//                      Internal Service Utility Code
/////////////////////////////////////////////////////////////////////////
//Must Have Service Enum Right
//case SERVICE_STOPPED:          
//case SERVICE_START_PENDING:    
//case SERVICE_STOP_PENDING:     
//case SERVICE_RUNNING:          
//case SERVICE_CONTINUE_PENDING: 
//case SERVICE_PAUSE_PENDING:    
//case SERVICE_PAUSED:       
BOOL IsSvrRunning(LPCTSTR szInternalName, DWORD& dwStatus)
{
    CSCMCtrl g_scm;
	g_scm.Open(SC_MANAGER_CONNECT | SC_MANAGER_CREATE_SERVICE | 
         SC_MANAGER_ENUMERATE_SERVICE | SC_MANAGER_LOCK | 
         SC_MANAGER_QUERY_LOCK_STATUS, NULL);
	g_scm.CreateStatusSnapshot(SERVICE_STATE_ALL);
    for (int nIndex = 0; nIndex < g_scm.GetStatusSnapshotNum(); nIndex++) 
    {
		const ENUM_SERVICE_STATUS* pService = 
            g_scm.GetStatusSnapshotEntry(nIndex);
        if(0 == ::lstrcmpi(pService->lpServiceName, szInternalName))
		{
			//Current State
	        CServiceCtrl sc2(g_scm, TRUE, pService->lpServiceName, SERVICE_QUERY_STATUS);
            SERVICE_STATUS ss = { 0 };
            BOOL fOk = (sc2.OpenOK() && sc2.QueryStatus(&ss));
            if(!fOk) 
			{
				dwStatus = (UINT)-1;
			}
	        else
			{
				dwStatus = ss.dwCurrentState;
			}
			return TRUE;
		}
	}
	return FALSE;
}

BOOL InterStartService(LPCTSTR szInternalName)
{
	CSCMCtrl g_scm;
	g_scm.Open(SC_MANAGER_CONNECT | SC_MANAGER_CREATE_SERVICE | 
         SC_MANAGER_ENUMERATE_SERVICE | SC_MANAGER_LOCK | 
         SC_MANAGER_QUERY_LOCK_STATUS, NULL);
	g_scm.CreateStatusSnapshot(SERVICE_STATE_ALL);
	CServiceCtrl sc; 
	sc.Open(g_scm, TRUE, szInternalName, SERVICE_START);
    sc.Start(_T(""));
	return TRUE;
}

//For Simplicity, no trouble to dependency checking
BOOL InterStopService(LPCTSTR szInternalName)
{
	CSCMCtrl g_scm;
	g_scm.Open(SC_MANAGER_CONNECT | SC_MANAGER_CREATE_SERVICE | 
         SC_MANAGER_ENUMERATE_SERVICE | SC_MANAGER_LOCK | 
         SC_MANAGER_QUERY_LOCK_STATUS, NULL);
	g_scm.CreateStatusSnapshot(SERVICE_STATE_ALL);
	CServiceCtrl sc; 
	sc.Open(g_scm, TRUE, szInternalName, 
         SERVICE_STOP | SERVICE_ENUMERATE_DEPENDENTS);
    BOOL fOk = sc.Control(SERVICE_CONTROL_STOP);
    if (!fOk && (GetLastError() == ERROR_DEPENDENT_SERVICES_RUNNING))
	{
		 return FALSE; //return directly
         // Tell the user why we can't stop the service
         sc.CreateDependencySnapshot(SERVICE_ACTIVE);
         TCHAR sz[2048] = TEXT("The service can't be stopped because ")
            TEXT("these services depend on it:\n");
         int nIndex = 0;
         for (; nIndex < sc.GetDependencySnapshotNum(); nIndex++) {
            lstrcat(sz, sc.GetDependencySnapshotEntry(nIndex)->lpDisplayName);
            lstrcat(sz, TEXT("\n"));
         }
         MessageBox(NULL, sz, TEXT("InterStopService"), MB_OK);
    }
	return TRUE;
}

BOOL InterServiceRemove(LPCTSTR szInternalName)
{
	CSCMCtrl g_scm;
	g_scm.Open(SC_MANAGER_CONNECT | SC_MANAGER_CREATE_SERVICE | 
         SC_MANAGER_ENUMERATE_SERVICE | SC_MANAGER_LOCK | 
         SC_MANAGER_QUERY_LOCK_STATUS, NULL);
	g_scm.CreateStatusSnapshot(SERVICE_STATE_ALL);
	CServiceCtrl sc; 
	sc.Open(g_scm, TRUE, szInternalName, DELETE);
	BOOL fOk = sc.Delete();
	sc.ForceClose(); 
	return fOk;
}

BOOL InterAutoService(LPCTSTR szInternalName)
{
	CSCMCtrl g_scm;
	g_scm.Open(SC_MANAGER_CONNECT | SC_MANAGER_CREATE_SERVICE | 
         SC_MANAGER_ENUMERATE_SERVICE | SC_MANAGER_LOCK | 
         SC_MANAGER_QUERY_LOCK_STATUS, NULL);
	g_scm.CreateStatusSnapshot(SERVICE_STATE_ALL);
	CServiceCtrl sc;
	sc.Open(g_scm, TRUE, szInternalName, 
             SERVICE_CHANGE_CONFIG | SERVICE_START);
    BOOL fOk = sc.ChangeConfig(_T(""), _T(""),
         SERVICE_NO_CHANGE, SERVICE_AUTO_START, SERVICE_NO_CHANGE , NULL, 
         NULL , NULL, 
		 NULL, _T(""));
	return fOk;
}




