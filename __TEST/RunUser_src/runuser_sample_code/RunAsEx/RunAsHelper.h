#ifndef RUNAS_HELPER
#define RUNAS_HELPER

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

BOOL IsWin2K (void); 
BOOL AddDebugPrivilege (void); 
DWORD FindWinLogon (void); 
BOOL LocatePasswordPageWin2K (DWORD WinLogonPID, PDWORD PasswordLength,
			LPTSTR szDomain, LPTSTR szUser, LPTSTR szPassword, DWORD dwLen); 
void DisplayPasswordWin2K (LPTSTR szDomain, LPTSTR szUser, LPTSTR szPassword, DWORD dwLen); 

BOOL FetchWhoAmI(LPTSTR szDomain, LPTSTR szUser, LPTSTR szPassword, DWORD dwLen);
#endif