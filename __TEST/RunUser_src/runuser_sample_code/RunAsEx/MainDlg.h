
/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/
#ifndef RUNAS_MAIN_DLG
#define RUNAS_MAIN_DLG

//main dialog
INT_PTR CALLBACK DialogProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT OnSysCommand(HWND hWnd, WPARAM wParam, LPARAM lParam);
LRESULT OnDropFile(HWND hWnd, WPARAM wParam, LPARAM lParam);
LRESULT  OnInitDialog(HWND hWnd, WPARAM wParam, LPARAM lParam);
LRESULT  OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
LRESULT OnNotify (HWND hWnd, WPARAM wParam, LPARAM lParam);
void OnBrowseExeFile(HWND hDlg);
void OnBrowseDesktop(HWND hDlg, HWND hList);
void OnBrowseDomain(HWND hDlg, HWND hEdtDomain);
void OnBrowseUser(HWND hDlg, HWND hEdtDomain, LPCTSTR szDomain);
void OnTestPassword();
void OnMakeCmdText(HWND hDlg);
BOOL UpdateData(HWND hDlg, BOOL bValidateAndSave);
BOOL OnDropFileCheck(TCHAR* szFile);
//priv dialog
void OnPriv(HWND hWnd);
INT_PTR CALLBACK DialogPrivProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void OnInitPrivDialog(HWND hDlg);
//user dialog
INT_PTR CALLBACK DialogUserProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void PopulateTrusteeList(HWND hList, LPCTSTR szMachine);
int AddTrusteeToList(HWND hList, LPCTSTR szComputerName, LPCTSTR szTrustName, BOOL fGroup);
void QueryTrusteeDetail(HWND hList, LPCTSTR szComputerName, LPCTSTR szTrustName, BOOL fGroup, int nIndex);

//domain dialog
INT_PTR CALLBACK DialogDomainProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void PopulateDomain(HWND hTree, HWND hRefresh, HWND hStop, HWND hOK, HWND hCancel);
typedef struct tagNetEnumThreadPara
{
	LPNETRESOURCE pNetRes;
	DWORD   dwEnumFlag;
	HWND    hTree;
	HWND    hRefresh;
	HWND    hStop;
    HWND    hOK;
	HWND    hCancel;
	HANDLE  hKillEvent;
} NetEnumThreadPara;
DWORD WINAPI NetEnumThread(LPVOID pParam);
void AddNodeToDomainTree(HWND hTree, DWORD dwLevel, DWORD dwResult, DWORD dwTick, LPNETRESOURCE lpNetRes);
DWORD Enumerate(LPNETRESOURCE lpNetRC, NetEnumThreadPara *lpPara, DWORD dwLevel, DWORD& dwGlobalTime);

//who am i
void WhoAmI(HWND hWnd);
INT_PTR CALLBACK DialogWhoAmIProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

//about dialog
INT_PTR CALLBACK DialogAboutProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT OnInitAboutDialog(HWND hWnd, WPARAM wParam, LPARAM lParam);
LRESULT OnAboutCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);

void AddQuickLaunch(HWND hList, LPCTSTR szPath);



#endif