//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by RunAsEx.rc
//
#define IDC_MYICON                      2
#define IDC_LOGOFF                      2
#define IDD_RUNASEX_DIALOG              102
#define IDD_ABOUTBOX                    103
#define IDS_APP_TITLE                   104
#define IDM_EXIT                        106
#define IDS_HELLO                       107
#define IDI_RUNASEX                     108
#define IDD_PRIV                        109
#define IDI_SMALL                       110
#define IDC_RUNASEX                     111
#define IDR_MAINFRAME                   112
#define IDC_RUNASEX2                    112
#define IDD_DIALOG1                     113
#define IDC_RUNASEX3                    113
#define IDC_RUNASEX4                    114
#define IDD_RUNAS                       129
#define IDD_WHO_AM_I                    130
#define IDI_MAIN                        131
#define IDI_EMPTY                       132
#define IDD_USER                        133
#define IDD_DOMAIN                      134
#define IDB_DOMAIN                      135
#define IDI_DENY                        136
#define IDI_DOMAIN                      137
#define IDI_FOLDER                      138
#define IDI_PRINTER                     139
#define IDI_PROTOCOL                    140
#define IDI_SERVER                      141
#define IDI_ICON1                       142
#define IDI_SKULL                       143
#define IDI_TIGER                       144
#define IDI_JAPAN                       145
#define IDI_NT                          146
#define IDB_IMAGE                       147
#define IDB_ACCOUNT                     149
#define IDC_EDT_USER                    1000
#define IDC_EDT_PWD                     1001
#define IDC_BUTTON_XWD                  1002
#define IDC_LIST                        1003
#define IDC_EDT_EXE                     1004
#define IDC_BTN_BROWSE_EXE              1005
#define IDC_BTN_LAUNCH                  1006
#define IDC_BTN_LAUNCH_ZW               1007
#define IDC_EDT_PRIV_USERNAME           1008
#define IDC_BTN_SVR_ZW                  1009
#define IDC_EDT_SID                     1010
#define IDC_BTN_DOMAIN                  1011
#define IDC_CHECK_CHANGE_NOTIFY         1012
#define IDC_EDT_COMMANDLINE             1013
#define IDC_CHECK_INCREASE_QUOTA        1014
#define IDC_BTN_USER                    1015
#define IDC_CHECK_ASSIGN_PRIMARY_TOKEN  1016
#define IDC_BUTTON_PRIVILEGE            1017
#define IDC_BTN_PRIVILEGE               1018
#define IDC_CHECK_TCB                   1019
#define IDC_CHECK_CREATETOKEN           1020
#define IDC_CHK_PROFILE                 1021
#define IDC_BTN_LAUNCH2                 1022
#define IDC_CHK_COPYCALLER              1022
#define IDC_BTN_LAUNCH_LOGON            1023
#define IDC_CHECK_TAKEOWNERSHIP         1024
#define IDC_CHK_KEEP_PRIV               1024
#define IDC_BTN_WHO                     1025
#define IDC_EDIT1                       1025
#define IDC_EDT_DOMAIN                  1026
#define IDC_GO_CODEGURU                 1026
#define IDC_BTN_SVR_LOGON               1027
#define IDC_CHK_SESSION                 1028
#define IDC_COMBO_DESKTOP               1029
#define IDC_TREE                        1030
#define IDC_COMBO_SESSION               1030
#define IDC_EDT_MACHINE                 1031
#define IDC_BTN_WHO2                    1031
#define IDC_BTN_BUILD_CMD_LINE          1031
#define IDC_BTN_CMD_LINE                1031
#define IDC_BTN_REFRESH                 1032
#define IDC_RADIO_ERROR_LOG_NO          1032
#define IDC_BTN_STOP                    1033
#define IDC_RADIO_ERROR_LOG_EVENTLOG    1033
#define IDC_RADIO_ERROR_LOG_DISKFILE    1034
#define IDS_LOGONTYPE                   1040
#define IDS_LOGONPROVIDER               1042
#define IDC_COMBO_LOGONTYPE             1044
#define IDC_COMBO_LOGONPROVIDER         1045
#define IDM_ABOUT                       0x1230
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        147
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
