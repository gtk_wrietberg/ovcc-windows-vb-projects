// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

/************************************
  REVISION LOG ENTRY
  Revision By: Zhefu Zhang 
  Contact : codetiger@hotmail.com
  Revised on 2/13/2004 10:11:25 AM
  Comment: it is part of the code sample of 
           http://www.codeguru.com/misc/RunUser.html
 ************************************/

#if !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
#define AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

//Code Only For Win2K/XP/2003  
#define WINVER  0x0500  
#define _WIN32_WINNT  0x0500 
#define _WIN32_IE 0x0600


// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <commctrl.h>
#include <winnetwk.h>
#pragma comment(lib, "Comctl32")
#pragma comment(lib, "Netapi32")
//#pragma message("network library is linking with \"mpr.lib\"")
#pragma comment(lib, "mpr.lib")

#include "LSAstr.h"
//to add priv of account
#include "Sddl.h"
#pragma comment(lib, "Advapi32")
#include "AutoBuf.h"
#include "Ntsecapi.h"

#include <AclAPI.h>
#include <AclUI.h>
#include <ObjBase.h>
#include <ObjSel.h>
#include <TlHelp32.h>

#define GLOBALNET  0x1		// search the entire network
#define CONNECTED  0x2		// search only currently connected resources
#define REMEMBERED 0x4		// search only "persistent" connections

#define TYPE_ANY   0x8		// search all types of resources
#define TYPE_DISK  0x10		// search all disk resources
#define TYPE_PRINT 0x20		// search all print resources

#include <Commdlg.h>

//#include <Userenv.h> //User Profile
//#pragma comment(lib, "Userenv")


//warning C4244: '=' : conversion from 'unsigned int' to 'unsigned short', possible loss of data
#pragma warning(disable : 4244)
//warning C4005: 'UNICODE' : macro redefinition
#pragma warning(disable : 4005)

//for the sake of CommandLineToArgvW
#include <Shellapi.h>
#pragma comment(lib, "Shell32")

#include "CmnHdr.h"

void PopMsg(LPCTSTR pszFormat, ...) ;
void ReportErr(LPCTSTR str);
void ReportErrEx(LPCTSTR pszFormat, ...) ;
void ReportErrExToEvtLog(LPCTSTR szEvtName, LPCTSTR pszFormat, ...);
void ReportErrExToFile(LPCTSTR szEvtName, LPCTSTR pszFormat, ...);
BOOL WriteLineLog(LPCTSTR szFilename, LPCTSTR szMessage);

void SvrPopMsg(LPCTSTR pszFormat, ...) ;
void SvrReportErr(LPCTSTR str);

//for ZwCreateToken
#pragma comment(lib, "ntdll")

//for NetUserGetLocalGroups. ... 
#include <Lm.h>

//for WTS
#include <Wtsapi32.h>
#pragma comment(lib, "Wtsapi32")
// Local Header Files

//#include <stdio.h> //for sprintf

// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__A9DB83DB_A9FD_11D0_BFD1_444553540000__INCLUDED_)
