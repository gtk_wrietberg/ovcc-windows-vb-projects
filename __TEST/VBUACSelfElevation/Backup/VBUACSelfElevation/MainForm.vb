﻿'********************************* Module Header ***********************************\
' Module Name:	MainForm.vb
' Project:		VBUACSelfElevation
' Copyright (c) Microsoft Corporation.
' 
' User Account Control (UAC) is a new security component in Windows Vista and newer 
' operating systems. With UAC fully enabled, interactive administrators normally run 
' with least user privileges. This example demonstrates how to self-elevate the 
' process by giving explicit consent with the Consent UI. 
' 
' This source is subject to the Microsoft Public License.
' See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
' All other rights reserved.
' 
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER 
' EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF 
' MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
'***********************************************************************************/

Imports System.Runtime.InteropServices


Public Class MainForm

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Load
        ' Update the Self-elevate button to show the UAC shield icon.
        Me.btnElevate.FlatStyle = FlatStyle.System
        SendMessage(Me.btnElevate.Handle, BCM_SETSHIELD, 0, New IntPtr(1))
    End Sub

    <DllImport("user32", CharSet:=CharSet.Auto, SetLastError:=True)> _
    Shared Function SendMessage( _
        ByVal hWnd As IntPtr, _
        ByVal Msg As UInt32, _
        ByVal wParam As Integer, _
        ByVal lParam As IntPtr) _
        As Integer
    End Function

    Const BCM_SETSHIELD As UInt32 = &H160C


    Private Sub btnElevate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles btnElevate.Click
        ' Launch itself as administrator
        Dim proc As New ProcessStartInfo
        proc.UseShellExecute = True
        proc.WorkingDirectory = Environment.CurrentDirectory
        proc.FileName = Application.ExecutablePath
        proc.Verb = "runas"

        Try
            Process.Start(proc)
        Catch
            ' The user refused to allow privileges elevation.
            ' Do nothing and return directly ...
            Return
        End Try

        Application.Exit()  ' Quit itself
    End Sub

End Class
