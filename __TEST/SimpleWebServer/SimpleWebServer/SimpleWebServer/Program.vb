Imports System
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Text

Module Program
    Sub Main(args As String())

        Dim hostName As String = Environment.MachineName

        If Not HttpListener.IsSupported Then
            Console.WriteLine("Unable to start web server. HttpListener class not supported.")
            Return
        End If

        Select Case args.Length
            Case 1

                If args(0).ToLower = "listnics" Then
                    ListNics()
                    Return
                End If

            Case 2
                If args(0).ToLower = "listen" Then
                    Dim netInterface As String = args(1)

                    Dim listener As New HttpListener

                    'This is where we tell the HttpListener what interface
                    'and port to listen on
                    listener.Prefixes.Add($"http://{netInterface}/")

                    Try
                        'Start listenting for HTTP requests
                        listener.Start()
                    Catch ex As Exception

                        Console.WriteLine($"Error trying to start server : {ex.Message}")

                        Return
                    End Try

                    Console.WriteLine($"Listening for HTTP requests at {netInterface}")
                    Console.WriteLine("Press any key to exit.")

                    'Start the web server on another thread
                    Threading.ThreadPool.QueueUserWorkItem(Sub()

                                                               Dim visCount As Integer

                                                               Do
                                                                   Dim context = listener.GetContext
                                                                   Dim response As HttpListenerResponse = context.Response
                                                                   Dim request = context.Request



                                                                   Select Case request.RawUrl.ToLower


                                                                       'Redirect to the home page
                                                                       Case "/"
                                                                           response.Redirect("/home")


                                                                       Case "/home"
                                                                           'Home page
                                                                           '******************************************
                                                                           visCount += 1

                                                                           WriteWebPageResponse(GetHomePage(hostName, visCount), response)

                                                                       Case "/info"
                                                                           'The info page that lists all network interfaces on the server machine
                                                                           '******************************************

                                                                           WriteWebPageResponse(GetInfoHTML(), response)

                                                                       Case "/img"
                                                                           'This path is used for the favicon.
                                                                           'This is the icon that shows up in the left side of
                                                                           'a web page's browser tab in the web browser.
                                                                           '******************************************

                                                                           WriteImageResponse(My.Resources.LocalImages.favicon, response)

                                                                       Case "/download/info"
                                                                           'This path is used to download a text file with the same
                                                                           'information shown in the info page.
                                                                           '******************************************
                                                                           WriteDownloadResponse(System.Text.Encoding.UTF8.
                                                                                                GetBytes(GetInfo()), $"{hostName}-info.txt", response)

                                                                       Case "/download/picture"
                                                                           'This path triggers a download of an image file
                                                                           '******************************************

                                                                           WriteDownloadResponse(My.Resources.LocalImages.Moon, "moon.jpg", response)

                                                                       Case Else
                                                                           '404 Error. This happens when a request is made for a page
                                                                           'or resource from a path that doesn't exist.
                                                                           '******************************************

                                                                           response.StatusCode = HttpStatusCode.NotFound
                                                                           WriteWebPageResponse(Get404HTML(), response)


                                                                   End Select

                                                                   'Send reponse to web browser by closing the output stream
                                                                   response.OutputStream.Close()
                                                               Loop
                                                           End Sub)

                    Console.ReadKey()

                    Return
                End If

        End Select

        'Generates help for using the web server's command line interface
        '***********************************************************************
        Console.WriteLine("Usage:")
        Console.WriteLine("Eg 1: SimpleWebServer listen 192.168.100.27:5050")
        Console.WriteLine("Listens on interface 192.168.100.27, port 5050 for HTTP requests.")
        Console.WriteLine()

        Console.WriteLine("Eg 2: SimpleWebServer listen localhost:5050")
        Console.WriteLine("Listens on whatever interface localhost resolves to which is usually 127.0.0.1, and port 5050 for HTTP requests.")
        Console.WriteLine()

        Console.WriteLine("Eg 3: SimpleWebServer listnics")
        Console.WriteLine("Lists all network interfaces and their IPv4 addresses.")
    End Sub


    Private Sub WriteWebPageResponse(ByVal text As String, ByVal r As HttpListenerResponse)

        Dim data As Byte() = System.Text.Encoding.UTF8.GetBytes(text)

        r.ContentLength64 = data.Length
        r.OutputStream.Write(data, 0, data.Length)

    End Sub

    Private Sub WriteImageResponse(ByVal img As Byte(), ByVal r As HttpListenerResponse)

        'This is how web servers send images to web browsers
        '*********************************************************
        r.ContentType = "image/png"
        r.ContentLength64 = img.Length
        r.OutputStream.Write(img, 0, img.Length)

    End Sub

    Private Sub WriteDownloadResponse(ByVal file As Byte(), ByVal saveAsFileName As String, ByVal r As HttpListenerResponse)

        'This is how web servers induce browsers to download files from them.
        '*********************************************************
        r.AddHeader("Content-Disposition", $"attachment; filename=""{saveAsFileName}""")
        r.ContentType = "application/octet-stream"
        r.ContentLength64 = file.Length
        r.OutputStream.Write(file, 0, file.Length)
    End Sub


    Private Function GetFaviconHTML() As String
        'This is how you give a web page a favicon
        '*********************************************************
        Dim sb As New StringBuilder
        sb.AppendLine("<head>")
        sb.AppendLine("<link rel=""icon"" href=""/img""")
        sb.AppendLine("</head>")

        Return sb.ToString
    End Function

    Private Function GetHomePage(ByVal hostName As String, ByVal visCount As Integer) As String
        'This function generates the HTML for the home page.
        '*********************************************************

        Dim sb As New StringBuilder

        sb.AppendLine("<HTML>")

        sb.AppendLine(GetFaviconHTML())

        sb.AppendLine($"<p>Hello from {hostName}!</p>")
        sb.AppendLine($"<p>You are visitor number {visCount.ToString} to this site!</p>")

        sb.AppendLine(GetLinksHTML())

        sb.AppendLine("</HTML>")

        Return sb.ToString
    End Function


    Private Function GetInfoHTML() As String
        'This function generates the HTML for the info page.
        '*********************************************************

        Dim sb As New StringBuilder
        sb.AppendLine("<HTML>")

        sb.AppendLine(GetFaviconHTML())

        sb.AppendLine($"<b style=""font-size:30px"">Network interfaces on {Environment.MachineName}:</b>")

        For Each ni In NetworkInterface.GetAllNetworkInterfaces()

            sb.AppendLine($"<p style=""text-decoration: underline;"">{ni.Name}</p>")

            For Each ip In ni.GetIPProperties.UnicastAddresses.Where(Function(ipi) ipi.Address.AddressFamily = Net.Sockets.AddressFamily.InterNetwork)
                sb.AppendLine($"<p><b style=""margin-left: 15px; color:blue;font-size:20px"">{ip.Address.ToString}</b></p>")
            Next

            sb.AppendLine()
        Next

        sb.AppendLine(GetLinksHTML())
        sb.AppendLine("</HTML>")


        Return sb.ToString
    End Function

    Private Function Get404HTML() As String
        'This function generates the HTML for the 404 page.
        '*********************************************************

        Dim sb As New StringBuilder
        sb.AppendLine("<HTML>")

        sb.AppendLine(GetFaviconHTML())

        sb.AppendLine($"<b style=""font-size:70px;color:red"">ERROR 404</b>")
        sb.AppendLine("</br>")
        sb.AppendLine($"<b style=""font-size:30px;color:blue"">There is nothing here. Get out!!</b>")

        sb.AppendLine(GetLinksHTML())
        sb.AppendLine("</HTML>")

        Return sb.ToString
    End Function

    Private Function GetLinksHTML() As String
        'This function generates the HTML for the links at the bottom
        'of every HTML page
        '*********************************************************

        Dim sb As New StringBuilder

        sb.AppendLine("<p>")

        sb.AppendLine("<span style=""margin-right:10px"">")
        sb.AppendLine("<a href=""/home"">Home</a>")
        sb.AppendLine("</span>")

        sb.AppendLine("<span style=""margin-right:10px"">")
        sb.AppendLine("<a href=""/info"">Info</a>")
        sb.AppendLine("</span>")

        sb.AppendLine("<span style=""margin-right:10px"">")
        sb.AppendLine("<a href=""/download/info"">Download info as text file</a>")
        sb.AppendLine("</span>")

        sb.AppendLine("<span style=""margin-right:10px"">")
        sb.AppendLine("<a href=""/download/picture"">Download a picture</a>")
        sb.AppendLine("</span>")

        sb.AppendLine("</p>")

        Return sb.ToString
    End Function


    Private Function GetInfo() As String
        'This function returns a plain text version of the network information
        'you see on the info page.
        '*********************************************************
        Dim sb As New StringBuilder

        sb.AppendLine($"Network interfaces on {Environment.MachineName}:")
        sb.AppendLine()

        For Each ni In NetworkInterface.GetAllNetworkInterfaces()

            sb.AppendLine(ni.Name)

            For Each ip In ni.GetIPProperties.UnicastAddresses.Where(Function(ipi) ipi.Address.AddressFamily = Net.Sockets.AddressFamily.InterNetwork)
                sb.Append("    ")
                sb.AppendLine(ip.Address.ToString)
            Next
            sb.AppendLine()
        Next

        Return sb.ToString
    End Function

    Private Sub ListNics()
        'This function is called by the command line interface when the
        '/listnics argument is used. It displays a list of all the network interfaces
        'on OS where the web server is being executed.
        For Each ni In NetworkInterface.GetAllNetworkInterfaces()

            Console.WriteLine(ni.Name)

            For Each ip In ni.GetIPProperties.UnicastAddresses.Where(Function(ipi) ipi.Address.AddressFamily = Net.Sockets.AddressFamily.InterNetwork)
                Console.Write("    ")
                Console.WriteLine(ip.Address.ToString)
            Next
            Console.WriteLine()
        Next

    End Sub




End Module
