Imports System.Net
Imports System.Net.Mail
Imports System.Text.RegularExpressions

Public Class SendMail
    Private mServer As String
    Private mPort As Integer
    Private mUsername As String
    Private mPassword As String
    Private mSender As String
    Private mRecipient As String
    Private mSubject As String
    Private mBody As String

    Public Property Server() As String
        Get
            Return mServer
        End Get
        Set(ByVal value As String)
            mServer = value
        End Set
    End Property

    Public Property Port() As Integer
        Get
            Return mPort
        End Get
        Set(ByVal value As Integer)
            mPort = value
        End Set
    End Property

    Public Property Username() As String
        Get
            Return mUsername
        End Get
        Set(ByVal value As String)
            mUsername = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(ByVal value As String)
            mPassword = value
        End Set
    End Property

    Public Property Sender() As String
        Get
            Return mSender
        End Get
        Set(ByVal value As String)
            mSender = value
        End Set
    End Property

    Public Property Recipient() As String
        Get
            Return mRecipient
        End Get
        Set(ByVal value As String)
            mRecipient = value
        End Set
    End Property

    Public Property Subject() As String
        Get
            Return mSubject
        End Get
        Set(ByVal value As String)
            mSubject = value
        End Set
    End Property

    Public Property Body() As String
        Get
            Return mBody
        End Get
        Set(ByVal value As String)
            mBody = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal server As String, ByVal port As Integer, ByVal username As String, ByVal password As String, ByVal sender_address As String, ByVal recipient_address As String)
        mServer = server
        mPort = port
        mUsername = username
        mPassword = password
        mSender = sender_address
        mRecipient = recipient_address
    End Sub

    Public Sub SendEmail()
        Dim mail As New MailMessage()

        mSender = CleanInput(mSender)
        mRecipient = CleanInput(mRecipient)

        mail.From = New MailAddress(mSender)
        mail.To.Add(mRecipient)

        mail.Subject = mSubject
        mail.Body = mBody

        Dim smtp As New SmtpClient(mServer, mPort)

        smtp.Credentials = New NetworkCredential(mUsername, mPassword)
        smtp.Send(mail)
    End Sub

    Public Function Debug() As String
        Dim sDebug As String

        sDebug = "SendMail properties:" & vbCrLf & vbCrLf
        sDebug = sDebug & "mServer = " & mServer & vbCrLf
        sDebug = sDebug & "mPort = " & mPort & vbCrLf
        sDebug = sDebug & "mUsername = " & mUsername & vbCrLf
        sDebug = sDebug & "mPassword = " & mPassword & vbCrLf
        sDebug = sDebug & "mSender = " & mSender & vbCrLf
        sDebug = sDebug & "mRecipient = " & mRecipient & vbCrLf
        sDebug = sDebug & "mSubject = " & mSubject & vbCrLf
        sDebug = sDebug & "mBody = " & mBody & vbCrLf

        Return sDebug
    End Function

    Private Function CleanInput(ByVal strIn As String) As String
        Try
            Return Regex.Replace(strIn, "[^\w\.@-]", "")
        Catch e As Exception
            Return String.Empty
        End Try
    End Function
End Class
