Module Main
    Public Const c__SmtpServerName As String = "smtp.lobbypc.com"
    Public Const c__SmtpServerPort As Integer = 587
    Public Const c__SmtpServerUserName As String = "3MA1LY00ser"
    Public Const c__SmtpServerPassWord As String = "$3k00r3MI$MtP@cNt"
    Public Const c__Recipient As String = "lobbypc.report@gmail.com"

    Public Sub Main()
        'Try
        Dim oMail As New SendMail
        Dim sBody As String

        oMail.Server = c__SmtpServerName
        oMail.Port = c__SmtpServerPort
        oMail.Username = c__SmtpServerUserName
        oMail.Password = c__SmtpServerPassWord
        oMail.Sender = Environment.MachineName & "_noreply@lobbypc.com"
        oMail.Recipient = c__Recipient

        oMail.Subject = Environment.MachineName & " squid install report"

        sBody = My.Resources.DefaultMailBody
        sBody = sBody.Replace("%%MACHINENAME%%", Environment.MachineName)
        sBody = sBody.Replace("%%RESULT_SKCFG%%", "True")
        sBody = sBody.Replace("%%COPY_SQUID%%", "1/1/0")
        sBody = sBody.Replace("%%COPY_SQUIDGUARD%%", "1/1/0")
        sBody = sBody.Replace("%%COPY_WINDOWS%%", "1/1/0")
        sBody = sBody.Replace("%%STDERR_INSTALL_SCRIPT%%", "Bla01")
        sBody = sBody.Replace("%%STDOUT_INSTALL_SCRIPT%%", "Bla02")
        sBody = sBody.Replace("%%STDERR_RUNAS%%", "Bla03")
        sBody = sBody.Replace("%%STDOUT_RUNAS%%", "Bla04")
        sBody = sBody.Replace("%%STDOUT_PROXY_SETTER%%", "Bla05")

        oMail.Body = sBody

        MsgBox(oMail.Debug)

        oMail.SendEmail()
        'Catch ex As Exception
        'MsgBox("ex.ToString: " & vbCrLf & ex.ToString)
        'MsgBox("ex.InnerException.ToString: " & vbCrLf & ex.InnerException.ToString)
        'End Try
    End Sub
End Module
