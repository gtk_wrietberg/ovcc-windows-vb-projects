Imports System
Imports System.Text
Imports System.Security.Cryptography

Public Class LicenseCrypt
    Public Function Encrypt(ByVal sMessage As String, ByVal sPassphrase As String) As String
        Dim byteResults As Byte()

        byteResults = Convert.FromBase64String("")

        Dim UTF8 = New System.Text.UTF8Encoding()
        Dim HashProvider As New MD5CryptoServiceProvider()

        Dim TDESKey As Byte()
        TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(sPassphrase))

        Dim TDESAlgorithm As New TripleDESCryptoServiceProvider
        TDESAlgorithm.Key = TDESKey
        TDESAlgorithm.Mode = CipherMode.ECB
        TDESAlgorithm.Padding = PaddingMode.PKCS7

        Dim DataToEncrypt As Byte()
        DataToEncrypt = UTF8.GetBytes(sMessage)

        Try
            Dim Encryptor As ICryptoTransform
            Encryptor = TDESAlgorithm.CreateEncryptor()
            byteResults = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length)
        Catch ex As Exception


        Finally
            TDESAlgorithm.Clear()
            HashProvider.Clear()
        End Try

        Return Convert.ToBase64String(byteResults)
    End Function

    Public Function Decrypt(ByVal sMessage As String, ByVal sPassphrase As String) As String
        Dim byteResults As Byte()

        byteResults = Convert.FromBase64String("")

        Dim UTF8 = New System.Text.UTF8Encoding()
        Dim HashProvider As New MD5CryptoServiceProvider()

        Dim TDESKey As Byte()
        TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(sPassphrase))

        Dim TDESAlgorithm As New TripleDESCryptoServiceProvider
        TDESAlgorithm.Key = TDESKey
        TDESAlgorithm.Mode = CipherMode.ECB
        TDESAlgorithm.Padding = PaddingMode.PKCS7

        Dim DataToDecrypt As Byte()
        DataToDecrypt = UTF8.GetBytes(sMessage)

        Try
            Dim Decryptor As ICryptoTransform
            Decryptor = TDESAlgorithm.CreateDecryptor()
            byteResults = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length)
        Catch ex As Exception


        Finally
            TDESAlgorithm.Clear()
            HashProvider.Clear()
        End Try

        Return Convert.ToBase64String(byteResults)
    End Function
End Class