using System;
using System.Collections.Generic;
using System.Text;
using System.Net.NetworkInformation;
using System.Management;
using System.Net;
using System.Windows;

namespace Test07
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {

            }

            System.Windows.Forms.MessageBox.Show("Test");

            return;

            string dnsServer = "172.17.128.8";
            Console.WriteLine("Pinging {0}...", dnsServer);
            if (Ping(dnsServer))
                Console.WriteLine("Ping completed successfully.");
            else
            {
                Console.WriteLine("Unable to ping {0}.", dnsServer);
                dnsServer = "";
            }

            Console.WriteLine();
            SetDnsServer(dnsServer);
        }

        private static void SetDnsServer(string host)
        {
            ManagementClass nicManagement = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection nicList = nicManagement.GetInstances();

            List<string> dnsServerList = new List<string>();

            //resolve host to ip address
            string action = "reset";
            bool resetDnsServer = true;
            if (!string.IsNullOrEmpty(host))
            {
                IPAddress[] ipList = Dns.GetHostAddresses(host);
                foreach (IPAddress ip in ipList)
                {
                    dnsServerList.Add(ip.ToString());
                }
                action = "set";
                resetDnsServer = false;
            }

            //set dns server
            foreach (ManagementObject nic in nicList)
            {
                bool isIpEnabled = (bool)nic["ipEnabled"];
                //set dns if NIC is enabled or in reset mode
                if (isIpEnabled || resetDnsServer)
                {
                    ManagementBaseObject dnsServerSearchOrderParameters = nic.GetMethodParameters("SetDNSServerSearchOrder");
                    if (dnsServerSearchOrderParameters != null)
                    {
                        string nicDescription = nic.Properties["Description"].Value.ToString();
                        dnsServerSearchOrderParameters["DNSServerSearchOrder"] = dnsServerList.ToArray();
                        nic.InvokeMethod("SetDNSServerSearchOrder", dnsServerSearchOrderParameters, null);
                        Console.WriteLine("DNS Server for '{0} has been {1}.", nicDescription, action);
                    }

                }
            }
        }

        private static bool Ping(string host)
        {
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(host);
                return (reply.Status == IPStatus.Success);
            }
            catch (PingException)
            {
                return false;
            }
        }
    }

}
