﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Timer1.Enabled = Not Timer1.Enabled

        TextBox1.Visible = Timer1.Enabled
    End Sub

    Private Sub NetStat_()
        Try
            Dim builder As New System.Text.StringBuilder
            Dim ipProps As System.Net.NetworkInformation.IPGlobalProperties = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties()
            Console.WriteLine("{0,-22} -> {1,-22} - {2,-25} {3}", "LOCAL ENDPOINT", "REMOTE ENDPOINT", "STATE", "ADDRESS FAMILY")
            Console.WriteLine("{0,-22} -> {1,-22} - {2,-25} {3}", "--------------", "---------------", "-----", "--------------")
            For Each connection As System.Net.NetworkInformation.TcpConnectionInformation In ipProps.GetActiveTcpConnections
                builder.AppendFormat("{0,-22} -> {1,-22} - {2,-25} {3}{4}", connection.LocalEndPoint, connection.RemoteEndPoint, connection.State, connection.RemoteEndPoint.AddressFamily.ToString, Environment.NewLine)
            Next
            Console.WriteLine(builder.ToString())
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Function NetStat() As Integer
        Dim connCount As Integer = 0

        Try
            Dim builder As New System.Text.StringBuilder
            Dim ipProps As System.Net.NetworkInformation.IPGlobalProperties = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties()


            Dim tmpStr As String = ""
            Dim rePattern As String = "(Mr\.? |Mrs\.? |Miss |Ms\.? )"

            For Each connection As System.Net.NetworkInformation.TcpConnectionInformation In ipProps.GetActiveTcpConnections
                If connection.State = Net.NetworkInformation.TcpState.Established Then
                    If connection.LocalEndPoint.Port > 52000 Then
                        If connection.RemoteEndPoint.Port = 80 Then
                            connCount += 1
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            connCount = -1
        End Try

        Return connCount
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        TextBox1.Text = ""

        TextBox1.Text = Class1.Network.LogMeIn.GetConnectionCount().ToString
    End Sub

End Class
