﻿Imports System.Runtime.InteropServices
Imports System.Net
Imports System.Net.NetworkInformation

Public Class Class1
#Region "Network"
    Public Class Network
        Public Class NetStat
            Private Const AF_INET As Integer = 2

            <StructLayout(LayoutKind.Sequential)> _
            Private Structure MIB_TCPTABLE_OWNER_PID
                Public dwNumEntries As UInteger
                Private table As MIB_TCPROW_OWNER_PID
            End Structure

            Private Enum TCP_TABLE_CLASS
                TCP_TABLE_BASIC_LISTENER
                TCP_TABLE_BASIC_CONNECTIONS
                TCP_TABLE_BASIC_ALL
                TCP_TABLE_OWNER_PID_LISTENER
                TCP_TABLE_OWNER_PID_CONNECTIONS
                TCP_TABLE_OWNER_PID_ALL
                TCP_TABLE_OWNER_MODULE_LISTENER
                TCP_TABLE_OWNER_MODULE_CONNECTIONS
                TCP_TABLE_OWNER_MODULE_ALL
            End Enum

            <DllImport("iphlpapi.dll", SetLastError:=True)> _
            Private Shared Function GetExtendedTcpTable(ByVal tcpTable As IntPtr, ByRef tcpTableLength As Integer, ByVal sort As Boolean, ByVal ipVersion As Integer, ByVal tcpTableType As TCP_TABLE_CLASS, ByVal reserved As Integer) As UInteger
            End Function

            <StructLayout(LayoutKind.Sequential)> _
            Public Structure MIB_TCPROW_OWNER_PID
                ' DWORD is System.UInt32 in C#
                <MarshalAs(UnmanagedType.U4)> Public state As UInt32
                <MarshalAs(UnmanagedType.U4)> Public localAddr As UInt32
                <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> Public m_localPort As Byte()
                <MarshalAs(UnmanagedType.U4)> Public remoteAddr As UInt32
                <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> Public m_remotePort As Byte()
                <MarshalAs(UnmanagedType.U4)> Public owningPid As UInt32

                Public ReadOnly Property LocalAddress() As IPAddress
                    Get
                        Return New IPAddress(localAddr)
                    End Get
                End Property

                Public ReadOnly Property LocalPort() As UShort
                    Get
                        Return BitConverter.ToUInt16(New Byte(1) {m_localPort(1), m_localPort(0)}, 0)
                    End Get
                End Property

                Public ReadOnly Property RemoteAddress() As IPAddress
                    Get
                        Return New IPAddress(remoteAddr)
                    End Get
                End Property

                Public ReadOnly Property RemotePort() As UShort
                    Get
                        Return BitConverter.ToUInt16(New Byte(1) {m_remotePort(1), m_remotePort(0)}, 0)
                    End Get
                End Property
            End Structure

            Public Class TcpRow
                Private ReadOnly _localEndPoint As IPEndPoint
                Private ReadOnly _remoteEndPoint As IPEndPoint
                Private ReadOnly _state As TcpState
                Private ReadOnly _process As Process

                Public Sub New(ByVal tcpRow As MIB_TCPROW_OWNER_PID)
                    _state = tcpRow.state
                    _process = Process.GetProcessById(tcpRow.owningPid)

                    Dim localPort As Integer = tcpRow.LocalPort
                    Dim localAddress As IPAddress = tcpRow.LocalAddress
                    _localEndPoint = New IPEndPoint(localAddress, localPort)

                    Dim remotePort As Integer = tcpRow.RemotePort
                    Dim remoteAddress As IPAddress = tcpRow.RemoteAddress
                    _remoteEndPoint = New IPEndPoint(remoteAddress, remotePort)
                End Sub

                Public ReadOnly Property LocalEndPoint() As IPEndPoint
                    Get
                        Return _localEndPoint
                    End Get
                End Property

                Public ReadOnly Property RemoteEndPoint() As IPEndPoint
                    Get
                        Return _remoteEndPoint
                    End Get
                End Property

                Public ReadOnly Property State() As TcpState
                    Get
                        Return _state
                    End Get
                End Property

                Public ReadOnly Property ProcessId() As Integer
                    Get
                        Return _process.Id
                    End Get
                End Property

                Public ReadOnly Property ProcessName() As String
                    Get
                        Return _process.ProcessName
                    End Get
                End Property

                Public Overrides Function ToString() As String
                    Return String.Format("TCP    {0,-23}{1, -23}{2,-14}{3,-6}{4}", LocalEndPoint, RemoteEndPoint, State, ProcessId, ProcessName)
                End Function
            End Class

            Public Shared Function GetTcpTable(Optional ByVal sorted As Boolean = False) As List(Of TcpRow)
                Dim tcpRows As New List(Of TcpRow)

                Dim tcpTable As IntPtr = IntPtr.Zero
                Dim tcpTableLength As Integer = 0

                If GetExtendedTcpTable(tcpTable, tcpTableLength, sorted, AF_INET, TCP_TABLE_CLASS.TCP_TABLE_OWNER_PID_ALL, 0) <> 0 Then
                    Try
                        tcpTable = Marshal.AllocHGlobal(tcpTableLength)
                        If GetExtendedTcpTable(tcpTable, tcpTableLength, True, AF_INET, TCP_TABLE_CLASS.TCP_TABLE_OWNER_PID_ALL, 0) = 0 Then
                            Dim table As MIB_TCPTABLE_OWNER_PID = CType(Marshal.PtrToStructure(tcpTable, GetType(MIB_TCPTABLE_OWNER_PID)), MIB_TCPTABLE_OWNER_PID)

                            Dim rowPtr As IntPtr = New IntPtr(tcpTable.ToInt64() + Marshal.SizeOf(table.dwNumEntries))
                            For i As Integer = 0 To table.dwNumEntries - 1
                                tcpRows.Add(New TcpRow(CType(Marshal.PtrToStructure(rowPtr, GetType(MIB_TCPROW_OWNER_PID)), MIB_TCPROW_OWNER_PID)))
                                rowPtr = New IntPtr(rowPtr.ToInt64() + Marshal.SizeOf(GetType(MIB_TCPROW_OWNER_PID)))
                            Next
                        End If
                    Finally
                        If tcpTable <> IntPtr.Zero Then
                            Marshal.FreeHGlobal(tcpTable)
                        End If
                    End Try
                End If

                Return tcpRows
            End Function
        End Class


        Public Class LogMeIn
            Public Shared Function GetConnectionCount() As Integer
                Dim iCount As Integer = 0

                For Each tcpr As NetStat.TcpRow In NetStat.GetTcpTable()
                    If tcpr.ProcessName.Equals("LogMeIn") And tcpr.State = TcpState.Established Then
                        iCount += 1
                    End If
                Next

                Return iCount
            End Function

            Public Shared Function IsConnected(Optional ThresholdCount As Integer = 3) As Boolean
                Return (GetConnectionCount() >= ThresholdCount)
            End Function
        End Class
    End Class
#End Region

End Class
