﻿Imports System.DirectoryServices.ActiveDirectory

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim dummyDomain As Domain = Domain.GetCurrentDomain()

            MsgBox("Domain: " & dummyDomain.Name)
        Catch ex As Exception
            MsgBox("Not on domain: " & ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            MsgBox("Domain: " & System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName)

        Catch ex As Exception
            MsgBox("Not on domain: " & ex.Message)
        End Try
    End Sub
End Class
