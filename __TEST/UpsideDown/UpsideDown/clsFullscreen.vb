﻿Imports System.Windows.Forms

Public Class Fullscreen
    Private Shared mOldBounds As Rectangle

    Private Shared mState As Boolean = False
    Public Shared ReadOnly Property FullscreenState() As Boolean
        Get
            Return mState
        End Get
    End Property

    Public Shared Function Toggle(targetForm As Form) As Boolean
        If mState Then
            Leave(targetForm)
        Else
            Enter(targetForm)
        End If

        Return mState
    End Function

    Public Shared Sub Enter(targetForm As Form, Optional OnTop As Boolean = True)
        mOldBounds = targetForm.Bounds

        targetForm.WindowState = FormWindowState.Normal
        targetForm.FormBorderStyle = FormBorderStyle.None
        targetForm.Bounds = Screen.PrimaryScreen.WorkingArea
        targetForm.TopMost = OnTop

        mState = True
    End Sub

    Public Shared Sub Leave(targetForm As Form)
        targetForm.FormBorderStyle = FormBorderStyle.FixedSingle
        targetForm.WindowState = FormWindowState.Normal
        targetForm.Bounds = mOldBounds

        mState = False
    End Sub
End Class
