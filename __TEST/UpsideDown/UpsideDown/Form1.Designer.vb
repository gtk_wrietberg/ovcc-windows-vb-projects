﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnScreenshot = New System.Windows.Forms.Button()
        Me.picScreenshot = New System.Windows.Forms.PictureBox()
        Me.btnFullscreenToggle = New System.Windows.Forms.Button()
        Me.btnRotate = New System.Windows.Forms.Button()
        CType(Me.picScreenshot, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnScreenshot
        '
        Me.btnScreenshot.Location = New System.Drawing.Point(91, 12)
        Me.btnScreenshot.Name = "btnScreenshot"
        Me.btnScreenshot.Size = New System.Drawing.Size(72, 24)
        Me.btnScreenshot.TabIndex = 0
        Me.btnScreenshot.Text = "screenshot"
        Me.btnScreenshot.UseVisualStyleBackColor = True
        '
        'picScreenshot
        '
        Me.picScreenshot.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picScreenshot.Location = New System.Drawing.Point(0, 0)
        Me.picScreenshot.Name = "picScreenshot"
        Me.picScreenshot.Size = New System.Drawing.Size(958, 573)
        Me.picScreenshot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picScreenshot.TabIndex = 1
        Me.picScreenshot.TabStop = False
        '
        'btnFullscreenToggle
        '
        Me.btnFullscreenToggle.Location = New System.Drawing.Point(12, 12)
        Me.btnFullscreenToggle.Name = "btnFullscreenToggle"
        Me.btnFullscreenToggle.Size = New System.Drawing.Size(73, 24)
        Me.btnFullscreenToggle.TabIndex = 2
        Me.btnFullscreenToggle.Text = "FS toggle"
        Me.btnFullscreenToggle.UseVisualStyleBackColor = True
        '
        'btnRotate
        '
        Me.btnRotate.Location = New System.Drawing.Point(169, 12)
        Me.btnRotate.Name = "btnRotate"
        Me.btnRotate.Size = New System.Drawing.Size(75, 24)
        Me.btnRotate.TabIndex = 3
        Me.btnRotate.Text = "rotate"
        Me.btnRotate.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(958, 573)
        Me.Controls.Add(Me.btnRotate)
        Me.Controls.Add(Me.btnFullscreenToggle)
        Me.Controls.Add(Me.btnScreenshot)
        Me.Controls.Add(Me.picScreenshot)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.picScreenshot, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnScreenshot As System.Windows.Forms.Button
    Friend WithEvents picScreenshot As System.Windows.Forms.PictureBox
    Friend WithEvents btnFullscreenToggle As System.Windows.Forms.Button
    Friend WithEvents btnRotate As System.Windows.Forms.Button

End Class
