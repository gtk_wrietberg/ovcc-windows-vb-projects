﻿Public Class Form1

    Private Sub button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnScreenshot.Click
        picScreenshot.Image = TakeScreenShot()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub


    Private Function TakeScreenShot() As Bitmap
        Dim screenSize As Size = New Size(My.Computer.Screen.Bounds.Width, My.Computer.Screen.Bounds.Height)
        Dim screenGrab As New Bitmap(My.Computer.Screen.Bounds.Width, My.Computer.Screen.Bounds.Height)
        Dim g As Graphics = Graphics.FromImage(screenGrab)

        g.CopyFromScreen(New Point(0, 0), New Point(0, 0), screenSize)

        Return screenGrab
    End Function

    Private Sub btnFullscreenToggle_Click(sender As Object, e As EventArgs) Handles btnFullscreenToggle.Click
        Fullscreen.Toggle(Me)
    End Sub

    Private Sub btnRotate_Click(sender As Object, e As EventArgs) Handles btnRotate.Click
        Try
            Dim bitmap1 As Bitmap = picScreenshot.Image

            bitmap1.RotateFlip(RotateFlipType.Rotate180FlipNone)

            picScreenshot.Image = bitmap1
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class
