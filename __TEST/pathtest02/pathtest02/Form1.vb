﻿Public Class Form1
    Public ReadOnly c_REGEXP_LOGFILE As String = "([0-9]{4})\-([0-9]{2})\-([0-9]{2})\.txt"

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MsgBox(GetSiteKioskInstallFolder)
        MsgBox(GetSiteKioskLogfileFolder)

        Dim di As New IO.DirectoryInfo(GetSiteKioskLogfileFolder)
        Dim diar1 As IO.FileInfo() = di.GetFiles()
        Dim dra As IO.FileInfo

        'list the names of all files in the specified directory
        For Each dra In diar1
            If System.Text.RegularExpressions.Regex.IsMatch(dra.Name, c_REGEXP_LOGFILE) Then
                MsgBox("YES: " & dra.Name)
            Else
                MsgBox("NO: " & dra.Name)
            End If

            'MsgBox(dra.Name)
        Next
    End Sub

    Public Function GetSiteKioskInstallFolder() As String
        Dim reg As Microsoft.Win32.RegistryKey

        reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Provisio\\SiteKiosk", False)
        If reg Is Nothing Then
            Return ""
        Else
            Return reg.GetValue("InstallDir", "")
        End If
    End Function

    Public Function GetSiteKioskLogfileFolder() As String
        Return GetSiteKioskInstallFolder() & "\logfiles"
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim dLastLog As Date

        dLastLog = Date.Parse("2015-1-19")
    End Sub
End Class
