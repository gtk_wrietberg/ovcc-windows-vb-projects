Public NotInheritable Class UFlatButton     'Remove NotInheritable if You want to inherit
    Inherits UBase                          'Inherits from my base UserControl
    'See the code to know what it does!


    'Welcome to the ultimate button wich requires still allot of work.
    'Everybody have probably seen some of XP's buttons.
    'This is impresive. The round edges make it feel much more not
    'like just a button. It gives it a nice feel to a form you want to 
    'create.
    '
    'Here is how to do this simple.
    'By that I mean there is no real text align property for the text
    'and it does not dislay the shortcut caracter. "E&xit" -> Exit
    '                                                          -
    'Ok so the code must go on.

    'Vir al die programeerder wat Afrikaans is, eks jammer die code
    'is in Engels geskryf, maar anders sal meeste ander mense dit mos nou 
    'nie verstaan het nie, but the English must mind my weak spelling mistakes.

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'UFlatButton
        '
        Me.Name = "UFlatButton"

    End Sub

#End Region

#Region "Style"

    'This is the different Styles the button Can have

    Enum ButtonStyle
        Flat = 0                            'Do not popup
        Popup = 1                           'Popup when mouse goes over
        Normal = 3                          'Like normal button, always popup
        SingleBorder = 4                    'Flat with single border
        SingleBorderPopup = 5               'Popup with single border
    End Enum

    Private btnStyle As ButtonStyle = ButtonStyle.Normal    'Holds the Style State

    Property Style() As ButtonStyle
        Get
            Style = btnStyle                'Retrieve Style
        End Get
        Set(ByVal Value As ButtonStyle)
            btnStyle = Value                'Set the style
            Me.Invalidate()                 'Refresh the view
        End Set
    End Property

#End Region

#Region "Image Position"
    'Holds the code for the positioning of the image

    Enum ImagePosition
        'The different Positions

        Center = 0                          'Center and Stretch
        Left = 1                            'Left of button
        Top = 2                             'Ect
        Right = 3
        Bottom = 4
    End Enum

    Private imAlign As ImagePosition        'Image Align Postion

    Private imgRect As New RectangleF(0, 0, 0, 0)   'The rectangle were the image is drawn
    Private txtRect As New RectangleF(0, 0, 0, 0)   'The String Text Region
    Private imgSize As New Size(32, 32)             'The Size of the Image to draw.

    Property ImageAlign() As ImagePosition          'Image Align Property
        Get
            ImageAlign = imAlign
        End Get
        Set(ByVal Value As ImagePosition)
            imAlign = Value
            SetImagePosition()                      'Call the SetImagePosition Sub
            Me.Invalidate()                         'Refresh the View
        End Set
    End Property

    Property ImageSize() As Size                    'Set the Image Size Property
        Get
            ImageSize = imgSize
        End Get
        Set(ByVal Value As Size)
            imgSize = Value
            SetImagePosition()                      'Call the SetImagePosition Sub
            Me.Invalidate()                         'Refresh the Button
        End Set
    End Property

    Private Sub SetImagePosition()
        'This sub has nothing to do with Theory
        'I only calculted the regions where the image and Caption must be drawn
        'After a long time I came up with this

        Select Case ImageAlign
            Case ImagePosition.Center
                imgRect = New RectangleF(3, 3, Me.Width - 6, Me.Height - 6)
                txtRect = imgRect
            Case ImagePosition.Top
                imgRect = New RectangleF((Me.Width - imgSize.Width) \ 2, 3, imgSize.Width, imgSize.Height)
                txtRect = New RectangleF(3, imgSize.Height + 3, Me.Width - 6, (Me.Height - imgSize.Height) - 6)
            Case ImagePosition.Bottom
                imgRect = New RectangleF((Me.Width - imgSize.Width) \ 2, (Me.Height - imgSize.Height) - 3, imgSize.Width, imgSize.Height)
                txtRect = New RectangleF(3, 3, Me.Width - 6, (Me.Height - imgSize.Height) - 6)
            Case ImagePosition.Right
                imgRect = New RectangleF((Me.Width - imgSize.Width) - 3, (Me.Height - imgSize.Height) \ 2, imgSize.Width, imgSize.Height)
                txtRect = New RectangleF(3, 3, (Me.Width - imgSize.Width) - 6, Me.Height - 6)
            Case Else
                imgRect = New RectangleF(3, (Me.Height - imgSize.Height) \ 2, imgSize.Width, imgSize.Height)
                txtRect = New RectangleF(imgSize.Width + 3, 3, (Me.Width - imgSize.Width) - 6, Me.Height - 6)
        End Select
    End Sub

#End Region

#Region "Focus"

    'Saves the focus state

    Private blnFocus As Boolean = False

    Private Sub UFlatButton_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.GotFocus
        blnFocus = True
        Me.Invalidate()
    End Sub

    Private Sub UFlatButton_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.LostFocus
        blnFocus = False
        Me.Invalidate()
    End Sub

#End Region

#Region "DialogResult"

    'property to use with your form

    Private dlgResult As DialogResult = DialogResult.None

    Property DialogResult() As DialogResult
        Get
            DialogResult = dlgResult
        End Get
        Set(ByVal Value As DialogResult)
            dlgResult = Value
        End Set
    End Property

#End Region

#Region "Image and Caption"

    Private imgImage As Image               'Changing Image acording to Contras
    Private imgGrey As Image                'Grey version of image
    Private imgOriginal As Image            'The original Image

    Private strText As String               'Caption

    Public Property Caption() As String
        Get
            Caption = strText
        End Get
        Set(ByVal Value As String)
            strText = Value
            Me.Invalidate()                 'Refresh View
        End Set
    End Property

    Property Image() As Image
        Get
            Image = imgOriginal
        End Get
        Set(ByVal Value As Image)
            imgOriginal = Value             'Set Original Image
            imgImage = GetImage(Value)      'Get Image with Contras
            imgGrey = GetGrayImage(Value)   'Get Gray Image
            Me.Invalidate()                 'Refresh View

        End Set
    End Property

#End Region

#Region "Get Alternative Color"

    Private Function BestColor(ByVal col As Integer) As Integer
        If col > 128 Then                       'if component is light
            col -= 50
        Else                                    'component is Dark
            col += 50
        End If
        Return col                              'return new component
    End Function

    Private Function MeColor() As Color
        'Return a color that is different than me.BackColor

        Return Color.FromArgb(40, BestColor(Me.BackColor.R), BestColor(Me.BackColor.G), BestColor(Me.BackColor.B))
    End Function

#End Region

#Region "Draw"

    'This wil be allot of code and I will try to explain it step by step
    '1. As you will see this sub has 3 arguments
    '       '1. g as graphics, so you can draw it on any surface
    '       '2. over, if the mouse goes over the button
    '       '3. clicked, if the mouse button is down

    Private Sub Draw(ByRef g As Graphics, ByVal over As Boolean, ByVal clicked As Boolean)

        'I am going to construct the round edge rectangle
        'First the X positions
        Dim intX() As Integer = {2, 0, 0, 2, Me.Width - 3, Me.Width - 1, Me.Width - 1, Me.Width - 3, 2}
        'Now the Y positions, note that this is the coordinants for the button
        Dim intY() As Integer = {Me.Height - 1, Me.Height - 3, 2, 0, 0, 2, Me.Height - 3, Me.Height - 1, Me.Height - 1}

        Dim intLoop As Integer                      'My Loop integer

        'This is the round rectangle path
        Dim gpath As New Drawing2D.GraphicsPath(Drawing2D.FillMode.Winding)

        'This is the top part of the rounded rectangle
        Dim tpath As New Drawing2D.GraphicsPath(Drawing2D.FillMode.Winding)

        'This is the bottom part of the rounded rectangle
        Dim bpath As New Drawing2D.GraphicsPath(Drawing2D.FillMode.Winding)

        'Add All the Coordinants to the Gpath graphicsPath
        For intLoop = 0 To 6
            gpath.AddLine(intX(intLoop), intY(intLoop), intX(intLoop + 1), intY(intLoop + 1))
        Next

        'Add only the first 5 coordinants to the Top Grahicspath
        For intLoop = 0 To 3
            tpath.AddLine(intX(intLoop) + 1, intY(intLoop) + 1, intX(intLoop + 1) + 1, intY(intLoop + 1) + 1)
        Next

        'Add only the last 5 coordinants to the bottom Graphics Path
        For intLoop = 4 To 7
            bpath.AddLine(intX(intLoop) - 1, intY(intLoop) - 1, intX(intLoop + 1) - 1, intY(intLoop + 1) - 1)
        Next

        'Close only the Whole Gpath Graphics Path
        gpath.CloseFigure()

        Try
            g.Clear(Me.Parent.BackColor)                        'Clear the background with the Parents Backcolor
            g.FillPath(New SolidBrush(Me.BackColor), gpath)     'Fill the GPath with Me.Backcolor
        Catch ex As Exception                                   'Error occurd
            g.Clear(Me.BackColor)                               'Clear Backcolor
        End Try

        Try                                                     'Try to Draw Apropiete Image

            If Me.Enabled Then
                g.DrawImage(imgImage, imgRect)                  'Draw Normal Image
            Else
                g.DrawImage(imgGrey, imgRect)                   'Draw Gray Image
            End If

        Catch
        End Try

        If blnFocus Then
            'if the control has focus then fill it with the alternative color
            g.FillPath(New SolidBrush(MeColor), gpath)
        End If

        If over And Me.Enabled Then                             'if the mouse is over and not Disabled


            Dim grect As New Rectangle(0, 0, Me.Width, Me.Height)   'Drawing Rectangle

            Dim gbrush As Drawing2D.LinearGradientBrush             ' Gradient Brush
            Dim tbrush As SolidBrush                                ' Top Brush
            Dim bbrush As SolidBrush                                ' Bottom Brush

            If clicked Then                                         'if mouse button clicked
                'set grdient brush to fill from Alpha 0 to 255
                gbrush = New Drawing2D.LinearGradientBrush(grect, Color.FromArgb(0, 100, 100, 100), Color.FromArgb(150, 255, 255, 255), 45)
                'Set Top brush to Black
                tbrush = New SolidBrush(Color.FromArgb(50, 0, 0, 0))
                'Set Bottom Brush to White
                bbrush = New SolidBrush(Color.FromArgb(150, 255, 255, 255))
            Else
                'set gradient brush to fill from Alpha 255 to 0
                gbrush = New Drawing2D.LinearGradientBrush(grect, Color.FromArgb(150, 255, 255, 255), Color.FromArgb(0, 255, 255, 255), 45)
                'Set Bottom brush to black
                bbrush = New SolidBrush(Color.FromArgb(50, 0, 0, 0))
                'Set Top brush to White
                tbrush = New SolidBrush(Color.FromArgb(150, 255, 255, 255))
            End If

            g.FillPath(gbrush, gpath)                       'Fill Gradient

        End If

        '   'if Popup and mouse must be over'        'Style = singlePopup and must be over'              'Style normal'           or clicked
        If ((Style = ButtonStyle.Popup And over) Or (Style = ButtonStyle.SingleBorderPopup And over) Or Style = ButtonStyle.Normal Or clicked) Then

            Dim tbrush As SolidBrush
            Dim bbrush As SolidBrush

            If clicked Then                                         'if mouse button clicked

                'Set Top brush to Black
                tbrush = New SolidBrush(Color.FromArgb(50, 0, 0, 0))
                'Set Bottom Brush to White
                bbrush = New SolidBrush(Color.FromArgb(150, 255, 255, 255))
            Else

                'Set Bottom brush to black
                bbrush = New SolidBrush(Color.FromArgb(50, 0, 0, 0))
                'Set Top brush to White
                tbrush = New SolidBrush(Color.FromArgb(150, 255, 255, 255))
            End If

            g.DrawPath(New Pen(tbrush, 3), tpath)               'Draw Top path
            g.DrawPath(New Pen(bbrush, 2), bpath)               'Draw Bottom Path

        End If

        If Style = ButtonStyle.SingleBorder Or over Or Style = ButtonStyle.Normal Or Style = ButtonStyle.SingleBorderPopup Then
            'Draw Grey border
            g.DrawPath(New Pen(Color.FromArgb(250, 130, 130, 115)), gpath)

        End If

        If blnFocus Then
            'draw black border
            g.DrawPath(New Pen(Color.FromArgb(255, 0, 0, 0)), gpath)

        End If

        If Caption <> "" Then

            'Set the string format to the centre
            Dim myformat As New StringFormat(StringFormatFlags.FitBlackBox)
            myformat.Alignment = StringAlignment.Center
            myformat.LineAlignment = StringAlignment.Center

            If Me.Enabled Then
                'Draw string in forecolor
                g.DrawString(Caption, Me.Font, New SolidBrush(Me.ForeColor), txtRect, myformat)

            Else
                'Draw string in Gray
                g.DrawString(Caption, Me.Font, New SolidBrush(Color.Gray), txtRect, myformat)
            End If


        End If

    End Sub

    Protected Overrides Sub OnPaintBackground(ByVal pevent As System.Windows.Forms.PaintEventArgs)
        'When the button is invalidated, or refreshed then it will draw the Sub DRAW
        Draw(pevent.Graphics, Over, False)

    End Sub

#End Region

#Region "Other"

    'The folowing code is neccesary only to make the control work nicer
    'but it needs it to work like a button


    Private Over As Boolean = False                 'Holds if mouse is over

    Private Sub UFlatButton_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.MouseEnter
        'Mouse Over
        Over = True
        Me.Invalidate()
    End Sub

    Private Sub UFlatButton_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.MouseLeave
        'Mouse not over
        Over = False
        Me.Invalidate()
    End Sub

    Private Sub UFlatButton_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        SetImagePosition()                          'Resize drawing rectangles
        Me.Invalidate()                             'Refresh
    End Sub

    Private Sub UFlatButton_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.EnabledChanged
        Me.Invalidate()                             'Refresh
    End Sub

    Private Sub UFlatButton_ContrastChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ContrastChanged
        imgImage = GetImage(imgOriginal)            'Get image with new contras
    End Sub

    Event ClickButton As EventHandler

    Private Sub UFlatButton_ClickStart(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ClickStart

        Draw(Me.CreateGraphics, Over, True)         'Draw Button

        Try
            Me.ParentForm.DialogResult = DialogResult   'Check DialogResult
            If DialogResult <> DialogResult.None Then
                Me.ParentForm.Close()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub UFlatButton_ClickRelease(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.ClickRelease

        Draw(Me.CreateGraphics, Over, False)            'Draw Button

        RaiseEvent ClickButton(sender, e)               'Click button

    End Sub

    Private Sub UFlatButton_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.StartClick()                             'Click Button
        End If
    End Sub

#End Region


    Private Sub UFlatButton_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''                This code was writen by Paul Salmon                    ''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Any enqueries? E-mail me at salmon@mail.co.za
'
' I Hope this code was any use to you
' When using this control remember to use the ClickButton event to run your code
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
