Public Class UBase
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tmrClick As System.Windows.Forms.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.tmrClick = New System.Windows.Forms.Timer(Me.components)
        '
        'tmrClick
        '
        Me.tmrClick.Interval = 200
        '
        'UBase
        '
        Me.Name = "UBase"
        Me.Size = New System.Drawing.Size(56, 48)

    End Sub

#End Region

#Region "Event Handlers I Created"

    Event ContrastChanged As System.EventHandler        'If the Contrast Changed
    Event ClickRelease As System.EventHandler           'When button must be restored
    Event ClickStart As System.EventHandler             'When button is presed

#End Region

#Region "Contras of the Color"

    'Simly what this is supose to do is that, if you retrieve
    'an image with getImage it will send an Image with the apropiet
    'contras of colors. Extreemly Useful if you have graystyle images
    'and want to change it to blue or something else.

    Private CR As Integer = 100                 'Contras of Red
    Private CG As Integer = 100                 'Contras of Green
    Private CB As Integer = 100                 'Contras of Blue

    Property ContrastRed() As Integer
        Get
            ContrastRed = CR                    'Retrieve Red persentage
        End Get
        Set(ByVal Value As Integer)
            If CR <> Value Then                 'If the Value Changed
                CR = Value
                Dim e As EventArgs
                RaiseEvent ContrastChanged(Me, e)  'Raise Contras Changed
                Me.Invalidate()                 'Redraw Everything
            End If
        End Set
    End Property

    Property ContrastGreen() As Integer
        Get
            ContrastGreen = CG                  'Retrieve Green persentage
        End Get
        Set(ByVal Value As Integer)
            If CG <> Value Then                 'If value Changed
                CG = Value
                Dim e As EventArgs
                RaiseEvent ContrastChanged(Me, e)   'Raise Contras Changed Event
                Me.Invalidate()                 'Redraw Everything
            End If
        End Set
    End Property

    Property ContrastBlue() As Integer
        Get
            ContrastBlue = CB                   'Retrieve Blue persentage
        End Get
        Set(ByVal Value As Integer)
            If CB <> Value Then                 'If value Changed
                CB = Value
                Dim e As EventArgs
                RaiseEvent ContrastChanged(Me, e)   'Raise Contras Event
                Me.Invalidate()                 'Redraw Everything
            End If
        End Set
    End Property

    Public Sub SetContras(ByVal r As Integer, ByVal g As Integer, ByVal b As Integer)
        'Set the different contras and raise Contras Changed Event
        'This is useful if you don't want to change all the properties
        'every time

        CR = r
        CG = g
        CB = b
        Dim e As EventArgs
        RaiseEvent ContrastChanged(Me, e)
        Me.Invalidate()
    End Sub

#End Region

#Region "Image Methods"

    Public Function GetImage(ByVal im As Bitmap) As Image
        'This retrieve the image with the apropiete contras 
        Try
            'If the image is not the Apropiet size
            If Not (im.Width > 0 And im.Height > 0) Then
                Return Nothing      'return nothing, theres no Image to return
            End If
        Catch                       'Could not mesure the bounds

            Return Nothing          'return nothing, theres no Image to return    
        End Try

        Dim intX, intY As Integer   'Positions of Pixels

        'Create a new Bitmap with the bounds
        Dim temp As New Bitmap(im.Size.Width, im.Size.Height)

        Dim tCol As Color           'Temp Color

        Dim r, g, b As Integer      'RGB values of pixel

        For intX = 0 To im.Size.Width - 1       'X position
            For intY = 0 To im.Size.Height - 1  'Y position

                tCol = im.GetPixel(intX, intY)  'Get Color for pixel

                r = tCol.R * CR \ 100           'Calculate r component
                If r > 255 Then                 'Can not be heigher than 255
                    r = 255
                End If
                g = tCol.G * CG \ 100           'Calculate g component
                If g > 255 Then                 'Can not be Heigher than 255
                    g = 255
                End If
                b = tCol.B * CB \ 100           'Calculate b component
                If b > 255 Then                 'Can not be Heigher than 255
                    b = 255
                End If

                'Set the new value to the Temp Bitmap
                temp.SetPixel(intX, intY, Color.FromArgb(tCol.A, r, g, b))
            Next
        Next

        Return temp     'Return the Temp Bitmap

    End Function

    Public Function GetGrayImage(ByVal im As Bitmap) As Image
        'This is very useful. What if your Control is Disabled
        'What if it only dislay an image, How are you going to know
        'if the control is Disabled

        Try
            'If image Excist
            If Not (im.Width > 0 And im.Height > 0) Then
                Return Nothing
            End If

            Dim intX, intY, av As Integer           'X and Y positions

            'Temp Image
            Dim temp As New Bitmap(im.Size.Width, im.Size.Height)

            Dim tCol As Color                       'Temp Color

            For intX = 0 To im.Size.Width - 1       'X positions
                For intY = 0 To im.Size.Height - 1  'Y Positions

                    tCol = im.GetPixel(intX, intY)  'Get Color from image

                    'Find the Heighest Color and set av to it
                    If tCol.G > tCol.R Then         'if Green > red
                        av = tCol.G
                        If tCol.G < tCol.B Then     'if Green < blue
                            av = tCol.B
                        End If
                    Else                            'Red is > Green
                        av = tCol.R
                        If tCol.B > tCol.R Then     'if blue > Red
                            av = tCol.B
                        End If
                    End If

                    If tCol.A < 80 Then             'if Alpha is < 80
                        'Set pixel with current alpha
                        temp.SetPixel(intX, intY, tCol.FromArgb(tCol.A, av, av, av))
                    Else
                        'Set pixel with alpha = 80
                        temp.SetPixel(intX, intY, tCol.FromArgb(80, av, av, av))
                    End If
                Next
            Next
            Return temp                             'Return Temp Bitmap
        Catch
            Return Nothing                          'Retun Nothing Bitmap does not excist
        End Try
    End Function

#End Region

#Region "Raise Events"

    Protected Sub StartClick()
        'Simulate the Click of the button
        'I use this when Enter is pressed while focus

        tmrClick.Enabled = True                 'Start Timer
        Dim e As New EventArgs
        RaiseEvent ClickStart(Me, e)            'Raise Event
    End Sub

    Private Sub tmrClick_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrClick.Tick
        'When timer is finished then Click is over

        tmrClick.Enabled = False                'Finish Timer
        RaiseEvent ClickRelease(sender, e)      'Raise Release
    End Sub

    Private Sub UBase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Click
        'When the User control is clicked on

        tmrClick.Enabled = True                 'Start Timer
        RaiseEvent ClickStart(sender, e)        'Raise StartClick event
    End Sub

#End Region

End Class

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''''                This code was writen by Paul Salmon                    ''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Any enqueries? E-mail me at salmon@mail.co.za
'
' See UFlatButton for an example how to Use this UserControl
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
