Public Class frmTest
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents UFlatButton1 As UFlatButton
    Friend WithEvents UFlatButton2 As UFlatButton
    Friend WithEvents UFlatButton3 As UFlatButton
    Friend WithEvents UFlatButton4 As UFlatButton
    Friend WithEvents btnClose As UFlatButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTest))
        Me.UFlatButton1 = New XPStyle.UFlatButton
        Me.UFlatButton2 = New XPStyle.UFlatButton
        Me.UFlatButton3 = New XPStyle.UFlatButton
        Me.UFlatButton4 = New XPStyle.UFlatButton
        Me.btnClose = New XPStyle.UFlatButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'UFlatButton1
        '
        Me.UFlatButton1.Caption = "Test"
        Me.UFlatButton1.ContrastBlue = 100
        Me.UFlatButton1.ContrastGreen = 100
        Me.UFlatButton1.ContrastRed = 100
        Me.UFlatButton1.DialogResult = System.Windows.Forms.DialogResult.None
        Me.UFlatButton1.Image = CType(resources.GetObject("UFlatButton1.Image"), System.Drawing.Image)
        Me.UFlatButton1.ImageAlign = XPStyle.UFlatButton.ImagePosition.Left
        Me.UFlatButton1.ImageSize = New System.Drawing.Size(24, 24)
        Me.UFlatButton1.Location = New System.Drawing.Point(96, 24)
        Me.UFlatButton1.Name = "UFlatButton1"
        Me.UFlatButton1.Size = New System.Drawing.Size(104, 32)
        Me.UFlatButton1.Style = XPStyle.UFlatButton.ButtonStyle.Normal
        Me.UFlatButton1.TabIndex = 0
        '
        'UFlatButton2
        '
        Me.UFlatButton2.Caption = "Test"
        Me.UFlatButton2.ContrastBlue = 150
        Me.UFlatButton2.ContrastGreen = 150
        Me.UFlatButton2.ContrastRed = 60
        Me.UFlatButton2.DialogResult = System.Windows.Forms.DialogResult.None
        Me.UFlatButton2.Image = CType(resources.GetObject("UFlatButton2.Image"), System.Drawing.Image)
        Me.UFlatButton2.ImageAlign = XPStyle.UFlatButton.ImagePosition.Left
        Me.UFlatButton2.ImageSize = New System.Drawing.Size(24, 24)
        Me.UFlatButton2.Location = New System.Drawing.Point(96, 64)
        Me.UFlatButton2.Name = "UFlatButton2"
        Me.UFlatButton2.Size = New System.Drawing.Size(104, 32)
        Me.UFlatButton2.Style = XPStyle.UFlatButton.ButtonStyle.Normal
        Me.UFlatButton2.TabIndex = 1
        '
        'UFlatButton3
        '
        Me.UFlatButton3.Caption = "Test"
        Me.UFlatButton3.ContrastBlue = 100
        Me.UFlatButton3.ContrastGreen = 100
        Me.UFlatButton3.ContrastRed = 100
        Me.UFlatButton3.DialogResult = System.Windows.Forms.DialogResult.None
        Me.UFlatButton3.Image = CType(resources.GetObject("UFlatButton3.Image"), System.Drawing.Image)
        Me.UFlatButton3.ImageAlign = XPStyle.UFlatButton.ImagePosition.Left
        Me.UFlatButton3.ImageSize = New System.Drawing.Size(30, 30)
        Me.UFlatButton3.Location = New System.Drawing.Point(96, 104)
        Me.UFlatButton3.Name = "UFlatButton3"
        Me.UFlatButton3.Size = New System.Drawing.Size(104, 32)
        Me.UFlatButton3.Style = XPStyle.UFlatButton.ButtonStyle.Normal
        Me.UFlatButton3.TabIndex = 2
        '
        'UFlatButton4
        '
        Me.UFlatButton4.Caption = "Test"
        Me.UFlatButton4.ContrastBlue = 100
        Me.UFlatButton4.ContrastGreen = 100
        Me.UFlatButton4.ContrastRed = 100
        Me.UFlatButton4.DialogResult = System.Windows.Forms.DialogResult.None
        Me.UFlatButton4.Image = CType(resources.GetObject("UFlatButton4.Image"), System.Drawing.Image)
        Me.UFlatButton4.ImageAlign = XPStyle.UFlatButton.ImagePosition.Left
        Me.UFlatButton4.ImageSize = New System.Drawing.Size(24, 24)
        Me.UFlatButton4.Location = New System.Drawing.Point(96, 144)
        Me.UFlatButton4.Name = "UFlatButton4"
        Me.UFlatButton4.Size = New System.Drawing.Size(104, 32)
        Me.UFlatButton4.Style = XPStyle.UFlatButton.ButtonStyle.Popup
        Me.UFlatButton4.TabIndex = 3
        '
        'btnClose
        '
        Me.btnClose.Caption = "Close"
        Me.btnClose.ContrastBlue = 100
        Me.btnClose.ContrastGreen = 100
        Me.btnClose.ContrastRed = 100
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ImageAlign = XPStyle.UFlatButton.ImagePosition.Left
        Me.btnClose.ImageSize = New System.Drawing.Size(24, 24)
        Me.btnClose.Location = New System.Drawing.Point(96, 184)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(104, 32)
        Me.btnClose.Style = XPStyle.UFlatButton.ButtonStyle.SingleBorderPopup
        Me.btnClose.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 24)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Normal"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(16, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 24)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Contras Changed"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(16, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 32)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Image Size Changed"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(16, 152)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 24)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Pupup"
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(16, 184)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 32)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Border Popup"
        '
        'frmTest
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.UFlatButton4)
        Me.Controls.Add(Me.UFlatButton3)
        Me.Controls.Add(Me.UFlatButton2)
        Me.Controls.Add(Me.UFlatButton1)
        Me.Name = "frmTest"
        Me.Text = "Test Run"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnClose_ClickButton(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.ClickButton
        Me.Close()
    End Sub
End Class
