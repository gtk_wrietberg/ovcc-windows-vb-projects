Imports System.Security.Principal
Imports System.Runtime.InteropServices
Imports System.Text

Public Class Impersonation

    'These constants are based on values in the Win32 API file WINBASE.H
    'Refer to http://msdn.microsoft.com/en-us/library/aa378184(VS.85).aspx for their meanings
    Const LOGON32_LOGON_INTERACTIVE As Integer = 2
    Const LOGON32_PROVIDER_DEFAULT As Integer = 0

    Enum SID_NAME_USE
        SidTypeUser = 1
        SidTypeGroup
        SidTypeDomain
        SidTypeAlias
        SidTypeWellKnownGroup
        SidTypeDeletedAccount
        SidTypeInvalid
        SidTypeUnknown
        SidTypeComputer
    End Enum

    'Private Declare Function LogonUserA Lib "advapi32.dll" (ByVal lpszUsername As String, _
    '                        ByVal lpszDomain As String, _
    '                        ByVal lpszPassword As String, _
    '                        ByVal dwLogonType As Integer, _
    '                        ByVal dwLogonProvider As Integer, _
    '                        ByRef phToken As IntPtr) As Integer

    'Private Declare Auto Function DuplicateToken Lib "advapi32.dll" ( _
    '                        ByVal ExistingTokenHandle As IntPtr, _
    '                        ByVal ImpersonationLevel As SECURITY_IMPERSONATION_LEVEL, _
    '                        ByRef DuplicateTokenHandle As IntPtr) As Integer

    <DllImport("advapi32.dll", SetLastError:=True)> _
    Private Shared Function LogonUserA(ByVal lpszUsername As String, ByVal lpszDomain As String, ByVal lpszPassword As String, ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Integer
    End Function

    <DllImport("advapi32.dll", SetLastError:=True)> _
    Private Shared Function DuplicateToken(ByVal ExistingTokenHandle As IntPtr, ByVal ImpersonationLevel As SECURITY_IMPERSONATION_LEVEL, ByRef DuplicateTokenHandle As IntPtr) As Integer
    End Function

    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
    Private Shared Function LookupAccountName(lpSystemName As String, lpAccountName As String, <MarshalAs(UnmanagedType.LPArray)> Sid As Byte(), ByRef cbSid As UInteger, ReferencedDomainName As StringBuilder, ByRef cchReferencedDomainName As UInteger, ByRef peUse As SID_NAME_USE) As Boolean
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Auto, SetLastError:=True)> _
    Private Shared Function ConvertSidToStringSid(<MarshalAs(UnmanagedType.LPArray)> pSID As Byte(), ByRef ptrSid As IntPtr) As Boolean
    End Function

    <DllImport("kernel32.dll")> _
    Private Shared Function LocalFree(hMem As IntPtr) As IntPtr
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)> _
    Private Shared Function CloseHandle(ByVal handle As IntPtr) As Long
    End Function

    Private Enum SECURITY_IMPERSONATION_LEVEL As Integer
        SecurityAnonymous = 0
        SecurityIdentification = 1
        SecurityImpersonation = 2
        SecurityDelegation = 3
    End Enum

    Private m_oImpersonationContext As WindowsImpersonationContext
    Private m_bImpersonationActive As Boolean

    Public Sub New()

    End Sub

    Public Function GetSidFromUsername(UserName As String) As String
        Dim ret As Boolean
        Dim ptrSid As Byte()
        Dim cbSid As Integer
        Dim ptrSidString As IntPtr
        Dim SidString As String = ""
        Dim refDomainName As New StringBuilder
        Dim cbRefDomainName As Integer
        Dim peUse As SID_NAME_USE

        Try

            'First get the buffer sizes
            ret = LookupAccountName(Nothing, UserName, Nothing, cbSid, Nothing, cbRefDomainName, peUse)

            'Adjust the buffers to the needed size
            'ptrSid = Marshal.AllocHGlobal(cbSid)
            ptrSid = New Byte(cbSid - 1) {}
            refDomainName.EnsureCapacity(cbRefDomainName)

            'Get the data again, now with the changed buffers
            ret = LookupAccountName(Nothing, UserName, ptrSid, cbSid, refDomainName, cbRefDomainName, peUse)

            If ret Then
                'If return doesn't fail (ret=true) then convert the buffer data at ptrSid to the SidString)
                If ConvertSidToStringSid(ptrSid, ptrSidString) Then

                    'Since ptrSidString is a memory pointer we need to make it a string
                    SidString = Marshal.PtrToStringAuto(ptrSidString)

                    'Finally free up the memory
                    LocalFree(ptrSidString)

                End If
            End If

            'Marshal.FreeHGlobal(ptrSid)
        Catch ex As Exception
            SidString = "ERROR: " & ex.Message
        End Try

        Return SidString
    End Function

    Public ReadOnly Property ImpersonationActive() As Boolean
        Get
            Return m_bImpersonationActive
        End Get
    End Property

    Public Function StartImpersonation(ByVal sUserName As String, ByVal sDomain As String, ByVal sPassword As String) As Boolean
        Dim bResults As Boolean = False
        Dim sErrorMessage As String
        Dim oWindowsIdentity As WindowsIdentity
        Dim hPrimaryToken As IntPtr = IntPtr.Zero           'a Win32 handle to our authentication token
        Dim hImpersonationToken As IntPtr = IntPtr.Zero     'a Win32 handle to our impersonation token

        If String.IsNullOrEmpty(sUserName) Then
            Throw New ArgumentException("UserName may not be NULL or String.Empty")
        End If

        'If no domain is given, assume the account is a local one
        If sDomain = String.Empty Then
            sDomain = Environment.MachineName
        End If

        Try
            'Validate the provided userid, password and domain.
            If LogonUserA(sUserName, sDomain, sPassword, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, hPrimaryToken) <> 0 Then

                'Convert our token to one whos handle has TOKEN_IMPERSONATE set
                If DuplicateToken(hPrimaryToken, SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation, hImpersonationToken) <> 0 Then

                    'Create a new identity object based on our impersonation token
                    oWindowsIdentity = New WindowsIdentity(hImpersonationToken)

                    'Switch to our new identity
                    m_oImpersonationContext = oWindowsIdentity.Impersonate()
                    If m_oImpersonationContext IsNot Nothing Then
                        m_bImpersonationActive = True
                        bResults = True
                    End If
                Else
                    sErrorMessage = String.Format("DuplicateToken failed (rc={0})", Runtime.InteropServices.Marshal.GetLastWin32Error)
                    Throw New Security.Authentication.AuthenticationException(sErrorMessage)
                End If
            Else
                sErrorMessage = String.Format("LogonUser failed (rc={0})", Runtime.InteropServices.Marshal.GetLastWin32Error)
                Throw New Security.Authentication.AuthenticationException(sErrorMessage)
            End If

        Finally
            If Not hImpersonationToken.Equals(IntPtr.Zero) Then
                CloseHandle(hImpersonationToken)
                hImpersonationToken = IntPtr.Zero
            End If
            If Not hPrimaryToken.Equals(IntPtr.Zero) Then
                CloseHandle(hPrimaryToken)
                hPrimaryToken = IntPtr.Zero
            End If
        End Try

        Return bResults
    End Function

    Public Sub EndImpersonation()
        If m_oImpersonationContext IsNot Nothing AndAlso m_bImpersonationActive Then
            m_oImpersonationContext.Undo()
            m_oImpersonationContext.Dispose()
            m_oImpersonationContext = Nothing
            m_bImpersonationActive = False
        End If
    End Sub
End Class