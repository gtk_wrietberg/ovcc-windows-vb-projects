Module Main
    Private c_REGKEY_OFFICESIGNIN As String = "Software\\Microsoft\\Office\\15.0\\Common\\SignIn"
    Private c_REGVALUE_OFFICESIGNIN As String = "SignInOptions"

    Public Sub Main()
        Try
            Dim oImpersonate As Impersonation

            oImpersonate = New Impersonation
            'oImpersonate.StartImpersonation("sitekiosk", String.Empty, "Provis10")


            MsgBox("Sitekiosk: " & oImpersonate.GetSidFromUsername("sitekiosk"))

            'Environment.Exit(0)


            oImpersonate.StartImpersonation("sitekiosk", String.Empty, "Provisi0")

            MsgBox("User = " & Environment.UserName)



            Dim regOfficeSignIn As Microsoft.Win32.RegistryKey
            regOfficeSignIn = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(c_REGKEY_OFFICESIGNIN, True)
            If regOfficeSignIn Is Nothing Then
                MsgBox("regOfficeSignIn not found, creating ")
                regOfficeSignIn = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(c_REGKEY_OFFICESIGNIN, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree)
            End If



            If regOfficeSignIn.GetValue("SignInOptions", Nothing) Is Nothing Then
                MsgBox("Value does not exist.")
            End If

            'Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\Software\Microsoft\Office\15.0\Common\SignIn", "SignInOptions", 4)

            regOfficeSignIn.SetValue(c_REGVALUE_OFFICESIGNIN, 4, Microsoft.Win32.RegistryValueKind.DWord)

            MsgBox("User = " & Environment.UserName)

            oImpersonate.EndImpersonation()
            MsgBox("User = " & Environment.UserName)

        Catch ex As Exception
            MsgBox("ERROR: " & ex.Message)
        End Try

        Environment.Exit(0)

    End Sub

End Module
