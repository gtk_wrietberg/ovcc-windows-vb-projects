

Module Main
    Public Sub Main()
        Dim oImpersonate As Impersonation

        oImpersonate = New Impersonation
        oImpersonate.StartImpersonation("sitekiosk", String.Empty, "Provis10")

        Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\TEST_KEY", "TEST_VALUE", "YES")

        MsgBox("User = " & Environment.UserName)

        oImpersonate.EndImpersonation()
        MsgBox("User = " & Environment.UserName)

        Environment.Exit(0)

    End Sub

End Module
