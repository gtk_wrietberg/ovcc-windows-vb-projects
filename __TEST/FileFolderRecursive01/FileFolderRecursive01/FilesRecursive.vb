Public Class FilesRecursive
    Private mPath As String
    Private mFiles As ArrayList
    Private mInternalCount As Integer

    Public Sub New()
        mFiles = New ArrayList
        mFiles.Clear()

        mPath = ""

        mInternalCount = 0
    End Sub

    Public Property Path() As String
        Get
            Return mPath
        End Get
        Set(ByVal value As String)
            mPath = value
        End Set
    End Property

    Public ReadOnly Property Files() As ArrayList
        Get
            Return mFiles
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Return mFiles.Count
        End Get
    End Property

    Public Sub Reset()
        mInternalCount = 0
    End Sub

    Public Function GetNext(ByRef sPath As String) As Boolean
        If mInternalCount >= mFiles.Count Or mInternalCount < 0 Then
            sPath = ""

            Return False
        End If

        Try
            sPath = mFiles.Item(mInternalCount)

            mInternalCount += 1

            Return True
        Catch ex As Exception
            sPath = ""

            Return False
        End Try
    End Function

    Public Function FindFiles() As Boolean
        If Not IO.Directory.Exists(mPath) Then
            Return False
        End If

        Return RecursiveSearch(mPath)
    End Function

    Private Function RecursiveSearch(ByVal sPath As String) As Boolean
        Dim dirInfo As New IO.DirectoryInfo(sPath)
        Dim fileObject As IO.FileSystemInfo

        For Each fileObject In dirInfo.GetFileSystemInfos()
            If fileObject.Attributes = IO.FileAttributes.Directory Then
                RecursiveSearch(fileObject.FullName)
            Else
                mFiles.Add(fileObject.FullName)
            End If
        Next

        Return True
    End Function

End Class
