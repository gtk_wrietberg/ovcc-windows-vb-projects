﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TranslucentButton3 = New Zeros.Windows.Forms.TranslucentControls.TranslucentButton()
        Me.TranslucentButton2 = New Zeros.Windows.Forms.TranslucentControls.TranslucentButton()
        Me.TranslucentButton1 = New Zeros.Windows.Forms.TranslucentControls.TranslucentButton()
        Me.TranslucentPanel1 = New Zeros.Windows.Forms.TranslucentControls.TranslucentPanel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TabPage3.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(601, 310)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "TabPage1"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Translucent TabPage"
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(192, 74)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "TabPage2"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Label4)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(225, 310)
        Me.TabPage5.TabIndex = 0
        Me.TabPage5.Text = "TabPage1"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(110, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Translucent TabPage"
        '
        'TabPage6
        '
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(192, 74)
        Me.TabPage6.TabIndex = 1
        Me.TabPage6.Text = "TabPage2"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'TabPage11
        '
        Me.TabPage11.Location = New System.Drawing.Point(4, 22)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage11.Size = New System.Drawing.Size(446, 136)
        Me.TabPage11.TabIndex = 0
        Me.TabPage11.Text = "TabPage9"
        Me.TabPage11.UseVisualStyleBackColor = True
        '
        'TabPage12
        '
        Me.TabPage12.Location = New System.Drawing.Point(4, 22)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage12.Size = New System.Drawing.Size(446, 196)
        Me.TabPage12.TabIndex = 1
        Me.TabPage12.Text = "TabPage10"
        Me.TabPage12.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(287, 136)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage9"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(287, 136)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage10"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage9
        '
        Me.TabPage9.Location = New System.Drawing.Point(4, 22)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(434, 343)
        Me.TabPage9.TabIndex = 0
        Me.TabPage9.Text = "TabPage7"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'TabPage10
        '
        Me.TabPage10.Location = New System.Drawing.Point(4, 22)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(434, 343)
        Me.TabPage10.TabIndex = 1
        Me.TabPage10.Text = "TabPage8"
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(12, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(268, 78)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Standard Button Control"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(8, 216)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(463, 20)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Translucent Controls Demo - www.ZerosAndTheOne.com"
        '
        'TranslucentButton3
        '
        Me.TranslucentButton3.BackgroundImage = CType(resources.GetObject("TranslucentButton3.BackgroundImage"), System.Drawing.Image)
        Me.TranslucentButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.TranslucentButton3.BackgroundImageSource = Zeros.Windows.Forms.TranslucentControls.ITranslucentControl.eBackgroundImageSource.Form
        Me.TranslucentButton3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TranslucentButton3.Gamma = 0.15!
        Me.TranslucentButton3.IsTranslucent = True
        Me.TranslucentButton3.Location = New System.Drawing.Point(286, 96)
        Me.TranslucentButton3.Name = "TranslucentButton3"
        Me.TranslucentButton3.Offset = New System.Drawing.Point(5, 5)
        Me.TranslucentButton3.Size = New System.Drawing.Size(268, 79)
        Me.TranslucentButton3.TabIndex = 3
        Me.TranslucentButton3.Text = "Translucent Button Control" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "IsTranslucent = True" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Gamma = 0.15"
        Me.TranslucentButton3.UseVisualStyleBackColor = True
        '
        'TranslucentButton2
        '
        Me.TranslucentButton2.BackgroundImage = CType(resources.GetObject("TranslucentButton2.BackgroundImage"), System.Drawing.Image)
        Me.TranslucentButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.TranslucentButton2.BackgroundImageSource = Zeros.Windows.Forms.TranslucentControls.ITranslucentControl.eBackgroundImageSource.Form
        Me.TranslucentButton2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TranslucentButton2.ForeColor = System.Drawing.Color.White
        Me.TranslucentButton2.Gamma = 1.0!
        Me.TranslucentButton2.IsTranslucent = True
        Me.TranslucentButton2.Location = New System.Drawing.Point(12, 96)
        Me.TranslucentButton2.Name = "TranslucentButton2"
        Me.TranslucentButton2.Offset = New System.Drawing.Point(5, 5)
        Me.TranslucentButton2.Size = New System.Drawing.Size(268, 79)
        Me.TranslucentButton2.TabIndex = 2
        Me.TranslucentButton2.Text = "Translucent Button Control" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "IsTranslucent = True" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Gamma = 1.0"
        Me.TranslucentButton2.UseVisualStyleBackColor = True
        '
        'TranslucentButton1
        '
        Me.TranslucentButton1.BackgroundImageSource = Zeros.Windows.Forms.TranslucentControls.ITranslucentControl.eBackgroundImageSource.Form
        Me.TranslucentButton1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TranslucentButton1.Gamma = 1.0!
        Me.TranslucentButton1.IsTranslucent = False
        Me.TranslucentButton1.Location = New System.Drawing.Point(286, 12)
        Me.TranslucentButton1.Name = "TranslucentButton1"
        Me.TranslucentButton1.Offset = New System.Drawing.Point(5, 5)
        Me.TranslucentButton1.Size = New System.Drawing.Size(268, 79)
        Me.TranslucentButton1.TabIndex = 1
        Me.TranslucentButton1.Text = "Translucent Button Control" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "IsTranslucent = False"
        Me.TranslucentButton1.UseVisualStyleBackColor = True
        '
        'TranslucentPanel1
        '
        Me.TranslucentPanel1.BackgroundImage = CType(resources.GetObject("TranslucentPanel1.BackgroundImage"), System.Drawing.Image)
        Me.TranslucentPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.TranslucentPanel1.BackgroundImageSource = Zeros.Windows.Forms.TranslucentControls.ITranslucentControl.eBackgroundImageSource.Form
        Me.TranslucentPanel1.Gamma = 0.3!
        Me.TranslucentPanel1.IsTranslucent = True
        Me.TranslucentPanel1.Location = New System.Drawing.Point(409, 283)
        Me.TranslucentPanel1.Name = "TranslucentPanel1"
        Me.TranslucentPanel1.Offset = New System.Drawing.Point(5, 5)
        Me.TranslucentPanel1.Size = New System.Drawing.Size(277, 172)
        Me.TranslucentPanel1.TabIndex = 5
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(83, 42)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(658, 354)
        Me.TextBox1.TabIndex = 6
        Me.TextBox1.Text = resources.GetString("TextBox1.Text")
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.TranslucentControlsTestBed.My.Resources.Resources.lake
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(923, 512)
        Me.Controls.Add(Me.TranslucentPanel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TranslucentButton3)
        Me.Controls.Add(Me.TranslucentButton2)
        Me.Controls.Add(Me.TranslucentButton1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox1)
        Me.DoubleBuffered = True
        Me.Name = "Form1"
        Me.Text = "Translucent Controls - ZerosAndTheOne.com - Matthew Kleinwaks - MSMVP"
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage11 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage12 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TranslucentButton1 As Zeros.Windows.Forms.TranslucentControls.TranslucentButton
    Friend WithEvents TranslucentButton2 As Zeros.Windows.Forms.TranslucentControls.TranslucentButton
    Friend WithEvents TranslucentButton3 As Zeros.Windows.Forms.TranslucentControls.TranslucentButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TranslucentPanel1 As Zeros.Windows.Forms.TranslucentControls.TranslucentPanel
    Friend WithEvents TextBox1 As TextBox
End Class
