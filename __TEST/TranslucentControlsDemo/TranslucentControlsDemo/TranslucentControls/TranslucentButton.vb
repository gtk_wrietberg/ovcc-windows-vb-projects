﻿Imports System.Windows.Forms
Imports System.Drawing

Public Class TranslucentButton
    Inherits System.Windows.Forms.Button
    Implements ITranslucentControl

#Region "CONSTRUCTOR(S)"
    Public Sub New()
        'SET DOUBLE BUFFERING TO REDUCE FLICKER WHERE POSSIBLE
        MyBase.DoubleBuffered = True
    End Sub
#End Region

#Region "PRIVATE FIELDS"
    'FLAG USED TO BYPASS REPAINTING AFTER THE BACKGROUND PICTURE IS SET
    Private _bypassPainting As Boolean = False
#End Region

#Region "PUBLIC PROPERTIES AND ASSOCIATED PRIVATE FIELDS"

    Private _gamma As Single = 1.0
    ''' <summary>
    ''' Gets or Sets the gamma correction to use for the translucent image background.
    ''' </summary>
    ''' <value>Single</value>
    ''' <returns>Value (as single) for the gamma correction</returns>
    ''' <remarks>None</remarks>
    Public Property Gamma() As Single _
                            Implements ITranslucentControl.Gamma
        Get
            Return _gamma
        End Get
        Set(ByVal value As Single)
            If value < 0.01 Then value = 0.01
            _gamma = value
        End Set
    End Property

    Private _isTranslucent As Boolean = False
    ''' <summary>
    ''' Indicates if control will be drawn translucent. False by default.
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>True if control is translucent, otherwise False.</returns>
    ''' <remarks>None</remarks>
    Public Property IsTranslucent() As Boolean _
                                    Implements ITranslucentControl.IsTranslucent
        Get
            Return _isTranslucent
        End Get
        Set(ByVal value As Boolean)
            If value <> _isTranslucent AndAlso value = False Then
                Me.BackgroundImage.Dispose()
                Me.BackgroundImage = Nothing
            End If
            _isTranslucent = value
        End Set
    End Property

    Private _Offset As New Point(5, 5)
    ''' <summary>
    ''' Provides an offset value incase the image is not correctly aligning in this control. 
    ''' This can occur when special using a 3d border, or something similar that would cause an offset
    ''' </summary>
    ''' <value>Point. Default value is 5, 5 for this control</value>
    ''' <returns>Current offset for this controls background drawing</returns>
    ''' <remarks>None</remarks>
    Public Property Offset() As Point _
                            Implements ITranslucentControl.Offset
        Get
            Return _Offset
        End Get
        Set(ByVal value As Point)
            _Offset = value
        End Set
    End Property

    Private _backgroundImageSource As ITranslucentControl.eBackgroundImageSource = ITranslucentControl.eBackgroundImageSource.Form
    ''' <summary>
    ''' Gets/Sets the source of the background image for this control
    ''' </summary>
    ''' <value>eBackgroundImageSource enumeration value</value>
    ''' <returns>eBackgroundImageSource.Form when the forms background should be used
    ''' eBackgroundImageSource.Parent when the forms immediate parent should be used
    ''' </returns>
    ''' <remarks>This setting is ignored when IsTranslucent is False</remarks>
    Public Property BackgroundImageSource() As ITranslucentControl.eBackgroundImageSource _
                                            Implements ITranslucentControl.BackgroundImageSource
        Get
            Return _backgroundImageSource
        End Get
        Set(ByVal value As ITranslucentControl.eBackgroundImageSource)
            _backgroundImageSource = value
        End Set
    End Property

#End Region

#Region "CUSTOM PAINTING ROUTINE"
    Protected Overrides Sub OnPaint(ByVal pevent As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(pevent)

        'IF BYPASSPAINTING FLAG WAS SET, RESET IT AND DO NOT PAINT
        If _bypassPainting Then
            _bypassPainting = False
            Return
        End If

        'DON'T DO ANY CUSTOM PAINTING IF THE FEATURE IS DISABLED
        If Not _isTranslucent Then Return

        'GET THE CONTROL THAT THE IMAGE IS ON WE WANT TO USE FOR TRANSLUCENCY
        Dim parentControl As Control = Nothing
        If _backgroundImageSource = ITranslucentControl.eBackgroundImageSource.Form Then
            'GET FORM
            parentControl = Me.FindForm
        Else
            'GET PARENT (WILL BE FORM IF NO CONTAINER EXISTS)
            parentControl = Me.Parent
        End If

        'IF THE PARENT CONTROL WE ARE REFERNCING HAS NO BACKGROUND IMAGE, DO NOTHING
        'ALSO CHECK TO MAKE SURE STRETCH IS THE LAYOUT TYPE
        If parentControl.BackgroundImage Is Nothing Then Return
        If parentControl.BackgroundImageLayout <> ImageLayout.Stretch Then Return

        'SOURCE RECTANGLE IS THE CLIPPED REGION OF THE FORMS BACKGROUND IMAGE
        'THAT IS BEHIND THE TABPAGE. CLIPPING ALLOWS US TO THEN PASTE THE COVERED PART
        'OF THE FORM BACKGROUND IMAGE ONTO THE LISTVIEW BACKGROUND TO MIMIC TRANSPARENCY
        Dim srcRect As Rectangle = Nothing

        If _backgroundImageSource = ITranslucentControl.eBackgroundImageSource.Form Then
            srcRect = New Rectangle(GetPointFromForm(Me), Me.Size)
        Else
            srcRect = Me.Bounds
        End If

        'IMAGE WITH THE CURRENT CLIENT SIZE OF THE BACKGROUND
        Dim mySourceImage As New Bitmap(parentControl.BackgroundImage, _
                                        parentControl.ClientSize.Width, _
                                        parentControl.ClientSize.Height)

        'BLANK IMAGE WE WANT TO DRAW THE SECTION TO THAT WE WILL DISPLAY IN THE LISTVIEW
        Dim myButtonImage As New Bitmap(srcRect.Width, srcRect.Height)

        'CREATE A GRAPHICS OBJECT FROM THE IMAGE
        Dim g As Graphics = Graphics.FromImage(myButtonImage)

        'IMAGE ATTRIBUTES SO WE CAN SET GAMMA (TO MAKE IMAGE LIGHTER)
        Dim image_attr As New Drawing.Imaging.ImageAttributes
        image_attr.SetGamma(_gamma)

        'DRAW CROPPED AND LIGHTENED IMAGE TO THE GRAPHICS OBJECT (WRITES TO myListViewImage OBJECT)
        g.DrawImage(mySourceImage, _
                    Me.ClientRectangle, _
                    (srcRect.X + _Offset.X), _
                    (srcRect.Y + _Offset.Y), _
                    srcRect.Width, _
                    srcRect.Height, _
                    GraphicsUnit.Pixel, _
                    image_attr)

        image_attr.Dispose()
        g.Dispose()
        image_attr = Nothing
        g = Nothing

        'BEFORE WE SET THE BACKGROUND IMAGE, SET THE BYPASS FLAG
        'SO WE DONT GET STUCK IN A PAINTING LOOP
        _bypassPainting = True

        Me.BackgroundImageLayout = ImageLayout.None
        Me.BackgroundImage = myButtonImage

    End Sub
#End Region

#Region "SUPPORT METHODS"
    Private Function GetPointFromForm(ByVal C As Control) As Point
        Try
            Return C.FindForm.PointToClient(C.Parent.PointToScreen(C.Location))
        Catch ex As Exception
            Return New Point(0, 0)
        End Try
    End Function
#End Region

End Class
