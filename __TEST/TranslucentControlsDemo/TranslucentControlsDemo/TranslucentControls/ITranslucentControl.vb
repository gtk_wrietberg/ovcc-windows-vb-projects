﻿'INTERFACE THAT ALL TRANSLUCENT CONTROLS WILL IMPLEMENT
Public Interface ITranslucentControl

    Enum eBackgroundImageSource
        Form = 0
        Container = 1
    End Enum

    Property Gamma() As Single
    Property IsTranslucent() As Boolean
    Property Offset() As System.Drawing.Point
    Property BackgroundImageSource() As eBackgroundImageSource

End Interface