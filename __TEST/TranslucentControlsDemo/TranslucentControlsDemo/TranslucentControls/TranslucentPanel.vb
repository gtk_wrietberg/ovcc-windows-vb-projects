﻿Imports System.Windows.Forms
Imports System.Drawing

Public Class TranslucentPanel
    Inherits System.Windows.Forms.Panel
    Implements ITranslucentControl

#Region "CONSTRUCTOR(S)"
    Public Sub New()
        'SET DOUBLE BUFFERING TO REDUCE FLICKER WHERE POSSIBLE
        MyBase.DoubleBuffered = True
    End Sub
#End Region

#Region "PRIVATE FIELDS"
    'FLAG USED TO BYPASS REPAINTING AFTER THE BACKGROUND PICTURE IS SET
    Private _bypassPainting As Boolean = False
#End Region

#Region "PUBLIC PROPERTIES AND ASSOCIATED PRIVATE FIELDS"

    Private _gamma As Single = 1.0
    ''' <summary>
    ''' Gets or Sets the gamma correction to use for the translucent image background.
    ''' </summary>
    ''' <value>Single</value>
    ''' <returns>Value (as single) for the gamma correction</returns>
    ''' <remarks>None</remarks>
    Public Property Gamma() As Single _
                            Implements ITranslucentControl.Gamma
        Get
            Return _gamma
        End Get
        Set(ByVal value As Single)
            If value < 0.01 Then value = 0.01
            _gamma = value
        End Set
    End Property

    Private _isTranslucent As Boolean = False
    ''' <summary>
    ''' Indicates if control will be drawn translucent. False by default.
    ''' </summary>
    ''' <value>Boolean</value>
    ''' <returns>True if control is translucent, otherwise False.</returns>
    ''' <remarks>None</remarks>
    Public Property IsTranslucent() As Boolean _
                                    Implements ITranslucentControl.IsTranslucent
        Get
            Return _isTranslucent
        End Get
        Set(ByVal value As Boolean)
            If value <> _isTranslucent AndAlso value = False Then
                Me.BackgroundImage.Dispose()
                Me.BackgroundImage = Nothing
            End If
            _isTranslucent = value
        End Set
    End Property

    Private _Offset As New Point(5, 5)
    ''' <summary>
    ''' Provides an offset value incase the image is not correctly aligning in this control. 
    ''' This can occur when special using a 3d border, or something similar that would cause an offset
    ''' </summary>
    ''' <value>Point. Default value is 5, 5 for this control</value>
    ''' <returns>Current offset for this controls background drawing</returns>
    ''' <remarks>None</remarks>
    Public Property Offset() As Point _
                            Implements ITranslucentControl.Offset
        Get
            Return _Offset
        End Get
        Set(ByVal value As Point)
            _Offset = value
        End Set
    End Property

    Private _backgroundImageSource As ITranslucentControl.eBackgroundImageSource = ITranslucentControl.eBackgroundImageSource.Form
    ''' <summary>
    ''' Gets/Sets the source of the background image for this control
    ''' </summary>
    ''' <value>eBackgroundImageSource enumeration value</value>
    ''' <returns>eBackgroundImageSource.Form when the forms background should be used
    ''' eBackgroundImageSource.Parent when the forms immediate parent should be used
    ''' </returns>
    ''' <remarks>This setting is ignored when IsTranslucent is False</remarks>
    Public Property BackgroundImageSource() As ITranslucentControl.eBackgroundImageSource _
                                            Implements ITranslucentControl.BackgroundImageSource
        Get
            Return _backgroundImageSource
        End Get
        Set(ByVal value As ITranslucentControl.eBackgroundImageSource)
            _backgroundImageSource = value
        End Set
    End Property

#End Region

#Region "CUSTOM PAINTING ROUTINE"
    Protected Overrides Sub OnPaint(ByVal pevent As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(pevent)

    End Sub

    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            ' Make background transparent
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ExStyle = cp.ExStyle Or &H20
            Return cp
        End Get
    End Property

    Protected Overrides Sub OnPaintBackground(e As PaintEventArgs)
        '' call MyBase.OnPaintBackground(e) only if the backColor is not Color.Transparent
        If Me.BackColor <> Color.Transparent Then
            MyBase.OnPaintBackground(e)
        End If
    End Sub
#End Region

#Region "SUPPORT METHODS"
    Private Function GetPointFromForm(ByVal C As Control) As Point
        Try
            Return C.FindForm.PointToClient(C.Parent.PointToScreen(C.Location))
        Catch ex As Exception
            Return New Point(0, 0)
        End Try
    End Function
#End Region

End Class
