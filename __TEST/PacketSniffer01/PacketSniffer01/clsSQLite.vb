Imports System.Data.SQLite
Imports System.IO

Public Class clsSQLite
    Private mDatabaseFile As String
    Private mConn As SQLite.SQLiteConnection
    Private mOpen As Boolean

    Public Event DatabaseConnectionOpening()
    Public Event DatabaseConnectionOpen()
    Public Event DatabaseConnectionString(ByVal s As String)
    Public Event DatabaseConnectionFailed(ByVal sMessage As String)
    Public Event DatabaseConnectionClosing()
    Public Event DatabaseConnectionClosed()
    Public Event PacketSkipped(ByVal pckt As PcapDotNet.Packets.Packet)

    Public Property DatabaseFile() As String
        Get
            Return mDatabaseFile
        End Get
        Set(ByVal value As String)
            mDatabaseFile = value
        End Set
    End Property

    Public Sub New()
        mConn = New SQLite.SQLiteConnection()

        mOpen = False
    End Sub

    Public Sub OpenConnection()
        RaiseEvent DatabaseConnectionOpening()
        Try
            If Not IO.File.Exists(mDatabaseFile) Then
                Throw New Exception("Db file not found")
            End If

            mConn.ConnectionString = "Data Source=" & mDatabaseFile & ";Version=3;"
            RaiseEvent DatabaseConnectionString(mConn.ConnectionString)

            mConn.Open()

            mOpen = True
            RaiseEvent DatabaseConnectionOpen()
        Catch ex As Exception
            RaiseEvent DatabaseConnectionFailed(ex.Message)
        End Try
    End Sub

    Public Sub CloseConnection()
        RaiseEvent DatabaseConnectionClosing()
        Try
            mConn.Close()
            RaiseEvent DatabaseConnectionClosed()
        Catch ex As Exception
            RaiseEvent DatabaseConnectionFailed(ex.Message)
        End Try

        mOpen = False
    End Sub

    Public Function AddPacket(ByVal pckt As PcapDotNet.Packets.Packet) As Boolean
        If Not mOpen Then
            Return False
        End If

        Try
            Dim iMacSourceId As Integer
            Dim iMacDestinationId As Integer
            Dim iIpSourceId As Integer
            Dim iIpDestinationId As Integer
            Dim iPortSource As Integer
            Dim iPortDestination As Integer
            Dim Protocol As PcapDotNet.Packets.IpV4.IpV4Protocol
            Dim sProtocol As String

            iMacSourceId = _AddMac(pckt.Ethernet.Source.ToString())
            iMacDestinationId = _AddMac(pckt.Ethernet.Destination.ToString())
            iIpSourceId = _AddIp(pckt.Ethernet.IpV4.Source.ToString())
            iIpDestinationId = _AddIp(pckt.Ethernet.IpV4.Destination.ToString())

            Protocol = pckt.Ethernet.IpV4.Protocol
            Select Case Protocol
                Case PcapDotNet.Packets.IpV4.IpV4Protocol.Tcp
                    sProtocol = "tcp"
                    iPortSource = pckt.Ethernet.IpV4.Tcp.SourcePort
                    iPortDestination = pckt.Ethernet.IpV4.Tcp.DestinationPort
                Case PcapDotNet.Packets.IpV4.IpV4Protocol.Udp
                    sProtocol = "udp"
                    iPortSource = pckt.Ethernet.IpV4.Udp.SourcePort
                    iPortDestination = pckt.Ethernet.IpV4.Udp.DestinationPort
                Case Else
                    RaiseEvent PacketSkipped(pckt)
                    'sProtocol = Protocol.ToString

                    'iPortSource = pckt.Ethernet.IpV4.Transport.SourcePort
                    'iPortDestination = pckt.Ethernet.IpV4.Transport.DestinationPort

                    Return False
            End Select

            Dim SQLcommand As SQLiteCommand
            Dim SQLparameter1 As SQLiteParameter
            Dim SQLparameter2 As SQLiteParameter
            Dim SQLparameter3 As SQLiteParameter
            Dim SQLparameter4 As SQLiteParameter
            Dim SQLparameter5 As SQLiteParameter
            Dim SQLparameter6 As SQLiteParameter
            Dim SQLparameter7 As SQLiteParameter
            Dim SQLparameter8 As SQLiteParameter

            SQLcommand = mConn.CreateCommand
            SQLcommand.CommandText = "INSERT INTO tblPackets (mac_source_id,ip_source_id,port_source,mac_destination_id,ip_destination_id,port_destination,protocol,time_stamp) VALUES(?,?,?,?,?,?,?,?);SELECT last_insert_rowid() AS [ID]"

            SQLparameter1 = SQLcommand.CreateParameter
            SQLcommand.Parameters.Add(SQLparameter1)
            SQLparameter1.Value = iMacSourceId

            SQLparameter2 = SQLcommand.CreateParameter
            SQLcommand.Parameters.Add(SQLparameter2)
            SQLparameter2.Value = iIpSourceId

            SQLparameter3 = SQLcommand.CreateParameter
            SQLcommand.Parameters.Add(SQLparameter3)
            SQLparameter3.Value = iPortSource

            SQLparameter4 = SQLcommand.CreateParameter
            SQLcommand.Parameters.Add(SQLparameter4)
            SQLparameter4.Value = iMacDestinationId

            SQLparameter5 = SQLcommand.CreateParameter
            SQLcommand.Parameters.Add(SQLparameter5)
            SQLparameter5.Value = iIpDestinationId

            SQLparameter6 = SQLcommand.CreateParameter
            SQLcommand.Parameters.Add(SQLparameter6)
            SQLparameter6.Value = iPortDestination

            SQLparameter7 = SQLcommand.CreateParameter
            SQLcommand.Parameters.Add(SQLparameter7)
            SQLparameter7.Value = sProtocol

            SQLparameter8 = SQLcommand.CreateParameter
            SQLcommand.Parameters.Add(SQLparameter8)
            SQLparameter8.Value = pckt.Timestamp.ToString("yyyy-MM-dd hh:mm:ss")

            SQLcommand.ExecuteNonQuery()

            Return True
        Catch ex As Exception
            RaiseEvent DatabaseConnectionFailed(ex.Message)

            Return False
        End Try
    End Function

    Private Function _AddIp(ByVal sIp As String) As Integer
        Dim iIndex As Integer

        iIndex = _GetIpIndex(sIp)

        If iIndex > 0 Then
            Return iIndex
        End If

        Dim SQLcommand As SQLiteCommand
        Dim SQLparameter As SQLiteParameter
        Dim o As Object

        SQLcommand = mConn.CreateCommand
        SQLcommand.CommandText = "INSERT INTO tblIpAddresses (ip_address) VALUES(?);SELECT last_insert_rowid() AS [ID]"

        SQLparameter = SQLcommand.CreateParameter
        SQLcommand.Parameters.Add(SQLparameter)
        SQLparameter.Value = sIp

        o = SQLcommand.ExecuteScalar
        If o Is Nothing Then
            Return -1
        Else
            Return CType(o, Integer)
        End If
    End Function

    Private Function _GetIpIndex(ByVal sIp As String) As Integer
        Dim SQLcommand As SQLiteCommand
        Dim iIndex As Integer
        Dim o As Object

        iIndex = -1

        SQLcommand = mConn.CreateCommand
        SQLcommand.CommandText = "SELECT id FROM tblIpAddresses WHERE ip_address='" & sIp & "'"

        o = SQLcommand.ExecuteScalar
        If Not o Is Nothing Then
            iIndex = CType(o, Integer)
        End If

        SQLcommand.Dispose()

        Return iIndex
    End Function

    Private Function _AddMac(ByVal sMac As String) As Integer
        Dim iIndex As Integer

        iIndex = _GetMacIndex(sMac)

        If iIndex > 0 Then
            Return iIndex
        End If

        Dim SQLcommand As SQLiteCommand
        Dim SQLparameter As SQLiteParameter
        Dim o As Object

        SQLcommand = mConn.CreateCommand
        SQLcommand.CommandText = "INSERT INTO tblmacAddresses (mac_address) VALUES(?);SELECT last_insert_rowid() AS [ID]"

        SQLparameter = SQLcommand.CreateParameter
        SQLcommand.Parameters.Add(SQLparameter)

        SQLparameter.Value = sMac

        o = SQLcommand.ExecuteScalar
        If o Is Nothing Then
            Return -1
        Else
            Return CType(o, Integer)
        End If
    End Function

    Private Function _GetMacIndex(ByVal sMac As String) As Integer
        Dim SQLcommand As SQLiteCommand
        Dim iIndex As Integer
        Dim o As Object

        iIndex = -1

        SQLcommand = mConn.CreateCommand
        SQLcommand.CommandText = "SELECT id FROM tblMacAddresses WHERE mac_address='" & sMac & "'"

        o = SQLcommand.ExecuteScalar
        If Not o Is Nothing Then
            iIndex = CType(o, Integer)
        End If

        SQLcommand.Dispose()

        Return iIndex
    End Function
End Class
