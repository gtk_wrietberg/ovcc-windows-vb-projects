Imports System
Imports System.Collections
Imports System.Threading
'Imports PcapDotNet.Core
'Imports PcapDotNet.Packets

Public Class frmMain
    Private lpdCollection As System.Collections.ObjectModel.ReadOnlyCollection(Of PcapDotNet.Core.LivePacketDevice)
    Private bCancel As Boolean

    Private snifferThread As Thread
    Private WithEvents oSQLite As clsSQLite
    Private oLogger As clsLogger

    Delegate Sub ToggleStartButtonCallback(ByVal [bool] As Boolean)
    Delegate Sub ToggleStopButtonCallback(ByVal [bool] As Boolean)

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lpdCollection = PcapDotNet.Core.LivePacketDevice.AllLocalMachine

        oLogger = New clsLogger
        oLogger.LogFilePath = My.Application.Info.DirectoryPath
        oLogger.LogFileName = "PacketSniffer.log"

        oLogger.WriteToLog("-------------")

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oSQLite = New clsSQLite
        oSQLite.DatabaseFile = My.Application.Info.DirectoryPath & "\sniff.s3db"
    End Sub

    Private Sub StartSniffing()
        ToggleStartButton(False)
        ToggleStopButton(True)

        oLogger.WriteToLog("Sniffing")

        bCancel = False

        Dim lpdDevice As PcapDotNet.Core.PacketDevice
        'Dim pcktCommunicator As PcapDotNet.Core.PacketCommunicator

        lpdDevice = lpdCollection(2)

        Using pcktCommunicator As PcapDotNet.Core.PacketCommunicator = lpdDevice.Open(65536, PcapDotNet.Core.PacketDeviceOpenAttributes.Promiscuous, 1000)
            Dim pckt As PcapDotNet.Packets.Packet
            Dim commReceiveResult As PcapDotNet.Core.PacketCommunicatorReceiveResult

            Do
                commReceiveResult = pcktCommunicator.ReceivePacket(pckt)

                Select Case commReceiveResult
                    Case PcapDotNet.Core.PacketCommunicatorReceiveResult.Timeout
                        Continue Do
                    Case PcapDotNet.Core.PacketCommunicatorReceiveResult.Ok
                        oSQLite.AddPacket(pckt)
                    Case Else
                        CancelSniffing()
                End Select

            Loop While (Not bCancel)
        End Using
    End Sub

    Private Sub ToggleStartButton(ByVal [bool] As Boolean)
        If Me.btnStart.InvokeRequired Then
            Dim d As New ToggleStartButtonCallback(AddressOf ToggleStartButton)
            Me.Invoke(d, New Object() {[bool]})
        Else
            Me.btnStart.Enabled = [bool]
        End If
    End Sub

    Private Sub ToggleStopButton(ByVal [bool] As Boolean)
        If Me.btnStart.InvokeRequired Then
            Dim d As New ToggleStopButtonCallback(AddressOf ToggleStopButton)
            Me.Invoke(d, New Object() {[bool]})
        Else
            Me.btnStop.Enabled = [bool]
        End If
    End Sub

    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        oSQLite.OpenConnection()

        Me.snifferThread = New Thread(New ThreadStart(AddressOf Me.StartSniffing))
        Me.snifferThread.Start()
    End Sub

    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
        CancelSniffing()
    End Sub

    Private Sub CancelSniffing()
        oSQLite.CloseConnection()

        ToggleStartButton(True)
        ToggleStopButton(False)

        bCancel = True

        oLogger.WriteToLog("Stopped...")
    End Sub

    Private Sub oSQLite_DatabaseConnectionFailed(ByVal sMessage As String) Handles oSQLite.DatabaseConnectionFailed
        oLogger.WriteToLog("SQLite error: " & sMessage, clsLogger.MESSAGE_TYPE.LOG_ERROR)
    End Sub
End Class
