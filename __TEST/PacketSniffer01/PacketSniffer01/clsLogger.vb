Public Class clsLogger
    Private ReadOnly DefaultLogFile As String = "PacketSniffer.log"
    Private mLogFileName As String
    Private mLogFilePath As String
    Private prevDepth As Integer = 0

    Public Sub New()
        mLogFileName = DefaultLogFile
        mLogFilePath = ""
    End Sub

    Public Enum MESSAGE_TYPE
        LOG_DEFAULT = 0
        LOG_WARNING = 1
        LOG_ERROR = 2
        LOG_DEBUG = 6
    End Enum

    Public Property LogFilePath() As String
        Get
            Return mLogFilePath
        End Get
        Set(ByVal value As String)
            If value.EndsWith("\") Then
                mLogFilePath = value
            Else
                mLogFilePath = value & "\"
            End If
        End Set
    End Property

    Public Property LogFileName() As String
        Get
            Return mLogFileName
        End Get
        Set(ByVal value As String)
            mLogFileName = value
        End Set
    End Property

    Public Sub WriteToLog(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0)
        Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String, iDepthStep As Integer

        If iDepth < 0 Then
            iDepth = 0
        End If

        sMessage = Trim(sMessage)

        Select Case cMessageType
            Case MESSAGE_TYPE.LOG_WARNING
                sMsgTypePrefix = "[*] "
            Case MESSAGE_TYPE.LOG_ERROR
                sMsgTypePrefix = "[!] "
            Case MESSAGE_TYPE.LOG_DEBUG
                sMsgTypePrefix = "[#] "
            Case MESSAGE_TYPE.LOG_DEFAULT
                sMsgTypePrefix = "[.] "
            Case Else
                sMsgTypePrefix = "[?] "
        End Select

        sMsgDatePrefix = Now.ToString("yyyy-MM-dd hh:mm:ss") & " - "

        sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

        If iDepth < prevDepth Then
            For iDepthStep = 1 To iDepth
                sMsgPrefix = sMsgPrefix & "| "
            Next

            sMsgPrefix = sMsgPrefix & vbCrLf
            sMsgPrefix = sMsgPrefix & sMsgTypePrefix & sMsgDatePrefix
        End If

        If iDepth > 0 Then
            For iDepthStep = 1 To iDepth - 1
                sMsgPrefix = sMsgPrefix & "| "
            Next

            sMsgPrefix = sMsgPrefix & "|-"
        End If

        UpdateLogfile(sMsgPrefix & sMessage)

        prevDepth = iDepth
    End Sub

    Public Sub WriteToLogWithoutDate(ByVal sMessage As String)
        UpdateLogfile(sMessage)
    End Sub

    Public Sub WriteEmptyLineToLog()
        WriteToLogWithoutDate(" ")
    End Sub

    Private Sub UpdateLogfile(ByVal sString As String)
        Try
            Dim sw As New IO.StreamWriter(mLogFilePath & mLogFileName, True)
            sw.WriteLine(sString)
            sw.Close()
        Catch ex As Exception

        End Try
    End Sub

End Class
