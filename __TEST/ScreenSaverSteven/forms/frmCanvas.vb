Public Class frmCanvas
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tmrLoadImage As System.Windows.Forms.Timer
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblTimestamp As System.Windows.Forms.Label
    Friend WithEvents progressLoading As System.Windows.Forms.ProgressBar
    Friend WithEvents progressDownload As System.Windows.Forms.ProgressBar
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents pnlImageBackground As System.Windows.Forms.Panel
    Friend WithEvents picboxImage As System.Windows.Forms.PictureBox
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents lblTimestampAge As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.tmrLoadImage = New System.Windows.Forms.Timer(Me.components)
        Me.picboxImage = New System.Windows.Forms.PictureBox
        Me.lblName = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblTimestamp = New System.Windows.Forms.Label
        Me.progressLoading = New System.Windows.Forms.ProgressBar
        Me.progressDownload = New System.Windows.Forms.ProgressBar
        Me.lblWarning = New System.Windows.Forms.Label
        Me.pnlImageBackground = New System.Windows.Forms.Panel
        Me.lblLocation = New System.Windows.Forms.Label
        Me.lblTimestampAge = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'tmrLoadImage
        '
        Me.tmrLoadImage.Interval = 500
        '
        'picboxImage
        '
        Me.picboxImage.BackColor = System.Drawing.Color.White
        Me.picboxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picboxImage.Location = New System.Drawing.Point(320, 80)
        Me.picboxImage.Name = "picboxImage"
        Me.picboxImage.Size = New System.Drawing.Size(288, 288)
        Me.picboxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picboxImage.TabIndex = 0
        Me.picboxImage.TabStop = False
        Me.picboxImage.Visible = False
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(104, 368)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(432, 16)
        Me.lblName.TabIndex = 1
        Me.lblName.Text = "NAME.JPG - 800x600"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblName.Visible = False
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Verdana", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(112, 160)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(464, 24)
        Me.lblDescription.TabIndex = 2
        Me.lblDescription.Text = "DESCRIPTION"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblDescription.Visible = False
        '
        'lblTimestamp
        '
        Me.lblTimestamp.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblTimestamp.Location = New System.Drawing.Point(112, 280)
        Me.lblTimestamp.Name = "lblTimestamp"
        Me.lblTimestamp.Size = New System.Drawing.Size(432, 16)
        Me.lblTimestamp.TabIndex = 3
        Me.lblTimestamp.Text = "TIMESTAMP"
        Me.lblTimestamp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblTimestamp.Visible = False
        '
        'progressLoading
        '
        Me.progressLoading.Location = New System.Drawing.Point(496, 504)
        Me.progressLoading.Name = "progressLoading"
        Me.progressLoading.Size = New System.Drawing.Size(200, 8)
        Me.progressLoading.TabIndex = 5
        Me.progressLoading.Value = 50
        Me.progressLoading.Visible = False
        '
        'progressDownload
        '
        Me.progressDownload.Location = New System.Drawing.Point(96, 424)
        Me.progressDownload.Name = "progressDownload"
        Me.progressDownload.Size = New System.Drawing.Size(392, 16)
        Me.progressDownload.TabIndex = 6
        Me.progressDownload.Visible = False
        '
        'lblWarning
        '
        Me.lblWarning.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.Red
        Me.lblWarning.Location = New System.Drawing.Point(464, 200)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(464, 48)
        Me.lblWarning.TabIndex = 7
        Me.lblWarning.Text = "WARNING"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblWarning.Visible = False
        '
        'pnlImageBackground
        '
        Me.pnlImageBackground.BackColor = System.Drawing.Color.White
        Me.pnlImageBackground.Location = New System.Drawing.Point(288, 48)
        Me.pnlImageBackground.Name = "pnlImageBackground"
        Me.pnlImageBackground.Size = New System.Drawing.Size(96, 88)
        Me.pnlImageBackground.TabIndex = 8
        Me.pnlImageBackground.Visible = False
        '
        'lblLocation
        '
        Me.lblLocation.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLocation.ForeColor = System.Drawing.Color.DimGray
        Me.lblLocation.Location = New System.Drawing.Point(0, 552)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(56, 24)
        Me.lblLocation.TabIndex = 9
        Me.lblLocation.Text = "location"
        Me.lblLocation.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.lblLocation.Visible = False
        '
        'lblTimestampAge
        '
        Me.lblTimestampAge.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, CType(0, Byte))
        Me.lblTimestampAge.Location = New System.Drawing.Point(248, 320)
        Me.lblTimestampAge.Name = "lblTimestampAge"
        Me.lblTimestampAge.Size = New System.Drawing.Size(432, 16)
        Me.lblTimestampAge.TabIndex = 10
        Me.lblTimestampAge.Text = "1 jaar, 2 maanden, 3 weken en 4 dagen"
        Me.lblTimestampAge.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblTimestampAge.Visible = False
        '
        'frmCanvas
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(968, 576)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblTimestampAge)
        Me.Controls.Add(Me.lblLocation)
        Me.Controls.Add(Me.lblWarning)
        Me.Controls.Add(Me.progressDownload)
        Me.Controls.Add(Me.progressLoading)
        Me.Controls.Add(Me.lblTimestamp)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.picboxImage)
        Me.Controls.Add(Me.pnlImageBackground)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.White
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCanvas"
        Me.ShowInTaskbar = False
        Me.Text = "frmCanvas"
        Me.TopMost = True
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private m_Xmax As Integer
    Private m_Ymax As Integer

    Private iMaxWidth As Integer
    Private iMaxHeight As Integer

    Private oPhoto As Photo
    Private WithEvents oSync As PhotoSynchronize
    Private WithEvents oHttp As WebFileDownloader

    Private sFilePrefix As String = ""

    Private iTickCount As Integer
    Private iTickCountMax As Integer = 60

    Private bVersionText As Boolean

    Private lFileSize As Long

    Private Sub frmCanvas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If m_Action = ActionType.actRun Then
            Windows.Forms.Cursor.Hide()
        Else
            RemoveHandler Me.KeyDown, AddressOf frmCanvas_KeyDown
            RemoveHandler Me.MouseMove, AddressOf frmCanvas_MouseMove
            RemoveHandler Me.MouseDown, AddressOf frmCanvas_MouseDown
        End If

        oSync = New PhotoSynchronize
        oHttp = New WebFileDownloader

        oSync.UserName = gUserName
        oSync.PassWord = gPassWordSHA1

        If Not IsConnectionAvailable() Then
            lblWarning.Text = "Geen internet verbinding"

            lblWarning.Left = (Me.Width - lblWarning.Width) / 2
            lblWarning.Top = (Me.Height - lblWarning.Height) / 2

            lblWarning.Visible = True

            Exit Sub
        End If

        If Not oSync.CheckLogin Then
            lblWarning.Text = "Gebruikersnaam/wachtwoord onjuist"

            lblWarning.Left = (Me.Width - lblWarning.Width) / 2
            lblWarning.Top = (Me.Height - lblWarning.Height) / 2

            lblWarning.Visible = True

            Exit Sub
        End If

        iTickCountMax = gDelay * 2

        m_Xmax = Me.Width
        m_Ymax = Me.Height

        picboxImage.Width = m_Xmax / 1.25
        picboxImage.Height = m_Ymax / 1.25

        iMaxWidth = picboxImage.Width
        iMaxHeight = picboxImage.Height

        picboxImage.Visible = False
        picboxImage.Visible = False

        Dim pbh As New ProgressBarHelper(progressDownload)
        pbh.ForeColor = Color.White
        pbh.BackColor = Color.Black

        progressDownload.Left = (m_Xmax - progressDownload.Width) / 2
        progressDownload.Top = (m_Ymax - progressDownload.Height) / 2

        pbh = New ProgressBarHelper(progressLoading)
        pbh.ForeColor = Color.White
        pbh.BackColor = Color.Black

        progressLoading.Minimum = 0
        progressLoading.Maximum = iTickCountMax
        progressLoading.Value = iTickCountMax
        'progressLoading.Width = m_Xmax
        progressLoading.Left = m_Xmax - progressLoading.Width
        progressLoading.Top = m_Ymax - progressLoading.Height
        progressLoading.Visible = True

        lblDescription.Top = 0
        lblDescription.Left = 0
        lblDescription.Width = m_Xmax

        lblTimestamp.Top = lblDescription.Height
        lblTimestamp.Left = 0
        lblTimestamp.Width = m_Xmax

        lblTimestampAge.Top = lblTimestamp.Top + lblTimestamp.Height
        lblTimestampAge.Left = 0
        lblTimestampAge.Width = m_Xmax

        lblName.Top = m_Ymax - lblName.Height
        lblName.Left = 0
        lblName.Width = m_Xmax

        lblLocation.Top = m_Ymax - lblLocation.Height
        lblLocation.Left = 0

        iTickCount = iTickCountMax

        LoadImage()
    End Sub

    Private Sub frmCanvas_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove
        Static done_before As Boolean
        Static mouse_x As Long
        Static mouse_y As Long

        If Not done_before Then
            done_before = True
            mouse_x = e.X
            mouse_y = e.Y
        Else
            If Math.Abs(e.X - mouse_x) > 10 Or Math.Abs(e.Y - mouse_y) > 10 Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub frmCanvas_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseDown
        Me.Close()
    End Sub

    Private Sub frmCanvas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Me.Close()
    End Sub

    Private Sub LoadImageAndResizePictureBox(ByVal sImageFile As String)
        Dim img As Image
        Dim dImg As Double
        Dim dBox As Double

        picboxImage.Visible = False
        pnlImageBackground.Visible = False

        img = Image.FromFile(sImageFile)

        If img.Width <= picboxImage.Width And img.Height <= picboxImage.Height Then
            picboxImage.Width = img.Width
            picboxImage.Height = img.Height
        Else
            picboxImage.Width = iMaxWidth
            picboxImage.Height = iMaxHeight

            dImg = img.Width / img.Height
            dBox = picboxImage.Width / picboxImage.Height

            If dBox > dImg Then
                'resize width
                picboxImage.Width = picboxImage.Height * dImg
            Else
                'resize height
                picboxImage.Height = picboxImage.Width * dImg
            End If
        End If

        picboxImage.Left = (Me.Width - picboxImage.Width) / 2
        picboxImage.Top = (Me.Height - picboxImage.Height) / 2

        pnlImageBackground.Width = picboxImage.Width + 2
        pnlImageBackground.Height = picboxImage.Height + 2

        pnlImageBackground.Left = picboxImage.Left - 1
        pnlImageBackground.Top = picboxImage.Top - 1

        picboxImage.Image = img
        picboxImage.Visible = True
        pnlImageBackground.Visible = True
    End Sub

    Private Sub tmrLoadImage_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrLoadImage.Tick
        iTickCount = iTickCount - 1
        progressLoading.Value = iTickCount

        If iTickCount = 0 Then
            iTickCount = iTickCountMax
            LoadImage()
        End If
    End Sub

    Private Sub LoadImage()
        Dim sFilePath As String
        Dim sFileName As String

        tmrLoadImage.Enabled = False

        lblDescription.Visible = False
        lblTimestamp.Visible = False
        lblTimestampAge.Visible = False
        lblName.Visible = False
        lblLocation.Visible = False

        If oSync.LoadRandomPhoto() Then
            Dim oTime As New Timestamp

            oPhoto = oSync.Photo

            lblDescription.Text = oPhoto.Description
            lblName.Text = oPhoto.Name & " - " & CStr(oPhoto.Width) & "x" & CStr(oPhoto.Height)

            oTime.Timestamp = CLng(oPhoto.Timestamp)
            lblTimestamp.Text = oTime.DateTime.ToLongDateString & " - " & oTime.DateTime.ToLongTimeString
            lblTimestampAge.Text = ConvertAgeToString(cStevenBirthday, oTime.DateTime)

            lblDescription.Visible = True
            lblTimestamp.Visible = True
            lblTimestampAge.Visible = True
            lblName.Visible = True

            sFilePath = gCacheFolder & "\"
            sFilePath = sFilePath.Replace("\\", "\")
            sFileName = gCachePrefix & "_" & oPhoto.Name

            If Not IO.File.Exists(sFilePath & sFileName) Then
                progressDownload.Visible = True
                oHttp.DownloadFileWithProgress(oSync.BaseUrl & oPhoto.Name, sFilePath & sFileName)
                progressDownload.Visible = False

                lblLocation.Text = "web"
            Else
                lblLocation.Text = "cache"
            End If

            lblLocation.Visible = True

            LoadImageAndResizePictureBox(sFilePath & sFileName)
        Else
            lblDescription.Text = "!!!OEPS!!!"
            lblDescription.Visible = True
        End If

        tmrLoadImage.Enabled = True
    End Sub

    Private Sub oSync_XmlFailed(ByVal ex As System.Exception) Handles oSync.XmlFailed
        MsgBox(ex.Message)
    End Sub

    Private Sub oSync_LoginFailed(ByVal Message As String) Handles oSync.LoginFailed

    End Sub

    Private Sub oSync_LoadingPhoto() Handles oSync.LoadingPhoto
        lblDescription.Text = "Bezig met laden"
        lblDescription.Visible = True
    End Sub

    Private Sub oHttp_AmountDownloadedChanged(ByVal iNewProgress As Long) Handles oHttp.AmountDownloadedChanged
        Dim p As Integer

        p = 100 * Math.Round(iNewProgress / lFileSize, 2)

        progressDownload.Value = p
    End Sub

    Private Sub oHttp_FileDownloadSizeObtained(ByVal iFileSize As Long) Handles oHttp.FileDownloadSizeObtained
        lFileSize = iFileSize
    End Sub

    Private Sub oHttp_FileSaved(ByVal sFileLocation As String) Handles oHttp.FileSaved

    End Sub
End Class
