Public Class frmPreview
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblNoPreview As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lblNoPreview = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblNoPreview
        '
        Me.lblNoPreview.ForeColor = System.Drawing.Color.White
        Me.lblNoPreview.Location = New System.Drawing.Point(80, 104)
        Me.lblNoPreview.Name = "lblNoPreview"
        Me.lblNoPreview.Size = New System.Drawing.Size(152, 72)
        Me.lblNoPreview.TabIndex = 0
        Me.lblNoPreview.Text = "no preview"
        Me.lblNoPreview.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmPreview
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.lblNoPreview)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmPreview"
        Me.Text = "frmPreview"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmPreview_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblNoPreview.Left = 0
        lblNoPreview.Top = 0
        lblNoPreview.Width = Me.Width
        lblNoPreview.Height = Me.Height
    End Sub
End Class
