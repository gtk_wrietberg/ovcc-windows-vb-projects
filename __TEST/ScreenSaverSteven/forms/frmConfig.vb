Imports System.Drawing

Public Class frmConfig
    Inherits System.Windows.Forms.Form

    Private brFont1 As New SolidBrush(Color.Black)
    Private brFont2 As New SolidBrush(Color.Black)
    Private brPanel1 As New SolidBrush(Color.Yellow)
    Private brPanel2 As New SolidBrush(Color.Black)

    Private ReadOnly sDefaultPassword As String = "********"
    Private bLoaded As Boolean = False

    Private oCache As Cache

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If

        brFont1.Dispose()
        brFont2.Dispose()
        brPanel1.Dispose()
        brPanel2.Dispose()

        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents lblPassWord As System.Windows.Forms.Label
    Friend WithEvents txtPassWord As System.Windows.Forms.TextBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents StatusBar1 As System.Windows.Forms.StatusBar
    Friend WithEvents statusPanelInternetConnection As System.Windows.Forms.StatusBarPanel
    Friend WithEvents statusPanelUserDetails As System.Windows.Forms.StatusBarPanel
    Friend WithEvents tmrConnection As System.Windows.Forms.Timer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents trackbarDelay As System.Windows.Forms.TrackBar
    Friend WithEvents lblDelay As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCheckLogin As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents btnClearCache As System.Windows.Forms.Button
    Friend WithEvents lblAppName As System.Windows.Forms.Label
    Friend WithEvents lblCacheLocation As System.Windows.Forms.Label
    Friend WithEvents lblCacheSize As System.Windows.Forms.Label
    Friend WithEvents lblCacheCount As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtUsername = New System.Windows.Forms.TextBox
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblPassWord = New System.Windows.Forms.Label
        Me.txtPassWord = New System.Windows.Forms.TextBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.StatusBar1 = New System.Windows.Forms.StatusBar
        Me.statusPanelInternetConnection = New System.Windows.Forms.StatusBarPanel
        Me.statusPanelUserDetails = New System.Windows.Forms.StatusBarPanel
        Me.tmrConnection = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnCheckLogin = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblDelay = New System.Windows.Forms.Label
        Me.trackbarDelay = New System.Windows.Forms.TrackBar
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.lblCacheSize = New System.Windows.Forms.Label
        Me.lblCacheLocation = New System.Windows.Forms.Label
        Me.btnClearCache = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblVersion = New System.Windows.Forms.Label
        Me.lblAppName = New System.Windows.Forms.Label
        Me.lblCacheCount = New System.Windows.Forms.Label
        CType(Me.statusPanelInternetConnection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.statusPanelUserDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.trackbarDelay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(8, 288)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(232, 24)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Opslaan"
        '
        'txtUsername
        '
        Me.txtUsername.Enabled = False
        Me.txtUsername.Location = New System.Drawing.Point(8, 24)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(144, 21)
        Me.txtUsername.TabIndex = 1
        Me.txtUsername.Text = ""
        Me.txtUsername.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblUserName
        '
        Me.lblUserName.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUserName.Location = New System.Drawing.Point(8, 8)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(144, 16)
        Me.lblUserName.TabIndex = 2
        Me.lblUserName.Text = "Gebruikersnaam"
        Me.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPassWord
        '
        Me.lblPassWord.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassWord.Location = New System.Drawing.Point(8, 56)
        Me.lblPassWord.Name = "lblPassWord"
        Me.lblPassWord.Size = New System.Drawing.Size(144, 16)
        Me.lblPassWord.TabIndex = 4
        Me.lblPassWord.Text = "Wachtwoord"
        Me.lblPassWord.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtPassWord
        '
        Me.txtPassWord.Enabled = False
        Me.txtPassWord.Location = New System.Drawing.Point(8, 72)
        Me.txtPassWord.Name = "txtPassWord"
        Me.txtPassWord.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtPassWord.Size = New System.Drawing.Size(144, 21)
        Me.txtPassWord.TabIndex = 2
        Me.txtPassWord.Text = ""
        Me.txtPassWord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(248, 288)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(88, 24)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "Annuleren"
        '
        'StatusBar1
        '
        Me.StatusBar1.Location = New System.Drawing.Point(0, 316)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.statusPanelInternetConnection, Me.statusPanelUserDetails})
        Me.StatusBar1.ShowPanels = True
        Me.StatusBar1.Size = New System.Drawing.Size(346, 20)
        Me.StatusBar1.SizingGrip = False
        Me.StatusBar1.TabIndex = 7
        '
        'statusPanelInternetConnection
        '
        Me.statusPanelInternetConnection.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusPanelInternetConnection.Style = System.Windows.Forms.StatusBarPanelStyle.OwnerDraw
        Me.statusPanelInternetConnection.Text = "Internet Connection"
        Me.statusPanelInternetConnection.Width = 173
        '
        'statusPanelUserDetails
        '
        Me.statusPanelUserDetails.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.statusPanelUserDetails.Style = System.Windows.Forms.StatusBarPanelStyle.OwnerDraw
        Me.statusPanelUserDetails.Text = "User details"
        Me.statusPanelUserDetails.Width = 173
        '
        'tmrConnection
        '
        Me.tmrConnection.Interval = 10000
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPassWord)
        Me.GroupBox1.Controls.Add(Me.lblPassWord)
        Me.GroupBox1.Controls.Add(Me.lblUserName)
        Me.GroupBox1.Controls.Add(Me.txtUsername)
        Me.GroupBox1.Controls.Add(Me.btnCheckLogin)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(160, 144)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        '
        'btnCheckLogin
        '
        Me.btnCheckLogin.Font = New System.Drawing.Font("Verdana", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCheckLogin.Location = New System.Drawing.Point(8, 104)
        Me.btnCheckLogin.Name = "btnCheckLogin"
        Me.btnCheckLogin.Size = New System.Drawing.Size(144, 32)
        Me.btnCheckLogin.TabIndex = 6
        Me.btnCheckLogin.Text = "Controleer naam en wachtwoord"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.lblDelay)
        Me.GroupBox2.Controls.Add(Me.trackbarDelay)
        Me.GroupBox2.Location = New System.Drawing.Point(176, 48)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(160, 104)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(144, 32)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Vertraging diavoorstelling"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDelay
        '
        Me.lblDelay.Location = New System.Drawing.Point(120, 48)
        Me.lblDelay.Name = "lblDelay"
        Me.lblDelay.Size = New System.Drawing.Size(32, 32)
        Me.lblDelay.TabIndex = 1
        Me.lblDelay.Text = "0"
        Me.lblDelay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'trackbarDelay
        '
        Me.trackbarDelay.LargeChange = 10
        Me.trackbarDelay.Location = New System.Drawing.Point(8, 48)
        Me.trackbarDelay.Maximum = 12
        Me.trackbarDelay.Minimum = 1
        Me.trackbarDelay.Name = "trackbarDelay"
        Me.trackbarDelay.Size = New System.Drawing.Size(120, 45)
        Me.trackbarDelay.TabIndex = 3
        Me.trackbarDelay.Value = 6
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblCacheCount)
        Me.GroupBox3.Controls.Add(Me.lblCacheSize)
        Me.GroupBox3.Controls.Add(Me.lblCacheLocation)
        Me.GroupBox3.Controls.Add(Me.btnClearCache)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 152)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(328, 120)
        Me.GroupBox3.TabIndex = 11
        Me.GroupBox3.TabStop = False
        '
        'lblCacheSize
        '
        Me.lblCacheSize.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCacheSize.Location = New System.Drawing.Point(8, 72)
        Me.lblCacheSize.Name = "lblCacheSize"
        Me.lblCacheSize.Size = New System.Drawing.Size(312, 16)
        Me.lblCacheSize.TabIndex = 8
        Me.lblCacheSize.Text = "Locatie"
        '
        'lblCacheLocation
        '
        Me.lblCacheLocation.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCacheLocation.Location = New System.Drawing.Point(8, 40)
        Me.lblCacheLocation.Name = "lblCacheLocation"
        Me.lblCacheLocation.Size = New System.Drawing.Size(312, 16)
        Me.lblCacheLocation.TabIndex = 7
        Me.lblCacheLocation.Text = "Locatie"
        '
        'btnClearCache
        '
        Me.btnClearCache.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearCache.ForeColor = System.Drawing.Color.Red
        Me.btnClearCache.Location = New System.Drawing.Point(8, 88)
        Me.btnClearCache.Name = "btnClearCache"
        Me.btnClearCache.Size = New System.Drawing.Size(312, 24)
        Me.btnClearCache.TabIndex = 6
        Me.btnClearCache.Text = "cache legen"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(312, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Cache"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVersion
        '
        Me.lblVersion.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(176, 32)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(160, 8)
        Me.lblVersion.TabIndex = 12
        Me.lblVersion.Text = "1.2.3456.7890"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAppName
        '
        Me.lblAppName.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppName.Location = New System.Drawing.Point(184, 16)
        Me.lblAppName.Name = "lblAppName"
        Me.lblAppName.Size = New System.Drawing.Size(152, 8)
        Me.lblAppName.TabIndex = 13
        Me.lblAppName.Text = "StevenScreenSaver"
        Me.lblAppName.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCacheCount
        '
        Me.lblCacheCount.Font = New System.Drawing.Font("Verdana", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCacheCount.Location = New System.Drawing.Point(8, 56)
        Me.lblCacheCount.Name = "lblCacheCount"
        Me.lblCacheCount.Size = New System.Drawing.Size(312, 16)
        Me.lblCacheCount.TabIndex = 9
        Me.lblCacheCount.Text = "Locatie"
        '
        'frmConfig
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 14)
        Me.ClientSize = New System.Drawing.Size(346, 336)
        Me.Controls.Add(Me.lblAppName)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnSave)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConfig"
        Me.Text = "Configuratie"
        CType(Me.statusPanelInternetConnection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.statusPanelUserDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.trackbarDelay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents oTest As PhotoSynchronize

    Private Sub frmConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtUsername.Text = gUserName
        txtPassWord.Text = gPassWord

        lblAppName.Text = Application.ProductName
        lblVersion.Text = "v" & Application.ProductVersion
        lblCacheLocation.Text = gCacheFolder

        ReloadCacheDetails

        txtUsername.Enabled = False
        txtPassWord.Enabled = False
        btnSave.Enabled = False
        btnClearCache.Enabled = False

        brFont1.Color = Color.Black
        brFont2.Color = Color.Black
        brPanel1.Color = Me.BackColor
        brPanel2.Color = Me.BackColor

        trackbarDelay.Value = gDelay / 5
        UpdateDelayValue()

        tmrConnection.Interval = 1000
        tmrConnection.Enabled = True

        bLoaded = True
    End Sub

    Private Sub ReloadCacheDetails()
        oCache = New Cache
        oCache.Folder = gCacheFolder
        oCache.Prefix = gCachePrefix
        oCache.Examine()

        lblCacheLocation.Text = gCacheFolder
        If oCache.Count <> 1 Then
            lblCacheCount.Text = oCache.Count & " bestanden"
        Else
            lblCacheCount.Text = oCache.Count & " bestand"
        End If
        lblCacheSize.Text = oCache.HumanReadableSize
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        gUserName = txtUsername.Text
        gPassWord = txtPassWord.Text
        gPassWordSHA1 = GetSHA1Hash(gPassWord)
        gCacheFolder = System.IO.Path.GetTempPath
        gDelay = trackbarDelay.Value * 5

        If CheckLoginDetails() Then
            SaveConfiguration()

            CloseConfig()
        End If
    End Sub

    Private Sub CloseConfig()
        Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        CloseConfig()
    End Sub

    Private Sub btnVerifyLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        gUserName = txtUsername.Text
        gPassWord = txtPassWord.Text
        gPassWordSHA1 = GetSHA1Hash(gPassWord)

        CheckLoginDetails()
    End Sub

    Private Sub oTest_LoginFailed(ByVal Message As String) Handles oTest.LoginFailed

    End Sub

    Private Sub oTest_XmlFailed(ByVal ex As System.Exception) Handles oTest.XmlFailed
        MsgBox("XMLFAILED: " & ex.Message)
    End Sub

    Private Sub StatusBar1_DrawItem(ByVal sender As Object, ByVal sbdevent As System.Windows.Forms.StatusBarDrawItemEventArgs) Handles StatusBar1.DrawItem
        Dim g As Graphics
        g = sbdevent.Graphics

        Dim sb As StatusBar
        sb = CType(sender, StatusBar)

        Dim rectf As New RectangleF(sbdevent.Bounds.X, sbdevent.Bounds.Y, sbdevent.Bounds.Width, sbdevent.Bounds.Height)

        Select Case sbdevent.Index
            Case 0
                sbdevent.Graphics.FillRectangle(brPanel1, sbdevent.Bounds)
                g.DrawString("Internet verbinding", sb.Font, brFont1, rectf)
            Case 1
                sbdevent.Graphics.FillRectangle(brPanel2, sbdevent.Bounds)
                g.DrawString("Gebruikersgegevens", sb.Font, brFont2, rectf)
        End Select
    End Sub

    Private Sub tmrConnection_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrConnection.Tick
        tmrConnection.Enabled = False

        If CheckInternetConnection() Then
            CheckLoginDetails()
        Else
            CheckLoginDetails(True)
        End If
    End Sub

    Private Function CheckInternetConnection() As Boolean
        Dim bRet As Boolean

        ToggleControls(False)

        brPanel1.Color = Color.Yellow
        brFont1.Color = Color.Black
        StatusBar1.Refresh()

        bRet = IsConnectionAvailable()

        If bRet Then
            brPanel1.Color = Color.Green
            brFont1.Color = Color.White

            ToggleControls(True)
        Else
            brPanel1.Color = Color.Red

            ToggleControls(True)
            txtUsername.Enabled = False
            txtPassWord.Enabled = False
            btnSave.Enabled = False
        End If

        StatusBar1.Refresh()

        Return bRet
    End Function

    Private Function CheckLoginDetails(Optional ByVal bJustSayItFailed As Boolean = False) As Boolean
        Dim bRet As Boolean

        ToggleControls(False)

        brPanel2.Color = Color.Yellow
        brFont2.Color = Color.Black
        StatusBar1.Refresh()

        oTest = New PhotoSynchronize
        oTest.UserName = gUserName
        oTest.PassWord = gPassWordSHA1

        If bJustSayItFailed Then
            bRet = False
        Else
            bRet = oTest.CheckLogin
        End If

        If bRet Then
            brPanel2.Color = Color.Green
            brFont2.Color = Color.White
        Else
            brPanel2.Color = Color.Red
        End If
        StatusBar1.Refresh()

        ToggleControls(True)

        Return bRet
    End Function

    Private Sub trackbarDelay_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles trackbarDelay.ValueChanged
        If bLoaded Then
            UpdateDelayValue()
        End If
    End Sub

    Private Sub UpdateDelayValue()
        gDelay = trackbarDelay.Value * 5
        lblDelay.Text = CStr(gDelay)
    End Sub

    Private Sub ToggleControls(ByVal bEnabled As Boolean)
        btnCancel.Enabled = bEnabled
        btnSave.Enabled = bEnabled
        btnCheckLogin.Enabled = bEnabled
        btnClearCache.Enabled = bEnabled
        trackbarDelay.Enabled = bEnabled
        txtUsername.Enabled = bEnabled
        txtPassWord.Enabled = bEnabled
    End Sub

    Private Sub btnCheckLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckLogin.Click
        gUserName = txtUsername.Text
        gPassWord = txtPassWord.Text
        gPassWordSHA1 = GetSHA1Hash(gPassWord)

        CheckLoginDetails()
    End Sub

    Private Sub btnClearCache_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearCache.Click
        ToggleControls(False)

        oCache.ClearCache()
        ReloadCacheDetails()

        ToggleControls(True)
    End Sub
End Class
