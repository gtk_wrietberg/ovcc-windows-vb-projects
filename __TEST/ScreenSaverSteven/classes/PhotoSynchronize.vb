Imports System.Xml

Public Class PhotoSynchronize
    Public Event LoginFailed(ByVal Message As String)
    Public Event XmlFailed(ByVal ex As Exception)
    Public Event LoadingPhoto()
    Public Event ReceivedPhoto()

    Private Const cUrl As String = "http://www.stevenrietberg.nl/screensaver/handler.php"

    Private mXml As XmlDocument
    Private mRootNode As XmlNode
    Private mNode As XmlNode
    Private mSubNode As XmlNode

    Private mBaseUrl As String
    Private mPhoto As Photo
    Private mCount As Integer

    Private mUserName As String
    Private mPassWord As String
    Private mLastPhotoDate As Long

    Public Sub New()
        mXml = New XmlDocument
        mPhoto = New Photo
        mCount = 0
    End Sub

    Public ReadOnly Property Photo() As Photo
        Get
            Return mPhoto
        End Get
    End Property

    Public Property BaseUrl() As String
        Get
            Return mBaseUrl
        End Get
        Set(ByVal Value As String)
            mBaseUrl = Value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mUserName
        End Get
        Set(ByVal Value As String)
            mUserName = Value
        End Set
    End Property

    Public Property PassWord() As String
        Get
            Return mPassWord
        End Get
        Set(ByVal Value As String)
            mPassWord = Value
        End Set
    End Property

    Public Property LastPhotoDate() As Long
        Get
            Return mLastPhotoDate
        End Get
        Set(ByVal Value As Long)
            mLastPhotoDate = Value
        End Set
    End Property

    Public Function CheckLogin() As Boolean
        Try
            mXml = New XmlDocument

            mXml.Load(CreateRequest("verify_login_details"))

            mRootNode = mXml.SelectSingleNode("sjr")
            mNode = mRootNode.SelectSingleNode("login")

            If Not mNode Is Nothing Then
                If mNode.InnerText = "ok" Then
                    CheckLogin = True
                Else
                    RaiseEvent LoginFailed("Incorrect login details")
                End If
            Else
                RaiseEvent LoginFailed("Something wrong with the xml!")
            End If
        Catch e As Exception
            RaiseEvent XmlFailed(e)
        End Try
    End Function

    Public Function LoadRandomPhoto() As Boolean
        RaiseEvent LoadingPhoto()

        Try
            mXml = New XmlDocument

            mXml.Load(CreateRequest("get_random_photo"))
            mRootNode = mXml.SelectSingleNode("sjr")
            mNode = mRootNode.SelectSingleNode("baseurl")
            mBaseUrl = mNode.InnerText

            mPhoto = New Photo

            mNode = mRootNode.SelectSingleNode("photos").SelectSingleNode("photo")

            mSubNode = mNode.SelectSingleNode("name")
            mPhoto.Name = mSubNode.InnerText

            mSubNode = mNode.SelectSingleNode("width")
            mPhoto.Width = CInt(mSubNode.InnerText)

            mSubNode = mNode.SelectSingleNode("height")
            mPhoto.Height = CInt(mSubNode.InnerText)

            mSubNode = mNode.SelectSingleNode("description")
            mPhoto.Description = mSubNode.InnerText

            mSubNode = mNode.SelectSingleNode("timestamp")
            mPhoto.Timestamp = CInt(mSubNode.InnerText)

            RaiseEvent ReceivedPhoto()

            LoadRandomPhoto = True
        Catch e As Exception
            RaiseEvent XmlFailed(e)
        End Try
    End Function

    Private Function CreateRequest(ByVal Action As String) As String
        Select Case Action
            Case "verify_login_details"
                CreateRequest = cUrl & "?action=verify_login_details&user=" & mUserName & "&pass=" & mPassWord
            Case "get_random_photo"
                CreateRequest = cUrl & "?action=get_random_photo&user=" & mUserName & "&pass=" & mPassWord & "&date=" & mLastPhotoDate
            Case Else
                CreateRequest = cUrl
        End Select
    End Function
End Class
