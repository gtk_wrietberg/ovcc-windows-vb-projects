Public Class Timestamp
    Private mDateTime As Date
    Private mTimestamp As Double

    Private ReadOnly EPOCH As Date = New Date(1970, 1, 1)

    Public Property DateTime() As Date
        Get
            Return mDateTime
        End Get
        Set(ByVal Value As Date)
            mDateTime = Value
            mTimestamp = Me.ConvertDateTimeToTimestamp(Value)
        End Set
    End Property

    Public Property Timestamp() As Long
        Get
            Return CLng(mTimestamp + 0.5)
        End Get
        Set(ByVal Value As Long)
            mTimestamp = CDbl(Value)
            mDateTime = Me.ConvertTimestampToDateTime(mTimestamp)
        End Set
    End Property

    Private Function ConvertDateTimeToTimestamp(ByVal value As Date) As Double
        Dim span As New TimeSpan

        span = value.Subtract(EPOCH)

        Return span.TotalSeconds
    End Function

    Private Function ConvertTimestampToDateTime(ByVal value As Double) As Date
        Return EPOCH.ToLocalTime.AddSeconds(value)
    End Function
End Class
