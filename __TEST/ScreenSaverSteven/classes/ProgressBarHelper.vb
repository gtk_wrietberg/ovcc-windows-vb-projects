Public Class ProgressBarHelper
    Private Declare Auto Function SendMessage Lib "user32.dll" (ByVal hWnd As IntPtr, ByVal wMsg As Int32, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As IntPtr

    Private Const WM_USER As Int32 = &H400
    Private Const CCM_FIRST As Int32 = &H2000
    Private Const CCM_SETBKCOLOR As Int32 = (CCM_FIRST + &H1)
    Private Const PBM_SETBARCOLOR As Int32 = (WM_USER + 9)
    Private Const PBM_SETBKCOLOR As Int32 = CCM_SETBKCOLOR
    Private Const CLR_DEFAULT As Int32 = &HFF000000

    Private m_ProgressBar As ProgressBar
    Private m_BackColor As Color
    Private m_ForeColor As Color
    Private m_OldBackColor As Color
    Private m_OldForeColor As Color

    Public Sub New(ByVal ProgressBar As ProgressBar)
        If ProgressBar Is Nothing Then
            Throw New NullReferenceException
        End If
        m_ProgressBar = ProgressBar

        ' Reset colors to make calls to property gets for 'ForeColor' and
        ' 'BackColor' return the right value.
        Me.ForeColor = DefaultColor
        Me.BackColor = DefaultColor
    End Sub

    Public ReadOnly Property ProgressBar() As ProgressBar
        Get
            Return m_ProgressBar
        End Get
    End Property

    Public Property ForeColor() As Color
        Get
            Return m_ForeColor
        End Get
        Set(ByVal Value As Color)
            m_ForeColor = Value
            SetBarColor(Me.ForeColor)
        End Set
    End Property

    Public Property BackColor() As Color
        Get
            Return m_BackColor
        End Get
        Set(ByVal Value As Color)
            m_BackColor = Value
            SetBkColor(Me.BackColor)
        End Set
    End Property

    Public Shared ReadOnly Property DefaultColor() As Color
        Get
            Return Color.Transparent
        End Get
    End Property

    Private Function SetBarColor(ByVal Color As Color) As Color
        Return ProgressBarColorToDotNetColor(SendMessage(m_ProgressBar.Handle, PBM_SETBARCOLOR, IntPtr.Zero, New IntPtr(DotNetColorToProgressBarColor(Color))).ToInt32())
    End Function

    Private Function SetBkColor(ByVal Color As Color) As Color
        Return ProgressBarColorToDotNetColor(SendMessage(m_ProgressBar.Handle, PBM_SETBKCOLOR, IntPtr.Zero, New IntPtr(DotNetColorToProgressBarColor(Color))).ToInt32())
    End Function

    Private Function DotNetColorToProgressBarColor(ByVal cColor As Color) As Int32
        If cColor.Equals(DefaultColor) Then
            Return CLR_DEFAULT
        Else
            Return ColorTranslator.ToWin32(cColor)
        End If
    End Function

    Private Function ProgressBarColorToDotNetColor(ByVal cColor As Int32) As Color
        If cColor.Equals(CLR_DEFAULT) Then
            Return DefaultColor
        Else
            Return ColorTranslator.FromWin32(cColor)
        End If
    End Function
End Class
