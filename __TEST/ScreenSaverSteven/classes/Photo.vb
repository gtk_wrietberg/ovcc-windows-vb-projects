Public Class Photo
    Private mName As String
    Private mWidth As Integer
    Private mHeight As Integer
    Private mDescription As String
    Private mTimestamp As Integer

    Public Property Name() As String
        Get
            Return mName
        End Get
        Set(ByVal Value As String)
            mName = Value
        End Set
    End Property

    Public Property Width() As Integer
        Get
            Return mWidth
        End Get
        Set(ByVal Value As Integer)
            mWidth = Value
        End Set
    End Property

    Public Property Height() As Integer
        Get
            Return mHeight
        End Get
        Set(ByVal Value As Integer)
            mHeight = Value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return mDescription
        End Get
        Set(ByVal Value As String)
            mDescription = Value
        End Set
    End Property

    Public Property Timestamp() As Integer
        Get
            Return mTimestamp
        End Get
        Set(ByVal Value As Integer)
            mTimestamp = Value
        End Set
    End Property
End Class
