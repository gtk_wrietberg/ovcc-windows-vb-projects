Public Class XorEncryption
    Private ReadOnly PassPhraseLength As Integer = 23
    Private arrPass() As Integer
    Private arrExtraPass() As Integer
    Private ReadOnly ExtraPassPhrase As String = "@#*&vnN:hKg87r3t5 b39;74"

    Private Sub InitializePasshrase()
        Dim i As Integer

        Randomize()

        ReDim arrPass(PassPhraseLength)
        For i = 0 To PassPhraseLength - 1
            arrPass(i) = Int(255 * Rnd())
        Next
    End Sub

    Private Sub InitializeExtraPasshrase()
        Dim i As Integer

        ReDim arrExtraPass(Len(ExtraPassPhrase))
        For i = 0 To Len(ExtraPassPhrase) - 1
            arrExtraPass(i) = Asc(Mid(ExtraPassPhrase, i + 1, 1))
        Next
    End Sub

    Public Function Encode(ByVal e_str As String) As String
        Dim i As Integer, p As Integer
        Dim encStr As String, encStr2 As String

        InitializePasshrase()
        InitializeExtraPasshrase()

        p = 0
        encStr = ""
        For i = 0 To PassPhraseLength - 1
            encStr = encStr & D2H(arrPass(i))
        Next
        For i = 0 To Len(e_str) - 1
            encStr = encStr & D2H(Asc(Mid(e_str, i + 1, 1)) Xor arrPass(p))
            p = p + 1
            If p >= PassPhraseLength Then
                p = 0
            End If
        Next

        p = 0
        encStr2 = ""
        For i = 0 To Len(encStr) - 1
            encStr2 = encStr2 & D2H(Asc(Mid(encStr, i + 1, 1)) Xor arrExtraPass(p))
            p = p + 1
            If p >= Len(ExtraPassPhrase) Then
                p = 0
            End If
        Next

        Return encStr2
    End Function

    Public Function Decode(ByVal d_str As String) As String
        Dim a() As Integer
        Dim i As Integer, j As Integer, p As Integer
        Dim decStr As String, decStr2 As String

        InitializeExtraPasshrase()

        p = 0
        decStr = ""

        For i = 1 To Len(d_str) Step 2
            decStr = decStr & Chr(H2D(Mid(d_str, i, 2)) Xor arrExtraPass(p))
            p = p + 1
            If p >= Len(ExtraPassPhrase) Then
                p = 0
            End If
        Next

        ReDim a(PassPhraseLength)
        p = 0
        decStr2 = ""

        For i = 1 To PassPhraseLength * 2 Step 2
            j = ((i + 1) / 2) - 1
            a(j) = H2D(Mid(decStr, i, 2))
        Next
        For i = PassPhraseLength * 2 + 1 To Len(decStr) Step 2
            decStr2 = decStr2 & Chr(H2D(Mid(decStr, i, 2)) Xor a(p))
            p = p + 1
            If p >= PassPhraseLength Then
                p = 0
            End If
        Next

        Return decStr2
    End Function

    Private Function D2H(ByVal Value As Integer) As String
        Dim iVal As Integer
        Dim temp As Integer
        Dim ret As Integer
        Dim i As Integer
        Dim Str As String = ""
        Dim BinVal() As String

        iVal = Value
        Do
            temp = Int(iVal / 16)
            ret = iVal Mod 16
            ReDim Preserve BinVal(i)
            BinVal(i) = NoToHex(ret)
            i = i + 1
            iVal = temp
        Loop While temp > 0
        For i = UBound(BinVal) To 0 Step -1
            Str = Str + CStr(BinVal(i))
        Next
        If Value < 16 Then
            Str = "0" & Str
        End If
        D2H = Str
    End Function

    Private Function H2D(ByVal BinVal As String) As Integer
        Dim iVal As Integer, temp As Integer

        temp = HexToNo(Mid(BinVal, 2, 1))
        iVal = iVal + temp
        temp = HexToNo(Mid(BinVal, 1, 1))
        iVal = iVal + (temp * 16)

        H2D = iVal
    End Function

    Private Function NoToHex(ByVal i As Integer) As String
        Select Case i
            Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                Return CStr(i)
            Case 10
                Return "a"
            Case 11
                Return "b"
            Case 12
                Return "c"
            Case 13
                Return "d"
            Case 14
                Return "e"
            Case 15
                Return "f"
            Case Else
                Return ""
        End Select
    End Function

    Private Function HexToNo(ByVal s As String) As Integer
        Select Case s
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                Return CInt(s)
            Case "A", "a"
                Return 10
            Case "B", "b"
                Return 11
            Case "C", "c"
                Return 12
            Case "D", "d"
                Return 13
            Case "E", "e"
                Return 14
            Case "F", "f"
                Return 15
            Case Else
                Return -1
        End Select
    End Function
End Class
