Public Class Cache
    Private mCacheFiles As ArrayList
    Private mCacheFilesFullPath As ArrayList
    Private mCacheFileCount As Integer
    Private mCacheSize As Long
    Private mCacheFolder As String
    Private mCachePrefix As String

#Region "properties"
    Public Property Folder() As String
        Get
            Return mCacheFolder
        End Get
        Set(ByVal Value As String)
            mCacheFolder = Value
        End Set
    End Property

    Public Property Prefix() As String
        Get
            Return mCachePrefix
        End Get
        Set(ByVal Value As String)
            mCachePrefix = Value
        End Set
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Return mCacheFiles.Count
        End Get
    End Property

    Public ReadOnly Property Size() As Long
        Get
            Return mCacheSize
        End Get
    End Property

    Public ReadOnly Property HumanReadableSize() As String
        Get
            Return _HumanReadableFileSize(mCacheSize)
        End Get
    End Property
#End Region

#Region "constructor"
    Public Sub New()
        mCacheFiles = New ArrayList
        mCacheFilesFullPath = New ArrayList
        Me.Reset()
        mCacheFolder = System.IO.Path.GetTempPath
    End Sub
#End Region

#Region "publics"
    Public Sub Examine()
        Dim myFile As String

        If mCacheFolder.Equals(String.Empty) Then
            Exit Sub
        End If

        If mCachePrefix.Equals(String.Empty) Then
            Exit Sub
        End If

        mCacheFiles = New ArrayList
        mCacheFilesFullPath = New ArrayList
        mCacheSize = 0

        For Each myFile In System.IO.Directory.GetFiles(mCacheFolder, mCachePrefix & "*.*")
            mCacheFiles.Add(System.IO.Path.GetFileName(myFile))
            mCacheFilesFullPath.Add(myFile)
            mCacheSize = mCacheSize + _GetFileSize(myFile)
        Next
    End Sub

    Public Sub Reset()
        mCacheFileCount = -1
    End Sub

    Public Function GetNext(ByRef Value As String) As Boolean
        mCacheFileCount = mCacheFileCount + 1
        If mCacheFileCount >= mCacheFiles.Count Then
            Value = ""
            Return False
        End If

        Value = CType(mCacheFiles.Item(mCacheFileCount), String)
        Return True
    End Function

    Public Sub ClearCache()
        Dim i As Integer

        For i = 0 To mCacheFiles.Count - 1
            Try
                System.IO.File.Delete(CType(mCacheFilesFullPath.Item(i), String))
            Catch ex As Exception

            End Try
        Next
    End Sub
#End Region

#Region "privates"
    Private Function _GetFileSize(ByVal MyFilePath As String) As Long
        Dim MyFile As New System.IO.FileInfo(MyFilePath)
        Dim FileSize As Long = MyFile.Length

        Return FileSize
    End Function

    Private Function _HumanReadableFileSize(ByVal lFileSize As Long) As String
        Dim sizes(3) As String
        Dim dFileSize As Double
        Dim i As Integer

        sizes(0) = "B"
        sizes(1) = "KB"
        sizes(2) = "MB"
        sizes(3) = "GB"

        i = 0

        dFileSize = CDbl(lFileSize)

        Do While (dFileSize >= 1024) And ((i + 1) < sizes.Length)
            i = i + 1
            dFileSize = dFileSize / 1024
        Loop

        Return String.Format("{0:0} {1}", dFileSize, sizes(i))
    End Function
#End Region
End Class
