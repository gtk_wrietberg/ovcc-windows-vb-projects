Imports Microsoft.Win32

Module RegistryHelper
    Public Function WriteToRegistry(ByVal ParentKeyHive As RegistryHive, ByVal SubKeyName As String, ByVal ValueName As String, ByVal Value As Object) As Boolean
        Dim objSubKey As RegistryKey
        Dim objParentKey As RegistryKey
        Dim bAns As Boolean

        Try
            Select Case ParentKeyHive
                Case RegistryHive.ClassesRoot
                    objParentKey = Registry.ClassesRoot
                Case RegistryHive.CurrentConfig
                    objParentKey = Registry.CurrentConfig
                Case RegistryHive.CurrentUser
                    objParentKey = Registry.CurrentUser
                Case RegistryHive.DynData
                    objParentKey = Registry.DynData
                Case RegistryHive.LocalMachine
                    objParentKey = Registry.LocalMachine
                Case RegistryHive.PerformanceData
                    objParentKey = Registry.PerformanceData
                Case RegistryHive.Users
                    objParentKey = Registry.Users
            End Select

            objSubKey = objParentKey.OpenSubKey(SubKeyName, True)
            If objSubKey Is Nothing Then
                objSubKey = objParentKey.CreateSubKey(SubKeyName)
            End If

            objSubKey.SetValue(ValueName, Value)
            bAns = True
        Catch ex As Exception
            bAns = False
        End Try

        Return bAns

    End Function

    Public Function ReadFromRegistry(ByVal Hive As RegistryHive, ByVal Key As String, ByVal ValueName As String, Optional ByVal DefaultValue As String = "", Optional ByRef ErrInfo As String = "") As Object
        Dim objParent As RegistryKey
        Dim objSubkey As RegistryKey
        Dim sAns As String = ""

        Select Case Hive
            Case RegistryHive.ClassesRoot
                objParent = Registry.ClassesRoot
            Case RegistryHive.CurrentConfig
                objParent = Registry.CurrentConfig
            Case RegistryHive.CurrentUser
                objParent = Registry.CurrentUser
            Case RegistryHive.DynData
                objParent = Registry.DynData
            Case RegistryHive.LocalMachine
                objParent = Registry.LocalMachine
            Case RegistryHive.PerformanceData
                objParent = Registry.PerformanceData
            Case RegistryHive.Users
                objParent = Registry.Users
        End Select

        Try
            objSubkey = objParent.OpenSubKey(Key)
            If Not objSubkey Is Nothing Then
                sAns = (objSubkey.GetValue(ValueName))
            End If

        Catch ex As Exception
            ErrInfo = ex.Message
        Finally
            If ErrInfo = "" And sAns = "" Then
                ErrInfo = "No value found for requested registry key, using default value"
                sAns = DefaultValue
            End If
        End Try

        Return sAns
    End Function
End Module
