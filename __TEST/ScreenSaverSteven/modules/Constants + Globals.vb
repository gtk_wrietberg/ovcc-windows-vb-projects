Module Constants___Globals
    Public gUserName As String
    Public gPassWord As String
    Public gPassWordSHA1 As String
    Public gDelay As Integer
    Public gCacheFolder As String
    Public gCachePrefix As String

    Public ReadOnly cRegRoot As String = "SOFTWARE\" & Application.CompanyName & "\" & Application.ProductName
    Public ReadOnly cStevenBirthday As Date = New Date(2008, 5, 2, 20, 44, 0)
End Module
