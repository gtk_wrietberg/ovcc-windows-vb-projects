Module SubMain
    Private ReadOnly SWP_NOACTIVATE As Integer = &H10
    Private ReadOnly SWP_NOZORDER As Integer = &H4
    Private ReadOnly SWP_SHOWWINDOW As Integer = &H40
    Private ReadOnly GWL_STYLE As Integer = -16
    Private ReadOnly WS_CHILD As Integer = &H40000000
    Private ReadOnly GWL_HWNDPARENT As Integer = -8
    Private ReadOnly HWND_TOP As Integer = 0

    Private Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer
    End Structure

    Private Declare Function GetClientRect Lib "user32" (ByVal hwnd As Integer, ByRef lpRect As RECT) As Integer
    Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hwnd As Integer, ByVal nIndex As Integer) As Integer
    Private Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Integer, ByVal nIndex As Integer, ByVal dwNewInteger As Integer) As Integer
    Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Integer, ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer) As Integer
    Private Declare Function SetParent Lib "user32" (ByVal hWndChild As Integer, ByVal hWndNewParent As Integer) As Integer

    Public Enum ActionType
        actConfigure
        actPreview
        actRun
    End Enum

    Public m_Action As ActionType

    Public Sub Main(ByVal args As String())
        LoadConfiguration()

        ' See what we should do.
        If args.Length = 0 Then
            m_Action = ActionType.actRun
            'm_Action = ActionType.actConfigure
        Else
            Select Case args(0).ToLower().Substring(0, 2)
                Case "/p"
                    m_Action = ActionType.actPreview
                Case "/c"
                    m_Action = ActionType.actConfigure
                Case "/s"
                    m_Action = ActionType.actRun
                Case Else
                    m_Action = ActionType.actRun
            End Select
        End If

        ' Do it.
        Select Case m_Action
            Case ActionType.actRun
                ' Normal screen saver.
                Dim canvas As New frmCanvas
                Application.Run(canvas)
            Case ActionType.actConfigure
                ' Configure.
                Dim dlg_config As New frmConfig
                Application.Run(dlg_config)
            Case ActionType.actPreview
                ' Preview.
                Dim nopreview As New frmPreview
                SetForm(CType(nopreview, Form), args(1))
                Application.Run(nopreview)
        End Select
    End Sub

    ' Reparent the form into the preview window.
    Private Sub SetForm(ByRef frm As Form, ByRef arg As String)
        Dim style As Integer
        Dim preview_hwnd As Integer = Integer.Parse(CType(arg, String))
        Dim r As New RECT

        ' Get the preview window's size.
        GetClientRect(preview_hwnd, r)

        With frm
            .WindowState = FormWindowState.Normal
            .FormBorderStyle = FormBorderStyle.None
            .Width = r.Right
            .Height = r.Bottom
        End With

        ' Add the WS_CHILD style to the form.
        style = GetWindowLong(frm.Handle.ToInt32, GWL_STYLE)
        style = style Or WS_CHILD
        SetWindowLong(frm.Handle.ToInt32, GWL_STYLE, style)

        ' Reparent the form into the preview window.
        SetParent(frm.Handle.ToInt32, preview_hwnd)

        ' Set the form's GWL_PARENT value to the preview window.
        SetWindowLong(frm.Handle.ToInt32, GWL_HWNDPARENT, preview_hwnd)

        ' Position the form in the preview window.
        SetWindowPos(frm.Handle.ToInt32, 0, r.Left, 0, r.Right, r.Bottom, SWP_NOACTIVATE Or SWP_NOZORDER Or SWP_SHOWWINDOW)
    End Sub
End Module
