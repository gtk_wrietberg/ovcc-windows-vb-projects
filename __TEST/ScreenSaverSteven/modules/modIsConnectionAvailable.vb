Module modIsConnectionAvailable
    Public Function IsConnectionAvailable() As Boolean
        Dim objUrl As New System.Uri("http://www.microsoft.com/")
        Dim objWebReq As System.Net.WebRequest
        objWebReq = System.Net.WebRequest.Create(objUrl)
        Dim objResp As System.Net.WebResponse
        Try
            ' Attempt to get response and return True
            objResp = objWebReq.GetResponse
            objResp.Close()
            objWebReq = Nothing
            Return True
        Catch ex As Exception
            ' Error, exit and return False
            objResp = Nothing
            objWebReq = Nothing
            Return False
        End Try
    End Function
End Module
