Module Configuration
    Public Sub LoadConfiguration()
        Dim sError As String = ""
        Dim oXor As New XorEncryption
        Dim s As String

        gUserName = oXor.Decode(CType(ReadFromRegistry(Microsoft.Win32.RegistryHive.CurrentUser, cRegRoot, "UserName", "", sError), String))

        gPassWord = oXor.Decode(CType(ReadFromRegistry(Microsoft.Win32.RegistryHive.CurrentUser, cRegRoot, "PassWord", "", sError), String))

        s = oXor.Decode(CType(ReadFromRegistry(Microsoft.Win32.RegistryHive.CurrentUser, cRegRoot, "Delay", "", sError), String))
        If IsNumeric(s) Then
            gDelay = CInt(s)
            If gDelay < 5 Then
                gDelay = 5
            End If
            If gDelay > 60 Then
                gDelay = 60
            End If
        Else
            gDelay = 30
        End If

        'gCacheFolder = oXor.Decode(CType(ReadFromRegistry(Microsoft.Win32.RegistryHive.CurrentUser, cRegRoot, "CacheFolder", IO.Path.GetTempPath(), sError), String))
        'If Not IO.Directory.Exists(gCacheFolder) Then
        gCacheFolder = IO.Path.GetTempPath
        'End If

        gCachePrefix = GetOwnGUID()

        gPassWordSHA1 = GetSHA1Hash(gPassWord)
    End Sub

    Public Sub SaveConfiguration()
        Dim oXor As New XorEncryption

        WriteToRegistry(Microsoft.Win32.RegistryHive.CurrentUser, cRegRoot, "UserName", oXor.Encode(gUserName))
        WriteToRegistry(Microsoft.Win32.RegistryHive.CurrentUser, cRegRoot, "PassWord", oXor.Encode(gPassWord))
        WriteToRegistry(Microsoft.Win32.RegistryHive.CurrentUser, cRegRoot, "Delay", oXor.Encode(CStr(gDelay)))
        WriteToRegistry(Microsoft.Win32.RegistryHive.CurrentUser, cRegRoot, "CacheFolder", oXor.Encode(gCacheFolder))
    End Sub
End Module
