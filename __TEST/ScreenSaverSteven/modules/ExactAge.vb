Module ExactAge
    Public Function ConvertAgeToString(ByVal BirthDate As Date, ByVal CompareDate As Date) As String
        Dim iYear As Integer, iMonth As Integer, iWeek As Integer, iDay As Integer
        Dim sDate As String()
        Dim i As Integer
        Dim sReturn As String

        ReDim sDate(0)

        BirthDate = CDate(BirthDate)
        If BirthDate > CompareDate Then
            Return ""
        End If

        iYear = BirthDate.Year
        iMonth = BirthDate.Month
        iDay = BirthDate.Day

        iYear = CompareDate.Year - iYear
        iMonth = CompareDate.Month - iMonth
        iDay = CompareDate.Day - iDay

        If System.Math.Sign(iDay) = -1 Then
            iDay = 30 - System.Math.Abs(iDay)
            iMonth = iMonth - 1
        End If

        If System.Math.Sign(iMonth) = -1 Then
            iMonth = 12 - System.Math.Abs(iMonth)
            iYear = iYear - 1
        End If

        iWeek = System.Math.Floor(iDay / 7)
        iDay = iDay - (7 * iWeek)

        If iYear > 0 Then
            ReDim Preserve sDate(sDate.Length)
            sDate(sDate.Length - 1) = CStr(iYear) & " jaar"
        End If

        If iMonth > 0 Then
            ReDim Preserve sDate(sDate.Length)

            If iMonth = 1 Then
                sDate(sDate.Length - 1) = CStr(iMonth) & " maand"
            Else
                sDate(sDate.Length - 1) = CStr(iMonth) & " maanden"
            End If
        End If

        If iWeek > 0 Then
            ReDim Preserve sDate(sDate.Length)

            If iWeek = 1 Then
                sDate(sDate.Length - 1) = CStr(iWeek) & " week"
            Else
                sDate(sDate.Length - 1) = CStr(iWeek) & " weken"
            End If
        End If

        If iDay > 0 Then
            ReDim Preserve sDate(sDate.Length)

            If iDay = 1 Then
                sDate(sDate.Length - 1) = CStr(iDay) & " dag"
            Else
                sDate(sDate.Length - 1) = CStr(iDay) & " dagen"
            End If
        End If

        sReturn = ""
        For i = 1 To sDate.Length - 3
            sReturn = sReturn & sDate(i) & ", "
        Next
        sReturn = sReturn & sDate(sDate.Length - 2) & " en "
        sReturn = sReturn & sDate(sDate.Length - 1)

        Return sReturn
    End Function
End Module
