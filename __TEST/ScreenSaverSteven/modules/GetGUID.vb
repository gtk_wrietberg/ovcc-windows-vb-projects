Imports System.Reflection
Imports System.Runtime.InteropServices

Module GetGUID
    Public Function GetOwnGUID() As String
        Dim assy As [Assembly]
        Dim Attributes As Object()

        assy = [Assembly].GetExecutingAssembly
        Attributes = assy.GetCustomAttributes(GetType(GuidAttribute), False)

        If Attributes.Length = 0 Then
            Return ""
        Else
            Return DirectCast(Attributes(0), GuidAttribute).Value
        End If
    End Function
End Module
