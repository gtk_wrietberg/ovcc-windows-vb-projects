﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnDisplayOff = New System.Windows.Forms.Button()
        Me.btnDisplayStandby = New System.Windows.Forms.Button()
        Me.tmrDisplayOn = New System.Windows.Forms.Timer(Me.components)
        Me.txtLog = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnDisplayOff
        '
        Me.btnDisplayOff.Location = New System.Drawing.Point(12, 12)
        Me.btnDisplayOff.Name = "btnDisplayOff"
        Me.btnDisplayOff.Size = New System.Drawing.Size(177, 90)
        Me.btnDisplayOff.TabIndex = 0
        Me.btnDisplayOff.Text = "Display Off"
        Me.btnDisplayOff.UseVisualStyleBackColor = True
        '
        'btnDisplayStandby
        '
        Me.btnDisplayStandby.Location = New System.Drawing.Point(12, 108)
        Me.btnDisplayStandby.Name = "btnDisplayStandby"
        Me.btnDisplayStandby.Size = New System.Drawing.Size(177, 90)
        Me.btnDisplayStandby.TabIndex = 1
        Me.btnDisplayStandby.Text = "Display stand by"
        Me.btnDisplayStandby.UseVisualStyleBackColor = True
        '
        'tmrDisplayOn
        '
        Me.tmrDisplayOn.Interval = 5000
        '
        'txtLog
        '
        Me.txtLog.Location = New System.Drawing.Point(204, 12)
        Me.txtLog.Multiline = True
        Me.txtLog.Name = "txtLog"
        Me.txtLog.ReadOnly = True
        Me.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtLog.Size = New System.Drawing.Size(507, 401)
        Me.txtLog.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(723, 425)
        Me.Controls.Add(Me.txtLog)
        Me.Controls.Add(Me.btnDisplayStandby)
        Me.Controls.Add(Me.btnDisplayOff)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnDisplayOff As System.Windows.Forms.Button
    Friend WithEvents btnDisplayStandby As System.Windows.Forms.Button
    Friend WithEvents tmrDisplayOn As System.Windows.Forms.Timer
    Friend WithEvents txtLog As System.Windows.Forms.TextBox

End Class
