﻿Imports System.Runtime.InteropServices

Public Class clsDisplayOnOff
    <DllImport("user32.dll")> _
    Private Shared Function SendMessage(hWnd As Integer, hMsg As Integer, wParam As Integer, lParam As Integer) As Integer
    End Function

    Private Enum MonitorState
        MonitorStateOn = -1
        MonitorStateOff = 2
        MonitorStateStandBy = 1
    End Enum

    Public Shared Sub DisplayOn()
        SetMonitorInState(MonitorState.MonitorStateOn)
    End Sub

    Public Shared Sub DisplayOff()
        SetMonitorInState(MonitorState.MonitorStateOff)
    End Sub

    Public Shared Sub DisplayStandBy()
        SetMonitorInState(MonitorState.MonitorStateStandBy)
    End Sub

    Private Shared Sub SetMonitorInState(state As MonitorState)
        SendMessage(&HFFFF, &H112, &HF170, CInt(state))
    End Sub
End Class
