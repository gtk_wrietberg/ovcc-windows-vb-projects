﻿Public Class Form1

    Private Sub btnDisplayOff_Click(sender As Object, e As EventArgs) Handles btnDisplayOff.Click
        LogMessage("DisplayOff init")
        clsDisplayOnOff.DisplayOff()
        LogMessage("DisplayOff done")

        LogMessage("tmrDisplayOn start")
        tmrDisplayOn.Enabled = True
    End Sub

    Private Sub btnDisplayStandby_Click(sender As Object, e As EventArgs) Handles btnDisplayStandby.Click
        LogMessage("DisplayStandby init")
        clsDisplayOnOff.DisplayStandBy()
        LogMessage("DisplayStandby done")

        LogMessage("tmrDisplayOn start")
        tmrDisplayOn.Enabled = True
    End Sub

    Private Sub tmrDisplayOn_Tick(sender As Object, e As EventArgs) Handles tmrDisplayOn.Tick
        LogMessage("tmrDisplayOn tick")
        tmrDisplayOn.Enabled = False

        LogMessage("DisplayOn init")
        clsDisplayOnOff.DisplayOn()
        LogMessage("DisplayOn done")
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub LogMessage(msg As String)
        txtLog.AppendText(Now.ToString("HH:mm:ss.fff") & " - " & msg & vbCrLf)
    End Sub
End Class
