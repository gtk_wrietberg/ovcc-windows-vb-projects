Public Class SquareClass
    Public Value As Double
    Public Square As Double

    Public Event ThreadComplete(ByVal Square As Double)
    Public Event ThreadStarted()

    Public Sub CalcSquare()
        RaiseEvent ThreadStarted()

        Square = Value * Value
        Threading.Thread.Sleep(5000)

        RaiseEvent ThreadComplete(Square)
    End Sub
End Class