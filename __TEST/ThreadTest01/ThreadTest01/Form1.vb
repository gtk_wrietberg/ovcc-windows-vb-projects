Imports System.ComponentModel
Imports System.Threading

Public Class Form1
    Public WithEvents oSquare As SquareClass

    Delegate Sub SetTextCallback(ByVal [text] As String)
    Private demoThread As Thread = Nothing

    Private iTestValue As Integer = 0

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""

        Me.demoThread = New Thread(New ThreadStart(AddressOf Me.ThreadProcSafe))
        Me.demoThread.Start()
    End Sub

    Private Sub ThreadProcSafe()
        oSquare = New SquareClass

        Try
            oSquare.Value = CDbl(TextBox1.Text)
        Catch ex As Exception
            oSquare.Value = 4
            Me.SetTextBox1("4")
        End Try

        oSquare.CalcSquare()

        'Me.SetText("This text was set safely.")
    End Sub

    Private Sub SetTextBox1(ByVal [text] As String)
        If Me.TextBox1.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetTextBox1)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.TextBox1.Text = [text]
        End If
    End Sub

    Private Sub SetTextBox2(ByVal [text] As String)
        If Me.TextBox2.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetTextBox2)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.TextBox2.Text = [text]
        End If
    End Sub

    Private Sub SetTextBox3(ByVal [text] As String)
        If Me.TextBox3.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetTextBox3)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.TextBox3.Text = [text]
        End If
    End Sub

    Private Sub SetTextBox4(ByVal [text] As String)
        If Me.TextBox4.InvokeRequired Then
            Dim d As New SetTextCallback(AddressOf SetTextBox4)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.TextBox4.Text = [text]
        End If
    End Sub


    Private Sub a()
        



        Dim t As New Threading.Thread(AddressOf oSquare.CalcSquare)

        Try
            oSquare.Value = CDbl(TextBox1.Text)
        Catch ex As Exception
            oSquare.Value = 4
            TextBox1.Text = "4"
        End Try

        t.Start()
    End Sub

    Private Sub oSquare_ThreadComplete(ByVal Square As Double) Handles oSquare.ThreadComplete
        Me.SetTextBox4(Now.ToLongTimeString)

        Me.SetTextBox2(oSquare.Square)
    End Sub


    Private Sub oSquare_ThreadStarted() Handles oSquare.ThreadStarted
        Me.SetTextBox3(Now.ToLongTimeString)
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim s As String

        s = "dates" & vbCrLf
        s &= "Now.ToString          : " & Now.ToString & vbCrLf
        s &= "Now.ToShortDateString : " & Now.ToShortDateString & vbCrLf
        s &= "Now.ToShortTimeString : " & Now.ToShortTimeString & vbCrLf
        s &= "Now.ToLongDateString  : " & Now.ToLongDateString & vbCrLf
        s &= "Now.ToLongTimeString  : " & Now.ToLongTimeString & vbCrLf

        MsgBox(s)

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        iTestValue = iTestValue Or 1

        UpdateTestValue()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        iTestValue = iTestValue Or 2

        UpdateTestValue()
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        iTestValue = iTestValue Or 4

        UpdateTestValue()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        iTestValue = iTestValue Xor 1

        UpdateTestValue()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        iTestValue = iTestValue Xor 2

        UpdateTestValue()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        iTestValue = iTestValue Xor 4

        UpdateTestValue()
    End Sub

    Private Sub UpdateTestValue()
        TextBox5.Text = iTestValue
    End Sub
End Class
