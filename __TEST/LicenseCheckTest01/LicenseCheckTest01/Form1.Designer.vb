<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtL1 = New System.Windows.Forms.TextBox
        Me.txtL2 = New System.Windows.Forms.TextBox
        Me.txtL3 = New System.Windows.Forms.TextBox
        Me.txtL4 = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(12, 12)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TextBox1.Size = New System.Drawing.Size(551, 203)
        Me.TextBox1.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(630, 137)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(150, 154)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtL1
        '
        Me.txtL1.Location = New System.Drawing.Point(57, 342)
        Me.txtL1.MaxLength = 32
        Me.txtL1.Name = "txtL1"
        Me.txtL1.Size = New System.Drawing.Size(80, 21)
        Me.txtL1.TabIndex = 2
        Me.txtL1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtL2
        '
        Me.txtL2.Location = New System.Drawing.Point(143, 342)
        Me.txtL2.MaxLength = 32
        Me.txtL2.Name = "txtL2"
        Me.txtL2.Size = New System.Drawing.Size(80, 21)
        Me.txtL2.TabIndex = 3
        Me.txtL2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtL3
        '
        Me.txtL3.Location = New System.Drawing.Point(229, 342)
        Me.txtL3.MaxLength = 32
        Me.txtL3.Name = "txtL3"
        Me.txtL3.Size = New System.Drawing.Size(80, 21)
        Me.txtL3.TabIndex = 4
        Me.txtL3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtL4
        '
        Me.txtL4.Location = New System.Drawing.Point(315, 342)
        Me.txtL4.MaxLength = 32
        Me.txtL4.Name = "txtL4"
        Me.txtL4.Size = New System.Drawing.Size(80, 21)
        Me.txtL4.TabIndex = 5
        Me.txtL4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(415, 346)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(88, 16)
        Me.Button2.TabIndex = 6
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(534, 440)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(216, 149)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(817, 646)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.txtL4)
        Me.Controls.Add(Me.txtL3)
        Me.Controls.Add(Me.txtL2)
        Me.Controls.Add(Me.txtL1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TextBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtL1 As System.Windows.Forms.TextBox
    Friend WithEvents txtL2 As System.Windows.Forms.TextBox
    Friend WithEvents txtL3 As System.Windows.Forms.TextBox
    Friend WithEvents txtL4 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button

End Class
