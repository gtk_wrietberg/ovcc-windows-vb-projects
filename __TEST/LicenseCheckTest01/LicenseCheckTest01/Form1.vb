Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Xml

Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ' Create a Request using a URL that can receive a post.
        Dim Request As WebRequest = WebRequest.Create("http://172.18.192.10/standalone_license/requesthandler.asp")
        Request.Method = "POST"

        TextBox1.Text += vbCrLf
        TextBox1.Text += vbCrLf

        Button1.Text = "please wait"
        Button1.Enabled = False
        Button1.Refresh()


        Dim PostData As String = "<iBAHN><StandAlone><License>4B552126d0a1b84b51ef2952f4f4b099</License></StandAlone></iBAHN>"
        Dim ByteArray As Byte() = Encoding.UTF8.GetBytes(PostData)
        Dim Instance As New WebException
        Dim Value As WebExceptionStatus
        ' Set the ContentType property of the WebRequest.
        'Request.ContentType = "application/x-www-form-urlencoded"
        Request.ContentType = "text/xml"
        'Request.ContentType = "XML"
        ' Set the ContentLength property of the WebRequest.
        Request.ContentLength = ByteArray.Length
        ' Get the Request stream.
        Dim dataStream As Stream = Request.GetRequestStream()
        ' Write the data to the Request stream.
        dataStream.Write(ByteArray, 0, ByteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Value = Instance.Status
        Dim response As WebResponse = Request.GetResponse()
        ' Display the status.
        Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        ' Open the stream using a StreamReader for easy access.
        Dim reader As New StreamReader(dataStream)
        ' Read the content.
        Dim responseFromServer As String = reader.ReadToEnd()

        ' Clean up the streams.
        reader.Close()
        dataStream.Close()
        response.Close()

        TextBox1.Text += responseFromServer

        Dim xmldoc As New Xml.XmlDocument
        Dim xmlRoot As Xml.XmlNode
        Dim xmlNodeLicense As Xml.XmlNode

        xmldoc.LoadXml(responseFromServer)

        xmlRoot = xmldoc.SelectSingleNode("iBAHN")
        xmlNodeLicense = xmlRoot.SelectSingleNode("StandAlone").SelectSingleNode("License")

        'MsgBox(xmlNodeLicense.SelectSingleNode("ReturnMessage").InnerText)

        '   <iBAHN>
        '   <StandAlone>
        '   <License>
        '   <ReturnCode>2</ReturnCode>
        '   <ReturnMessage>License is valid but expired</ReturnMessage>
        '   <LicenseCount>3</LicenseCount>
        '   <LicenseCountMax>3</LicenseCountMax>
        '   <Workgroup>LPC_STANDALONE</Workgroup>
        '   <MachineName>ENG_STANDALONE_TEST</MachineName>
        '   </License>
        '   </StandAlone>
        '   </iBAHN>

        Button1.Text = "validate"
        Button1.Enabled = True
        Button1.Refresh()
    End Sub

    Private Function _zero_padding(ByVal iNum As Integer, ByVal iZeros As Integer) As String
        Dim sNum As String

        sNum = iNum.ToString
        If sNum.Length < iZeros Then
            sNum = (New String("0", iZeros - sNum.Length)) & sNum
        End If

        Return sNum
    End Function

    Private Sub txtL1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtL1.TextChanged
        If txtL1.Text.Length > 8 Then
            txtL2.Text = txtL1.Text.Substring(8)
            txtL1.Text = txtL1.Text.Substring(0, 8)
        End If
        If txtL1.Text.Length = 8 Then
            txtL2.Focus()
        End If
        If txtL1.Text.Length = 8 And txtL2.Text.Length = 8 And txtL3.Text.Length = 8 And txtL4.Text.Length = 8 Then
            Button2.Focus()
        End If
    End Sub

    Private Sub txtL2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtL2.TextChanged
        If txtL2.Text.Length > 8 Then
            txtL3.Text = txtL2.Text.Substring(8)
            txtL2.Text = txtL2.Text.Substring(0, 8)
        End If
        If txtL2.Text.Length = 8 Then
            txtL3.Focus()
        End If
    End Sub

    Private Sub txtL3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtL3.TextChanged
        If txtL3.Text.Length > 8 Then
            txtL4.Text = txtL3.Text.Substring(8)
            txtL3.Text = txtL3.Text.Substring(0, 8)
        End If
        If txtL3.Text.Length = 8 Then
            txtL4.Focus()
        End If
    End Sub

    Private Sub txtL4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtL4.TextChanged
        If txtL4.Text.Length > 8 Then
            txtL4.Text = txtL4.Text.Substring(0, 8)
        End If
        If txtL4.Text.Length = 8 Then
            Button2.Focus()
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Button1.Text = "validate"
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        MsgBox(_zero_padding(10, 3))
    End Sub
End Class
