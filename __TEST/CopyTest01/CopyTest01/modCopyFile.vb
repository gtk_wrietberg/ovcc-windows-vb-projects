Module modCopyFile
    Public Function CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, Optional ByVal iLogLevel As Integer = 0) As Boolean
        Dim lFileSizeBefore As Long, lFileSizeAfter As Long
        Dim bContinue As Boolean = False

        oLogger.WriteToLog("source file", , iLogLevel)
        oLogger.WriteToLog("path: " & sSourcePath & "\" & sFileName, , iLogLevel + 1)
        Try
            If Not IO.File.Exists(sSourcePath & "\" & sFileName) Then
                oLogger.WriteToLog("does not exist!", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
                Return False
            End If
        Catch ex As Exception
            oLogger.WriteToLog("Exception occurred!", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 3)

            Return False
        End Try


        lFileSizeBefore = GetFileSize(sSourcePath & "\" & sFileName, iLogLevel + 1)
        If lFileSizeBefore < 0 Then
            oLogger.WriteToLog("error", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            Return False
        End If
        If lFileSizeBefore = 0 Then
            oLogger.WriteToLog("empty file", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            Return False
        End If
        oLogger.WriteToLog("size: " & lFileSizeBefore.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)


        bContinue = False
        oLogger.WriteToLog("copying", , iLogLevel)
        Try
            IO.File.Copy(sSourcePath & "\" & sFileName, sDestinationPath & "\" & sFileName, True)
            oLogger.WriteToLog("ok", , iLogLevel + 1)
            bContinue = True
        Catch ex As IO.DirectoryNotFoundException
            oLogger.WriteToLog("IO.DirectoryNotFoundException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        Catch ex As UnauthorizedAccessException
            oLogger.WriteToLog("UnauthorizedAccessException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        Catch ex As NotSupportedException
            oLogger.WriteToLog("NotSupportedException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        Catch ex As IO.PathTooLongException
            oLogger.WriteToLog("IO.PathTooLongException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        Catch ex As ArgumentNullException
            oLogger.WriteToLog("ArgumentNullException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        Catch ex As IO.FileNotFoundException
            oLogger.WriteToLog("IO.FileNotFoundException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        Catch ex As ArgumentException
            oLogger.WriteToLog("ArgumentException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        Catch ex As IO.IOException
            oLogger.WriteToLog("IO.IOException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        Catch ex As Exception
            oLogger.WriteToLog("Exception", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
        End Try

        If Not bContinue Then
            Return False
        End If


        oLogger.WriteToLog("destination file", , iLogLevel)
        oLogger.WriteToLog("path: " & sDestinationPath & "\" & sFileName, , iLogLevel + 1)
        Try
            If Not IO.File.Exists(sDestinationPath & "\" & sFileName) Then
                oLogger.WriteToLog("does not exist!", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
                Return False
            End If
        Catch ex As Exception
            oLogger.WriteToLog("Exception occurred!", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 3)

            Return False
        End Try


        lFileSizeAfter = GetFileSize(sDestinationPath & "\" & sFileName, iLogLevel + 1)
        If lFileSizeAfter < 0 Then
            oLogger.WriteToLog("error", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            Return False
        End If
        If lFileSizeAfter = 0 Then
            oLogger.WriteToLog("empty file", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            Return False
        End If
        oLogger.WriteToLog("size: " & lFileSizeAfter.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)


        If lFileSizeBefore <> lFileSizeAfter Then
            oLogger.WriteToLog("file sizes don't match", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel)

            Return False
        End If


        Return True
    End Function

    Private Function GetFileSize(ByVal sFilePath As String, ByVal iLogLevel As Integer) As Long
        Dim lFileSize As Long

        Try
            Dim MyFile As New IO.FileInfo(sFilePath)

            lFileSize = MyFile.Length
        Catch ex As IO.FileNotFoundException
            oLogger.WriteToLog("IO.FileNotFoundException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)

            lFileSize = -1
        Catch ex As IO.IOException
            oLogger.WriteToLog("IO.IOException", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)

            lFileSize = -2
        Catch ex As Exception
            oLogger.WriteToLog("Exception", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
            oLogger.WriteToLog("Exception.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 2)

            lFileSize = -3
        End Try

        Return lFileSize
    End Function
End Module
