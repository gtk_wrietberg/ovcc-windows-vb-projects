Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oLogger = New Logger

        oLogger.WriteToLog(New String("*", 50))
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        oLogger.WriteToLog(TextBox1.Text, , 1)
        _CopyFile(TextBox1.Text, TextBox2.Text, TextBox3.Text, 2)


        oLogger.WriteToLog(TextBox4.Text, , 1)
        _CopyFile(TextBox4.Text, TextBox2.Text, TextBox3.Text, 2) 

        oLogger.WriteToLog(TextBox5.Text, , 1)
        _CopyFile(TextBox5.Text, TextBox2.Text, TextBox3.Text, 2) 
    End Sub

    Private Sub _CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, ByVal iLogLevel As Integer)
        oLogger.WriteToLog("copying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)

        oLogger.WriteToLog("retrying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
    End Sub
End Class
