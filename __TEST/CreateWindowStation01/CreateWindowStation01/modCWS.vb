﻿Module modCWS
    Structure RECT
        Public left, top, right, bottom As Integer
    End Structure

    Structure GUITHREADINFO
        Public cbSize As Integer
        Public flags As Integer
        Public hwndActive As IntPtr
        Public hwndFocus As IntPtr
        Public hwndCapture As IntPtr
        Public hwndMenuOwner As IntPtr
        Public hwndMoveSize As IntPtr
        Public hwndCaret As IntPtr
        Public rcCaret As RECT
    End Structure

    Structure SECURITY_ATTRIBUTES
        Public nLength As Integer
        Public lpSecurityDescriptor As IntPtr
        Public bInheritHandle As Boolean
    End Structure

    Declare Auto Function CreateWindowStation Lib "user32.dll" (lpwinsta As String, dwFlags As Integer, dwDesiredAccess As Integer, lpsa As IntPtr) As IntPtr
    Declare Function SetProcessWindowStation Lib "user32.dll" (hWinSta As IntPtr) As Boolean
    Declare Auto Function CreateDesktop Lib "user32.dll" (lpszDesktop As String, lpszDevice As IntPtr, pDevmode As IntPtr, dwFlags As Integer, dwDesiredAccess As Integer, lpsa As IntPtr) As IntPtr
    Declare Function SetThreadDesktop Lib "user32.dll" (hDesktop As IntPtr) As Boolean
    Declare Function GetGUIThreadInfo Lib "user32.dll" (idThread As Integer, ByRef lpgui As GUITHREADINFO) As Boolean

    Public Sub TeSt()
        Dim ptrRet As IntPtr
        Dim sa As SECURITY_ATTRIBUTES

        sa.nLength = 1


        ptrRet = CreateWindowStation("Test001", 1, 1, 1)
    End Sub
End Module
