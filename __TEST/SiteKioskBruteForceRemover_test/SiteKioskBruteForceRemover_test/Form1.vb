Public Class Form1
    Public ReadOnly cCLEANFOLDER_SITEKIOSK As String = "c:\Program Files\SiteKiosk"
    Private oLogger As New Logger

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        _UninstallSiteKioskBruteForce()
    End Sub

    Private Sub _UninstallSiteKioskBruteForce()
        oLogger.WriteToLog("Uninstalling SiteKiosk - BRUTE FORCE")

        oLogger.WriteToLog("Killing SiteKiosk processes", , 1)
        _KillProcess("Configure")
        _KillProcess("SiteKiosk")
        _KillProcess("skpcdsvc")
        _KillProcess("SmartCard")
        _KillProcess("SystemSecurity")
        _KillProcess("Watchdog")
        _KillProcess("SessionMonitor")
        _KillProcess("SiteRemoteClientService")
        _KillProcess("SKScreenshot")
        _KillProcess("msiexec")

        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK)
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu) & "\Programs\SiteKiosk")

        __CleanUpLocalUser("sitekiosk")
        __CleanUpFolder("C:\Documents and Settings\sitekiosk")

        Dim rk_SERVICES As Microsoft.Win32.RegistryKey

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services", , 1)
            rk_SERVICES = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services", True)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)

            ErrorDetected()
            Exit Sub
        End Try

        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\skpcdsvc
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteRemote Client

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\skpcdsvc", , 1)
            rk_SERVICES.DeleteSubKeyTree("skpcdsvc")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey", , 1)
            rk_SERVICES.DeleteSubKeyTree("SiteKey")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteRemote Client", , 1)
            rk_SERVICES.DeleteSubKeyTree("SiteRemote Client")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{ADE43478-C0B2-422D-B111-4DF94171B6C3}
        Dim sId As String
        Dim rk_UNINSTALL As Microsoft.Win32.RegistryKey

        sId = GetUninstallIdentifier("sitekiosk")

        If sId <> "" Then
            Try
                oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", , 1)
                rk_UNINSTALL = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", True)
                oLogger.WriteToLog("ok", , 2)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 2)

                ErrorDetected()
                Exit Sub
            End Try

            Try
                oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{" & sId & "}", , 1)
                rk_UNINSTALL.DeleteSubKeyTree("{" & sId & "}")
                oLogger.WriteToLog("ok", , 2)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 2)
            End Try
        End If
    End Sub

    Private Sub _KillProcess(ByVal sProcessName As String)
        Dim p As System.Diagnostics.Process

        oLogger.WriteToLog(sProcessName, , 2)
        For Each p In System.Diagnostics.Process.GetProcessesByName(sProcessName)
            Try
                p.Kill()
                oLogger.WriteToLog("dead", , 3)
            Catch ex As Exception
                oLogger.WriteToLog("zombie", , 3)
            End Try
        Next
    End Sub

    Public Function GetUninstallIdentifier(ByVal sSearchForApplication As String) As String
        Dim KeyName As String
        Dim Value_DisplayName As String
        Dim Value_UninstallString As String = ""
        Dim UninstallRegKey As Microsoft.Win32.RegistryKey
        Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

        UninstallRegKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", False)

        If sSearchForApplication = "" Then
            'Yes that's right
            sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
        End If

        sSearchForApplication = sSearchForApplication.ToLower

        For Each KeyName In UninstallRegKey.GetSubKeyNames
            ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

            Try
                Value_DisplayName = ApplicationRegKey.GetValue("DisplayName", "").ToString.ToLower
            Catch ex As Exception
                Value_DisplayName = ""
            End Try

            If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                Try
                    Value_UninstallString = ApplicationRegKey.GetValue("UninstallString", "").ToString
                Catch ex As Exception
                    Value_UninstallString = ""
                End Try

                Value_UninstallString = System.Text.RegularExpressions.Regex.Replace(Value_UninstallString, ".*\{(.+)\}.*", "$1")

                Exit For
            End If
        Next

        Return Value_UninstallString
    End Function

    Private Sub __CleanUpFolder(ByVal sFolder As String)
        oLogger.WriteToLog("removing: " & sFolder, , 1)
        If System.IO.Directory.Exists(sFolder) Then
            Try
                __ClearAttributes(sFolder)
                System.IO.Directory.Delete(sFolder, True)
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                ErrorDetected()
            End Try
            If System.IO.Directory.Exists(sFolder) Then
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                ErrorDetected()
            Else
                oLogger.WriteToLog("ok", , 2)
            End If
        Else
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub __CleanUpFile(ByVal sFile As String)
        oLogger.WriteToLog("removing: " & sFile, , 1)
        If System.IO.File.Exists(sFile) Then
            Try
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
                System.IO.File.Delete(sFile)
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                ErrorDetected()
            End Try
            If System.IO.File.Exists(sFile) Then
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                ErrorDetected()
            Else
                oLogger.WriteToLog("ok", , 2)
            End If
        Else
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub __ClearAttributes(ByVal sCurrentFolder As String)
        If System.IO.Directory.Exists(sCurrentFolder) Then
            Dim sSubFolders() As String = System.IO.Directory.GetDirectories(sCurrentFolder)
            Dim sSubFolder As String

            For Each sSubFolder In sSubFolders
                System.IO.File.SetAttributes(sSubFolder, IO.FileAttributes.Normal)
                __ClearAttributes(sSubFolder)
            Next

            Dim sFiles() As String = System.IO.Directory.GetFiles(sCurrentFolder)
            Dim sFile As String

            For Each sFile In sFiles
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
            Next
        End If
    End Sub

    Private Sub ErrorDetected()

    End Sub

    Private Sub __CleanUpLocalUser(ByVal sUserName As String)
        oLogger.WriteToLog("removing user: " & sUserName, , 1)

        If WindowsUser.RemoveUser(sUserName) Then
            oLogger.WriteToLog("ok", , 2)
        Else
            oLogger.WriteToLog("fail (does not exist?)", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

End Class
