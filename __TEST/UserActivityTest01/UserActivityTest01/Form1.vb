﻿Public Class Form1


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Timer1.Enabled = True
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Timer1.Enabled = False
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        '
        'This timer will fire every 1000ms(One Second) or so displaying the last time the user was active.
        '
        'The size of the structure for the API call.
        info.structSize = Len(info)
        '
        'Call the API.
        GetLastInputInfo(info)
        '
        'Compare the tickcount values to determine if activity has occurred or not.
        If firstTick <> info.tickCount Then
            '
            'Display the current time of the users last activity. 
            '
            'Change lblTime.Caption to lblTime.Text if using .NET and likewise change Time
            'to Now instead.
            TextBox1.Text = "Last Active: " & Date.Now
            '
            'Get the new tick value.
            firstTick = info.tickCount

        End If
    End Sub
End Class
