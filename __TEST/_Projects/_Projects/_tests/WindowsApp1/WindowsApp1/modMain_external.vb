﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Environment
Imports System.IO
Imports System.IO.Compression
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports System.Reflection
Imports System.Diagnostics
Imports System.Text


Module modMain_external
    Public Sub Main()
        Dim ass As Assembly = Assembly.GetExecutingAssembly()
        Dim res As String() = ass.GetManifestResourceNames()

        Try
            'First get settings and file list
            For Each name As String In res
                If name.Equals(Shared_Constants.SettingsFileName) Then
                    'settings
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        Settings.Parse(streamText)

                        reader.Close()
                    End Using
                End If

                If name.Equals(Shared_Constants.FileListFileName) Then
                    'filelist
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        FileList.Parse(streamText)

                        reader.Close()
                    End Using
                End If
            Next

            MessageBox.Show(Settings.Dump)
            MessageBox.Show(FileList.Dump)

            Dim location As String = System.Environment.GetCommandLineArgs()(0)
            Dim appName As String = System.IO.Path.GetFileName(location)

            'Dim basepath As String = Path.Combine(Path.GetDirectoryName(ass.Location), Path.GetFileNameWithoutExtension(appName))
            Dim basepath As String = Settings.BaseDir

            If MessageBox.Show(basepath, "Continue?", MessageBoxButtons.YesNo) = DialogResult.No Then
                Throw New Exception("User canceled!")
            End If

            For Each name As String In res
                If name.Equals(Shared_Constants.FileListFileName) Or name.Equals(Shared_Constants.SettingsFileName) Then
                    Continue For
                End If

                Dim rs As Stream = ass.GetManifestResourceStream(name)

                Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                    Dim relative_path As String = FileList.GetNext
                    Dim fullpath As String = Path.Combine(basepath, relative_path)
                    Dim filepath As String = Path.Combine(fullpath, Path.GetFileNameWithoutExtension(name))

                    If Not IO.Directory.Exists(fullpath) Then
                        IO.Directory.CreateDirectory(fullpath)
                    End If



                    'MessageBox.Show("name: '" & name & "' - name_fixed: '" & Path.GetFileNameWithoutExtension(name) & "' - filepath: '" & filepath & "' - relative_path: '" & relative_path & "'")
                    'MessageBox.Show("filepath: '" & filepath & "' - relative_path: '" & relative_path & "'")
                    MessageBox.Show("fullpath: '" & fullpath & "' - filepath: '" & filepath & "'")

                    Using unpacked_file As Stream = File.Create(filepath)
                        Dim b As Integer = gzip.ReadByte()

                        While b <> -1
                            unpacked_file.WriteByte(CByte(b))
                            b = gzip.ReadByte()
                        End While
                    End Using
                End Using

            Next




        Catch ex As Exception
            MessageBox.Show(ex.Message, ass.GetName().Name, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try
    End Sub
End Module
