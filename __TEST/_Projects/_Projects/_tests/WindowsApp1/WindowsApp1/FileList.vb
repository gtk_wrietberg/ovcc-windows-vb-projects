﻿Imports System
Imports System.Collections.Generic

Public Class FileList
    Private Shared mFiles As New List(Of String)
    Private Shared mPaths As New List(Of String)
    Private Shared mCount As Long = -1

    Public Shared Sub Reset()
        mFiles = New List(Of String)
        mPaths = New List(Of String)
        mCount = -1
    End Sub

    Public Shared Sub Add(file As String, path As String)
        mFiles.Add(file)
        mPaths.Add(path)
    End Sub

    Public Shared Function Parse(str As String) As Boolean
        Reset()

        Dim TextLines() As String = str.Split(Environment.NewLine.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

        For Each textline As String In TextLines
            Dim keyvaluepair() As String = textline.Split("*".ToCharArray(), 2)

            If keyvaluepair.Length = 2 Then
                'ok
                Add(keyvaluepair(0), keyvaluepair(1))
            End If
        Next

        Return True
    End Function

    Public Shared Function Dump() As String
        Dim str As String = ""

        For i As Integer = 0 To mFiles.Count - 1
            str &= mFiles.Item(i) & "*" & mPaths.Item(i)
            str &= Environment.NewLine
        Next

        Return str
    End Function

    Public Shared Function GetNext() As String
        mCount += 1

        If mCount >= mPaths.Count Then
            Return ""
        End If

        Return mPaths.Item(mCount)
    End Function
End Class
