﻿Imports System


Public Class Settings
    Private Shared mBaseDir As String = ""
    Public Shared Property BaseDir() As String
        Get
            Return mBaseDir
        End Get
        Set(ByVal value As String)
            mBaseDir = value
        End Set
    End Property

    Private Shared mAutoExec As String = ""
    Public Shared Property AutoExec() As String
        Get
            Return mAutoExec
        End Get
        Set(ByVal value As String)
            mAutoExec = value
        End Set
    End Property

    Public Shared Function Parse(str As String) As Boolean
        Dim TextLines() As String = str.Split(Environment.NewLine.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

        For Each textline As String In TextLines
            Dim keyvaluepair() As String = textline.Split("=".ToCharArray(), 2)

            If keyvaluepair.Length = 2 Then
                'ok
                Select Case keyvaluepair(0)
                    Case "BaseDir"
                        mBaseDir = keyvaluepair(1)
                    Case "AutoExec"
                        mAutoExec = keyvaluepair(1)
                End Select
            End If
        Next

        Return True
    End Function

    Public Shared Function Dump() As String
        Dim str As String = ""

        str &= "BaseDir=" & mBaseDir
        str &= Environment.NewLine

        str &= "AutoExec=" & mAutoExec
        str &= Environment.NewLine

        Return str
    End Function
End Class