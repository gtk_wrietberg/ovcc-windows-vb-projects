﻿Public Class PackedFiles
    Private Shared mFiles_RealPath As New List(Of String)
    Private Shared mFiles_ArchivePath As New List(Of String)

    Private Shared mCurrentRelativePath As String = ""

    Private Shared mCount As Integer = -1

    Public Shared Sub Reset()
        mFiles_RealPath = New List(Of String)
        mFiles_ArchivePath = New List(Of String)
        mCurrentRelativePath = ""

        ResetCount()
    End Sub

    Public Shared Sub ResetCount()
        mCount = -1
    End Sub

    Public Shared Sub Add(real_path As String, archive_path As String)
        mFiles_RealPath.Add(real_path)
        mFiles_ArchivePath.Add(archive_path)
    End Sub

    Public Shared Sub Delete(archive_path As String)
        Dim index As Integer = mFiles_ArchivePath.IndexOf(archive_path)

        If index > -1 Then
            mFiles_RealPath.RemoveAt(index)
            mFiles_ArchivePath.RemoveAt(index)
        End If
    End Sub

    Public Shared Sub Sort()
        Dim swaps As Boolean = True

        While swaps = True
            swaps = False

            For i As Integer = 0 To mFiles_ArchivePath.Count - 2
                If mFiles_ArchivePath(i) > mFiles_ArchivePath(i + 1) Then
                    Dim temp As String = mFiles_ArchivePath(i + 1)
                    mFiles_ArchivePath(i + 1) = mFiles_ArchivePath(i)
                    mFiles_ArchivePath(i) = temp

                    temp = mFiles_RealPath(i + 1)
                    mFiles_RealPath(i + 1) = mFiles_RealPath(i)
                    mFiles_RealPath(i) = temp

                    swaps = True
                End If
            Next
        End While
    End Sub

    Public Shared Function GetNext(ByRef real_path As String, ByRef archive_path As String) As Boolean
        mCount += 1

        If mCount >= mFiles_ArchivePath.Count Then
            real_path = ""
            archive_path = ""

            Return False
        End If

        real_path = mFiles_RealPath.Item(mCount)
        archive_path = mFiles_ArchivePath.Item(mCount)

        Return True
    End Function
End Class
