﻿Imports System.ComponentModel

Public Class frmMain
    Private sdf As New ArchiveCompiler.SelfExtractor

    'Private BindingList<IconFileInfo> files = new BindingList<IconFileInfo>();
    Private files2pack As New BindingList(Of IconFileInfo)



    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dataGridView1.AutoGenerateColumns = False
        dataGridView1.DataSource = files2pack


        'Settings.AutoExec = "123 hopsasa"
        'Settings.BaseDir = "D:\Temp\___test__test_test\_UnPaCkInGbAsEfOlDeR"
        'FileList.Reset()

        'sdf.AddSettingsFile()

        'sdf.AddFile("D:\Temp\___test__test_test\_src\1\1.txt", "poep")
        'sdf.AddFile("D:\Temp\___test__test_test\_src\2\2.txt", "kak")
        'sdf.AddFile("D:\Temp\___test__test_test\_src\3\3.txt", "schijt")
        ''sdf.AddFile("D:\Temp\___test__test_test\_src\Gaia_s_sky_in_colour.png", "")

        'sdf.AddFileListFile()

        'sdf.CompileArchive("D:\Temp\___test__test_test\_unpacked\archive.test.exe", """D:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\TextTemplate.ico""")

        'sdf.Dispose()

    End Sub

    Private Sub frmMain_DragEnter(sender As Object, e As DragEventArgs) Handles Me.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Private Sub frmMain_DragDrop(sender As Object, e As DragEventArgs) Handles Me.DragDrop

        '     foreach(String filename In (String[])e.Data.GetData(DataFormats.FileDrop))
        '{
        '	files.Add(New IconFileInfo(filename));
        '}

        For Each filename As String In CType(e.Data.GetData(DataFormats.FileDrop), String())
            files2pack.Add(New IconFileInfo(filename))
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Settings.AutoExec = "123 hopsasa"
        Settings.BaseDir = "D:\Temp\___test__test_test\_UnPaCkInGbAsEfOlDeR"
        FileList.Reset()

        sdf.AddSettingsFile()

        sdf.AddFile("D:\Temp\___test__test_test\_src\1\1.txt", "poep", True, "/test /test2 /param")
        sdf.AddFile("D:\Temp\___test__test_test\_src\2\2.txt", "kak")
        sdf.AddFile("D:\Temp\___test__test_test\_src\3\3.txt", "schijt")
        'sdf.AddFile("D:\Temp\___test__test_test\_src\Gaia_s_sky_in_colour.png", "")

        sdf.AddFileListFile()

        sdf.CompileArchive("D:\Temp\___test__test_test\_unpacked\archive.test.exe", """D:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\TextTemplate.ico""")

        sdf.Dispose()
    End Sub
End Class
