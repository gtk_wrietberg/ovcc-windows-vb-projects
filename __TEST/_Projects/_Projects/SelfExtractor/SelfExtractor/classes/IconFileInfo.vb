﻿Imports System.IO

Public Class IconFileInfo
    Private mFi As FileInfo
    Private mIcon As Drawing.Icon

    Public Sub New(filename As String)
        mFi = New FileInfo(filename)
        mIcon = Drawing.Icon.ExtractAssociatedIcon(filename)
    End Sub

    Public ReadOnly Property Icon() As Drawing.Icon
        Get
            Return mIcon
        End Get
    End Property

    Public ReadOnly Property Name() As String
        Get
            Return mFi.Name
        End Get
    End Property

    Public ReadOnly Property Size() As Long
        Get
            Return mFi.Length
        End Get
    End Property

    Public ReadOnly Property FullName() As String
        Get
            Return mFi.FullName
        End Get
    End Property

    Public ReadOnly Property LastWriteTime() As DateTime
        Get
            Return mFi.LastWriteTime
        End Get
    End Property
End Class
