﻿Imports System.IO
Imports System.Text

Public Class StreamString
    Public Shared Function MemoryStreamToString(ms As MemoryStream) As String
        Return Encoding.UTF8.GetString(ms.GetBuffer(), 0, CInt(ms.Length))
    End Function

    Public Shared Function StringToStream(str As String) As Stream
        Return New MemoryStream(Encoding.UTF8.GetBytes(str))
    End Function
End Class
