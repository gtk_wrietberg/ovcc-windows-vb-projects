﻿Imports System
Imports System.Collections.Generic

Public Class FileList
    Private Shared mFiles As New List(Of String)
    Private Shared mPaths As New List(Of String)
    Private Shared mSizes As New List(Of Long)
    Private Shared mExec As New List(Of Boolean)
    Private Shared mParams As New List(Of String)
    Private Shared mCount As Long = -1
    Private Shared mTotalSize As Long = 0

    Public Shared ReadOnly Property TotalSize() As Long
        Get
            Return mTotalSize
        End Get
    End Property

    Public Shared Sub Reset()
        mFiles = New List(Of String)
        mPaths = New List(Of String)
        mSizes = New List(Of Long)
        mExec = New List(Of Boolean)
        mParams = New List(Of String)

        mCount = -1
        mTotalSize = 0
    End Sub

    Public Shared Sub Add(file As String, path As String, size As Long)
        Add(file, path, size, False, "")
    End Sub

    Public Shared Sub Add(file As String, path As String, size As Long, exec As Boolean, params As String)
        mFiles.Add(file)
        mPaths.Add(path)
        mSizes.Add(size)
        mExec.Add(exec)
        mParams.Add(params)

        mTotalSize += size
    End Sub

    Public Shared Function Parse(str As String) As Boolean
        Reset()

        Dim TextLines() As String = str.Split(Environment.NewLine.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

        For Each textline As String In TextLines
            Dim keyvaluepair() As String = textline.Split("*".ToCharArray(), 5)
            Dim size As Long, exec As Boolean

            If keyvaluepair.Length = 5 Then
                'ok
                If Not Integer.TryParse(keyvaluepair(2), size) Then
                    size = 0
                End If

                If Not Boolean.TryParse(keyvaluepair(3), exec) Then
                    exec = False
                End If

                Add(keyvaluepair(0), keyvaluepair(1), size, exec, keyvaluepair(4))
            End If
        Next

        Return True
    End Function

    Public Shared Function Dump() As String
        Dim str As String = ""

        For i As Integer = 0 To mFiles.Count - 1
            str &= mFiles.Item(i) & "*" & mPaths.Item(i) & "*" & mSizes.Item(i).ToString & "*" & mExec.Item(i).ToString & "*" & mParams.Item(i).ToString
            str &= Environment.NewLine
        Next

        Return str
    End Function

    Public Shared Function GetNext(ByRef file As String, ByRef path As String, ByRef size As Long, ByRef exec As Boolean, ByRef params As String) As Boolean
        mCount += 1

        If mCount < 0 Or mCount >= mFiles.Count Or mCount >= mPaths.Count Or mCount >= mSizes.Count Or mCount >= mExec.Count Or mCount >= mParams.Count Then
            file = ""
            path = ""
            size = 0
            exec = False
            params = ""

            Return False
        End If

        file = mFiles.Item(mCount)
        path = mPaths.Item(mCount)
        size = mSizes.Item(mCount)
        exec = mExec.Item(mCount)
        params = mParams.Item(mCount)

        Return True
    End Function
End Class
