﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Environment
Imports System.IO
Imports System.IO.Compression
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports System.Reflection
Imports System.Diagnostics
Imports System.Text
Imports System.Threading

Public Class frmUnpack
    Private Sub bgWorker_Unpack_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker_Unpack.DoWork
        ExtractFiles()
    End Sub

    Private Sub frmUnpack_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ReadConfiguration()

        txtSettings.Text = Settings.Dump
        txtFileList.Text = FileList.Dump
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        bgWorker_Unpack.RunWorkerAsync()
    End Sub

    Private Sub bgWorker_Unpack_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bgWorker_Unpack.ProgressChanged
        txtProgress.AppendText(e.UserState.ToString & ": " & e.ProgressPercentage.ToString & "%" & vbCrLf)
    End Sub


    Private Sub ReadConfiguration()
        Settings.Reset()
        FileList.Reset()

        Dim ass As Assembly = Assembly.GetExecutingAssembly()
        Dim res As String() = ass.GetManifestResourceNames()

        Try
            For Each name As String In res
                If name.Equals(Shared_Constants.SettingsFileName) Then
                    'settings
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        Settings.Parse(streamText)

                        reader.Close()
                    End Using
                End If

                If name.Equals(Shared_Constants.FileListFileName) Then
                    'filelist
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        MessageBox.Show(streamText)

                        FileList.Parse(streamText)

                        reader.Close()
                    End Using
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(Me, ex.Message, "SelfExtractor", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ExtractFiles()
        Dim ass As Assembly = Assembly.GetExecutingAssembly()
        Dim res As String() = ass.GetManifestResourceNames()
        Dim sRunFileAfterExtraction As String = ""

        Try
            Dim location As String = System.Environment.GetCommandLineArgs()(0)
            Dim appName As String = System.IO.Path.GetFileName(location)
            Dim size_sofar As Integer = 0

            Dim basepath As String = Settings.BaseDir

            'If MessageBox.Show(basepath, "Continue?", MessageBoxButtons.YesNo) = DialogResult.No Then
            '    Throw New Exception("User canceled!")
            'End If

            For Each name As String In res
                If name.Equals(Shared_Constants.FileListFileName) Or name.Equals(Shared_Constants.SettingsFileName) Then
                    Continue For
                End If

                Dim rs As Stream = ass.GetManifestResourceStream(name)

                Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                    Dim sFile As String = "", sPath As String = "", iSize As Integer = 0, bExec As Boolean = False, sParams As String = ""

                    FileList.GetNext(sFile, sPath, iSize, bExec, sParams)

                    Dim fullpath As String = Path.Combine(basepath, sPath)
                    Dim filepath As String = Path.Combine(fullpath, Path.GetFileNameWithoutExtension(name))

                    If Not IO.Directory.Exists(fullpath) Then
                        IO.Directory.CreateDirectory(fullpath)
                    End If



                    'MessageBox.Show("name: '" & name & "' - name_fixed: '" & Path.GetFileNameWithoutExtension(name) & "' - filepath: '" & filepath & "' - relative_path: '" & relative_path & "'")
                    'MessageBox.Show("filepath: '" & filepath & "' - relative_path: '" & relative_path & "'")
                    'MessageBox.Show("fullpath: '" & fullpath & "' - filepath: '" & filepath & "'")

                    Using unpacked_file As Stream = File.Create(filepath)
                        Dim b As Integer = gzip.ReadByte()

                        While b <> -1
                            unpacked_file.WriteByte(CByte(b))
                            b = gzip.ReadByte()
                        End While
                    End Using

                    If bExec Then
                        sRunFileAfterExtraction = """" & filepath & """ " & sParams
                    End If

                    size_sofar += iSize

                    bgWorker_Unpack.ReportProgress((100 * size_sofar) / FileList.TotalSize, filepath)
                End Using

                Threading.Thread.Sleep(1000)
            Next


            MessageBox.Show("sRunFileAfterExtraction: " & sRunFileAfterExtraction)


        Catch ex As Exception
            MessageBox.Show(ex.Message, ass.GetName().Name, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try
    End Sub




    'Private Sub Masdfsdfin()
    '    Dim ass As Assembly = Assembly.GetExecutingAssembly()
    '    Dim res As String() = ass.GetManifestResourceNames()

    '    Try
    '        'First get settings and file list
    '        For Each name As String In res
    '            If name.Equals(Shared_Constants.SettingsFileName) Then
    '                'settings
    '                Dim rs As Stream = ass.GetManifestResourceStream(name)

    '                Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
    '                    Dim reader As New StreamReader(gzip)
    '                    Dim streamText As String = reader.ReadToEnd()

    '                    Settings.Parse(streamText)

    '                    reader.Close()
    '                End Using
    '            End If

    '            If name.Equals(Shared_Constants.FileListFileName) Then
    '                'filelist
    '                Dim rs As Stream = ass.GetManifestResourceStream(name)

    '                Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
    '                    Dim reader As New StreamReader(gzip)
    '                    Dim streamText As String = reader.ReadToEnd()

    '                    FileList.Parse(streamText)

    '                    reader.Close()
    '                End Using
    '            End If
    '        Next

    '        MessageBox.Show(Settings.Dump)
    '        MessageBox.Show(FileList.Dump)

    '        Dim location As String = System.Environment.GetCommandLineArgs()(0)
    '        Dim appName As String = System.IO.Path.GetFileName(location)

    '        'Dim basepath As String = Path.Combine(Path.GetDirectoryName(ass.Location), Path.GetFileNameWithoutExtension(appName))
    '        Dim basepath As String = Settings.BaseDir

    '        If MessageBox.Show(basepath, "Continue?", MessageBoxButtons.YesNo) = DialogResult.No Then
    '            Throw New Exception("User canceled!")
    '        End If

    '        For Each name As String In res
    '            If name.Equals(Shared_Constants.FileListFileName) Or name.Equals(Shared_Constants.SettingsFileName) Then
    '                Continue For
    '            End If

    '            Dim rs As Stream = ass.GetManifestResourceStream(name)

    '            Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
    '                Dim sFile As String = "", sPath As String = "", iSize As Integer = 0
    '                'Dim relative_path As String = FileList.GetNext

    '                FileList.GetNext(sFile, sPath, iSize)

    '                Dim fullpath As String = Path.Combine(basepath, sPath)
    '                Dim filepath As String = Path.Combine(fullpath, Path.GetFileNameWithoutExtension(name))

    '                If Not IO.Directory.Exists(fullpath) Then
    '                    IO.Directory.CreateDirectory(fullpath)
    '                End If



    '                'MessageBox.Show("name: '" & name & "' - name_fixed: '" & Path.GetFileNameWithoutExtension(name) & "' - filepath: '" & filepath & "' - relative_path: '" & relative_path & "'")
    '                'MessageBox.Show("filepath: '" & filepath & "' - relative_path: '" & relative_path & "'")
    '                MessageBox.Show("fullpath: '" & fullpath & "' - filepath: '" & filepath & "'")

    '                Using unpacked_file As Stream = File.Create(filepath)
    '                    Dim b As Integer = gzip.ReadByte()

    '                    While b <> -1
    '                        unpacked_file.WriteByte(CByte(b))
    '                        b = gzip.ReadByte()
    '                    End While
    '                End Using
    '            End Using

    '        Next




    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ass.GetName().Name, MessageBoxButtons.OK, MessageBoxIcon.Error)

    '    End Try
    'End Sub


End Class