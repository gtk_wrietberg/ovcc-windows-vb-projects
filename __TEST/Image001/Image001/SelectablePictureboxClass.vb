﻿'picturebox with selectable mouse rectangle that can be resized and moved
'clipped image method returns the clipped image within the rect
Public Class SelectablePictureboxClass
    Inherits PictureBox

    Public MouseDownStage As Integer
    Public MouseDownPt, MouseDownOffsetPt, MouseMovePt As PointF
    Public ClipRectf As RectangleF

    Public Sub New()
        BackgroundImageLayout = ImageLayout.Zoom
    End Sub

    Private Sub SelectablePicturebox_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown

        MouseDownStage = MouseOverRectangle(e.X, e.Y)
        Select Case MouseDownStage
            Case 1
                MouseDownPt = e.Location
                MouseMovePt = e.Location

            Case 2, 3
                MouseDownPt = ClipRectf.Location
                MouseDownOffsetPt = New PointF(e.X - ClipRectf.X, e.Y - ClipRectf.Y)
        End Select
    End Sub

    Private Sub SelectablePicturebox_MouseMove(sender As Object, e As MouseEventArgs) Handles Me.MouseMove
        Dim pt As Point = e.Location
        If pt.X > Width - 1 Then pt.X = Width - 1
        If pt.Y > Height - 1 Then pt.Y = Height - 1
        If pt.X < 0 Then pt.X = 0
        If pt.Y < 0 Then pt.Y = 0

        If MouseDownStage > 0 Then
            Select Case MouseDownStage
                Case 1
                    MouseMovePt = pt
                Case 2
                    'moving fence
                    Dim dx, dy, x, y As Single
                    dx = (pt.X - MouseDownPt.X)
                    dy = (pt.Y - MouseDownPt.Y)
                    x = (MouseDownPt.X + dx - MouseDownOffsetPt.X)
                    If x < 0 Then x = 0
                    y = (MouseDownPt.Y + dy - MouseDownOffsetPt.Y)
                    If y < 0 Then y = 0
                    ClipRectf.X = x
                    ClipRectf.Y = y
                Case 3
                    'moving handle
                    ClipRectf.Width = Math.Abs(ClipRectf.X - pt.X)
                    ClipRectf.Height = Math.Abs(ClipRectf.Y - pt.Y)
            End Select
        Else
            Select Case MouseOverRectangle(pt.X, pt.Y)
                Case 2  'fence
                    Cursor.Current = Cursors.Hand
                Case 3 'handle
                    Cursor.Current = Cursors.SizeNESW
                Case Else
                    Cursor.Current = Cursors.Default
            End Select
        End If

        Invalidate()
    End Sub

    Private Sub SelectablePicturebox_MouseUp(sender As Object, e As MouseEventArgs) Handles Me.MouseUp
        Select Case MouseDownStage
            Case 1
                Dim w1, w2, h1, h2 As Integer
                w1 = CInt(ClientRectangle.Width - MouseDownPt.X)
                w2 = CInt(MouseMovePt.X - MouseDownPt.X)
                If w2 > w1 Then w2 = w1

                h1 = CInt(ClientRectangle.Height - MouseDownPt.Y)
                h2 = CInt(MouseMovePt.Y - MouseDownPt.Y)
                If h2 > h1 Then h2 = h1

                ClipRectf = New RectangleF(MouseDownPt.X, MouseDownPt.Y, w2, h2)

        End Select
        MouseDownStage = 0
        Invalidate()
    End Sub

    Private Sub SelectablePicturebox_Paint(sender As Object, e As PaintEventArgs) Handles Me.Paint
        DrawMouseRect(e.Graphics)
    End Sub

    Public Function GetClippedImage() As Bitmap
        Dim ratio As Double
        Dim x1 As Integer = 0
        Dim y1 As Integer = 0
        GetClippedImage = Nothing

        If BackgroundImage IsNot Nothing Then
            If ClipRectf.Width > 4 AndAlso ClipRectf.Height > 4 Then
                Dim w As Integer = BackgroundImage.Width
                Dim h As Integer = BackgroundImage.Height

                If ClientSize.Height / ClientSize.Width < h / w Then
                    'image fit to height
                    ratio = ClientSize.Height / h
                    x1 = -CInt((ClientSize.Width - (w * ratio)) / 2)
                Else
                    'image fit to width
                    ratio = ClientSize.Width / w
                    y1 = -CInt((ClientSize.Height - (h * ratio)) / 2)
                End If

                Dim dx As Integer = CInt(ClipRectf.Width / ratio)
                Dim dy As Integer = CInt(ClipRectf.Height / ratio)

                If dx > 0 AndAlso dy > 0 Then
                    Using bmp As New Bitmap(dx, dy),
                g As Graphics = Graphics.FromImage(bmp)
                        Dim srcRect As Rectangle = New Rectangle(CInt((x1 + ClipRectf.X) / ratio),
                                                         CInt((y1 + ClipRectf.Y) / ratio),
                                                         dx, dy)
                        Dim destRect As New Rectangle(0, 0, dx, dy)
                        g.DrawImage(BackgroundImage, destRect, srcRect, GraphicsUnit.Pixel)
                        GetClippedImage = CType(bmp.Clone, Bitmap)
                    End Using
                End If
            End If
        End If
    End Function

    Private Sub DrawMouseRect(g As Graphics)

        Using p As New Pen(Color.Red, 2)
            If ClipRectf.Width > 0 Then
                p.DashStyle = Drawing2D.DashStyle.Dash
                g.DrawRectangle(p, Rectangle.Round(ClipRectf))

                p.Width = 3
                p.Color = Color.LimeGreen
                p.DashStyle = Drawing2D.DashStyle.Solid

                Dim h As Integer = 5
                g.DrawRectangle(p, Rectangle.Round(New RectangleF(ClipRectf.X + ClipRectf.Width - h,
                                           ClipRectf.Y + ClipRectf.Height - h, 2 * h, 2 * h)))
            End If

            If MouseDownStage = 1 Then
                p.Width = 2
                p.Color = Color.OrangeRed
                p.DashStyle = Drawing2D.DashStyle.Dash
                g.DrawRectangle(p, Rectangle.Round(New RectangleF(MouseDownPt.X, MouseDownPt.Y,
                                             MouseMovePt.X - MouseDownPt.X, MouseMovePt.Y - MouseDownPt.Y)))
            End If
        End Using
    End Sub

    Private Function MouseOverRectangle(x As Integer, y As Integer) As Integer
        'determine if the mouse pointer is over the handle or fence
        Dim h As Integer = 5
        Dim handleRect As New RectangleF(ClipRectf.X + ClipRectf.Width - h,
                                            ClipRectf.Y + ClipRectf.Height - h, 2 * h, 2 * h)
        Dim fenceRect As New RectangleF
        fenceRect = ClipRectf

        If handleRect.Contains(x, y) Then
            MouseOverRectangle = 3
        ElseIf fenceRect.Contains(x, y) Then
            MouseOverRectangle = 2
        Else
            MouseOverRectangle = 1
        End If
    End Function
End Class