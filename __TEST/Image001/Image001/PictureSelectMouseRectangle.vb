﻿Public Class PictureSelectMouseRectangle
    Private WithEvents PictureboxSelectable As New SelectablePictureboxClass With {.Parent = Me,
        .Location = New Point(10, 20), .Size = New Size(240, 240), .BackColor = Color.Black}

    Private WithEvents Picturebox1 As New PictureBox With {.Parent = Me,
            .Location = New Point(4 + PictureboxSelectable.Right, PictureboxSelectable.Top),
            .Size = PictureboxSelectable.Size, .BackColor = PictureboxSelectable.BackColor,
            .BackgroundImageLayout = ImageLayout.Zoom}

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Text = "Selectable Picture Box Example"
        ClientSize = New Size(500, 300)
        PictureboxSelectable.BackgroundImage = Image.FromFile("C:\Temp\inns.jpg")

        Me.AllowDrop = True
        PictureboxSelectable.AllowDrop = True
    End Sub

    Private Sub PictureboxSelectable_MouseMove(sender As Object, e As MouseEventArgs) Handles PictureboxSelectable.MouseMove
        'if the clipping fence is being dragged or resized update the clipped image
        Select Case PictureboxSelectable.MouseDownStage
            Case 2, 3
                UpdateClippedImange()
        End Select
    End Sub

    Private Sub PictureboxSelectable_MouseUp(sender As Object, e As MouseEventArgs) Handles PictureboxSelectable.MouseUp
        UpdateClippedImange()
    End Sub

    Private Sub PictureboxSelectable_DragDrop(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles PictureboxSelectable.DragDrop
        Dim picbox As PictureBox = CType(sender, PictureBox)

        Dim files() As String = CType(e.Data.GetData(DataFormats.FileDrop), String())

        If files.Length <> 0 Then
            Try
                picbox.BackgroundImage = Image.FromFile(files(0))
            Catch ex As Exception
                MessageBox.Show("Problem opening file ")
            End Try
        End If
    End Sub

    Private Sub PictureboxSelectable_DragEnter(sender As System.Object, e As System.Windows.Forms.DragEventArgs) Handles PictureboxSelectable.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub UpdateClippedImange()
        Dim img As Image = Picturebox1.BackgroundImage
        Picturebox1.BackgroundImage = PictureboxSelectable.GetClippedImage
        Picturebox1.Invalidate()
        If img IsNot Nothing Then img.Dispose()
    End Sub
End Class
