﻿Imports System.Security.AccessControl
Imports System.IO
Imports Microsoft.Win32
Imports System.Security.Principal

Public Class regcanon

    Public Shared Sub Blaat()
        Dim sid As SecurityIdentifier = New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
        Dim account As NTAccount = TryCast(sid.Translate(GetType(NTAccount)), NTAccount)


        ' directory with known ACL problem (created using Icacls)
        '' '' '' '' '' ''Dim directoryInfo As New DirectoryInfo("C:\acltest\test01")

        '' '' '' '' '' ''Dim directorySecurity = directoryInfo.GetAccessControl(AccessControlSections.Access)
        '' '' '' '' '' ''Dim directorySecurity = directoryInfo.GetAccessControl()

        'Dim directoryInfo As New DirectoryInfo("C:\acltest\test01")

        Dim directoryInfo As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\GuestTek\Watchdog\acl_test02", RegistryKeyPermissionCheck.ReadWriteSubTree)


        Dim directorySecurity = directoryInfo.GetAccessControl

        'HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\GuestTek\Watchdog\acl_test

        'Using rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, RegistryKeyPermissionCheck.ReadWriteSubTree)
        '    If rk Is Nothing Then
        '        Throw New InvalidOperationException("key '" & key & "' does not exist in HKEY_LOCAL_MACHINE")
        '    End If

        '    Dim rs As RegistrySecurity = rk.GetAccessControl
        '    Dim rar As RegistryAccessRule = New RegistryAccessRule( _
        '                                    account.ToString, _
        '                                    RegistryRights.FullControl, _
        '                                    InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, _
        '                                    PropagationFlags.None, _
        '                                    AccessControlType.Allow)


        '    rs.SetAccessRuleProtection(True, False)
        '    rs.AddAccessRule(rar)

        '    rk.SetAccessControl(rs)
        'End Using



        MsgBox(CanonicalizeDacl(directorySecurity))

        directoryInfo.SetAccessControl(directorySecurity)


        directorySecurity.SetAccessRuleProtection(True, False)


        Dim rules As AuthorizationRuleCollection = directorySecurity.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
        For Each rule As RegistryAccessRule In rules
            directorySecurity.RemoveAccessRule(rule)
        Next

        directorySecurity.AddAccessRule(New RegistryAccessRule(account.ToString, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow))
        'directorySecurity.AddAccessRule(New RegistryAccessRule(account.ToString, FileSystemRights.FullControl, InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow))

        directoryInfo.SetAccessControl(directorySecurity)
    End Sub

    Public Shared Function CanonicalizeDacl(objectSecurity As NativeObjectSecurity) As String
        Try
            If objectSecurity Is Nothing Then
                Throw New ArgumentNullException("objectSecurity is null")
            End If
            If objectSecurity.AreAccessRulesCanonical Then
                Throw New Exception("Rules are already canonical")
            End If

            ' A canonical ACL must have ACES sorted according to the following order:
            '   1. Access-denied on the object
            '   2. Access-denied on a child or property
            '   3. Access-allowed on the object
            '   4. Access-allowed on a child or property
            '   5. All inherited ACEs 
            Dim descriptor As New RawSecurityDescriptor(objectSecurity.GetSecurityDescriptorSddlForm(AccessControlSections.Access))

            Dim implicitDenyDacl As New List(Of CommonAce)()
            Dim implicitDenyObjectDacl As New List(Of CommonAce)()
            Dim inheritedDacl As New List(Of CommonAce)()
            Dim implicitAllowDacl As New List(Of CommonAce)()
            Dim implicitAllowObjectDacl As New List(Of CommonAce)()

            For Each ace As CommonAce In descriptor.DiscretionaryAcl
                If (ace.AceFlags And AceFlags.Inherited) = AceFlags.Inherited Then
                    inheritedDacl.Add(ace)
                Else
                    Select Case ace.AceType
                        Case AceType.AccessAllowed
                            implicitAllowDacl.Add(ace)
                            Exit Select

                        Case AceType.AccessDenied
                            implicitDenyDacl.Add(ace)
                            Exit Select

                        Case AceType.AccessAllowedObject
                            implicitAllowObjectDacl.Add(ace)
                            Exit Select

                        Case AceType.AccessDeniedObject
                            implicitDenyObjectDacl.Add(ace)
                            Exit Select
                    End Select
                End If
            Next

            Dim aceIndex As Int32 = 0
            Dim newDacl As New RawAcl(descriptor.DiscretionaryAcl.Revision, descriptor.DiscretionaryAcl.Count)

            For Each tmp As CommonAce In implicitDenyDacl
                'newDacl.InsertAce(System.Math.Max(System.Threading.Interlocked.Increment(aceIndex), aceIndex - 1), tmp)
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next

            For Each tmp As CommonAce In implicitDenyObjectDacl
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next

            For Each tmp As CommonAce In implicitAllowDacl
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next

            For Each tmp As CommonAce In implicitAllowObjectDacl
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next

            For Each tmp As CommonAce In inheritedDacl
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next


            If aceIndex <> descriptor.DiscretionaryAcl.Count Then
                Throw New Exception("The DACL cannot be canonicalized since it would potentially result in a loss of information")
            End If

            descriptor.DiscretionaryAcl = newDacl
            objectSecurity.SetSecurityDescriptorSddlForm(descriptor.GetSddlForm(AccessControlSections.Access), AccessControlSections.Access)

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

End Class
