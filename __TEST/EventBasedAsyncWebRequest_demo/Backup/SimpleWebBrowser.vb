Imports Brewder.NetLib

Public Class SimpleWebBrowser

    Private mRequestKey As Object
    Private Sub txtAddress_Validated( _
        ByVal sender As System.Object, _
        ByVal e As System.EventArgs) _
        Handles txtAddress.Validated

        ' make sure we cancel the last request
        wrcBrowser.CancelRequest(mRequestKey)

        If txtAddress.Text = "" Then
            mRequestKey = Nothing
            browser.DocumentText = ""
        Else
            ' GetResponseAsyc returns an object that is
            ' used to identify the specific web request.
            ' The value must be stored in order to 
            ' perform operations on it (such as cancelling)
            mRequestKey = wrcBrowser.GetResponseAsync( _
                                        txtAddress.Text)
        End If
    End Sub

    Private Sub SetStatus(ByVal status As Brewder.NetLib.WebRequestStatus, ByVal err As Exception)
        Select Case status
            Case Brewder.NetLib.WebRequestStatus.AuthenticationFailed
                lblStatus.Text = "Unauthorized"
                MsgBox("Unauthorized access attempted. If you are attempting to illegally access a website, please report yourself to the police immediately.")
            Case Brewder.NetLib.WebRequestStatus.Cancelled
                lblStatus.Text = "Cancelled"
            Case Brewder.NetLib.WebRequestStatus.Complete
                lblStatus.Text = "Complete"
            Case Brewder.NetLib.WebRequestStatus.Connected
                lblStatus.Text = "Connected"
            Case Brewder.NetLib.WebRequestStatus.Connecting
                lblStatus.Text = "Connecting"
            Case Brewder.NetLib.WebRequestStatus.Error
                lblStatus.Text = "Error"
                statusStrip.Text = err.Message
        End Select
    End Sub

    Private Sub wrcBrowser_GetRequestCompleted( _
        ByVal sender As System.Object, _
        ByVal e As GetRequestCompletedEventArgs) _
        Handles wrcBrowser.GetRequestCompleted

        ' we only care about the last request that we made
        If e.RequestKey IsNot mRequestKey Then Return
        ' This method simply displays the status in the 
        ' status bar.
        SetStatus(e.Status, e.Error)

        If e.Status = WebRequestStatus.Complete Then
            ' The request completed successfully, dump the 
            ' html into the web browser.
            browser.DocumentText = CStr(e.ResponseResults)
            lblAddress.Text = e.Uri
        End If
    End Sub

    Private Sub wrcBrowser_GetRequestProgressChanged(ByVal sender As System.Object, ByVal e As Brewder.NetLib.GetRequestProgressChangedEventArgs) Handles wrcBrowser.GetRequestProgressChanged
        ' we only care about the request that we last made
        If e.RequestKey IsNot mRequestKey Then Return
        SetStatus(e.Status, Nothing)
    End Sub

End Class
