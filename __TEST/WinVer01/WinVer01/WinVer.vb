﻿Imports System.Runtime.InteropServices

Public Class WinVer
    <StructLayout(LayoutKind.Sequential)> _
        Private Structure OsVersionInfoEx
        Public OSVersionInfoSize As UInteger
        Public MajorVersion As UInteger
        Public MinorVersion As UInteger
        Public BuildNumber As UInteger
        Public PlatformId As UInteger
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)> _
        Public CSDVersion As String
        Public ServicePackMajor As UShort
        Public ServicePackMinor As UShort
        Public SuiteMask As UShort
        Public ProductType As Byte
        Public Reserved As Byte
    End Structure

    <DllImport("kernel32.dll")> _
    Private Shared Function VerSetConditionMask(dwlConditionMask As ULong, dwTypeBitMask As UInteger, dwConditionMask As Byte) As ULong
    End Function
    <DllImport("kernel32.dll")> _
    Private Shared Function VerifyVersionInfo(<[In]> ByRef lpVersionInfo As OsVersionInfoEx, dwTypeMask As UInteger, dwlConditionMask As ULong) As Boolean
    End Function

    Public Shared Function GetFullVersion() As String
        Dim osvi As New OsVersionInfoEx()
        Dim sVer As String = ""

        sVer &= osvi.PlatformId.ToString
        sVer &= "."
        sVer &= osvi.PlatformId.ToString
        sVer &= "."
        sVer &= osvi.PlatformId.ToString


        Return sVer
    End Function


    Private Shared Function IsWindowsVersionOrGreater(majorVersion As UInteger, minorVersion As UInteger, servicePackMajor As UShort) As Boolean
        Dim osvi As New OsVersionInfoEx()
        osvi.OSVersionInfoSize = CUInt(Marshal.SizeOf(osvi))
        osvi.MajorVersion = majorVersion
        osvi.MinorVersion = minorVersion
        osvi.ServicePackMajor = servicePackMajor
        ' These constants initialized with corresponding definitions in
        ' winnt.h (part of Windows SDK)
        Const VER_MINORVERSION As UInteger = &H1
        Const VER_MAJORVERSION As UInteger = &H2
        Const VER_SERVICEPACKMAJOR As UInteger = &H20
        Const VER_GREATER_EQUAL As Byte = 3
        Dim versionOrGreaterMask As ULong = VerSetConditionMask(VerSetConditionMask(VerSetConditionMask(0, VER_MAJORVERSION, VER_GREATER_EQUAL), VER_MINORVERSION, VER_GREATER_EQUAL), VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL)
        Dim versionOrGreaterTypeMask As UInteger = VER_MAJORVERSION Or VER_MINORVERSION Or VER_SERVICEPACKMAJOR

        Return VerifyVersionInfo(osvi, versionOrGreaterTypeMask, versionOrGreaterMask)
    End Function
End Class
