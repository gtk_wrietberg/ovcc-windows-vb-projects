﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox1.Text = GetWinVer_classic()
        TextBox2.Text = GetWinVer_new()
    End Sub

    Private Function GetWinVer_classic() As String
        Dim sVer As String = ""

        sVer &= Environment.OSVersion.Platform.ToString
        sVer &= " - "
        sVer &= Environment.OSVersion.Version.Major.ToString
        sVer &= "."
        sVer &= Environment.OSVersion.Version.Minor.ToString

        Return sVer
    End Function

    Private Function GetWinVer_new() As String
        Return OSVersionInfo.VersionString
    End Function

End Class
