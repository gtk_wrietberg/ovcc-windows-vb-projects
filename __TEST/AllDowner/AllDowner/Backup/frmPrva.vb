Public Class frmPrva
    Inherits System.Windows.Forms.Form
    Private broj As Integer
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents sat As System.Windows.Forms.NumericUpDown
    Friend WithEvents minut As System.Windows.Forms.NumericUpDown
    Friend WithEvents sekund As System.Windows.Forms.NumericUpDown
    Friend WithEvents tVreme As System.Windows.Forms.Timer
    Friend WithEvents pgProcenat As System.Windows.Forms.ProgressBar
    Friend WithEvents paOdustani As System.Windows.Forms.Panel
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents tt As System.Windows.Forms.ToolTip
    Friend WithEvents lbProcesi As System.Windows.Forms.ListBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents tmSat As System.Windows.Forms.Timer
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Sysni As System.Windows.Forms.NotifyIcon
    Friend WithEvents desMeni As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents mi10 As System.Windows.Forms.MenuItem
    Friend WithEvents mi30 As System.Windows.Forms.MenuItem
    Friend WithEvents mi50 As System.Windows.Forms.MenuItem
    Friend WithEvents mi70 As System.Windows.Forms.MenuItem
    Friend WithEvents mi90 As System.Windows.Forms.MenuItem
    Friend WithEvents mi100 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mi9 As System.Windows.Forms.MenuItem
    Friend WithEvents mi3 As System.Windows.Forms.MenuItem
    Friend WithEvents mi1 As System.Windows.Forms.MenuItem
    Friend WithEvents mi7 As System.Windows.Forms.MenuItem
    Friend WithEvents mi5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPrva))
        Me.sat = New System.Windows.Forms.NumericUpDown
        Me.minut = New System.Windows.Forms.NumericUpDown
        Me.sekund = New System.Windows.Forms.NumericUpDown
        Me.pgProcenat = New System.Windows.Forms.ProgressBar
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.tVreme = New System.Windows.Forms.Timer(Me.components)
        Me.paOdustani = New System.Windows.Forms.Panel
        Me.PictureBox4 = New System.Windows.Forms.PictureBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.PictureBox5 = New System.Windows.Forms.PictureBox
        Me.tt = New System.Windows.Forms.ToolTip(Me.components)
        Me.Button2 = New System.Windows.Forms.Button
        Me.lbProcesi = New System.Windows.Forms.ListBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.tmSat = New System.Windows.Forms.Timer(Me.components)
        Me.Sysni = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.desMeni = New System.Windows.Forms.ContextMenu
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mi1 = New System.Windows.Forms.MenuItem
        Me.mi3 = New System.Windows.Forms.MenuItem
        Me.mi5 = New System.Windows.Forms.MenuItem
        Me.mi7 = New System.Windows.Forms.MenuItem
        Me.mi9 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.mi10 = New System.Windows.Forms.MenuItem
        Me.mi30 = New System.Windows.Forms.MenuItem
        Me.mi50 = New System.Windows.Forms.MenuItem
        Me.mi70 = New System.Windows.Forms.MenuItem
        Me.mi90 = New System.Windows.Forms.MenuItem
        Me.mi100 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        CType(Me.sat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.minut, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sekund, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.paOdustani.SuspendLayout()
        Me.SuspendLayout()
        '
        'sat
        '
        Me.sat.BackColor = System.Drawing.Color.MediumBlue
        Me.sat.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.sat.ForeColor = System.Drawing.Color.White
        Me.sat.Location = New System.Drawing.Point(85, 16)
        Me.sat.Maximum = New Decimal(New Integer() {24, 0, 0, 0})
        Me.sat.Name = "sat"
        Me.sat.Size = New System.Drawing.Size(40, 23)
        Me.sat.TabIndex = 0
        '
        'minut
        '
        Me.minut.BackColor = System.Drawing.Color.MediumBlue
        Me.minut.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.minut.ForeColor = System.Drawing.Color.White
        Me.minut.Location = New System.Drawing.Point(133, 16)
        Me.minut.Maximum = New Decimal(New Integer() {59, 0, 0, 0})
        Me.minut.Name = "minut"
        Me.minut.Size = New System.Drawing.Size(40, 23)
        Me.minut.TabIndex = 1
        '
        'sekund
        '
        Me.sekund.BackColor = System.Drawing.Color.MediumBlue
        Me.sekund.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.sekund.ForeColor = System.Drawing.Color.White
        Me.sekund.Location = New System.Drawing.Point(181, 16)
        Me.sekund.Maximum = New Decimal(New Integer() {59, 0, 0, 0})
        Me.sekund.Name = "sekund"
        Me.sekund.Size = New System.Drawing.Size(40, 23)
        Me.sekund.TabIndex = 2
        '
        'pgProcenat
        '
        Me.pgProcenat.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pgProcenat.Location = New System.Drawing.Point(1, 188)
        Me.pgProcenat.Name = "pgProcenat"
        Me.pgProcenat.Size = New System.Drawing.Size(312, 10)
        Me.pgProcenat.TabIndex = 3
        Me.pgProcenat.Visible = False
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(93, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 17)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Sat"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(133, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 17)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Minut"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(173, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Sekund"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(21, 72)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(33, 33)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        Me.tt.SetToolTip(Me.PictureBox1, "Odjavljivanje ulogovanog korisnika")
        '
        'PictureBox2
        '
        Me.PictureBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(261, 72)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(33, 33)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 8
        Me.PictureBox2.TabStop = False
        Me.tt.SetToolTip(Me.PictureBox2, "Ga�enje racunara")
        '
        'PictureBox3
        '
        Me.PictureBox3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(181, 72)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(33, 33)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 9
        Me.PictureBox3.TabStop = False
        Me.tt.SetToolTip(Me.PictureBox3, "Restartovanje racunara")
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(9, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Log off"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(245, 112)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Turn off"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(169, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 16)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Restart"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tVreme
        '
        Me.tVreme.Interval = 1000
        '
        'paOdustani
        '
        Me.paOdustani.BackColor = System.Drawing.Color.Transparent
        Me.paOdustani.Controls.Add(Me.PictureBox4)
        Me.paOdustani.Controls.Add(Me.Label7)
        Me.paOdustani.Location = New System.Drawing.Point(0, 136)
        Me.paOdustani.Name = "paOdustani"
        Me.paOdustani.Size = New System.Drawing.Size(128, 40)
        Me.paOdustani.TabIndex = 13
        Me.paOdustani.Visible = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(21, 5)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 9
        Me.PictureBox4.TabStop = False
        Me.tt.SetToolTip(Me.PictureBox4, "Prekidanje zapocete akcije")
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(56, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 23)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Odustani"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(74, 112)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(87, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Switch user"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox5
        '
        Me.PictureBox5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(101, 72)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(33, 33)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 7
        Me.PictureBox5.TabStop = False
        Me.tt.SetToolTip(Me.PictureBox5, "Promena ulogovanog korisnika")
        '
        'tt
        '
        Me.tt.ShowAlways = True
        '
        'Button2
        '
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(256, 168)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(56, 20)
        Me.Button2.TabIndex = 16
        Me.Button2.Text = "Izlaz"
        Me.tt.SetToolTip(Me.Button2, "Izlazak iz programa")
        '
        'lbProcesi
        '
        Me.lbProcesi.BackColor = System.Drawing.Color.CornflowerBlue
        Me.lbProcesi.ColumnWidth = 80
        Me.lbProcesi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbProcesi.ForeColor = System.Drawing.Color.White
        Me.lbProcesi.Location = New System.Drawing.Point(360, 96)
        Me.lbProcesi.MultiColumn = True
        Me.lbProcesi.Name = "lbProcesi"
        Me.lbProcesi.Size = New System.Drawing.Size(232, 212)
        Me.lbProcesi.Sorted = True
        Me.lbProcesi.TabIndex = 14
        Me.lbProcesi.Visible = False
        '
        'Button1
        '
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Button1.Location = New System.Drawing.Point(480, 328)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(80, 23)
        Me.Button1.TabIndex = 15
        Me.Button1.Text = "Prekini proces"
        Me.Button1.Visible = False
        '
        'tmSat
        '
        Me.tmSat.Interval = 5000
        '
        'Sysni
        '
        Me.Sysni.ContextMenu = Me.desMeni
        Me.Sysni.Icon = CType(resources.GetObject("Sysni.Icon"), System.Drawing.Icon)
        Me.Sysni.Text = "AllDowner"
        Me.Sysni.Visible = True
        '
        'desMeni
        '
        Me.desMeni.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem6, Me.MenuItem4, Me.MenuItem12, Me.MenuItem1, Me.MenuItem2, Me.MenuItem11, Me.MenuItem3})
        '
        'MenuItem6
        '
        Me.MenuItem6.Checked = True
        Me.MenuItem6.DefaultItem = True
        Me.MenuItem6.Enabled = False
        Me.MenuItem6.Index = 0
        Me.MenuItem6.Text = "Software by Stojiljkovic"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mi1, Me.mi3, Me.mi5, Me.mi7, Me.mi9, Me.MenuItem5, Me.mi10, Me.mi30, Me.mi50, Me.mi70, Me.mi90, Me.mi100})
        Me.MenuItem4.Text = "Providnost"
        '
        'mi1
        '
        Me.mi1.Index = 0
        Me.mi1.Text = "1 %"
        '
        'mi3
        '
        Me.mi3.Index = 1
        Me.mi3.Text = "3 %"
        '
        'mi5
        '
        Me.mi5.Index = 2
        Me.mi5.Text = "5 %"
        '
        'mi7
        '
        Me.mi7.Index = 3
        Me.mi7.Text = "7 %"
        '
        'mi9
        '
        Me.mi9.Index = 4
        Me.mi9.Text = "9 %"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 5
        Me.MenuItem5.Text = "-"
        '
        'mi10
        '
        Me.mi10.Index = 6
        Me.mi10.Text = "10 %"
        '
        'mi30
        '
        Me.mi30.Index = 7
        Me.mi30.Text = "30 %"
        '
        'mi50
        '
        Me.mi50.Index = 8
        Me.mi50.Text = "50 %"
        '
        'mi70
        '
        Me.mi70.Index = 9
        Me.mi70.Text = "70 %"
        '
        'mi90
        '
        Me.mi90.Index = 10
        Me.mi90.Text = "90 %"
        '
        'mi100
        '
        Me.mi100.Index = 11
        Me.mi100.Text = "100 %"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 2
        Me.MenuItem12.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 3
        Me.MenuItem1.Text = "Prikazi"
        '
        'MenuItem2
        '
        Me.MenuItem2.Enabled = False
        Me.MenuItem2.Index = 4
        Me.MenuItem2.Text = "Sakrij"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 5
        Me.MenuItem11.Text = "-"
        '
        'MenuItem3
        '
        Me.MenuItem3.DefaultItem = True
        Me.MenuItem3.Index = 6
        Me.MenuItem3.Text = "Izlaz"
        '
        'frmPrva
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.CancelButton = Me.Button2
        Me.ClientSize = New System.Drawing.Size(314, 199)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lbProcesi)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.pgProcenat)
        Me.Controls.Add(Me.sekund)
        Me.Controls.Add(Me.minut)
        Me.Controls.Add(Me.sat)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.paOdustani)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPrva"
        Me.Opacity = 0
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ALL Downer by Ivan� S"
        CType(Me.sat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.minut, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sekund, System.ComponentModel.ISupportInitialize).EndInit()
        Me.paOdustani.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim myProcesses() As Process
    Dim myProcess As Process
    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        broj = 1
        tVreme.Enabled = True
        pgProcenat.Value = 0
        pgProcenat.Maximum = 60 * 60 * sat.Value + 60 * minut.Value + sekund.Value
        paOdustani.Visible = True
        pgProcenat.Visible = True
        sekund.Enabled = False
        minut.Enabled = False
        sat.Enabled = False
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tVreme.Tick
        If (sekund.Value = 0) And (minut.Value = 0) And (sat.Value = 0) Then
            If broj = 2 Then
                Shell("tsshutdn 0 /powerdown /delay:0")
                tVreme.Enabled = False
                Exit Sub
            End If
            If broj = 3 Then
                Shell("tsshutdn 0 /reboot /delay:0")
                tVreme.Enabled = False
                Exit Sub
            End If
            If broj = 1 Then
                Shell("shutdown -l")
                tVreme.Enabled = False
                Exit Sub
            End If
            'tsdiscon.exe
            If broj = 4 Then
                'tVreme.Enabled = False
                Shell("tsdiscon")
                tVreme.Enabled = False
                Exit Sub
            End If
        End If

        If sekund.Value = 0 Then
            If minut.Value = 0 Then
                sat.Value = sat.Value - 1
                minut.Value = 59
            Else
                minut.Value = minut.Value - 1

            End If
            sekund.Value = 59
        Else
            sekund.Value = sekund.Value - 1
        End If

        pgProcenat.Value = pgProcenat.Value + 1

    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        broj = 2
        tVreme.Enabled = True
        pgProcenat.Value = 0
        pgProcenat.Maximum = 60 * 60 * sat.Value + 60 * minut.Value + sekund.Value
        paOdustani.Visible = True
        pgProcenat.Visible = True
        sekund.Enabled = False
        minut.Enabled = False
        sat.Enabled = False
    End Sub

    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click
        broj = 3
        tVreme.Enabled = True
        pgProcenat.Value = 0
        pgProcenat.Maximum = 60 * 60 * sat.Value + 60 * minut.Value + sekund.Value
        paOdustani.Visible = True
        pgProcenat.Visible = True
        sekund.Enabled = False
        minut.Enabled = False
        sat.Enabled = False
    End Sub

    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click
        sekund.Enabled = True
        minut.Enabled = True
        sat.Enabled = True

        tVreme.Enabled = False
        sat.Value = 0
        minut.Value = 0
        sekund.Value = 0
        pgProcenat.Value = 0
        paOdustani.Visible = False
        pgProcenat.Visible = False
    End Sub

    Private Sub PictureBox5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox5.Click
        broj = 4
        tVreme.Enabled = True
        pgProcenat.Value = 0
        pgProcenat.Maximum = 60 * 60 * sat.Value + 60 * minut.Value + sekund.Value
        paOdustani.Visible = True
        pgProcenat.Visible = True
        sekund.Enabled = False
        minut.Enabled = False
        sat.Enabled = False
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Dim myProcesses() As Process
        'Dim myProcess As Process
        MessageBox.Show("Neki od ovih procesa su sistemski," & vbCrLf & "njihovo prekidanje mo�e prouzrokovati" & vbCrLf & "N E S T A B I L N O S T   S I S T E M A   !", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Warning)

        Try
            ' Returns array containing all instances of "Notepad".
            myProcesses = Process.GetProcessesByName(lbProcesi.SelectedItem.ToString)
            For Each myProcess In myProcesses
                myProcess.Kill()
            Next
        Catch ex As Exception
            MessageBox.Show("Ovo je sistemski proces i nemoguce ga je prekinuti !", "Upozorenje", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End Try
    End Sub

    Private Sub tmSat_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmSat.Tick
        lbProcesi.Items.Clear()
        'Dim myProcesses() As Process
        'Dim myProcess As Process
        myProcesses = Process.GetProcesses
        ' Iterate through the process array.
        For Each myProcess In myProcesses
            lbProcesi.Items.Add(myProcess.ProcessName.ToString)
        Next
    End Sub

    Private Sub frmPrva_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lbProcesi.Items.Clear()
        'Dim myProcesses() As Process
        'Dim myProcess As Process
        myProcesses = Process.GetProcesses
        ' Iterate through the process array.
        For Each myProcess In myProcesses
            lbProcesi.Items.Add(myProcess.ProcessName.ToString)
        Next
        'Me.Hide()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Application.Exit()
        Me.Visible = False
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        mi100.Checked = True
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        mi100.Checked = False
        Me.Visible = False
        MenuItem1.Enabled = True
        MenuItem2.Enabled = False
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Application.Exit()
    End Sub

    Private Sub mi10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi10.Click
        Me.Opacity = 0.1
        mi10.Checked = Not mi10.Checked
        mi1.Checked = False
        mi3.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi30_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi30.Click
        Me.Opacity = 0.3
        mi30.Checked = Not mi30.Checked
        mi1.Checked = False
        mi3.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi10.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi50_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi50.Click
        Me.Opacity = 0.5
        mi50.Checked = Not mi50.Checked
        mi1.Checked = False
        mi3.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi30.Checked = False
        mi10.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi70_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi70.Click
        Me.Opacity = 0.7
        mi70.Checked = Not mi70.Checked
        mi1.Checked = False
        mi3.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi10.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi90_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi90.Click
        Me.Opacity = 0.9
        mi90.Checked = Not mi90.Checked
        mi1.Checked = False
        mi3.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi10.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi100_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi100.Click, Sysni.Click
        Me.Opacity = 1
        mi100.Checked = True 'Not mi100.Checked
        mi1.Checked = False
        mi3.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi10.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi1.Click
        Me.Opacity = 0.01
        mi1.Checked = Not mi1.Checked
        mi3.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi10.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi3.Click
        Me.Opacity = 0.03
        mi3.Checked = Not mi3.Checked
        mi1.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi10.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi5.Click
        Me.Opacity = 0.05
        mi5.Checked = Not mi5.Checked
        mi3.Checked = False
        mi1.Checked = False
        mi7.Checked = False
        mi9.Checked = False
        mi10.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi7.Click
        Me.Opacity = 0.07
        mi7.Checked = Not mi7.Checked
        mi3.Checked = False
        mi5.Checked = False
        mi1.Checked = False
        mi9.Checked = False
        mi10.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub

    Private Sub mi9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mi9.Click
        Me.Opacity = 0.09
        mi9.Checked = Not mi9.Checked
        mi3.Checked = False
        mi5.Checked = False
        mi7.Checked = False
        mi1.Checked = False
        mi10.Checked = False
        mi30.Checked = False
        mi50.Checked = False
        mi70.Checked = False
        mi90.Checked = False
        mi100.Checked = False
        Me.Visible = True
        MenuItem1.Enabled = False
        MenuItem2.Enabled = True
    End Sub
    Private Sub Sysni_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Sysni.Click
        'Me.Visible = True
        'Me.Opacity = 1
    End Sub
End Class
