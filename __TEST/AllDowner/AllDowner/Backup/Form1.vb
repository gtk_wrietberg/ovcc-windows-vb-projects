Imports System
Imports System.Drawing
Imports System.Windows.Forms

Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.components = New System.ComponentModel.Container
        Me.contextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem

        ' Initialize contextMenu1
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() _
                            {Me.MenuItem1})

        ' Initialize menuItem1
        Me.menuItem1.Index = 0
        Me.MenuItem1.Text = "E&xit"

        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() _
                                    {Me.MenuItem2})
        ' Initialize menuItem2
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "T&his program is private property of BossSoft"
        Me.MenuItem2.Enabled = False

        ' Set up how the form should be displayed.
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Text = "Notify Icon Example"

        ' Create the NotifyIcon.
        Me.notifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)

        ' The Icon property sets the icon that will appear
        ' in the systray for this application.
        notifyIcon1.Icon = New Icon("appicon.ico")

        ' The ContextMenu property sets the menu that will
        ' appear when the systray icon is right clicked.
        notifyIcon1.ContextMenu = Me.contextMenu1

        ' The Text property sets the text that will be displayed,
        ' in a tooltip, when the mouse hovers over the systray icon.
        NotifyIcon1.Text = "This program is private property of BossSoft"
        notifyIcon1.Visible = True
    End Sub 'New

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        ' Clean up any components being used.
        If Disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub 'Dispose

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    <System.STAThread()> _
       Public Shared Sub Main()
        System.Windows.Forms.Application.Run(New Form1)
    End Sub 'Main
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(Form1))
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "ivan"
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Text = "NotifyIcon1"
        Me.NotifyIcon1.Visible = True
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3})
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Izlaz"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "Ivan"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 273)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Opacity = 0.5
        Me.ShowInTaskbar = False
        Me.Text = "Shouldn't see me, hee, heee !"

    End Sub

#End Region
    Private Sub notifyIcon1_DoubleClick(ByVal Sender As Object, ByVal e As EventArgs) Handles NotifyIcon1.DoubleClick
        ' Show the form when the user double clicks on the notify icon.

        ' Set the WindowState to normal if the form is minimized.
        If (Me.WindowState = FormWindowState.Minimized) Then _
            Me.WindowState = FormWindowState.Normal

        ' Activate the form.
        Me.Activate()
    End Sub

    Private Sub menuItem1_Click(ByVal Sender As Object, ByVal e As EventArgs) Handles menuItem1.Click
        ' Close the form, which closes the application.
        Me.Close()
    End Sub
End Class
