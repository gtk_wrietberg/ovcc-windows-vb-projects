﻿Public Class Form1
    Private mValue As Integer
    Private mValues(10) As Integer
    Private mCalcs As Integer

#Region "thread safe"
    Delegate Sub UpdateTextbox_Callback(ByVal [name] As String, ByVal [text] As String)

    Public Sub UpdateTextbox(ByVal [num] As Integer, ByVal [text] As String)
        UpdateTextbox("TextBox" & [num].ToString, [text])
    End Sub

    Public Sub UpdateTextbox(ByVal [name] As String, ByVal [text] As String)
        Dim txtbox As New TextBox, bfound As Boolean = False

        For Each c As Control In Me.Controls
            If c.Name = name Then
                txtbox = c
                bfound = True

                Exit For
            End If
        Next

        If Not bfound Then
            Exit Sub
        End If

        If txtbox.InvokeRequired Then
            Dim d As New UpdateTextbox_Callback(AddressOf UpdateTextbox)
            Me.Invoke(d, New Object() {[name], [text]})
        Else
            txtbox.Text = [text]
        End If
    End Sub


#End Region

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ResetTextboxes()

        If Not Int32.TryParse(txtValue.Text, mValue) Then
            mValue = 10
            txtValue.Text = mValue.ToString
        Else

        End If

        Button1.Enabled = False
        Button2.Enabled = False

        bgWorker_Calc.RunWorkerAsync()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        ResetTextboxes()
    End Sub

    Private Sub ResetTextboxes()
        For i As Integer = 0 To 9
            mValues(i) = 0
        Next

        UpdateNumbersTextboxes()

        UpdateTextbox("txtProgress", "")
        UpdateTextbox("txtCalcs", "")
    End Sub

    Private Sub UpdateNumbersTextboxes()
        For i As Integer = 0 To 9
            UpdateTextbox(i, mValues(i).ToString)
        Next
    End Sub

    Private Sub bgWorker_Calc_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker_Calc.DoWork
        mCalcs = 0

        Dim k As Integer, si As String, sj As String, sk As String, m As Integer

        For i As Integer = 1 To mValue
            For j As Integer = 1 To 10
                'A * B = C

                'A
                sj = j.ToString
                For l As Integer = 0 To sj.Length - 1
                    m = CInt(sj.Substring(l, 1))

                    mValues(m) += 1
                Next


                'B
                si = i.ToString
                For l As Integer = 0 To si.Length - 1
                    m = CInt(si.Substring(l, 1))

                    mValues(m) += 1
                Next



                'C
                k = i * j
                sk = k.ToString

                'Console.WriteLine(i & " * " & j & " = " & k)

                For l As Integer = 0 To sk.Length - 1
                    m = CInt(sk.Substring(l, 1))

                    mValues(m) += 1
                Next

                UpdateNumbersTextboxes()
                mCalcs += 1
            Next

            bgWorker_Calc.ReportProgress(i)
        Next
    End Sub


    Private Sub bgWorker_Calc_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgWorker_Calc.ProgressChanged
        UpdateTextbox("txtProgress", e.ProgressPercentage.ToString)
        UpdateTextbox("txtCalcs", mCalcs.ToString)
    End Sub

    Private Sub bgWorker_Calc_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgWorker_Calc.RunWorkerCompleted
        Button1.Enabled = True
        Button2.Enabled = True
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim Black As Pen = New Pen(Color.Black, 3)
        Dim total As Decimal
        Dim data(5) As Decimal
        Dim percentage(5) As Decimal


        data(0) = Val(1)
        data(1) = Val(2)
        data(2) = Val(4)
        data(3) = Val(8)
        data(4) = Val(16)
        total = data(0) + data(1) + data(2) + data(3) + data(4)
        percentage(0) = data(0) / total
        percentage(1) = data(1) / total
        percentage(2) = data(2) / total
        percentage(3) = data(3) / total
        percentage(4) = data(4) / total
        Dim g As Graphics = PictureBox1.CreateGraphics()
        g.Clear(Color.LightSkyBlue)
        g.FillRectangle(Brushes.Blue, 30, 30, 30, 300 * percentage(0))

        g.FillRectangle(Brushes.Red, 65, 30, 30, 300 * percentage(1))

        g.FillRectangle(Brushes.Purple, 100, 30, 30, 300 * percentage(2))

        g.FillRectangle(Brushes.Green, 135, 30, 30, 300 * percentage(3))

        g.FillRectangle(Brushes.Yellow, 170, 30, 30, 300 * percentage(4))


    End Sub
End Class
