﻿Module Module1
    Const SC_MONITORPOWER As Integer = &HF170
    Const WM_SYSCOMMAND As Short = &H112S

    Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Long) As Long
    Private Declare Function GetDesktopWindow Lib "user32" () As Long

    Public Sub MonStandBy()
        Dim lH As Long = GetDesktopWindow

        MonStandBy(lH)
    End Sub

    Public Sub MonStandBy(hWnd As Int32)
        SendMessage(hWnd, WM_SYSCOMMAND, SC_MONITORPOWER, 1)
    End Sub


    Public Sub MonOff()
        Dim lH As Long = GetDesktopWindow

        MonOff(lH)
    End Sub

    Public Sub MonOff(hWnd As Int32)
        SendMessage(hWnd, WM_SYSCOMMAND, SC_MONITORPOWER, 2)
    End Sub


    Public Sub MonOn()
        Dim lH As Long = GetDesktopWindow

        MonOn(lH)
    End Sub

    Public Sub MonOn(hWnd As Int32)
        SendMessage(hWnd, WM_SYSCOMMAND, SC_MONITORPOWER, -1)
    End Sub
End Module
