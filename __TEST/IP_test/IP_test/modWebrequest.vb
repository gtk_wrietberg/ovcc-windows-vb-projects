﻿Module modWebrequest
    Public Sub SendRequest()
        Dim request As Net.WebRequest = Net.WebRequest.Create("https://lpchbserver01.ibahn.com/axis/servlet/HeartbeatReceiverServlet")

        request.Method = "POST"

        Dim xmlDoc As String
        xmlDoc = ""
        xmlDoc &= "<lobbypc>"
        xmlDoc &= "<build>2.1.136</build>"
        xmlDoc &= "<heartbeat>"
        'xmlDoc &= "<terminal-id>" & Environment.MachineName & "</terminal-id>"
        xmlDoc &= "<terminal-id>AEDXBABPC001</terminal-id>"
        xmlDoc &= "<internal-ip>" & GetIps() & "</internal-ip>"
        xmlDoc &= "</heartbeat>"
        xmlDoc &= "</lobbypc>"

        Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(xmlDoc)

        request.ContentType = "application/x-www-form-urlencoded"
        request.ContentLength = byteArray.Length

        Dim dataStream As IO.Stream = request.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()

        Dim response As Net.WebResponse = request.GetResponse()

        'MsgBox(CType(response, Net.HttpWebResponse).StatusDescription)

        dataStream = response.GetResponseStream()

        Dim reader As New IO.StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()


        'MsgBox(responseFromServer)
        Console.WriteLine(responseFromServer)



        reader.Close()
        dataStream.Close()
        response.Close()
    End Sub

    Public Sub SendAsynchRequest()
        Dim xmlDoc As String
        Dim request As Net.HttpWebRequest


        ' Create the request
        request = CType(Net.WebRequest.Create("https://lpchbserver01.ibahn.com/axis/servlet/HeartbeatReceiverServlet"), Net.HttpWebRequest)

        request.Method = "POST"


        xmlDoc = ""
        xmlDoc &= "<lobbypc>"
        xmlDoc &= "<build>2.1.136</build>"
        xmlDoc &= "<heartbeat>"
        xmlDoc &= "<terminal-id>" & Environment.MachineName & "</terminal-id>"
        xmlDoc &= "<internal-ip>" & GetIps() & "</internal-ip>"
        xmlDoc &= "</heartbeat>"
        xmlDoc &= "</lobbypc>"

        MsgBox(xmlDoc)


        ' Convert the xml doc string into a byte array
        Dim bytes As Byte()
        bytes = System.Text.Encoding.Unicode.GetBytes(xmlDoc)

        ' Assign the content length
        request.ContentLength = bytes.Length

        ' Write the xml doc bytes to request stream
        request.GetRequestStream.Write(bytes, 0, bytes.Length)

        Dim result As IAsyncResult
        Dim state As WebRequestState
        Dim timeout As Integer

        ' Create the state object used to access the web request
        state = New WebRequestState(request)

        ' Begin the async request
        result = request.BeginGetResponse(New AsyncCallback(AddressOf RequestComplete), state)

        ' Set timeout at 1 minute
        timeout = 1000 * 60

        ' Register a timeout for the async request
        Threading.ThreadPool.RegisterWaitForSingleObject(result.AsyncWaitHandle, New Threading.WaitOrTimerCallback(AddressOf TimeoutCallback), state, timeout, True)


    End Sub

    ' Method called when a request times out
    Private Sub TimeoutCallback(ByVal state As Object, ByVal timeOut As Boolean)
        If (timeOut) Then
            ' Abort the request
            CType(state, WebRequestState).Request.Abort()
        End If
    End Sub

    ' Method called when the request completes
    Private Sub RequestComplete(ByVal result As IAsyncResult)


        MsgBox("result.ToString=" & result.ToString)

        ' Get the request
        Dim request As Net.WebRequest
        request = DirectCast(result.AsyncState, WebRequestState).Request

        Dim response As Net.WebResponse
        response = request.EndGetResponse(result)



        MsgBox((New IO.StreamReader(response.GetResponseStream)).ReadToEnd())


        '    // grab the custom state object
        'RequestState state = (RequestState)result.AsyncState;
        'WebRequest request = (WebRequest)state.request;
        '// get the Response
        'HttpWebResponse response =
        '  (HttpWebResponse )request.EndGetResponse(result);
    End Sub

    ' Stores web request for access during async processing
    Private Class WebRequestState
        ' Holds the request object
        Public Request As Net.WebRequest

        Public Sub New(ByVal newRequest As Net.WebRequest)
            Request = newRequest
        End Sub
    End Class
End Module
