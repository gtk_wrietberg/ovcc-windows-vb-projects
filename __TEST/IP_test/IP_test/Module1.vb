﻿Imports System.Net

Module Module1
    Public Function GetIps() As String
        Dim strIP As String
        Dim listIp As New List(Of String)

        For Each ip As Net.IPAddress In Dns.GetHostEntry(Dns.GetHostName).AddressList
            Dim regexIP As System.Text.RegularExpressions.Regex
            regexIP = New System.Text.RegularExpressions.Regex("^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$")
            strIP = regexIP.Match(ip.ToString).Value

            If Not strIP.Equals("") Then
                listIp.Add(strIP)
            End If
        Next

        Return String.Join(",", listIp)
    End Function
End Module
