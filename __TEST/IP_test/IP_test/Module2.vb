﻿
Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.IO.Pipes
Imports System.Linq
Imports System.Net
Imports System.Net.Http
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Text
Imports System.Threading.Tasks

Namespace winKiosk
    Public Enum LOG_APPS
        _UNKNOWN = 0
        winKioskLogger = 1
        winKioskService = 2
        winKiosk = 3
        API = 4
        UI_HTML = 5
    End Enum

    Public Enum LOG_TYPE
        _UNKNOWN = 0
        Debug = 1
        Normal = 2
        Warning = 3
        [Error] = 4
        Fatal = 5
    End Enum

    Class clsLoggerClient
        'private readonly string log_server_url = "http://172.17.148.100/Kiosk/api/php/log/receiver.php";

        Public Property LogServerUrl() As String
            Get
                Return m_LogServerUrl
            End Get
            Set(value As String)
                m_LogServerUrl = Value
            End Set
        End Property
        Private m_LogServerUrl As String
        Public Property AppId() As LOG_APPS
            Get
                Return m_AppId
            End Get
            Set(value As LOG_APPS)
                m_AppId = Value
            End Set
        End Property
        Private m_AppId As LOG_APPS

        Public Sub SendLogMessage(message As String)
            Debug.WriteLine("SendLogMessage(string message)")

            Dim task As Task(Of [Boolean]) = _SendLogMessageAsync(Me.AppId, LOG_TYPE.Normal, message)
        End Sub

        Public Sub SendLogMessage(logType As LOG_TYPE, message As String)
            Debug.WriteLine("SendLogMessage(LOG_TYPE logType, string message)")

            Dim task As Task(Of [Boolean]) = _SendLogMessageAsync(Me.AppId, logType, message)
        End Sub

        Public Sub SendLogMessage(appId As LOG_APPS, logType As LOG_TYPE, message As String)
            Debug.WriteLine("SendLogMessage(LOG_APPS appId, LOG_TYPE logType, string message)")

            Dim task As Task(Of [Boolean]) = _SendLogMessageAsync(appId, logType, message)
        End Sub



        Private Async Function _SendLogMessageAsync(appId As LOG_APPS, logType As LOG_TYPE, message As String) As Task(Of [Boolean])
            Dim ret As [Boolean] = False

            Try
                Dim postData = New List(Of KeyValuePair(Of String, String))()
                postData.Add(New KeyValuePair(Of String, String)("appId", appId.ToString()))
                postData.Add(New KeyValuePair(Of String, String)("logType", logType.ToString()))
                postData.Add(New KeyValuePair(Of String, String)("message", WebUtility.HtmlEncode(message)))

                Dim content As HttpContent = New FormUrlEncodedContent(postData)
                Dim getResponseTask As Task(Of HttpResponseMessage) = (New HttpClient()).PostAsync(Me.LogServerUrl, content)
                Dim webResponse As HttpResponseMessage = Await getResponseTask

                Dim responseBodyAsText As String = Await webResponse.Content.ReadAsStringAsync()

                ret = True
            Catch e As Exception
                Debug.WriteLine((Convert.ToString("ERROR: Writing to ") & Me.LogServerUrl) + " failed")
                Debug.WriteLine("ERROR: " + e.Message)
            End Try


            Return ret
        End Function
    End Class
End Namespace

