Imports FlickrNet

Public Class Example1Form

    Private Sub SimpleSearchButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleSearchButton1.Click

        OutputTextbox.Text = ""

        Dim key As String = ApiKey.Text

        Dim f As Flickr = New Flickr(key)

        Dim options As PhotoSearchOptions = New PhotoSearchOptions()
        options.Tags = "microsoft"
        options.PerPage = 10
        Dim microsoftPhotos As Photos = f.PhotosSearch(options)
        options.Page = 2
        Dim microsoftPhotos2 As Photos = f.PhotosSearch(options)
        options.Page = 3
        Dim microsoftPhotos3 As Photos = f.PhotosSearch(options)

        Dim allPhotos As PhotoCollection = microsoftPhotos.PhotoCollection
        allPhotos.AddRange(microsoftPhotos2.PhotoCollection)
        allPhotos.AddRange(microsoftPhotos3.PhotoCollection)

        For Each photo As Photo In allPhotos
            OutputTextbox.Text &= "Photos title is " & photo.Title & " " & photo.WebUrl & vbCrLf
        Next

    End Sub

    Private Sub FindUserButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FindUserButton.Click

        Dim f As Flickr = New Flickr(ApiKey.Text)
        Dim user As FoundUser
        Try
            user = f.PeopleFindByUsername(Username.Text)
            OutputTextbox.Text = "User Id = " & user.UserId & vbCrLf & "Username = " & user.Username & vbCrLf
        Catch ex As FlickrException
            OutputTextbox.Text = ex.Message
            Exit Sub
        End Try

        Dim userSearch As PhotoSearchOptions = New PhotoSearchOptions()
        userSearch.UserId = user.UserId
        userSearch.SortOrder = PhotoSearchSortOrder.InterestingnessDesc

        Dim usersPhotos As Photos = f.PhotosSearch(userSearch)
        Dim contacts As Contacts = f.ContactsGetPublicList(user.UserId)
        Dim usersFavoritePhotos As Photos = f.FavoritesGetPublicList(user.UserId)
        Dim usersGroups As PublicGroupInfo() = f.PeopleGetPublicGroups(user.UserId)

        Dim i As Integer = 0

        For Each contact As Contact In contacts.ContactCollection
            OutputTextbox.Text &= "Contact " & contact.UserName & vbCrLf
            If i > 10 Then Exit For
            i += 1
        Next

        i = 0
        For Each group As PublicGroupInfo In usersGroups
            OutputTextbox.Text &= "Group " & group.GroupName & vbCrLf
            If i > 10 Then Exit For
            i += 1
        Next

        i = 0
        For Each photo As Photo In usersPhotos.PhotoCollection
            OutputTextbox.Text &= "Interesting photo title is " & photo.Title & " " & photo.WebUrl & vbCrLf
            If i > 10 Then Exit For
            i += 1
        Next

        i = 0
        For Each photo As Photo In usersFavoritePhotos.PhotoCollection
            OutputTextbox.Text &= "Favourite photo title is " & photo.Title & " " & photo.WebUrl & vbCrLf
            If i > 10 Then Exit For
            i += 1
        Next

    End Sub
End Class
