<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.UpdateFormButton = New System.Windows.Forms.Button
        Me.button3 = New System.Windows.Forms.Button
        Me.button2 = New System.Windows.Forms.Button
        Me.button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'UpdateFormButton
        '
        Me.UpdateFormButton.Location = New System.Drawing.Point(72, 179)
        Me.UpdateFormButton.Name = "UpdateFormButton"
        Me.UpdateFormButton.Size = New System.Drawing.Size(133, 23)
        Me.UpdateFormButton.TabIndex = 7
        Me.UpdateFormButton.Text = "Update Form"
        Me.UpdateFormButton.UseVisualStyleBackColor = True
        '
        'button3
        '
        Me.button3.Location = New System.Drawing.Point(72, 133)
        Me.button3.Name = "button3"
        Me.button3.Size = New System.Drawing.Size(133, 23)
        Me.button3.TabIndex = 6
        Me.button3.Text = "Upload Form"
        Me.button3.UseVisualStyleBackColor = True
        '
        'button2
        '
        Me.button2.Location = New System.Drawing.Point(72, 88)
        Me.button2.Name = "button2"
        Me.button2.Size = New System.Drawing.Size(133, 23)
        Me.button2.TabIndex = 5
        Me.button2.Text = "Authentication Form"
        Me.button2.UseVisualStyleBackColor = True
        '
        'button1
        '
        Me.button1.Location = New System.Drawing.Point(72, 44)
        Me.button1.Name = "button1"
        Me.button1.Size = New System.Drawing.Size(133, 23)
        Me.button1.TabIndex = 4
        Me.button1.Text = "Example Form 1"
        Me.button1.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.UpdateFormButton)
        Me.Controls.Add(Me.button3)
        Me.Controls.Add(Me.button2)
        Me.Controls.Add(Me.button1)
        Me.Name = "MainForm"
        Me.Text = "MainForm"
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents UpdateFormButton As System.Windows.Forms.Button
    Private WithEvents button3 As System.Windows.Forms.Button
    Private WithEvents button2 As System.Windows.Forms.Button
    Private WithEvents button1 As System.Windows.Forms.Button
End Class
