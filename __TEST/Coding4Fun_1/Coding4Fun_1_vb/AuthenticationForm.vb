Imports FlickrNet

Public Class AuthenticationForm

    Private frob As String

    Private Sub Step1Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Step1Button.Click

        Dim f As Flickr = New Flickr(ApiKey.Text, SharedSecret.Text)

        frob = f.AuthGetFrob()

        OutputTextbox.Text &= "Frob = " & frob & vbCrLf

        Dim url As String = f.AuthCalcUrl(frob, AuthLevel.Write)

        OutputTextbox.Text &= "Url = " & url & vbCrLf

        System.Diagnostics.Process.Start(url)

        Step1Button.Enabled = False
        Step2Button.Enabled = True
        Step3Button.Enabled = True

    End Sub

    Private Sub Step2Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Step2Button.Click

        Dim f As Flickr = New Flickr(ApiKey.Text, SharedSecret.Text)

        Try

            Dim a As Auth = f.AuthGetToken(frob)
            OutputTextbox.Text &= "User Authenticated = " & a.User.Username & vbCrLf
            OutputTextbox.Text &= "Auth Token = " & a.Token & vbCrLf
            AuthToken.Text = a.Token

        Catch ex As FlickrException

            OutputTextbox.Text &= "Authentication failed : " & ex.Message & vbCrLf

        End Try

        Step1Button.Enabled = True
        Step2Button.Enabled = False
        Step3Button.Enabled = False

    End Sub

    Private Sub Step3Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Step3Button.Click

        Step1Button.Enabled = True
        Step2Button.Enabled = False
        Step3Button.Enabled = False

    End Sub

End Class
