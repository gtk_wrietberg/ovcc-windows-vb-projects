Public Class MainForm

    Private Sub button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button1.Click

        Dim frm As Example1Form = New Example1Form()
        frm.Show()

    End Sub

    Private Sub button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button2.Click

        Dim frm As AuthenticationForm = New AuthenticationForm()
        frm.Show()

    End Sub

    Private Sub button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles button3.Click

        Dim frm As UploadForm = New UploadForm()
        frm.Show()

    End Sub

    Private Sub UpdateFormButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdateFormButton.Click

        Dim frm As UpdateForm = New UpdateForm()
        frm.Show()

    End Sub

End Class