<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TemplateForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OutputTextbox = New System.Windows.Forms.RichTextBox
        Me.ApiKey = New System.Windows.Forms.TextBox
        Me.label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'OutputTextbox
        '
        Me.OutputTextbox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OutputTextbox.Location = New System.Drawing.Point(12, 94)
        Me.OutputTextbox.Name = "OutputTextbox"
        Me.OutputTextbox.Size = New System.Drawing.Size(598, 253)
        Me.OutputTextbox.TabIndex = 7
        Me.OutputTextbox.Text = ""
        '
        'ApiKey
        '
        Me.ApiKey.Location = New System.Drawing.Point(88, 6)
        Me.ApiKey.Name = "ApiKey"
        Me.ApiKey.Size = New System.Drawing.Size(196, 20)
        Me.ApiKey.TabIndex = 6
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(12, 9)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(70, 13)
        Me.label1.TabIndex = 5
        Me.label1.Text = "Your API Key"
        '
        'TemplateForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(620, 359)
        Me.Controls.Add(Me.OutputTextbox)
        Me.Controls.Add(Me.ApiKey)
        Me.Controls.Add(Me.label1)
        Me.Name = "TemplateForm"
        Me.Text = "Template Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Protected WithEvents OutputTextbox As System.Windows.Forms.RichTextBox
    Protected WithEvents ApiKey As System.Windows.Forms.TextBox
    Private WithEvents label1 As System.Windows.Forms.Label

End Class
