<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AuthenticationForm
    Inherits Coding4Fun_1_vb.TemplateForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Step3Button = New System.Windows.Forms.Button
        Me.SharedSecret = New System.Windows.Forms.TextBox
        Me.AuthToken = New System.Windows.Forms.TextBox
        Me.Step2Button = New System.Windows.Forms.Button
        Me.Step1Button = New System.Windows.Forms.Button
        Me.label2 = New System.Windows.Forms.Label
        Me.label3 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'OutputTextbox
        '
        Me.OutputTextbox.Size = New System.Drawing.Size(598, 250)
        '
        'Step3Button
        '
        Me.Step3Button.Enabled = False
        Me.Step3Button.Location = New System.Drawing.Point(179, 65)
        Me.Step3Button.Name = "Step3Button"
        Me.Step3Button.Size = New System.Drawing.Size(75, 23)
        Me.Step3Button.TabIndex = 14
        Me.Step3Button.Text = "Cancel"
        Me.Step3Button.UseVisualStyleBackColor = True
        '
        'SharedSecret
        '
        Me.SharedSecret.Location = New System.Drawing.Point(396, 6)
        Me.SharedSecret.Name = "SharedSecret"
        Me.SharedSecret.Size = New System.Drawing.Size(106, 21)
        Me.SharedSecret.TabIndex = 16
        '
        'AuthToken
        '
        Me.AuthToken.Location = New System.Drawing.Point(88, 33)
        Me.AuthToken.Name = "AuthToken"
        Me.AuthToken.Size = New System.Drawing.Size(196, 21)
        Me.AuthToken.TabIndex = 18
        '
        'Step2Button
        '
        Me.Step2Button.Enabled = False
        Me.Step2Button.Location = New System.Drawing.Point(97, 65)
        Me.Step2Button.Name = "Step2Button"
        Me.Step2Button.Size = New System.Drawing.Size(75, 23)
        Me.Step2Button.TabIndex = 13
        Me.Step2Button.Text = "Continue"
        Me.Step2Button.UseVisualStyleBackColor = True
        '
        'Step1Button
        '
        Me.Step1Button.Location = New System.Drawing.Point(15, 65)
        Me.Step1Button.Name = "Step1Button"
        Me.Step1Button.Size = New System.Drawing.Size(75, 23)
        Me.Step1Button.TabIndex = 12
        Me.Step1Button.Text = "Authenticate"
        Me.Step1Button.UseVisualStyleBackColor = True
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(290, 9)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(100, 13)
        Me.label2.TabIndex = 15
        Me.label2.Text = "Your Shared Secret"
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(12, 36)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(62, 13)
        Me.label3.TabIndex = 17
        Me.label3.Text = "Auth Token"
        '
        'AuthenticationForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(620, 356)
        Me.Controls.Add(Me.Step3Button)
        Me.Controls.Add(Me.SharedSecret)
        Me.Controls.Add(Me.AuthToken)
        Me.Controls.Add(Me.Step2Button)
        Me.Controls.Add(Me.Step1Button)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.label3)
        Me.Name = "AuthenticationForm"
        Me.Controls.SetChildIndex(Me.ApiKey, 0)
        Me.Controls.SetChildIndex(Me.OutputTextbox, 0)
        Me.Controls.SetChildIndex(Me.label3, 0)
        Me.Controls.SetChildIndex(Me.label2, 0)
        Me.Controls.SetChildIndex(Me.Step1Button, 0)
        Me.Controls.SetChildIndex(Me.Step2Button, 0)
        Me.Controls.SetChildIndex(Me.AuthToken, 0)
        Me.Controls.SetChildIndex(Me.SharedSecret, 0)
        Me.Controls.SetChildIndex(Me.Step3Button, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents Step3Button As System.Windows.Forms.Button
    Protected WithEvents SharedSecret As System.Windows.Forms.TextBox
    Protected WithEvents AuthToken As System.Windows.Forms.TextBox
    Private WithEvents Step2Button As System.Windows.Forms.Button
    Private WithEvents Step1Button As System.Windows.Forms.Button
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents label3 As System.Windows.Forms.Label

End Class
