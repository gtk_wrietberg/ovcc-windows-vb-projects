Imports FlickrNet

Public Class UpdateForm

    Private Sub GetPhotoButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GetPhotoButton.Click

        Dim f As Flickr = New Flickr(ApiKey.Text, SharedSecret.Text, AuthToken.Text)
        Dim a As Auth = f.AuthCheckToken(AuthToken.Text)
        Dim options As PhotoSearchOptions = New PhotoSearchOptions(a.User.UserId)
        options.SortOrder = PhotoSearchSortOrder.DatePostedDesc
        options.PerPage = 1

        Dim photos As Photos = f.PhotosSearch(options)
        Flickr.FlushCache(f.LastRequest)

        Dim photo As Photo = photos.PhotoCollection(0)

        webBrowser1.Navigate(photo.SmallUrl)

        OldTitle.Text = photo.Title
        PhotoId.Text = photo.PhotoId

    End Sub

End Class
