Imports FlickrNet

Public Class UploadForm

    Private Sub FindFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FindFileButton.Click

        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Exit Sub

        Filename.Text = OpenFileDialog1.FileName

    End Sub

    Private Sub UploadFileButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UploadFileButton.Click

        Dim f As Flickr = New Flickr(ApiKey.Text, SharedSecret.Text, AuthToken.Text)

        Dim uploadAsPublic As Boolean = False

        Dim title As String = "Test title"
        Dim description As String = "Test Description"

        Dim photoId As String = f.UploadPicture(Filename.Text, title, description, "", uploadAsPublic, False, False)

        OutputTextbox.Text = "Photo uploaded Successfully" & vbCrLf
        OutputTextbox.Text &= "Photo Id = " & photoId & vbCrLf

    End Sub
End Class
