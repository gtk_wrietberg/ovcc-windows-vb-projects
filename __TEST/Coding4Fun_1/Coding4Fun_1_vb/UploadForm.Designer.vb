<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UploadForm
    Inherits Coding4Fun_1_vb.AuthenticationForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.UploadFileButton = New System.Windows.Forms.Button
        Me.FindFileButton = New System.Windows.Forms.Button
        Me.label4 = New System.Windows.Forms.Label
        Me.Filename = New System.Windows.Forms.TextBox
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.SuspendLayout()
        '
        'UploadFileButton
        '
        Me.UploadFileButton.Location = New System.Drawing.Point(350, 65)
        Me.UploadFileButton.Name = "UploadFileButton"
        Me.UploadFileButton.Size = New System.Drawing.Size(85, 23)
        Me.UploadFileButton.TabIndex = 22
        Me.UploadFileButton.Text = "Upload File"
        Me.UploadFileButton.UseVisualStyleBackColor = True
        '
        'FindFileButton
        '
        Me.FindFileButton.Location = New System.Drawing.Point(566, 33)
        Me.FindFileButton.Name = "FindFileButton"
        Me.FindFileButton.Size = New System.Drawing.Size(24, 20)
        Me.FindFileButton.TabIndex = 21
        Me.FindFileButton.Text = ".."
        Me.FindFileButton.UseVisualStyleBackColor = True
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(294, 36)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(49, 13)
        Me.label4.TabIndex = 19
        Me.label4.Text = "Filename"
        '
        'Filename
        '
        Me.Filename.Location = New System.Drawing.Point(349, 33)
        Me.Filename.Name = "Filename"
        Me.Filename.Size = New System.Drawing.Size(208, 20)
        Me.Filename.TabIndex = 20
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Upload
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(620, 359)
        Me.Controls.Add(Me.UploadFileButton)
        Me.Controls.Add(Me.FindFileButton)
        Me.Controls.Add(Me.label4)
        Me.Controls.Add(Me.Filename)
        Me.Name = "Upload"
        Me.Controls.SetChildIndex(Me.ApiKey, 0)
        Me.Controls.SetChildIndex(Me.OutputTextbox, 0)
        Me.Controls.SetChildIndex(Me.AuthToken, 0)
        Me.Controls.SetChildIndex(Me.SharedSecret, 0)
        Me.Controls.SetChildIndex(Me.Filename, 0)
        Me.Controls.SetChildIndex(Me.label4, 0)
        Me.Controls.SetChildIndex(Me.FindFileButton, 0)
        Me.Controls.SetChildIndex(Me.UploadFileButton, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents UploadFileButton As System.Windows.Forms.Button
    Private WithEvents FindFileButton As System.Windows.Forms.Button
    Private WithEvents label4 As System.Windows.Forms.Label
    Private WithEvents Filename As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog

End Class
