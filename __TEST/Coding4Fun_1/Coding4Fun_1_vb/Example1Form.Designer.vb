<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Example1Form
    Inherits Coding4Fun_1_vb.TemplateForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SimpleSearchButton1 = New System.Windows.Forms.Button
        Me.FindUserButton = New System.Windows.Forms.Button
        Me.Username = New System.Windows.Forms.TextBox
        Me.label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'SimpleSearchButton1
        '
        Me.SimpleSearchButton1.Location = New System.Drawing.Point(12, 43)
        Me.SimpleSearchButton1.Name = "SimpleSearchButton1"
        Me.SimpleSearchButton1.Size = New System.Drawing.Size(98, 23)
        Me.SimpleSearchButton1.TabIndex = 8
        Me.SimpleSearchButton1.Text = "Simple Search 1"
        Me.SimpleSearchButton1.UseVisualStyleBackColor = True
        '
        'FindUserButton
        '
        Me.FindUserButton.Location = New System.Drawing.Point(196, 44)
        Me.FindUserButton.Name = "FindUserButton"
        Me.FindUserButton.Size = New System.Drawing.Size(98, 23)
        Me.FindUserButton.TabIndex = 9
        Me.FindUserButton.Text = "Find User"
        Me.FindUserButton.UseVisualStyleBackColor = True
        '
        'Username
        '
        Me.Username.Location = New System.Drawing.Point(372, 46)
        Me.Username.Name = "Username"
        Me.Username.Size = New System.Drawing.Size(100, 20)
        Me.Username.TabIndex = 11
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(304, 49)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(58, 13)
        Me.label2.TabIndex = 10
        Me.label2.Text = "Username:"
        '
        'Example1Form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(620, 359)
        Me.Controls.Add(Me.FindUserButton)
        Me.Controls.Add(Me.Username)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.SimpleSearchButton1)
        Me.Name = "Example1Form"
        Me.Controls.SetChildIndex(Me.ApiKey, 0)
        Me.Controls.SetChildIndex(Me.OutputTextbox, 0)
        Me.Controls.SetChildIndex(Me.SimpleSearchButton1, 0)
        Me.Controls.SetChildIndex(Me.label2, 0)
        Me.Controls.SetChildIndex(Me.Username, 0)
        Me.Controls.SetChildIndex(Me.FindUserButton, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents SimpleSearchButton1 As System.Windows.Forms.Button
    Private WithEvents FindUserButton As System.Windows.Forms.Button
    Private WithEvents Username As System.Windows.Forms.TextBox
    Private WithEvents label2 As System.Windows.Forms.Label

End Class
