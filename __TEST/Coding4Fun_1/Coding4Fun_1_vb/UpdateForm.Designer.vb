<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpdateForm
    Inherits Coding4Fun_1_vb.AuthenticationForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PhotoId = New System.Windows.Forms.Label
        Me.UpdatePhotoButton = New System.Windows.Forms.Button
        Me.GetPhotoButton = New System.Windows.Forms.Button
        Me.OldTitle = New System.Windows.Forms.Label
        Me.label5 = New System.Windows.Forms.Label
        Me.webBrowser1 = New System.Windows.Forms.WebBrowser
        Me.NewTitle = New System.Windows.Forms.TextBox
        Me.label4 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'OutputTextbox
        '
        Me.OutputTextbox.Size = New System.Drawing.Size(272, 253)
        '
        'PhotoId
        '
        Me.PhotoId.AutoSize = True
        Me.PhotoId.Location = New System.Drawing.Point(568, 121)
        Me.PhotoId.Name = "PhotoId"
        Me.PhotoId.Size = New System.Drawing.Size(35, 13)
        Me.PhotoId.TabIndex = 28
        Me.PhotoId.Text = "label6"
        Me.PhotoId.Visible = False
        '
        'UpdatePhotoButton
        '
        Me.UpdatePhotoButton.Location = New System.Drawing.Point(517, 74)
        Me.UpdatePhotoButton.Name = "UpdatePhotoButton"
        Me.UpdatePhotoButton.Size = New System.Drawing.Size(86, 23)
        Me.UpdatePhotoButton.TabIndex = 27
        Me.UpdatePhotoButton.Text = "Update Photo"
        Me.UpdatePhotoButton.UseVisualStyleBackColor = True
        '
        'GetPhotoButton
        '
        Me.GetPhotoButton.Location = New System.Drawing.Point(517, 40)
        Me.GetPhotoButton.Name = "GetPhotoButton"
        Me.GetPhotoButton.Size = New System.Drawing.Size(86, 23)
        Me.GetPhotoButton.TabIndex = 26
        Me.GetPhotoButton.Text = "Get Photo"
        Me.GetPhotoButton.UseVisualStyleBackColor = True
        '
        'OldTitle
        '
        Me.OldTitle.AutoSize = True
        Me.OldTitle.Location = New System.Drawing.Point(345, 50)
        Me.OldTitle.Name = "OldTitle"
        Me.OldTitle.Size = New System.Drawing.Size(0, 13)
        Me.OldTitle.TabIndex = 25
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Location = New System.Drawing.Point(295, 50)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(49, 13)
        Me.label5.TabIndex = 24
        Me.label5.Text = "Old Title:"
        '
        'webBrowser1
        '
        Me.webBrowser1.Location = New System.Drawing.Point(327, 123)
        Me.webBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webBrowser1.Name = "webBrowser1"
        Me.webBrowser1.Size = New System.Drawing.Size(214, 185)
        Me.webBrowser1.TabIndex = 23
        '
        'NewTitle
        '
        Me.NewTitle.Location = New System.Drawing.Point(348, 77)
        Me.NewTitle.Name = "NewTitle"
        Me.NewTitle.Size = New System.Drawing.Size(163, 20)
        Me.NewTitle.TabIndex = 22
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(295, 80)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(55, 13)
        Me.label4.TabIndex = 21
        Me.label4.Text = "New Title:"
        '
        'UpdateForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(620, 359)
        Me.Controls.Add(Me.PhotoId)
        Me.Controls.Add(Me.UpdatePhotoButton)
        Me.Controls.Add(Me.GetPhotoButton)
        Me.Controls.Add(Me.OldTitle)
        Me.Controls.Add(Me.label5)
        Me.Controls.Add(Me.webBrowser1)
        Me.Controls.Add(Me.NewTitle)
        Me.Controls.Add(Me.label4)
        Me.Name = "UpdateForm"
        Me.Controls.SetChildIndex(Me.ApiKey, 0)
        Me.Controls.SetChildIndex(Me.OutputTextbox, 0)
        Me.Controls.SetChildIndex(Me.AuthToken, 0)
        Me.Controls.SetChildIndex(Me.SharedSecret, 0)
        Me.Controls.SetChildIndex(Me.label4, 0)
        Me.Controls.SetChildIndex(Me.NewTitle, 0)
        Me.Controls.SetChildIndex(Me.webBrowser1, 0)
        Me.Controls.SetChildIndex(Me.label5, 0)
        Me.Controls.SetChildIndex(Me.OldTitle, 0)
        Me.Controls.SetChildIndex(Me.GetPhotoButton, 0)
        Me.Controls.SetChildIndex(Me.UpdatePhotoButton, 0)
        Me.Controls.SetChildIndex(Me.PhotoId, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents PhotoId As System.Windows.Forms.Label
    Private WithEvents UpdatePhotoButton As System.Windows.Forms.Button
    Private WithEvents GetPhotoButton As System.Windows.Forms.Button
    Private WithEvents OldTitle As System.Windows.Forms.Label
    Private WithEvents label5 As System.Windows.Forms.Label
    Private WithEvents webBrowser1 As System.Windows.Forms.WebBrowser
    Private WithEvents NewTitle As System.Windows.Forms.TextBox
    Private WithEvents label4 As System.Windows.Forms.Label

End Class
