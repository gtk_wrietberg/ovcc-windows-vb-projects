using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using FlickrNet;

namespace Coding4Fun_1
{
    public partial class ExampleForm1 : TemplateForm
    {
        public ExampleForm1()
        {
            InitializeComponent();
        }

        private void SimpleSearchButton1_Click(object sender, EventArgs e)
        {
            OutputTextbox.Text = "";

            // Example 1
            string apikey = ApiKey.Text;
            Flickr flickr = new Flickr(apikey);

            // Example 2
            PhotoSearchOptions searchOptions = new PhotoSearchOptions();
            searchOptions.Tags = "microsoft";
            searchOptions.PerPage = 10;
            Photos microsoftPhotos = flickr.PhotosSearch(searchOptions);

            // Example 3
            searchOptions.Page = 2;
            Photos microsoftPhotos2 = flickr.PhotosSearch(searchOptions);
            searchOptions.Page = 3;
            Photos microsoftPhotos3 = flickr.PhotosSearch(searchOptions);

            // Eample 4
            PhotoCollection allPhotos = microsoftPhotos.PhotoCollection;
            allPhotos.AddRange(microsoftPhotos2.PhotoCollection);
            allPhotos.AddRange(microsoftPhotos3.PhotoCollection);
            foreach (Photo photo in allPhotos)
            {
                OutputTextbox.Text += "Photos title is " + photo.Title + " " + photo.WebUrl + "\r\n";
            }
        }

        private void FindUserButton_Click(object sender, EventArgs e)
        {
            // First page of the users photos
            // Sorted by interestingness

            Flickr flickr = new Flickr(ApiKey.Text);
            FoundUser user;
            try
            {
                user = flickr.PeopleFindByUsername(Username.Text);
                OutputTextbox.Text = "User Id = " + user.UserId + "\r\n" + "Username = " + user.Username + "\r\n";
            }
            catch (FlickrException ex)
            {
                OutputTextbox.Text = ex.Message;
                return;
            }

            PhotoSearchOptions userSearch = new PhotoSearchOptions();
            userSearch.UserId = user.UserId;
            userSearch.SortOrder = PhotoSearchSortOrder.InterestingnessDesc;
            Photos usersPhotos = flickr.PhotosSearch(userSearch);
            // Get users contacts
            Contacts contacts = flickr.ContactsGetPublicList(user.UserId);
            // Get first page of a users favorites
            Photos usersFavoritePhotos = flickr.FavoritesGetPublicList(user.UserId);
            // Get a list of the users groups
            PublicGroupInfo[] usersGroups = flickr.PeopleGetPublicGroups(user.UserId);

            int i = 0;
            foreach (Contact contact in contacts.ContactCollection)
            {
                OutputTextbox.Text += "Contact " + contact.UserName + "\r\n";
                if (i++ > 10) break; // only list the first 10
            }

            i = 0;
            foreach (PublicGroupInfo group in usersGroups)
            {
                OutputTextbox.Text += "Group " + group.GroupName + "\r\n";
                if (i++ > 10) break; // only list the first 10
            }

            i = 0;
            foreach (Photo photo in usersPhotos.PhotoCollection)
            {
                OutputTextbox.Text += "Interesting photo title is " + photo.Title + " " + photo.WebUrl + "\r\n";
                if (i++ > 10) break; // only list the first 10
            }

            i = 0;
            foreach (Photo photo in usersFavoritePhotos.PhotoCollection)
            {
                OutputTextbox.Text += "Favourite photo title is " + photo.Title + " " + photo.WebUrl + "\r\n";
                if (i++ > 10) break; // only list the first 10
            }
        }
    }
}


