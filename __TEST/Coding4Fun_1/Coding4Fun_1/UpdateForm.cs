using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FlickrNet;


namespace Coding4Fun_1
{
    public partial class UpdateForm : Coding4Fun_1.AuthenticationForm
    {
        public UpdateForm()
        {
            InitializeComponent();
        }

        private void GetPhotoButton_Click(object sender, EventArgs e)
        {
            Flickr flickr = new Flickr(ApiKey.Text, SharedSecret.Text, AuthToken.Text);

            Auth auth = flickr.AuthCheckToken(AuthToken.Text);

            PhotoSearchOptions options = new PhotoSearchOptions(auth.User.UserId);
            options.SortOrder = PhotoSearchSortOrder.DatePostedDesc;
            options.PerPage = 1;

            Photos photos = flickr.PhotosSearch(options);

            Flickr.FlushCache(flickr.LastRequest);

            Photo photo = photos.PhotoCollection[0];

            webBrowser1.Navigate(photo.SmallUrl);

            OldTitle.Text = photo.Title;
            PhotoId.Text = photo.PhotoId;
        }

        private void UpdatePhotoButton_Click(object sender, EventArgs e)
        {

            Flickr flickr = new Flickr(ApiKey.Text, SharedSecret.Text, AuthToken.Text);

            flickr.PhotosSetMeta(PhotoId.Text, NewTitle.Text, null);

            OutputTextbox.Text += "Photo title updated";

            Photosets sets = flickr.PhotosetsGetList();
            Photoset set = sets.PhotosetCollection[0];

            flickr.PhotosetsAddPhoto(set.PhotosetId, PhotoId.Text);
        }
    }
}

