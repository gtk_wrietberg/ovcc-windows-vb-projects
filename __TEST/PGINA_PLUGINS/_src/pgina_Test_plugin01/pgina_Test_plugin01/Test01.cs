﻿using log4net;
using pGina.Shared.Interfaces;
using pGina.Shared.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace pgina_Test_plugin01
{
    public class Test01 : IPluginAuthentication
    {
        private static readonly Guid m_uuid = new Guid("A461D996-2B4C-4BE9-A668-3B4B6BD21524");

        private static dynamic m_settings;
        internal static dynamic Settings { get { return m_settings; } }

        private ILog m_logger = LogManager.GetLogger("pgina_Test_plugin01_Plugin");

        public Test01()
        {
            using (Process me = Process.GetCurrentProcess())
            {
                Settings.Init();
                m_logger.DebugFormat("Plugin initialized on {0} in PID: {1} Session: {2}", Environment.MachineName, me.Id, me.SessionId);
            }

            m_settings = new pGina.Shared.Settings.pGinaDynamicSettings(m_uuid);

            m_settings.SetDefault("Username", "ovcctest");
            m_settings.SetDefault("Password", "0VCCt3st");

        }

        public string Name
        {
            get { return "pgina_Test_plugin01"; }
        }

        public string Description
        {
            get { return "Blablabla"; }
        }

        
        public Guid Uuid
        {
            get { return m_uuid; }
        }

        public string Version
        {
            get
            {
                return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public void Starting() { }

        public void Stopping() { }

        public BooleanResult AuthenticateUser(SessionProperties properties)
        {
            UserInformation userInfo = properties.GetTrackedSingle<UserInformation>();

            //properties.GetTrackedSingle<UserInformation>().Username = "ovcctest";


            string defaultUsername = Settings.Store.Username;
            string domain = Settings.Store.Domain;
            string defaultPassword = Settings.Store.GetEncryptedSetting("Password", null);


            //if (userInfo.Username.Contains("hello") && userInfo.Password.Contains("pGina"))
            //{
            //    // Successful authentication
            //    return new BooleanResult() { Success = true };
            //}
            //// Authentication failure



            return new BooleanResult() { Success = false, Message = "Incorrect username or password." };
        }
    }
}
