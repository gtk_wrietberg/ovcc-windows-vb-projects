﻿Imports System.Security.Principal
Imports System.DirectoryServices.ActiveDirectory
Imports System.DirectoryServices.AccountManagement

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        MsgBox(GetDomainSid)
        'Dim sid As SecurityIdentifier = New SecurityIdentifier(WellKnownSidType.AccountAdministratorSid, Nothing)

        'MsgBox(sid.ToString)
    End Sub

    Public Function GetDomainSid() As String
        Dim domainSid As Byte()

        Dim directoryContext = New DirectoryContext(DirectoryContextType.Domain, Environment.MachineName)

        Using domain__1 = Domain.GetDomain(directoryContext)
            Using directoryEntry = domain__1.GetDirectoryEntry()
                domainSid = DirectCast(directoryEntry.Properties("objectSid").Value, Byte())
            End Using
        End Using

        Dim sid = New SecurityIdentifier(domainSid, 0)

        Return sid.ToString
        'Dim validUser As Boolean = UserPrincipal.Current.Sid.IsEqualDomainSid(sid)
    End Function
End Class
