﻿Public Class Form1
    Public ReadOnly c_FlashInstallerPattern As String = "install_flash_player"
    Public ReadOnly c_FlashInstallerDir As String = "C:\__temp\Flash_test\files"
    Public ReadOnly c_FlashInstallerExt As String = ".jpg"


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim dZero As New Date(0)
        Dim dDate As Date = New Date(2014, 1, 1)
        Dim dNow As Date = Date.Now

        Dim dDiff1 As Long = DateDiff(DateInterval.Second, dZero, dDate)
        Dim dDiff2 As Long = DateDiff(DateInterval.Second, dZero, dNow)

        MessageBox.Show(dZero.ToString("yyyy-MM-dd HH:mm:ss"))
        MessageBox.Show(dNow.ToString("yyyy-MM-dd HH:mm:ss"))

        If dZero < dNow Then
            MessageBox.Show("<")
        End If
        If dZero > dNow Then
            MessageBox.Show("<")
        End If

        MessageBox.Show(DateDiff(DateInterval.Second, dZero, dNow))
    End Sub

    Private Sub FindFlashInstaller()
        Dim bFound As Boolean = False, sFlash As String = ""
        Dim currentFile As String = "", currentDiff As Long = 0
        Dim dZero As New Date(0)


        MessageBox.Show("Searching Flash")

        Dim dirFlash As New IO.DirectoryInfo(c_FlashInstallerDir)
        Dim filesFlash As IO.FileInfo() = dirFlash.GetFiles()

        For Each fileFlash As IO.FileInfo In filesFlash
            If fileFlash.Name.StartsWith(c_FlashInstallerPattern) Then
                If fileFlash.Name.EndsWith(c_FlashInstallerExt) Then
                    MessageBox.Show("found:" & fileFlash.Name)

                    If DateDiff(DateInterval.Second, dZero, fileFlash.LastWriteTime) > currentDiff Then
                        If currentFile <> "" Then
                            MessageBox.Show("it's newer than '" & currentFile & "', so using this one")
                        End If

                        currentDiff = DateDiff(DateInterval.Second, dZero, fileFlash.LastWriteTime)
                        currentFile = fileFlash.Name
                    End If

                    bFound = True
                End If
            End If
        Next

        If Not bFound Then
            MessageBox.Show("did not find any flash packages")
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        FindFlashInstaller()
    End Sub
End Class
