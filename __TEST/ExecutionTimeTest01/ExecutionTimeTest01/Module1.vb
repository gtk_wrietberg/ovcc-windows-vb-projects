Imports System.Threading

Module Module1

    Sub Main()
        Dim startTime As DateTime = DateTime.Now
        Dim executionTime As TimeSpan = DateTime.Now - startTime

        Thread.Sleep(2000)

        executionTime = DateTime.Now - startTime
        Console.WriteLine("Ticks: {0} ; t/ms: {1} ; ms: {2}", executionTime.Ticks, TimeSpan.TicksPerMillisecond, (executionTime.Ticks / TimeSpan.TicksPerMillisecond))

        Thread.Sleep(1000)

        executionTime = DateTime.Now - startTime
        Console.WriteLine("Ticks: {0} ; t/ms: {1} ; ms: {2}", executionTime.Ticks, TimeSpan.TicksPerMillisecond, (executionTime.Ticks / TimeSpan.TicksPerMillisecond))


        MsgBox("done")
    End Sub

End Module
