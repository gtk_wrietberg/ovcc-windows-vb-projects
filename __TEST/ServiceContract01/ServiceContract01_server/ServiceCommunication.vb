﻿Imports System.ServiceModel

Public Class ServiceCommunication
    Public Class Constants
        Public Class Pipe
            Public Shared URI As String = "net.pipe://localhost"
            Public Shared Endpoint As String = "OVCCPipe"
        End Class
    End Class

    Interface IMyContractCallback
        <OperationContract>
        Sub OnCallback()
    End Interface

    <ServiceContract(CallbackContract:=GetType(IMyContractCallback))>
    Interface IMyContract
        <OperationContract(IsOneWay:=True)>
        Sub DoSomething()
    End Interface

    <ServiceBehavior(ConcurrencyMode:=ConcurrencyMode.Multiple, UseSynchronizationContext:=False)>
    Public Class MyService
        Implements IMyContract

        Public Sub DoSomething() Implements IMyContract.DoSomething
            Console.WriteLine("Hi from server!")
            Dim callback = OperationContext.Current.GetCallbackChannel(Of IMyContractCallback)()
            callback.OnCallback()
        End Sub
    End Class

    Public Class MyContractClient
        Inherits DuplexClientBase(Of IMyContract)

        Public Sub New(ByVal callbackInstance As Object, ByVal binding As Channels.Binding, ByVal remoteAddress As EndpointAddress)
            MyBase.New(callbackInstance, binding, remoteAddress)
        End Sub
    End Class

    <CallbackBehavior(ConcurrencyMode:=ConcurrencyMode.Multiple, UseSynchronizationContext:=False)>
    Public Class MyCallbackClient
        Implements IMyContractCallback

        Public Sub OnCallback() Implements IMyContractCallback.OnCallback
            Console.WriteLine("Hi from client!")
        End Sub
    End Class

End Class
