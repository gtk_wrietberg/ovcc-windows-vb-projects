﻿Imports System.ServiceModel

Public Class Form1
    Private host As ServiceHost

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim uri = New Uri("net.tcp://localhost")
        Dim binding = New NetTcpBinding()

        host = New ServiceHost(GetType(ServiceCommunication.MyService), New Uri() {New Uri(ServiceCommunication.Constants.Pipe.URI)})
        host.AddServiceEndpoint(GetType(ServiceCommunication.IMyContract), New NetNamedPipeBinding(), ServiceCommunication.Constants.Pipe.Endpoint)
        host.Open()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim s As String = ""

        Try
            s = host.State.ToString
        Catch ex As Exception
            s = ex.Message
        End Try

        TextBox1.Text = "host: " & s
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            host.Close()

        Catch ex As Exception

        End Try
    End Sub
End Class
