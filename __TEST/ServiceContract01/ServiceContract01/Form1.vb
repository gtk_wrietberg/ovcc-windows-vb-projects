﻿Imports System.ServiceModel

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim callback = New ServiceCommunication.MyCallbackClient()
        Dim client = New ServiceCommunication.MyContractClient(callback, New NetNamedPipeBinding(), New EndpointAddress(ServiceCommunication.Constants.Pipe.URI & "/" & ServiceCommunication.Constants.Pipe.Endpoint))
        Dim proxy = client.ChannelFactory.CreateChannel()
        proxy.DoSomething()
        client.Close()
    End Sub
End Class
