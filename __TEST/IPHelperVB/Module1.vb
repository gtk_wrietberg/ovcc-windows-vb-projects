﻿Imports System.Runtime.InteropServices

Module Module1

    Sub Main()

        Dim ipTable As iphelper.CMIB_IPNETTABLE = iphelper.iphelper.GetIpNetTable()
        For i As Integer = 0 To ipTable.table.Length - 1
            Dim ipRow As iphelper.CMIB_IPNETROW = ipTable.table(i)
            Console.WriteLine("Index: {0}" & vbTab & "Address: {1}", ipRow.dwIndex, ipRow.get_ip())
        Next
        Console.WriteLine("---------------------------------------------------")

        Dim tcpTable As iphelper.CMIB_TCPEXTABLE = iphelper.iphelper.GetTcpExTable()
        For i As Integer = 0 To tcpTable.table.Length - 1
            Dim tcpRow As iphelper.CMIB_TCPEXROW = tcpTable.table(i)
            Console.WriteLine("Process: {0}" & vbTab & "Local: {1}:{2}" & vbTab & "Remote: {3}:{4}" & vbTab & "State: {5}", tcpRow.dwProcessId, tcpRow.get_localip(), tcpRow.get_local_port(), tcpRow.get_remoteip(), tcpRow.get_remote_port(), _
            tcpRow.get_state())
        Next
        Console.WriteLine("---------------------------------------------------")

        Dim udpTable As iphelper.CMIB_UDPEXTABLE = iphelper.iphelper.GetUdpExTable()
        For i As Integer = 0 To udpTable.table.Length - 1
            Dim udpRow As iphelper.CMIB_UDPEXROW = udpTable.table(i)
            Console.WriteLine("Process: {0}" & vbTab & "Local: {1}:{2}", udpRow.dwProcessId, udpRow.get_ip(), udpRow.get_port())
        Next

        Console.Read()
    End Sub

End Module

Namespace iphelper
    Class iphelper_Process
#Region "GetProcessById"
        Public Shared Function GetProcessById(ByVal processID As UInt32) As System.Diagnostics.Process
            Try
                Return System.Diagnostics.Process.GetProcessById(CInt(processID))
            Catch
                Return Nothing
            End Try
        End Function
#End Region
    End Class
    Class iphelper
        Public Const StatisticsEx_dwFamily_AF_INET As UInt32 = 2
        ' UInt32ernetwork: UDP, TCP, etc. 
        Public Const StatisticsEx_dwFamily_AF_INET6 As UInt32 = 23
        ' UInt32ernetwork Version 6 
#Region "GetProcessHeap"
        <DllImport("kernel32", SetLastError:=True)> _
        Private Shared Function GetProcessHeap() As IntPtr
        End Function
#End Region

#Region "GetIpNetTable"
        'CMIB_IPNETTABLE pIpNetTable, 
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetIpNetTable(ByVal pIpNetTable As Byte(), ByRef pdwSize As UInt32, ByVal bOrder As Boolean) As UInt32
        End Function

        Public Shared Function GetIpNetTable() As CMIB_IPNETTABLE
            Dim error_code As UInt32 = 0
            Return iphelper.GetIpNetTable(True, error_code)
        End Function
        Public Shared Function GetIpNetTable(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As CMIB_IPNETTABLE
            Dim size As UInt32 = CMIB_IPNETROW.size * 16 + 4
            ' allow memory for 16 ipnetrow (+4 for dwNumEntries) 
            Dim buffer As Byte() = New Byte(size - 1) {}
            error_code = GetIpNetTable(buffer, size, True)
            If error_code = 122 Then
                'not enougth memory 
                buffer = New Byte(size - 1) {}
                error_code = GetIpNetTable(buffer, size, True)
            End If
            If error_code = 0 Then
                ' no error 
                Dim ipnettable As New CMIB_IPNETTABLE()
                ipnettable.decode(buffer)
                Return ipnettable
            End If
            ' else 
            If b_verbose Then
                'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                ' System.Windows.Forms.MessageBoxButtons.OK, 
                ' System.Windows.Forms.MessageBoxIcon.Error); 
            End If
            Return Nothing
        End Function
#End Region

#Region "GetIpStatisticsEx"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetIpStatisticsEx(ByRef pmips As MIB_IPSTATS, ByVal dwFamily As UInt32) As UInt32
        End Function

        Public Shared Function GetIpStatisticsEx() As MIB_IPSTATS
            Dim error_code As UInt32 = 0
            Return iphelper.GetIpStatisticsEx(iphelper.StatisticsEx_dwFamily_AF_INET, True, error_code)
        End Function
        Public Shared Function GetIpStatisticsEx(ByVal family_type As UInt32, ByVal b_verbose As Boolean, ByRef error_code As UInt32) As MIB_IPSTATS
            Dim mips As New MIB_IPSTATS()
            error_code = GetIpStatisticsEx(mips, family_type)
            If (error_code <> 0) AndAlso b_verbose Then
                ' error 
                'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                ' System.Windows.Forms.MessageBoxButtons.OK, 
                ' System.Windows.Forms.MessageBoxIcon.Error); 
            End If
            Return mips
        End Function
#End Region

#Region "GetIpStatistics"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetIpStatistics(ByRef pmips As MIB_IPSTATS) As UInt32
        End Function

        Public Shared Function GetIpStatistics() As MIB_IPSTATS
            Dim error_code As UInt32 = 0
            Return iphelper.GetIpStatistics(True, error_code)
        End Function
        Public Shared Function GetIpStatistics(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As MIB_IPSTATS
            Dim mips As New MIB_IPSTATS()
            error_code = GetIpStatistics(mips)
            If (error_code <> 0) AndAlso b_verbose Then
                ' error 
                'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                ' System.Windows.Forms.MessageBoxButtons.OK, 
                ' System.Windows.Forms.MessageBoxIcon.Error); 
            End If
            Return mips
        End Function
#End Region

#Region "GetIcmpStatistics"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetIcmpStatistics(ByRef pmi As MIB_ICMP) As UInt32
        End Function
        Public Shared Function GetIcmpStatistics() As MIB_ICMP
            Dim error_code As UInt32 = 0
            Return iphelper.GetIcmpStatistics(True, error_code)
        End Function
        Public Shared Function GetIcmpStatistics(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As MIB_ICMP
            Dim mi As New MIB_ICMP()
            error_code = GetIcmpStatistics(mi)
            If (error_code <> 0) AndAlso b_verbose Then
                ' error 
                'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                ' System.Windows.Forms.MessageBoxButtons.OK, 
                ' System.Windows.Forms.MessageBoxIcon.Error); 
            End If
            Return mi
        End Function
#End Region

#Region "GetUdpStatisticsEx"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetUdpStatisticsEx(ByRef pStats As MIB_UDPSTATS, ByVal dwFamily As UInt32) As UInt32
        End Function
        Public Shared Function GetUdpStatisticsEx() As MIB_UDPSTATS
            Dim error_code As UInt32 = 0
            Return iphelper.GetUdpStatisticsEx(iphelper.StatisticsEx_dwFamily_AF_INET, True, error_code)
        End Function
        Public Shared Function GetUdpStatisticsEx(ByVal dwFamily As UInt32, ByVal b_verbose As Boolean, ByRef error_code As UInt32) As MIB_UDPSTATS
            Dim m As New MIB_UDPSTATS()
            error_code = GetUdpStatisticsEx(m, dwFamily)
            If (error_code <> 0) AndAlso b_verbose Then
                ' error 
                'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                ' System.Windows.Forms.MessageBoxButtons.OK, 
                ' System.Windows.Forms.MessageBoxIcon.Error); 
            End If
            Return m
        End Function
#End Region

#Region "GetUdpStatistics"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetUdpStatistics(ByRef pStats As MIB_UDPSTATS) As UInt32
        End Function
        Public Shared Function GetUdpStatistics() As MIB_UDPSTATS
            Dim error_code As UInt32 = 0
            Return iphelper.GetUdpStatistics(True, error_code)
        End Function
        Public Shared Function GetUdpStatistics(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As MIB_UDPSTATS
            Dim m As New MIB_UDPSTATS()
            error_code = GetUdpStatistics(m)
            If (error_code <> 0) AndAlso b_verbose Then
                ' error 
                'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                ' System.Windows.Forms.MessageBoxButtons.OK, 
                ' System.Windows.Forms.MessageBoxIcon.Error); 
            End If
            Return m
        End Function
#End Region

#Region "GetUdpTable"
        'PCMIB_UDPTABLE pUdpTable, 
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetUdpTable(ByVal pUdpTable As Byte(), ByRef pdwSize As UInt32, ByVal bOrder As Boolean) As UInt32
        End Function
        Public Shared Function GetUdpTable() As CMIB_UDPTABLE
            Dim error_code As UInt32 = 0
            Return iphelper.GetUdpTable(True, error_code)
        End Function
        Public Shared Function GetUdpTable(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As CMIB_UDPTABLE
            Dim size As UInt32 = CMIB_UDPROW.size * 100 + 4
            ' allow memory for 100 udprow (+4 for dwNumEntries) 
            Dim buffer As Byte() = New Byte(size - 1) {}
            error_code = GetUdpTable(buffer, size, True)
            If error_code = 122 Then
                'not enougth memory 
                buffer = New Byte(size - 1) {}
                error_code = GetUdpTable(buffer, size, True)
            End If
            If error_code = 0 Then
                ' no error 
                Dim udptable As New CMIB_UDPTABLE()
                udptable.decode(buffer)
                Return udptable
            End If
            ' else 
            If b_verbose Then
                'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                ' System.Windows.Forms.MessageBoxButtons.OK, 
                ' System.Windows.Forms.MessageBoxIcon.Error); 
            End If
            Return Nothing
        End Function
#End Region

#Region "GetUdpExTable"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function AllocateAndGetUdpExTableFromStack(ByRef pUdpTable As IntPtr, ByVal bOrder As Boolean, ByVal heap As IntPtr, ByVal zero As UInt32, ByVal flags As UInt32) As UInt32
        End Function
        Public Shared Function GetUdpExTable() As CMIB_UDPEXTABLE
            Dim error_code As UInt32 = 0
            Return iphelper.GetUdpExTable(True, error_code)
        End Function
        Public Shared Function GetUdpExTable(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As CMIB_UDPEXTABLE
            ' allocate a dump memory space in order to retrieve nb of connexion 
            Dim BufferSize As Integer = 100 * CMIB_UDPEXROW.size_ex + 4
            'NumEntries*CMIB_UDPEXROW.size_ex+4 
            Dim lpTable As IntPtr = Marshal.AllocHGlobal(BufferSize)
            'getting infos 
            error_code = AllocateAndGetUdpExTableFromStack(lpTable, True, GetProcessHeap(), 0, 2)
            If error_code <> 0 Then
                If b_verbose Then
                    'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                    'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                    ' System.Windows.Forms.MessageBoxButtons.OK, 
                    ' System.Windows.Forms.MessageBoxIcon.Error); 
                End If
                Return Nothing
            End If
            'get the number of entries in the table 
            Dim NumEntries As Integer = CInt(Marshal.ReadIntPtr(lpTable))
            Dim real_buffer_size As Integer = NumEntries * CMIB_UDPEXROW.size_ex + 4
            ' check if memory was enougth (needed buffer size: NumEntries*MIB_UDPEXROW.size_ex +4 (for dwNumEntries)) 
            If BufferSize < real_buffer_size Then
                ' if it wasn't the case call function another time 
                ' free the buffer 
                Marshal.FreeHGlobal(lpTable)
                ' get the needed buffer size: NumEntries*MIB_UDPEXROW.size_ex +4 (for dwNumEntries) 
                BufferSize = real_buffer_size
                ' Allocate memory 
                lpTable = Marshal.AllocHGlobal(BufferSize)
                error_code = AllocateAndGetUdpExTableFromStack(lpTable, True, GetProcessHeap(), 0, 2)
                If error_code <> 0 Then
                    If b_verbose Then
                        'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                        'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                        ' System.Windows.Forms.MessageBoxButtons.OK, 
                        ' System.Windows.Forms.MessageBoxIcon.Error); 
                    End If
                    Return Nothing
                End If
            End If
            BufferSize = real_buffer_size
            Dim array As Byte() = New Byte(BufferSize - 1) {}
            Marshal.Copy(lpTable, array, 0, BufferSize)
            ' free the buffer 
            Marshal.FreeHGlobal(lpTable)

            Dim muet As New CMIB_UDPEXTABLE()
            muet.decode(array)
            Return muet
        End Function

#End Region

#Region "GetTcpStatisticsEx"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetTcpStatisticsEx(ByRef pStats As MIB_TCPSTATS, ByVal dwFamily As UInt32) As UInt32
        End Function
        Public Shared Function GetTcpStatisticsEx() As MIB_TCPSTATS
            Dim error_code As UInt32 = 0
            Return iphelper.GetTcpStatisticsEx(iphelper.StatisticsEx_dwFamily_AF_INET, True, error_code)
        End Function
        Public Shared Function GetTcpStatisticsEx(ByVal dwFamily As UInt32, ByVal b_verbose As Boolean, ByRef error_code As UInt32) As MIB_TCPSTATS
            Dim m As New MIB_TCPSTATS()
            error_code = GetTcpStatisticsEx(m, dwFamily)
            If error_code <> 0 Then
                If b_verbose Then
                    'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                    'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                    ' System.Windows.Forms.MessageBoxButtons.OK, 
                    ' System.Windows.Forms.MessageBoxIcon.Error); 
                End If
            End If
            Return m
        End Function
#End Region

#Region "GetTcpStatistics"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetTcpStatistics(ByRef pStats As MIB_TCPSTATS) As UInt32
        End Function
        Public Shared Function GetTcpStatistics() As MIB_TCPSTATS
            Dim error_code As UInt32 = 0
            Return iphelper.GetTcpStatistics(True, error_code)
        End Function
        Public Shared Function GetTcpStatistics(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As MIB_TCPSTATS
            Dim m As New MIB_TCPSTATS()
            error_code = GetTcpStatistics(m)
            If error_code <> 0 Then
                If b_verbose Then
                    'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                    'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                    ' System.Windows.Forms.MessageBoxButtons.OK, 
                    ' System.Windows.Forms.MessageBoxIcon.Error); 
                End If
            End If
            Return m
        End Function
#End Region

#Region "GetTcpTable"
        'MIB_TCPTABLE pTcpTable, 
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function GetTcpTable(ByVal pTcpTable As Byte(), ByRef pdwSize As UInt32, ByVal bOrder As Boolean) As UInt32
        End Function
        Public Shared Function GetTcpTable() As CMIB_TCPTABLE
            Dim error_code As UInt32 = 0
            Return iphelper.GetTcpTable(True, error_code)
        End Function
        Public Shared Function GetTcpTable(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As CMIB_TCPTABLE
            Dim size As UInt32 = CMIB_TCPROW.size * 300 + 4
            ' allow memory for 300 tcprow (+4 for dwNumEntries) 
            Dim buffer As Byte() = New Byte(size - 1) {}
            error_code = GetTcpTable(buffer, size, True)
            If error_code = 122 Then
                'not enougth memory 
                buffer = New Byte(size - 1) {}
                error_code = GetTcpTable(buffer, size, True)
            End If
            If error_code = 0 Then
                ' no error 
                Dim tcptable As New CMIB_TCPTABLE()
                tcptable.decode(buffer)
                Return tcptable
            End If
            ' else 
            If b_verbose Then
                'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                ' System.Windows.Forms.MessageBoxButtons.OK, 
                ' System.Windows.Forms.MessageBoxIcon.Error); 
            End If
            Return Nothing
        End Function
#End Region

#Region "GetTcpExTable"
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function AllocateAndGetTcpExTableFromStack(ByRef pTcpTable As IntPtr, ByVal bOrder As Boolean, ByVal heap As IntPtr, ByVal zero As UInt32, ByVal flags As UInt32) As UInt32
        End Function
        Public Shared Function GetTcpExTable() As CMIB_TCPEXTABLE
            Dim error_code As UInt32 = 0
            Return iphelper.GetTcpExTable(True, error_code)
        End Function
        Public Shared Function GetTcpExTable(ByVal b_verbose As Boolean, ByRef error_code As UInt32) As CMIB_TCPEXTABLE
            ' allocate a dump memory space in order to retrieve nb of connexion 
            Dim BufferSize As Integer = 300 * CMIB_TCPEXROW.size_ex + 4
            'NumEntries*CMIB_TCPEXROW.size_ex+4 
            Dim lpTable As IntPtr = Marshal.AllocHGlobal(BufferSize)
            'getting infos 
            error_code = AllocateAndGetTcpExTableFromStack(lpTable, True, GetProcessHeap(), 0, 2)
            If error_code <> 0 Then
                If b_verbose Then
                    'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                    'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                    ' System.Windows.Forms.MessageBoxButtons.OK, 
                    ' System.Windows.Forms.MessageBoxIcon.Error); 
                End If
                Return Nothing
            End If
            'get the number of entries in the table 
            Dim NumEntries As Integer = CInt(Marshal.ReadIntPtr(lpTable))
            Dim real_buffer_size As Integer = NumEntries * CMIB_TCPEXROW.size_ex + 4
            ' check if memory was enougth (needed buffer size: NumEntries*MIB_UDPEXROW.size_ex +4 (for dwNumEntries)) 
            If BufferSize < real_buffer_size Then
                ' free the buffer 
                Marshal.FreeHGlobal(lpTable)

                ' get the needed buffer size: NumEntries*MIB_TCPEXROW.size_ex +4 (for dwNumEntries) 
                BufferSize = real_buffer_size
                ' Allocate memory 
                lpTable = Marshal.AllocHGlobal(BufferSize)

                error_code = AllocateAndGetTcpExTableFromStack(lpTable, True, GetProcessHeap(), 0, 2)
                If error_code <> 0 Then
                    If b_verbose Then
                        'string str_msg=Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                        'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                        ' System.Windows.Forms.MessageBoxButtons.OK, 
                        ' System.Windows.Forms.MessageBoxIcon.Error); 
                    End If
                    Return Nothing
                End If
            End If
            BufferSize = real_buffer_size
            Dim array As Byte() = New Byte(BufferSize - 1) {}
            Marshal.Copy(lpTable, array, 0, BufferSize)
            ' free the buffer 
            Marshal.FreeHGlobal(lpTable)
            Dim mtet As New CMIB_TCPEXTABLE()
            mtet.decode(array)
            Return mtet
        End Function
#End Region

        'MSDN:"Currently, the only state to which a TCP connection can be set is MIB_TCP_STATE_DELETE_TCB" 
        'return 65 - User has no sufficient privilege to execute this API successfully 
        'return 87 - Specified port is not in state to be closed down. 
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Public Shared Function SetTcpEntry(ByRef pTcpRow As MIB_TCPROW) As UInt32
        End Function

#Region "sendarp"
        ' destination IP address 
        ' IP address of sender 
        ' returned physical address 
        <DllImport("Iphlpapi.dll", SetLastError:=True)> _
        Private Shared Function SendARP(ByVal DestIP As UInt32, ByVal SrcIP As UInt32, ByRef pMacAddr As UInt32, ByRef PhyAddrLen As UInt32) As UInt32
        End Function
        ' length of returned physical addr. 

        Public Shared Function SendArp(ByVal ip As String) As String
            Dim error_code As UInt32 = 0
            Return iphelper.SendARP(ip, True, error_code)
        End Function
        Public Shared Function SendArp(ByVal ip As String, ByVal b_verbose As Boolean, ByRef error_code As UInt32) As String
            ' Windows NT/2000/XP: Included in Windows 2000; Windows XP Pro; and Windows .NET Server. 
            ' Windows 95/98/Me: Unsupported. 
            Try

                Dim DestIP As UInt32
                ' destination IP address 
                Dim SrcIP As UInt32
                ' IP address of sender 
                Dim pMacAddr As UInt32() = New UInt32(1) {}
                ' returned physical address 
                Dim PhyAddrLen As UInt32
                ' length of returned physical addr. 

                DestIP = System.BitConverter.ToUInt32(System.Net.IPAddress.Parse(ip).GetAddressBytes(), 0)
                SrcIP = 0
                pMacAddr(0) = 255
                pMacAddr(1) = 255
                PhyAddrLen = 6
                error_code = SendArp(DestIP, SrcIP, pMacAddr(0), PhyAddrLen)
                If error_code <> 0 Then
                    If b_verbose Then
                        'string str_msg="Error retrieving MAC address of "+ip;//+"\r\n"+Tools.API.API_error.GetAPIErrorMessageDescription(error_code); 
                        'System.Windows.Forms.MessageBox.Show( str_msg,"Error", 
                        ' System.Windows.Forms.MessageBoxButtons.OK, 
                        ' System.Windows.Forms.MessageBoxIcon.Error); 
                    End If
                    Return ""
                End If

                Dim bMacAddr As Byte() = New Byte(5) {}
                bMacAddr(3) = CByte(((pMacAddr(0) >> 24) And &HFF))
                bMacAddr(2) = CByte(((pMacAddr(0) >> 16) And &HFF))
                bMacAddr(1) = CByte(((pMacAddr(0) >> 8) And &HFF))
                bMacAddr(0) = CByte(((pMacAddr(0)) And &HFF))
                bMacAddr(5) = CByte(((pMacAddr(1) >> 8) And &HFF))
                bMacAddr(4) = CByte(((pMacAddr(1)) And &HFF))

                Dim str_mac_addr As String = System.BitConverter.ToString(bMacAddr)
                Return str_mac_addr
            Catch ex As Exception
                If b_verbose Then
                    'System.Windows.Forms.MessageBox.Show( ex.Message,"Error", 
                    ' System.Windows.Forms.MessageBoxButtons.OK, 
                    ' System.Windows.Forms.MessageBoxIcon.Error); 
                End If
                Return ""
            End Try
        End Function
#End Region

        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Public Shared Function CreateIpNetEntry(ByRef pArpEntry As MIB_IPNETROW) As UInt32
        End Function
        ' pointer to info for new entry 
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Public Shared Function DeleteIpNetEntry(ByRef pArpEntry As MIB_IPNETROW) As UInt32
        End Function
        ' info identifying entry to delete 
        <DllImport("iphlpapi.dll", SetLastError:=True)> _
        Public Shared Function SetIpNetEntry(ByRef pArpEntry As MIB_IPNETROW) As UInt32
        End Function
        ' pointer to new information 
    End Class
#Region "GetIpNetTable class/structs"
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure MIB_IPNETROW
        'Specifies the index of the adapter. 
        Public dwIndex As UInt32
        'Specifies the length of the physical address. 
        Public dwPhysAddrLen As UInt32
        'Specifies the physical address. 
        'MAXLEN_PHYSADDR 
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public bPhysAddr As Byte()
        'Specifies the IP address in networkhost order 
        Public dwAddr As UInt32
        'Specifies the type of ARP entry. This type can be one of the following values. Value Meaning 4 Static; 3 Dynamic; 2 Invalid; 1 Other 
        Public dwType As UInt32
    End Structure
    Public Class CMIB_IPNETROW
        Public Const dwType_Static As Byte = 4
        Public Const dwType_Dynamic As Byte = 3
        Public Const dwType_Invalid As Byte = 2
        Public Const dwType_Other As Byte = 1

        Public Const MAXLEN_PHYSADDR As Byte = 8
        Public Const size As Byte = MAXLEN_PHYSADDR + 16
        'Specifies the index of the adapter. 
        Public dwIndex As UInt32 = 0
        'Specifies the length of the physical address. 
        Public dwPhysAddrLen As UInt32 = 0
        'Specifies the physical address. 
        Public bPhysAddr As Byte() = New Byte(MAXLEN_PHYSADDR - 1) {}
        'Specifies the IP address in networkhost order 
        Public dwAddr As UInt32 = 0
        'Specifies the type of ARP entry. This type can be one of the following values. Value Meaning 4 Static; 3 Dynamic; 2 Invalid; 1 Other 
        Public dwType As UInt32 = 0

        Public Function decode(ByVal array As Byte()) As Boolean
            If array Is Nothing Then
                Return False
            End If
            If array.Length < 24 Then
                Return False
            End If
            Me.dwIndex = System.BitConverter.ToUInt32(array, 0)
            Me.dwPhysAddrLen = System.BitConverter.ToUInt32(array, 4)
            System.Array.Copy(array, 8, Me.bPhysAddr, 0, MAXLEN_PHYSADDR)
            Me.dwAddr = System.BitConverter.ToUInt32(array, 8 + MAXLEN_PHYSADDR)
            Me.dwType = System.BitConverter.ToUInt32(array, 8 + MAXLEN_PHYSADDR + 4)
            Return True
        End Function
        ''' <summary> 
        ''' get ip in standard notation 
        ''' </summary> 
        ''' <returns></returns> 
        Public Function get_ip() As String
            Dim ipa As New System.Net.IPAddress(Me.dwAddr)
            Dim ret As String = ipa.ToString()
            Return ret
        End Function
        ''' <summary> 
        ''' get mac address of the host 
        ''' </summary> 
        ''' <returns></returns> 
        Public Function get_mac() As String
            Return System.BitConverter.ToString(Me.bPhysAddr, 0, CInt(Me.dwPhysAddrLen))
        End Function
        Public Function get_type() As String
            Dim ret As String = ""
            Select Case Me.dwType
                Case CMIB_IPNETROW.dwType_Dynamic
                    ret = "Dynamic"
                    Exit Select
                Case CMIB_IPNETROW.dwType_Invalid
                    ret = "Invalid"
                    Exit Select
                Case CMIB_IPNETROW.dwType_Other
                    ret = "Other"
                    Exit Select
                Case CMIB_IPNETROW.dwType_Static
                    ret = "Static"
                    Exit Select
            End Select
            Return ret
        End Function
        Public Function get_MIB_IPNETROW() As MIB_IPNETROW
            Dim ret As New MIB_IPNETROW()
            ret.bPhysAddr = New Byte(MAXLEN_PHYSADDR - 1) {}
            System.Array.Copy(Me.bPhysAddr, ret.bPhysAddr, MAXLEN_PHYSADDR)
            ret.dwAddr = Me.dwAddr
            ret.dwIndex = Me.dwIndex
            ret.dwPhysAddrLen = Me.dwPhysAddrLen
            ret.dwType = Me.dwType
            Return ret
        End Function
    End Class

    Public Class CMIB_IPNETTABLE
        Public dwNumEntries As UInt32 = 0
        Public table As CMIB_IPNETROW()
        'ANY_SIZE 
        Public Function decode(ByVal array As Byte()) As Boolean
            Dim pb_tmp As Byte() = New Byte(CMIB_IPNETROW.size - 1) {}
            If array Is Nothing Then
                Return False
            End If
            If array.Length < 4 Then
                Return False
            End If
            Me.dwNumEntries = System.BitConverter.ToUInt32(array, 0)
            If array.Length < Me.dwNumEntries Then
                Return False
            End If
            Me.table = New CMIB_IPNETROW(Me.dwNumEntries - 1) {}
            For cpt As UInt32 = 0 To Me.dwNumEntries - 1
                System.Array.Copy(array, CInt((cpt * CMIB_IPNETROW.size + 4)), pb_tmp, 0, CMIB_IPNETROW.size)
                Me.table(cpt) = New CMIB_IPNETROW()
                If Not Me.table(cpt).decode(pb_tmp) Then
                    Return False
                End If
            Next
            Return True
        End Function
    End Class
#End Region

#Region "GetIpStatisticsEx struct"
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure MIB_IPSTATS
        'Specifies whether IP forwarding is enabled or disabled. 
        Public dwForwarding As UInt32
        'Specifies the default initial time to live (TTL) for datagrams originating on a particular computer. 
        Public dwDefaultTTL As UInt32
        'Specifies the number of datagrams received. 
        Public dwInReceives As UInt32
        'Specifies the number of datagrams received that have header errors. 
        Public dwInHdrErrors As UInt32
        'Specifies the number of datagrams received that have address errors. 
        Public dwInAddrErrors As UInt32
        'Specifies the number of datagrams forwarded. 
        Public dwForwDatagrams As UInt32
        'Specifies the number of datagrams received that have an unknown protocol. 
        Public dwInUnknownProtos As UInt32
        'Specifies the number of received datagrams discarded. 
        Public dwInDiscards As UInt32
        'Specifies the number of received datagrams delivered. 
        Public dwInDelivers As UInt32
        'Specifies the number of outgoing datagrams that IP is requested to transmit. This number does not include forwarded datagrams. 
        Public dwOutRequests As UInt32
        'Specifies the number of outgoing datagrams discarded. 
        Public dwRoutingDiscards As UInt32
        'Specifies the number of transmitted datagrams discarded. 
        Public dwOutDiscards As UInt32
        'Specifies the number of datagrams for which this computer did not have a route to the destination IP address. These datagrams were discarded. 
        Public dwOutNoRoutes As UInt32
        'Specifies the amount of time allowed for all pieces of a fragmented datagram to arrive. If all pieces do not arrive within this time, the datagram is discarded. 
        Public dwReasmTimeout As UInt32
        'Specifies the number of datagrams that require re-assembly. 
        Public dwReasmReqds As UInt32
        'Specifies the number of datagrams that were successfully reassembled. 
        Public dwReasmOks As UInt32
        'Specifies the number of datagrams that cannot be reassembled. 
        Public dwReasmFails As UInt32
        'Specifies the number of datagrams that were fragmented successfully. 
        Public dwFragOks As UInt32
        'Specifies the number of datagrams that have not been fragmented because the IP header specifies no fragmentation. These datagrams are discarded. 
        Public dwFragFails As UInt32
        'Specifies the number of fragments created. 
        Public dwFragCreates As UInt32
        'Specifies the number of interfaces. 
        Public dwNumIf As UInt32
        'Specifies the number of IP addresses associated with this computer. 
        Public dwNumAddr As UInt32
        'Specifies the number of routes in the IP routing table. 
        Public dwNumRoutes As UInt32
    End Structure
#End Region

#Region "GetIcmpStatistics struct"
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure MIBICMPSTATS
        Public dwMsgs As UInt32
        Public dwErrors As UInt32
        Public dwDestUnreachs As UInt32
        Public dwTimeExcds As UInt32
        Public dwParmProbs As UInt32
        Public dwSrcQuenchs As UInt32
        Public dwRedirects As UInt32
        Public dwEchos As UInt32
        Public dwEchoReps As UInt32
        Public dwTimestamps As UInt32
        Public dwTimestampReps As UInt32
        Public dwAddrMasks As UInt32
        Public dwAddrMaskReps As UInt32
    End Structure
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure MIBICMPINFO
        Public icmpInStats As MIBICMPSTATS
        Public icmpOutStats As MIBICMPSTATS
    End Structure
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure MIB_ICMP
        Public stats As MIBICMPINFO
    End Structure
#End Region

#Region "GetUdpStatistics(Ex) struct"
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure MIB_UDPSTATS
        Public dwInDatagrams As UInt32
        Public dwNoPorts As UInt32
        Public dwInErrors As UInt32
        Public dwOutDatagrams As UInt32
        Public dwNumAddrs As UInt32
    End Structure
#End Region

#Region "GetUdpTable class"
    Public Class CMIB_UDPTABLE
        Public dwNumEntries As UInt32 = 0
        Public table As CMIB_UDPROW()
        Public Function decode(ByVal array As Byte()) As Boolean
            Dim pb_tmp As Byte() = New Byte(CMIB_UDPROW.size - 1) {}
            If array Is Nothing Then
                Return False
            End If
            If array.Length < 4 Then
                Return False
            End If
            Me.dwNumEntries = System.BitConverter.ToUInt32(array, 0)
            If array.Length < Me.dwNumEntries Then
                Return False
            End If
            Me.table = New CMIB_UDPROW(Me.dwNumEntries - 1) {}
            For cpt As UInt32 = 0 To Me.dwNumEntries - 1
                System.Array.Copy(array, CInt((cpt * CMIB_UDPROW.size + 4)), pb_tmp, 0, CMIB_UDPROW.size)
                Me.table(cpt) = New CMIB_UDPROW()
                If Not Me.table(cpt).decode(pb_tmp) Then
                    Return False
                End If
            Next
            Return True
        End Function
    End Class

    Public Class CMIB_UDPROW
        Public Const size As Byte = 8
        ' in networkhost order 
        Public dwLocalAddr As UInt32 = 0
        ' in networkhost order 
        Public dwLocalPort As UInt32 = 0
        Public Function decode(ByVal array As Byte()) As Boolean
            If array Is Nothing Then
                Return False
            End If
            If array.Length < CMIB_UDPROW.size Then
                Return False
            End If
            Me.dwLocalAddr = System.BitConverter.ToUInt32(array, 0)
            Me.dwLocalPort = System.BitConverter.ToUInt32(array, 4)
            Return True
        End Function
        ''' <summary> 
        ''' get ip in standard notation 
        ''' </summary> 
        ''' <returns></returns> 
        Public Function get_ip() As String
            Dim ipa As New System.Net.IPAddress(Me.dwLocalAddr)
            Dim ret As String = ipa.ToString()
            Return ret
        End Function
        Public Function get_port() As UInt32
            Dim ret As UInt32 = ((Me.dwLocalPort >> 8) And &HFF) + ((Me.dwLocalPort And &HFF) << 8)
            Return ret
        End Function
    End Class
#End Region

#Region "GetUdpExTable class"
    Public Class CMIB_UDPEXTABLE
        Public dwNumEntries As UInt32 = 0
        Public table As CMIB_UDPEXROW()
        Public Function decode(ByVal array As Byte()) As Boolean
            Dim pb_tmp As Byte() = New Byte(CMIB_UDPEXROW.size_ex - 1) {}
            If array Is Nothing Then
                Return False
            End If
            If array.Length < 4 Then
                Return False
            End If
            Me.dwNumEntries = System.BitConverter.ToUInt32(array, 0)
            If array.Length < Me.dwNumEntries Then
                Return False
            End If
            Me.table = New CMIB_UDPEXROW(Me.dwNumEntries - 1) {}
            For cpt As UInt32 = 0 To Me.dwNumEntries - 1
                System.Array.Copy(array, CInt((cpt * CMIB_UDPEXROW.size_ex + 4)), pb_tmp, 0, CMIB_UDPEXROW.size_ex)
                Me.table(cpt) = New CMIB_UDPEXROW()
                If Not Me.table(cpt).decode_ex(pb_tmp) Then
                    Return False
                End If
            Next
            Return True
        End Function
    End Class

    Public Class CMIB_UDPEXROW
        Inherits CMIB_UDPROW
        Public Const size_ex As Byte = CMIB_UDPROW.size + 4
        Public dwProcessId As UInt32 = 0
        Public pProcess As System.Diagnostics.Process = Nothing
        Public Function decode_ex(ByVal array As Byte()) As Boolean
            If Not Me.decode(array) Then
                Return False
            End If
            ' array is not null (see decode()) 
            If array.Length < CMIB_UDPEXROW.size_ex Then
                Return False
            End If
            Me.dwProcessId = System.BitConverter.ToUInt32(array, CMIB_UDPROW.size)
            Me.pProcess = iphelper_Process.GetProcessById(Me.dwProcessId)
            Return True
        End Function
    End Class

#End Region

#Region "GetTcpStatistics(Ex) struct"
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure MIB_TCPSTATS
        Public dwRtoAlgorithm As UInt32
        Public dwRtoMin As UInt32
        Public dwRtoMax As UInt32
        Public dwMaxConn As UInt32
        Public dwActiveOpens As UInt32
        Public dwPassiveOpens As UInt32
        Public dwAttemptFails As UInt32
        Public dwEstabResets As UInt32
        Public dwCurrEstab As UInt32
        Public dwInSegs As UInt32
        Public dwOutSegs As UInt32
        Public dwRetransSegs As UInt32
        Public dwInErrs As UInt32
        Public dwOutRsts As UInt32
        Public dwNumConns As UInt32
    End Structure
#End Region

#Region "GetTcpTable"
    Public Class CMIB_TCPTABLE
        Public dwNumEntries As UInt32 = 0
        Public table As CMIB_TCPROW() = Nothing
        Public Function decode(ByVal array As Byte()) As Boolean
            Dim pb_tmp As Byte() = New Byte(CMIB_TCPROW.size - 1) {}
            If array Is Nothing Then
                Return False
            End If
            If array.Length < 4 Then
                Return False
            End If
            Me.dwNumEntries = System.BitConverter.ToUInt32(array, 0)
            If array.Length < Me.dwNumEntries Then
                Return False
            End If
            Me.table = New CMIB_TCPROW(Me.dwNumEntries - 1) {}
            For cpt As UInt32 = 0 To Me.dwNumEntries - 1
                System.Array.Copy(array, CInt((cpt * CMIB_TCPROW.size + 4)), pb_tmp, 0, CMIB_TCPROW.size)
                Me.table(cpt) = New CMIB_TCPROW()
                If Not Me.table(cpt).decode(pb_tmp) Then
                    Return False
                End If
            Next
            Return True
        End Function
    End Class
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure MIB_TCPROW
        Public dwState As UInt32
        Public dwLocalAddr As UInt32
        Public dwLocalPort As UInt32
        Public dwRemoteAddr As UInt32
        Public dwRemotePort As UInt32
    End Structure
    Public Class CMIB_TCPROW
        Public Const MIB_TCP_STATE_CLOSED As Byte = 1
        Public Const MIB_TCP_STATE_LISTEN As Byte = 2
        Public Const MIB_TCP_STATE_SYN_SENT As Byte = 3
        Public Const MIB_TCP_STATE_SYN_RCVD As Byte = 4
        Public Const MIB_TCP_STATE_ESTAB As Byte = 5
        Public Const MIB_TCP_STATE_FIN_WAIT1 As Byte = 6
        Public Const MIB_TCP_STATE_FIN_WAIT2 As Byte = 7
        Public Const MIB_TCP_STATE_CLOSE_WAIT As Byte = 8
        Public Const MIB_TCP_STATE_CLOSING As Byte = 9
        Public Const MIB_TCP_STATE_LAST_ACK As Byte = 10
        Public Const MIB_TCP_STATE_TIME_WAIT As Byte = 11
        Public Const MIB_TCP_STATE_DELETE_TCB As Byte = 12

        Public Const size As Byte = 5 * 4
        Public dwState As UInt32 = 0
        ' in networkhost order 
        Public dwLocalAddr As UInt32 = 0
        ' in networkhost order 
        Public dwLocalPort As UInt32 = 0
        ' in networkhost order 
        Public dwRemoteAddr As UInt32 = 0
        ' in networkhost order 
        Public dwRemotePort As UInt32 = 0
        Public Function decode(ByVal array As Byte()) As Boolean
            If array Is Nothing Then
                Return False
            End If
            If array.Length < CMIB_TCPROW.size Then
                Return False
            End If
            Me.dwState = System.BitConverter.ToUInt32(array, 0)
            Me.dwLocalAddr = System.BitConverter.ToUInt32(array, 4)
            Me.dwLocalPort = System.BitConverter.ToUInt32(array, 8)
            Me.dwRemoteAddr = System.BitConverter.ToUInt32(array, 12)
            If Me.dwState = CMIB_TCPROW.MIB_TCP_STATE_LISTEN Then
                Me.dwRemotePort = 0
            Else
                Me.dwRemotePort = System.BitConverter.ToUInt32(array, 16)
            End If
            Return True
        End Function
        Public Function get_localip() As String
            Dim ipa As New System.Net.IPAddress(Me.dwLocalAddr)
            Dim ret As String = ipa.ToString()
            Return ret
        End Function
        Public Function get_remoteip() As String
            Dim ipa As New System.Net.IPAddress(Me.dwRemoteAddr)
            Dim ret As String = ipa.ToString()
            Return ret
        End Function
        Public Function get_local_port() As UInt32
            Dim ret As UInt32 = ((Me.dwLocalPort >> 8) And &HFF) + ((Me.dwLocalPort And &HFF) << 8)
            Return ret
        End Function
        Public Function get_remote_port() As UInt32
            Dim ret As UInt32 = ((Me.dwRemotePort >> 8) And &HFF) + ((Me.dwRemotePort And &HFF) << 8)
            Return ret
        End Function
        Public Function get_state() As String
            Dim ret As String = "Unknown"
            Select Case Me.dwState
                Case MIB_TCP_STATE_CLOSED
                    ret = "CLOSED"
                    Exit Select
                Case MIB_TCP_STATE_LISTEN
                    ret = "LISTENING"
                    Exit Select
                Case MIB_TCP_STATE_SYN_SENT
                    ret = "SYN_SENT"
                    Exit Select
                Case MIB_TCP_STATE_SYN_RCVD
                    ret = "SYN_RCVD"
                    Exit Select
                Case MIB_TCP_STATE_ESTAB
                    ret = "ESTABLISHED"
                    Exit Select
                Case MIB_TCP_STATE_FIN_WAIT1
                    ret = "FIN_WAIT1"
                    Exit Select
                Case MIB_TCP_STATE_FIN_WAIT2
                    ret = "FIN_WAIT2"
                    Exit Select
                Case MIB_TCP_STATE_CLOSE_WAIT
                    ret = "CLOSE_WAIT"
                    Exit Select
                Case MIB_TCP_STATE_CLOSING
                    ret = "CLOSING"
                    Exit Select
                Case MIB_TCP_STATE_LAST_ACK
                    ret = "LAST_ACK"
                    Exit Select
                Case MIB_TCP_STATE_TIME_WAIT
                    ret = "TIME_WAIT"
                    Exit Select
                Case MIB_TCP_STATE_DELETE_TCB
                    ret = "DELETE_TCB"
                    Exit Select
            End Select
            Return ret
        End Function
        ''' <summary> 
        ''' used for SetTcpEntry 
        ''' </summary> 
        ''' <returns></returns> 
        Public Function get_MIB_TCPROW_struct() As MIB_TCPROW
            Dim ret As New MIB_TCPROW()
            ret.dwLocalAddr = Me.dwLocalAddr
            ret.dwLocalPort = Me.dwLocalPort
            ret.dwRemoteAddr = Me.dwRemoteAddr
            ret.dwRemotePort = Me.dwRemotePort
            ret.dwState = Me.dwState
            Return ret
        End Function
    End Class
#End Region

#Region "GetTcpExTable class"

    Public Class CMIB_TCPEXTABLE
        Public dwNumEntries As UInt32 = 0
        Public table As CMIB_TCPEXROW()
        Public Function decode(ByVal array As Byte()) As Boolean
            Dim pb_tmp As Byte() = New Byte(CMIB_TCPEXROW.size_ex - 1) {}
            If array Is Nothing Then
                Return False
            End If
            If array.Length < 4 Then
                Return False
            End If
            Me.dwNumEntries = System.BitConverter.ToUInt32(array, 0)
            If array.Length < Me.dwNumEntries Then
                Return False
            End If
            Me.table = New CMIB_TCPEXROW(Me.dwNumEntries - 1) {}
            For cpt As UInt32 = 0 To Me.dwNumEntries - 1
                System.Array.Copy(array, CInt((cpt * CMIB_TCPEXROW.size_ex + 4)), pb_tmp, 0, CMIB_TCPEXROW.size_ex)
                Me.table(cpt) = New CMIB_TCPEXROW()
                If Not Me.table(cpt).decode_ex(pb_tmp) Then
                    Return False
                End If
            Next
            Return True
        End Function
    End Class

    Public Class CMIB_TCPEXROW
        Inherits CMIB_TCPROW
        Public Const size_ex As Byte = CMIB_TCPROW.size + 4
        Public dwProcessId As UInt32 = 0
        Public pProcess As System.Diagnostics.Process = Nothing
        Public Function decode_ex(ByVal array As Byte()) As Boolean
            If Not Me.decode(array) Then
                Return False
            End If
            ' array is not null (see decode()) 
            If array.Length < CMIB_TCPEXROW.size_ex Then
                Return False
            End If
            Me.dwProcessId = System.BitConverter.ToUInt32(array, CMIB_TCPEXROW.size)
            Me.pProcess = iphelper_Process.GetProcessById(Me.dwProcessId)
            Return True
        End Function
    End Class
#End Region
End Namespace
