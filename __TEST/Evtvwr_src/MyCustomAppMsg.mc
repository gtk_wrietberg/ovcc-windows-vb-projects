;This file contains both categories and Events. Always category definitions should precede event defintions
; if they are in the same file

;//**********************Category Definitions***********************
MessageId=1
Facility=Application
Severity=Success
SymbolicName=CATA
Language=English
CategoryA
.

MessageId=2
Facility=Application
Severity=Success
SymbolicName=CATB
Language=English
CategoryB
.

MessageId=3
Facility=Application
Severity=Success
SymbolicName=CATC
Language=English
CategoryC
.

;//********************End of Category Definitions*******************


MessageIdTypedef=DWORD

;//***********************Event Definitions******************************


MessageId=1000
Severity=Success
SymbolicName=APP_GENERIC
Language=English
An Error Occured
.

; This event can hold any message string. Just pass the string as the parameter to the WriteEntry method.
; That string is substituted for %1

MessageId=2000
Severity=Error
SymbolicName=SOME_ERROR
Language=English
%1
.


