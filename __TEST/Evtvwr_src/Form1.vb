Public Class Form1
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboCategories As System.Windows.Forms.ComboBox
    Friend WithEvents cboEvents As System.Windows.Forms.ComboBox
    Friend WithEvents txtMsg As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cboEvents = New System.Windows.Forms.ComboBox()
        Me.cboCategories = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtMsg = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(54, 110)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(134, 28)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&Log"
        '
        'cboEvents
        '
        Me.cboEvents.Items.AddRange(New Object() {"1000", "2000"})
        Me.cboEvents.Location = New System.Drawing.Point(10, 34)
        Me.cboEvents.Name = "cboEvents"
        Me.cboEvents.Size = New System.Drawing.Size(84, 21)
        Me.cboEvents.TabIndex = 1
        '
        'cboCategories
        '
        Me.cboCategories.Items.AddRange(New Object() {"CategoryA", "CategoryB", "CategoryC"})
        Me.cboCategories.Location = New System.Drawing.Point(112, 34)
        Me.cboCategories.Name = "cboCategories"
        Me.cboCategories.Size = New System.Drawing.Size(130, 21)
        Me.cboCategories.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(112, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 23)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Category"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 18)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "EventID"
        '
        'txtMsg
        '
        Me.txtMsg.Location = New System.Drawing.Point(72, 70)
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.Size = New System.Drawing.Size(168, 20)
        Me.txtMsg.TabIndex = 5
        Me.txtMsg.Text = "Generic Message"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 20)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Err Msg"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(250, 169)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Label3, Me.txtMsg, Me.Label2, Me.Label1, Me.cboCategories, Me.cboEvents, Me.Button1})
        Me.Name = "Form1"
        Me.Text = "Sample Logger"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Function WriteToEventLog(ByVal Entry As String, _
  ByVal Source As String, _
  ByVal EventType As EventLogEntryType, _
  ByVal LogName As String, _
    ByVal EventID As Integer, _
  ByVal CategoryID As Short) As Boolean

        Dim objEventLog As New EventLog()

        Try
            'Register the  Event Source if not already done
            If Not objEventLog.SourceExists(Source) Then
                objEventLog.CreateEventSource(Source, LogName)
            End If

            objEventLog.Source = Source
            objEventLog.WriteEntry(Entry, EventType, EventID, CShort(CategoryID))

            Return True
        Catch Ex As Exception
            Return False
        End Try

    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim categoryID As Short
        Select Case cboCategories.Text
            Case "CategoryA"
                categoryID = 1
            Case "CategoryB"
                categoryID = 2
            Case "CategoryC"
                categoryID = 3
        End Select

        If WriteToEventLog(txtMsg.Text, "AppSource", EventLogEntryType.Error, "MyCustomApp", CInt(cboEvents.Text), categoryID) Then
            MessageBox.Show("Done")
        End If

    End Sub
End Class

