﻿Imports System.Text.RegularExpressions

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ' <string id="1100000">OneView Connection Centre helpdesk (gratis): 00800 3400 1111</string>

        Dim input As String = TextBox1.Text
        Dim pattern As String = "(.*)(:|：) (.+)"
        Dim replacement As String = "$3"
        Dim rgx As New Regex(pattern)
        Dim result As String = rgx.Replace(input, replacement)

        MsgBox(input & vbCrLf & "==>" & vbCrLf & result)
    End Sub
End Class
