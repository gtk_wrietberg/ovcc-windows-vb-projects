Imports System.Threading

Public Class Form1
    Private calcThread As Thread

    Private WithEvents oCalc As New Calc

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Button1.Enabled = False
        Button2.Enabled = True

        _SetResult("")

        Me.calcThread = New Thread(New ThreadStart(AddressOf Me._RunCalculations))
        Me.calcThread.Start()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Button1.Enabled = True
        Button2.Enabled = False

        Try
            Me.calcThread.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub _RunCalculations()
        oCalc.RunCalculations()
    End Sub



    Delegate Sub SetTextDelegate(ByVal message As String)

    Public Sub _SetDigit1(ByVal message As String)
        If TextBox1.InvokeRequired Then
            Dim oCall As New SetTextDelegate(AddressOf _SetDigit1)
            Me.Invoke(oCall, New Object() {message})
        Else
            TextBox1.Text = message
        End If
    End Sub
    Public Sub _SetDigit2(ByVal message As String)
        If TextBox2.InvokeRequired Then
            Dim oCall As New SetTextDelegate(AddressOf _SetDigit2)
            Me.Invoke(oCall, New Object() {message})
        Else
            TextBox2.Text = message
        End If
    End Sub
    Public Sub _SetDigit3(ByVal message As String)
        If TextBox3.InvokeRequired Then
            Dim oCall As New SetTextDelegate(AddressOf _SetDigit3)
            Me.Invoke(oCall, New Object() {message})
        Else
            TextBox3.Text = message
        End If
    End Sub
    Public Sub _SetDigit4(ByVal message As String)
        If TextBox4.InvokeRequired Then
            Dim oCall As New SetTextDelegate(AddressOf _SetDigit4)
            Me.Invoke(oCall, New Object() {message})
        Else
            TextBox4.Text = message
        End If
    End Sub

    Public Sub _SetResult(ByVal message As String)
        If TextBox5.InvokeRequired Then
            Dim oCall As New SetTextDelegate(AddressOf _SetResult)
            Me.Invoke(oCall, New Object() {message})
        Else
            TextBox5.Text = message
        End If
    End Sub

    Public Sub _AppendResult(ByVal message As String)
        If TextBox5.InvokeRequired Then
            Dim oCall As New SetTextDelegate(AddressOf _AppendResult)
            Me.Invoke(oCall, New Object() {message})
        Else
            TextBox5.AppendText(message)
        End If
    End Sub


    Private Sub oCalc_DigitsDone(ByVal d1 As Integer, ByVal d2 As Integer, ByVal d3 As Integer, ByVal d4 As Integer) Handles oCalc.DigitsDone
        If d1 < 0 Then
            _SetDigit1("")
        Else
            _SetDigit1(d1.ToString)
        End If

        _SetDigit2(d2.ToString)
        _SetDigit3(d3.ToString)
        _SetDigit4(d4.ToString)
    End Sub

    Private Sub oCalc_FoundOne(ByVal result As String) Handles oCalc.FoundMatch
        _AppendResult(result & " ; ")
    End Sub

    Private Sub oCalc_NextMatchLine() Handles oCalc.NextMatchLine
        _AppendResult(vbCrLf)
    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
