Public Class Calc
    Private mCalculationsDone As List(Of String)

    'Private operators() As String = {"+", "-", "*", "/", "^"}
    Private operators() As String = {"+", "-", "*", "/"}

    Private sc As New MSScriptControl.ScriptControl()

    Public Event DigitsDone(ByVal d1 As Integer, ByVal d2 As Integer, ByVal d3 As Integer, ByVal d4 As Integer)
    Public Event FoundMatch(ByVal result As String)
    Public Event NextMatchLine()

    Public Sub New()
        mCalculationsDone = New List(Of String)
    End Sub

    Public Sub RunCalculations()
        sc.Language = "VBScript"

        RunCalculations3()
        RunCalculations4()
    End Sub

    Private Sub RunCalculations3()
        Dim digit1 As Integer, digit2 As Integer, digit3 As Integer

        For digit1 = 0 To 9
            For digit2 = 0 To 5
                For digit3 = 0 To 9
                    _Calculate3(digit1, digit2, digit3)
                Next
            Next
        Next
    End Sub

    Private Sub RunCalculations4()
        Dim digit1 As Integer, digit2 As Integer, digit3 As Integer, digit4 As Integer

        For digit1 = 1 To 2
            For digit2 = 0 To 9
                If digit1 < 2 Or (digit1 = 2 And digit2 < 4) Then
                    For digit3 = 0 To 5
                        For digit4 = 0 To 9
                            _Calculate4(digit1, digit2, digit3, digit4)
                        Next
                    Next
                End If
            Next
        Next
    End Sub

    Private Sub _Calculate3(ByVal d1 As Integer, ByVal d2 As Integer, ByVal d3 As Integer)
        Dim expression As String = "", result As Decimal, bTrigger As Boolean = False

        For o As Integer = 0 To operators.Length - 1
            ' d1 <operator> d2 = d3
            expression = d1.ToString & operators(o) & d2.ToString

            Try
                result = sc.Eval(expression)

                If result = d3 Then
                    bTrigger = True
                    RaiseEvent FoundMatch(expression & "=" & d3.ToString)
                End If
            Catch ex As Exception

            End Try

            ' d1 = d2 <operator> d3
            expression = d2.ToString & operators(o) & d3.ToString

            Try
                result = sc.Eval(expression)

                If result = d1 Then
                    bTrigger = True
                    RaiseEvent FoundMatch(d1.ToString & "=" & expression)
                End If
            Catch ex As Exception

            End Try
        Next

        mCalculationsDone.Add(d1.ToString & ":" & d2.ToString & d3.ToString)

        RaiseEvent DigitsDone(-1, d1, d2, d3)
        If bTrigger Then RaiseEvent NextMatchLine()
    End Sub

    Private Sub _Calculate4(ByVal d1 As Integer, ByVal d2 As Integer, ByVal d3 As Integer, ByVal d4 As Integer)
        Dim expression As String = "", expression2 As String = "", result As Decimal, result2 As Decimal, bTrigger As Boolean = False

        For o1 As Integer = 0 To operators.Length - 1
            ' d1d2 <operator> d3 = d4
            expression = d1.ToString & d2.ToString & operators(o1) & d3.ToString

            Try
                result = sc.Eval(expression)

                If result = d4 Then
                    bTrigger = True
                    RaiseEvent FoundMatch(expression & "=" & d4.ToString)
                End If
            Catch ex As Exception

            End Try


            ' d1 <operator> d2d3 = d4
            expression = d1.ToString & operators(o1) & d2.ToString & d3.ToString

            Try
                result = sc.Eval(expression)

                If result = d4 Then
                    bTrigger = True
                    RaiseEvent FoundMatch(expression & "=" & d4.ToString)
                End If
            Catch ex As Exception

            End Try


            ' d1 = d2d3 <operator> d4
            expression = d2.ToString & d3.ToString & operators(o1) & d4.ToString

            Try
                result = sc.Eval(expression)

                If result = d1 Then
                    bTrigger = True
                    RaiseEvent FoundMatch(d1.ToString & "=" & expression)
                End If
            Catch ex As Exception

            End Try


            ' d1 = d2 <operator> d3d4
            expression = d2.ToString & operators(o1) & d3.ToString & d4.ToString

            Try
                result = sc.Eval(expression)

                If result = d1 Then
                    bTrigger = True
                    RaiseEvent FoundMatch(d1.ToString & "=" & expression)
                End If
            Catch ex As Exception

            End Try

            For o2 As Integer = 0 To operators.Length - 1
                ' d1 <operator1> d2 <operator2> d3 = d4
                expression = d1.ToString & operators(o1) & d2.ToString & operators(o2) & d3.ToString

                Try
                    result = sc.Eval(expression)

                    If result = d4 Then
                        bTrigger = True
                        RaiseEvent FoundMatch(expression & "=" & d4.ToString)
                    End If
                Catch ex As Exception

                End Try


                ' d1 = d2 <operator2> d3 <operator1> d4
                expression = d2.ToString & operators(o1) & d3.ToString & operators(o2) & d4.ToString

                Try
                    result = sc.Eval(expression)

                    If result = d1 Then
                        bTrigger = True
                        RaiseEvent FoundMatch(d1.ToString & "=" & expression)
                    End If
                Catch ex As Exception

                End Try


                ' d1 <operator1> d2 = d3 <operator2> d4
                expression = d1.ToString & operators(o1) & d2.ToString
                expression2 = d3.ToString & operators(o2) & d4.ToString

                Try
                    result = sc.Eval(expression)
                    result2 = sc.Eval(expression2)

                    If result = result2 Then
                        bTrigger = True
                        RaiseEvent FoundMatch(expression & "=" & expression2)
                    End If
                Catch ex As Exception

                End Try
            Next
        Next

        mCalculationsDone.Add(d1.ToString & d2.ToString & ":" & d3.ToString & d4.ToString)

        RaiseEvent DigitsDone(d1, d2, d3, d4)
        If bTrigger Then RaiseEvent NextMatchLine()
    End Sub
End Class
