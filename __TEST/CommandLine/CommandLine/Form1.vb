﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CommandLineParameters.AddParam("-test")
        CommandLineParameters.AddParam("-testwithvalue:")
        CommandLineParameters.AddParam("-testwithdefaultvalue:", "defualtpeop")
        CommandLineParameters.AddParam("-test2:", "defualtpeop")

        CommandLineParameters.Parse()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sTmp As String = ""

        sTmp &= "-test" & vbCrLf
        sTmp &= "  Found  : " & CommandLineParameters.Item("-test").Found.ToString & vbCrLf
        sTmp &= "  IsValue: " & CommandLineParameters.Item("-test").IsValue.ToString & vbCrLf
        sTmp &= "  Value  : " & CommandLineParameters.Item("-test").Value & vbCrLf
        sTmp &= vbCrLf & vbCrLf

        sTmp &= "-testwithvalue:" & vbCrLf
        sTmp &= "  Found  : " & CommandLineParameters.Item("-testwithvalue:").Found.ToString & vbCrLf
        sTmp &= "  IsValue: " & CommandLineParameters.Item("-testwithvalue:").IsValue.ToString & vbCrLf
        sTmp &= "  Value  : " & CommandLineParameters.Item("-testwithvalue:").Value & vbCrLf
        sTmp &= vbCrLf & vbCrLf

        sTmp &= "-testwithdefaultvalue:" & vbCrLf
        sTmp &= "  Found  : " & CommandLineParameters.Item("-testwithdefaultvalue:").Found.ToString & vbCrLf
        sTmp &= "  IsValue: " & CommandLineParameters.Item("-testwithdefaultvalue:").IsValue.ToString & vbCrLf
        sTmp &= "  Value  : " & CommandLineParameters.Item("-testwithdefaultvalue:").Value & vbCrLf
        sTmp &= vbCrLf & vbCrLf

        sTmp &= "-test2:" & vbCrLf
        sTmp &= "  Found  : " & CommandLineParameters.Item("-test2:").Found.ToString & vbCrLf
        sTmp &= "  IsValue: " & CommandLineParameters.Item("-test2:").IsValue.ToString & vbCrLf
        sTmp &= "  Value  : " & CommandLineParameters.Item("-test2:").Value & vbCrLf
        sTmp &= vbCrLf & vbCrLf

        sTmp &= "-test3:" & vbCrLf
        sTmp &= "  Found  : " & CommandLineParameters.Item("-test3:").Found.ToString & vbCrLf
        sTmp &= "  IsValue: " & CommandLineParameters.Item("-test3:").IsValue.ToString & vbCrLf
        sTmp &= "  Value  : " & CommandLineParameters.Item("-test3:").Value & vbCrLf
        sTmp &= vbCrLf & vbCrLf


        MsgBox(sTmp)
    End Sub
End Class
