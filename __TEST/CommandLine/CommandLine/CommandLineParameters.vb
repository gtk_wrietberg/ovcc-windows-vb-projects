﻿Public Class CommandLineParameters
    Public Class Param
        Private mParam As String
        Public Property Param() As String
            Get
                Return mParam
            End Get
            Set(ByVal value As String)
                mParam = value
            End Set
        End Property

        Private mFound As Boolean
        Public Property Found() As String
            Get
                Return mFound
            End Get
            Set(ByVal value As String)
                mFound = value
            End Set
        End Property

        Private mIsValue As Boolean
        Public Property IsValue() As Boolean
            Get
                Return mIsValue
            End Get
            Set(ByVal value As Boolean)
                mIsValue = value
            End Set
        End Property

        Private mValue As String
        Public Property Value() As String
            Get
                Return mValue
            End Get
            Set(ByVal value As String)
                mValue = value
            End Set
        End Property

        Private mDefaultValue As String
        Public Property DefaultValue() As String
            Get
                Return mDefaultValue
            End Get
            Set(ByVal value As String)
                mDefaultValue = value
            End Set
        End Property
    End Class

    Private Class oParam
        Private mParam As List(Of String)
        Private mFound As List(Of Boolean)
        Private mIsValue As List(Of Boolean)
        Private mDefaultValue As List(Of String)
        Private mValue As List(Of String)

        Private mUnknowns As List(Of String)

        Public Sub New()
            mParam = New List(Of String)
            mFound = New List(Of Boolean)
            mIsValue = New List(Of Boolean)
            mDefaultValue = New List(Of String)
            mValue = New List(Of String)

            mUnknowns = New List(Of String)
        End Sub

        Public Function Add(Param As String, Optional DefaultValue As String = "") As Boolean
            Dim sTmp As String = ""

            If Param.StartsWith("-") Then
                If Param.Contains(":") Then
                    'has value
                    sTmp = Param.Substring(0, Param.IndexOf(":"))
                End If
            Else
                'nope, needs to start with -
                Return False
            End If

            If Param.StartsWith("-") Then
                mParam.Add(Param)
                mFound.Add(False)
                mIsValue.Add(Param.EndsWith(":"))
                mValue.Add(String.Empty)
                mDefaultValue.Add(DefaultValue)

                Return True
            Else
                'nope, needs to start with -
                Return False
            End If
        End Function

        Public Sub Compare(param As String)
            Dim tmpP As String = "", tmpV As String = "", found As Boolean = False

            If param.StartsWith("-") Then
                If param.Contains(":") Then
                    'has value
                    tmpP = param.Substring(0, param.IndexOf(":") + 1)
                    tmpV = param.Substring(param.IndexOf(":") + 1)
                Else
                    tmpP = param
                    tmpV = ""
                End If

                For i As Integer = 0 To mParam.Count - 1
                    If mParam.Item(i).Equals(tmpP) Then
                        mFound.Item(i) = True
                        mValue.Item(i) = tmpV

                        found = True
                    End If
                Next
            End If

            If Not found Then
                mUnknowns.Add(param)
            End If
        End Sub

        Public Function GetStuff(Param As String, ByRef Found As Boolean, ByRef IsValue As Boolean, ByRef Value As String) As Boolean
            Dim index As Integer = mParam.IndexOf(Param)

            If index > -1 Then
                Found = mFound.Item(index)
                IsValue = mIsValue.Item(index)
                Value = mValue.Item(index)

                If IsValue And Value.Equals("") Then
                    Value = mDefaultValue.Item(index)
                End If

                Return True
            Else
                Return False
            End If
        End Function
    End Class

    Private Shared mParams As New oParam

    Public Shared Sub AddParam(Param As String)
        mParams.Add(Param, "")
    End Sub

    Public Shared Sub AddParam(Param As String, DefaultValue As String)
        mParams.Add(Param, DefaultValue)
    End Sub

    Public Shared Sub Parse()
        Dim args() As String = System.Environment.GetCommandLineArgs()

        If args.Length > 1 Then
            For i As Integer = 1 To args.Length - 1
                mParams.Compare(args(i))
            Next
        End If
    End Sub

    Public Shared ReadOnly Property Item(Param As String) As Param
        Get
            Dim tmpParam As New Param

            tmpParam.Param = Param

            If mParams.GetStuff(Param, tmpParam.Found, tmpParam.IsValue, tmpParam.Value) Then
                'found!
            End If

            Return tmpParam
        End Get
    End Property
End Class
