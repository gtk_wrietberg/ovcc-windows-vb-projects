Imports System.ComponentModel

Public Class GetRequestCompletedEventArgs
    Inherits AsyncCompletedEventArgs

#Region "Constructors and Destructors"

    Friend Sub New(ByVal wrop As WebRequestOperation, ByVal [error] As Exception)
        MyBase.New([error], wrop.Status = WebRequestStatus.Cancelled, wrop.RequestKey)

        mOperation = wrop
    End Sub

#End Region

#Region "Properties"

    Private mOperation As WebRequestOperation
    Friend ReadOnly Property Operation() As WebRequestOperation
        Get
            Return mOperation
        End Get
    End Property

    Public ReadOnly Property Status() As WebRequestStatus
        Get
            Return mOperation.Status
        End Get
    End Property

    Public ReadOnly Property RequestKey() As Object
        Get
            Return mOperation.RequestKey
        End Get
    End Property

    Public ReadOnly Property Argument() As Object
        Get
            Return mOperation.Argument
        End Get
    End Property

    Public ReadOnly Property ResponseResults() As Object
        Get
            Return mOperation.ResponseResult
        End Get
    End Property

    Public ReadOnly Property ElapsedTimeInSeconds() As Double
        Get
            Return mOperation.Stopwatch.Elapsed.TotalSeconds()
        End Get
    End Property

    Public ReadOnly Property Uri() As String
        Get
            Return mOperation.Request.RequestUri.AbsoluteUri
        End Get
    End Property

#End Region

End Class
