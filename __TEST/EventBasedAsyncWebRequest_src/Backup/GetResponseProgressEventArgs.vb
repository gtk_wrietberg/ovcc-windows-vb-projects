Imports System.ComponentModel

Public Class GetRequestProgressChangedEventArgs
    Inherits ProgressChangedEventArgs

#Region "Constructors and Destructors"

    Friend Sub New(ByVal wrop As WebRequestOperation, ByVal progressPercentage As Integer)
        MyBase.New(progressPercentage, wrop.RequestKey)

        mOperation = wrop
    End Sub

#End Region

#Region "Properties"

    Private mOperation As WebRequestOperation
    Friend ReadOnly Property Operation() As WebRequestOperation
        Get
            Return mOperation
        End Get
    End Property

    Public ReadOnly Property Status() As WebRequestStatus
        Get
            Return mOperation.Status
        End Get
    End Property

    Public ReadOnly Property RequestKey() As Object
        Get
            Return mOperation.RequestKey
        End Get
    End Property

    Public ReadOnly Property Argument() As Object
        Get
            Return mOperation.Argument
        End Get
    End Property

#End Region

End Class
