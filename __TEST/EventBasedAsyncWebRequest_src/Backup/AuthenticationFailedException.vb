Public Class AuthenticationFailedException
    Inherits ApplicationException

    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

End Class
