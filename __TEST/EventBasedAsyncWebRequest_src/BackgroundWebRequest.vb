Imports System
Imports System.ComponentModel
Imports System.Runtime.CompilerServices
Imports System.Security.Permissions
Imports System.Threading
Imports System.Net
Imports System.Collections.Generic
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.Drawing
Imports System.Collections.Specialized

''' <summary>Executes an operation on a separate thread.</summary>
''' <remarks>
''' <para>This code is based on a reflected version of the BackgroundWorker class using Lutz Roeder's .Net Reflector and the File Reflector add-in.</para>
''' <para>This class uses the Event-based Asynchronous Pattern as described at http://msdn2.microsoft.com/en-us/library/wewwczdw.aspx. </para>
''' </remarks>
<Description("A background reference tot he ")> _
<DefaultEvent("GetRequestCompleted")> _
Public Class BackgroundWebRequest
    Inherits Component

#Region "Fields"

    Private Shared ReadOnly sRunWorkerCompletedKey As Object
    Private Shared ReadOnly sProgressChangedKey As Object
    Private Shared ReadOnly sDoWorkKey As Object

    Private ReadOnly mThreadStart As WorkerThreadStartDelegate

#End Region

#Region "Constructors and Destructors"

    Shared Sub New()
        sDoWorkKey = New Object
        sRunWorkerCompletedKey = New Object
        sProgressChangedKey = New Object
    End Sub

    ''' <summary>Initializes a new instance of the BackgroundWebRequest class.</summary>
    Public Sub New()
        mThreadStart = New WorkerThreadStartDelegate(AddressOf ProcessRequest)
    End Sub

#End Region

#Region "Properties"

    Private mRequests As New Dictionary(Of Object, WebRequestOperation)
    Private ReadOnly Property Requests() As Dictionary(Of Object, WebRequestOperation)
        Get
            Return mRequests
        End Get
    End Property

    Private mUrl As String = ""
    ''' <summary>
    ''' The URI that identifies the Internet resource.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Description("The URI that identifies the Internet resource.")> _
    <Category("Behavior")> _
    <DefaultValue("")> _
    Public Property URL() As String
        Get
            Return mUrl
        End Get
        Set(ByVal value As String)
            mUrl = value
        End Set
    End Property

    Private mTimeout As Integer = 100000
    ''' <summary>
    ''' The number of milliseconds to wait before the request times out. The default is 100,000 milliseconds (100 seconds). 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Description("The number of milliseconds to wait before the request times out. The default is 100,000 milliseconds (100 seconds).")> _
    <Category("Behavior")> _
    <DefaultValue(100000)> _
    Public Property Timeout() As Integer
        Get
            Return mTimeout
        End Get
        Set(ByVal value As Integer)
            mTimeout = value
        End Set
    End Property

    Private mPreAuthenticate As Boolean = False
    ''' <summary>
    ''' true to send a WWW-authenticate HTTP header with requests after authentication has taken place; otherwise, false. The default is false. 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Description("true to send a WWW-authenticate HTTP header with requests after authentication has taken place; otherwise, false. The default is false.")> _
    <Category("Behavior")> _
    <DefaultValue(False)> _
    Public Property PreAuthenticate() As Boolean
        Get
            Return mPreAuthenticate
        End Get
        Set(ByVal value As Boolean)
            mPreAuthenticate = value
        End Set
    End Property

    Private mCredentials As System.Net.ICredentials = Nothing
    ''' <summary>
    ''' Gets or sets the credentials used for authenticating a request for an Internet resource.
    ''' </summary>
    ''' <value>Defaults to CredentialCache.DefaultNetworkCredentials()</value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Browsable(False)> _
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Property Credentials() As System.Net.ICredentials
        Get
            Return mCredentials
        End Get
        Set(ByVal value As System.Net.ICredentials)
            mCredentials = value
        End Set
    End Property

    Private mDefaultCharset As String = "utf-8"
    ''' <summary>
    ''' If the WebResponse does not indicate the charset, this charset is used for processing text responses.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <Description("If the WebResponse does not indicate the charset, this charset is used for processing text responses.")> _
    <Category("Behavior")> _
    <DefaultValue("utf-8")> _
    Public Property DefaultCharset() As String
        Get
            Return mDefaultCharset
        End Get
        Set(ByVal value As String)
            mDefaultCharset = value
        End Set
    End Property

#End Region

#Region "Methods"

    ''' <summary>Requests cancellation of a pending background operation.</summary>
    Public Sub CancelRequest(ByVal requestKey As Object)
        If requestKey Is Nothing Then Return
        If Not mRequests.ContainsKey(requestKey) Then Return
        mRequests(requestKey).Cancel()
    End Sub

    ''' <summary>Gets a value indicating whether the BackgroundWebRequest is running an asynchronous operation.</summary>
    ''' <returns>true, if the <see cref="T:System.ComponentModel.MyBackgroundWorker"></see> is running an asynchronous operation; otherwise, false.</returns>
    Public Function IsBusy(ByVal requestKey As Object) As Boolean
        If requestKey Is Nothing Then Return False
        If mRequests.ContainsKey(requestKey) Then Return True
        Return False
    End Function

    ''' <summary>Starts execution of a background web request.</summary>
    ''' <param name="argument">A parameter for use by the background operation to be executed in the <see cref="E:System.ComponentModel.MyBackgroundWorker.DoWork"></see> event handler. </param>
    ''' <returns>A key that can be used to get state information for a particular asynchronous request.</returns>
    ''' <exception cref="T:System.InvalidOperationException">IsBusy is true.</exception>
    Public Function GetResponseAsync(Optional ByVal argument As Object = Nothing) As Object
        Return GetResponseAsync(mUrl, argument)
    End Function

    ''' <summary>Starts execution of a background web request using the supplied URL.</summary>
    ''' <param name="url">The URL used to get the response.</param>
    ''' <param name="argument">A parameter for use by the background operation to be executed in the <see cref="E:System.ComponentModel.MyBackgroundWorker.DoWork"></see> event handler. </param>
    ''' <returns>A key that can be used to get state information for a particular asynchronous request.</returns>
    ''' <exception cref="T:System.InvalidOperationException">IsBusy is true. </exception>
    Public Function GetResponseAsync(ByVal url As String, Optional ByVal argument As Object = Nothing) As Object
        Dim request As WebRequest = WebRequest.Create(url)
        If mCredentials Is Nothing Then
            request.Credentials = CredentialCache.DefaultCredentials()
        Else
            request.Credentials = mCredentials
        End If
        request.PreAuthenticate = mPreAuthenticate
        request.Timeout = mTimeout
        Return GetResponseAsync(request, argument)
    End Function

    ''' <summary>Starts execution of a background web request using the supplied WebResponse.</summary>
    ''' <param name="request">The WebRequest object to use to get the response.</param>
    ''' <param name="argument">A parameter for use by the background operation to be executed in the <see cref="E:System.ComponentModel.MyBackgroundWorker.DoWork"></see> event handler. </param>
    ''' <returns>A key that can be used to get state information for a particular asynchronous request.</returns>
    ''' <exception cref="T:System.InvalidOperationException">IsBusy is true.</exception>
    Public Function GetResponseAsync(ByVal request As WebRequest, Optional ByVal argument As Object = Nothing) As Object
        Dim key As New Object
        Dim asyncOperation As AsyncOperation = AsyncOperationManager.CreateOperation(key)
        Dim wrop As New WebRequestOperation(key, asyncOperation, request, argument)

        mRequests.Add(key, wrop)

        mThreadStart.BeginInvoke(wrop, Nothing, Nothing)

        Return key
    End Function

    Private Sub ProcessRequest(ByVal wrop As WebRequestOperation)
        Dim response As WebResponse = Nothing
        Dim err As Exception = Nothing
        Dim credentials As NetworkCredential = TryCast(wrop.Request.Credentials, NetworkCredential)

        Try
            Try
                wrop.Status = WebRequestStatus.Connecting
                PostProgressChanged(wrop, 0)
                response = wrop.Request.GetResponse()

                If OperationCancelled(wrop) Then Return

                wrop.Status = WebRequestStatus.Connected
                PostProgressChanged(wrop, 0)
            Catch ex As WebException When ex.Status = WebExceptionStatus.ProtocolError AndAlso ex.Message.Contains("401")
                ' We throw this exception so that the stack trace can be populated correctly. 
                ' It will be caught in the Catch below.
                Throw New AuthenticationFailedException(String.Format("Unable to authenticate user '{0}'.", credentials.UserName), ex)
            End Try

            wrop.Response = response
            Dim processResponseArgs As New ProcessResponseEventArgs(wrop)
            OnProcessResponse(processResponseArgs)
            If Not processResponseArgs.Handled Then
                ProcessResponseStream(wrop)
            End If

            If OperationCancelled(wrop) Then Return

            wrop.Status = WebRequestStatus.Complete
            PostProgressChanged(wrop, 0)
            wrop.ResponseResult = processResponseArgs.ResponseResult

        Catch ex As AuthenticationFailedException
            wrop.Status = WebRequestStatus.AuthenticationFailed
            err = ex
        Catch ex As Exception
            wrop.Status = WebRequestStatus.Error
            err = ex ' pass the error back to the main thread so it can be dealt with properly
        Finally
            wrop.Stopwatch.Stop()

            ' make sure this always fires
            PostRequestCompleted(wrop, err)
        End Try
    End Sub

    Private Sub ProcessResponseStream(ByVal wrop As WebRequestOperation)
        Dim st As IO.Stream
        st = wrop.Response.GetResponseStream()

        Dim mem As New IO.MemoryStream()
        Dim buffer(1024) As Byte
        Dim read As Integer = 0

        Try
            ' read the response stream into memory and post the progress
            Do
                read = st.Read(buffer, 0, buffer.Length)
                mem.Write(buffer, 0, read)

                ' calculate the progress we've made downloading
                ' the content. 
                Dim progressPercent As Double
                progressPercent = (mem.Length / wrop.Response.ContentLength) * 100
                PostProgressChanged(wrop, CInt(progressPercent))
            Loop While read > 0
            st.Close()
            wrop.Response.Close()

            ' Reset the memory position so we can read from the stream.
            mem.Position = 0

            ' Parse the content type (ContentType is an internal class
            ' that is defined below).
            Dim contentType As ContentType
            contentType = New ContentType(wrop.Response.ContentType)

            Select Case contentType.Type
                Case "text"
                    ' we should be able to read any text 
                    ' content into a string (assuming we 
                    ' have the correct encoding type).
                    Dim result(CInt(mem.Length)) As Byte

                    mem.Read(result, 0, CInt(mem.Length))

                    ' We need to get the appropriate
                    ' charset in order to decode the 
                    ' byte array as a string.
                    ' This information can be sent
                    ' in the Content-Type. If it isn't
                    ' we need to use the default 
                    ' charset.
                    Dim charset As String
                    charset = contentType.GetValue( _
                                    "charset", _
                                    mDefaultCharset)

                    ' We have the charset, now get the 
                    ' Encoding object and decode the 
                    ' content into a string.
                    Dim enc As Encoding
                    enc = Encoding.GetEncoding(charset)
                    wrop.ResponseResult = enc.GetString(result)
                Case "image"
                    ' We should be able to read most image 
                    ' types directly into an image. 
                    wrop.ResponseResult = Image.FromStream(mem)
                Case Else
                    ' Let the caller figure out how to 
                    ' handle this content.
                    Dim result(CInt(mem.Length)) As Byte
                    mem.Read(result, 0, CInt(mem.Length))
                    wrop.ResponseResult = result
            End Select
        Finally
            mem.Close()
        End Try
    End Sub

    ' this function is not actually used (it is the sample from the article)
    Public Shared Function ProcessResponseStream( _
        ByVal response As WebResponse, _
        Optional ByVal defaultCharset As String = "utf-8") _
        As Object

        Dim st As IO.Stream
        st = response.GetResponseStream()

        Dim mem As New IO.MemoryStream()
        Dim buffer(1024) As Byte
        Dim read As Integer = 0

        Try
            ' Read the response stream into memory
            Do
                read = st.Read(buffer, 0, buffer.Length)
                mem.Write(buffer, 0, read)
            Loop While read > 0
            st.Close()
            response.Close()

            ' Reset the memory position so we can read 
            ' from the stream.
            mem.Position = 0

            ' Parse the content type (ContentType is an 
            ' internal class).
            Dim contentType As ContentType
            contentType = _
                New ContentType(response.ContentType)

            Select Case contentType.Type
                Case "text"
                    ' we should be able to read any text 
                    ' content into a string (assuming we 
                    ' have the correct encoding type).
                    Dim result(CInt(mem.Length)) As Byte

                    mem.Read(result, 0, CInt(mem.Length))

                    ' We need to get the appropriate
                    ' charset in order to decode the 
                    ' byte array as a string.
                    ' This information can be sent
                    ' in the Content-Type. If it isn't
                    ' we need to use the default 
                    ' charset.
                    Dim charset As String
                    charset = contentType.GetValue( _
                                    "charset", _
                                    defaultCharset)

                    ' We have the charset, now get the 
                    ' Encoding object and decode the 
                    ' content into a string.
                    Dim enc As Encoding
                    enc = Encoding.GetEncoding(charset)
                    Return enc.GetString(result)
                Case "image"
                    ' We should be able to read most image 
                    ' types directly into an image. 
                    Return Image.FromStream(mem)
                Case Else
                    ' Let the caller figure out how to 
                    ' handle this content.
                    Dim result(CInt(mem.Length)) As Byte
                    mem.Read(result, 0, CInt(mem.Length))
                    Return result
            End Select
        Finally
            mem.Close()
        End Try
    End Function

    Public Sub ReportProgress(ByVal requestKey As Object, ByVal progressPercent As Integer)
        Dim wrop As WebRequestOperation = mRequests(requestKey)
        PostProgressChanged(wrop, progressPercent)
    End Sub

    Private Sub PostProgressChanged(ByVal wrop As WebRequestOperation, ByVal progressPercent As Integer)
        Dim e As New GetRequestProgressChangedEventArgs(wrop, progressPercent)
        wrop.Operation.Post(AddressOf PostProgressChanged, e)
    End Sub

    Private Sub PostProgressChanged(ByVal arg As Object)
        Dim e As GetRequestProgressChangedEventArgs = DirectCast(arg, GetRequestProgressChangedEventArgs)
        OnGetRequestProgressChanged(e)
    End Sub

    Private Function OperationCancelled(ByVal wrop As WebRequestOperation) As Boolean
        If Not wrop.CancellationPending Then Return False
        ' make sure the status is set correctly
        wrop.Status = WebRequestStatus.Cancelled
        Return True
    End Function

    Private Sub PostRequestCompleted(ByVal wrop As WebRequestOperation, ByVal err As Exception)
        Dim e As New GetRequestCompletedEventArgs(wrop, err)
        wrop.Operation.PostOperationCompleted(AddressOf PostRequestCompleted, e)
    End Sub

    Private Sub PostRequestCompleted(ByVal arg As Object)
        Dim e As GetRequestCompletedEventArgs = DirectCast(arg, GetRequestCompletedEventArgs)
        mRequests.Remove(e.RequestKey)
        OnGetRequestCompleted(e)
    End Sub

#End Region

#Region "Events"

    ''' <summary>
    ''' Handles the processing of the WebResponse. This event is raised on the request processing thread. Set the ProcessResponseEventArgs.ResponseResults property with the value retrieved from the WebResponse.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Event ProcessResponse(ByVal sender As Object, ByVal e As ProcessResponseEventArgs)
    Protected Overridable Sub OnProcessResponse(ByVal e As ProcessResponseEventArgs)
        RaiseEvent ProcessResponse(Me, e)
    End Sub

    Public Event GetRequestCompleted(ByVal sender As Object, ByVal e As GetRequestCompletedEventArgs)
    Protected Overridable Sub OnGetRequestCompleted(ByVal e As GetRequestCompletedEventArgs)
        RaiseEvent GetRequestCompleted(Me, e)
    End Sub

    Public Event GetRequestProgressChanged(ByVal sender As Object, ByVal e As GetRequestProgressChangedEventArgs)
    Protected Overridable Sub OnGetRequestProgressChanged(ByVal e As GetRequestProgressChangedEventArgs)
        RaiseEvent GetRequestProgressChanged(Me, e)
    End Sub

#End Region

#Region "Delegates"

    Private Delegate Sub WorkerThreadStartDelegate(ByVal argument As WebRequestOperation)

#End Region

    Private Class ContentType
        Public Sub New(ByVal contentType As String)
            mContentType = contentType
            ParseContentType(contentType)
        End Sub

        Private Sub ParseContentType(ByVal contentType As String)
            'examples of possible content-type values...
            ' - text/html
            ' - image/gif
            ' - audio/basic
            ' - application/octet-stream
            ' - text/html; charset=utf-8

            ' This regular expression should parse the
            ' content type and return the type, subtype,
            ' and any parameters.
            Dim regex As New Regex( _
                "(?<type>[\w-]*)/" _
                & "(?<subtype>[\w-]*)" _
                & "(?<params>.*)?")

            Dim m As Match = regex.Match(contentType)
            mType = m.Groups("type").Value
            mSubtype = m.Groups("subtype").Value

            ' The parameters are specified as name=value
            ' pairs. There can be multiple parameters which
            ' are separated by a semi-colon.
            Dim params() As String
            params = m.Groups("params").Value.Split(";"c)

            For Each keyval As String In params
                keyval = keyval.Trim()

                If Not keyval.Contains("=") Then
                    ' parameters should always contain an =
                    ' if they don't, we don't know what to
                    ' do with it. It's likely just an empty
                    ' string.

                    Dim msg As String
                    msg = String.Format( _
                        "Invalid parameter: '{0}'", keyval)
                    Debug.Assert(keyval = "", msg)
                    Continue For
                End If

                Dim splitKeyVal() As String
                splitKeyVal = keyval.Split("="c)
                If splitKeyVal.Length <> 2 Then
                    ' This is not a valid parameter.
                    ' Ignore it and continue.
                    Dim msg As String
                    msg = String.Format( _
                        "Invalid parameter: '{0}'", keyval)
                    Debug.Fail(msg)
                    Continue For
                End If

                ' add the parameter to the collection
                Dim key As String = splitKeyVal(0)
                Dim val As String = splitKeyVal(1)
                mParameters.Add(key, val)
            Next
        End Sub

        Private mContentType As String
        Public ReadOnly Property ContentType() As String
            Get
                Return mContentType
            End Get
        End Property

        Private mType As String
        Public ReadOnly Property Type() As String
            Get
                Return mType
            End Get
        End Property

        Private mSubtype As String
        Public ReadOnly Property Subtype() As String
            Get
                Return mSubtype
            End Get
        End Property

        Private mParameters As New StringDictionary
        Private ReadOnly Property Parameters() As StringDictionary
            Get
                Return mParameters
            End Get
        End Property

        Public Function GetValue( _
            ByVal paramName As String, _
            Optional ByVal defaultValue As String = "") _
            As String

            If mParameters.ContainsKey(paramName) Then
                Return mParameters(paramName)
            Else
                Return defaultValue
            End If
        End Function

    End Class

End Class

Public Enum WebRequestStatus
    Ready
    Connecting
    Connected
    Cancelled
    [Error]
    AuthenticationFailed
    Complete
End Enum

