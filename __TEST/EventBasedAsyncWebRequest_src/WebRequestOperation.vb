Imports System.ComponentModel
Imports System.Net

Friend Class WebRequestOperation

#Region "Constructors and Destructors"

    Public Sub New(ByVal key As Object, ByVal operation As AsyncOperation, ByVal request As WebRequest, ByVal argument As Object)
        mKey = key
        mOperation = operation
        mRequest = request
        mArgs = argument
    End Sub

#End Region

#Region "Properties"

    Private mKey As Object
    Public ReadOnly Property RequestKey() As Object
        Get
            Return mKey
        End Get
    End Property

    Private mStatus As WebRequestStatus = WebRequestStatus.Ready
    Public Property Status() As WebRequestStatus
        Get
            Return mStatus
        End Get
        Set(ByVal value As WebRequestStatus)
            mStatus = value
        End Set
    End Property

    Private mCancellationPending As Boolean = False
    Public ReadOnly Property CancellationPending() As Boolean
        Get
            Return mCancellationPending
        End Get
    End Property

    Private mOperation As AsyncOperation
    Public ReadOnly Property Operation() As AsyncOperation
        Get
            Return mOperation
        End Get
    End Property

    Private mRequest As WebRequest
    Public ReadOnly Property Request() As WebRequest
        Get
            Return mRequest
        End Get
    End Property

    Private mResponse As WebResponse
    Public Property Response() As WebResponse
        Get
            Return mResponse
        End Get
        Set(ByVal value As WebResponse)
            mResponse = value
        End Set
    End Property

    Private mArgs As Object
    Public ReadOnly Property Argument() As Object
        Get
            Return mArgs
        End Get
    End Property

    Private mResult As Object
    Public Property ResponseResult() As Object
        Get
            Return mResult
        End Get
        Set(ByVal value As Object)
            mResult = value
        End Set
    End Property

    Private mError As Exception
    Public Property [Error]() As Exception
        Get
            Return mError
        End Get
        Set(ByVal value As Exception)
            mError = value
        End Set
    End Property

    Private mStopwatch As Stopwatch = System.Diagnostics.Stopwatch.StartNew()
    Public ReadOnly Property Stopwatch() As Stopwatch
        Get
            Return mStopwatch
        End Get
    End Property

#End Region

#Region "Methods"

    Public Sub Cancel()
        mCancellationPending = True
    End Sub

#End Region

End Class
