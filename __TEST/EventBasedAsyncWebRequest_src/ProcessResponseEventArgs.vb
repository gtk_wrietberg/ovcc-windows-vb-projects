Imports System.Net
Imports System.ComponentModel

Public Class ProcessResponseEventArgs
    Inherits EventArgs

#Region "Constructors and Destructors"

    Friend Sub New(ByVal wrop As WebRequestOperation)
        mOperation = wrop
    End Sub

#End Region

#Region "Properties"

    Private mOperation As WebRequestOperation
    Friend ReadOnly Property Operation() As WebRequestOperation
        Get
            Return mOperation
        End Get
    End Property

    Public ReadOnly Property RequestKey() As Object
        Get
            Return mOperation.RequestKey
        End Get
    End Property

    Public ReadOnly Property Argument() As Object
        Get
            Return mOperation.Argument
        End Get
    End Property

    Public ReadOnly Property Request() As WebRequest
        Get
            Return mOperation.Request
        End Get
    End Property

    Public ReadOnly Property Response() As WebResponse
        Get
            Return mOperation.Response
        End Get
    End Property

    Public Property ResponseResult() As Object
        Get
            Return mOperation.ResponseResult
        End Get
        Set(ByVal value As Object)
            mOperation.ResponseResult = value
        End Set
    End Property

    Private mHandled As Boolean = False
    Public Property Handled() As Boolean
        Get
            Return mHandled
        End Get
        Set(ByVal value As Boolean)
            mHandled = value
        End Set
    End Property

#End Region

End Class
