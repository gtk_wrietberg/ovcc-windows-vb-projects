Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Xml

Public Class clsReport
    Private mXmlReport As XmlDocument
    Private mServerUrl As String



    Public ReadOnly Property Xml() As String
        Get
            Return mXmlReport.InnerXml
        End Get
    End Property

    Public Property ServerUrl() As String
        Get
            Return mServerUrl
        End Get
        Set(ByVal value As String)
            mServerUrl = value
        End Set
    End Property

    Public Function CreateReportTemplate() As Boolean
        Dim xmlRootNode As XmlElement
        Dim xmlNode As XmlElement

        mXmlReport = New XmlDocument

        Dim xmlDec As XmlDeclaration = mXmlReport.CreateXmlDeclaration("1.0", Nothing, Nothing)
        mXmlReport.AppendChild(xmlDec)

        xmlRootNode = mXmlReport.CreateElement("LobbyPCAgentAnalyzer")
        mXmlReport.AppendChild(xmlRootNode)

        xmlNode = mXmlReport.CreateElement("AnalyzerVersion")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("TerminalName")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("SiteKioskVersion")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("AgentVersion")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("AgentPath")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("HeartbeatUrl")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("LogfileUrl")
        xmlRootNode.AppendChild(xmlNode)

        Return True
    End Function

    Public Sub SetTerminalName(ByVal sValue As String)
        Dim xmlNode As XmlElement

        xmlNode = mXmlReport.SelectSingleNode("/LobbyPCAgentAnalyzer/TerminalName")
        xmlNode.AppendChild(mXmlReport.CreateTextNode(sValue))
    End Sub

    Public Sub SetSiteKioskVersion(ByVal sValue As String)
        Dim xmlNode As XmlElement

        xmlNode = mXmlReport.SelectSingleNode("/LobbyPCAgentAnalyzer/SiteKioskVersion")
        xmlNode.AppendChild(mXmlReport.CreateTextNode(sValue))
    End Sub

    Public Sub SetAgentVersion(ByVal sValue As String)
        Dim xmlNode As XmlElement

        xmlNode = mXmlReport.SelectSingleNode("/LobbyPCAgentAnalyzer/AgentVersion")
        xmlNode.AppendChild(mXmlReport.CreateTextNode(sValue))
    End Sub

    Public Sub SetAgentPath(ByVal sValue As String)
        Dim xmlNode As XmlElement

        xmlNode = mXmlReport.SelectSingleNode("/LobbyPCAgentAnalyzer/AgentPath")
        xmlNode.AppendChild(mXmlReport.CreateTextNode(sValue))
    End Sub

    Public Sub SetHeartbeatUrl(ByVal sValue As String)
        Dim xmlNode As XmlElement

        xmlNode = mXmlReport.SelectSingleNode("/LobbyPCAgentAnalyzer/HeartbeatUrl")
        xmlNode.AppendChild(mXmlReport.CreateTextNode(sValue))
    End Sub

    Public Sub SetLogfileUrl(ByVal sValue As String)
        Dim xmlNode As XmlElement

        xmlNode = mXmlReport.SelectSingleNode("/LobbyPCAgentAnalyzer/LogfileUrl")
        xmlNode.AppendChild(mXmlReport.CreateTextNode(sValue))
    End Sub

    Public Sub SetAnalyzerVersion(ByVal sValue As String)
        Dim xmlNode As XmlElement

        xmlNode = mXmlReport.SelectSingleNode("/LobbyPCAgentAnalyzer/AnalyzerVersion")
        xmlNode.AppendChild(mXmlReport.CreateTextNode(sValue))
    End Sub

    Public Function Send() As Boolean
        Try

            Dim request As WebRequest = WebRequest.Create(mServerUrl)

            request.Method = "POST"

            Dim postData As String
            postData = mXmlReport.InnerXml

            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)

            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length

            Dim dataStream As Stream = request.GetRequestStream()

            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            Dim response As WebResponse = request.GetResponse()

            'Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)

            dataStream = response.GetResponseStream()

            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()

            'Console.WriteLine(responseFromServer)

            reader.Close()
            dataStream.Close()
            response.Close()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub New()
        Me.CreateReportTemplate()
    End Sub
End Class
