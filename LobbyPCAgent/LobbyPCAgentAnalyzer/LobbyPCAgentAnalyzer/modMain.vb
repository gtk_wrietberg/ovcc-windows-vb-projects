Module modMain
    Public Sub Main()
        Dim oReport As New clsReport

        oReport.ServerUrl = "http://lobbypclicensing.ibahn.com/lpcagentanalyzer/"

        '--------------------------------------------------------------------------------------------------------------------------------------------------------
        'Analyzer Version
        Try
            Dim oFileVersionInfo1 As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

            oReport.SetAnalyzerVersion(oFileVersionInfo1.FileVersion)
        Catch ex As Exception
            oReport.SetAnalyzerVersion("0.0")
        End Try

        '--------------------------------------------------------------------------------------------------------------------------------------------------------
        'LPC Agent Version
        Dim sAgentPath As String

        Try
            sAgentPath = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\LobbyPCAgent", "ImagePath", "(not installed)")

            If sAgentPath Is Nothing Then
                sAgentPath = "(not installed)"
            End If
        Catch ex As Exception
            sAgentPath = ex.Message.Replace(",", " ")
        End Try

        oReport.SetAgentPath(sAgentPath)

        If IO.File.Exists(sAgentPath) Then
            Try
                Dim oFileVersionInfo2 As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAgentPath)

                oReport.SetAgentVersion(oFileVersionInfo2.FileVersion)
            Catch ex As Exception
                oReport.SetAgentVersion(ex.Message.Replace(",", " "))
            End Try
        Else
            oReport.SetAgentVersion("(file does not exist)")
        End If


        '--------------------------------------------------------------------------------------------------------------------------------------------------------
        'Heartbeat & Logfile Url
        Dim sHeartbeatUrl As String, sLogfileUrl As String

        Try
            sHeartbeatUrl = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPCAgent", "HeartBeatServerURL", "http://")
        Catch ex As Exception
            sHeartbeatUrl = ex.Message.Replace(",", " ")
        End Try
        Try
            sLogfileUrl = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPCAgent", "LogServerURL", "http://")
        Catch ex As Exception
            sLogfileUrl = ex.Message.Replace(",", " ")
        End Try

        oReport.SetHeartbeatUrl(sHeartbeatUrl)
        oReport.SetLogfileUrl(sLogfileUrl)


        '--------------------------------------------------------------------------------------------------------------------------------------------------------
        'SiteKiosk Version
        Dim sSiteKioskVersion As String

        Try
            sSiteKioskVersion = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "Build", "0.0")
        Catch ex As Exception
            sSiteKioskVersion = ex.Message.Replace(",", " ")
        End Try
        oReport.SetSiteKioskVersion(sSiteKioskVersion)


        '--------------------------------------------------------------------------------------------------------------------------------------------------------
        'LPC Name
        Dim sTerminalName As String = ""

        Try
            sTerminalName = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPCAgent", "LastPCName", "")
        Catch ex As Exception
        End Try

        If sTerminalName = "" Then
            Try
                sTerminalName = Environment.MachineName
            Catch ex As Exception
                sTerminalName = "Name could not be retrieved (" & ex.Message.Replace(",", " ") & ")"
            End Try
        End If

        oReport.SetTerminalName(sTerminalName)


        '--------------------------------------------------------------------------------------------------------------------------------------------------------
        'Send
        If oReport.Send() Then
            System.Environment.Exit(0)
        Else
            System.Environment.Exit(-1)
        End If
    End Sub
End Module
