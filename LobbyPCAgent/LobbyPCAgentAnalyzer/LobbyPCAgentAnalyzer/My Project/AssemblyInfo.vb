﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("LobbyPCAgentAnalyzer")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("LobbyPCAgentAnalyzer")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c99f59d3-4b34-4269-8e44-9f6d6818a620")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.15.1608")> 
<Assembly: AssemblyFileVersion("1.0.15.1608")> 
