﻿Public Class LobbyPCLogfileUploader_service
    Private mThreading_Main As Threading.Thread
    Private mThreading_Upload As Threading.Thread

    Private _WaitHandle_FirstThreadDone As New Threading.AutoResetEvent(False)

    Private mLoop_Abort As Boolean

    '======================================================================================================================================================================
    Protected Overrides Sub OnStart(ByVal args() As String)
        Globals.InitGlobals()

        Globals.Logger.LogFileDirectory = Globals.LogFileDirectory
        Globals.Logger.LogFileName = Globals.LogFileName
        Globals.Logger.Debugging = Globals.Settings.Debugging

        Globals.Logger.WriteMessage(New String("*", 50))
        Globals.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Globals.Logger.WriteMessage("service started")

        '-----------------------------------

        StartMainThread()
    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()

        Globals.Logger.WriteMessage("service stopped")
    End Sub

    '======================================================================================================================================================================

    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception
        End Try
        Try
            mThreading_Upload.Abort()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Thread_Main()
        Globals.Logger.WriteDebug("(MainThread) init")

        mLoop_Abort = False

        Globals.Logger.WriteDebug("(MainThread) started")

        Dim lFileList As List(Of String)
        Dim dLastLog As Date, dCheck As Date, dNow As Date
        Dim sFile As String


        Do While Not mLoop_Abort
            Globals.Settings.LoadSettings()

            Globals.Logger.WriteDebug("(MainThread) get dates")

            dNow = Date.Now

            Globals.Logger.WriteDebug("date last log registry: " & Globals.Settings.Date_LastLog, 1)

            If Helpers.IsValidHBDate(Globals.Settings.Date_LastLog) Then
                dLastLog = Date.Parse(Globals.Settings.Date_LastLog)

                Globals.Logger.WriteDebug("last log: " & Helpers.FromNormalDateToHBDate(dLastLog), 1)
                Globals.Logger.WriteDebug("last log: " & Globals.Settings.Date_LastLog, 1)


                Globals.Logger.WriteDebug("(MainThread) get log files")

                lFileList = New List(Of String)

                Dim dSkLogs As New IO.DirectoryInfo(Helpers.GetSiteKioskLogfileFolder)
                Dim fSkLogs As IO.FileInfo() = dSkLogs.GetFiles()

                Globals.Logger.WriteDebug("folder: " & Helpers.GetSiteKioskLogfileFolder)

                For Each fSkLog As IO.FileInfo In fSkLogs
                    If System.Text.RegularExpressions.Regex.IsMatch(fSkLog.Name, Globals.CONSTANTS__REGEXP_LOGFILE) Then
                        lFileList.Add(fSkLog.Name)
                    End If
                Next

                Globals.Logger.WriteDebug("files: " & lFileList.Count.ToString)


                Globals.Logger.WriteDebug("(MainThread) find files to upload")
                Dim iDiff As Integer

                iDiff = Math.Abs(DateDiff(DateInterval.Day, DateAdd(DateInterval.Day, -1, dNow), dLastLog))

                If iDiff > 0 Then
                    If iDiff = 1 Then
                        Globals.Logger.WriteMessage("Uploading " & iDiff.ToString & " file", 0)
                    Else
                        Globals.Logger.WriteMessage("Uploading " & iDiff.ToString & " files", 0)
                    End If

                    For i As Integer = iDiff To 1 Step -1
                        dCheck = DateAdd(DateInterval.Day, -i, dNow)

                        sFile = Helpers.FromNormalDateToHBDate(dCheck) & ".txt"

                        If lFileList.Contains(sFile) Then
                            Globals.Logger.WriteMessage(sFile, 1)

                            _WaitHandle_FirstThreadDone.Reset()
                            'mThreading_Upload = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Upload))
                            mThreading_Upload = New Threading.Thread(Sub() Me.Thread_Upload(sFile))
                            mThreading_Upload.Start()
                            Try
                                If Not _WaitHandle_FirstThreadDone.WaitOne(Globals.CONSTANTS__MAX_UPLOAD_DELAY) Then
                                    Globals.Logger.WriteError("TIMEOUT!!!", 2)
                                    Try
                                        'If mThreading_Upload.IsAlive Then
                                        mThreading_Upload.Abort()
                                        'End If
                                    Catch ex As Exception
                                        Globals.Logger.WriteError("(MainThread) mThreading_Upload.Abort exception", 2)
                                        Globals.Logger.WriteError(ex.Message, 3)
                                    End Try
                                End If
                            Catch ex As Exception
                                Globals.Logger.WriteError("(MainThread) _WaitHandle_FirstThreadDone exception", 2)
                                Globals.Logger.WriteError(ex.Message, 3)
                            End Try


                        Else
                            Globals.Logger.WriteError(sFile & " does not exist!", 1)
                        End If
                    Next
                End If
            Else
                'Invalid last log date in registry
                'Hopefully the next heartbeat will fix that
                Globals.Logger.WriteError("Invalid last log date in registry. Next heartbeat iteration should fix that.", 1)
            End If


            Threading.Thread.Sleep(Globals.CONSTANTS__LOOP_DELAY)
        Loop

    End Sub

    Private Sub Thread_Upload(sLogFile As String)
        Dim oUpload = New Upload

        Try
            Dim request As Net.WebRequest = Net.WebRequest.Create(Globals.Settings.ServerUrl_Logfiles)

            request.Method = "POST"

            Globals.Logger.WriteDebug("(UploadThread) init upload")

            oUpload.ComputerName = Environment.MachineName
            oUpload.SitekioskVersion = Helpers.GetSiteKioskVersion(True)
            oUpload.LogName = sLogFile

            Dim objReader As New System.IO.StreamReader(Helpers.GetSiteKioskLogfileFolder & "\" & sLogFile)
            Dim sLine As String

            Do While objReader.Peek() <> -1
                sLine = objReader.ReadLine()
                oUpload.AddLine(Helpers.CleanSiteKioskLogLine(sLine))
            Loop


            Globals.Logger.WriteDebug("(UploadThread) sending logfile")
            Globals.Logger.WriteDebug("length: " & oUpload.Request.Length.ToString, 1)

            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(oUpload.Request)

            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length

            Dim dataStream As IO.Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            Globals.Logger.WriteDebug("(UploadThread) receiving server response")

            Dim response As Net.WebResponse = request.GetResponse()

            dataStream = response.GetResponseStream()

            Dim reader As New IO.StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()


            oUpload.Response = responseFromServer

            Globals.Logger.WriteDebug(oUpload.Response, 1)

            reader.Close()
            dataStream.Close()
            response.Close()
        Catch ex As Exception
            Globals.Logger.WriteError("(UploadThread) upload exception", 0)
            Globals.Logger.WriteError("server: " & Globals.Settings.ServerUrl_Logfiles, 1)
            Globals.Logger.WriteError(ex.Message, 2)
        End Try

        'Analyse the response ; should look something like this:
        '  <lobbypc><logfile><result>0</result></logfile></lobbypc>
        Globals.Logger.WriteDebug("(UploadThread) parsing server response")

        Dim xmlDoc As Xml.XmlDocument
        Dim xmlNode_Root As Xml.XmlNode
        Dim xmlNode__logfile As Xml.XmlNode
        Dim xmlNode__logfile_result As Xml.XmlNode
        Dim xmlNode__logfile_ignored As Xml.XmlNode

        Try
            xmlDoc = New Xml.XmlDocument
            xmlDoc.LoadXml(oUpload.Response)
            xmlNode_Root = xmlDoc.SelectSingleNode("lobbypc")
            xmlNode__logfile = xmlNode_Root.SelectSingleNode("logfile")
            xmlNode__logfile_result = xmlNode__logfile.SelectSingleNode("result")
            xmlNode__logfile_ignored = xmlNode__logfile.SelectSingleNode("ignored")

            If Not xmlNode__logfile_result Is Nothing Then
                Dim sResult As String

                sResult = xmlNode__logfile_result.InnerText

                If sResult = "0" Then
                    'All seems ok
                    Globals.Logger.WriteMessage("ok", 2)
                Else
                    'Server error
                    Globals.Logger.WriteError("server reported '" & sResult & "', that's all I know. But yes, it's an error.", 2)
                End If
            Else
                If Not xmlNode__logfile_ignored Is Nothing Then
                    Globals.Logger.WriteMessage("ignored by server. retrying on next iteration.", 2)
                Else
                    Globals.Logger.WriteError("invalid server response", 2)
                    Globals.Logger.WriteError("this was the response:", 3)
                    Globals.Logger.WriteError(oUpload.Response, 4)
                End If
            End If
        Catch ex As Exception
            Globals.Logger.WriteError("(UploadThread) response exception", 0)
            Globals.Logger.WriteError(ex.Message, 1)
        End Try

        _WaitHandle_FirstThreadDone.Set()
    End Sub

End Class
