﻿Public Class Logfiles
    Private mFileList As List(Of String)
    Private mFileDate As List(Of String)

    Private mCounter As Integer



    Public Sub Add(sFile As String)
        If Not mFileList.Contains(sFile) Then
            mFileList.Add(sFile)


        End If
    End Sub

    Public Function GetFileByDate(sDate As String) As String
        Dim i As Integer = mFileDate.IndexOf(sDate)

        If i >= 0 Then
            Return mFileList.Item(i)
        Else
            Return ""
        End If
    End Function

    Public Sub Reset()
        mCounter = 0
    End Sub

    Public Function GetNext(ByRef sFile As String, ByRef sDate As String) As Boolean
        If mCounter < mFileList.Count Then
            sFile = mFileList.Item(mCounter)
            sDate = mFileDate.Item(mCounter)

            mCounter += 1

            Return True
        Else
            Return False
        End If
    End Function
End Class
