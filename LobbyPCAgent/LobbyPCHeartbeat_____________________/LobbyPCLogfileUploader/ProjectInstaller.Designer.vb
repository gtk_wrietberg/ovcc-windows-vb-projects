﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ServiceProcessInstaller_LPCLogfiles = New System.ServiceProcess.ServiceProcessInstaller()
        Me.ServiceInstaller_LPCLogfiles = New System.ServiceProcess.ServiceInstaller()
        '
        'ServiceProcessInstaller_LPCLogfiles
        '
        Me.ServiceProcessInstaller_LPCLogfiles.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.ServiceProcessInstaller_LPCLogfiles.Password = Nothing
        Me.ServiceProcessInstaller_LPCLogfiles.Username = Nothing
        '
        'ServiceInstaller_LPCLogfiles
        '
        Me.ServiceInstaller_LPCLogfiles.Description = "LobbyPC Logfile Uploader"
        Me.ServiceInstaller_LPCLogfiles.DisplayName = "LobbyPCLogfileUploader"
        Me.ServiceInstaller_LPCLogfiles.ServiceName = "LobbyPCLogfileUploader"
        Me.ServiceInstaller_LPCLogfiles.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.ServiceProcessInstaller_LPCLogfiles, Me.ServiceInstaller_LPCLogfiles})

    End Sub
    Friend WithEvents ServiceProcessInstaller_LPCLogfiles As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents ServiceInstaller_LPCLogfiles As System.ServiceProcess.ServiceInstaller

End Class
