﻿Module modMain
    Public Sub Main()



        'Copy files
        '------------------------------------------------------
        Dim sBackupFolder As String = ""
        Dim dDate As Date = Now()

        sBackupFolder = LPCConstants.FilesAndFolders.FOLDER__Backup

        sBackupFolder = sBackupFolder.Replace("%%Application.ProductName%%", Application.ProductName)
        sBackupFolder = sBackupFolder.Replace("%%Application.ProductVersion%%", Application.ProductVersion)
        sBackupFolder = sBackupFolder.Replace("%%Backup.Date%%", dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))



        Dim oCopyFiles As CopyFiles = New CopyFiles
        oCopyFiles.SuppressCopyLogging = False
        oCopyFiles.BackupDirectory = sBackupFolder

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        LPCGlobals.Objects.Logger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        'Install service part
        ServiceInstaller.InstallAndStart("LobbyPCWatchdog", "LobbyPC Watchdog service", LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.PROCESS__WatchdogService)



    End Sub
End Module
