﻿Public Class Globals
    Public Shared Logger As clsLogger
    Public Shared XOrObfuscation As clsXOrObfuscation
    Public Shared Settings As clsSettings
    Public Shared Heartbeat As clsHeartbeat

    Public Shared Property LogFileDirectory As String
    Public Shared Property LogFileName As String
    Public Shared Property BackupDirectory As String
    Public Shared Property OverwriteExistingConfig As Boolean
    Public Shared Property TESTMODE As Boolean

    Public Shared ReadOnly CONSTANTS__LOOP_DELAY As Integer = 5000
    Public Shared ReadOnly CONSTANTS__HEARTBEAT_APP_VERSION As String = "2.1.136"

    Public Shared Sub InitGlobals()
        LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        Logger = New clsLogger
        Settings = New clsSettings
        XOrObfuscation = New clsXOrObfuscation

        InitLogFileName()


        Try
            IO.Directory.CreateDirectory(LogFileDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_LogFileDirectory & "'): " & ex.Message)
        End Try

        Try
            IO.Directory.CreateDirectory(BackupDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_BackupDirectory & "'): " & ex.Message)
        End Try

    End Sub

    Public Shared Sub InitLogFileName()
        Dim dDate As Date = Now()

        LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"
    End Sub

End Class
