﻿Imports Microsoft.Win32

Public Class clsSettings
    Private ReadOnly cRegistry_Root As String = "Software\\GuestTek\LobbyPC\\Heartbeat"
    Private ReadOnly cRegistry_Key_ServerUrl_Heartbeat As String = "ServerUrl_Heartbeat"
    Private ReadOnly cRegistry_Key_ServerUrl_Logfiles As String = "ServerUrl_Logfiles"
    Private ReadOnly cRegistry_Key_Loop_Delay As String = "Loop_Delay"
    Private ReadOnly cRegistry_Key_Loop_SkipRandomisation As String = "Loop_SkipRandomisation"
    Private ReadOnly cRegistry_Key_Debug As String = "Debug"
    Private ReadOnly cRegistry_Key_Date_LastHeartbeat As String = "Date_LastHeartbeat"
    Private ReadOnly cRegistry_Key_Date_LastLog As String = "Date_LastLog"
    Private ReadOnly cRegistry_Key_Log_UploadHistoryDays As String = "Log_UploadHistoryDays"


    Private ReadOnly cDEFAULT_ServerUrl_Heartbeat As String = "https://lpchbserver01.ibahn.com/axis/servlet/HeartbeatReceiver"
    Private ReadOnly cDEFAULT_ServerUrl_Logfiles As String = "https://lpchbserver01.ibahn.com/axis/servlet/LogfileLineReceiver"
    Private ReadOnly cDEFAULT_Loop_Delay As Integer = 30000
    Private ReadOnly cDEFAULT_Loop_SkipRandomisation As Boolean = False
    Private ReadOnly cDEFAULT_Debug As Boolean = True
    Private ReadOnly cDEFAULT_UploadHistoryDays As Integer = 30

    Private mServerUrl_Heartbeat As String = cDEFAULT_ServerUrl_Heartbeat
    Private mServerUrl_Logfiles As String = cDEFAULT_ServerUrl_Logfiles
    Private mLoop_Delay As Integer = cDEFAULT_Loop_Delay
    Private mLoop_SkipRandomisation As Boolean = cDEFAULT_Loop_SkipRandomisation
    Private mDebug As Boolean = cDEFAULT_Debug
    Private mLog_UploadHistoryDays As Integer = cDEFAULT_UploadHistoryDays

    Private mDate_LastHeartbeat As String
    Private mDate_LastLog As String

#Region "set/get"
    Public Property ServerUrl_Heartbeat As String
        Get
            Return mServerUrl_Heartbeat
        End Get
        Set(value As String)
            mServerUrl_Heartbeat = value
        End Set
    End Property

    Public Property ServerUrl_Logfiles As String
        Get
            Return mServerUrl_Logfiles
        End Get
        Set(value As String)
            mServerUrl_Logfiles = value
        End Set
    End Property

    Public Property Loop_Delay As Integer
        Get
            Return mLoop_Delay
        End Get
        Set(value As Integer)
            mLoop_Delay = value
        End Set
    End Property

    Public Property Loop_SkipRandomisation As Boolean
        Get
            Return mLoop_SkipRandomisation
        End Get
        Set(value As Boolean)
            mLoop_SkipRandomisation = value
        End Set
    End Property

    Public Property Debugging As Boolean
        Get
            Return mDebug
        End Get
        Set(value As Boolean)
            mDebug = value
        End Set
    End Property

    Public Property Date_LastLog As String
        Get
            Return mDate_LastLog
        End Get
        Set(value As String)
            mDate_LastLog = value
        End Set
    End Property

    Public Property Date_LastHeartbeat As String
        Get
            Return mDate_LastHeartbeat
        End Get
        Set(value As String)
            mDate_LastHeartbeat = value
        End Set
    End Property

    Public Property Log_UploadHistoryDays As Integer
        Get
            Return mLog_UploadHistoryDays
        End Get
        Set(value As Integer)
            mLog_UploadHistoryDays = value
        End Set
    End Property
#End Region

    Public Sub LoadSettings()
        Dim regRoot As RegistryKey

        regRoot = Registry.LocalMachine.OpenSubKey(cRegistry_Root, False)
        If regRoot Is Nothing Then
            Me.SaveSettings()
        Else
            mServerUrl_Heartbeat = regRoot.GetValue(cRegistry_Key_ServerUrl_Heartbeat, cDEFAULT_ServerUrl_Heartbeat)
            mServerUrl_Logfiles = regRoot.GetValue(cRegistry_Key_ServerUrl_Logfiles, cDEFAULT_ServerUrl_Logfiles)
            mLoop_Delay = Convert.ToInt32(regRoot.GetValue(cRegistry_Key_Loop_Delay, cDEFAULT_Loop_Delay))
            mLoop_SkipRandomisation = Convert.ToBoolean(regRoot.GetValue(cRegistry_Key_Loop_SkipRandomisation, cDEFAULT_Loop_SkipRandomisation))
            mDebug = Convert.ToBoolean(regRoot.GetValue(cRegistry_Key_Debug, cDEFAULT_Debug))
            mLog_UploadHistoryDays = Convert.ToInt32(regRoot.GetValue(cRegistry_Key_Log_UploadHistoryDays, cDEFAULT_UploadHistoryDays))
            mDate_LastLog = regRoot.GetValue(cRegistry_Key_Date_LastLog, "")
            mDate_LastHeartbeat = regRoot.GetValue(cRegistry_Key_Date_LastHeartbeat, "")
        End If

        Me.CheckValues()
    End Sub

    Public Sub SaveSettings()
        _SaveSettings(False)
    End Sub

    Public Sub SaveDates()
        _SaveSettings(True)
    End Sub

    Private Sub _SaveSettings(Optional DateOnly As Boolean = False)
        Dim regRoot As RegistryKey

        regRoot = Registry.LocalMachine.OpenSubKey(cRegistry_Root, True)
        If regRoot Is Nothing Then
            regRoot = Registry.LocalMachine.CreateSubKey(cRegistry_Root)
        End If

        Me.CheckValues()

        If DateOnly Then
            regRoot.SetValue(cRegistry_Key_Date_LastLog, mDate_LastLog, RegistryValueKind.String)
            regRoot.SetValue(cRegistry_Key_Date_LastHeartbeat, mDate_LastHeartbeat, RegistryValueKind.String)
        Else
            regRoot.SetValue(cRegistry_Key_ServerUrl_Heartbeat, mServerUrl_Heartbeat, RegistryValueKind.String)
            regRoot.SetValue(cRegistry_Key_ServerUrl_Logfiles, mServerUrl_Logfiles, RegistryValueKind.String)
            regRoot.SetValue(cRegistry_Key_Loop_Delay, mLoop_Delay, RegistryValueKind.DWord)
            regRoot.SetValue(cRegistry_Key_Log_UploadHistoryDays, mLog_UploadHistoryDays, RegistryValueKind.DWord)
            If mLoop_SkipRandomisation Then
                regRoot.SetValue(cRegistry_Key_Loop_SkipRandomisation, 1, RegistryValueKind.DWord)
            Else
                regRoot.SetValue(cRegistry_Key_Loop_SkipRandomisation, 0, RegistryValueKind.DWord)
            End If
            If mDebug Then
                regRoot.SetValue(cRegistry_Key_Debug, 1, RegistryValueKind.DWord)
            Else
                regRoot.SetValue(cRegistry_Key_Debug, 0, RegistryValueKind.DWord)
            End If
        End If
    End Sub

    Public Sub CheckValues()
        If mLoop_Delay < cDEFAULT_Loop_Delay Then
            mLoop_Delay = cDEFAULT_Loop_Delay
        End If
        If mLog_UploadHistoryDays < cDEFAULT_UploadHistoryDays Then
            mLog_UploadHistoryDays = cDEFAULT_UploadHistoryDays
        End If
    End Sub
End Class
