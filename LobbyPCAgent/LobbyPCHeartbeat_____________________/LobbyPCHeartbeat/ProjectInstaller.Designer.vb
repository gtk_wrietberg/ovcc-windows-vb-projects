﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ServiceProcessInstaller_LPCHeartbeat = New System.ServiceProcess.ServiceProcessInstaller()
        Me.ServiceInstaller_LPCHeartbeat = New System.ServiceProcess.ServiceInstaller()
        '
        'ServiceProcessInstaller_LPCHeartbeat
        '
        Me.ServiceProcessInstaller_LPCHeartbeat.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.ServiceProcessInstaller_LPCHeartbeat.Password = Nothing
        Me.ServiceProcessInstaller_LPCHeartbeat.Username = Nothing
        '
        'ServiceInstaller_LPCHeartbeat
        '
        Me.ServiceInstaller_LPCHeartbeat.Description = "LobbyPC Heartbeat"
        Me.ServiceInstaller_LPCHeartbeat.DisplayName = "LobbyPCHeartbeat"
        Me.ServiceInstaller_LPCHeartbeat.ServiceName = "LobbyPCHeartbeat"
        Me.ServiceInstaller_LPCHeartbeat.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.ServiceProcessInstaller_LPCHeartbeat, Me.ServiceInstaller_LPCHeartbeat})

    End Sub
    Friend WithEvents ServiceProcessInstaller_LPCHeartbeat As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents ServiceInstaller_LPCHeartbeat As System.ServiceProcess.ServiceInstaller

End Class
