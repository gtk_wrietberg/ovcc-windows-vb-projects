Imports System.Threading

Module modMain
    Private oLogger As Logger
    Private WithEvents oProcess As ProcessRunner

    Private mValue__uninstall_app As String

    Private mTest As Boolean

    Public Sub Main()
        mTest = True

        oLogger = New Logger
        oLogger.LogFilePath = "c:\"
        oLogger.LogFileName = Application.ProductName & ".log"

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        '-----------------------------------
        Dim sArg As String

        mValue__uninstall_app = ""

        oLogger.WriteToLog("Loading command line arguments", , 0)
        If Environment.GetCommandLineArgs.Length <= 1 Then
            oLogger.WriteToLog("none found", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            Bye(1)
            Exit Sub
        End If

        For Each sArg In My.Application.CommandLineArgs
            sArg = sArg.ToLower

            If InStr(sArg, cParams__uninstall_app) > 0 Then
                mValue__uninstall_app = LettersAndDigitsOnly(sArg.Replace(cParams__uninstall_app, ""))

                oLogger.WriteToLog("Uninstall app: " & mValue__uninstall_app, , 2)
            End If

            If sArg = cParams__go_for_it Then
                mTest = False
            End If
        Next

        '-----------------------------------
        oLogger.WriteToLog("Uninstalling '" & mValue__uninstall_app & "'", , 0)
        If mValue__uninstall_app.Length < 3 Then
            oLogger.WriteToLog("Incorrect app name!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            If mTest Then
                MsgBox("App name '" & mValue__uninstall_app & "' is too short", MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Application.ProductName)
            End If

            Bye(2)
            Exit Sub
        End If


        Dim sId As String, sParams As String

        sId = ProductGUID.GetUninstallIdentifier(mValue__uninstall_app)

        If sId = "" Then
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            If mTest Then
                MsgBox("App name '" & mValue__uninstall_app & "' not found in registry", MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Application.ProductName)
            End If

            Bye(4)
            Exit Sub
        End If

        sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        If Not mTest Then
            oLogger.WriteToLog("Starting uninstaller", , 1)
            oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 2)
            oLogger.WriteToLog("Params : " & sParams, , 2)
            oLogger.WriteToLog("Timeout: 1800", , 2)

            oProcess = New ProcessRunner
            oProcess.FileName = cUNINSTALL_COMMAND
            oProcess.Arguments = sParams
            oProcess.MaxTimeout = 1800
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()

            Do
                Thread.Sleep(500)
            Loop Until oProcess.IsProcessDone

            Bye(oProcess.LastErrorCode)
        Else
            oLogger.WriteToLog("Not starting uninstaller (testing)", , 1)
            oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 2)
            oLogger.WriteToLog("Params : " & sParams, , 2)
            oLogger.WriteToLog("Timeout: 1800", , 2)

            MsgBox("I would have started the following command:" & vbCrLf & vbCrLf & cUNINSTALL_COMMAND & " " & sParams, MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, Application.ProductName)

            Bye()
        End If
    End Sub

    Private Function LettersAndDigitsOnly(ByVal sString As String) As String
        Dim str As String = "to## remove' all"" special^& characters from string"
        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In sString
            If Char.IsLetterOrDigit(ch) Then
                sb.Append(ch)
            End If
        Next

        Return sb.ToString()
    End Function


    Private Sub Bye(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("Bye (" & iExitCode.ToString & ")", , 0)
        oLogger.WriteToLog(New String("=", 50))

        Environment.ExitCode = iExitCode
        Application.Exit()
    End Sub


#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLog("done", , 2)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 3)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLog("failed", , 2)
        oLogger.WriteToLog(ErrorMessage, , 3)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_KillFailed(ByVal EventProcess As System.Diagnostics.Process, ByVal ErrorMessage As String) Handles oProcess.KillFailed
        oLogger.WriteToLog("killing process failed", , 2)
        oLogger.WriteToLog(ErrorMessage, , 3)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLog("started", , 2)
        oLogger.WriteToLog("id  : " & EventProcess.Id.ToString, , 3)
        oLogger.WriteToLog("name: " & EventProcess.ProcessName, , 3)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLog("timed out", , 2)

        oProcess.ProcessDone()
    End Sub
#End Region

End Module
