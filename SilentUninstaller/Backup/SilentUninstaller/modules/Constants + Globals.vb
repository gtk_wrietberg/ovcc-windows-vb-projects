Module Constants___Globals
    'Uninstall
    Public ReadOnly cUNINSTALL_COMMAND As String = "msiexec.exe"
    Public ReadOnly cUNINSTALL_PARAMS As String = "/X{%%APP_IDENTIFIER%%} /quiet /qn /norestart"

    'Params
    Public ReadOnly cParams__uninstall_app As String = "--uninstall-app:"
    Public ReadOnly cParams__go_for_it As String = "--go-for-it"
End Module
