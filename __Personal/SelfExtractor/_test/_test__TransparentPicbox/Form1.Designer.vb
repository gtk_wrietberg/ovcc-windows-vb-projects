﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlFolderFiles = New System.Windows.Forms.Panel()
        Me.treeFolders = New System.Windows.Forms.TreeView()
        Me.listviewFiles = New System.Windows.Forms.ListView()
        Me.colHeader_Name = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Date = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeaderHidden_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TransparentPictureBox1 = New _test__TransparentPicbox.TransparentPictureBox()
        Me.pnlFolderFiles.SuspendLayout()
        CType(Me.TransparentPictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlFolderFiles
        '
        Me.pnlFolderFiles.AllowDrop = True
        Me.pnlFolderFiles.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.pnlFolderFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlFolderFiles.Controls.Add(Me.TransparentPictureBox1)
        Me.pnlFolderFiles.Controls.Add(Me.treeFolders)
        Me.pnlFolderFiles.Controls.Add(Me.listviewFiles)
        Me.pnlFolderFiles.Location = New System.Drawing.Point(12, 135)
        Me.pnlFolderFiles.Name = "pnlFolderFiles"
        Me.pnlFolderFiles.Size = New System.Drawing.Size(1432, 379)
        Me.pnlFolderFiles.TabIndex = 10
        '
        'treeFolders
        '
        Me.treeFolders.AllowDrop = True
        Me.treeFolders.BackColor = System.Drawing.Color.White
        Me.treeFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.treeFolders.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText
        Me.treeFolders.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeFolders.ForeColor = System.Drawing.Color.Black
        Me.treeFolders.HideSelection = False
        Me.treeFolders.Indent = 20
        Me.treeFolders.ItemHeight = 20
        Me.treeFolders.Location = New System.Drawing.Point(3, 3)
        Me.treeFolders.Name = "treeFolders"
        Me.treeFolders.PathSeparator = "/"
        Me.treeFolders.ShowPlusMinus = False
        Me.treeFolders.Size = New System.Drawing.Size(473, 371)
        Me.treeFolders.TabIndex = 5
        '
        'listviewFiles
        '
        Me.listviewFiles.AllowDrop = True
        Me.listviewFiles.BackColor = System.Drawing.Color.White
        Me.listviewFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.listviewFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colHeader_Name, Me.colHeader_Date, Me.colHeader_Type, Me.colHeader_Size, Me.colHeaderHidden_Size})
        Me.listviewFiles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listviewFiles.GridLines = True
        Me.listviewFiles.Location = New System.Drawing.Point(479, 3)
        Me.listviewFiles.MultiSelect = False
        Me.listviewFiles.Name = "listviewFiles"
        Me.listviewFiles.Size = New System.Drawing.Size(948, 371)
        Me.listviewFiles.TabIndex = 6
        Me.listviewFiles.UseCompatibleStateImageBehavior = False
        Me.listviewFiles.View = System.Windows.Forms.View.Details
        '
        'colHeader_Name
        '
        Me.colHeader_Name.Text = "Name"
        Me.colHeader_Name.Width = 560
        '
        'colHeader_Date
        '
        Me.colHeader_Date.Text = "Date modified"
        Me.colHeader_Date.Width = 136
        '
        'colHeader_Type
        '
        Me.colHeader_Type.Text = "Type"
        Me.colHeader_Type.Width = 163
        '
        'colHeader_Size
        '
        Me.colHeader_Size.Text = "Size"
        Me.colHeader_Size.Width = 77
        '
        'colHeaderHidden_Size
        '
        Me.colHeaderHidden_Size.Text = ""
        Me.colHeaderHidden_Size.Width = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(41, 22)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(61, 46)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TransparentPictureBox1
        '
        Me.TransparentPictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.TransparentPictureBox1.Image = Global._test__TransparentPicbox.My.Resources.Resources.Drag_and_Drop_512
        Me.TransparentPictureBox1.Location = New System.Drawing.Point(516, 107)
        Me.TransparentPictureBox1.Name = "TransparentPictureBox1"
        Me.TransparentPictureBox1.Size = New System.Drawing.Size(291, 206)
        Me.TransparentPictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.TransparentPictureBox1.TabIndex = 1
        Me.TransparentPictureBox1.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1463, 602)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.pnlFolderFiles)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.pnlFolderFiles.ResumeLayout(False)
        CType(Me.TransparentPictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TransparentPictureBox1 As TransparentPictureBox
    Friend WithEvents pnlFolderFiles As Panel
    Friend WithEvents treeFolders As TreeView
    Friend WithEvents listviewFiles As ListView
    Friend WithEvents colHeader_Name As ColumnHeader
    Friend WithEvents colHeader_Date As ColumnHeader
    Friend WithEvents colHeader_Type As ColumnHeader
    Friend WithEvents colHeader_Size As ColumnHeader
    Friend WithEvents colHeaderHidden_Size As ColumnHeader
    Friend WithEvents Button1 As Button
End Class
