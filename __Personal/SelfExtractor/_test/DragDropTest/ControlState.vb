﻿Public Class ControlState
    Private mControls As New List(Of String)
    Private mStates As New List(Of Boolean)
    Private mIterator As Integer = -1

    Public Sub New()
        Reset()
    End Sub

    Public Sub Reset()
        mControls = New List(Of String)
        mStates = New List(Of Boolean)

        mIterator = -1
    End Sub

    Public Sub ResetIterator()
        mIterator = -1
    End Sub


    Public Sub Add(_name As String, _state As Boolean)
        mControls.Add(_name)
        mStates.Add(_state)
    End Sub

    Public Function GetNext(ByRef _name As String, ByRef _state As Boolean) As Boolean
        mIterator += 1

        If mIterator < 0 Or mIterator >= mControls.Count Then
            _name = ""
            _state = False

            Return False
        End If

        _name = mControls.Item(mIterator)
        _state = mStates.Item(mIterator)

        Return True
    End Function

End Class
