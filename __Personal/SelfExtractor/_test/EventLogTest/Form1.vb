﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim b1 As Boolean, b2 As Boolean, b3 As Boolean

        b1 = WriteToEventLog(TextBox1.Text, EventLogEntryType.Error)
        b2 = WriteToEventLog(TextBox1.Text, EventLogEntryType.Information)
        b3 = WriteToEventLog(TextBox1.Text, EventLogEntryType.Warning)

        MsgBox(b1.ToString & " - " & b2.ToString & " - " & b3.ToString)
    End Sub



    Public Function WriteToEventLog(ByVal Entry As String, eventType As EventLogEntryType) As Boolean

        Dim appName As String = "EventLogTest"
        Dim logName As String = "Application"

        Dim objEventLog As New EventLog()

        'Try

        If Not EventLog.SourceExists(appName) Then
                EventLog.CreateEventSource(appName, logName)
            End If

            objEventLog.Source = appName

            objEventLog.WriteEntry(Entry, eventType)
            Return True
        'Catch Ex As Exception
        '    MsgBox(Ex.Message)
        '    Return False

        'End Try

    End Function



    Private theSecureString As New System.Security.SecureString
    Private Sub TextBox2_LostFocus(sender As Object, e As EventArgs) Handles TextBox2.LostFocus
        Console.WriteLine("oh no, where's my focus gone")

        theSecureString = New Net.NetworkCredential("", TextBox2.Text).SecurePassword

        TextBox2.Text = New String(" ", TextBox2.Text.Length)


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim theString = New Net.NetworkCredential("", theSecureString).Password

        MsgBox(TextBox2.Text & " - " & theString)

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
