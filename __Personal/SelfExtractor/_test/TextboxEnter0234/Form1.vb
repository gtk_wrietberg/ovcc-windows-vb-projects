﻿Public Class Form1
    Private mTextBoxShake_start As Integer = 0
    Private mTextBoxShake_value As Integer = 0
    Private mTextBoxShake_max As Integer = 2
    Private mTextBoxShake_step As Integer = 2
    Private mTextBoxShake_count As Integer = 20



    Private Sub TextBox1_KeyDown(sender As Object, e As KeyEventArgs) Handles TextBox1.KeyDown
        If e.KeyData = Keys.Enter Then
            _SEFUER()
        End If
    End Sub

    Private Sub _SEFUER()
        MsgBox(TextBox1.Text & vbCrLf & TextBox1.Text.Length.ToString)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        _SEFUER()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        tmrTextboxShake.Enabled = Not tmrTextboxShake.Enabled

        If Not tmrTextboxShake.Enabled Then
            TextBox1.Left = mTextBoxShake_start
            'TextBox1.BorderStyle = BorderStyle.FixedSingle
        Else
            mTextBoxShake_count = 20
            mTextBoxShake_value = 0

            TextBox1.Select()
            TextBox1.SelectAll()
        End If
    End Sub

    Private Sub tmrTextboxShake_Tick(sender As Object, e As EventArgs) Handles tmrTextboxShake.Tick
        mTextBoxShake_value += mTextBoxShake_step
        mTextBoxShake_count -= 1

        If mTextBoxShake_count < 0 Then
            tmrTextboxShake.Enabled = False
            TextBox1.Left = mTextBoxShake_start
        Else

            If Math.Abs(mTextBoxShake_value) = mTextBoxShake_max Then
                mTextBoxShake_step = -mTextBoxShake_step
            End If

            TextBox1.Left = mTextBoxShake_start + mTextBoxShake_value
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mTextBoxShake_start = TextBox1.Left
    End Sub
End Class
