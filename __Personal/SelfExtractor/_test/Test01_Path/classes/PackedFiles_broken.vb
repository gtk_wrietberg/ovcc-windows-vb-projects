﻿Public Class PackedFiles_broken
    Public Class File
        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mRealPath As String
        Public Property RealPath() As String
            Get
                Return mRealPath
            End Get
            Set(ByVal value As String)
                mRealPath = value
            End Set
        End Property

        Private mRelativePath As String
        Public Property RelativePath() As String
            Get
                Return mRelativePath
            End Get
            Set(ByVal value As String)
                mRelativePath = value
            End Set
        End Property
    End Class

    Public Class Folder
        Private mPrevious As Folder
        Public Property PreviousFolder() As Folder
            Get
                Return mPrevious
            End Get
            Set(ByVal value As Folder)
                mPrevious = value
            End Set
        End Property

        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mFiles As New List(Of File)
        Private mFolders As New List(Of Folder)

        Public Sub AddFile(file As File)
            mFiles.Add(file)
        End Sub

        Public Property Files() As List(Of File)
            Get
                Return mFiles
            End Get
            Set(ByVal value As List(Of File))
                mFiles = value
            End Set
        End Property

        Public Sub New()
            mFiles = New List(Of File)
        End Sub
    End Class

    Private mFolders As New List(Of Folder)

    Public Sub Add(folder As Folder)
        mFolders.Add(folder)
    End Sub
End Class
