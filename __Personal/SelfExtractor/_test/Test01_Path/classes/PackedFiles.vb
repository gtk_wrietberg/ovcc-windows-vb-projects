﻿Public Class PackedFiles
    Public Shared ReadOnly c_ARCHIVE_INTERNAL_PATH_SEPARATOR As String = "/"

    Private mPaths As New List(Of String)
    Private mPathDepth As New List(Of Integer)
    Private mFiles As New List(Of List(Of IconFileInfo))

    Private mCurrentIndex As Integer = -1

    Private mIterator_Path As Integer = -1
    Private mIterator_File As Integer = -1

    Public Sub New()
        Reset()
    End Sub

    Public Sub Reset()
        mPaths = New List(Of String)
        mPathDepth = New List(Of Integer)
        mFiles = New List(Of List(Of IconFileInfo))

        mIterator_Path = -1
        mIterator_File = -1
        mCurrentIndex = -1
    End Sub

    Public Sub AddPath(path As String)
        mPaths.Add(path)
        mFiles.Add(New List(Of IconFileInfo))

        If path.Equals(c_ARCHIVE_INTERNAL_PATH_SEPARATOR) Then
            mPathDepth.Add(1)
        Else
            mPathDepth.Add(path.Split(c_ARCHIVE_INTERNAL_PATH_SEPARATOR).Length)
        End If

        mCurrentIndex = mPaths.Count - 1
    End Sub

    Public Function AddFile(filename As String) As Boolean
        Dim ifi As New IconFileInfo(filename)

        Return AddFile(ifi)
    End Function

    Public Function AddFile(ifi As IconFileInfo) As Boolean
        If Not DoesFileExist(ifi.FullName) Then
            mFiles.Item(mCurrentIndex).Add(ifi)

            Return True
        Else
            Return False
        End If
    End Function

    Public Function DoesFileExist(filename As String) As Boolean
        Return DoesFileExist(mCurrentIndex, filename)
    End Function

    Public Function DoesFileExist(pathindex As Integer, filename As String) As Boolean
        If pathindex < 0 Or pathindex >= mFiles.Count Then
            Return False
        End If

        Dim bRet As Boolean = False

        For Each fi As IconFileInfo In mFiles.Item(pathindex)
            If fi.FullName.Equals(filename) Then
                bRet = True

                Exit For
            End If
        Next

        Return bRet
    End Function

    Public Sub Sort()
        Dim swaps As Boolean = True

        While swaps = True
            swaps = False

            For i As Integer = 0 To mPaths.Count - 2
                If mPaths.Item(i) > mPaths.Item(i + 1) Then
                    Dim temp_mPaths As String = mPaths.Item(i + 1)
                    mPaths.Item(i + 1) = mPaths.Item(i)
                    mPaths.Item(i) = temp_mPaths

                    Dim temp_mFiles1 As New List(Of IconFileInfo), temp_mFiles2 As New List(Of IconFileInfo)
                    For Each item As IconFileInfo In mFiles.Item(i + 1)
                        temp_mFiles1.Add(item)
                    Next
                    For Each item As IconFileInfo In mFiles.Item(i)
                        temp_mFiles2.Add(item)
                    Next
                    mFiles.Item(i + 1).Clear()
                    mFiles.Item(i + 1).AddRange(temp_mFiles2)
                    mFiles.Item(i).Clear()
                    mFiles.Item(i).AddRange(temp_mFiles1)

                    Dim temp_mPathDepth As Integer = mPathDepth.Item(i + 1)
                    mPathDepth.Item(i + 1) = mPathDepth.Item(i)
                    mPathDepth.Item(i) = temp_mPathDepth

                    swaps = True
                End If
            Next
        End While
    End Sub

    Public Function GetPathIndex(path As String) As Integer
        'replace double c_ARCHIVE_INTERNAL_PATH_SEPARATOR
        path = path.Replace(c_ARCHIVE_INTERNAL_PATH_SEPARATOR & c_ARCHIVE_INTERNAL_PATH_SEPARATOR, c_ARCHIVE_INTERNAL_PATH_SEPARATOR)
        If path.EndsWith(c_ARCHIVE_INTERNAL_PATH_SEPARATOR) And Not path.Equals(c_ARCHIVE_INTERNAL_PATH_SEPARATOR) Then
            path = path.Substring(0, path.Length - 1)
        End If

        Console.WriteLine("searching for '" & path & "'")

        Return mPaths.IndexOf(path)
    End Function

    Public Sub ResetIterator_Path()
        mIterator_Path = -1
    End Sub

    Public Function GetNext_Path(ByRef path As String, ByRef depth As Integer) As Boolean
        mIterator_Path += 1

        If mIterator_Path < mPaths.Count Then
            mIterator_File = -1
            path = mPaths.Item(mIterator_Path)
            depth = mPathDepth.Item(mIterator_Path)

            Return True
        Else
            Return False
        End If
    End Function


    Public Sub ResetIterator_File()
        mIterator_File = -1
    End Sub

    Public Function GetNext_File(ByRef file As IconFileInfo) As Boolean
        Return GetNext_File(mCurrentIndex, file)
    End Function

    Public Function GetNext_File(ByVal pathindex As Integer, ByRef file As IconFileInfo) As Boolean
        mIterator_File += 1

        If pathindex < 0 Or pathindex >= mFiles.Count Then
            Return False
        End If

        If mIterator_File < mFiles.Item(pathindex).Count Then
            file = mFiles.Item(pathindex).Item(mIterator_File)

            Return True
        Else
            Return False
        End If
    End Function

    'Public Class PackedFile
    '    Private mFiles As New List(Of IconFileInfo)

    '    Public Sub New()
    '        mFiles = New List(Of IconFileInfo)
    '    End Sub

    '    Public Sub Add(fullname As String)
    '        Try
    '            Dim ifi As New IconFileInfo(fullname)

    '            Add(ifi)
    '        Catch ex As Exception

    '        End Try
    '    End Sub

    '    Public Sub Add(ifi As IconFileInfo)
    '        mFiles.Add(ifi)
    '    End Sub
    'End Class

    'Private Shared mPaths As New List(Of String)
    'Private Shared mFiles As New List(Of PackedFile)

    'Private Shared mCurrentPathIndex As Integer = -1

    'Private Shared mIterator_Path As Integer = -1
    'Private Shared mIterator_File As Integer = -1



    'Public Shared Sub Reset()
    '    mPaths = New List(Of String)
    '    mFiles = New List(Of PackedFile)

    '    mCurrentPathIndex = -1
    'End Sub

    'Public Shared Sub AddPath(path As String)
    '    mPaths.Add(path)
    '    mFiles.Add(Nothing)
    '    mCurrentPathIndex = mPaths.Count - 1
    'End Sub

    'Public Shared Sub AddFileToCurrentPath(fullname As String)
    '    Dim ifi As New IconFileInfo(fullname)

    '    AddFileToCurrentPath(ifi)
    'End Sub

    'Public Shared Sub AddFileToCurrentPath(ifi As IconFileInfo)
    '    If mCurrentPathIndex >= mFiles.Count Then
    '        Dim pf As New PackedFile()

    '        pf.Add(ifi)
    '        mFiles.Add(pf)
    '    Else
    '        mFiles.Item(mCurrentPathIndex).Add(ifi)

    '    End If
    'End Sub

    'Public Shared Function GetNext_Path(ByRef path As String) As Boolean
    '    mIterator_Path += 1

    '    If mIterator_Path < mPaths.Count Then
    '        mIterator_File = -1
    '        path = mPaths.Item(mIterator_Path)

    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    'Public Shared Function GetNext_File(ByRef ifi As IconFileInfo) As Boolean
    '    mIterator_File += 1

    '    If mIterator_File < mFiles.Count And mFiles.Count > 1 Then
    '        ifi = mFiles.Item(mIterator_File)
    '    End If
    'End Function










    'Private Shared mFiles_RealPath As New List(Of String)
    'Private Shared mFiles_ArchivePath As New List(Of String)

    'Private Shared mCurrentRelativePath As String = ""

    'Private Shared mCount As Integer = -1

    'Public Shared Sub Reset()
    '    mFiles_RealPath = New List(Of String)
    '    mFiles_ArchivePath = New List(Of String)
    '    mCurrentRelativePath = ""

    '    ResetCount()
    'End Sub

    'Public Shared Sub ResetCount()
    '    mCount = -1
    'End Sub

    'Public Shared Sub Add(real_path As String, archive_path As String)
    '    mFiles_RealPath.Add(real_path)
    '    mFiles_ArchivePath.Add(archive_path)
    'End Sub

    'Public Shared Sub Delete(archive_path As String)
    '    Dim index As Integer = mFiles_ArchivePath.IndexOf(archive_path)

    '    If index > -1 Then
    '        mFiles_RealPath.RemoveAt(index)
    '        mFiles_ArchivePath.RemoveAt(index)
    '    End If
    'End Sub

    'Public Shared Sub Sort()
    '    Dim swaps As Boolean = True

    '    While swaps = True
    '        swaps = False

    '        For i As Integer = 0 To mFiles_ArchivePath.Count - 2
    '            If mFiles_ArchivePath(i) > mFiles_ArchivePath(i + 1) Then
    '                Dim temp As String = mFiles_ArchivePath(i + 1)
    '                mFiles_ArchivePath(i + 1) = mFiles_ArchivePath(i)
    '                mFiles_ArchivePath(i) = temp

    '                temp = mFiles_RealPath(i + 1)
    '                mFiles_RealPath(i + 1) = mFiles_RealPath(i)
    '                mFiles_RealPath(i) = temp

    '                swaps = True
    '            End If
    '        Next
    '    End While
    'End Sub

    'Public Shared Function GetNext(ByRef real_path As String, ByRef archive_path As String) As Boolean
    '    mCount += 1

    '    If mCount >= mFiles_ArchivePath.Count Then
    '        real_path = ""
    '        archive_path = ""

    '        Return False
    '    End If

    '    real_path = mFiles_RealPath.Item(mCount)
    '    archive_path = mFiles_ArchivePath.Item(mCount)

    '    Return True
    'End Function
End Class
