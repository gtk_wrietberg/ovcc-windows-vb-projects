﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.listviewFiles = New System.Windows.Forms.ListView()
        Me.colHeader_Name = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Date = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeaderHidden_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.treeFolders = New System.Windows.Forms.TreeView()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'listviewFiles
        '
        Me.listviewFiles.BackColor = System.Drawing.Color.White
        Me.listviewFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.listviewFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colHeader_Name, Me.colHeader_Date, Me.colHeader_Type, Me.colHeader_Size, Me.colHeaderHidden_Size})
        Me.listviewFiles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listviewFiles.GridLines = True
        Me.listviewFiles.Location = New System.Drawing.Point(488, 12)
        Me.listviewFiles.MultiSelect = False
        Me.listviewFiles.Name = "listviewFiles"
        Me.listviewFiles.Size = New System.Drawing.Size(938, 371)
        Me.listviewFiles.TabIndex = 4
        Me.listviewFiles.UseCompatibleStateImageBehavior = False
        Me.listviewFiles.View = System.Windows.Forms.View.Details
        '
        'colHeader_Name
        '
        Me.colHeader_Name.Text = "Name"
        Me.colHeader_Name.Width = 560
        '
        'colHeader_Date
        '
        Me.colHeader_Date.Text = "Date modified"
        Me.colHeader_Date.Width = 136
        '
        'colHeader_Type
        '
        Me.colHeader_Type.Text = "Type"
        Me.colHeader_Type.Width = 163
        '
        'colHeader_Size
        '
        Me.colHeader_Size.Text = "Size"
        Me.colHeader_Size.Width = 77
        '
        'colHeaderHidden_Size
        '
        Me.colHeaderHidden_Size.Text = ""
        Me.colHeaderHidden_Size.Width = 0
        '
        'treeFolders
        '
        Me.treeFolders.BackColor = System.Drawing.Color.White
        Me.treeFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.treeFolders.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText
        Me.treeFolders.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeFolders.ForeColor = System.Drawing.Color.Black
        Me.treeFolders.HideSelection = False
        Me.treeFolders.Indent = 20
        Me.treeFolders.ItemHeight = 20
        Me.treeFolders.Location = New System.Drawing.Point(12, 12)
        Me.treeFolders.Name = "treeFolders"
        Me.treeFolders.ShowPlusMinus = False
        Me.treeFolders.Size = New System.Drawing.Size(470, 371)
        Me.treeFolders.TabIndex = 3
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(783, 557)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(202, 468)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(488, 468)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1438, 620)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.listviewFiles)
        Me.Controls.Add(Me.treeFolders)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents listviewFiles As ListView
    Friend WithEvents colHeader_Name As ColumnHeader
    Friend WithEvents colHeader_Date As ColumnHeader
    Friend WithEvents colHeader_Type As ColumnHeader
    Friend WithEvents colHeader_Size As ColumnHeader
    Friend WithEvents colHeaderHidden_Size As ColumnHeader
    Friend WithEvents treeFolders As TreeView
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button3 As Button
End Class
