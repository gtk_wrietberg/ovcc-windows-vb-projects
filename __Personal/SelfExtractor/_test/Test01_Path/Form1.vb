﻿Imports System.IO

Public Class Form1
    Private pf As PackedFiles

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim nodeRoot As TreeNode = New TreeNode("Archive")

        For i As Integer = 0 To 9
            nodeRoot.Nodes.Add("Path" & i.ToString, RandomString(10) & "_" & i.ToString)

        Next

        treeFolders.Nodes.Add(nodeRoot)

        treeFolders.ExpandAll()
    End Sub

#Region "drawnode"
    Private Sub treeFolders_DrawNode(ByVal sender As Object, ByVal e As DrawTreeNodeEventArgs) Handles treeFolders.DrawNode
        If (e.State And TreeNodeStates.Selected) <> 0 Then
            e.Graphics.FillRectangle(Brushes.LightGray, e.Bounds)
            TextRenderer.DrawText(e.Graphics, e.Node.Text, e.Node.NodeFont, e.Bounds, Color.Black, Color.Empty, TextFormatFlags.VerticalCenter)
        Else
            e.Graphics.FillRectangle(SystemBrushes.Window, e.Bounds)
            TextRenderer.DrawText(e.Graphics, e.Node.Text, e.Node.NodeFont, e.Bounds, SystemColors.WindowText, Color.Empty, TextFormatFlags.VerticalCenter)
        End If
    End Sub
#End Region

    Public Function RandomString(Length As Integer) As String
        Return RandomString(Length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    End Function

    Public Function RandomString(Length As Integer, CharsToChooseFrom As String) As String
        If CharsToChooseFrom = "" Then
            'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
            CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        End If
        If Length < 1 Then
            Length = 1
        End If

        Dim sTmp As String = "", iRnd As Integer

        Randomize()

        For i As Integer = 0 To Length - 1
            iRnd = Int(CharsToChooseFrom.Length * Rnd())
            sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
        Next

        Return sTmp
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click



        pf.Reset()

        pf.AddPath("root/oug")
        pf.AddFile("D:\Temp\___test__test_test\_src\1\1.txt")
        pf.AddFile("D:\Temp\___test__test_test\_src\2\2.txt")

        pf.AddPath("/folder1")
        pf.AddFile("D:\Temp\___test__test_test\_src\3\3.txt")

        pf.AddPath("/folder2/folder3")
        pf.AddFile("D:\Temp\___test__test_test\_src\Gaia_s_sky_in_colour.png")

        pf.AddPath("/folder1/folder4")
        pf.AddFile("D:\Temp\___test__test_test\_src\1\1.txt")
        pf.AddFile("D:\Temp\___test__test_test\_src\1\1.txt")
        pf.AddFile("D:\Temp\___test__test_test\_src\1\1.txt")
        pf.AddFile("D:\Temp\___test__test_test\_src\1\1.txt")
        pf.AddFile("D:\Temp\___test__test_test\_src\1\1.txt")

        pf.AddPath("/folder1/folder5")
        pf.AddFile("D:\Temp\___test__test_test\_src\2\2.txt")

        pf.AddPath("/folder2")

        pf.AddPath("/sk_downloads")
        pf.AddFile("C:\Users\sitekiosk\downloads\Golden Crop_v093beta.zip")

        pf.AddPath("/folder2/folder6")
        pf.AddFile("D:\Temp\___test__test_test\_src\3\3.txt")


        pf.Sort()

        pf.ResetIterator_Path()
        treeFolders.Nodes.Clear()


        Dim lastNode As TreeNode = Nothing
        Dim subPathAgg As String = ""

        Dim path As String = "", depth As Integer
        While pf.GetNext_Path(path, depth)
            subPathAgg = ""

            If path = PackedFiles.c_ARCHIVE_INTERNAL_PATH_SEPARATOR Then
                'skip
            Else
                For Each subPath As String In path.Split(PackedFiles.c_ARCHIVE_INTERNAL_PATH_SEPARATOR)
                    subPathAgg &= subPath & PackedFiles.c_ARCHIVE_INTERNAL_PATH_SEPARATOR

                    Dim nodes As TreeNode() = treeFolders.Nodes.Find(subPathAgg, True)
                    If nodes.Length = 0 Then
                        If lastNode Is Nothing Then
                            If subPath.Equals("") Then
                                subPath = PackedFiles.c_ARCHIVE_INTERNAL_PATH_SEPARATOR
                            End If
                            lastNode = treeFolders.Nodes.Add(subPathAgg, subPath)
                        Else
                            lastNode = lastNode.Nodes.Add(subPathAgg, subPath)
                        End If
                    Else
                        lastNode = nodes(0)
                    End If
                Next
            End If
        End While


        treeFolders.ExpandAll()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pf = New PackedFiles
    End Sub

    Private Sub treeFolders_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles treeFolders.AfterSelect
        Dim pathindex As Integer = pf.GetPathIndex(treeFolders.SelectedNode.Name)

        If pathindex < 0 Then
            'oops, not found!

            Exit Sub
        End If


        Dim imageListFiles As New ImageList()

        imageListFiles.ColorDepth = ColorDepth.Depth32Bit
        imageListFiles.ImageSize = New Size(16, 16)


        listviewFiles.Items.Clear()
        listviewFiles.SmallImageList = imageListFiles


        Dim file As New IconFileInfo

        pf.ResetIterator_File()


        While pf.GetNext_File(pathindex, file)
            imageListFiles.Images.Add(file.FullName, file.Icon)

            Dim _listviewitem As New ListViewItem(New String() {file.Name, file.LastWriteTime.ToString, "bla", file.Size, file.Size})

            _listviewitem.ImageIndex = imageListFiles.Images.Count - 1

            Console.WriteLine("imagekey=" & _listviewitem.ImageKey)

            listviewFiles.Items.Add(_listviewitem)
        End While


    End Sub

    Private Sub treeFolders_BeforeCollapse(sender As Object, e As TreeViewCancelEventArgs) Handles treeFolders.BeforeCollapse
        e.Cancel = True
    End Sub


    'Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
    '    Dim p As New PackedFiles



    '    IO.Directory.GetDirectories()
    'End Sub

    'Private Sub IterateFolders(sPath As String)
    '    Dim d As String
    '    Dim f As String

    '    Try
    '        For Each d In Directory.GetDirectories(sPath)
    '            For Each f In Directory.GetFiles(d)
    '                lstFilesFound.Items.Add(f)
    '            Next

    '            IterateFolders(d)
    '        Next
    '    Catch excpt As System.Exception
    '        Debug.WriteLine(excpt.Message)
    '    End Try
    'End Sub

    'Private Sub IterateFolder(sPath As String, sPreviuosPath As String)

    'End Sub

    Private Sub treeFolders_MouseEnter(sender As Object, e As EventArgs) Handles treeFolders.MouseEnter
        treeFolders.Focus()
    End Sub

    Private Sub listviewFiles_MouseEnter(sender As Object, e As EventArgs) Handles listviewFiles.MouseEnter
        listviewFiles.Focus()
    End Sub

    Private Sub listviewFiles_ColumnWidthChanging(sender As Object, e As ColumnWidthChangingEventArgs) Handles listviewFiles.ColumnWidthChanging
        e.Cancel = True
        e.NewWidth = Me.listviewFiles.Columns(e.ColumnIndex).Width
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim _path As String = "/poep/drol/schijt"

        _path = _path.Substring(1, _path.Length - 1)

        MsgBox(_path)
    End Sub
End Class
