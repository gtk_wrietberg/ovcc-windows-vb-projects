﻿Imports System.IO
Imports System.Reflection
Imports System.Xml

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click




        'FileList.Reset()
        'Settings.Reset()



        Settings.ArchiveAutoExecParams = "POEPZOOI"

        FileList.Add("Poep", "Hoer", "Kak")
        FileList.Add("Sloerie", "Prostituee", "Schijt")


        Dim xmlDoc As New XmlDocument
        Dim xmlNodeRoot As XmlNode, xmlNodeSettings As XmlNode, xmlNodeFilelist As XmlNode, xmlNodeFilelistFile As XmlNode, xmlNodeNew As XmlNode

        xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", ""))

        xmlNodeRoot = xmlDoc.CreateElement("SelfExtractor")


        xmlNodeSettings = xmlDoc.CreateElement("Settings")

        Dim type_Settings As Type = GetType(Settings)
        For Each member As MemberInfo In type_Settings.GetMembers
            If member.MemberType = MemberTypes.Property Then
                xmlNodeNew = xmlDoc.CreateElement(member.Name)

                Try
                    xmlNodeNew.InnerText = DirectCast(member, PropertyInfo).GetValue(member.Name).ToString
                Catch ex As Exception
                    xmlNodeNew.InnerText = ""
                End Try

                xmlNodeSettings.AppendChild(xmlNodeNew)
            End If
        Next

        xmlNodeRoot.AppendChild(xmlNodeSettings)



        xmlNodeFilelist = xmlDoc.CreateElement("FileList")

        FileList.ResetFileCounter()


        Dim type_FileList As Type = GetType(FileList)
        For Each member As MemberInfo In type_FileList.GetMembers
            If member.MemberType = MemberTypes.Property Then
                xmlNodeNew = xmlDoc.CreateElement(member.Name)
                xmlNodeNew.InnerText = DirectCast(member, PropertyInfo).GetValue(member.Name).ToString

                xmlNodeFilelist.AppendChild(xmlNodeNew)
            End If
        Next


        Dim _file As String = "", _path As String = "", _destination As String = "", _crc As String = ""
        While FileList.GetNext(_file, _path, _destination, _crc)
            xmlNodeFilelistFile = xmlDoc.CreateElement("file")

            xmlNodeNew = xmlDoc.CreateElement("file")
            xmlNodeNew.InnerText = _file
            xmlNodeFilelistFile.AppendChild(xmlNodeNew)

            xmlNodeNew = xmlDoc.CreateElement("path")
            xmlNodeNew.InnerText = _path
            xmlNodeFilelistFile.AppendChild(xmlNodeNew)

            xmlNodeNew = xmlDoc.CreateElement("destination")
            xmlNodeNew.InnerText = _destination
            xmlNodeFilelistFile.AppendChild(xmlNodeNew)

            xmlNodeNew = xmlDoc.CreateElement("crc")
            xmlNodeNew.InnerText = _crc
            xmlNodeFilelistFile.AppendChild(xmlNodeNew)


            xmlNodeFilelist.AppendChild(xmlNodeFilelistFile)
        End While


        xmlNodeRoot.AppendChild(xmlNodeFilelist)


        xmlDoc.AppendChild(xmlNodeRoot)





        TextBox1.Text = PrettyXML(xmlDoc.InnerXml)

        TextBox2.Text = XOrObfuscation.v2.Obfuscate(xmlDoc.InnerXml)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Function PrettyXML(XMLString As String) As String
        Dim sw As New StringWriter()
        Dim xw As New XmlTextWriter(sw)

        xw.Formatting = Formatting.Indented
        xw.Indentation = 4

        Dim doc As New XmlDocument
        doc.LoadXml(XMLString)
        doc.Save(xw)

        Return sw.ToString()
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox1.Clear()

        MsgBox("ready?")

        TextBox1.Text = XOrObfuscation.v2.Deobfuscate(TextBox2.Text)
    End Sub
End Class
