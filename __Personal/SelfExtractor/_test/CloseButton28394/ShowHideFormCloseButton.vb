﻿Imports System.Runtime.InteropServices

Public Class ShowHideFormCloseButton
    '    [DllImport("user32.dll")]
    'Static extern bool DeleteMenu(IntPtr hMenu, uint uPosition, uint uFlags);

    Private Shared ReadOnly SC_SIZE As UInteger = &HF000
    Private Shared ReadOnly SC_MOVE As UInteger = &HF010
    Private Shared ReadOnly SC_MINIMIZE As UInteger = &HF020
    Private Shared ReadOnly SC_MAXIMIZE As UInteger = &HF030
    Private Shared ReadOnly SC_NEXTWINDOW As UInteger = &HF040
    Private Shared ReadOnly SC_PREVWINDOW As UInteger = &HF050
    Private Shared ReadOnly SC_CLOSE As UInteger = &HF060
    Private Shared ReadOnly SC_VSCROLl As UInteger = &HF070
    Private Shared ReadOnly SC_HSCROLL As UInteger = &HF080
    Private Shared ReadOnly SC_MOUSEMENU As UInteger = &HF090
    Private Shared ReadOnly SC_KEYMENU As UInteger = &HF100
    Private Shared ReadOnly SC_ARRANGE As UInteger = &HF110
    Private Shared ReadOnly SC_RESTORE As UInteger = &HF120
    Private Shared ReadOnly SC_TASKLIST As UInteger = &HF130
    Private Shared ReadOnly SC_SCREENSAVE As UInteger = &HF140
    Private Shared ReadOnly SC_HOTKEY As UInteger = &HF150
    Private Shared ReadOnly SC_DEFAULT As UInteger = &HF160
    Private Shared ReadOnly SC_MONITORPOWER As UInteger = &HF170
    Private Shared ReadOnly SC_CONTEXTHELP As UInteger = &HF180
    Private Shared ReadOnly SC_SEPARATOR As UInteger = &HF00F

    Private Shared ReadOnly MF_BYPOSITION As UInteger = &H400
    Private Shared ReadOnly MF_REMOVE As UInteger = &H1000


    <Flags>
    Enum MIIM
        BITMAP = &H80
        CHECKMARKS = &H8
        DATA = &H20
        FTYPE = &H100
        ID = &H2
        STATE = &H1
        [STRING] = &H40
        SUBMENU = &H4
        TYPE = &H10
    End Enum

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
    Public Class MENUITEMINFO
        Public cbSize As Int32 = Marshal.SizeOf(GetType(MENUITEMINFO))
        Public fMask As MIIM
        Public fType As UInt32
        Public fState As UInt32
        Public wID As UInt32
        Public hSubMenu As IntPtr
        Public hbmpChecked As IntPtr
        Public hbmpUnchecked As IntPtr
        Public dwItemData As IntPtr
        Public dwTypeData As String = Nothing
        Public cch As UInt32
        Public hbmpItem As IntPtr
    End Class



    <DllImport("user32.dll", SetLastError:=True)>
    Private Shared Function GetSystemMenu(ByVal hWnd As IntPtr, ByVal bRevert As Boolean) As IntPtr
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Private Shared Function GetMenuItemCount(ByVal hMenu As IntPtr) As Integer
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Private Shared Function RemoveMenu(ByVal hMenu As IntPtr, ByVal uPosition As UInteger, uFlags As UInteger) As IntPtr
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Private Shared Function GetMenuItemInfo(ByVal hMenu As IntPtr, ByVal uItem As UInteger, ByVal fByPosition As Boolean, ByRef lpmii As MENUITEMINFO) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("user32.dll", SetLastError:=True)>
    Private Shared Function SetMenuItemInfo(ByVal hMenu As IntPtr, ByVal uItem As UInteger, ByVal fByPosition As Boolean, ByRef lpmii As MENUITEMINFO) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Friend Shared Function InsertMenuItem(ByVal hMenu As IntPtr, ByVal uItem As UInteger, ByVal fByPosition As Boolean, ByRef lpmii As MENUITEMINFO) As Boolean
    End Function


    Private Shared _current_state_stored As Boolean = False
    Private Shared _current_MENUITEMINFO As New MENUITEMINFO

    Private Shared Function _StoreCurrentState(hMenu As IntPtr) As Boolean
        Dim menuItems As Integer = GetMenuItemCount(hMenu)

        If menuItems > 0 Then
            _current_MENUITEMINFO = New MENUITEMINFO
            _current_MENUITEMINFO.dwTypeData = New String(vbNullChar, 80)
            _current_MENUITEMINFO.cch = _current_MENUITEMINFO.dwTypeData.Length
            _current_MENUITEMINFO.fMask = MIIM.STATE

            If GetMenuItemInfo(hMenu, CUInt(menuItems - 1), True, _current_MENUITEMINFO) Then
                _current_state_stored = True
            End If

            Return _current_state_stored
        Else
            Return False
        End If
    End Function

    Private Shared Function _ResetCurrentState(hMenu As IntPtr) As Boolean
        If Not _current_state_stored Then
            Return False
        End If

        Dim menuItems As Integer = GetMenuItemCount(hMenu)

        If menuItems > 0 Then
            Return InsertMenuItem(hMenu, CUInt(menuItems - 1), True, _current_MENUITEMINFO)
        Else
            Return False
        End If
    End Function


    Public Shared Function ToggleCloseButton(hWnd As IntPtr, _enabled As Boolean) As Boolean
        Dim hMenu As IntPtr

        hMenu = GetSystemMenu(hWnd, False)

        If hMenu = IntPtr.Zero Then
            Return False
        End If

        Dim menuItems As Integer = GetMenuItemCount(hMenu)

        If menuItems > 0 Then
            If Not _current_state_stored Then
                _StoreCurrentState(hMenu)
            End If

            If _enabled Then
                RemoveMenu(hMenu, CUInt(menuItems - 1), MF_BYPOSITION)
            Else
                _ResetCurrentState(hMenu)

                'RemoveMenu(hMenu, CUInt(menuItems - 1), MF_BYPOSITION Or MF_REMOVE)
            End If
            'RemoveMenu(hMenu, CUInt(menuItems - 1), MF_BYPOSITION Or MF_REMOVE)

            Return True
        Else
            Return False
        End If
    End Function


    ''Private Const SC_CLOSE As Long = &HF060&
    ''Private Const MIIM_STATE As Long = &H1&
    ''Private Const MIIM_ID As Long = &H2&
    ''Private Const MFS_GRAYED As Long = &H3&
    ''Private Const WM_NCACTIVATE As Long = &H86

    ''Private Shared ReadOnly c_CLOSE_BUTTON_ENABLED As Long = -10
    'Private Shared ReadOnly c_CLOSE_BUTTON_ENABLED As UInt32 = &H0&
    'Private Shared ReadOnly c_CLOSE_BUTTON_DISABLED As UInt32 = &HF060&
    'Private Shared ReadOnly c_CLOSE_BUTTON_GRAYED_OUT As Long = &H3&
    'Private Shared ReadOnly c_WM_NCACTIVATE As Long = &H86

    '<Flags>
    'Enum MIIM
    '    BITMAP = &H80
    '    CHECKMARKS = &H8
    '    DATA = &H20
    '    FTYPE = &H100
    '    ID = &H2
    '    STATE = &H1
    '    [STRING] = &H40
    '    SUBMENU = &H4
    '    TYPE = &H10
    'End Enum

    '<StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
    'Public Class MENUITEMINFO
    '    Public cbSize As Int32 = Marshal.SizeOf(GetType(MENUITEMINFO))
    '    Public fMask As MIIM
    '    Public fType As UInt32
    '    Public fState As UInt32
    '    Public wID As UInt32
    '    Public hSubMenu As IntPtr
    '    Public hbmpChecked As IntPtr
    '    Public hbmpUnchecked As IntPtr
    '    Public dwItemData As IntPtr
    '    Public dwTypeData As String = Nothing
    '    Public cch As UInt32
    '    Public hbmpItem As IntPtr
    'End Class

    '<DllImport("user32.dll", SetLastError:=True)>
    'Private Shared Function GetSystemMenu(ByVal hWnd As IntPtr, ByVal bRevert As Boolean) As IntPtr
    'End Function

    '<DllImport("user32.dll", SetLastError:=True)>
    'Private Shared Function GetMenuItemInfo(ByVal hMenu As IntPtr, ByVal uItem As UInt32, ByVal fByPosition As Boolean, ByRef lpmii As MENUITEMINFO) As <MarshalAs(UnmanagedType.Bool)> Boolean
    'End Function

    '<DllImport("user32.dll", CharSet:=CharSet.Auto)>
    'Public  Shared Function SetMenuItemInfo(ByVal hMenu As IntPtr, ByVal uItem As UInt32, ByVal fByPosition As Boolean, ByRef lpmii As MENUITEMINFO) As <MarshalAs(UnmanagedType.Bool)> Boolean
    'End Function

    '<DllImport("user32.dll", CharSet:=CharSet.Auto)>
    'Public Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As IntPtr) As IntPtr
    'End Function

    '<DllImport("user32.dll", SetLastError:=True)>
    'Private Shared Function IsWindowVisible(ByVal hWnd As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    'End Function

    ''*******************************************************************************
    '' Enables / Disables the close button on the titlebar and in the system menu
    '' of the form window passed.
    ''-------------------------------------------------------------------------------
    '' Return Values:
    ''
    ''    0  Close button state changed succesfully / nothing to do.
    ''   -1  Invalid Window Handle (hWnd argument) Passed to the function
    ''   -2  Failed to switch command ID of Close menu item in system menu
    ''   -3  Failed to switch enabled state of Close menu item in system menu
    ''
    ''-------------------------------------------------------------------------------
    '' Parameters:
    ''
    ''   hWnd    The window handle of the form whose close button is to be enabled/
    ''           disabled / greyed out.
    ''
    ''   Enable  True if the close button is to be enabled, or False if it is to
    ''           be disabled / greyed out.
    ''

    'Public Shared Function EnableCloseButton(ByVal hWnd As IntPtr, Enable As Boolean) As Integer
    '    ' Check that the window handle passed is valid

    '    If IsWindowVisible(hWnd) = 0 Then
    '        Return -1
    '    End If

    '    ' Retrieve a handle to the window's system menu

    '    Dim hMenu As IntPtr
    '    hMenu = GetSystemMenu(hWnd, False)

    '    Console.WriteLine("hMenu=" & hMenu.ToInt32.ToString)

    '    ' Retrieve the menu item information for the close menu item/button

    '    Dim MII As New MENUITEMINFO
    '    'MII.cbSize = Marshal.SizeOf(MII)
    '    MII.dwTypeData = New String(vbNullChar, 80)
    '    MII.cch = Len(MII.dwTypeData)
    '    MII.fMask = MIIM.STATE

    '    If Enable Then
    '        MII.wID = c_CLOSE_BUTTON_ENABLED
    '    Else
    '        MII.wID = c_CLOSE_BUTTON_DISABLED
    '    End If


    '    If Not GetMenuItemInfo(hMenu, MII.wID, False, MII) Then
    '        'Return -2
    '    End If

    '    ' Switch the ID of the menu item so that VB can not undo the action itself

    '    Dim lngMenuID As UInt32
    '    lngMenuID = MII.wID

    '    If Enable Then
    '        MII.wID = c_CLOSE_BUTTON_ENABLED
    '    Else
    '        MII.wID = c_CLOSE_BUTTON_DISABLED
    '    End If

    '    MII.fMask = MIIM.ID

    '    If Not SetMenuItemInfo(hMenu, lngMenuID, False, MII) Then
    '        'Return -3
    '    End If

    '    ' Set the enabled / disabled state of the menu item

    '    If Enable Then
    '        MII.fState = (MII.fState Or c_CLOSE_BUTTON_GRAYED_OUT)
    '        MII.fState = MII.fState - c_CLOSE_BUTTON_GRAYED_OUT
    '    Else
    '        MII.fState = (MII.fState Or c_CLOSE_BUTTON_GRAYED_OUT)
    '    End If

    '    MII.fMask = MIIM.STATE

    '    If Not SetMenuItemInfo(hMenu, MII.wID, False, MII) Then
    '        'Return -4
    '    End If

    '    ' Activate the non-client area of the window to update the titlebar, and
    '    ' draw the close button in its new state.

    '    SendMessage(hWnd, c_WM_NCACTIVATE, True, 0)

    '    Return 0
    'End Function
End Class
