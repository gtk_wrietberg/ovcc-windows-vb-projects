﻿Imports System.Runtime.InteropServices

Public Class FormManipulate
    Public NotInheritable Class SystemMenuManager

#Region " Types "
        Public Enum MenuItemState As UInteger
            Enabled = MF_ENABLED
            Disabled = MF_DISABLED
            Greyed = MF_GRAYED
            Removed
        End Enum

        Public Enum MenuItems As Integer
            Close = 0
            Move = 1
            Size = 2
        End Enum

        <Flags>
        Enum MIIM
            BITMAP = &H80
            CHECKMARKS = &H8
            DATA = &H20
            FTYPE = &H100
            ID = &H2
            STATE = &H1
            [STRING] = &H40
            SUBMENU = &H4
            TYPE = &H10
        End Enum

        <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
        Public Class MENUITEMINFO
            Public cbSize As Int32 = Marshal.SizeOf(GetType(MENUITEMINFO))
            Public fMask As MIIM
            Public fType As UInt32
            Public fState As UInt32
            Public wID As UInt32
            Public hSubMenu As IntPtr
            Public hbmpChecked As IntPtr
            Public hbmpUnchecked As IntPtr
            Public dwItemData As IntPtr
            Public dwTypeData As String = Nothing
            Public cch As UInt32
            Public hbmpItem As IntPtr
        End Class

#End Region

#Region " Constants "
        Private Const SC_SIZE As UInteger = &HF000   'The Size menu item.
        Private Const SC_MOVE As UInteger = &HF010   'The Move menu item.
        Private Const SC_CLOSE As UInteger = &HF060  'The Close menu item.

        Private Const MF_BYCOMMAND As UInteger = &H0 'The menu item is specified by command.

        Private Const MF_ENABLED As UInteger = &H0   'Enable the menu item.
        Private Const MF_DISABLED As UInteger = &H2  'Disable the menu item.
        Private Const MF_GRAYED As UInteger = &H1    'Grey out the menu item.
#End Region 'Constants

#Region "Variables"
        Private Shared m_States As New List(Of MenuItemState)
        Private Shared m_States_init As Boolean = False

        Private Shared m_MenuHandle As IntPtr = IntPtr.Zero
#End Region 'Variables

#Region " APIs "
        <DllImport("user32.dll", SetLastError:=True)>
        Private Shared Function GetSystemMenu(ByVal hWnd As IntPtr, ByVal bRevert As Boolean) As IntPtr
        End Function

        <DllImport("user32.dll", SetLastError:=True)>
        Private Shared Function EnableMenuItem(ByVal hMenu As IntPtr, ByVal wIDEnableItem As UInteger, ByVal wEnable As UInteger) As IntPtr
        End Function

        <DllImport("user32.dll", SetLastError:=True)>
        Private Shared Function DeleteMenu(ByVal hMenu As IntPtr, ByVal uPosition As UInteger, uFlags As UInteger) As Boolean
        End Function

        <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
        Private Shared Function InsertMenuItem(ByVal hMenu As IntPtr, ByVal uItem As Integer, ByVal fByPosition As Boolean, ByRef lpmii As MENUITEMINFO) As Boolean
        End Function

#End Region 'APIs

#Region " Methods "
        'Public Shared Function ChangeMenu(ByVal hFrmHandle As IntPtr, menuitem_Move As Boolean, menuitem_Close As Boolean, ByVal _enabled As Boolean) As Boolean
        '    m_MenuHandle = GetSystemMenu(hFrmHandle, False)

        '    If m_MenuHandle = IntPtr.Zero Then
        '        Return False
        '    End If


        '    If _enabled Then
        '        m_CloseState = MenuItemState.Enabled

        '        If menuitem_Move Then
        '            EnableMenuItem(m_MenuHandle, SC_MOVE, MF_DISABLED Or MF_GRAYED)
        '        End If

        '        If menuitem_Close Then
        '            EnableMenuItem(m_MenuHandle, SC_CLOSE, MF_DISABLED Or MF_GRAYED)
        '        End If
        '    Else
        '        If menuitem_Close Then
        '            EnableMenuItem(m_MenuHandle, SC_CLOSE, MF_ENABLED)
        '        End If

        '    End If

        '    'RefreshCloseItem()
        '    'If Me.m_CloseState <> MenuItemState.Enabled Then
        '    '    'Set the Keypreview to True so that the Alt+F4 key combination can be detected.
        '    '    Me.m_Form.KeyPreview = True
        '    'End If
        '    Return True
        'End Function


        Public Shared Function ToggleAll(hFrmHandle As IntPtr, _state As Boolean) As Boolean
            Dim bRet As Boolean = False

            bRet = bRet Or ToggleClose(hFrmHandle, _state)
            bRet = bRet Or ToggleMove(hFrmHandle, _state)
            bRet = bRet Or ToggleSize(hFrmHandle, _state)

            Return bRet
        End Function

        Public Shared Function RefreshAll(hFrmHandle As IntPtr) As Boolean
            Dim bRet As Boolean = False

            bRet = bRet Or RefreshClose(hFrmHandle)
            bRet = bRet Or RefreshMove(hFrmHandle)
            bRet = bRet Or RefreshSize(hFrmHandle)

            Return bRet
        End Function


        Public Shared Function ToggleClose(hFrmHandle As IntPtr, _state As Boolean) As Boolean
            If _state Then
                Return EnableClose(hFrmHandle)
            Else
                Return DisableClose(hFrmHandle)
            End If
        End Function

        Public Shared Function EnableClose(hFrmHandle As IntPtr) As Boolean
            Return _ChangeMenuItem(hFrmHandle, MenuItems.Close, True)
        End Function

        Public Shared Function DisableClose(hFrmHandle As IntPtr) As Boolean
            Return _ChangeMenuItem(hFrmHandle, MenuItems.Close, False)
        End Function

        Public Shared Function RefreshClose(hFrmHandle As IntPtr) As Boolean
            Return _RefreshMenuItem(hFrmHandle, MenuItems.Close)
        End Function


        Public Shared Function ToggleMove(hFrmHandle As IntPtr, _state As Boolean) As Boolean
            If _state Then
                Return EnableMove(hFrmHandle)
            Else
                Return DisableMove(hFrmHandle)
            End If
        End Function

        Public Shared Function EnableMove(hFrmHandle As IntPtr) As Boolean
            Return _ChangeMenuItem(hFrmHandle, MenuItems.Move, True)
        End Function

        Public Shared Function DisableMove(hFrmHandle As IntPtr) As Boolean
            Return _ChangeMenuItem(hFrmHandle, MenuItems.Move, False)
        End Function

        Public Shared Function RefreshMove(hFrmHandle As IntPtr) As Boolean
            Return _RefreshMenuItem(hFrmHandle, MenuItems.Move)
        End Function


        Public Shared Function ToggleSize(hFrmHandle As IntPtr, _state As Boolean) As Boolean
            If _state Then
                Return EnableSize(hFrmHandle)
            Else
                Return DisableSize(hFrmHandle)
            End If
        End Function

        Public Shared Function EnableSize(hFrmHandle As IntPtr) As Boolean
            Return _ChangeMenuItem(hFrmHandle, MenuItems.Size, True)
        End Function

        Public Shared Function DisableSize(hFrmHandle As IntPtr) As Boolean
            Return _ChangeMenuItem(hFrmHandle, MenuItems.Size, False)
        End Function

        Public Shared Function RefreshSize(hFrmHandle As IntPtr) As Boolean
            Return _RefreshMenuItem(hFrmHandle, MenuItems.Size)
        End Function



        Private Shared Function _ChangeMenuItem(hFrmHandle As IntPtr, _item As MenuItems, _enabled As Boolean) As Boolean
            If _item < 0 Or _item > 2 Then
                Return False
            End If

            _init(hFrmHandle)


            Dim _menu_item As UInteger

            Select Case _item
                Case MenuItems.Close
                    _menu_item = SC_CLOSE
                Case MenuItems.Move
                    _menu_item = SC_MOVE
                Case MenuItems.Size
                    _menu_item = SC_SIZE
                Case Else
                    Return False
            End Select




            If m_MenuHandle = IntPtr.Zero Then 'oops
                Return False
            End If


            If Not _enabled Then
                m_States.Item(_item) = MenuItemState.Disabled

                EnableMenuItem(m_MenuHandle, _menu_item, MF_BYCOMMAND Or MF_DISABLED Or MF_GRAYED)
            Else
                m_States.Item(_item) = MenuItemState.Enabled

                EnableMenuItem(m_MenuHandle, _menu_item, MF_BYCOMMAND Or MF_ENABLED)
            End If


            Return True
        End Function

        Private Shared Function _RefreshMenuItem(hFrmHandle As IntPtr, _item As MenuItems) As Boolean
            If _item < 0 Or _item > 2 Then
                Return False
            End If

            _init(hFrmHandle)


            Dim _menu_item As UInteger

            Select Case _item
                Case MenuItems.Close
                    _menu_item = SC_CLOSE
                Case MenuItems.Move
                    _menu_item = SC_MOVE
                Case MenuItems.Size
                    _menu_item = SC_SIZE
                Case Else
                    Return False
            End Select


            If m_MenuHandle = IntPtr.Zero Then 'oops
                Return False
            End If

            EnableMenuItem(m_MenuHandle, _menu_item, m_States.Item(_item))
            'If m_States.Item(_item) = MenuItemState.Disabled OrElse m_States.Item(_item) = MenuItemState.Greyed Then
            '    EnableMenuItem(m_MenuHandle, _menu_item, m_States.Item(_item))
            'End If

            Return True
        End Function

        Private Shared Sub _init(hFrmHandle As IntPtr)
            If Not m_States_init Then
                m_States = New List(Of MenuItemState)

                m_States.Add(MenuItemState.Enabled)
                m_States.Add(MenuItemState.Enabled)
                m_States.Add(MenuItemState.Enabled)

                m_States_init = True
            End If

            If m_MenuHandle = IntPtr.Zero Then
                m_MenuHandle = GetSystemMenu(hFrmHandle, False)
            End If
        End Sub

#End Region 'Methods

        '#Region " Event Handlers "

        '        Private Sub managedForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles m_Form.Load
        '            'The Close menu item must have it's state refresehed if it is present and not enabled.
        '            Me.RefreshCloseItem()
        '        End Sub

        '        Private Sub managedForm_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles m_Form.Resize
        '            'The Close menu item must have it's state refresehed if it is present and not enabled.
        '            Me.RefreshCloseItem()
        '        End Sub

        '        Private Sub m_Form_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles m_Form.KeyDown
        '            If e.KeyCode = Keys.F4 AndAlso
        '           e.Alt AndAlso
        '           Me.m_CloseState <> MenuItemState.Enabled Then
        '                'Disable the Alt+F4 key combination.
        '                e.Handled = True
        '            End If
        '        End Sub

        '#End Region 'Event Handlers

    End Class

End Class
