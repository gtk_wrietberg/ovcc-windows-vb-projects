﻿Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.IO.Compression
Imports Microsoft.CSharp
Imports System.CodeDom.Compiler
Imports System.Diagnostics
Imports System.Reflection
Imports System.Windows.Forms
Imports System.Resources

'Namespace ArchiveCompiler
Class SelfExtractorCompiler
    Implements IDisposable

    'Protected ReadOnly FilesToCompile As String() = {
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "My Project\AssemblyInfo.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\Shared_Constants.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\StreamString.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\Settings.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\FileList.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\CRC32.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\XOrObfuscation.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\ExitCode.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\ExtractFiles.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\Logger.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\frmUnpack.Designer.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\frmUnpack.vb"),
    '        Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\modMain.vb")
    '    }

    Protected FilesToCompile As List(Of String)


    'Protected ReadOnly sourceName As String = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "modMain_external.vb")
    Protected filenames As List(Of String) = New List(Of String)()
    Protected tempZipFile As String = ""

    Public Sub CreateTempZipFileName()
        tempZipFile = IO.Path.GetTempPath() & Guid.NewGuid().ToString & ".zip"

        Settings.InternalZipFileName = IO.Path.GetFileName(tempZipFile)
    End Sub

    Private Sub _CreateTempEmbeddedResourcesFiles()
        FilesToCompile = New List(Of String)


        Dim tempPath As String = IO.Path.GetTempPath() & Guid.NewGuid().ToString

        Dim rs As ResourceSet
        Dim dictEntry As DictionaryEntry

        rs = My.Resources.ResourceManager.GetResourceSet(System.Globalization.CultureInfo.CurrentCulture, True, True)
        For Each dictEntry In rs
            Dim resName As String = dictEntry.Key

            If resName.StartsWith("COMPILE_") Then
                Dim file As System.IO.StreamWriter
                Dim tempFile As String = tempPath & resName & ".vb"

                file = My.Computer.FileSystem.OpenTextFileWriter(tempFile, False, Text.Encoding.UTF8)
                file.Write(My.Resources.ResourceManager.GetString(resName))
                file.Close()
                file.Dispose()

                FilesToCompile.Add(tempFile)
            End If
        Next
    End Sub

    Public Sub AddIdentifierFile()
        Using file As Stream = StreamString.StringToStream(Identifier.Dump)
            Dim buffer As Byte() = New Byte(file.Length - 1) {}
            If file.Length <> file.Read(buffer, 0, buffer.Length) Then Throw New IOException("Unable to read filelist")

            Dim fi_gz As New IO.FileInfo(Shared_Constants.IdentifierFileName)
            Using gzFile As Stream = fi_gz.Create()
                'Using gzip As Stream = New GZipStream(gzFile, CompressionMode.Compress)
                Using gzip As Stream = New GZipStream(gzFile, CompressionLevel.NoCompression)
                    gzip.Write(buffer, 0, buffer.Length)
                End Using
            End Using
        End Using

        filenames.Add(Shared_Constants.IdentifierFileName)
    End Sub

    Public Sub AddSettingsFile()
        Using file As Stream = StreamString.StringToStream(Settings.Dump)
            Dim buffer As Byte() = New Byte(file.Length - 1) {}
            If file.Length <> file.Read(buffer, 0, buffer.Length) Then Throw New IOException("Unable to read settings")

            Dim fi_gz As New IO.FileInfo(Shared_Constants.SettingsFileName)
            Using gzFile As Stream = fi_gz.Create()
                'Using gzip As Stream = New GZipStream(gzFile, CompressionMode.Compress)
                Using gzip As Stream = New GZipStream(gzFile, CompressionLevel.NoCompression)
                    gzip.Write(buffer, 0, buffer.Length)
                End Using
            End Using
        End Using

        filenames.Add(Shared_Constants.SettingsFileName)
    End Sub

    Public Sub AddFileListFile()
        Using file As Stream = StreamString.StringToStream(FileList.Dump)
            Dim buffer As Byte() = New Byte(file.Length - 1) {}
            If file.Length <> file.Read(buffer, 0, buffer.Length) Then Throw New IOException("Unable to read filelist")

            Dim fi_gz As New IO.FileInfo(Shared_Constants.FileListFileName)
            Using gzFile As Stream = fi_gz.Create()
                'Using gzip As Stream = New GZipStream(gzFile, CompressionMode.Compress)
                Using gzip As Stream = New GZipStream(gzFile, CompressionLevel.NoCompression)
                    gzip.Write(buffer, 0, buffer.Length)
                End Using
            End Using
        End Using

        filenames.Add(Shared_Constants.FileListFileName)
    End Sub

    Public Function AddFile(ByVal filepath_local As String, ByVal filename As String, ByVal path As String) As Boolean
        Try
            If Not IO.File.Exists(filepath_local) Then
                Throw New IOException("'" & filepath_local & "' not found!")
            End If


            'Dim fi As New IO.FileInfo(filename)
            Dim crc As String = "(null)"


            If Settings.CheckCRC32 Then
                'crc = CRC32.fromFile(filepath_local)
                crc = Crypto.SHA._256.fromFile(filepath_local)
            End If
            FileList.Add(filepath_local, path, filename, crc)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub CompileArchive(ByVal archiveFilename As String)
        CompileArchive(archiveFilename, Nothing)
    End Sub

    Public Sub CompileArchive(ByVal archiveFilename As String, ByVal iconFilename As String)
        Dim vbCP As New VBCodeProvider
        Dim cp As New CompilerParameters

        _CreateTempZip()


        cp.GenerateExecutable = True
        cp.OutputAssembly = archiveFilename
        cp.CompilerOptions = "/target:winexe"


        If Settings.ShowUI Then
            cp.CompilerOptions &= " /define:REPORT_EXTRACTING_PROGRESS"
        End If

        If Not String.IsNullOrEmpty(iconFilename) Then
            If IO.File.Exists(iconFilename) Then
                cp.CompilerOptions &= " /win32icon:""" & iconFilename & """"
            End If
        End If


        cp.ReferencedAssemblies.Add("System.dll")
        cp.ReferencedAssemblies.Add("System.Collections.dll")
        cp.ReferencedAssemblies.Add("System.Core.dll")
        cp.ReferencedAssemblies.Add("System.IO.Compression.dll")
        cp.ReferencedAssemblies.Add("System.IO.Compression.FileSystem.dll")
        'cp.ReferencedAssemblies.Add("System.Security")
        cp.ReferencedAssemblies.Add("System.Drawing.dll")
        'cp.ReferencedAssemblies.Add("System.Diagnostics.dll")
        cp.ReferencedAssemblies.Add("System.Windows.Forms.dll")
        cp.EmbeddedResources.AddRange(filenames.ToArray())



        _CreateTempEmbeddedResourcesFiles()


        Dim cr As CompilerResults = vbCP.CompileAssemblyFromFile(cp, FilesToCompile.ToArray)

        If cr.Errors.Count > 0 Then
            Dim msg As String = "Errors building " & cr.PathToAssembly

            For Each ce As CompilerError In cr.Errors
                msg += Environment.NewLine & ce.ToString()
            Next

            'Throw New ApplicationException(msg)
            MessageBox.Show(msg)
        End If
    End Sub

    Private Function _CreateTempZip() As String
        Try
            FileList.ResetFileCounter()

            Dim _file As String = "", _path As String = "", _destination_path As String = "", _crc As String = ""
            Using tempZip As ZipArchive = ZipFile.Open(tempZipFile, ZipArchiveMode.Create)
                While FileList.GetNext(_file, _path, _destination_path, _crc)
                    _destination_path = _destination_path.Replace(Shared_Constants.INTERNAL_PATH_SEPARATOR, Shared_Constants.NORMAL_PATH_SEPARATOR)
                    If _destination_path.StartsWith(Shared_Constants.NORMAL_PATH_SEPARATOR) Then
                        _destination_path = _destination_path.Substring(1, _destination_path.Length - 1)
                    End If

                    If Settings.InternalZipCompressed Then
                        tempZip.CreateEntryFromFile(_file, _destination_path, CompressionLevel.Optimal)
                    Else
                        tempZip.CreateEntryFromFile(_file, _destination_path, CompressionLevel.NoCompression)
                    End If
                End While
            End Using

            filenames.Add(tempZipFile)

            Return tempZipFile
        Catch ex As Exception
            MsgBox(ex.Message)

            Return ""
        End Try
    End Function

    Public Sub Dispose() Implements System.IDisposable.Dispose
        For Each path As String In filenames
            Try
                File.Delete(path)
            Catch ex As Exception

            End Try
        Next


        Try
            File.Delete(tempZipFile)
        Catch ex As Exception

        End Try


        Try
            For Each _tmpfile As String In FilesToCompile
                Try
                    File.Delete(_tmpfile)
                Catch ex As Exception

                End Try
            Next
        Catch ex As Exception

        End Try


        filenames.Clear()
        FilesToCompile = New List(Of String)


        GC.SuppressFinalize(Me)
    End Sub
End Class
'End Namespace
