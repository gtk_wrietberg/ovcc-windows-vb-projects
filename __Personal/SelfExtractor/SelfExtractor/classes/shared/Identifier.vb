﻿Imports System

Public Class Identifier
    Private Shared mGuid As String
    Public Shared Property Guid() As String
        Get
            Return mGuid
        End Get
        Set(ByVal value As String)
            mGuid = value
        End Set
    End Property

    Private Shared mVersion As String
    Public Shared Property Version() As String
        Get
            Return mVersion
        End Get
        Set(ByVal value As String)
            mVersion = value
        End Set
    End Property


    Public Shared Sub Reset()
        mGuid = ""
        mVersion = ""
    End Sub

    Public Shared Function Parse(str As String) As Boolean
        Reset()

        Dim TextLines() As String = str.Split(Environment.NewLine.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

        For Each textline As String In TextLines
            Dim keyvaluepair() As String = textline.Split("=".ToCharArray(), 2)

            If keyvaluepair.Length = 2 Then
                'ok
                Select Case keyvaluepair(0)
                    Case "Guid"
                        mGuid = keyvaluepair(1)
                    Case "Version"
                        mVersion = keyvaluepair(1)
                End Select
            End If
        Next

        Return True
    End Function

    Public Shared Function Dump() As String
        Dim str As String = ""

        str &= "Guid=" & mGuid
        str &= Environment.NewLine

        str &= "Version=" & mVersion
        str &= Environment.NewLine


        Return str
    End Function

End Class
