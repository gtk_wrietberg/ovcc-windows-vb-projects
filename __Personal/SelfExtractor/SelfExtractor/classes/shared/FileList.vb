﻿Imports System
Imports System.IO
Imports System.Collections.Generic

Public Class FileList
    Private Shared mFiles As New List(Of String)
    Private Shared mPaths As New List(Of String)
    Private Shared mDestinationFiles As New List(Of String)
    Private Shared mCRCs As New List(Of String)

    Private Shared mCount As Integer = -1

    Private Shared ReadOnly c_FILELIST_SEPARATOR As String = "*"


    Public Shared Sub Reset()
        mFiles = New List(Of String)
        mPaths = New List(Of String)
        mDestinationFiles = New List(Of String)
        mCRCs = New List(Of String)

        mCount = -1
    End Sub

    Public Shared Sub Add(file As String, path As String, crc As String)
        Add(file, path, "", crc)
    End Sub

    Public Shared Sub Add(file As String, path As String, name As String, crc As String)
        If name.Equals("") OrElse IO.Path.GetFileName(file).Equals(name) Then
            _Add(file, path, IO.Path.Combine(path, IO.Path.GetFileName(file)), crc)
        Else
            _Add(file, path, IO.Path.Combine(path, name), crc)
        End If
    End Sub

    Private Shared Sub _Add(file As String, path As String, destination_path As String, crc As String)
        mFiles.Add(file)
        mPaths.Add(path)

        destination_path = destination_path.Replace(Shared_Constants.INTERNAL_PATH_SEPARATOR, Shared_Constants.NORMAL_PATH_SEPARATOR)
        mDestinationFiles.Add(destination_path)
        mCRCs.Add(crc)
    End Sub

    Public Shared ReadOnly Property Count() As Integer
        Get
            Return mFiles.Count
        End Get
    End Property

    Public Shared Function Parse(str As String) As Boolean
        Reset()

        Dim _columns As Integer = 4

        Dim TextLines() As String = str.Split(Environment.NewLine.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

        For Each textline As String In TextLines
            Dim keyvaluepair() As String = textline.Split(c_FILELIST_SEPARATOR.ToCharArray(), _columns)

            If keyvaluepair.Length = _columns Then
                'ok

                _Add(keyvaluepair(0), keyvaluepair(1), keyvaluepair(2), keyvaluepair(3))
            End If
        Next

        Return True
    End Function

    Public Shared Function Dump() As String
        Dim str As String = ""

        For i As Integer = 0 To mFiles.Count - 1
            str &= mFiles.Item(i) & c_FILELIST_SEPARATOR & mPaths.Item(i) & c_FILELIST_SEPARATOR & mDestinationFiles.Item(i) & c_FILELIST_SEPARATOR & mCRCs.Item(i)
            str &= Environment.NewLine
        Next

        Return str
    End Function

    Public Shared Sub ResetFileCounter()
        mCount = -1
    End Sub

    Public Shared Function GetNext(ByRef file As String, ByRef path As String, ByRef destination_path As String, ByRef crc As String) As Boolean
        mCount += 1

        If mCount < 0 Or mCount >= mFiles.Count Or mCount >= mPaths.Count Or mCount >= mDestinationFiles.Count Or mCount >= mCRCs.Count Then
            file = ""
            path = ""
            destination_path = ""
            crc = ""

            Return False
        End If

        file = mFiles.Item(mCount)
        path = mPaths.Item(mCount)
        destination_path = mDestinationFiles.Item(mCount)
        crc = mCRCs.Item(mCount)

        Return True
    End Function
End Class
