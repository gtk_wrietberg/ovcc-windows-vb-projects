﻿Public Class ProgressBarWithText
    Inherits System.Windows.Forms.ProgressBar

    Public Sub New()
        Me.SetStyle(ControlStyles.UserPaint, True)

        Me.Minimum = 0
        Me.Maximum = 100
        Me.Value = 0
        Me.Visible = True

    End Sub

    Private mShowText As Boolean
    Public Property ShowText() As Boolean
        Get
            Return mShowText
        End Get
        Set(ByVal value As Boolean)
            mShowText = value
        End Set
    End Property

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)

        If Me.mShowText Then
            Dim _str As String = Me.Value.ToString & "/" & Me.Maximum.ToString

            Using _g As Graphics = Me.CreateGraphics()
                Using _f As Font = New Font("Segoe UI", 8)
                    Dim _ms As SizeF = _g.MeasureString(_str, _f)
                    Dim _p As New PointF((Me.Width - _ms.Width) / 2, (Me.Height - _ms.Height) / 2)

                    Console.WriteLine("progressBusy_Paint - " & _p.X & "-" & _p.Y)

                    _g.DrawString(_str, _f, Brushes.Black, _p)
                End Using
            End Using
        End If

    End Sub
End Class
