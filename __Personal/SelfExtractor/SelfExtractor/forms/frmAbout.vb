﻿Public Class frmAbout
    Private immobiliser As New FormFrozenInPlace(Me)

    Private Sub frmAbout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = "About " & Application.ProductName

        lblTitle.Text = Application.ProductName
        lblVersion.Text = "v" & Application.ProductVersion
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class