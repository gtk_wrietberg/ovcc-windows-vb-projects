﻿Public Class frmExtraSettings
    Private immobiliser As New FormFrozenInPlace(Me)

    Private tempPasswordStorage As String = ""

    Private Sub frmExtraSettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmbExecutablePath.Items.Clear()

        Dim exe_files As String() = frmMain._PackedFiles.GetAllExecutables()

        For Each exe_file As String In exe_files
            cmbExecutablePath.Items.Add(exe_file)

            If exe_file.Equals(Settings.ArchiveAutoExecPath) Then
                'selected
                cmbExecutablePath.SelectedValue = exe_file
            End If
        Next


        If Not Settings.Saved Then
            'load default values
            txtBuildFolder.Text = My.Settings.ARCHIVE_DEFAULT_BuildFolder
            txtArchiveName.Text = My.Settings.ARCHIVE_DEFAULT_ArchiveName
            txtDestinationRootFolder.Text = My.Settings.ARCHIVE_DEFAULT_DestinationRootFolder
            chkCheckCRC.Checked = My.Settings.ARCHIVE_DEFAULT_CheckCRC
            chkShowUI.Checked = My.Settings.ARCHIVE_DEFAULT_ShowUI
            chkDebugging.Checked = My.Settings.APP_DEBUGGING
            txtExecutableTimeout.Text = My.Settings.APP_POST_EXE_TIMEOUT.ToString
            chkInternalZipCompressed.Checked = My.Settings.ARCHIVE_DEFAULT_CompressInternalZip

            chkPasswordProtection.Checked = False
            txtArchivePassword.Text = ""
        Else
            txtBuildFolder.Text = Settings.ArchivePath
            txtArchiveName.Text = Settings.ArchiveName
            txtDestinationRootFolder.Text = Settings.DestinationPath
            chkCheckCRC.Checked = Settings.CheckCRC32
            chkShowUI.Checked = Settings.ShowUI
            chkDebugging.Checked = Settings.Debugging
            chkInternalZipCompressed.Checked = Settings.InternalZipCompressed

            txtExecutableDomain.Text = Settings.ArchiveAutoExecDomain
            chkExecutableHidden.Checked = Settings.ArchiveAutoExecHidden
            txtExecutableParams.Text = Settings.ArchiveAutoExecParams
            txtExecutablePassword.Text = XOrObfuscation.v2.Deobfuscate(Settings.ArchiveAutoExecPassWord)
            txtExecutableTimeout.Text = Settings.ArchiveAutoExecTimeout.ToString
            txtExecutableUserName.Text = Settings.ArchiveAutoExecUserName
        End If

#If DEBUG Then
        If txtBuildFolder.Text = "" Then
            txtBuildFolder.Text = "D:\Temp\___test__test_test\__________TEST\_build"
        End If

        If txtDestinationRootFolder.Text = "" Then
            txtDestinationRootFolder.Text = "D:\Temp\___test__test_test\__________TEST\_destination"
        End If
#End If

        'icons listview
        Dim imglist As New ImageList

        Dim _icons As String() = IO.Directory.GetFiles(FunctionsAndStuff.Constants.FOLDER_ICONS, "*.ico")

        listviewIcons.Items.Clear()
        listviewIcons.Columns.Clear()
        listviewIcons.SmallImageList = imglist
        listviewIcons.Scrollable = True
        listviewIcons.View = View.Details
        'listviewIcons.OwnerDraw = True
        listviewIcons.HideSelection = True

        Dim _header As New ColumnHeader
        _header.Text = ""
        _header.Name = "voidCol"
        _header.Width = listviewIcons.Width - 24

        listviewIcons.Columns.Add(_header)
        listviewIcons.HeaderStyle = ColumnHeaderStyle.None

        For i As Integer = 0 To _icons.Length - 1
            Try
                Dim _icon As New Icon(_icons(i))
                Dim _l As New ListViewItem

                imglist.Images.Add(_icon)


                _l.Text = IO.Path.GetFileName(_icons(i))
                _l.Name = IO.Path.GetFullPath(_icons(i))
                _l.ImageIndex = imglist.Images.Count - 1

                Console.WriteLine("_icons: _l.name='" & _l.Name & "' - Settings.IconPath='" & Settings.IconPath & "'")
                If Not Settings.IconPath.Equals("") And _l.Name.Equals(Settings.IconPath) Then
                    Console.WriteLine("UWIUEWRYUIWYERUIYWEUIRYWEIURYIUWEYRUIWEYIRU")
                    _l.Selected = True
                End If

                listviewIcons.Items.Add(_l)
            Catch ex As Exception

            End Try
        Next


        chkShowUI_CheckedChanged(chkShowUI, New EventArgs())
    End Sub

    Private Sub listviewIcons_DrawItem(ByVal sender As Object, ByVal e As DrawListViewItemEventArgs) Handles listviewIcons.DrawItem

        If Not (e.State And ListViewItemStates.Selected) = 0 Then

            ' Draw the background for a selected item.
            e.Graphics.FillRectangle(Brushes.Azure, e.Bounds)
            e.DrawFocusRectangle()

        End If

        ' Draw the item text for views other than the Details view.
        If Not listviewIcons.View = View.Details Then
            e.DrawText()
        End If

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim sError As String = ""
        Dim iTmp As Integer = -1


        If Not FunctionsAndStuff.FilenameIsOK(txtArchiveName.Text) Then
            sError &= "The archive name '" & txtArchiveName.Text & "' is not valid!"
            sError &= vbCrLf
        End If

        If Not IO.Directory.Exists(txtBuildFolder.Text) Then
            sError &= "The build folder '" & txtBuildFolder.Text & "' does not exist!"
            sError &= vbCrLf
        End If

        If Not IO.Directory.Exists(txtDestinationRootFolder.Text) Then
            sError &= "The destination folder '" & txtDestinationRootFolder.Text & "' does not exist!"
            sError &= vbCrLf
        End If


        If Not sError.Equals("") Then
            MessageBox.Show(sError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)

            Exit Sub
        End If



        Try
            If cmbExecutablePath.SelectedItem Is Nothing Then
                Settings.ArchiveAutoExecPath = ""
            Else
                Settings.ArchiveAutoExecPath = cmbExecutablePath.SelectedItem.ToString
            End If
        Catch ex As Exception
            Settings.ArchiveAutoExecPath = ""
        End Try




        Settings.ArchiveAutoExecDomain = txtExecutableDomain.Text
        Settings.ArchiveAutoExecHidden = chkExecutableHidden.Checked
        Settings.ArchiveAutoExecParams = txtExecutableParams.Text
        Settings.ArchiveAutoExecPassWord = XOrObfuscation.v2.Obfuscate(txtExecutablePassword.Text)
        If Integer.TryParse(txtExecutableTimeout.Text, iTmp) Then
            Settings.ArchiveAutoExecTimeout = iTmp
        Else
            Settings.ArchiveAutoExecTimeout = -1
            txtExecutableTimeout.Text = -1
        End If
        Settings.ArchiveAutoExecUserName = txtExecutableUserName.Text



        Settings.DestinationPath = txtDestinationRootFolder.Text
        Settings.ArchivePath = txtBuildFolder.Text
        Settings.ArchiveName = txtArchiveName.Text

        Settings.CheckCRC32 = chkCheckCRC.Checked

        Settings.InternalZipCompressed = chkInternalZipCompressed.Checked

        Settings.ShowUI = chkShowUI.Checked

        If listviewIcons.SelectedItems.Count = 1 Then
            Settings.IconPath = listviewIcons.SelectedItems(0).Name
        Else
            Settings.IconPath = ""
        End If

        Settings.Debugging = chkDebugging.Checked


        If chkPasswordProtection.Checked And Not txtArchivePassword.Text.Equals("") Then
            Settings.ArchivePassword_PLAINTEXT = txtArchivePassword.Text
            Settings.ArchivePassword_Encrypted = XOrObfuscation.v2.Obfuscate(Crypto.SHA._512.fromString(Settings.ArchivePassword_PLAINTEXT))
        Else
            Settings.ArchivePassword_PLAINTEXT = ""
            Settings.ArchivePassword_Encrypted = ""
        End If


        Settings.Saved = True

        CloseDialog()
    End Sub

    Private Sub btnBrowseExtractFolder_Click(sender As Object, e As EventArgs) Handles btnBrowseExtractFolder.Click
        folderBrowserDialogExtract.RootFolder = Environment.SpecialFolder.MyComputer
        folderBrowserDialogExtract.SelectedPath = txtDestinationRootFolder.Text

        If folderBrowserDialogExtract.ShowDialog(Me) = DialogResult.OK Then
            txtDestinationRootFolder.Text = folderBrowserDialogExtract.SelectedPath
        End If
    End Sub

    Private Sub btnBrowseBuildFolder_Click(sender As Object, e As EventArgs) Handles btnBrowseBuildFolder.Click
        folderBrowserDialogBuild.RootFolder = Environment.SpecialFolder.MyComputer
        folderBrowserDialogBuild.SelectedPath = txtBuildFolder.Text

        If folderBrowserDialogBuild.ShowDialog(Me) = DialogResult.OK Then
            txtBuildFolder.Text = folderBrowserDialogBuild.SelectedPath
        End If
    End Sub

    Private Sub CloseDialog()
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        CloseDialog()
    End Sub


    Private Sub listviewIcons_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles listviewIcons.LostFocus
        If listviewIcons.SelectedItems.Count = 1 Then
            listviewIcons.SelectedItems(0).BackColor = Color.Silver
            listviewIcons.Refresh()
        End If
    End Sub

    Private Sub listviewIcons_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles listviewIcons.GotFocus
        If listviewIcons.SelectedItems.Count = 1 Then
            listviewIcons.SelectedItems(0).BackColor = Color.Blue
            listviewIcons.Refresh()
        End If
    End Sub

    Private Sub txtExecutablePassword_LostFocus(sender As Object, e As EventArgs) Handles txtExecutablePassword.LostFocus

    End Sub

    Private Sub txtExecutableTimeout_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtExecutableTimeout.KeyPress
        If Not Char.IsDigit(e.KeyChar) And Not Char.IsControl(e.KeyChar) And Not e.KeyChar = "-" Then
            e.Handled = True
        End If
    End Sub

    Private Sub chkPasswordProtection_CheckedChanged(sender As Object, e As EventArgs) Handles chkPasswordProtection.CheckedChanged
        txtArchivePassword.Enabled = chkPasswordProtection.Checked
    End Sub

    Private Sub chkShowUI_CheckedChanged(sender As Object, e As EventArgs) Handles chkShowUI.CheckedChanged
        chkPasswordProtection.Enabled = chkShowUI.Checked
        txtArchivePassword.Enabled = chkShowUI.Checked
    End Sub

    Private Sub cmbExecutablePath_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbExecutablePath.SelectedIndexChanged

    End Sub
End Class