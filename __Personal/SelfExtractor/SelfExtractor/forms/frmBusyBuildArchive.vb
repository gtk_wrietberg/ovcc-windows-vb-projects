﻿Public Class frmBusyBuildArchive
    Private immobiliser As New FormFrozenInPlace(Me)

    Private ReadOnly c_MARGIN As Integer = 3

    Public Event eventActionCanceled()


    Public Property Progress_Text As String
        Get
            Return lblProgress.Text
        End Get
        Set(value As String)
            lblProgress.Text = value
            lblProgress.Refresh()
        End Set
    End Property

    Public Property ProgressBar_Max As Integer
        Get
            Return progressBusy.Maximum
        End Get
        Set([value] As Integer)
            progressBusy.Maximum = [value]
        End Set
    End Property

    Public Property ProgressBar_Value() As Integer
        Get
            Return progressBusy.Value
        End Get
        Set(ByVal [value] As Integer)
            If [value] < 0 Then
                [value] = 0
            End If
            If [value] > progressBusy.Maximum Then
                [value] = progressBusy.Maximum
            End If

            progressBusy.Value = [value]
            progressBusy.Refresh()
        End Set
    End Property

    Public Property ShowCancelButton() As Boolean
        Get
            Return btnCancel.Enabled
        End Get
        Set(ByVal value As Boolean)
            btnCancel.Enabled = value
            btnCancel.Visible = value
        End Set
    End Property


    Private Sub frmBusy_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Me.Size = frmMain.ClientSize
        Me.Height = frmMain.ClientSize.Height / 2
        Me.Width = frmMain.ClientSize.Width / 2

        Me.Top = frmMain.Top + (frmMain.Height - Me.Height) / 2
        Me.Left = frmMain.Left + (frmMain.Width - Me.Width) / 2


        pnlText.Top = c_MARGIN
        pnlText.Left = c_MARGIN
        pnlText.Width = Me.Width - (2 * c_MARGIN)
        pnlText.Height = Me.Height - (2 * c_MARGIN)


        lblText.Width = pnlText.Width - (2 * c_MARGIN)
        lblText.Top = c_MARGIN
        lblText.Left = 0


        lblProgress.Width = pnlText.Width - (2 * c_MARGIN)
        lblProgress.Top = lblText.Top + lblText.Height
        lblProgress.Left = 0
        lblProgress.Text = ""


        progressBusy.Top = pnlText.Height - (progressBusy.Height + 3 * c_MARGIN)
        progressBusy.Left = 3 * c_MARGIN
        progressBusy.Width = pnlText.Width - (3 * 2 * c_MARGIN)


        btnCancel.Top = progressBusy.Top - (btnCancel.Height + 2 * c_MARGIN)
        btnCancel.Left = (pnlText.Width - btnCancel.Width) / 2


        progressBusy.Value = 0

        Me.TransparencyKey = Color.Lime
    End Sub

    Private Sub pnlText_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlText.Paint
        Dim borderWidth As Integer = 1
        Dim theColor As Color = Color.Red

        ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, theColor, borderWidth, ButtonBorderStyle.Solid, theColor, borderWidth, ButtonBorderStyle.Solid, theColor, borderWidth, ButtonBorderStyle.Solid, theColor, borderWidth, ButtonBorderStyle.Solid)
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Console.WriteLine("frmBusyFileList.btnCancel_Click")

        RaiseEvent eventActionCanceled()
    End Sub
End Class