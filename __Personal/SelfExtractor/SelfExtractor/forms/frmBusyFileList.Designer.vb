﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBusyFileList
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlText = New System.Windows.Forms.Panel()
        Me.progressBusy = New System.Windows.Forms.ProgressBar()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblText = New System.Windows.Forms.Label()
        Me.lblProgressBarText = New System.Windows.Forms.Label()
        Me.pnlText.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlText
        '
        Me.pnlText.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlText.Controls.Add(Me.lblProgressBarText)
        Me.pnlText.Controls.Add(Me.progressBusy)
        Me.pnlText.Controls.Add(Me.lblProgress)
        Me.pnlText.Controls.Add(Me.btnCancel)
        Me.pnlText.Controls.Add(Me.lblText)
        Me.pnlText.Location = New System.Drawing.Point(12, 12)
        Me.pnlText.Name = "pnlText"
        Me.pnlText.Size = New System.Drawing.Size(499, 296)
        Me.pnlText.TabIndex = 0
        '
        'progressBusy
        '
        Me.progressBusy.Location = New System.Drawing.Point(3, 266)
        Me.progressBusy.Name = "progressBusy"
        Me.progressBusy.Size = New System.Drawing.Size(491, 25)
        Me.progressBusy.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressBusy.TabIndex = 3
        '
        'lblProgress
        '
        Me.lblProgress.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress.Location = New System.Drawing.Point(66, 112)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(364, 31)
        Me.lblProgress.TabIndex = 2
        Me.lblProgress.Tag = ""
        Me.lblProgress.Text = "progress text here"
        Me.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(145, 173)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(195, 37)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel current operation"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblText
        '
        Me.lblText.Font = New System.Drawing.Font("Segoe UI", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblText.Location = New System.Drawing.Point(66, 28)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(405, 71)
        Me.lblText.TabIndex = 0
        Me.lblText.Text = "adding files and folders"
        Me.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProgressBarText
        '
        Me.lblProgressBarText.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblProgressBarText.Location = New System.Drawing.Point(3, 240)
        Me.lblProgressBarText.Name = "lblProgressBarText"
        Me.lblProgressBarText.Size = New System.Drawing.Size(491, 23)
        Me.lblProgressBarText.TabIndex = 4
        Me.lblProgressBarText.Text = "0/0"
        Me.lblProgressBarText.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmBusyFileList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.ClientSize = New System.Drawing.Size(641, 370)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlText)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBusyFileList"
        Me.Opacity = 0.9R
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmBusy"
        Me.pnlText.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlText As Panel
    Friend WithEvents lblText As Label
    Friend WithEvents btnCancel As Button
    Friend WithEvents lblProgress As Label
    Friend WithEvents progressBusy As ProgressBar
    Friend WithEvents lblProgressBarText As Label
End Class
