﻿Public Class frmOptions
    Private immobiliser As New FormFrozenInPlace(Me)

    Private Sub frmOptions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        chk_DEFAULT_ShowUI.Checked = My.Settings.ARCHIVE_DEFAULT_ShowUI
        txt_DEFAULT_ArchiveName.Text = My.Settings.ARCHIVE_DEFAULT_ArchiveName
        txt_DEFAULT_BuildFolder.Text = My.Settings.ARCHIVE_DEFAULT_BuildFolder
        txt_DEFAULT_DestinationRootFolder.Text = My.Settings.ARCHIVE_DEFAULT_DestinationRootFolder
        chk_DEFAULT_CheckCRC.Checked = My.Settings.ARCHIVE_DEFAULT_CheckCRC

        chk_APP_Debugging.Checked = My.Settings.APP_DEBUGGING
    End Sub
















    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        CloseDialog()
    End Sub

    Private Sub CloseDialog()
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim sError As String = ""


        If Not FunctionsAndStuff.FilenameIsOK(txt_DEFAULT_ArchiveName.Text) Then
            sError &= "The archive name '" & txt_DEFAULT_ArchiveName.Text & "' is not valid!"
            sError &= vbCrLf
        End If

        If Not txt_DEFAULT_BuildFolder.Text.Equals("") And Not IO.Directory.Exists(txt_DEFAULT_BuildFolder.Text) Then
            sError &= "The build folder '" & txt_DEFAULT_BuildFolder.Text & "' does not exist!"
            sError &= vbCrLf
        End If

        If Not txt_DEFAULT_DestinationRootFolder.Text.Equals("") And Not IO.Directory.Exists(txt_DEFAULT_DestinationRootFolder.Text) Then
            sError &= "The destination folder '" & txt_DEFAULT_DestinationRootFolder.Text & "' does not exist!"
            sError &= vbCrLf
        End If


        If Not sError.Equals("") Then
            MessageBox.Show(sError, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)

            Exit Sub
        End If



        My.Settings.ARCHIVE_DEFAULT_DestinationRootFolder = txt_DEFAULT_DestinationRootFolder.Text
        My.Settings.ARCHIVE_DEFAULT_BuildFolder = txt_DEFAULT_BuildFolder.Text
        My.Settings.ARCHIVE_DEFAULT_ArchiveName = txt_DEFAULT_ArchiveName.Text

        My.Settings.ARCHIVE_DEFAULT_CheckCRC = chk_DEFAULT_CheckCRC.Checked
        My.Settings.ARCHIVE_DEFAULT_ShowUI = chk_DEFAULT_ShowUI.Checked

        My.Settings.APP_DEBUGGING = chk_APP_Debugging.Checked


        CloseDialog()
    End Sub

    Private Sub btnBrowseExtractFolder_Click(sender As Object, e As EventArgs) Handles btnBrowseExtractFolder.Click
        folderBrowserDialogExtract.RootFolder = Environment.SpecialFolder.MyComputer
        folderBrowserDialogExtract.SelectedPath = txt_DEFAULT_DestinationRootFolder.Text
        If folderBrowserDialogExtract.ShowDialog(Me) = DialogResult.OK Then
            txt_DEFAULT_DestinationRootFolder.Text = folderBrowserDialogExtract.SelectedPath
        End If
    End Sub

    Private Sub btnBrowseBuildFolder_Click(sender As Object, e As EventArgs) Handles btnBrowseBuildFolder.Click
        folderBrowserDialogBuild.RootFolder = Environment.SpecialFolder.MyComputer
        folderBrowserDialogBuild.SelectedPath = txt_DEFAULT_BuildFolder.Text

        If folderBrowserDialogBuild.ShowDialog(Me) = DialogResult.OK Then
            txt_DEFAULT_BuildFolder.Text = folderBrowserDialogBuild.SelectedPath
        End If
    End Sub
End Class