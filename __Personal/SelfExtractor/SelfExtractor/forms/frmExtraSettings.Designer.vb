﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmExtraSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExtraSettings))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkExecutableHidden = New System.Windows.Forms.CheckBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtExecutableTimeout = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtExecutableDomain = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtExecutablePassword = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtExecutableUserName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtExecutableParams = New System.Windows.Forms.TextBox()
        Me.lblCaption_Executable = New System.Windows.Forms.Label()
        Me.cmbExecutablePath = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnBrowseExtractFolder = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDestinationRootFolder = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.folderBrowserDialogExtract = New System.Windows.Forms.FolderBrowserDialog()
        Me.chkCheckCRC = New System.Windows.Forms.CheckBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtArchiveName = New System.Windows.Forms.TextBox()
        Me.btnBrowseBuildFolder = New System.Windows.Forms.Button()
        Me.txtBuildFolder = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.folderBrowserDialogBuild = New System.Windows.Forms.FolderBrowserDialog()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.chkInternalZipCompressed = New System.Windows.Forms.CheckBox()
        Me.txtArchivePassword = New System.Windows.Forms.TextBox()
        Me.chkPasswordProtection = New System.Windows.Forms.CheckBox()
        Me.chkDebugging = New System.Windows.Forms.CheckBox()
        Me.chkShowUI = New System.Windows.Forms.CheckBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.listviewIcons = New System.Windows.Forms.ListView()
        Me.grpboxArchiveSettings = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.grpboxArchiveSettings.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.chkExecutableHidden)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txtExecutableTimeout)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txtExecutableDomain)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtExecutablePassword)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txtExecutableUserName)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txtExecutableParams)
        Me.Panel1.Controls.Add(Me.lblCaption_Executable)
        Me.Panel1.Controls.Add(Me.cmbExecutablePath)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Location = New System.Drawing.Point(6, 21)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(474, 286)
        Me.Panel1.TabIndex = 9
        '
        'chkExecutableHidden
        '
        Me.chkExecutableHidden.AutoSize = True
        Me.chkExecutableHidden.Location = New System.Drawing.Point(213, 125)
        Me.chkExecutableHidden.Name = "chkExecutableHidden"
        Me.chkExecutableHidden.Size = New System.Drawing.Size(257, 17)
        Me.chkExecutableHidden.TabIndex = 20
        Me.chkExecutableHidden.Text = "Run hidden (WARNING: no user interaction)"
        Me.chkExecutableHidden.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 94)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(79, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Timeout (ms)"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExecutableTimeout
        '
        Me.txtExecutableTimeout.Location = New System.Drawing.Point(3, 121)
        Me.txtExecutableTimeout.Name = "txtExecutableTimeout"
        Me.txtExecutableTimeout.Size = New System.Drawing.Size(87, 22)
        Me.txtExecutableTimeout.TabIndex = 17
        Me.txtExecutableTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 195)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(466, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Domain/Workgroup"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExecutableDomain
        '
        Me.txtExecutableDomain.Location = New System.Drawing.Point(3, 211)
        Me.txtExecutableDomain.Name = "txtExecutableDomain"
        Me.txtExecutableDomain.Size = New System.Drawing.Size(466, 22)
        Me.txtExecutableDomain.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 240)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(466, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Password"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExecutablePassword
        '
        Me.txtExecutablePassword.Location = New System.Drawing.Point(3, 256)
        Me.txtExecutablePassword.Name = "txtExecutablePassword"
        Me.txtExecutablePassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtExecutablePassword.Size = New System.Drawing.Size(466, 22)
        Me.txtExecutablePassword.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 150)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(466, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Username"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExecutableUserName
        '
        Me.txtExecutableUserName.Location = New System.Drawing.Point(3, 166)
        Me.txtExecutableUserName.Name = "txtExecutableUserName"
        Me.txtExecutableUserName.Size = New System.Drawing.Size(466, 22)
        Me.txtExecutableUserName.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(466, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "with parameters"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExecutableParams
        '
        Me.txtExecutableParams.Location = New System.Drawing.Point(3, 63)
        Me.txtExecutableParams.Name = "txtExecutableParams"
        Me.txtExecutableParams.Size = New System.Drawing.Size(466, 22)
        Me.txtExecutableParams.TabIndex = 9
        '
        'lblCaption_Executable
        '
        Me.lblCaption_Executable.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption_Executable.Location = New System.Drawing.Point(3, 2)
        Me.lblCaption_Executable.Name = "lblCaption_Executable"
        Me.lblCaption_Executable.Size = New System.Drawing.Size(466, 13)
        Me.lblCaption_Executable.TabIndex = 8
        Me.lblCaption_Executable.Text = "Executable to run after extraction"
        '
        'cmbExecutablePath
        '
        Me.cmbExecutablePath.FormattingEnabled = True
        Me.cmbExecutablePath.Location = New System.Drawing.Point(3, 18)
        Me.cmbExecutablePath.Name = "cmbExecutablePath"
        Me.cmbExecutablePath.Size = New System.Drawing.Size(466, 21)
        Me.cmbExecutablePath.TabIndex = 7
        Me.cmbExecutablePath.Text = "select exe from archive..."
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(3, 105)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(200, 13)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "(zero or negative means no timeout)"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnBrowseExtractFolder
        '
        Me.btnBrowseExtractFolder.BackColor = System.Drawing.Color.Silver
        Me.btnBrowseExtractFolder.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowseExtractFolder.Location = New System.Drawing.Point(2, 154)
        Me.btnBrowseExtractFolder.Name = "btnBrowseExtractFolder"
        Me.btnBrowseExtractFolder.Size = New System.Drawing.Size(74, 24)
        Me.btnBrowseExtractFolder.TabIndex = 13
        Me.btnBrowseExtractFolder.Text = "Browse..."
        Me.btnBrowseExtractFolder.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 118)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(466, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Destination root folder"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDestinationRootFolder
        '
        Me.txtDestinationRootFolder.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDestinationRootFolder.Location = New System.Drawing.Point(3, 134)
        Me.txtDestinationRootFolder.Name = "txtDestinationRootFolder"
        Me.txtDestinationRootFolder.Size = New System.Drawing.Size(466, 19)
        Me.txtDestinationRootFolder.TabIndex = 11
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(6, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(118, 77)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(362, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(118, 77)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'chkCheckCRC
        '
        Me.chkCheckCRC.AutoSize = True
        Me.chkCheckCRC.Location = New System.Drawing.Point(3, 49)
        Me.chkCheckCRC.Name = "chkCheckCRC"
        Me.chkCheckCRC.Size = New System.Drawing.Size(163, 17)
        Me.chkCheckCRC.TabIndex = 14
        Me.chkCheckCRC.Text = "Add checksum verification"
        Me.chkCheckCRC.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.txtArchiveName)
        Me.Panel2.Controls.Add(Me.btnBrowseBuildFolder)
        Me.Panel2.Controls.Add(Me.txtBuildFolder)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.btnBrowseExtractFolder)
        Me.Panel2.Controls.Add(Me.txtDestinationRootFolder)
        Me.Panel2.Location = New System.Drawing.Point(6, 21)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(474, 182)
        Me.Panel2.TabIndex = 15
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(466, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Name"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtArchiveName
        '
        Me.txtArchiveName.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArchiveName.Location = New System.Drawing.Point(3, 18)
        Me.txtArchiveName.Name = "txtArchiveName"
        Me.txtArchiveName.Size = New System.Drawing.Size(466, 19)
        Me.txtArchiveName.TabIndex = 17
        '
        'btnBrowseBuildFolder
        '
        Me.btnBrowseBuildFolder.BackColor = System.Drawing.Color.Silver
        Me.btnBrowseBuildFolder.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowseBuildFolder.Location = New System.Drawing.Point(2, 84)
        Me.btnBrowseBuildFolder.Name = "btnBrowseBuildFolder"
        Me.btnBrowseBuildFolder.Size = New System.Drawing.Size(74, 24)
        Me.btnBrowseBuildFolder.TabIndex = 16
        Me.btnBrowseBuildFolder.Text = "Browse..."
        Me.btnBrowseBuildFolder.UseVisualStyleBackColor = False
        '
        'txtBuildFolder
        '
        Me.txtBuildFolder.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuildFolder.Location = New System.Drawing.Point(3, 64)
        Me.txtBuildFolder.Name = "txtBuildFolder"
        Me.txtBuildFolder.Size = New System.Drawing.Size(466, 19)
        Me.txtBuildFolder.TabIndex = 15
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(466, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Build folder"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.chkInternalZipCompressed)
        Me.Panel5.Controls.Add(Me.txtArchivePassword)
        Me.Panel5.Controls.Add(Me.chkPasswordProtection)
        Me.Panel5.Controls.Add(Me.chkCheckCRC)
        Me.Panel5.Controls.Add(Me.chkDebugging)
        Me.Panel5.Controls.Add(Me.chkShowUI)
        Me.Panel5.Location = New System.Drawing.Point(6, 21)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(474, 122)
        Me.Panel5.TabIndex = 16
        '
        'chkInternalZipCompressed
        '
        Me.chkInternalZipCompressed.AutoSize = True
        Me.chkInternalZipCompressed.Location = New System.Drawing.Point(3, 72)
        Me.chkInternalZipCompressed.Name = "chkInternalZipCompressed"
        Me.chkInternalZipCompressed.Size = New System.Drawing.Size(138, 17)
        Me.chkInternalZipCompressed.TabIndex = 18
        Me.chkInternalZipCompressed.Text = "Compress internal zip"
        Me.chkInternalZipCompressed.UseVisualStyleBackColor = True
        '
        'txtArchivePassword
        '
        Me.txtArchivePassword.Location = New System.Drawing.Point(209, 92)
        Me.txtArchivePassword.Name = "txtArchivePassword"
        Me.txtArchivePassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtArchivePassword.Size = New System.Drawing.Size(260, 22)
        Me.txtArchivePassword.TabIndex = 17
        '
        'chkPasswordProtection
        '
        Me.chkPasswordProtection.AutoSize = True
        Me.chkPasswordProtection.Location = New System.Drawing.Point(3, 95)
        Me.chkPasswordProtection.Name = "chkPasswordProtection"
        Me.chkPasswordProtection.Size = New System.Drawing.Size(206, 17)
        Me.chkPasswordProtection.TabIndex = 16
        Me.chkPasswordProtection.Text = "Password protected (with UI only):"
        Me.chkPasswordProtection.UseVisualStyleBackColor = True
        '
        'chkDebugging
        '
        Me.chkDebugging.AutoSize = True
        Me.chkDebugging.Location = New System.Drawing.Point(3, 26)
        Me.chkDebugging.Name = "chkDebugging"
        Me.chkDebugging.Size = New System.Drawing.Size(281, 17)
        Me.chkDebugging.TabIndex = 15
        Me.chkDebugging.Text = "Show extra debugging info in the archive log file"
        Me.chkDebugging.UseVisualStyleBackColor = True
        '
        'chkShowUI
        '
        Me.chkShowUI.AutoSize = True
        Me.chkShowUI.Location = New System.Drawing.Point(3, 3)
        Me.chkShowUI.Name = "chkShowUI"
        Me.chkShowUI.Size = New System.Drawing.Size(156, 17)
        Me.chkShowUI.TabIndex = 14
        Me.chkShowUI.Text = "Show UI when extracting"
        Me.chkShowUI.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.Label5)
        Me.Panel6.Controls.Add(Me.listviewIcons)
        Me.Panel6.Location = New System.Drawing.Point(6, 206)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(474, 112)
        Me.Panel6.TabIndex = 16
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(0, 2)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(466, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Icon"
        '
        'listviewIcons
        '
        Me.listviewIcons.FullRowSelect = True
        Me.listviewIcons.HideSelection = False
        Me.listviewIcons.Location = New System.Drawing.Point(3, 18)
        Me.listviewIcons.Name = "listviewIcons"
        Me.listviewIcons.Size = New System.Drawing.Size(466, 89)
        Me.listviewIcons.TabIndex = 0
        Me.listviewIcons.UseCompatibleStateImageBehavior = False
        '
        'grpboxArchiveSettings
        '
        Me.grpboxArchiveSettings.Controls.Add(Me.Panel2)
        Me.grpboxArchiveSettings.Controls.Add(Me.Panel6)
        Me.grpboxArchiveSettings.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.grpboxArchiveSettings.Location = New System.Drawing.Point(12, 12)
        Me.grpboxArchiveSettings.Name = "grpboxArchiveSettings"
        Me.grpboxArchiveSettings.Size = New System.Drawing.Size(486, 324)
        Me.grpboxArchiveSettings.TabIndex = 17
        Me.grpboxArchiveSettings.TabStop = False
        Me.grpboxArchiveSettings.Text = "Archive settings"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel5)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(504, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(486, 150)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Misc archive settings"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox2.Location = New System.Drawing.Point(504, 168)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(486, 314)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Post extraction executable"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnCancel)
        Me.GroupBox3.Controls.Add(Me.btnSave)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 385)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(486, 97)
        Me.GroupBox3.TabIndex = 20
        Me.GroupBox3.TabStop = False
        '
        'frmExtraSettings
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(1003, 491)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grpboxArchiveSettings)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExtraSettings"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Archive settings"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.grpboxArchiveSettings.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents txtExecutableParams As TextBox
    Friend WithEvents lblCaption_Executable As Label
    Friend WithEvents cmbExecutablePath As ComboBox
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents txtDestinationRootFolder As TextBox
    Friend WithEvents btnBrowseExtractFolder As Button
    Friend WithEvents folderBrowserDialogExtract As FolderBrowserDialog
    Friend WithEvents chkCheckCRC As CheckBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents btnBrowseBuildFolder As Button
    Friend WithEvents txtBuildFolder As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtArchiveName As TextBox
    Friend WithEvents folderBrowserDialogBuild As FolderBrowserDialog
    Friend WithEvents Panel5 As Panel
    Friend WithEvents chkShowUI As CheckBox
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents listviewIcons As ListView
    Friend WithEvents chkDebugging As CheckBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtExecutableDomain As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtExecutablePassword As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtExecutableUserName As TextBox
    Friend WithEvents grpboxArchiveSettings As GroupBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txtExecutableTimeout As TextBox
    Friend WithEvents chkExecutableHidden As CheckBox
    Friend WithEvents txtArchivePassword As TextBox
    Friend WithEvents chkPasswordProtection As CheckBox
    Friend WithEvents chkInternalZipCompressed As CheckBox
End Class
