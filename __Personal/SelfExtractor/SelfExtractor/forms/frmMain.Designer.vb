﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.listviewFiles = New System.Windows.Forms.ListView()
        Me.colHeader_Name = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Date = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeaderHidden_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.imagelist_listviewFiles = New System.Windows.Forms.ImageList(Me.components)
        Me.treeFolders = New System.Windows.Forms.TreeView()
        Me.pnlFolderFiles = New System.Windows.Forms.Panel()
        Me.mnustripMain = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveAsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BuildToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnArchiveSettings = New System.Windows.Forms.Button()
        Me.btnBuildArchive = New System.Windows.Forms.Button()
        Me.ctxmenuFolders = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ctxmenuitemFoldersCreate = New System.Windows.Forms.ToolStripMenuItem()
        Me.ctxmenuitemFoldersCreate_Textbox = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ctxmenuitemFoldersRename = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ctxmenuitemFoldersDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.ctxmenuFiles = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ctxmenuitemFilesRename = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ctxmenuitemFilesDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.bgworkerParseFiles = New System.ComponentModel.BackgroundWorker()
        Me.bgworkerFillLists = New System.ComponentModel.BackgroundWorker()
        Me.bgworkerBuildArchive = New System.ComponentModel.BackgroundWorker()
        Me.dlgOpenFile = New System.Windows.Forms.OpenFileDialog()
        Me.pnlFolderFiles.SuspendLayout()
        Me.mnustripMain.SuspendLayout()
        Me.ctxmenuFolders.SuspendLayout()
        Me.ctxmenuFiles.SuspendLayout()
        Me.SuspendLayout()
        '
        'listviewFiles
        '
        Me.listviewFiles.AllowDrop = True
        Me.listviewFiles.BackColor = System.Drawing.Color.White
        Me.listviewFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.listviewFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colHeader_Name, Me.colHeader_Date, Me.colHeader_Type, Me.colHeader_Size, Me.colHeaderHidden_Size})
        Me.listviewFiles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listviewFiles.GridLines = True
        Me.listviewFiles.HideSelection = False
        Me.listviewFiles.Location = New System.Drawing.Point(479, 3)
        Me.listviewFiles.MultiSelect = False
        Me.listviewFiles.Name = "listviewFiles"
        Me.listviewFiles.Size = New System.Drawing.Size(948, 371)
        Me.listviewFiles.SmallImageList = Me.imagelist_listviewFiles
        Me.listviewFiles.TabIndex = 6
        Me.listviewFiles.UseCompatibleStateImageBehavior = False
        Me.listviewFiles.View = System.Windows.Forms.View.Details
        '
        'colHeader_Name
        '
        Me.colHeader_Name.Text = "Name"
        Me.colHeader_Name.Width = 560
        '
        'colHeader_Date
        '
        Me.colHeader_Date.Text = "Date modified"
        Me.colHeader_Date.Width = 136
        '
        'colHeader_Type
        '
        Me.colHeader_Type.Text = "Type"
        Me.colHeader_Type.Width = 163
        '
        'colHeader_Size
        '
        Me.colHeader_Size.Text = "Size"
        Me.colHeader_Size.Width = 77
        '
        'colHeaderHidden_Size
        '
        Me.colHeaderHidden_Size.Text = ""
        Me.colHeaderHidden_Size.Width = 0
        '
        'imagelist_listviewFiles
        '
        Me.imagelist_listviewFiles.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.imagelist_listviewFiles.ImageSize = New System.Drawing.Size(16, 16)
        Me.imagelist_listviewFiles.TransparentColor = System.Drawing.Color.Transparent
        '
        'treeFolders
        '
        Me.treeFolders.AllowDrop = True
        Me.treeFolders.BackColor = System.Drawing.Color.White
        Me.treeFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.treeFolders.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText
        Me.treeFolders.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeFolders.ForeColor = System.Drawing.Color.Black
        Me.treeFolders.HideSelection = False
        Me.treeFolders.Indent = 20
        Me.treeFolders.ItemHeight = 20
        Me.treeFolders.Location = New System.Drawing.Point(3, 3)
        Me.treeFolders.Name = "treeFolders"
        Me.treeFolders.PathSeparator = "/"
        Me.treeFolders.ShowPlusMinus = False
        Me.treeFolders.Size = New System.Drawing.Size(473, 371)
        Me.treeFolders.TabIndex = 5
        '
        'pnlFolderFiles
        '
        Me.pnlFolderFiles.AllowDrop = True
        Me.pnlFolderFiles.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.pnlFolderFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlFolderFiles.Controls.Add(Me.treeFolders)
        Me.pnlFolderFiles.Controls.Add(Me.listviewFiles)
        Me.pnlFolderFiles.Location = New System.Drawing.Point(12, 37)
        Me.pnlFolderFiles.Name = "pnlFolderFiles"
        Me.pnlFolderFiles.Size = New System.Drawing.Size(1432, 379)
        Me.pnlFolderFiles.TabIndex = 9
        '
        'mnustripMain
        '
        Me.mnustripMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.mnustripMain.Location = New System.Drawing.Point(0, 0)
        Me.mnustripMain.Name = "mnustripMain"
        Me.mnustripMain.Size = New System.Drawing.Size(1456, 24)
        Me.mnustripMain.TabIndex = 10
        Me.mnustripMain.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripMenuItem, Me.OpenToolStripMenuItem, Me.toolStripSeparator, Me.SaveToolStripMenuItem, Me.SaveAsToolStripMenuItem, Me.toolStripSeparator1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.Image = CType(resources.GetObject("NewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.NewToolStripMenuItem.Text = "&New"
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Image = CType(resources.GetObject("OpenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.OpenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.OpenToolStripMenuItem.Text = "&Open"
        '
        'toolStripSeparator
        '
        Me.toolStripSeparator.Name = "toolStripSeparator"
        Me.toolStripSeparator.Size = New System.Drawing.Size(143, 6)
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.Image = CType(resources.GetObject("SaveToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SaveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.SaveToolStripMenuItem.Text = "&Save"
        '
        'SaveAsToolStripMenuItem
        '
        Me.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem"
        Me.SaveAsToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.SaveAsToolStripMenuItem.Text = "Save &As"
        '
        'toolStripSeparator1
        '
        Me.toolStripSeparator1.Name = "toolStripSeparator1"
        Me.toolStripSeparator1.Size = New System.Drawing.Size(143, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BuildToolStripMenuItem, Me.ToolStripSeparator2, Me.OptionsToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(46, 20)
        Me.ToolsToolStripMenuItem.Text = "&Tools"
        '
        'BuildToolStripMenuItem
        '
        Me.BuildToolStripMenuItem.Name = "BuildToolStripMenuItem"
        Me.BuildToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.BuildToolStripMenuItem.Text = "&Build"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(113, 6)
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.OptionsToolStripMenuItem.Text = "&Options"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "&Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.AboutToolStripMenuItem.Text = "&About..."
        '
        'btnArchiveSettings
        '
        Me.btnArchiveSettings.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnArchiveSettings.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnArchiveSettings.FlatAppearance.BorderSize = 2
        Me.btnArchiveSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.btnArchiveSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnArchiveSettings.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnArchiveSettings.ForeColor = System.Drawing.Color.Black
        Me.btnArchiveSettings.Location = New System.Drawing.Point(12, 422)
        Me.btnArchiveSettings.Name = "btnArchiveSettings"
        Me.btnArchiveSettings.Size = New System.Drawing.Size(710, 94)
        Me.btnArchiveSettings.TabIndex = 12
        Me.btnArchiveSettings.Text = "Archive settings"
        Me.btnArchiveSettings.UseVisualStyleBackColor = False
        '
        'btnBuildArchive
        '
        Me.btnBuildArchive.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnBuildArchive.Enabled = False
        Me.btnBuildArchive.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnBuildArchive.FlatAppearance.BorderSize = 2
        Me.btnBuildArchive.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White
        Me.btnBuildArchive.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuildArchive.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me.btnBuildArchive.ForeColor = System.Drawing.Color.Black
        Me.btnBuildArchive.Location = New System.Drawing.Point(734, 422)
        Me.btnBuildArchive.Name = "btnBuildArchive"
        Me.btnBuildArchive.Size = New System.Drawing.Size(710, 94)
        Me.btnBuildArchive.TabIndex = 13
        Me.btnBuildArchive.Text = "Build archive"
        Me.btnBuildArchive.UseVisualStyleBackColor = False
        '
        'ctxmenuFolders
        '
        Me.ctxmenuFolders.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ctxmenuitemFoldersCreate, Me.ToolStripSeparator6, Me.ctxmenuitemFoldersRename, Me.ToolStripSeparator3, Me.ctxmenuitemFoldersDelete})
        Me.ctxmenuFolders.Name = "ctxmenuFolders"
        Me.ctxmenuFolders.Size = New System.Drawing.Size(143, 82)
        '
        'ctxmenuitemFoldersCreate
        '
        Me.ctxmenuitemFoldersCreate.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ctxmenuitemFoldersCreate_Textbox})
        Me.ctxmenuitemFoldersCreate.Name = "ctxmenuitemFoldersCreate"
        Me.ctxmenuitemFoldersCreate.Size = New System.Drawing.Size(142, 22)
        Me.ctxmenuitemFoldersCreate.Text = "Create folder"
        '
        'ctxmenuitemFoldersCreate_Textbox
        '
        Me.ctxmenuitemFoldersCreate_Textbox.AutoSize = False
        Me.ctxmenuitemFoldersCreate_Textbox.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ctxmenuitemFoldersCreate_Textbox.Name = "ctxmenuitemFoldersCreate_Textbox"
        Me.ctxmenuitemFoldersCreate_Textbox.Size = New System.Drawing.Size(100, 23)
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(139, 6)
        '
        'ctxmenuitemFoldersRename
        '
        Me.ctxmenuitemFoldersRename.Name = "ctxmenuitemFoldersRename"
        Me.ctxmenuitemFoldersRename.Size = New System.Drawing.Size(142, 22)
        Me.ctxmenuitemFoldersRename.Text = "Rename"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(139, 6)
        '
        'ctxmenuitemFoldersDelete
        '
        Me.ctxmenuitemFoldersDelete.Name = "ctxmenuitemFoldersDelete"
        Me.ctxmenuitemFoldersDelete.Size = New System.Drawing.Size(142, 22)
        Me.ctxmenuitemFoldersDelete.Text = "Delete"
        '
        'ctxmenuFiles
        '
        Me.ctxmenuFiles.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ctxmenuitemFilesRename, Me.ToolStripSeparator4, Me.ctxmenuitemFilesDelete})
        Me.ctxmenuFiles.Name = "ctxmenuFolders"
        Me.ctxmenuFiles.Size = New System.Drawing.Size(118, 54)
        '
        'ctxmenuitemFilesRename
        '
        Me.ctxmenuitemFilesRename.Name = "ctxmenuitemFilesRename"
        Me.ctxmenuitemFilesRename.Size = New System.Drawing.Size(117, 22)
        Me.ctxmenuitemFilesRename.Text = "Rename"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(114, 6)
        '
        'ctxmenuitemFilesDelete
        '
        Me.ctxmenuitemFilesDelete.Name = "ctxmenuitemFilesDelete"
        Me.ctxmenuitemFilesDelete.Size = New System.Drawing.Size(117, 22)
        Me.ctxmenuitemFilesDelete.Text = "Delete"
        '
        'bgworkerParseFiles
        '
        Me.bgworkerParseFiles.WorkerReportsProgress = True
        Me.bgworkerParseFiles.WorkerSupportsCancellation = True
        '
        'bgworkerFillLists
        '
        Me.bgworkerFillLists.WorkerReportsProgress = True
        Me.bgworkerFillLists.WorkerSupportsCancellation = True
        '
        'bgworkerBuildArchive
        '
        Me.bgworkerBuildArchive.WorkerReportsProgress = True
        Me.bgworkerBuildArchive.WorkerSupportsCancellation = True
        '
        'dlgOpenFile
        '
        Me.dlgOpenFile.Filter = "Executables|*.exe"
        Me.dlgOpenFile.SupportMultiDottedExtensions = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(1456, 528)
        Me.Controls.Add(Me.btnBuildArchive)
        Me.Controls.Add(Me.btnArchiveSettings)
        Me.Controls.Add(Me.pnlFolderFiles)
        Me.Controls.Add(Me.mnustripMain)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.mnustripMain
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "SelfExtractor"
        Me.pnlFolderFiles.ResumeLayout(False)
        Me.mnustripMain.ResumeLayout(False)
        Me.mnustripMain.PerformLayout()
        Me.ctxmenuFolders.ResumeLayout(False)
        Me.ctxmenuFiles.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents listviewFiles As ListView
    Friend WithEvents colHeader_Name As ColumnHeader
    Friend WithEvents colHeader_Date As ColumnHeader
    Friend WithEvents colHeader_Type As ColumnHeader
    Friend WithEvents colHeader_Size As ColumnHeader
    Friend WithEvents colHeaderHidden_Size As ColumnHeader
    Friend WithEvents treeFolders As TreeView
    Friend WithEvents pnlFolderFiles As Panel
    Friend WithEvents mnustripMain As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents toolStripSeparator As ToolStripSeparator
    Friend WithEvents SaveToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SaveAsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents toolStripSeparator1 As ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BuildToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents btnArchiveSettings As Button
    Friend WithEvents btnBuildArchive As Button
    Friend WithEvents ctxmenuFolders As ContextMenuStrip
    Friend WithEvents ctxmenuitemFoldersRename As ToolStripMenuItem
    Friend WithEvents ctxmenuitemFoldersDelete As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents ctxmenuFiles As ContextMenuStrip
    Friend WithEvents ctxmenuitemFilesRename As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents ctxmenuitemFilesDelete As ToolStripMenuItem
    Friend WithEvents bgworkerParseFiles As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgworkerFillLists As System.ComponentModel.BackgroundWorker
    Friend WithEvents imagelist_listviewFiles As ImageList
    Friend WithEvents bgworkerBuildArchive As System.ComponentModel.BackgroundWorker
    Friend WithEvents ctxmenuitemFoldersCreate As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As ToolStripSeparator
    Friend WithEvents ctxmenuitemFoldersCreate_Textbox As ToolStripTextBox
    Friend WithEvents dlgOpenFile As OpenFileDialog
End Class
