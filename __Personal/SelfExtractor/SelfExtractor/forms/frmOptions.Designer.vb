﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOptions))
        Me.grpboxDefaults = New System.Windows.Forms.GroupBox()
        Me.pnlShowUI = New System.Windows.Forms.Panel()
        Me.chk_DEFAULT_ShowUI = New System.Windows.Forms.CheckBox()
        Me.pnlCheckCRC = New System.Windows.Forms.Panel()
        Me.chk_DEFAULT_CheckCRC = New System.Windows.Forms.CheckBox()
        Me.pnlArchiveProperties = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_DEFAULT_ArchiveName = New System.Windows.Forms.TextBox()
        Me.btnBrowseBuildFolder = New System.Windows.Forms.Button()
        Me.txt_DEFAULT_BuildFolder = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnBrowseExtractFolder = New System.Windows.Forms.Button()
        Me.txt_DEFAULT_DestinationRootFolder = New System.Windows.Forms.TextBox()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.grpboxApp = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chk_APP_Debugging = New System.Windows.Forms.CheckBox()
        Me.folderBrowserDialogBuild = New System.Windows.Forms.FolderBrowserDialog()
        Me.folderBrowserDialogExtract = New System.Windows.Forms.FolderBrowserDialog()
        Me.grpboxDefaults.SuspendLayout()
        Me.pnlShowUI.SuspendLayout()
        Me.pnlCheckCRC.SuspendLayout()
        Me.pnlArchiveProperties.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        Me.grpboxApp.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpboxDefaults
        '
        Me.grpboxDefaults.Controls.Add(Me.pnlShowUI)
        Me.grpboxDefaults.Controls.Add(Me.pnlCheckCRC)
        Me.grpboxDefaults.Controls.Add(Me.pnlArchiveProperties)
        Me.grpboxDefaults.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpboxDefaults.Location = New System.Drawing.Point(12, 71)
        Me.grpboxDefaults.Name = "grpboxDefaults"
        Me.grpboxDefaults.Size = New System.Drawing.Size(486, 265)
        Me.grpboxDefaults.TabIndex = 1
        Me.grpboxDefaults.TabStop = False
        Me.grpboxDefaults.Text = "Default archive settings"
        '
        'pnlShowUI
        '
        Me.pnlShowUI.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.pnlShowUI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlShowUI.Controls.Add(Me.chk_DEFAULT_ShowUI)
        Me.pnlShowUI.Location = New System.Drawing.Point(6, 21)
        Me.pnlShowUI.Name = "pnlShowUI"
        Me.pnlShowUI.Size = New System.Drawing.Size(474, 23)
        Me.pnlShowUI.TabIndex = 19
        '
        'chk_DEFAULT_ShowUI
        '
        Me.chk_DEFAULT_ShowUI.AutoSize = True
        Me.chk_DEFAULT_ShowUI.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.chk_DEFAULT_ShowUI.Location = New System.Drawing.Point(3, 3)
        Me.chk_DEFAULT_ShowUI.Name = "chk_DEFAULT_ShowUI"
        Me.chk_DEFAULT_ShowUI.Size = New System.Drawing.Size(155, 17)
        Me.chk_DEFAULT_ShowUI.TabIndex = 14
        Me.chk_DEFAULT_ShowUI.Text = "Show UI when extracting"
        Me.chk_DEFAULT_ShowUI.UseVisualStyleBackColor = True
        '
        'pnlCheckCRC
        '
        Me.pnlCheckCRC.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.pnlCheckCRC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCheckCRC.Controls.Add(Me.chk_DEFAULT_CheckCRC)
        Me.pnlCheckCRC.Location = New System.Drawing.Point(6, 234)
        Me.pnlCheckCRC.Name = "pnlCheckCRC"
        Me.pnlCheckCRC.Size = New System.Drawing.Size(474, 23)
        Me.pnlCheckCRC.TabIndex = 17
        '
        'chk_DEFAULT_CheckCRC
        '
        Me.chk_DEFAULT_CheckCRC.AutoSize = True
        Me.chk_DEFAULT_CheckCRC.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.chk_DEFAULT_CheckCRC.Location = New System.Drawing.Point(3, 3)
        Me.chk_DEFAULT_CheckCRC.Name = "chk_DEFAULT_CheckCRC"
        Me.chk_DEFAULT_CheckCRC.Size = New System.Drawing.Size(160, 17)
        Me.chk_DEFAULT_CheckCRC.TabIndex = 14
        Me.chk_DEFAULT_CheckCRC.Text = "Add checksum verification"
        Me.chk_DEFAULT_CheckCRC.UseVisualStyleBackColor = True
        '
        'pnlArchiveProperties
        '
        Me.pnlArchiveProperties.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.pnlArchiveProperties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlArchiveProperties.Controls.Add(Me.Label4)
        Me.pnlArchiveProperties.Controls.Add(Me.txt_DEFAULT_ArchiveName)
        Me.pnlArchiveProperties.Controls.Add(Me.btnBrowseBuildFolder)
        Me.pnlArchiveProperties.Controls.Add(Me.txt_DEFAULT_BuildFolder)
        Me.pnlArchiveProperties.Controls.Add(Me.Label3)
        Me.pnlArchiveProperties.Controls.Add(Me.Label2)
        Me.pnlArchiveProperties.Controls.Add(Me.btnBrowseExtractFolder)
        Me.pnlArchiveProperties.Controls.Add(Me.txt_DEFAULT_DestinationRootFolder)
        Me.pnlArchiveProperties.Location = New System.Drawing.Point(6, 48)
        Me.pnlArchiveProperties.Name = "pnlArchiveProperties"
        Me.pnlArchiveProperties.Size = New System.Drawing.Size(474, 182)
        Me.pnlArchiveProperties.TabIndex = 18
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 2)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(466, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Archive name"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txt_DEFAULT_ArchiveName
        '
        Me.txt_DEFAULT_ArchiveName.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DEFAULT_ArchiveName.Location = New System.Drawing.Point(3, 18)
        Me.txt_DEFAULT_ArchiveName.Name = "txt_DEFAULT_ArchiveName"
        Me.txt_DEFAULT_ArchiveName.Size = New System.Drawing.Size(466, 19)
        Me.txt_DEFAULT_ArchiveName.TabIndex = 17
        '
        'btnBrowseBuildFolder
        '
        Me.btnBrowseBuildFolder.BackColor = System.Drawing.Color.Silver
        Me.btnBrowseBuildFolder.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowseBuildFolder.Location = New System.Drawing.Point(2, 84)
        Me.btnBrowseBuildFolder.Name = "btnBrowseBuildFolder"
        Me.btnBrowseBuildFolder.Size = New System.Drawing.Size(74, 24)
        Me.btnBrowseBuildFolder.TabIndex = 16
        Me.btnBrowseBuildFolder.Text = "Browse..."
        Me.btnBrowseBuildFolder.UseVisualStyleBackColor = False
        '
        'txt_DEFAULT_BuildFolder
        '
        Me.txt_DEFAULT_BuildFolder.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DEFAULT_BuildFolder.Location = New System.Drawing.Point(3, 64)
        Me.txt_DEFAULT_BuildFolder.Name = "txt_DEFAULT_BuildFolder"
        Me.txt_DEFAULT_BuildFolder.Size = New System.Drawing.Size(466, 19)
        Me.txt_DEFAULT_BuildFolder.TabIndex = 15
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(466, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Archive build folder"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 118)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(466, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Archive destination root folder"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnBrowseExtractFolder
        '
        Me.btnBrowseExtractFolder.BackColor = System.Drawing.Color.Silver
        Me.btnBrowseExtractFolder.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowseExtractFolder.Location = New System.Drawing.Point(2, 154)
        Me.btnBrowseExtractFolder.Name = "btnBrowseExtractFolder"
        Me.btnBrowseExtractFolder.Size = New System.Drawing.Size(74, 24)
        Me.btnBrowseExtractFolder.TabIndex = 13
        Me.btnBrowseExtractFolder.Text = "Browse..."
        Me.btnBrowseExtractFolder.UseVisualStyleBackColor = False
        '
        'txt_DEFAULT_DestinationRootFolder
        '
        Me.txt_DEFAULT_DestinationRootFolder.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_DEFAULT_DestinationRootFolder.Location = New System.Drawing.Point(3, 134)
        Me.txt_DEFAULT_DestinationRootFolder.Name = "txt_DEFAULT_DestinationRootFolder"
        Me.txt_DEFAULT_DestinationRootFolder.Size = New System.Drawing.Size(466, 19)
        Me.txt_DEFAULT_DestinationRootFolder.TabIndex = 11
        '
        'pnlButtons
        '
        Me.pnlButtons.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.pnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlButtons.Controls.Add(Me.btnSave)
        Me.pnlButtons.Controls.Add(Me.btnCancel)
        Me.pnlButtons.Location = New System.Drawing.Point(18, 342)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(474, 48)
        Me.pnlButtons.TabIndex = 17
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(3, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(118, 40)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(351, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(118, 40)
        Me.btnCancel.TabIndex = 11
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'grpboxApp
        '
        Me.grpboxApp.Controls.Add(Me.Panel1)
        Me.grpboxApp.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpboxApp.Location = New System.Drawing.Point(12, 12)
        Me.grpboxApp.Name = "grpboxApp"
        Me.grpboxApp.Size = New System.Drawing.Size(486, 53)
        Me.grpboxApp.TabIndex = 20
        Me.grpboxApp.TabStop = False
        Me.grpboxApp.Text = "App settings"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.chk_APP_Debugging)
        Me.Panel1.Location = New System.Drawing.Point(6, 21)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(474, 23)
        Me.Panel1.TabIndex = 19
        '
        'chk_APP_Debugging
        '
        Me.chk_APP_Debugging.AutoSize = True
        Me.chk_APP_Debugging.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.chk_APP_Debugging.Location = New System.Drawing.Point(3, 3)
        Me.chk_APP_Debugging.Name = "chk_APP_Debugging"
        Me.chk_APP_Debugging.Size = New System.Drawing.Size(279, 17)
        Me.chk_APP_Debugging.TabIndex = 14
        Me.chk_APP_Debugging.Text = "Show extra debugging info in the archive log file"
        Me.chk_APP_Debugging.UseVisualStyleBackColor = True
        '
        'frmOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Silver
        Me.ClientSize = New System.Drawing.Size(510, 409)
        Me.Controls.Add(Me.grpboxApp)
        Me.Controls.Add(Me.pnlButtons)
        Me.Controls.Add(Me.grpboxDefaults)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmOptions"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Options"
        Me.grpboxDefaults.ResumeLayout(False)
        Me.pnlShowUI.ResumeLayout(False)
        Me.pnlShowUI.PerformLayout()
        Me.pnlCheckCRC.ResumeLayout(False)
        Me.pnlCheckCRC.PerformLayout()
        Me.pnlArchiveProperties.ResumeLayout(False)
        Me.pnlArchiveProperties.PerformLayout()
        Me.pnlButtons.ResumeLayout(False)
        Me.grpboxApp.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grpboxDefaults As GroupBox
    Friend WithEvents pnlShowUI As Panel
    Friend WithEvents chk_DEFAULT_ShowUI As CheckBox
    Friend WithEvents pnlCheckCRC As Panel
    Friend WithEvents chk_DEFAULT_CheckCRC As CheckBox
    Friend WithEvents pnlArchiveProperties As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents txt_DEFAULT_ArchiveName As TextBox
    Friend WithEvents btnBrowseBuildFolder As Button
    Friend WithEvents txt_DEFAULT_BuildFolder As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnBrowseExtractFolder As Button
    Friend WithEvents txt_DEFAULT_DestinationRootFolder As TextBox
    Friend WithEvents pnlButtons As Panel
    Friend WithEvents btnSave As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents grpboxApp As GroupBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents chk_APP_Debugging As CheckBox
    Friend WithEvents folderBrowserDialogBuild As FolderBrowserDialog
    Friend WithEvents folderBrowserDialogExtract As FolderBrowserDialog
End Class
