﻿Imports System.ComponentModel
Imports System.IO
Imports System.IO.Compression
Imports System.Reflection
Imports System.Runtime.InteropServices.WindowsRuntime
Imports System.Web.UI

Public Class frmMain
    Private _SelfExtractorCompiler As New SelfExtractorCompiler
    Public WithEvents _PackedFiles As New PackedFiles

    Private _dragItems As String() = {}

    Private mSelectedNodeKey As String = ""

    Private mSelectedFileName As String = ""
    Private mSelectedFileIndex As Integer = -1

    Private mLastIndexBeforeChange As Integer = -1


    Private lvwColumnSorter As ListViewColumnSorter


    Private mFoldersRootNode As TreeNode


    Private WithEvents mFrmBusy_FileList As New frmBusyFileList
    Private WithEvents mFrmBusy_BuildArchive As New frmBusyBuildArchive


#Region "thread safe"
#Region "buttons"
    Delegate Sub _ThreadSafe_Delegate__btnArchiveSettings_Enabled(ByVal [value] As Boolean)
    Delegate Sub _ThreadSafe_Delegate__btnBuildArchive_Enabled(ByVal [value] As Boolean)

    Public Sub _ThreadSafe__btnArchiveSettings_Enabled(ByVal [value] As Boolean)
        If Me.btnArchiveSettings.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__btnArchiveSettings_Enabled(AddressOf _ThreadSafe__btnArchiveSettings_Enabled)

            Me.Invoke(d, New Object() {[value]})
        Else
            btnArchiveSettings.Enabled = [value]

            If [value] Then
                btnArchiveSettings.FlatAppearance.BorderColor = Color.DarkGreen
                btnArchiveSettings.FlatAppearance.BorderSize = 2

                btnArchiveSettings.Font = New Font("Segoe UI", 16, FontStyle.Bold)
            Else
                btnArchiveSettings.FlatAppearance.BorderColor = Color.DarkRed
                btnArchiveSettings.FlatAppearance.BorderSize = 1

                btnArchiveSettings.Font = New Font("Segoe UI", 12, FontStyle.Regular)
            End If
        End If
    End Sub

    Public Sub _ThreadSafe__btnBuildArchive_Enabled(ByVal [value] As Boolean)
        If Me.btnBuildArchive.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__btnBuildArchive_Enabled(AddressOf _ThreadSafe__btnBuildArchive_Enabled)

            Me.Invoke(d, New Object() {[value]})
        Else
            btnBuildArchive.Enabled = [value]

            If [value] Then
                btnBuildArchive.FlatAppearance.BorderColor = Color.DarkGreen
                btnBuildArchive.FlatAppearance.BorderSize = 2

                btnBuildArchive.Font = New Font("Segoe UI", 16, FontStyle.Bold)
            Else
                btnBuildArchive.FlatAppearance.BorderColor = Color.DarkRed
                btnBuildArchive.FlatAppearance.BorderSize = 1

                btnBuildArchive.Font = New Font("Segoe UI", 12, FontStyle.Regular)
            End If

            BuildToolStripMenuItem.Enabled = [value]
        End If
    End Sub
#End Region

#Region "treeFolders"
    Delegate Sub _ThreadSafe_Delegate__treeFolders_Nodes_Clear()
    Delegate Function _ThreadSafe_Delegate__treeFolders_Nodes_Find(ByVal [key] As String, ByVal [searchAllChildren] As Boolean) As TreeNode()
    Delegate Function _ThreadSafe_Delegate__treeFolders_Nodes_Add(ByVal [key] As String, ByVal [text] As String) As TreeNode
    Delegate Function _ThreadSafe_Delegate__treeFolders_Nodes_AddNode(ByVal [node] As TreeNode) As Integer
    Delegate Sub _ThreadSafe_Delegate__treeFolders_Set_Selected_Node(ByVal [node] As TreeNode)
    Delegate Function _ThreadSafe_Delegate__treeFolders_Nodes_Root() As TreeNode
    Delegate Sub _ThreadSafe_Delegate__treeFolders_Expand_All()
    Delegate Function _ThreadSafe_Delegate__treeFolders_SelectedNode_Name() As String


    Public Sub _ThreadSafe__treeFolders_Nodes_Clear()
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__treeFolders_Nodes_Clear(AddressOf _ThreadSafe__treeFolders_Nodes_Clear)

            Me.Invoke(d, New Object() {})
        Else
            Me.treeFolders.Nodes.Clear()
        End If
    End Sub

    Public Function _ThreadSafe__treeFolders_Nodes_Find(ByVal [key] As String, ByVal [searchAllChildren] As Boolean) As TreeNode()
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__treeFolders_Nodes_Find(AddressOf _ThreadSafe__treeFolders_Nodes_Find)

            Return DirectCast(Me.Invoke(d, New Object() {[key], [searchAllChildren]}), TreeNode())
        Else
            Return treeFolders.Nodes.Find([key], [searchAllChildren])
        End If
    End Function

    Public Function _ThreadSafe__treeFolders_Nodes_Add(ByVal [key] As String, ByVal [text] As String) As TreeNode
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__treeFolders_Nodes_Add(AddressOf _ThreadSafe__treeFolders_Nodes_Add)

            Return DirectCast(Me.Invoke(d, New Object() {[key], [text]}), TreeNode)
        Else
            Return treeFolders.Nodes.Add([key], [text])
        End If
    End Function

    Public Function _ThreadSafe__treeFolders_Nodes_AddNode(ByVal [node] As TreeNode) As Integer
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__treeFolders_Nodes_AddNode(AddressOf _ThreadSafe__treeFolders_Nodes_AddNode)

            Return DirectCast(Me.Invoke(d, New Object() {[node]}), Integer)
        Else

            Return treeFolders.Nodes.Add([node])
        End If
    End Function

    Public Sub _ThreadSafe__treeFolders_Set_Selected_Node(ByVal [node] As TreeNode)
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__treeFolders_Set_Selected_Node(AddressOf _ThreadSafe__treeFolders_Set_Selected_Node)

            Me.Invoke(d, New Object() {[node]})
        Else
            treeFolders.SelectedNode = [node]
        End If
    End Sub

    Public Function _ThreadSafe__treeFolders_Nodes_Root() As TreeNode
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__treeFolders_Nodes_Root(AddressOf _ThreadSafe__treeFolders_Nodes_Root)

            Return DirectCast(Me.Invoke(d, New Object() {}), TreeNode)
        Else
            Return treeFolders.Nodes(0)
        End If
    End Function

    Public Sub _ThreadSafe__treeFolders_Expand_All()
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__treeFolders_Expand_All(AddressOf _ThreadSafe__treeFolders_Expand_All)

            Me.Invoke(d, New Object() {})
        Else
            Me.treeFolders.ExpandAll()
        End If
    End Sub

    Public Function _ThreadSafe__treeFolders_SelectedNode_Name() As String
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__treeFolders_SelectedNode_Name(AddressOf _ThreadSafe__treeFolders_SelectedNode_Name)

            Return DirectCast(Me.Invoke(d, New Object() {}), String)
        Else
            Return treeFolders.SelectedNode.Name
        End If
    End Function
#End Region

#Region "listviewFiles"
    Delegate Sub _ThreadSafe_Delegate__listviewFiles_Items_Clear()
    Delegate Sub _ThreadSafe_Delegate__listviewFiles_Items_Add(ByVal [listitem] As ListViewItem)


    Public Sub _ThreadSafe__listviewFiles_Items_Clear()
        If Me.listviewFiles.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__listviewFiles_Items_Clear(AddressOf _ThreadSafe__listviewFiles_Items_Clear)

            Me.Invoke(d, New Object() {})
        Else
            Me.listviewFiles.Items.Clear()
        End If
    End Sub

    Public Sub _ThreadSafe__listviewFiles_Items_Add(ByVal [listitem] As ListViewItem)
        If Me.listviewFiles.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__listviewFiles_Items_Add(AddressOf _ThreadSafe__listviewFiles_Items_Add)

            Me.Invoke(d, New Object() {[listitem]})
        Else
            listviewFiles.Items.Add([listitem])
        End If
    End Sub
#End Region

#Region "frmBusy_FileList"
    Delegate Sub _ThreadSafe_Delegate__frmBusyFileList_ProgressBar_Values(ByVal [max] As Integer, ByVal [value] As Integer)
    Delegate Sub _ThreadSafe_Delegate__frmBusyFileList_Progress_Text(ByVal [text] As String)

    Public Sub _ThreadSafe__frmBusyFileList_ProgressBar_Value(ByVal [max] As Integer, ByVal [value] As Integer)
        If mFrmBusy_FileList.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__frmBusyFileList_ProgressBar_Values(AddressOf _ThreadSafe__frmBusyFileList_ProgressBar_Value)

            Me.Invoke(d, New Object() {[max], [value]})
        Else
            If [max] > 0 Then
                If [value] < 0 Then
                    [value] = 0
                End If
                If [value] > [max] Then
                    [value] = [max]
                End If

                mFrmBusy_FileList.ProgressBar_Max = [max]
                mFrmBusy_FileList.ProgressBar_Value = [value]
            Else
                mFrmBusy_FileList.ProgressBar_Value = 0
            End If
        End If
    End Sub

    Public Sub _ThreadSafe__frmBusyFileList_Progress_Text(ByVal [text] As String)
        If Me.listviewFiles.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__frmBusyFileList_Progress_Text(AddressOf _ThreadSafe__frmBusyFileList_Progress_Text)

            Me.Invoke(d, New Object() {[text]})
        Else
            mFrmBusy_FileList.Progress_Text = [text]
            'Console.WriteLine("Progress_Text='" & [text] & "'")
        End If
    End Sub
#End Region

#Region "frmBusy_BuildArchive"
    Delegate Sub _ThreadSafe_Delegate__frmBusyBuildArchive_ProgressBar_Values(ByVal [max] As Integer, ByVal [value] As Integer)
    Delegate Sub _ThreadSafe_Delegate__frmBusyBuildArchive_Progress_Text(ByVal [text] As String)

    Public Sub _ThreadSafe__frmBusyBuildArchive_ProgressBar_Value(ByVal [max] As Integer, ByVal [value] As Integer)
        If mFrmBusy_BuildArchive.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__frmBusyBuildArchive_ProgressBar_Values(AddressOf _ThreadSafe__frmBusyBuildArchive_ProgressBar_Value)

            Me.Invoke(d, New Object() {[max], [value]})
        Else
            If [max] > 0 Then
                If [value] < 0 Then
                    [value] = 0
                End If
                If [value] > [max] Then
                    [value] = [max]
                End If

                mFrmBusy_BuildArchive.ProgressBar_Max = [max]
                mFrmBusy_BuildArchive.ProgressBar_Value = [value]
            Else
                mFrmBusy_BuildArchive.ProgressBar_Value = 0
            End If
        End If
    End Sub

    Public Sub _ThreadSafe__frmBusyBuildArchive_Progress_Text(ByVal [text] As String)
        If mFrmBusy_BuildArchive.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__frmBusyBuildArchive_Progress_Text(AddressOf _ThreadSafe__frmBusyBuildArchive_Progress_Text)

            Me.Invoke(d, New Object() {[text]})
        Else
            mFrmBusy_BuildArchive.Progress_Text = [text]
        End If
    End Sub
#End Region

#End Region


#Region "Methods"

#Region "Folder Tree"
    Private Sub GrowFolderTree()
        Dim newNode As New TreeNode(Shared_Constants.INTERNAL_PATH_SEPARATOR)
        Dim lastNode As TreeNode = Nothing
        Dim subPathAgg As String = ""


        _ThreadSafe__treeFolders_Nodes_Clear()

        _PackedFiles.Sort()
        _PackedFiles.ResetIterator_Path()

        newNode.Nodes.Clear()



        Dim _path As String = ""
        While _PackedFiles.GetNext_Path(_path)
            subPathAgg = ""

            'If _path = Shared_Constants.INTERNAL_PATH_SEPARATOR  Then
            'skip
            'Else
            For Each subPath As String In _path.Split(Shared_Constants.INTERNAL_PATH_SEPARATOR)
                subPathAgg &= subPath & Shared_Constants.INTERNAL_PATH_SEPARATOR

                Dim nodes As TreeNode() = newNode.Nodes.Find(subPathAgg, True)

                If nodes.Length = 0 Then
                    If lastNode Is Nothing Then
                        If subPath.Equals("") Then
                            subPath = Shared_Constants.INTERNAL_PATH_SEPARATOR & "ROOT"
                        End If

                        lastNode = newNode.Nodes.Add(subPathAgg, subPath)
                    Else
                        If subPath.Equals("") Then
                            Continue For
                        End If

                        lastNode = lastNode.Nodes.Add(subPathAgg, subPath)
                    End If
                Else
                    lastNode = nodes(0)
                End If
            Next
            'End If
        End While


        If newNode.Nodes.Count > 0 Then
            _ThreadSafe__treeFolders_Nodes_AddNode(newNode.Nodes(0))
        Else
            _ThreadSafe__treeFolders_Nodes_AddNode(newNode)
        End If


        'set selected node
        Dim nodes_findselected As TreeNode() = _ThreadSafe__treeFolders_Nodes_Find(mSelectedNodeKey, True)
        Dim node_zero As TreeNode = _ThreadSafe__treeFolders_Nodes_Root()

        If nodes_findselected.Length = 0 Then
            _ThreadSafe__treeFolders_Set_Selected_Node(node_zero)
        Else
            _ThreadSafe__treeFolders_Set_Selected_Node(nodes_findselected(0))
        End If


        _ThreadSafe__treeFolders_Expand_All()
    End Sub
#End Region

#Region "File ListView"
    Private Sub FillFileListView()
        Dim pathindex As Integer = 0

        imagelist_listviewFiles.Images.Clear()

        mSelectedFileIndex = -1
        _ThreadSafe__listviewFiles_Items_Clear()


        Try
            pathindex = _PackedFiles.GetPathIndex(_ThreadSafe__treeFolders_SelectedNode_Name(), True)
        Catch ex As Exception

        End Try


        If pathindex < 0 Then
            'oops, not found!

            Exit Sub
        End If

        'set as active path
        _PackedFiles.SetPathIndex(pathindex)


        Dim file As New IconFileInfo

        _PackedFiles.ResetIterator_File()

        While _PackedFiles.GetNext_File(pathindex, file)
            Dim _listviewitem As New ListViewItem(New String() {
                                                  file.Name,
                                                  file.LastWriteTime.ToString,
                                                  FunctionsAndStuff.FilesAndFolders.GetFileTypeFromRegistry(file.Extension),
                                                  FunctionsAndStuff.FilesAndFolders.GetFileSizeHumanReadable(file.Size),
                                                  file.Size})

            imagelist_listviewFiles.Images.Add(file.FullName, file.Icon)
            _listviewitem.ImageIndex = imagelist_listviewFiles.Images.Count - 1

            _ThreadSafe__listviewFiles_Items_Add(_listviewitem)
        End While
    End Sub
#End Region

#End Region


#Region "events"
#Region "treeFolders"
#Region "DrawNode"
    Private Sub treeFolders_DrawNode(ByVal sender As Object, ByVal e As DrawTreeNodeEventArgs) Handles treeFolders.DrawNode
        If (e.State And TreeNodeStates.Selected) <> 0 Then
            e.Graphics.FillRectangle(Brushes.LightGray, e.Bounds)
            TextRenderer.DrawText(e.Graphics, e.Node.Text, e.Node.NodeFont, e.Bounds, Color.Black, Color.Empty, TextFormatFlags.VerticalCenter)
        Else
            e.Graphics.FillRectangle(SystemBrushes.Window, e.Bounds)
            TextRenderer.DrawText(e.Graphics, e.Node.Text, e.Node.NodeFont, e.Bounds, SystemColors.WindowText, Color.Empty, TextFormatFlags.VerticalCenter)
        End If
    End Sub
#End Region

#Region "AfterSelect"
    Private Sub treeFolders_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles treeFolders.AfterSelect
        mSelectedNodeKey = treeFolders.SelectedNode.Name

        Console.WriteLine("mSelectedNodeKey='" & mSelectedNodeKey.ToString & "'")

        FillFileListView()
    End Sub
#End Region

#Region "BeforeCollapse"
    Private Sub treeFolders_BeforeCollapse(sender As Object, e As TreeViewCancelEventArgs) Handles treeFolders.BeforeCollapse
        ' no collapsing pleae
        e.Cancel = True
    End Sub
#End Region

#Region "MouseUp"
    Private Sub treeFolders_MouseUp(sender As Object, e As MouseEventArgs) Handles treeFolders.MouseUp
        If e.Button = MouseButtons.Right Then
            Dim p As Point = New Point(e.X, e.Y)

            Dim node As TreeNode = treeFolders.GetNodeAt(p)
            If Not node Is Nothing Then
                If node.Name.Equals(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
                    'root node, cannot rename/delete
                    ctxmenuitemFoldersRename.Enabled = False
                    ctxmenuitemFoldersDelete.Enabled = False
                Else
                    ctxmenuitemFoldersRename.Enabled = True
                    ctxmenuitemFoldersDelete.Enabled = True
                End If
                'ctxmenuitemFoldersPath.Text = node.Text
                treeFolders.SelectedNode = node
                mSelectedNodeKey = treeFolders.SelectedNode.Name

                If mSelectedNodeKey.Length < 1 Or mSelectedNodeKey.Equals(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
                    ctxmenuitemFoldersRename.Enabled = False
                    ctxmenuitemFoldersDelete.Enabled = False
                Else
                    ctxmenuitemFoldersRename.Enabled = True
                    ctxmenuitemFoldersDelete.Enabled = True
                End If

                ctxmenuFolders.Show(treeFolders, p)
                'MsgBox(node.Text)
            End If
        End If
    End Sub
#End Region

#Region "BeforeLabelEdit"
    Private Sub treeFolders_BeforeLabelEdit(sender As Object, e As NodeLabelEditEventArgs) Handles treeFolders.BeforeLabelEdit
        If e.Node.Name.Equals(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
            e.CancelEdit = True

            Exit Sub
        End If
    End Sub
#End Region

#Region "AfterLabelEdit"
    Private Sub treeFolders_AfterLabelEdit(sender As Object, e As NodeLabelEditEventArgs) Handles treeFolders.AfterLabelEdit
        Dim go_ahead As Boolean = True
        Dim newPath As String = ""

        Do 'fake loop, dirty goto alternative
            If e.Label Is Nothing Then
                go_ahead = False

                Exit Do
            End If

            If e.Node.Name.Equals(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
                go_ahead = False

                Exit Do
            End If

            If e.Label.Length <= 0 Or Not FunctionsAndStuff.FilenameIsOK(e.Label) Then
                go_ahead = False

                Exit Do
            End If


            newPath = _PackedFiles.RenamePath(mSelectedNodeKey, e.Label)
            If newPath.Equals("") Then
                go_ahead = False

                Exit Do
            End If
        Loop While False

        If Not go_ahead Then
            e.CancelEdit = True
            e.Node.BeginEdit()
        Else
            e.Node.EndEdit(False)
            treeFolders.LabelEdit = False

            mSelectedNodeKey = newPath
            If Not mSelectedNodeKey.EndsWith(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
                mSelectedNodeKey &= Shared_Constants.INTERNAL_PATH_SEPARATOR
            End If

            GrowFolderTree()
        End If
    End Sub
#End Region
#End Region

#Region "ctxmenuitemFolders"
#Region "Rename.Click"
    Private Sub ctxmenuitemFoldersRename_Click(sender As Object, e As EventArgs) Handles ctxmenuitemFoldersRename.Click
        If mSelectedNodeKey.Equals("") Then
            Exit Sub
        End If

        If Not treeFolders.SelectedNode.IsEditing Then
            Try
                treeFolders.LabelEdit = True
                treeFolders.SelectedNode.BeginEdit()
            Catch ex As Exception

            End Try
        End If
    End Sub
#End Region

#Region "Delete.Click"
    Private Sub ctxmenuitemFoldersDelete_Click(sender As Object, e As EventArgs) Handles ctxmenuitemFoldersDelete.Click
        If mSelectedNodeKey.Equals("") Then
            Exit Sub
        End If

        If MessageBox.Show("Are you sure you want to delete '" & mSelectedNodeKey & "'?", "Delete warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            If Not _PackedFiles.DeletePath(mSelectedNodeKey) Then
                MsgBox("fail")
            End If

            GrowFolderTree()
            FillFileListView()
        End If
    End Sub
#End Region
#End Region

#Region "Drag and drop"
    Private Sub pnlFolderFiles_DragEnter(sender As Object, e As DragEventArgs) Handles pnlFolderFiles.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Private Sub pnlFolderFiles_DragDrop(sender As Object, e As DragEventArgs) Handles pnlFolderFiles.DragDrop
        _handle_drag_drop(CType(e.Data.GetData(DataFormats.FileDrop), Array))
    End Sub


    Private Sub treeFolders_DragEnter(sender As Object, e As DragEventArgs) Handles treeFolders.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Private Sub treeFolders_DragDrop(sender As Object, e As DragEventArgs) Handles treeFolders.DragDrop
        _handle_drag_drop(CType(e.Data.GetData(DataFormats.FileDrop), Array))
    End Sub


    Private Sub listviewFiles_DragEnter(sender As Object, e As DragEventArgs) Handles listviewFiles.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Private Sub listviewFiles_DragDrop(sender As Object, e As DragEventArgs) Handles listviewFiles.DragDrop
        _handle_drag_drop(CType(e.Data.GetData(DataFormats.FileDrop), Array))
    End Sub


    Private Sub _handle_drag_drop(_items As String())
        Try
            Array.Clear(_dragItems, 0, _dragItems.Length)
        Catch ex As Exception

        End Try

        For i As Integer = 0 To _items.Length - 1
            Console.WriteLine("_handle_drag_drop - file (" & i.ToString & "): " & _items(i))
        Next


        _dragItems = _items

        bgworkerParseFiles.RunWorkerAsync()

        ShowBusyForm_FileList()
    End Sub
#End Region


#Region "form events"
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Width = 1472
        Me.Height = 567

        ResetEverything()


        lvwColumnSorter = New ListViewColumnSorter()
        listviewFiles.ListViewItemSorter = lvwColumnSorter
    End Sub
#End Region

#End Region




#Region "busy"
    Private Sub ShowBusyForm_FileList()
        mFrmBusy_FileList = New frmBusyFileList

        AddHandler mFrmBusy_FileList.eventActionCanceled, AddressOf bgworkerParseFiles_Cancel


        mFrmBusy_FileList.ShowCancelButton = True
        mFrmBusy_FileList.ShowDialog(Me)
    End Sub

    Private Sub HideBusyForm_FileList()
        mFrmBusy_FileList.Close()
        mFrmBusy_FileList.Dispose()
    End Sub

    Private Sub ShowBusyForm_BuildArchive()
        mFrmBusy_BuildArchive = New frmBusyBuildArchive

        AddHandler mFrmBusy_BuildArchive.eventActionCanceled, AddressOf bgworkerBuildArchive_Cancel


        mFrmBusy_BuildArchive.ShowCancelButton = True
        mFrmBusy_BuildArchive.ShowDialog(Me)
    End Sub

    Private Sub HideBusyForm_BuildArchive()
        mFrmBusy_BuildArchive.Close()
        mFrmBusy_BuildArchive.Dispose()
    End Sub

#End Region


    Private Function TranslatePathToArchivePath(root_dir As String, dir As String) As String
        Dim _tdir As String = dir

        _tdir = _tdir.Replace(root_dir, "")
        _tdir = _tdir.Replace(Shared_Constants.NORMAL_PATH_SEPARATOR, Shared_Constants.INTERNAL_PATH_SEPARATOR)
        _tdir = _PackedFiles.GetCurrentPath() & Shared_Constants.INTERNAL_PATH_SEPARATOR & _tdir
        _tdir = _tdir.Replace(Shared_Constants.INTERNAL_PATH_SEPARATOR & Shared_Constants.INTERNAL_PATH_SEPARATOR, Shared_Constants.INTERNAL_PATH_SEPARATOR)

        Return _tdir
    End Function





#Region "archive"
    Private Sub ResetEverything()
        _PackedFiles = New PackedFiles
        _SelfExtractorCompiler = New SelfExtractorCompiler

        _ThreadSafe__treeFolders_Nodes_Clear()
        listviewFiles.Items.Clear()

        _ThreadSafe__btnArchiveSettings_Enabled(False)
        _ThreadSafe__btnBuildArchive_Enabled(False)

        Settings.Reset()
        Settings.DestinationPath = ""
        Settings.ArchivePath = "D:\Temp\___test__test_test\__________TEST\_build"
        Settings.ArchiveName = "TESTNAME"

        FileList.Reset()


        '-----------------------
        AddHandler _PackedFiles.ContentsChanged, AddressOf PackedFiles_OnContentsChanged
        _PackedFiles.Reset()
        _PackedFiles.AddPath(Shared_Constants.INTERNAL_PATH_SEPARATOR)
        _PackedFiles.SetPathIndex(0)


        mLastIndexBeforeChange = -1


        GrowFolderTree()
    End Sub

    Private Sub UndoLastChange()
        If mLastIndexBeforeChange < 0 Then
            ResetEverything()
        Else
            _PackedFiles.UndoNewPaths(mLastIndexBeforeChange)
        End If
    End Sub

    Private Sub PackedFiles_OnContentsChanged(PathCount As Integer, FileCount As Integer)
        If PathCount > 0 And FileCount > 0 Then
            _ThreadSafe__btnArchiveSettings_Enabled(True)

            If Settings.Saved Then
                _ThreadSafe__btnBuildArchive_Enabled(True)
            Else
                _ThreadSafe__btnBuildArchive_Enabled(False)
            End If
        Else
            _ThreadSafe__btnArchiveSettings_Enabled(False)
            _ThreadSafe__btnBuildArchive_Enabled(False)
        End If
    End Sub

    Private Sub BuildArchive()
        FileList.Reset()
        Identifier.Reset()

        Identifier.Guid = FunctionsAndStuff.Assembly.Guid
        Identifier.Version = FunctionsAndStuff.Assembly.Version


        Dim fileTotal As Integer = _PackedFiles.Count_Files_All, fileCounter As Integer = fileTotal


        _ThreadSafe__frmBusyBuildArchive_ProgressBar_Value(fileTotal, fileCounter)
        _ThreadSafe__frmBusyBuildArchive_Progress_Text("init")


        _SelfExtractorCompiler.CreateTempZipFileName()

        _SelfExtractorCompiler.AddIdentifierFile()
        _SelfExtractorCompiler.AddSettingsFile()



        _PackedFiles.ResetIterator_Path()
        _PackedFiles.ResetIterator_File()

        Dim _path As String = "", _file As IconFileInfo = Nothing, _index As Integer = -1
        While _PackedFiles.GetNext_Path(_path)
            _index = _PackedFiles.GetPathIndex(_path)

            If _index > -1 Then
                While _PackedFiles.GetNext_File(_index, _file)
                    fileCounter -= 1
                    _ThreadSafe__frmBusyBuildArchive_ProgressBar_Value(fileTotal, fileCounter)
                    _ThreadSafe__frmBusyBuildArchive_Progress_Text(_file.FullName)

                    _SelfExtractorCompiler.AddFile(_file.FullName, _file.Name, _path)
                End While
            End If
        End While

        _SelfExtractorCompiler.AddFileListFile()


        _ThreadSafe__frmBusyBuildArchive_ProgressBar_Value(1, 0)
        _ThreadSafe__frmBusyBuildArchive_Progress_Text("compiling self extractor")

        If IO.File.Exists(Settings.IconPath) Then
            _SelfExtractorCompiler.CompileArchive(IO.Path.Combine(Settings.ArchivePath, Settings.ArchiveName & ".exe"), Settings.IconPath)
        Else
            _SelfExtractorCompiler.CompileArchive(IO.Path.Combine(Settings.ArchivePath, Settings.ArchiveName & ".exe"))
        End If

        _SelfExtractorCompiler.Dispose()
    End Sub
#End Region


















#Region "top menu & context menu"
    Private Sub NewToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewToolStripMenuItem.Click
        If MessageBox.Show("Are you sure?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            ResetEverything()
        End If
    End Sub

    Private Sub btnArchiveSettings_Click(sender As Object, e As EventArgs) Handles btnArchiveSettings.Click
        frmExtraSettings.ShowDialog(Me)

        If Settings.Saved Then
            _ThreadSafe__btnBuildArchive_Enabled(True)
        Else
            _ThreadSafe__btnBuildArchive_Enabled(False)
        End If
    End Sub

    Private Sub btnBuildArchive_Click(sender As Object, e As EventArgs) Handles btnBuildArchive.Click
        bgworkerBuildArchive.RunWorkerAsync()

        ShowBusyForm_BuildArchive()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        frmAbout.ShowDialog(Me)
    End Sub

    Private Sub OptionsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OptionsToolStripMenuItem.Click
        frmOptions.ShowDialog(Me)
    End Sub

    Private Sub listviewFiles_MouseUp(sender As Object, e As MouseEventArgs) Handles listviewFiles.MouseUp
        If e.Button = MouseButtons.Right Then
            Dim p As Point = New Point(e.X, e.Y)
            Dim l As ListViewItem

            l = listviewFiles.GetItemAt(e.X, e.Y)

            If l Is Nothing Then
                Exit Sub
            End If

            mSelectedFileIndex = l.Index
            mSelectedFileName = l.SubItems.Item(0).Text

            ctxmenuFiles.Show(listviewFiles, p)
        End If
    End Sub

    Private Sub ctxmenuitemFilesRename_Click(sender As Object, e As EventArgs) Handles ctxmenuitemFilesRename.Click

        Try
            listviewFiles.LabelEdit = True
            Try
                listviewFiles.SelectedItems.Item(0).BeginEdit()
            Catch ex As Exception

            End Try

        Catch ex As Exception

        End Try
    End Sub

    Private Sub listviewFiles_AfterLabelEdit(sender As Object, e As LabelEditEventArgs) Handles listviewFiles.AfterLabelEdit
        Dim go_ahead As Boolean = True
        Dim newPath As String = ""

        Do 'fake loop, dirty goto alternative
            If e.Label Is Nothing Then
                go_ahead = False

                Exit Do
            End If

            If e.Label.Length <= 0 Or Not FunctionsAndStuff.FilenameIsOK(e.Label) Then
                go_ahead = False

                Exit Do
            End If


            If Not _PackedFiles.RenameFile(mSelectedNodeKey, mSelectedFileIndex, e.Label) Then
                go_ahead = False

                Exit Do
            End If
        Loop While False

        If Not go_ahead Then
            e.CancelEdit = True

            Try
                listviewFiles.SelectedItems.Item(0).BeginEdit()
            Catch ex As Exception

            End Try
        Else
            treeFolders.LabelEdit = False

            FillFileListView()
        End If
    End Sub

    Private Sub ctxmenuitemFilesDelete_Click(sender As Object, e As EventArgs) Handles ctxmenuitemFilesDelete.Click
        If MessageBox.Show("Are you sure you want to delete '" & mSelectedFileName & "'?", "Delete warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            If Not _PackedFiles.DeleteFile(mSelectedNodeKey, mSelectedFileIndex) Then
                MsgBox("fail")
            End If

            FillFileListView()
        End If
    End Sub

    Private Sub ctxmenuitemFoldersCreate_Textbox_LostFocus(sender As Object, e As EventArgs) Handles ctxmenuitemFoldersCreate_Textbox.LostFocus
        ctxmenuitemFoldersCreate_Textbox.Clear()
    End Sub

    Private Sub ctxmenuitemFoldersCreate_Textbox_KeyDown(sender As Object, e As KeyEventArgs) Handles ctxmenuitemFoldersCreate_Textbox.KeyDown
        If e.KeyData = Keys.Enter Then
            If FunctionsAndStuff.FilenameIsOK(ctxmenuitemFoldersCreate_Textbox.Text) Then
                If _PackedFiles.Count_Paths = 0 Then
                    'uh oh, no root node
                    _PackedFiles.AddPath(Shared_Constants.INTERNAL_PATH_SEPARATOR)
                End If

                If _PackedFiles.AddPath(ctxmenuitemFoldersCreate_Textbox.Text, False, True) Then
                    GrowFolderTree()

                    ctxmenuitemFoldersCreate_Textbox.Clear()
                    ctxmenuFolders.Hide()

                    Exit Sub
                End If
            End If

            ctxmenuitemFoldersCreate_Textbox.SelectAll()
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Environment.ExitCode = 0

        Me.Close()
    End Sub


#End Region

#Region "background workers"
    Private Sub bgworkerParseFiles_Cancel()
        Console.WriteLine("frmMain.bgworkerParseFiles_Cancel")

        bgworkerParseFiles.CancelAsync()
    End Sub

    Private Sub bgworkerParseFiles_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgworkerParseFiles.DoWork
        mLastIndexBeforeChange = _PackedFiles.Count_Paths - 1

        Dim directories_stack As New Stack(Of String)
        Dim root_dir As String = "", root_dir_count As Integer = -1, root_dir_count_min As Integer = -1

        Dim fileCountTotal As Integer = 0
        Dim fileCounter As Integer = 0

        Dim watch As Stopwatch = Stopwatch.StartNew()


        _ThreadSafe__frmBusyFileList_ProgressBar_Value(_dragItems.Length, _dragItems.Length)
        _ThreadSafe__frmBusyFileList_Progress_Text("collecting data and counting stuff")

        For i As Integer = 0 To _dragItems.Length - 1
            If bgworkerParseFiles.CancellationPending Then
                e.Cancel = True
                Exit Sub
            End If

            Try
                If IO.Directory.Exists(_dragItems(i)) Then
                    'is directory, throw it on the stack

                    directories_stack.Push(_dragItems(i))

                    'Count dir depth
                    root_dir_count = _dragItems(i).Split("\").Length

                    If root_dir_count < root_dir_count_min Or root_dir_count_min < 0 Then
                        root_dir_count_min = root_dir_count
                        root_dir = _dragItems(i)
                    End If

                    fileCountTotal += FunctionsAndStuff.FilesAndFolders.FileCountRecursive(_dragItems(i))
                ElseIf IO.File.Exists(_dragItems(i)) Then
                    'is file, add to active directory
                    If _PackedFiles.Count_Paths <= 0 Then
                        _PackedFiles.AddPath(Shared_Constants.INTERNAL_PATH_SEPARATOR)
                    End If

                    _PackedFiles.AddFile(_dragItems(i))
                    fileCountTotal += 1
                    _ThreadSafe__frmBusyFileList_Progress_Text(_dragItems(i))
                Else
                    'huh?
                End If
            Catch ex As Exception

            End Try

            _ThreadSafe__frmBusyFileList_ProgressBar_Value(_dragItems.Length, _dragItems.Length - (i + 1))
        Next


        '----------------------------------------------------------------------------------
        'process directories


        'edit root dir
        If directories_stack.Count > 0 Then
            Try
                root_dir = IO.Directory.GetParent(root_dir).FullName
            Catch ex As Exception
                MsgBox(ex.Message)

                Exit Sub
            End Try


            fileCounter = fileCountTotal
            _ThreadSafe__frmBusyFileList_ProgressBar_Value(fileCountTotal, fileCounter)


            ' Continue processing for each stacked directory
            Do While (directories_stack.Count > 0)
                If bgworkerParseFiles.CancellationPending Then
                    e.Cancel = True
                    Exit Sub
                End If

                ' Get top directory string
                Dim _dir As String = directories_stack.Pop
                Dim _translated_dir As String = TranslatePathToArchivePath(root_dir, _dir)


                Try
                    _PackedFiles.AddPath(_translated_dir)

                    For Each _file As String In IO.Directory.GetFiles(_dir, "*.*")
                        fileCounter -= 1
                        _ThreadSafe__frmBusyFileList_ProgressBar_Value(fileCountTotal, fileCounter)
                        _ThreadSafe__frmBusyFileList_Progress_Text(_file)

                        _PackedFiles.AddFile(_translated_dir, _file)
                    Next


                    ' Loop through all subdirectories and add them to the stack.
                    For Each _subdir As String In IO.Directory.GetDirectories(_dir)
                        directories_stack.Push(_subdir)
                    Next
                Catch ex As Exception

                End Try


            Loop
        End If


        Settings.TotalSizeInArchive = _PackedFiles.TotalSize


        watch.Stop()

        Console.WriteLine("bgworkerParseFiles_DoWork time elapsed: " & watch.ElapsedMilliseconds.ToString)
    End Sub

    Private Sub bgworkerParseFiles_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworkerParseFiles.RunWorkerCompleted
        If e.Cancelled Then
            HideBusyForm_FileList()

            If mLastIndexBeforeChange > -1 Then
                UndoLastChange()
            Else
                ResetEverything()
            End If
        Else
            bgworkerFillLists.RunWorkerAsync()
        End If
    End Sub

    Private Sub bgworkerFillLists_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgworkerFillLists.DoWork
        GrowFolderTree()

        FillFileListView()
    End Sub

    Private Sub bgworkerFillLists_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworkerFillLists.RunWorkerCompleted
        HideBusyForm_FileList()
    End Sub


    Private Sub bgworkerBuildArchive_Cancel()
        Console.WriteLine("frmMain.bgworkerBuildArchive_Cancel")

        bgworkerBuildArchive.CancelAsync()
    End Sub

    Private Sub bgworkerBuildArchive_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgworkerBuildArchive.DoWork
        BuildArchive()
    End Sub

    Private Sub bgworkerBuildArchive_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworkerBuildArchive.RunWorkerCompleted
        HideBusyForm_BuildArchive()
    End Sub
#End Region

    Private Sub listviewFiles_ColumnWidthChanging(sender As Object, e As ColumnWidthChangingEventArgs) Handles listviewFiles.ColumnWidthChanging
        e.Cancel = True
        e.NewWidth = Me.listviewFiles.Columns(e.ColumnIndex).Width
    End Sub

    Private Sub listviewFiles_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles listviewFiles.ColumnClick
        Dim _column As Integer

        If e.Column = 3 Then
            _column = 4
        Else
            _column = e.Column
        End If

        If (_column = lvwColumnSorter.SortColumn) Then
            ' Reverse the current sort direction for this column.
            If (lvwColumnSorter.Order = SortOrder.Ascending) Then
                lvwColumnSorter.Order = SortOrder.Descending
            Else
                lvwColumnSorter.Order = SortOrder.Ascending
            End If
        Else
            ' Set the column number that is to be sorted; default to ascending.
            lvwColumnSorter.SortColumn = _column
            lvwColumnSorter.Order = SortOrder.Ascending
        End If


        ' Perform the sort with these new sort options.
        listviewFiles.Sort()
    End Sub

    Private Sub BuildToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BuildToolStripMenuItem.Click
        If btnBuildArchive.Enabled Then
            btnBuildArchive_Click(btnBuildArchive, New EventArgs)
        End If
    End Sub

    Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
        _LOADEXISTINGARCHIVE_OpenFileDialog()
    End Sub


#Region "LOAD EXISTING ARCHIVE"
    Private Sub _LOADEXISTINGARCHIVE_OpenFileDialog()
        dlgOpenFile.ShowDialog()
    End Sub


    Private Sub _LOADEXISTINGARCHIVE_dlgOpenFile_FileOk(sender As Object, e As CancelEventArgs) Handles dlgOpenFile.FileOk
        Dim _ret As String = _LOADEXISTINGARCHIVE_IsExeAnArchive(dlgOpenFile.FileName)

        If _ret.Equals(Shared_Constants.STANDARD_OK_REPLY_STRING_THINGY) Then
            If _PackedFiles.IsUpdated Then

                'There's already stuff
                If MessageBox.Show(Me, "Any unsaved changes to '" & Settings.ArchiveName & "' will be lost! Are you sure?",
                    Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then

                    Exit Sub
                End If
            End If

            _LOADEXISTINGARCHIVE_LoadArchive(dlgOpenFile.FileName)
        Else
            If MessageBox.Show(Me, _ret, Application.ProductName, MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation) = DialogResult.Retry Then
                e.Cancel = True
            End If
        End If
    End Sub

    Private Function _LOADEXISTINGARCHIVE_LoadArchive(sPath As String) As Boolean
        Try
            ResetEverything()


            Dim ass As Assembly = Assembly.LoadFile(sPath)
            Dim rn As String() = ass.GetManifestResourceNames()


            Identifier.Reset()
            FileList.Reset()
            Settings.Reset()


            For Each name As String In rn
                If name.Equals(Shared_Constants.IdentifierFileName) Then
                    'identifier
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        Identifier.Parse(streamText)

                        reader.Close()
                    End Using
                End If

                If name.Equals(Shared_Constants.SettingsFileName) Then
                    'settings
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        Dim bSettingsParsed As Boolean = False
                        Try
                            bSettingsParsed = Settings.Parse(streamText)
                        Catch ex As Exception

                        End Try

                        If Not bSettingsParsed Then
                            Try
                                bSettingsParsed = Settings.Parse(streamText, False)
                            Catch ex As Exception

                            End Try
                        End If

                        If Not bSettingsParsed Then
                            Throw New Exception("Could not parse internal config")
                        End If


                        Settings.Saved = True

                        reader.Close()
                    End Using
                End If

                If name.Equals(Shared_Constants.FileListFileName) Then
                    'filelist
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        FileList.Parse(streamText)

                        reader.Close()
                    End Using
                End If
            Next


            If Not rn.Contains(Settings.InternalZipFileName) Then
                Throw New Exception("Could not find internal zip file resource ('" & Settings.InternalZipFileName & "') in assembly!")
            End If

            If Settings.Saved Then
                'unpack to temp location
                Dim rs As Stream = ass.GetManifestResourceStream(Settings.InternalZipFileName)

                Dim tempZipPath As String = IO.Path.Combine(IO.Path.GetTempPath(), Settings.InternalZipFileName)
                'Dim lFiles As New List(Of String)
                Dim lPaths As New List(Of String)


                If Not IO.Directory.Exists(tempZipPath) Then
                    IO.Directory.CreateDirectory(tempZipPath)
                End If


                Using _zip As ZipArchive = New ZipArchive(rs)
                    For Each entry As ZipArchiveEntry In _zip.Entries
                        Try
                            Dim entryFullName As String = entry.FullName

                            If entryFullName.StartsWith(Shared_Constants.NORMAL_PATH_SEPARATOR) Then
                                If entryFullName.Length > 1 Then
                                    entryFullName = entryFullName.Substring(1, entryFullName.Length - 1)
                                End If
                            End If

                            Dim _file As String = IO.Path.Combine(tempZipPath, entryFullName)
                            Dim _path As String = IO.Path.GetDirectoryName(_file)


                            Console.WriteLine("_file: '" & _file & "' - _path: '" & _path & "'")
                            If Not lPaths.Contains(_path) Then
                                lPaths.Add(_path)
                            End If


                            If Not IO.Directory.Exists(_path) Then
                                IO.Directory.CreateDirectory(_path)
                            End If

                            entry.ExtractToFile(_file, True)

                            'lFiles.Add(_file)
                        Catch ex As Exception


                        End Try
                    Next
                End Using


                'import files into program
                'lPaths.AddRange(lFiles)
                _handle_drag_drop(lPaths.ToArray)
            End If


            Return True
        Catch ex As Exception
            MsgBox("_LOADEXISTINGARCHIVE_LoadArchive('" & sPath & "') exception: " & ex.Message)

            Return False
        End Try
    End Function

    Private Function _LOADEXISTINGARCHIVE_IsExeAnArchive(sPath As String) As String
        Try
            Dim ass As Assembly = Assembly.LoadFile(sPath)


            Identifier.Reset()


            Dim rn As String() = ass.GetManifestResourceNames()

            For Each _rn As String In rn
                If _rn.Equals(Shared_Constants.IdentifierFileName) Then
                    Dim rs As Stream = ass.GetManifestResourceStream(_rn)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        Identifier.Parse(streamText)

                        reader.Close()
                    End Using

                    Exit For
                End If
            Next


            If Identifier.Guid.Equals(FunctionsAndStuff.Assembly.Guid) Then
                Return Shared_Constants.STANDARD_OK_REPLY_STRING_THINGY
            Else
                If Identifier.Guid.Equals("") Then
                    Return "Not a proper archive (No identifier found)"
                Else
                    Return "Not a proper archive (Guid does not match)"
                End If
            End If
        Catch ex As Exception
            Return "Not a proper archive"
        End Try
    End Function

    Private Sub listviewFiles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listviewFiles.SelectedIndexChanged

    End Sub
End Class
#End Region

