﻿Imports System
Imports System.IO
Imports System.Windows.Forms

Public Class Logger
    Private Shared _initialised As Boolean = False

    Private Shared _logfile As String = ""

    Public Enum LoggerTypes
        Debug = -1
        Message = 0
        Warning = 1
        NormalError = 2
        FatalError = 3
    End Enum

    Public Shared Function WriteDebug(_msg As String) As Boolean
        Return _write(_msg, LoggerTypes.Debug, True)
    End Function

    Public Shared Function WriteMessage(_msg As String) As Boolean
        Return _write(_msg, LoggerTypes.Message, True)
    End Function

    Public Shared Function WriteWarning(_msg As String) As Boolean
        Return _write(_msg, LoggerTypes.Warning, True)
    End Function

    Public Shared Function WriteError(_msg As String) As Boolean
        Return _write(_msg, LoggerTypes.NormalError, True)
    End Function

    Public Shared Function WriteFatalError(_msg As String) As Boolean
        Return _write(_msg, LoggerTypes.FatalError, True)
    End Function

    Public Shared Function WriteWithoutPrefix(_msg As String) As Boolean
        Return _write(_msg, LoggerTypes.Message, False)
    End Function


    Private Shared Function _init() As Boolean
        If _initialised Then
            Return _initialised
        End If

        'Dim sPath As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), Application.ProductName)
        Dim sPath As String = "C:\"
        Dim sFile As String = Application.ProductName & "__" & Date.Now.ToString("yyyy-MM-dd_HHmmss") & ".log"

        Try
            'If Not IO.Directory.Exists(sPath) Then
            '    IO.Directory.CreateDirectory(sPath)
            'End If

            _logfile = IO.Path.Combine(sPath, sFile)


            _initialised = True
        Catch ex As Exception
            _initialised = False
        End Try

        Return _initialised
    End Function

    Private Shared Function _write(_msg As String, _type As LoggerTypes, _prefix As Boolean) As Boolean
        If Not _init() Then
            Return False
        End If

        Try
            Dim sPrefix As String = _type_to_string(_type)
            Dim sDate As String = Date.Now.ToString("yyyy-MM-dd HH:mm:ss")

            Dim sLine As String = ""


            If _prefix Then
                sLine = sPrefix & " " & sDate & " - " & _msg
            Else
                sLine = _msg
            End If

            '-------------------------------------------------------------------------------------------------------
            Using sw As New IO.StreamWriter(IO.File.Open(_logfile, FileMode.Append))
                sw.WriteLine(sLine)
            End Using

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Shared Function _type_to_string(_type As LoggerTypes) As String
        Select Case _type
            Case LoggerTypes.Debug
                Return "[d]"
            Case LoggerTypes.FatalError
                Return "[F]"
            Case LoggerTypes.Message
                Return "[ ]"
            Case LoggerTypes.NormalError
                Return "[E]"
            Case LoggerTypes.Warning
                Return "[w]"
            Case Else
                Return ""
        End Select
    End Function
End Class
