﻿Imports System
Imports System.IO
Imports System.IO.Compression
Imports System.Reflection
Imports System.Windows.Forms

Module modMain
    Public Sub Main()
        Logger.WriteMessage("Started")

        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


        Logger.WriteMessage("Reading config")
        If ReadConfiguration() Then
            Logger.WriteMessage("ok")

            If Settings.Debugging Then
                Logger.WriteDebug("Settings dump")
                Logger.WriteWithoutPrefix(" ")
                Logger.WriteWithoutPrefix(Settings.Dump(False))
                Logger.WriteWithoutPrefix(" ")
                Logger.WriteWithoutPrefix(" ")
                Logger.WriteDebug("FileList dump")
                Logger.WriteWithoutPrefix(" ")
                Logger.WriteWithoutPrefix(FileList.Dump)
                Logger.WriteWithoutPrefix(" ")
                Logger.WriteWithoutPrefix(" ")
            End If

            'Settings.


            If Settings.ShowUI Then
                If Settings.Debugging Then Logger.WriteMessage("Showing form")

                Dim frm As New frmUnpack
                Application.Run(frm)

                If Settings.Debugging Then Logger.WriteMessage("Form closed")
            Else
                Extractor.Extract.DoIt()

                Try
                    Directory.SetCurrentDirectory(Settings.DestinationPath)
                Catch e As DirectoryNotFoundException
                    Logger.WriteError("Could not set current directory to '" & Settings.DestinationPath & "'")
                End Try

                Extractor.Run.PostExtractingExe()
            End If
        Else
            Logger.WriteFatalError("FAILED")
        End If





        Environment.Exit(ExitCode.GetValue())
    End Sub

    Private Function ReadConfiguration() As Boolean
        Settings.Reset()
        FileList.Reset()
        Identifier.Reset()

        Dim ass As Assembly = Assembly.GetExecutingAssembly()
        Dim res As String() = ass.GetManifestResourceNames()

        Try
            For Each name As String In res
                If name.Equals(Shared_Constants.IdentifierFileName) Then
                    'identifier
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        Identifier.Parse(streamText)

                        reader.Close()
                    End Using
                End If

                If name.Equals(Shared_Constants.SettingsFileName) Then
                    'settings
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        Settings.Parse(streamText)

                        reader.Close()
                    End Using
                End If

                If name.Equals(Shared_Constants.FileListFileName) Then
                    'filelist
                    Dim rs As Stream = ass.GetManifestResourceStream(name)

                    Using gzip As Stream = New GZipStream(rs, CompressionMode.Decompress, True)
                        Dim reader As New StreamReader(gzip)
                        Dim streamText As String = reader.ReadToEnd()

                        FileList.Parse(streamText)

                        reader.Close()
                    End Using
                End If
            Next

            Return True
        Catch ex As Exception
            ExitCode.SetValue(ExitCode.ExitCodes.SETTINGS_NOT_LOADED)

            Logger.WriteFatalError(ex.Message)

            Return False
        End Try
    End Function

End Module
