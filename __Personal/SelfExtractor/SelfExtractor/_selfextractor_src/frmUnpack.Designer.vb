﻿Imports System.Windows.Forms
Imports System.Drawing

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmUnpack
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtProgress = New System.Windows.Forms.TextBox()
        Me.bgWorker_Unpack = New System.ComponentModel.BackgroundWorker()
        Me.progressFiles = New System.Windows.Forms.ProgressBar()
        Me.tmrDone = New System.Windows.Forms.Timer(Me.components)
        Me.tmrStart = New System.Windows.Forms.Timer(Me.components)
        Me.bgWorker_RunExe = New System.ComponentModel.BackgroundWorker()
        Me.pnlPassword = New System.Windows.Forms.Panel()
        Me.lblTriesLeft = New System.Windows.Forms.Label()
        Me.btnVerifyPassword = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtArchivePassword = New System.Windows.Forms.TextBox()
        Me.tmrPWDialogOscillator = New System.Windows.Forms.Timer(Me.components)
        Me.tmrTextboxShake = New System.Windows.Forms.Timer(Me.components)
        Me.pnlPassword.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtProgress
        '
        Me.txtProgress.BackColor = System.Drawing.Color.White
        Me.txtProgress.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProgress.Location = New System.Drawing.Point(12, 12)
        Me.txtProgress.Multiline = True
        Me.txtProgress.Name = "txtProgress"
        Me.txtProgress.ReadOnly = True
        Me.txtProgress.Size = New System.Drawing.Size(760, 310)
        Me.txtProgress.TabIndex = 1
        Me.txtProgress.Text = "Starting"
        '
        'bgWorker_Unpack
        '
        Me.bgWorker_Unpack.WorkerReportsProgress = True
        '
        'progressFiles
        '
        Me.progressFiles.BackColor = System.Drawing.Color.White
        Me.progressFiles.Location = New System.Drawing.Point(12, 328)
        Me.progressFiles.Name = "progressFiles"
        Me.progressFiles.Size = New System.Drawing.Size(760, 24)
        Me.progressFiles.Step = 1
        Me.progressFiles.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressFiles.TabIndex = 5
        '
        'tmrDone
        '
        Me.tmrDone.Interval = 1000
        '
        'tmrStart
        '
        Me.tmrStart.Interval = 250
        '
        'bgWorker_RunExe
        '
        Me.bgWorker_RunExe.WorkerReportsProgress = True
        '
        'pnlPassword
        '
        Me.pnlPassword.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.pnlPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPassword.Controls.Add(Me.lblTriesLeft)
        Me.pnlPassword.Controls.Add(Me.btnVerifyPassword)
        Me.pnlPassword.Controls.Add(Me.Label1)
        Me.pnlPassword.Controls.Add(Me.txtArchivePassword)
        Me.pnlPassword.Location = New System.Drawing.Point(885, 214)
        Me.pnlPassword.Name = "pnlPassword"
        Me.pnlPassword.Size = New System.Drawing.Size(337, 96)
        Me.pnlPassword.TabIndex = 6
        '
        'lblTriesLeft
        '
        Me.lblTriesLeft.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTriesLeft.Location = New System.Drawing.Point(264, 72)
        Me.lblTriesLeft.Name = "lblTriesLeft"
        Me.lblTriesLeft.Size = New System.Drawing.Size(68, 16)
        Me.lblTriesLeft.TabIndex = 7
        Me.lblTriesLeft.Text = "x tries left"
        Me.lblTriesLeft.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnVerifyPassword
        '
        Me.btnVerifyPassword.BackColor = System.Drawing.Color.Gainsboro
        Me.btnVerifyPassword.Location = New System.Drawing.Point(81, 60)
        Me.btnVerifyPassword.Name = "btnVerifyPassword"
        Me.btnVerifyPassword.Size = New System.Drawing.Size(176, 28)
        Me.btnVerifyPassword.TabIndex = 2
        Me.btnVerifyPassword.Text = "Verify"
        Me.btnVerifyPassword.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(329, 22)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Password please"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtArchivePassword
        '
        Me.txtArchivePassword.Location = New System.Drawing.Point(3, 29)
        Me.txtArchivePassword.Name = "txtArchivePassword"
        Me.txtArchivePassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtArchivePassword.Size = New System.Drawing.Size(329, 22)
        Me.txtArchivePassword.TabIndex = 0
        Me.txtArchivePassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tmrPWDialogOscillator
        '
        Me.tmrPWDialogOscillator.Interval = 1000
        '
        'frmUnpack
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1378, 564)
        Me.Controls.Add(Me.pnlPassword)
        Me.Controls.Add(Me.progressFiles)
        Me.Controls.Add(Me.txtProgress)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUnpack"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmUnpack"
        Me.pnlPassword.ResumeLayout(False)
        Me.pnlPassword.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtProgress As TextBox
    Friend WithEvents bgWorker_Unpack As System.ComponentModel.BackgroundWorker
    Friend WithEvents progressFiles As ProgressBar
    Friend WithEvents tmrDone As Timer
    Friend WithEvents tmrStart As Timer
    Friend WithEvents bgWorker_RunExe As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlPassword As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents txtArchivePassword As TextBox
    Friend WithEvents btnVerifyPassword As Button
    Friend WithEvents tmrPWDialogOscillator As Timer
    Friend WithEvents tmrTextboxShake As Timer
    Friend WithEvents lblTriesLeft As Label
End Class
