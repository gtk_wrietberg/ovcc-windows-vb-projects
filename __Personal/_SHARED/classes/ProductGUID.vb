Public Class ProductGUID
    Public Shared Function GetUninstallIdentifier(ByVal sSearchForApplication As String) As String
        Dim KeyName As String
        Dim Value_DisplayName As String
        Dim Value_UninstallString As String = ""
        Dim UninstallRegKey As Microsoft.Win32.RegistryKey
        Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

        UninstallRegKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", False)

        If sSearchForApplication = "" Then
            'Yes that's right
            sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
        End If

        sSearchForApplication = sSearchForApplication.ToLower

        For Each KeyName In UninstallRegKey.GetSubKeyNames
            ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

            Try
                Value_DisplayName = ApplicationRegKey.GetValue("DisplayName", "").ToString.ToLower
            Catch ex As Exception
                Value_DisplayName = ""
            End Try

            If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                Try
                    Value_UninstallString = ApplicationRegKey.GetValue("UninstallString", "").ToString
                Catch ex As Exception
                    Value_UninstallString = ""
                End Try

                Value_UninstallString = System.Text.RegularExpressions.Regex.Replace(Value_UninstallString, ".*\{(.+)\}.*", "$1")

                Exit For
            End If
        Next

        Return Value_UninstallString
    End Function

    Public Shared Function GetProductIdentifier(ByVal sSearchForApplication As String) As String
        Dim KeyName As String
        Dim Value_DisplayName As String
        Dim Value_UninstallString As String = ""
        Dim UninstallRegKey As Microsoft.Win32.RegistryKey
        Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

        UninstallRegKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Installer\Products", False)

        If sSearchForApplication = "" Then
            'Yes that's right
            sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
        End If

        sSearchForApplication = sSearchForApplication.ToLower

        For Each KeyName In UninstallRegKey.GetSubKeyNames
            ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

            Try
                Value_DisplayName = ApplicationRegKey.GetValue("ProductName", "").ToString.ToLower
            Catch ex As Exception
                Value_DisplayName = ""
            End Try

            If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                Try
                    Value_UninstallString = KeyName
                Catch ex As Exception
                    Value_UninstallString = ""
                End Try

                Exit For
            End If
        Next

        Return Value_UninstallString
    End Function

End Class
