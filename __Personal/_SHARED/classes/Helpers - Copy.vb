﻿Imports Microsoft.Win32
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Security.Principal
Imports System.Security


Public Class Helpers
#Region "Logging"
    Public Class Logger
        Public Shared Function InitialiseLogger() As Boolean
            Dim sPath As String = ""

            sPath = FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_logs"

            Return InitialiseLogger(sPath)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String) As Boolean
            Dim dDate As Date = Now()

            mLogFilePath = LogFilePath & "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
            mLogFile = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

            Try
                If Not IO.Directory.Exists(Logger.LogFilePath) Then
                    IO.Directory.CreateDirectory(Logger.LogFilePath)
                End If
            Catch ex As Exception

            End Try

            Return IO.Directory.Exists(mLogFilePath)
        End Function

        Private Shared ReadOnly DefaultLogFile As String = "placeholder.log"
        Private Shared mLogFilePath As String
        Private Shared mLogFile As String = ""
        Private Shared pPrevDepth As Integer = 0
        Private Shared pDoNotStoreLogLevel As Boolean = False

        Private Shared mDebugging As Boolean = True
        Public Shared Property Debugging() As Boolean
            Get
                Return mDebugging
            End Get
            Set(ByVal value As Boolean)
                mDebugging = value
            End Set
        End Property

        Private Shared mExtraPrefix As String
        Public Shared Property ExtraPrefix() As String
            Get
                Return mExtraPrefix
            End Get
            Set(ByVal value As String)
                mExtraPrefix = value
            End Set
        End Property

        Public Enum MESSAGE_TYPE
            LOG_DEFAULT = 0
            LOG_WARNING = 1
            LOG_ERROR = 2
            LOG_DEBUG = 6
        End Enum

        Public Shared Property LogFilePath() As String
            Get
                Return mLogFilePath
            End Get
            Set(ByVal value As String)
                If value.EndsWith("\") Then
                    mLogFilePath = value
                Else
                    mLogFilePath = value & "\"
                End If
            End Set
        End Property

        Public Shared Property LogFileName() As String
            Get
                Return mLogFile
            End Get
            Set(ByVal value As String)
                mLogFile = value
            End Set
        End Property

        Public Shared Function WriteMessage(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteMessageRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteWarning(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteWarningRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteError(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteErrorRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteDebug(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteDebugRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteRelative(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iRelativeDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            pDoNotStoreLogLevel = True
            Return Write(sMessage, cMessageType, pPrevDepth + iRelativeDepth)
        End Function

        Public Shared Function Write(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String, iDepthStep As Integer

            If iDepth < 0 Then
                iDepth = 0
            End If

            If Not mDebugging And cMessageType = MESSAGE_TYPE.LOG_DEBUG Then
                bDoNotWriteToFile = True
            End If

            sMessage = Trim(sMessage)

            Select Case cMessageType
                Case MESSAGE_TYPE.LOG_WARNING
                    sMsgTypePrefix = "[*] "
                Case MESSAGE_TYPE.LOG_ERROR
                    sMsgTypePrefix = "[!] "
                Case MESSAGE_TYPE.LOG_DEBUG
                    sMsgTypePrefix = "[#] "
                Case MESSAGE_TYPE.LOG_DEFAULT
                    sMsgTypePrefix = "[.] "
                Case Else
                    sMsgTypePrefix = "[?] "
            End Select


            sMsgDatePrefix = Now.ToString & " - "

            sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

            If iDepth < pPrevDepth Then
                For iDepthStep = 1 To iDepth
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & vbCrLf
                sMsgPrefix = sMsgPrefix & sMsgTypePrefix & sMsgDatePrefix
            End If

            If iDepth > 0 Then
                For iDepthStep = 1 To iDepth - 1
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & "|-"
            End If

            If Not bDoNotWriteToFile Then
                UpdateLogfile(sMsgPrefix & sMessage)
            End If

            If Not pDoNotStoreLogLevel Then
                pPrevDepth = iDepth
            End If

            pDoNotStoreLogLevel = False

            Return sMsgPrefix & sMessage
        End Function

        Public Shared Sub WriteWithoutDate(ByVal sMessage As String)
            UpdateLogfile(sMessage)
        End Sub

        Public Shared Sub WriteEmptyLine()
            WriteWithoutDate(" ")
        End Sub

        Private Shared Sub UpdateLogfile(ByVal sString As String)
            Try
                Dim sw As New IO.StreamWriter(mLogFilePath & "\" & mLogFile, True)
                sw.WriteLine(sString)
                sw.Close()
            Catch ex As Exception

            End Try
        End Sub
    End Class
#End Region

#Region "Forms"
    Public Class Forms
        <DllImport("user32.dll", SetLastError:=True)> _
        Private Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As Integer) As Boolean
        End Function

        <DllImport("user32", CharSet:=CharSet.Auto, SetLastError:=True)> _
        Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As UInt32, ByVal wParam As Integer, ByVal lParam As IntPtr) As Integer
        End Function

        Private Const BCM_SETSHIELD As UInt32 = &H160C


        Private Const SWP_NOSIZE As Integer = &H1
        Private Const SWP_NOMOVE As Integer = &H2

        Private Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
        Private Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)

        Public Shared Sub TopMost(FormHandle As System.IntPtr, bTopMost As Boolean)
            If bTopMost Then
                SetWindowPos(FormHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            Else
                SetWindowPos(FormHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End If
        End Sub

        Public Shared Sub AddUACShield(handle As IntPtr)
            SendMessage(handle, BCM_SETSHIELD, 0, New IntPtr(1))
        End Sub
    End Class
#End Region

#Region "PuppyPower"
    Public Class PuppyPower
        Private Shared ReadOnly KEY__Puppypower As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\PuppyPower"
        Private Shared ReadOnly KEY_OLD__Puppypower As String = "HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\Watchdog\PuppyPower"
        Private Shared ReadOnly VALUE__PuppyPowerUserName As String = "username"
        Private Shared ReadOnly VALUE__PuppyPowerPassWord As String = "password"

        Public Shared ReadOnly Property Username As String
            Get
                Dim sTmp As String = "", sVoid As String = ""

                If Registry.KeyExists(KEY__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerUserName, "")
                ElseIf Registry.KeyExists(KEY_OLD__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerUserName, "")
                Else
                    sTmp = ""
                End If

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
        End Property

        Public Shared ReadOnly Property Password As String
            Get
                Dim sTmp As String = "", sVoid As String = ""

                If Registry.KeyExists(KEY__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerPassWord, "")
                ElseIf Registry.KeyExists(KEY_OLD__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerPassWord, "")
                Else
                    sTmp = ""
                End If

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
        End Property
    End Class
#End Region

#Region "XOrObfuscation"
    Public Class XOrObfuscation
        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "$#(98)&_Ndf9m0987e5WRW#%rtefgm03947"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            If PlainText.Length > 1000 Then
                Return "(string too long)"
            End If

            InitializePasshrases()

            Try
                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(Asc(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(Asc(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String

            InitializePasshrases()

            Try
                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & Chr(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & Chr(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                PlainText_RandomPassphrase = ""
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = Asc(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            temp = _HexToNo(Mid(BinVal, 2, 1))
            iVal = iVal + temp
            temp = _HexToNo(Mid(BinVal, 1, 1))
            iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class
#End Region

#Region "misc helpers"
    Public Class Generic
        Public Shared Function IsDevMachine() As Boolean
            Dim sComputerName As String = Environment.MachineName.ToLower
            Dim bRet As Boolean = False

            If InStr(sComputerName, "rietberg") > 0 _
            Or InStr(sComputerName, "superlekkerding") > 0 _
            Or InStr(sComputerName, "wrdev") > 0 Then
                bRet = True
            End If

            Return bRet
        End Function

        Public Shared Function IsRunningAsAdmin() As Boolean
            Return My.User.IsInRole(ApplicationServices.BuiltInRole.Administrator)
        End Function
    End Class
#End Region

#Region "network"
    Public Class Network
        Public Class DomainWorkgroup
            Private Enum JoinStatus
                Unknown = 0
                UnJoined = 1
                Workgroup = 2
                Domain = 3
            End Enum

            <DllImport("netapi32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)> _
            Private Shared Function NetGetJoinInformation( _
            ByVal computerName As String, _
            ByRef buffer As IntPtr, _
            ByRef status As JoinStatus) As Integer
            End Function

            <DllImport("netapi32.dll", SetLastError:=True)> _
            Private Shared Function NetApiBufferFree(ByVal buffer As IntPtr) As Integer
            End Function

            Public Shared Function GetName() As String
                Dim pBuffer As IntPtr = IntPtr.Zero
                Dim mStatus As JoinStatus, mDomainName As String = ""

                Try
                    Dim result As Integer = NetGetJoinInformation(Environment.MachineName, pBuffer, mStatus)
                    If 0 <> result Then Throw New Exception("error")

                    mDomainName = Marshal.PtrToStringUni(pBuffer)
                Catch ex As Exception
                    mDomainName = "error: " & ex.Message
                Finally
                    If Not IntPtr.Zero.Equals(pBuffer) Then
                        NetApiBufferFree(pBuffer)
                    End If
                End Try

                Return mDomainName
            End Function
        End Class
    End Class
#End Region

#Region "OS version"
    Public Class OSVersion
#Region "OSVERSIONINFOEX"
        <StructLayout(LayoutKind.Sequential)> _
        Private Structure OSVERSIONINFOEX
            Public dwOSVersionInfoSize As Integer
            Public dwMajorVersion As Integer
            Public dwMinorVersion As Integer
            Public dwBuildNumber As Integer
            Public dwPlatformId As Integer
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)> _
            Public szCSDVersion As String
            Public wServicePackMajor As Short
            Public wServicePackMinor As Short
            Public wSuiteMask As Short
            Public wProductType As Byte
            Public wReserved As Byte
        End Structure
#End Region

#Region "64 BIT OS DETECTION"
        Private Delegate Function IsWow64ProcessDelegate(<[In]> handle As IntPtr, <Out> ByRef isWow64Process As Boolean) As Boolean

        <DllImport("kernel32", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)> _
        Public Shared Function LoadLibrary(libraryName As String) As IntPtr
        End Function

        <DllImport("kernel32", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)> _
        Public Shared Function GetProcAddress(hwnd As IntPtr, procedureName As String) As IntPtr
        End Function

        Private Shared Function GetIsWow64ProcessDelegate() As IsWow64ProcessDelegate
            Dim handle As IntPtr = LoadLibrary("kernel32")

            If handle <> IntPtr.Zero Then
                Dim fnPtr As IntPtr = GetProcAddress(handle, "IsWow64Process")

                If fnPtr <> IntPtr.Zero Then
                    Return DirectCast(Marshal.GetDelegateForFunctionPointer(DirectCast(fnPtr, IntPtr), GetType(IsWow64ProcessDelegate)), IsWow64ProcessDelegate)
                End If
            End If

            Return Nothing
        End Function

        Private Shared Function Is32BitProcessOn64BitProcessor() As Boolean
            Dim fnDelegate As IsWow64ProcessDelegate = GetIsWow64ProcessDelegate()

            If fnDelegate Is Nothing Then
                Return False
            End If

            Dim isWow64 As Boolean
            Dim retVal As Boolean = fnDelegate.Invoke(Process.GetCurrentProcess().Handle, isWow64)

            If retVal = False Then
                Return False
            End If

            Return isWow64
        End Function
#End Region

        Public Shared Function OSFull() As String
            Return OSName() & " (" & OSBits() & ")"
        End Function

        Public Shared Function OSBits() As String
            Dim sBits As String = "x??"

            Select Case IntPtr.Size * 8
                Case 64
                    sBits = "x64"
                Case 32
                    If Is32BitProcessOn64BitProcessor() Then
                        sBits = "x64"
                    Else
                        sBits = "x32"
                    End If
                Case Else
                    sBits = "x??"
            End Select

            Return sBits
        End Function

        Public Shared Function OSName() As String
            Dim sName As String = "unknown"

            Try
                Dim osVersion As OperatingSystem = Environment.OSVersion
                Dim majorVersion As Integer = osVersion.Version.Major
                Dim minorVersion As Integer = osVersion.Version.Minor

                Dim osVersionInfo As New OSVERSIONINFOEX()
                osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(GetType(OSVERSIONINFOEX))
                Dim productType As Byte = osVersionInfo.wProductType

                sName = "??? (" & majorVersion.ToString & "." & minorVersion.ToString & "." & productType.ToString & ")"

                Select Case osVersion.Platform
                    Case PlatformID.Win32NT
                        Select Case majorVersion
                            Case 3
                                sName = "Windows NT 3.51"
                            Case 4
                                sName = "Windows NT 4.0"
                            Case 5
                                Select Case minorVersion
                                    Case 0
                                        sName = "Windows 2000"
                                    Case 1
                                        sName = "Windows XP"
                                    Case 2
                                        sName = "Windows Server 2003"
                                End Select
                            Case 6
                                Select Case minorVersion
                                    Case 0
                                        sName = "Windows Vista"
                                    Case 1
                                        sName = "Windows 7"
                                    Case 2
                                        sName = "Windows 8"
                                    Case 3
                                        sName = "Windows 8.1"
                                End Select
                            Case 10
                                sName = "Windows 10"
                        End Select
                End Select

            Catch ex As Exception

            End Try

            Return sName
        End Function
    End Class
#End Region

#Region "XML"
    Public Class XML
        Public Shared Function Beautify(sIn As String) As String
            Dim sRet As String = ""

            Try
                Dim xmlDoc As System.Xml.XmlDocument

                xmlDoc = New System.Xml.XmlDocument
                xmlDoc.LoadXml(sIn)

                sRet = Beautify(xmlDoc)
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Public Shared Function Beautify(xmlDoc As System.Xml.XmlDocument) As String
            Dim sRet As String = ""

            Try
                Dim sb As New System.Text.StringBuilder()
                Dim settings As New System.Xml.XmlWriterSettings()

                settings.Indent = True
                settings.IndentChars = "    "
                settings.NewLineChars = vbCrLf
                settings.NewLineHandling = System.Xml.NewLineHandling.Replace

                Using writer As System.Xml.XmlWriter = System.Xml.XmlWriter.Create(sb, settings)
                    xmlDoc.Save(writer)
                End Using

                sRet = sb.ToString()
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

    End Class
#End Region

#Region "types"
    Public Class Types
        Public Shared Function CastStringToInteger_safe(s As String) As Integer
            Dim iTmp As Integer = -1

            If Integer.TryParse(s, iTmp) Then

            End If

            Return iTmp
        End Function

        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function LeadingZero(num As Integer, Optional len As Integer = 2) As String
            Dim sNum As String = num.ToString

            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Public Shared Function RandomString(Length As Integer, Optional CharsToChooseFrom As String = "") As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function

        Public Shared Function FromNormalDateToHBDate(Optional WithTime As Boolean = False) As String
            Return FromNormalDateToHBDate(Now, WithTime)
        End Function

        Public Shared Function FromNormalDateToHBDate(d As Date, Optional WithTime As Boolean = False) As String
            Dim sTmp As String = d.ToString("yyyy-MM-dd")

            If WithTime Then
                sTmp &= " " & d.ToString("HH:mm:ss")
            End If

            Return sTmp
        End Function

        Public Shared Function FromHBDateToNormalDate(s As String) As Date
            Dim d As Date

            Try
                d = Date.Parse(s)
            Catch ex As Exception
                d = Nothing
            End Try

            Return d
        End Function

        Public Shared Function IsValidHBDate(s As String) As Boolean
            Dim d As Date

            Try
                d = Date.Parse(s)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
    End Class
#End Region

#Region "time and date"
    Public Class TimeDate
        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function
    End Class
#End Region

#Region "registry"
    Public Class Registry
        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SetValue_Boolean(keyName As String, valueName As String, value As Boolean)
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "yes", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "no", Microsoft.Win32.RegistryValueKind.String)
                End If
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "string"
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            sTmp = Convert.ToString(oTmp)

            Return sTmp
        End Function

        Public Shared Sub SetValue_String(keyName As String, valueName As String, value As String)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            iTmp = Helpers.Types.CastToInteger_safe(oTmp)

            Return iTmp
        End Function

        Public Shared Sub SetValue_Integer(keyName As String, valueName As String, value As Integer)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "misc"
        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

#End Region

    End Class
#End Region

#Region "sitekiosk"
    Public Class SiteKiosk
        Public Shared Function GetSiteKioskVersion(Optional bShort As Boolean = False) As String
            Dim sTmp As String = Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk", "Build", "0.0")

            If bShort Then
                Return sTmp.Split(".")(0)
            Else
                Return sTmp
            End If
        End Function

        Public Shared Function GetSiteKioskInstallFolder() As String
            Return Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk", "InstallDir", "")
        End Function

        Public Shared Function GetSiteKioskLogfileFolder() As String
            Return Helpers.SiteKiosk.GetSiteKioskInstallFolder & "\logfiles"
        End Function

        Public Shared Function GetMachineNameFromScript() As String
            Dim sRet As String = ""
            Dim sFileBase As String, sFile As String
            sFileBase = GetSiteKioskInstallFolder()
            sFileBase = sFileBase & "\skins\default\scripts"

            sFile = sFileBase & "\guesttek.js"
            If Not IO.File.Exists(sFile) Then
                sFile = sFileBase & "\ibahn.js"
                If Not IO.File.Exists(sFile) Then
                    sFile = sFileBase & "\mycall.js"
                    If Not IO.File.Exists(sFile) Then
                        Return "NO_SCRIPT_FOUND"
                    End If
                End If
            End If

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var LocationID=") Then
                            line = line.Replace("var LocationID=", "")
                            line = line.Replace("""", "")
                            line = line.Replace(";", "")

                            sRet = line

                            Exit While
                        End If
                    End While
                End Using
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Private Shared ReadOnly _sReplacements() As String = { _
            "[SiteKiosk] Notification: [Memory Status]", _
            "[SiteKiosk] Notification: [Process Status]", _
            ""}

        Public Shared Function CleanSiteKioskLogLine(ByVal input As String) As String
            Dim result As String = input

            For Each item As String In _sReplacements
                If Not item.Equals("") Then
                    If result.Contains(item) Then
                        result = ""
                        Exit For
                    End If
                End If
            Next

            If result <> "" Then
                'remove unwanted chars
                For i = 0 To 31
                    result = result.Replace(Chr(i), "")
                Next
                For i = 127 To 255
                    result = result.Replace(Chr(i), "")
                Next
                'Additionally, let's remove </ < and >
                result = result.Replace("</", "[")
                result = result.Replace("<", "[")
                result = result.Replace("/>", "]")
                result = result.Replace(">", "]")
            End If

            Return result
        End Function

        Public Shared Function TranslateSiteKioskPathToNormalPath(sSkPath As String) As String
            Dim sPath As String = sSkPath

            If sPath.StartsWith("%SiteKioskPath%") Then
                sPath = sPath.Replace("%SiteKioskPath%", GetSiteKioskInstallFolder)
                sPath = sPath.Replace("%%PROGRAMFILES%%/SiteKiosk", GetSiteKioskInstallFolder)
                sPath = sPath.Replace("/", "\")
            ElseIf sPath.StartsWith("file://") Then
                sPath = sPath.Replace("file:///", "")
                sPath = sPath.Replace("file://", "")
                sPath = sPath.Replace("/", "\")
            Else

            End If

            Return sPath
        End Function
    End Class
#End Region

#Region "files and folders"
    Public Class FilesAndFolders
        Public Shared Function GetFileCreationDate(sFile As String) As String
            Try
                Dim dDate As Date = IO.File.GetCreationTime(sFile)

                Return dDate.ToString("yyyy-MM-dd")
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String) As String
            Try
                Dim dDate As Date = IO.File.GetLastWriteTime(sFile)

                Return dDate.ToString("yyyy-MM-dd")
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetProgramFilesFolder() As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sPath.Equals(String.Empty) Then
                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            Return sPath
        End Function
    End Class
#End Region

#Region "Crypto"
    Public Class Crypto
        Public Class SHA512
            Private Shared ReadOnly _sha1 As Cryptography.SHA512 = Cryptography.SHA512.Create()

            Public Shared Function GetHash(source As String) As String
                Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
                Dim sb As New System.Text.StringBuilder()

                Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

                Return sb.ToString()
            End Function

            Public Shared Function VerifyHash(source As String, hash As String) As Boolean
                Dim sourceHash = GetHash(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function

            Public Shared Function GetHashBase64(source As String) As String
                Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

                Return Convert.ToBase64String(data)
            End Function

            Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
                Dim sourceHash = GetHashBase64(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function
        End Class

        Public Class MD5
            Private Shared ReadOnly _md5 As Cryptography.MD5 = Cryptography.MD5.Create()

            Public Shared Function GetHash(source As String) As String
                Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
                Dim sb As New System.Text.StringBuilder()

                Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

                Return sb.ToString()
            End Function

            Public Shared Function VerifyHash(source As String, hash As String) As Boolean
                Dim sourceHash = GetHash(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function

            Public Shared Function GetHashBase64(source As String) As String
                Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

                Return Convert.ToBase64String(data)
            End Function

            Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
                Dim sourceHash = GetHashBase64(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function
        End Class
    End Class
#End Region

#Region "prerequisites"
    Public Class Prerequisites
        Public Class DomainOrWorkgroup
            Public Shared Function IsOnDomain() As Boolean
                Try
                    Return Not Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.Equals(String.Empty)
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 1)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 2)

                    Return False
                End Try
            End Function

            Public Shared Function GetDomainName() As String
                Dim sDomain As String = ""

                Try
                    sDomain = Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 1)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 2)
                End Try

                Return sDomain
            End Function
        End Class

        Public Class IE
            Public Enum IEVersions As Integer 'useless ENUM
                NONE = 0
                IE5 = 5
                IE6 = 6
                IE7 = 7
                IE8 = 8
                IE9 = 9
                IE10 = 10
                IE11 = 11
                IE12 = 12
            End Enum

            Public Shared Function IsRunningVersion(version As IEVersions) As Boolean
                Return (version <= _GetIEFromRegistry())
            End Function

            Public Shared Function Version() As String
                Dim sTmp As String = ""

                Try
                    Using ieKey As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Internet Explorer\")
                        If ieKey IsNot Nothing AndAlso ieKey.GetValue("svcVersion") IsNot Nothing Then
                            sTmp = DirectCast(ieKey.GetValue("svcVersion", ""), String)
                        End If
                    End Using
                Catch ex As Exception
                    sTmp = ex.Message
                End Try

                Return sTmp
            End Function

            Private Shared Function _GetIEFromRegistry() As IEVersions
                Dim iVersion As Integer = IEVersions.NONE

                Try
                    Using ieKey As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Internet Explorer\")
                        If ieKey IsNot Nothing AndAlso ieKey.GetValue("svcVersion") IsNot Nothing Then
                            Dim sTmp As String = DirectCast(ieKey.GetValue("svcVersion", ""), String)

                            Try
                                iVersion = CInt(sTmp.Substring(0, sTmp.IndexOf(".")))
                            Catch ex As Exception

                            End Try
                        End If
                    End Using
                Catch ex As Exception

                End Try

                Return iVersion
            End Function
        End Class

        Public Class DotNet
            Public Enum DotNetVersions As Integer
                NONE = 0
                v2 = 1
                v3 = 2
                v4 = 4
                v4_5 = 8
                v4_5_1 = 16
                v4_5_2 = 32
                v4_6 = 64
                v4_6_1 = 128
                v4_6_2 = 256
            End Enum

            Public Shared Function IsRunningVersion(version As DotNetVersions) As Boolean
                Dim fullVersion As Integer = GetVersion()

                Return (version <= fullVersion)
            End Function

            Public Shared Function GetVersion() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                iVersion = iVersion Or _Get4orLowerFromRegistry()
                iVersion = iVersion Or _Get45or451FromRegistry()

                Return iVersion
            End Function

            Private Shared Function _Get4orLowerFromRegistry() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                Using ndpKey As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\")
                    For Each versionKeyName As String In ndpKey.GetSubKeyNames()
                        Try
                            Dim versionKey As RegistryKey = ndpKey.OpenSubKey(versionKeyName)
                            Dim name As String = DirectCast(versionKey.GetValue("Version", ""), String)
                            Dim sp As String = versionKey.GetValue("SP", "").ToString()
                            Dim install As String = versionKey.GetValue("Install", "").ToString()

                            If sp <> "" AndAlso install = "1" Then
                                Select Case True
                                    Case versionKeyName.StartsWith("v2")
                                        iVersion = iVersion Or DotNetVersions.v2
                                    Case versionKeyName.StartsWith("v3")
                                        iVersion = iVersion Or DotNetVersions.v3
                                    Case versionKeyName.StartsWith("v4.0")
                                        iVersion = iVersion Or DotNetVersions.v4
                                End Select
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                End Using

                Return iVersion
            End Function

            Private Shared Function _Get45or451FromRegistry() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                Try
                    Using ndpKey As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\")
                        If ndpKey IsNot Nothing AndAlso ndpKey.GetValue("Release") IsNot Nothing Then
                            iVersion = _CheckFor45DotVersion(CInt(ndpKey.GetValue("Release")))
                        End If
                    End Using
                Catch ex As Exception

                End Try

                Return iVersion
            End Function

            Private Shared Function _CheckFor45DotVersion(releaseKey As Integer) As Integer
                If releaseKey >= 394747 Then
                    Return DotNetVersions.v4_6_2
                End If
                If releaseKey >= 394254 Then
                    Return DotNetVersions.v4_6_1
                End If
                If releaseKey >= 393295 Then
                    Return DotNetVersions.v4_6
                End If
                If releaseKey >= 379893 Then
                    Return DotNetVersions.v4_5_2
                End If
                If releaseKey >= 378675 Then
                    Return DotNetVersions.v4_5_1
                End If
                If releaseKey >= 378389 Then
                    Return DotNetVersions.v4_5
                End If

                ' This line should never execute. A non-null release key should mean
                ' that 4.5 or later is installed.
                Return DotNetVersions.NONE
            End Function
        End Class
    End Class
#End Region

#Region "Windows"
    Public Class Windows
        Public Shared Function IsRunningAsAdmin() As Boolean
            Dim bTmp As Boolean

            Try
                Dim user As WindowsIdentity = WindowsIdentity.GetCurrent()
                Dim principal As New WindowsPrincipal(user)

                bTmp = principal.IsInRole(WindowsBuiltInRole.Administrator)
            Catch ex As UnauthorizedAccessException
                bTmp = False
            Catch ex As Exception
                bTmp = False
            End Try

            Return bTmp
        End Function
    End Class
#End Region

#Region "Processes"
    Public Class Processes
        <DllImport("advapi32.dll", SetLastError:=True)> _
        Private Shared Function OpenProcessToken(ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        Private Shared mLastError As String = ""
        Public Shared ReadOnly Property LastError As String
            Get
                Dim sTmp As String = mLastError

                mLastError = ""

                Return sTmp
            End Get
        End Property

        Private Shared Function ConvertPasswordStringToSecureString(ByVal str As String) As System.Security.SecureString
            Dim password As New System.Security.SecureString

            For Each c As Char In str.ToCharArray
                password.AppendChar(c)
            Next

            Return password
        End Function

        Public Shared Function IsProcessRunning(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String

            sSearchProcessName__ToLower = sProcessName.ToLower

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            For Each p As Process In Process.GetProcesses
                sProcessName__ToLower = p.ProcessName.ToLower
                If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                    bRet = True
                    Exit For
                End If
            Next

            Return bRet
        End Function

        Public Shared Function IsUserLoggedIn(UserName As String, Optional CompletelyArbitraryProcessCountThreshold As Integer = 2) As Boolean
            Dim iCount As Integer

            iCount = CountProcessesOwnedByUser(UserName)

            If iCount >= CompletelyArbitraryProcessCountThreshold Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
            Dim wiName As String, lCount As Long

            lCount = 0

            For Each p As Process In Process.GetProcesses
                Try
                    Dim hToken As IntPtr
                    If OpenProcessToken(p.Handle, TokenAccessLevels.Query, hToken) Then
                        Using wi As New WindowsIdentity(hToken)
                            wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                            If wiName.ToLower = sUserNameToCheck.ToLower Then
                                lCount += 1
                            End If
                        End Using
                        CloseHandle(hToken)
                    End If
                Catch ex As Exception

                End Try
            Next

            Return lCount
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOut As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, waitForExit, timeOut)
        End Function

        Public Shared Function StartProcess(cmdLine As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOut As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, "", "", cmdArguments, waitForExit, timeOut)
        End Function

        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    mLastError = "not started (pid<=0)"
                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOut)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        mLastError = "timed out!"
                        Return False
                    End If
                End If
            Catch ex As Exception
                mLastError = "not started (" & ex.Message & ")"
                Return False
            End Try

            Return True
        End Function

        Public Shared Function IsAppAlreadyRunning() As Boolean
            Dim appProc() As Process
            Dim strModName, strProcName As String

            Try
                strModName = Process.GetCurrentProcess.MainModule.ModuleName
                strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

                appProc = Process.GetProcessesByName(strProcName)

                If appProc.Length > 1 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Sub SelfElevate(Optional Argument As String = "--elevated")
            Dim proc As New ProcessStartInfo
            proc.UseShellExecute = True
            proc.WorkingDirectory = Environment.CurrentDirectory
            proc.FileName = Application.ExecutablePath
            proc.Verb = "runas"
            proc.Arguments = Argument

            Try
                Process.Start(proc)
            Catch
                Return
            End Try

            Application.Exit()
        End Sub
    End Class
#End Region

End Class
