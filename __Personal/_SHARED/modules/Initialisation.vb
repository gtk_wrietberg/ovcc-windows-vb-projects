Module Globals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public g_BackupDirectory As String




    Public Sub Initialise()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        Try
            System.IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try
        Try
            System.IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
