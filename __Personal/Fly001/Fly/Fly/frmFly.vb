﻿Public Class frmFly
    Private Sub frmFly_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ChangeFly()

        FormsHelper.TopMost(Me.Handle, True)

        tmrFlyPosition.Interval = 1000
        tmrFlyPosition.Enabled = True
    End Sub

    Private Sub tmrFlyPosition_Tick(sender As Object, e As EventArgs) Handles tmrFlyPosition.Tick
        ChangeFly()
    End Sub

    Private Sub ChangeFly()
        tmrFlyPosition.Interval = GetRandom(5000, 10000)


        Dim iFly As Integer = GetRandom(1, 12)

        Dim resource As Object = My.Resources.ResourceManager.GetObject("fly_" & iFly)
        Me.BlendedBackground = DirectCast(resource, Bitmap)


        Dim iFly_x As Integer = GetRandom(0, Screen.PrimaryScreen.Bounds.Width - Me.Width)
        Dim iFly_y As Integer = GetRandom(0, Screen.PrimaryScreen.Bounds.Height - Me.Height)

        Me.Top = iFly_y
        Me.Left = iFly_x
    End Sub

    Private Function GetRandom(ByVal Min As Integer, ByVal Max As Integer) As Integer
        Dim Generator As System.Random = New System.Random()
        Return Generator.Next(Min, Max + 1)
    End Function
End Class