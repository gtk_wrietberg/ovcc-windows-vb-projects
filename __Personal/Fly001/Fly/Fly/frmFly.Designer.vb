﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFly
    Inherits AlphaForms.AlphaForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        tmrFlyPosition = New Timer(components)
        SuspendLayout()
        ' 
        ' tmrFlyPosition
        ' 
        tmrFlyPosition.Interval = 10000
        ' 
        ' frmFly
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        BlendedBackground = My.Resources.Resources.fly_1
        ClientSize = New Size(83, 75)
        ControlBox = False
        FormBorderStyle = FormBorderStyle.None
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmFly"
        ShowIcon = False
        ShowInTaskbar = False
        StartPosition = FormStartPosition.CenterScreen
        Text = "Form2"
        TopMost = True
        TransparencyKey = Color.Fuchsia
        ResumeLayout(False)
    End Sub

    Friend WithEvents tmrFlyPosition As Timer
End Class
