﻿Public Class frmMain
    Private mPanels As List(Of Panel)
    Private mPanelActive As Integer = 0

    Private mAnimator As New Animator

    Private ReadOnly mPanel_Location As New Point(145, 133) '145; 131
    Private ReadOnly mPanel_Size As New Size(498, 440) ' 505; 351

    Private ReadOnly mPanel_LocationInactive As New Point(1480, 1360)


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Helpers.Forms.OnTop.Put(Me.Handle)


        'Settings
        LoadSettings()




        Me.TransparencyKey = Me.BackColor
        Me.Size = New Size(858, 858)
        Me.Top = (Screen.PrimaryScreen.Bounds.Height - Me.Height) / 2
        Me.Left = (Screen.PrimaryScreen.Bounds.Width - Me.Width) / 2


        picSloeberBg.Image = My.Resources.img_gif_bg

        mAnimator.Add("img_anim_frame_01", 500)
        mAnimator.Add("img_anim_frame_02", 500)
        mAnimator.Add("img_anim_frame_03", 200)
        mAnimator.Add("img_anim_frame_04", 200)
        mAnimator.Add("img_anim_frame_05", 200)
        mAnimator.Add("img_anim_frame_06", 200)
        mAnimator.Add("img_anim_frame_07", 200)
        mAnimator.Add("img_anim_frame_08", 200)
        mAnimator.Add("img_anim_frame_09", 200)
        mAnimator.Add("img_anim_frame_10", 200)
        mAnimator.Add("img_anim_frame_11", 200)
        mAnimator.Add("img_anim_frame_12", 200)
        mAnimator.Add("img_anim_frame_13", 200)
        mAnimator.Add("img_anim_frame_14", 200)
        mAnimator.Add("img_anim_frame_15", 200)
        mAnimator.Add("img_anim_frame_16", 200)
        mAnimator.Add("img_anim_frame_17", 200)
        mAnimator.Add("img_anim_frame_18", 200)
        mAnimator.Add("img_anim_frame_19", 200)
        mAnimator.Add("img_anim_frame_20", 200)
        mAnimator.Add("img_anim_frame_21", 200)
        mAnimator.Add("img_anim_frame_22", 200)
        mAnimator.Add("img_anim_frame_23", 200)
        mAnimator.Add("img_anim_frame_24", 200)
        mAnimator.Add("img_anim_frame_25", 200)
        mAnimator.Add("img_anim_frame_26", 200)
        mAnimator.Add("img_anim_frame_27", 200)
        mAnimator.Add("img_anim_frame_28", 200)
        mAnimator.Add("img_anim_frame_29", 200)
        mAnimator.Add("img_anim_frame_30", 2000)
        mAnimator.Add("img_anim_frame_02", 500)



        'text
        lblStartup_Welcome.Text = lblStartup_Welcome.Text.Replace("%%USER%%", Helpers.Generic.GetCurrentUser)
        lblStartup_Title.Text = lblStartup_Title.Text.Replace("%%VERSION%%", Application.ProductVersion)


        'stuff
        trbarLength.Minimum = Rekensom.Length_Min
        trbarLength.Maximum = Rekensom.Length_Max
        trbarLength.Value = Rekensom.Length

        trbarDifficulty.Minimum = Rekensom.Difficulty_Min
        trbarDifficulty.Maximum = Rekensom.Difficulty_Max
        trbarDifficulty.Value = Rekensom.Difficulty

        For i As Integer = 0 To chklistOperators.Items.Count - 1
            If ((2 ^ i) And Rekensom.Operators) Then
                chklistOperators.SetItemChecked(i, True)
            End If
        Next
        If chklistOperators.CheckedItems.Count = 0 Then
            chklistOperators.SetItemChecked(0, True)
        End If


        AddHandler trbarLength.ValueChanged, AddressOf UpdateSettings
        AddHandler trbarDifficulty.ValueChanged, AddressOf UpdateSettings
        AddHandler chklistOperators.SelectedValueChanged, AddressOf UpdateSettings
        AddHandler chklistAnswerType.SelectedValueChanged, AddressOf UpdateSettings

        AddHandler trbarLength.Scroll, AddressOf AddTooltipToTrackbar
        AddHandler trbarDifficulty.Scroll, AddressOf AddTooltipToTrackbar


        mPanels = New List(Of Panel)
        mPanels.Add(Me.pnlStartup)
        mPanels.Add(Me.pnlOptions)
        mPanels.Add(Me.pnlRekensomQuestion)


        If Not Helpers.Generic.IsDevMachine() Then
            HidePanels()

            tmrStartApp.Enabled = True
        Else
            ShowActivePanel()
        End If
    End Sub

    Private Sub VolgendeSom()

    End Sub

    Private Sub AddTooltipToTrackbar(sender As Object, e As EventArgs)
        tltipTrackbar.SetToolTip(sender, sender.Value)
    End Sub

    Private Sub tmrStartApp_Tick(sender As Object, e As EventArgs) Handles tmrStartApp.Tick
        tmrStartApp.Enabled = False

        tmrAnimation.Interval = 100
        tmrAnimation.Enabled = True

        My.Computer.Audio.Play(My.Resources.snd__kandidaat_2sec_silence, AudioPlayMode.Background)

    End Sub

    Private Sub tmrAnimation_Tick(sender As Object, e As EventArgs) Handles tmrAnimation.Tick
        Dim sName As String = "", iDuration As Integer = 0

        If mAnimator.GetNext(sName, iDuration) Then
            'frame found

            Try
                picSloeberBg.Image = My.Resources.ResourceManager.GetObject(sName)
            Catch ex As Exception

            End Try

            tmrAnimation.Interval = iDuration
            tmrAnimation.Enabled = True
        Else
            tmrAnimation.Enabled = False

            ShowActivePanel()
        End If
    End Sub

    Private Sub HidePanels()
        For i As Integer = 0 To mPanels.Count - 1
            HidePanel(i)
        Next
    End Sub

    Private Sub HidePanel(index As Integer)
        mPanels.Item(index).Visible = False
        mPanels.Item(index).Location = mPanel_LocationInactive
    End Sub

    Private Sub ShowPanel(index As Integer)
        mPanels.Item(index).Visible = True
        mPanels.Item(index).Location = mPanel_Location
    End Sub

    Private Sub NextPanel()
        mPanelActive += 1
        ShowActivePanel()
    End Sub

    Private Sub ShowActivePanel()
        For i As Integer = 0 To mPanels.Count - 1
            If i = mPanelActive Then
                ShowPanel(i)
            Else
                HidePanel(i)
            End If
        Next
    End Sub

    Private Sub picClose_Click(sender As Object, e As EventArgs) Handles picClose.Click
        ExitApplication()
    End Sub

    Private Sub ExitApplication()
        Me.Close()
    End Sub

    Private Sub btnStartup_Continue_Click(sender As Object, e As EventArgs) Handles btnStartup_Continue.Click
        NextPanel()
    End Sub

    Private Sub chklistOperators_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles chklistOperators.ItemCheck
        If chklistOperators.CheckedItems.Count = 1 Then
            If e.CurrentValue = CheckState.Checked Then
                e.NewValue = CheckState.Checked
            End If
        End If
    End Sub

    Private Sub chklistAnswerType_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles chklistAnswerType.ItemCheck
        For i As Integer = 0 To chklistAnswerType.Items.Count - 1
            If i <> e.Index Then
                chklistAnswerType.SetItemCheckState(i, CheckState.Unchecked)
            End If
        Next
    End Sub

    Private Sub LoadSettings()
        Rekensom.Length = My.Settings.Rekensom_Length
        Rekensom.Difficulty = My.Settings.Rekensom_Difficulty
        Rekensom.Operators = My.Settings.Rekensom_Operators
        Rekensom.AnswerType = My.Settings.Rekensom_AnswerType


        'sanity checks are done by Rekensom, save back values just to be sure
        SaveSettings()
    End Sub

    Private Sub UpdateSettings()
        Rekensom.Length = trbarLength.Value

        Rekensom.Difficulty = trbarDifficulty.Value

        Dim _operators As Rekensom._Operators
        For i As Integer = 0 To chklistOperators.Items.Count - 1
            If chklistOperators.GetItemChecked(i) Then
                _operators = _operators Or (2 ^ i)
            End If
        Next
        Rekensom.Operators = _operators

        For i As Integer = 0 To chklistAnswerType.Items.Count - 1
            If chklistAnswerType.GetItemChecked(i) Then
                Rekensom.AnswerType = i
                Exit For
            End If
        Next
    End Sub

    Private Sub SaveSettings()
        My.Settings.Rekensom_Length = Rekensom.Length
        My.Settings.Rekensom_Difficulty = Rekensom.Difficulty
        My.Settings.Rekensom_Operators = Rekensom.Operators
        My.Settings.Rekensom_AnswerType = Rekensom.AnswerType

        My.Settings.Save()
    End Sub

    Private Sub btnOptions_Continue_Click(sender As Object, e As EventArgs) Handles btnOptions_Continue.Click
        SaveSettings()

        NextPanel()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox1.Text = SloeberData._db_test
    End Sub

End Class
