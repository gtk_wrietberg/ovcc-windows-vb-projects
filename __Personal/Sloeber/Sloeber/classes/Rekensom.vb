﻿Public Class Rekensom
    Private Shared _scriptcontrol As New MSScriptControl.ScriptControl()

    Public Enum _Operators As Integer
        Plus = 1
        Minus = 2
        Times = 4
        Divide = 8
    End Enum


    Private Shared ReadOnly cSanityCheck__Difficulty_Min As Integer = 1
    Private Shared ReadOnly cSanityCheck__Difficulty_Max As Integer = 10
    Private Shared ReadOnly cSanityCheck__Value_Min As Integer = -10000
    Private Shared ReadOnly cSanityCheck__Value_Max As Integer = 10000
    Private Shared ReadOnly cSanityCheck__Length_Min As Integer = 2
    Private Shared ReadOnly cSanityCheck__Length_Max As Integer = 10
    Private Shared ReadOnly cSanityCheck__AnswerType_Min As Integer = 0
    Private Shared ReadOnly cSanityCheck__AnswerType_Max As Integer = 3


    Private Shared _random As New Random()


    Private Shared mDifficulty As Integer = 1
    Public Shared Property Difficulty() As Integer
        Get
            Return mDifficulty
        End Get
        Set(ByVal value As Integer)
            If value < cSanityCheck__Difficulty_Min Then
                mDifficulty = cSanityCheck__Difficulty_Min
            ElseIf value > cSanityCheck__Difficulty_Max Then
                mDifficulty = cSanityCheck__Difficulty_Max
            Else
                mDifficulty = value
            End If

        End Set
    End Property

    Private Shared mOperators As _Operators = _Operators.Plus
    Public Shared Property Operators() As _Operators
        Get
            Return mOperators
        End Get
        Set(ByVal value As _Operators)
            Dim _min_operator = [Enum].GetValues(GetType(_Operators)).Cast(Of _Operators)().Min()
            Dim _max_operator = [Enum].GetValues(GetType(_Operators)).Cast(Of _Operators)().Max()


            If value < _min_operator Or value > ((_max_operator * 2) - 1) Then
                mOperators = _Operators.Plus
            Else
                mOperators = value
            End If
        End Set
    End Property

    Private Shared mLength As Integer = 2
    Public Shared Property Length() As Integer
        Get
            Return mLength
        End Get
        Set(ByVal value As Integer)
            If value < cSanityCheck__Length_Min Then
                mLength = cSanityCheck__Length_Min
            ElseIf value > cSanityCheck__Length_Max Then
                mLength = cSanityCheck__Length_Max
            Else
                mLength = value
            End If
        End Set
    End Property

    Private Shared mAnswerType As Integer
    Public Shared Property AnswerType() As Integer
        Get
            Return mAnswerType
        End Get
        Set(ByVal value As Integer)
            If value < cSanityCheck__AnswerType_Min Then
                mAnswerType = cSanityCheck__AnswerType_Min
            ElseIf value > cSanityCheck__AnswerType_Max Then
                mAnswerType = cSanityCheck__AnswerType_Max
            Else
                mAnswerType = value
            End If
        End Set
    End Property

    Private Shared mLastCalculation As String
    Public Shared ReadOnly Property LastCalculation() As String
        Get
            Return mLastCalculation
        End Get
    End Property

    Private Shared mLastAnswer As Integer
    Public Shared ReadOnly Property LastAnswer() As Integer
        Get
            Return mLastAnswer
        End Get
    End Property

    Private Shared mMultipleChoiceCount As Integer
    Public Shared Property MultipleChoiceCount() As Integer
        Get
            Return mMultipleChoiceCount
        End Get
        Set(ByVal value As Integer)
            mMultipleChoiceCount = value
        End Set
    End Property

    Private Shared mLastAnswers As New List(Of Integer)
    Public Shared ReadOnly Property LastAnswers() As List(Of Integer)
        Get
            Return mLastAnswers
        End Get
    End Property

    Public Shared ReadOnly Property Length_Min() As Integer
        Get
            Return cSanityCheck__Length_Min
        End Get
    End Property

    Public Shared ReadOnly Property Length_Max() As Integer
        Get
            Return cSanityCheck__Length_Max
        End Get
    End Property

    Public Shared ReadOnly Property Difficulty_Min() As Integer
        Get
            Return cSanityCheck__Difficulty_Min
        End Get
    End Property

    Public Shared ReadOnly Property Difficulty_Max() As Integer
        Get
            Return cSanityCheck__Difficulty_Max
        End Get
    End Property


    Public Shared Function Create() As String
        If mDifficulty < 1 Then
            mDifficulty = 1
        End If
        If mDifficulty > 10 Then
            mDifficulty = 10
        End If


        If mDifficulty <= 5 Then
            Return Create(mOperators, 0, 20 * mDifficulty, mLength)
        Else
            Return Create(mOperators, -20 * (mDifficulty - 5), 20 * (mDifficulty - 5), mLength)
        End If
    End Function

    Public Shared Function Create(_operator As _Operators, MinValue As Integer, MaxValue As Integer, Length As Integer) As String
        'First, some sanity checks
        Dim _min_operator = [Enum].GetValues(GetType(_Operators)).Cast(Of _Operators)().Min()
        Dim _max_operator = [Enum].GetValues(GetType(_Operators)).Cast(Of _Operators)().Max()

        If _operator < _min_operator Or _operator > ((_max_operator * 2) - 1) Then
            Return ""
        End If

        If Length < cSanityCheck__Length_Min Then
            Length = cSanityCheck__Length_Min
        End If
        If Length > cSanityCheck__Length_Max Then
            Length = cSanityCheck__Length_Max
        End If

        If MinValue < cSanityCheck__Value_Min Then
            MinValue = cSanityCheck__Value_Min
        End If

        If MaxValue > cSanityCheck__Value_Max Then
            MaxValue = cSanityCheck__Value_Max
        End If

        If mMultipleChoiceCount < 2 Then
            mMultipleChoiceCount = 2
        End If
        If mMultipleChoiceCount > 10 Then
            mMultipleChoiceCount = 10
        End If
        'Sanity restored




        Dim bSingle As Boolean = False
        For Each t As _Operators In [Enum].GetValues(GetType(_Operators)).Cast(Of _Operators)()
            If _operator = t Then
                mLastCalculation = _create_single_operator(_operator, MinValue, MaxValue, Length)
                bSingle = True
                Exit For
            End If
        Next

        If Not bSingle Then
            mLastCalculation = _create_multiple_operator(_operator, MinValue, MaxValue, Length)
        End If


        mLastAnswer = Evaluate()

        _multiple_choice_answers()


        Return mLastCalculation
    End Function

    Public Shared Sub _multiple_choice_answers()
        mLastAnswers = New List(Of Integer)

        mLastAnswers.Add(mLastAnswer)

        Dim _numbers = Enumerable.Range(1, mDifficulty * mMultipleChoiceCount).ToList()
        Dim _RandomIndex As Integer
        For counter As Integer = 1 To mMultipleChoiceCount - 1
            _RandomIndex = _random_number(0, _numbers.Count - 1)

            If _random_number(1, 2) = 1 Then
                mLastAnswers.Add(mLastAnswer - _numbers(_RandomIndex))
            Else
                mLastAnswers.Add(mLastAnswer + _numbers(_RandomIndex))
            End If
            _numbers.RemoveAt(_RandomIndex)
        Next

        _shuffle_list()
    End Sub

    Public Shared Function _multiple_choice_factor(index As Integer) As Double




        Dim _numbers = Enumerable.Range(1, mDifficulty * mMultipleChoiceCount * 2).ToList()
        Dim _RandomIndex As Integer
        For counter As Integer = 1 To mMultipleChoiceCount - 1
            _RandomIndex = _random_number(0, _numbers.Count - 1)
            MsgBox(_numbers(_RandomIndex))
            _numbers.RemoveAt(_RandomIndex)
        Next





        Dim dTmp As Double = index + (_random_number(1, mLastAnswer) * (_random.NextDouble() + 1) * (_random.NextDouble() + 1) * 2)

        If _random_number(1, 2) = 1 Then
            Return -1 * dTmp
        Else
            Return dTmp
        End If
        'Return ((index - (mMultipleChoiceCount / 2)) * _random_number(1, mMultipleChoiceCount) * _random_number(1, mDifficulty))
    End Function

    Private Shared Function _create_single_operator(_operator As _Operators, MinValue As Integer, MaxValue As Integer, Length As Integer) As String
        Dim sSom As String = ""

        For i As Integer = 0 To Length - 2
            sSom &= _random_number(MinValue, MaxValue).ToString
            sSom &= " "
            sSom &= _operatorToString(_operator)
            sSom &= " "
        Next

        sSom &= _random_number(MinValue, MaxValue).ToString


        Return sSom
    End Function

    Private Shared Function _create_multiple_operator(_operator As _Operators, MinValue As Integer, MaxValue As Integer, Length As Integer) As String
        Dim sSom As String = ""
        Dim lOperators As New List(Of String)

        For Each t As _Operators In [Enum].GetValues(GetType(_Operators)).Cast(Of _Operators)()
            If (_operator And t) Then
                lOperators.Add(_operatorToString(t))
            End If
        Next


        For i As Integer = 0 To Length - 2
            sSom &= _random_number(MinValue, MaxValue).ToString
            sSom &= " "
            sSom &= lOperators.Item(_random_number(0, lOperators.Count - 1))
            sSom &= " "
        Next

        sSom &= _random_number(MinValue, MaxValue).ToString


        Return sSom
    End Function

    Public Shared Function Evaluate() As Integer
        Return Evaluate(mLastCalculation)
    End Function

    Public Shared Function Evaluate(Som As String) As Integer
        Dim iResult As Integer = -1

        Som = Som.Replace("x", "*")

        _scriptcontrol.Language = "VBScript"

        Try
            iResult = _scriptcontrol.Eval(Som)
        Catch ex As Exception
            Return Integer.MinValue
        End Try

        Return iResult
    End Function

    Private Shared Function _operatorToString(_operator As _Operators) As String
        Select Case _operator
            Case _Operators.Divide
                Return "/"
            Case _Operators.Minus
                Return "-"
            Case _Operators.Plus
                Return "+"
            Case _Operators.Times
                Return "x"
            Case Else
                Return "?" & _operator & "?"
        End Select
    End Function

    Private Shared Function _random_number(Min As Integer, Max As Integer) As Integer
        Return _random.Next(Min, Max + 1)
    End Function

    Private Shared Sub _shuffle_list()
        Dim index As Integer, tmp_integer As Integer

        For i = 0 To mLastAnswers.Count - 1
            index = _random_number(i, mLastAnswers.Count - 1)
            If i <> index Then
                tmp_integer = mLastAnswers(i)
                mLastAnswers(i) = mLastAnswers(index)
                mLastAnswers(index) = tmp_integer
            End If
        Next
    End Sub
End Class
