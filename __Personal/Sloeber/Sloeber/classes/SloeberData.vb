﻿Public Class SloeberData

    Private Shared mDb As New SQLiteDatabase

    Public Shared Function LoadSettings()

        mDb.OpenConnection()
    End Function

#Region "settings"
    Private mRekensom_Length As Integer
    Public Property Rekensom_Length() As Integer
        Get
            Return mRekensom_Length
        End Get
        Set(ByVal value As Integer)
            mRekensom_Length = value
        End Set
    End Property

    Private mRekensom_Difficulty As Integer
    Public Property Rekensom_Difficulty() As Integer
        Get
            Return mRekensom_Difficulty
        End Get
        Set(ByVal value As Integer)
            mRekensom_Difficulty = value
        End Set
    End Property

    Private mRekensom_Operators As Integer
    Public Property Rekensom_Operators() As Integer
        Get
            Return mRekensom_Operators
        End Get
        Set(ByVal value As Integer)
            mRekensom_Operators = value
        End Set
    End Property

    Private mRekensom_AnswerType As Integer
    Public Property Rekensom_AnswerType() As Integer
        Get
            Return mRekensom_AnswerType
        End Get
        Set(ByVal value As Integer)
            mRekensom_AnswerType = value
        End Set
    End Property
#End Region

#Region "history data"

#End Region

#Region "database stuff"
    Public Shared Function _db_test() As String
        Dim query As String = ""
        Dim returntext As String = ""


        mDb.OpenConnection()


        query = "SELECT * FROM tblSettings ORDER BY id"


        Using cmd As SQLite.SQLiteCommand = New SQLite.SQLiteCommand(query, mDb.Connection)
            returntext &= String.Format("{0,10} | {1,30} | {2}", "id", "name", "value") & vbLf
            Using selectResult As SQLite.SQLiteDataReader = cmd.ExecuteReader()
                If selectResult.HasRows = True Then
                    While selectResult.Read()
                        returntext &= String.Format("{0, 11} | {1, 30} | {2}", selectResult("id"), selectResult("name"), selectResult("value")) & vbLf
                    End While
                End If
            End Using
        End Using

        Return returntext
    End Function

#End Region
End Class
