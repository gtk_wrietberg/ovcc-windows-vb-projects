﻿Public Class Animator
    Private Class _frame
        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mDuration As Integer
        Public Property Duration() As Integer
            Get
                Return mDuration
            End Get
            Set(ByVal value As Integer)
                mDuration = value
            End Set
        End Property
    End Class

    Private mFrames As List(Of _frame)
    Private mCurrent As Integer

    Public Sub New()
        mFrames = New List(Of _frame)
        mCurrent = -1
    End Sub

    Public Sub Add(Name As String, Duration As Integer)
        Dim _f As New _frame

        _f.Name = Name
        _f.Duration = Duration

        mFrames.Add(_f)
    End Sub

    Public Sub Reset()
        mCurrent = -1
    End Sub

    Public Function GetNext(ByRef Name As String, ByRef Duration As Integer) As Boolean
        mCurrent += 1

        If mCurrent >= mFrames.Count Or mCurrent < 0 Then
            Name = ""
            Duration = -1

            Return False
        End If

        Name = mFrames.Item(mCurrent).Name
        Duration = mFrames.Item(mCurrent).Duration

        Return True
    End Function

End Class
