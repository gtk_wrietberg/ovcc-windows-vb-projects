﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.picClose = New System.Windows.Forms.PictureBox()
        Me.picSloeberBg = New System.Windows.Forms.PictureBox()
        Me.pnlStartup = New System.Windows.Forms.Panel()
        Me.btnStartup_Continue = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblStartup_Title = New System.Windows.Forms.Label()
        Me.lblStartup_Welcome = New System.Windows.Forms.Label()
        Me.tmrAnimation = New System.Windows.Forms.Timer(Me.components)
        Me.tmrStartApp = New System.Windows.Forms.Timer(Me.components)
        Me.pnlOptions = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chklistAnswerType = New System.Windows.Forms.CheckedListBox()
        Me.btnOptions_Continue = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chklistOperators = New System.Windows.Forms.CheckedListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.trbarDifficulty = New System.Windows.Forms.TrackBar()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.trbarLength = New System.Windows.Forms.TrackBar()
        Me.tltipTrackbar = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlRekensomQuestion = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tmrRekensom = New System.Windows.Forms.Timer(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picSloeberBg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStartup.SuspendLayout()
        Me.pnlOptions.SuspendLayout()
        CType(Me.trbarDifficulty, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.trbarLength, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlRekensomQuestion.SuspendLayout()
        Me.SuspendLayout()
        '
        'picClose
        '
        Me.picClose.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picClose.Image = Global.Sloeber.My.Resources.Resources.img_gif_close
        Me.picClose.Location = New System.Drawing.Point(612, 38)
        Me.picClose.Margin = New System.Windows.Forms.Padding(4)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(50, 40)
        Me.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picClose.TabIndex = 0
        Me.picClose.TabStop = False
        '
        'picSloeberBg
        '
        Me.picSloeberBg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.picSloeberBg.Image = Global.Sloeber.My.Resources.Resources.img_gif_bg
        Me.picSloeberBg.Location = New System.Drawing.Point(0, 0)
        Me.picSloeberBg.Margin = New System.Windows.Forms.Padding(4)
        Me.picSloeberBg.Name = "picSloeberBg"
        Me.picSloeberBg.Size = New System.Drawing.Size(858, 858)
        Me.picSloeberBg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picSloeberBg.TabIndex = 1
        Me.picSloeberBg.TabStop = False
        '
        'pnlStartup
        '
        Me.pnlStartup.BackColor = System.Drawing.Color.White
        Me.pnlStartup.Controls.Add(Me.btnStartup_Continue)
        Me.pnlStartup.Controls.Add(Me.Label1)
        Me.pnlStartup.Controls.Add(Me.lblStartup_Title)
        Me.pnlStartup.Controls.Add(Me.lblStartup_Welcome)
        Me.pnlStartup.Location = New System.Drawing.Point(866, 38)
        Me.pnlStartup.Margin = New System.Windows.Forms.Padding(4)
        Me.pnlStartup.Name = "pnlStartup"
        Me.pnlStartup.Size = New System.Drawing.Size(505, 347)
        Me.pnlStartup.TabIndex = 2
        '
        'btnStartup_Continue
        '
        Me.btnStartup_Continue.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStartup_Continue.Location = New System.Drawing.Point(13, 292)
        Me.btnStartup_Continue.Margin = New System.Windows.Forms.Padding(4)
        Me.btnStartup_Continue.Name = "btnStartup_Continue"
        Me.btnStartup_Continue.Size = New System.Drawing.Size(480, 47)
        Me.btnStartup_Continue.TabIndex = 3
        Me.btnStartup_Continue.Text = "Ga verder"
        Me.btnStartup_Continue.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Comic Sans MS", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(5, 198)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(496, 27)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "is klaar voor gebruik"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStartup_Title
        '
        Me.lblStartup_Title.Font = New System.Drawing.Font("Impact", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartup_Title.Location = New System.Drawing.Point(3, 131)
        Me.lblStartup_Title.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblStartup_Title.Name = "lblStartup_Title"
        Me.lblStartup_Title.Size = New System.Drawing.Size(498, 94)
        Me.lblStartup_Title.TabIndex = 1
        Me.lblStartup_Title.Text = "SLOEBER v%%VERSION%%"
        Me.lblStartup_Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStartup_Welcome
        '
        Me.lblStartup_Welcome.Font = New System.Drawing.Font("Comic Sans MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartup_Welcome.Location = New System.Drawing.Point(4, 0)
        Me.lblStartup_Welcome.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblStartup_Welcome.Name = "lblStartup_Welcome"
        Me.lblStartup_Welcome.Size = New System.Drawing.Size(497, 131)
        Me.lblStartup_Welcome.TabIndex = 0
        Me.lblStartup_Welcome.Text = "Hallo %%USER%%"
        Me.lblStartup_Welcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tmrAnimation
        '
        '
        'tmrStartApp
        '
        Me.tmrStartApp.Interval = 1000
        '
        'pnlOptions
        '
        Me.pnlOptions.BackColor = System.Drawing.Color.White
        Me.pnlOptions.Controls.Add(Me.Label5)
        Me.pnlOptions.Controls.Add(Me.chklistAnswerType)
        Me.pnlOptions.Controls.Add(Me.btnOptions_Continue)
        Me.pnlOptions.Controls.Add(Me.Label4)
        Me.pnlOptions.Controls.Add(Me.chklistOperators)
        Me.pnlOptions.Controls.Add(Me.Label3)
        Me.pnlOptions.Controls.Add(Me.trbarDifficulty)
        Me.pnlOptions.Controls.Add(Me.Label2)
        Me.pnlOptions.Controls.Add(Me.trbarLength)
        Me.pnlOptions.Location = New System.Drawing.Point(1398, 38)
        Me.pnlOptions.Margin = New System.Windows.Forms.Padding(4)
        Me.pnlOptions.Name = "pnlOptions"
        Me.pnlOptions.Size = New System.Drawing.Size(505, 347)
        Me.pnlOptions.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(261, 101)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(169, 19)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Wat voor sommen wil je?"
        '
        'chklistAnswerType
        '
        Me.chklistAnswerType.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.chklistAnswerType.CheckOnClick = True
        Me.chklistAnswerType.FormattingEnabled = True
        Me.chklistAnswerType.Items.AddRange(New Object() {"Zonder tijd", "Met tijd", "Multiple choice zonder tijd", "Multiple choice met tijd"})
        Me.chklistAnswerType.Location = New System.Drawing.Point(265, 123)
        Me.chklistAnswerType.Name = "chklistAnswerType"
        Me.chklistAnswerType.Size = New System.Drawing.Size(227, 84)
        Me.chklistAnswerType.TabIndex = 7
        '
        'btnOptions_Continue
        '
        Me.btnOptions_Continue.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOptions_Continue.Location = New System.Drawing.Point(12, 292)
        Me.btnOptions_Continue.Margin = New System.Windows.Forms.Padding(4)
        Me.btnOptions_Continue.Name = "btnOptions_Continue"
        Me.btnOptions_Continue.Size = New System.Drawing.Size(480, 47)
        Me.btnOptions_Continue.TabIndex = 4
        Me.btnOptions_Continue.Text = "Ga verder"
        Me.btnOptions_Continue.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 101)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(169, 19)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Wat voor sommen wil je?"
        '
        'chklistOperators
        '
        Me.chklistOperators.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.chklistOperators.CheckOnClick = True
        Me.chklistOperators.FormattingEnabled = True
        Me.chklistOperators.Items.AddRange(New Object() {"Plus", "Min", "Keer"})
        Me.chklistOperators.Location = New System.Drawing.Point(7, 123)
        Me.chklistOperators.Name = "chklistOperators"
        Me.chklistOperators.Size = New System.Drawing.Size(165, 63)
        Me.chklistOperators.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(261, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(241, 19)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Hoe moeilijk mogen de sommen zijn?"
        '
        'trbarDifficulty
        '
        Me.trbarDifficulty.Location = New System.Drawing.Point(265, 43)
        Me.trbarDifficulty.Minimum = 1
        Me.trbarDifficulty.Name = "trbarDifficulty"
        Me.trbarDifficulty.Size = New System.Drawing.Size(237, 45)
        Me.trbarDifficulty.TabIndex = 3
        Me.trbarDifficulty.Value = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(218, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Hoe lang mogen de sommen zijn?"
        '
        'trbarLength
        '
        Me.trbarLength.Location = New System.Drawing.Point(3, 43)
        Me.trbarLength.Minimum = 2
        Me.trbarLength.Name = "trbarLength"
        Me.trbarLength.Size = New System.Drawing.Size(214, 45)
        Me.trbarLength.TabIndex = 1
        Me.trbarLength.Value = 2
        '
        'pnlRekensomQuestion
        '
        Me.pnlRekensomQuestion.BackColor = System.Drawing.Color.White
        Me.pnlRekensomQuestion.Controls.Add(Me.TextBox1)
        Me.pnlRekensomQuestion.Controls.Add(Me.Label6)
        Me.pnlRekensomQuestion.Controls.Add(Me.Button1)
        Me.pnlRekensomQuestion.Location = New System.Drawing.Point(866, 407)
        Me.pnlRekensomQuestion.Margin = New System.Windows.Forms.Padding(4)
        Me.pnlRekensomQuestion.Name = "pnlRekensomQuestion"
        Me.pnlRekensomQuestion.Size = New System.Drawing.Size(505, 347)
        Me.pnlRekensomQuestion.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Comic Sans MS", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 29)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(499, 105)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Label6"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(13, 292)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(480, 47)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Ga verder"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tmrRekensom
        '
        Me.tmrRekensom.Interval = 1000
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(13, 108)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(480, 177)
        Me.TextBox1.TabIndex = 5
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Blue
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(1916, 1053)
        Me.Controls.Add(Me.pnlRekensomQuestion)
        Me.Controls.Add(Me.pnlOptions)
        Me.Controls.Add(Me.pnlStartup)
        Me.Controls.Add(Me.picClose)
        Me.Controls.Add(Me.picSloeberBg)
        Me.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sloeber"
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picSloeberBg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStartup.ResumeLayout(False)
        Me.pnlOptions.ResumeLayout(False)
        Me.pnlOptions.PerformLayout()
        CType(Me.trbarDifficulty, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.trbarLength, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlRekensomQuestion.ResumeLayout(False)
        Me.pnlRekensomQuestion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents picClose As PictureBox
    Friend WithEvents picSloeberBg As PictureBox
    Friend WithEvents pnlStartup As Panel
    Friend WithEvents lblStartup_Welcome As Label
    Friend WithEvents tmrAnimation As Timer
    Friend WithEvents tmrStartApp As Timer
    Friend WithEvents lblStartup_Title As Label
    Friend WithEvents btnStartup_Continue As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents pnlOptions As Panel
    Friend WithEvents trbarLength As TrackBar
    Friend WithEvents Label4 As Label
    Friend WithEvents chklistOperators As CheckedListBox
    Friend WithEvents Label3 As Label
    Friend WithEvents trbarDifficulty As TrackBar
    Friend WithEvents Label2 As Label
    Friend WithEvents btnOptions_Continue As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents chklistAnswerType As CheckedListBox
    Friend WithEvents tltipTrackbar As ToolTip
    Friend WithEvents pnlRekensomQuestion As Panel
    Friend WithEvents Label6 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents tmrRekensom As Timer
    Friend WithEvents TextBox1 As TextBox
End Class
