Module modMain
    Public Sub Main()
        SetSiteKioskUserRegistryKeys()
    End Sub

    Private Function SetSiteKioskUserRegistryKeys() As Boolean
        Dim oHive As RegistryHive

        oLogger.WriteToLog("updating registry for SiteKiosk user", , 1)


        oLogger.WriteToLog("initialising", , 2)
        oHive = New RegistryHive

        If oHive.Initialise Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

            Return False
        End If


        oLogger.WriteToLog("loading SiteKiosk hive", , 2)
        oLogger.WriteToLog(cSiteKioskRegistryHive, , 3)
        If Not IO.File.Exists(cSiteKioskRegistryHive) Then
            oLogger.WriteToLog("does not exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End If

        If oHive.LoadHive(cSiteKioskRegistryHive) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

            oHive.UnloadHive()

            Return False
        End If


        oLogger.WriteToLog("setting values", , 2)

        oLogger.WriteToLog("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
        oLogger.WriteToLog("name : " & cSiteKioskRegistry_Name_SecureProtocols, , 3)
        oLogger.WriteToLog("value: " & cSiteKioskRegistry_Value_SecureProtocols.ToString, , 3)
        If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_SecureProtocols, cSiteKioskRegistry_Value_SecureProtocols) Then
            oLogger.WriteToLog("ok", , 4)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
        End If

        oLogger.WriteToLog("key  : " & cSiteKioskRegistry_Key_InternetSettings_Zone3, , 3)
        oLogger.WriteToLog("name : " & cSiteKioskRegistry_Name_DisplayMixedContent, , 3)
        oLogger.WriteToLog("value: " & cSiteKioskRegistry_Value_DisplayMixedContent.ToString, , 3)
        If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_DisplayMixedContent, cSiteKioskRegistry_Value_DisplayMixedContent) Then
            oLogger.WriteToLog("ok", , 4)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
        End If


        oLogger.WriteToLog("unloading SiteKiosk hive", , 2)
        If oHive.UnloadHive() Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

            Return False
        End If

        Return True
    End Function

End Module
