Imports System.serviceprocess
Imports System.Runtime.InteropServices

Public Class WindowsService
    Private iExceptionCaught As Integer

    Private mServiceName As String

    Public Property ServiceName() As String
        Get
            Return mServiceName
        End Get
        Set(ByVal value As String)
            mServiceName = value
        End Set
    End Property

#Region "Values for Services"
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Private Shared Function OpenSCManager(ByVal sMachineName As String, ByVal sDbName As String, ByVal iAccess As Integer) As IntPtr
    End Function
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Private Shared Function CreateService(ByVal hSCM As IntPtr, ByVal sName As String, ByVal sDisplay As String, ByVal iAccess As Integer, ByVal iServiceType As Integer, ByVal iStartType As Integer, ByVal iError As Integer, ByVal sPath As String, ByVal sGroup As String, ByVal iTag As Integer, ByVal sDepends As String, ByVal sUser As String, ByVal sPass As String) As IntPtr
    End Function
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Private Shared Function CloseServiceHandle(ByVal iHandle As IntPtr) As Integer
    End Function
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Private Shared Function OpenService(ByVal hSCM As IntPtr, ByVal sServiceName As String, ByVal iDesiredAccess As Integer) As IntPtr
    End Function
    <DllImport("advapi32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> Private Shared Function DeleteService(ByVal hService As IntPtr) As Boolean
    End Function

    Private Enum ServiceControlManagerEnum
        Connect = &H1
        CreateService = &H2
        EnumerateService = &H4
        Lock = &H8
        QueryLockStatus = &H10
        ModifyBootConfig = &H20
        AllAccess = (StandardRightsRequired Or Connect Or ServiceControlManagerEnum.CreateService Or EnumerateService Or ServiceControlManagerEnum.Lock Or QueryLockStatus Or ModifyBootConfig)
        StandardRightsRequired = &HF0000
    End Enum

    Private Enum ServiceAccessTypeEnum
        QueryConfig = &H1
        ChangeConfig = &H2
        QueryStatus = &H4
        EnumerateDependents = &H8
        Start = &H10
        [Stop] = &H20
        PauseContinue = &H40
        Interrogate = &H80
        UserDefinedControl = &H100
        AllAccess = (StandardRightsRequired Or QueryConfig Or ChangeConfig Or QueryStatus Or EnumerateDependents Or Start Or [Stop] Or PauseContinue Or Interrogate Or UserDefinedControl)
        StandardRightsRequired = &HF0000
    End Enum

    Private Enum ServiceTypeEnum
        Win32OwnProcess = &H10
        AutoStart = &H2
        ErrorNormal = &H1
    End Enum

    Private Enum ServiceControlTypeEnum
        OwnProcess = &H10
        ShareProcess = &H20
        KernelDriver = &H1
        FileSystemDriver = &H2
        InteractiveProcess = &H100
    End Enum

#End Region

    Public Function UninstallService() As String
        Return _UninstallService()
    End Function

    Private Function _UninstallService() As String
        Dim sStatus As String

        If IsServiceInstalled() Then
            sStatus = ControlServices("Stop")
        Else
            Return "not installed"
        End If

        Try

            Dim hSCM As IntPtr = OpenSCManager(Nothing, Nothing, ServiceControlManagerEnum.AllAccess)
            If hSCM.ToInt32 = 0 Then
                Throw New Exception("Could not delete service. [1]")
            Else
                Dim hService As IntPtr = OpenService(hSCM, mServiceName, ServiceAccessTypeEnum.AllAccess)
                If hService.ToInt32 = 0 Then
                    ' TODO: FAILED
                    sStatus = "could not open service"
                Else
                    If Not DeleteService(hService) Then
                        Throw New Exception("Could not delete service. [2]")
                    End If

                    CloseServiceHandle(hService)
                End If

                CloseServiceHandle(hSCM)

                Return "uninstalled"
            End If
        Catch ex As Exception
            sStatus = ex.Message

            Return False
        End Try
    End Function

#Region "Helper Functions"

    Private Function IsServiceInstalled() As Boolean
        Dim bResult As Boolean = False

        Dim oServiceArray() As ServiceProcess.ServiceController
        oServiceArray = ServiceProcess.ServiceController.GetServices

        For Each oServiceController As ServiceProcess.ServiceController In oServiceArray
            If oServiceController.ServiceName.Trim.ToUpper = mServiceName.Trim.ToUpper Then
                Dim i As New ServiceControllerPermissionAttribute(Security.Permissions.SecurityAction.Demand)
                Dim d As New ServiceControllerPermission
                Try
                    If d.Any Then
                        d.ToString()
                    End If
                Catch
                End Try
                bResult = True
                Exit For
            End If
        Next

        Return bResult
    End Function

    Private Function ControlServices(ByVal sTask As String) As String
        Dim sStatus As String = ""
        Dim objWinServ As New ServiceController

        objWinServ.ServiceName = mServiceName
        Try
            Select Case sTask
                Case "Reset"
                    If objWinServ.Status = ServiceControllerStatus.Running Then
                        'Service is currently running, so you will have to stop it before starting it
                        'First stop all it's child services, then stop the service itself
                        Call CheckForChildServices(mServiceName, "Stop")
                    End If

                    If objWinServ.Status = ServiceControllerStatus.Stopped Then 'This is satisfied if the service was running and then was stopped by code or if it was already stopped
                        'service is already stopped so just start it...so first start all it's parent services
                        Call CheckForParentServices(mServiceName, "Start")
                        StartService(objWinServ.DisplayName)
                    End If

                    If objWinServ.Status = ServiceControllerStatus.Running Then
                        sStatus = "reset"
                    Else
                        sStatus = "not started after reset"
                    End If

                Case "Start"
                    If objWinServ.Status = ServiceControllerStatus.Stopped Then 'This is satisfied if the service was running and then was stopped by code or if it was already stopped
                        Call CheckForParentServices(mServiceName, "Start")
                        If iExceptionCaught = 0 Then
                            sStatus = "started"
                        Else
                            sStatus = "could not start " & objWinServ.DisplayName
                            iExceptionCaught = 0
                        End If
                    End If

                Case "Stop"
                    If objWinServ.Status <> ServiceControllerStatus.Stopped Then
                        Call CheckForChildServices(mServiceName, "Stop")
                        If iExceptionCaught = 0 Then
                            sStatus = "stopped"
                        Else
                            iExceptionCaught = 0
                            sStatus = "could not stop " & objWinServ.DisplayName
                        End If
                    End If
            End Select
        Catch ex As System.InvalidOperationException
            sStatus = "Service does not exist"
        Catch e As Exception
            sStatus = e.Message
        End Try

        Return sStatus
    End Function

    Private Sub CheckForParentServices(ByVal sServiceName As String, ByVal NextService As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName
        Dim objParentService As ServiceController

        For Each objParentService In objService.ServicesDependedOn
            CheckForParentServices(objParentService.DisplayName, NextService)
        Next
        If NextService = "Start" Then
            Call StartService(sServiceName)
        End If

    End Sub

    Private Sub StartService(ByVal sServiceName As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName

        If objService.Status = ServiceControllerStatus.Stopped Then
            Try
                objService.Start()
                objService.WaitForStatus(ServiceControllerStatus.Running, System.TimeSpan.FromSeconds(20))
            Catch ex As TimeoutException
                'Console.WriteLine("Could not Start " & sServiceName & " - TimeOut expired")
                iExceptionCaught = 1
            Catch e As Exception
                'Console.WriteLine("Could not Start " & sServiceName & " - " & e.Message)
                iExceptionCaught = 1
            End Try
        End If
    End Sub

    Private Sub CheckForChildServices(ByVal sServiceName As String, ByVal NextService As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName
        Dim objChildService As ServiceController

        For Each objChildService In objService.DependentServices
            CheckForChildServices(objChildService.DisplayName, NextService)
        Next

        If NextService = "Stop" Then
            Call StopService(sServiceName)
        Else
            Call ContinueService(sServiceName)
        End If

    End Sub

    Private Sub StopService(ByVal sServiceName As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName

        If objService.Status = ServiceControllerStatus.Running Then
            Try
                objService.Stop()
                objService.WaitForStatus(ServiceControllerStatus.Stopped, System.TimeSpan.FromSeconds(20))
            Catch ex As TimeoutException
                iExceptionCaught = 1
                'Console.WriteLine("Could not Stop " & sServiceName & " - TimeOut expired")
            Catch e As Exception
                iExceptionCaught = 1
                'Console.WriteLine("Could not Stop " & sServiceName & " - " & e.Message)
            End Try
        End If

    End Sub

    Private Sub ContinueService(ByVal sServiceName As String)
        Dim objService As New ServiceController
        objService.ServiceName = sServiceName
        If objService.Status = ServiceControllerStatus.Paused Then
            Try
                objService.[Continue]()
                objService.WaitForStatus(ServiceControllerStatus.Running, System.TimeSpan.FromSeconds(20))
            Catch ex As TimeoutException
                iExceptionCaught = 1
                'Console.WriteLine("Could not change status from Paused To Continue for " & sServiceName & " - TimeOut expired")
            Catch e As Exception
                iExceptionCaught = 1
                'Console.WriteLine("Could not change status from Paused To Continue for " & sServiceName & " - " & e.Message)
            End Try
        End If

        If objService.Status = ServiceControllerStatus.Stopped Then
            Call StopService(sServiceName)
        End If

    End Sub

    Private Function CheckStatusOfService(ByVal MachineName As String) As Boolean
        Dim Status As Boolean = False
        Dim objWinServ As New ServiceController
        objWinServ.ServiceName = mServiceName
        objWinServ.MachineName = MachineName

        'If the service is stopped then the button to start it needs to be enabled
        If objWinServ.Status = ServiceControllerStatus.Stopped Then
            Status = True
        End If

        Return Status
    End Function

#End Region
End Class
