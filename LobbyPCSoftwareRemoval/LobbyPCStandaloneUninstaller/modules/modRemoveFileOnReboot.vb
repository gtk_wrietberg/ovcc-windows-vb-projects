Module modRemoveFileOnReboot
    Private ReadOnly MOVEFILE_DELAY_UNTIL_REBOOT As Integer = 4

    <Runtime.InteropServices.DllImport("kernel32", SetLastError:=True)> _
    Private Function MoveFileEx(ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal dwFlags As Integer) As Boolean
    End Function

    Public Function DeleteFileOnReboot(ByVal sFileName As String) As Boolean
        Dim bReturn As Boolean

        Try
            bReturn = MoveFileEx(sFileName, Nothing, MOVEFILE_DELAY_UNTIL_REBOOT)
        Catch ex As Exception
            bReturn = False
        End Try

        Return bReturn
    End Function
End Module
