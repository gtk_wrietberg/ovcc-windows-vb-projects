#Region "[### TO DO ###]"

'### TO DO #####################################################################
'-------------------------------------------------------------------------------
' DATE: 2008-03-12
' WHO: Warrick Procter
' DESCRIPTION:
' o Test.
'-------------------------------------------------------------------------------
'### TO DO #####################################################################

#End Region

#Region "[=== OPTIONS ===]"

Option Strict On
Option Explicit On
Option Compare Binary

#End Region

#Region "[=== IMPORTS ===]"

Imports System.Runtime.InteropServices

#End Region


''' <copyright>
'''###########################################################################
'''##                Copyright (c) 2008 Warrick Procter.                    ##
'''##                                                                       ##
'''## This work is covered by the "Code Project Open License", a copy of    ##
'''## which is enclosed with this package as:                               ##
'''##         "Code Project Open License (CPOL).txt",                       ##
'''## and is available from http://www.codeproject.com/.                    ##
'''##                                                                       ##
'''## No other use is permitted without the express prior written           ##
'''## permission of Warrick Procter.                                        ##
'''## For permission, try these contact addresses (current at the time of   ##
'''## writing):                                                             ##
'''##     procter_AT_xtra_DOT_co_DOT_nz                                     ##
'''##     The address for service of company "ZED Limited", New Zealand.    ##
'''###########################################################################
''' </copyright>
''' <disclaimer>
'''###########################################################################
'''## REPRESENTATIONS, WARRANTIES AND DISCLAIMER                            ##
'''## ------------------------------------------                            ##
'''## THIS WORK IS PROVIDED "AS IS", "WHERE IS" AND "AS AVAILABLE", WITHOUT ##
'''## ANY EXPRESS OR IMPLIED WARRANTIES OR CONDITIONS OR GUARANTEES. YOU,   ##
'''## THE USER, ASSUME ALL RISK IN ITS USE, INCLUDING COPYRIGHT             ##
'''## INFRINGEMENT, PATENT INFRINGEMENT, SUITABILITY, ETC. AUTHOR EXPRESSLY ##
'''## DISCLAIMS ALL EXPRESS, IMPLIED OR STATUTORY WARRANTIES OR CONDITIONS, ##
'''## INCLUDING WITHOUT LIMITATION, WARRANTIES OR CONDITIONS OF             ##
'''## MERCHANTABILITY, MERCHANTABLE QUALITY OR FITNESS FOR A PARTICULAR     ##
'''## PURPOSE, OR ANY WARRANTY OF TITLE OR NON-INFRINGEMENT, OR THAT THE    ##
'''## WORK (OR ANY PORTION THEREOF) IS CORRECT, USEFUL, BUG-FREE OR FREE OF ##
'''## VIRUSES. YOU MUST PASS THIS DISCLAIMER ON WHENEVER YOU DISTRIBUTE THE ##
'''## WORK OR DERIVATIVE WORKS.                                             ##
'''###########################################################################
''' </disclaimer>
''' <history>
''' 2008-03-12 [Warrick Procter] Created.
''' </history>
''' <summary>
''' enuCSIDLVirtual - CSIDL Virtual directory enumeration.
''' See enuCSIDL for detailed documentation.
''' </summary>
''' <overview>
''' </overview>
''' <remarks>
''' See enuCSIDL for detailed documentation.
''' </remarks>
''' <notes>
''' </notes>
Public Enum enuCSIDLVirtual As Int32

    ''' <summary>
    ''' The virtual folder containing the objects in the user's Recycle Bin.
    ''' </summary>
    ''' <remarks></remarks>
    BitBucket = kCSIDL_BITBUCKET

    ''' <summary>
    ''' Links to All Users OEM specific apps.
    ''' </summary>
    ''' <remarks></remarks>
    CommonOEMLinks = kCSIDL_COMMON_OEM_LINKS

    ''' <summary>
    ''' Computers Near Me (computered from Workgroup membership).
    ''' </summary>
    ''' <remarks></remarks>
    ComputersNearMe = kCSIDL_COMPUTERSNEARME

    ''' <summary>
    ''' Network and Dial-up Connections.
    ''' </summary>
    ''' <remarks></remarks>
    Connections = kCSIDL_CONNECTIONS

    ''' <summary>
    ''' The virtual folder containing icons for the Control Panel applications.
    ''' </summary>
    ''' <remarks></remarks>
    Controls = kCSIDL_CONTROLS

    ''' <summary>
    ''' The virtual folder representing My Computer, containing everything on
    ''' the local computer: storage devices, printers, and Control Panel.
    ''' The folder may also contain mapped network drives.
    ''' </summary>
    ''' <remarks></remarks>
    Drives = kCSIDL_DRIVES

    ''' <summary>
    ''' A virtual folder representing the Internet.
    ''' </summary>
    ''' <remarks></remarks>
    Internet = kCSIDL_INTERNET

    ''' <summary>
    ''' A virtual folder representing Network Neighborhood, the root of the
    ''' network namespace hierarchy.
    ''' </summary>
    ''' <remarks></remarks>
    Network = kCSIDL_NETWORK

    ''' <summary>
    ''' The virtual folder containing installed printers.
    ''' </summary>
    ''' <remarks></remarks>
    Printers = kCSIDL_PRINTERS

End Enum
