Module CopyFiles
    Public Function CopyMultipleFiles(ByVal sourcePath As String, ByVal DestinationPath As String, ByRef sReturnMessage As String) As Boolean
        Dim dFile As String = String.Empty
        Dim dFilePath As String = String.Empty
        Dim totalFiles As Integer = 0, filesCopied As Integer = 0

        sourcePath = sourcePath.Replace("""", "")
        If Right(DestinationPath, 1) <> "\" Then
            DestinationPath = DestinationPath & "\"
        End If

        If System.IO.Directory.Exists(sourcePath) Then
            totalFiles = System.IO.Directory.GetFiles(sourcePath).Length

            For Each fName As String In System.IO.Directory.GetFiles(sourcePath)
                If System.IO.File.Exists(fName) Then
                    dFile = System.IO.Path.GetFileName(fName)
                    dFilePath = DestinationPath + dFile

                    System.IO.File.Copy(fName, dFilePath, True)
                End If
                If System.IO.File.Exists(dFilePath) Then
                    filesCopied = filesCopied + 1
                End If
            Next
        Else
            sReturnMessage = sourcePath & " does not exist"
            Return False
        End If

        sReturnMessage = "Copied files " & CStr(filesCopied) & "/" & CStr(totalFiles)

        Return filesCopied = totalFiles
    End Function
End Module
