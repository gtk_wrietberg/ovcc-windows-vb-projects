Imports System.Threading

Public Class frmMain
    Private iApplicationInstallationCount As Integer = 0
    Private iApplicationInstallationCountMax As Integer = 13

    Private oPrerequisites As Prerequisites

    Private WithEvents oProcess As ProcessRunner
    Private WithEvents oLicense As LicenseKey

    Private oComputerName As ComputerName
    Private oLocalization As Localization

    Private installThread As Thread

    Private Panels() As Panel
    Private iActiveGroupBox As Integer

    Private Enum GroupBoxes As Integer
        Prerequisites = 0
        StartScreen = 1
        TermsAndConditions = 2
        LicenseKey = 3
        LicenseKeyValidation = 4
        HotelInformation = 5
        Installation = 6
        DoneAndRestart = 7
        ErrorDuringInstallation = 8
    End Enum

    Private Enum InstallationOrder As Integer
        PCAnywhere = 1
        Altiris
        LobbyPCAgent
        LobbyPCWatchdog
        CreateShortcut
        PdfProxy
        Skype
        WindowsLive
        SiteKiosk
        iBAHNUpdate
        FlashPlayer
        PDFReader
        Shortcuts
        PostInstallation
    End Enum

    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Panels = New Panel() {Me.pnlPrerequisites, Me.pnlStartScreen, Me.pnlTermsAndConditions, Me.pnlLicenseKey, Me.pnlLicenseKeyValidation, Me.pnlHotelInformationPage, Me.pnlInstallation, Me.pnlDoneAndRestart, Me.pnlErrorOccurred}
        'Panels = New Panel() {Me.pnlPrerequisites, Me.pnlStartScreen, Me.pnlTermsAndConditions, Me.pnlLicenseKey, Me.pnlLicenseKeyValidation, Me.pnlInstallation, Me.pnlDoneAndRestart}
    End Sub

#Region " ClientAreaMove Handling "
    'Private Const WM_NCHITTEST As Integer = &H84
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    'Private Const HTCLIENT As Integer = &H1
    Private Const HTCAPTION As Integer = &H2
    '    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
    '        Select Case m.Msg
    '            Case WM_NCHITTEST
    '                MyBase.WndProc(m)
    '    'If m.Result = HTCLIENT Then m.Result = HTCAPTION
    '                If m.Result.ToInt32 = HTCLIENT Then m.Result = IntPtr.op_Explicit(HTCAPTION) 'Try this in VS.NET 2002/2003 if the latter line of code doesn't do it... thx to Suhas for the tip.
    '            Case Else
    '    'Make sure you pass unhandled messages back to the default message handler.
    '                MyBase.WndProc(m)
    '        End Select
    '    End Sub
#End Region

#Region "Form Events"
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Don't close the form when we're still busy
        e.Cancel = gBusyInstalling
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActivatePrevInstance(Me.Text)

        oPrerequisites = New Prerequisites
        oComputerName = New ComputerName
        oSettings = New Settings
        oLicense = New LicenseKey
        oLogger = New Logger

        If InStr(oComputerName.ComputerName.ToLower, "rietberg".ToLower) > 0 _
        Or InStr(oComputerName.ComputerName.ToLower, "superlekkerding".ToLower) > 0 Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If
        End If

        oPrerequisites.CheckWindowsVersion(cPREREQUISITES_NeededWindowsVersion, False)
        oPrerequisites.CheckInternetConnection()
        oPrerequisites.IsUserAdmin()
        oPrerequisites.IsAlreadyInstalled()
        If cDEBUG_ForcePrerequisiteWindowsVersionError Or cDEBUG_ForcePrerequisiteInternetConnectionError Or cDEBUG_ForcePrerequisiteUserIsAdminError Or cDEBUG_ForcePrerequisiteAlreadyInstalledError Then
            oPrerequisites.ForcePrerequisiteErrors(cDEBUG_ForcePrerequisiteWindowsVersionError, cDEBUG_ForcePrerequisiteInternetConnectionError, cDEBUG_ForcePrerequisiteUserIsAdminError, cDEBUG_ForcePrerequisiteAlreadyInstalledError)
        End If


        lblWindowsVersion.Text = oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion)

        oLocalization = New Localization
        oLocalization.LoadLanguageFile()

        PopulateLanguageComboBox()
        ReloadLocalization()

        oLicense.LicenseCodeLength = cLICENSE_KeyLength
        oLicense.PermitTestLicense = cLICENSE_PermitOfflineLicense

        txtLicenseCode.Text = ""

        'GUI
        Me.Width = 640
        Me.Height = 480

        Me.StartPosition = FormStartPosition.Manual

        Me.Left = (My.Computer.Screen.Bounds.Width - Me.Width) / 2
        Me.Top = (My.Computer.Screen.Bounds.Height - Me.Height) / 2

        Me.BackColor = Color.White
        Me.BackgroundImage = My.Resources.Resources.brushedsteel640x480_blackborder
        Me.BackgroundImageLayout = ImageLayout.Tile

        Me.pnlBackgroundBorder.Visible = cGUI_GroupBoxBorder_visible

        btnExit.Top = btnContinue.Top
        btnExit.Left = btnContinue.Left

        btnBack.Top = btnCancel.Top
        btnBack.Left = btnCancel.Left

        picClose.Top = 1
        picClose.Left = Me.Width - picClose.Width - 1
        picClose.Visible = True

        If Not IO.Directory.Exists(cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder) Then
            IO.Directory.CreateDirectory(cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder)
        End If
        If Not IO.Directory.Exists(cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder) Then
            IO.Directory.CreateDirectory(cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder)
        End If

        oLogger.LogFilePath = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder & "\"

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = "v" & myBuildInfo.FileMajorPart & "." & myBuildInfo.FileMinorPart

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        If gTestMode Then
            oLogger.WriteToLog("Running in test mode!!!", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        End If


        gBusyInstalling = False

        'Terms and Conditions
        Dim tmpResources As New MyResources
        txtTC.Text = tmpResources.ToString("TermsAndConditions")

        txtTC.ReadOnly = True
        ParseTermsAndConditions(txtTC)

        'Install count max
        iApplicationInstallationCountMax = InstallationOrder.PostInstallation + 1


        'Panels
        Dim i As Integer
        For i = 0 To Panels.Length - 1
            Panels(i).Top = 84
            Panels(i).Left = 16

            Panels(i).Visible = False
        Next

        iActiveGroupBox = -1
        NextGroupBox()
    End Sub
#End Region

#Region "Private Functions"
#Region "Localization"
    Private Sub PopulateLanguageComboBox()
        '-----------------------------------
        cmbBoxLanguages.Items.Clear()
        Try

            Dim sLanguages() As String
            Dim i As Integer

            sLanguages = oLocalization.GetLanguageNames

            For i = 0 To sLanguages.Length - 1
                If Not sLanguages(i) Is Nothing Then
                    cmbBoxLanguages.Items.Add(sLanguages(i))
                End If
            Next

            cmbBoxLanguages.SelectedIndex = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ReloadLocalization()
        lblTitle.Text = oLocalization.CurrentLanguage.Title

        btnBack.Text = oLocalization.CurrentLanguage.ButtonBack
        btnCancel.Text = oLocalization.CurrentLanguage.ButtonCancel
        btnContinue.Text = oLocalization.CurrentLanguage.ButtonNext
        btnExit.Text = oLocalization.CurrentLanguage.ButtonExit

        lblSelectLanguage.Text = oLocalization.CurrentLanguage.WelcomeSelectLanguage

        lblPrerequisitesRestart.Text = oLocalization.CurrentLanguage.PrerequisitesErrorRestartText

        pnlTermsAndConditions.Text = oLocalization.CurrentLanguage.TermsAndConditionsTitle
        lblTCText.Text = oLocalization.CurrentLanguage.TermsAndConditionsText
        chkTCAgree.Text = oLocalization.CurrentLanguage.TermsAndConditionsCheckBox

        pnlLicenseKey.Text = oLocalization.CurrentLanguage.LicenseValidationTitle
        lblLicenseValidation.Text = oLocalization.CurrentLanguage.LicenseValidationText.Replace("%%LICENSE_CODE_LENGTH%%", cLICENSE_KeyLength.ToString)

        grpbxLicense.Text = oLocalization.CurrentLanguage.LicenseValidationTitle2

        pnlLicenseKeyValidation.Text = oLocalization.CurrentLanguage.LicenseValidationTitle
        lblLicenseValidationCopy.Text = oLocalization.CurrentLanguage.LicenseValidationText.Replace("%%LICENSE_CODE_LENGTH%%", cLICENSE_KeyLength.ToString)
        grpbxLicenseCopy.Text = oLocalization.CurrentLanguage.LicenseValidationTitle2
        lblLicenseProgress.Text = oLocalization.CurrentLanguage.LicenseValidationProgressText

        lblHotelInformation.Text = oLocalization.CurrentLanguage.HotelInformationText

        pnlInstallation.Text = oLocalization.CurrentLanguage.InstallationProgressTitle
        lblInstallationProgressText.Text = oLocalization.CurrentLanguage.InstallationProgressText

        pnlDoneAndRestart.Text = oLocalization.CurrentLanguage.DoneTitle
        lblReboot.Text = oLocalization.CurrentLanguage.DoneRestartText
        lblReboot2.Text = oLocalization.CurrentLanguage.DoneRestartText2
        radRestartNow.Text = oLocalization.CurrentLanguage.DoneRestartNowText
        radRestartLater.Text = oLocalization.CurrentLanguage.DoneRestartLaterText
    End Sub
#End Region

#Region "License code validation"
    Private Sub CheckLicenseCode()
        oLogger.WriteToLog("Validating license code")

        oLicense.LicenseCode = txtLicenseCode.Text

        oLogger.WriteToLog("license code: " & oLicense.LicenseCode, , 1)

        oLicense.CheckLicense()
    End Sub

    'Private Sub CheckLicenseCode()
    '    oLogger.WriteToLog("Validating license code")

    '    oLicense.LicenseCode = txtLicenseCode.Text

    '    oLogger.WriteToLog("license code: " & oLicense.LicenseCode, , 1)

    '    If oLicense.CheckLicense Then
    '        oLogger.WriteToLog("valid", , 2)

    '        'Go to next step
    '        NextGroupBox()
    '    Else
    '        oLogger.WriteToLog("invalid", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
    '        oLogger.WriteToLog(oLicense.ReturnCode, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
    '        oLogger.WriteToLog(oLicense.ReturnMessage, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

    '        lblLicenseError.Text = oLocalization.CurrentLanguage.LicenseValidationProgressError
    '        If oLicense.ReturnCode = 1 Then
    '            lblLicenseError.Text &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage1
    '        ElseIf oLicense.ReturnCode = 2 Then
    '            lblLicenseError.Text &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage2
    '        Else
    '            lblLicenseError.Text &= oLocalization.CurrentLanguage.LicenseValidationErrorMessageUnknown
    '        End If

    '        txtLicenseCode.SelectAll()
    '        grpboxLicenseError.Visible = True

    '        PreviousGroupBox()
    '    End If

    'End Sub
#End Region

#Region "Windows version and Internet connectivity checker"
    Private Sub CheckPrerequisites()
        Dim sError As String

        If oPrerequisites.CheckComplete Then
            If oPrerequisites.AllIsOk Then
                NextGroupBox()
            Else
                oLogger.WriteToLog("Prerequisites error", Logger.MESSAGE_TYPE.LOG_ERROR)

                sError = ""
                If Not oPrerequisites.InternetConnectivity Then
                    oLogger.WriteToLog("No internet connection", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorInternetConnection
                End If
                If Not oPrerequisites.CorrectWindowsVersion Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If

                    If oPrerequisites.HigherWindowsVersionsAreAccepted Then
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " < " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= oLocalization.CurrentLanguage.PrerequisitesErrorMinimumWindowsVersion
                    Else
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " != " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= oLocalization.CurrentLanguage.PrerequisitesErrorWindowsVersion
                    End If

                    sError = sError.Replace("%%NEEDED_WINDOWS_VERSION%%", oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion))
                    sError = sError.Replace("%%RUNNING_WINDOWS_VERSION%%", oPrerequisites.WindowsVersion)
                End If
                If Not oPrerequisites.UserIsAdmin Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("No admin privileges", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorAdminRights
                End If
                If oPrerequisites.IsAlreadyInstalled Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("Already installed", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorAlreadyInstalled
                End If

                lblPrerequisitesError.Text = sError

                UpdateButtons(0, 0, 0, 3)
            End If
        Else
            lblPrerequisitesError.Text = "Unknown error 31. Please contact the helpdesk."

            UpdateButtons(0, 0, 0, 3)
        End If
    End Sub
#End Region

#Region "Groupbox handling"
    Private Sub GotoGroupBox(ByVal iGroupBoxIndex As Integer)
        iActiveGroupBox = iGroupBoxIndex
        UpdateGroupBox()
    End Sub

    Private Sub NextGroupBox()
        iActiveGroupBox += 1
        UpdateGroupBox()
    End Sub

    Private Sub PreviousGroupBox()
        iActiveGroupBox -= 1
        'Skip these group box if we are going BACK
        'Windows version and internet connection error dialog
        If iActiveGroupBox = GroupBoxes.StartScreen Then
            iActiveGroupBox = -1
        End If
        'License validation dialog
        If iActiveGroupBox = GroupBoxes.LicenseKeyValidation Then
            iActiveGroupBox = GroupBoxes.LicenseKey
        End If
        UpdateGroupBox()
    End Sub

    Private Sub UpdateGroupBox()
        If iActiveGroupBox >= Panels.Length Then
            iActiveGroupBox = Panels.Length - 1
        End If
        'We handle the case of <0 in the following select case statement
        'If iActiveGroupBox < 0 Then
        '   iActiveGroupBox = 0
        'End If

        oLogger.WriteToLog("Groupbox / action: " & iActiveGroupBox.ToString, Logger.MESSAGE_TYPE.LOG_DEBUG)

        Select Case iActiveGroupBox
            Case GroupBoxes.Prerequisites
                UpdateButtons(0, 0, 0, 0)
                CheckPrerequisites()
            Case GroupBoxes.StartScreen
                UpdateButtons(0, 3, 3, 0)
            Case GroupBoxes.TermsAndConditions
                UpdateButtons(0, 0, 2, 0)
                chkTCAgree.Checked = False
            Case GroupBoxes.LicenseKey
                UpdateButtons(3, 0, 2, 0)
                picClose.Visible = True

                txtLicenseCode.SelectAll()
            Case GroupBoxes.LicenseKeyValidation
                UpdateButtons(2, 0, 2, 0)
                picClose.Visible = False

                txtLicenseCodeCopy.Text = txtLicenseCode.Text
                tmrLicenseValidationDelay.Enabled = True
            Case GroupBoxes.HotelInformation
                UpdateButtons(0, 0, 3, 0)
            Case GroupBoxes.Installation
                UpdateButtons(0, 0, 0, 0)
                picClose.Visible = False

                gBusyInstalling = True

                ProgressBarMarqueeStart()
                UnpackResourcesInBackground()
            Case GroupBoxes.DoneAndRestart
                UpdateButtons(0, 0, 0, 3)
                radRestartNow.Checked = True
                radRestartLater.Checked = False

                PostDeployment()
            Case GroupBoxes.ErrorDuringInstallation
                UpdateButtons(0, 0, 0, 3)

                lblErrorCode.Text = "Error code:  # " & iApplicationInstallationCount.ToString
            Case Else
                Application.Exit()
        End Select

        If iActiveGroupBox >= 0 And iActiveGroupBox < Panels.Length Then
            Panels(iActiveGroupBox).Visible = True
            pnlBackgroundBorder.Top = Panels(iActiveGroupBox).Top - 1
            pnlBackgroundBorder.Left = Panels(iActiveGroupBox).Left - 1
            pnlBackgroundBorder.Width = Panels(iActiveGroupBox).Width + 2
            pnlBackgroundBorder.Height = Panels(iActiveGroupBox).Height + 2
            pnlBackgroundBorder.Refresh()
        End If

        Dim i As Integer
        For i = 0 To Panels.Length - 1
            If i <> iActiveGroupBox Then
                Panels(i).Visible = False
            End If
        Next
    End Sub
#End Region

#Region "Thread Safe stuff"
    Private Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text] & vbCrLf)
        End If
    End Sub

    Private Sub UupdateLicenseError(ByVal [text] As String)
        lblLicenseError.Text = [text]
    End Sub

    Private Sub SshowHideLicenseError(ByVal [bool] As Boolean)
        grpboxLicenseError.Visible = [bool]
    End Sub
#End Region

#Region "Button Handling"
    Private Sub UpdateButtons(ByVal iBack As Integer, ByVal iCancel As Integer, ByVal iContinue As Integer, ByVal iExit As Integer)
        btnBack.Enabled = iBack And 1
        btnBack.Visible = iBack And 2

        btnCancel.Enabled = iCancel And 1
        btnCancel.Visible = iCancel And 2

        btnContinue.Enabled = iContinue And 1
        btnContinue.Visible = iContinue And 2

        btnExit.Enabled = iExit And 1
        btnExit.Visible = iExit And 2
    End Sub
#End Region

#Region "Deployment and Installers"
#Region "Deployment"
    Private Sub StartDeployment()
        LogToProcessTextBox("Backing up some stuff")
        oLogger.WriteToLog("Backing up some stuff")
        oLogger.WriteToLog("License code", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup\LicenseKey", , 2)
        oLogger.WriteToLog("value: " & oLicense.LicenseCode, , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup", "LicenseKey", oLicense.LicenseCode)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oLogger.WriteToLog("Computer name", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup\Computer Name", , 2)
        oLogger.WriteToLog("value: " & oComputerName.ComputerName, , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup", "Computer Name", oComputerName.ComputerName)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oLogger.WriteToLog("Workgroup name", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup\Workgroup Name", , 2)
        oLogger.WriteToLog("value: " & oComputerName.Workgroup, , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup", "Workgroup Name", oComputerName.Workgroup)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogToProcessTextBox("Updating default user profile")
        oLogger.WriteToLog("Updating default user profile")
        oLogger.WriteToLog("Registry", , 1)
        oLogger.WriteToLog("key  : HKEY_USERS\.DEFAULT\Control Panel\PowerCfg\CurrentPowerPolicy", , 2)
        oLogger.WriteToLog("value: 2", , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_USERS\.DEFAULT\Control Panel\PowerCfg", "CurrentPowerPolicy", "2")
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogToProcessTextBox("Storing drives that are going to be blocked")
        oLogger.WriteToLog("Storing drives that are going to be blocked")
        Try
            Dim sDrivesToBeBlocked As String
            sDrivesToBeBlocked = GetDrivesToBeBlocked()
            oLogger.WriteToLog("Registry", , 1)
            oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\BlockedDrives", , 2)
            oLogger.WriteToLog("value: " & sDrivesToBeBlocked, , 2)
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone", "BlockedDrives", sDrivesToBeBlocked)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogToProcessTextBox("Storing Hotel Information url")
        oLogger.WriteToLog("Storing Hotel Information url")
        Try
            oLogger.WriteToLog("Registry", , 1)
            oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\HotelInformationUrl", , 2)
            oLogger.WriteToLog("value: " & txtHotelInformation.Text, , 2)
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone", "HotelInformationUrl", txtHotelInformation.Text)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        oLogger.WriteToLog("OS version", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup\OS", , 2)
        oLogger.WriteToLog("value: " & oPrerequisites.WindowsVersion, , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup", "OS", oPrerequisites.WindowsVersion)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        If Not gTestMode Then
            LogToProcessTextBox("Updating workgroup and computer name")
            oLogger.WriteToLog("Updating workgroup and computer name")
            oLogger.WriteToLog("workgroup   : " & oLicense.Workgroup, , 1)
            oLogger.WriteToLog("computername: " & oLicense.MachineName, , 1)

            oComputerName.ComputerName = oLicense.MachineName
            oComputerName.Workgroup = oLicense.Workgroup
        Else
            oLogger.WriteToLog("Skipped", , 2)
        End If

        InstallationController()
    End Sub

    Private Sub PostDeployment()
        oLogger.WriteToLog("Post deployment steps")
        oLogger.WriteToLog("Registry", , 1)

        oLogger.WriteToLog("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone", , 2)
        oLogger.WriteToLog("Restarted = 'no'", , 3)
        Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone", "Restarted", "no")

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Version", , 2)
        oLogger.WriteToLog("Installer = '" & myBuildInfo.FileVersion & "'", , 3)
        Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Version", "Installer", myBuildInfo.FileVersion)


        oLogger.WriteToLog("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce", , 2)
        oLogger.WriteToLog("PcHasRebooted = '" & oSettings.Path_PcHasRebooted & "'", , 3)
        Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce", "PcHasRebooted", oSettings.Path_PcHasRebooted)

        oLogger.WriteToLog("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", , 2)
        oLogger.WriteToLog("PostSiteKioskUpdating = '" & oSettings.Path_PostSiteKioskUpdating & "'", , 3)
        Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "PostSiteKioskUpdating", oSettings.Path_PostSiteKioskUpdating)

        'Copy c:\Windows\explorer.exe
        oLogger.WriteToLog("Copy C:\Windows\Explorer.exe to C:\Windows\explorer_.exe", , 1)
        Try
            IO.File.Copy("c:\windows\explorer.exe", "c:\windows\explorer_.exe")
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        If Not IO.File.Exists("c:\windows\explorer_.exe") Then
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("Destination file does not exist!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            oLogger.WriteToLog("ok", , 2)
        End If

        'Copy C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp
        oLogger.WriteToLog("Copy c:\Program Files\SiteKiosk\Bitmaps\iBAHN.bmp to C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp", , 1)
        Try
            IO.File.Copy("c:\Program Files\SiteKiosk\Bitmaps\iBAHN.bmp", "C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp")
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        If Not IO.File.Exists("C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp") Then
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("Destination file does not exist!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            oLogger.WriteToLog("ok", , 2)
        End If

        oLogger.WriteToLog("DONE")
    End Sub

    Private Sub UnpackResourcesInBackground()
        LogToProcessTextBox("Unpacking resources")
        oLogger.WriteToLog("Unpacking resources")

        UpdateInstallationProgressCount()

        tmrUnpackWait.Enabled = True

        Me.installThread = New Thread(New ThreadStart(AddressOf UnpackResourcesModule.UnpackResources))
        Me.installThread.Start()
    End Sub

    Private Sub InstallationController()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner

        If iApplicationInstallationCount < iApplicationInstallationCountMax Then
            tmrInstallerWait.Enabled = True
        End If

        If gTestMode Then
            iApplicationInstallationCount = iApplicationInstallationCountMax
        End If

        oLogger.WriteToLog("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

        Select Case iApplicationInstallationCount
            Case InstallationOrder.PCAnywhere
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallPCAnywhere))
                Me.installThread.Start()
            Case InstallationOrder.Altiris
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallAltiris))
                Me.installThread.Start()
            Case InstallationOrder.LobbyPCAgent
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLobbyPCAgent))
                Me.installThread.Start()
            Case InstallationOrder.LobbyPCWatchdog
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLobbyPCWatchdog))
                Me.installThread.Start()
            Case InstallationOrder.CreateShortcut
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallCreateShortcut))
                Me.installThread.Start()
            Case InstallationOrder.PdfProxy
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallPdfProxy))
                Me.installThread.Start()
            Case InstallationOrder.Skype
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSkype))
                Me.installThread.Start()
            Case InstallationOrder.WindowsLive
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallWindowsLive))
                Me.installThread.Start()
            Case InstallationOrder.SiteKiosk
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKiosk))
                Me.installThread.Start()
            Case InstallationOrder.iBAHNUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstalliBAHNUpdate))
                Me.installThread.Start()
            Case InstallationOrder.FlashPlayer
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallFlashPlayer))
                Me.installThread.Start()
            Case InstallationOrder.PDFReader
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallPDFReader))
                Me.installThread.Start()
            Case InstallationOrder.Shortcuts
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallShortCuts))
                Me.installThread.Start()
            Case InstallationOrder.PostInstallation
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._PostSiteKioskUpdating))
                Me.installThread.Start()
            Case Else
                LogToProcessTextBox("Done")

                ProgressBarMarqueeStop()

                gBusyInstalling = False

                NextGroupBox()
        End Select
    End Sub
#End Region

#Region "Installers"
    Private Sub _InstallPCAnywhere()
        LogToProcessTextBox("Installing remote access application")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_pcAnywhere)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 600")

        oLogger.WriteToLog("Installing remote access application")
        oLogger.WriteToLog("Path   : " & oSettings.Path_pcAnywhere, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_pcAnywhere
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLobbyPCAgent()
        LogToProcessTextBox("Installing iBAHN service")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_LobbyPCAgent)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 300")

        oLogger.WriteToLog("Installing iBAHN service")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LobbyPCAgent, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCAgent
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "LobbyPCAgentPatchInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLobbyPCWatchdog()
        LogToProcessTextBox("Installing iBAHN security service")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_LobbyPCWatchdog)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 300")

        oLogger.WriteToLog("Installing iBAHN security service")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LobbyPCWatchdog, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCWatchdog
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "LobbyPCWatchDogInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstalliBAHNUpdate()
        LogToProcessTextBox("Installing iBAHN files")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_iBAHNUpdate)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 300")

        oLogger.WriteToLog("Installing iBAHN files")
        oLogger.WriteToLog("Path   : " & oSettings.Path_iBAHNUpdate, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_iBAHNUpdate
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "SiteKiosk7Updater"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallFlashPlayer()
        LogToProcessTextBox("Installing Flash player")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_FlashPlayer)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 600")

        oLogger.WriteToLog("Installing Flash player")
        oLogger.WriteToLog("Path   : " & oSettings.Path_FlashPlayer, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_FlashPlayer
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "install_flash_player_ax"
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallPDFReader()
        LogToProcessTextBox("Installing PDF Reader")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_PdfReader)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 900")

        oLogger.WriteToLog("Installing PDF Reader")
        oLogger.WriteToLog("Path   : " & oSettings.Path_PdfReader, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 900", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_PdfReader
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "Setup_AcroRead_silent"
        oProcess.MaxTimeout = 900
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallCreateShortcut()
        LogToProcessTextBox("Installing iBAHN files")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_CreateShortcut)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 300")

        oLogger.WriteToLog("Installing shortcut creator")
        oLogger.WriteToLog("Path   : " & oSettings.Path_CreateShortcut, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_CreateShortcut
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallPdfProxy()
        LogToProcessTextBox("Installing PdfProxy")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_PdfProxy)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 300")

        oLogger.WriteToLog("Installing PdfProxy")
        oLogger.WriteToLog("Path   : " & oSettings.Path_PdfProxy, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_PdfProxy
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSkype()
        Dim sSkypeParams As String

        'sSkypeParams = "/i """ & oSettings.Path_Skype & """ /quiet /qn /norestart /l* """ & System.Windows.Forms.Application.ExecutablePath & "\Skype.msi.log"""
        sSkypeParams = "/i """ & oSettings.Path_Skype & """ INSTALLLEVEL=1 ALLUSERS=1 TRANSFORMS=:RemoveDesktopShortcut.mst;:RemoveStartup.mst /qn /l* """ & oSettings.Path_Skype & ".log"""

        LogToProcessTextBox("Installing Skype")
        LogToProcessTextBox(vbTab & "Path        : msiexec.exe")
        LogToProcessTextBox(vbTab & "Params      : " & sSkypeParams)
        LogToProcessTextBox(vbTab & "Timeout     : 600")

        oLogger.WriteToLog("Installing Skype")
        oLogger.WriteToLog("Path   : msiexec.exe", , 1)
        oLogger.WriteToLog("Params : " & sSkypeParams, , 1)
        oLogger.WriteToLog("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = "msiexec.exe"
        oProcess.Arguments = sSkypeParams
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallWindowsLive()
        Dim sWindowsLiveParams As String

        sWindowsLiveParams = "/q /NOToolbarCEIP /NOhomepage /nolaunch /nosearch /AppSelect:moviemaker /log:""" & oSettings.Path_WindowsLive & ".log"""

        LogToProcessTextBox("Installing WindowsLive")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_WindowsLive)
        LogToProcessTextBox(vbTab & "Params      : " & sWindowsLiveParams)
        LogToProcessTextBox(vbTab & "Timeout     : 1800")

        oLogger.WriteToLog("Installing WindowsLive")
        oLogger.WriteToLog("Path   : " & oSettings.Path_WindowsLive, , 1)
        oLogger.WriteToLog("Params : " & sWindowsLiveParams, , 1)
        oLogger.WriteToLog("Timeout: 1800", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_WindowsLive
        oProcess.Arguments = sWindowsLiveParams
        oProcess.MaxTimeout = 1800
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _PrepareShortcutInstallation()
        Dim sPath As String

        sPath = IO.Path.GetDirectoryName(oSettings.Path_CreateShortcut) & "\CreateShortcut.exe"
    End Sub

    Private Sub _InstallShortCuts()
        Dim sPath As String
        Dim sTemplate As String
        Dim sShortcut1 As String, sShortcut2 As String

        oLogger.WriteToLog(oSettings.Path_CreateShortcut, Logger.MESSAGE_TYPE.LOG_DEBUG)

        'sPath = IO.Path.GetDirectoryName(oSettings.Path_CreateShortcut) & "\shortcuts.template.txt"
        sPath = IO.Path.GetDirectoryName(oSettings.Path_CreateShortcut) & "\CreateShortcut"

        Dim sr As New IO.StreamReader(sPath & "\shortcuts.template.txt")
        sTemplate = sr.ReadLine()
        sr.Close()

        sShortcut1 = sTemplate
        sShortcut1 = sShortcut1.Replace("%%NAME%%", "Start LobbyPC")
        sShortcut1 = sShortcut1.Replace("%%DESTINATION_FOLDER%%", GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory))
        sShortcut1 = sShortcut1.Replace("%%TARGET_PATH%%", oSettings.Path_AutoStart)
        sShortcut1 = sShortcut1.Replace("%%ARGUMENTS%%", "")
        sShortcut1 = sShortcut1.Replace("%%TARGET_WORKING_DIR%%", IO.Path.GetDirectoryName(oSettings.Path_AutoStart))
        sShortcut1 = sShortcut1.Replace("%%ICON_FILE%%", oSettings.Path_AutoStart)
        sShortcut1 = sShortcut1.Replace("%%ICON_INDEX%%", "0")

        sShortcut2 = sTemplate
        sShortcut2 = sShortcut2.Replace("%%NAME%%", "Start LobbyPC")
        sShortcut2 = sShortcut2.Replace("%%DESTINATION_FOLDER%%", GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu))
        sShortcut2 = sShortcut2.Replace("%%TARGET_PATH%%", oSettings.Path_AutoStart)
        sShortcut2 = sShortcut2.Replace("%%ARGUMENTS%%", "")
        sShortcut2 = sShortcut2.Replace("%%TARGET_WORKING_DIR%%", IO.Path.GetDirectoryName(oSettings.Path_AutoStart))
        sShortcut2 = sShortcut2.Replace("%%ICON_FILE%%", oSettings.Path_AutoStart)
        sShortcut2 = sShortcut2.Replace("%%ICON_INDEX%%", "0")

        Dim sw As New IO.StreamWriter(sPath & "\shortcuts.txt", False)
        sw.WriteLine(sShortcut1)
        sw.WriteLine(sShortcut2)
        sw.Close()

        LogToProcessTextBox("Creating shortcuts")
        LogToProcessTextBox(vbTab & "Path        : " & sPath & "\CreateShortcut.exe")
        LogToProcessTextBox(vbTab & "Timeout     : 300")

        oLogger.WriteToLog("Installing shortcut creator")
        oLogger.WriteToLog("Path   : " & sPath & "\CreateShortcut.exe", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = sPath & "\CreateShortcut.exe"
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallAltiris()
        LogToProcessTextBox("Installing remote administration application")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Path_Altiris)
        LogToProcessTextBox(vbTab & "Params      : ")
        LogToProcessTextBox(vbTab & "Timeout     : 300")

        oLogger.WriteToLog("Installing remote administration application")
        oLogger.WriteToLog("Path   : " & oSettings.Path_Altiris, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_Altiris
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "AltirisInstallation"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKiosk()
        LogToProcessTextBox("Installing kiosk software")
        LogToProcessTextBox(vbTab & "This may take up to 10 minutes to complete")
        LogToProcessTextBox(vbTab & "Path   : " & oSettings.Path_SiteKiosk)
        LogToProcessTextBox(vbTab & "Params : /S /V""/qn""")
        LogToProcessTextBox(vbTab & "Timeout: 1200")

        oLogger.WriteToLog("Installing kiosk software")
        oLogger.WriteToLog("Path   : " & oSettings.Path_SiteKiosk, , 1)
        oLogger.WriteToLog("Params : /S /V""/qn""", , 1)
        oLogger.WriteToLog("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKiosk
        oProcess.Arguments = "/S /V""/qn"""
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _PostSiteKioskUpdating()
        LogToProcessTextBox("Post-SiteKiosk updating")
        LogToProcessTextBox(vbTab & "Path   : " & oSettings.Path_PostSiteKioskUpdating)
        LogToProcessTextBox(vbTab & "Timeout: 10")

        oLogger.WriteToLog("Post-SiteKiosk updating")
        oLogger.WriteToLog("Path   : " & oSettings.Path_PostSiteKioskUpdating, , 1)
        oLogger.WriteToLog("Timeout: 10", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_PostSiteKioskUpdating
        oProcess.Arguments = ""
        'oProcess.UserName = cSITEKIOSK_UserName
        'oProcess.PassWord = cSITEKIOSK_PassWord
        oProcess.MaxTimeout = 10
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

#End Region
#End Region

#Region "Progress bars"
    Private Sub UpdateInstallationProgressBar()
        If iApplicationInstallationCount <= iApplicationInstallationCountMax Then
            UpdateInstallationProgressCount()
            progressInstallation.Value = (100 / iApplicationInstallationCountMax) * iApplicationInstallationCount
        End If
    End Sub

    Private Sub UpdateInstallationProgressCount()
        lblInstallationProgressStep.Text = (iApplicationInstallationCount + 1).ToString & " / " & (iApplicationInstallationCountMax + 1).ToString
        lblInstallationProgressStep.Visible = True
    End Sub


    Private Sub ProgressBarMarqueeStart()
        progressMarquee.Style = ProgressBarStyle.Marquee
        progressMarquee.MarqueeAnimationSpeed = 100
        progressMarquee.Value = 0
    End Sub

    Private Sub ProgressBarMarqueeStop()
        progressMarquee.Style = ProgressBarStyle.Blocks
        progressMarquee.MarqueeAnimationSpeed = 0
        progressMarquee.Value = 0
    End Sub
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationIsCanceled()
        Application.Exit()
        'gBusyInstalling = False

        'MsgBox("Installation cancelled by user", MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation, Application.ProductName)
    End Sub

    Private Sub ApplicationIsExited()
        oLogger.WriteToLog("Exiting application")

        If radRestartNow.Checked And iActiveGroupBox >= GroupBoxes.DoneAndRestart Then
            oLogger.WriteToLog("restarting", , 1)

            If gTestMode Then
                oLogger.WriteToLog("test mode, so not restarting after all", , 2)
                MsgBox("I would have restarted here!", MsgBoxStyle.OkOnly Or MsgBoxStyle.Information, Application.ProductName)
            Else
                oLogger.WriteToLog("bye bye", , 2)
                WindowsController.ExitWindows(RestartOptions.Reboot, True)
            End If
        End If

        Application.Exit()
    End Sub
#End Region
#End Region

#Region "Events"
#Region "DragWindow events"
    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picboxLogo.MouseDown
        Me.picboxLogo.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub lblTitle_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseDown
        Me.lblTitle.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        LogToProcessTextBox(vbTab & "Done - Time elapsed: " & TimeElapsed.ToString)
        oLogger.WriteToLog("done", , 1)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        LogToProcessTextBox(vbTab & "Process failed")
        oLogger.WriteToLog("failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        LogToProcessTextBox(vbTab & "Process started")
        oLogger.WriteToLog("started", , 1)
        oLogger.WriteToLog("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        LogToProcessTextBox(vbTab & "Process timed out")
        oLogger.WriteToLog("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
#End Region

#Region "Button Events"
    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        gBusyInstalling = True

        pnlInstallation.Top = pnlLicenseKey.Top
        pnlInstallation.Left = pnlLicenseKey.Left

        pnlLicenseKey.Visible = False
        pnlInstallation.Visible = True

        ProgressBarMarqueeStart()

        UnpackResourcesInBackground()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ApplicationIsCanceled()
    End Sub

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        NextGroupBox()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        PreviousGroupBox()
    End Sub

    Private Sub btnClearLicense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearLicense.Click
        txtLicenseCode.Text = ""
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        ApplicationIsExited()
    End Sub

    Private Sub btnTestHotelInformation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestHotelInformation.Click
        System.Diagnostics.Process.Start(txtHotelInformation.Text)
    End Sub
#End Region

#Region "Timer Events"
    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            If oProcess.ErrorsOccurred Then
                GotoGroupBox(GroupBoxes.ErrorDuringInstallation)
            Else
                iApplicationInstallationCount += 1
                InstallationController()

                UpdateInstallationProgressBar()
            End If
        End If
    End Sub

    Private Sub tmrUnpackWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrUnpackWait.Tick
        If Not Me.installThread.ThreadState = ThreadState.Running And Not Me.installThread.ThreadState = ThreadState.Unstarted Then
            tmrUnpackWait.Enabled = False

            iApplicationInstallationCount = 1
            UpdateInstallationProgressBar()

            StartDeployment()
        End If
    End Sub

    Private Sub tmrLicenseValidationDelay_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrLicenseValidationDelay.Tick
        tmrLicenseValidationDelay.Enabled = False

        CheckLicenseCode()
    End Sub
#End Region

#Region "CheckBox Events"
    Private Sub chkTCAgree_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTCAgree.CheckedChanged
        btnContinue.Enabled = chkTCAgree.Checked
    End Sub
#End Region

#Region "RadioButton Events"
    Private Sub radRestartNow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radRestartNow.CheckedChanged
        'UpdateButtons(0, 0, 0, 3)
    End Sub

    Private Sub radRestartLater_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radRestartLater.CheckedChanged
        'UpdateButtons(0, 0, 0, 2)
    End Sub
#End Region

#Region "ComboBox Events"
    Private Sub cmbBoxLanguages_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBoxLanguages.SelectedIndexChanged
        oLocalization.SetCurrentLanguage = cmbBoxLanguages.SelectedIndex + 1

        ReloadLocalization()
    End Sub
#End Region

#Region "Textbox Events"
    Private Sub txtLicenseCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLicenseCode.TextChanged
        grpboxLicenseError.Visible = False

        If txtLicenseCode.Text.Length = cLICENSE_KeyLength Then
            UpdateButtons(3, 0, 3, 0)
        Else
            UpdateButtons(3, 0, 2, 0)
        End If
    End Sub
#End Region

#Region "Paint Events"
    Private Sub pnlBackgroundBorder_Paint(ByVal sender As System.Object, ByVal pe As System.Windows.Forms.PaintEventArgs) Handles pnlBackgroundBorder.Paint
        If cGUI_GroupBoxBorder_visible Then
            pe.Graphics.DrawRectangle(cGUI_GroupBoxBorder_color, pe.ClipRectangle.Left, pe.ClipRectangle.Top, pe.ClipRectangle.Width - 1, pe.ClipRectangle.Height - 1)
        End If
    End Sub
#End Region

#Region "PicBox Events"
    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picClose.Click
        ApplicationIsCanceled()
    End Sub
#End Region

#Region "License Events"
    Private Sub onLicenseIsInvalid(ByVal ReturnCode As Integer, ByVal ReturnMessage As String) Handles oLicense.LicenseIsInvalid
        Dim sError As String = ""

        oLogger.WriteToLog("invalid", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        oLogger.WriteToLog(ReturnCode, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        oLogger.WriteToLog(ReturnMessage, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

        sError = oLocalization.CurrentLanguage.LicenseValidationProgressError
        Select Case ReturnCode
            Case 1
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage1
            Case 2
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage2
            Case -1
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage3
            Case -2
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage4
            Case Else
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessageUnknown
        End Select

        lblLicenseError.Text = sError
        grpboxLicenseError.Visible = True
        txtLicenseCode.SelectAll()

        PreviousGroupBox()
    End Sub

    Private Sub onLicenseValid() Handles oLicense.LicenseIsValid
        oLogger.WriteToLog("valid", , 2)

        'Save key in registry
        oLogger.WriteToLog("saving key to registry", , 1)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone", "LicenseKey", oLicense.LicenseCode)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("Exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog(".message" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

        'Go to next step
        NextGroupBox()
    End Sub
#End Region
#End Region

    Private Sub frmMain_MaximizedBoundsChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MaximizedBoundsChanged

    End Sub
End Class
