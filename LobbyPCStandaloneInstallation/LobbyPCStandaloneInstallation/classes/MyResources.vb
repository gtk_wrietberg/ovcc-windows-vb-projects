Public Class MyResources
    Private mDestinationFolder As String
    Private mLastFullPath As String
    Private mLastError As String

    Public Sub New()
        mDestinationFolder = ""
        mLastError = ""
    End Sub

    Public Property DestinationFolder() As String
        Get
            Return mDestinationFolder
        End Get
        Set(ByVal value As String)
            mDestinationFolder = value
        End Set
    End Property

    Public ReadOnly Property LastFullPath() As String
        Get
            Return mLastFullPath
        End Get
    End Property

    Public ReadOnly Property LastError() As String
        Get
            Return mLastError
        End Get
    End Property

    Public Function UnpackResource(ByVal ResourceName As String, Optional ByVal ResourceExtension As String = ".package.exe") As Boolean
        Try
            Dim byteResource As Byte() = Nothing

            If _FindResource(ResourceName, byteResource) Then
                Dim FilePath As String
                Dim FileFullPath As String
                FilePath = mDestinationFolder & "\" & ResourceName
                FileFullPath = FilePath & "\" & ResourceName & ResourceExtension

                If Not IO.Directory.Exists(FilePath) Then
                    IO.Directory.CreateDirectory(FilePath)
                End If

                Dim streamResource As IO.FileStream = New IO.FileStream(FileFullPath, IO.FileMode.Create, IO.FileAccess.ReadWrite)
                Dim binResource As IO.BinaryWriter = New IO.BinaryWriter(streamResource)

                binResource.Write(byteResource)

                binResource.Close()
                streamResource.Close()

                mLastFullPath = FileFullPath

                Return True
            Else
                mLastError = "Resource """ & ResourceName & """ does not exist"

                Return False
            End If
        Catch ex As Exception
            mLastError = "ex.Message=" & ex.Message & Environment.NewLine & _
                         "ex.Source =" & ex.Source
            Return False
        End Try
    End Function

    Public Shadows Function ToString(ByVal ResourceName As String) As String
        Try
            Dim strResource As String = ""

            If _FindResource(ResourceName, strResource) Then
                Return strResource
            Else
                Return "Error: (not found)"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Private Function _FindResource(ByVal ResourceName As String, ByRef ResourceBytes As Byte()) As Boolean
        Dim objResource As New Object

        objResource = My.Resources.ResourceManager.GetObject(ResourceName)

        If objResource.Equals(Nothing) Then
            Return False
        End If

        ResourceBytes = CType(objResource, Byte())

        If ResourceBytes.Length > 0 Then
            Return True
        End If
    End Function

    Private Function _FindResource(ByVal ResourceName As String, ByRef ResourceString As String) As Boolean
        ResourceString = My.Resources.ResourceManager.GetString(ResourceName)

        If ResourceString.Equals(Nothing) Then
            Return False
        End If

        If ResourceString.Length > 0 Then
            Return True
        End If
    End Function

End Class
