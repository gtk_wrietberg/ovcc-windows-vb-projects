Imports System.Xml

Public Class Localization
    Private mCurrentLanguage As Integer
    Private mLanguages As Collection

    Public Sub New()
        _LoadDefaultLanguage()
    End Sub

    Private Sub _LoadDefaultLanguage()
        Dim defaultLanguage As Language
        defaultLanguage = New Language

        mLanguages = New Collection

        mCurrentLanguage = 1
        mLanguages.Add(defaultLanguage)
    End Sub

    Public ReadOnly Property CurrentLanguage() As Language
        Get
            Return mLanguages(mCurrentLanguage)
        End Get
    End Property

    Public Property SetCurrentLanguage() As Integer
        Get
            Return mCurrentLanguage
        End Get
        Set(ByVal value As Integer)
            If value < 1 Or value > mLanguages.Count Then
                value = 1
            End If

            mCurrentLanguage = value
        End Set
    End Property

    Public Sub LoadLanguageFile()
        Dim xmlDoc As XmlDocument
        Dim tmpResources As New MyResources
        Dim newLanguage As Language

        mLanguages = New Collection

        Try
            Dim xmlRootNode As XmlNode
            Dim xmlLanguages As XmlNodeList
            Dim xmlLanguage As XmlNode

            xmlDoc = New XmlDocument
            xmlDoc.LoadXml(tmpResources.ToString("LanguageFile"))

            xmlRootNode = xmlDoc.SelectSingleNode("LobbyPCInstallation")
            xmlLanguages = xmlRootNode.SelectSingleNode("localization").SelectNodes("language")

            For Each xmlLanguage In xmlLanguages
                newLanguage = New Language

                newLanguage.Name = xmlLanguage.Attributes.GetNamedItem("name").InnerText

                newLanguage.Title = xmlLanguage.SelectSingleNode("Title").InnerText

                newLanguage.ButtonBack = xmlLanguage.SelectSingleNode("ButtonBack").InnerText
                newLanguage.ButtonCancel = xmlLanguage.SelectSingleNode("ButtonCancel").InnerText
                newLanguage.ButtonNext = xmlLanguage.SelectSingleNode("ButtonNext").InnerText
                newLanguage.ButtonExit = xmlLanguage.SelectSingleNode("ButtonExit").InnerText

                newLanguage.StartScreenWarningText = xmlLanguage.SelectSingleNode("StartScreenWarningText").InnerText
                newLanguage.StartScreenContinueText = xmlLanguage.SelectSingleNode("StartScreenContinueText").InnerText

                newLanguage.PrerequisitesErrorWindowsVersion = xmlLanguage.SelectSingleNode("PrerequisitesErrorWindowsVersion").InnerText
                newLanguage.PrerequisitesErrorMinimumWindowsVersion = xmlLanguage.SelectSingleNode("PrerequisitesErrorMinimumWindowsVersion").InnerText
                newLanguage.PrerequisitesErrorInternetConnection = xmlLanguage.SelectSingleNode("PrerequisitesErrorInternetConnection").InnerText
                newLanguage.PrerequisitesErrorAdminRights = xmlLanguage.SelectSingleNode("PrerequisitesErrorAdminRights").InnerText
                newLanguage.PrerequisitesErrorAlreadyInstalled = xmlLanguage.SelectSingleNode("PrerequisitesErrorAlreadyInstalled").InnerText
                newLanguage.PrerequisitesErrorRestartText = xmlLanguage.SelectSingleNode("PrerequisitesErrorRestartText").InnerText

                newLanguage.WelcomeSelectLanguage = xmlLanguage.SelectSingleNode("WelcomeSelectLanguage").InnerText
                newLanguage.WelcomeTitle = xmlLanguage.SelectSingleNode("WelcomeTitle").InnerText

                newLanguage.TermsAndConditionsCheckBox = xmlLanguage.SelectSingleNode("TermsAndConditionsCheckBox").InnerText
                newLanguage.TermsAndConditionsText = xmlLanguage.SelectSingleNode("TermsAndConditionsText").InnerText
                newLanguage.TermsAndConditionsTitle = xmlLanguage.SelectSingleNode("TermsAndConditionsTitle").InnerText

                newLanguage.LicenseValidationText = xmlLanguage.SelectSingleNode("LicenseValidationText").InnerText
                newLanguage.LicenseValidationTitle = xmlLanguage.SelectSingleNode("LicenseValidationTitle").InnerText
                newLanguage.LicenseValidationTitle2 = xmlLanguage.SelectSingleNode("LicenseValidationTitle2").InnerText

                newLanguage.LicenseValidationProgressText = xmlLanguage.SelectSingleNode("LicenseValidationProgressText").InnerText
                newLanguage.LicenseValidationProgressError = xmlLanguage.SelectSingleNode("LicenseValidationProgressError").InnerText
                newLanguage.LicenseValidationErrorMessage1 = xmlLanguage.SelectSingleNode("LicenseValidationErrorMessage1").InnerText
                newLanguage.LicenseValidationErrorMessage2 = xmlLanguage.SelectSingleNode("LicenseValidationErrorMessage2").InnerText
                newLanguage.LicenseValidationErrorMessage3 = xmlLanguage.SelectSingleNode("LicenseValidationErrorMessage3").InnerText
                newLanguage.LicenseValidationErrorMessage4 = xmlLanguage.SelectSingleNode("LicenseValidationErrorMessage4").InnerText
                newLanguage.LicenseValidationErrorMessageUnknown = xmlLanguage.SelectSingleNode("LicenseValidationErrorMessageUnknown").InnerText

                newLanguage.HotelInformationText = xmlLanguage.SelectSingleNode("HotelInformationText").InnerText

                newLanguage.InstallationProgressTitle = xmlLanguage.SelectSingleNode("InstallationProgressTitle").InnerText
                newLanguage.InstallationProgressText = xmlLanguage.SelectSingleNode("InstallationProgressText").InnerText

                newLanguage.DoneRestartNowText = xmlLanguage.SelectSingleNode("DoneRestartNowText").InnerText
                newLanguage.DoneRestartLaterText = xmlLanguage.SelectSingleNode("DoneRestartLaterText").InnerText
                newLanguage.DoneRestartText = xmlLanguage.SelectSingleNode("DoneRestartText").InnerText
                newLanguage.DoneRestartText2 = xmlLanguage.SelectSingleNode("DoneRestartText2").InnerText
                newLanguage.DoneTitle = xmlLanguage.SelectSingleNode("DoneTitle").InnerText

                mLanguages.Add(newLanguage)
            Next
        Catch ex As Exception
            _LoadDefaultLanguage()
        End Try
    End Sub

    Public Function GetLanguageNames() As String()
        Dim s() As String

        ReDim s(mLanguages.Count)

        Dim i As Integer

        For i = 1 To mLanguages.Count
            s(i - 1) = CType(mLanguages.Item(i), Language).Name
        Next

        Return s
    End Function

    '--------------------------------------------------------------------
    Public Class Language
        Private mName As String = ""

        Private mTitle As String = ""

        Private mButtonBack As String = ""
        Private mButtonCancel As String = ""
        Private mButtonNext As String = ""
        Private mButtonExit As String = ""

        Private mStartScreenWarningText As String = ""
        Private mStartScreenContinueText As String = ""

        Private mPrerequisitesErrorWindowsVersion As String = ""
        Private mPrerequisitesErrorMinimumWindowsVersion As String = ""
        Private mPrerequisitesErrorInternetConnection As String = ""
        Private mPrerequisitesErrorAdminRights As String = ""
        Private mPrerequisitesErrorAlreadyInstalled As String = ""
        Private mPrerequisitesErrorRestartText As String = ""

        Private mWelcomeTitle As String = ""
        Private mWelcomeSelectLanguage As String = ""

        Private mTermsAndConditionsTitle As String = ""
        Private mTermsAndConditionsText As String = ""
        Private mTermsAndConditionsCheckBox As String = ""

        Private mLicenseValidationTitle As String = ""
        Private mLicenseValidationText As String = ""
        Private mLicenseValidationTitle2 As String = ""

        Private mLicenseValidationProgressText As String = ""
        Private mLicenseValidationProgressError As String = ""

        Private mLicenseValidationErrorMessage1 As String = ""
        Private mLicenseValidationErrorMessage2 As String = ""
        Private mLicenseValidationErrorMessage3 As String = ""
        Private mLicenseValidationErrorMessage4 As String = ""
        Private mLicenseValidationErrorMessageUnknown As String = ""

        Private mHotelInformationText As String = ""

        Private mInstallationProgressTitle As String = ""
        Private mInstallationProgressText As String = ""

        Private mDoneTitle As String = ""
        Private mDoneRestartText As String = ""
        Private mDoneRestartText2 As String = ""
        Private mDoneRestartNowText As String = ""
        Private mDoneRestartLaterText As String = ""

        Public Sub New()

        End Sub

        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property


        Public Property Title() As String
            Get
                Return mTitle
            End Get
            Set(ByVal value As String)
                mTitle = value
            End Set
        End Property


        Public Property ButtonBack() As String
            Get
                Return mButtonBack
            End Get
            Set(ByVal value As String)
                mButtonBack = value
            End Set
        End Property

        Public Property ButtonCancel() As String
            Get
                Return mButtonCancel
            End Get
            Set(ByVal value As String)
                mButtonCancel = value
            End Set
        End Property

        Public Property ButtonNext() As String
            Get
                Return mButtonNext
            End Get
            Set(ByVal value As String)
                mButtonNext = value
            End Set
        End Property

        Public Property ButtonExit() As String
            Get
                Return mButtonExit
            End Get
            Set(ByVal value As String)
                mButtonExit = value
            End Set
        End Property


        Public Property StartScreenWarningText() As String
            Get
                Return mStartScreenWarningText
            End Get
            Set(ByVal value As String)
                mStartScreenWarningText = value
            End Set
        End Property

        Public Property StartScreenContinueText() As String
            Get
                Return mStartScreenContinueText
            End Get
            Set(ByVal value As String)
                mStartScreenContinueText = value
            End Set
        End Property


        Public Property PrerequisitesErrorWindowsVersion() As String
            Get
                Return mPrerequisitesErrorWindowsVersion
            End Get
            Set(ByVal value As String)
                mPrerequisitesErrorWindowsVersion = value
            End Set
        End Property

        Public Property PrerequisitesErrorMinimumWindowsVersion() As String
            Get
                Return mPrerequisitesErrorMinimumWindowsVersion
            End Get
            Set(ByVal value As String)
                mPrerequisitesErrorMinimumWindowsVersion = value
            End Set
        End Property

        Public Property PrerequisitesErrorInternetConnection() As String
            Get
                Return mPrerequisitesErrorInternetConnection
            End Get
            Set(ByVal value As String)
                mPrerequisitesErrorInternetConnection = value
            End Set
        End Property

        Public Property PrerequisitesErrorAdminRights() As String
            Get
                Return mPrerequisitesErrorAdminRights
            End Get
            Set(ByVal value As String)
                mPrerequisitesErrorAdminRights = value
            End Set
        End Property

        Public Property PrerequisitesErrorAlreadyInstalled() As String
            Get
                Return mPrerequisitesErrorAlreadyInstalled
            End Get
            Set(ByVal value As String)
                mPrerequisitesErrorAlreadyInstalled = value
            End Set
        End Property

        Public Property PrerequisitesErrorRestartText() As String
            Get
                Return mPrerequisitesErrorRestartText
            End Get
            Set(ByVal value As String)
                mPrerequisitesErrorRestartText = value
            End Set
        End Property


        Public Property WelcomeTitle() As String
            Get
                Return mWelcomeTitle
            End Get
            Set(ByVal value As String)
                mWelcomeTitle = value
            End Set
        End Property

        Public Property WelcomeSelectLanguage() As String
            Get
                Return mWelcomeSelectLanguage
            End Get
            Set(ByVal value As String)
                mWelcomeSelectLanguage = value
            End Set
        End Property


        Public Property TermsAndConditionsTitle() As String
            Get
                Return mTermsAndConditionsTitle
            End Get
            Set(ByVal value As String)
                mTermsAndConditionsTitle = value
            End Set
        End Property

        Public Property TermsAndConditionsText() As String
            Get
                Return mTermsAndConditionsText
            End Get
            Set(ByVal value As String)
                mTermsAndConditionsText = value
            End Set
        End Property

        Public Property TermsAndConditionsCheckBox() As String
            Get
                Return mTermsAndConditionsCheckBox
            End Get
            Set(ByVal value As String)
                mTermsAndConditionsCheckBox = value
            End Set
        End Property


        Public Property LicenseValidationTitle() As String
            Get
                Return mLicenseValidationTitle
            End Get
            Set(ByVal value As String)
                mLicenseValidationTitle = value
            End Set
        End Property

        Public Property LicenseValidationText() As String
            Get
                Return mLicenseValidationText
            End Get
            Set(ByVal value As String)
                mLicenseValidationText = value
            End Set
        End Property

        Public Property LicenseValidationTitle2() As String
            Get
                Return mLicenseValidationTitle2
            End Get
            Set(ByVal value As String)
                mLicenseValidationTitle2 = value
            End Set
        End Property


        Public Property LicenseValidationProgressText() As String
            Get
                Return mLicenseValidationProgressText
            End Get
            Set(ByVal value As String)
                mLicenseValidationProgressText = value
            End Set
        End Property

        Public Property LicenseValidationProgressError() As String
            Get
                Return mLicenseValidationProgressError
            End Get
            Set(ByVal value As String)
                mLicenseValidationProgressError = value
            End Set
        End Property

        Public Property LicenseValidationErrorMessage1() As String
            Get
                Return mLicenseValidationErrorMessage1
            End Get
            Set(ByVal value As String)
                mLicenseValidationErrorMessage1 = value
            End Set
        End Property

        Public Property LicenseValidationErrorMessage2() As String
            Get
                Return mLicenseValidationErrorMessage2
            End Get
            Set(ByVal value As String)
                mLicenseValidationErrorMessage2 = value
            End Set
        End Property

        Public Property LicenseValidationErrorMessage3() As String
            Get
                Return mLicenseValidationErrorMessage3
            End Get
            Set(ByVal value As String)
                mLicenseValidationErrorMessage3 = value
            End Set
        End Property

        Public Property LicenseValidationErrorMessage4() As String
            Get
                Return mLicenseValidationErrorMessage4
            End Get
            Set(ByVal value As String)
                mLicenseValidationErrorMessage4 = value
            End Set
        End Property

        Public Property LicenseValidationErrorMessageUnknown() As String
            Get
                Return mLicenseValidationErrorMessageUnknown
            End Get
            Set(ByVal value As String)
                mLicenseValidationErrorMessageUnknown = value
            End Set
        End Property


        Public Property HotelInformationText() As String
            Get
                Return mHotelInformationText
            End Get
            Set(ByVal value As String)
                mHotelInformationText = value
            End Set
        End Property


        Public Property InstallationProgressTitle() As String
            Get
                Return mInstallationProgressTitle
            End Get
            Set(ByVal value As String)
                mInstallationProgressTitle = value
            End Set
        End Property

        Public Property InstallationProgressText() As String
            Get
                Return mInstallationProgressText
            End Get
            Set(ByVal value As String)
                mInstallationProgressText = value
            End Set
        End Property


        Public Property DoneTitle() As String
            Get
                Return mDoneTitle
            End Get
            Set(ByVal value As String)
                mDoneTitle = value
            End Set
        End Property

        Public Property DoneRestartText() As String
            Get
                Return mDoneRestartText
            End Get
            Set(ByVal value As String)
                mDoneRestartText = value
            End Set
        End Property

        Public Property DoneRestartText2() As String
            Get
                Return mDoneRestartText2
            End Get
            Set(ByVal value As String)
                mDoneRestartText2 = value
            End Set
        End Property

        Public Property DoneRestartNowText() As String
            Get
                Return mDoneRestartNowText
            End Get
            Set(ByVal value As String)
                mDoneRestartNowText = value
            End Set
        End Property

        Public Property DoneRestartLaterText() As String
            Get
                Return mDoneRestartLaterText
            End Get
            Set(ByVal value As String)
                mDoneRestartLaterText = value
            End Set
        End Property
    End Class
End Class
