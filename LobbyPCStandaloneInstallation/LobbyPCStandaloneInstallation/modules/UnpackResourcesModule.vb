Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sSourceFolder As String
        Dim sDestinationFolder As String

        sSourceFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder
        sDestinationFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder

        oLogger.WriteToLog("Altiris package", , 1)
        _CopyFile("Altiris.package.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.exe"

        oLogger.WriteToLog("AdobeReader package", , 1)
        _CopyFile("AdobeReader_9.4.package.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_PdfReader = sDestinationFolder & "\AdobeReader_9.4.package.exe"

        oLogger.WriteToLog("CreateShortcut package", , 1)
        _CopyFile("CreateShortcut.package.exe", sSourceFolder, sDestinationFolder, 2) 
        oSettings.Path_CreateShortcut = sDestinationFolder & "\CreateShortcut.package.exe"

        oLogger.WriteToLog("pcAnywhere package", , 1)
        _CopyFile("pcAnywhere.exe", sSourceFolder, sDestinationFolder, 2) 
        oSettings.Path_pcAnywhere = sDestinationFolder & "\pcAnywhere.exe"

        oLogger.WriteToLog("FlashPlayer package", , 1)
        _CopyFile("FlashPlayer.package.exe", sSourceFolder, sDestinationFolder, 2) 
        oSettings.Path_FlashPlayer = sDestinationFolder & "\FlashPlayer.package.exe"

        oLogger.WriteToLog("iBAHN Updater package (Win7)", , 1)
        _CopyFile("iBAHNUpdate.package.Win7.exe", sSourceFolder, sDestinationFolder, 2) 
        oSettings.Path_iBAHNUpdate = sDestinationFolder & "\iBAHNUpdate.package.Win7.exe"

        oLogger.WriteToLog("LobbyPCAgent package", , 1)
        _CopyFile("LobbyPCAgentPatchInstaller.package.exe", sSourceFolder, sDestinationFolder, 2) 
        oSettings.Path_LobbyPCAgent = sDestinationFolder & "\LobbyPCAgentPatchInstaller.package.exe"

        oLogger.WriteToLog("LobbyPCWatchdog package", , 1)
        _CopyFile("LobbyPCWatchdog.package.exe", sSourceFolder, sDestinationFolder, 2) 
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\LobbyPCWatchdog.package.exe"

        oLogger.WriteToLog("PdfProxy package", , 1)
        _CopyFile("PdfProxy.package.exe", sSourceFolder, sDestinationFolder, 2) 
        oSettings.Path_PdfProxy = sDestinationFolder & "\PdfProxy.package.exe"

        oLogger.WriteToLog("Skype", , 1)
        _CopyFile("SkypeSetup.msi", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_Skype = sDestinationFolder & "\SkypeSetup.msi"

        oLogger.WriteToLog("WindowsLive web installer", , 1)
        _CopyFile("wlsetup-web.exe", sSourceFolder, sDestinationFolder, 2) 
        oSettings.Path_WindowsLive = sDestinationFolder & "\wlsetup-web.exe"

        oLogger.WriteToLog("SiteKiosk7", , 1)
        _CopyFile("sitekiosk7.exe", sSourceFolder, sDestinationFolder,2) 
        oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk7.exe"


        sDestinationFolder = cPATHS_iBAHNProgramFilesFolder

        oLogger.WriteToLog("LobbyPCStandalonePostSiteKiosk", , 1)
        _CopyFile("LobbyPCStandalonePostSiteKiosk.exe", sSourceFolder, sDestinationFolder,2) 
        oSettings.Path_PostSiteKioskUpdating = sDestinationFolder & "\LobbyPCStandalonePostSiteKiosk.exe"

        oLogger.WriteToLog("PcHasRebooted", , 1)
        _CopyFile("PcHasRebooted.exe", sSourceFolder, sDestinationFolder,2) 
        oSettings.Path_PcHasRebooted = sDestinationFolder & "\PcHasRebooted.exe"

        oLogger.WriteToLog("LobbyPCAutoStart", , 1)
        _CopyFile("LobbyPCAutoStart.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("LobbyPCAutoStart.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_AutoStart = sDestinationFolder & "\LobbyPCAutoStart.exe"

    End Sub

    Private Sub _CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, ByVal iLogLevel As Integer)
        oLogger.WriteToLog("copying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)

        oLogger.WriteToLog("retrying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
    End Sub
End Module
