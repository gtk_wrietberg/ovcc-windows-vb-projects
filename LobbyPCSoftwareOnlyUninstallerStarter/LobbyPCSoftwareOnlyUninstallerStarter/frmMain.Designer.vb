<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.picboxLogo = New System.Windows.Forms.PictureBox
        Me.tmrStart = New System.Windows.Forms.Timer(Me.components)
        Me.tmrExit = New System.Windows.Forms.Timer(Me.components)
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picboxLogo
        '
        Me.picboxLogo.BackColor = System.Drawing.Color.Transparent
        Me.picboxLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.picboxLogo.Image = Global.LobbyPCSoftwareOnlyUninstaller.My.Resources.Resources.iBAHN_logo_01
        Me.picboxLogo.Location = New System.Drawing.Point(12, 12)
        Me.picboxLogo.Name = "picboxLogo"
        Me.picboxLogo.Size = New System.Drawing.Size(120, 68)
        Me.picboxLogo.TabIndex = 29
        Me.picboxLogo.TabStop = False
        '
        'tmrStart
        '
        Me.tmrStart.Interval = 1000
        '
        'tmrExit
        '
        Me.tmrExit.Interval = 500
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(144, 92)
        Me.ControlBox = False
        Me.Controls.Add(Me.picboxLogo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmMain"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picboxLogo As System.Windows.Forms.PictureBox
    Friend WithEvents tmrStart As System.Windows.Forms.Timer
    Friend WithEvents tmrExit As System.Windows.Forms.Timer

End Class
