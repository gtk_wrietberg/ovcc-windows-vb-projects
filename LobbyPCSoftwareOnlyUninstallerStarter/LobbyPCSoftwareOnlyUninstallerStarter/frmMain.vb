Public Class frmMain
    Private myProcess As New System.Diagnostics.Process

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tmrStart.Enabled = True
    End Sub

    Private Sub tmrStart_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrStart.Tick
        tmrStart.Enabled = False

        StartUninstaller()
    End Sub

    Private Sub StartUninstaller()
        Dim sTempPath As String
        Dim sAppName As String, sManifestName As String

        sAppName = "LobbyPCSoftwareOnlyUninstaller_.exe"
        sManifestName = "LobbyPCSoftwareOnlyUninstaller_.exe.manifest"
        sTempPath = IO.Path.GetTempPath

        Try
            If IO.File.Exists(cPATHS_iBAHNProgramFilesFolder_32 & "\" & sAppName) Then
                IO.File.Copy(cPATHS_iBAHNProgramFilesFolder_32 & "\" & sAppName, sTempPath & sAppName, True)
                IO.File.Copy(cPATHS_iBAHNProgramFilesFolder_32 & "\" & sManifestName, sTempPath & sManifestName, True)
            Else
                IO.File.Copy(cPATHS_iBAHNProgramFilesFolder_64 & "\" & sAppName, sTempPath & sAppName, True)
                IO.File.Copy(cPATHS_iBAHNProgramFilesFolder_64 & "\" & sManifestName, sTempPath & sManifestName, True)
            End If
        Catch ex As Exception
            MsgBox("Unable to copy uninstaller to temporary location. Please run manually.", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Application.ProductName)

            ExitApplication()

            Exit Sub
        End Try

        If IO.File.Exists(sTempPath & sAppName) Then
            myProcess = New Diagnostics.Process

            Dim sArgs As String = "", sArg As String

            For Each sArg In My.Application.CommandLineArgs
                sArgs &= " " & sArg
            Next

            myProcess.StartInfo.FileName = sTempPath & sAppName
            myProcess.StartInfo.Arguments = sArgs
            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal
            myProcess.EnableRaisingEvents = True
            myProcess.Start()

            tmrExit.Enabled = True
        Else
            MsgBox("Unable to run uninstaller from temporary location. Please run manually.", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Application.ProductName)

            ExitApplication()

            Exit Sub
        End If
    End Sub

    Private Sub ExitApplication()
        Application.Exit()
    End Sub

    Private Sub tmrExit_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrExit.Tick
        tmrExit.Enabled = False

        ExitApplication()
    End Sub
End Class
