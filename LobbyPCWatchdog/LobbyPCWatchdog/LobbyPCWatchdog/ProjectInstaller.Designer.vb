﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LPCWdServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.LPCWdServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'LPCWdServiceProcessInstaller
        '
        Me.LPCWdServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.LPCWdServiceProcessInstaller.Password = Nothing
        Me.LPCWdServiceProcessInstaller.Username = Nothing
        '
        'LPCWdServiceInstaller
        '
        Me.LPCWdServiceInstaller.Description = "LobbyPCWatchdog"
        Me.LPCWdServiceInstaller.DisplayName = "LobbyPCWatchdog"
        Me.LPCWdServiceInstaller.ServiceName = "LobbyPCWatchdog_service"
        Me.LPCWdServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.LPCWdServiceInstaller, Me.LPCWdServiceProcessInstaller})

    End Sub
    Friend WithEvents LPCWdServiceProcessInstaller As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents LPCWdServiceInstaller As System.ServiceProcess.ServiceInstaller

End Class
