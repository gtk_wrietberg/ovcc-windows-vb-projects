﻿Imports System.Runtime.InteropServices



Public Class LPCState
    <DllImport("advapi32.dll", SetLastError:=True)> _
    Private Shared Function OpenProcessToken( _
    ByVal ProcessHandle As IntPtr, _
    ByVal DesiredAccess As Integer, _
    ByRef TokenHandle As IntPtr _
    ) As Boolean
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)> _
    Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Private mUsernameToCheck As String
    Private ReadOnly c_CompletelyArbitraryProcessCountThreshold As Integer = 2

    Public Property UsernameToCheck As String
        Get
            Return mUsernameToCheck
        End Get
        Set(value As String)
            mUsernameToCheck = value
        End Set
    End Property

    Public Function IsExplorerRunning() As Boolean
        Return _IsProcessRunning("Explorer")
    End Function

    Public Function IsSiteKioskRunning() As Boolean
        Return _IsProcessRunning("SiteKiosk")
    End Function

    Public Function IsUserLoggedIn() As Boolean
        Return IsUserLoggedIn(mUsernameToCheck)
    End Function

    Public Function IsUserLoggedIn(UserName As String) As Boolean
        Dim iCount As Integer

        iCount = _CountProcessesOwnedByUser(UserName)

        If iCount >= c_CompletelyArbitraryProcessCountThreshold Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function _IsProcessRunning(sProcessName As String) As Boolean
        Dim bRet As Boolean = False

        For Each p As Process In Process.GetProcesses
            If p.ProcessName.Contains(sProcessName) Then
                bRet = True
                Exit For
            End If
        Next

        Return bRet
    End Function

    Private Function _CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
        Dim wiName As String, lCount As Long

        lCount = 0

        For Each p As Process In Process.GetProcesses
            Try
                Dim hToken As IntPtr
                If OpenProcessToken(p.Handle, Security.Principal.TokenAccessLevels.Query, hToken) Then
                    Using wi As New Security.Principal.WindowsIdentity(hToken)
                        wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                        If wiName.ToLower = sUserNameToCheck.ToLower Then
                            lCount += 1
                        End If
                    End Using
                    CloseHandle(hToken)
                End If
            Catch ex As Exception

            End Try
        Next

        Return lCount
    End Function
End Class
