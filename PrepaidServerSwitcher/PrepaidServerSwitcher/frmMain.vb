Public Class frmMain
    Private sPrimaryServer As String = ""
    Private sPrimaryServerName As String = ""
    Private sSecondaryServer As String = ""
    Private sSecondaryServerName As String = ""

    Private ReadOnly SCRIPT_FILE_NAME As String = "C:\Program Files\SiteKiosk\skins\default\scripts\mycall.js"

    Private ReadOnly SERVER_URL_EU As String = "https://ppc02.ibahn.com/testverification/userpost.asp"
    Private ReadOnly SERVER_NAME_EU As String = "EMEA prepaid server (https)"
    Private ReadOnly SERVER_URL_US As String = "https://ppc01.ibahn.com/testverification/userpost.asp"
    Private ReadOnly SERVER_NAME_US As String = "US prepaid server (https)"

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Visible = False

        Dim args() As String, i As Integer

        args = Environment.GetCommandLineArgs()

        If args.Length <= 1 Then
            ExitApplication(-1)
        End If

        For i = 1 To args.Length - 1
            If args(i).IndexOf("--primary-server:") >= 0 Then
                sPrimaryServer = args(i).Replace("--primary-server:", "")
            End If
        Next

        If Not sPrimaryServer.Equals("EU") And Not sPrimaryServer.Equals("US") Then
            ExitApplication(-2)
        End If

        If sPrimaryServer = "EU" Then
            sPrimaryServer = SERVER_URL_EU
            sPrimaryServerName = SERVER_NAME_EU
            sSecondaryServer = SERVER_URL_US
            sSecondaryServerName = SERVER_NAME_US
        Else
            sPrimaryServer = SERVER_URL_US
            sPrimaryServerName = SERVER_NAME_US
            sSecondaryServer = SERVER_URL_EU
            sSecondaryServerName = SERVER_NAME_EU
        End If

        SwitchPrepaidServer()

        ExitApplication()
    End Sub

    Private Sub ExitApplication(Optional ByVal iExitCode As Integer = 0)
        Environment.Exit(iExitCode)
    End Sub

    Private Sub SwitchPrepaidServer()
        If Not System.IO.File.Exists(SCRIPT_FILE_NAME) Then
            ExitApplication(-3)
        End If

        If Not BackupFile(SCRIPT_FILE_NAME) Then
            ExitApplication(-4)
        End If

        Dim sContents As String = ""
        Dim sLine As String
        Dim oRead As New System.IO.StreamReader(SCRIPT_FILE_NAME)



        Do While oRead.Peek() <> -1
            sLine = oRead.ReadLine

            If sLine.IndexOf("ServerPages[0]") >= 0 Then
                sContents = sContents & "ServerPages[0]=""" & sPrimaryServer & """;" & vbNewLine
            ElseIf sLine.IndexOf("ServerNames[0]") >= 0 Then
                sContents = sContents & "ServerNames[0]=""" & sPrimaryServerName & """;" & vbNewLine
            ElseIf sLine.IndexOf("ServerPages[1]") >= 0 Then
                sContents = sContents & "ServerPages[1]=""" & sSecondaryServer & """;" & vbNewLine
            ElseIf sLine.IndexOf("ServerNames[1]") >= 0 Then
                sContents = sContents & "ServerNames[1]=""" & sSecondaryServerName & """;" & vbNewLine
            Else
                sContents = sContents & sLine & vbNewLine
            End If
        Loop
        oRead.Close()

        Dim oWrite As New System.IO.StreamWriter(SCRIPT_FILE_NAME)
        oWrite.Write(sContents)
        oWrite.Close()
    End Sub
End Class
