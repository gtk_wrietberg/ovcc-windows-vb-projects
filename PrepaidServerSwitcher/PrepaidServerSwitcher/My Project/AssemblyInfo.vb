﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("PrepaidServerSwitcher")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("PrepaidServerSwitcher")> 
<Assembly: AssemblyCopyright("Copyright ©  2009")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("9234da4f-98b2-45c6-b569-aa12f00065d1")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.6.1305")> 
<Assembly: AssemblyFileVersion("1.0.6.1305")> 
