Module modMain

    Private oComputerName As ComputerName

    Public Sub Main()
        Dim iExitCode As Integer

        iExitCode = 0

        oLogger = New Logger
        oLogger.LogFilePath = "C:\Program Files\iBAHN\"
        oLogger.LogFileName = "LPCSoftware_SP1.log"

        oComputerName = New ComputerName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        oLogger.WriteToLog(New String("-", 10))

        '--------------------
        oLogger.WriteToLog("Bug 6021 - Sitekiosk Unauthorized Function Call Popup", , 0)
        If Not Bug6021_SitekioskUnauthorizedFunctionCallPopup() Then
            oLogger.WriteToLog("fail", , 1)
            iExitCode += 1
        Else
            oLogger.WriteToLog("ok", , 1)
        End If

        '--------------------
        oLogger.WriteToLog("SkCFG updating", , 0)
        UpdateSkCFG()
    End Sub

    Public Sub ExitApplication(ByVal iExitCode As Integer)
        oLogger.WriteToLog("Exiting...", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        oLogger.WriteToLog("exit code: " & iExitCode & " (" & iExitCode.ToString & ")", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLog("bye", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLogWithoutDate(New String("=", 100))

        System.Environment.Exit(iExitCode)
    End Sub

    Private Function Bug6021_SitekioskUnauthorizedFunctionCallPopup() As Boolean
        Try

            Dim oHive As RegistryHive

            oLogger.WriteToLogRelative("updating registry for SiteKiosk user", , 1)


            oLogger.WriteToLogRelative("initialising", , 2)
            oHive = New RegistryHive

            If oHive.Initialise Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                Return False
            End If


            oLogger.WriteToLogRelative("loading SiteKiosk hive", , 2)
            oLogger.WriteToLogRelative(cSiteKioskRegistryHive, , 3)
            If Not IO.File.Exists(cSiteKioskRegistryHive) Then
                oLogger.WriteToLogRelative("does not exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                Return False
            End If

            If oHive.LoadHive(cSiteKioskRegistryHive) Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                oHive.UnloadHive()

                Return False
            End If


            oLogger.WriteToLogRelative("setting values", , 2)

            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_SecureProtocols, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_SecureProtocols.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_SecureProtocols, cSiteKioskRegistry_Value_SecureProtocols) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings_Zone3, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_DisplayMixedContent, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_DisplayMixedContent.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_DisplayMixedContent, cSiteKioskRegistry_Value_DisplayMixedContent) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If


            oLogger.WriteToLogRelative("unloading SiteKiosk hive", , 2)
            If oHive.UnloadHive() Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                Return False
            End If

            Return True

        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative("ex.Source", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function

    Private Function UpdateSkCFG() As Boolean
        'Try
        Dim mSkCfgFile As String

        Dim nt As Xml.XmlNameTable
        Dim ns As Xml.XmlNamespaceManager

        Dim mSkCfgXml As Xml.XmlDocument

        Dim mXmlNode_Root As Xml.XmlNode
        Dim mXmlNode_SurfArea As Xml.XmlNode
        Dim mXmlNodeList_Urls As Xml.XmlNodeList
        Dim mXmlNode_Node As Xml.XmlNode
        Dim mXmlNode_Url As Xml.XmlNode
        Dim mXmlNode_ScreenSaver As Xml.XmlNode
        Dim mXmlNode_New As Xml.XmlNode
        Dim mXmlNode_WindowManager As Xml.XmlNode
        Dim mXmlNodeList_Windows As Xml.XmlNodeList
        Dim mXmlNode_Window As Xml.XmlNode


        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

        mSkCfgXml = New Xml.XmlDocument

        oLogger.WriteToLogRelative("loading skcfg", , 1)
        oLogger.WriteToLogRelative(mSkCfgFile, , 2)
        mSkCfgXml.Load(mSkCfgFile)
        oLogger.WriteToLogRelative("ok", , 3)

        nt = mSkCfgXml.NameTable
        ns = New Xml.XmlNamespaceManager(nt)
        ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
        ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

        oLogger.WriteToLogRelative("editing skcfg", , 1)

        oLogger.WriteToLogRelative("sitekiosk-configuration", , 2)
        mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

        '--------------------------------------------------------------------------------------------------------
        oLogger.WriteToLogRelative("surfarea", , 3)
        mXmlNode_SurfArea = mXmlNode_Root.SelectSingleNode("sk:surfarea", ns)
        mXmlNodeList_Urls = mXmlNode_SurfArea.SelectNodes("sk:url", ns)

        For Each mXmlNode_Url In mXmlNodeList_Urls
            oLogger.WriteToLogRelative("deleting url", , 4)
            oLogger.WriteToLogRelative(mXmlNode_Url.InnerText, , 5)
            mXmlNode_SurfArea.RemoveChild(mXmlNode_Url)
        Next

        oLogger.WriteToLogRelative("adding url", , 4)
        oLogger.WriteToLogRelative("file://$(WinDir)\*.ht*", , 5)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
        mXmlNode_New.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("allowed"))
        mXmlNode_New.Attributes.GetNamedItem("allowed").Value = "false"
        mXmlNode_New.InnerText = "file://$(WinDir)\*.ht*"
        mXmlNode_SurfArea.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("adding url", , 4)
        oLogger.WriteToLogRelative("file://$(UserProfile)\*.ht*", , 5)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
        mXmlNode_New.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("allowed"))
        mXmlNode_New.Attributes.GetNamedItem("allowed").Value = "false"
        mXmlNode_New.InnerText = "file://$(UserProfile)\*.ht*"
        mXmlNode_SurfArea.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("adding url", , 4)
        oLogger.WriteToLogRelative("file://$(AllUsersProfile)\*Microsoft*\*.ht*", , 5)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
        mXmlNode_New.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("allowed"))
        mXmlNode_New.Attributes.GetNamedItem("allowed").Value = "false"
        mXmlNode_New.InnerText = "file://$(AllUsersProfile)\*Microsoft*\*.ht*"
        mXmlNode_SurfArea.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("adding url", , 4)
        oLogger.WriteToLogRelative("file://$(SiteKioskPath)\*", , 5)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
        mXmlNode_New.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("allowed"))
        mXmlNode_New.Attributes.GetNamedItem("allowed").Value = "false"
        mXmlNode_New.InnerText = "file://$(SiteKioskPath)\*"
        mXmlNode_SurfArea.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("adding url", , 4)
        oLogger.WriteToLogRelative("file://$(SiteKioskPath)\html\*", , 5)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
        mXmlNode_New.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("allowed"))
        mXmlNode_New.Attributes.GetNamedItem("allowed").Value = "true"
        mXmlNode_New.InnerText = "file://$(SiteKioskPath)\html\*"
        mXmlNode_SurfArea.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("adding url", , 4)
        oLogger.WriteToLogRelative("file://$(SiteKioskPath)\skins\public\*", , 5)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
        mXmlNode_New.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("allowed"))
        mXmlNode_New.Attributes.GetNamedItem("allowed").Value = "true"
        mXmlNode_New.InnerText = "file://$(SiteKioskPath)\skins\public\*"
        mXmlNode_SurfArea.AppendChild(mXmlNode_New)

        '--------------------------------------------------------------------------------------------------------
        oLogger.WriteToLogRelative("screensaver", , 3)
        mXmlNode_ScreenSaver = mXmlNode_Root.SelectSingleNode("sk:screensaver", ns)
        oLogger.WriteToLogRelative("disable-if-credit", , 4)
        mXmlNode_Node = mXmlNode_ScreenSaver.SelectSingleNode("sk:disable-if-credit", ns)
        oLogger.WriteToLogRelative("true", , 5)
        mXmlNode_Node.InnerText = "true"

        '--------------------------------------------------------------------------------------------------------
        oLogger.WriteToLogRelative("screensaver", , 3)
        mXmlNode_ScreenSaver = mXmlNode_Root.SelectSingleNode("sk:screensaver", ns)
        oLogger.WriteToLogRelative("disable-if-credit", , 4)
        mXmlNode_Node = mXmlNode_ScreenSaver.SelectSingleNode("sk:disable-if-credit", ns)
        oLogger.WriteToLogRelative("true", , 5)
        mXmlNode_Node.InnerText = "true"


        '--------------------------------------------------------------------------------------------------------
        oLogger.WriteToLogRelative("windowmanager", , 3)
        mXmlNode_WindowManager = mXmlNode_Root.SelectSingleNode("sk:windowmanager", ns)

        mXmlNodeList_Windows = mXmlNode_WindowManager.SelectNodes("sk:window", ns)

        For Each mXmlNode_Window In mXmlNodeList_Windows
            mXmlNode_Node = mXmlNode_Window.SelectSingleNode("sk:name", ns)
            If mXmlNode_Node.InnerText = "Windows Media Center" Then
                oLogger.WriteToLogRelative("deleting window", , 4)
                oLogger.WriteToLogRelative(mXmlNode_Node.InnerText, , 5)
                mXmlNode_WindowManager.RemoveChild(mXmlNode_Window)
            End If
            If mXmlNode_Node.InnerText = "Windows Media Centre" Then
                oLogger.WriteToLogRelative("deleting window", , 4)
                oLogger.WriteToLogRelative(mXmlNode_Node.InnerText, , 5)
                mXmlNode_WindowManager.RemoveChild(mXmlNode_Window)
            End If
        Next

        oLogger.WriteToLogRelative("adding window", , 4)
        mXmlNode_Window = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "window", ns.LookupNamespace("sk"))
        mXmlNode_Window.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("id"))
        mXmlNode_Window.Attributes.GetNamedItem("id").Value = "-1"

        oLogger.WriteToLogRelative("close-on-screensaver", , 5)
        oLogger.WriteToLogRelative("true", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "close-on-screensaver", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "true"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("kill-process", , 5)
        oLogger.WriteToLogRelative("false", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "kill-process", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "false"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("secure", , 5)
        oLogger.WriteToLogRelative("false", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "secure", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "false"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("langid", , 5)
        oLogger.WriteToLogRelative("0", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "langid", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "0"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("name", , 5)
        oLogger.WriteToLogRelative("Windows Media Center", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "name", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "Windows Media Center"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("class", , 5)
        oLogger.WriteToLogRelative("*", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "class", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "*"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("action", , 5)
        oLogger.WriteToLogRelative("0", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "action", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "0"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("enum-subwindows", , 5)
        oLogger.WriteToLogRelative("0", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "enum-subwindows", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "0"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("close-type", , 5)
        oLogger.WriteToLogRelative("0", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "close-type", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "0"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("close-command-id", , 5)
        oLogger.WriteToLogRelative("1", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "close-command-id", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "1"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        mXmlNode_WindowManager.AppendChild(mXmlNode_Window)


        oLogger.WriteToLogRelative("adding window", , 4)
        mXmlNode_Window = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "window", ns.LookupNamespace("sk"))
        mXmlNode_Window.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("id"))
        mXmlNode_Window.Attributes.GetNamedItem("id").Value = "-1"

        oLogger.WriteToLogRelative("close-on-screensaver", , 5)
        oLogger.WriteToLogRelative("true", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "close-on-screensaver", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "true"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("kill-process", , 5)
        oLogger.WriteToLogRelative("false", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "kill-process", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "false"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("secure", , 5)
        oLogger.WriteToLogRelative("false", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "secure", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "false"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("langid", , 5)
        oLogger.WriteToLogRelative("0", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "langid", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "0"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("name", , 5)
        oLogger.WriteToLogRelative("Windows Media Centre", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "name", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "Windows Media Centre"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("class", , 5)
        oLogger.WriteToLogRelative("*", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "class", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "*"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("action", , 5)
        oLogger.WriteToLogRelative("0", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "action", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "0"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("enum-subwindows", , 5)
        oLogger.WriteToLogRelative("0", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "enum-subwindows", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "0"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("close-type", , 5)
        oLogger.WriteToLogRelative("0", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "close-type", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "0"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        oLogger.WriteToLogRelative("close-command-id", , 5)
        oLogger.WriteToLogRelative("1", , 6)
        mXmlNode_New = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "close-command-id", ns.LookupNamespace("sk"))
        mXmlNode_New.InnerText = "1"
        mXmlNode_Window.AppendChild(mXmlNode_New)

        mXmlNode_WindowManager.AppendChild(mXmlNode_Window)

        '--------------------------------------------------------------------------------------------------------
        oLogger.WriteToLogRelative("saving skcfg", , 1)
        oLogger.WriteToLogRelative(mSkCfgFile, , 2)
        mSkCfgXml.Save(mSkCfgFile)
        oLogger.WriteToLogRelative("ok", , 3)
        'Catch ex As Exception
        '    MsgBox(ex.Message)
        'End Try
    End Function
End Module
