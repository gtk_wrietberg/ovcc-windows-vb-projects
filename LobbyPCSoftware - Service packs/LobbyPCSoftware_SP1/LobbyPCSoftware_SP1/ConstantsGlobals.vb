Module ConstantsGlobals
    Public oLogger As Logger

    Public Const cSiteKioskRegistryHive As String = "C:\Users\sitekiosk\ntuser.dat"

    Public Const cSiteKioskRegistry_Key_InternetSettings As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings"
    Public Const cSiteKioskRegistry_Key_InternetSettings_Zone3 As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3"

    Public Const cSiteKioskRegistry_Name_DisplayMixedContent As String = "1609"
    Public Const cSiteKioskRegistry_Value_DisplayMixedContent As Long = 0

    Public Const cSiteKioskRegistry_Name_SecureProtocols As String = "SecureProtocols"
    Public Const cSiteKioskRegistry_Value_SecureProtocols As Long = 168 '0x000000a8


End Module
