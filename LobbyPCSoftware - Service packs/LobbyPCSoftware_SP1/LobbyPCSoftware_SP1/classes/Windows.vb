Public Class Windows
    Private mNames As ArrayList
    Private mCount As Integer

    Public Sub New()
        mNames = New ArrayList
        mCount = 0
    End Sub

    Public Function Add(ByVal sName As String) As Boolean
        If mNames.Contains(sName) Then
            Return False
        End If

        mNames.Add(sName)

        Return True
    End Function

    Public Sub Reset()
        mCount = 0
    End Sub

    Public Function GetNext() As String
        If mCount > mNames.Count Then
            Return String.Empty
        End If

        mCount += 1
        Return mNames.Item(mCount - 1).ToString
    End Function
End Class
