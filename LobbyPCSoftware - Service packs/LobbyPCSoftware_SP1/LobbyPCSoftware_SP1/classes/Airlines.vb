Public Class Airlines
    Private mAirlines As ArrayList
    Private mCount As Integer

    Public Sub New()
        mAirlines = New ArrayList
        mCount = 0
    End Sub

    Public Function Add(ByVal sName As String) As Boolean
        If mAirlines.Contains(sName) Then
            Return False
        End If

        mAirlines.Add(sName)

        Return True
    End Function

    Public Sub Reset()
        mCount = 0
    End Sub

    Public Function GetNext() As String
        If mCount > mAirlines.Count Then
            Return String.Empty
        End If

        mCount += 1
        Return mAirlines.Item(mCount - 1).ToString
    End Function
End Class
