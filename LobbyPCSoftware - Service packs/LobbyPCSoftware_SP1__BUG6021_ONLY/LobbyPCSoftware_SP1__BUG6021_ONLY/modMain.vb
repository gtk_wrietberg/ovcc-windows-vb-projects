Module modMain

    Private oComputerName As ComputerName

    Public Sub Main()
        Dim iExitCode As Integer

        iExitCode = 0

        oLogger = New Logger
        oLogger.LogFilePath = "C:\Program Files\iBAHN\"
        oLogger.LogFileName = "LPCSoftware_SP1__BUG6021_ONLY.log"

        oComputerName = New ComputerName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        oLogger.WriteToLog(New String("-", 10))

        '--------------------
        oLogger.WriteToLog("Bug 6021 - Sitekiosk Unauthorized Function Call Popup", , 0)
        If Not Bug6021_SitekioskUnauthorizedFunctionCallPopup() Then
            oLogger.WriteToLog("fail", , 1)
            iExitCode += 1
        Else
            oLogger.WriteToLog("ok", , 1)
        End If

        '--------------------
        ExitApplication(iExitCode)
    End Sub

    Public Sub ExitApplication(ByVal iExitCode As Integer)
        oLogger.WriteToLog("Exiting...", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        oLogger.WriteToLog("exit code: " & iExitCode & " (" & iExitCode.ToString & ")", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLog("bye", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLogWithoutDate(New String("=", 100))

        System.Environment.Exit(iExitCode)
    End Sub

    Private Function Bug6021_SitekioskUnauthorizedFunctionCallPopup() As Boolean
        Try

            Dim oHive As RegistryHive

            oLogger.WriteToLogRelative("updating registry for SiteKiosk user", , 1)


            oLogger.WriteToLogRelative("initialising", , 2)
            oHive = New RegistryHive

            If oHive.Initialise Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                Return False
            End If


            oLogger.WriteToLogRelative("loading SiteKiosk hive", , 2)
            oLogger.WriteToLogRelative(cSiteKioskRegistryHive, , 3)
            If Not IO.File.Exists(cSiteKioskRegistryHive) Then
                oLogger.WriteToLogRelative("does not exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                Return False
            End If

            If oHive.LoadHive(cSiteKioskRegistryHive) Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                oHive.UnloadHive()

                Return False
            End If


            oLogger.WriteToLogRelative("setting values", , 2)

            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_SecureProtocols, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_SecureProtocols.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_SecureProtocols, cSiteKioskRegistry_Value_SecureProtocols) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings_Zone3, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_DisplayMixedContent, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_DisplayMixedContent.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_DisplayMixedContent, cSiteKioskRegistry_Value_DisplayMixedContent) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If


            oLogger.WriteToLogRelative("unloading SiteKiosk hive", , 2)
            If oHive.UnloadHive() Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                Return False
            End If

            Return True

        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative("ex.Source", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function
End Module
