﻿Module modMain
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        '-----------------------------------

        For Each sArg In My.Application.CommandLineArgs
            sArg = sArg.ToLower

            If sArg.Equals(cParam__reboot) Then
                g_Reboot = True
            End If
        Next

        '-----------------------------------


        oLogger.WriteToLog("Disabling LLMNR", , 0)
        oLogger.WriteToLog("registry", , 1)
        oLogger.WriteToLog("LocalMachine", , 2)
        oLogger.WriteToLog(c_RegKey__Multicast, , 3)
        oLogger.WriteToLog(c_RegValue__Multicast, , 4)
        oLogger.WriteToLog(c_RegValue__Value.ToString, , 5)

        Try
            Dim rk As Microsoft.Win32.RegistryKey

            rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(c_RegKey__Multicast, True)
            If rk Is Nothing Then
                rk = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(c_RegKey__Multicast)
            End If

            rk.SetValue(c_RegValue__Multicast, c_RegValue__Value, Microsoft.Win32.RegistryValueKind.DWord)

            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        oLogger.WriteToLog("Done", , 0)
        If g_Reboot Then
            oLogger.WriteToLog("rebooting", , 1)
            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        End If
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
