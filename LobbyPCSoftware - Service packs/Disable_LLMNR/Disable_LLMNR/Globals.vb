Module Globals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public g_Reboot As Boolean

    Public g_TESTMODE As Boolean

    Public ReadOnly cParam__reboot As String = "--reboot"

    Public ReadOnly c_RegKey__Multicast As String = "SOFTWARE\\Policies\\Microsoft\\Windows NT\\DNSClient"
    Public ReadOnly c_RegValue__Multicast As String = "EnableMulticast"
    Public ReadOnly c_RegValue__Value As Integer = 0

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_Reboot = False


        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory(g_LogFileDirectory): " & ex.Message)
        End Try

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

    End Sub
End Module
