Module UpdateScriptFile
    Public Function PatchScriptFile__CommonJs() As Boolean
        Dim lines As New List(Of String)

        Dim sFileScript As String
        sFileScript = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFileScript &= "\skins\default\scripts\common.js"


        If Not IO.File.Exists(sFileScript) Then
            oLogger.WriteToLogRelative("script file not found", , 1)
            Return False
        Else
            oLogger.WriteToLogRelative(sFileScript, , 1)
        End If


        oLogger.WriteToLogRelative("creating backup", , 1)
        If oCopyFiles.BackupFile(New IO.FileInfo(sFileScript)) Then
            oLogger.WriteToLogRelative("ok", , 2)
        Else
            oLogger.WriteToLogRelative("OOPS", , 2)
            Return False
        End If


        oLogger.WriteToLogRelative("updating", , 1)
        Try
            Using sr As New IO.StreamReader(sFileScript)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("gk_LanguageInfo[9][""img""].src") Then
                        line = "gk_LanguageInfo[9][""img""].src	= "".\\img\\language\\popup\\en.gif"";"
                    End If

                    lines.Add(line)
                End While
            End Using

            Using sw As New IO.StreamWriter(sFileScript)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using
            oLogger.WriteToLogRelative("ok", , 1)
            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("error", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Function
End Module
