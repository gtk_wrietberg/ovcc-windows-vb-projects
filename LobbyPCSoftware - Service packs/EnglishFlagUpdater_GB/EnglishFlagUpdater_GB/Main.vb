Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        oLogger.WriteToLog("Patch script", , 0)
        PatchScriptFile__CommonJs()


        '------------------------------------------------------
        oLogger.WriteToLog("Killing SiteKiosk", , 0)
        KillSiteKiosk()

        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
