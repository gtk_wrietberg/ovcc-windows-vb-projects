﻿Imports Microsoft.Win32

Module Main
    Private ReadOnly REG_KEY__ROOT001 As String = "Software\Microsoft\MSOIdentityCRL"
    Private ReadOnly REG_KEY__SUB001 As String = "UserExtendedProperties"

    Private ReadOnly REG_KEY__ROOT002 As String = "Software\Microsoft\Office\%%OFFICE_VERSION%%.0\Common\Identity"
    Private ReadOnly REG_KEY__SUB002 As String = "Identities"

    Private ReadOnly REG_KEY__ROOT003 As String = "Software\Microsoft\Office\%%OFFICE_VERSION%%.0\Common\ServicesManagerCache"
    Private ReadOnly REG_KEY__SUB003 As String = "Identities"

    ' [HKEY_LOCAL_MACHINE\SK_USER_HIVE\Software\Microsoft\MSOIdentityCRL\UserExtendedProperties\microsoftonline.com::liam.mcglynn@itb.ie]
    ' [HKEY_LOCAL_MACHINE\SK_USER_HIVE\Software\Microsoft\Office\15.0\Common\Identity\Identities\675b3290714db151_OrgId]
    ' [HKEY_LOCAL_MACHINE\SK_USER_HIVE\Software\Microsoft\Office\15.0\Common\ServicesManagerCache\Identities\675b3290714db151_OrgId]


    Public Sub Main()
        Helpers.Logger.InitialiseLogger(Environment.CurrentDirectory & "\_logs", False)
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)


        'Kill al previous instances, if any
        Helpers.Logger.WriteMessage("Killing previous instances", 0)
        Dim prevInstances As Integer = 0
        prevInstances = Helpers.Processes.KillPreviousInstances()
        If prevInstances < 0 Then
            Helpers.Logger.WriteError("failed", 1)
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
        Else
            If prevInstances > 0 Then
                Helpers.Logger.WriteMessage(prevInstances.ToString & " killed", 1)
            Else
                Helpers.Logger.WriteMessage("none found", 1)
            End If
        End If


        '-----------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Cleaning registry", 0)

        Helpers.Logger.WriteMessage(REG_KEY__ROOT001, 1)
        Helpers.Logger.WriteMessage(REG_KEY__SUB001, 2)

        If Helpers.Registry.DeleteSubKey(REG_KEY__ROOT001, REG_KEY__SUB001) Then
            Helpers.Logger.WriteMessage("ok", 3)
        Else
            Helpers.Logger.WriteError("return message: " & Helpers.Errors.GetLast(False), 3)
        End If


        '-----------------------------------------------------------------------------------------------
        'Sledgehammer time. 
        Dim sTmp As String = ""
        For iOfficeVersion As Integer = 11 To 20
            sTmp = REG_KEY__ROOT002.Replace("%%OFFICE_VERSION%%", iOfficeVersion.ToString)

            Helpers.Logger.WriteMessage(sTmp, 1)
            If Helpers.Registry.KeyExists_CU(sTmp) Then
                Helpers.Logger.WriteMessage(REG_KEY__SUB002, 2)
                If Helpers.Registry.KeyExists_CU(sTmp & "\" & REG_KEY__SUB002) Then
                    If Helpers.Registry.DeleteSubKey(sTmp, REG_KEY__SUB002) Then
                        Helpers.Logger.WriteMessage("ok", 3)
                    Else
                        Helpers.Logger.WriteError("return message: " & Helpers.Errors.GetLast(False), 3)
                    End If
                Else
                    Helpers.Logger.WriteWarning("not found", 3)
                    Helpers.Logger.WriteWarning("last error", 4)
                    Helpers.Logger.WriteWarning(Helpers.Errors.GetLast(False), 5)
                End If
            Else
                Helpers.Logger.WriteWarning("not found", 2)
                Helpers.Logger.WriteWarning("last error", 3)
                Helpers.Logger.WriteWarning(Helpers.Errors.GetLast(False), 4)
            End If


            '---------------------------------------
            sTmp = REG_KEY__ROOT003.Replace("%%OFFICE_VERSION%%", iOfficeVersion.ToString)

            Helpers.Logger.WriteMessage(sTmp, 1)
            If Helpers.Registry.KeyExists_CU(sTmp) Then
                Helpers.Logger.WriteMessage(REG_KEY__SUB003, 2)
                If Helpers.Registry.KeyExists_CU(sTmp & "\" & REG_KEY__SUB003) Then
                    If Helpers.Registry.DeleteSubKey(sTmp, REG_KEY__SUB003) Then
                        Helpers.Logger.WriteMessage("ok", 3)
                    Else
                        Helpers.Logger.WriteError("return message: " & Helpers.Errors.GetLast(False), 3)
                    End If
                Else
                    Helpers.Logger.WriteWarning("not found", 3)
                    Helpers.Logger.WriteWarning("last error", 4)
                    Helpers.Logger.WriteWarning(Helpers.Errors.GetLast(False), 5)
                End If
            Else
                Helpers.Logger.WriteWarning("not found", 2)
                Helpers.Logger.WriteWarning("last error", 3)
                Helpers.Logger.WriteWarning(Helpers.Errors.GetLast(False), 4)
            End If
        Next


        '-----------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Done", 0)
        Helpers.Logger.WriteMessage("ok bye", 1)
    End Sub
End Module
