Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If

        SkCfg_Update()

        Bye()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_DownloadManager As Xml.XmlNode
            Dim mXmlNode_DeleteDirectories As Xml.XmlNode
            Dim mXmlNode_Dirs As Xml.XmlNodeList
            Dim mXmlNode_Dir As Xml.XmlNode
            Dim mXmlNode_NewDir As Xml.XmlNode
            Dim bDirFound As Boolean = False


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("download-manager node", , 1)
            mXmlNode_DownloadManager = mXmlNode_Root.SelectSingleNode("sk:download-manager", ns)
            If Not mXmlNode_DownloadManager Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_DownloadManager Is Nothing")
            End If

            oLogger.WriteToLog("delete-directories node", , 1)
            mXmlNode_DeleteDirectories = mXmlNode_DownloadManager.SelectSingleNode("sk:delete-directories", ns)
            If Not mXmlNode_DeleteDirectories Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_DeleteDirectories Is Nothing")
            End If

            oLogger.WriteToLog("dir nodelist", , 1)
            mXmlNode_Dirs = mXmlNode_DeleteDirectories.SelectNodes("sk:dir", ns)
            If Not mXmlNode_Dirs Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_Dirs Is Nothing")
            End If

            oLogger.WriteToLog("checking for fix", , 1)
            For Each mXmlNode_Dir In mXmlNode_Dirs
                If mXmlNode_Dir.InnerText = cDir_History_XP Then
                    bDirFound = True
                End If
            Next

            If Not bDirFound Then
                oLogger.WriteToLog("not found, adding", , 2)
                mXmlNode_NewDir = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "dir", ns.LookupNamespace("sk"))
                mXmlNode_NewDir.InnerText = cDir_History_XP

                mXmlNode_DeleteDirectories.AppendChild(mXmlNode_NewDir)
                oLogger.WriteToLog("ok", , 3)

                mConfigChanged = True
            Else
                oLogger.WriteToLog("already there, no update needed", , 2)
            End If


            oLogger.WriteToLog("saving", , 0)
            If mConfigChanged Then
                oLogger.WriteToLog("creating backup of '" & mSkCfgFile & "'", , 1)
                BackupFile(New IO.FileInfo(mSkCfgFile))

                oLogger.WriteToLog("saving existing file", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("skipped, nothing was changed", , 1)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLogRelative("writing: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLogRelative("done", , 2)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

