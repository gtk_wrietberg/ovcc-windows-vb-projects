Public Class SkCfg
    Private mSkCfgFile As String
    Private mSkBuild As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String
    Private mConfigChanged As Boolean

    Private ReadOnly c_REGROOT_SITEKIOSK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public ReadOnly Property SiteKioskFolder As String
        Get
            Return mSiteKioskFolder
        End Get
    End Property


    Public Function Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_Global As Xml.XmlNode
            Dim mXmlNode_SkUserAgent As Xml.XmlNode
            Dim mXmlNode_UserAgent As Xml.XmlNode

            Dim sTmp As String = ""


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("find global node", , 1)
            mXmlNode_Global = mXmlNode_Root.SelectSingleNode("sk:global", ns)


            oLogger.WriteToLog("find skuseragent node", , 1)
            mXmlNode_SkUserAgent = mXmlNode_Global.SelectSingleNode("sk:skuseragent", ns)

            sTmp = mXmlNode_SkUserAgent.Attributes.GetNamedItem("enabled").Value.ToLower
            If sTmp.Equals("false") Then
                'Nothing to change here
                oLogger.WriteToLog("already set to false", , 2)
            Else
                oLogger.WriteToLog("setting to false", , 2)
                mXmlNode_SkUserAgent.Attributes.GetNamedItem("enabled").Value = "false"

                mConfigChanged = True
            End If


            oLogger.WriteToLog("find useragent node", , 1)
            mXmlNode_UserAgent = mXmlNode_Global.SelectSingleNode("sk:useragent", ns)

            sTmp = mXmlNode_UserAgent.Attributes.GetNamedItem("enabled").Value.ToLower
            If sTmp.Equals("false") Then
                'Nothing to change here
                oLogger.WriteToLog("already set to false", , 2)
            Else
                oLogger.WriteToLog("setting to false", , 2)
                mXmlNode_UserAgent.Attributes.GetNamedItem("enabled").Value = "false"

                mConfigChanged = True
            End If


            If mConfigChanged Then
                oLogger.WriteToLog("saving", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("not saving, nothing was changed", , 1)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try

        Return True
    End Function


    Public Sub New()
        mSiteKioskFolder = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "InstallDir", "")
        mSkCfgFile = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "LastCfg", "")
        mSkBuild = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "Build", "")
        mConfigChanged = False
    End Sub
End Class
