﻿Module modMain
    Public Sub Main()
        If Helpers.Generic.IsDevMachine Then
            g_TESTMODE = True
        End If

        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))
        oLogger.WriteToLog(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        oLogger.WriteToLog("init", , 0)
        'Dim sTmp As String = ""

        'For Each arg As String In Environment.GetCommandLineArgs()
        '    If arg.Equals(c_PARAM_ON0) Or arg.Equals(c_PARAM_ON1) Or arg.Equals(c_PARAM_ON2) Then
        '        g_ContentFilterState = True
        '    End If

        '    If arg.Equals(c_PARAM_OFF0) Or arg.Equals(c_PARAM_OFF1) Or arg.Equals(c_PARAM_OFF2) Then
        '        g_ContentFilterState = False
        '    End If
        'Next


        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        oLogger.WriteToLog("updating SiteKiosk config", , 0)

        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory
        If Not oSkCfg.Update() Then
            ExitCode.SetValue(ExitCode.ExitCodes.ERROR_OCCURRED)
        End If


        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        ExitApplication()
    End Sub


    Private Sub ExitApplication()
        '-----------------------------------------------
        'done
        oLogger.WriteToLog("exiting", , 0)
        oLogger.WriteToLog("exit code", , 1)
        oLogger.WriteToLog(ExitCode.ToString(), , 2)
        oLogger.WriteToLog("bye", , 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

End Module
