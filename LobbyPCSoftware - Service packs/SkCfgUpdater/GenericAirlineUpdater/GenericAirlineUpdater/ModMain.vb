Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If

        LoadPatches()

        SkCfg_Update()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Sub LoadPatches()
        oLogger.WriteToLog("loading patches", , 0)

        gArr_Urls = New List(Of String)
        gArr_Patterns = New List(Of String)
        Try
            Using sr As New IO.StreamReader("configuration.js.patch.ap")
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    gArr_Urls.Add(line)
                End While
            End Using
        Catch ex As Exception
            oLogger.WriteToLog("ERROR WHILE LOADING configuration.js.patch.ap", , 1)
        End Try

        Try
            Using sr As New IO.StreamReader("sitekiosk.config.patch.ap")
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    gArr_Patterns.Add(line)
                End While
            End Using
        Catch ex As Exception
            oLogger.WriteToLog("ERROR WHILE LOADING sitekiosk.config.patch.ap", , 1)
        End Try

    End Sub

    Private Function UpdateAirlinePCConfig() As Boolean
        Dim lines As New List(Of String)
        Dim bUpdated As Boolean = False

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\html\AirlineKiosk\js\configuration.js"

        oLogger.WriteToLog("updating AirlinePC config", , 0)

        oLogger.WriteToLog("looking for '" & sFile & "'", , 1)

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLog("not found!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        End If

        BackupFile(New IO.FileInfo(sFile))

        oLogger.WriteToLog("ok", , 2)

        Dim reAirlines As Regex = New Regex("(aAirlines\['link'\].+="")(.+)("";)")

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String, newline As String, url_replacement() As String
                Dim tmpLine As String, tmpUrl As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    tmpLine = Regex.Replace(line, cRegExp_Airlines, "$1%%%$3")
                    tmpUrl = Regex.Replace(line, cRegExp_Airlines, "$2")

                    For Each url As String In gArr_Urls
                        url_replacement = Split(url, "|", 2)

                        If tmpUrl = url_replacement(0) Then
                            newline = tmpLine.Replace("%%%", url_replacement(1))

                            oLogger.WriteToLog("replacing '" & line & "' with '" & newline & "'", , 1)
                            line = newline

                            bUpdated = True
                        End If
                    Next

                    lines.Add(line)
                End While
            End Using

            If bUpdated Then
                oLogger.WriteToLog("writing '" & sFile & "'", , 1)

                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using

                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("leaving '" & sFile & "' alone", , 1)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function UpdateiBAHNJSConfig() As Boolean
        Dim lines As New List(Of String)
        Dim bUpdated As Boolean = False

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\default\Scripts\ibahn.js"

        oLogger.WriteToLog("updating iBAHN.js", , 0)

        oLogger.WriteToLog("looking for '" & sFile & "'", , 1)

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLog("not found!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        End If

        BackupFile(New IO.FileInfo(sFile))

        oLogger.WriteToLog("ok", , 2)

        Dim reAirlines As Regex = New Regex("(FAAwebsites\['link'\].+="")(.+)("";)")

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String, newline As String, url_replacement() As String
                Dim tmpLine As String, tmpUrl As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    tmpLine = Regex.Replace(line, cRegExp_Airlines, "$1%%%$3")
                    tmpUrl = Regex.Replace(line, cRegExp_Airlines, "$2")

                    For Each url As String In gArr_Urls
                        url_replacement = Split(url, "|", 2)

                        If tmpUrl = url_replacement(0) Then
                            newline = tmpLine.Replace("%%%", url_replacement(1))

                            oLogger.WriteToLog("replacing '" & line & "' with '" & newline & "'", , 1)
                            line = newline

                            bUpdated = True
                        End If
                    Next

                    lines.Add(line)
                End While
            End Using

            If bUpdated Then
                oLogger.WriteToLog("writing '" & sFile & "'", , 1)

                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using

                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("leaving '" & sFile & "' alone", , 1)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_StartPageConfig As Xml.XmlNode
            Dim mXmlNode_StartPage As Xml.XmlNode

            Dim mXmlNode_SurfArea As Xml.XmlNode
            Dim mXmlNode_SurfAreaMode As Xml.XmlNode
            Dim mXmlNode_SurfAreaUrls As Xml.XmlNodeList
            Dim mXmlNode_SurfAreaUrl As Xml.XmlNode

            Dim mXmlNode_Plugins As Xml.XmlNodeList
            Dim mXmlNode_Plugin As Xml.XmlNode
            Dim mXmlNode_SiteCash As Xml.XmlNode, bSiteCashFound As Boolean = False
            Dim mXmlNode_UrlZones As Xml.XmlNodeList
            Dim mXmlNode_UrlZone As Xml.XmlNode
            Dim mXmlNode_AirlinesZone As Xml.XmlNode, bAirlinesZoneFound As Boolean = False
            Dim mXmlNode_Urls As Xml.XmlNodeList
            Dim mXmlNode_Url As Xml.XmlNode

            Dim bAirlinePC As Boolean
            Dim pattern_replacement() As String

            bAirlinePC = False

            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            'Checking start page
            oLogger.WriteToLog("checking LobbyPC type", , 1)
            mXmlNode_StartPageConfig = mXmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)
            mXmlNode_StartPage = mXmlNode_StartPageConfig.SelectSingleNode("sk:startpage", ns)
            If mXmlNode_StartPage.InnerText.Contains("Airline") Then
                oLogger.WriteToLog("AirlinePC", , 2)

                bAirlinePC = True
            Else
                oLogger.WriteToLog("normal", , 2)
            End If

            If bAirlinePC Then
                UpdateAirlinePCConfig()

                oLogger.WriteToLog("update SurfArea", , 1)

                mXmlNode_SurfArea = mXmlNode_Root.SelectSingleNode("sk:surfarea", ns)
                mXmlNode_SurfAreaMode = mXmlNode_SurfArea.SelectSingleNode("sk:mode", ns)
                mXmlNode_SurfAreaMode.InnerText = "1"

                mConfigChanged = True

                oLogger.WriteToLog("Deny access to other websites", , 2)

                mXmlNode_SurfAreaUrls = mXmlNode_SurfArea.SelectNodes("sk:url", ns)
                For Each mXmlNode_SurfAreaUrl In mXmlNode_SurfAreaUrls
                    If mXmlNode_SurfAreaUrl.InnerText.Contains("http") Then
                        For Each pattern As String In gArr_Patterns
                            pattern_replacement = Split(pattern, "|", 2)

                            If mXmlNode_SurfAreaUrl.InnerText.Equals(pattern_replacement(0)) Then
                                oLogger.WriteToLog("updating '" & pattern_replacement(0) & "' to '" & pattern_replacement(1) & "'", , 2)
                                mXmlNode_SurfAreaUrl.InnerText = pattern_replacement(1)

                                mConfigChanged = True
                            End If
                        Next
                    End If
                Next
            Else
                UpdateiBAHNJSConfig()

                oLogger.WriteToLog("update SurfArea", , 1)

                mXmlNode_SurfArea = mXmlNode_Root.SelectSingleNode("sk:surfarea", ns)
                mXmlNode_SurfAreaMode = mXmlNode_SurfArea.SelectSingleNode("sk:mode", ns)

                If mXmlNode_SurfAreaMode.InnerText = "1" Then
                    mXmlNode_SurfAreaMode.InnerText = "0"
                    oLogger.WriteToLog("Allow access to other websites", , 2)
                    mConfigChanged = True
                Else
                    oLogger.WriteToLog("not needed", , 2)
                End If

                mXmlNode_SurfAreaUrls = mXmlNode_SurfArea.SelectNodes("sk:url", ns)
                For Each mXmlNode_SurfAreaUrl In mXmlNode_SurfAreaUrls
                    If mXmlNode_SurfAreaUrl.InnerText.Contains("http") Then
                        If mXmlNode_SurfAreaUrl.InnerText = "http://*.*" Then
                            oLogger.WriteToLog("removing sledgehammer #1 (http://*.*)", , 2)
                            mXmlNode_SurfArea.RemoveChild(mXmlNode_SurfAreaUrl)
                            mConfigChanged = True
                        End If

                        If mXmlNode_SurfAreaUrl.InnerText = "https://*.*" Then
                            oLogger.WriteToLog("removing sledgehammer #2 (https://*.*)", , 2)
                            mXmlNode_SurfArea.RemoveChild(mXmlNode_SurfAreaUrl)
                            mConfigChanged = True
                        End If
                    End If
                Next


                oLogger.WriteToLog("update UrlZone", , 1)

                mXmlNode_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", ns)
                For Each mXmlNode_Plugin In mXmlNode_Plugins
                    If mXmlNode_Plugin.Attributes.GetNamedItem("name").Value = "SiteCash" Then
                        mXmlNode_SiteCash = mXmlNode_Plugin
                        bSiteCashFound = True

                        Exit For
                    End If
                Next

                If Not bSiteCashFound Then
                    Throw New Exception("SiteCash not found!")
                End If


                mXmlNode_UrlZones = mXmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
                For Each mXmlNode_UrlZone In mXmlNode_UrlZones
                    If mXmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = "FreeAirlines" Then
                        mXmlNode_AirlinesZone = mXmlNode_UrlZone
                        bAirlinesZoneFound = True
                    End If
                Next

                If Not bAirlinesZoneFound Then
                    Throw New Exception("Airlines url-zone not found!")
                End If


                mXmlNode_Urls = mXmlNode_AirlinesZone.SelectNodes("sk:url", ns)
                For Each mXmlNode_Url In mXmlNode_Urls
                    For Each pattern As String In gArr_Patterns
                        pattern_replacement = Split(pattern, "|", 2)

                        If mXmlNode_Url.InnerText.Equals(pattern_replacement(0)) Then
                            oLogger.WriteToLog("updating '" & pattern_replacement(0) & "' to '" & pattern_replacement(1) & "'", , 2)
                            mXmlNode_Url.InnerText = pattern_replacement(1)

                            mConfigChanged = True
                        End If
                    Next
                Next
            End If

            If mConfigChanged Then
                BackupFile(New IO.FileInfo(mSkCfgFile))

                oLogger.WriteToLog("saving", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("not saving, nothing was changed", , 1)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        oLogger.WriteToLog("creating backup of '" & oFile.FullName & "'", , 0)

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLog("backup file: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLog("done", , 1)
                bRet = True
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

