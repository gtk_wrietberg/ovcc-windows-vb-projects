﻿Module modMain
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------
        oLogger.WriteToLog("Updating SiteKiosk config", , 0)
        Dim oSkCfg As New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Update SiteKiosk config", , 0)
        oSkCfg.Update(g_OverwriteExistingConfig)



        '---------------------------------------
        oLogger.WriteToLog("done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub


    Private Sub Bye(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("Bye (" & iExitCode.ToString & ")", , 0)
        oLogger.WriteToLog(New String("=", 50))

        Environment.ExitCode = iExitCode
        Application.Exit()
    End Sub
End Module
