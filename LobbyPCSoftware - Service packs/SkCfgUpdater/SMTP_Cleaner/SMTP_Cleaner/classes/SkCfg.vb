Public Class SkCfg
    Private mOldSkCfgFile As String
    Private mNewSkCfgFile As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Sub Update(newPassword As String, Optional ByVal bOverWriteOldFile As Boolean = False)
        Try
            oLogger.WriteToLogRelative("finding SiteKiosk install directory", , 1)
            mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            oLogger.WriteToLogRelative("found: " & mSiteKioskFolder, , 2)

            oLogger.WriteToLogRelative("finding active skcfg file", , 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            oLogger.WriteToLogRelative("found: " & mOldSkCfgFile, , 2)



            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            If bOverWriteOldFile Then
                oLogger.WriteToLogRelative("backing up current config", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
                oFile = New IO.FileInfo(mOldSkCfgFile)
                oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".pw_changer.backup", True)

                oLogger.WriteToLogRelative("backup: " & mOldSkCfgFile & "." & sDateString & ".pw_changer.backup", , 2)

                mNewSkCfgFile = mOldSkCfgFile
            Else
                oLogger.WriteToLogRelative("making copy", , 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)
                mNewSkCfgFile = oFile.DirectoryName & "\iBAHN_pw_changer__" & sDateString & ".skcfg"
                oFile.CopyTo(mNewSkCfgFile, True)

                oLogger.WriteToLogRelative("copy: " & mNewSkCfgFile, , 2)
            End If


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If


            '*************************************************************************************************************
            'Password 
            oLogger.WriteToLogRelative("Password update", , 2)
            Dim xmlNode_Password1 As Xml.XmlNode
            Dim xmlNode_Password2 As Xml.XmlNode
            'Dim sPwFirstTwoChars As String

            oLogger.WriteToLogRelative("loading Password node", , 3)
            xmlNode_Password1 = xmlNode_Root.SelectSingleNode("sk:password", ns)

            oLogger.WriteToLogRelative("loading Password subnode", , 4)
            xmlNode_Password2 = xmlNode_Password1.SelectSingleNode("sk:password", ns)

            oLogger.WriteToLogRelative("setting new password", , 5)
            xmlNode_Password2.InnerText = newPassword


            'Dim xmlNode_Sub1 As Xml.XmlNode
            'Dim xmlNode_Sub2 As Xml.XmlNode
            'Dim xmlNode_Sub3 As Xml.XmlNode
            'Dim xmlNode_Sub4 As Xml.XmlNode
            'Dim xmlNode_Sub5 As Xml.XmlNode
            'Dim xmlNode_Sub6 As Xml.XmlNode
            'Dim xmlNodeList_1 As Xml.XmlNodeList
            'sPwFirstTwoChars = newPassword.Substring(0, 2)

            'oLogger.WriteToLogRelative("updating empty password thingies", , 3)
            'xmlNode_Sub1 = xmlNode_Password1.SelectSingleNode("sk:secondary-password", ns)
            'xmlNode_Sub2 = xmlNode_Sub1.SelectSingleNode("sk:password", ns)

            'xmlNode_Sub2.InnerText = sPwFirstTwoChars


            'xmlNode_Sub1 = xmlNode_Root.SelectSingleNode("sk:programs", ns)
            'xmlNodeList_1 = xmlNode_Sub1.SelectNodes("sk:file", ns)
            'For Each xmlNode_Sub2 In xmlNodeList_1
            '    xmlNode_Sub3 = xmlNode_Sub2.SelectSingleNode("sk:runas", ns)
            '    xmlNode_Sub4 = xmlNode_Sub3.SelectSingleNode("sk:password", ns)
            '    xmlNode_Sub4.InnerText = sPwFirstTwoChars

            '    xmlNode_Sub5 = xmlNode_Sub2.SelectSingleNode("sk:startup", ns)
            '    xmlNode_Sub6 = xmlNode_Sub5.SelectSingleNode("sk:password", ns)
            '    xmlNode_Sub6.InnerText = sPwFirstTwoChars
            'Next

            'xmlNode_Sub2 = xmlNode_Sub1.SelectSingleNode("sk:password", ns)

            ''<programs enabled="true" autostart-delay="10000">
            ''	<file autostart="false" filename="C:\Windows\explorer.exe" description="File Explorer">
            ''		<runas enabled="false">
            ''			<username></username>
            ''			<password>U1</password>
            ''			<domainname></domainname>
            ''		</runas>
            ''		<startup enabled="false">
            ''			<password>U1</password>
            ''		</startup>
            ''	</file>




            '*************************************************************************************************************
            'Saving
            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Save(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)

            If Not bOverWriteOldFile Then
                oLogger.WriteToLogRelative("activating", , 2)
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

                Dim sTempRegVal As String
                sTempRegVal = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

                If sTempRegVal = mNewSkCfgFile Then
                    oLogger.WriteToLogRelative("ok", , 3)
                Else
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_WARNING, 3)

                    oLogger.WriteToLogRelative("trying to fix this by overwriting the existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 4)

                    oLogger.WriteToLogRelative("backing up existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mOldSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", True)

                    oLogger.WriteToLogRelative("overwriting existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mNewSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile, True)
                End If
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub
End Class
