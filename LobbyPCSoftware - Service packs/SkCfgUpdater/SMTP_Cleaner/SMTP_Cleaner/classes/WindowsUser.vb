﻿Public Class WindowsUser
    Private mUsername As String
    Private mLastErrorCode As Integer
    Private mLastErrorDescription As String

    Public Function ChangePassword(NewPassword As String) As Integer
        Dim ret As Boolean = False

        Try
            Dim UserObject As Object

            UserObject = GetObject("WinNT://" & System.Environment.MachineName & "/" & mUsername & "")

            Call UserObject.SetPassword(NewPassword)

            mLastErrorCode = Err.Number
            mLastErrorDescription = Err.Description

            ret = True
        Catch ex As Exception
            mLastErrorCode = 100
            mLastErrorDescription = ex.Message

            ret = False
        End Try

        Return ret
    End Function

    Public Sub New()
        mUsername = ""
    End Sub

    Public Sub New(Username As String)
        mUsername = Username
    End Sub

    Public Property Username() As String
        Get
            Return mUsername
        End Get
        Set(ByVal value As String)
            mUsername = value
        End Set
    End Property

    Public ReadOnly Property LastErrorCode() As Integer
        Get
            Dim tmp As Integer = mLastErrorCode
            mLastErrorCode = -1
            Return tmp
        End Get
    End Property

    Public ReadOnly Property LastErrorDescription() As String
        Get
            Dim tmp As String = mLastErrorDescription
            mLastErrorDescription = ""
            Return tmp
        End Get
    End Property


End Class
