﻿Public Class cc_regions
    Private Shared mUserName As String = ""
    Private Shared mPassWord As String = ""
    Private Shared mSize As String = ""

    Private Shared mRegion As String
    Public Shared Property Region() As String
        Get
            Return mRegion
        End Get
        Set(ByVal value As String)
            mRegion = value
        End Set
    End Property

    Public Shared Function GetUsername() As String
        Return GetUsername(mRegion)
    End Function

    Public Shared Function GetUsername(Region As String) As String
        _LoadStuff(Region)

        Return mUserName
    End Function

    Public Shared Function GetPassword() As String
        Return GetPassword(mRegion)
    End Function

    Public Shared Function GetPassword(Region As String) As String
        _LoadStuff(Region)

        Return mPassWord
    End Function

    Public Shared Function GetSize() As String
        Return GetSize(mRegion)
    End Function

    Public Shared Function GetSize(Region As String) As String
        _LoadStuff(Region)

        Return mSize
    End Function


    Private Shared Sub _LoadStuff(Region As String)
        Select Case Region
            Case "GBP"
                mUserName = "stsngbp"
                mPassWord = "XY25AhAnltBvovlEHxIYNw=="
                mSize = "16"
            Case "EUR"
                mUserName = "stsneur"
                mPassWord = "/x+5dAN7YPhvovlEHxIYNw=="
                mSize = "16"
            Case "US"
                mUserName = "stsn"
                mPassWord = "YIUFAXQoC8w="
                mSize = "8"
            Case "FRA"
                mUserName = "STSNFRA"
                mPassWord = "xrljBnP2ZvxvovlEHxIYNw=="
                mSize = "16"
            Case "SPA"
                mUserName = "STSNSPA"
                mPassWord = "Oev/6z8000ZvovlEHxIYNw=="
                mSize = "16"
            Case "NET"
                mUserName = "STSNNET"
                mPassWord = "4MvYySP9YsFvovlEHxIYNw=="
                mSize = "16"
            Case "CAD"
                mUserName = "STSNCAD"
                mPassWord = "v78bE7gL1eBvovlEHxIYNw=="
                mSize = "16"
            Case Else
                mUserName = ""
                mPassWord = ""
                mSize = ""
        End Select
    End Sub
End Class
