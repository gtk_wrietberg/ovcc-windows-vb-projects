Module modConstants
    Public ReadOnly cParams__CC_Currency As String = "--currency:"
    Public ReadOnly cParams__CC_HourlyRate As String = "--hourly-rate:"
    Public ReadOnly cParams__CC_Region As String = "--region-cc:"
    Public ReadOnly cParams__CC_Region_FromRegistry As String = "--region-cc-from-registry"
    Public ReadOnly cParams__CC_Max As String = "--max-amount-cc:"
    Public ReadOnly cParams__CC_Min As String = "--min-amount-cc:"
    Public ReadOnly cParams__CC_Increment As String = "--increment-cc:"
    Public ReadOnly cParams__CC_IncrementAmount As String = "--increment-amount-cc:"
    Public ReadOnly cParams__CC_Enable As String = "--enable-cc"
    Public ReadOnly cParams__CC_Disable As String = "--disable-cc"

    Public ReadOnly cParams__PP_Enable As String = "--enable-pp"
    Public ReadOnly cParams__PP_Disable As String = "--disable-pp"
End Module
