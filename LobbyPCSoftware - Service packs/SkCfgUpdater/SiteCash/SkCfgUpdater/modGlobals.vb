Module modGlobals
    Public gArr_Urls As New List(Of String)

    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String
    Public g_ProgramsFiles As String

    Public Sub InitGlobals()
        Dim dDate As Date = Now()


        g_ProgramsFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
        If g_ProgramsFiles.Equals(String.Empty) Then
            g_ProgramsFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        g_LogFileDirectory = g_ProgramsFiles & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = g_ProgramsFiles & "\GuestTek\_backups"
        'g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & Application.ProductName
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
