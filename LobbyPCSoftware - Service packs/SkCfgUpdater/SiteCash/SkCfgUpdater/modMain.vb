Module modMain
    Public oLogger As Logger

    Private mSkCfgFile As String

    Private mValue__CC_Currency As String
    Private mValue__CC_HourlyRate As String
    Private mValue__CC_Region As String
    Private mValue__CC_Max As String
    Private mValue__CC_Min As String
    Private mValue__CC_Increment As String
    Private mValue__CC_Enable As Boolean
    Private mValue__CC_Disable As Boolean
    Private mValue__PP_Enable As Boolean
    Private mValue__PP_Disable As Boolean

    Private mRegionInfo As System.Globalization.RegionInfo

    Private mConfigChanged As Boolean
    Private mShowHelp As Boolean

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        oLogger.WriteToLog("Finding SkCfg file (try #1)", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If


        mValue__CC_Region = String.Empty
        mValue__CC_Currency = String.Empty
        mValue__CC_HourlyRate = String.Empty
        mValue__CC_Max = String.Empty
        mValue__CC_Min = String.Empty
        mValue__CC_Disable = False

        mConfigChanged = False
        mShowHelp = False

        oLogger.WriteToLog("Loading command line arguments", , 0)
        If Environment.GetCommandLineArgs.Length <= 1 Then
            oLogger.WriteToLog("none found, showing help dialog", , 1)

            mShowHelp = True
        End If

        Dim arg As String, arg_index As Integer
        For arg_index = 1 To Environment.GetCommandLineArgs.Length - 1
            arg = Environment.GetCommandLineArgs(arg_index)

            oLogger.WriteToLog("found: " & arg, , 1)

            If arg = "--help" Or arg = "-help" Or arg = "/help" Or arg = "/h" Or arg = "/?" Then
                mShowHelp = True
            End If
            If InStr(arg, cParams__CC_Currency) > 0 Then
                mValue__CC_Currency = arg.Replace(cParams__CC_Currency, "")

                GetRegionInfo(mValue__CC_Currency)
                If mRegionInfo Is Nothing Then
                    oLogger.WriteToLog("invalid currency!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    mValue__CC_Currency = String.Empty
                Else
                    oLogger.WriteToLog("currency: " & mValue__CC_Currency, , 2)
                End If
            End If
            If InStr(arg, cParams__CC_HourlyRate) > 0 Then
                mValue__CC_HourlyRate = arg.Replace(cParams__CC_HourlyRate, "")

                oLogger.WriteToLog("hourly rate: " & mValue__CC_HourlyRate, , 2)
            End If
            If InStr(arg, cParams__CC_Region) > 0 Then
                mValue__CC_Region = arg.Replace(cParams__CC_Region, "")

                oLogger.WriteToLog("region: " & mValue__CC_Region, , 2)
            End If
            If InStr(arg, cParams__CC_Max) > 0 Then
                mValue__CC_Max = arg.Replace(cParams__CC_Max, "")

                oLogger.WriteToLog("max: " & mValue__CC_Max, , 2)
            End If
            If InStr(arg, cParams__CC_Min) > 0 Then
                mValue__CC_Min = arg.Replace(cParams__CC_Min, "")

                oLogger.WriteToLog("min: " & mValue__CC_Min, , 2)
            End If
            If InStr(arg, cParams__CC_Increment) > 0 Or InStr(arg, cParams__CC_IncrementAmount) > 0 Then
                If InStr(arg, cParams__CC_Increment) > 0 Then
                    mValue__CC_Increment = arg.Replace(cParams__CC_Increment, "")
                Else
                    mValue__CC_Increment = arg.Replace(cParams__CC_IncrementAmount, "")
                End If

                oLogger.WriteToLog("increment: " & mValue__CC_Increment, , 2)
            End If
            If InStr(arg, cParams__CC_Disable) > 0 Then
                mValue__CC_Disable = True

                oLogger.WriteToLog("disable cc", , 2)
            End If
            If InStr(arg, cParams__CC_Enable) > 0 Then
                mValue__CC_Enable = True

                oLogger.WriteToLog("enable cc", , 2)
            End If
            If InStr(arg, cParams__PP_Disable) > 0 Then
                mValue__pp_Disable = True

                oLogger.WriteToLog("disable prepaid", , 2)
            End If
            If InStr(arg, cParams__PP_Enable) > 0 Then
                mValue__PP_Enable = True

                oLogger.WriteToLog("enable prepaid", , 2)
            End If
        Next

        If mValue__CC_Disable And mValue__CC_Enable Then
            mValue__CC_Disable = False
        End If

        If mValue__PP_Disable And mValue__PP_Enable Then
            mValue__PP_Disable = False
        End If

        If mShowHelp Then
            Dim sHelp As String = ""

            sHelp &= "Command line switches:" & vbCrLf & vbCrLf
            sHelp &= vbCrLf
            sHelp &= cParams__CC_Region & "xxx" & vbCrLf
            sHelp &= vbTab & "Set CC region" & vbCrLf
            sHelp &= cParams__CC_Currency & "xxx" & vbCrLf
            sHelp &= vbTab & "Set currency to xxx (EUR/GBP/USD)" & vbCrLf
            sHelp &= cParams__CC_HourlyRate & "xxx" & vbCrLf
            sHelp &= vbTab & "Set hourly rate to xxx" & vbCrLf
            sHelp &= cParams__CC_Min & "xxx" & vbCrLf
            sHelp &= vbTab & "Set minimum cc amount to xxx" & vbCrLf
            sHelp &= cParams__CC_Max & "xxx" & vbCrLf
            sHelp &= vbTab & "Set maximum cc amount to xxx" & vbCrLf
            sHelp &= cParams__CC_IncrementAmount & "xxx" & vbCrLf
            sHelp &= vbTab & "Set cc increment to xxx" & vbCrLf
            sHelp &= cParams__CC_Enable & vbCrLf
            sHelp &= vbTab & "Enable cc" & vbCrLf
            sHelp &= cParams__CC_Disable & vbCrLf
            sHelp &= vbTab & "Disable cc" & vbCrLf
            sHelp &= vbCrLf
            sHelp &= cParams__PP_Enable & vbCrLf
            sHelp &= vbTab & "Enable SiteCash" & vbCrLf
            sHelp &= cParams__PP_Disable & vbCrLf
            sHelp &= vbTab & "Disable SiteCash" & vbCrLf

            MsgBox(sHelp, MsgBoxStyle.OkOnly, Application.ProductName & " - help")
        Else
            oLogger.WriteToLog("Updating SkCfg file", , 0)
            SkCfg_Update()
        End If

        Bye()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Sub GetRegionInfo(ByVal sCurrencyCode As String)
        'See: http://nl.wikipedia.org/wiki/ISO_3166
        Select Case sCurrencyCode.ToUpper
            Case "EUR"
                mRegionInfo = New System.Globalization.RegionInfo("nl")
            Case "GBP"
                mRegionInfo = New System.Globalization.RegionInfo("gb")
            Case "USD"
                mRegionInfo = New System.Globalization.RegionInfo("us")
            Case Else
                mRegionInfo = Nothing
        End Select
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNodeList_Plugins As Xml.XmlNodeList
            Dim mXmlNode_Plugin As Xml.XmlNode
            Dim mXmlNode_SiteCash As Xml.XmlNode


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If

            oLogger.WriteToLog("find SiteCash node", , 1)
            mXmlNodeList_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each mXmlNode_Plugin In mXmlNodeList_Plugins
                If mXmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    mXmlNode_SiteCash = mXmlNode_Plugin

                    Exit For
                End If
            Next

            If Not mXmlNode_SiteCash Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNode_SiteCash Is Nothing")
            End If

            Dim mXmlNodeList_DeviceData As Xml.XmlNodeList
            Dim mXmlNode_DeviceData As Xml.XmlNode
            Dim mXmlNode_CreditCard As Xml.XmlNode

            mXmlNodeList_DeviceData = mXmlNode_SiteCash.SelectNodes("sk:device-data", ns)

            For Each mXmlNode_DeviceData In mXmlNodeList_DeviceData
                If mXmlNode_DeviceData.Attributes.GetNamedItem("clsid").InnerText = "dbbdad62-8c09-45f3-b189-9777ec1c8e47" Then
                    mXmlNode_CreditCard = mXmlNode_DeviceData

                    Exit For
                End If
            Next




            '<currency>
            '   <locale>1024</locale>
            '   <symbol>£</symbol>
            '   <isosymbol>GBP</isosymbol>
            '   <placement>0</placement>
            '   <digits>2</digits>
            '</currency>

            If mValue__CC_Currency <> String.Empty Then
                oLogger.WriteToLog("changing currency", , 2)

                If mRegionInfo Is Nothing Then
                    oLogger.WriteToLog("invalid currency selected!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Else
                    Dim mXmlNode_Currency As Xml.XmlNode
                    Dim mXmlNode_CurrencySymbol As Xml.XmlNode
                    Dim mXmlNode_CurrencyIsoSymbol As Xml.XmlNode

                    mXmlNode_Currency = mXmlNode_SiteCash.SelectSingleNode("sk:currency", ns)
                    mXmlNode_CurrencySymbol = mXmlNode_Currency.SelectSingleNode("sk:symbol", ns)
                    mXmlNode_CurrencyIsoSymbol = mXmlNode_Currency.SelectSingleNode("sk:isosymbol", ns)

                    oLogger.WriteToLog("currency symbol    : " & mRegionInfo.CurrencySymbol, , 3)
                    oLogger.WriteToLog("currency iso symbol: " & mRegionInfo.ISOCurrencySymbol, , 3)

                    mXmlNode_CurrencySymbol.InnerText = mRegionInfo.CurrencySymbol
                    mXmlNode_CurrencyIsoSymbol.InnerText = mRegionInfo.ISOCurrencySymbol

                    oLogger.WriteToLog("ok", , 4)

                    mConfigChanged = True
                End If
            End If

            If mValue__CC_HourlyRate <> String.Empty Then
                oLogger.WriteToLog("changing hourly rate", , 2)

                Dim mXmlNode_HourlyRate As Xml.XmlNode
                Dim mXmlNode_ApplicationPrice As Xml.XmlNode
                Dim mXmlNode_ApplicationMultiplier As Xml.XmlNode

                mXmlNode_HourlyRate = mXmlNode_SiteCash.SelectSingleNode("sk:hourprice", ns)

                oLogger.WriteToLog("hourly rate: " & mValue__CC_HourlyRate, , 3)

                mXmlNode_HourlyRate.InnerText = mValue__CC_HourlyRate

                oLogger.WriteToLog("ok", , 4)

                oLogger.WriteToLog("application price", , 3)
                mXmlNode_ApplicationPrice = mXmlNode_SiteCash.SelectSingleNode("sk:applicationprice", ns)
                mXmlNode_ApplicationPrice.Attributes.GetNamedItem("enabled").InnerText = "false"
                oLogger.WriteToLog("disabled", , 4)

                oLogger.WriteToLog("application multiplier", , 3)
                mXmlNode_ApplicationMultiplier = mXmlNode_SiteCash.SelectSingleNode("sk:applicationmultiplier", ns)
                mXmlNode_ApplicationMultiplier.Attributes.GetNamedItem("enabled").InnerText = "true"
                mXmlNode_ApplicationMultiplier.InnerText = "100"
                oLogger.WriteToLog("enabled (100%)", , 4)

                mConfigChanged = True
            End If

            If mValue__CC_Max <> String.Empty Or mValue__CC_Min <> String.Empty Or mValue__CC_Increment <> String.Empty Then
                oLogger.WriteToLog("changing min/max/increment", , 2)

                Dim mXmlNode_MinAmount As Xml.XmlNode
                Dim mXmlNode_MaxAmount As Xml.XmlNode
                Dim mXmlNode_Increment As Xml.XmlNode


                If mXmlNode_CreditCard Is Nothing Then
                    oLogger.WriteToLog("could not find credit card node!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Else
                    If mValue__CC_Min <> String.Empty Then
                        mXmlNode_MinAmount = mXmlNode_CreditCard.SelectSingleNode("sk:min-amount", ns)
                        mXmlNode_MinAmount.InnerText = mValue__CC_Min
                    End If

                    If mValue__CC_Max <> String.Empty Then
                        mXmlNode_MaxAmount = mXmlNode_CreditCard.SelectSingleNode("sk:max-amount", ns)
                        mXmlNode_MaxAmount.InnerText = mValue__CC_Max
                    End If

                    If mValue__CC_Increment <> String.Empty Then
                        mXmlNode_Increment = mXmlNode_CreditCard.SelectSingleNode("sk:amount-increment", ns)
                        mXmlNode_Increment.InnerText = mValue__CC_Increment
                    End If

                    mConfigChanged = True
                End If
            End If

            If mValue__CC_Enable Or mValue__CC_Disable Then
                oLogger.WriteToLog("enabling/disabling cc", , 2)

                If mXmlNode_CreditCard Is Nothing Then
                    oLogger.WriteToLog("could not find credit card node!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Else
                    If mValue__CC_Disable Then
                        oLogger.WriteToLog("disabling", , 3)
                        mXmlNode_CreditCard.Attributes.GetNamedItem("enabled").InnerText = "false"
                    Else
                        oLogger.WriteToLog("enabling", , 3)
                        mXmlNode_CreditCard.Attributes.GetNamedItem("enabled").InnerText = "true"
                    End If

                    oLogger.WriteToLog("ok", , 4)

                    mConfigChanged = True
                End If
            End If

            If mValue__CC_Region <> String.Empty Then
                oLogger.WriteToLog("setting cc region", , 2)

                oLogger.WriteToLog("finding settings for region '" & mValue__CC_Region & "'", , 3)
                oLogger.WriteToLog("username", , 4)
                oLogger.WriteToLog(cc_regions.GetUsername(mValue__CC_Region), , 5)
                oLogger.WriteToLog("password", , 4)
                oLogger.WriteToLog(cc_regions.GetPassword(mValue__CC_Region), , 5)
                oLogger.WriteToLog("size", , 4)
                oLogger.WriteToLog(cc_regions.GetSize(mValue__CC_Region), , 5)


                If mXmlNode_CreditCard Is Nothing Then
                    oLogger.WriteToLog("could not find credit card node!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Else
                    Dim mXmlNodeList_Gateways As Xml.XmlNodeList
                    Dim mXmlNode_Gateway As Xml.XmlNode
                    Dim mXmlNode_CCGateway As Xml.XmlNode

                    mXmlNodeList_Gateways = mXmlNode_CreditCard.SelectNodes("sk:gateway", ns)

                    For Each mXmlNode_Gateway In mXmlNodeList_Gateways
                        If mXmlNode_Gateway.Attributes.GetNamedItem("clsid").InnerText = "b9bae269-bad6-41eb-a84f-fdfc938cb93a" Then
                            mXmlNode_CCGateway = mXmlNode_Gateway

                            Exit For
                        End If
                    Next

                    If mXmlNode_CCGateway Is Nothing Then
                        oLogger.WriteToLog("could not find credit card gateway node!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    Else
                        mXmlNode_CCGateway.SelectSingleNode("sk:user", ns).InnerText = cc_regions.GetUsername(mValue__CC_Region)
                        mXmlNode_CCGateway.SelectSingleNode("sk:vendor", ns).InnerText = cc_regions.GetUsername(mValue__CC_Region)
                        mXmlNode_CCGateway.SelectSingleNode("sk:partner", ns).InnerText = "verisign"
                        mXmlNode_CCGateway.SelectSingleNode("sk:password", ns).InnerText = cc_regions.GetPassword(mValue__CC_Region)
                        mXmlNode_CCGateway.SelectSingleNode("sk:password", ns).Attributes.GetNamedItem("size").InnerText = cc_regions.GetSize(mValue__CC_Region)
                        mXmlNode_CCGateway.SelectSingleNode("sk:comment", ns).InnerText = Environment.MachineName
                        mXmlNode_CCGateway.SelectSingleNode("sk:comment", ns).Attributes.GetNamedItem("enabled").InnerText = "true"
                        mXmlNode_CCGateway.SelectSingleNode("sk:comment2", ns).InnerText = "OVCC purchase"

                        oLogger.WriteToLog("ok", , 3)
                    End If

                    mConfigChanged = True
                End If
            End If



            If mValue__PP_Enable Or mValue__PP_Disable Then
                oLogger.WriteToLog("enabling/disabling pp", , 2)

                Dim mXmlNode_PrepaidCard As Xml.XmlNode

                mXmlNodeList_DeviceData = mXmlNode_SiteCash.SelectNodes("sk:device-data", ns)

                For Each mXmlNode_DeviceData In mXmlNodeList_DeviceData
                    If mXmlNode_DeviceData.Attributes.GetNamedItem("clsid").InnerText = "0550dfaf-eec1-4e16-a59e-d37ff6395a56" Then
                        mXmlNode_PrepaidCard = mXmlNode_DeviceData

                        Exit For
                    End If
                Next

                If mXmlNode_PrepaidCard Is Nothing Then
                    oLogger.WriteToLog("could not find prepaid card node!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Else
                    If mValue__PP_Disable Then
                        oLogger.WriteToLog("disabling", , 3)
                        mXmlNode_PrepaidCard.Attributes.GetNamedItem("enabled").InnerText = "false"
                    Else
                        oLogger.WriteToLog("enabling", , 3)
                        mXmlNode_PrepaidCard.Attributes.GetNamedItem("enabled").InnerText = "true"
                    End If

                    oLogger.WriteToLog("ok", , 4)

                    mConfigChanged = True
                End If
            End If


            If mValue__CC_Disable And mValue__PP_Disable Then
                oLogger.WriteToLog("disabling SiteCash", , 3)
                mXmlNode_SiteCash.Attributes.GetNamedItem("enabled").Value = "false"

                mConfigChanged = True
            Else
                oLogger.WriteToLog("enabling SiteCash", , 3)
                mXmlNode_SiteCash.Attributes.GetNamedItem("enabled").Value = "true"

                mConfigChanged = True
            End If


            If mConfigChanged Then
                oLogger.WriteToLog("saving", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            End If

        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try

    End Function
End Module
