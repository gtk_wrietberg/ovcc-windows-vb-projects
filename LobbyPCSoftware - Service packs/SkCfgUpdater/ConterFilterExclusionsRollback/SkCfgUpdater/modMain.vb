Module modMain
    Private mSkCfgFile As String

    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("Finding SkCfg file (try #1)", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If

        LoadPatches()

        SkCfg_Update()

        '------------------------------------------------------
        If mConfigChanged Then
            oLogger.WriteToLog("Killing SiteKiosk", , 0)
            KillSiteKiosk()
        End If

        Bye()
    End Sub

    Private Sub LoadPatches()
        oLogger.WriteToLog("loading patches", , 0)

        gArr_Urls = New List(Of String)
        Try
            Using sr As New IO.StreamReader("excluded.urls.txt")
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    gArr_Urls.Add(line)
                End While
            End Using
        Catch ex As Exception
            oLogger.WriteToLog("ERROR WHILE LOADING excluded.urls.txt", , 1)
        End Try
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_Plugins As Xml.XmlNodeList
            Dim mXmlNode_Plugin As Xml.XmlNode
            Dim mXmlNode_Excludes As Xml.XmlNodeList
            Dim mXmlNode_Exclude As Xml.XmlNode
            Dim mXmlNode_SiteCoach As Xml.XmlNode
            Dim sTmp As String = ""


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("find SiteCoach node", , 1)

            mXmlNode_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", ns)
            For Each mXmlNode_Plugin In mXmlNode_Plugins
                sTmp = mXmlNode_Plugin.Attributes.GetNamedItem("name").Value.ToLower

                If sTmp = "SiteCoach".ToLower Then
                    mXmlNode_SiteCoach = mXmlNode_Plugin
                End If
            Next

            If Not mXmlNode_SiteCoach Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_SiteCoach Is Nothing")
            End If


            oLogger.WriteToLog("find exclude nodes", , 1)

            mXmlNode_Excludes = mXmlNode_SiteCoach.SelectNodes("sk:exclude", ns)
            If mXmlNode_Excludes.Count > 0 Then
                oLogger.WriteToLog(mXmlNode_Excludes.Count.ToString, , 2)

                oLogger.WriteToLog("removing", , 1)
                For Each mXmlNode_Exclude In mXmlNode_Excludes
                    sTmp = mXmlNode_Exclude.InnerText

                    If gArr_Urls.Contains(sTmp) Then
                        oLogger.WriteToLog("removed (" & sTmp & ")", , 2)
                        gArr_Urls.Remove(sTmp)
                        mXmlNode_SiteCoach.RemoveChild(mXmlNode_Exclude)

                        mConfigChanged = True
                    End If
                Next
            Else
                oLogger.WriteToLog("none", , 2)
            End If


            If mConfigChanged Then
                oLogger.WriteToLog("saving", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("not saving, nothing was changed", , 1)
            End If

        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try

    End Function
End Module
