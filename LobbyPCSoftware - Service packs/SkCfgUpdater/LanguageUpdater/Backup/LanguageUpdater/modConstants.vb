Module modConstants
    Public ReadOnly REGKEY__32 As String = "SOFTWARE\PROVISIO\SiteKiosk"
    Public ReadOnly REGKEY__64 As String = "SOFTWARE\Wow6432Node\PROVISIO\SiteKiosk"

    Public ReadOnly PARAM__LANG_ID As String = "--lang-id="
    Public ReadOnly PARAM__DEFAULT_LANG_ID As String = "--default-lang-id="
    Public ReadOnly PARAM__SET_AS_DEFAULT As String = "--set-as-default"

    Public ReadOnly DEFAULT__LANG_ID As Integer = 9
End Module
