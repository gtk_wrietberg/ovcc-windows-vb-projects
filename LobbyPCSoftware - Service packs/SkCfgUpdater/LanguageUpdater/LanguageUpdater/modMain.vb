Imports System.Text.RegularExpressions

Module ModMain
    Private mSkVersion As String
    Private mSkVersionInt As Integer
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False
    Private mLangId As Integer = -1
    Private mDefaultLangId As Integer = -1
    Private mSetDefaultLanguage As Boolean = False

    Private oCopyFiles As CopyFiles

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        Dim sTmp As String, iTmp As Integer
        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, PARAM__LANG_ID) > 0 Then
                sTmp = arg.Replace(PARAM__LANG_ID, "")
                If Integer.TryParse(sTmp, iTmp) Then
                    If iTmp < 1 Or iTmp > 10000 Then
                        iTmp = -1
                    End If
                    mLangId = iTmp
                End If
            End If
            If InStr(arg, PARAM__DEFAULT_LANG_ID) > 0 Then
                sTmp = arg.Replace(PARAM__DEFAULT_LANG_ID, "")
                If Integer.TryParse(sTmp, iTmp) Then
                    If iTmp < 1 Or iTmp > 10000 Then
                        iTmp = DEFAULT__LANG_ID
                    End If
                    mSetDefaultLanguage = True
                    mDefaultLangId = iTmp
                End If
            End If

            If InStr(arg, PARAM__SET_AS_DEFAULT) > 0 Then
                mSetDefaultLanguage = True
                mDefaultLangId = mLangId
            End If
        Next

        oLogger.WriteToLog("Language id", , 0)
        oLogger.WriteToLog("value: " & mLangId.ToString, , 1)
        If mLangId < 0 Then
            oLogger.WriteToLog("uh oh", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Bye()
            Exit Sub
        End If
        If mSetDefaultLanguage Then
            oLogger.WriteToLog("default: " & mDefaultLangId.ToString, , 1)
        End If


        oLogger.WriteToLog("searching for SiteKiosk version", , 0)
        If RegistryKeyExists(Microsoft.Win32.RegistryHive.LocalMachine, REGKEY__64) Then
            oLogger.WriteToLog("registry: HKEY_LOCAL_MACHINE\" & REGKEY__64 & "!Build ", , 1)
            mSkVersion = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__64, "Build", "0")
        ElseIf RegistryKeyExists(Microsoft.Win32.RegistryHive.LocalMachine, REGKEY__32) Then
            oLogger.WriteToLog("registry: HKEY_LOCAL_MACHINE\" & REGKEY__32 & "!Build ", , 1)
            mSkVersion = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__32, "Build", "0")
        Else
            oLogger.WriteToLog("registry: ????????????????????? ", , 1)
        End If

        Dim sVersionParts As String() = mSkVersion.Split(".")
        oLogger.WriteToLog("major version", , 1)
        If sVersionParts.Length > 1 Then
            mSkVersionInt = CInt(sVersionParts(0))
            oLogger.WriteToLog(mSkVersionInt.ToString, , 2)
        Else
            oLogger.WriteToLog("?", , 2)
            mSkVersionInt = 0
        End If


        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = "WTF!"
        If RegistryKeyExists(Microsoft.Win32.RegistryHive.LocalMachine, REGKEY__64) Then
            oLogger.WriteToLog("registry: HKEY_LOCAL_MACHINE\" & REGKEY__64 & "!LastCfg ", , 1)
            mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__64, "LastCfg", "")
        ElseIf RegistryKeyExists(Microsoft.Win32.RegistryHive.LocalMachine, REGKEY__32) Then
            oLogger.WriteToLog("registry: HKEY_LOCAL_MACHINE\" & REGKEY__32 & "!LastCfg ", , 1)
            mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__32, "LastCfg", "")
        Else
            oLogger.WriteToLog("registry: ????????????????????? ", , 1)
        End If
        oLogger.WriteToLog("found: " & mSkCfgFile, , 2)


        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("File does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Bye()
            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If

        '---------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        Select Case mSkVersionInt
            Case 9
                oCopyFiles.SourceDirectory = "files_sk9"
            Case Else
                oCopyFiles.SourceDirectory = "files_default"
        End Select

        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        SkCfg_Update()

        Bye()
    End Sub

    Private Function RegistryKeyExists(ByVal hive As Microsoft.Win32.RegistryHive, ByVal key As String) As Boolean
        Dim tmpHive As Microsoft.Win32.RegistryKey
        Dim tmpRegKey As Microsoft.Win32.RegistryKey


        Select Case hive
            Case Microsoft.Win32.RegistryHive.ClassesRoot
                tmpHive = Microsoft.Win32.Registry.ClassesRoot
            Case Microsoft.Win32.RegistryHive.CurrentConfig
                tmpHive = Microsoft.Win32.Registry.CurrentConfig
            Case Microsoft.Win32.RegistryHive.CurrentUser
                tmpHive = Microsoft.Win32.Registry.CurrentUser
            Case Microsoft.Win32.RegistryHive.LocalMachine
                tmpHive = Microsoft.Win32.Registry.LocalMachine
            Case Microsoft.Win32.RegistryHive.PerformanceData
                tmpHive = Microsoft.Win32.Registry.PerformanceData
            Case Microsoft.Win32.RegistryHive.Users
                tmpHive = Microsoft.Win32.Registry.Users
            Case Else
                Return False
        End Select

        tmpRegKey = tmpHive.OpenSubKey(key, False)
        If tmpRegKey Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_SiteSkin As Xml.XmlNode
            Dim mXmlNode_Languages As Xml.XmlNodeList
            Dim mXmlNode_Language As Xml.XmlNode
            Dim mXmlNode_NewLanguage As Xml.XmlNode
            Dim mXmlNode_DefaultLanguage As Xml.XmlNode

            Dim bLangChangeNeeded As Boolean = True
            Dim bDefaultLangInList As Boolean = False

            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("siteskin node", , 1)
            mXmlNode_SiteSkin = mXmlNode_Root.SelectSingleNode("sk:siteskin", ns)
            If Not mXmlNode_SiteSkin Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_SiteSkin Is Nothing")
            End If

            oLogger.WriteToLog("language nodes", , 1)

            mXmlNode_Languages = mXmlNode_SiteSkin.SelectNodes("sk:language", ns)
            For Each mXmlNode_Language In mXmlNode_Languages
                If mXmlNode_Language.InnerText = mLangId.ToString Then
                    oLogger.WriteToLog("lang id already exists, no change needed", , 2)
                    bLangChangeNeeded = False
                End If

                If mSetDefaultLanguage And mDefaultLangId > 0 And Not bDefaultLangInList Then
                    If mXmlNode_Language.InnerText = mDefaultLangId.ToString Then
                        bDefaultLangInList = True
                    End If
                End If
            Next
            If bLangChangeNeeded Then
                oLogger.WriteToLog("adding new lang id", , 2)
                mXmlNode_NewLanguage = mXmlNode_SiteSkin.SelectSingleNode("sk:language", ns).CloneNode(True)
                mXmlNode_NewLanguage.InnerText = mLangId.ToString
                mXmlNode_SiteSkin.AppendChild(mXmlNode_NewLanguage)

                If mDefaultLangId = mLangId Then
                    bDefaultLangInList = True
                End If

                mConfigChanged = True
            End If

            If mSetDefaultLanguage Then
                oLogger.WriteToLog("defaultlanguage node", , 1)
                If bDefaultLangInList Then
                    mXmlNode_DefaultLanguage = mXmlNode_SiteSkin.SelectSingleNode("sk:defaultlanguage", ns)
                    If Not mXmlNode_DefaultLanguage Is Nothing Then
                        oLogger.WriteToLog("setting to: " & mDefaultLangId.ToString, , 2)
                        mXmlNode_DefaultLanguage.InnerText = mDefaultLangId.ToString

                        mConfigChanged = True
                    Else
                        Throw New Exception("mXmlNode_DefaultLanguage Is Nothing")
                    End If
                Else
                    oLogger.WriteToLog("value for defaultlanguage not found in available languages, skipping", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End If
            End If


            oLogger.WriteToLog("saving", , 0)
            If mConfigChanged Then
                oLogger.WriteToLog("creating backup of '" & mSkCfgFile & "'", , 1)
                BackupFile(New IO.FileInfo(mSkCfgFile))

                oLogger.WriteToLog("saving existing file", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)

                oLogger.WriteToLog("restarting SiteKiosk", , 1)
                KillSiteKiosk()
            Else
                oLogger.WriteToLog("skipped, nothing was changed", , 1)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLogRelative("writing: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLogRelative("done", , 2)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

