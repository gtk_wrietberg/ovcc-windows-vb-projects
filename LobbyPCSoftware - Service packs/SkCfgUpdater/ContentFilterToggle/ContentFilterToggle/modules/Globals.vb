Module Globals
    Public oLogger As Logger
    Public oSkCfg As SkCfg

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String
    Public g_OverwriteExistingConfig As Boolean

    Public g_TESTMODE As Boolean = False


    Public ReadOnly c_PARAM_ON0 As String = "on"
    Public ReadOnly c_PARAM_ON1 As String = "-on"
    Public ReadOnly c_PARAM_ON2 As String = "--on"
    Public ReadOnly c_PARAM_OFF0 As String = "off"
    Public ReadOnly c_PARAM_OFF1 As String = "-off"
    Public ReadOnly c_PARAM_OFF2 As String = "--off"

    Public g_ContentFilterState As Boolean


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        Try
            System.IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try
        Try
            System.IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
