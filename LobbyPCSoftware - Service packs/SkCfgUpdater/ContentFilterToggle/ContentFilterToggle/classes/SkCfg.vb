Public Class SkCfg
    Private mSkCfgFile As String
    Private mSkBuild As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String
    Private mConfigChanged As Boolean

    Private ReadOnly c_REGROOT_SITEKIOSK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public ReadOnly Property SiteKioskFolder As String
        Get
            Return mSiteKioskFolder
        End Get
    End Property


    Public Function Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_Plugins As Xml.XmlNodeList
            Dim mXmlNode_Plugin As Xml.XmlNode
            Dim mXmlNode_SiteCoach As Xml.XmlNode
            Dim sTmp As String = ""


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("find SiteCoach node", , 1)

            mXmlNode_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", ns)
            For Each mXmlNode_Plugin In mXmlNode_Plugins
                sTmp = mXmlNode_Plugin.Attributes.GetNamedItem("name").Value.ToLower

                If sTmp = "SiteCoach".ToLower Then
                    mXmlNode_SiteCoach = mXmlNode_Plugin
                End If
            Next

            If Not mXmlNode_SiteCoach Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_SiteCoach Is Nothing")
            End If


            oLogger.WriteToLog("is SiteCoach enabled?", , 1)
            Dim sVal As String = mXmlNode_SiteCoach.Attributes.GetNamedItem("enabled").Value.ToLower

            If sVal.Equals("true") Then
                oLogger.WriteToLog("yes", , 2)
                If Not g_ContentFilterState Then
                    oLogger.WriteToLog("disabling", , 3)
                    mXmlNode_SiteCoach.Attributes.GetNamedItem("enabled").Value = "false"
                    mConfigChanged = True
                Else
                    oLogger.WriteToLog("nothing to do here", , 3)
                End If
            Else
                oLogger.WriteToLog("no", , 2)
                If g_ContentFilterState Then
                    oLogger.WriteToLog("enabling", , 3)
                    mXmlNode_SiteCoach.Attributes.GetNamedItem("enabled").Value = "true"
                    mConfigChanged = True
                Else
                    oLogger.WriteToLog("nothing to do here", , 3)
                End If
            End If


            If mConfigChanged Then
                oLogger.WriteToLog("saving", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("not saving, nothing was changed", , 1)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try

        Return True
    End Function


    Public Sub New()
        mSiteKioskFolder = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "InstallDir", "")
        mSkCfgFile = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "LastCfg", "")
        mSkBuild = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "Build", "")
        mConfigChanged = False
    End Sub
End Class
