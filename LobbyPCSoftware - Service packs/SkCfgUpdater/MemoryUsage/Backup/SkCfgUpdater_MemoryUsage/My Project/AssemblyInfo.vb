﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SkCfgUpdater_MemoryUsage")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("SkCfgUpdater_MemoryUsage")> 
<Assembly: AssemblyCopyright("Copyright ©  2013")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("3e3b472f-e990-4f27-8378-3dccaebe24f0")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.14.1312")> 
<Assembly: AssemblyFileVersion("1.0.14.1312")> 
