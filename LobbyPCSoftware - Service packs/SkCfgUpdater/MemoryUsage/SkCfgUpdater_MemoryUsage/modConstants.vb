Module modConstants
    Public ReadOnly DEFAULT_SiteKioskMaxMemUsage As String = "4072"
    Public ReadOnly DEFAULT_SiteKioskMaxPagefile As String = "90"
    Public ReadOnly DEFAULT_Pagefile As String = "4072"

    Public ReadOnly REGKEY__Software_Provisio_SiteKiosk As String = "SOFTWARE\PROVISIO\SiteKiosk"

    Public ReadOnly ARGS_Pagefile As String = "--pagefile-size="
    Public ReadOnly ARGS_SiteKioskMaxMemUsage As String = "--sitekiosk-max-mem="
    Public ReadOnly ARGS_SiteKioskMaxPagefile As String = "--sitekiosk-max-pagefile="

    Public ReadOnly CONSTANTS_PageFileRegistryString As String = "C:\pagefile.sys %%SIZE%% %%SIZE%%"
End Module
