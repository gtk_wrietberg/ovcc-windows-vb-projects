Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        Dim sTmp As String, iTmp As Integer
        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, ARGS_Pagefile) > 0 Then
                sTmp = arg.Replace(ARGS_Pagefile, "")
                If Integer.TryParse(sTmp, iTmp) Then
                    If iTmp < 2048 Or iTmp > 20480 Then
                        sTmp = DEFAULT_SiteKioskMaxMemUsage
                    End If
                    g_Pagefile = sTmp
                    g_Pagefile_SET = True
                End If
            End If
            If InStr(arg, ARGS_SiteKioskMaxMemUsage) > 0 Then
                sTmp = arg.Replace(ARGS_SiteKioskMaxMemUsage, "")
                If Integer.TryParse(sTmp, iTmp) Then
                    If iTmp < 2048 Or iTmp > 20480 Then
                        sTmp = DEFAULT_SiteKioskMaxMemUsage
                    End If
                    g_SiteKioskMaxMemUsage = sTmp
                    g_SiteKioskMaxMemUsage_SET = True
                End If
            End If
            If InStr(arg, ARGS_SiteKioskMaxPagefile) > 0 Then
                sTmp = arg.Replace(ARGS_SiteKioskMaxPagefile, "")
                If Integer.TryParse(sTmp, iTmp) Then
                    If iTmp < 0 Or iTmp > 100 Then
                        sTmp = DEFAULT_SiteKioskMaxPagefile
                    End If
                    g_SiteKioskMaxPagefile = sTmp
                    g_SiteKioskMaxPagefile_SET = True
                End If
            End If
        Next

        oLogger.WriteToLog("settings", , 0)
        oLogger.WriteToLog("page file size: " & g_Pagefile, , 1)
        oLogger.WriteToLog("SiteKiosk Max Mem usage: " & g_SiteKioskMaxMemUsage, , 1)
        oLogger.WriteToLog("SiteKiosk Max Pagefile usage: " & g_SiteKioskMaxPagefile, , 1)

        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__Software_Provisio_SiteKiosk, "LastCfg", "WTF")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 2)


        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("File does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Else
            oLogger.WriteToLog("ok", , 2)

            oLogger.WriteToLog("updating SkCfg file", , 0)
            SkCfg_Update()
        End If


        oLogger.WriteToLog("updating page file size", , 0)
        If g_Pagefile_SET Then
            If SetPageFile() Then
                oLogger.WriteToLog("ok", , 1)
            Else
                oLogger.WriteToLog("fail", , 1)
            End If
        Else
            oLogger.WriteToLog("skipped, not provided", , 1)
        End If

        Bye()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_Security As Xml.XmlNode
            Dim mXmlNode_MemCheck As Xml.XmlNode
            Dim mXmlNode_MaxMemUsage As Xml.XmlNode
            Dim mXmlNode_MaxPagefile As Xml.XmlNode


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("security node", , 1)
            mXmlNode_Security = mXmlNode_Root.SelectSingleNode("sk:security", ns)
            If Not mXmlNode_Security Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_Security Is Nothing")
            End If

            oLogger.WriteToLog("mem-check node", , 1)
            mXmlNode_MemCheck = mXmlNode_Security.SelectSingleNode("sk:mem-check", ns)
            If Not mXmlNode_MemCheck Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_MemCheck Is Nothing")
            End If

            oLogger.WriteToLog("max-mem-usage node", , 1)
            mXmlNode_MaxMemUsage = mXmlNode_MemCheck.SelectSingleNode("sk:max-mem-usage", ns)
            If Not mXmlNode_MaxMemUsage Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_MaxMemUsage Is Nothing")
            End If

            oLogger.WriteToLog("max-pagefile node", , 1)
            mXmlNode_MaxPagefile = mXmlNode_MemCheck.SelectSingleNode("sk:max-pagefile", ns)
            If Not mXmlNode_MaxPagefile Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_MaxPagefile Is Nothing")
            End If


            oLogger.WriteToLog("max-mem-usage value", , 1)
            If g_SiteKioskMaxMemUsage_SET Then
                If mXmlNode_MaxMemUsage.InnerText <> g_SiteKioskMaxMemUsage Then
                    mXmlNode_MaxMemUsage.InnerText = g_SiteKioskMaxMemUsage
                    mConfigChanged = True
                    oLogger.WriteToLog("set", , 2)
                Else
                    oLogger.WriteToLog("equal, not changed", , 2)
                End If
            Else
                oLogger.WriteToLog("skipped, not provided", , 2)
            End If


            If g_SiteKioskMaxPagefile_SET Then
                oLogger.WriteToLog("max-pagefile value", , 1)
                If mXmlNode_MaxPagefile.InnerText <> g_SiteKioskMaxPagefile Then
                    mXmlNode_MaxPagefile.InnerText = g_SiteKioskMaxPagefile
                    mConfigChanged = True
                    oLogger.WriteToLog("set", , 2)
                Else
                    oLogger.WriteToLog("equal, not changed", , 2)
                End If
            Else
                oLogger.WriteToLog("skipped, not provided", , 2)
            End If



            oLogger.WriteToLog("saving", , 0)
            If mConfigChanged Then
                oLogger.WriteToLog("creating backup of '" & mSkCfgFile & "'", , 1)
                BackupFile(New IO.FileInfo(mSkCfgFile))

                oLogger.WriteToLog("saving existing file", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)

                oLogger.WriteToLog("restarting SiteKiosk", , 1)
                KillSiteKiosk()
            Else
                oLogger.WriteToLog("skipped, nothing was changed", , 1)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLogRelative("writing: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLogRelative("done", , 2)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

    Private Function SetPageFile() As Boolean
        Dim bRet As Boolean = False
        Dim sTmpPagefileSizeString As String = ""
        sTmpPagefileSizeString = CONSTANTS_PageFileRegistryString.Replace("%%SIZE", g_Pagefile)

        oLogger.WriteToLog("setting page file size", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management!PagingFiles", , 2)
        oLogger.WriteToLog("value: " & sTmpPagefileSizeString & " (REG_MULTI_SZ)", , 2)
        Try
            Dim sValuePageFile As String() = {sTmpPagefileSizeString}

            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management", "PagingFiles", sValuePageFile, Microsoft.Win32.RegistryValueKind.MultiString)

            bRet = True
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Return bRet
    End Function

End Module

