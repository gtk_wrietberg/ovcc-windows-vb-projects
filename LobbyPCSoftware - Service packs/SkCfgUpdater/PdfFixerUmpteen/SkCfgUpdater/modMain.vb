Module modMain
    Public oLogger As clsLogger

    Private mSkCfgFile As String

    Private mConfigChanged As Boolean = False

    Public Sub Main()
        oLogger = New clsLogger

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("Finding SkCfg file (try #1)", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", clsLogger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If


        SkCfg_Update()

        Bye()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_FileManager As Xml.XmlNode
            Dim mXmlNode_MimeFilters As Xml.XmlNode
            Dim mXmlNode_Filters As Xml.XmlNodeList
            Dim mXmlNode_Filter As Xml.XmlNode


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If

            oLogger.WriteToLog("find filemanager node", , 1)
            mXmlNode_FileManager = mXmlNode_Root.SelectSingleNode("sk:filemanager", ns)

            oLogger.WriteToLog("find mime-filters node", , 1)
            mXmlNode_MimeFilters = mXmlNode_FileManager.SelectSingleNode("sk:mime-filters", ns)

            oLogger.WriteToLog("find filter nodes", , 1)
            mXmlNode_Filters = mXmlNode_MimeFilters.SelectNodes("sk:filter", ns)

            Dim sPlayer As String = ""
            Dim sEnabled As String = ""

            For Each mXmlNode_Filter In mXmlNode_Filters
                sEnabled = mXmlNode_Filter.Attributes.GetNamedItem("enabled").Value
                sPlayer = mXmlNode_Filter.Attributes.GetNamedItem("player").Value

                If sPlayer = "pdf" Then
                    oLogger.WriteToLog("found pdf filter node", , 1)

                    If sEnabled = "true" Then
                        oLogger.WriteToLog("it's enabled, let's disable it", , 1)
                        mXmlNode_Filter.Attributes.GetNamedItem("enabled").Value = "false"

                        mConfigChanged = True
                    Else
                        oLogger.WriteToLog("it's already disabled", , 1)
                    End If
                End If
            Next

            If mConfigChanged Then
                oLogger.WriteToLog("saving", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("not saving, nothing was changed", , 1)
            End If

        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try

    End Function
End Module
