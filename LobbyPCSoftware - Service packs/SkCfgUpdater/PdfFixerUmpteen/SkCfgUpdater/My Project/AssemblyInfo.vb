﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SkCfgUpdater_PdfFixerUmpteen")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("SkCfgUpdater_PdfFixerUmpteen")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("01726d0d-2694-4827-bf70-6ad90ea76c3d")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.4.1700")> 
<Assembly: AssemblyFileVersion("1.0.4.1700")> 
