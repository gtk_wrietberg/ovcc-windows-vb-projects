Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String

    Public Sub Main()
        Dim bSomethingChanged As Boolean = False

        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If


        '------------------------------------------------------
        oLogger.WriteToLog(" ", , 0)
        oLogger.WriteToLog("Updating 'configuration.js'", , 0)
        UpdateJavascriptFile()


        '------------------------------------------------------
        oLogger.WriteToLog("Saving skcfg", , 0)
        SkCfg_Update()


        '------------------------------------------------------
        oLogger.WriteToLog("Restarting SiteKiosk", , 0)
        KillSiteKiosk()

        Bye()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Sub UpdateJavascriptFile()
        Dim lines As New List(Of String)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Html\GuiV2\js\configuration.js"
        sFile = sFile.Replace("\\", "\")

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String
                Dim iLineNumber As Integer = 0

                While Not sr.EndOfStream
                    line = sr.ReadLine
                    iLineNumber += 1

                    If line.Contains(cHOTELURL__SEARCH) Then
                        line = cHOTELURL__REPLACE
                    End If

                    lines.Add(line)
                End While
            End Using

            BackupFile(New IO.FileInfo(sFile))

            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_Global As Xml.XmlNode
            Dim mXmlNode_StartpageConfig As Xml.XmlNode
            Dim mXmlNode_Startpage As Xml.XmlNode
            

            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("Setting start page to: " & cSTARTPAGE__URL, , 2)
            mXmlNode_Global = mXmlNode_Root.SelectSingleNode("sk:global", ns)
            mXmlNode_Startpage = mXmlNode_Global.SelectSingleNode("sk:homepage", ns)
            If mXmlNode_Startpage Is Nothing Then
                mXmlNode_StartpageConfig = mXmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)
                mXmlNode_Startpage = mXmlNode_StartpageConfig.SelectSingleNode("sk:startpage", ns)
                oLogger.WriteToLog("new school", , 3)
            Else
                oLogger.WriteToLog("old school", , 3)
            End If
            mXmlNode_Startpage.InnerText = cSTARTPAGE__URL


            BackupFile(New IO.FileInfo(mSkCfgFile))

            oLogger.WriteToLog("saving", , 1)
            mSkCfgXml.Save(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        oLogger.WriteToLog("creating backup of '" & oFile.FullName & "'", , 0)

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLog("backup file: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLog("done", , 1)
                bRet = True
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

