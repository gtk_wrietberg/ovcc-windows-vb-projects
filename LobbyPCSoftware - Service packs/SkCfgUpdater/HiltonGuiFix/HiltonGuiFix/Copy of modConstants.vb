Module modConstants
    Public ReadOnly cRegExp_Airline As String = "(FAAwebsites\[')([a-z}+)('\].+="")(.+)("";)"

    Public ReadOnly cRegExp_Airline_Name As String = "(FAAwebsites\['name'\].+="")(.+)("";)"
    Public ReadOnly cRegExp_Airline_Link As String = "(FAAwebsites\['link'\].+="")(.+)("";)"
    Public ReadOnly cRegExp_Airline_Image As String = "(FAAwebsites\['image'\].+="")(.+)("";)"

    Public ReadOnly cTemplate_Airline As String = "FAAwebsites['name'].+="")(.+)("";)"


    Public ReadOnly cXML_DELETE As String = "AirlineUpdates.delete.xml"
    Public ReadOnly cXML_UPDATE As String = "AirlineUpdates.update.xml"
    Public ReadOnly cXML_ADD As String = "AirlineUpdates.add.xml"
End Module
