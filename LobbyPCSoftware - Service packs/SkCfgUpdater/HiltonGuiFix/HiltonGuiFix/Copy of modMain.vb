Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If

        LoadPatches()


        SkCfg_Update()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Sub ParseJavscriptFile()
        Dim lines As New List(Of String)
        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\default\Scripts\ibahn.js"

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String, newline As String, url_replacement() As String
                Dim tmpName As String, tmpCurrentName As String = "ThisNameDoesNotExistHopeFully"
                Dim tmpLine As String, tmpUrl As String, bSkip As Boolean

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If Not Regex.IsMatch(line, cRegExp_Airline) Then
                        'failsafe
                        bSkip = False
                    End If

                    tmpLine = Regex.Replace(line, cRegExp_Airline_Link, "$1%%%$3")
                    tmpName = Regex.Replace(line, cRegExp_Airline_Name, "$2")

                    If tmpName <> "" And tmpName <> tmpCurrentName Then
                        'we found an airline section
                        bSkip = False
                        tmpCurrentName = tmpName
                    End If


                    If tmpCurrentName <> "" Then
                        For Each replacement_string As String In gArr_Update
                            url_replacement = Split(replacement_string, "|", 3)

                            If tmpCurrentName = url_replacement(0) Then
                                bSkip = True
                                oLogger.WriteToLog("updating '" & tmpCurrentName & "'", , 2)
                                newline = tmpLine.Replace("%%%", "")

                                line = newline

                                bUpdated = True
                            End If
                        Next
                        If bYes Then
                            gArr_Delete.Remove(tmpName)
                        End If
                    End If

                    If Not bSkip Then
                        lines.Add(line)
                    End If
                End While
            End Using
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try

    End Sub

    Private Sub LoadPatches()
        oLogger.WriteToLog("loading patches", , 0)

        oLogger.WriteToLog("delete patch", , 1)
        gArr_Delete = New List(Of String)
        Try
            Dim mXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_delete As Xml.XmlNode
            Dim mXmlNodeList_names As Xml.XmlNodeList
            Dim mXmlNode_name As Xml.XmlNode

            mXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 2)
            oLogger.WriteToLog(cXML_DELETE, , 3)
            mXml.Load(cXML_DELETE)
            oLogger.WriteToLog("ok", , 4)

            oLogger.WriteToLog("root node", , 2)
            mXmlNode_Root = mXml.SelectSingleNode("airlineupdates")

            oLogger.WriteToLog("delete node", , 2)
            mXmlNode_delete = mXmlNode_Root.SelectSingleNode("delete")

            oLogger.WriteToLog("name nodes", , 2)
            mXmlNodeList_names = mXmlNode_delete.SelectNodes("name")
            For Each mXmlNode_name In mXmlNodeList_names
                oLogger.WriteToLog(mXmlNode_name.InnerText, , 3)
                gArr_Delete.Add(mXmlNode_name.InnerText)
            Next
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try


        oLogger.WriteToLog("update patch", , 1)
        gArr_Update = New List(Of String)
        Try
            Dim mXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_update As Xml.XmlNode
            Dim mXmlNodeList_items As Xml.XmlNodeList
            Dim mXmlNode_item As Xml.XmlNode
            Dim mXmlNode_name As Xml.XmlNode
            Dim mXmlNode_url As Xml.XmlNode
            Dim mXmlNode_icon As Xml.XmlNode
            Dim sTmp As String

            mXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 2)
            oLogger.WriteToLog(cXML_UPDATE, , 3)
            mXml.Load(cXML_UPDATE)
            oLogger.WriteToLog("ok", , 4)

            oLogger.WriteToLog("root node", , 2)
            mXmlNode_Root = mXml.SelectSingleNode("airlineupdates")

            oLogger.WriteToLog("update node", , 2)
            mXmlNode_update = mXmlNode_Root.SelectSingleNode("update")

            oLogger.WriteToLog("item nodes", , 2)
            mXmlNodeList_items = mXmlNode_update.SelectNodes("item")
            For Each mXmlNode_item In mXmlNodeList_items
                mXmlNode_name = mXmlNode_item.SelectSingleNode("name")
                mXmlNode_url = mXmlNode_item.SelectSingleNode("url")
                mXmlNode_icon = mXmlNode_item.SelectSingleNode("icon")

                sTmp = mXmlNode_name.InnerText & "|" & mXmlNode_url.InnerText & "|" & mXmlNode_icon.InnerText

                oLogger.WriteToLog(sTmp, , 3)
                gArr_Update.Add(sTmp)
            Next
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try


        oLogger.WriteToLog("add patch", , 1)
        gArr_Add = New List(Of String)
        Try
            Dim mXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_add As Xml.XmlNode
            Dim mXmlNodeList_items As Xml.XmlNodeList
            Dim mXmlNode_item As Xml.XmlNode
            Dim mXmlNode_name As Xml.XmlNode
            Dim mXmlNode_url As Xml.XmlNode
            Dim mXmlNode_icon As Xml.XmlNode
            Dim sTmp As String

            mXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 2)
            oLogger.WriteToLog(cXML_ADD, , 3)
            mXml.Load(cXML_ADD)
            oLogger.WriteToLog("ok", , 4)

            oLogger.WriteToLog("root node", , 2)
            mXmlNode_Root = mXml.SelectSingleNode("airlineupdates")

            oLogger.WriteToLog("add node", , 2)
            mXmlNode_add = mXmlNode_Root.SelectSingleNode("add")

            oLogger.WriteToLog("item nodes", , 2)
            mXmlNodeList_items = mXmlNode_add.SelectNodes("item")
            For Each mXmlNode_item In mXmlNodeList_items
                mXmlNode_name = mXmlNode_item.SelectSingleNode("name")
                mXmlNode_url = mXmlNode_item.SelectSingleNode("url")
                mXmlNode_icon = mXmlNode_item.SelectSingleNode("icon")

                sTmp = mXmlNode_name.InnerText & "|" & mXmlNode_url.InnerText & "|" & mXmlNode_icon.InnerText

                oLogger.WriteToLog(sTmp, , 3)
                gArr_Add.Add(sTmp)
            Next
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try
    End Sub

    Private Function UpdateiBAHNJSConfig() As Boolean
        Dim lines As New List(Of String)
        Dim bUpdated As Boolean = False

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\default\Scripts\ibahn.js"

        oLogger.WriteToLog("updating iBAHN.js", , 0)

        oLogger.WriteToLog("looking for '" & sFile & "'", , 1)

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLog("not found!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        End If

        BackupFile(New IO.FileInfo(sFile))

        oLogger.WriteToLog("ok", , 2)


        If gArr_Delete.Count > 0 Then
            oLogger.WriteToLog("deleting stuff", , 1)

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String, newline As String, url_replacement() As String
                    Dim tmpName As String, tmpLine As String, tmpUrl As String, bYes As Boolean

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        tmpLine = Regex.Replace(line, cRegExp_Airline_Link, "$1%%%$3")
                        tmpName = Regex.Replace(line, cRegExp_Airline_Name, "$2")

                        tmpLine = Regex.Replace(line, cRegExp_Airline_Link, "$1%%%$3")
                        tmpUrl = Regex.Replace(line, cRegExp_Airline_Link, "$2")

                        If tmpName <> "" Then
                            bYes = False
                            For Each replacement_string As String In gArr_Delete
                                If tmpName = replacement_string Then
                                    oLogger.WriteToLog("removing '" & tmpName & "'", , 2)
                                    newline = tmpLine.Replace("%%%", "")

                                    line = newline

                                    bUpdated = True
                                    Exit For
                                End If
                            Next
                            If bYes Then
                                gArr_Delete.Remove(tmpName)
                            End If
                        End If

                        lines.Add(line)
                    End While
                End Using
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", , 2)
                oLogger.WriteToLog(ex.Message, , 3)

                Return False
            End Try
        Else
            oLogger.WriteToLog("nothing to delete", , 1)
        End If


        If gArr_Update.Count > 0 Then
            oLogger.WriteToLog("updating stuff", , 1)

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String, newline As String, url_replacement() As String
                    Dim tmpName As String, tmpCurrentName As String = "ThisNameDoesNotExistHopeFully"
                    Dim tmpLine As String, tmpUrl As String, bSkip As Boolean

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If Not Regex.IsMatch(line, cRegExp_Airline) Then
                            'failsafe
                            bSkip = False
                        End If

                        tmpLine = Regex.Replace(line, cRegExp_Airline_Link, "$1%%%$3")
                        tmpName = Regex.Replace(line, cRegExp_Airline_Name, "$2")

                        If tmpName <> "" And tmpName <> tmpCurrentName Then
                            'we found an airline section
                            bSkip = False
                            tmpCurrentName = tmpName
                        End If


                        If tmpCurrentName <> "" Then
                            For Each replacement_string As String In gArr_Update
                                url_replacement = Split(replacement_string, "|", 3)

                                If tmpCurrentName = url_replacement(0) Then
                                    bSkip = True
                                    oLogger.WriteToLog("updating '" & tmpCurrentName & "'", , 2)
                                    newline = tmpLine.Replace("%%%", "")

                                    line = newline

                                    bUpdated = True
                                End If
                            Next
                            If bYes Then
                                gArr_Delete.Remove(tmpName)
                            End If
                        End If

                        If Not bSkip Then
                            lines.Add(line)
                        End If
                    End While
                End Using
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", , 2)
                oLogger.WriteToLog(ex.Message, , 3)

                Return False
            End Try
        Else
            oLogger.WriteToLog("nothing to delete", , 1)
        End If



        If bUpdated Then
            oLogger.WriteToLog("writing '" & sFile & "'", , 1)

            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using

            oLogger.WriteToLog("ok", , 2)
        Else
            oLogger.WriteToLog("leaving '" & sFile & "' alone", , 1)
        End If

        Return True

          
    End Function

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNode_StartPageConfig As Xml.XmlNode
            Dim mXmlNode_StartPage As Xml.XmlNode

            Dim mXmlNode_SurfArea As Xml.XmlNode
            Dim mXmlNode_SurfAreaMode As Xml.XmlNode
            Dim mXmlNode_SurfAreaUrls As Xml.XmlNodeList
            Dim mXmlNode_SurfAreaUrl As Xml.XmlNode

            Dim mXmlNode_Plugins As Xml.XmlNodeList
            Dim mXmlNode_Plugin As Xml.XmlNode
            Dim mXmlNode_SiteCash As Xml.XmlNode, bSiteCashFound As Boolean = False
            Dim mXmlNode_UrlZones As Xml.XmlNodeList
            Dim mXmlNode_UrlZone As Xml.XmlNode
            Dim mXmlNode_AirlinesZone As Xml.XmlNode, bAirlinesZoneFound As Boolean = False
            Dim mXmlNode_Urls As Xml.XmlNodeList
            Dim mXmlNode_Url As Xml.XmlNode

            Dim bAirlinePC As Boolean
            Dim pattern_replacement() As String

            bAirlinePC = False

            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            'Checking start page
            oLogger.WriteToLog("checking LobbyPC type", , 1)
            mXmlNode_StartPageConfig = mXmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)
            mXmlNode_StartPage = mXmlNode_StartPageConfig.SelectSingleNode("sk:startpage", ns)
            If mXmlNode_StartPage.InnerText.Contains("Airline") Then
                oLogger.WriteToLog("AirlinePC", , 2)

                bAirlinePC = True
            Else
                oLogger.WriteToLog("normal", , 2)
            End If

            If bAirlinePC Then
                UpdateAirlinePCConfig()

                oLogger.WriteToLog("update SurfArea", , 1)

                mXmlNode_SurfArea = mXmlNode_Root.SelectSingleNode("sk:surfarea", ns)
                mXmlNode_SurfAreaMode = mXmlNode_SurfArea.SelectSingleNode("sk:mode", ns)
                mXmlNode_SurfAreaMode.InnerText = "1"

                mConfigChanged = True

                oLogger.WriteToLog("Deny access to other websites", , 2)

                mXmlNode_SurfAreaUrls = mXmlNode_SurfArea.SelectNodes("sk:url", ns)
                For Each mXmlNode_SurfAreaUrl In mXmlNode_SurfAreaUrls
                    If mXmlNode_SurfAreaUrl.InnerText.Contains("http") Then
                        For Each pattern As String In gArr_Patterns
                            pattern_replacement = Split(pattern, "|", 2)

                            If mXmlNode_SurfAreaUrl.InnerText.Equals(pattern_replacement(0)) Then
                                oLogger.WriteToLog("updating '" & pattern_replacement(0) & "' to '" & pattern_replacement(1) & "'", , 2)
                                mXmlNode_SurfAreaUrl.InnerText = pattern_replacement(1)

                                mConfigChanged = True
                            End If
                        Next
                    End If
                Next
            Else
                UpdateiBAHNJSConfig()

                oLogger.WriteToLog("update SurfArea", , 1)

                mXmlNode_SurfArea = mXmlNode_Root.SelectSingleNode("sk:surfarea", ns)
                mXmlNode_SurfAreaMode = mXmlNode_SurfArea.SelectSingleNode("sk:mode", ns)

                If mXmlNode_SurfAreaMode.InnerText = "1" Then
                    mXmlNode_SurfAreaMode.InnerText = "0"
                    oLogger.WriteToLog("Allow access to other websites", , 2)
                    mConfigChanged = True
                Else
                    oLogger.WriteToLog("not needed", , 2)
                End If

                mXmlNode_SurfAreaUrls = mXmlNode_SurfArea.SelectNodes("sk:url", ns)
                For Each mXmlNode_SurfAreaUrl In mXmlNode_SurfAreaUrls
                    If mXmlNode_SurfAreaUrl.InnerText.Contains("http") Then
                        If mXmlNode_SurfAreaUrl.InnerText = "http://*.*" Then
                            oLogger.WriteToLog("removing sledgehammer #1 (http://*.*)", , 2)
                            mXmlNode_SurfArea.RemoveChild(mXmlNode_SurfAreaUrl)
                            mConfigChanged = True
                        End If

                        If mXmlNode_SurfAreaUrl.InnerText = "https://*.*" Then
                            oLogger.WriteToLog("removing sledgehammer #2 (https://*.*)", , 2)
                            mXmlNode_SurfArea.RemoveChild(mXmlNode_SurfAreaUrl)
                            mConfigChanged = True
                        End If
                    End If
                Next


                oLogger.WriteToLog("update UrlZone", , 1)

                mXmlNode_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", ns)
                For Each mXmlNode_Plugin In mXmlNode_Plugins
                    If mXmlNode_Plugin.Attributes.GetNamedItem("name").Value = "SiteCash" Then
                        mXmlNode_SiteCash = mXmlNode_Plugin
                        bSiteCashFound = True

                        Exit For
                    End If
                Next

                If Not bSiteCashFound Then
                    Throw New Exception("SiteCash not found!")
                End If


                mXmlNode_UrlZones = mXmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
                For Each mXmlNode_UrlZone In mXmlNode_UrlZones
                    If mXmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = "FreeAirlines" Then
                        mXmlNode_AirlinesZone = mXmlNode_UrlZone
                        bAirlinesZoneFound = True
                    End If
                Next

                If Not bAirlinesZoneFound Then
                    Throw New Exception("Airlines url-zone not found!")
                End If


                mXmlNode_Urls = mXmlNode_AirlinesZone.SelectNodes("sk:url", ns)
                For Each mXmlNode_Url In mXmlNode_Urls
                    For Each pattern As String In gArr_Patterns
                        pattern_replacement = Split(pattern, "|", 2)

                        If mXmlNode_Url.InnerText.Equals(pattern_replacement(0)) Then
                            oLogger.WriteToLog("updating '" & pattern_replacement(0) & "' to '" & pattern_replacement(1) & "'", , 2)
                            mXmlNode_Url.InnerText = pattern_replacement(1)

                            mConfigChanged = True
                        End If
                    Next
                Next
            End If

            If mConfigChanged Then
                BackupFile(New IO.FileInfo(mSkCfgFile))

                oLogger.WriteToLog("saving", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("not saving, nothing was changed", , 1)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        oLogger.WriteToLog("creating backup of '" & oFile.FullName & "'", , 0)

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLog("backup file: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLog("done", , 1)
                bRet = True
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

