Imports System.Text.RegularExpressions

Module ModMain
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        Dim dir As New IO.DirectoryInfo(g_BackupDirectory)
        Dim subdir As IO.DirectoryInfo, lastdir As New IO.DirectoryInfo("c:\temp"), bFound As Boolean = False
        Dim lastdate As Date, tmpDate As Date

        Dim reDate As Regex = New Regex("^([0-9]{8})_([0-9]{6})$")
        Dim reMatch As Match


        For Each subdir In dir.GetDirectories
            reMatch = reDate.Match(subdir.Name)

            If reMatch.Success Then
                tmpDate = subdir.CreationTime
                If tmpDate > lastdate Then
                    lastdate = tmpDate
                    lastdir = subdir
                    bFound = True
                End If
            End If
        Next

        If bFound Then
            RecursiveDirectoryCopy(lastdir.FullName, "c:\")

            IO.Directory.Move(lastdir.FullName, g_BackupDirectory & "\_restored_" & lastdir.Name)
        End If
    End Sub

    Private Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String)
        Dim sDir As String
        Dim dDirInfo As IO.DirectoryInfo
        Dim sDirInfo As IO.DirectoryInfo
        Dim sFile As String
        Dim sFileInfo As IO.FileInfo
        Dim dFileInfo As IO.FileInfo

        ' Add trailing separators to the supplied paths if they don't exist.
        If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            sourceDir &= System.IO.Path.DirectorySeparatorChar
        End If
        If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            destDir &= System.IO.Path.DirectorySeparatorChar
        End If

        'If destination directory does not exist, create it.
        dDirInfo = New System.IO.DirectoryInfo(destDir)
        If dDirInfo.Exists = False Then dDirInfo.Create()
        dDirInfo = Nothing


        ' Get a list of directories from the current parent.
        For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
            sDirInfo = New System.IO.DirectoryInfo(sDir)
            dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
            ' Create the directory if it does not exist.
            If dDirInfo.Exists = False Then dDirInfo.Create()
            ' Since we are in recursive mode, copy the children also
            RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName)
            sDirInfo = Nothing
            dDirInfo = Nothing
        Next

        ' Get the files from the current parent.
        For Each sFile In System.IO.Directory.GetFiles(sourceDir)
            sFileInfo = New System.IO.FileInfo(sFile)
            dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

            oLogger.WriteToLogRelative("copying", , 1)
            oLogger.WriteToLogRelative("source: " & sFileInfo.FullName, , 2)
            oLogger.WriteToLogRelative("dest  : " & dFileInfo.FullName, , 2)

            'If File does exists, backup.
            If dFileInfo.Exists Then
                oLogger.WriteToLogRelative("already exists, will be overwritten!", Logger.MESSAGE_TYPE.LOG_WARNING, 3)

                If dFileInfo.IsReadOnly Then
                    oLogger.WriteToLogRelative("file is read-only, fixing...", , 4)
                    dFileInfo.IsReadOnly = False
                End If
            End If

            Try
                sFileInfo.CopyTo(dFileInfo.FullName, True)
                If IO.File.Exists(dFileInfo.FullName) Then
                    oLogger.WriteToLogRelative("ok", , 2)
                Else
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End If
            Catch ex As Exception
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            End Try

            sFileInfo = Nothing
            dFileInfo = Nothing
        Next
    End Sub

End Module
