Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        ' Dim sTmp As String, iTmp As Integer
        ' For Each arg As String In Environment.GetCommandLineArgs()

        ' Next

        ' oLogger.WriteToLog("settings", , 0)
        ' oLogger.WriteToLog("Max Mem usage: " & g_MaxMemUsage, , 0)
        ' oLogger.WriteToLog("Max Pagefile usage: " & g_MaxPagefile, , 0)

        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = "WTF!"
        If RegistryKeyExists(Microsoft.Win32.RegistryHive.LocalMachine, REGKEY__64) Then
            oLogger.WriteToLog("registry: HKEY_LOCAL_MACHINE\" & REGKEY__64 & "!LastCfg ", , 1)
            mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__64, "LastCfg", "")
        ElseIf RegistryKeyExists(Microsoft.Win32.RegistryHive.LocalMachine, REGKEY__32) Then
            oLogger.WriteToLog("registry: HKEY_LOCAL_MACHINE\" & REGKEY__32 & "!LastCfg ", , 1)
            mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__32, "LastCfg", "")
        Else
            oLogger.WriteToLog("registry: ????????????????????? ", , 1)
        End If
        oLogger.WriteToLog("found: " & mSkCfgFile, , 2)


        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("File does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Else
            oLogger.WriteToLog("ok", , 2)

            SkCfg_Update()
        End If

        Bye()
    End Sub

    Private Function RegistryKeyExists(ByVal hive As Microsoft.Win32.RegistryHive, ByVal key As String) As Boolean
        Dim tmpHive As Microsoft.Win32.RegistryKey
        Dim tmpRegKey As Microsoft.Win32.RegistryKey


        Select Case hive
            Case Microsoft.Win32.RegistryHive.ClassesRoot
                tmpHive = Microsoft.Win32.Registry.ClassesRoot
            Case Microsoft.Win32.RegistryHive.CurrentConfig
                tmpHive = Microsoft.Win32.Registry.CurrentConfig
            Case Microsoft.Win32.RegistryHive.CurrentUser
                tmpHive = Microsoft.Win32.Registry.CurrentUser
            Case Microsoft.Win32.RegistryHive.DynData
                tmpHive = Microsoft.Win32.Registry.DynData
            Case Microsoft.Win32.RegistryHive.LocalMachine
                tmpHive = Microsoft.Win32.Registry.LocalMachine
            Case Microsoft.Win32.RegistryHive.PerformanceData
                tmpHive = Microsoft.Win32.Registry.PerformanceData
            Case Microsoft.Win32.RegistryHive.Users
                tmpHive = Microsoft.Win32.Registry.Users
            Case Else
                Return False
        End Select

        tmpRegKey = tmpHive.OpenSubKey(key, False)
        If tmpRegKey Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_SurfArea As Xml.XmlNode
            Dim mXmlNode_UrlClone As Xml.XmlNode
            Dim mXmlNode_Urls As Xml.XmlNodeList
            Dim mXmlNode_Url As Xml.XmlNode
            Dim mXmlNode_New As Xml.XmlNode


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("surfarea node", , 1)
            mXmlNode_SurfArea = mXmlNode_Root.SelectSingleNode("sk:surfarea", ns)
            If Not mXmlNode_SurfArea Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_SurfArea Is Nothing")
            End If


            oLogger.WriteToLog("clone url node", , 1)
            mXmlNode_UrlClone = mXmlNode_SurfArea.SelectSingleNode("sk:url", ns).CloneNode(True)
            oLogger.WriteToLog("ok", , 2)


            oLogger.WriteToLog("remove all url nodes", , 1)
            mXmlNode_Urls = mXmlNode_SurfArea.SelectNodes("sk:url", ns)
            For Each mXmlNode_Url In mXmlNode_Urls
                mXmlNode_SurfArea.RemoveChild(mXmlNode_Url)
                oLogger.WriteToLog("ok", , 2)
            Next


            ' Defaults for Surf Area for SK 7.8
            '   <surfarea>
            '		<url allowed="false">file://$(WinDir)\*.ht*</url>
            '		<url allowed="false">file://$(UserProfile)\*.ht*</url>
            '		<url allowed="false">file://$(AllUsersProfile)\*Microsoft*\*.ht*</url>
            '		<url allowed="false">file://$(SiteKioskPath)\*</url>
            '		<url allowed="true">file://$(SiteKioskPath)\html\*</url>
            '		<url allowed="true">file://$(SiteKioskPath)\skins\public\*</url>
            '	</surfarea>

            oLogger.WriteToLog("add default url nodes", , 1)

            mXmlNode_New = mXmlNode_UrlClone.CloneNode(True)
            mXmlNode_New.Attributes.GetNamedItem("allowed").InnerText = "false"
            mXmlNode_New.InnerText = "file://$(WinDir)\*.ht*"
            mXmlNode_SurfArea.AppendChild(mXmlNode_New)
            oLogger.WriteToLog("ok", , 2)

            mXmlNode_New = mXmlNode_UrlClone.CloneNode(True)
            mXmlNode_New.Attributes.GetNamedItem("allowed").InnerText = "false"
            mXmlNode_New.InnerText = "file://$(UserProfile)\*.ht*"
            mXmlNode_SurfArea.AppendChild(mXmlNode_New)
            oLogger.WriteToLog("ok", , 2)

            mXmlNode_New = mXmlNode_UrlClone.CloneNode(True)
            mXmlNode_New.Attributes.GetNamedItem("allowed").InnerText = "false"
            mXmlNode_New.InnerText = "file://$(AllUsersProfile)\*Microsoft*\*.ht*"
            mXmlNode_SurfArea.AppendChild(mXmlNode_New)
            oLogger.WriteToLog("ok", , 2)

            mXmlNode_New = mXmlNode_UrlClone.CloneNode(True)
            mXmlNode_New.Attributes.GetNamedItem("allowed").InnerText = "false"
            mXmlNode_New.InnerText = "file://$(SiteKioskPath)\*"
            mXmlNode_SurfArea.AppendChild(mXmlNode_New)
            oLogger.WriteToLog("ok", , 2)

            mXmlNode_New = mXmlNode_UrlClone.CloneNode(True)
            mXmlNode_New.Attributes.GetNamedItem("allowed").InnerText = "true"
            mXmlNode_New.InnerText = "file://$(SiteKioskPath)\html\*"
            mXmlNode_SurfArea.AppendChild(mXmlNode_New)
            oLogger.WriteToLog("ok", , 2)

            mXmlNode_New = mXmlNode_UrlClone.CloneNode(True)
            mXmlNode_New.Attributes.GetNamedItem("allowed").InnerText = "true"
            mXmlNode_New.InnerText = "file://$(SiteKioskPath)\skins\public\*"
            mXmlNode_SurfArea.AppendChild(mXmlNode_New)
            oLogger.WriteToLog("ok", , 2)


            oLogger.WriteToLog("saving", , 0)
            oLogger.WriteToLog("creating backup of '" & mSkCfgFile & "'", , 1)
            BackupFile(New IO.FileInfo(mSkCfgFile))

            oLogger.WriteToLog("saving existing file", , 1)
            mSkCfgXml.Save(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            oLogger.WriteToLog("restarting SiteKiosk", , 1)
            KillSiteKiosk()

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLogRelative("writing: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLogRelative("done", , 2)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

