Module modConstants
    Public ReadOnly DEFAULT_MaxMemUsage As String = "4072"
    Public ReadOnly DEFAULT_MaxPagefile As String = "90"

    Public ReadOnly REGKEY__32 As String = "SOFTWARE\PROVISIO\SiteKiosk"
    Public ReadOnly REGKEY__64 As String = "SOFTWARE\Wow6432Node\PROVISIO\SiteKiosk"
End Module
