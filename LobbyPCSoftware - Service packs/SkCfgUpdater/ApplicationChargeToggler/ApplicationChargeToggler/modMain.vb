Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        Dim sTmp As String, iTmp As Integer
        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals("--disable") Then
                g_ApplicationPricingEnabled = False
            End If
            If arg.Equals("--enable") Then
                g_ApplicationPricingEnabled = True
            End If
            If InStr(arg, "--pricing=") > 0 Then
                sTmp = arg.Replace("--pricing=", "")
                If Integer.TryParse(sTmp, iTmp) Then
                    If iTmp < 1 Or iTmp > 1000 Then
                        sTmp = "0"
                        g_ApplicationPricingEnabled = False
                    Else
                        g_ApplicationPricingEnabled = True
                        g_ApplicationMultiplier = ""
                    End If
                    g_ApplicationPrice = sTmp
                Else
                    g_ApplicationPricingEnabled = False
                End If
            End If
            If InStr(arg, "--multiplier=") > 0 Then
                sTmp = arg.Replace("--multiplier=", "")
                If Integer.TryParse(sTmp, iTmp) Then
                    If iTmp < 1 Or iTmp > 100 Then
                        sTmp = "0"
                        g_ApplicationPricingEnabled = False
                    Else
                        g_ApplicationPricingEnabled = True
                        g_ApplicationPrice = ""
                    End If
                    g_ApplicationMultiplier = sTmp
                Else
                    g_ApplicationPricingEnabled = False
                End If
            End If
        Next

        oLogger.WriteToLog("settings", , 0)
        If g_ApplicationPricingEnabled Then
            oLogger.WriteToLog("Application pricing: On", , 1)
            If g_ApplicationPrice <> "" Then
                oLogger.WriteToLog("Application price: " & g_ApplicationPrice, , 1)
            End If
            If g_ApplicationMultiplier <> "" Then
                oLogger.WriteToLog("Application multiplier: " & g_ApplicationMultiplier, , 1)
            End If
        Else
            oLogger.WriteToLog("Application pricing: Off", , 1)
        End If



        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = "WTF!"
        If RegistryKeyExists(Microsoft.Win32.RegistryHive.LocalMachine, REGKEY__64) Then
            oLogger.WriteToLog("registry: HKEY_LOCAL_MACHINE\" & REGKEY__64 & "!LastCfg ", , 1)
            mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__64, "LastCfg", "")
        ElseIf RegistryKeyExists(Microsoft.Win32.RegistryHive.LocalMachine, REGKEY__32) Then
            oLogger.WriteToLog("registry: HKEY_LOCAL_MACHINE\" & REGKEY__32 & "!LastCfg ", , 1)
            mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & REGKEY__32, "LastCfg", "")
        Else
            oLogger.WriteToLog("registry: ????????????????????? ", , 1)
        End If
        oLogger.WriteToLog("found: " & mSkCfgFile, , 2)


        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("File does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Else
            oLogger.WriteToLog("ok", , 2)

            SkCfg_Update()

            KillSiteKiosk()
        End If

        Bye()
    End Sub

    Private Function RegistryKeyExists(ByVal hive As Microsoft.Win32.RegistryHive, ByVal key As String) As Boolean
        Dim tmpHive As Microsoft.Win32.RegistryKey
        Dim tmpRegKey As Microsoft.Win32.RegistryKey


        Select Case hive
            Case Microsoft.Win32.RegistryHive.ClassesRoot
                tmpHive = Microsoft.Win32.Registry.ClassesRoot
            Case Microsoft.Win32.RegistryHive.CurrentConfig
                tmpHive = Microsoft.Win32.Registry.CurrentConfig
            Case Microsoft.Win32.RegistryHive.CurrentUser
                tmpHive = Microsoft.Win32.Registry.CurrentUser
            Case Microsoft.Win32.RegistryHive.DynData
                tmpHive = Microsoft.Win32.Registry.DynData
            Case Microsoft.Win32.RegistryHive.LocalMachine
                tmpHive = Microsoft.Win32.Registry.LocalMachine
            Case Microsoft.Win32.RegistryHive.PerformanceData
                tmpHive = Microsoft.Win32.Registry.PerformanceData
            Case Microsoft.Win32.RegistryHive.Users
                tmpHive = Microsoft.Win32.Registry.Users
            Case Else
                Return False
        End Select

        tmpRegKey = tmpHive.OpenSubKey(key, False)
        If tmpRegKey Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNodeList_Plugins As Xml.XmlNodeList
            Dim mXmlNode_Plugin As Xml.XmlNode
            Dim mXmlNode_SiteCash As Xml.XmlNode


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If

            oLogger.WriteToLog("find SiteCash node", , 1)
            mXmlNodeList_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each mXmlNode_Plugin In mXmlNodeList_Plugins
                If mXmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    mXmlNode_SiteCash = mXmlNode_Plugin

                    Exit For
                End If
            Next

            If Not mXmlNode_SiteCash Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNode_SiteCash Is Nothing")
            End If


            Dim mXmlNode_ApplicationPrice As Xml.XmlNode
            Dim mXmlNode_ApplicationMultiplier As Xml.XmlNode

            If g_ApplicationPricingEnabled Then
                If g_ApplicationPrice <> "" Then
                    oLogger.WriteToLog("application price", , 3)
                    mXmlNode_ApplicationPrice = mXmlNode_SiteCash.SelectSingleNode("sk:applicationprice", ns)
                    mXmlNode_ApplicationPrice.Attributes.GetNamedItem("enabled").InnerText = "true"
                    mXmlNode_ApplicationPrice.InnerText = g_ApplicationPrice

                    mConfigChanged = True

                    oLogger.WriteToLog("enabled", , 4)
                ElseIf g_ApplicationMultiplier <> "" Then
                    oLogger.WriteToLog("application multiplier", , 3)
                    mXmlNode_ApplicationMultiplier = mXmlNode_SiteCash.SelectSingleNode("sk:applicationmultiplier", ns)
                    mXmlNode_ApplicationMultiplier.Attributes.GetNamedItem("enabled").InnerText = "true"
                    mXmlNode_ApplicationMultiplier.InnerText = g_ApplicationMultiplier

                    mConfigChanged = True

                    oLogger.WriteToLog("enabled", , 4)
                Else
                    'oops
                End If
            Else
                oLogger.WriteToLog("application price", , 3)
                mXmlNode_ApplicationPrice = mXmlNode_SiteCash.SelectSingleNode("sk:applicationprice", ns)
                mXmlNode_ApplicationPrice.Attributes.GetNamedItem("enabled").InnerText = "false"
                oLogger.WriteToLog("disabled", , 4)

                oLogger.WriteToLog("application multiplier", , 3)
                mXmlNode_ApplicationMultiplier = mXmlNode_SiteCash.SelectSingleNode("sk:applicationmultiplier", ns)
                mXmlNode_ApplicationMultiplier.Attributes.GetNamedItem("enabled").InnerText = "false"
                oLogger.WriteToLog("disabled", , 4)

                mConfigChanged = True
            End If


            If mConfigChanged Then
                oLogger.WriteToLog("saving", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            End If

        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try

    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLogRelative("writing: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLogRelative("done", , 2)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

