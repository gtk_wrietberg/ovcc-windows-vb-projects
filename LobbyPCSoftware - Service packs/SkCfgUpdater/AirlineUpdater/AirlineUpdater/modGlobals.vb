Module modGlobals
    Public gAirlines As clsAirline
    Public gWalledGarden As clsWalledGarden

    Public gArr_FreeUrls As New List(Of String)

    Public oLogger As Logger

    Public oCopyFiles As CopyFiles

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        'g_BackupDirectory &= "\" & Application.ProductName
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(g_BackupDirectory)

        gAirlines = New clsAirline
        gAirlines.IsOldStyleMachine = True 'Will be set to false when images are detected in ibahn.js file

        gWalledGarden = New clsWalledGarden
    End Sub
End Module
