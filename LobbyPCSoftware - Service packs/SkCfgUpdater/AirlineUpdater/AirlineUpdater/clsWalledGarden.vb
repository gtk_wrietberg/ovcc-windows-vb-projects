Public Class clsWalledGarden
    Private mChanged As Boolean
    Private mUrls As List(Of String)
    Private mExportCount As Integer

    Public Sub New()
        mChanged = False

        mUrls = New List(Of String)

        mExportCount = 0
    End Sub

    Public ReadOnly Property HasChanged() As Boolean
        Get
            Return mChanged
        End Get
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Return mUrls.Count
        End Get
    End Property

#Region "Add"
    Public Function Add(ByVal url As String) As Boolean
        Dim bRet As Boolean = False

        If url <> "" Then
            bRet = Populate(url)
            If bRet Then
                mChanged = True
            End If
        End If

        Return bRet
    End Function

    Public Function Populate(ByVal url As String) As Boolean
        If Not mUrls.Contains(url) Then
            mUrls.Add(url)
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "Edit"
    Public Sub Edit(ByVal old_url As String, ByVal new_url As String)
        If old_url <> "" And new_url <> "" Then
            For i As Integer = 0 To mUrls.Count - 1
                If mUrls.Item(i).ToLower = old_url.ToLower Then
                    If new_url <> "" And old_url <> new_url Then
                        mChanged = True
                        mUrls.Item(i) = new_url
                    End If
                End If
            Next
        End If
    End Sub
#End Region

#Region "Delete"
    Public Sub Delete(ByVal url As String)
        If url <> "" Then
            For i As Integer = 0 To mUrls.Count - 1
                If mUrls.Item(i).ToLower = url.ToLower Then
                    mChanged = True
                    mUrls.Item(i) = ""
                End If
            Next
        End If
    End Sub
#End Region

#Region "Export"
    Public Sub ExportReset()
        mExportCount = 0
    End Sub

    Public Function ExportNext(ByRef url As String) As Boolean
        If mExportCount >= mUrls.Count Then
            url = ""
            Return False
        End If

        url = mUrls.Item(mExportCount)
        mExportCount += 1

        Return True
    End Function
#End Region

    Public Overrides Function ToString() As String
        Return String.Join("\n", mUrls.ToArray())
    End Function
End Class
