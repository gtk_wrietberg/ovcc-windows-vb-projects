Module modConstants
    Public ReadOnly cRegExp_Airline As String = "(FAAwebsites\[')([a-z]+)('\].+="")(.+)("";)"

    Public ReadOnly cRegExp_Airline_Name As String = "(FAAwebsites\['name'\].+="")(.+)("";)"
    Public ReadOnly cRegExp_Airline_Link As String = "(FAAwebsites\['link'\].+="")(.+)("";)"
    Public ReadOnly cRegExp_Airline_Image As String = "(FAAwebsites\['image'\].+="")(.+)("";)"

    Public ReadOnly cTemplate_Airline_Name As String = "FAAwebsites['name'][%%NUM%%]=""%%STRING%%"";"
    Public ReadOnly cTemplate_Airline_Link As String = "FAAwebsites['link'][%%NUM%%]=""%%STRING%%"";"
    Public ReadOnly cTemplate_Airline_Image As String = "FAAwebsites['image'][%%NUM%%]=""%%STRING%%"";"
    Public ReadOnly cTemplate_Airline_List_Replacement As String = "%%AIRLINES_GO_HERE%%"


    Public ReadOnly cXML_PATCH__AIRLINES As String = "AirlineUpdates.airlines.xml"
    Public ReadOnly cXML_PATCH__WALLEDGARDEN As String = "AirlineUpdates.walledgarden.xml"

    Public ReadOnly cPATH__AIRLINE_IMAGES As String = "sitekiosk\Skins\default\Systemdialog\AIRLINES_files\images\airlines\"
End Module
