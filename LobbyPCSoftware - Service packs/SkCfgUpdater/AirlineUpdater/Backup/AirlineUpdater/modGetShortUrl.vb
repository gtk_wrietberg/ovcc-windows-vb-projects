Module modGetShortUrl
    Public Function GetShortUrl_http(ByVal sUrl As String) As String
        Return _GetShortUrl("http", sUrl)
    End Function

    Public Function GetShortUrl_https(ByVal sUrl As String) As String
        Return _GetShortUrl("https", sUrl)
    End Function

    Private Function _GetShortUrl(ByVal sType As String, ByVal sFullUrl As String) As String
        Try
            Dim sTmpUrl As String
            Dim aTmpUrl() As String

            sTmpUrl = sFullUrl

            sTmpUrl = sTmpUrl.Replace("http", "__protocol__")
            sTmpUrl = sTmpUrl.Replace("https", "__protocol__")

            sTmpUrl = sTmpUrl.Replace("//", "<<")

            aTmpUrl = sTmpUrl.Split("/")
            sTmpUrl = aTmpUrl(LBound(aTmpUrl))
            sTmpUrl = sTmpUrl.Replace("<<", "//")
            sTmpUrl = sTmpUrl.Replace("/www.", "/*.")

            Select Case sType
                Case "http", "https"
                    sTmpUrl = sTmpUrl.Replace("__protocol__", sType)
                Case Else
                    sTmpUrl = ""
            End Select

            Return sTmpUrl
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Module
