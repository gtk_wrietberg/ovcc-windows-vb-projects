Public Class clsAirline
    Private mYeOldeStyle As Boolean
    Private mChanged As Boolean
    Private mDuplicateDetected As Boolean

    Private mAirlineSectionFirstLine As Integer
    Private mAirlineSectionLastLine As Integer

    Private mNames As List(Of String)
    Private mUrls As List(Of String)
    Private mImages As List(Of String)

    Public Sub New()
        mYeOldeStyle = False
        mChanged = False
        mDuplicateDetected = False

        mAirlineSectionFirstLine = 0
        mAirlineSectionLastLine = 0

        mNames = New List(Of String)
        mUrls = New List(Of String)
        mImages = New List(Of String)
    End Sub

    Public Property IsOldStyleMachine() As Boolean
        Get
            Return mYeOldeStyle
        End Get
        Set(ByVal value As Boolean)
            mYeOldeStyle = value
        End Set
    End Property

    Public ReadOnly Property IsChanged() As Boolean
        Get
            Return mChanged
        End Get
    End Property

    Public ReadOnly Property IsDuplicateDetected() As Boolean
        Get
            Return mDuplicateDetected
        End Get
    End Property

    Public Property AirlineSectionFirstLine() As Integer
        Get
            Return mAirlineSectionFirstLine
        End Get
        Set(ByVal value As Integer)
            mAirlineSectionFirstLine = value
        End Set
    End Property

    Public Property AirlineSectionLastLine() As Integer
        Get
            Return mAirlineSectionLastLine
        End Get
        Set(ByVal value As Integer)
            mAirlineSectionLastLine = value
        End Set
    End Property

    Public ReadOnly Property Count() As Integer
        Get
            Return mNames.Count
        End Get
    End Property

#Region "Add"
    Public Function Add(ByVal name As String, ByVal url As String, ByVal image As String) As Boolean
        Dim bRet As Boolean = False

        If name <> "" And url <> "" And image <> "" Then
            bRet = _Populate(name, url, image, False)
            If bRet Then
                mChanged = True
            End If
        End If

        Return bRet
    End Function

    Public Function Populate(ByVal name As String, ByVal url As String, ByVal image As String) As Boolean
        Return _Populate(name, url, image, True)
    End Function

    Private Function _Populate(ByVal name As String, ByVal url As String, ByVal image As String, ByVal bForcedChangedWhenDuplicate As Boolean) As Boolean
        If Not mNames.Contains(name) Then
            mNames.Add(name)
            mUrls.Add(url)
            mImages.Add(image)
            Return True
        Else
            If bForcedChangedWhenDuplicate Then
                mChanged = True
                mDuplicateDetected = True
            End If
            Return False
        End If
    End Function
#End Region

#Region "Edit"
    Public Sub Edit(ByVal ByType As String, ByVal name As String, ByVal url As String, ByVal image As String)
        Select Case ByType
            Case "name"
                EditByName(name, url, image)
            Case "url"
                EditByUrl(name, url, image)
            Case "image"
                EditByImage(name, url, image)
            Case Else
                'Doh...
        End Select
    End Sub

#Region "by name"
    Private Sub EditByName(ByVal name As String, ByVal url As String, ByVal image As String)
        Dim iEditType As Integer = 0, bMatch As Boolean

        If name.StartsWith("*") And name.EndsWith("*") Then
            iEditType = 1
            name = name.Replace("*", "")
        End If
        If name.StartsWith("*") Then
            iEditType = 2
            name = name.Replace("*", "")
        End If
        If name.EndsWith("*") Then
            iEditType = 3
            name = name.Replace("*", "")
        End If

        For i As Integer = 0 To mNames.Count - 1
            bMatch = False

            Select Case iEditType
                Case 0
                    bMatch = (mNames.Item(i).ToLower = name.ToLower)
                Case 1
                    bMatch = (mNames.Item(i).ToLower.Contains(name.ToLower))
                Case 2
                    bMatch = (mNames.Item(i).ToLower.EndsWith(name.ToLower))
                Case 3
                    bMatch = (mNames.Item(i).ToLower.StartsWith(name.ToLower))
                Case Else
                    'oops
            End Select
            If bMatch Then
                If url <> "" And mUrls.Item(i) <> url Then
                    mChanged = True
                    mUrls.Item(i) = url
                End If
                If image <> "" And mImages.Item(i) <> image Then
                    mChanged = True
                    mImages.Item(i) = image
                End If
            End If
        Next
    End Sub
#End Region

#Region "by url"
    Private Sub EditByUrl(ByVal name As String, ByVal url As String, ByVal image As String)
        Dim iEditType As Integer = 0, bMatch As Boolean

        If url.StartsWith("*") And url.EndsWith("*") Then
            iEditType = 1
            url = url.Replace("*", "")
        End If
        If url.StartsWith("*") Then
            iEditType = 2
            url = url.Replace("*", "")
        End If
        If url.EndsWith("*") Then
            iEditType = 3
            url = url.Replace("*", "")
        End If

        For i As Integer = 0 To mNames.Count - 1
            bMatch = False

            Select Case iEditType
                Case 0
                    bMatch = (mUrls.Item(i).ToLower = url.ToLower)
                Case 1
                    bMatch = (mUrls.Item(i).ToLower.Contains(url.ToLower))
                Case 2
                    bMatch = (mUrls.Item(i).ToLower.EndsWith(url.ToLower))
                Case 3
                    bMatch = (mUrls.Item(i).ToLower.StartsWith(url.ToLower))
                Case Else
                    'oops
            End Select

            If bMatch Then
                If name <> "" And mNames.Item(i) <> name Then
                    mChanged = True
                    mNames.Item(i) = name
                End If
                If image <> "" And mImages.Item(i) <> image Then
                    mChanged = True
                    mImages.Item(i) = image
                End If
            End If
        Next
    End Sub
#End Region

#Region "by image"
    Private Sub EditByImage(ByVal name As String, ByVal url As String, ByVal image As String)
        Dim iEditType As Integer = 0, bMatch As Boolean

        If image.StartsWith("*") And image.EndsWith("*") Then
            iEditType = 1
            image = image.Replace("*", "")
        End If
        If image.StartsWith("*") Then
            iEditType = 2
            image = image.Replace("*", "")
        End If
        If image.EndsWith("*") Then
            iEditType = 3
            image = image.Replace("*", "")
        End If

        For i As Integer = 0 To mNames.Count - 1
            bMatch = False

            Select Case iEditType
                Case 0
                    bMatch = (mImages.Item(i).ToLower = image.ToLower)
                Case 1
                    bMatch = (mImages.Item(i).ToLower.Contains(image.ToLower))
                Case 2
                    bMatch = (mImages.Item(i).ToLower.EndsWith(image.ToLower))
                Case 3
                    bMatch = (mImages.Item(i).ToLower.StartsWith(image.ToLower))
                Case Else
                    'oops
            End Select

            If bMatch Then
                If name <> "" And mNames.Item(i) <> name Then
                    mChanged = True
                    mNames.Item(i) = name
                End If
                If url <> "" And mUrls.Item(i) <> url Then
                    mChanged = True
                    mUrls.Item(i) = url
                End If
            End If
        Next
    End Sub
#End Region
#End Region

#Region "Delete"
    Public Sub Delete(ByVal name As String, ByVal url As String, ByVal image As String)
        If Not name.Equals("") Then
            DeleteByName(name)
        ElseIf Not url.Equals("") Then
            DeleteByName(url)
        ElseIf Not image.Equals("") Then
            DeleteByName(image)
        Else
            'Oops
        End If
    End Sub

#Region "by name"
    Private Sub DeleteByName(ByVal name As String)
        Dim iEditType As Integer = 0, bMatch As Boolean

        If name.StartsWith("*") And name.EndsWith("*") Then
            iEditType = 1
            name = name.Replace("*", "")
        End If
        If name.StartsWith("*") Then
            iEditType = 2
            name = name.Replace("*", "")
        End If
        If name.EndsWith("*") Then
            iEditType = 3
            name = name.Replace("*", "")
        End If

        For i As Integer = 0 To mNames.Count - 1
            bMatch = False

            Select Case iEditType
                Case 0
                    bMatch = (mNames.Item(i).ToLower = name.ToLower)
                Case 1
                    bMatch = (mNames.Item(i).ToLower.Contains(name.ToLower))
                Case 2
                    bMatch = (mNames.Item(i).ToLower.EndsWith(name.ToLower))
                Case 3
                    bMatch = (mNames.Item(i).ToLower.StartsWith(name.ToLower))
                Case Else
                    'oops
            End Select
            If bMatch Then
                mChanged = True

                mNames.Item(i) = ""
                mUrls.Item(i) = ""
                mImages.Item(i) = ""
            End If
        Next
    End Sub
#End Region

#Region "by url"
    Private Sub DeleteByUrl(ByVal url As String)
        Dim iEditType As Integer = 0, bMatch As Boolean

        If url.StartsWith("*") And url.EndsWith("*") Then
            iEditType = 1
            url = url.Replace("*", "")
        End If
        If url.StartsWith("*") Then
            iEditType = 2
            url = url.Replace("*", "")
        End If
        If url.EndsWith("*") Then
            iEditType = 3
            url = url.Replace("*", "")
        End If

        For i As Integer = 0 To mNames.Count - 1
            bMatch = False

            Select Case iEditType
                Case 0
                    bMatch = (mUrls.Item(i).ToLower = url.ToLower)
                Case 1
                    bMatch = (mUrls.Item(i).ToLower.Contains(url.ToLower))
                Case 2
                    bMatch = (mUrls.Item(i).ToLower.EndsWith(url.ToLower))
                Case 3
                    bMatch = (mUrls.Item(i).ToLower.StartsWith(url.ToLower))
                Case Else
                    'oops
            End Select
            If bMatch Then
                mChanged = True

                mNames.Item(i) = ""
                mUrls.Item(i) = ""
                mImages.Item(i) = ""
            End If
        Next
    End Sub
#End Region

#Region "by image"
    Private Sub DeleteByImage(ByVal image As String)
        Dim iEditType As Integer = 0, bMatch As Boolean

        If image.StartsWith("*") And image.EndsWith("*") Then
            iEditType = 1
            image = image.Replace("*", "")
        End If
        If image.StartsWith("*") Then
            iEditType = 2
            image = image.Replace("*", "")
        End If
        If image.EndsWith("*") Then
            iEditType = 3
            image = image.Replace("*", "")
        End If

        For i As Integer = 0 To mNames.Count - 1
            bMatch = False

            Select Case iEditType
                Case 0
                    bMatch = (mImages.Item(i).ToLower = image.ToLower)
                Case 1
                    bMatch = (mImages.Item(i).ToLower.Contains(image.ToLower))
                Case 2
                    bMatch = (mImages.Item(i).ToLower.EndsWith(image.ToLower))
                Case 3
                    bMatch = (mImages.Item(i).ToLower.StartsWith(image.ToLower))
                Case Else
                    'oops
            End Select

            If bMatch Then
                mChanged = True

                mNames.Item(i) = ""
                mUrls.Item(i) = ""
                mImages.Item(i) = ""
            End If
        Next
    End Sub
#End Region
#End Region

#Region "Export"
    Public Function ExportJavascript() As List(Of String)
        If mYeOldeStyle Then
            Return _ExportAndSkipImagesForOlderMachines()
        Else
            Return _ExportJavascript()
        End If
    End Function

    Private Function _ExportJavascript() As List(Of String)
        Dim lines As New List(Of String)
        Dim iCount As Integer = 0
        Dim sName As String, sUrl As String, sImage As String

        For i As Integer = 0 To mNames.Count - 1
            If mNames.Item(i) <> "" And mUrls.Item(i) <> "" Then
                sName = cTemplate_Airline_Name.Replace("%%NUM%%", iCount.ToString).Replace("%%STRING%%", mNames.Item(i))
                sUrl = cTemplate_Airline_Link.Replace("%%NUM%%", iCount.ToString).Replace("%%STRING%%", mUrls.Item(i))
                sImage = cTemplate_Airline_Image.Replace("%%NUM%%", iCount.ToString).Replace("%%STRING%%", mImages.Item(i))

                lines.Add(sName)
                lines.Add(sUrl)
                lines.Add(sImage)

                lines.Add(" ")

                iCount += 1
            End If
        Next

        Return lines
    End Function


    Private Function _ExportAndSkipImagesForOlderMachines() As List(Of String)
        Dim lines As New List(Of String)
        Dim iCount As Integer = 0
        Dim sName As String, sUrl As String

        For i As Integer = 0 To mNames.Count - 1
            If mNames.Item(i) <> "" And mUrls.Item(i) <> "" Then
                sName = cTemplate_Airline_Name.Replace("%%NUM%%", iCount.ToString).Replace("%%STRING%%", mNames.Item(i))
                sUrl = cTemplate_Airline_Link.Replace("%%NUM%%", iCount.ToString).Replace("%%STRING%%", mUrls.Item(i))

                lines.Add(sName)
                lines.Add(sUrl)

                lines.Add(" ")

                iCount += 1
            End If
        Next

        Return lines
    End Function

#End Region
End Class
