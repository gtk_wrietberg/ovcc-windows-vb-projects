Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String

    Public Sub Main()
        Dim bSomethingChanged As Boolean = False

        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "airline_images"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\" & cPATH__AIRLINE_IMAGES
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\" & cPATH__AIRLINE_IMAGES
        End If
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        oLogger.WriteToLog(" ", , 0)
        oLogger.WriteToLog("Parsing 'walled garden'", , 0)
        ParseWalledGarden()
        oLogger.WriteToLog("found " & gWalledGarden.Count.ToString & " entries", , 1)


        oLogger.WriteToLog("Parsing ibahn.js", , 0)
        If ParseJavascriptFile() Then
            oLogger.WriteToLog("ok", , 1)
            oLogger.WriteToLog("found " & gAirlines.Count.ToString & " entries", , 1)
            If gAirlines.IsOldStyleMachine Then
                oLogger.WriteToLog("detected old style ibahn.js", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
            End If
            If gAirlines.IsDuplicateDetected Then
                oLogger.WriteToLog("detected duplicate entries", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
            End If
        Else
            oLogger.WriteToLog("Whoops", , 1)
        End If


        oLogger.WriteToLog("Loading patches for ibahn.js", , 0)
        LoadAirlinePatches()


        oLogger.WriteToLog("Loading patches for skcfg", , 0)
        LoadWalledGardenPatches()


        If gAirlines.IsChanged Then
            bSomethingChanged = True

            oLogger.WriteToLog("Create template for ibahn.js", , 0)
            TemplatizeJavascriptFile()

            oLogger.WriteToLog("Saving ibahn.js", , 0)
            SaveJavascriptFile()
        Else
            oLogger.WriteToLog("Data not changed, ibahn.js not saved", , 0)
        End If

        If gWalledGarden.HasChanged Then
            bSomethingChanged = True

            oLogger.WriteToLog("Saving skcfg", , 0)
            SkCfg_Update()
        Else
            oLogger.WriteToLog("Data not changed, skcfg not saved", , 0)
        End If

        If bSomethingChanged Then
            oLogger.WriteToLog("Restarting SiteKiosk", , 0)
            KillSiteKiosk()
        End If

        Bye()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function ParseJavascriptFile() As Boolean
        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\default\Scripts\ibahn.js"
        sFile = sFile.Replace("\\", "\")

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String
                Dim tmpName As String
                Dim tmpCurrentName As String = ""
                Dim tmpCurrentUrl As String = ""
                Dim tmpCurrentImage As String = ""
                Dim iLineNumber As Integer = 0, iLineNumber_Name As Integer = 0

                While Not sr.EndOfStream
                    line = sr.ReadLine
                    iLineNumber += 1

                    If (iLineNumber - iLineNumber_Name) > 2 Then
                        'save
                        If tmpCurrentName <> "" Then
                            gAirlines.Populate(tmpCurrentName, tmpCurrentUrl, tmpCurrentImage)
                            gAirlines.AirlineSectionLastLine = iLineNumber
                            tmpCurrentName = ""
                            tmpCurrentUrl = ""
                            tmpCurrentImage = ""
                        End If
                    End If
                    If Regex.IsMatch(line, cRegExp_Airline_Name) Then
                        iLineNumber_Name = iLineNumber

                        If gAirlines.AirlineSectionFirstLine = 0 Then
                            gAirlines.AirlineSectionFirstLine = iLineNumber
                        End If
                        tmpName = Regex.Replace(line, cRegExp_Airline_Name, "$2")
                        If tmpName <> "" And tmpName <> tmpCurrentName Then
                            tmpCurrentName = tmpName
                        End If
                    End If
                    If Regex.IsMatch(line, cRegExp_Airline_Link) Then
                        tmpCurrentUrl = Regex.Replace(line, cRegExp_Airline_Link, "$2")
                    End If
                    If Regex.IsMatch(line, cRegExp_Airline_Image) Then
                        tmpCurrentImage = Regex.Replace(line, cRegExp_Airline_Image, "$2")
                        gAirlines.IsOldStyleMachine = False
                    End If
                End While
            End Using

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try

        Return False
    End Function

    Private Sub TemplatizeJavascriptFile()
        Dim lines As New List(Of String)
        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\default\Scripts\ibahn.js"
        sFile = sFile.Replace("\\", "\")

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String
                Dim iLineNumber As Integer = 0

                While Not sr.EndOfStream
                    line = sr.ReadLine
                    iLineNumber += 1

                    If iLineNumber < gAirlines.AirlineSectionFirstLine Or iLineNumber >= gAirlines.AirlineSectionLastLine Then
                        lines.Add(line)
                    End If
                    If iLineNumber = gAirlines.AirlineSectionFirstLine Then
                        lines.Add(cTemplate_Airline_List_Replacement)
                    End If
                End While
            End Using

            Using sw As New IO.StreamWriter(sFile & ".template")
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try
    End Sub

    Private Sub SaveJavascriptFile()
        Dim lines As New List(Of String)
        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\default\Scripts\ibahn.js"
        sFile = sFile.Replace("\\", "\")

        Try
            Using sr As New IO.StreamReader(sFile & ".template")
                Dim line As String
                Dim iLineNumber As Integer = 0

                While Not sr.EndOfStream
                    line = sr.ReadLine
                    iLineNumber += 1

                    If line.Equals(cTemplate_Airline_List_Replacement) Then
                        lines.AddRange(gAirlines.ExportJavascript)
                    Else
                        lines.Add(line)
                    End If
                End While
            End Using

            BackupFile(New IO.FileInfo(sFile))

            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try
    End Sub

    Private Sub ParseWalledGarden()
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_Plugins As Xml.XmlNodeList
            Dim mXmlNode_Plugin As Xml.XmlNode
            Dim mXmlNode_SiteCash As Xml.XmlNode, bSiteCashFound As Boolean = False
            Dim mXmlNode_UrlZones As Xml.XmlNodeList
            Dim mXmlNode_UrlZone As Xml.XmlNode
            Dim mXmlNode_AirlinesZone As Xml.XmlNode, bAirlinesZoneFound As Boolean = False
            Dim mXmlNode_Urls As Xml.XmlNodeList
            Dim mXmlNode_Url As Xml.XmlNode


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("looking for FreeAirlines url-zone", , 1)

            mXmlNode_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", ns)
            For Each mXmlNode_Plugin In mXmlNode_Plugins
                If mXmlNode_Plugin.Attributes.GetNamedItem("name").Value = "SiteCash" Then
                    mXmlNode_SiteCash = mXmlNode_Plugin
                    bSiteCashFound = True

                    Exit For
                End If
            Next

            If Not bSiteCashFound Then
                Throw New Exception("SiteCash not found!")
            End If


            mXmlNode_UrlZones = mXmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
            For Each mXmlNode_UrlZone In mXmlNode_UrlZones
                If mXmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = "FreeAirlines" Then
                    mXmlNode_AirlinesZone = mXmlNode_UrlZone
                    bAirlinesZoneFound = True
                End If
            Next

            If Not bAirlinesZoneFound Then
                Throw New Exception("Airlines url-zone not found!")
            End If

            oLogger.WriteToLog("ok", , 2)
            mXmlNode_Urls = mXmlNode_AirlinesZone.SelectNodes("sk:url", ns)
            For Each mXmlNode_Url In mXmlNode_Urls
                gWalledGarden.Populate(mXmlNode_Url.InnerText)
            Next

        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try
    End Sub

    Private Sub LoadAirlinePatches()
        oLogger.WriteToLog("loading 'airline' patches", , 0)

        Try
            Dim mXmlNodeList_items As Xml.XmlNodeList
            Dim mXmlNode_item As Xml.XmlNode
            Dim mXmlNode_name As Xml.XmlNode
            Dim mXmlNode_url As Xml.XmlNode
            Dim mXmlNode_image As Xml.XmlNode

            Dim mXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            mXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            oLogger.WriteToLog(cXML_PATCH__AIRLINES, , 2)
            mXml.Load(cXML_PATCH__AIRLINES)
            oLogger.WriteToLog("ok", , 3)

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mXml.SelectSingleNode("airlineupdates")

            oLogger.WriteToLog("reading 'delete' section", , 2)
            Try
                Dim mXmlNode_delete As Xml.XmlNode

                oLogger.WriteToLog("delete node", , 3)
                mXmlNode_delete = mXmlNode_Root.SelectSingleNode("delete")

                oLogger.WriteToLog("item nodes", , 3)
                mXmlNodeList_items = mXmlNode_delete.SelectNodes("item")
                For Each mXmlNode_item In mXmlNodeList_items
                    mXmlNode_name = mXmlNode_item.SelectSingleNode("name")
                    mXmlNode_url = mXmlNode_item.SelectSingleNode("url")
                    mXmlNode_image = mXmlNode_item.SelectSingleNode("image")

                    oLogger.WriteToLog("item node", , 3)
                    oLogger.WriteToLog("name", , 4)
                    oLogger.WriteToLog(mXmlNode_name.InnerText, , 5)
                    oLogger.WriteToLog("url", , 4)
                    oLogger.WriteToLog(mXmlNode_url.InnerText, , 5)
                    oLogger.WriteToLog("image", , 4)
                    oLogger.WriteToLog(mXmlNode_image.InnerText, , 5)

                    gAirlines.Delete(mXmlNode_name.InnerText, mXmlNode_url.InnerText, mXmlNode_image.InnerText)
                    gWalledGarden.Delete(GetShortUrl_http(mXmlNode_url.InnerText))
                    gWalledGarden.Delete(GetShortUrl_https(mXmlNode_url.InnerText))
                Next
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", , 3)
                oLogger.WriteToLog(ex.Message, , 4)
            End Try

            '------------------------------

            oLogger.WriteToLog("reading 'update' section", , 2)
            Try
                Dim mXmlNode_update As Xml.XmlNode

                oLogger.WriteToLog("update node", , 3)
                mXmlNode_update = mXmlNode_Root.SelectSingleNode("update")

                oLogger.WriteToLog("item nodes", , 3)
                mXmlNodeList_items = mXmlNode_update.SelectNodes("item")
                For Each mXmlNode_item In mXmlNodeList_items
                    mXmlNode_name = mXmlNode_item.SelectSingleNode("name")
                    mXmlNode_url = mXmlNode_item.SelectSingleNode("url")
                    mXmlNode_image = mXmlNode_item.SelectSingleNode("image")

                    oLogger.WriteToLog("item node", , 3)
                    oLogger.WriteToLog("name", , 4)
                    oLogger.WriteToLog(mXmlNode_name.InnerText, , 5)
                    oLogger.WriteToLog("url", , 4)
                    oLogger.WriteToLog(mXmlNode_url.InnerText, , 5)
                    oLogger.WriteToLog("image", , 4)
                    oLogger.WriteToLog(mXmlNode_image.InnerText, , 5)

                    gAirlines.Edit("name", mXmlNode_name.InnerText, mXmlNode_url.InnerText, mXmlNode_image.InnerText)
                    gWalledGarden.Add(GetShortUrl_http(mXmlNode_url.InnerText))
                    gWalledGarden.Add(GetShortUrl_https(mXmlNode_url.InnerText))
                Next
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", , 3)
                oLogger.WriteToLog(ex.Message, , 4)
            End Try

            '------------------------------

            oLogger.WriteToLog("reading 'add' section", , 2)
            Try
                Dim mXmlNode_add As Xml.XmlNode

                oLogger.WriteToLog("add node", , 3)
                mXmlNode_add = mXmlNode_Root.SelectSingleNode("add")

                oLogger.WriteToLog("item nodes", , 3)
                mXmlNodeList_items = mXmlNode_add.SelectNodes("item")
                For Each mXmlNode_item In mXmlNodeList_items
                    mXmlNode_name = mXmlNode_item.SelectSingleNode("name")
                    mXmlNode_url = mXmlNode_item.SelectSingleNode("url")
                    mXmlNode_image = mXmlNode_item.SelectSingleNode("image")

                    oLogger.WriteToLog("item node", , 3)
                    oLogger.WriteToLog("name", , 4)
                    oLogger.WriteToLog(mXmlNode_name.InnerText, , 5)
                    oLogger.WriteToLog("url", , 4)
                    oLogger.WriteToLog(mXmlNode_url.InnerText, , 5)
                    oLogger.WriteToLog("image", , 4)
                    oLogger.WriteToLog(mXmlNode_image.InnerText, , 5)

                    gAirlines.Add(mXmlNode_name.InnerText, mXmlNode_url.InnerText, mXmlNode_image.InnerText)
                    gWalledGarden.Add(GetShortUrl_http(mXmlNode_url.InnerText))
                    gWalledGarden.Add(GetShortUrl_https(mXmlNode_url.InnerText))
                Next
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", , 3)
                oLogger.WriteToLog(ex.Message, , 4)
            End Try
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try
    End Sub

    Private Sub LoadWalledGardenPatches()
        oLogger.WriteToLog("loading 'walled garden' patches", , 0)

        Try
            Dim mXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            mXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            oLogger.WriteToLog(cXML_PATCH__WALLEDGARDEN, , 2)
            mXml.Load(cXML_PATCH__WALLEDGARDEN)
            oLogger.WriteToLog("ok", , 3)

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mXml.SelectSingleNode("airline-walledgarden")

            oLogger.WriteToLog("reading 'delete' section", , 2)
            Try
                Dim mXmlNode_delete As Xml.XmlNode
                Dim mXmlNodeList_items As Xml.XmlNodeList
                Dim mXmlNode_item As Xml.XmlNode
                Dim mXmlNode_url As Xml.XmlNode

                oLogger.WriteToLog("delete node", , 3)
                mXmlNode_delete = mXmlNode_Root.SelectSingleNode("delete")

                oLogger.WriteToLog("item nodes", , 3)
                mXmlNodeList_items = mXmlNode_delete.SelectNodes("item")
                For Each mXmlNode_item In mXmlNodeList_items
                    mXmlNode_url = mXmlNode_item.SelectSingleNode("url")

                    oLogger.WriteToLog("item node", , 3)
                    oLogger.WriteToLog("url", , 4)
                    oLogger.WriteToLog(mXmlNode_url.InnerText, , 5)

                    gWalledGarden.Delete(mXmlNode_url.InnerText)
                Next
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", , 3)
                oLogger.WriteToLog(ex.Message, , 4)
            End Try

            '------------------------------

            oLogger.WriteToLog("reading 'update' section", , 2)
            Try
                Dim mXmlNode_update As Xml.XmlNode
                Dim mXmlNodeList_items As Xml.XmlNodeList
                Dim mXmlNode_item As Xml.XmlNode
                Dim mXmlNode__old_url As Xml.XmlNode, mXmlNode__new_url As Xml.XmlNode

                oLogger.WriteToLog("update node", , 3)
                mXmlNode_update = mXmlNode_Root.SelectSingleNode("update")

                oLogger.WriteToLog("item nodes", , 3)
                mXmlNodeList_items = mXmlNode_update.SelectNodes("item")
                For Each mXmlNode_item In mXmlNodeList_items
                    mXmlNode__old_url = mXmlNode_item.SelectSingleNode("old-url")
                    mXmlNode__new_url = mXmlNode_item.SelectSingleNode("new-url")

                    oLogger.WriteToLog("item node", , 3)
                    oLogger.WriteToLog("name", , 4)
                    oLogger.WriteToLog("old-url", , 4)
                    oLogger.WriteToLog(mXmlNode__old_url.InnerText, , 5)
                    oLogger.WriteToLog("new-url", , 4)
                    oLogger.WriteToLog(mXmlNode__new_url.InnerText, , 5)

                    gWalledGarden.Edit(mXmlNode__old_url.InnerText, mXmlNode__new_url.InnerText)
                Next
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", , 3)
                oLogger.WriteToLog(ex.Message, , 4)
            End Try

            '------------------------------

            oLogger.WriteToLog("reading 'add' section", , 2)
            Try
                Dim mXmlNode_add As Xml.XmlNode
                Dim mXmlNodeList_items As Xml.XmlNodeList
                Dim mXmlNode_item As Xml.XmlNode
                Dim mXmlNode_url As Xml.XmlNode

                oLogger.WriteToLog("add node", , 3)
                mXmlNode_add = mXmlNode_Root.SelectSingleNode("add")

                oLogger.WriteToLog("item nodes", , 3)
                mXmlNodeList_items = mXmlNode_add.SelectNodes("item")
                For Each mXmlNode_item In mXmlNodeList_items
                    mXmlNode_url = mXmlNode_item.SelectSingleNode("url")

                    oLogger.WriteToLog("item node", , 3)
                    oLogger.WriteToLog("url", , 4)
                    oLogger.WriteToLog(mXmlNode_url.InnerText, , 5)

                    If Not mXmlNode_url.InnerText.StartsWith("http://") And Not mXmlNode_url.InnerText.StartsWith("https://") Then
                        gWalledGarden.Add("http://" & mXmlNode_url.InnerText)
                        gWalledGarden.Add("https://" & mXmlNode_url.InnerText)
                    Else
                        gWalledGarden.Add(mXmlNode_url.InnerText)
                    End If
                Next
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", , 3)
                oLogger.WriteToLog(ex.Message, , 4)
            End Try
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)
        End Try
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_Plugins As Xml.XmlNodeList
            Dim mXmlNode_Plugin As Xml.XmlNode
            Dim mXmlNode_SiteCash As Xml.XmlNode, bSiteCashFound As Boolean = False
            Dim mXmlNode_UrlZones As Xml.XmlNodeList
            Dim mXmlNode_UrlZone As Xml.XmlNode
            Dim mXmlNode_AirlinesZone As Xml.XmlNode, bAirlinesZoneFound As Boolean = False
            Dim mXmlNode_Urls As Xml.XmlNodeList
            Dim mXmlNode_Url As Xml.XmlNode
            Dim mXmlNode_Url__clone As Xml.XmlNode

            Dim sUrl As String = ""

            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("update UrlZone", , 1)

            mXmlNode_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", ns)
            For Each mXmlNode_Plugin In mXmlNode_Plugins
                If mXmlNode_Plugin.Attributes.GetNamedItem("name").Value = "SiteCash" Then
                    mXmlNode_SiteCash = mXmlNode_Plugin
                    bSiteCashFound = True

                    Exit For
                End If
            Next

            If Not bSiteCashFound Then
                Throw New Exception("SiteCash not found!")
            End If


            mXmlNode_UrlZones = mXmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
            For Each mXmlNode_UrlZone In mXmlNode_UrlZones
                If mXmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = "FreeAirlines" Then
                    mXmlNode_AirlinesZone = mXmlNode_UrlZone
                    bAirlinesZoneFound = True
                End If
            Next

            If Not bAirlinesZoneFound Then
                Throw New Exception("Airlines url-zone not found!")
            End If


            mXmlNode_Url__clone = mXmlNode_AirlinesZone.SelectSingleNode("sk:url", ns).CloneNode(True)


            oLogger.WriteToLog("removing existing urls", , 2)
            mXmlNode_Urls = mXmlNode_AirlinesZone.SelectNodes("sk:url", ns)
            For Each mXmlNode_Url In mXmlNode_Urls
                mXmlNode_AirlinesZone.RemoveChild(mXmlNode_Url)
            Next


            oLogger.WriteToLog("repopulating urls", , 2)
            gWalledGarden.ExportReset()
            Do While gWalledGarden.ExportNext(sUrl)
                mXmlNode_Url = mXmlNode_Url__clone.CloneNode(True)
                mXmlNode_Url.InnerText = sUrl
                mXmlNode_AirlinesZone.AppendChild(mXmlNode_Url)
            Loop


            BackupFile(New IO.FileInfo(mSkCfgFile))

            oLogger.WriteToLog("saving", , 1)
            mSkCfgXml.Save(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        oLogger.WriteToLog("creating backup of '" & oFile.FullName & "'", , 0)

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLog("backup file: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLog("done", , 1)
                bRet = True
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

