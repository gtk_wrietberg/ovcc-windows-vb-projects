Module Globals
    Public oLogger As Logger
    Public oSkCfg As SkCfg

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String
    Public g_OverwriteExistingConfig As Boolean

    Public g_TESTMODE As Boolean = False


    Public ReadOnly c_PARAM_DISABLE_0 As String = "disable"
    Public ReadOnly c_PARAM_DISABLE_1 As String = "-disable"
    Public ReadOnly c_PARAM_DISABLE_2 As String = "--disable"
    Public ReadOnly c_PARAM_ENABLE_0 As String = "enable"
    Public ReadOnly c_PARAM_ENABLE_1 As String = "-enable"
    Public ReadOnly c_PARAM_ENABLE_2 As String = "--enable"

    Public g_DaylightSavingsDisabled As Boolean


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        Try
            System.IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try
        Try
            System.IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
