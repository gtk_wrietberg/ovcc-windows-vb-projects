﻿Module modMain
    Public Sub Main()
        If Helpers.Generic.IsDevMachine Then
            g_TESTMODE = True
        End If

        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))
        oLogger.WriteToLog(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        oLogger.WriteToLog("init", , 0)
        Dim sTmp As String = "", bSet As Boolean = False

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals(c_PARAM_DISABLE_0) Or arg.Equals(c_PARAM_DISABLE_1) Or arg.Equals(c_PARAM_DISABLE_2) Then
                g_DaylightSavingsDisabled = True
                bSet = True
            End If

            If arg.Equals(c_PARAM_ENABLE_0) Or arg.Equals(c_PARAM_ENABLE_1) Or arg.Equals(c_PARAM_ENABLE_2) Then
                g_DaylightSavingsDisabled = False
                bSet = True
            End If
        Next


        Dim iTmp1 As Integer, iTmp2 As Integer

        iTmp1 = Helpers.Registry.GetValue_Integer("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation", "DisableAutoDaylightTimeSet", -1)
        iTmp2 = Helpers.Registry.GetValue_Integer("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation", "DynamicDaylightTimeDisabled", -1)

        oLogger.WriteToLog("DaylightSavings state", , 1)
        oLogger.WriteToLog("currently", , 2)

        oLogger.WriteToLog("DisableAutoDaylightTimeSet (Win XP)", , 3)
        If iTmp1 < 0 Then
            oLogger.WriteToLog("not set", , 4)
        Else
            oLogger.WriteToLog(iTmp1.ToString, , 4)
        End If

        oLogger.WriteToLog("DynamicDaylightTimeDisabled", , 3)
        If iTmp2 < 0 Then
            oLogger.WriteToLog("not set", , 4)
        Else
            oLogger.WriteToLog(iTmp2.ToString, , 4)
        End If


        oLogger.WriteToLog("new value", , 2)
        If bSet Then
            If g_DaylightSavingsDisabled Then
                oLogger.WriteToLog("1", , 3)

                oLogger.WriteToLog("1", , 3)

                Helpers.Registry.SetValue_Integer("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation", "DisableAutoDaylightTimeSet", 1)
                Helpers.Registry.SetValue_Integer("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation", "DynamicDaylightTimeDisabled", 1)
            Else
                oLogger.WriteToLog("0", , 3)

                Helpers.Registry.SetValue_Integer("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation", "DisableAutoDaylightTimeSet", 0)
                Helpers.Registry.SetValue_Integer("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TimeZoneInformation", "DynamicDaylightTimeDisabled", 0)
            End If
        Else
            oLogger.WriteToLog("not specified, nothing was changed!", Logger.MESSAGE_TYPE.LOG_WARNING, 3)
        End If


        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        ExitApplication()
    End Sub

    Private Sub ExitApplication()
        '-----------------------------------------------
        'done
        oLogger.WriteToLog("exiting", , 0)
        oLogger.WriteToLog("exit code", , 1)
        oLogger.WriteToLog(ExitCode.ToString(), , 2)
        oLogger.WriteToLog("bye", , 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

End Module
