﻿Module Module1

    Sub Main()
        MsgBox(ExitCode.ToString())

        ExitCode.SetValue(ExitCode.ExitCodes.DATA_LOADING_ERROR)
        MsgBox(ExitCode.ToString())

        ExitCode.SetValue(ExitCode.ExitCodes.JAVASCRIPT_UPDATE_ERROR)
        MsgBox(ExitCode.ToString())

        ExitCode.SetValue(ExitCode.ExitCodes.MACHINE_NAME_ERROR)
        MsgBox(ExitCode.ToString())

        ExitCode.UnsetValue(ExitCode.ExitCodes.DATA_LOADING_ERROR)
        MsgBox(ExitCode.ToString())

    End Sub

End Module
