Module Globals
    Public oLogger As Logger
    Public oSkCfg As SkCfg

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public g_BackupDirectory As String

    Public g_OverwriteExistingConfig As Boolean

    Public g_TESTMODE As Boolean = False

    Public ReadOnly c_RESOURCES__DatFilePrefix As String = "HiltonDataCsv"

    Public ReadOnly c_PARAM_TESTMODE As String = "--testmode"



    Public Sub InitGlobals()
        Dim sComputerName As String = Environment.MachineName.ToLower

        If InStr(sComputerName, "rietberg") > 0 _
        Or InStr(sComputerName, "superlekkerding") > 0 _
        Or InStr(sComputerName, "wrdev") > 0 Then
            g_TESTMODE = True
        End If


        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        Try
            System.IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try
        Try
            System.IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
