Prerequisites:

.NET framework 4


Make sure that Altiris runs this with "Display window: hidden"!!!


-------------------------------------------------------------------------

No additional files or config needed, list is embedded. To update the list, just mail/skype it to me ( = Wouter)...

HiltonInternetPageUpdater expects LPC to follow LPC naming rule XXYYYYYPCnnn
where XX is country code, YYYYY is property code, and nnn is pc number.

If the machine name does not match the rule, HiltonInternetPageUpdater tries to match the full machine name against the list.


Return/exit codes:

0    All seems fine
1    Error while loading internal data list
2    Machine appears not to follow LPC naming rule
4    No match found in list
8    Error while updating JavaScript file
16   Error while updating SiteKiosk config file
32   Error in URL. Might be incorrect, or unreachable
64   Unknown error
128  Started in test mode. Nothing was changed
256  Nothing happened at all. Very bad


The actual exit code can be a combination of the above. So e.g. an exit code of 6 means that the machine doesn't follow the naming rule, and no match was found in the list (2 + 4 = 6)
