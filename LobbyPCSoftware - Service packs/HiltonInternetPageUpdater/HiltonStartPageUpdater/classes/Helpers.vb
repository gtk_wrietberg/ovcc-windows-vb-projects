﻿Imports System.Reflection
Imports System.Net

Public Class Helpers
    Public Class Network
        Public Shared Function GetHostnameFromUrl(ByVal sUrl As String) As String
            Dim u As New Uri(sUrl)

            Return u.Host
        End Function

        Public Shared Function GetStatusCodeForUrl(url As String) As HttpStatusCode
            Dim hRet As HttpStatusCode

            Try
                Dim myHttpWebRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)
                Dim myHttpWebResponse As HttpWebResponse = CType(myHttpWebRequest.GetResponse(), HttpWebResponse)

                hRet = myHttpWebResponse.StatusCode

                myHttpWebResponse.Close()
            Catch exWeb As WebException
                Dim response As HttpWebResponse = exWeb.Response

                hRet = response.StatusCode
            Catch ex As Exception
                hRet = 0
            End Try

            Return hRet
        End Function

        Public Shared Function DoesURLExist(ByVal sURL As String) As Boolean
            Dim iCode As HttpStatusCode = GetStatusCodeForUrl(sURL)

            Return iCode = HttpStatusCode.OK
        End Function

        Public Shared Function CheckURL(ByVal HostAddress As String) As Boolean
            CheckURL = False

            Dim url As New System.Uri(HostAddress)
            Dim wRequest As System.Net.WebRequest
            wRequest = System.Net.WebRequest.Create(url)
            Dim wResponse As System.Net.WebResponse
            Try
                wResponse = wRequest.GetResponse()
                'Is the responding address the same as HostAddress to avoid false positive from an automatic redirect.
                If wResponse.ResponseUri.AbsoluteUri().ToString = HostAddress Then 'include query strings
                    CheckURL = True
                End If
                wResponse.Close()
                wRequest = Nothing
            Catch ex As Exception
                wRequest = Nothing
                MsgBox(ex.ToString)
            End Try

            Return CheckURL
        End Function
    End Class

    Public Class SiteKiosk
        Public Shared Function Kill() As Integer
            Dim iCount As Integer = 0

            For Each p As Process In Process.GetProcessesByName("sitekiosk")
                Try
                    oLogger.WriteToLog("killing", , 1)
                    p.Kill()
                    oLogger.WriteToLog("ok", , 2)
                    iCount += 1
                Catch ex As Exception
                    iCount = -1
                End Try
            Next

            Return iCount
        End Function
    End Class

    Public Class Misc
        Public Shared Function ResolveAssemblies(sender As Object, e As System.ResolveEventArgs) As Reflection.Assembly
            Dim desiredAssembly = New Reflection.AssemblyName(e.Name)

            If desiredAssembly.Name = "LumenWorks.Framework.IO" Then
                Return Reflection.Assembly.Load(My.Resources.LumenWorks_Framework_IO)
            Else
                Return Nothing
            End If
        End Function

    End Class

    Public Class Resources
        Public Shared Function GetMyResourcesDictionary() As Dictionary(Of String, Object)
            Dim ItemDictionary As New Dictionary(Of String, Object)
            Dim ItemEnumerator As System.Collections.IDictionaryEnumerator
            Dim ItemResourceSet As System.Resources.ResourceSet
            Dim ResourceNameList As New List(Of String)

            ItemResourceSet = My.Resources.ResourceManager.GetResourceSet(New System.Globalization.CultureInfo("en"), True, True)

            'Get the enumerator for My.Resources 
            ItemEnumerator = ItemResourceSet.GetEnumerator

            Do While ItemEnumerator.MoveNext
                ResourceNameList.Add(ItemEnumerator.Key.ToString)
            Loop

            For Each resourceName As String In ResourceNameList
                ItemDictionary.Add(resourceName, GetItem(resourceName))
            Next

            ResourceNameList = Nothing

            Return ItemDictionary
        End Function

        Private Shared Function GetItem(ByVal resourceName As String) As Object
            Return My.Resources.ResourceManager.GetObject(resourceName)
        End Function
    End Class
End Class
