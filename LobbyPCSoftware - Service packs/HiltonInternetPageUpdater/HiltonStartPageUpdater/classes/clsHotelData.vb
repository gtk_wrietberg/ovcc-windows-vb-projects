﻿Public Class HotelData
    Public Class HotelItem
        Private mPropCode As String
        Public Property PropCode() As String
            Get
                Return mPropCode
            End Get
            Set(ByVal value As String)
                mPropCode = value
            End Set
        End Property

        Private mSecondaryPropCode As String
        Public Property PropCodeSecondary() As String
            Get
                Return mSecondaryPropCode
            End Get
            Set(ByVal value As String)
                mSecondaryPropCode = value
            End Set
        End Property

        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mUrl As String
        Public Property URL() As String
            Get
                Return mUrl
            End Get
            Set(ByVal value As String)
                mUrl = value
            End Set
        End Property
    End Class

    Private mHotelData As List(Of HotelItem)


    Public Sub New()
        mHotelData = New List(Of HotelItem)
    End Sub

    Public ReadOnly Property Count() As Integer
        Get
            Return mHotelData.Count
        End Get
    End Property


    Private Function _exists(sPropcode As String) As Boolean
        Dim bRet As Boolean = False

        For Each item As HotelItem In mHotelData
            If item.PropCode.ToLower.Equals(sPropcode.ToLower) Then
                bRet = True
                Exit For
            End If
        Next

        Return bRet
    End Function

    Public Function Add(sPropCode As String, sURL As String) As Boolean
        Return Add(sPropCode, "(not set)", "(not set)", sURL)
    End Function

    Public Function Add(sPropCode As String, sSecondaryPropcode As String, sURL As String) As Boolean
        Return Add(sPropCode, sSecondaryPropcode, "(not set)", sURL)
    End Function

    Public Function Add(sPropCode As String, sSecondaryPropcode As String, sName As String, sURL As String) As Boolean
        If _exists(sPropCode) Then
            Return False
        Else
            Dim nItem As New HotelItem

            nItem.PropCode = sPropCode
            nItem.PropCodeSecondary = sSecondaryPropcode
            nItem.Name = sName
            nItem.URL = sURL
            mHotelData.Add(nItem)

            Return True
        End If
    End Function

    Public Function Find(sPropCode As String, ByRef nItem As HotelItem) As Boolean
        Dim bRet As Boolean = False

        For Each item As HotelItem In mHotelData
            If item.PropCode.ToLower.Equals(sPropCode.ToLower) Then
                nItem = item
                bRet = True
                Exit For
            End If
        Next

        Return bRet
    End Function

End Class
