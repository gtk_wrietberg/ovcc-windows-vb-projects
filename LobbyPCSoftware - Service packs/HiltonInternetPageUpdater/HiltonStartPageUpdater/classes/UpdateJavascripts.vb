﻿Public Class UpdateJavascripts
    Public Shared Function iBAHN_functions__SetInternetURL(urlInternet As String) As Boolean
        Return _iBAHN_functions(, urlInternet, , , , )
    End Function

    Public Shared Function iBAHN_functions(urlHotel As String, urlInternet As String, urlWeather As String, urlMap As String, siteAddress As String, indexBrand As Integer) As Boolean
        Return _iBAHN_functions(urlHotel, urlInternet, urlWeather, urlMap, siteAddress, indexBrand)
    End Function

    Private Shared Function _iBAHN_functions(Optional urlHotel As String = "", Optional urlInternet As String = "", Optional urlWeather As String = "", Optional urlMap As String = "", Optional siteAddress As String = "", Optional indexBrand As Integer = -1) As Boolean
        Dim lines As New List(Of String)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

        oLogger.WriteToLogRelative("editing", , 1)
        oLogger.WriteToLogRelative(sFile, , 2)

        If Not System.IO.File.Exists(sFile) Then
            oLogger.WriteToLogRelative("not found", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End If

        Try
            Using sr As New System.IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        If Not urlHotel.Equals("") Then
                            line = "var iBAHN_Global_HotelURL=""" & urlHotel & """;"
                            oLogger.WriteToLogRelative("updating HotelURL", , 2)
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        If Not urlInternet.Equals("") Then
                            line = "var iBAHN_Global_InternetURL=""" & urlInternet & """;"
                            oLogger.WriteToLogRelative("updating InternetURL", , 2)
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        If Not urlWeather.Equals("") Then
                            line = "var iBAHN_Global_WeatherURL=""" & urlWeather & """;"
                            oLogger.WriteToLogRelative("updating WeatherURL", , 2)
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        If Not urlMap.Equals("") Then
                            line = "var iBAHN_Global_MapURL=""" & urlMap & """;"
                            oLogger.WriteToLogRelative("updating MapURL", , 2)
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        If Not siteAddress.Equals("") Then
                            line = "var iBAHN_Global_SiteAddress=""" & siteAddress & """;"
                            oLogger.WriteToLogRelative("updating SiteAddress", , 2)
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        If indexBrand > -1 Then
                            line = "var iBAHN_Global_Brand=" & indexBrand.ToString & ";"
                            oLogger.WriteToLogRelative("updating Brand", , 2)
                        End If
                    End If

                    lines.Add(line)
                End While
            End Using

            oLogger.WriteToLogRelative("saving", , 2)
            Using sw As New System.IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("error", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function

End Class
