﻿Imports LumenWorks.Framework.IO.Csv
Imports System.IO
Imports System.Net

Module modMain
    Private mHotelData As HotelData
    Private mResources As Dictionary(Of String, Object)

    Public Sub Main()
        AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf Helpers.Misc.ResolveAssemblies

        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)



        '------------------------------
        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, c_PARAM_TESTMODE) > 0 Then
                g_TESTMODE = True
            End If
        Next

        If g_TESTMODE Then
            oLogger.WriteToLog("running in TEST MODE", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
            ExitCode.SetValue(ExitCode.ExitCodes.TEST_MODE)
        End If


        '------------------------------
        'load data
        oLogger.WriteToLog("loading data", , 0)
        If LoadData() Then
            oLogger.WriteToLog("ok", , 1)

            oLogger.WriteToLog("total items in list: " & mHotelData.Count.ToString, , 2)

            'try to match propcode of machine to list
            Dim sMachineName As String = Environment.MachineName
            Dim sPropCode As String

            sPropCode = ExtractPropcodeFromMachineName(sMachineName)


            oLogger.WriteToLog("matching propcode to list", , 0)
            oLogger.WriteToLog("machine name", , 1)
            oLogger.WriteToLog(sMachineName, , 2)
            oLogger.WriteToLog("prop code", , 1)
            If sPropCode.Equals("") Then
                oLogger.WriteToLog("could not extract propcode from machine name", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                ExitCode.SetValue(ExitCode.ExitCodes.MACHINE_NAME_ERROR)

                oLogger.WriteToLog("setting propcode to machine name", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                sPropCode = Environment.MachineName

                oLogger.WriteToLog(sPropCode, , 3)
            Else
                oLogger.WriteToLog(sPropCode, , 2)
            End If


            Dim mHotelItem As New HotelData.HotelItem

            If mHotelData.Find(sPropCode, mHotelItem) Then
                oLogger.WriteToLog("found it", , 1)
                oLogger.WriteToLog("PropCode", , 2)
                oLogger.WriteToLog(mHotelItem.PropCode, , 3)
                oLogger.WriteToLog("PropCodeSecondary", , 2)
                oLogger.WriteToLog(mHotelItem.PropCodeSecondary, , 3)
                oLogger.WriteToLog("Name", , 2)
                oLogger.WriteToLog(mHotelItem.Name, , 3)
                oLogger.WriteToLog("URL", , 2)
                oLogger.WriteToLog(mHotelItem.URL, , 3)

                oLogger.WriteToLog("check url", , 1)
                Dim hStatus As HttpStatusCode

                hStatus = Helpers.Network.GetStatusCodeForUrl(mHotelItem.URL)

                If hStatus = HttpStatusCode.OK Then
                    oLogger.WriteToLog("looks fine", , 2)

                    If g_TESTMODE Then
                        oLogger.WriteToLog("not going to change anything, TESTMODE!!", , 1)
                        ExitCode.SetValueExclusive(ExitCode.ExitCodes.TEST_MODE)
                    Else
                        '-----------------------------------------------
                        'set internet url in iBAHN_functions javascript
                        oLogger.WriteToLog("set internet url in javascript", , 1)
                        If UpdateJavascripts.iBAHN_functions__SetInternetURL(mHotelItem.URL) Then
                            oLogger.WriteToLog("ok", , 2)

                            '-----------------------------------------------
                            'set internet url in free url in SiteCash
                            oLogger.WriteToLog("set internet url in free zone in SkCfg", , 1)
                            oSkCfg = New SkCfg
                            oSkCfg.AddFreeSiteToList(mHotelItem.URL)
                            If oSkCfg.AddPagesToFreezone() Then
                                oLogger.WriteToLog("ok", , 2)

                                'Everything seems ok
                                ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)
                            Else
                                oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                                ExitCode.SetValue(ExitCode.ExitCodes.SKCFG_UPDATE_ERROR)
                            End If
                        Else
                            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                            ExitCode.SetValue(ExitCode.ExitCodes.JAVASCRIPT_UPDATE_ERROR)
                        End If
                    End If
                Else
                    oLogger.WriteToLog("something's wrong", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    oLogger.WriteToLog("HTTP status code: " & hStatus & " (" & hStatus.ToString & ")", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    ExitCode.SetValue(ExitCode.ExitCodes.URL_ERROR)
                End If
            Else
                oLogger.WriteToLog("not in list", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                ExitCode.SetValue(ExitCode.ExitCodes.NO_MATCH)
            End If
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            ExitCode.SetValue(ExitCode.ExitCodes.DATA_LOADING_ERROR)
        End If


        '-----------------------------------------------
        'SiteKiosk restart
        If ExitCode.IsValue(ExitCode.ExitCodes.OK) Then
            oLogger.WriteToLog("restarting SiteKiosk, sledgehammer time. dum dum dum dumdumdum dum.", , 0)
            If g_TESTMODE Then
                oLogger.WriteToLog("not doing anything, we are in TESTMODE, remember?", , 1)
            Else
                Dim iTmp As Integer = Helpers.SiteKiosk.Kill()
                If iTmp > 0 Then
                    oLogger.WriteToLog("ok", , 1)
                ElseIf iTmp = 0 Then
                    oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
                Else
                    oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                End If
            End If
        Else
            oLogger.WriteToLog("nothing changed, or something went wrong. skipping SiteKiosk restart.", , 0)
        End If

        '-----------------------------------------------
        'done
        oLogger.WriteToLog(" ", , 0)
        oLogger.WriteToLog("exiting", , 0)
        oLogger.WriteToLog("exit code", , 1)
        oLogger.WriteToLog(ExitCode.ToString(), , 2)
        oLogger.WriteToLog("bye", , 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

    Private Function LoadData() As Boolean
        Dim iItemCount As Integer = 0
        Dim iDupeCount As Integer = 0
        Dim iTotalCount As Integer = 0

        oLogger.WriteToLogRelative("reading data from internal resources", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        Try
            mHotelData = New HotelData
            mResources = New Dictionary(Of String, Object)

            mResources = Helpers.Resources.GetMyResourcesDictionary()

            For Each kvp As KeyValuePair(Of String, Object) In mResources
                Dim name As String = kvp.Key

                iItemCount = 0
                iDupeCount = 0

                If TypeOf kvp.Value Is String Then
                    oLogger.WriteToLogRelative("found resource", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                    oLogger.WriteToLogRelative(name, Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)

                    If name.StartsWith(c_RESOURCES__DatFilePrefix) Then
                        oLogger.WriteToLogRelative("reading", Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

                        Try
                            Using csv As New CsvReader(New StringReader(kvp.Value.ToString), True)
                                csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                                Dim fieldCount As Integer = csv.FieldCount
                                Dim headers As String() = csv.GetFieldHeaders()

                                oLogger.WriteToLogRelative("data headers", Logger.MESSAGE_TYPE.LOG_DEFAULT, 5)
                                oLogger.WriteToLogRelative(String.Join(",", headers), Logger.MESSAGE_TYPE.LOG_DEFAULT, 6)

                                If fieldCount = 2 Then
                                    While csv.ReadNextRecord()
                                        If mHotelData.Add(csv(0), csv(1)) Then
                                            iItemCount += 1
                                        Else
                                            iDupeCount += 1
                                        End If
                                        iTotalCount += 1
                                    End While
                                ElseIf fieldCount = 3 Then
                                    While csv.ReadNextRecord()
                                        If mHotelData.Add(csv(0), csv(1), csv(2)) Then
                                            iItemCount += 1
                                        Else
                                            iDupeCount += 1
                                        End If
                                        iTotalCount += 1
                                    End While
                                ElseIf fieldCount = 4 Then
                                    While csv.ReadNextRecord()
                                        If mHotelData.Add(csv(0), csv(1), csv(2), csv(3)) Then
                                            iItemCount += 1
                                        Else
                                            iDupeCount += 1
                                        End If
                                        iTotalCount += 1
                                    End While
                                Else
                                    oLogger.WriteToLogRelative("malformed csv. expected 2, 3 or 4 fields, but has " & fieldCount.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                                End If

                                oLogger.WriteToLogRelative("items", Logger.MESSAGE_TYPE.LOG_DEFAULT, 5)
                                oLogger.WriteToLogRelative("found: " & (iItemCount + iDupeCount).ToString, Logger.MESSAGE_TYPE.LOG_DEFAULT, 6)
                                oLogger.WriteToLogRelative("added: " & iItemCount.ToString, Logger.MESSAGE_TYPE.LOG_DEFAULT, 6)
                                oLogger.WriteToLogRelative("dupes: " & iDupeCount.ToString, Logger.MESSAGE_TYPE.LOG_DEFAULT, 6)
                            End Using
                        Catch ex As Exception
                            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                        End Try

                    Else
                        oLogger.WriteToLogRelative("not recognised!", Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                    End If
                End If
            Next

            Return mHotelData.Count > 0
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Return False
    End Function

    Private Function ExtractPropcodeFromMachineName(Optional sMachineName As String = "") As String
        If sMachineName.Equals("") Then
            sMachineName = Environment.MachineName
        End If
        sMachineName = sMachineName.ToLower

        Dim sPropCode As String = ""

        'Machine name should follow rule: AABBBBBBBBBBBBBBBCCCnnn
        ' where AA is country code
        '       BBBB(etc) is propcode
        '       CCC is either PC or MAC
        '       nnn is the number
        '
        'added logic for machines that follow this rule: AABBBBBBBBBBBBBBBCCCnnn

        'Dim pattern As String = "^([a-z]{2})(.+)(pc|mac)([0-9]{3})$"
        Dim pattern As String = "^([a-z]{2})(.{5,})(pc|mac)([0-9]{3})$"
        Dim replacement As String = "$2"
        Dim exception As String = "bt"
        Dim rgx As New System.Text.RegularExpressions.Regex(pattern)
        sPropCode = rgx.Replace(sMachineName, replacement)

        'check if propcode ends with BT, remove BT if it's not part of the propcode
        If sPropCode.EndsWith(exception) Then
            If sPropCode.Length >= 7 Then
                Dim tmpLastIndex As Integer = sPropCode.LastIndexOf(exception)

                sPropCode = sPropCode.Remove(tmpLastIndex, exception.Length)
            End If
        End If

        If Not sPropCode.Equals(sMachineName) Then
            Return sPropCode
        Else
            Return ""
        End If
    End Function

End Module
