Prerequisites:

.NET framework 4


Make sure that Altiris runs this with "Display window: hidden"!!!


-------------------------------------------------------------------------

No additional files or config needed, list is embedded. To update the list, just mail/skype it to me ( = Wouter)...

HiltonInternetPageUpdater expects LPC to follow LPC naming rule XXYYYYYPCnnn
where XX is country code, YYYYY is property code, and nnn is pc number.


Return/exit codes:

0   All seems fine
1   Error while loading internal data list
2   Machine appears not to follow LPC naming rule
3   No match found in list
4   Error while updating JavaScript file
5   Error while updating SiteKiosk config file
666 Unknown error
