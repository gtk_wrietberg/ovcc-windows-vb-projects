﻿Public Class OVCC_CleanupService
    Private mThreading_Main As Threading.Thread
    Private mLoopAbort As Boolean = False


    Protected Overrides Sub OnStart(ByVal args() As String)
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))
        oLogger.WriteToLog(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        oLogger.WriteToLog("service started", , 0)
    End Sub

    Protected Overrides Sub OnStop()
        oLogger.WriteToLog("service stopped", , 0)
    End Sub

    Protected Overrides Sub OnCustomCommand(command As Integer)
        Select Case command
            Case c_CustomCommand_ClearPrintQueue
                oLogger.WriteToLog("received command " & command.ToString, , 0)

            Case Else
                'pass it through
                MyBase.OnCustomCommand(command)
        End Select

    End Sub

    Protected Sub HandleCommand_PrintQueue()
        Dim iTmp As Integer = -1

        oLogger.WriteToLog("cleaning print queues", , 1)

        iTmp = Print.CountQueueJobs()
        If iTmp = 1 Then
            oLogger.WriteToLog("found 1 job", , 2)
        Else
            oLogger.WriteToLog("found " & iTmp.ToString & " jobs", , 2)
        End If

        oLogger.WriteToLog("purging", , 3)
        Print.ClearQueue()


    End Sub
End Class
