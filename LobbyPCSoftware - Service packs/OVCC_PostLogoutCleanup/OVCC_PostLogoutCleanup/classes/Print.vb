﻿Imports System.Printing

Public Class Print
    Public Shared Function CountQueueJobs() As Integer
        Dim ret As Integer = 0

        Try
            Dim ps = New LocalPrintServer(PrintSystemDesiredAccess.AdministrateServer)
            Dim pqs As New PrintQueueCollection
            pqs = ps.GetPrintQueues()

            For Each pq As PrintQueue In pqs
                ret += pq.NumberOfJobs
            Next
        Catch ex As Exception
            ret = -1
        End Try

        Return ret
    End Function

    Public Shared Function ClearQueue() As Boolean
        Dim ret As Boolean = False

        oLogger.WriteToLogRelative("iterating print queues", , 1)

        Try
            Dim ps = New LocalPrintServer(PrintSystemDesiredAccess.AdministrateServer)
            Dim pqs As New PrintQueueCollection

            pqs = ps.GetPrintQueues()

            For Each pq As PrintQueue In pqs
                oLogger.WriteToLogRelative("found: " & pq.FullName, , 2)

                Try
                    oLogger.WriteToLogRelative("connecting", , 3)
                    Dim tmppq As New PrintQueue(ps, pq.FullName, PrintSystemDesiredAccess.AdministratePrinter)

                    oLogger.WriteToLogRelative("purging", , 3)
                    tmppq.Purge()
                Catch pex As System.Printing.PrintQueueException
                    oLogger.WriteToLogRelative(pex.GetType.Name, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                    oLogger.WriteToLogRelative(pex.Message.Replace(vbCr, "").Replace(vbLf, ""), Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                End Try
            Next

            ret = True
        Catch ex As Exception
            oLogger.WriteToLogRelative(ex.GetType.Name, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message.Replace(vbCr, "").Replace(vbLf, ""), Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            ret = False
        End Try

        Return ret
    End Function

End Class
