Module Globals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String
    Public g_ServiceDirectory As String


    Public g_TESTMODE As Boolean = False
    Public g_DEBUG As Boolean = False


    Public ReadOnly c_SERVICE_Name As String = "OVCC_PostLogoutCleanup_service"
    Public ReadOnly c_SERVICE_FileName As String = "OVCC_PostLogoutCleanup.exe"
    Public ReadOnly c_SERVICE_DisplayName As String = "OVCC Cleanup service"
    Public ReadOnly c_SERVICE_Description As String = ""


    Public ReadOnly c_CustomCommand_ClearPrintQueue As Integer = 128




    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        Dim sProgFile As String = String.Empty

        sProgFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
        If sProgFile.Equals(String.Empty) Then
            sProgFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        g_LogFileDirectory = sProgFile & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = sProgFile & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        g_ServiceDirectory = sProgFile & "\GuestTek\OVCC\PostLogoutCleanup"


        Try
            System.IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try
        Try
            System.IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
        Try
            System.IO.Directory.CreateDirectory(g_ServiceDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
