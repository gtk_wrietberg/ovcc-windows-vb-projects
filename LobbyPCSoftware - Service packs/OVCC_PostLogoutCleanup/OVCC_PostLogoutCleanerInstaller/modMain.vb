﻿Module modMain
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))
        oLogger.WriteToLog(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        oLogger.WriteToLog("started", , 0)


        'Copy files
        Dim sBackupFolder As String = ""
        Dim dDate As Date = Now()

        sBackupFolder = g_BackupDirectory

        Dim oCopyFiles As CopyFiles = New CopyFiles
        oCopyFiles.SuppressCopyLogging = False
        oCopyFiles.BackupDirectory = sBackupFolder

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        oLogger.WriteToLog("copy files", , 0)
        oCopyFiles.CopyFiles()


        'Install service part
        oLogger.WriteToLog("install service", , 0)
        InstallService(c_SERVICE_Name, c_SERVICE_DisplayName, c_SERVICE_Description, g_ServiceDirectory & "\" & c_SERVICE_FileName)


        'Done
        oLogger.WriteToLog("done", , 0)
        oLogger.WriteToLog("ok", , 1)
        oLogger.WriteToLog("bye", , 2)
    End Sub

    Private Sub StopService(Name As String)
        oLogger.WriteToLog(Name, Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            oLogger.WriteToLog("stopping", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            ServiceInstaller.StopService(Name)

            Threading.Thread.Sleep(15000)

            If ServiceInstaller.GetServiceStatus(Name) = ServiceState.Stop Then
                oLogger.WriteToLog("ok", Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog("service status: " & ServiceInstaller.GetServiceStatus(Name).ToString, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If

            Threading.Thread.Sleep(5000)
        Else
            oLogger.WriteToLog("not needed", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        End If
    End Sub

    Private Sub InstallService(Name As String, DisplayName As String, Description As String, ServicePath As String)
        Dim sStmp As String = ""

        oLogger.WriteToLog(Name, Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            oLogger.WriteToLog("already exists", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            oLogger.WriteToLog("stopping", Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            sStmp = ServiceInstaller.StopService(Name)
            oLogger.WriteToLog(sStmp, Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

            Threading.Thread.Sleep(15000)

            oLogger.WriteToLog("uninstalling", Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            sStmp = ServiceInstaller.Uninstall(Name)
            oLogger.WriteToLog(sStmp, Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

            Threading.Thread.Sleep(15000)
        End If



        If Not ServiceInstaller.ServiceIsInstalled(Name) Then
            oLogger.WriteToLog("ok", Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If


        Threading.Thread.Sleep(5000)

        oLogger.WriteToLog("installing (but not starting yet, will start after reboot)", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        ServiceInstaller.InstallService(Name, DisplayName, ServicePath)

        Threading.Thread.Sleep(5000)

        oLogger.WriteToLog("set description", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        ServiceInstaller.ChangeServiceDescription(Name, Description)

        oLogger.WriteToLog("installed?", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        If ServiceInstaller.ServiceIsInstalled(Name) Then
            oLogger.WriteToLog("ok", Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
    End Sub

End Module
