﻿Module modMain
    Public Sub Main()
        InitGlobals()


        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))
        oLogger.WriteToLog(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


        If Helpers.Generic.IsDevMachine Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                ApplicationExit()
                Exit Sub
            End If
        End If


        If Helpers.SiteKiosk.GetSiteKioskVersion(True) < 9 Then
            oLogger.WriteToLog("SiteKiosk is old! Not touching this machine.", , 0)
            oLogger.WriteToLog("version", , 1)
            oLogger.WriteToLog(Helpers.SiteKiosk.GetSiteKioskVersion(), , 2)

            ExitCode.SetValue(ExitCode.ExitCodes.OLD_SITEKIOSK_VERSION)

            ApplicationExit()
            Exit Sub
        End If

        If Not Helpers.Prerequisites.IE.IsRunningVersion(Helpers.Prerequisites.IE.IEVersions.IE11) Then
            oLogger.WriteToLog("IE is old! Not touching this machine.", , 0)
            oLogger.WriteToLog("version", , 1)
            oLogger.WriteToLog(Helpers.Prerequisites.IE.Version, , 2)

            ExitCode.SetValue(ExitCode.ExitCodes.OLD_IE_VERSION)

            ApplicationExit()
            Exit Sub
        End If

        oLogger.WriteToLog("Removing", , 0)
        oLogger.WriteToLog("key", , 1)
        oLogger.WriteToLog(c_REG__MSIE, , 2)
        Dim sError As String = ""
        If Not Helpers.Registry.RemoveValue(c_REG__MSIE, "Version", sError) Then
            oLogger.WriteToLog("something went wrong", , 3)
            oLogger.WriteToLog(sError, , 4)

            ExitCode.SetValue(ExitCode.ExitCodes.KEY_NOT_REMOVED)
        Else
            oLogger.WriteToLog("ok", , 3)
        End If


        ApplicationExit()
    End Sub

    Private Sub ApplicationExit()
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("exit code", , 1)
        oLogger.WriteToLog(CStr(ExitCode.GetValue()), , 2)
        oLogger.WriteToLog(ExitCode.ToString(), , 3)

        oLogger.WriteToLog("bye", , 1)

        Environment.ExitCode = ExitCode.GetValue()
        Application.Exit()
    End Sub

End Module
