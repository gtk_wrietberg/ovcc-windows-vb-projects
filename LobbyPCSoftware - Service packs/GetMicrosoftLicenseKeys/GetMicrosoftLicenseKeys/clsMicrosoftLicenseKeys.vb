﻿Imports Microsoft.Win32
Imports System.Xml

Public Class clsMicrosoftLicenseKeys
    Private Class AppInfo
        Private mVersion As String
        Public Property Version() As String
            Get
                Return mVersion
            End Get
            Set(ByVal value As String)
                mVersion = value
            End Set
        End Property

        Private mProductName As String
        Public Property ProductName() As String
            Get
                Return mProductName
            End Get
            Set(ByVal value As String)
                mProductName = value
            End Set
        End Property

        Private mDigitalProductId As String
        Public Property DigitalProductId() As String
            Get
                Return mDigitalProductId
            End Get
            Set(ByVal value As String)
                mDigitalProductId = value
            End Set
        End Property

        Private mDigitalProductId4 As String
        Public Property DigitalProductId4() As String
            Get
                Return mDigitalProductId4
            End Get
            Set(ByVal value As String)
                mDigitalProductId4 = value
            End Set
        End Property

        Private mProductId As String
        Public Property ProductId() As String
            Get
                Return mProductId
            End Get
            Set(ByVal value As String)
                mProductId = value
            End Set
        End Property
    End Class

    Private mWindows As New AppInfo
    Private mOffice As New AppInfo

    Private ReadOnly c_REGKEY__WINDOWS_KEY As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion"
    Private ReadOnly c_REGVALUE__Windows_CurrentVersion As String = "CurrentVersion"
    Private ReadOnly c_REGVALUE__Windows_ProductName As String = "ProductName"
    Private ReadOnly c_REGVALUE__Windows_DigitalProductId As String = "DigitalProductId"
    Private ReadOnly c_REGVALUE__Windows_DigitalProductId4 As String = "DigitalProductId4"
    Private ReadOnly c_REGVALUE__Windows_ProductId As String = "ProductId"

    Private ReadOnly c_REGKEY__Office_KEY As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office"
    Private ReadOnly c_REGVALUE__Office_CurrentVersion As String = "LastProduct"
    Private ReadOnly c_REGVALUE__Office_ProductName As String = "ProductName"
    Private ReadOnly c_REGVALUE__Office_DigitalProductId As String = "DigitalProductID"
    Private ReadOnly c_REGVALUE__Office_DigitalProductId4 As String = "DigitalProductId4"
    Private ReadOnly c_REGVALUE__Office_ProductId As String = "ProductID"
    Private m_REGKEY__Office_ProductVersion As String = ""
    Private m_REGKEY__Office_Registration As String = ""

    Public Sub New()
        mWindows = New AppInfo
        mOffice = New AppInfo

        FindOfficeRegKeys()
        LoadInfo()
    End Sub

    Private Sub LoadInfo()
        mWindows.DigitalProductId = Me.ProductKey(c_REGKEY__WINDOWS_KEY, c_REGVALUE__Windows_DigitalProductId)
        mWindows.DigitalProductId4 = Me.ProductKey(c_REGKEY__WINDOWS_KEY, c_REGVALUE__Windows_DigitalProductId4)
        mWindows.ProductId = Registry_GetValue(c_REGKEY__WINDOWS_KEY, c_REGVALUE__Windows_ProductId, "")
        mWindows.ProductName = Registry_GetValue(c_REGKEY__WINDOWS_KEY, c_REGVALUE__Windows_ProductName, "")
        mWindows.Version = Registry_GetValue(c_REGKEY__WINDOWS_KEY, c_REGVALUE__Windows_CurrentVersion, "")

        mOffice.DigitalProductId = Me.ProductKey(m_REGKEY__Office_Registration, c_REGVALUE__Office_DigitalProductId)
        mOffice.DigitalProductId4 = Me.ProductKey(m_REGKEY__Office_Registration, c_REGVALUE__Office_DigitalProductId4)
        mOffice.ProductId = Registry_GetValue(m_REGKEY__Office_Registration, c_REGVALUE__Office_ProductId, "")
        mOffice.ProductName = "MS Office"
        mOffice.Version = Registry_GetValue(m_REGKEY__Office_ProductVersion, c_REGVALUE__Office_CurrentVersion, "")
    End Sub

    Private Function Registry_GetValue(key As String, value As String, default_value As String)
        Try
            Return Registry.GetValue(key, value, default_value)
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


    Public Function ExportToXML() As String
        Dim xmlDoc As New XmlDocument

        Dim elemRoot As XmlElement = xmlDoc.CreateElement("guest-tek")
        xmlDoc.AppendChild(elemRoot)

        Dim elemOffice As XmlElement = xmlDoc.CreateElement("office")
        elemRoot.AppendChild(elemOffice)

        Dim elemOffice1 As XmlElement = xmlDoc.CreateElement("DigitalProductId")
        elemOffice1.InnerText = mOffice.DigitalProductId
        elemOffice.AppendChild(elemOffice1)
        Dim elemOffice2 As XmlElement = xmlDoc.CreateElement("DigitalProductId4")
        elemOffice2.InnerText = mOffice.DigitalProductId4
        elemOffice.AppendChild(elemOffice2)
        Dim elemOffice3 As XmlElement = xmlDoc.CreateElement("ProductId")
        elemOffice3.InnerText = mOffice.ProductId
        elemOffice.AppendChild(elemOffice3)
        Dim elemOffice4 As XmlElement = xmlDoc.CreateElement("ProductName")
        elemOffice4.InnerText = mOffice.ProductName
        elemOffice.AppendChild(elemOffice4)
        Dim elemOffice5 As XmlElement = xmlDoc.CreateElement("Version")
        elemOffice5.InnerText = mOffice.Version
        elemOffice.AppendChild(elemOffice5)

        Dim elemWindows As XmlElement = xmlDoc.CreateElement("windows")
        elemRoot.AppendChild(elemWindows)

        Dim elemWindows1 As XmlElement = xmlDoc.CreateElement("DigitalProductId")
        elemWindows1.InnerText = mWindows.DigitalProductId
        elemWindows.AppendChild(elemWindows1)
        Dim elemWindows2 As XmlElement = xmlDoc.CreateElement("DigitalProductId4")
        elemWindows2.InnerText = mWindows.DigitalProductId4
        elemWindows.AppendChild(elemWindows2)
        Dim elemWindows3 As XmlElement = xmlDoc.CreateElement("ProductId")
        elemWindows3.InnerText = mWindows.ProductId
        elemWindows.AppendChild(elemWindows3)
        Dim elemWindows4 As XmlElement = xmlDoc.CreateElement("ProductName")
        elemWindows4.InnerText = mWindows.ProductName
        elemWindows.AppendChild(elemWindows4)
        Dim elemWindows5 As XmlElement = xmlDoc.CreateElement("Version")
        elemWindows5.InnerText = mWindows.Version
        elemWindows.AppendChild(elemWindows5)


        Return xmlDoc.InnerXml
    End Function

    Private Sub FindOfficeRegKeys()
        Try
            Dim regKey As RegistryKey
            Dim tmpVal As Double = 0.0
            Dim sOfficeVer As String = ""

            'find office version
            regKey = Registry.LocalMachine.OpenSubKey(c_REGKEY__Office_KEY.Replace("HKEY_LOCAL_MACHINE\", ""))
            For Each subKeyName As String In regKey.GetSubKeyNames()
                If IsNumeric(subKeyName) Then
                    If CDbl(subKeyName) > tmpVal Then
                        sOfficeVer = subKeyName
                    End If
                End If
            Next

            m_REGKEY__Office_ProductVersion = c_REGKEY__Office_KEY & "\" & sOfficeVer & "\Common\ProductVersion"


            'find office info
            regKey = Registry.LocalMachine.OpenSubKey(c_REGKEY__Office_KEY & "\" & sOfficeVer & "\Registration")
            For Each subKeyName As String In regKey.GetSubKeyNames()
                Dim tmpKey As RegistryKey = regKey.OpenSubKey(subKeyName)
                Dim tmpScore As Integer = 0

                For Each valueName As String In tmpKey.GetValueNames()
                    If valueName.Equals(c_REGVALUE__Office_ProductId) Or valueName.Equals(c_REGVALUE__Office_DigitalProductId) Or valueName.Equals(c_REGVALUE__Office_DigitalProductId4) Then
                        tmpScore += 1
                    End If
                Next

                If tmpScore >= 2 Then
                    m_REGKEY__Office_Registration = c_REGKEY__Office_KEY & "\" & sOfficeVer & "\Registration\" & subKeyName

                    Exit For
                End If
            Next


        Catch ex As Exception

        End Try


        'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\14.0\Registration\{90140000-003D-0000-0000-0000000FF1CE}
        'ProductID 	REG_SZ (48) 	82503-OEM-1170471-58806
        'DigitalProductID 	REG_BINARY (1272) 	F8 04 00 00 04 00 00 00 38 00 32 00 35 00 30 00...

        'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Office\14.0\Common\ProductVersion
        'LastProduct 	REG_SZ (30) 	14.0.7015.1000

    End Sub

    Private Function ProductKey(ByVal KeyPath As String, ByVal ValueName As String) As String
        Try
            'Dim HexBuf As Object = My.Computer.Registry.GetValue(KeyPath, ValueName, 0)
            Dim HexBuf As Object = Registry.GetValue(KeyPath, ValueName, 0)
            If HexBuf Is Nothing Then Return "N/A"
            Dim tmp As String = ""
            For l As Integer = LBound(HexBuf) To UBound(HexBuf)
                tmp = tmp & " " & Hex(HexBuf(l))
            Next
            Dim Digits(24) As String
            Digits(0) = "B" : Digits(1) = "C" : Digits(2) = "D" : Digits(3) = "F"
            Digits(4) = "G" : Digits(5) = "H" : Digits(6) = "J" : Digits(7) = "K"
            Digits(8) = "M" : Digits(9) = "P" : Digits(10) = "Q" : Digits(11) = "R"
            Digits(12) = "T" : Digits(13) = "V" : Digits(14) = "W" : Digits(15) = "X"
            Digits(16) = "Y" : Digits(17) = "2" : Digits(18) = "3" : Digits(19) = "4"
            Digits(20) = "6" : Digits(21) = "7" : Digits(22) = "8" : Digits(23) = "9"

            Dim HexDigitalPID(15) As String
            Dim Des(30) As String
            Dim tmp2 As String = ""
            Dim StartOffset As Integer = 52, EndOffset As Integer = 67
            For i = StartOffset To EndOffset
                HexDigitalPID(i - StartOffset) = HexBuf(i)
                tmp2 = tmp2 & " " & Hex(HexDigitalPID(i - StartOffset))
            Next
            Dim KEYSTRING As String = ""
            Dim dLen As Integer = 29
            For i As Integer = dLen - 1 To 0 Step -1
                If ((i + 1) Mod 6) = 0 Then
                    Des(i) = "-"
                    KEYSTRING = KEYSTRING & "-"
                Else
                    Dim HN As Integer = 0
                    Dim sLen As Integer = 15
                    For N As Integer = (sLen - 1) To 0 Step -1
                        Dim Value As Integer = ((HN * 2 ^ 8) Or HexDigitalPID(N))
                        HexDigitalPID(N) = Value \ 24
                        HN = (Value Mod 24)
                    Next
                    Des(i) = Digits(HN)
                    KEYSTRING = KEYSTRING & Digits(HN)
                End If
            Next

            Return StrReverse(KEYSTRING)
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Class
