Module Main
    Public Sub Main()
        InitGlobals()


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.LogFilePath = g_LogFileDirectory

        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)



        '------------------------------------------------------
        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles = New Helpers.FilesAndFolders.CopyFiles With {
            .BackupDirectory = g_BackupDirectory,
            .SourceDirectory = "files",
            .DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder(True)
        }
        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("bye", , 1)
    End Sub
End Module
