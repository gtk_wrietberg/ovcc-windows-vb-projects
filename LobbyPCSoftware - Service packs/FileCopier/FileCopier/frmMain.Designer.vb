﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblText = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.tmrStart = New System.Windows.Forms.Timer(Me.components)
        Me.bgWork = New System.ComponentModel.BackgroundWorker()
        Me.tmrDone = New System.Windows.Forms.Timer(Me.components)
        Me.pBar = New System.Windows.Forms.ProgressBar()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblText
        '
        Me.lblText.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblText.Location = New System.Drawing.Point(185, 12)
        Me.lblText.Name = "lblText"
        Me.lblText.Size = New System.Drawing.Size(266, 67)
        Me.lblText.TabIndex = 1
        Me.lblText.Text = "One moment please"
        Me.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Image = Global.FileCopier.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(167, 67)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'tmrStart
        '
        Me.tmrStart.Interval = 500
        '
        'bgWork
        '
        '
        'tmrDone
        '
        Me.tmrDone.Interval = 5000
        '
        'pBar
        '
        Me.pBar.Location = New System.Drawing.Point(190, 62)
        Me.pBar.MarqueeAnimationSpeed = 50
        Me.pBar.Name = "pBar"
        Me.pBar.Size = New System.Drawing.Size(253, 17)
        Me.pBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.pBar.TabIndex = 2
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(455, 91)
        Me.ControlBox = False
        Me.Controls.Add(Me.pBar)
        Me.Controls.Add(Me.lblText)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "File Integrity v1"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PictureBox1 As Windows.Forms.PictureBox
    Friend WithEvents lblText As Windows.Forms.Label
    Friend WithEvents tmrStart As Windows.Forms.Timer
    Friend WithEvents bgWork As ComponentModel.BackgroundWorker
    Friend WithEvents tmrDone As Windows.Forms.Timer
    Friend WithEvents pBar As Windows.Forms.ProgressBar
End Class
