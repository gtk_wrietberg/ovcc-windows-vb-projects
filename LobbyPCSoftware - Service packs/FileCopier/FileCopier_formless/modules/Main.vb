Module Main
    Private mAppsToKill As List(Of String)

    Public Sub Main()
        InitGlobals()


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.LogFilePath = g_LogFileDirectory

        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        '------------------------------------------------------
        mAppsToKill = New List(Of String)

        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, "--kill-app:") > 0 Then
                mAppsToKill.Add(arg.Replace("--kill-app:", ""))
            End If
        Next


        '------------------------------------------------------
        'kill apps if specified
        Helpers.Logger.Write("Kill apps", , 0)

        If mAppsToKill.Count <= 0 Then
            Helpers.Logger.Write("No apps needed killing", , 1)
        Else
            Dim iKilled As Integer = 0

            For i As Integer = 0 To mAppsToKill.Count - 1
                Helpers.Logger.Write("killing", , 1)
                Helpers.Logger.Write(mAppsToKill.Item(i), , 2)

                iKilled = Helpers.Processes.KillProcesses(mAppsToKill.Item(i))
                If iKilled < 0 Then
                    'oops
                    Helpers.Logger.Write("FAIL!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Else
                    Helpers.Logger.Write("ok (x" & iKilled & ")", , 3)
                End If
            Next
        End If


        '------------------------------------------------------
        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles = New Helpers.FilesAndFolders.CopyFiles With {
            .BackupDirectory = g_BackupDirectory,
            .SourceDirectory = "files",
            .DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder(True)
        }
        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("bye", , 1)
    End Sub
End Module
