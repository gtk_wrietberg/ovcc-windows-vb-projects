using System;
using System.Collections.Generic;
using System.Text;

namespace ChangeDnsServer
{
    static class iExitCode
    {
        static int mExitCode =0;

        public static int Value
        {
            get { return mExitCode; }
            set { mExitCode = value; }
        }
    }
}
