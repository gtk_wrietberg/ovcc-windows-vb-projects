using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Management;
using System.Net;

namespace ChangeDnsServer
{
    public partial class formMain : Form
    {
        public formMain()
        {
            InitializeComponent();

            LoadCommandLineParameters();
        }

        private void ExitApplication(int iExitCode)
        {
            Environment.Exit(iExitCode);
        }

        private void LoadCommandLineParameters()
        {
            if (Environment.GetCommandLineArgs().Length == 3)
            {
                SetDnsServer(Environment.GetCommandLineArgs()[1], Environment.GetCommandLineArgs()[2]);
            }
            else
            {
                ExitApplication(-666);
            }
        }

        private void SetDnsServer(string host1, string host2)
        {
            ManagementClass nicManagement = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection nicList = nicManagement.GetInstances();

            List<string> dnsServerList = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(host1))
                {
                    IPAddress[] ipList = Dns.GetHostAddresses(host1);
                    foreach (IPAddress ip in ipList)
                    {
                        if(Ping(ip.ToString())) {
                            dnsServerList.Add(ip.ToString());
                        }
                    }
                }
                else
                {
                    ExitApplication(-100);
                }
            }
            catch (Exception)
            {
                ExitApplication(-110);
            }

            try
            {
                if (!string.IsNullOrEmpty(host2))
                {
                    IPAddress[] ipList = Dns.GetHostAddresses(host2);
                    foreach (IPAddress ip in ipList)
                    {
                        if (Ping(ip.ToString()))
                        {
                            dnsServerList.Add(ip.ToString());
                        }
                    }
                }
                else
                {
                    ExitApplication(-200);
                }
            }
            catch (Exception)
            {
                ExitApplication(-210);
            }

            if (dnsServerList.Count == 0)
            {
                ExitApplication(-300);
            }

            foreach (ManagementObject nic in nicList)
            {
                bool isIpEnabled = (bool)nic["ipEnabled"];

                if (isIpEnabled )
                {
                    ManagementBaseObject dnsServerSearchOrderParameters = nic.GetMethodParameters("SetDNSServerSearchOrder");
                    if (dnsServerSearchOrderParameters != null)
                    {
                        string nicDescription = nic.Properties["Description"].Value.ToString();
                        dnsServerSearchOrderParameters["DNSServerSearchOrder"] = dnsServerList.ToArray();
                        nic.InvokeMethod("SetDNSServerSearchOrder", dnsServerSearchOrderParameters, null);

                        ExitApplication(0);
                    }
                }
            }

            ExitApplication(-2);
        }

        private bool Ping(string host)
        {
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(host);
                return (reply.Status == IPStatus.Success);
            }
            catch (PingException)
            {
                return false;
            }
        }

        private void formMain_Load(object sender, EventArgs e)
        {
            this.Top = Screen.PrimaryScreen.Bounds.Height +100;
            this.Left = Screen.PrimaryScreen.Bounds.Width  + 100;
            this.Visible = false;
        }

    }
}