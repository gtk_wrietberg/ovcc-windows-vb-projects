﻿Public Class frmMain
    Private p As Process

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            If Not p.HasExited Then
                p.Kill()
            End If
        Catch ex As Exception

        Finally
            p.Dispose()
        End Try
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Visible = False
        Me.Hide()

        Dim sFile As String = Helpers.FilesAndFolders.GetProgramFilesFolder(False) & "\GuestTek\OVCCSoftware\tools\PrintQueueCleaner.exe"
        'Dim sFile As String = """C:\__temp\PrintQueueCleaner.exe"""
        'Dim sUsername As String = Helpers.PuppyPower.Username
        'Dim sPassword As String = Helpers.PuppyPower.Password
        Dim sUsername As String = "guesttek"
        Dim sPassword As String = "rancidkipper"

        Dim sParams As String = ""
        Dim sWorkDir As String = Helpers.FilesAndFolders.GetProgramFilesFolder(False) & "\GuestTek"

        p = New Process
        p = Helpers.Processes.StartProcessAsUser(sFile, sUsername, sPassword, sParams, sWorkDir)

        Try
            Dim iVoid As Integer = p.Id
            'ok

            tmrProcessCheck.Enabled = True
        Catch ex As Exception
            'fail

            MsgBox(Helpers.Errors.GetLast(False))

            Me.Close()
        End Try
    End Sub

    Private Sub tmrProcessCheck_Tick(sender As Object, e As EventArgs) Handles tmrProcessCheck.Tick
        Try
            If p.HasExited Then
                'close if child process has exited
                Me.Close()
            End If
        Catch ex As Exception
            'close if child process does not exist
            Me.Close()
        End Try
    End Sub
End Class
