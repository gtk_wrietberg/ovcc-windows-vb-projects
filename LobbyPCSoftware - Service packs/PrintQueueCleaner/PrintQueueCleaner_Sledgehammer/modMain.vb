﻿Imports System.ServiceProcess
Imports System.Security.Principal

Module modMain
    Private ReadOnly c__SERVICE_Spooler As String = "Spooler"
    Private ReadOnly c__FILE_PrintFilterPipelineSvc As String = "printfilterpipelinesvc"
    Private ReadOnly c__PATH_Printers As String = "%systemroot%\system32\spool\printers"

    Private ReadOnly c__TIMEOUT_ServiceController As Integer = 20
    Private ReadOnly c__TIMEOUT_Delay As Integer = 1000

    Public Sub Main()
        Dim sc As ServiceController
        Dim iStatusTimeout As Integer

        Console.WriteLine(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Console.WriteLine(" ")


        '--------------------------------------------------------------------

        Dim identity = WindowsIdentity.GetCurrent()
        Dim principal = New WindowsPrincipal(identity)
        Dim isElevated As Boolean = principal.IsInRole(WindowsBuiltInRole.Administrator)

        If Not isElevated Then
            Console.WriteLine("ERROR: Not running as an admin, exiting...")

            Exit Sub
        End If


        '--------------------------------------------------------------------

        sc = New ServiceController(c__SERVICE_Spooler)
        iStatusTimeout = 0

        Console.Write("stopping Spooler service ...")
        sc = New ServiceController("Spooler")
        Try
            sc.Stop()
        Catch ex As Exception
            Console.WriteLine("ERROR: Could not stop service ('" & ex.Message & "')")

            Exit Sub
        End Try

        Do While Not sc.Status = ServiceControllerStatus.Stopped And iStatusTimeout < c__TIMEOUT_ServiceController
            Console.Write(".")

            iStatusTimeout += 1
            Threading.Thread.Sleep(c__TIMEOUT_Delay)

            sc.Refresh()
        Loop

        If sc.Status = ServiceControllerStatus.Stopped Then
            Console.WriteLine(" ok")
        Else
            Console.WriteLine(" FAILED (status = '" & sc.Status.ToString & "')")
        End If


        Threading.Thread.Sleep(1000)
        '--------------------------------------------------------------------
        Dim ps() As Process = Process.GetProcessesByName(c__FILE_PrintFilterPipelineSvc)

        If ps.Count > 0 Then
            Console.Write("killing '" & c__FILE_PrintFilterPipelineSvc & "' process ...")

            For Each p As Process In ps
                Console.Write(".")
                p.Kill()
            Next

            Console.WriteLine(" ok")
        End If


        Threading.Thread.Sleep(1000)
        '--------------------------------------------------------------------

        Console.WriteLine("clearing spooler directory")

        Dim pathSpool As String = c__PATH_Printers.Replace("%systemroot%", Environment.GetEnvironmentVariable("systemroot"))
        Dim iFileCount As Integer = -1

        Try
            iFileCount = IO.Directory.GetFiles(pathSpool).Count
            If iFileCount = 1 Then
                Console.WriteLine("    found 1 file")
            Else
                Console.WriteLine("    found " & iFileCount.ToString & " files")
            End If
        Catch ex As Exception
            Console.WriteLine("    ERROR: " & ex.Message)
        End Try

        If iFileCount > 0 Then
            Try
                Console.Write("    deleting ")

                DeleteFilesFromFolder(pathSpool)

                Console.WriteLine(" ok")
            Catch ex As Exception
                Console.WriteLine(" ERROR: " & ex.Message)
            End Try
        Else
            Console.WriteLine("    no clearing needed")
        End If


        Threading.Thread.Sleep(1000)
        '--------------------------------------------------------------------

        sc = New ServiceController(c__SERVICE_Spooler)
        iStatusTimeout = 0


        Console.Write("starting Spooler service ...")
        sc = New ServiceController("Spooler")
        Try
            sc.Start()
        Catch ex As Exception
            Console.WriteLine("ERROR: Could not start service ('" & ex.Message & "')")
        End Try


        Do While Not sc.Status = ServiceControllerStatus.Running And iStatusTimeout < c__TIMEOUT_ServiceController
            Console.Write(".")

            iStatusTimeout += 1
            Threading.Thread.Sleep(c__TIMEOUT_Delay)

            sc.Refresh()
        Loop

        If sc.Status = ServiceControllerStatus.Running Then
            Console.WriteLine(" ok")
        Else
            Console.WriteLine(" FAILED (status = '" & sc.Status.ToString & "')")
        End If


        '--------------------------------------------------------------------

        Console.WriteLine("done")
    End Sub

    Private Sub DeleteFilesFromFolder(Folder As String)
        If IO.Directory.Exists(Folder) Then
            For Each _file As String In IO.Directory.GetFiles(Folder)
                Console.Write(".")
                IO.File.Delete(_file)
            Next

            For Each _folder As String In IO.Directory.GetDirectories(Folder)
                DeleteFilesFromFolder(_folder)
            Next
        End If
    End Sub
End Module
