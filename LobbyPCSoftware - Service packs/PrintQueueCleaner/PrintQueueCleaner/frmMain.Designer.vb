﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblTitleBar = New System.Windows.Forms.Label()
        Me.pnlInfo = New System.Windows.Forms.Panel()
        Me.lblPrintJobCount = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.pnlTitleBar = New System.Windows.Forms.Panel()
        Me.picTitleBar = New System.Windows.Forms.PictureBox()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.btnClearQueue = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.tmrPrintJobChecker = New System.Windows.Forms.Timer(Me.components)
        Me.pnlMessage = New System.Windows.Forms.Panel()
        Me.lblMessageContent = New System.Windows.Forms.Label()
        Me.btnMessageOk = New System.Windows.Forms.Button()
        Me.lblMessageTitle = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pnlInfo.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.pnlTitleBar.SuspendLayout()
        CType(Me.picTitleBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlButtons.SuspendLayout()
        Me.pnlMessage.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitleBar
        '
        Me.lblTitleBar.BackColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.lblTitleBar.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleBar.ForeColor = System.Drawing.Color.Black
        Me.lblTitleBar.Location = New System.Drawing.Point(3, 3)
        Me.lblTitleBar.Name = "lblTitleBar"
        Me.lblTitleBar.Size = New System.Drawing.Size(556, 35)
        Me.lblTitleBar.TabIndex = 1
        Me.lblTitleBar.Text = "Clear print queue"
        Me.lblTitleBar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.lblPrintJobCount)
        Me.pnlInfo.Controls.Add(Me.Label2)
        Me.pnlInfo.Location = New System.Drawing.Point(3, 53)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(564, 63)
        Me.pnlInfo.TabIndex = 2
        '
        'lblPrintJobCount
        '
        Me.lblPrintJobCount.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrintJobCount.Location = New System.Drawing.Point(3, 30)
        Me.lblPrintJobCount.Name = "lblPrintJobCount"
        Me.lblPrintJobCount.Size = New System.Drawing.Size(556, 24)
        Me.lblPrintJobCount.TabIndex = 1
        Me.lblPrintJobCount.Text = "X"
        Me.lblPrintJobCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(556, 30)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Jobs in printer queue"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.Gray
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.pnlTitleBar)
        Me.pnlMain.Controls.Add(Me.pnlButtons)
        Me.pnlMain.Controls.Add(Me.pnlInfo)
        Me.pnlMain.Location = New System.Drawing.Point(12, 13)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(572, 173)
        Me.pnlMain.TabIndex = 3
        '
        'pnlTitleBar
        '
        Me.pnlTitleBar.BackColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.pnlTitleBar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTitleBar.Controls.Add(Me.picTitleBar)
        Me.pnlTitleBar.Controls.Add(Me.lblTitleBar)
        Me.pnlTitleBar.Location = New System.Drawing.Point(3, 3)
        Me.pnlTitleBar.Name = "pnlTitleBar"
        Me.pnlTitleBar.Size = New System.Drawing.Size(564, 44)
        Me.pnlTitleBar.TabIndex = 6
        '
        'picTitleBar
        '
        Me.picTitleBar.BackgroundImage = Global.PrintQueueCleaner.My.Resources.Resources.guest_tek
        Me.picTitleBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.picTitleBar.Location = New System.Drawing.Point(3, 3)
        Me.picTitleBar.Name = "picTitleBar"
        Me.picTitleBar.Size = New System.Drawing.Size(64, 35)
        Me.picTitleBar.TabIndex = 0
        Me.picTitleBar.TabStop = False
        '
        'pnlButtons
        '
        Me.pnlButtons.BackColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.pnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlButtons.Controls.Add(Me.btnClearQueue)
        Me.pnlButtons.Controls.Add(Me.btnExit)
        Me.pnlButtons.Location = New System.Drawing.Point(3, 122)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(564, 45)
        Me.pnlButtons.TabIndex = 3
        '
        'btnClearQueue
        '
        Me.btnClearQueue.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearQueue.ForeColor = System.Drawing.Color.DarkRed
        Me.btnClearQueue.Location = New System.Drawing.Point(3, 3)
        Me.btnClearQueue.Name = "btnClearQueue"
        Me.btnClearQueue.Size = New System.Drawing.Size(145, 37)
        Me.btnClearQueue.TabIndex = 5
        Me.btnClearQueue.Text = "Clear queue"
        Me.btnClearQueue.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.ForeColor = System.Drawing.Color.Black
        Me.btnExit.Location = New System.Drawing.Point(414, 3)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(145, 37)
        Me.btnExit.TabIndex = 4
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'tmrPrintJobChecker
        '
        Me.tmrPrintJobChecker.Enabled = True
        Me.tmrPrintJobChecker.Interval = 2000
        '
        'pnlMessage
        '
        Me.pnlMessage.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlMessage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMessage.Controls.Add(Me.lblMessageContent)
        Me.pnlMessage.Controls.Add(Me.btnMessageOk)
        Me.pnlMessage.Controls.Add(Me.lblMessageTitle)
        Me.pnlMessage.Location = New System.Drawing.Point(658, 351)
        Me.pnlMessage.Name = "pnlMessage"
        Me.pnlMessage.Size = New System.Drawing.Size(382, 112)
        Me.pnlMessage.TabIndex = 4
        '
        'lblMessageContent
        '
        Me.lblMessageContent.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessageContent.Location = New System.Drawing.Point(3, 32)
        Me.lblMessageContent.Name = "lblMessageContent"
        Me.lblMessageContent.Size = New System.Drawing.Size(374, 46)
        Me.lblMessageContent.TabIndex = 2
        Me.lblMessageContent.Text = "Message title"
        Me.lblMessageContent.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnMessageOk
        '
        Me.btnMessageOk.Location = New System.Drawing.Point(158, 81)
        Me.btnMessageOk.Name = "btnMessageOk"
        Me.btnMessageOk.Size = New System.Drawing.Size(65, 24)
        Me.btnMessageOk.TabIndex = 1
        Me.btnMessageOk.Text = "OK"
        Me.btnMessageOk.UseVisualStyleBackColor = True
        '
        'lblMessageTitle
        '
        Me.lblMessageTitle.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMessageTitle.Location = New System.Drawing.Point(3, 0)
        Me.lblMessageTitle.Name = "lblMessageTitle"
        Me.lblMessageTitle.Size = New System.Drawing.Size(374, 32)
        Me.lblMessageTitle.TabIndex = 0
        Me.lblMessageTitle.Text = "Message title"
        Me.lblMessageTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(322, 273)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 100)
        Me.Panel1.TabIndex = 5
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LimeGreen
        Me.ClientSize = New System.Drawing.Size(1107, 553)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlMessage)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clear Print Queue"
        Me.pnlInfo.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlTitleBar.ResumeLayout(False)
        CType(Me.picTitleBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlButtons.ResumeLayout(False)
        Me.pnlMessage.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTitleBar As System.Windows.Forms.Label
    Friend WithEvents pnlInfo As System.Windows.Forms.Panel
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents pnlButtons As System.Windows.Forms.Panel
    Friend WithEvents btnClearQueue As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tmrPrintJobChecker As System.Windows.Forms.Timer
    Friend WithEvents pnlTitleBar As System.Windows.Forms.Panel
    Friend WithEvents picTitleBar As System.Windows.Forms.PictureBox
    Friend WithEvents pnlMessage As System.Windows.Forms.Panel
    Friend WithEvents lblMessageContent As System.Windows.Forms.Label
    Friend WithEvents btnMessageOk As System.Windows.Forms.Button
    Friend WithEvents lblMessageTitle As System.Windows.Forms.Label
    Friend WithEvents lblPrintJobCount As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel

End Class
