﻿Public Class frmMain

#Region " Move Form "
    Private MoveForm As Boolean
    Private MoveForm_MousePosition As Point

    Private Sub TitleBar_MouseDown(sender As Object, e As MouseEventArgs) Handles picTitleBar.MouseDown
        If e.Button = MouseButtons.Left Then
            MoveForm = True
            MoveForm_MousePosition = e.Location
        End If
    End Sub

    Private Sub TitleBar_MouseEnter(sender As Object, e As EventArgs) Handles picTitleBar.MouseEnter
        Me.Cursor = Cursors.NoMove2D
    End Sub

    Private Sub TitleBar_MouseLeave(sender As Object, e As EventArgs) Handles picTitleBar.MouseLeave
        Me.Cursor = Cursors.Default
    End Sub

    Public Sub TitleBar_MouseMove(sender As Object, e As MouseEventArgs) Handles picTitleBar.MouseMove
        If MoveForm Then
            Me.Location = Me.Location + (e.Location - MoveForm_MousePosition)
        End If
    End Sub

    Public Sub TitleBar_MouseUp(sender As Object, e As MouseEventArgs) Handles picTitleBar.MouseUp
        If e.Button = MouseButtons.Left Then
            MoveForm = False
        End If
    End Sub
#End Region

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.LimeGreen
        Me.Size = New Size(600, 200)

        'Me.btnClearQueue.FlatStyle = FlatStyle.System
        'Me.btnExit.FlatStyle = FlatStyle.System
        Helpers.Forms.TopMost(Me.Handle, True)

        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("Running as user", 0)
        Helpers.Logger.WriteMessage(Environment.UserName, 1)

        CountJobs()
    End Sub

    Private Sub ToggleClearButton()
        ToggleClearButton(Not Me.btnClearQueue.Enabled)
    End Sub

    Private Sub ToggleClearButton(state As Boolean)
        Me.btnClearQueue.Enabled = state
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private mButtonState_btnClearQueue As Boolean
    Private mButtonState_btnExit As Boolean
    Private Sub ShowMessagePanel(Title As String, Message As String, Optional ButtonText As String = "OK")
        mButtonState_btnClearQueue = btnClearQueue.Enabled
        mButtonState_btnExit = btnExit.Enabled

        btnClearQueue.Enabled = False
        btnExit.Enabled = False

        lblMessageTitle.Text = Title
        lblMessageContent.Text = Message
        btnMessageOk.Text = ButtonText
        btnMessageOk.Enabled = True

        'calc coords
        Dim x As Integer = 0, y As Integer = 0

        x = (Me.Size.Width - pnlMessage.Size.Width) / 2
        y = (Me.Size.Height - pnlMessage.Size.Height) / 2


        pnlMessage.Location = New Point(x, y)
        pnlMessage.Visible = True
    End Sub

    Private Sub HideMessagePanel()
        btnClearQueue.Enabled = mButtonState_btnClearQueue
        btnExit.Enabled = mButtonState_btnExit

        btnMessageOk.Enabled = False

        pnlMessage.Location = New Point(Me.Size.Width + 1, Me.Size.Height + 1)
        pnlMessage.Visible = False
    End Sub

    Private Sub ClearPrintQueue()
        'tmrPrintJobChecker.Enabled = False

        ShowMessagePanel("Purging print jobs", "It may take a few moments for the jobs to disappear from the list")

        Printing.PrinterManagement.CountJobs()

        Helpers.Logger.WriteMessage("Clearing printer queue", 0)
        Helpers.Logger.WriteMessage("jobs in queue / error", 1)
        Helpers.Logger.WriteMessage(Printing.PrinterManagement.JobCount_total.ToString & "/" & Printing.PrinterManagement.JobCount_error, 2)

        Helpers.Logger.WriteMessage("purging", 1)
        If Not Printing.PrinterManagement.PurgeAll() Then
            Helpers.Logger.WriteError("FAILED", 2)
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 3)
        Else
            Helpers.Logger.WriteMessage("ok", 2)
        End If

        'tmrPrintJobChecker.Enabled = True
    End Sub

    Private Sub CountJobs()
        Printing.PrinterManagement.CountJobs()

        btnClearQueue.Enabled = (Printing.PrinterManagement.JobCount_total > 0)
        mButtonState_btnClearQueue = (Printing.PrinterManagement.JobCount_total > 0)

        Dim sText As String = Printing.PrinterManagement.JobCount_total.ToString

        If Printing.PrinterManagement.JobCount_error > 0 Or Printing.PrinterManagement.JobCount_deleting > 0 Then
            sText &= " ("

            If Printing.PrinterManagement.JobCount_error > 0 Then
                If Printing.PrinterManagement.JobCount_error > 1 Then
                    sText &= Printing.PrinterManagement.JobCount_error.ToString & " errors"
                Else
                    sText &= "1 error"
                End If
            End If

            If Printing.PrinterManagement.JobCount_deleting > 0 Then
                If Printing.PrinterManagement.JobCount_error > 0 Then
                    sText &= " - "
                End If

                sText &= Printing.PrinterManagement.JobCount_deleting.ToString & " deleting"
            End If

            sText &= ")"
        End If

        If Printing.PrinterManagement.JobCount_error > 0 Then
            If Printing.PrinterManagement.JobCount_error > 1 Then
                lblPrintJobCount.Text = Printing.PrinterManagement.JobCount_total.ToString & " (" & Printing.PrinterManagement.JobCount_error.ToString & " errors)"
            Else
                lblPrintJobCount.Text = Printing.PrinterManagement.JobCount_total.ToString & " (1 error)"
            End If
        Else
            lblPrintJobCount.Text = Printing.PrinterManagement.JobCount_total.ToString
        End If
    End Sub

    Private Sub btnClearQueue_Click(sender As Object, e As EventArgs) Handles btnClearQueue.Click
        ClearPrintQueue()
    End Sub

    Private Sub tmrPrintJobChecker_Tick(sender As Object, e As EventArgs) Handles tmrPrintJobChecker.Tick
        CountJobs()
    End Sub

    Private Sub btnMessageOk_Click(sender As Object, e As EventArgs) Handles btnMessageOk.Click
        HideMessagePanel()
    End Sub
End Class
