﻿Imports System.Security.Principal

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'impersonatetest01()

        If Not Printing.PrinterManagement.PurgeAll() Then
            MsgBox(Helpers.Errors.GetLast())
        Else
            MsgBox("ok")
        End If
    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MsgBox(WindowsIdentity.GetCurrent().Name)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Helpers.Impersonate.Active Then
            If Not Helpers.Impersonate.StopImpersonation Then
                MsgBox(Helpers.Errors.GetLast())
            Else
                MsgBox("StopImpersonation ok")
            End If
        Else
            If Not Helpers.Impersonate.StartImpersonation("__OVCC_Puppy__", "Test1234") Then
                MsgBox(Helpers.Errors.GetLast())
            Else
                MsgBox("StartImpersonation ok")
            End If
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
