﻿Imports System.Security.Permissions
Imports System.Runtime.InteropServices
Imports Microsoft.Win32.SafeHandles
Imports System.Security
Imports System.Runtime.ConstrainedExecution
Imports System.Security.Principal

Module Module2
    'Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String], _
    '    ByVal lpszDomain As [String], ByVal lpszPassword As [String], _
    '    ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, _
    '    ByRef phToken As IntPtr) As Boolean

    Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String], _
        ByVal lpszDomain As [String], ByVal lpszPassword As [String], _
        ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, _
        <Out()> ByRef phToken As SafeTokenHandle) As Boolean

    Public Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean

    ' Test harness.
    ' If you incorporate this code into a DLL, be sure to demand FullTrust.
    '<PermissionSetAttribute(SecurityAction.Demand, Name:="FullTrust")> _
    Public Sub impersonatetest01()
        Dim safeTokenHandle As SafeTokenHandle
        Dim tokenHandle As New IntPtr(0)
        Try
            Dim userName As String, passWord As String, domainName As String

            ' Get the user token for the specified user, domain, and password using the 
            ' unmanaged LogonUser method.  
            ' The local machine name can be used for the domain name to impersonate a user on this machine.
            domainName = Environment.UserDomainName
            userName = "__OVCC_Puppy__"
            passWord = "Test1234"

            Const LOGON32_PROVIDER_DEFAULT As Integer = 0
            'This parameter causes LogonUser to create a primary token.
            Const LOGON32_LOGON_INTERACTIVE As Integer = 2

            ' Call LogonUser to obtain a handle to an access token.
            Dim returnValue As Boolean = LogonUser(userName, domainName, passWord, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, safeTokenHandle)

            Console.WriteLine("LogonUser called.")

            If False = returnValue Then
                Dim ret As Integer = Marshal.GetLastWin32Error()
                Console.WriteLine("LogonUser failed with error code : {0}", ret)
                Throw New System.ComponentModel.Win32Exception(ret)

                Return
            End If
            Using safeTokenHandle
                Dim success As String
                If returnValue Then success = "Yes" Else success = "No"
                Console.WriteLine(("Did LogonUser succeed? " + success))
                Console.WriteLine(("Value of Windows NT token: " + safeTokenHandle.DangerousGetHandle().ToString()))

                ' Check the identity.
                Console.WriteLine(("Before impersonation: " + WindowsIdentity.GetCurrent().Name))
                Console.WriteLine("Is admin? " + Helpers.Generic.IsRunningAsAdmin().ToString)

                ' Use the token handle returned by LogonUser.
                Using impersonatedUser As WindowsImpersonationContext = WindowsIdentity.Impersonate(safeTokenHandle.DangerousGetHandle())

                    ' Check the identity.
                    Console.WriteLine(("After impersonation: " + WindowsIdentity.GetCurrent().Name))
                    Console.WriteLine("Is admin? " + Helpers.Generic.IsRunningAsAdmin().ToString)


                    ' Free the tokens.
                End Using
            End Using
        Catch ex As Exception
            Console.WriteLine(("Exception occurred. " + ex.Message))
        End Try
    End Sub 'Main 

    Public NotInheritable Class SafeTokenHandle
        Inherits SafeHandleZeroOrMinusOneIsInvalid

        Private Sub New()
            MyBase.New(True)

        End Sub 'NewNew

        Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String], _
                ByVal lpszDomain As [String], ByVal lpszPassword As [String], _
                ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, _
                ByRef phToken As IntPtr) As Boolean
        <DllImport("kernel32.dll"), ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success), SuppressUnmanagedCodeSecurity()> _
        Private Shared Function CloseHandle(ByVal handle As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean

        End Function
        Protected Overrides Function ReleaseHandle() As Boolean
            Return CloseHandle(handle)

        End Function 'ReleaseHandle
    End Class 'SafeTokenHandle
End Module
