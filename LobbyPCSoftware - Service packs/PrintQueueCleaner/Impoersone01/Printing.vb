﻿Imports System.Printing

Public Class Printing
    Public Class PrinterManagement
        Public Shared Function PurgeAll() As Boolean
            Dim bRet As Boolean = True

            Try
                Using ps As New LocalPrintServer()
                    Using pq As New PrintQueue(ps, ps.DefaultPrintQueue.FullName, PrintSystemDesiredAccess.AdministratePrinter)
                        If pq.NumberOfJobs > 0 Then
                            pq.Purge()
                        End If
                    End Using
                End Using
            Catch ex As Exception
                Helpers.Errors.Add(ex.Message)

                bRet = False
            End Try

            Return bRet
        End Function

        Public Shared Function GetActiveJobs() As Integer
            Dim iRet As Integer = -1

            Try
                Using ps As New LocalPrintServer()
                    Using pq As New PrintQueue(ps, ps.DefaultPrintQueue.FullName, PrintSystemDesiredAccess.AdministratePrinter)
                        iRet = pq.NumberOfJobs
                    End Using
                End Using
            Catch ex As Exception
                Helpers.Errors.Add(ex.Message)
            End Try

            Return iRet
        End Function
    End Class

End Class
