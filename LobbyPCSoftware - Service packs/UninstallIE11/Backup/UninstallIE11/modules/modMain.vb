Module modMain
    Private WithEvents oProcess As ProcessRunner

    Public Sub Main()
        'FORFILES /P %WINDIR%\servicing\Packages /M Microsoft-Windows-InternetExplorer-*11.*.mum /c "cmd /c echo Uninstalling package @fname && start /w pkgmgr /up:@fname /norestart"

        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        Try
            Dim sUpdatesFolder As String = GetSpecialFolderPath(enuCSIDLPhysical.Windows) & cPackages__WindowsServicingPackages
            Dim aFiles() As String = IO.Directory.GetFiles(sUpdatesFolder, cPackages__FileWildcard)
            Dim sArguments As String = ""
            Dim iCount As Integer = 0

            For Each sFile As String In aFiles
                sArguments = cProcess__Arguments
                sArguments = sArguments.Replace("%%PROCESS%%", """" & sFile & """")
                sArguments = sArguments.Replace("%%LOGFILE%%", """" & oLogger.LogFileDirectory & "\" & oLogger.LogFileName & "_pkgmgr" & iCount.ToString & """")
                sArguments = sArguments.Replace("\\", "\")
                RunProcess(cProcess__Name, sArguments)

                iCount += 1
            Next

            oLogger.WriteToLog("bye bye", , 0)
            If iCount > 0 Then
                oLogger.WriteToLog("rebooting", , 1)
                WindowsController.ExitWindows(RestartOptions.Reboot, True)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("EPIC FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub

    Private Sub RunProcess(ByVal sFilename As String, ByVal sArguments As String)
        oLogger.WriteToLog("Starting process", , 1)
        oLogger.WriteToLog("Path   : " & sFilename, , 2)
        oLogger.WriteToLog("Params : " & sArguments, , 2)
        oLogger.WriteToLog("Timeout: 1800", , 2)

        oProcess = New ProcessRunner
        oProcess.FileName = sFilename
        oProcess.Arguments = sArguments
        oProcess.MaxTimeout = 1800
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()

        Do
            System.Threading.Thread.Sleep(500)
        Loop Until oProcess.IsProcessDone
    End Sub

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLog("done", , 1)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLog("failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_KillFailed(ByVal EventProcess As System.Diagnostics.Process, ByVal ErrorMessage As String) Handles oProcess.KillFailed
        oLogger.WriteToLog("killing process failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLog("started", , 1)
        oLogger.WriteToLog("id  : " & EventProcess.Id.ToString, , 2)
        oLogger.WriteToLog("name: " & EventProcess.ProcessName, , 2)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLog("timed out", , 1)

        oProcess.ProcessDone()
    End Sub
#End Region

End Module
