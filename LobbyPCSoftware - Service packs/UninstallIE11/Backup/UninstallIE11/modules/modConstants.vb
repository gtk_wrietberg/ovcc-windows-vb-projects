Module modConstants
    Public ReadOnly cPackages__WindowsServicingPackages As String = "\servicing\Packages"
    Public ReadOnly cPackages__FileWildcard As String = "Microsoft-Windows-InternetExplorer-*11.*.mum"

    Public ReadOnly cProcess__Name As String = "pkgmgr"
    Public ReadOnly cProcess__Arguments As String = "/up:%%PROCESS%% /quiet /norestart /l:%%LOGFILE%%"
End Module
