﻿Module modMain
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------
        Dim lUsernames As New List(Of String)
        Dim lPasswords As New List(Of String)
        Dim skPassword As String = ""

        oLogger.WriteToLog("Loading settings", , 0)
        oLogger.WriteToLog(cXML_SETTINGS, , 1)

        Try
            Dim mXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim mXmlNodeList_items As Xml.XmlNodeList
            Dim mXmlNode_item As Xml.XmlNode

            mXml = New Xml.XmlDocument
            mXml.Load(cXML_SETTINGS)
            oLogger.WriteToLog("ok", , 2)

            mXmlNode_Root = mXml.SelectSingleNode("settings")

            oLogger.WriteToLog("reading", , 1)

            Dim mXmlNode_users As Xml.XmlNode
            Dim mXmlNode_username As Xml.XmlNode
            Dim mXmlNode_password As Xml.XmlNode

            oLogger.WriteToLog("users node", , 2)
            mXmlNode_users = mXmlNode_Root.SelectSingleNode("users")

            oLogger.WriteToLog("user nodes", , 2)
            mXmlNodeList_items = mXmlNode_users.SelectNodes("user")
            For Each mXmlNode_item In mXmlNodeList_items
                mXmlNode_username = mXmlNode_item.SelectSingleNode("name")
                mXmlNode_password = mXmlNode_item.SelectSingleNode("pass")

                oLogger.WriteToLog("user", , 3)
                oLogger.WriteToLog("name", , 4)
                oLogger.WriteToLog(mXmlNode_username.InnerText, , 5)
                oLogger.WriteToLog("pass", , 4)
                oLogger.WriteToLog(mXmlNode_password.InnerText, , 5)

                lUsernames.Add(XOrObfuscation.Deobfuscate(mXmlNode_username.InnerText))
                lPasswords.Add(XOrObfuscation.Deobfuscate(mXmlNode_password.InnerText))
            Next


            Dim mXmlNode_SiteKiosk As Xml.XmlNode

            oLogger.WriteToLog("sitekiosk node", , 2)
            mXmlNode_SiteKiosk = mXmlNode_Root.SelectSingleNode("sitekiosk")

            oLogger.WriteToLog("pass node", , 2)
            mXmlNode_password = mXmlNode_SiteKiosk.SelectSingleNode("pass")
            oLogger.WriteToLog(mXmlNode_password.InnerText, , 5)

            skPassword = XOrObfuscation.Deobfuscate(mXmlNode_password.InnerText)
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try



        '---------------------------------------
        oLogger.WriteToLog("Changing passwords", , 0)
        For u = 0 To lUsernames.Count - 1
            Try
                _ChangePassword(lUsernames.Item(u), lPasswords.Item(u))
            Catch ex As Exception
                oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End Try
        Next


        '---------------------------------------
        oLogger.WriteToLog("Updating SiteKiosk config", , 0)
        Dim oSkCfg As New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Update SiteKiosk config", , 0)
        If (skPassword.Length < 3) Then
            oLogger.WriteToLog("Incorrect password (empty, or too short). Double check encrypted value in xml file!", , 0)
        Else
            oSkCfg.Update(skPassword, g_OverwriteExistingConfig)
        End If


        '---------------------------------------
        oLogger.WriteToLog("done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub

    Private Sub _ChangePassword(username As String, password As String)
        Dim sRet As String = ""

        oLogger.WriteToLogRelative("changing password", , 1)
        oLogger.WriteToLogRelative("user", , 1)
        oLogger.WriteToLogRelative(username, , 2)

        sRet = cWindowsUser.ChangePassword(username, password)

        If Not sRet.Equals("ok") Then
            oLogger.WriteToLogRelative("fail: " & sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        Else
            oLogger.WriteToLogRelative("ok", , 1)
        End If
    End Sub

    Private Sub Bye(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("Bye (" & iExitCode.ToString & ")", , 0)
        oLogger.WriteToLog(New String("=", 50))

        Environment.ExitCode = iExitCode
        Application.Exit()
    End Sub
End Module
