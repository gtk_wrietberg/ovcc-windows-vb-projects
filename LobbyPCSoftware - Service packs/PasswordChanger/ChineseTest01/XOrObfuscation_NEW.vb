Public Class XOrObfuscation_NEW
    Private Const cPassPhraseLength = 23
    Private Shared arrPass() As Integer
    Private Const cExtraPassPhraseString As String = "_()$%!�"
    Private Shared arrExtraPass() As Integer

    Private Shared mExtraPassphrase As Boolean = True
    Public Shared Property ExtraPassphrase() As Boolean
        Get
            Return mExtraPassphrase
        End Get
        Set(ByVal value As Boolean)
            mExtraPassphrase = value
        End Set
    End Property

    Public Shared Function Obfuscate(ByVal PlainText As String) As String
        Dim i As Integer, p As Integer
        Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

        InitializePasshrases()

        Try
            p = 0
            ObscuredText_RandomPassphrase = ""
            For i = 0 To cPassPhraseLength - 1
                ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
            Next
            For i = 0 To Len(PlainText) - 1
                ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(Ascw(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                p = p + 1
                If p >= cPassPhraseLength Then
                    p = 0
                End If
            Next

            If mExtraPassphrase Then
                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(Ascw(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(cExtraPassPhraseString) Then
                        p = 0
                    End If
                Next
            Else
                ObscuredText_ExtraPassphrase = ObscuredText_RandomPassphrase
            End If
        Catch ex As Exception
            ObscuredText_ExtraPassphrase = ""
        End Try

        Obfuscate = ObscuredText_ExtraPassphrase
    End Function

    Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
        Dim a() As Integer
        Dim i As Integer, j As Integer, p As Integer
        Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String

        InitializePasshrases()

        Try
            If mExtraPassphrase Then
                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & ChrW(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(cExtraPassPhraseString) Then
                        p = 0
                    End If
                Next
            Else
                PlainText_ExtraPassphrase = ObscuredText
            End If


            ReDim a(cPassPhraseLength)
            p = 0
            PlainText_RandomPassphrase = ""
            For i = 1 To cPassPhraseLength * 2 Step 2
                j = ((i + 1) / 2) - 1
                a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
            Next
            For i = cPassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                PlainText_RandomPassphrase = PlainText_RandomPassphrase & ChrW(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                p = p + 1
                If p >= cPassPhraseLength Then
                    p = 0
                End If
            Next
        Catch ex As Exception
            PlainText_RandomPassphrase = ""
        End Try

        Return PlainText_RandomPassphrase
    End Function

    Private Shared Sub InitializePasshrases()
        Dim i As Integer

        Randomize()

        ReDim arrPass(cPassPhraseLength)
        For i = 0 To cPassPhraseLength - 1
            arrPass(i) = Int(255 * Rnd())
        Next

        ReDim arrExtraPass(Len(cExtraPassPhraseString))
        For i = 0 To Len(cExtraPassPhraseString) - 1
            arrExtraPass(i) = Ascw(Mid(cExtraPassPhraseString, i + 1, 1))
        Next
    End Sub

    Private Shared Function _D2H(ByVal Value As Integer) As String
        Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
        Dim BinVal() As String

        iVal = Value
        Do
            temp = Int(iVal / 16)
            ret = iVal Mod 16
            ReDim Preserve BinVal(i)
            BinVal(i) = _NoToHex(ret)
            i = i + 1
            iVal = temp
        Loop While temp > 0
        For i = UBound(BinVal) To 0 Step -1
            Str = Str & CStr(BinVal(i))
        Next
        If Value < 16 Then
            Str = "0" & Str
        End If

        _D2H = Str
    End Function

    Private Shared Function _H2D(ByVal BinVal As String) As Integer
        Dim iVal As Integer, temp As Integer

        temp = _HexToNo(Mid(BinVal, 2, 1))
        iVal = iVal + temp
        temp = _HexToNo(Mid(BinVal, 1, 1))
        iVal = iVal + (temp * 16)

        _H2D = iVal
    End Function

    Private Shared Function _NoToHex(ByVal i As Integer) As String
        Select Case i
            Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                _NoToHex = CStr(i)
            Case 10
                _NoToHex = "a"
            Case 11
                _NoToHex = "b"
            Case 12
                _NoToHex = "c"
            Case 13
                _NoToHex = "d"
            Case 14
                _NoToHex = "e"
            Case 15
                _NoToHex = "f"
            Case Else
                _NoToHex = ""
        End Select
    End Function

    Private Shared Function _HexToNo(ByVal s As String) As Integer
        Select Case s
            Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                _HexToNo = CInt(s)
            Case "A", "a"
                _HexToNo = 10
            Case "B", "b"
                _HexToNo = 11
            Case "C", "c"
                _HexToNo = 12
            Case "D", "d"
                _HexToNo = 13
            Case "E", "e"
                _HexToNo = 14
            Case "F", "f"
                _HexToNo = 15
            Case Else
                _HexToNo = -1
        End Select
    End Function
End Class
