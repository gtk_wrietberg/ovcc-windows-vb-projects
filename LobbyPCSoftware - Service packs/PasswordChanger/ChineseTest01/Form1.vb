﻿Public Class Form1

    Private Sub btnStep1_Click(sender As Object, e As EventArgs) Handles btnStep1.Click
        btnStep1.Enabled = False

        txtResult1_1.Text = XOrObfuscation.Obfuscate(txtSource1.Text)
        txtResult1_2.Text = XOrObfuscation_NEW.Obfuscate(txtSource1.Text)

        btnStep2.Enabled = True
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        btnStep1.Enabled = False
        btnStep2.Enabled = False
        btnStep3.Enabled = False
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        btnStep1.Enabled = True
        btnStep2.Enabled = False

        txtSource1.Text = "Test String"
    End Sub

    Private Sub btnStep2_Click(sender As Object, e As EventArgs) Handles btnStep2.Click
        btnStep2.Enabled = False

        txtResult2_1.Text = XOrObfuscation.Deobfuscate(txtResult1_1.Text)
        txtResult2_2.Text = XOrObfuscation_NEW.Deobfuscate(txtResult1_2.Text)
    End Sub

    Private Sub btnClear2_Click(sender As Object, e As EventArgs) Handles btnClear2.Click
        btnStep3.Enabled = True

        txtSource2.Text = "6d1048134311913a4c101543139f6c19101d1519946c4e4d454440c66f4b1e101d10c5671e48141413906d1e1d171110c46a111814134390"

    End Sub

    Private Sub btnStep3_Click(sender As Object, e As EventArgs) Handles btnStep3.Click
        btnStep3.Enabled = False

        txtResult3_1.Text = XOrObfuscation.Deobfuscate(txtSource2.Text)
        txtResult3_2.Text = XOrObfuscation_NEW.Deobfuscate(txtSource2.Text)
    End Sub
End Class
