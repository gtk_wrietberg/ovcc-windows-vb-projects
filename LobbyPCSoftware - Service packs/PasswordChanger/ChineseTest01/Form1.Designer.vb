﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnStep1 = New System.Windows.Forms.Button()
        Me.txtSource1 = New System.Windows.Forms.TextBox()
        Me.txtResult1_1 = New System.Windows.Forms.TextBox()
        Me.btnStep2 = New System.Windows.Forms.Button()
        Me.txtResult1_2 = New System.Windows.Forms.TextBox()
        Me.txtResult2_2 = New System.Windows.Forms.TextBox()
        Me.txtResult2_1 = New System.Windows.Forms.TextBox()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtSource2 = New System.Windows.Forms.TextBox()
        Me.btnClear2 = New System.Windows.Forms.Button()
        Me.btnStep3 = New System.Windows.Forms.Button()
        Me.txtResult3_1 = New System.Windows.Forms.TextBox()
        Me.txtResult3_2 = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnStep1
        '
        Me.btnStep1.Location = New System.Drawing.Point(14, 40)
        Me.btnStep1.Name = "btnStep1"
        Me.btnStep1.Size = New System.Drawing.Size(224, 32)
        Me.btnStep1.TabIndex = 0
        Me.btnStep1.Text = "step 1"
        Me.btnStep1.UseVisualStyleBackColor = True
        '
        'txtSource1
        '
        Me.txtSource1.Location = New System.Drawing.Point(14, 14)
        Me.txtSource1.Name = "txtSource1"
        Me.txtSource1.Size = New System.Drawing.Size(224, 20)
        Me.txtSource1.TabIndex = 1
        Me.txtSource1.Text = "test string"
        '
        'txtResult1_1
        '
        Me.txtResult1_1.BackColor = System.Drawing.Color.White
        Me.txtResult1_1.Location = New System.Drawing.Point(14, 78)
        Me.txtResult1_1.Name = "txtResult1_1"
        Me.txtResult1_1.ReadOnly = True
        Me.txtResult1_1.Size = New System.Drawing.Size(583, 20)
        Me.txtResult1_1.TabIndex = 2
        '
        'btnStep2
        '
        Me.btnStep2.Location = New System.Drawing.Point(14, 130)
        Me.btnStep2.Name = "btnStep2"
        Me.btnStep2.Size = New System.Drawing.Size(224, 32)
        Me.btnStep2.TabIndex = 3
        Me.btnStep2.Text = "step 2"
        Me.btnStep2.UseVisualStyleBackColor = True
        '
        'txtResult1_2
        '
        Me.txtResult1_2.BackColor = System.Drawing.Color.White
        Me.txtResult1_2.Location = New System.Drawing.Point(14, 104)
        Me.txtResult1_2.Name = "txtResult1_2"
        Me.txtResult1_2.ReadOnly = True
        Me.txtResult1_2.Size = New System.Drawing.Size(583, 20)
        Me.txtResult1_2.TabIndex = 4
        '
        'txtResult2_2
        '
        Me.txtResult2_2.BackColor = System.Drawing.Color.White
        Me.txtResult2_2.Location = New System.Drawing.Point(14, 194)
        Me.txtResult2_2.Name = "txtResult2_2"
        Me.txtResult2_2.ReadOnly = True
        Me.txtResult2_2.Size = New System.Drawing.Size(583, 20)
        Me.txtResult2_2.TabIndex = 6
        '
        'txtResult2_1
        '
        Me.txtResult2_1.BackColor = System.Drawing.Color.White
        Me.txtResult2_1.Location = New System.Drawing.Point(14, 168)
        Me.txtResult2_1.Name = "txtResult2_1"
        Me.txtResult2_1.ReadOnly = True
        Me.txtResult2_1.Size = New System.Drawing.Size(583, 20)
        Me.txtResult2_1.TabIndex = 5
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(521, 14)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 23)
        Me.btnClear.TabIndex = 7
        Me.btnClear.Text = "clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtSource1)
        Me.Panel1.Controls.Add(Me.btnClear)
        Me.Panel1.Controls.Add(Me.btnStep1)
        Me.Panel1.Controls.Add(Me.txtResult2_2)
        Me.Panel1.Controls.Add(Me.txtResult1_1)
        Me.Panel1.Controls.Add(Me.txtResult2_1)
        Me.Panel1.Controls.Add(Me.btnStep2)
        Me.Panel1.Controls.Add(Me.txtResult1_2)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(615, 235)
        Me.Panel1.TabIndex = 8
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.txtSource2)
        Me.Panel2.Controls.Add(Me.btnClear2)
        Me.Panel2.Controls.Add(Me.btnStep3)
        Me.Panel2.Controls.Add(Me.txtResult3_1)
        Me.Panel2.Controls.Add(Me.txtResult3_2)
        Me.Panel2.Location = New System.Drawing.Point(12, 268)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(615, 138)
        Me.Panel2.TabIndex = 9
        '
        'txtSource2
        '
        Me.txtSource2.Location = New System.Drawing.Point(14, 14)
        Me.txtSource2.Name = "txtSource2"
        Me.txtSource2.Size = New System.Drawing.Size(582, 20)
        Me.txtSource2.TabIndex = 1
        Me.txtSource2.Text = "6d1048134311913a4c101543139f6c19101d1519946c4e4d454440c66f4b1e101d10c5671e4814141" & _
    "3906d1e1d171110c46a111814134390"
        '
        'btnClear2
        '
        Me.btnClear2.Location = New System.Drawing.Point(521, 40)
        Me.btnClear2.Name = "btnClear2"
        Me.btnClear2.Size = New System.Drawing.Size(75, 23)
        Me.btnClear2.TabIndex = 7
        Me.btnClear2.Text = "clear"
        Me.btnClear2.UseVisualStyleBackColor = True
        '
        'btnStep3
        '
        Me.btnStep3.Location = New System.Drawing.Point(14, 40)
        Me.btnStep3.Name = "btnStep3"
        Me.btnStep3.Size = New System.Drawing.Size(224, 32)
        Me.btnStep3.TabIndex = 0
        Me.btnStep3.Text = "step 1"
        Me.btnStep3.UseVisualStyleBackColor = True
        '
        'txtResult3_1
        '
        Me.txtResult3_1.BackColor = System.Drawing.Color.White
        Me.txtResult3_1.Location = New System.Drawing.Point(14, 78)
        Me.txtResult3_1.Name = "txtResult3_1"
        Me.txtResult3_1.ReadOnly = True
        Me.txtResult3_1.Size = New System.Drawing.Size(583, 20)
        Me.txtResult3_1.TabIndex = 2
        '
        'txtResult3_2
        '
        Me.txtResult3_2.BackColor = System.Drawing.Color.White
        Me.txtResult3_2.Location = New System.Drawing.Point(14, 104)
        Me.txtResult3_2.Name = "txtResult3_2"
        Me.txtResult3_2.ReadOnly = True
        Me.txtResult3_2.Size = New System.Drawing.Size(583, 20)
        Me.txtResult3_2.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(638, 420)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnStep1 As System.Windows.Forms.Button
    Friend WithEvents txtSource1 As System.Windows.Forms.TextBox
    Friend WithEvents txtResult1_1 As System.Windows.Forms.TextBox
    Friend WithEvents btnStep2 As System.Windows.Forms.Button
    Friend WithEvents txtResult1_2 As System.Windows.Forms.TextBox
    Friend WithEvents txtResult2_2 As System.Windows.Forms.TextBox
    Friend WithEvents txtResult2_1 As System.Windows.Forms.TextBox
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtSource2 As System.Windows.Forms.TextBox
    Friend WithEvents btnClear2 As System.Windows.Forms.Button
    Friend WithEvents btnStep3 As System.Windows.Forms.Button
    Friend WithEvents txtResult3_1 As System.Windows.Forms.TextBox
    Friend WithEvents txtResult3_2 As System.Windows.Forms.TextBox

End Class
