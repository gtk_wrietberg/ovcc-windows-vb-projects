﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        chkExtraPass.Checked = XOrObfuscation.ExtraPassphrase
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        XOrObfuscation.ExtraPassphrase = chkExtraPass.Checked

        TextBox2.Text = XOrObfuscation.Obfuscate(TextBox1.Text)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        XOrObfuscation.ExtraPassphrase = chkExtraPass.Checked


        TextBox3.Text = XOrObfuscation.Deobfuscate(TextBox2.Text)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            If TextBox2.TextLength > 0 Then
                Clipboard.SetText(TextBox2.Text)

                If Clipboard.GetText().Equals(TextBox2.Text) Then
                    Button3.ForeColor = Color.Green
                Else
                    Button3.ForeColor = Color.Yellow
                End If
            End If
        Catch ex As Exception
            Button3.ForeColor = Color.Red
        End Try

        tmrButtonColour.Enabled = True
    End Sub

    Private Sub tmrButtonColour_Tick(sender As Object, e As EventArgs) Handles tmrButtonColour.Tick
        tmrButtonColour.Enabled = False

        Button3.ForeColor = Color.Black
    End Sub
End Class
