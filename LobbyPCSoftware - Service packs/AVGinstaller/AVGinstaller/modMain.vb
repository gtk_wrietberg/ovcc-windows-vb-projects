﻿Module modMain
    Private WithEvents oProcess As ProcessRunner

    Public Sub InstallAvgStuff()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------
        Dim sSetupCmd As String = "", sCfgFileArguments As String = ""
        Dim App_Path As String

        App_Path = New System.IO.FileInfo(Application.ExecutablePath).DirectoryName

        sSetupCmd = "files\avg_ipw_x86_all_2013_3495a8522.exe"
        sSetupCmd = "files\avg_ipw_x64_all_2013_3495a8522.exe"
        sSetupCmd = App_Path & "\" & sSetupCmd

        sCfgFileArguments = "files\AvgSetup.ini"
        sCfgFileArguments = App_Path & "\" & sCfgFileArguments
        sCfgFileArguments = "/ConfigFilePath=""" & sCfgFileArguments & """"


        '------------------------------
        Dim oProcess As ProcessRunner

        oLogger.WriteToLogRelative("Installing Flash", , 1)
        oLogger.WriteToLogRelative("Path   : " & sSetupCmd, , 2)
        oLogger.WriteToLogRelative("Params : " & sCfgFileArguments, , 2)
        oLogger.WriteToLogRelative("Timeout: 300000ms", , 2)

        oProcess = New ProcessRunner
        oProcess.FileName = sSetupCmd
        oProcess.Arguments = sCfgFileArguments
        'oProcess.ExternalAppToWaitFor = "InstallFlashPlayer"
        oProcess.MaxTimeout = 1200000
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub
End Module
