Module Globals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String

    Public g_OverwriteExistingConfig As Boolean

    Public g_TESTMODE As Boolean = True


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")



        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception
            MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_LogFileDirectory & "'): " & ex.Message)
        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception
            MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_BackupDirectory & "'): " & ex.Message)
        End Try

        g_OverwriteExistingConfig = True
    End Sub
End Module
