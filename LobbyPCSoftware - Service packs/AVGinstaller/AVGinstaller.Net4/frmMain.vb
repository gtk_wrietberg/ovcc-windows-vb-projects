﻿Public Class frmMain
    Private WithEvents oProcess As ProcessRunner

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        '------------------------------
        Dim sSetupArg As String = ""

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg = "--safety-off" Then
                g_TESTMODE = False
            End If

            If arg.Contains("--avg:") Then
                sSetupArg = arg.Replace("--avg:", "")
            End If
        Next


        '------------------------------
        Dim sSetupCmd As String = "", sCfgFileArguments As String = ""
        Dim App_Path As String

        App_Path = New System.IO.FileInfo(Application.ExecutablePath).DirectoryName

        'sSetupCmd = "files\avg_ipw_x86_all_2013_3495a8522.exe"
        'sSetupCmd = "files\avg_ipw_x64_all_2013_3495a8522.exe"
        sSetupCmd = sSetupArg
        sSetupCmd = App_Path & "\" & sSetupCmd

        sCfgFileArguments = "files\AvgSetup.ini"
        sCfgFileArguments = App_Path & "\" & sCfgFileArguments
        sCfgFileArguments = "/ConfigFilePath=""" & sCfgFileArguments & """"

        If Not IO.File.Exists(sSetupCmd) Then
            oLogger.WriteToLog("""" & sSetupCmd & """ does not exist!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 0)
            Application.Exit()
        Else
            oLogger.WriteToLogRelative("Installing AVG", , 1)
            oLogger.WriteToLogRelative("Path   : " & sSetupCmd, , 2)
            oLogger.WriteToLogRelative("Params : " & sCfgFileArguments, , 2)
            oLogger.WriteToLogRelative("Timeout: 1200000ms", , 2)

            oProcess = New ProcessRunner
            oProcess.FileName = sSetupCmd
            oProcess.Arguments = sCfgFileArguments
            'oProcess.ExternalAppToWaitFor = "InstallFlashPlayer"
            oProcess.MaxTimeout = 1200000
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()
        End If
    End Sub

    Private Sub PostInstall()
        oLogger.WriteToLog("Done", , 0)


        If g_TESTMODE Then
            oLogger.WriteToLog("Rebooting (not!)", , 1)
            Me.Close()
        Else
            oLogger.WriteToLog("Rebooting", , 1)
            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        End If
    End Sub


#Region "Process Events"
    Private Sub oProcess_Done(ByVal ExitCode As Integer, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLogRelative("done", , 2)
        oLogger.WriteToLogRelative("time elapsed: " & TimeElapsed.ToString & " sec", , 3)
        oLogger.WriteToLogRelative("exit code: " & ExitCode.ToString, , 3)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLogRelative("failed", , 2)
        oLogger.WriteToLogRelative(ErrorMessage, , 3)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal pId As Integer, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLogRelative("started", , 2)
        oLogger.WriteToLogRelative("pid: " & pId.ToString, , 3)
    End Sub
#End Region

    Private Sub tmrInstallerWait_Tick(sender As Object, e As EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            PostInstall()
        End If
    End Sub
End Class