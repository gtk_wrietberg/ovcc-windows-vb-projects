﻿Imports System.Threading

Module modMain
    Private bDidIDoSomething As Boolean = False


    Private Enum InstallationProgress As Integer
        UninstallLogMeIn = 0
        UninstallLogMeInClient
        Done
    End Enum


    Public Sub Main()
        Dim sReturnMessage As String = ""
        Dim oProcess As ProcessRunnerWithoutEvents
        Dim sParams As String = ""
        Dim sId As String = ""

        If Helpers.Generic.IsDevMachine Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If

        End If


        InitGlobals()


        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))
        oLogger.WriteToLog(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


        If gTestMode Then
            oLogger.WriteToLog("TEST MODE!!!!!", , 0)
        End If


        Dim sArg As String, sTmp As String = ""

        oLogger.WriteToLog("Command line parameters", , 0)
        For Each sArg In My.Application.CommandLineArgs
            sArg = sArg.ToLower

            oLogger.WriteToLog("found: " & sArg, , 1)

            If sArg = "--safety-off" Then
                gSafetyOff = True
            End If

            If sArg = "--force-reboot" Then
                gForceReboot = True
            End If
        Next


        If gTestMode Then
            ExitCode.SetValue(ExitCode.ExitCodes.TEST_MODE)
        End If

        If Not gSafetyOff Then
            ExitCode.SetValue(ExitCode.ExitCodes.SAFETY_STILL_ON)
            oLogger.WriteToLog("Safety still on, cancelling...")
            ApplicationExit()

            Exit Sub
        End If


        oLogger.WriteToLog("Uninstalling remote access application")

        sId = ProductGUID.GetUninstallIdentifier("logmein")
        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        Else
            bDidIDoSomething = True

            sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

            oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
            oLogger.WriteToLog("Params : " & sParams, , 1)
            oLogger.WriteToLog("Timeout: 300", , 1)

            oProcess = New ProcessRunnerWithoutEvents
            oProcess.TestMode = gTestMode
            oProcess.FileName = cUNINSTALL_COMMAND
            oProcess.Arguments = sParams
            oProcess.MaxTimeout = 300
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden

            If oProcess.StartProcess(sReturnMessage) Then
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog(sReturnMessage, , 3)
            End If

            oLogger.WriteToLog("time elapsed: " & oProcess.ElapsedTime & " sec", 1)
        End If


        oLogger.WriteToLog("Uninstalling remote access application (client)")

        sId = ProductGUID.GetUninstallIdentifier("logmein client")
        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        Else
            bDidIDoSomething = True

            sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

            oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
            oLogger.WriteToLog("Params : " & sParams, , 1)
            oLogger.WriteToLog("Timeout: 300", , 1)

            oProcess = New ProcessRunnerWithoutEvents
            oProcess.TestMode = gTestMode
            oProcess.FileName = cUNINSTALL_COMMAND
            oProcess.Arguments = sParams
            oProcess.MaxTimeout = 300
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden

            If oProcess.StartProcess(sReturnMessage) Then
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog(sReturnMessage, , 3)
            End If

            oLogger.WriteToLog("time elapsed: " & oProcess.ElapsedTime & " sec", 1)
        End If


        ApplicationExit()
    End Sub


    Private Sub ApplicationExit()
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("exit code", , 1)
        oLogger.WriteToLog(CStr(ExitCode.GetValue()), , 2)
        oLogger.WriteToLog(ExitCode.ToString(), , 3)

        If gSafetyOff Then
            oLogger.WriteToLog("rebooting", , 1)
            If gForceReboot Then
                If bDidIDoSomething Then
                    If gTestMode Then
                        oLogger.WriteToLog("WindowsController.ExitWindows(RestartOptions.Reboot, True)", , 1)
                    Else
                        WindowsController.ExitWindows(RestartOptions.Reboot, True)
                    End If
                Else
                    oLogger.WriteToLog("skipped, because nothing happened", , 2)
                End If
            Else
                oLogger.WriteToLog("skipped, not forced", , 2)
            End If
        End If

        oLogger.WriteToLog("bye", , 1)

        Environment.ExitCode = ExitCode.GetValue()
        Application.Exit()
    End Sub
End Module
