﻿Imports System.Threading

Public Class ProcessRunnerWithoutEvents
    Private WithEvents myProcess As New System.Diagnostics.Process
    Private mElapsedTime As Double

    Private mTestMode As Boolean

    Private mFileName As String
    Private mArguments As String
    Private mUserName As String
    Private mPassWord As String
    Private mExternalAppToWaitFor As String

    Private Const OneSec As Double = 1.0# / (1440.0# * 60.0#)
    Private Const cSleepAmount As Double = 0.2#


    Private mMaxTimeout As Double = 30

    Private mProcessStyle As ProcessWindowStyle


#Region "Properties"
    Public Property TestMode() As Boolean
        Get
            Return mTestMode
        End Get
        Set(ByVal value As Boolean)
            mTestMode = value
        End Set
    End Property

    Public Property MaxTimeout() As Double
        Get
            Return mMaxTimeout
        End Get
        Set(ByVal value As Double)
            mMaxTimeout = value
        End Set
    End Property

    Public Property ProcessStyle() As ProcessWindowStyle
        Get
            Return mProcessStyle
        End Get
        Set(ByVal value As ProcessWindowStyle)
            mProcessStyle = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return mFileName
        End Get
        Set(ByVal value As String)
            mFileName = value
        End Set
    End Property

    Public Property Arguments() As String
        Get
            Return mArguments
        End Get
        Set(ByVal value As String)
            mArguments = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set
    End Property

    Public Property PassWord() As String
        Get
            Return mPassWord
        End Get
        Set(ByVal value As String)
            mPassWord = value
        End Set
    End Property

    Public Property ExternalAppToWaitFor() As String
        Get
            Return mExternalAppToWaitFor
        End Get
        Set(ByVal value As String)
            mExternalAppToWaitFor = value
        End Set
    End Property

    Public ReadOnly Property ElapsedTime As Double
        Get
            Return mElapsedTime
        End Get
    End Property
#End Region

    Public Sub New()
        mTestMode = False
        mMaxTimeout = 30000
        mProcessStyle = ProcessWindowStyle.Normal

        mFileName = ""
        mArguments = ""
        mUserName = ""
        mPassWord = ""
        mExternalAppToWaitFor = ""
    End Sub

    Public Function StartProcess(ByRef ReturnMessage As String) As Boolean
        Return StartProcess(mFileName, mArguments, ReturnMessage)
    End Function

    Public Function StartProcess(ByVal FileName As String, ByVal Arguments As String, ByRef ReturnMessage As String) As Boolean
        Dim bRet As Boolean = False

        mElapsedTime = 0.0#

        If mTestMode Then
            ReturnMessage = "TESTMODE! I would have started '" & FileName & "' with arguments '" & Arguments & "' !"

            Return True

            Exit Function
        End If

        Try
            myProcess.StartInfo.FileName = FileName
            myProcess.StartInfo.Arguments = Arguments
            If mUserName <> "" Then
                Dim securePass As New Security.SecureString()

                For Each c As Char In mPassWord
                    securePass.AppendChar(c)
                Next c

                myProcess.StartInfo.UseShellExecute = False
                myProcess.StartInfo.UserName = mUserName
                myProcess.StartInfo.Password = securePass
            End If
            myProcess.StartInfo.WindowStyle = mProcessStyle
            myProcess.EnableRaisingEvents = True
            myProcess.Start()
        Catch ex As Exception
            ReturnMessage = "ERROR starting: " & ex.Message

            Return False

            Exit Function
        End Try

        Try
            If myProcess.Id <= 0 Then
                ReturnMessage = "ERROR pid: <=0"

                Return False

                Exit Function
            End If
        Catch ex As Exception
            ReturnMessage = "ERROR pid: " & ex.Message

            Return False

            Exit Function
        End Try

        bRet = True
        ReturnMessage = "ok"
        Do While _TimeoutLoop()
            mElapsedTime += cSleepAmount

            mElapsedTime = Math.Round(mElapsedTime, 2)

            If mElapsedTime > mMaxTimeout Then
                bRet = False

                ReturnMessage = "ERROR timeout: " & _StopProcess()

                Exit Do
            End If

            Thread.Sleep(cSleepAmount * 1000)
        Loop

        Return bRet
    End Function

    Private Function _TimeoutLoop() As Boolean
        Dim bLoop As Boolean

        bLoop = Not myProcess.HasExited

        If mExternalAppToWaitFor <> "" Then
            If Process.GetProcessesByName(mExternalAppToWaitFor).Length > 0 Then
                bLoop = True
            End If
        End If

        Return bLoop
    End Function

    Private Function _StopProcess() As String
        Dim sRet As String = ""

        Try
            myProcess.Kill()

            sRet = "process killed"
        Catch ex As Exception
            sRet = ex.Message
        End Try

        Return sRet
    End Function
End Class
