﻿Public Class clsEventLog
    Private Shared ReadOnly c_Event_LogName As String = "FirefoxProfileSwitcher"
    Private Shared ReadOnly c_Event_Source As String = "GuestTek"

    Private Shared ReadOnly c_Event_MachineName As String = "."

    Public Shared Function WriteInfo(EventText As String) As Boolean
        Return WriteEvent(EventText, EventLogEntryType.Information)
    End Function

    Public Shared Function WriteWarning(EventText As String) As Boolean
        Return WriteEvent(EventText, EventLogEntryType.Warning)
    End Function

    Public Shared Function WriteError(EventText As String) As Boolean
        Return WriteEvent(EventText, EventLogEntryType.Error)
    End Function

    Public Shared Function Register() As Boolean
        Try
            If Not EventLog.SourceExists(c_Event_Source, c_Event_MachineName) Then
                Dim enew As New EventSourceCreationData(c_Event_Source, c_Event_LogName)

                enew.Source = c_Event_Source
                enew.LogName = c_Event_LogName
                enew.MachineName = c_Event_MachineName

                EventLog.CreateEventSource(enew)
            End If

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Shared Function WriteEvent(EventText As String, EventType As EventLogEntryType) As Boolean
        Try
            Dim ELog As New EventLog(c_Event_LogName, c_Event_MachineName, c_Event_Source)

            ELog.WriteEntry(EventText, EventType, 1000)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
