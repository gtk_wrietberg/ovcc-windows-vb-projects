﻿Imports System.Runtime.InteropServices
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports Microsoft.Win32

Public Class Helpers_old
#Region "Types"

    Public Class Types
        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function


        Public Shared Function RandomString(Length As Integer, Optional CharsToChooseFrom As String = "") As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function
    End Class
#End Region

#Region "Registry"
    Public Class Registry
#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SetValue_Boolean(keyName As String, valueName As String, value As Boolean)
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "yes", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "no", Microsoft.Win32.RegistryValueKind.String)
                End If
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "string"
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            sTmp = Convert.ToString(oTmp)

            Return sTmp
        End Function

        Public Shared Sub SetValue_String(keyName As String, valueName As String, value As String)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Long) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            iTmp = Helpers_old.Types.CastToInteger_safe(oTmp)

            Return iTmp
        End Function

        Public Shared Sub SetValue_Integer(keyName As String, valueName As String, value As Integer)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "misc"
        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function AllowAllForEveryone(key As String) As String
            Dim sRet As String = ""

            Try
                Dim sid As SecurityIdentifier = New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
                Dim account As NTAccount = TryCast(sid.Translate(GetType(NTAccount)), NTAccount)

                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Using rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, RegistryKeyPermissionCheck.ReadWriteSubTree)
                    If rk Is Nothing Then
                        Throw New InvalidOperationException("key '" & key & "' does not exist in HKEY_LOCAL_MACHINE")
                    End If

                    Dim rs As RegistrySecurity = rk.GetAccessControl
                    Dim rar As RegistryAccessRule = New RegistryAccessRule( _
                                                    account.ToString, _
                                                    RegistryRights.FullControl, _
                                                    InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, _
                                                    PropagationFlags.None, _
                                                    AccessControlType.Allow)


                    rs.SetAccessRuleProtection(True, False)
                    rs.AddAccessRule(rar)

                    rk.SetAccessControl(rs)
                End Using

                sRet = "ok"
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function
#End Region

        Private Shared ReadOnly _md5 As Security.Cryptography.MD5 = Security.Cryptography.MD5.Create()
    End Class
#End Region

#Region "Crypto"
    Public Class MD5Class
        Private Shared ReadOnly _md5 As Security.Cryptography.MD5 = Security.Cryptography.MD5.Create()

        Public Shared Function GetMd5Hash(source As String) As String
            Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
            Dim sb As New System.Text.StringBuilder()

            Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

            Return sb.ToString()
        End Function

        Public Shared Function VerifyMd5Hash(source As String, hash As String) As Boolean
            Dim sourceHash = GetMd5Hash(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function

        Public Shared Function GetMd5HashBase64(source As String) As String
            Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

            Return Convert.ToBase64String(data)
        End Function

        Public Shared Function VerifyMd5HashBase64(source As String, hash As String) As Boolean
            Dim sourceHash = GetMd5HashBase64(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function
    End Class
#End Region

#Region "TimeDate"
    Public Class TimeDate
        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function
    End Class
#End Region

#Region "Processes"
    Public Class Processes
        <DllImport("advapi32.dll", SetLastError:=True)> _
        Private Shared Function OpenProcessToken(ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        Private Shared mLastError As String = ""
        Public Shared ReadOnly Property LastError As String
            Get
                Dim sTmp As String = mLastError

                mLastError = ""

                Return sTmp
            End Get
        End Property

        Private Shared Function ConvertPasswordStringToSecureString(ByVal str As String) As Security.SecureString
            Dim password As New Security.SecureString

            For Each c As Char In str.ToCharArray
                password.AppendChar(c)
            Next

            Return password
        End Function

        Public Shared Function IsProcessRunning(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String

            sSearchProcessName__ToLower = sProcessName.ToLower

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            For Each p As Process In Process.GetProcesses
                sProcessName__ToLower = p.ProcessName.ToLower
                If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                    bRet = True
                    Exit For
                End If
            Next

            Return bRet
        End Function

        Public Shared Function IsUserLoggedIn(UserName As String) As Boolean
            Dim iCount As Integer

            iCount = CountProcessesOwnedByUser(UserName)

            If iCount >= 3 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
            Dim wiName As String, lCount As Long

            lCount = 0

            For Each p As Process In Process.GetProcesses
                Try
                    Dim hToken As IntPtr
                    If OpenProcessToken(p.Handle, TokenAccessLevels.Query, hToken) Then
                        Using wi As New WindowsIdentity(hToken)
                            wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                            If wiName.ToLower = sUserNameToCheck.ToLower Then
                                lCount += 1
                            End If
                        End Using
                        CloseHandle(hToken)
                    End If
                Catch ex As Exception

                End Try
            Next

            Return lCount
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOut As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, waitForExit, timeOut)
        End Function

        Public Shared Function StartProcess(cmdLine As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOut As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, "", "", cmdArguments, waitForExit, timeOut)
        End Function

        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    mLastError = "not started (pid<=0)"
                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOut)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        mLastError = "timed out!"
                        Return False
                    End If
                End If
            Catch ex As Exception
                mLastError = "not started (" & ex.Message & ")"
                Return False
            End Try

            Return True
        End Function

        Public Shared Function IsAppAlreadyRunning() As Boolean
            Dim appProc() As Process
            Dim strModName, strProcName As String

            Try
                strModName = Process.GetCurrentProcess.MainModule.ModuleName
                strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

                appProc = Process.GetProcessesByName(strProcName)

                If appProc.Length > 1 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception

            End Try

            Return False
        End Function
    End Class
#End Region

#Region "XOrObfuscation"
    Public Class XOrObfuscation
        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "$#(98)&_Ndf9m0987e5WRW#%rtefgm03947"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            If PlainText.Length > 1000 Then
                Return "(string too long)"
            End If

            InitializePasshrases()

            Try
                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(Asc(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(Asc(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String

            InitializePasshrases()

            Try
                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & Chr(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & Chr(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                PlainText_RandomPassphrase = ""
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = Asc(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            temp = _HexToNo(Mid(BinVal, 2, 1))
            iVal = iVal + temp
            temp = _HexToNo(Mid(BinVal, 1, 1))
            iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class
#End Region

End Class
