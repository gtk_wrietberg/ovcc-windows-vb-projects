﻿Imports System.IO

Public Class clsFirefox
    Private mProfileRootPath As String
    Public Property ProfileRootPath() As String
        Get
            Return mProfileRootPath
        End Get
        Set(ByVal value As String)
            mProfileRootPath = value
        End Set
    End Property

    Private mProfilesIni As String
    Public Property ProfilesIni() As String
        Get
            Return mProfilesIni
        End Get
        Set(ByVal value As String)
            mProfilesIni = value
        End Set
    End Property

    Private mActiveProfilePath As String
    Public ReadOnly Property ActiveProfilePath() As String
        Get
            Return mActiveProfilePath
        End Get
    End Property

    Private mLastError As String
    Public Property LastError() As String
        Get
            Return mLastError
        End Get
        Set(ByVal value As String)
            mLastError = value
        End Set
    End Property

    Private mUserName As String
    Public Property Username() As String
        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set
    End Property



    'Typical profiles.ini looks like this
    '    [General]
    'StartWithLastProfile=1

    '[Profile0]
    'Name=Wouter_main_profile
    'IsRelative=1
    'Path=Profiles/j98xt73i.default
    'Default=1

    '[Profile1]
    'Name=test_profile
    'IsRelative=1
    'Path=Profiles/ahbw0lkv.test_profile

    Public Function FindActiveFirefoxProfile() As Boolean
        Dim sPath As String = mProfileRootPath.Replace("%%%USERNAME%%%", mUserName)
        Dim sLine As String = "", sTmp As String = ""
        Dim bRet As Boolean = False

        mActiveProfilePath = ""

        Helpers.Logger.WriteMessageRelative("search path", 1)
        Helpers.Logger.WriteMessageRelative(sPath, 2)

        Try

            'Dir does not exist
            If Not IO.Directory.Exists(sPath) Then
                Throw New Exception("Folder '" & sPath & "' does not exist")
            End If

            If Not IO.File.Exists(sPath & "\" & mProfilesIni) Then
                Throw New Exception("File '" & sPath & "\" & mProfilesIni & "' does not exist")
            End If

            Helpers.Logger.WriteMessageRelative("search through file", 1)
            Helpers.Logger.WriteMessageRelative(sPath & "\" & mProfilesIni, 2)

            Using sr As New StreamReader(sPath & "\" & mProfilesIni)
                Do While sr.Peek() <> -1
                    sLine = sr.ReadLine()
                    If Not sTmp.Equals("") Then
                        If sLine.Contains("Default=1") Then
                            mActiveProfilePath = sPath & "\" & sTmp

                            Helpers.Logger.WriteMessageRelative("found profile", 3)
                            Helpers.Logger.WriteMessageRelative(mActiveProfilePath, 4)

                            bRet = True
                            Exit Do
                        End If
                    End If
                    If sLine.Contains("Path=") Then
                        Helpers.Logger.WriteMessageRelative("found trigger line", 3)
                        Helpers.Logger.WriteMessageRelative(sLine, 4)

                        sTmp = sLine.Replace("Path=", "")
                    End If
                Loop
            End Using

            mActiveProfilePath = mActiveProfilePath.Replace("/", "\")
            mActiveProfilePath = mActiveProfilePath.Replace("\\", "\")
        Catch ex As Exception
            bRet = False
            mLastError = ex.Message

            Helpers.Logger.WriteErrorRelative("error", 1)
            Helpers.Logger.WriteErrorRelative(ex.Message, 2)
        End Try

        Return bRet
    End Function
End Class
