﻿Public Class Globals
    Public Class CommandLineArguments
        Public Shared ReadOnly Switch_Clean As String = "--clean"
        Public Shared ReadOnly Switch_Save As String = "--save"
    End Class

    Public Class FilesAndFolders
        Public Shared ReadOnly FirefoxAppDataPath As String = "C:\Users\%%%USERNAME%%%\AppData\Roaming\Mozilla\Firefox\"
        Public Shared ReadOnly FirefoxProfiles_ini As String = "profiles.ini"

        'Public Shared ReadOnly SavedProfilePath As String = "saved_profile\%%TIMESTAMP%%"
        Public Shared ReadOnly SavedProfilePath As String = "saved_profile"
    End Class

    Public Class ExitCodes
        Public Shared ReadOnly AllisOk As Integer = 0
        Public Shared ReadOnly FirefoxProfileError As Integer = 1
        Public Shared ReadOnly ShouldRunAsAdmin As Integer = 2
    End Class

    Public Class Registry
        Public Shared ReadOnly KEY__FirefoxProfileSwitcher As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\FirefoxProfileSwitcher"
        Public Shared ReadOnly VALUE__SavedProfile As String = "SavedProfile"
    End Class
End Class
