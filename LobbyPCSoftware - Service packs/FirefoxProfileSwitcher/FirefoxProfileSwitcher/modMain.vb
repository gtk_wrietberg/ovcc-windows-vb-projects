﻿Module modMain
    Private mFireFoxActiveProfile As String = ""
    Private mFirefox As clsFirefox

    Public Sub Main()
        ExitCode.SetValue(ExitCode.ExitCodes.OK)

        'If Not Helpers.Generic.IsRunningAsAdmin Then
        '    ExitCode.SetValue(ExitCode.ExitCodes.SHOULD_RUN_AS_ADMIN)

        '    ExitApplication()
        'End If

        If Not Helpers.Logger.InitialiseLogger() Then
            ExitCode.SetValue(ExitCode.ExitCodes.LOGGING_DISABLED)
        End If

        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        If Environment.GetCommandLineArgs().Count = 2 Then
            Dim arg As String = Environment.GetCommandLineArgs().GetValue(1)

            If arg.Equals(Globals.CommandLineArguments.Switch_Clean) Or arg.Equals(Globals.CommandLineArguments.Switch_Save) Then
                Helpers.Logger.WriteMessage("searching for Firefox profile", 0)
                LoadFirefoxStuff()


                If arg.Equals(Globals.CommandLineArguments.Switch_Clean) Then
                    Helpers.Logger.WriteMessage("cleaning current profile", 0)

                    Switch_Clean()
                End If

                If arg.Equals(Globals.CommandLineArguments.Switch_Save) Then
                    If Not Helpers.Generic.IsRunningAsAdmin Then
                        ExitCode.SetValue(ExitCode.ExitCodes.SHOULD_RUN_AS_ADMIN)

                        MessageBox.Show("Please run with admin privileges!", My.Application.Info.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                        ExitApplication()
                    End If

                    Helpers.Logger.WriteMessage("saving current profile", 0)

                    Switch_Save()
                End If
            Else
                ExitCode.SetValue(ExitCode.ExitCodes.INVALID_ACTION_SELECTED)
            End If
        Else
            ExitCode.SetValue(ExitCode.ExitCodes.INVALID_ACTION_SELECTED)
        End If


        ExitApplication()
    End Sub

    Private Sub LoadFirefoxStuff()
        mFirefox = New clsFirefox
        'mFirefox.Username = Environment.UserName
        mFirefox.Username = "sitekiosk"
        mFirefox.ProfileRootPath = Globals.FilesAndFolders.FirefoxAppDataPath
        mFirefox.ProfilesIni = Globals.FilesAndFolders.FirefoxProfiles_ini

        Helpers.Logger.WriteMessageRelative("Firefox settings", 1)
        Helpers.Logger.WriteMessageRelative("username", 2)
        Helpers.Logger.WriteMessageRelative(mFirefox.Username, 3)
        Helpers.Logger.WriteMessageRelative("ProfileRootPath", 2)
        Helpers.Logger.WriteMessageRelative(mFirefox.ProfileRootPath, 3)
        Helpers.Logger.WriteMessageRelative("ProfilesIni", 2)
        Helpers.Logger.WriteMessageRelative(mFirefox.ProfilesIni, 3)


        If mFirefox.FindActiveFirefoxProfile() Then
            Helpers.Logger.WriteMessageRelative("found Firefox profile", 1)
            Helpers.Logger.WriteMessageRelative(mFirefox.ActiveProfilePath, 2)
        Else
            ExitCode.SetValue(ExitCode.ExitCodes.FIREFOX_PROFILE_NOT_FOUND)

            Helpers.Logger.WriteErrorRelative("error", 1)
            Helpers.Logger.WriteErrorRelative(mFirefox.LastError, 2)

            ExitApplication()
        End If
    End Sub

    Private Function GetLatestSavedProfileFolder() As String
        Return Helpers.FilesAndFolders.GetNewestFolder(My.Application.Info.DirectoryPath & "\" & Globals.FilesAndFolders.SavedProfilePath)
    End Function

    Private Sub Switch_Clean()
        Dim sPath As String

        'sPath = Helpers.Registry.GetValue_String(Globals.Registry.KEY__FirefoxProfileSwitcher, Globals.Registry.VALUE__SavedProfile)
        sPath = GetLatestSavedProfileFolder()

        Helpers.Logger.WriteMessageRelative("searching for latest saved profile", 1)
        If IO.Directory.Exists(sPath) Then
            Helpers.Logger.WriteMessageRelative(sPath, 2)
        Else
            Helpers.Logger.WriteMessageRelative("none found in '" & My.Application.Info.DirectoryPath & "\" & Globals.FilesAndFolders.SavedProfilePath & "'", 2)

            ExitCode.SetValue(ExitCode.ExitCodes.NO_SAVED_PROFILE_AVAILABLE)

            Exit Sub
        End If


        If sPath.Equals("") Then
            ExitCode.SetValue(ExitCode.ExitCodes.NO_SAVED_PROFILE_AVAILABLE)

            Exit Sub
        End If

        Try
            If IO.Directory.Exists(mFirefox.ActiveProfilePath) And IO.Directory.Exists(sPath) Then
                Helpers.Logger.WriteMessageRelative("cleaning", 1)
                Helpers.Logger.WriteMessageRelative(mFirefox.ActiveProfilePath, 2)

                CleanDirectory(mFirefox.ActiveProfilePath)


                Helpers.Logger.WriteMessageRelative("copying", 1)
                Helpers.Logger.WriteMessageRelative(sPath & " ==> " & mFirefox.ActiveProfilePath, 2)

                My.Computer.FileSystem.CopyDirectory(sPath, mFirefox.ActiveProfilePath, True)
            End If
        Catch ex As Exception
            ExitCode.SetValue(ExitCode.ExitCodes.CLEAN_ERROR)

            Helpers.WindowsEventLog.WriteError("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub Switch_Save()
        Try
            Helpers.Logger.WriteMessageRelative("creating profile folder copy", 1)

            Dim sPath As String

            sPath = My.Application.Info.DirectoryPath & "\" & Globals.FilesAndFolders.SavedProfilePath
            sPath = sPath.Replace("/", "\")
            sPath = sPath.Replace("\\", "\")
            'sPath = sPath.Replace("%%TIMESTAMP%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))
            sPath &= "\" & Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            Helpers.Logger.WriteMessageRelative(sPath, 2)

            If Not IO.Directory.Exists(sPath) Then
                IO.Directory.CreateDirectory(sPath)
            End If


            If IO.Directory.Exists(mFirefox.ActiveProfilePath) And IO.Directory.Exists(sPath) Then
                Helpers.Logger.WriteMessageRelative("copying", 1)
                Helpers.Logger.WriteMessageRelative(mFirefox.ActiveProfilePath & " ==> " & sPath, 2)

                My.Computer.FileSystem.CopyDirectory(mFirefox.ActiveProfilePath, sPath, True)
            End If
        Catch ex As Exception
            ExitCode.SetValue(ExitCode.ExitCodes.SAVE_ERROR)

            Helpers.Logger.WriteMessageRelative("error", 1)
            Helpers.Logger.WriteMessageRelative(ex.Message, 2)
        End Try
    End Sub

    Private Sub CleanDirectory(dir As String)
        For Each d In IO.Directory.GetDirectories(dir)
            IO.Directory.Delete(d, True)
        Next

        For Each f In IO.Directory.GetFiles(dir)
            IO.File.Delete(f)
        Next
    End Sub

    Private Sub ExitApplication()
        MsgBox(ExitCode.ToString)
        Environment.ExitCode = ExitCode.GetValue
        Environment.Exit(ExitCode.GetValue)
    End Sub
End Module
