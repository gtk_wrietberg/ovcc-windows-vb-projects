;//**************Category Definitions************
MessageId=1
Facility=Application
Severity=Success
SymbolicName=CAT1
Language=English
MyCategory1
.
MessageId=2
Facility=Application
Severity=Success
SymbolicName=CAT2
Language=English
MyCategory2
.

;//***********Event Definitions**************
MessageId=1000
Severity=Success
SymbolicName=MSG1
Language=English
My Error Message
.
MessageId=2000
Severity=Success
SymbolicName=GENERIC
Language=English
%1
.
