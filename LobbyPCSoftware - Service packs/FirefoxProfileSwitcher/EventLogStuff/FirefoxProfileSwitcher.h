//**************Category Definitions************
//
//  Values are 32 bit values laid out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: CAT1
//
// MessageText:
//
// MyCategory1
//
#define CAT1                             0x00000001L

//
// MessageId: CAT2
//
// MessageText:
//
// MyCategory2
//
#define CAT2                             0x00000002L

//***********Event Definitions**************
//
// MessageId: MSG1
//
// MessageText:
//
// My Error Message
//
#define MSG1                             0x000003E8L

//
// MessageId: GENERIC
//
// MessageText:
//
// %1
//
#define GENERIC                          0x000007D0L

