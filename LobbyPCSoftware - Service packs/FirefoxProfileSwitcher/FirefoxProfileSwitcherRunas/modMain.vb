﻿Module modMain
    Public Sub Main()
        If Helpers.Processes.IsAppAlreadyRunning Then
            Helpers.WindowsEventLog.WriteWarning("Already running.")

            Application.Exit()
            Exit Sub
        End If


        Dim sUser As String = Helpers.PuppyPower.Username
        Dim sPass As String = Helpers.PuppyPower.Password

        Helpers.WindowsEventLog.WriteInformation("starting FirefoxProfileSwitcher")

        If sUser.Equals("") Or sPass.Equals("") Then
            Helpers.WindowsEventLog.WriteError("unable to get puppy power, exiting...")

            Application.Exit()
            Exit Sub
        End If

        Dim stpw As New Stopwatch
        stpw.Reset()
        stpw.Start()

        Dim iExitCode As Integer = -123

        If Not Helpers.Processes.StartProcessAsUserWithExitcode("FirefoxProfileSwitcher.exe", Helpers.PuppyPower.Username, Helpers.PuppyPower.Password, "--save", True, 30000, iExitCode) Then
            Helpers.WindowsEventLog.WriteError("exit code " & iExitCode.ToString)
            Helpers.WindowsEventLog.WriteError("error: " & Helpers.Errors.GetLast(False))
        Else
            Helpers.WindowsEventLog.WriteInformation("exit code " & iExitCode.ToString)
        End If

        stpw.Stop()
        Helpers.WindowsEventLog.WriteInformation("time elapsed: " & stpw.ElapsedMilliseconds.ToString & " ms")
    End Sub
End Module
