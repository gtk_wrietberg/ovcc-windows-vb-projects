Module Main


    Public Sub Main()
        Dim iExitCode As Integer

        iExitCode = 0

        oLogger = New Logger
        oLogger.LogFilePath = "C:\Program Files\iBAHN\"
        oLogger.LogFileName = "DisableCompatibilityView.log"

        oComputerName = New ComputerName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        oLogger.WriteToLog(New String("-", 10))

        '--------------------
        For Each arg As String In Environment.GetCommandLineArgs()
            oLogger.WriteToLog("found: " & arg, , 2)

            If InStr(arg, "-loadhive") > 0 Then
                g_LoadSiteKioskHive = True
            End If
        Next

        '--------------------
        oLogger.WriteToLog("DisableCompatibilityView", , 0)
        If g_LoadSiteKioskHive Then
            If Not FixStuff_SiteKioskHive() Then
                oLogger.WriteToLog("fail", , 1)
                iExitCode += 1
            Else
                oLogger.WriteToLog("ok", , 1)
            End If
        Else
            If Not FixStuff_CurrentUser() Then
                oLogger.WriteToLog("fail", , 1)
                iExitCode += 1
            Else
                oLogger.WriteToLog("ok", , 1)
            End If
        End If

        '--------------------
        ExitApplication(iExitCode)
    End Sub


    Public Function FixStuff_SiteKioskHive() As Boolean
        Try
            Dim oHive As RegistryHive
            Dim sSiteKioskRegistryHive As String

            oLogger.WriteToLogRelative("updating registry for SiteKiosk user", , 1)

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("initialising", , 2)
            oHive = New RegistryHive

            If oHive.Initialise Then
                oLogger.WriteToLog("ok", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                Return False
            End If

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("loading SiteKiosk hive", , 2)
            If IO.File.Exists(cSiteKioskRegistryHive_Win7) Then
                sSiteKioskRegistryHive = cSiteKioskRegistryHive_Win7
            ElseIf IO.File.Exists(cSiteKioskRegistryHive_WinXP) Then
                sSiteKioskRegistryHive = cSiteKioskRegistryHive_WinXP
            Else
                oLogger.WriteToLogRelative("No hives found!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                Return False
            End If
            oLogger.WriteToLogRelative("hive found", , 3)
            oLogger.WriteToLogRelative(sSiteKioskRegistryHive, , 4)


            If oHive.LoadHive(sSiteKioskRegistryHive) Then
                oLogger.WriteToLogRelative("loaded", , 3)
            Else
                oLogger.WriteToLogRelative("loading failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                oHive.UnloadHive()

                Return False
            End If


            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("setting values", , 2)

            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_SecureProtocols, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_SecureProtocols.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_SecureProtocols, cSiteKioskRegistry_Value_SecureProtocols) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            '-------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings_Zone3, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_DisplayMixedContent, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_DisplayMixedContent.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings_Zone3, cSiteKioskRegistry_Name_DisplayMixedContent, cSiteKioskRegistry_Value_DisplayMixedContent) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            '-------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_BrowserEmulation, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_MSCompatibilityMode, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_MSCompatibilityMode.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_BrowserEmulation, cSiteKioskRegistry_Name_MSCompatibilityMode, cSiteKioskRegistry_Value_MSCompatibilityMode) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            '-------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_BrowserEmulation, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_IntranetCompatibilityMode, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_IntranetCompatibilityMode.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_BrowserEmulation, cSiteKioskRegistry_Name_IntranetCompatibilityMode, cSiteKioskRegistry_Value_IntranetCompatibilityMode) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("unloading SiteKiosk hive", , 2)
            If oHive.UnloadHive() Then
                oLogger.WriteToLogRelative("unloaded", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                Return False
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative("ex.Source", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function

    Public Function FixStuff_CurrentUser() As Boolean
        Try
            oLogger.WriteToLogRelative("updating registry for current user", , 1)

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("setting values", , 2)

            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_SecureProtocols, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_SecureProtocols.ToString, , 3)
            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\" & cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_SecureProtocols, cSiteKioskRegistry_Value_SecureProtocols)

            '-------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings_Zone3, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_DisplayMixedContent, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_DisplayMixedContent.ToString, , 3)
            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\" & cSiteKioskRegistry_Key_InternetSettings_Zone3, cSiteKioskRegistry_Name_DisplayMixedContent, cSiteKioskRegistry_Value_DisplayMixedContent)

            '-------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_BrowserEmulation, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_MSCompatibilityMode, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_MSCompatibilityMode.ToString, , 3)
            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\" & cSiteKioskRegistry_Key_BrowserEmulation, cSiteKioskRegistry_Name_MSCompatibilityMode, cSiteKioskRegistry_Value_MSCompatibilityMode)

            '-------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_BrowserEmulation, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_IntranetCompatibilityMode, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_IntranetCompatibilityMode.ToString, , 3)
            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\" & cSiteKioskRegistry_Key_BrowserEmulation, cSiteKioskRegistry_Name_IntranetCompatibilityMode, cSiteKioskRegistry_Value_IntranetCompatibilityMode)

            '------------------------------------------------------------------------------------
            Return True

        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative("ex.Source", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function

    Public Sub ExitApplication(ByVal iExitCode As Integer)
        oLogger.WriteToLog("Exiting...", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        oLogger.WriteToLog("exit code: " & iExitCode & " (" & iExitCode.ToString & ")", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLog("bye", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLogWithoutDate(New String("=", 100))

        System.Environment.Exit(iExitCode)
    End Sub
End Module
