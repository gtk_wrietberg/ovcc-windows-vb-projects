Module Constants___Globals
    'Constants
    Public Const cSiteKioskRegistryHive_WinXP As String = "C:\Documents and Settings\sitekiosk\ntuser.dat"
    Public Const cSiteKioskRegistryHive_Win7 As String = "C:\Users\sitekiosk\ntuser.dat"

    Public Const cSiteKioskRegistry_Key_InternetSettings As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings"
    Public Const cSiteKioskRegistry_Key_InternetSettings_Zone3 As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3"
    Public Const cSiteKioskRegistry_Key_BrowserEmulation As String = "SOFTWARE\Microsoft\Internet Explorer\BrowserEmulation"


    Public Const cSiteKioskRegistry_Name_DisplayMixedContent As String = "1609"
    Public Const cSiteKioskRegistry_Value_DisplayMixedContent As Long = 0

    Public Const cSiteKioskRegistry_Name_SecureProtocols As String = "SecureProtocols"
    Public Const cSiteKioskRegistry_Value_SecureProtocols As Long = 168 '0x000000a8

    Public Const cSiteKioskRegistry_Name_MSCompatibilityMode As String = "MSCompatibilityMode"
    Public Const cSiteKioskRegistry_Value_MSCompatibilityMode As Long = 0

    Public Const cSiteKioskRegistry_Name_IntranetCompatibilityMode As String = "IntranetCompatibilityMode"
    Public Const cSiteKioskRegistry_Value_IntranetCompatibilityMode As Long = 0


    'Globals
    Public oLogger As Logger
    Public oComputerName As ComputerName

    Public g_LoadSiteKioskHive As Boolean = False
End Module
