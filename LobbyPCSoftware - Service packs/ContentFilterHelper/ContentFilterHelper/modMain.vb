﻿Imports System.IO
Imports System.Text.RegularExpressions

Public Class HOSTSEntry
    Public Sub New(sIP As String, sHOST As String)
        mIP = sIP
        mHOST = sHOST
    End Sub

    Private mIP As String
    Public Property IP() As String
        Get
            Return mIP
        End Get
        Set(ByVal value As String)
            mIP = value
        End Set
    End Property

    Private mHOST As String
    Public Property HOST() As String
        Get
            Return mHOST
        End Get
        Set(ByVal value As String)
            mHOST = value
        End Set
    End Property
End Class

Module modMain
    Private ReadOnly c__FILE__HOSTS As String = "C:\Windows\System32\drivers\etc\hosts"
    'Private ReadOnly c__FILE__HOSTS As String = "C:\Users\Wouter\Documents\My Programming\My Programming\iBAHN\VisualStudio Projects\LobbyPCSoftware - Service packs\ContentFilterHelper\ContentFilterHelper\hosts"

    Public Sub Main()
        Initialise()

        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)

        ExitCode.SetValue(ExitCode.ExitCodes.OK)

        Try
            Helpers.Logger.WriteMessage("Backing up hosts file", 0)
            Helpers.Logger.WriteMessage("file", 1)
            Helpers.Logger.WriteMessage(c__FILE__HOSTS, 2)

            Helpers.FilesAndFolders.Backup.File(c__FILE__HOSTS, g_BackupDirectory)

            '----------------------------------------------------------------------------------------------------------------
            Dim sLine As String = ""
            Dim lstExisting As New List(Of HOSTSEntry)
            Dim lstNew As New List(Of HOSTSEntry)
            Dim lstFinal As New List(Of HOSTSEntry)
            Dim lCount As Long = 0

            '--------------------------------
            ExitCode.SetValue(ExitCode.ExitCodes.HOSTS_READ_ERROR)

            Helpers.Logger.WriteMessage("Reading hosts file", 0)
            Helpers.Logger.WriteMessage("file", 1)
            Helpers.Logger.WriteMessage(c__FILE__HOSTS, 2)

            Dim regexHOSTS As Regex = New Regex("^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})\s+(.+)$")
            Dim oReader_HOSTS As New StreamReader(c__FILE__HOSTS)
            Dim b_HOSTS_startfound As Boolean = False

            lCount = 0

            sLine = oReader_HOSTS.ReadLine()
            Do Until sLine Is Nothing
                If regexHOSTS.IsMatch(sLine) Then
                    lCount += 1
                    lstExisting.Add(New HOSTSEntry(regexHOSTS.Replace(sLine, "$1"), regexHOSTS.Replace(sLine, "$2")))
                End If

                sLine = oReader_HOSTS.ReadLine()
            Loop
            oReader_HOSTS.Close()

            If lCount = 1 Then
                Helpers.Logger.WriteMessage("found 1 entry", 1)
            Else
                Helpers.Logger.WriteMessage("found " & lCount & " entries", 1)
            End If


            For i As Integer = 0 To lstExisting.Count - 1
                For j As Integer = 0 To lstExisting.Count - 1
                    If i <> j Then
                        If lstExisting.Item(i).HOST.Equals(lstExisting.Item(j).HOST) Then
                            lstExisting.Item(j).IP = String.Empty
                            lstExisting.Item(j).HOST = String.Empty
                        End If
                    End If
                Next
            Next


            ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


            '----------------------------------------------------------------------------------------------------------------
            ExitCode.SetValue(ExitCode.ExitCodes.BLACKLIST_ERROR)

            Dim regex As Regex = New Regex("^([a-zA-Z0-9_\-]+\.){1,5}[a-zA-Z]{2,12}(/\S*)?$")
            Dim match As Match


            Helpers.Logger.WriteMessage("Reading internal blacklist", 0)

            Dim oReaderBlacklist As New StringReader(My.Resources.black_list)
            Dim lTmp As New List(Of String)
            Dim lBlackList As New List(Of String)
            Dim lInvalid As New List(Of String)

            lCount = 0

            sLine = oReaderBlacklist.ReadLine()

            Do Until sLine Is Nothing
                lTmp.Add(sLine)

                lCount += 1

                sLine = oReaderBlacklist.ReadLine()
            Loop

            oReaderBlacklist.Close()

            If lCount = 1 Then
                Helpers.Logger.WriteMessage("found 1 entry", 1)
            Else
                Helpers.Logger.WriteMessage("found " & lCount & " entries", 1)
            End If


            Helpers.Logger.WriteMessage("removing dupes", 1)
            lBlackList = lTmp.Distinct().ToList()
            Helpers.Logger.WriteMessage("removed " & (lTmp.Count - lBlackList.Count).ToString, 2)

            Dim tmpURI As Uri
            Dim tmpStr As String = ""

            Helpers.Logger.WriteMessage("checking url validity", 1)

            For i As Integer = 0 To lBlackList.Count - 1
                tmpStr = lBlackList.Item(i)
                match = regex.Match(tmpStr)

                If match.Success Then
                    lstNew.Add(New HOSTSEntry("127.0.0.1", tmpStr))
                Else
                    lInvalid.Add(tmpStr)
                End If
            Next

            If lInvalid.Count = 1 Then
                Helpers.Logger.WriteMessage("removed 1 entry", 2)
            Else
                Helpers.Logger.WriteMessage("removed " & lInvalid.Count & " entries", 2)
            End If


            ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


            '----------------------------------------------------------------------------------------------------------------
            ExitCode.SetValue(ExitCode.ExitCodes.MERGE_ERROR)

            Dim bFound As Boolean = False

            Helpers.Logger.WriteMessage("Merge existing and blacklist", 0)
            Helpers.Logger.WriteMessage("pre-merge entries", 1)
            Helpers.Logger.WriteMessage((lstExisting.Count + lstNew.Count).ToString, 2)

            For i As Integer = 0 To lstNew.Count - 1
                bFound = False
                For j As Integer = 0 To lstExisting.Count - 1
                    'Console.WriteLine("Checking: " & lstExisting.Item(j).HOST & " = " & lstNew.Item(i).HOST & " : " & (lstExisting.Item(j).HOST.Equals(lstNew.Item(i).HOST)).ToString)
                    If lstExisting.Item(j).HOST.Equals(lstNew.Item(i).HOST) Or lstExisting.Item(j).HOST.Equals(String.Empty) Then
                        bFound = True
                        lstExisting.RemoveAt(j)

                        Exit For
                    End If
                Next
            Next

            Helpers.Logger.WriteMessage("post-merge entries", 1)
            Helpers.Logger.WriteMessage((lstExisting.Count + lstNew.Count).ToString, 2)

            For i As Integer = 0 To lstExisting.Count - 1
                'Console.WriteLine("existing: " & lstExisting.Item(i).HOST)
                lstFinal.Add(New HOSTSEntry(lstExisting.Item(i).IP, lstExisting.Item(i).HOST))
            Next

            lstFinal.Add(New HOSTSEntry(String.Empty, String.Empty))

            For i As Integer = 0 To lstNew.Count - 1
                'Console.WriteLine("new: " & lstNew.Item(i).HOST)
                lstFinal.Add(New HOSTSEntry(lstNew.Item(i).IP, lstNew.Item(i).HOST))
            Next


            ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


            '----------------------------------------------------------------------------------------------------------------
            ExitCode.SetValue(ExitCode.ExitCodes.WRITE_ERROR)

            Helpers.Logger.WriteMessage("Writing hosts file", 0)
            Helpers.Logger.WriteMessage("file", 1)
            Helpers.Logger.WriteMessage(c__FILE__HOSTS, 2)

            Dim lstLines As New List(Of String)

            Dim oReaderHostsDefault As New StringReader(My.Resources.hosts_default)
            sLine = oReaderHostsDefault.ReadLine()

            Do Until sLine Is Nothing
                lstLines.Add(sLine)

                sLine = oReaderHostsDefault.ReadLine()
            Loop

            oReaderHostsDefault.Close()


            lstLines.Add(" ")

            For i As Integer = 0 To lstFinal.Count - 1
                If lstFinal.Item(i).IP.Equals(String.Empty) Then
                    lstLines.Add(" ")
                Else
                    lstLines.Add(lstFinal.Item(i).IP & vbTab & lstFinal.Item(i).HOST)
                End If
            Next


            Dim oWriter As New StreamWriter(c__FILE__HOSTS)
            For i As Integer = 0 To lstLines.Count - 1
                oWriter.WriteLine(lstLines.Item(i))

            Next
            oWriter.Close()


            ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


            '----------------------------------------------------------------------------------------------------------------
            Helpers.Logger.WriteMessage("Done", 0)
            Helpers.Logger.WriteMessage("ok", 1)
            Helpers.Logger.WriteMessage("bye", 2)
        Catch ex As Exception
            Helpers.Logger.WriteError("FATAL ERROR", 0)
            Helpers.Logger.WriteError(ex.Message, 1)
        End Try

        Helpers.Logger.WriteMessage("Exit code", 0)
        Helpers.Logger.WriteMessage(ExitCode.ToString, 1)
    End Sub
End Module
