﻿Module modMain
    Public Sub main()
        '------------------------------------------------------
        InitGlobals()

        '------------------------------------------------------
        'Logger initialisation
        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        oLogger.WriteEmptyLineToLog()


        g_UserName = c_DefaultUsername


        '------------------------------------------------------
        'Group names
        oLogger.WriteToLog("Get group names", , 0)

        g_AdminGroupName = cWindowsUser.GetAdministratorsGroupName
        oLogger.WriteToLog("Administrators", , 1)
        oLogger.WriteToLog("'" & g_AdminGroupName & "'", , 2)

        g_UsersGroupName = cWindowsUser.GetUsersGroupName
        oLogger.WriteToLog("Users", , 1)
        oLogger.WriteToLog("'" & g_UsersGroupName & "'", , 2)


        'Check if user exists
        oLogger.WriteToLog("Does user '" & g_UserName & "' exist?", , 0)
        If Not cWindowsUser.DoesUserExist(g_UserName, g_UsersGroupName) And Not cWindowsUser.DoesUserExist(g_UserName, g_AdminGroupName) Then
            oLogger.WriteToLog("nope", , 1)
        Else
            oLogger.WriteToLog("yep", , 1)

            oLogger.WriteToLog("But is he an admin?", , 0)
            If cWindowsUser.DoesUserExist(g_UserName, g_AdminGroupName) Then
                oLogger.WriteToLog("yes!", , 1)
            Else
                oLogger.WriteToLog("nope", , 1)
            End If
        End If

    End Sub
End Module
