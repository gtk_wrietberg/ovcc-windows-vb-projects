﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        InitGlobals()

        '------------------------------------------------------
        'Logger initialisation
        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        oLogger.WriteEmptyLineToLog()


        g_UserName = c_DefaultUsername


        '------------------------------------------------------
        'Group names

        MsgBox("starting")

        g_AdminGroupName = cWindowsUser.GetAdministratorsGroupName
        MsgBox("Administrators = '" & g_AdminGroupName & "'")

        g_UsersGroupName = cWindowsUser.GetUsersGroupName
        MsgBox("Users = '" & g_UsersGroupName & "'")


        'Check if user exists
        oLogger.WriteToLog("Does user '" & g_UserName & "' exist?", , 0)
        If Not cWindowsUser.DoesUserExist(g_UserName, g_UsersGroupName) And Not cWindowsUser.DoesUserExist(g_UserName, g_AdminGroupName) Then
            oLogger.WriteToLog("nope", , 1)
        Else
            oLogger.WriteToLog("yep", , 1)

            oLogger.WriteToLog("But is he an admin?", , 0)
            If cWindowsUser.DoesUserExist(g_UserName, g_AdminGroupName) Then
                oLogger.WriteToLog("yes!", , 1)
            Else
                oLogger.WriteToLog("nope", , 1)
            End If
        End If


    End Sub
End Class