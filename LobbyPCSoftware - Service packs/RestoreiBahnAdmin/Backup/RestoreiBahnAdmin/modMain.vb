Module modMain
    Public Sub Main()
        '------------------------------------------------------
        InitGlobals()

        '------------------------------------------------------
        'Logger initialisation
        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        oLogger.WriteEmptyLineToLog()

        If Environment.GetCommandLineArgs().Length > 1 Then
            oLogger.WriteToLog("Command line arguments ", , 0)
            For Each arg As String In Environment.GetCommandLineArgs()
                oLogger.WriteToLog("found argument: " & arg, , 1)

                If InStr(arg, cArg__USERNAME) > 0 Then
                    g_UserName = arg.Replace(cArg__USERNAME, "")
                    oLogger.WriteToLog("username: " & g_UserName, , 2)
                End If
                If InStr(arg, cArg__PASSWORD) > 0 Then
                    g_PassWord = arg.Replace(cArg__PASSWORD, "")
                    oLogger.WriteToLog("password: " & HidePassword(g_PassWord.Length), , 2)
                End If
            Next
        End If

        If g_UserName = "" Then
            oLogger.WriteToLog("No username specified, using default", , 0)
            g_UserName = c_DefaultUsername
            oLogger.WriteToLog(g_UserName, , 1)
        End If

        If g_PassWord = "" Then
            oLogger.WriteToLog("No password specified, using default", , 0)
            g_PassWord = Base64.Decode(c_DefaultPassword)
            oLogger.WriteToLog(HidePassword(g_PassWord.Length), , 1)
        End If


        '------------------------------------------------------
        'Group names
        oLogger.WriteToLog("Get group names", , 0)

        g_AdminGroupName = cWindowsUser.GetAdministratorsGroupName
        oLogger.WriteToLog("Administrators", , 1)
        oLogger.WriteToLog(g_AdminGroupName, , 2)

        g_UsersGroupName = cWindowsUser.GetUsersGroupName
        oLogger.WriteToLog("Users", , 1)
        oLogger.WriteToLog(g_UsersGroupName, , 2)


        'Check if user exists
        oLogger.WriteToLog("Does user '" & g_UserName & "' exist?", , 0)
        If Not cWindowsUser.FindUser(g_UserName, g_UsersGroupName) Then
            oLogger.WriteToLog("nope", , 1)
            oLogger.WriteToLog("creating", , 1)

            Dim sRet As String = cWindowsUser.AddUser(g_UserName, g_UserName, g_PassWord, g_AdminGroupName)
            If sRet = "ok" Then
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("error:", , 2)
                oLogger.WriteToLog(sRet, , 3)
            End If
        Else
            oLogger.WriteToLog("yep", , 1)

            oLogger.WriteToLog("But is he an admin?", , 0)
            If cWindowsUser.FindUser(g_UserName, g_AdminGroupName) Then
                oLogger.WriteToLog("yes!", , 1)
            Else
                oLogger.WriteToLog("nope", , 1)
                oLogger.WriteToLog("adding to admin group", , 1)

                Dim sRet As String = cWindowsUser.AddUserToGroup(g_UserName, g_AdminGroupName)
                If sRet = "ok" Then
                    oLogger.WriteToLog("ok", , 2)
                Else
                    oLogger.WriteToLog("error:", , 2)
                    oLogger.WriteToLog(sRet, , 3)
                End If
            End If
        End If

        ApplicationExit()
    End Sub

    Private Sub ApplicationExit()
        oLogger.WriteToLog("Exiting application", , 0)
        oLogger.WriteToLog("bye", , 1)

        Application.Exit()
    End Sub

    Private Function HidePassword(ByVal s As String, Optional ByVal iShowCharsNum As Integer = 1) As String
        Try
            If s.Length <= (iShowCharsNum * 2) Then
                Return New String("*", s.Length)
            Else
                Return s.Substring(0, iShowCharsNum) & (New String("*", s.Length - (iShowCharsNum * 2))) & s.Substring(s.Length - iShowCharsNum, iShowCharsNum)
            End If
        Catch ex As Exception
            Return "????????"
        End Try
    End Function
End Module
