Module Constants___Globals
    'Constants
    Public Const cArg__USERNAME As String = "--username="
    Public Const cArg__PASSWORD As String = "--password="

    Public Const c_DefaultUsername As String = "ibahn"
    Public Const c_DefaultPassword As String = "cmFuY2lka2lwcGVy"


    'Globals. Me like globals, me are lazy
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public g_UserName As String = ""
    Public g_PassWord As String = ""

    Public g_AdminGroupName As String = ""
    Public g_UsersGroupName As String = ""


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"


        IO.Directory.CreateDirectory(g_LogFileDirectory)
    End Sub
End Module
