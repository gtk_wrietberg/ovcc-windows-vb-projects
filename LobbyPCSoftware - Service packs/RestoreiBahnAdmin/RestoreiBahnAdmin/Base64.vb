Imports System.Text

Public Class Base64
    Public Shared Function Encode(ByVal inputstring As String) As String
        Dim bytesToEncode As Byte()
        bytesToEncode = Encoding.UTF8.GetBytes(inputstring)

        Return Convert.ToBase64String(bytesToEncode)
    End Function

    Public Shared Function Decode(ByVal inputstring As String) As String
        Dim decodedBytes As Byte()
        decodedBytes = Convert.FromBase64String(inputstring)

        Return Encoding.UTF8.GetString(decodedBytes)
    End Function
End Class
