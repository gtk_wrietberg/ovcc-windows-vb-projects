Module modMain
    Public Sub Main()
        '------------------------------------------------------
        InitGlobals()

        '------------------------------------------------------
        'Logger initialisation
        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        oLogger.WriteEmptyLineToLog()

        If Environment.GetCommandLineArgs().Length > 1 Then
            oLogger.WriteToLog("Command line arguments ", , 0)
            For Each arg As String In Environment.GetCommandLineArgs()
                If InStr(arg, cArg__USERNAME) > 0 Then
                    g_UserName = arg.Replace(cArg__USERNAME, "")
                    oLogger.WriteToLog("username: " & g_UserName, , 1)
                End If
                If InStr(arg, cArg__PASSWORD) > 0 Then
                    g_PassWord = arg.Replace(cArg__PASSWORD, "")
                    oLogger.WriteToLog("password: " & cWindowsUser.HidePassword_ShowFirstLastChar(g_PassWord), , 1)
                End If
                If arg = cArg__UPDATEPASSWORD Then
                    g_UpdatePassword = True
                    oLogger.WriteToLog("update password: True", , 1)
                End If
                If arg = cArg__CHECKPASSWORD Then
                    g_CheckPassword = True
                    oLogger.WriteToLog("check password: True", , 1)
                End If
            Next
        End If

        If g_UserName = "" Then
            oLogger.WriteToLog("No username specified, using default", , 0)
            g_UserName = c_DefaultUsername
            oLogger.WriteToLog(g_UserName, , 1)
        End If

        If g_PassWord = "" Then
            oLogger.WriteToLog("No password specified, using default", , 0)
            g_PassWord = Base64.Decode(c_DefaultPassword)
            oLogger.WriteToLog(cWindowsUser.HidePassword_ShowFirstLastChar(g_PassWord), , 1)
        End If


        '------------------------------------------------------
        'Group names
        oLogger.WriteToLog("Get group names", , 0)

        g_AdminGroupName = cWindowsUser.GetAdministratorsGroupName
        oLogger.WriteToLog("Administrators", , 1)
        oLogger.WriteToLog(g_AdminGroupName, , 2)

        g_UsersGroupName = cWindowsUser.GetUsersGroupName
        oLogger.WriteToLog("Users", , 1)
        oLogger.WriteToLog(g_UsersGroupName, , 2)


        If g_CheckPassword Then
            'Only going to check, no changes
            oLogger.WriteToLog("Password check", , 0)

            'Check if user exists
            oLogger.WriteToLog("Does user '" & g_UserName & "' exist?", , 0)
            If Not cWindowsUser.DoesUserExist(g_UserName, g_UsersGroupName) And Not cWindowsUser.DoesUserExist(g_UserName, g_AdminGroupName) Then
                oLogger.WriteToLog("nope", , 1)

                ExitCode.SetValue(ExitCode.ExitCodes.PASSWORD_CHECK_USER_DOES_NOT_EXIST)
            Else
                Dim sRet As String = cWindowsUser.VerifyUserPassword(g_UserName, g_PassWord)

                If sRet = "ok" Then
                    oLogger.WriteToLog("ok", , 2)

                    ExitCode.SetValue(ExitCode.ExitCodes.OK)
                Else
                    oLogger.WriteToLog("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    oLogger.WriteToLog(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                    ExitCode.SetValue(ExitCode.ExitCodes.PASSWORD_CHECK_NOT_OK)
                End If
            End If

        Else
            oLogger.WriteToLog("User creation/updating", , 0)

            'Check if user exists
            oLogger.WriteToLog("Does user '" & g_UserName & "' exist?", , 0)
            If Not cWindowsUser.DoesUserExist(g_UserName, g_UsersGroupName) And Not cWindowsUser.DoesUserExist(g_UserName, g_AdminGroupName) Then
                oLogger.WriteToLog("nope", , 1)
                oLogger.WriteToLog("creating", , 1)

                Dim sRet As String = cWindowsUser.AddAdminUser(g_UserName, g_UserName, g_PassWord)
                If sRet = "ok" Then
                    oLogger.WriteToLog("ok", , 2)

                    ExitCode.SetValue(ExitCode.ExitCodes.OK)
                Else
                    oLogger.WriteToLog("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    oLogger.WriteToLog(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                    ExitCode.SetValue(ExitCode.ExitCodes.ADD_USER_ERROR)
                End If
            Else
                oLogger.WriteToLog("yep", , 1)

                oLogger.WriteToLog("But is he an admin?", , 0)
                If cWindowsUser.DoesUserExist(g_UserName, g_AdminGroupName) Then
                    oLogger.WriteToLog("yes!", , 1)

                    ExitCode.SetValue(ExitCode.ExitCodes.OK)
                Else
                    oLogger.WriteToLog("nope", , 1)
                    oLogger.WriteToLog("adding to admin group", , 1)

                    Dim sRet As String = cWindowsUser.PromoteUserToAdmin(g_UserName)
                    If sRet = "ok" Then
                        oLogger.WriteToLog("ok", , 2)

                        ExitCode.SetValue(ExitCode.ExitCodes.OK)
                    Else
                        oLogger.WriteToLog("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                        oLogger.WriteToLog(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                        ExitCode.SetValue(ExitCode.ExitCodes.ADD_USER_TO_GROUP_ERROR)
                    End If
                End If

                oLogger.WriteToLog("Are we going to update the password?", , 0)
                If g_UpdatePassword Then
                    oLogger.WriteToLog("yep", , 1)

                    If g_PassWord = Base64.Decode(c_DefaultPassword) Then
                        oLogger.WriteToLog("setting password to default", , 1)
                    Else
                        oLogger.WriteToLog("setting password to specified value", , 1)
                    End If

                    If cWindowsUser.ChangePassword(g_UserName, g_PassWord) Then
                        oLogger.WriteToLog("ok", , 2)

                        ExitCode.SetValue(ExitCode.ExitCodes.OK)
                    Else
                        oLogger.WriteToLog("oops", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                        ExitCode.SetValue(ExitCode.ExitCodes.UPDATE_PASSWORD_ERROR)
                    End If
                Else
                    oLogger.WriteToLog("nope", , 1)
                End If
            End If
        End If


        ApplicationExit()
    End Sub

    Private Sub ApplicationExit()
        oLogger.WriteToLog("Exiting application", , 0)
        oLogger.WriteToLog("exit code", , 1)
        oLogger.WriteToLog(ExitCode.ToString(), , 2)
        oLogger.WriteToLog("bye", , 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

End Module
