﻿Imports System.Threading

Public Class frmMain
    Private installThread As Thread
    Private WithEvents oProcess As ProcessRunner

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------

        Fullscreen.Enter(Me)

        If g_TESTMODE Then
            btnCancelFullscreen.Visible = True
            btnCancelFullscreen.Enabled = True
        Else
            btnCancelFullscreen.Visible = False
            btnCancelFullscreen.Enabled = False
        End If


        '------------------------------
        InstallFlash()
    End Sub

    Private Sub btnCancelFullscreen_Click(sender As Object, e As EventArgs) Handles btnCancelFullscreen.Click
        Fullscreen.Leave(Me)
    End Sub

    Private Sub frmMain_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        'moving debug exit fullscreen button
        btnCancelFullscreen.Left = Me.Width - btnCancelFullscreen.Width
    End Sub

#Region "install"
    Private Function FindFlashInstaller() As Boolean
        Dim bFound As Boolean = False, sFlash As String = ""
        Dim currentFile As String = "", currentDiff As Long = 0
        Dim dZero As New Date(0), lDiff As Long = 0

        oLogger.WriteToLogRelative("searching in subfolder", , 1)
        oLogger.WriteToLogRelative(c_FlashInstallerDir, , 2)

        Dim dirFlash As New IO.DirectoryInfo(c_FlashInstallerDir)
        Dim filesFlash As IO.FileInfo() = dirFlash.GetFiles()

        For Each fileFlash As IO.FileInfo In filesFlash
            If fileFlash.Name.StartsWith(c_FlashInstallerPattern) Then
                If fileFlash.Name.EndsWith(c_FlashInstallerExt) Then
                    oLogger.WriteToLogRelative("found flash installer", , 3)
                    oLogger.WriteToLogRelative(fileFlash.Name, , 4)

                    lDiff = DateDiff(DateInterval.Second, dZero, fileFlash.LastWriteTime)
                    If lDiff > currentDiff Then
                        If currentFile <> "" Then
                            oLogger.WriteToLogRelative("it's newer than previous found file '" & currentFile & "', so using this newer one", , 5)
                        End If

                        currentDiff = lDiff
                        currentFile = fileFlash.Name
                    End If

                    bFound = True
                End If
            End If
        Next

        If Not bFound Then
            oLogger.WriteToLogRelative("no flash packages found", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        Else
            g_FlashInstaller = c_FlashInstallerDir & "\" & currentFile
        End If

        Return bFound
    End Function

    Private Sub InstallFlash()
        oLogger.WriteToLog("Searching for Flash installer", , 0)
        If Not FindFlashInstaller() Then
            FailedInstall()
            Exit Sub
        End If


        oLogger.WriteToLog("Getting rid of offending processes", , 0)
        StopOffendingProcesses()


        oLogger.WriteToLog("Installing Flash", , 0)
        UpdateProgress("installing updates")
        tmrInstallerWait.Enabled = True

        Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallFlash))
        Me.installThread.Start()
    End Sub

    Private Sub _InstallFlash()
        oLogger.WriteToLogRelative("Installing Flash", , 1)
        oLogger.WriteToLogRelative("Path   : " & g_FlashInstaller, , 2)
        oLogger.WriteToLogRelative("Params : " & c_FlashInstallerParameters, , 2)
        oLogger.WriteToLogRelative("Timeout: 900000ms", , 2)

        oProcess = New ProcessRunner
        oProcess.FileName = g_FlashInstaller
        oProcess.Arguments = c_FlashInstallerParameters
        'oProcess.ExternalAppToWaitFor = "InstallFlashPlayer"
        oProcess.MaxTimeout = 900000
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub FailedInstall()
        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 0)
        Application.Exit()
    End Sub

    Private Sub PostInstall()
        oLogger.WriteToLog("Done", , 0)


        If g_TESTMODE Then
            oLogger.WriteToLog("Rebooting (not!)", , 1)
            UpdateProgress("done, rebooting, NOT!")
            Me.Close()
        Else
            oLogger.WriteToLog("Rebooting", , 1)
            UpdateProgress("done, rebooting")
            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        End If
    End Sub

    Private Sub StopOffendingProcesses()
        oLogger.WriteToLogRelative("killing processes", , 1)

        UpdateProgress("killing processes (0%)")
        _KillProcess("Configure")
        _KillProcess("SiteKiosk")
        _KillProcess("skpcdsvc")
        _KillProcess("SmartCard")
        _KillProcess("SystemSecurity")
        _KillProcess("Watchdog")
        _KillProcess("SessionMonitor")
        _KillProcess("SiteRemoteClientService")
        _KillProcess("SKScreenshot")
        _KillProcess("msiexec")
        _KillProcess("iexplore")
    End Sub

    Private Sub _KillProcess(ByVal sProcessName As String)
        Dim p As System.Diagnostics.Process

        oLogger.WriteToLog(sProcessName, , 2)
        For Each p In System.Diagnostics.Process.GetProcessesByName(sProcessName)
            Try
                p.Kill()
                oLogger.WriteToLogRelative("dead", , 3)
            Catch ex As Exception
                oLogger.WriteToLogRelative("zombie", , 3)
            End Try
        Next
    End Sub
#End Region

#Region "Thread Safe stuff"
    Private Sub UpdateProgress(ByVal [text] As String)
        lblProgress.Text = [text]
    End Sub

    Private Sub ShowHideProgress(ByVal [bool] As Boolean)
        lblProgress.Visible = [bool]
    End Sub
#End Region


#Region "Process Events"
    Private Sub oProcess_Done(ByVal ExitCode As Integer, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLogRelative("done", , 2)
        oLogger.WriteToLogRelative("time elapsed: " & TimeElapsed.ToString & " sec", , 3)
        oLogger.WriteToLogRelative("exit code: " & ExitCode.ToString, , 3)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLogRelative("failed", , 2)
        oLogger.WriteToLogRelative(ErrorMessage, , 3)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal pId As Integer, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLogRelative("started", , 2)
        oLogger.WriteToLogRelative("pid: " & pId.ToString, , 3)
    End Sub
#End Region


    Private Sub tmrInstallerWait_Tick(sender As Object, e As EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            PostInstall()
        End If
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click

    End Sub

    Private Sub lblTitle_Click(sender As Object, e As EventArgs) Handles lblTitle.Click

    End Sub
End Class
