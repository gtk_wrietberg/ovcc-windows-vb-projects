﻿Imports System.Windows.Forms

Public Class Fullscreen
    Private Shared mOldBounds As Rectangle

    Public Shared Sub Enter(targetForm As Form, Optional OnTop As Boolean = True)
        mOldBounds = targetForm.Bounds

        targetForm.WindowState = FormWindowState.Normal
        targetForm.FormBorderStyle = FormBorderStyle.None
        targetForm.Bounds = Screen.PrimaryScreen.WorkingArea
        targetForm.TopMost = OnTop
    End Sub

    Public Shared Sub Leave(targetForm As Form)
        targetForm.FormBorderStyle = FormBorderStyle.FixedSingle
        targetForm.WindowState = FormWindowState.Normal
        targetForm.Bounds = mOldBounds
    End Sub
End Class
