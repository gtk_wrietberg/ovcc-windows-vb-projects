Imports System.Diagnostics
Imports System.Threading

Public Class ProcessRunner
    Private WithEvents myProcess As New System.Diagnostics.Process
    Private elapsedTime As Double

    Private Const cSleepAmount As Double = 0.5#

    Private Const OneSec As Double = 1.0# / (1440.0# * 60.0#)

    Private mMaxTimeout As Double = 30
    Private mProcessStyle As ProcessWindowStyle

    Private mFileName As String
    Private mArguments As String
    Private mUserName As String
    Private mPassWord As String

    Private watch As Stopwatch

    Private mProcessExitCode As Integer
    Private mIsProcessDone As Boolean

    Public Event Started(ByVal pId As Integer, ByVal TimeElapsed As Double)
    Public Event Failed(ByVal TimeElapsed As Double, ByVal ErrorMessage As String)
    Public Event Done(ByVal ExitCode As Integer, ByVal TimeElapsed As Double)

    Public Sub New()
        mMaxTimeout = 30000
        mProcessStyle = ProcessWindowStyle.Normal

        mFileName = ""
        mArguments = ""
        mUserName = ""
        mPassWord = ""

        mIsProcessDone = False

        elapsedTime = 0.0
    End Sub

#Region "Properties"
    Public Property MaxTimeout() As Double
        Get
            Return mMaxTimeout
        End Get
        Set(ByVal value As Double)
            mMaxTimeout = value
        End Set
    End Property

    Public Property ProcessStyle() As ProcessWindowStyle
        Get
            Return mProcessStyle
        End Get
        Set(ByVal value As ProcessWindowStyle)
            mProcessStyle = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return mFileName
        End Get
        Set(ByVal value As String)
            mFileName = value
        End Set
    End Property

    Public Property Arguments() As String
        Get
            Return mArguments
        End Get
        Set(ByVal value As String)
            mArguments = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set
    End Property

    Public Property PassWord() As String
        Get
            Return mPassWord
        End Get
        Set(ByVal value As String)
            mPassWord = value
        End Set
    End Property

    Public ReadOnly Property IsProcessDone() As Boolean
        Get
            Return mIsProcessDone
        End Get
    End Property

    Public ReadOnly Property ProcessExitCode() As Integer
        Get
            Return mProcessExitCode
        End Get
    End Property
#End Region

    Public Sub StartProcess()
        StartProcess(mFileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String)
        StartProcess(FileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String, ByVal Arguments As String)
        Try
            myProcess.StartInfo.FileName = FileName
            myProcess.StartInfo.Arguments = Arguments
            If mUserName <> "" Then
                Dim securePass As New Security.SecureString()

                For Each c As Char In mPassWord
                    securePass.AppendChar(c)
                Next c

                myProcess.StartInfo.UseShellExecute = True
                myProcess.StartInfo.UserName = mUserName
                myProcess.StartInfo.Password = securePass
            End If
            myProcess.StartInfo.WindowStyle = mProcessStyle
            myProcess.EnableRaisingEvents = True

            watch = Stopwatch.StartNew()

            myProcess.Start()

            RaiseEvent Started(myProcess.Id, elapsedTime)

            myProcess.WaitForExit(mMaxTimeout)

            watch.Stop()
            elapsedTime = Math.Round(watch.Elapsed.TotalMilliseconds / 1000, 3)

            mProcessExitCode = myProcess.ExitCode
        Catch ex As Exception
            mProcessExitCode = -1
            RaiseEvent Failed(elapsedTime, ex.Message)
        End Try

        Try
            myProcess.Close()
        Catch ex As Exception

        End Try

        RaiseEvent Done(mProcessExitCode, elapsedTime)
    End Sub

    Public Sub ProcessDone()
        mIsProcessDone = True
    End Sub
End Class
