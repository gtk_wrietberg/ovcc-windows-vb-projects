Module Globals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public g_OverwriteExistingConfig As Boolean

    Public g_TESTMODE As Boolean

    Public g_FlashInstaller As String = ""

    'Public ReadOnly c_FlashInstaller As String = "files\install_flash_player_17_active_x.exe"

    Public ReadOnly c_FlashInstallerPattern As String = "install_flash_player"
    Public ReadOnly c_FlashInstallerDir As String = "files"
    Public ReadOnly c_FlashInstallerExt As String = ".exe"


    Public ReadOnly c_FlashInstallerParameters As String = "-install"


    Public Sub InitGlobals()
        Dim sComputerName As String = Environment.MachineName.ToLower

        If InStr(sComputerName, "rietberg") > 0 _
        Or InStr(sComputerName, "superlekkerding") > 0 _
        Or InStr(sComputerName, "wrdev") > 0 Then
            g_TESTMODE = True
        End If


        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception
            MessageBox.Show("FAIL IO.Directory.CreateDirectory(g_LogFileDirectory): " & ex.Message)
        End Try

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

    End Sub
End Module
