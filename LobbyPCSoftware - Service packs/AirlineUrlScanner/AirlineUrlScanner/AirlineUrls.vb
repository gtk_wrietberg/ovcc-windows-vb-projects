﻿Public Class AirlineUrls
    Private mList As List(Of String)

    Public ReadOnly Property Count As Integer
        Get
            Return mList.Count
        End Get
    End Property

    Public ReadOnly Property List As List(Of String)
        Get
            Return mList
        End Get
    End Property

    Public Sub New()
        mList = New List(Of String)
    End Sub

    Public Sub Add(s As String)
        Try
            Dim qUri As New Uri(s)

            If Not mList.Contains(qUri.Host) Then
                mList.Add(qUri.Host)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private mCount As Integer = 0
    Public Sub GetReset()
        mCount = 0
    End Sub

    Public Function GetNext(ByRef sItem As String) As Boolean
        If mCount >= 0 And mCount < mList.Count Then
            sItem = mList.Item(mCount)
            mCount += 1
            Return True
        Else
            sItem = ""
            Return False
        End If
    End Function
End Class
