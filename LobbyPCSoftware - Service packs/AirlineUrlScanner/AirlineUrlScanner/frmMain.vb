﻿Imports System.Text.RegularExpressions
Imports System.Net
Imports System.IO
Imports System.Text

Public Class frmMain
    Private doneEvent As Threading.AutoResetEvent = New Threading.AutoResetEvent(False)

    Private oAirlines As AirlineUrlData

    Private sPattern_HREF As String = "href\s*=\s*(?:[""'](?<1>[^""']*)[""']|(?<1>\S+))"
    Private sPattern_ACTION As String = "action\s*=\s*(?:[""'](?<1>[^""']*)[""']|(?<1>\S+))"


    Delegate Sub LogTextBoxCallback(ByVal [text] As String)

    Private Sub WriteLogTextBox(ByVal [text] As String)
        'If Me.txtLog.InvokeRequired Then
        '    Dim d As New LogTextBoxCallback(AddressOf WriteLogTextBox)
        '    Me.Invoke(d, New Object() {[text]})
        'Else
        '    Me.txtLog.AppendText([text])
        'End If

        Me.txtLog.AppendText([text] & vbCrLf)
    End Sub



    Private Sub btnGO_Click(sender As Object, e As EventArgs) Handles btnGO.Click
        btnGO.Enabled = False
        txtInput.Enabled = False
        txtOutput.Clear()
        txtLog.Clear()

        oAirlines = New AirlineUrlData

        bgWorker.RunWorkerAsync()
        doneEvent.WaitOne()


        Dim stmp As String = ""
        Dim sUrl As String = ""
        Do While oAirlines.GetNext(sUrl)
            stmp &= sUrl & vbCrLf
        Loop

        txtOutput.Text = stmp



        btnGO.Enabled = True
        txtInput.Enabled = True
    End Sub

    Private Sub BGW_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        CollectUrls(txtInput.Text)

        'do it again with the collected urls
        'Dim tmpList As New List(Of String)

        'tmpList = oAirlines.List

        'For Each tmpUrl As String In tmpList
        '    If Not tmpUrl.StartsWith("http") Then
        '        tmpUrl = "http://" & tmpUrl
        '    End If

        '    Console.WriteLine(tmpUrl)
        '    CollectUrls(tmpUrl)
        'Next


        doneEvent.Set()
    End Sub

    Private Sub CollectUrls(sSourceUrl As String)
        Try
            Dim sTmp As String, iRet As AirlineUrlData.AddReturnValue
            Dim iAddCounter As Integer, iIgnoreCounter As Integer

            WriteLogTextBox("getting source for: " & sSourceUrl)

            'download html source
            Dim sHtmlSource As String = GetPageHTML(sSourceUrl)

            Dim matches1 As MatchCollection = Regex.Matches(sHtmlSource, sPattern_HREF)

            WriteLogTextBox("found " & matches1.Count.ToString & " matches for links")

            iAddCounter = 0
            iIgnoreCounter = 0
            'For Each m As Match In matches1
            '    sTmp = Regex.Replace(m.Value, sPattern_HREF, "$1")
            '    If sTmp.StartsWith("http") Or sTmp.StartsWith("//") Then
            '        If oAirlines.Add(sTmp) Then iAddCounter += 1
            '    Else
            '        iIgnoreCounter += 1
            '    End If
            'Next
            For Each m As Match In matches1
                sTmp = Regex.Replace(m.Value, sPattern_HREF, "$1")
                iRet = oAirlines.Add(sTmp)

                If sTmp.StartsWith("http") Or sTmp.StartsWith("//") Then
                    If oAirlines.Add(sTmp) Then iAddCounter += 1
                Else
                    iIgnoreCounter += 1
                End If
            Next


            If iIgnoreCounter = 1 Then
                WriteLogTextBox("ignored 1 local link")
            Else
                WriteLogTextBox("ignored " & iIgnoreCounter.ToString & " local links")
            End If
            If iAddCounter = 1 Then
                WriteLogTextBox("added 1 host")
            Else
                WriteLogTextBox("added " & iAddCounter.ToString & " hosts")
            End If


            Dim matches2 As MatchCollection = Regex.Matches(sHtmlSource, sPattern_ACTION)

            WriteLogTextBox("found " & matches2.Count.ToString & " matches for forms")

            iAddCounter = 0
            iIgnoreCounter = 0
            For Each m As Match In matches2
                sTmp = Regex.Replace(m.Value, sPattern_ACTION, "$1")
                If sTmp.StartsWith("http") Or sTmp.StartsWith("//") Then
                    If oAirlines.Add(sTmp) Then iAddCounter += 1
                Else
                    iIgnoreCounter += 1
                End If
            Next

            WriteLogTextBox("ignored " & iIgnoreCounter.ToString & " local links")
            WriteLogTextBox("added " & iAddCounter.ToString & " hosts")
        Catch ex As Exception
            MsgBox(ex.Message)
            'WriteLogTextBox("exception: " & ex.Message)
        End Try
    End Sub

    Private Function GetPageHTML(ByVal sUrl As String, Optional ByVal iTimeout As Integer = 10) As String
        Dim req As WebRequest
        Dim resp As WebResponse
        Dim stream_resp As Stream
        Dim enc As Encoding
        Dim stream_read As StreamReader
        Dim sret As String = ""

        Try
            req = WebRequest.Create(sUrl)
            req.Timeout = iTimeout * 1000

            resp = req.GetResponse
            stream_resp = resp.GetResponseStream

            enc = Encoding.GetEncoding("utf-8")
            stream_read = New StreamReader(stream_resp, enc)
            sret = stream_read.ReadToEnd()

            If Not resp Is Nothing Then
                resp.Close()
            End If
        Catch
            sret = ""
        End Try

        Return sret
    End Function

    Private Sub BGW_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgWorker.RunWorkerCompleted

    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Windows.Forms.Control.CheckForIllegalCrossThreadCalls = False
    End Sub
End Class