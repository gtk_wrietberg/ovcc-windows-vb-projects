﻿Public Class AirlineUrlDataCounter
    Private mAdded As Integer
    Public ReadOnly Property Adds() As Integer
        Get
            Return mAdded
        End Get
    End Property

    Private mInvalid As Integer
    Public ReadOnly Property Invalids() As String
        Get
            Return mInvalid
        End Get
    End Property

    Private mDuplicate As Integer
    Public ReadOnly Property Duplicates() As String
        Get
            Return mDuplicate
        End Get
    End Property

    Private mError As Integer
    Public ReadOnly Property Errors() As String
        Get
            Return mError
        End Get
    End Property

    Public Sub Add(Optional iAdd As Integer = 0, Optional iInvalid As Integer = 0, Optional iDuplicate As Integer = 0, Optional iError As Integer = 0)
        mAdded += iAdd
        mInvalid += iInvalid
        mDuplicate += iDuplicate
        mError += iError
    End Sub
End Class
