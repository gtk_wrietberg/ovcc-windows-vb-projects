﻿Public Class AirlineUrlData
    Private mList As List(Of String)

    Public Enum AddReturnValue As Integer
        Ok = 0
        EXISTS = 1
        INVALID = 2
        WTF = 3
    End Enum

    Public ReadOnly Property Count As Integer
        Get
            Return mList.Count
        End Get
    End Property

    Public ReadOnly Property List As List(Of String)
        Get
            Return mList
        End Get
    End Property

    Public Sub New()
        mList = New List(Of String)
    End Sub

    Public Function Add(s As String) As AddReturnValue
        Try
            Dim qUri As New Uri(s)

            If Not mList.Contains(qUri.Host) Then
                mList.Add(qUri.Host)

                Return AddReturnValue.Ok
            Else
                Return AddReturnValue.EXISTS
            End If
        Catch ex As Exception
            Return AddReturnValue.INVALID
        End Try

        Return AddReturnValue.WTF
    End Function

    Private mGetCounter As Integer = 0

    Public Sub GetReset()
        mGetCounter = 0
    End Sub

    Public Function GetNext(ByRef sUrl As String) As Boolean
        If mGetCounter >= 0 And mGetCounter < mList.Count Then
            sUrl = mList.Item(mGetCounter)
            mGetCounter += 1
            Return True
        Else
            sUrl = ""
            Return False
        End If
    End Function

End Class
