Module ModMain
    Public Sub main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        Dim arg As String, arg_index As Integer
        For arg_index = 1 To Environment.GetCommandLineArgs.Length - 1
            arg = Environment.GetCommandLineArgs(arg_index)

            oLogger.WriteToLog("found: " & arg, , 1)

            If arg = "-on" Then
                g_AirlineButton_State.State = Tristate.States._True
            End If
            If arg = "-off" Then
                g_AirlineButton_State.State = Tristate.States._False
            End If
        Next

        If g_AirlineButton_State.State = Tristate.States._Undefined Then
            oLogger.WriteToLog("No state defined (on/off)!", Logger.MESSAGE_TYPE.LOG_ERROR, 0)
            oLogger.WriteToLog("Exiting!", Logger.MESSAGE_TYPE.LOG_ERROR, 0)
            Environment.Exit(1)
            Exit Sub
        End If



        oLogger.WriteToLog("Updating all", , 0)
        UpdateAirlineButtons()

        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
        Environment.Exit(0)
    End Sub
End Module
