Module UpdateAirlineButton_Old_New
    Public Sub UpdateAirlineButtons()
        Dim bRet As Boolean

        oLogger.WriteToLog("Iterating through script files", , 1)

        oLogger.WriteToLog("Old gui (iBAHN)", , 2)
        bRet = _ReplaceSetting_OLDSTYLE_iBAHN()
        If bRet Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        oLogger.WriteToLog("Old gui (Hilton)", , 2)
        bRet = _ReplaceSetting_OLDSTYLE_Hilton()
        If bRet Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        oLogger.WriteToLog("New UI (iBAHN)", , 2)
        bRet = _ReplaceSetting_NEWSTYLE_iBAHN()
        If bRet Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        oLogger.WriteToLog("New UI (Hilton)", , 2)
        bRet = _ReplaceSetting_NEWSTYLE_Hilton()
        If bRet Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
    End Sub

    Private Function _ReplaceSetting_OLDSTYLE_iBAHN() As Boolean
        Dim lines As New List(Of String)

        oLogger.WriteToLogRelative("Updating script file (old gui)", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Html\GUIv2.1\js\configuration.js"
        sFile = sFile.Replace("\\", "\")

        oLogger.WriteToLogRelative("File: " & sFile, , 2)

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFile &= "\Html\GUIv2\js\configuration.js"
            sFile = sFile.Replace("\\", "\")

            oLogger.WriteToLogRelative("File: " & sFile, , 2)

            If Not IO.File.Exists(sFile) Then
                oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Return False
            End If
        End If

        Dim bAirlinesFound As Boolean = False
        Dim bFileChanged As Boolean = False

        oLogger.WriteToLogRelative("updating", , 3)
        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String, tmpline As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If bAirlinesFound Then
                        If line.Contains("aButtons['active']") Then
                            bAirlinesFound = False
                            If g_AirlineButton_State.State = Tristate.States._True Then
                                tmpline = line.Replace("false", "true")
                            Else
                                tmpline = line.Replace("true", "false")
                            End If
                            If Not tmpline.Equals(line) Then
                                line = tmpline
                                bFileChanged = True
                            End If
                        End If
                    End If

                    If line.Contains("//Airlines") Then
                        bAirlinesFound = True
                    End If

                    lines.Add(line)
                End While
            End Using

            If bFileChanged Then
                oLogger.WriteToLogRelative("writing", , 3)
                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
            Else
                oLogger.WriteToLogRelative("not writing, no change", , 3)
            End If

            Return True
        Catch ex As Exception : Return False : End Try
    End Function

    Private Function _ReplaceSetting_OLDSTYLE_Hilton() As Boolean
        Dim lines As New List(Of String)

        oLogger.WriteToLogRelative("Updating script file (old gui)", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Html\GUIv2.1_Hilton\js\configuration.js"
        sFile = sFile.Replace("\\", "\")

        oLogger.WriteToLogRelative("File: " & sFile, , 2)

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFile &= "\Html\GUIv2_Hilton\js\configuration.js"
            sFile = sFile.Replace("\\", "\")

            oLogger.WriteToLogRelative("File: " & sFile, , 2)

            If Not IO.File.Exists(sFile) Then
                oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Return False
            End If
        End If

        Dim bAirlinesFound As Boolean = False
        Dim bFileChanged As Boolean = False

        oLogger.WriteToLogRelative("updating", , 3)
        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String, tmpline As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If bAirlinesFound Then
                        If line.Contains("aButtons['active']") Then
                            bAirlinesFound = False
                            If g_AirlineButton_State.State = Tristate.States._True Then
                                tmpline = line.Replace("false", "true")
                            Else
                                tmpline = line.Replace("true", "false")
                            End If
                            If Not tmpline.Equals(line) Then
                                line = tmpline
                                bFileChanged = True
                            End If
                        End If
                    End If

                    If line.Contains("//Airlines") Then
                        bAirlinesFound = True
                    End If

                    lines.Add(line)
                End While
            End Using

            If bFileChanged Then
                oLogger.WriteToLogRelative("writing", , 3)
                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
            Else
                oLogger.WriteToLogRelative("not writing, no change", , 3)
            End If

            Return True
        Catch ex As Exception : Return False : End Try
    End Function

    Private Function _ReplaceSetting_NEWSTYLE_Hilton() As Boolean
        Dim lines As New List(Of String)

        oLogger.WriteToLogRelative("Updating script file (Hilton newUI)", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\Startpages\Hilton\js\buttons.js"
        sFile = sFile.Replace("\\", "\")

        oLogger.WriteToLogRelative("File: " & sFile, , 2)

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End If

        Dim bAirlinesFound As Boolean = False
        Dim bFileChanged As Boolean = False

        oLogger.WriteToLogRelative("updating", , 3)
        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine
                    '_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenFreeAirlineDialog()"><img src="img/icons/boarding.png" alt="" border="0"></a>');
                    '_Labels.push('<a class="aButtons" onclick="javascript:iBAHN__OpenFreeAirlineDialog()">'+LoadString(53)+'</a>');

                    If line.Contains("javascript:iBAHN__OpenFreeAirlineDialog()") Then
                        If g_AirlineButton_State.State = Tristate.States._True Then
                            If line.StartsWith("//") Then
                                line = line.Replace("//", "")
                                bFileChanged = True
                            End If
                        Else
                            If Not line.StartsWith("//") Then
                                line = "//" & line
                                bFileChanged = True
                            End If
                        End If
                    End If

                    lines.Add(line)
                End While
            End Using

            If bFileChanged Then
                oLogger.WriteToLogRelative("writing", , 3)
                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
            Else
                oLogger.WriteToLogRelative("not writing, no change", , 3)
            End If

            Return True
        Catch ex As Exception : Return False : End Try
    End Function

    Private Function _ReplaceSetting_NEWSTYLE_iBAHN() As Boolean
        Dim lines As New List(Of String)

        oLogger.WriteToLogRelative("Updating script file (iBAHN newUI)", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\Startpages\iBAHN\scripts\buttons.js"
        sFile = sFile.Replace("\\", "\")

        oLogger.WriteToLogRelative("File: " & sFile, , 2)

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End If

        Dim bAirlinesFound As Boolean = False
        Dim bFileChanged As Boolean = False

        oLogger.WriteToLogRelative("updating", , 3)
        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine
                    '_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenFreeAirlineDialog()"><img src="img/icons/boarding.png" alt="" border="0"></a>');
                    '_Labels.push('<a class="aButtons" onclick="javascript:iBAHN__OpenFreeAirlineDialog()">'+LoadString(53)+'</a>');

                    If line.Contains("javascript:iBAHN__OpenFreeAirlineDialog()") Then
                        If g_AirlineButton_State.State = Tristate.States._True Then
                            If line.StartsWith("//") Then
                                line = line.Replace("//", "")
                                bFileChanged = True
                            End If
                        Else
                            If Not line.StartsWith("//") Then
                                line = "//" & line
                                bFileChanged = True
                            End If
                        End If
                    End If

                    lines.Add(line)
                End While
            End Using

            If bFileChanged Then
                oLogger.WriteToLogRelative("writing", , 3)
                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
            Else
                oLogger.WriteToLogRelative("not writing, no change", , 3)
            End If

            Return True
        Catch ex As Exception : Return False : End Try
    End Function

End Module
