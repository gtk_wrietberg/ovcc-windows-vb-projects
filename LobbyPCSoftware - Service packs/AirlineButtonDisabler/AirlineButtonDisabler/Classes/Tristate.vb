Public Class Tristate
    Public Enum States
        _True = 0
        _False = 1
        _Undefined = 2
    End Enum

    Public State As States

    Public Shared Operator And(ByVal cn1 As Tristate, ByVal cn2 As Tristate) As Tristate
        Dim Result As New Tristate
        Dim b1 As Boolean
        Dim b2 As Boolean

        If (cn1.State <> States._Undefined And cn2.State <> States._Undefined) Then
            Select Case cn1.State
                Case 0
                    b1 = True
                Case 1
                    b1 = False
            End Select

            Select Case cn2.State
                Case 0
                    b2 = True
                Case 1
                    b2 = False
            End Select

            Dim b3 As Boolean = b1 And b2

            If (b3) Then
                Result = New Tristate
                Result.State = States._True
            Else
                Result = New Tristate
                Result.State = States._False
            End If
        ElseIf (cn1.State <> States._Undefined Or cn2.State <> States._Undefined) Then
            If (cn1.State <> States._Undefined) Then
                Result = cn1
            Else
                Result = cn2
            End If
        Else
            Result = New Tristate
            Result.State = States._Undefined
        End If

        Return Result
    End Operator

    Public Shared Operator Or(ByVal cn1 As Tristate, ByVal cn2 As Tristate) As Tristate
        Dim Result As New Tristate
        Dim b1 As Boolean
        Dim b2 As Boolean

        If (cn1.State <> States._Undefined And cn2.State <> States._Undefined) Then
            Select Case cn1.State
                Case 0
                    b1 = True
                Case 1
                    b1 = False
            End Select

            Select Case cn2.State
                Case 0
                    b2 = True
                Case 1
                    b2 = False
            End Select

            Dim b3 As Boolean = b1 Or b2

            If (b3) Then
                Result = New Tristate
                Result.State = States._True
            Else
                Result = New Tristate
                Result.State = States._False
            End If
        ElseIf (cn1.State <> States._Undefined Or cn2.State <> States._Undefined) Then
            If (cn1.State <> States._Undefined) Then
                Result = cn1
            Else
                Result = cn2
            End If
        Else
            Result = New Tristate
            Result.State = States._Undefined
        End If

        Return Result
    End Operator
End Class
