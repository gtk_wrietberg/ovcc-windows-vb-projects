﻿Public Class frmMain
    Private p As Process

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            If Not p.HasExited Then
                p.Kill()
            End If
        Catch ex As Exception

        Finally
            Try
                p.Dispose()
            Catch ex As Exception

            End Try
        End Try
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Visible = False
        Me.Hide()

        Dim sUsername As String = Constants.cUser_SiteKiosk
        Dim sPassword As String = ""


        Dim sTmp As String = ""

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)

        Helpers.Logger.WriteMessage("Checking user", 0)
        Helpers.Logger.WriteMessage(Constants.cUser_SiteKiosk, 1)
        If Helpers.WindowsUser.CheckLogon(Constants.cUser_SiteKiosk, Constants.cUser_Password0, sTmp) Then
            Helpers.Logger.WriteMessage("CheckLogon() returned: " & sTmp, 2)
            Helpers.Logger.WriteMessage("using password 0", 2)

            sPassword = Constants.cUser_Password0
        ElseIf Helpers.WindowsUser.CheckLogon(Constants.cUser_SiteKiosk, Constants.cUser_Password1, sTmp) Then
            Helpers.Logger.WriteMessage("CheckLogon() returned: " & sTmp, 2)
            Helpers.Logger.WriteMessage("using password 1", 2)

            sPassword = Constants.cUser_Password1
        Else
            Helpers.Logger.WriteMessage("CheckLogon() returned: " & sTmp, 2)
            Helpers.Logger.WriteError("uh oh, I can´t seem to logon to '" & sUsername & "'", 2)

            ExitApplication()
            Exit Sub
        End If


        Dim sFileName As String = ""

        If Environment.GetCommandLineArgs().Length > 1 Then
            Helpers.Logger.WriteMessage("Command line arguments ", , 0)

            For Each arg As String In Environment.GetCommandLineArgs()
                Helpers.Logger.WriteMessage("found", , 1)
                Helpers.Logger.WriteMessage(arg, , 2)

                If InStr(arg, "--filename:") > 0 Then
                    sFileName = arg.Replace("--filename:", "")
                End If
            Next
        End If

        If sFileName.Equals("") Then
            Helpers.Logger.WriteError("No file specified!!!", , 0)

            ExitApplication()
            Exit Sub
        End If



        Dim sFile As String = Helpers.FilesAndFolders.GetCurrentFolder() & "\" & sFileName & ".exe"
        Dim sParams As String = ""
        Dim sWorkDir As String = Helpers.FilesAndFolders.GetCurrentFolder()

        If Not IO.File.Exists(sFile) Then
            Helpers.Logger.WriteError("File does not exist", , 0)
            Helpers.Logger.WriteError(sFile, , 1)

            ExitApplication()
            Exit Sub
        End If


        Helpers.Logger.WriteMessage("Starting process", 0)
        Helpers.Logger.WriteMessage(sFile, 1)


        p = New Process
        p = Helpers.Processes.StartProcessAsUser(sFile, sUsername, sPassword, sParams, sWorkDir, True)

        Try
            Dim iVoid As Integer = p.Id

            Helpers.Logger.WriteMessage("process id", 2)
            Helpers.Logger.WriteMessage(iVoid.ToString, 3)

            tmrProcessCheck.Enabled = True
        Catch ex As Exception
            'fail
            Helpers.Logger.WriteMessage("failed", 2)
            Helpers.Logger.WriteMessage("Last error", 3)
            Helpers.Logger.WriteMessage(Helpers.Errors.GetLast(False), 4)
            Helpers.Logger.WriteMessage("Exception", 3)
            Helpers.Logger.WriteMessage(ex.Message, 4)

            ExitApplication()
        End Try
    End Sub

    Private Sub tmrProcessCheck_Tick(sender As Object, e As EventArgs) Handles tmrProcessCheck.Tick
        Try
            If p.HasExited Then
                'close if child process has exited
                Helpers.Logger.WriteMessageRelative("process exited", 1)
                Helpers.Logger.WriteMessageRelative("exit code", 2)

                Try
                    Helpers.Logger.WriteMessageRelative(p.ExitCode.ToString, 3)
                Catch exc As Exception
                    Helpers.Logger.WriteErrorRelative("ERROR: " & exc.Message, 3)
                End Try

                ExitApplication()
            End If
        Catch ex As Exception
            'close if child process does not exist
            Helpers.Logger.WriteMessageRelative("process does not exist", 1)

            ExitApplication()
        End Try
    End Sub

    Private Sub ExitApplication()
        Helpers.Logger.WriteMessage("done", 0)
        Helpers.Logger.WriteMessage("bye", 1)

        Me.Close()
    End Sub
End Class
