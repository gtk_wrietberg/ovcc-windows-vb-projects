﻿Module modMain
    Public Sub Main()
        ExitCode.SetValue(ExitCode.ExitCodes.OK)

        If Not Helpers.Registry.SetValue_Integer("HKEY_CURRENT_USER\" & Constants.cSiteKioskRegistry_Key_BrowserEmulation, Constants.cSiteKioskRegistry_Name_AllSitesCompatibilityMode, Constants.cSiteKioskRegistry_Value_AllSitesCompatibilityMode) Then
            ExitCode.SetValue(ExitCode.ExitCodes.FAILED_AllSitesCompatibilityMode)
        End If

        If Not Helpers.Registry.SetValue_Integer("HKEY_CURRENT_USER\" & Constants.cSiteKioskRegistry_Key_BrowserEmulation, Constants.cSiteKioskRegistry_Name_MSCompatibilityMode, Constants.cSiteKioskRegistry_Value_MSCompatibilityMode) Then
            ExitCode.SetValue(ExitCode.ExitCodes.FAILED_MSCompatibilityMode)
        End If

        If Not Helpers.Registry.SetValue_Integer("HKEY_CURRENT_USER\" & Constants.cSiteKioskRegistry_Key_BrowserEmulation, Constants.cSiteKioskRegistry_Name_IntranetCompatibilityMode, Constants.cSiteKioskRegistry_Value_IntranetCompatibilityMode) Then
            ExitCode.SetValue(ExitCode.ExitCodes.FAILED_IntranetCompatibilityMode)
        End If

        ApplicationExit()
    End Sub

    Private Sub ApplicationExit()
        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
