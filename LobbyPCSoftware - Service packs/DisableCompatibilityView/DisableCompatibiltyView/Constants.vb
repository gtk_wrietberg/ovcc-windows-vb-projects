﻿Public Class Constants
    Public Shared ReadOnly cSiteKioskRegistry_Key_InternetSettings As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings"
    Public Shared ReadOnly cSiteKioskRegistry_Key_InternetSettings_Zone3 As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Zones\3"
    Public Shared ReadOnly cSiteKioskRegistry_Key_BrowserEmulation As String = "SOFTWARE\Microsoft\Internet Explorer\BrowserEmulation"


    Public Shared ReadOnly cSiteKioskRegistry_Name_DisplayMixedContent As String = "1609"
    Public Shared ReadOnly cSiteKioskRegistry_Value_DisplayMixedContent As Long = 0

    Public Shared ReadOnly cSiteKioskRegistry_Name_SecureProtocols As String = "SecureProtocols"
    Public Shared ReadOnly cSiteKioskRegistry_Value_SecureProtocols As Long = 2728 '0x00000aa8

    Public Shared ReadOnly cSiteKioskRegistry_Name_AllSitesCompatibilityMode As String = "AllSitesCompatibilityMode "
    Public Shared ReadOnly cSiteKioskRegistry_Value_AllSitesCompatibilityMode As Long = 0

    Public Shared ReadOnly cSiteKioskRegistry_Name_MSCompatibilityMode As String = "MSCompatibilityMode"
    Public Shared ReadOnly cSiteKioskRegistry_Value_MSCompatibilityMode As Long = 0

    Public Shared ReadOnly cSiteKioskRegistry_Name_IntranetCompatibilityMode As String = "IntranetCompatibilityMode"
    Public Shared ReadOnly cSiteKioskRegistry_Value_IntranetCompatibilityMode As Long = 0
End Class
