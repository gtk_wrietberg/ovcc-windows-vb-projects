﻿Module modMain
    Public Sub Main()
        ExitCode.SetValue(ExitCode.ExitCodes.OK)

        If Not Helpers.Registry.SetValue_Integer("HKEY_CURRENT_USER\" & Constants.cSiteKioskRegistry_Key_InternetSettings, Constants.cSiteKioskRegistry_Name_SecureProtocols, Constants.cSiteKioskRegistry_Value_SecureProtocols) Then
            ExitCode.SetValue(ExitCode.ExitCodes.FAILED_SecureProtocols)
        End If

        ApplicationExit()
    End Sub

    Private Sub ApplicationExit()
        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
