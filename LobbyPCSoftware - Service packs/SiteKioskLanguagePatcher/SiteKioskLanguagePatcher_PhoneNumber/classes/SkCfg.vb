Public Class SkCfg
    Private mSkCfgFile As String
    Private mSkBuild As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String
    Private mFreeSites As List(Of String)

    Private ReadOnly c_REGROOT_SITEKIOSK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"
    Private ReadOnly c_FreeUrlZoneName As String = "No Charge (HiltonWifi)"

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public ReadOnly Property SiteKioskFolder As String
        Get
            Return mSiteKioskFolder
        End Get
    End Property

    Public Sub AddFreeSiteToList(freeSite As String)
        mFreeSites.Add(freeSite)
    End Sub

    Public Sub CleanFreeSitesList()
        mFreeSites = New List(Of String)
    End Sub

    Public ReadOnly Property FreeSitesCount() As Integer
        Get
            Return mFreeSites.Count
        End Get
    End Property


    Public Function AddPagesToFreezone() As Boolean
        Dim bRet As Boolean = False

        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument


            oLogger.WriteToLogRelative("backing up current config", , 1)
            mBackupFolder = Globals.g_BackupDirectory

            Dim oFile As System.IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            oFile = New System.IO.FileInfo(mSkCfgFile)

            Dim sBackupPath As String = oFile.DirectoryName
            Dim sBackupName As String = oFile.Name
            Dim sBackupFile As String
            Dim oDirInfo As System.IO.DirectoryInfo
            sBackupPath = sBackupPath.Replace("C:", "")
            sBackupPath = sBackupPath.Replace("c:", "")
            sBackupPath = sBackupPath.Replace("\\", "\")
            sBackupPath = mBackupFolder & "\" & sBackupPath

            sBackupFile = sBackupPath & "\" & sBackupName

            oLogger.WriteToLogRelative("backup: " & mSkCfgFile & " => " & sBackupFile, , 2)

            oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oFile = New System.IO.FileInfo(mSkCfgFile)
            oFile.CopyTo(sBackupFile, True)

            oLogger.WriteToLogRelative("ok", , 3)


            oLogger.WriteToLogRelative("updating", , 1)
            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(mSkCfgFile, , 3)
            xmlSkCfg.Load(mSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If

            '*************************************************************************************************************
            'SiteCash - replace current url zones
            oLogger.WriteToLogRelative("SiteCash url zones update", , 2)
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode

            oLogger.WriteToLogRelative("loading SiteCash node", , 3)
            xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

            If Not xmlNode_SiteCash Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                Throw New Exception("SiteCash node not found")
            End If



            Dim xmlNodeList_UrlZones As Xml.XmlNodeList
            Dim xmlNode_UrlZone As Xml.XmlNode
            Dim xmlNodeList_Urls As Xml.XmlNodeList
            Dim xmlNode_Url As Xml.XmlNode
            Dim xmlNode_UrlClone As Xml.XmlNode
            Dim xmlNode_UrlCloneClone As Xml.XmlNode
            Dim xmlNode_FreeUrlZone As Xml.XmlNode

            oLogger.WriteToLogRelative("removing '" & c_FreeUrlZoneName & "' url zone, if there", , 3)
            xmlNodeList_UrlZones = xmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
            For Each xmlNode_UrlZone In xmlNodeList_UrlZones
                If xmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = c_FreeUrlZoneName Then
                    xmlNode_SiteCash.RemoveChild(xmlNode_UrlZone)
                    oLogger.WriteToLogRelative("ok", , 4)
                End If
            Next

            oLogger.WriteToLogRelative("adding '" & c_FreeUrlZoneName & "' url zone", , 3)
            xmlNode_FreeUrlZone = xmlNode_SiteCash.SelectSingleNode("sk:url-zone", ns).CloneNode(True)
            xmlNode_FreeUrlZone.SelectSingleNode("sk:name", ns).InnerText = c_FreeUrlZoneName

            xmlNodeList_Urls = xmlNode_FreeUrlZone.SelectNodes("sk:url", ns)
            xmlNode_UrlClone = xmlNode_FreeUrlZone.SelectSingleNode("sk:url", ns).CloneNode(True)
            For Each xmlNode_Url In xmlNodeList_Urls
                xmlNode_FreeUrlZone.RemoveChild(xmlNode_Url)
            Next

            For Each freeSite As String In mFreeSites
                oLogger.WriteToLogRelative("adding: " & Helpers.Network.GetHostnameFromUrl(freeSite), , 4)
                xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
                xmlNode_UrlCloneClone.InnerText = "http://" & Helpers.Network.GetHostnameFromUrl(freeSite)
                xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
                xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
                xmlNode_UrlCloneClone.InnerText = "https://" & Helpers.Network.GetHostnameFromUrl(freeSite)
                xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            Next

            xmlNode_SiteCash.AppendChild(xmlNode_FreeUrlZone)

            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(mSkCfgFile, , 3)
            xmlSkCfg.Save(mSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)

            bRet = True
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        Return bRet
    End Function

    Public Sub New()
        mSiteKioskFolder = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "InstallDir", "")
        mSkCfgFile = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "LastCfg", "")
        mSkBuild = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "Build", "")
        mFreeSites = New List(Of String)
    End Sub
End Class
