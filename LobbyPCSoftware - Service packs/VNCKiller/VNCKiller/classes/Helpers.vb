﻿Public Class Helpers
    Public Shared Function DoesKeyExist(subKey As String) As Boolean
        Dim regKey As Microsoft.Win32.RegistryKey

        regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(subKey, True)
        If regKey Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Function EnableService(subKey As String) As Boolean
        Try
            Dim regKey As Microsoft.Win32.RegistryKey

            regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(subKey, True)

            regKey.SetValue("Start", Globals.c_RegValue__AUTOMATIC, Microsoft.Win32.RegistryValueKind.DWord)

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("FAIL (DisableService)", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        End Try
    End Function

    Public Shared Function DisableService(subKey As String) As Boolean
        Try
            Dim regKey As Microsoft.Win32.RegistryKey

            regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(subKey, True)

            regKey.SetValue("Start", Globals.c_RegValue__DISABLED, Microsoft.Win32.RegistryValueKind.DWord)

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("FAIL (DisableService)", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        End Try
    End Function

End Class
