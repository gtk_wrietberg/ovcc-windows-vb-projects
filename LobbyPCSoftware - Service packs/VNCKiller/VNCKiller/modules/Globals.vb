Module Globals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public g_OverwriteExistingConfig As Boolean

    Public g_TESTMODE As Boolean

    Public ReadOnly c_RegKey__VNC4 As String = "SYSTEM\\CurrentControlSet\\Services\\WinVNC4"
    Public ReadOnly c_RegKey__VNC5 As String = "SYSTEM\\CurrentControlSet\\Services\\vncserver"
    Public ReadOnly c_RegKey__tVNC As String = "SYSTEM\\CurrentControlSet\\Services\\tvnserver"

    Public ReadOnly c_RegValue__DISABLED As Integer = 4
    Public ReadOnly c_RegValue__MANUAL As Integer = 3
    Public ReadOnly c_RegValue__AUTOMATIC As Integer = 2

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception
            MessageBox.Show("FAIL IO.Directory.CreateDirectory(g_LogFileDirectory): " & ex.Message)
        End Try

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

    End Sub
End Module
