﻿Module modMain
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        '------------------------------

        oLogger.WriteToLog("Disabling VNC", , 0)

        oLogger.WriteToLog("Looking for VNC4", , 1)
        If Helpers.DoesKeyExist(c_RegKey__VNC4) Then
            oLogger.WriteToLog("disabling", , 2)
            Helpers.DisableService(c_RegKey__VNC4)
        Else
            oLogger.WriteToLog("nope", , 2)
        End If

        oLogger.WriteToLog("Looking for VNC5", , 1)
        If Helpers.DoesKeyExist(c_RegKey__VNC5) Then
            oLogger.WriteToLog("disabling", , 2)
            Helpers.DisableService(c_RegKey__VNC5)
        Else
            oLogger.WriteToLog("nope", , 2)
        End If

        oLogger.WriteToLog("Looking for tVNC", , 1)
        If Helpers.DoesKeyExist(c_RegKey__tVNC) Then
            oLogger.WriteToLog("disabling", , 2)
            Helpers.DisableService(c_RegKey__tVNC)
        Else
            oLogger.WriteToLog("nope", , 2)
        End If

        oLogger.WriteToLog("done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub

End Module
