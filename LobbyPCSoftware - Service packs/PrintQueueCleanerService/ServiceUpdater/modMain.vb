﻿Imports System.ServiceProcess

Module modMain
    Public ReadOnly Property FOLDER__ProgramFiles As String
        Get
            Dim sTmp As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sTmp.Equals(String.Empty) Then
                sTmp = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            Return sTmp
        End Get
    End Property

    Public ReadOnly Property FOLDER__ServicePath As String
        Get
            Dim sTmp As String = FOLDER__ProgramFiles & "\GuestTek\PrintQueueCleanerService"

            Return sTmp
        End Get
    End Property

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 30))
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.WriteMessage("started")


        Dim sStmp As String = ""

        Helpers.Logger.WriteMessage("stopping service", 0)
        Helpers.Logger.WriteMessage(Constants.c__SERVICE_Name, 1)
        sStmp = ServiceInstaller.StopService(Constants.c__SERVICE_Name)
        Helpers.Logger.WriteMessage(sStmp, 4)

        Threading.Thread.Sleep(10000)


        'file copy
        Dim oCopy As New Helpers.FilesAndFolders.CopyFiles

        oCopy.SourceDirectory = "files"
        oCopy.DestinationDirectory = FOLDER__ServicePath
        oCopy.CopyFiles()

        Threading.Thread.Sleep(1000)


        Helpers.Logger.WriteMessage("starting", 2)
        ServiceInstaller.StartService(Constants.c__SERVICE_Name)

        Threading.Thread.Sleep(10000)


        Dim tSS As ServiceControllerStatus = ServiceInstaller.GetServiceStatus(Constants.c__SERVICE_Name)
        Helpers.Logger.WriteMessage("is running?", 2)
        Helpers.Logger.WriteMessage("service state", 3)
        Helpers.Logger.WriteMessage(tSS.ToString, 4)
        If tSS = ServiceControllerStatus.Running Or tSS = ServiceControllerStatus.StartPending Then
            Helpers.Logger.WriteMessage("ok", 3)
        Else
            Helpers.Logger.WriteError("FAIL", 3)
        End If


        '----------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("done")
    End Sub
End Module
