﻿Imports System.Text
Imports System.IO
Imports System.Management
Imports System.ServiceProcess

Public Class frmMain
    Private iJobs_total As Integer = 0

    Private iTimeoutMax As Integer = 120 'seconds
    Private iTimeout As Integer = iTimeoutMax

    Private mTriggeredMuch As Boolean = False

    Private mCloseCountDownMax As Integer = 15
    Private mCloseCountDown As Integer = mCloseCountDownMax
    Private mCloseCountDownThreshold As Integer = 10

    Private mCountdownPaused As Boolean = False

    Private mCleaningDone As Boolean = False

    Private mServiceFound As Boolean = False

    Private mCurrentServiceState As String = ""
    Private mLastServiceState As String = "USIDGFSDUGFSDUIGFISDFU"


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tmrJobCount.Enabled = True
        tmrClose.Enabled = True

        btnClear.Enabled = False

        Dim ssCleaner As ServiceControllerStatus = ServiceInstaller.GetServiceStatus(Constants.c__SERVICE_Name)
        'AddProgressMessage("ServiceState: " & ssCleaner.ToString)
        If ssCleaner = ServiceControllerStatus.Running Or ssCleaner = ServiceControllerStatus.StartPending Then
            AddProgressMessage("initialised")
            mServiceFound = True
        Else
            AddProgressMessage("ERROR: cleaner '" & Constants.c__SERVICE_Name & "' not found!")
            btnClear.Enabled = False
            mServiceFound = False
        End If


        Helpers.Forms.TopMost(Me.Handle, True)
    End Sub

    Private Function TriggerQueueCleanup() As Boolean
        Try
            Dim fs As FileStream = File.Create(Constants.c__Path_WindowsTemp & "\" & Constants.c__File_TriggerFile)

            ' Add text to the file.
            Dim info As Byte() = New UTF8Encoding(True).GetBytes(Helpers.Types.RandomString(32))
            fs.Write(info, 0, info.Length)
            fs.Close()

            Return True
        Catch ex As Exception
            Helpers.Errors.Add("trigger error: " & ex.Message)

            MessageBox.Show("Error: " & Helpers.Errors.GetLast(False), Me.Name, MessageBoxButtons.OK, MessageBoxIcon.Error)

            Return False
        End Try
    End Function

    Private Sub tmrJobCount_Tick(sender As Object, e As EventArgs) Handles tmrJobCount.Tick
        Dim bButtonState As Boolean = False

        mCountdownPaused = True

        CountPrintJobs()

        lblJobCount_total.Text = iJobs_total.ToString()

        If iJobs_total > 0 Then
            bButtonState = True

            If mTriggeredMuch Then
                bButtonState = False
            End If
        Else
            bButtonState = False
        End If

        If Not mServiceFound Then
            bButtonState = False
        End If

        btnClear.Enabled = bButtonState


        mCurrentServiceState = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__PQCS, Constants.RegistryKeys.VALUE__PQCS_State, "Error while retrieving status")
        If Not mCurrentServiceState.Equals(mLastServiceState) Then
            mLastServiceState = mCurrentServiceState

            If mServiceFound Then
                AddProgressMessage("Service status: " & mCurrentServiceState)
            Else
                'Let´s not display a message about a service that´s not there
            End If
        End If


        If iJobs_total = 0 And mCloseCountDown > mCloseCountDownThreshold Then
            If mCleaningDone Then
                AddProgressMessage("no jobs in queue; cleaning done")
            Else
                AddProgressMessage("no jobs in queue; no cleaning needed")
            End If

            btnClear.Enabled = False
        End If

        mCountdownPaused = False
    End Sub

    Private mLastMessage As String = ""
    Private Sub AddProgressMessage(str As String)
        AddProgressMessage(str, True, False)
    End Sub

    Private Sub AddProgressMessage(str As String, OverWriteLastLine As Boolean)
        AddProgressMessage(str, True, OverWriteLastLine)
    End Sub

    Private Sub AddProgressMessage(str As String, PreventRepeat As Boolean, OverWriteLastLine As Boolean)
        If OverWriteLastLine Then
            If txtMessage.Lines.Count > 0 Then
                Dim tempArray() As String
                tempArray = txtMessage.Lines
                tempArray(tempArray.Length - 1) = str
                txtMessage.Lines = tempArray
            End If
        Else
            If PreventRepeat And str.Equals(mLastMessage) Then
                'let´s not print this
            Else
                txtMessage.AppendText(str & vbCrLf)
            End If
        End If

        mLastMessage = str
    End Sub

    Private Sub CountPrintJobs()
        Dim oq As New System.Management.ObjectQuery("SELECT * FROM Win32_PrintJob")
        Dim query1 As New ManagementObjectSearcher(oq)
        Dim queryCollection1 As ManagementObjectCollection = query1.[Get]()

        iJobs_total = queryCollection1.Count.ToString
    End Sub

    Private Sub tmrClose_Tick(sender As Object, e As EventArgs) Handles tmrClose.Tick
        'main idle timeout
        iTimeout -= 1

        'Check if we´re counting down
        If Not mCountdownPaused Or iTimeout <= 0 Or Not mServiceFound Then
            mCloseCountDown -= 1
        End If

        'If we´re waiting on user input, and job>0, and service running: reset count down
        If iJobs_total > 0 And iTimeout > 0 And mServiceFound Then
            mCloseCountDown = mCloseCountDownMax
        End If

        'Check if main idle timeout is overtaking count down
        If iTimeout <= mCloseCountDownMax Then
            mCloseCountDown = iTimeout
        End If


        'count down
        If mCloseCountDown <= mCloseCountDownThreshold Then
            If mCloseCountDown = 1 Then
                AddProgressMessage("Auto-closing in 1 second", True)
            Else
                AddProgressMessage("Auto-closing in " & mCloseCountDown.ToString & " seconds", True)
            End If
        End If

        'check
        If mCloseCountDown <= 0 Then
            tmrJobCount.Enabled = False

            ExitApp()
        End If
    End Sub

    Private Sub ExitApp()
        Me.Close()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        mTriggeredMuch = True
        btnClear.Enabled = False

        iTimeout = iTimeoutMax

        AddProgressMessage("cleaning up queue")
        AddProgressMessage("service status: ")

        If TriggerQueueCleanup() Then
            'blah
            AddProgressMessage("waiting for cleaner")

            mCleaningDone = True
        Else
            AddProgressMessage("error while communication to cleaner:")
            AddProgressMessage(Helpers.Errors.GetLast(False))
        End If
    End Sub
End Class
