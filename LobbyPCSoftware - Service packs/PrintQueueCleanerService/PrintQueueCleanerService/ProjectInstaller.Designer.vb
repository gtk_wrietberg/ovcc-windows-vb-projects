﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PrintQueueCleanerServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.PrintQueueCleanerServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'PrintQueueCleanerServiceProcessInstaller
        '
        Me.PrintQueueCleanerServiceProcessInstaller.Password = Nothing
        Me.PrintQueueCleanerServiceProcessInstaller.Username = Nothing
        '
        'PrintQueueCleanerServiceInstaller
        '
        Me.PrintQueueCleanerServiceInstaller.ServiceName = "PrintQueueCleanerService"
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.PrintQueueCleanerServiceProcessInstaller, Me.PrintQueueCleanerServiceInstaller})

    End Sub
    Friend WithEvents PrintQueueCleanerServiceProcessInstaller As System.ServiceProcess.ServiceProcessInstaller
    Friend WithEvents PrintQueueCleanerServiceInstaller As System.ServiceProcess.ServiceInstaller

End Class
