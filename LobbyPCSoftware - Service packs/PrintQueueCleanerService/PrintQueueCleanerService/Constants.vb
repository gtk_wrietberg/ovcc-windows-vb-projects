﻿Public Class Constants
    Public Shared ReadOnly c__Path_WindowsTemp As String = Environment.ExpandEnvironmentVariables("%SystemRoot%") & "\Temp"
    Public Shared ReadOnly c__File_TriggerFile As String = "PQCS.trigger"

    Public Shared ReadOnly c__SERVICE_Spooler As String = "Spooler"
    Public Shared ReadOnly c__FILE_PrintFilterPipelineSvc As String = "printfilterpipelinesvc"
    Public Shared ReadOnly c__PATH_Printers As String = Environment.ExpandEnvironmentVariables("%SystemRoot%") & "\system32\spool\printers"

    Public Shared ReadOnly c__TIMEOUT_ServiceController As Integer = 20
    Public Shared ReadOnly c__TIMEOUT_Delay As Integer = 1000


    Public Shared ReadOnly c__SERVICE_Name As String = "PrintQueueCleanerService"
    Public Shared ReadOnly c__SERVICE_DisplayName As String = "Print Queue Cleaner Service"
    Public Shared ReadOnly c__SERVICE_Description As String = "Print Queue Cleaner Service"
    Public Shared ReadOnly c__SERVICE_Path As String = Helpers.FilesAndFolders.GetProgramFilesFolder & "\GuestTek\PrintQueueCleanerService\PrintQueueCleanerService.exe"

    Public Class RegistryKeys
        Public Shared ReadOnly KEY__PQCS As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\PrintQueueCleanerService"
        Public Shared ReadOnly VALUE__PQCS_State As String = "State"


        Public Shared ReadOnly DEFAULTVALUE__PQCS_State As String = ""

        Public Shared ReadOnly KEY__SITEKIOSK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"
        Public Shared ReadOnly VALUE__SiteKiosk_LastCfg As String = "LastCfg"

    End Class

    Public Class Installer
        Public Shared ReadOnly SK_APP_TITLE As String = "Clean Printer Queue"
        Public Shared ReadOnly SK_APP_DESCRIPTION As String = "Clean Printer Queue"

        Public Shared ReadOnly SK_APP_SEARCHSTRING As String = "PrintQueueCleanerTrigger"
    End Class

    Public Class UsernamePassword
        Public Class SiteKiosk
            Public Shared ReadOnly Username As String = "sitekiosk"
            Public Shared ReadOnly Password As String = "005d430300567d7d7936235d6f4c1f170e515b5316555a500a050d45110e59080a53555b150507527e7d78627101661f1a4750075b5015010854595104471c5e5d540c51550b4a0207057b78776121526818111a5157595b15015801080400161c0859580e51005947515353282e"
            Public Shared ReadOnly Password_old As String = "565247030055292e256a77016f1a1f130b535156415753065b0607471452590a0e530308120104547a7c233070566b101e47505f0b0312005c535907041a465c0c085c070552415055057e7c723225003b194f4109555c5517505e515d02051a1c5a085f0905570c47055151737a"
        End Class
    End Class

End Class
