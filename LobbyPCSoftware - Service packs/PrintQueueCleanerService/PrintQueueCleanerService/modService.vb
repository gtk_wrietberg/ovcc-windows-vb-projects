﻿Imports System.ServiceProcess

Public Class PrintQueueCleanerService
    Private mThreading_Main As Threading.Thread
    Private mLoopAbort As Boolean = False
    Private mFailCount As Long = 0
    Private mJobCountDelay As Integer = 0
    Private mTriggerFileCounter As Integer = 0
    Private mLoopTimeout As Integer = 1000


    Protected Overrides Sub OnStart(ByVal args() As String)
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 30))
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.WriteMessage("started")


        'Start the whole thing
        StartMainThread()
    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()
    End Sub

    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Thread_Main()
        If Not IO.Directory.Exists(Constants.c__Path_WindowsTemp) Then
            Exit Sub
        End If

        Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__PQCS, Constants.RegistryKeys.VALUE__PQCS_State, "OK")


        mJobCountDelay = 0

        Do While Not mLoopAbort
            If IO.File.Exists(Constants.c__Path_WindowsTemp & "\" & Constants.c__File_TriggerFile) Then
                Helpers.Logger.WriteMessage("triggered!!")

                mTriggerFileCounter += 1

                If mTriggerFileCounter > 10 Then
                    Helpers.Logger.WriteMessage("something is wrong, the trigger file has been here for a while!!! increasing the loop timeout!!!")

                    mLoopTimeout = 10000
                End If


                CleanPrinterQueue()
                CleanChromeUserData()

                Helpers.Logger.WriteMessage("removing trigger")
                IO.File.Delete(Constants.c__Path_WindowsTemp & "\" & Constants.c__File_TriggerFile)
            Else
                mTriggerFileCounter = 0

                mLoopTimeout = 1000
            End If

            Threading.Thread.Sleep(mLoopTimeout)
        Loop
    End Sub

    Private Sub CleanPrinterQueue()
        Printing.PrinterManagement.CountJobs()

        Helpers.Logger.WriteMessageRelative("cleaning print queue", 1)
        Helpers.Logger.WriteMessageRelative("jobs in queue", 2)
        Helpers.Logger.WriteMessageRelative("deleting", 3)
        Helpers.Logger.WriteMessageRelative(Printing.PrinterManagement.JobCount_deleting.ToString, 4)
        Helpers.Logger.WriteMessageRelative("error", 3)
        Helpers.Logger.WriteMessageRelative(Printing.PrinterManagement.JobCount_error.ToString, 4)
        Helpers.Logger.WriteMessageRelative("total", 3)
        Helpers.Logger.WriteMessageRelative(Printing.PrinterManagement.JobCount_total.ToString, 4)


        If Printing.PrinterManagement.JobCount_total > 0 Then
            If Not Printing.PrinterManagement.PurgeAll() Then
                Helpers.Logger.WriteErrorRelative("error", 2)
                Helpers.Logger.WriteErrorRelative(Helpers.Errors.GetLast(False), 3)

                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__PQCS, Constants.RegistryKeys.VALUE__PQCS_State, "ERROR: " & Helpers.Errors.GetLast(False))
            Else
                Helpers.Logger.WriteMessageRelative("ok", 2)

                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__PQCS, Constants.RegistryKeys.VALUE__PQCS_State, "OK")
            End If


            Helpers.Logger.WriteMessageRelative("3 second pause", 2)
            Threading.Thread.Sleep(3000)
            Helpers.Logger.WriteMessageRelative("ok", 3)


            Printing.PrinterManagement.CountJobs()

            Helpers.Logger.WriteMessageRelative("jobs in queue", 2)
            Helpers.Logger.WriteMessageRelative("deleting", 3)
            Helpers.Logger.WriteMessageRelative(Printing.PrinterManagement.JobCount_deleting.ToString, 4)
            Helpers.Logger.WriteMessageRelative("error", 3)
            Helpers.Logger.WriteMessageRelative(Printing.PrinterManagement.JobCount_error.ToString, 4)
            Helpers.Logger.WriteMessageRelative("total", 3)
            Helpers.Logger.WriteMessageRelative(Printing.PrinterManagement.JobCount_total.ToString, 4)
        Else
            Helpers.Logger.WriteError("no jobs found", 2)
        End If
    End Sub

    Private Sub CleanPrinterQueue__SLEDGEHAMMER()
        Helpers.Logger.WriteMessageRelative("cleaning printer queue")

        Dim sc As ServiceController
        Dim iStatusTimeout As Integer

        sc = New ServiceController(Constants.c__SERVICE_Spooler)
        iStatusTimeout = 0

        Helpers.Logger.WriteMessageRelative("stopping " & Constants.c__SERVICE_Spooler & " service", 1)
        sc = New ServiceController(Constants.c__SERVICE_Spooler)
        Try
            sc.Stop()
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("ERROR: Could not stop service ('" & ex.Message & "')", 2)

            Exit Sub
        End Try

        Do While Not sc.Status = ServiceControllerStatus.Stopped And iStatusTimeout < Constants.c__TIMEOUT_ServiceController
            iStatusTimeout += 1
            Threading.Thread.Sleep(Constants.c__TIMEOUT_Delay)

            sc.Refresh()
        Loop

        If sc.Status = ServiceControllerStatus.Stopped Then
            Helpers.Logger.WriteMessageRelative("ok", 2)
        Else
            Helpers.Logger.WriteErrorRelative(" FAILED (status = '" & sc.Status.ToString & "')", 2)
        End If


        Threading.Thread.Sleep(1000)
        '--------------------------------------------------------------------
        Dim ps() As Process = Process.GetProcessesByName(Constants.c__FILE_PrintFilterPipelineSvc)

        If ps.Count > 0 Then
            Helpers.Logger.WriteMessageRelative("killing '" & Constants.c__FILE_PrintFilterPipelineSvc & "' process", 1)

            For Each p As Process In ps
                p.Kill()
            Next

            Helpers.Logger.WriteMessageRelative("ok", 2)
        End If


        Threading.Thread.Sleep(1000)
        '--------------------------------------------------------------------

        Helpers.Logger.WriteMessageRelative("clearing spooler directory", 1)

        Dim pathSpool As String = Constants.c__PATH_Printers
        Dim iFileCount As Integer = -1

        Try
            iFileCount = IO.Directory.GetFiles(pathSpool).Count
            If iFileCount = 1 Then
                Helpers.Logger.WriteMessageRelative("found 1 file", 2)
            Else
                Helpers.Logger.WriteMessageRelative("found " & iFileCount.ToString & " files", 2)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteMessageRelative("ERROR: " & ex.Message, 2)
        End Try

        If iFileCount > 0 Then
            Try
                Helpers.Logger.WriteMessageRelative("deleting", 3)

                DeleteFilesFromFolder(pathSpool)

                Helpers.Logger.WriteMessageRelative("ok", 4)
            Catch ex As Exception
                Helpers.Logger.WriteMessageRelative(" ERROR: " & ex.Message, 4)
            End Try
        Else
            Helpers.Logger.WriteMessageRelative("no clearing needed", 3)
        End If


        Threading.Thread.Sleep(1000)
        '--------------------------------------------------------------------

        sc = New ServiceController(Constants.c__SERVICE_Spooler)
        iStatusTimeout = 0

        Helpers.Logger.WriteMessageRelative("starting " & Constants.c__SERVICE_Spooler & " service", 1)
        sc = New ServiceController(Constants.c__SERVICE_Spooler)
        Try
            sc.Start()
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("ERROR: Could not start service ('" & ex.Message & "')", 2)

            Exit Sub
        End Try

        Do While Not sc.Status = ServiceControllerStatus.Running And iStatusTimeout < Constants.c__TIMEOUT_ServiceController
            iStatusTimeout += 1
            Threading.Thread.Sleep(Constants.c__TIMEOUT_Delay)

            sc.Refresh()
        Loop

        If sc.Status = ServiceControllerStatus.Running Then
            Helpers.Logger.WriteMessageRelative("ok", 2)
        Else
            Helpers.Logger.WriteErrorRelative(" FAILED (status = '" & sc.Status.ToString & "')", 2)
        End If


        '--------------------------------------------------------------------

        Helpers.Logger.WriteMessageRelative("done", 1)
    End Sub


    Private Sub CleanChromeUserData()
        Helpers.Logger.WriteMessageRelative("cleaning chrome user data", 1)


        Dim sSiteKioskProfileDir As String = ""

        If Not Helpers.WindowsUser.GetProfileDirectory(Constants.UsernamePassword.SiteKiosk.Username, sSiteKioskProfileDir) Then
            Helpers.Logger.WriteErrorRelative("Could not find profile dir for user '" & Constants.UsernamePassword.SiteKiosk.Username & "'", 2)
            Exit Sub
        End If

        Dim sChromeUserDataDir As String = IO.Path.Combine(sSiteKioskProfileDir, "AppData\Local\Google\Chrome\User Data\")

        Helpers.Logger.WriteMessageRelative("nuking dir: " & sChromeUserDataDir, 2)

        If Not IO.Directory.Exists(sChromeUserDataDir) Then
            Helpers.Logger.WriteErrorRelative("doesn't exist!", 3)
            Exit Sub
        End If



        For i As Integer = 1 To 3
            Helpers.Logger.WriteMessageRelative("pass #" & i, 3)

            Try
                My.Computer.FileSystem.DeleteDirectory(sChromeUserDataDir, FileIO.DeleteDirectoryOption.DeleteAllContents)
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("ERROR!", 4)
                Helpers.Logger.WriteErrorRelative(ex.GetType().ToString, 5)
                Helpers.Logger.WriteErrorRelative(ex.Message, 6)
            End Try

            Threading.Thread.Sleep(500)
        Next
    End Sub

    Private Sub DeleteFilesFromFolder(Folder As String)
        If IO.Directory.Exists(Folder) Then
            For Each _file As String In IO.Directory.GetFiles(Folder)
                Console.Write(".")
                IO.File.Delete(_file)
            Next

            For Each _folder As String In IO.Directory.GetDirectories(Folder)
                DeleteFilesFromFolder(_folder)
            Next
        End If
    End Sub
End Class
