﻿Imports System.Printing

Public Class Printing
    Public Class PrinterManagement
        'Public Shared Function PurgeAll() As Boolean
        '    Dim bRet As Boolean = True

        '    Try
        '        Using ps As New LocalPrintServer()
        '            For Each psq As PrintQueue In ps.GetPrintQueues
        '                Try
        '                    Console.WriteLine("FullName: " & psq.FullName)
        '                    Using pq As New PrintQueue(ps, psq.FullName, PrintSystemDesiredAccess.AdministratePrinter)
        '                        Console.WriteLine("NumberOfJobs: " & pq.NumberOfJobs.ToString)
        '                        If pq.NumberOfJobs > 0 Then
        '                            pq.Purge()
        '                        End If
        '                    End Using
        '                Catch ex As Exception

        '                End Try
        '            Next
        '        End Using
        '    Catch ex As Exception
        '        Helpers.Errors.Add(ex.Message)

        '        bRet = False
        '    End Try

        '    Return bRet
        'End Function

        Private Shared mJobCount_total As Integer
        Public Shared ReadOnly Property JobCount_total() As Integer
            Get
                Return mJobCount_total
            End Get
        End Property

        Private Shared mJobCount_error As Integer
        Public Shared ReadOnly Property JobCount_error() As Integer
            Get
                Return mJobCount_error
            End Get
        End Property

        Private Shared mJobCount_deleting As Integer
        Public Shared ReadOnly Property JobCount_deleting() As Integer
            Get
                Return mJobCount_deleting
            End Get
        End Property

        Public Shared Function PurgeAll() As Boolean
            Dim bRet As Boolean = True

            Try
                Using ps As New LocalPrintServer()
                    For Each psq As PrintQueue In ps.GetPrintQueues
                        Try
                            Using pq As New PrintQueue(ps, psq.FullName, PrintSystemDesiredAccess.AdministratePrinter)
                                If pq.NumberOfJobs > 0 Then
                                    pq.Purge()
                                End If
                            End Using
                        Catch ex As Exception

                        End Try
                    Next
                End Using
            Catch ex As Exception
                Helpers.Errors.Add(ex.Message)

                bRet = False
            End Try

            Return bRet
        End Function

        Public Shared Function CountJobs() As Boolean
            mJobCount_total = 0
            mJobCount_error = 0
            mJobCount_deleting = 0

            Try
                Using ps As New LocalPrintServer()
                    For Each psq As PrintQueue In ps.GetPrintQueues
                        Try
                            Using pq As New PrintQueue(ps, psq.FullName, PrintSystemDesiredAccess.AdministratePrinter)
                                For Each job As PrintSystemJobInfo In pq.GetPrintJobInfoCollection()
                                    mJobCount_total += 1

                                    If job.JobStatus And PrintJobStatus.Error Then
                                        mJobCount_error += 1
                                    End If

                                    If job.JobStatus And PrintJobStatus.Deleting Then
                                        mJobCount_deleting += 1
                                    End If
                                Next
                            End Using
                        Catch ex As Exception

                        End Try
                    Next
                End Using

            Catch ex As Exception
                Helpers.Errors.Add(ex.Message)

                Return False
            End Try

            Return True
        End Function
    End Class
End Class
