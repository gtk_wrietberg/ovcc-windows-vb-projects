﻿Imports System.ServiceProcess

Module modMain
    Public ReadOnly Property FOLDER__ProgramFiles As String
        Get
            Dim sTmp As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sTmp.Equals(String.Empty) Then
                sTmp = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            Return sTmp
        End Get
    End Property

    Public ReadOnly Property FOLDER__ServicePath As String
        Get
            Dim sTmp As String = FOLDER__ProgramFiles & "\GuestTek\PrintQueueCleanerService"

            Return sTmp
        End Get
    End Property

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 30))
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.WriteMessage("started")


        'file copy
        Dim oCopy As New Helpers.FilesAndFolders.CopyFiles

        oCopy.SourceDirectory = "files"
        oCopy.DestinationDirectory = FOLDER__ServicePath
        oCopy.CopyFiles()





        Dim sStmp As String = ""

        Helpers.Logger.WriteMessage("installing service", 0)
        Helpers.Logger.WriteMessage(Constants.c__SERVICE_Name, 1)

        If ServiceInstaller.ServiceIsInstalled(Constants.c__SERVICE_Name) Then
            Helpers.Logger.WriteMessage("already exists", 2)
            Helpers.Logger.WriteMessage("stopping", 3)
            sStmp = ServiceInstaller.StopService(Constants.c__SERVICE_Name)
            Helpers.Logger.WriteMessage(sStmp, 4)

            Threading.Thread.Sleep(15000)

            Helpers.Logger.WriteMessage("uninstalling", 3)
            sStmp = ServiceInstaller.Uninstall(Constants.c__SERVICE_Name)
            Helpers.Logger.WriteMessage(sStmp, 4)

            Threading.Thread.Sleep(15000)

            Helpers.Logger.WriteMessage("is uninstalled?", 2)
            If Not ServiceInstaller.ServiceIsInstalled(Constants.c__SERVICE_Name) Then
                Helpers.Logger.WriteMessage("ok", 4)
            Else
                Helpers.Logger.WriteError("fail", 4)
            End If

            Threading.Thread.Sleep(5000)
        End If


        Helpers.Logger.WriteMessage("installing", 2)
        ServiceInstaller.InstallService(Constants.c__SERVICE_Name, Constants.c__SERVICE_DisplayName, FOLDER__ServicePath & "\PrintQueueCleanerService.exe")

        Threading.Thread.Sleep(5000)


        Helpers.Logger.WriteMessage("set description", 2)
        ServiceInstaller.ChangeServiceDescription(Constants.c__SERVICE_Name, Constants.c__SERVICE_Description)

        Helpers.Logger.WriteMessage("installed?", 2)
        If ServiceInstaller.ServiceIsInstalled(Constants.c__SERVICE_Name) Then
            Helpers.Logger.WriteMessage("ok", 3)

            Threading.Thread.Sleep(5000)

            Helpers.Logger.WriteMessage("starting", 2)
            ServiceInstaller.StartService(Constants.c__SERVICE_Name)

            Threading.Thread.Sleep(10000)
        Else
            Helpers.Logger.WriteMessage("fail", 3)
        End If


        Dim tSS As ServiceControllerStatus = ServiceInstaller.GetServiceStatus(Constants.c__SERVICE_Name)
        Helpers.Logger.WriteMessage("is running?", 2)
        Helpers.Logger.WriteMessage("service state", 3)
        Helpers.Logger.WriteMessage(tSS.ToString, 4)
        If tSS = ServiceControllerStatus.Running Or tSS = ServiceControllerStatus.StartPending Then
            Helpers.Logger.WriteMessage("ok", 3)
        Else
            Helpers.Logger.WriteError("FAIL", 3)
        End If


        '----------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("registry stuff", 2)
        Helpers.Logger.WriteMessage(Constants.RegistryKeys.KEY__PQCS, , 3)
        Helpers.Logger.Write(Constants.RegistryKeys.VALUE__PQCS_State, , 4)
        Helpers.Logger.Write(Constants.RegistryKeys.DEFAULTVALUE__PQCS_State, , 5)
        Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__PQCS, Constants.RegistryKeys.VALUE__PQCS_State, Constants.RegistryKeys.DEFAULTVALUE__PQCS_State)


        '----------------------------------------------------------------------------
        'sitekiosk stuff
        Helpers.Logger.WriteMessage("registry stuff", 2)
        Helpers.Logger.WriteMessage("reading", 3)
        Helpers.Logger.WriteMessage(Constants.RegistryKeys.KEY__SITEKIOSK, 4)
        Helpers.Logger.WriteMessage(Constants.RegistryKeys.VALUE__SiteKiosk_LastCfg, 5)

        Dim mSkCfgFile As String = ""

        mSkCfgFile = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__SITEKIOSK, Constants.RegistryKeys.VALUE__SiteKiosk_LastCfg)

        Helpers.Logger.WriteMessage("SkCfg file", 3)
        Helpers.Logger.WriteMessage(mSkCfgFile, 4)

        Try
            If Not IO.File.Exists(mSkCfgFile) Then
                Helpers.Logger.WriteMessage("does not exist!!!", 3)
            Else
                Dim nt As Xml.XmlNameTable
                Dim ns As Xml.XmlNamespaceManager
                Dim mSkCfgXml As Xml.XmlDocument
                Dim mXmlNode_Root As Xml.XmlNode
                Dim sTmp As String = ""


                mSkCfgXml = New Xml.XmlDocument

                Helpers.Logger.WriteMessage("loading", 5)
                mSkCfgXml.Load(mSkCfgFile)
                Helpers.Logger.WriteMessage("ok", 6)

                nt = mSkCfgXml.NameTable
                ns = New Xml.XmlNamespaceManager(nt)
                ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
                ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

                Helpers.Logger.WriteMessage("root node", 5)
                mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

                If Not mXmlNode_Root Is Nothing Then
                    Helpers.Logger.WriteMessage("ok", 6)
                Else
                    Throw New Exception("XmlNodeRoot Is Nothing")
                End If


                'Create base node for program

                '      <file autostart="false" filename="C:\Windows\notepad.exe" description="description" title="title" screenshot-path="">
                '           <runas enabled="false">
                '               <username></username>
                '               <password></password>
                '               <domainname></domainname>
                '           </runas>
                '           <startup enabled="false">
                '               <password></password>
                '           </startup>
                '       </file>

                Dim mXmlNode_Template_Program__File As Xml.XmlNode
                Dim mXmlNode_Template_Program__Runas As Xml.XmlNode
                Dim mXmlNode_Template_Program__Startup As Xml.XmlNode
                Dim mXmlNode_Template_Program__tmp As Xml.XmlNode

                mXmlNode_Template_Program__File = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "file", ns.LookupNamespace("sk"))
                mXmlNode_Template_Program__Runas = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "runas", ns.LookupNamespace("sk"))
                mXmlNode_Template_Program__Startup = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "startup", ns.LookupNamespace("sk"))


                mXmlNode_Template_Program__tmp = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
                mXmlNode_Template_Program__tmp.InnerText = ""
                mXmlNode_Template_Program__Startup.AppendChild(mXmlNode_Template_Program__tmp)

                mXmlNode_Template_Program__Startup.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("enabled"))
                mXmlNode_Template_Program__Startup.Attributes.GetNamedItem("enabled").Value = "false"

                mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Startup)


                mXmlNode_Template_Program__tmp = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "username", ns.LookupNamespace("sk"))
                mXmlNode_Template_Program__tmp.InnerText = ""
                mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

                mXmlNode_Template_Program__tmp = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
                mXmlNode_Template_Program__tmp.InnerText = ""
                mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

                mXmlNode_Template_Program__tmp = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "domainname", ns.LookupNamespace("sk"))
                mXmlNode_Template_Program__tmp.InnerText = ""
                mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

                mXmlNode_Template_Program__Runas.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("enabled"))
                mXmlNode_Template_Program__Runas.Attributes.GetNamedItem("enabled").Value = "false"

                mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Runas)


                mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("autostart"))
                mXmlNode_Template_Program__File.Attributes.GetNamedItem("autostart").Value = "false"

                mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("filename"))
                mXmlNode_Template_Program__File.Attributes.GetNamedItem("filename").Value = ""

                mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("description"))
                mXmlNode_Template_Program__File.Attributes.GetNamedItem("description").Value = ""

                mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("title"))
                mXmlNode_Template_Program__File.Attributes.GetNamedItem("title").Value = ""

                mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("screenshot-path"))
                mXmlNode_Template_Program__File.Attributes.GetNamedItem("screenshot-path").Value = ""


                Helpers.Logger.WriteMessage("find programs node", 5)

                Dim mXmlNode_Programs As Xml.XmlNode
                Dim mXmlNode_Files As Xml.XmlNodeList
                Dim mXmlNode_File As Xml.XmlNode


                mXmlNode_Programs = mXmlNode_Root.SelectSingleNode("sk:programs", ns)
                mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)
                For Each mXmlNode_File In mXmlNode_Files
                    sTmp = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower

                    Helpers.Logger.WriteMessage("found program", 6)
                    Helpers.Logger.WriteMessage(sTmp, 7)

                    If sTmp.Contains(Constants.Installer.SK_APP_SEARCHSTRING.ToLower) Then
                        'found app already there, remove it

                        mXmlNode_Programs.RemoveChild(mXmlNode_File)
                    End If
                Next

                Helpers.Logger.WriteMessage("adding program", 5)

                Dim mXmlNode_FileClone As Xml.XmlNode
                mXmlNode_FileClone = mXmlNode_Template_Program__File.CloneNode(True)
                mXmlNode_FileClone.Attributes.GetNamedItem("filename").Value = FOLDER__ServicePath & "\" & Constants.Installer.SK_APP_SEARCHSTRING & ".exe"
                mXmlNode_FileClone.Attributes.GetNamedItem("title").Value = Constants.Installer.SK_APP_TITLE
                mXmlNode_FileClone.Attributes.GetNamedItem("description").Value = Constants.Installer.SK_APP_DESCRIPTION

                Helpers.Logger.WriteMessage("filename", 6)
                Helpers.Logger.WriteMessage(FOLDER__ServicePath & "\" & Constants.Installer.SK_APP_SEARCHSTRING & ".exe", 7)
                Helpers.Logger.WriteMessage("title", 6)
                Helpers.Logger.WriteMessage(Constants.Installer.SK_APP_TITLE, 7)
                Helpers.Logger.WriteMessage("description", 6)
                Helpers.Logger.WriteMessage(Constants.Installer.SK_APP_DESCRIPTION, 7)


                mXmlNode_Programs.AppendChild(mXmlNode_FileClone)


                Helpers.Logger.WriteRelative("saving", , 1)
                Helpers.Logger.WriteRelative(mSkCfgFile, , 2)
                mSkCfgXml.Save(mSkCfgFile)
                Helpers.Logger.WriteRelative("ok", , 3)
            End If

        Catch ex As Exception
            Helpers.Logger.WriteRelative("FAIL", , 2)
            Helpers.Logger.WriteRelative(ex.Message, , 3)
        End Try





        '----------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("done")
    End Sub
End Module
