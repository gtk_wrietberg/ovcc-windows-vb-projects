Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("PrintQueueCleanerServiceInstaller")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("GuestTek")>
<Assembly: AssemblyProduct("ServiceInstaller")> 
<Assembly: AssemblyCopyright("Copyright ©  2017")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f356e67e-7c3e-4e07-a5c7-f263ec172790")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.24186")> 

<Assembly: AssemblyVersion("1.0.24186")>

<Assembly: AssemblyFileVersion("1.0.24186.1051")>
<Assembly: AssemblyInformationalVersion("1.0.24186.1051")>