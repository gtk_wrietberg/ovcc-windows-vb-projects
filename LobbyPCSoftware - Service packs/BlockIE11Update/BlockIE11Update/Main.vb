Module Main
    Private Const cPolicy_Key__DoNotAllowIE11 As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\Setup\11.0"
    Private Const cPolicy_Name__DoNotAllowIE11 As String = "DoNotAllowIE11"
    Private Const cPolicy_Value__DoNotAllowIE11 As Integer = 1

    Public Sub Main()
        Try
            Microsoft.Win32.Registry.SetValue(cPolicy_Key__DoNotAllowIE11, _
                                cPolicy_Name__DoNotAllowIE11, _
                                cPolicy_Value__DoNotAllowIE11, _
                                Microsoft.Win32.RegistryValueKind.DWord)
        Catch ex As Exception

        End Try
    End Sub
End Module
