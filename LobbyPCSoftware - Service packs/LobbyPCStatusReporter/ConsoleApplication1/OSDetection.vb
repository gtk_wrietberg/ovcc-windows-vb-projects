﻿Imports System.Management

Public Class OSDetection
    Public Class OSInfo
        'BootDevice = \Device\HarddiskVolume1
        'BuildNumber = 9600
        'BuildType = Multiprocessor Free
        'Caption = Microsoft Windows 8.1 Pro
        'CodeSet = 1252
        'CountryCode = 31
        'CreationClassName = Win32_OperatingSystem
        'CSCreationClassName = Win32_ComputerSystem
        'CSName = WRDEVWIN8
        'CurrentTimeZone = 60
        'DataExecutionPrevention_32BitApplications = True
        'DataExecutionPrevention_Available = True
        'DataExecutionPrevention_Drivers = True
        'DataExecutionPrevention_SupportPolicy = 2
        'Debug = False
        'Description =
        'Distributed = False
        'EncryptionLevel = 256
        'ForegroundApplicationBoost = 2
        'FreePhysicalMemory = 1313460
        'FreeSpaceInPagingFiles = 462968
        'FreeVirtualMemory = 1758952
        'InstallDate = 20140409140939.000000+120
        'LastBootUpTime = 20151210035417.488695+060
        'LocalDateTime = 20151216100656.353000+060
        'Locale = 0413
        'Manufacturer = Microsoft Corporation
        'MaxNumberOfProcesses = 4294967295
        'MaxProcessMemorySize = 137438953344
        'MUILanguages = System.String[]
        'Name = Microsoft Windows 8.1 Pro|C:\Windows|\Device\Harddisk0\Partition2
        'NumberOfProcesses = 67
        'NumberOfUsers = 9
        'OperatingSystemSKU = 48
        'Organization =
        'OSArchitecture = 64-bit
        'OSLanguage = 1033
        'OSProductSuite = 256
        'OSType = 18
        'PortableOperatingSystem = False
        'Primary = True
        'ProductType = 1
        'RegisteredUser = wrietberg@ibahn.com
        'SerialNumber = 00261-80245-53762-AA090
        'ServicePackMajorVersion = 0
        'ServicePackMinorVersion = 0
        'SizeStoredInPagingFiles = 720896
        'Status = OK
        'SuiteMask = 272
        'SystemDevice = \Device\HarddiskVolume2
        'SystemDirectory = C:\Windows\system32
        'SystemDrive = C:
        'TotalVirtualMemorySize = 4716788
        'TotalVisibleMemorySize = 3995892
        'Version = 6.3.9600
        'WindowsDirectory = C:\Windows


        Public Property BuildNumber As String = "?"
        Public Property Caption As String = "?"
        Public Property CodeSet As String = "?"
        Public Property CountryCode As String = "?"
        Public Property CSName As String = "?"
        Public Property InstallDate As String = "?"
        Public Property LastBootUpTime As String = "?"
        Public Property LocalDateTime As String = "?"
        Public Property CurrentTimeZone As String = "?"
        Public Property OSArchitecture As String = "?"
        Public Property OSLanguage As String = "?"
        Public Property RegisteredUser As String = "?"
        Public Property SerialNumber As String = "?"
        Public Property ServicePackMajorVersion As String = "?"
        Public Property ServicePackMinorVersion As String = "?"
        Public Property Status As String = "?"
        Public Property Version As String = "?"

        'human readable properties
        Public Property HR_InstallDate As DateTime = DateTime.MinValue
        Public Property HR_LastBootUpTime As DateTime = DateTime.MinValue
        Public Property HR_LocalDateTime As DateTime = DateTime.MinValue
    End Class

    Private Shared mInfo As New OSInfo
    Public Shared ReadOnly Property Info As OSInfo
        Get
            Return mInfo
        End Get
    End Property

    ''' <summary>
    ''' Loads info about OS into OSInfo property
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Sub LoadOSInfo()
        mInfo = New OSInfo

        Try
            Dim query As String = "SELECT * FROM Win32_OperatingSystem"
            Dim searcher As New ManagementObjectSearcher(query)

            For Each info As ManagementObject In searcher.[Get]()
                For Each p As Reflection.PropertyInfo In mInfo.GetType().GetProperties()
                    Try
                        If p.Name.StartsWith("HR_") Then
                            'skip, this property is done separately
                        Else
                            If p.CanWrite Then
                                p.SetValue(mInfo, info.Properties(p.Name).Value.ToString().Trim(), Nothing)
                            End If
                        End If
                    Catch ex As Exception

                    End Try
                Next
            Next
        Catch ex As Exception

        End Try

        Try
            mInfo.HR_InstallDate = ManagementDateTimeConverter.ToDateTime(mInfo.InstallDate)
        Catch ex As Exception
            mInfo.HR_InstallDate = DateTime.MinValue
        End Try

        Try
            mInfo.HR_LastBootUpTime = ManagementDateTimeConverter.ToDateTime(mInfo.LastBootUpTime)
        Catch ex As Exception
            mInfo.HR_LastBootUpTime = DateTime.MinValue
        End Try

        Try
            mInfo.HR_LocalDateTime = ManagementDateTimeConverter.ToDateTime(mInfo.LocalDateTime)
        Catch ex As Exception
            mInfo.HR_LocalDateTime = DateTime.MinValue
        End Try
    End Sub

    ''' <summary>
    ''' Returns all properties of OSInfo property as string with vbCrLf linebreaks
    ''' </summary>
    ''' <returns>String</returns>
    ''' <remarks></remarks>
    Public Shared Function Dump() As String
        Dim sRet As String = ""

        For Each p As Reflection.PropertyInfo In mInfo.GetType().GetProperties()
            Try
                If p.CanRead Then
                    sRet = sRet & p.Name & " = " & p.GetValue(mInfo, Nothing).ToString() & vbCrLf
                Else
                    sRet = sRet & p.Name & " = (can not read)" & vbCrLf
                End If
            Catch ex As Exception
                sRet = sRet & ex.Message & vbCrLf
            End Try
        Next

        Return sRet
    End Function
End Class
