﻿Imports System.Text.RegularExpressions

Module modMain
    Private doneEvent As Threading.AutoResetEvent = New Threading.AutoResetEvent(False)
    Private WithEvents BGW As New System.ComponentModel.BackgroundWorker

    Private mRetries As Integer = 0
    Private mRetriesMax As Integer = 5
    Private mSuccess As Boolean = False

    Public Sub Main()
        Globals.InitGlobals()

        Globals.Logger.LogFileDirectory = Globals.LogFileDirectory
        Globals.Logger.LogFileName = Globals.LogFileName
        Globals.Logger.Debugging = True

        Globals.Logger.WriteMessage(New String("*", 50))
        Globals.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Globals.Logger.WriteMessage("started")


        '------------------------------
        'check for dev machine
        If Helpers.Generic.IsDevMachine Then
            Globals.TESTMODE = True
            Globals.Logger.WriteMessage("TEST MODE enabled")
            Globals.Logger.WriteMessage("dev or test machine detected", 1)
        End If


        '------------------------------
        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, Globals.c_PARAM_TESTMODE) > 0 Then
                Globals.TESTMODE = True
            End If

            If InStr(arg, Globals.c_PARAM_FORCE) > 0 Then
                Globals.IGNORE_DUPLICATE_RUN_CHECK = True
            End If
        Next


        If Globals.TESTMODE Then
            Globals.Logger.WriteMessage("running in TEST MODE")
            Globals.Logger.WriteMessage("which basically just means that there is no startup delay", 1)
        End If


        '----------------------------------------------------
        'check if report already ran on machine, to prevent db overload
        'first check if we can write to the registry
        Dim bCanWriteToRegistry As Boolean = False
        Dim sTmp As String = "", sTmp_Hash As String, sTmp_Check As String = ""

        sTmp = Helpers.Registry.GetValue_String(Globals.c_DUPLICATERUNPREVENTIONREGKEY, "WriteCheck")
        sTmp_Hash = Helpers.MD5Class.GetMd5Hash(sTmp)
        Helpers.Registry.SetValue_String(Globals.c_DUPLICATERUNPREVENTIONREGKEY, "WriteCheck", sTmp_Hash)
        sTmp_Check = Helpers.Registry.GetValue_String(Globals.c_DUPLICATERUNPREVENTIONREGKEY, "WriteCheck")
        If Not sTmp.Equals(sTmp_Check) Then
            'ok

            'clear out crap from older version
            Helpers.Registry.RemoveValue(Globals.c_DUPLICATERUNPREVENTIONREGKEY, "HasAlreadyRun")
            Helpers.Registry.RemoveValue(Globals.c_DUPLICATERUNPREVENTIONREGKEY, "HasAlreadyRunDate")
        Else
            'failed
            Globals.Logger.WriteError("I can't write to '" & Globals.c_DUPLICATERUNPREVENTIONREGKEY & "'")
            Globals.Logger.WriteError("setting duplicate run prevention will not work!", 1)
        End If


        Dim iAlreadyRunTimestamp As Integer, iNow As Integer = Helpers.TimeDate.ToUnix()

        iAlreadyRunTimestamp = Helpers.Registry.GetValue_Integer(Globals.c_DUPLICATERUNPREVENTIONREGKEY, "HasAlreadyRunTimestamp")
        If (iNow - iAlreadyRunTimestamp) < Globals.c_DUPLICATERUNPREVENTIONDELAY Then
            Globals.Logger.WriteMessage("this report already ran on this machine in the last " & Globals.c_DUPLICATERUNPREVENTIONDELAY.ToString & " seconds")

            If Globals.IGNORE_DUPLICATE_RUN_CHECK Then
                Globals.Logger.WriteMessage("but we are forcing a run anyway", 1)
            Else
                ExitCode.SetValue(ExitCode.ExitCodes.DUPLICATE_RUN)
                ExitApplication()
            End If
        End If


        '----------------------------------------------------
        'do a random wait, to prevent clogging on server
        Dim rndWait As New Random(), iRnd As Integer

        iRnd = (rndWait.Next(1, 30)) * 2

        Globals.Logger.WriteMessage("random delay")
        Globals.Logger.WriteMessage(iRnd.ToString & " seconds", 1)
        If Globals.TESTMODE Then
            Globals.Logger.WriteMessage("is not happening, because of TEST MODE", 2)
        Else
            Globals.Logger.WriteMessage("zzzzzzzzz", 2)
            Threading.Thread.Sleep(iRnd * 1000)
            Globals.Logger.WriteMessage("tringgggg, wake up", 2)
        End If

        BGW.RunWorkerAsync()
        doneEvent.WaitOne()



        ExitApplication()
    End Sub

    Private Sub BGW_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BGW.DoWork
        Dim bSuccess As Boolean = False

        Do
            mRetries += 1
            Globals.Logger.WriteMessage("sending status report (" & mRetries.ToString & "/" & mRetriesMax.ToString & ")", 0)
            bSuccess = SendStatusReport()

            If Not bSuccess Then
                Globals.Logger.WriteError("FAIL", 1)
                Globals.Logger.WriteMessage("will try again in " & Globals.c_RetryDelay.ToString & " seconds", 2)
                Threading.Thread.Sleep(Globals.c_RetryDelay * 1000)
            Else
                Globals.Logger.WriteMessage("ok", 1)
                mSuccess = True

                ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)
            End If
        Loop While Not bSuccess And mRetries < mRetriesMax

        If Not mSuccess Then
            Globals.Logger.WriteError("I give up", 1)

            ExitCode.SetValue(ExitCode.ExitCodes.SEND_ERROR)
        End If
    End Sub

    Private Sub BGW_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGW.RunWorkerCompleted
        doneEvent.Set()
    End Sub

    Private Sub ExitApplication()
        If ExitCode.IsValue(ExitCode.ExitCodes.OK) Then
            '----------------------------------------------------
            'write value in registry, to prevent duplicate runs
            Helpers.Registry.SetValue_Integer(Globals.c_DUPLICATERUNPREVENTIONREGKEY, "HasAlreadyRunTimestamp", Helpers.TimeDate.ToUnix())
        End If


        '-----------------------------------------------
        'done
        Globals.Logger.WriteMessage("exiting", 0)
        Globals.Logger.WriteMessage("exit code", 1)
        Globals.Logger.WriteMessage(ExitCode.ToString(), 2)
        Globals.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

    Private Function SendStatusReport() As Boolean
        Dim bRet As Boolean = False

        Try
            Dim sTmp As String = ""
            Dim SK As SkCfg
            Dim request As Net.WebRequest = Net.WebRequest.Create(Globals.c_ServerUrl)

            SK = New SkCfg
            SK.GetSiteKioskConfigFile()
            sTmp = SK.Load()
            If Not sTmp.Equals("") Then
                ExitCode.SetValue(ExitCode.ExitCodes.SKCFG_READ_ERROR)
                Throw New Exception("Could not load SkCfg: " & sTmp)
            End If

            request.Method = "POST"

            Globals.Logger.WriteRelativeMessage("create request", 1)
            Globals.Logger.WriteRelativeMessage("server url", 2)
            Globals.Logger.WriteRelativeMessage(Globals.c_ServerUrl, 3)


            Globals.Logger.WriteRelativeMessage("getting data", 1)
            Globals.StatusRequest = New clsStatusRequest

            Globals.StatusRequest.ErrorString = "ok"

            Globals.StatusRequest.SkStatusData.Client_Version = My.Application.Info.Version.ToString


            'OS stuff
            Globals.StatusRequest.SkStatusData.OS_Version = Helpers.OSVersion.OSFull()
            Globals.StatusRequest.SkStatusData.OS_Workgroup = Helpers.Network.DomainWorkgroup.GetName()


            'name stuff
            Globals.Logger.WriteRelativeMessage("Name_Machine", 2)
            Globals.StatusRequest.SkStatusData.Name_Machine = Environment.MachineName
            Globals.Logger.WriteRelativeMessage(Globals.StatusRequest.SkStatusData.Name_Machine, 3)

            Globals.Logger.WriteRelativeMessage("Name_Script", 2)
            Globals.StatusRequest.SkStatusData.Name_Script = Helpers.SiteKiosk.GetMachineNameFromScript
            Globals.Logger.WriteRelativeMessage(Globals.StatusRequest.SkStatusData.Name_Script, 3)

            Globals.Logger.WriteRelativeMessage("Name_CreditCard", 2)
            Globals.StatusRequest.SkStatusData.Name_CreditCard = SK.GetMachineNameFromCreditCard
            Globals.Logger.WriteRelativeMessage(Globals.StatusRequest.SkStatusData.Name_CreditCard, 3)


            'SiteKiosk stuff
            Globals.Logger.WriteRelativeMessage("Sitekiosk_Version", 2)
            Globals.StatusRequest.SkStatusData.Sitekiosk_Version = Helpers.SiteKiosk.GetSiteKioskVersion
            Globals.Logger.WriteRelativeMessage(Globals.StatusRequest.SkStatusData.Sitekiosk_Version, 3)


            Globals.Logger.WriteRelativeMessage("Sitekiosk_PaymentEnabled", 2)
            sTmp = SK.GetSiteKioskPaymentStatus
            If sTmp.ToLower.Equals("true") Then
                Globals.StatusRequest.SkStatusData.Sitekiosk_PaymentEnabled = True
            ElseIf sTmp.ToLower.Equals("false") Then
                Globals.StatusRequest.SkStatusData.Sitekiosk_PaymentEnabled = False
            Else
                Globals.StatusRequest.SkStatusData.Sitekiosk_PaymentEnabled = False
                Globals.StatusRequest.ErrorString = sTmp
            End If
            Globals.Logger.WriteRelativeMessage(sTmp, 3)


            Globals.Logger.WriteRelativeMessage("SiteKiosk_Startpage", 2)
            sTmp = SK.GetSiteKioskStartPage

            Globals.Logger.WriteRelativeMessage(sTmp, 3)
            Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage = sTmp
            sTmp = Helpers.SiteKiosk.TranslateSiteKioskPathToNormalPath(sTmp)
            Globals.Logger.WriteRelativeMessage(sTmp, 3)
            If IO.File.Exists(sTmp) Then
                Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage_Creation = Helpers.FilesAndFolders.GetFileCreationDate(sTmp)
                Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage_Modified = Helpers.FilesAndFolders.GetFileLastModifiedDate(sTmp)
            Else
                Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage_Creation = "file not found"
                Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage_Modified = "file not found"
            End If

            Globals.Logger.WriteRelativeMessage("SiteKiosk_Theme", 2)
            If Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage.ToLower.Contains("guesttek") Then
                Globals.StatusRequest.SkStatusData.SiteKiosk_Theme = "GuestTek"
            ElseIf Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage.ToLower.Contains("guest-tek") Then
                Globals.StatusRequest.SkStatusData.SiteKiosk_Theme = "GuestTek"
            ElseIf Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage.ToLower.Contains("ibahn") Then
                Globals.StatusRequest.SkStatusData.SiteKiosk_Theme = "iBAHN"
            ElseIf Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage.ToLower.Contains("hilton") Then
                Globals.StatusRequest.SkStatusData.SiteKiosk_Theme = "Hilton"
            ElseIf Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage.ToLower.Contains("guiv2") Then
                Globals.StatusRequest.SkStatusData.SiteKiosk_Theme = "GUIv2"
            ElseIf Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage.ToLower.Contains("airlinekiosk") Then
                Globals.StatusRequest.SkStatusData.SiteKiosk_Theme = "AirlinePC"
            Else
                Globals.StatusRequest.SkStatusData.SiteKiosk_Theme = "unknown"
            End If
            Globals.Logger.WriteRelativeMessage(Globals.StatusRequest.SkStatusData.SiteKiosk_Theme, 3)


            Globals.Logger.WriteRelativeMessage("SiteKiosk_Startpage_Creation", 2)
            Globals.Logger.WriteRelativeMessage(Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage_Creation, 3)
            Globals.Logger.WriteRelativeMessage("SiteKiosk_Startpage_Modified", 2)
            Globals.Logger.WriteRelativeMessage(Globals.StatusRequest.SkStatusData.SiteKiosk_Startpage_Modified, 3)


            Globals.StatusRequest.Create()
            Globals.Logger.WriteRelativeMessage("request:", 2)
            Globals.Logger.WriteEmptyLineToLog()
            Globals.Logger.WriteToLogWithoutDate(Globals.StatusRequest.RequestIndented)
            Globals.Logger.WriteEmptyLineToLog()


            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(Globals.StatusRequest.Request)

            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length

            Dim dataStream As IO.Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            Dim response As Net.WebResponse = request.GetResponse()

            dataStream = response.GetResponseStream()

            Dim reader As New IO.StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()


            Globals.StatusRequest.Response = responseFromServer
            Globals.StatusRequest.ResponseIndented = Helpers.XML.Beautify(responseFromServer)

            reader.Close()
            dataStream.Close()
            response.Close()


            Globals.Logger.WriteRelativeMessage("response:", 2)
            Globals.Logger.WriteEmptyLineToLog()
            Globals.Logger.WriteToLogWithoutDate(Globals.StatusRequest.Response)
            Globals.Logger.WriteEmptyLineToLog()
            Globals.Logger.WriteEmptyLineToLog()
            Globals.Logger.WriteToLogWithoutDate(Globals.StatusRequest.ResponseIndented)
            Globals.Logger.WriteEmptyLineToLog()


            'Analyse the response ; should look something like this:
            '  <lobbypc><response>ok (n)</response></lobbypc>
            ' where n is the id of the data row in the db

            Dim xmlDoc As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode
            Dim xmlNode__response As Xml.XmlNode
            Dim sResponse As String = ""

            xmlDoc = New Xml.XmlDocument
            xmlDoc.LoadXml(Globals.StatusRequest.Response)
            xmlNode_Root = xmlDoc.SelectSingleNode("lobbypc")
            xmlNode__response = xmlNode_Root.SelectSingleNode("response")

            If Not xmlNode__response Is Nothing Then
                sResponse = xmlNode__response.InnerText

                Dim match As Match = Globals.ResponseRegex.Match(sResponse)
                If match.Success Then
                    Globals.Logger.WriteRelativeMessage("ok", 4)

                    sTmp = Globals.ResponseRegex.Replace(sResponse, "$1")

                    Dim iId As Integer = Helpers.Types.CastStringToInteger_safe(sTmp)
                    Globals.Logger.WriteRelativeMessage("db id: " & iId.ToString, 5)

                    If iId > 0 Then
                        Globals.Logger.WriteRelativeMessage("ok", 6)
                        bRet = True
                    Else
                        Globals.Logger.WriteRelativeError("hmm, data probably not stored", 6)
                        bRet = False
                    End If
                Else
                    Globals.Logger.WriteRelativeMessage("fail", 4)
                    bRet = False
                End If
            Else
                bRet = False
                Globals.Logger.WriteRelativeError("fail", 4)
            End If
        Catch ex As Exception
            Globals.Logger.WriteRelativeError("exception", 2)
            Globals.Logger.WriteRelativeError(ex.Message, 3)
        End Try

        Return bRet
    End Function
End Module
