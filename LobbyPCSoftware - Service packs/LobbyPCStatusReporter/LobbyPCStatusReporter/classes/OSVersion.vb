﻿Imports System.Collections.Generic
Imports System.Diagnostics
Imports System.Runtime.InteropServices
Imports System.Text

''' <summary>
''' Provides detailed information about the host operating system.
''' </summary>
Public NotInheritable Class OSVersionInfo
    Private Sub New()
    End Sub
#Region "ENUMS"
    Public Enum SoftwareArchitecture
        Unknown = 0
        Bit32 = 1
        Bit64 = 2
    End Enum

    Public Enum ProcessorArchitecture
        Unknown = 0
        Bit32 = 1
        Bit64 = 2
        Itanium64 = 3
    End Enum
#End Region

#Region "DELEGATE DECLARATION"
    Private Delegate Function IsWow64ProcessDelegate(<[In]> handle As IntPtr, <Out> ByRef isWow64Process As Boolean) As Boolean
#End Region

#Region "BITS"
    ''' <summary>
    ''' Determines if the current application is 32 or 64-bit.
    ''' </summary>
    Public Shared ReadOnly Property ProgramBits() As SoftwareArchitecture
        Get
            Dim pbits As SoftwareArchitecture = SoftwareArchitecture.Unknown

            Dim test As System.Collections.IDictionary = Environment.GetEnvironmentVariables()

            Select Case IntPtr.Size * 8
                Case 64
                    pbits = SoftwareArchitecture.Bit64
                    Exit Select

                Case 32
                    pbits = SoftwareArchitecture.Bit32
                    Exit Select
                Case Else

                    pbits = SoftwareArchitecture.Unknown
                    Exit Select
            End Select

            ' int getOSArchitecture()
            '{
            '    string pa = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
            '    return ((String.IsNullOrEmpty(pa) || String.Compare(pa, 0, "x86", 0, 3, true) == 0) ? 32 : 64);
            '}



            'ProcessorArchitecture pbits = ProcessorArchitecture.Unknown;

            'try
            '{
            '    SYSTEM_INFO l_System_Info = new SYSTEM_INFO();
            '    GetSystemInfo(ref l_System_Info);

            '    switch (l_System_Info.uProcessorInfo.wProcessorArchitecture)
            '    {
            '        case 9: // PROCESSOR_ARCHITECTURE_AMD64
            '            pbits = ProcessorArchitecture.Bit64;
            '            break;
            '        case 6: // PROCESSOR_ARCHITECTURE_IA64
            '            pbits = ProcessorArchitecture.Itanium64;
            '            break;
            '        case 0: // PROCESSOR_ARCHITECTURE_INTEL
            '            pbits = ProcessorArchitecture.Bit32;
            '            break;
            '        default: // PROCESSOR_ARCHITECTURE_UNKNOWN
            '            pbits = ProcessorArchitecture.Unknown;
            '            break;
            '    }
            '}
            'catch
            '{
            '     Ignore        
            '}

            'return pbits;
            Return pbits
        End Get
    End Property

    Public Shared ReadOnly Property OSBits() As SoftwareArchitecture
        Get
            Dim osbits__1 As SoftwareArchitecture = SoftwareArchitecture.Unknown

            Select Case IntPtr.Size * 8
                Case 64
                    osbits__1 = SoftwareArchitecture.Bit64
                    Exit Select

                Case 32
                    If Is32BitProcessOn64BitProcessor() Then
                        osbits__1 = SoftwareArchitecture.Bit64
                    Else
                        osbits__1 = SoftwareArchitecture.Bit32
                    End If
                    Exit Select
                Case Else

                    osbits__1 = SoftwareArchitecture.Unknown
                    Exit Select
            End Select

            Return osbits__1
        End Get
    End Property

    ''' <summary>
    ''' Determines if the current processor is 32 or 64-bit.
    ''' </summary>
    Public Shared ReadOnly Property ProcessorBits() As ProcessorArchitecture
        Get
            Dim pbits As ProcessorArchitecture = ProcessorArchitecture.Unknown

            Try
                Dim l_System_Info As New SYSTEM_INFO()
                GetNativeSystemInfo(l_System_Info)

                Select Case l_System_Info.uProcessorInfo.wProcessorArchitecture
                    Case 9
                        ' PROCESSOR_ARCHITECTURE_AMD64
                        pbits = ProcessorArchitecture.Bit64
                        Exit Select
                    Case 6
                        ' PROCESSOR_ARCHITECTURE_IA64
                        pbits = ProcessorArchitecture.Itanium64
                        Exit Select
                    Case 0
                        ' PROCESSOR_ARCHITECTURE_INTEL
                        pbits = ProcessorArchitecture.Bit32
                        Exit Select
                    Case Else
                        ' PROCESSOR_ARCHITECTURE_UNKNOWN
                        pbits = ProcessorArchitecture.Unknown
                        Exit Select
                End Select
                ' Ignore        
            Catch
            End Try

            Return pbits
        End Get
    End Property
#End Region

#Region "EDITION"
    Private Shared s_Edition As String
    ''' <summary>
    ''' Gets the edition of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property Edition() As String
        Get
            If s_Edition IsNot Nothing Then
                Return s_Edition
            End If
            '***** RETURN *****//
            Dim edition__1 As String = [String].Empty

            Dim osVersion As OperatingSystem = Environment.OSVersion
            Dim osVersionInfo As New OSVERSIONINFOEX()
            osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(GetType(OSVERSIONINFOEX))

            If GetVersionEx(osVersionInfo) Then
                Dim majorVersion As Integer = osVersion.Version.Major
                Dim minorVersion As Integer = osVersion.Version.Minor
                Dim productType As Byte = osVersionInfo.wProductType
                Dim suiteMask As Short = osVersionInfo.wSuiteMask

                '#Region "VERSION 4"
                If majorVersion = 4 Then
                    If productType = VER_NT_WORKSTATION Then
                        ' Windows NT 4.0 Workstation
                        edition__1 = "Workstation"
                    ElseIf productType = VER_NT_SERVER Then
                        If (suiteMask And VER_SUITE_ENTERPRISE) <> 0 Then
                            ' Windows NT 4.0 Server Enterprise
                            edition__1 = "Enterprise Server"
                        Else
                            ' Windows NT 4.0 Server
                            edition__1 = "Standard Server"
                        End If
                    End If
                    '#End Region

                    '#Region "VERSION 5"
                ElseIf majorVersion = 5 Then
                    If productType = VER_NT_WORKSTATION Then
                        If (suiteMask And VER_SUITE_PERSONAL) <> 0 Then
                            edition__1 = "Home"
                        Else
                            If GetSystemMetrics(86) = 0 Then
                                ' 86 == SM_TABLETPC
                                edition__1 = "Professional"
                            Else
                                edition__1 = "Tablet Edition"
                            End If
                        End If
                    ElseIf productType = VER_NT_SERVER Then
                        If minorVersion = 0 Then
                            If (suiteMask And VER_SUITE_DATACENTER) <> 0 Then
                                ' Windows 2000 Datacenter Server
                                edition__1 = "Datacenter Server"
                            ElseIf (suiteMask And VER_SUITE_ENTERPRISE) <> 0 Then
                                ' Windows 2000 Advanced Server
                                edition__1 = "Advanced Server"
                            Else
                                ' Windows 2000 Server
                                edition__1 = "Server"
                            End If
                        Else
                            If (suiteMask And VER_SUITE_DATACENTER) <> 0 Then
                                ' Windows Server 2003 Datacenter Edition
                                edition__1 = "Datacenter"
                            ElseIf (suiteMask And VER_SUITE_ENTERPRISE) <> 0 Then
                                ' Windows Server 2003 Enterprise Edition
                                edition__1 = "Enterprise"
                            ElseIf (suiteMask And VER_SUITE_BLADE) <> 0 Then
                                ' Windows Server 2003 Web Edition
                                edition__1 = "Web Edition"
                            Else
                                ' Windows Server 2003 Standard Edition
                                edition__1 = "Standard"
                            End If
                        End If
                    End If
                    '#End Region

                    '#Region "VERSION 6"
                ElseIf majorVersion = 6 Then
                    Dim ed As Integer
                    If GetProductInfo(majorVersion, minorVersion, osVersionInfo.wServicePackMajor, osVersionInfo.wServicePackMinor, ed) Then
                        Select Case ed
                            Case PRODUCT_BUSINESS
                                edition__1 = "Business"
                                Exit Select
                            Case PRODUCT_BUSINESS_N
                                edition__1 = "Business N"
                                Exit Select
                            Case PRODUCT_CLUSTER_SERVER
                                edition__1 = "HPC Edition"
                                Exit Select
                            Case PRODUCT_CLUSTER_SERVER_V
                                edition__1 = "HPC Edition without Hyper-V"
                                Exit Select
                            Case PRODUCT_DATACENTER_SERVER
                                edition__1 = "Datacenter Server"
                                Exit Select
                            Case PRODUCT_DATACENTER_SERVER_CORE
                                edition__1 = "Datacenter Server (core installation)"
                                Exit Select
                            Case PRODUCT_DATACENTER_SERVER_V
                                edition__1 = "Datacenter Server without Hyper-V"
                                Exit Select
                            Case PRODUCT_DATACENTER_SERVER_CORE_V
                                edition__1 = "Datacenter Server without Hyper-V (core installation)"
                                Exit Select
                            Case PRODUCT_EMBEDDED
                                edition__1 = "Embedded"
                                Exit Select
                            Case PRODUCT_ENTERPRISE
                                edition__1 = "Enterprise"
                                Exit Select
                            Case PRODUCT_ENTERPRISE_N
                                edition__1 = "Enterprise N"
                                Exit Select
                            Case PRODUCT_ENTERPRISE_E
                                edition__1 = "Enterprise E"
                                Exit Select
                            Case PRODUCT_ENTERPRISE_SERVER
                                edition__1 = "Enterprise Server"
                                Exit Select
                            Case PRODUCT_ENTERPRISE_SERVER_CORE
                                edition__1 = "Enterprise Server (core installation)"
                                Exit Select
                            Case PRODUCT_ENTERPRISE_SERVER_CORE_V
                                edition__1 = "Enterprise Server without Hyper-V (core installation)"
                                Exit Select
                            Case PRODUCT_ENTERPRISE_SERVER_IA64
                                edition__1 = "Enterprise Server for Itanium-based Systems"
                                Exit Select
                            Case PRODUCT_ENTERPRISE_SERVER_V
                                edition__1 = "Enterprise Server without Hyper-V"
                                Exit Select
                            Case PRODUCT_ESSENTIALBUSINESS_SERVER_MGMT
                                edition__1 = "Essential Business Server MGMT"
                                Exit Select
                            Case PRODUCT_ESSENTIALBUSINESS_SERVER_ADDL
                                edition__1 = "Essential Business Server ADDL"
                                Exit Select
                            Case PRODUCT_ESSENTIALBUSINESS_SERVER_MGMTSVC
                                edition__1 = "Essential Business Server MGMTSVC"
                                Exit Select
                            Case PRODUCT_ESSENTIALBUSINESS_SERVER_ADDLSVC
                                edition__1 = "Essential Business Server ADDLSVC"
                                Exit Select
                            Case PRODUCT_HOME_BASIC
                                edition__1 = "Home Basic"
                                Exit Select
                            Case PRODUCT_HOME_BASIC_N
                                edition__1 = "Home Basic N"
                                Exit Select
                            Case PRODUCT_HOME_BASIC_E
                                edition__1 = "Home Basic E"
                                Exit Select
                            Case PRODUCT_HOME_PREMIUM
                                edition__1 = "Home Premium"
                                Exit Select
                            Case PRODUCT_HOME_PREMIUM_N
                                edition__1 = "Home Premium N"
                                Exit Select
                            Case PRODUCT_HOME_PREMIUM_E
                                edition__1 = "Home Premium E"
                                Exit Select
                            Case PRODUCT_HOME_PREMIUM_SERVER
                                edition__1 = "Home Premium Server"
                                Exit Select
                            Case PRODUCT_HYPERV
                                edition__1 = "Microsoft Hyper-V Server"
                                Exit Select
                            Case PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT
                                edition__1 = "Windows Essential Business Management Server"
                                Exit Select
                            Case PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING
                                edition__1 = "Windows Essential Business Messaging Server"
                                Exit Select
                            Case PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY
                                edition__1 = "Windows Essential Business Security Server"
                                Exit Select
                            Case PRODUCT_PROFESSIONAL
                                edition__1 = "Professional"
                                Exit Select
                            Case PRODUCT_PROFESSIONAL_N
                                edition__1 = "Professional N"
                                Exit Select
                            Case PRODUCT_PROFESSIONAL_E
                                edition__1 = "Professional E"
                                Exit Select
                            Case PRODUCT_SB_SOLUTION_SERVER
                                edition__1 = "SB Solution Server"
                                Exit Select
                            Case PRODUCT_SB_SOLUTION_SERVER_EM
                                edition__1 = "SB Solution Server EM"
                                Exit Select
                            Case PRODUCT_SERVER_FOR_SB_SOLUTIONS
                                edition__1 = "Server for SB Solutions"
                                Exit Select
                            Case PRODUCT_SERVER_FOR_SB_SOLUTIONS_EM
                                edition__1 = "Server for SB Solutions EM"
                                Exit Select
                            Case PRODUCT_SERVER_FOR_SMALLBUSINESS
                                edition__1 = "Windows Essential Server Solutions"
                                Exit Select
                            Case PRODUCT_SERVER_FOR_SMALLBUSINESS_V
                                edition__1 = "Windows Essential Server Solutions without Hyper-V"
                                Exit Select
                            Case PRODUCT_SERVER_FOUNDATION
                                edition__1 = "Server Foundation"
                                Exit Select
                            Case PRODUCT_SMALLBUSINESS_SERVER
                                edition__1 = "Windows Small Business Server"
                                Exit Select
                            Case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM
                                edition__1 = "Windows Small Business Server Premium"
                                Exit Select
                            Case PRODUCT_SMALLBUSINESS_SERVER_PREMIUM_CORE
                                edition__1 = "Windows Small Business Server Premium (core installation)"
                                Exit Select
                            Case PRODUCT_SOLUTION_EMBEDDEDSERVER
                                edition__1 = "Solution Embedded Server"
                                Exit Select
                            Case PRODUCT_SOLUTION_EMBEDDEDSERVER_CORE
                                edition__1 = "Solution Embedded Server (core installation)"
                                Exit Select
                            Case PRODUCT_STANDARD_SERVER
                                edition__1 = "Standard Server"
                                Exit Select
                            Case PRODUCT_STANDARD_SERVER_CORE
                                edition__1 = "Standard Server (core installation)"
                                Exit Select
                            Case PRODUCT_STANDARD_SERVER_SOLUTIONS
                                edition__1 = "Standard Server Solutions"
                                Exit Select
                            Case PRODUCT_STANDARD_SERVER_SOLUTIONS_CORE
                                edition__1 = "Standard Server Solutions (core installation)"
                                Exit Select
                            Case PRODUCT_STANDARD_SERVER_CORE_V
                                edition__1 = "Standard Server without Hyper-V (core installation)"
                                Exit Select
                            Case PRODUCT_STANDARD_SERVER_V
                                edition__1 = "Standard Server without Hyper-V"
                                Exit Select
                            Case PRODUCT_STARTER
                                edition__1 = "Starter"
                                Exit Select
                            Case PRODUCT_STARTER_N
                                edition__1 = "Starter N"
                                Exit Select
                            Case PRODUCT_STARTER_E
                                edition__1 = "Starter E"
                                Exit Select
                            Case PRODUCT_STORAGE_ENTERPRISE_SERVER
                                edition__1 = "Enterprise Storage Server"
                                Exit Select
                            Case PRODUCT_STORAGE_ENTERPRISE_SERVER_CORE
                                edition__1 = "Enterprise Storage Server (core installation)"
                                Exit Select
                            Case PRODUCT_STORAGE_EXPRESS_SERVER
                                edition__1 = "Express Storage Server"
                                Exit Select
                            Case PRODUCT_STORAGE_EXPRESS_SERVER_CORE
                                edition__1 = "Express Storage Server (core installation)"
                                Exit Select
                            Case PRODUCT_STORAGE_STANDARD_SERVER
                                edition__1 = "Standard Storage Server"
                                Exit Select
                            Case PRODUCT_STORAGE_STANDARD_SERVER_CORE
                                edition__1 = "Standard Storage Server (core installation)"
                                Exit Select
                            Case PRODUCT_STORAGE_WORKGROUP_SERVER
                                edition__1 = "Workgroup Storage Server"
                                Exit Select
                            Case PRODUCT_STORAGE_WORKGROUP_SERVER_CORE
                                edition__1 = "Workgroup Storage Server (core installation)"
                                Exit Select
                            Case PRODUCT_UNDEFINED
                                edition__1 = "Unknown product"
                                Exit Select
                            Case PRODUCT_ULTIMATE
                                edition__1 = "Ultimate"
                                Exit Select
                            Case PRODUCT_ULTIMATE_N
                                edition__1 = "Ultimate N"
                                Exit Select
                            Case PRODUCT_ULTIMATE_E
                                edition__1 = "Ultimate E"
                                Exit Select
                            Case PRODUCT_WEB_SERVER
                                edition__1 = "Web Server"
                                Exit Select
                            Case PRODUCT_WEB_SERVER_CORE
                                edition__1 = "Web Server (core installation)"
                                Exit Select
                        End Select
                    End If
                    '#End Region
                End If
            End If

            s_Edition = edition__1
            Return edition__1
        End Get
    End Property
#End Region

#Region "NAME"
    Private Shared s_Name As String
    ''' <summary>
    ''' Gets the name of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property Name() As String
        Get
            If s_Name IsNot Nothing Then
                Return s_Name
            End If
            '***** RETURN *****//
            Dim name__1 As String = "unknown"

            Dim osVersion As OperatingSystem = Environment.OSVersion
            Dim osVersionInfo As New OSVERSIONINFOEX()
            osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(GetType(OSVERSIONINFOEX))

            If GetVersionEx(osVersionInfo) Then
                Dim majorVersion As Integer = osVersion.Version.Major
                Dim minorVersion As Integer = osVersion.Version.Minor

                Select Case osVersion.Platform
                    Case PlatformID.Win32S
                        name__1 = "Windows 3.1"
                        Exit Select
                    Case PlatformID.WinCE
                        name__1 = "Windows CE"
                        Exit Select
                    Case PlatformID.Win32Windows
                        If True Then
                            If majorVersion = 4 Then
                                Dim csdVersion As String = osVersionInfo.szCSDVersion
                                Select Case minorVersion
                                    Case 0
                                        If csdVersion = "B" OrElse csdVersion = "C" Then
                                            name__1 = "Windows 95 OSR2"
                                        Else
                                            name__1 = "Windows 95"
                                        End If
                                        Exit Select
                                    Case 10
                                        If csdVersion = "A" Then
                                            name__1 = "Windows 98 Second Edition"
                                        Else
                                            name__1 = "Windows 98"
                                        End If
                                        Exit Select
                                    Case 90
                                        name__1 = "Windows Me"
                                        Exit Select
                                End Select
                            End If
                            Exit Select
                        End If
                    Case PlatformID.Win32NT
                        If True Then
                            Dim productType As Byte = osVersionInfo.wProductType

                            Select Case majorVersion
                                Case 3
                                    name__1 = "Windows NT 3.51"
                                    Exit Select
                                Case 4
                                    Select Case productType
                                        Case 1
                                            name__1 = "Windows NT 4.0"
                                            Exit Select
                                        Case 3
                                            name__1 = "Windows NT 4.0 Server"
                                            Exit Select
                                    End Select
                                    Exit Select
                                Case 5
                                    Select Case minorVersion
                                        Case 0
                                            name__1 = "Windows 2000"
                                            Exit Select
                                        Case 1
                                            name__1 = "Windows XP"
                                            Exit Select
                                        Case 2
                                            name__1 = "Windows Server 2003"
                                            Exit Select
                                    End Select
                                    Exit Select
                                Case 6
                                    Select Case minorVersion
                                        Case 0
                                            Select Case productType
                                                Case 1
                                                    name__1 = "Windows Vista"
                                                    Exit Select
                                                Case 3
                                                    name__1 = "Windows Server 2008"
                                                    Exit Select
                                            End Select
                                            Exit Select

                                        Case 1
                                            Select Case productType
                                                Case 1
                                                    name__1 = "Windows 7"
                                                    Exit Select
                                                Case 3
                                                    name__1 = "Windows Server 2008 R2"
                                                    Exit Select
                                            End Select
                                            Exit Select
                                        Case 2
                                            Select Case productType
                                                Case 1
                                                    name__1 = "Windows 8"
                                                    Exit Select
                                                Case 3
                                                    name__1 = "Windows Server 2012"
                                                    Exit Select
                                            End Select
                                            Exit Select
                                    End Select
                                    Exit Select
                            End Select
                            Exit Select
                        End If
                End Select
            End If

            s_Name = name__1
            Return name__1
        End Get
    End Property
#End Region

#Region "PINVOKE"

#Region "GET"
#Region "PRODUCT INFO"
    <DllImport("Kernel32.dll")> _
    Friend Shared Function GetProductInfo(osMajorVersion As Integer, osMinorVersion As Integer, spMajorVersion As Integer, spMinorVersion As Integer, ByRef edition As Integer) As Boolean
    End Function
#End Region

#Region "VERSION"
    <DllImport("kernel32.dll")> _
    Private Shared Function GetVersionEx(ByRef osVersionInfo As OSVERSIONINFOEX) As Boolean
    End Function
#End Region

#Region "SYSTEMMETRICS"
    <DllImport("user32")> _
    Public Shared Function GetSystemMetrics(nIndex As Integer) As Integer
    End Function
#End Region

#Region "SYSTEMINFO"
    <DllImport("kernel32.dll")> _
    Public Shared Sub GetSystemInfo(<MarshalAs(UnmanagedType.Struct)> ByRef lpSystemInfo As SYSTEM_INFO)
    End Sub

    <DllImport("kernel32.dll")> _
    Public Shared Sub GetNativeSystemInfo(<MarshalAs(UnmanagedType.Struct)> ByRef lpSystemInfo As SYSTEM_INFO)
    End Sub
#End Region

#End Region

#Region "OSVERSIONINFOEX"
    <StructLayout(LayoutKind.Sequential)> _
    Private Structure OSVERSIONINFOEX
        Public dwOSVersionInfoSize As Integer
        Public dwMajorVersion As Integer
        Public dwMinorVersion As Integer
        Public dwBuildNumber As Integer
        Public dwPlatformId As Integer
        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)> _
        Public szCSDVersion As String
        Public wServicePackMajor As Short
        Public wServicePackMinor As Short
        Public wSuiteMask As Short
        Public wProductType As Byte
        Public wReserved As Byte
    End Structure
#End Region

#Region "SYSTEM_INFO"
    <StructLayout(LayoutKind.Sequential)> _
    Public Structure SYSTEM_INFO
        Friend uProcessorInfo As _PROCESSOR_INFO_UNION
        Public dwPageSize As UInteger
        Public lpMinimumApplicationAddress As IntPtr
        Public lpMaximumApplicationAddress As IntPtr
        Public dwActiveProcessorMask As IntPtr
        Public dwNumberOfProcessors As UInteger
        Public dwProcessorType As UInteger
        Public dwAllocationGranularity As UInteger
        Public dwProcessorLevel As UShort
        Public dwProcessorRevision As UShort
    End Structure
#End Region

#Region "_PROCESSOR_INFO_UNION"
    <StructLayout(LayoutKind.Explicit)> _
    Public Structure _PROCESSOR_INFO_UNION
        <FieldOffset(0)> _
        Friend dwOemId As UInteger
        <FieldOffset(0)> _
        Friend wProcessorArchitecture As UShort
        <FieldOffset(2)> _
        Friend wReserved As UShort
    End Structure
#End Region

#Region "64 BIT OS DETECTION"
    <DllImport("kernel32", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)> _
    Public Shared Function LoadLibrary(libraryName As String) As IntPtr
    End Function

    <DllImport("kernel32", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)> _
    Public Shared Function GetProcAddress(hwnd As IntPtr, procedureName As String) As IntPtr
    End Function
#End Region

#Region "PRODUCT"
    Private Const PRODUCT_UNDEFINED As Integer = &H0
    Private Const PRODUCT_ULTIMATE As Integer = &H1
    Private Const PRODUCT_HOME_BASIC As Integer = &H2
    Private Const PRODUCT_HOME_PREMIUM As Integer = &H3
    Private Const PRODUCT_ENTERPRISE As Integer = &H4
    Private Const PRODUCT_HOME_BASIC_N As Integer = &H5
    Private Const PRODUCT_BUSINESS As Integer = &H6
    Private Const PRODUCT_STANDARD_SERVER As Integer = &H7
    Private Const PRODUCT_DATACENTER_SERVER As Integer = &H8
    Private Const PRODUCT_SMALLBUSINESS_SERVER As Integer = &H9
    Private Const PRODUCT_ENTERPRISE_SERVER As Integer = &HA
    Private Const PRODUCT_STARTER As Integer = &HB
    Private Const PRODUCT_DATACENTER_SERVER_CORE As Integer = &HC
    Private Const PRODUCT_STANDARD_SERVER_CORE As Integer = &HD
    Private Const PRODUCT_ENTERPRISE_SERVER_CORE As Integer = &HE
    Private Const PRODUCT_ENTERPRISE_SERVER_IA64 As Integer = &HF
    Private Const PRODUCT_BUSINESS_N As Integer = &H10
    Private Const PRODUCT_WEB_SERVER As Integer = &H11
    Private Const PRODUCT_CLUSTER_SERVER As Integer = &H12
    Private Const PRODUCT_HOME_SERVER As Integer = &H13
    Private Const PRODUCT_STORAGE_EXPRESS_SERVER As Integer = &H14
    Private Const PRODUCT_STORAGE_STANDARD_SERVER As Integer = &H15
    Private Const PRODUCT_STORAGE_WORKGROUP_SERVER As Integer = &H16
    Private Const PRODUCT_STORAGE_ENTERPRISE_SERVER As Integer = &H17
    Private Const PRODUCT_SERVER_FOR_SMALLBUSINESS As Integer = &H18
    Private Const PRODUCT_SMALLBUSINESS_SERVER_PREMIUM As Integer = &H19
    Private Const PRODUCT_HOME_PREMIUM_N As Integer = &H1A
    Private Const PRODUCT_ENTERPRISE_N As Integer = &H1B
    Private Const PRODUCT_ULTIMATE_N As Integer = &H1C
    Private Const PRODUCT_WEB_SERVER_CORE As Integer = &H1D
    Private Const PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT As Integer = &H1E
    Private Const PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY As Integer = &H1F
    Private Const PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING As Integer = &H20
    Private Const PRODUCT_SERVER_FOUNDATION As Integer = &H21
    Private Const PRODUCT_HOME_PREMIUM_SERVER As Integer = &H22
    Private Const PRODUCT_SERVER_FOR_SMALLBUSINESS_V As Integer = &H23
    Private Const PRODUCT_STANDARD_SERVER_V As Integer = &H24
    Private Const PRODUCT_DATACENTER_SERVER_V As Integer = &H25
    Private Const PRODUCT_ENTERPRISE_SERVER_V As Integer = &H26
    Private Const PRODUCT_DATACENTER_SERVER_CORE_V As Integer = &H27
    Private Const PRODUCT_STANDARD_SERVER_CORE_V As Integer = &H28
    Private Const PRODUCT_ENTERPRISE_SERVER_CORE_V As Integer = &H29
    Private Const PRODUCT_HYPERV As Integer = &H2A
    Private Const PRODUCT_STORAGE_EXPRESS_SERVER_CORE As Integer = &H2B
    Private Const PRODUCT_STORAGE_STANDARD_SERVER_CORE As Integer = &H2C
    Private Const PRODUCT_STORAGE_WORKGROUP_SERVER_CORE As Integer = &H2D
    Private Const PRODUCT_STORAGE_ENTERPRISE_SERVER_CORE As Integer = &H2E
    Private Const PRODUCT_STARTER_N As Integer = &H2F
    Private Const PRODUCT_PROFESSIONAL As Integer = &H30
    Private Const PRODUCT_PROFESSIONAL_N As Integer = &H31
    Private Const PRODUCT_SB_SOLUTION_SERVER As Integer = &H32
    Private Const PRODUCT_SERVER_FOR_SB_SOLUTIONS As Integer = &H33
    Private Const PRODUCT_STANDARD_SERVER_SOLUTIONS As Integer = &H34
    Private Const PRODUCT_STANDARD_SERVER_SOLUTIONS_CORE As Integer = &H35
    Private Const PRODUCT_SB_SOLUTION_SERVER_EM As Integer = &H36
    Private Const PRODUCT_SERVER_FOR_SB_SOLUTIONS_EM As Integer = &H37
    Private Const PRODUCT_SOLUTION_EMBEDDEDSERVER As Integer = &H38
    Private Const PRODUCT_SOLUTION_EMBEDDEDSERVER_CORE As Integer = &H39
    'private const int ???? = 0x0000003A;
    Private Const PRODUCT_ESSENTIALBUSINESS_SERVER_MGMT As Integer = &H3B
    Private Const PRODUCT_ESSENTIALBUSINESS_SERVER_ADDL As Integer = &H3C
    Private Const PRODUCT_ESSENTIALBUSINESS_SERVER_MGMTSVC As Integer = &H3D
    Private Const PRODUCT_ESSENTIALBUSINESS_SERVER_ADDLSVC As Integer = &H3E
    Private Const PRODUCT_SMALLBUSINESS_SERVER_PREMIUM_CORE As Integer = &H3F
    Private Const PRODUCT_CLUSTER_SERVER_V As Integer = &H40
    Private Const PRODUCT_EMBEDDED As Integer = &H41
    Private Const PRODUCT_STARTER_E As Integer = &H42
    Private Const PRODUCT_HOME_BASIC_E As Integer = &H43
    Private Const PRODUCT_HOME_PREMIUM_E As Integer = &H44
    Private Const PRODUCT_PROFESSIONAL_E As Integer = &H45
    Private Const PRODUCT_ENTERPRISE_E As Integer = &H46
    Private Const PRODUCT_ULTIMATE_E As Integer = &H47
    'private const int PRODUCT_UNLICENSED = 0xABCDABCD;
#End Region

#Region "VERSIONS"
    Private Const VER_NT_WORKSTATION As Integer = 1
    Private Const VER_NT_DOMAIN_CONTROLLER As Integer = 2
    Private Const VER_NT_SERVER As Integer = 3
    Private Const VER_SUITE_SMALLBUSINESS As Integer = 1
    Private Const VER_SUITE_ENTERPRISE As Integer = 2
    Private Const VER_SUITE_TERMINAL As Integer = 16
    Private Const VER_SUITE_DATACENTER As Integer = 128
    Private Const VER_SUITE_SINGLEUSERTS As Integer = 256
    Private Const VER_SUITE_PERSONAL As Integer = 512
    Private Const VER_SUITE_BLADE As Integer = 1024
#End Region

#End Region

#Region "SERVICE PACK"
    ''' <summary>
    ''' Gets the service pack information of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property ServicePack() As String
        Get
            Dim servicePack__1 As String = [String].Empty
            Dim osVersionInfo As New OSVERSIONINFOEX()

            osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(GetType(OSVERSIONINFOEX))

            If GetVersionEx(osVersionInfo) Then
                servicePack__1 = osVersionInfo.szCSDVersion
            End If

            Return servicePack__1
        End Get
    End Property
#End Region

#Region "VERSION"
#Region "BUILD"
    ''' <summary>
    ''' Gets the build version number of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property BuildVersion() As Integer
        Get
            Return Environment.OSVersion.Version.Build
        End Get
    End Property
#End Region

#Region "FULL"
#Region "STRING"
    ''' <summary>
    ''' Gets the full version string of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property VersionString() As String
        Get
            Return Environment.OSVersion.Version.ToString()
        End Get
    End Property
#End Region

#Region "VERSION"
    ''' <summary>
    ''' Gets the full version of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property Version() As Version
        Get
            Return Environment.OSVersion.Version
        End Get
    End Property
#End Region
#End Region

#Region "MAJOR"
    ''' <summary>
    ''' Gets the major version number of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property MajorVersion() As Integer
        Get
            Return Environment.OSVersion.Version.Major
        End Get
    End Property
#End Region

#Region "MINOR"
    ''' <summary>
    ''' Gets the minor version number of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property MinorVersion() As Integer
        Get
            Return Environment.OSVersion.Version.Minor
        End Get
    End Property
#End Region

#Region "REVISION"
    ''' <summary>
    ''' Gets the revision version number of the operating system running on this computer.
    ''' </summary>
    Public Shared ReadOnly Property RevisionVersion() As Integer
        Get
            Return Environment.OSVersion.Version.Revision
        End Get
    End Property
#End Region
#End Region

#Region "64 BIT OS DETECTION"
    Private Shared Function GetIsWow64ProcessDelegate() As IsWow64ProcessDelegate
        Dim handle As IntPtr = LoadLibrary("kernel32")

        If handle <> IntPtr.Zero Then
            Dim fnPtr As IntPtr = GetProcAddress(handle, "IsWow64Process")

            If fnPtr <> IntPtr.Zero Then
                Return DirectCast(Marshal.GetDelegateForFunctionPointer(DirectCast(fnPtr, IntPtr), GetType(IsWow64ProcessDelegate)), IsWow64ProcessDelegate)
            End If
        End If

        Return Nothing
    End Function

    Private Shared Function Is32BitProcessOn64BitProcessor() As Boolean
        Dim fnDelegate As IsWow64ProcessDelegate = GetIsWow64ProcessDelegate()

        If fnDelegate Is Nothing Then
            Return False
        End If

        Dim isWow64 As Boolean
        Dim retVal As Boolean = fnDelegate.Invoke(Process.GetCurrentProcess().Handle, isWow64)

        If retVal = False Then
            Return False
        End If

        Return isWow64
    End Function
#End Region
End Class

