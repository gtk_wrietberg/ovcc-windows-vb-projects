﻿Public Class SkStatus
    Private mMachineName As String
    Public Property Name_Machine() As String
        Get
            Return mMachineName
        End Get
        Set(ByVal value As String)
            mMachineName = value
        End Set
    End Property

    Private mScriptName As String
    Public Property Name_Script() As String
        Get
            Return mScriptName
        End Get
        Set(ByVal value As String)
            mScriptName = value
        End Set
    End Property

    Private mCCName As String
    Public Property Name_CreditCard() As String
        Get
            Return mCCName
        End Get
        Set(ByVal value As String)
            mCCName = value
        End Set
    End Property

    Private mSitekioskVersion As String
    Public Property Sitekiosk_Version() As String
        Get
            Return mSitekioskVersion
        End Get
        Set(ByVal value As String)
            mSitekioskVersion = value
        End Set
    End Property

    Private mPaymentEnabled As Boolean
    Public Property Sitekiosk_PaymentEnabled() As Boolean
        Get
            Return mPaymentEnabled
        End Get
        Set(ByVal value As Boolean)
            mPaymentEnabled = value
        End Set
    End Property

    Private mStartpage As String
    Public Property SiteKiosk_Startpage() As String
        Get
            Return mStartpage
        End Get
        Set(ByVal value As String)
            mStartpage = value
        End Set
    End Property

    Private mTheme As String
    Public Property SiteKiosk_Theme() As String
        Get
            Return mTheme
        End Get
        Set(ByVal value As String)
            mTheme = value
        End Set
    End Property

    Private mStartpage_creation As String
    Public Property SiteKiosk_Startpage_Creation() As String
        Get
            Return mStartpage_creation
        End Get
        Set(ByVal value As String)
            mStartpage_creation = value
        End Set
    End Property

    Private mStartpage_modified As String
    Public Property SiteKiosk_Startpage_Modified() As String
        Get
            Return mStartpage_modified
        End Get
        Set(ByVal value As String)
            mStartpage_modified = value
        End Set
    End Property

End Class
