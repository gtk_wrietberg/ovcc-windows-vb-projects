﻿Imports System.Text.RegularExpressions

Public Class Globals
    Public Shared Logger As clsLogger
    Public Shared StatusRequest As clsStatusRequest

    Public Shared Property LogFileDirectory As String
    Public Shared Property LogFileName As String
    Public Shared Property BackupDirectory As String
    Public Shared Property TESTMODE As Boolean = False
    Public Shared Property IGNORE_DUPLICATE_RUN_CHECK As Boolean = False

    Public Shared ReadOnly c_ServerUrl As String = "http://guesttek.wouterrietberg.nl/lpcstatusreport/receive.php"
    Public Shared ReadOnly c_RetryDelay As Integer = 10 'seconds

    Public Shared ReadOnly c_DUPLICATERUNPREVENTIONREGKEY As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\LobbyPCStatusReporter"
    Public Shared ReadOnly c_DUPLICATERUNPREVENTIONDELAY As Integer = 3600 * 12 'seconds => 12 hours

    Public Shared ReadOnly c_PARAM_TESTMODE As String = "--testmode"
    Public Shared ReadOnly c_PARAM_FORCE As String = "--force"

    Public Shared ResponseRegex As Regex = New Regex("^ok \(([0-9]+)\)$")


    Public Shared Sub InitGlobals()
        LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) & "\GuestTek\_logs"
        LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        Dim dDate As Date = Now()

        BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) & "\GuestTek\_backups"
        BackupDirectory = BackupDirectory & "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
        BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        Logger = New clsLogger

        InitLogFileName()


        Try
            IO.Directory.CreateDirectory(LogFileDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_LogFileDirectory & "'): " & ex.Message)
        End Try

        Try
            IO.Directory.CreateDirectory(BackupDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_BackupDirectory & "'): " & ex.Message)
        End Try

    End Sub

    Public Shared Sub InitLogFileName()
        Dim dDate As Date = Now()

        LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"
    End Sub

End Class
