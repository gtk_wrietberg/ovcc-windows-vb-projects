﻿Public Class SkCfg
    Private mSkCfgFile As String
    Private mXmlSkCfg = New Xml.XmlDocument
    Private mXmlNameTable As Xml.XmlNameTable
    Private mXmlNameSpace As Xml.XmlNamespaceManager
    Private mXmlNode_Root As Xml.XmlNode


    Public Sub GetSiteKioskConfigFile()
        Dim reg As Microsoft.Win32.RegistryKey

        reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Provisio\\SiteKiosk", False)
        If reg Is Nothing Then
            mSkCfgFile = ""
        Else
            mSkCfgFile = reg.GetValue("LastCfg", "")
        End If
    End Sub

    Public Function Load() As String
        Dim sRet As String = ""

        Try
            If Not IO.File.Exists(mSkCfgFile) Then
                Throw New Exception(mSkCfgFile & " not found!")
            End If


            mXmlSkCfg = New Xml.XmlDocument

            mXmlSkCfg.Load(mSkCfgFile)


            mXmlNameTable = mXmlSkCfg.NameTable
            mXmlNameSpace = New Xml.XmlNamespaceManager(mXmlNameTable)
            mXmlNameSpace.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            mXmlNameSpace.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            mXmlNode_Root = mXmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", mXmlNameSpace)

            If mXmlNode_Root Is Nothing Then
                Throw New Exception("sitekiosk-configuration node not found")
            End If

            Return ""
        Catch ex As Exception
            sRet = ex.Message
        End Try

        Return sRet
    End Function

    Public Function GetMachineNameFromCreditCard() As String
        Dim sRet As String = ""

        Try
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode
            Dim xmlNodeList_DeviceData As Xml.XmlNodeList
            Dim xmlNode_DeviceData As Xml.XmlNode
            Dim xmlNode_CCDevice As Xml.XmlNode
            Dim xmlNodeList_Gateways As Xml.XmlNodeList
            Dim xmlNode_Gateway As Xml.XmlNode
            Dim xmlNode_CCGateway As Xml.XmlNode
            Dim xmlNode_Comment As Xml.XmlNode


            xmlNodeList_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", mXmlNameSpace)
            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

            If xmlNode_SiteCash Is Nothing Then
                Throw New Exception("SiteCash node not found")
            End If


            xmlNodeList_DeviceData = xmlNode_SiteCash.SelectNodes("sk:device-data", mXmlNameSpace)
            For Each xmlNode_DeviceData In xmlNodeList_DeviceData
                If xmlNode_DeviceData.Attributes.GetNamedItem("clsid").InnerText = "dbbdad62-8c09-45f3-b189-9777ec1c8e47" Then
                    xmlNode_CCDevice = xmlNode_DeviceData

                    Exit For
                End If
            Next

            If xmlNode_CCDevice Is Nothing Then
                Throw New Exception("CC device node not found")
            End If


            xmlNodeList_Gateways = xmlNode_CCDevice.SelectNodes("sk:gateway", mXmlNameSpace)
            For Each xmlNode_Gateway In xmlNodeList_Gateways
                If xmlNode_Gateway.Attributes.GetNamedItem("clsid").InnerText = "b9bae269-bad6-41eb-a84f-fdfc938cb93a" Then
                    xmlNode_CCGateway = xmlNode_Gateway

                    Exit For
                End If
            Next

            If xmlNode_CCGateway Is Nothing Then
                Throw New Exception("CC gateway node not found")
            End If


            xmlNode_Comment = xmlNode_CCGateway.SelectSingleNode("sk:comment", mXmlNameSpace)

            If xmlNode_Comment Is Nothing Then
                Throw New Exception("CC comment node not found")
            End If


            sRet = xmlNode_Comment.InnerText
        Catch ex As Exception
            sRet = ex.Message
        End Try

        Return sRet
    End Function

    Public Function GetSiteKioskPaymentStatus() As String
        Dim sRet As String = ""

        Try
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode


            xmlNodeList_Plugins = mXmlNode_Root.SelectNodes("sk:plugin", mXmlNameSpace)
            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

            If xmlNode_SiteCash Is Nothing Then
                Throw New Exception("SiteCash node not found")
            End If


            sRet = xmlNode_SiteCash.Attributes.GetNamedItem("enabled").InnerText
        Catch ex As Exception
            sRet = ex.Message
        End Try

        Return sRet
    End Function

    Public Function GetSiteKioskStartPage() As String
        Dim sRet As String = ""

        Try
            Dim xmlNode_StartPageConfig As Xml.XmlNode
            Dim xmlNode_StartPage As Xml.XmlNode


            xmlNode_StartPageConfig = mXmlNode_Root.SelectSingleNode("sk:startpageconfig", mXmlNameSpace)
            xmlNode_StartPage = xmlNode_StartPageConfig.SelectSingleNode("sk:startpage", mXmlNameSpace)

            sRet = xmlNode_StartPage.InnerText
        Catch ex As Exception
            sRet = ex.Message
        End Try

        Return sRet
    End Function

End Class
