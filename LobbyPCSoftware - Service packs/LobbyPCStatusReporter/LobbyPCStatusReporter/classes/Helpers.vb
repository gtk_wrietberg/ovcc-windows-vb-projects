﻿Imports Microsoft.Win32
Imports System.Runtime.InteropServices

Public Class Helpers
    Public Class Generic
        Public Shared Function IsDevMachine() As Boolean
            Dim sComputerName As String = Environment.MachineName.ToLower
            Dim bRet As Boolean = False

            If InStr(sComputerName, "rietberg") > 0 _
            Or InStr(sComputerName, "superlekkerding") > 0 _
            Or InStr(sComputerName, "wrdev") > 0 Then
                bRet = True
            End If

            Return bRet
        End Function
    End Class

    Public Class Network
        Public Class DomainWorkgroup
            Private Enum JoinStatus
                Unknown = 0
                UnJoined = 1
                Workgroup = 2
                Domain = 3
            End Enum

            <DllImport("netapi32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)> _
            Private Shared Function NetGetJoinInformation( _
            ByVal computerName As String, _
            ByRef buffer As IntPtr, _
            ByRef status As JoinStatus) As Integer
            End Function

            <DllImport("netapi32.dll", SetLastError:=True)> _
            Private Shared Function NetApiBufferFree(ByVal buffer As IntPtr) As Integer
            End Function

            Public Shared Function GetName() As String
                Dim pBuffer As IntPtr = IntPtr.Zero
                Dim mStatus As JoinStatus, mDomainName As String = ""

                Try
                    Dim result As Integer = NetGetJoinInformation(Environment.MachineName, pBuffer, mStatus)
                    If 0 <> result Then Throw New Exception("error")

                    mDomainName = Marshal.PtrToStringUni(pBuffer)
                Catch ex As Exception
                    mDomainName = "error: " & ex.Message
                Finally
                    If Not IntPtr.Zero.Equals(pBuffer) Then
                        NetApiBufferFree(pBuffer)
                    End If
                End Try

                Return mDomainName
            End Function
        End Class
    End Class

    Public Class OSVersion
#Region "OSVERSIONINFOEX"
        <StructLayout(LayoutKind.Sequential)> _
        Private Structure OSVERSIONINFOEX
            Public dwOSVersionInfoSize As Integer
            Public dwMajorVersion As Integer
            Public dwMinorVersion As Integer
            Public dwBuildNumber As Integer
            Public dwPlatformId As Integer
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)> _
            Public szCSDVersion As String
            Public wServicePackMajor As Short
            Public wServicePackMinor As Short
            Public wSuiteMask As Short
            Public wProductType As Byte
            Public wReserved As Byte
        End Structure
#End Region

#Region "64 BIT OS DETECTION"
        Private Delegate Function IsWow64ProcessDelegate(<[In]> handle As IntPtr, <Out> ByRef isWow64Process As Boolean) As Boolean

        <DllImport("kernel32", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)> _
        Public Shared Function LoadLibrary(libraryName As String) As IntPtr
        End Function

        <DllImport("kernel32", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)> _
        Public Shared Function GetProcAddress(hwnd As IntPtr, procedureName As String) As IntPtr
        End Function

        Private Shared Function GetIsWow64ProcessDelegate() As IsWow64ProcessDelegate
            Dim handle As IntPtr = LoadLibrary("kernel32")

            If handle <> IntPtr.Zero Then
                Dim fnPtr As IntPtr = GetProcAddress(handle, "IsWow64Process")

                If fnPtr <> IntPtr.Zero Then
                    Return DirectCast(Marshal.GetDelegateForFunctionPointer(DirectCast(fnPtr, IntPtr), GetType(IsWow64ProcessDelegate)), IsWow64ProcessDelegate)
                End If
            End If

            Return Nothing
        End Function

        Private Shared Function Is32BitProcessOn64BitProcessor() As Boolean
            Dim fnDelegate As IsWow64ProcessDelegate = GetIsWow64ProcessDelegate()

            If fnDelegate Is Nothing Then
                Return False
            End If

            Dim isWow64 As Boolean
            Dim retVal As Boolean = fnDelegate.Invoke(Process.GetCurrentProcess().Handle, isWow64)

            If retVal = False Then
                Return False
            End If

            Return isWow64
        End Function
#End Region

        Public Shared Function OSFull() As String
            Return OSName() & " (" & OSBits() & ")"
        End Function

        Public Shared Function OSBits() As String
            Dim sBits As String = "x??"

            Select Case IntPtr.Size * 8
                Case 64
                    sBits = "x64"
                Case 32
                    If Is32BitProcessOn64BitProcessor() Then
                        sBits = "x64"
                    Else
                        sBits = "x32"
                    End If
                Case Else
                    sBits = "x??"
            End Select

            Return sBits
        End Function

        Public Shared Function OSName() As String
            Dim sName As String = "unknown"

            Try
                Dim osVersion As OperatingSystem = Environment.OSVersion
                Dim majorVersion As Integer = osVersion.Version.Major
                Dim minorVersion As Integer = osVersion.Version.Minor

                Dim osVersionInfo As New OSVERSIONINFOEX()
                osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(GetType(OSVERSIONINFOEX))
                Dim productType As Byte = osVersionInfo.wProductType

                sName = "??? (" & majorVersion.ToString & "." & minorVersion.ToString & "." & productType.ToString & ")"

                Select Case osVersion.Platform
                    Case PlatformID.Win32NT
                        Select Case majorVersion
                            Case 3
                                sName = "Windows NT 3.51"
                            Case 4
                                sName = "Windows NT 4.0"
                            Case 5
                                Select Case minorVersion
                                    Case 0
                                        sName = "Windows 2000"
                                    Case 1
                                        sName = "Windows XP"
                                    Case 2
                                        sName = "Windows Server 2003"
                                End Select
                            Case 6
                                Select Case minorVersion
                                    Case 0
                                        sName = "Windows Vista"
                                    Case 1
                                        sName = "Windows 7"
                                    Case 2
                                        sName = "Windows 8"
                                    Case 3
                                        sName = "Windows 8.1"
                                End Select
                            Case 10
                                sName = "Windows 10"
                        End Select
                End Select

            Catch ex As Exception

            End Try

            Return sName
        End Function
    End Class

    Public Class XML
        Public Shared Function Beautify(sIn As String) As String
            Dim sRet As String = ""

            Try
                Dim xmlDoc As System.Xml.XmlDocument

                xmlDoc = New System.Xml.XmlDocument
                xmlDoc.LoadXml(sIn)

                sRet = Beautify(xmlDoc)
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Public Shared Function Beautify(xmlDoc As System.Xml.XmlDocument) As String
            Dim sRet As String = ""

            Try
                Dim sb As New System.Text.StringBuilder()
                Dim settings As New System.Xml.XmlWriterSettings()

                settings.Indent = True
                settings.IndentChars = "    "
                settings.NewLineChars = vbCrLf
                settings.NewLineHandling = System.Xml.NewLineHandling.Replace

                Using writer As System.Xml.XmlWriter = System.Xml.XmlWriter.Create(sb, settings)
                    xmlDoc.Save(writer)
                End Using

                sRet = sb.ToString()
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

    End Class

    Public Class Types
        Public Shared Function CastStringToInteger_safe(s As String) As Integer
            Dim iTmp As Integer = -1

            If Integer.TryParse(s, iTmp) Then

            End If

            Return iTmp
        End Function

        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function LeadingZero(num As Integer, Optional len As Integer = 2) As String
            Dim sNum As String = num.ToString

            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Public Shared Function RandomString(Length As Integer, Optional CharsToChooseFrom As String = "") As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function

        Public Shared Function FromNormalDateToHBDate(Optional WithTime As Boolean = False) As String
            Return FromNormalDateToHBDate(Now, WithTime)
        End Function

        Public Shared Function FromNormalDateToHBDate(d As Date, Optional WithTime As Boolean = False) As String
            Dim sTmp As String = d.ToString("yyyy-MM-dd")

            If WithTime Then
                sTmp &= " " & d.ToString("HH:mm:ss")
            End If

            Return sTmp
        End Function

        Public Shared Function FromHBDateToNormalDate(s As String) As Date
            Dim d As Date

            Try
                d = Date.Parse(s)
            Catch ex As Exception
                d = Nothing
            End Try

            Return d
        End Function

        Public Shared Function IsValidHBDate(s As String) As Boolean
            Dim d As Date

            Try
                d = Date.Parse(s)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
    End Class

    Public Class TimeDate
        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function
    End Class

    Public Class Registry
        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey


                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("nope")
                End If

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SetValue_Boolean(keyName As String, valueName As String, value As Boolean)
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "yes", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "no", Microsoft.Win32.RegistryValueKind.String)
                End If
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "string"
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            sTmp = Convert.ToString(oTmp)

            Return sTmp
        End Function

        Public Shared Sub SetValue_String(keyName As String, valueName As String, value As String)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            iTmp = Helpers.Types.CastToInteger_safe(oTmp)

            Return iTmp
        End Function

        Public Shared Sub SetValue_Integer(keyName As String, valueName As String, value As Integer)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "misc"
        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

#End Region

    End Class

    Public Class SiteKiosk
        Public Shared Function GetSiteKioskVersion(Optional bShort As Boolean = False) As String
            Dim sTmp As String = Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk", "Build", "0.0")

            If bShort Then
                Return sTmp.Split(".")(0)
            Else
                Return sTmp
            End If
        End Function

        Public Shared Function GetSiteKioskInstallFolder() As String
            Return Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk", "InstallDir", "")
        End Function

        Public Shared Function GetSiteKioskLogfileFolder() As String
            Return Helpers.SiteKiosk.GetSiteKioskInstallFolder & "\logfiles"
        End Function

        Public Shared Function GetMachineNameFromScript() As String
            Dim sRet As String = ""
            Dim sFileBase As String, sFile As String
            sFileBase = GetSiteKioskInstallFolder()
            sFileBase = sFileBase & "\skins\default\scripts"

            sFile = sFileBase & "\guesttek.js"
            If Not IO.File.Exists(sFile) Then
                sFile = sFileBase & "\ibahn.js"
                If Not IO.File.Exists(sFile) Then
                    sFile = sFileBase & "\mycall.js"
                    If Not IO.File.Exists(sFile) Then
                        Return "NO_SCRIPT_FOUND"
                    End If
                End If
            End If

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var LocationID=") Then
                            line = line.Replace("var LocationID=", "")
                            line = line.Replace("""", "")
                            line = line.Replace(";", "")

                            sRet = line

                            Exit While
                        End If
                    End While
                End Using
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Private Shared ReadOnly _sReplacements() As String = { _
            "[SiteKiosk] Notification: [Memory Status]", _
            "[SiteKiosk] Notification: [Process Status]", _
            ""}

        Public Shared Function CleanSiteKioskLogLine(ByVal input As String) As String
            Dim result As String = input

            For Each item As String In _sReplacements
                If Not item.Equals("") Then
                    If result.Contains(item) Then
                        result = ""
                        Exit For
                    End If
                End If
            Next

            If result <> "" Then
                'remove unwanted chars
                For i = 0 To 31
                    result = result.Replace(Chr(i), "")
                Next
                For i = 127 To 255
                    result = result.Replace(Chr(i), "")
                Next
                'Additionally, let's remove </ < and >
                result = result.Replace("</", "[")
                result = result.Replace("<", "[")
                result = result.Replace("/>", "]")
                result = result.Replace(">", "]")
            End If

            Return result
        End Function

        Public Shared Function TranslateSiteKioskPathToNormalPath(sSkPath As String) As String
            Dim sPath As String = sSkPath

            If sPath.StartsWith("%SiteKioskPath%") Then
                sPath = sPath.Replace("%SiteKioskPath%", GetSiteKioskInstallFolder)
                sPath = sPath.Replace("%%PROGRAMFILES%%/SiteKiosk", GetSiteKioskInstallFolder)
                sPath = sPath.Replace("/", "\")
            ElseIf sPath.StartsWith("file://") Then
                sPath = sPath.Replace("file:///", "")
                sPath = sPath.Replace("file://", "")
                sPath = sPath.Replace("/", "\")
            Else

            End If

            Return sPath
        End Function
    End Class

    Public Class FilesAndFolders
        Public Shared Function GetFileCreationDate(sFile As String) As String
            Try
                Dim dDate As Date = IO.File.GetCreationTime(sFile)

                Return dDate.ToString("yyyy-MM-dd")
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String) As String
            Try
                Dim dDate As Date = IO.File.GetLastWriteTime(sFile)

                Return dDate.ToString("yyyy-MM-dd")
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

    End Class

    Public Class MD5Class
        Private Shared ReadOnly _md5 As Security.Cryptography.MD5 = Security.Cryptography.MD5.Create()

        Public Shared Function GetMd5Hash(source As String) As String
            Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
            Dim sb As New System.Text.StringBuilder()

            Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

            Return sb.ToString()
        End Function

        Public Shared Function VerifyMd5Hash(source As String, hash As String) As Boolean
            Dim sourceHash = GetMd5Hash(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function

        Public Shared Function GetMd5HashBase64(source As String) As String
            Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

            Return Convert.ToBase64String(data)
        End Function

        Public Shared Function VerifyMd5HashBase64(source As String, hash As String) As Boolean
            Dim sourceHash = GetMd5HashBase64(source)
            Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

            Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
        End Function
    End Class
End Class
