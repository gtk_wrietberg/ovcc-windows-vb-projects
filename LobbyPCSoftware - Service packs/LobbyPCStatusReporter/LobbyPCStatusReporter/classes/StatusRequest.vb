﻿Public Class clsStatusRequest
    Public Class SkStatus
        Private mClientVersion As String
        Public Property Client_Version() As String
            Get
                Return mClientVersion
            End Get
            Set(ByVal value As String)
                mClientVersion = value
            End Set
        End Property

        Private mOsVersion As String
        Public Property OS_Version() As String
            Get
                Return mOsVersion
            End Get
            Set(ByVal value As String)
                mOsVersion = value
            End Set
        End Property

        Private mOsWorkgroup As String
        Public Property OS_Workgroup() As String
            Get
                Return mOsWorkgroup
            End Get
            Set(ByVal value As String)
                mOsWorkgroup = value
            End Set
        End Property

        Private mMachineName As String
        Public Property Name_Machine() As String
            Get
                Return mMachineName
            End Get
            Set(ByVal value As String)
                mMachineName = value
            End Set
        End Property

        Private mScriptName As String
        Public Property Name_Script() As String
            Get
                Return mScriptName
            End Get
            Set(ByVal value As String)
                mScriptName = value
            End Set
        End Property

        Private mCCName As String
        Public Property Name_CreditCard() As String
            Get
                Return mCCName
            End Get
            Set(ByVal value As String)
                mCCName = value
            End Set
        End Property

        Private mSitekioskVersion As String
        Public Property Sitekiosk_Version() As String
            Get
                Return mSitekioskVersion
            End Get
            Set(ByVal value As String)
                mSitekioskVersion = value
            End Set
        End Property

        Private mPaymentEnabled As Boolean
        Public Property Sitekiosk_PaymentEnabled() As Boolean
            Get
                Return mPaymentEnabled
            End Get
            Set(ByVal value As Boolean)
                mPaymentEnabled = value
            End Set
        End Property

        Private mStartpage As String
        Public Property SiteKiosk_Startpage() As String
            Get
                Return mStartpage
            End Get
            Set(ByVal value As String)
                mStartpage = value
            End Set
        End Property

        Private mTheme As String
        Public Property SiteKiosk_Theme() As String
            Get
                Return mTheme
            End Get
            Set(ByVal value As String)
                mTheme = value
            End Set
        End Property

        Private mStartpage_creation As String
        Public Property SiteKiosk_Startpage_Creation() As String
            Get
                Return mStartpage_creation
            End Get
            Set(ByVal value As String)
                mStartpage_creation = value
            End Set
        End Property

        Private mStartpage_modified As String
        Public Property SiteKiosk_Startpage_Modified() As String
            Get
                Return mStartpage_modified
            End Get
            Set(ByVal value As String)
                mStartpage_modified = value
            End Set
        End Property

    End Class

    Public SkStatusData As SkStatus

    Private mError As String
    Public Property ErrorString() As String
        Get
            Return mError
        End Get
        Set(ByVal value As String)
            mError = value
        End Set
    End Property

    Private mResponse As String
    Public Property Response() As String
        Get
            Return mResponse
        End Get
        Set(ByVal value As String)
            mResponse = value
        End Set
    End Property

    Private mResponseIndented As String
    Public Property ResponseIndented() As String
        Get
            Return mResponseIndented
        End Get
        Set(ByVal value As String)
            mResponseIndented = value
        End Set
    End Property

    Private mRequest As String
    Public ReadOnly Property Request() As String
        Get
            Return mRequest
        End Get
    End Property

    Private mRequestIndented As String
    Public ReadOnly Property RequestIndented() As String
        Get
            Return mRequestIndented
        End Get
    End Property


    Public Sub New()
        mError = ""
        mResponse = ""

        SkStatusData = New SkStatus
    End Sub


    Public Sub Create()
        'request is going to look somewhat like this:
        '
        '<lobbypc>
        '    <client>
        '        <version>%%VERSION%%</version>
        '        <error>%%ERROR%%</error>
        '    <name>
        '        <machine>%%NAME_MACHINE%%</machine>
        '        <script>%%NAME_SCRIPT%%</script>
        '        <cc>%%NAME_CC%%</cc>
        '    </name>
        '    <sitekiosk>
        '        <version>%%SK_VERSION%%</version>
        '        <payment>%%PAYMENT%%</payment>
        '    </sitekiosk>
        '</lobbypc>

        Dim doc As New Xml.XmlDocument
        Dim nodeSub As Xml.XmlNode

        Dim nodeRoot As Xml.XmlNode = doc.CreateElement("lobbypc")
        doc.AppendChild(nodeRoot)


        Dim nodeClient As Xml.XmlNode = doc.CreateElement("client")
        nodeRoot.AppendChild(nodeClient)

        nodeSub = doc.CreateElement("version")
        nodeSub.InnerText = SkStatusData.Client_Version
        nodeClient.AppendChild(nodeSub)

        nodeSub = doc.CreateElement("message")
        nodeSub.InnerText = mError
        nodeClient.AppendChild(nodeSub)


        Dim nodeOS As Xml.XmlNode = doc.CreateElement("os")
        nodeRoot.AppendChild(nodeOS)

        nodeSub = doc.CreateElement("version")
        nodeSub.InnerText = SkStatusData.OS_Version
        nodeOS.AppendChild(nodeSub)

        nodeSub = doc.CreateElement("workgroup")
        nodeSub.InnerText = SkStatusData.OS_Workgroup
        nodeOS.AppendChild(nodeSub)


        Dim nodeName As Xml.XmlNode = doc.CreateElement("name")
        nodeRoot.AppendChild(nodeName)

        nodeSub = doc.CreateElement("machine")
        nodeSub.InnerText = SkStatusData.Name_Machine
        nodeName.AppendChild(nodeSub)

        nodeSub = doc.CreateElement("script")
        nodeSub.InnerText = SkStatusData.Name_Script
        nodeName.AppendChild(nodeSub)

        nodeSub = doc.CreateElement("cc")
        nodeSub.InnerText = SkStatusData.Name_CreditCard
        nodeName.AppendChild(nodeSub)


        Dim nodeSK As Xml.XmlNode = doc.CreateElement("sitekiosk")
        nodeRoot.AppendChild(nodeSK)

        nodeSub = doc.CreateElement("version")
        nodeSub.InnerText = SkStatusData.Sitekiosk_Version
        nodeSK.AppendChild(nodeSub)

        nodeSub = doc.CreateElement("payment")
        If SkStatusData.Sitekiosk_PaymentEnabled Then
            nodeSub.InnerText = "1"
        Else
            nodeSub.InnerText = "0"
        End If
        nodeSK.AppendChild(nodeSub)


        Dim nodeStartpage As Xml.XmlNode = doc.CreateElement("startpage")
        nodeSK.AppendChild(nodeStartpage)

        nodeSub = doc.CreateElement("theme")
        nodeSub.InnerText = SkStatusData.SiteKiosk_Theme
        nodeStartpage.AppendChild(nodeSub)

        nodeSub = doc.CreateElement("file")
        nodeSub.InnerText = SkStatusData.SiteKiosk_Startpage
        nodeStartpage.AppendChild(nodeSub)

        nodeSub = doc.CreateElement("created")
        nodeSub.InnerText = SkStatusData.SiteKiosk_Startpage_Creation
        nodeStartpage.AppendChild(nodeSub)

        nodeSub = doc.CreateElement("modified")
        nodeSub.InnerText = SkStatusData.SiteKiosk_Startpage_Modified
        nodeStartpage.AppendChild(nodeSub)


        mRequest = doc.InnerXml

        mRequestIndented = Helpers.XML.Beautify(doc)
    End Sub

End Class
