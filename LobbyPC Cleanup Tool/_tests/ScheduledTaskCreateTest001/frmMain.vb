﻿Imports System.Security
Imports System.Text
Imports System.Threading
Imports Microsoft.Win32.TaskScheduler

Public Class frmMain
    Private ReadOnly TASK_FOLDER As String = "OVCC"
    Private ReadOnly TASK_NAME As String = "Janitor_Helper"
    Private ReadOnly TASK_PATH As String = "\" & TASK_FOLDER & "\" & TASK_NAME




    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        CleanDebug()


        Dim lTasks As List(Of Task) = TaskService.Instance.AllTasks.ToList

        For Each _task As Task In lTasks
            Dim _sb As New StringBuilder

            _sb.Append("path: " & _task.Path)
            _sb.Append(" | ")

            _sb.Append("name: " & _task.Name)


            AddDebug(_sb.ToString)
        Next
    End Sub



    Private Sub CleanDebug()
        TextBox1.Clear()
    End Sub

    Private Sub AddDebug(s As String)
        TextBox1.AppendText(s & vbCrLf)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        CleanDebug()


        Dim sXml As String = My.Computer.FileSystem.ReadAllText("C:\Temp\sched_test\ScheduledTask__Janitor_Helper.xml")

        Dim _td As TaskDefinition = TaskService.Instance.NewTask

        _td.XmlText = sXml


        TaskService.Instance.RootFolder.RegisterTaskDefinition(TASK_PATH, _td)


        'TaskService.Instance.RootFolder.RegisterTask("\OVCC\Janitor_Helper", sXml)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        CleanDebug()

        Dim _tf As TaskFolder = TaskService.Instance.GetFolder(TASK_FOLDER)

        If _tf Is Nothing Then
            AddDebug("Folder " & TASK_FOLDER & " not found!")

            Exit Sub
        End If


        Dim lTasks As List(Of Task) = _tf.AllTasks.ToList
        Dim _task As Task = Nothing

        For Each _task In lTasks
            If _task.Name.Equals(TASK_NAME) Then
                AddDebug("Found task")

                Exit For
            End If
        Next

        If _task IsNot Nothing Then
            AddDebug("Running task")

            Dim _tdr As RunningTask = _task.Run()


            For i As Integer = 0 To 20
                AddDebug("IsActive" & _tdr.IsActive)

                Threading.Thread.Sleep(500)
            Next
        Else
            AddDebug("Task not found")
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        CleanDebug()

        Dim _tf As TaskFolder = TaskService.Instance.GetFolder(TASK_FOLDER)

        If _tf Is Nothing Then
            AddDebug("Folder " & TASK_FOLDER & " not found!")

            Exit Sub
        End If


        Dim lTasks As List(Of Task) = _tf.AllTasks.ToList
        Dim _task As Task = Nothing

        For Each _task In lTasks
            If _task.Name.Equals(TASK_NAME) Then
                AddDebug("Found task")

                Exit For
            End If
        Next


        Dim a As Action = _task.Definition.Actions(0)
        If a.ActionType = TaskActionType.Execute Then
            Dim aa As ExecAction = a
            AddDebug("action path: " & aa.Path)
            AddDebug("action params: " & aa.Arguments)

            aa.Arguments = "piemeltjepiksaus"

            AddDebug("action path: " & aa.Path)
            AddDebug("action params: " & aa.Arguments)

            Dim na As New ExecAction With {
                .Path = aa.Path,
                .WorkingDirectory = aa.WorkingDirectory,
                .Arguments = aa.Arguments
            }


            _task.Definition.Actions(0) = na
            _task.RegisterChanges()
            _task.Run()
        Else
            AddDebug("huh?")
        End If



    End Sub

End Class
