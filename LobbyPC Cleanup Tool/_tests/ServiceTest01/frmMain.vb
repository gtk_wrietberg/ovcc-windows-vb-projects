﻿Public Class frmMain
    Private bLoop As Boolean = True

    Private mThread_Timer As Threading.Thread
    Private mThread_Update As Threading.Thread


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Debugging = Settings.Registry.Get.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__Debug, False)
        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.Write("started")


        Helpers.Logger.Write(DateTime.Now.Ticks)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not Thread__Timer.IsActive Then
            mThread_Update = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Update.Main))
            mThread_Update.Start()

            mThread_Timer = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Timer.Main))
            mThread_Timer.Start()
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Thread__Timer.IsActive Then
            Thread__Timer.Stop()
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        'mThread_Update = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Update.Main))
        'mThread_Update.Start()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim o As New Cleanup

        o.AddScheduledTask("*")

        o.Cleanup_ScheduledTasks()
    End Sub
End Class