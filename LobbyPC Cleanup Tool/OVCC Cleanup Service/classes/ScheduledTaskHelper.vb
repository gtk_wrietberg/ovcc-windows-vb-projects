﻿Public Class ScheduledTaskHelper
    Public Enum ScheduledTaskResultCode
        OK = &H0 ' Ok
        TaskReady = &H41300 'The task is ready to run at its next scheduled time.
        TaskRunning = &H41301 'The task is currently running.
        TaskDisabled = &H41302 'The task will not run at the scheduled times because it has been disabled.
        TaskHasNotRun = &H41303 'The task has not yet run.
        TaskNoMoreRuns = &H41304 'There are no more runs scheduled for this task.
        TaskNotScheduled = &H41305 'One or more of the properties that are needed to run this task on a schedule have not been set.
        TaskTerminated = &H41306 'The last run of the task was terminated by the user.
        TaskNoValidTriggers = &H41307 'Either the task has no triggers or the existing triggers are disabled or not set.
        EventTrigger = &H41308 'Event triggers do not have set run times.
        TriggerNotFound = &H80041309 'A task's trigger is not found.
        TaskNotReady = &H8004130A 'One or more of the properties required to run this task have not been set.
        TaskNotRunning = &H8004130B 'There is no running instance of the task.
        ServiceNotInstalled = &H8004130C 'The Task Scheduler service is not installed on this computer.
        CannotOpenTask = &H8004130D 'The task object could not be opened.
        InvalidTask = &H8004130E 'The object is either an invalid task object or is not a task object.
        AccountInformationNotSet = &H8004130F 'No account information could be found in the Task Scheduler security database for the task indicated.
        AccountNameNotFound = &H80041310 'Unable to establish existence of the account specified.
        AccountDbaseCorrupt = &H80041311 'Corruption was detected in the Task Scheduler security database; the database has been reset.
        NoSecurityServices = &H80041312 'Task Scheduler security services are available only on Windows NT.
        UnknownObjectVersion = &H80041313 'The task object version is either unsupported or invalid.
        UnsupportedAccountOption = &H80041314 'The task has been configured with an unsupported combination of account settings and run time options.
        ServiceNotRunning = &H80041315 'The Task Scheduler Service is not running.
        UnexpectedNode = &H80041316 'The task XML contains an unexpected node.
        [Namespace] = &H80041317 'The task XML contains an element or attribute from an unexpected namespace.
        InvalidValue = &H80041318 'The task XML contains a value which is incorrectly formatted or out of range.
        MissingNode = &H80041319 'The task XML is missing a required element or attribute.
        MalformedXml = &H8004131A 'The task XML is malformed.
        SomeTriggersFailed = &H4131B 'The task is registered, but not all specified triggers will start the task.
        BatchLogonProblem = &H4131C 'The task is registered, but may fail to start. Batch logon privilege needs to be enabled for the task principal.
        TooManyNodes = &H8004131D 'The task XML contains too many nodes of the same type.
        PastEndBoundary = &H8004131E 'The task cannot be started after the trigger end boundary.
        AlreadyRunning = &H8004131F 'An instance of this task is already running.
        UserNotLoggedOn = &H80041320 'The task will not run because the user is not logged on.
        InvalidTaskHash = &H80041321 'The task image is corrupt or has been tampered with.
        ServiceNotAvailable = &H80041322 'The Task Scheduler service is not available.
        ServiceTooBusy = &H80041323 'The Task Scheduler service is too busy to handle your request. Please try again later.
        TaskAttempted = &H80041324 'The Task Scheduler service attempted to run the task, but the task did not run due to one of the constraints in the task definition
        TaskQueued = &H41325 'The Task Scheduler service has asked the task to run.
        TaskDisabled2 = &H80041326 'The task is disabled.
        TaskNotV1Compatible = &H80041327 'The task has properties that are not compatible with earlier versions of Windows.
        StartOnDemand = &H80041328 'The task settings do not allow the task to start on demand.
    End Enum

    Public Shared Function GetResultCodeAsString(Code As Integer) As String
        Dim sRet As String = "[UNKNOWN]"
        Dim aCodes As Array
        aCodes = System.Enum.GetValues(GetType(ScheduledTaskResultCode))

        For Each iCode As Integer In aCodes
            If Code = iCode Then
                sRet = "[" & System.Enum.GetName(GetType(ScheduledTaskResultCode), iCode) & "]"
                Exit For
            End If
        Next

        Return Code.ToString & "=" & sRet
    End Function
End Class
