﻿Imports System.IO

Public Class HashFile
    Implements IDisposable

    Private mSuperDuperSecretSalt() As Byte = System.Text.Encoding.Unicode.GetBytes("Th1s_1s_a_s3cr3t")


    Private mFilePath As String
    Public Property FilePath() As String
        Get
            Return mFilePath
        End Get
        Set(ByVal value As String)
            mFilePath = value
        End Set
    End Property

    Private mLastError As String
    Public ReadOnly Property LastError() As String
        Get
            Return mLastError
        End Get
    End Property

    Private mFileStream As IO.FileStream

    Public Sub New()
    End Sub

    Public Sub New(FilePath As String)
        mFilePath = FilePath
    End Sub

    Public Sub Dispose() Implements System.IDisposable.Dispose
        Try
            mFileStream.Close()
            mFileStream.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Public Function Calculate(ByRef HashString As String) As Boolean
        Try
            mFileStream = New FileStream(mFilePath, FileMode.Open)
            Dim SHA512 As New Security.Cryptography.SHA512CryptoServiceProvider()
            Dim byteHash() As Byte = SHA512.ComputeHash(mFileStream)
            Dim saltedByte() As Byte = byteHash.Concat(mSuperDuperSecretSalt).ToArray()
            Dim saltedByteHash() As Byte = SHA512.ComputeHash(saltedByte)

            mFileStream.Close()

            HashString = Convert.ToBase64String(saltedByteHash)

            Return True
        Catch ex As Exception
            mLastError = ex.Message
        End Try

        Return False
    End Function
End Class
