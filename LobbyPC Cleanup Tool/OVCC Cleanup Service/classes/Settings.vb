﻿Imports System.Security.Policy
Imports System.Xml

Public Class Settings
    Public Class Constants
        Public Class Service
            Public Shared ReadOnly NAME As String = "OVCC_Janitor"
            Public Shared ReadOnly DISPLAY_NAME As String = "OVCC Janitor"
            Public Shared ReadOnly DESCRIPTION As String = "Scheduled cleaning"

            Public Shared ReadOnly PATH As String = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\OVCC_Janitor")
            Public Shared ReadOnly FILE As String = "OVCC_Janitor.exe"
            Public Shared ReadOnly FULL_PATH As String = IO.Path.Combine(PATH, FILE)
        End Class

        Public Class Helper
            Public Shared ReadOnly PATH As String = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\OVCC_Janitor")
            Public Shared ReadOnly FILE As String = "OVCC_Janitor_Helper.exe"
            Public Shared ReadOnly FULL_PATH As String = IO.Path.Combine(PATH, FILE)

            Public Shared ReadOnly LOG_NAME As String = "HELPER"
            Public Shared ReadOnly LOG_PATH As String = Helpers.WindowsUser.GetProfileTempDirectory(Settings.Constants.SiteKiosk.USERNAME)

            Public Class ScheduledTask
                Public Shared ReadOnly FOLDER As String = "OVCC"
                Public Shared ReadOnly NAME As String = "Janitor_Helper"
                Public Shared ReadOnly PATH As String = "\" & FOLDER & "\" & NAME
            End Class
        End Class

        Public Class SiteKiosk
            Public Shared ReadOnly USERNAME As String = "SiteKiosk"
            Public Shared ReadOnly PASSWORD As String = "Provisi0"
        End Class

        Public Class Misc
            Public Shared ReadOnly SERVICE_DISABLED_VALUE As Long = 4&
        End Class

        Public Class Urls
            Public Shared ReadOnly UPDATE As String = "https://ccms-public.s3.amazonaws.com/OVCCJanitor/"
        End Class

        Public Class Timeouts
            Public Shared ReadOnly MILLISECONDS_IN_ONE_SECOND As Integer = 1000 'just in case this changes ;-)
            Public Shared ReadOnly TIMER_LOOP_DELAY__SECONDS As Integer = 3600

            Public Shared ReadOnly HELPER_TASK As Integer = 10
            Public Shared ReadOnly HELPER_PROCESS As Integer = 300
        End Class

        Public Class Files
            Public Shared ReadOnly SCHEDULED_TASK_TEMPLATE_FILE As String = "TEMPLATE__ScheduledTask__Janitor_Helper.xml"

            Public Shared ReadOnly CLEANER_CONFIG_FILE As String = "OVCC_Janitor_config.xml"
            Public Shared ReadOnly CLEANER_CONFIG_TMP_FILE As String = "OVCC_Janitor_config.tmp.xml"

            Public Shared ReadOnly CLEANER_CONFIG_PATH As String = IO.Path.Combine(Service.PATH, CLEANER_CONFIG_FILE)
            Public Shared ReadOnly CLEANER_CONFIG_TMP_PATH As String = IO.Path.Combine(Service.PATH, CLEANER_CONFIG_TMP_FILE)
        End Class

        Public Class Registry
            Public Shared ReadOnly KEY__GuestTek_Root As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek"

            Public Shared ReadOnly KEY__OVCCJanitor As String = KEY__GuestTek_Root & "\OVCCJanitor"
            Public Shared ReadOnly VALUENAME__LastUpdate As String = "LastUpdate"
            Public Shared ReadOnly VALUENAME__LastActivation As String = "LastActivation"
            Public Shared ReadOnly VALUENAME__LastHelperActivation As String = "LastHelperActivation"
            Public Shared ReadOnly VALUENAME__LastError As String = "LastError"
            Public Shared ReadOnly VALUENAME__LastErrorMessage As String = "LastErrorMessage"
            Public Shared ReadOnly VALUENAME__ConfigHash As String = "ConfigHash"
            Public Shared ReadOnly VALUENAME__ForcedCleaning As String = "ForcedCleaning"
            Public Shared ReadOnly VALUENAME__ForcedUpdate As String = "ForcedUpdate"

            Public Shared ReadOnly KEY__OVCCJanitor__Settings As String = KEY__OVCCJanitor & "\Settings"
            Public Shared ReadOnly VALUENAME__Settings__Debug As String = "Debug"
            Public Shared ReadOnly VALUENAME__Settings__ActivationType As String = "ActivationType"
            Public Shared ReadOnly VALUENAME__Settings__ActivationHour As String = "ActivationHour"
            Public Shared ReadOnly VALUENAME__Settings__ServerUrl As String = "ServerUrl"
        End Class

        Public Class Config
            Public Shared ReadOnly ROOT As String = "/janitor/config"
            Public Shared ReadOnly SYSTEM As String = "/system"
            Public Shared ReadOnly USER As String = "/user"
        End Class
    End Class


    Public Class Registry
        Public Class [Get]
            Public Shared Function [String](Key As String, ValueName As String) As String
                Return [String](Key, ValueName, "")
            End Function

            Public Shared Function [String](Key As String, ValueName As String, [Default] As String) As String
                Return Helpers.Registry.GetValue_String(Key, ValueName, [Default])
            End Function


            Public Shared Function [Boolean](Key As String, ValueName As String) As Boolean
                Return [Boolean](Key, ValueName, False)
            End Function

            Public Shared Function [Boolean](Key As String, ValueName As String, [Default] As Boolean) As Boolean
                Return Helpers.Registry.GetValue_Boolean(Key, ValueName, [Default])
            End Function


            Public Shared Function [Integer](Key As String, ValueName As String) As Integer
                Return [Integer](Key, ValueName, 0)
            End Function

            Public Shared Function [Integer](Key As String, ValueName As String, [Default] As Integer) As Integer
                Return Helpers.Registry.GetValue_Integer(Key, ValueName, [Default])
            End Function


            Public Shared Function [Long](Key As String, ValueName As String) As Long
                Return [Long](Key, ValueName, 0)
            End Function

            Public Shared Function [Long](Key As String, ValueName As String, [Default] As Long) As Long
                Return Helpers.Registry.GetValue_Long(Key, ValueName, [Default])
            End Function
        End Class

        Public Class [Set]
            Public Shared Function [String](Key As String, ValueName As String, Value As String) As Boolean
                Return Helpers.Registry.SetValue_String(Key, ValueName, Value)
            End Function

            Public Shared Function [Boolean](Key As String, ValueName As String, Value As Boolean) As Boolean
                Return Helpers.Registry.SetValue_Boolean(Key, ValueName, Value)
            End Function

            Public Shared Function [Integer](Key As String, ValueName As String, Value As Integer) As Boolean
                Return Helpers.Registry.SetValue_Integer(Key, ValueName, Value)
            End Function

            Public Shared Function [Long](Key As String, ValueName As String, Value As Long) As Boolean
                Return Helpers.Registry.SetValue_Long(Key, ValueName, Value)
            End Function
        End Class
    End Class


    Public Shared CleanupContainer As Cleanup = New Cleanup


    Public Class XML
        Public Shared Function Test() As Boolean
            Helpers.Logger.WriteMessageRelative("SETTINGS", "checking cleaner settings", 1)

            Try
                Dim xmlDoc As XmlDocument
                Dim xmlNodelist As XmlNodeList
                Dim xmlNodelist2 As XmlNodeList
                Dim xmlNode As XmlNode
                Dim xmlNode2 As XmlNode
                Dim sTmp As String
                Dim sTmpName As String, bTmpRecursive As Boolean, sTmpPattern As String, iTmpMaxAge As Integer
                Dim sKeyName As String, sValueName As String, sValue As String, sType As String, oType As Microsoft.Win32.RegistryValueKind

                xmlDoc = New XmlDocument()


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "loading config XML file", 2)
                Helpers.Logger.WriteMessageRelative("SETTINGS", Settings.Constants.Files.CLEANER_CONFIG_TMP_PATH, 3)
                xmlDoc.Load(Settings.Constants.Files.CLEANER_CONFIG_TMP_PATH)
                Helpers.Logger.WriteMessageRelative("SETTINGS", "ok", 4)


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "verifying config", 2)


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "services", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & Constants.Config.SYSTEM & "/services/service")
                For Each xmlNode In xmlNodelist
                    sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                    Helpers.Logger.WriteMessageRelative("SETTINGS", sTmpName, 4)
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "startup apps", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & Constants.Config.SYSTEM & "/startups/application")
                For Each xmlNode In xmlNodelist
                    sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                    Helpers.Logger.WriteMessageRelative("SETTINGS", sTmpName, 4)
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "files and folders", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & Constants.Config.SYSTEM & "/files-and-folders/location")
                For Each xmlNode In xmlNodelist
                    sTmpName = xmlNode.Attributes.GetNamedItem("path").Value

                    Helpers.Logger.WriteMessageRelative("SETTINGS", sTmpName, 4)
                    Helpers.Logger.WriteMessageRelative("SETTINGS", "params", 5)

                    sTmp = xmlNode.ChildNodes.Item(0).InnerText
                    If sTmp = "yes" Then
                        bTmpRecursive = True
                    Else
                        bTmpRecursive = False
                    End If
                    Helpers.Logger.WriteMessageRelative("SETTINGS", "recursive", 6)
                    Helpers.Logger.WriteMessageRelative("SETTINGS", bTmpRecursive.ToString, 7)

                    sTmpPattern = xmlNode.ChildNodes.Item(1).InnerText
                    Helpers.Logger.WriteMessageRelative("SETTINGS", "pattern", 6)
                    Helpers.Logger.WriteMessageRelative("SETTINGS", sTmpPattern, 7)

                    sTmp = xmlNode.ChildNodes.Item(2).InnerText
                    If IsNumeric(sTmp) Then
                        Try
                            iTmpMaxAge = Integer.Parse(sTmp)
                        Catch ex As Exception
                            iTmpMaxAge = 0
                        End Try
                    Else
                        iTmpMaxAge = 0
                    End If
                    Helpers.Logger.WriteMessageRelative("SETTINGS", "max age", 6)
                    Helpers.Logger.WriteMessageRelative("SETTINGS", iTmpMaxAge.ToString, 7)
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "registry keys", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & Constants.Config.SYSTEM & "/registry/key")
                For Each xmlNode In xmlNodelist
                    sKeyName = xmlNode.Attributes.GetNamedItem("name").Value

                    xmlNodelist2 = xmlNode.SelectNodes("value")
                    For Each xmlNode2 In xmlNodelist2
                        sValueName = xmlNode2.Attributes.GetNamedItem("name").Value
                        sType = xmlNode2.Attributes.GetNamedItem("type").Value
                        sValue = xmlNode2.InnerText

                        Select Case sType.ToLower
                            Case "DWord".ToLower
                                oType = Microsoft.Win32.RegistryValueKind.DWord
                            Case "MultiString".ToLower
                                oType = Microsoft.Win32.RegistryValueKind.MultiString
                            Case Else
                                sType = "String"
                                oType = Microsoft.Win32.RegistryValueKind.String
                        End Select

                        Helpers.Logger.WriteMessageRelative("SETTINGS", "key", 4)
                        Helpers.Logger.WriteMessageRelative("SETTINGS", sKeyName, 5)
                        Helpers.Logger.WriteMessageRelative("SETTINGS", "value name", 4)
                        Helpers.Logger.WriteMessageRelative("SETTINGS", sValueName, 5)
                        Helpers.Logger.WriteMessageRelative("SETTINGS", "value", 4)
                        Helpers.Logger.WriteMessageRelative("SETTINGS", sValue, 5)
                        Helpers.Logger.WriteMessageRelative("SETTINGS", "type", 4)
                        Helpers.Logger.WriteMessageRelative("SETTINGS", sType, 5)
                    Next
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "scheduled tasks", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & Constants.Config.SYSTEM & "/scheduled-tasks/task")
                For Each xmlNode In xmlNodelist
                    sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                    Helpers.Logger.WriteMessageRelative("SETTINGS", sTmpName, 4)
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Return True
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("SETTINGS", "ERROR", 2)
                Helpers.Logger.WriteErrorRelative("SETTINGS", ex.Message, 3)

                Return False
            End Try
        End Function

        Public Shared Function Verify() As Boolean
            Dim bRet As Boolean = False

            Helpers.Logger.WriteDebugRelative("SETTINGS", "verifying config hash", 1)

            If IO.File.Exists(Settings.Constants.Files.CLEANER_CONFIG_PATH) Then
                Dim _hash As New HashFile()

                _hash.FilePath = Settings.Constants.Files.CLEANER_CONFIG_PATH

                Helpers.Logger.WriteDebugRelative("SETTINGS", "config file found", 2)

                Dim sConfigHashStored As String = Settings.Registry.Get.String(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ConfigHash, "(empty)")
                Dim sConfigHashCalculated As String = ""

                Helpers.Logger.WriteDebugRelative("SETTINGS", "stored hash: " & sConfigHashStored, 3)


                If _hash.Calculate(sConfigHashCalculated) Then
                    Helpers.Logger.WriteDebugRelative("SETTINGS", "calculated hash: " & sConfigHashCalculated, 3)

                    If sConfigHashStored.Equals(sConfigHashCalculated) Then
                        Helpers.Logger.WriteDebugRelative("SETTINGS", "hashes match", 4)

                        bRet = True
                    Else
                        Helpers.Logger.WriteDebugRelative("SETTINGS", "hash mismatch", 4)
                    End If
                Else
                    Helpers.Logger.WriteErrorRelative("SETTINGS", "calculating hash failed: " & _hash.LastError, 4)
                End If
            Else
                Helpers.Logger.WriteErrorRelative("SETTINGS", "file not found!", 2)
            End If

            Return bRet
        End Function

        Public Shared Function Hash() As String
            Dim sConfigHashCalculated As String = ""

            Helpers.Logger.WriteMessageRelative("SETTINGS", "calculating config hash", 1)

            If IO.File.Exists(Settings.Constants.Files.CLEANER_CONFIG_PATH) Then
                Dim _hash As New HashFile()

                _hash.FilePath = Settings.Constants.Files.CLEANER_CONFIG_PATH

                If _hash.Calculate(sConfigHashCalculated) Then
                    Helpers.Logger.WriteMessageRelative("SETTINGS", "calculated hash: " & sConfigHashCalculated, 2)
                Else
                    Helpers.Logger.WriteErrorRelative("SETTINGS", "calculating hash failed: " & _hash.LastError, 2)
                End If
            Else
                Helpers.Logger.WriteErrorRelative("SETTINGS", "calculating hash failed: file does not exist", 2)
            End If

            Return sConfigHashCalculated
        End Function

        Public Shared Function Load() As Boolean
            Helpers.Logger.WriteMessageRelative("SETTINGS", "loading cleaner settings", 1)

            Try
                Dim xmlDoc As XmlDocument
                Dim xmlNodelist As XmlNodeList
                Dim xmlNodelist2 As XmlNodeList
                Dim xmlNode As XmlNode
                Dim xmlNode2 As XmlNode
                Dim sTmp As String
                Dim sTmpName As String, bTmpRecursive As Boolean, sTmpPattern As String, iTmpMaxAge As Integer
                Dim sKeyName As String, sValueName As String, sValue As String, sType As String, oType As Microsoft.Win32.RegistryValueKind

                xmlDoc = New XmlDocument()
                CleanupContainer = New Cleanup


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "loading config XML file", 2)
                Helpers.Logger.WriteMessageRelative("SETTINGS", Settings.Constants.Files.CLEANER_CONFIG_PATH, 3)
                xmlDoc.Load(Settings.Constants.Files.CLEANER_CONFIG_PATH)
                Helpers.Logger.WriteMessageRelative("SETTINGS", "ok", 4)


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "loading services", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & "/config/system/services/service")
                For Each xmlNode In xmlNodelist
                    sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                    Helpers.Logger.WriteDebugRelative("SETTINGS", sTmpName, 4)
                    CleanupContainer.AddService(sTmpName)
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "loading startup apps", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & "/config/system/startups/startup")
                For Each xmlNode In xmlNodelist
                    sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                    Helpers.Logger.WriteDebugRelative("SETTINGS", sTmpName, 4)
                    CleanupContainer.AddStartup(sTmpName)
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "loading folders", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & "/config/system/folders/folder")
                For Each xmlNode In xmlNodelist
                    sTmpName = xmlNode.Attributes.GetNamedItem("path").Value

                    Helpers.Logger.WriteDebugRelative("SETTINGS", sTmpName, 4)
                    Helpers.Logger.WriteDebugRelative("SETTINGS", "params", 5)

                    sTmp = xmlNode.ChildNodes.Item(0).InnerText
                    If sTmp = "yes" Then
                        bTmpRecursive = True
                    Else
                        bTmpRecursive = False
                    End If
                    Helpers.Logger.WriteDebugRelative("SETTINGS", "recursive", 6)
                    Helpers.Logger.WriteDebugRelative("SETTINGS", bTmpRecursive.ToString, 7)

                    sTmpPattern = xmlNode.ChildNodes.Item(1).InnerText
                    Helpers.Logger.WriteDebugRelative("SETTINGS", "pattern", 6)
                    Helpers.Logger.WriteDebugRelative("SETTINGS", sTmpPattern, 7)

                    sTmp = xmlNode.ChildNodes.Item(2).InnerText
                    If IsNumeric(sTmp) Then
                        Try
                            iTmpMaxAge = Integer.Parse(sTmp)
                        Catch ex As Exception
                            iTmpMaxAge = 0
                        End Try
                    Else
                        iTmpMaxAge = 0
                    End If
                    Helpers.Logger.WriteDebugRelative("SETTINGS", "max age", 6)
                    Helpers.Logger.WriteDebugRelative("SETTINGS", iTmpMaxAge.ToString, 7)

                    CleanupContainer.AddFolder(sTmpName, bTmpRecursive, sTmpPattern, iTmpMaxAge)
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "loading registry keys", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & "/config/system/registry/key")
                For Each xmlNode In xmlNodelist
                    sKeyName = xmlNode.Attributes.GetNamedItem("name").Value

                    xmlNodelist2 = xmlNode.SelectNodes("value")
                    For Each xmlNode2 In xmlNodelist2
                        sValueName = xmlNode2.Attributes.GetNamedItem("name").Value
                        sType = xmlNode2.Attributes.GetNamedItem("type").Value
                        sValue = xmlNode2.InnerText

                        Select Case sType.ToLower
                            Case "DWord".ToLower
                                oType = Microsoft.Win32.RegistryValueKind.DWord
                            Case "MultiString".ToLower
                                oType = Microsoft.Win32.RegistryValueKind.MultiString
                            Case Else
                                sType = "String"
                                oType = Microsoft.Win32.RegistryValueKind.String
                        End Select

                        Helpers.Logger.WriteDebugRelative("SETTINGS", "key", 4)
                        Helpers.Logger.WriteDebugRelative("SETTINGS", sKeyName, 5)
                        Helpers.Logger.WriteDebugRelative("SETTINGS", "value name", 4)
                        Helpers.Logger.WriteDebugRelative("SETTINGS", sValueName, 5)
                        Helpers.Logger.WriteDebugRelative("SETTINGS", "value", 4)
                        Helpers.Logger.WriteDebugRelative("SETTINGS", sValue, 5)
                        Helpers.Logger.WriteDebugRelative("SETTINGS", "type", 4)
                        Helpers.Logger.WriteDebugRelative("SETTINGS", sType, 5)
                        CleanupContainer.AddRegistry(sKeyName, sValueName, sValue, oType)
                    Next
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Helpers.Logger.WriteMessageRelative("SETTINGS", "loading scheduled tasks", 3)
                xmlNodelist = xmlDoc.SelectNodes(Constants.Config.ROOT & "/config/system/scheduled-tasks/task")
                For Each xmlNode In xmlNodelist
                    sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                    Helpers.Logger.WriteDebugRelative("SETTINGS", sTmpName, 4)
                    CleanupContainer.AddScheduledTask(sTmpName)
                Next


                '------------------------------------------------------------------------------------------------------------------------------------
                Return True
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("SETTINGS", "ERROR", 2)
                Helpers.Logger.WriteErrorRelative("SETTINGS", ex.Message, 3)

                Return False
            End Try
        End Function


    End Class
End Class
