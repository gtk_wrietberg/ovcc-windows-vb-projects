Imports Microsoft.Win32

Public Class Cleanup
    Private ReadOnly WILDCARD As String = "*"

    Private Const cDoesNotExistString As String = "(does not exist)"
    Private Const cRegistryStartupLocation1 As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"
    Private Const cRegistryStartupLocation2 As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce"

    Private mServices As List(Of Cleanup_Service)
    Private mCountServices As Integer

    Private mStartups As List(Of Cleanup_Startup)
    Private mStartupsLocations As List(Of String)
    Private mStartupsKeys As List(Of String)
    Private mCountStartups As Integer

    Private mFolders As List(Of Cleanup_Folder)
    Private mCountFolders As Integer

    Private mRegistry As List(Of Cleanup_Registry)
    Private mCountRegistry As Integer

    Private mScheduledTasks As List(Of Cleanup_ScheduledTask)
    Private mCountScheduledTasks

    Private mFolderTotalFileSize As Long
    Private mFolderTotalDeletedFileSize As Long
    Private mFolderTotalDelayedFileSize As Long

    Public Sub New()
        mServices = New List(Of Cleanup_Service)
        mStartups = New List(Of Cleanup_Startup)
        mStartupsKeys = New List(Of String)
        mStartupsLocations = New List(Of String)
        mFolders = New List(Of Cleanup_Folder)
        mRegistry = New List(Of Cleanup_Registry)
        mScheduledTasks = New List(Of Cleanup_ScheduledTask)

        mCountServices = 0
        mCountStartups = 0
        mCountFolders = 0
        mCountRegistry = 0
        mCountScheduledTasks = 0

        mFolderTotalFileSize = 0
        mFolderTotalDeletedFileSize = 0
        mFolderTotalDelayedFileSize = 0
    End Sub

#Region "properties"
    Public ReadOnly Property NumberOfServices() As Integer
        Get
            Return mServices.Count
        End Get
    End Property

    Public ReadOnly Property NumberOfStartups() As Integer
        Get
            Return mStartups.Count
        End Get
    End Property

    Public ReadOnly Property NumberOfFolders() As Integer
        Get
            Return mFolders.Count
        End Get
    End Property

    Public ReadOnly Property TotalFileSize() As Long
        Get
            Return mFolderTotalFileSize
        End Get
    End Property

    Public ReadOnly Property TotalDeletedFileSize() As Long
        Get
            Return mFolderTotalDeletedFileSize
        End Get
    End Property

    Public ReadOnly Property TotalDelayedFileSize() As Long
        Get
            Return mFolderTotalDelayedFileSize
        End Get
    End Property
#End Region

#Region "reset counters"
    Public Sub ResetCounters()
        mCountServices = 0
        mCountStartups = 0
        mCountFolders = 0
        mCountRegistry = 0
        mCountScheduledTasks = 0
    End Sub

    Public Sub ResetCounterServices()
        mCountServices = 0
    End Sub

    Public Sub ResetCounterStartups()
        mCountStartups = 0
    End Sub

    Public Sub ResetCounterFolders()
        mCountFolders = 0
    End Sub
#End Region

#Region "setters"
    Public Sub AddService(ByVal sServiceName As String)
        Dim oConfig_Service As New Cleanup_Service

        oConfig_Service.Name = sServiceName

        mServices.Add(oConfig_Service)
    End Sub

    Public Sub AddStartup(ByVal sStartupName As String)
        Dim oConfig_Startup As New Cleanup_Startup

        oConfig_Startup.Name = sStartupName

        mStartups.Add(oConfig_Startup)
    End Sub

    Public Sub AddRegistry(ByVal sKeyName As String, ByVal sValueName As String, ByVal oValue As Object, ByVal oType As RegistryValueKind)
        Dim oConfig_Registry As New Cleanup_Registry

        oConfig_Registry.KeyName = sKeyName
        oConfig_Registry.ValueName = sValueName
        oConfig_Registry.Value = oValue
        oConfig_Registry.Type = oType

        mRegistry.Add(oConfig_Registry)
    End Sub

    Public Sub AddFolder(ByVal sPath As String, ByVal bRecursive As Boolean, ByVal sFileNamePattern As String, ByVal iMaxFileAge As Integer)
        Dim oConfig_Folder As New Cleanup_Folder

        oConfig_Folder.Path = sPath
        oConfig_Folder.Recursive = bRecursive
        oConfig_Folder.FileNamePattern = sFileNamePattern
        oConfig_Folder.MaxFileAge = iMaxFileAge

        mFolders.Add(oConfig_Folder)
    End Sub

    Public Sub AddScheduledTask(ByVal sName As String)
        Dim oConfig_ScheduledTask As New Cleanup_ScheduledTask

        oConfig_ScheduledTask.ScheduleName = sName
        oConfig_ScheduledTask.IsInvalid = False

        mScheduledTasks.Add(oConfig_ScheduledTask)
    End Sub
#End Region

#Region "getters"
    Public Function GetNextService(ByRef sName As String, ByRef bFound As Boolean, ByRef bDisabled As Boolean) As Boolean
        If mCountServices > mServices.Count - 1 Then
            Return False
        End If

        mCountServices += 1

        sName = mServices.Item(mCountServices - 1).Name
        bFound = mServices.Item(mCountServices - 1).Found
        bDisabled = mServices.Item(mCountServices - 1).Disabled

        Return True
    End Function

    Public Function GetNextStartup(ByRef sName As String, ByRef bFound As Boolean) As Boolean
        If mCountStartups > mStartups.Count - 1 Then
            Return False
        End If

        mCountStartups += 1

        sName = mStartups.Item(mCountStartups - 1).Name
        bFound = mStartups.Item(mCountStartups - 1).Found

        Return True
    End Function

    Public Function GetNextFolder(ByRef sPath As String, ByRef iFound As Integer, ByRef iDeleted As Integer, ByRef iDelayed As Integer) As Boolean
        If mCountFolders > mFolders.Count - 1 Then
            Return False
        End If

        mCountFolders += 1

        sPath = mFolders.Item(mCountFolders - 1).Path
        iFound = mFolders.Item(mCountFolders - 1).Found
        iDeleted = mFolders.Item(mCountFolders - 1).Deleted
        iDelayed = mFolders.Item(mCountFolders - 1).Delayed

        Return True
    End Function
#End Region

#Region "cleaners"
#Region "public"
    Public Function CleanUp_Services() As Boolean
        Dim bRet As Boolean = True

        For Each oService As Cleanup_Service In mServices
            bRet = bRet And _DisableWindowsService(oService)
        Next

        Return bRet
    End Function


    Public Function CleanUp_Startups__LOCAL_MACHINE() As Boolean
        Return _CleanUp_Startups(False)
    End Function

    Public Function CleanUp_Startups__CURRENT_USER() As Boolean
        Return _CleanUp_Startups(False)
    End Function

    Private Function _CleanUp_Startups(bCurrentUser As Boolean) As Boolean
        mStartupsLocations = New List(Of String)
        mStartupsLocations.Add(cRegistryStartupLocation1)
        mStartupsLocations.Add(cRegistryStartupLocation2)

        Dim bFound As Boolean = False

        If bCurrentUser Then
            bFound = _FindWindowsStartupKeys__CURRENT_USER()
        Else
            bFound = _FindWindowsStartupKeys__LOCAL_MACHINE()
        End If
        If Not bFound Then
            Return False
        End If

        If mStartupsKeys.Count > 0 Then
            Return _DisableWindowsStartup()
        Else
            Helpers.Logger.WriteRelative("CLEANER", "no startup keys found", , 2)

            Return True
        End If
    End Function


    Public Function CleanUp_Folders() As Boolean
        Dim oFileCleanUp As FileCleanup
        Dim oFolder As Cleanup_Folder
        Dim bRet As Boolean

        bRet = True

        For Each oFolder In mFolders
            oFileCleanUp = New FileCleanup

            oFileCleanUp.Folder = oFolder.Path
            oFileCleanUp.LoadFiles(oFolder.FileNamePattern, oFolder.MaxFileAge, oFolder.Recursive)

            oFolder.Found = oFileCleanUp.FilesCount
            oFolder.TotalFileSize = oFileCleanUp.FilesTotalSize
            mFolderTotalFileSize += oFileCleanUp.FilesTotalSize

            oFileCleanUp.DeleteFilesAndFolders()

            oFolder.Deleted = oFileCleanUp.FilesDeleted
            oFolder.Delayed = oFileCleanUp.FilesDelayed
            oFolder.TotalDeletedFileSize = oFileCleanUp.FilesDeletedTotalSize
            mFolderTotalDeletedFileSize += oFileCleanUp.FilesDeletedTotalSize
            oFolder.TotalDelayedFileSize = oFileCleanUp.FilesDelayedTotalSize
            mFolderTotalDelayedFileSize += oFileCleanUp.FilesDelayedTotalSize
        Next

        Return bRet
    End Function

    Public Function CleanUp_RegistryKeys() As Boolean
        Dim bRet As Boolean = True

        For Each oRegistry As Cleanup_Registry In mRegistry
            bRet = bRet And _DisableRegistryKeys(oRegistry)
        Next

        Return bRet
    End Function

    Public Function Cleanup_ScheduledTasks() As Boolean
        Dim bRet As Boolean = True


        Helpers.Logger.WriteMessageRelative("CLEANER", "scheduled tasks to delete", 1)
        For Each oScheduledTask As Cleanup_ScheduledTask In mScheduledTasks
            Helpers.Logger.WriteMessageRelative("CLEANER", oScheduledTask.ScheduleName, 2)
        Next


        Dim lTasks As List(Of TaskScheduler.Task) = TaskScheduler.TaskService.Instance.AllTasks.ToList

        Helpers.Logger.WriteMessageRelative("CLEANER", "found total " & lTasks.Count & " scheduled tasks", 1)
        Helpers.Logger.WriteMessageRelative("CLEANER", "cleaning up " & mScheduledTasks.Count & " scheduled tasks", 1)

        For Each _task As TaskScheduler.Task In lTasks
            For Each oScheduledTask As Cleanup_ScheduledTask In mScheduledTasks
                If _WildcardCount(oScheduledTask.ScheduleName) > 2 Then                                                             ' too many wildcards
                    Helpers.Logger.WriteWarningRelative("CLEANER", "rule '" & oScheduledTask.ScheduleName & "' is invalid, skipped", 2)
                    Continue For
                End If


                Dim sScheduleNameWithoutWildcard As String = oScheduledTask.ScheduleName.Replace(WILDCARD, "")

                If _WildcardCount(oScheduledTask.ScheduleName) = 2 Then
                    If oScheduledTask.ScheduleName.StartsWith(WILDCARD) And oScheduledTask.ScheduleName.EndsWith(WILDCARD) Then     ' *???????*
                        If _task.Name.Contains(sScheduleNameWithoutWildcard) Then
                            bRet = bRet And _DeleteScheduledTask(_task, oScheduledTask.ScheduleName)
                        End If
                    Else
                        Helpers.Logger.WriteWarningRelative("CLEANER", "rule '" & oScheduledTask.ScheduleName & "' is invalid, skipped", 2)
                        Continue For
                    End If
                ElseIf _WildcardCount(oScheduledTask.ScheduleName) = 1 Then
                    If oScheduledTask.ScheduleName.StartsWith(WILDCARD) Then                                                        ' *???????
                        If _task.Name.EndsWith(sScheduleNameWithoutWildcard) Then
                            bRet = bRet And _DeleteScheduledTask(_task, oScheduledTask.ScheduleName)
                        End If
                    ElseIf oScheduledTask.ScheduleName.EndsWith(WILDCARD) Then                                                      ' ???????*
                        If _task.Name.StartsWith(sScheduleNameWithoutWildcard) Then
                            bRet = bRet And _DeleteScheduledTask(_task, oScheduledTask.ScheduleName)
                        End If
                    Else                                                                                                            ' ???*????
                        Dim wildcardIndex As Integer = oScheduledTask.ScheduleName.IndexOf(WILDCARD)
                        Dim firstPart As String = oScheduledTask.ScheduleName.Substring(0, wildcardIndex)
                        Dim lastPart As String = oScheduledTask.ScheduleName.Substring(wildcardIndex + 1)

                        If _task.Name.StartsWith(firstPart) And _task.Name.EndsWith(lastPart) Then
                            bRet = bRet And _DeleteScheduledTask(_task, oScheduledTask.ScheduleName)
                        End If
                    End If
                Else
                    If oScheduledTask.ScheduleName.Equals(_task.Name) Then                                                          ' no wildcards
                        bRet = bRet And _DeleteScheduledTask(_task, oScheduledTask.ScheduleName)
                    End If
                End If
            Next
        Next

        Return bRet
    End Function

    Private Function _WildcardCount(s As String) As Integer
        Dim _count As Integer = 0

        For _i As Integer = 0 To s.Length - 1
            If s.Substring(_i, 1).Equals(WILDCARD) Then
                _count += 1
            End If
        Next

        Return _count
    End Function
#End Region

#Region "private"
    Private Function _DisableWindowsService(ByRef oService As Cleanup_Service) As Boolean
        Dim rk_SERVICE As RegistryKey
        oService.Found = False
        oService.Disabled = False

        Helpers.Logger.WriteRelative("CLEANER", "disabling " & oService.Name, , 1)
        Helpers.Logger.WriteRelative("CLEANER", "opening registry key", , 2)
        Try
            Helpers.Logger.WriteRelative("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\" & oService.Name, , 3)
            rk_SERVICE = Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & oService.Name, True)
        Catch ex As Exception
            Helpers.Logger.WriteRelative("CLEANER", "fail", , 4)
            Helpers.Logger.WriteRelative("CLEANER", ex.ToString, , 5)

            Return False
        End Try

        If rk_SERVICE Is Nothing Then
            Helpers.Logger.WriteRelative("CLEANER", "does not exist", , 4)

            Return True
        End If

        Helpers.Logger.WriteRelative("CLEANER", "ok", , 4)

        oService.Found = True

        Helpers.Logger.WriteRelative("CLEANER", "disabling service", , 2)

        Try
            Helpers.Logger.WriteRelative("CLEANER", "setting 'Start' to " & Settings.Constants.Misc.SERVICE_DISABLED_VALUE.ToString, , 3)
            rk_SERVICE.SetValue("Start", Settings.Constants.Misc.SERVICE_DISABLED_VALUE, RegistryValueKind.DWord)
            Helpers.Logger.WriteRelative("CLEANER", "ok", , 4)
        Catch ex As Exception
            Helpers.Logger.WriteRelative("CLEANER", "fail", , 4)
            Helpers.Logger.WriteRelative("CLEANER", ex.ToString, , 5)

            Return False
        End Try

        Helpers.Logger.WriteRelative("CLEANER", "double checking", , 2)

        Dim lCheckValue As Long

        Try
            Helpers.Logger.WriteRelative("CLEANER", "getting 'Start' value", , 3)
            lCheckValue = Long.Parse(rk_SERVICE.GetValue("Start").ToString)

            If lCheckValue = Settings.Constants.Misc.SERVICE_DISABLED_VALUE Then
                Helpers.Logger.WriteRelative("CLEANER", "ok, because " & lCheckValue.ToString & " = " & Settings.Constants.Misc.SERVICE_DISABLED_VALUE.ToString, , 4)
            Else
                Helpers.Logger.WriteRelative("CLEANER", "fail, because " & lCheckValue.ToString & " != " & Settings.Constants.Misc.SERVICE_DISABLED_VALUE.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                Return False
            End If
        Catch ex As Exception
            Helpers.Logger.WriteRelative("CLEANER", "fail", , 4)
            Helpers.Logger.WriteRelative("CLEANER", ex.ToString, , 5)

            Return False
        End Try

        oService.Disabled = True

        Return True
    End Function

    Private Function _FindWindowsStartupKeys__LOCAL_MACHINE() As Boolean
        Return _FindWindowsStartupKeys(Microsoft.Win32.Registry.LocalMachine)
    End Function

    Private Function _FindWindowsStartupKeys__CURRENT_USER() As Boolean
        Return _FindWindowsStartupKeys(Microsoft.Win32.Registry.CurrentUser)
    End Function

    Private Function _FindWindowsStartupKeys(regROOT As RegistryKey) As Boolean
        Dim rk_STARTUP As RegistryKey
        Dim oStartup As Cleanup_Startup
        Dim sLocation As String, sValueName As String, sCheckValue As String
        Dim bOk As Boolean

        mStartupsKeys = New List(Of String)

        Helpers.Logger.WriteRelative("CLEANER", "finding startup keys ", , 1)
        Helpers.Logger.WriteRelative("CLEANER", "startup location", , 2)

        For Each sLocation In mStartupsLocations
            bOk = True

            Try
                Helpers.Logger.WriteRelative("CLEANER", regROOT.ToString & "\" & sLocation, , 3)
                rk_STARTUP = regROOT.OpenSubKey(sLocation, True)

                If rk_STARTUP Is Nothing Then
                    Helpers.Logger.WriteRelative("CLEANER", "does not exist", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                    bOk = False
                End If
            Catch ex As Exception
                Helpers.Logger.WriteRelative("CLEANER", "fail", , 4)
                Helpers.Logger.WriteRelative("CLEANER", ex.ToString, , 5)

                bOk = False
            End Try

            If bOk Then
                Helpers.Logger.WriteRelative("CLEANER", "searching for startup locations", , 2)
                Helpers.Logger.WriteRelative("CLEANER", "iterating startups", , 3)
                For Each oStartup In mStartups
                    Try
                        Helpers.Logger.WriteRelative("CLEANER", "iterating subkeys", , 4)
#Disable Warning BC42104 ' Variable is used before it has been assigned a value
                        For Each sValueName In rk_STARTUP.GetValueNames
#Enable Warning BC42104 ' Variable is used before it has been assigned a value
                            Try
                                sCheckValue = rk_STARTUP.GetValue(sValueName, cDoesNotExistString).ToString

                                Dim sStartupString As String = oStartup.Name.ToLower

                                If sStartupString.StartsWith(WILDCARD) Then
                                    sStartupString = sStartupString.Replace("*", "")

                                    If sValueName.ToLower.EndsWith(sStartupString) Or sCheckValue.ToLower.EndsWith(sStartupString) Then
                                        oStartup.Found = True
                                        mStartupsKeys.Add(sLocation & "!" & sValueName)
                                        Helpers.Logger.WriteRelative("CLEANER", "found: " & sLocation & "!" & sValueName, , 5)
                                    Else
                                        Helpers.Logger.WriteRelative("CLEANER", "this is not the subkey you're looking for.", , 5)
                                    End If
                                ElseIf sStartupString.eNdsWIth(WILDCARD) Then
                                    sStartupString = sStartupString.Replace("*", "")

                                    If sValueName.ToLower.StartsWith(sStartupString) Or sCheckValue.ToLower.StartsWith(sStartupString) Then
                                        oStartup.Found = True
                                        mStartupsKeys.Add(sLocation & "!" & sValueName)
                                        Helpers.Logger.WriteRelative("CLEANER", "found: " & sLocation & "!" & sValueName, , 5)
                                    Else
                                        Helpers.Logger.WriteRelative("CLEANER", "this is not the subkey you're looking for.", , 5)
                                    End If
                                Else
                                    sStartupString = sStartupString.Replace("*", "")

                                    If InStr(sValueName.ToLower, sStartupString, CompareMethod.Text) > 0 Or InStr(sCheckValue.ToLower, sStartupString, CompareMethod.Text) > 0 Then
                                        oStartup.Found = True
                                        mStartupsKeys.Add(sLocation & "!" & sValueName)
                                        Helpers.Logger.WriteRelative("CLEANER", "found: " & sLocation & "!" & sValueName, , 5)
                                    Else
                                        Helpers.Logger.WriteRelative("CLEANER", "this is not the subkey you're looking for.", , 5)
                                    End If
                                End If
                            Catch ex As Exception
                                Helpers.Logger.WriteRelative("CLEANER", "fail", , 5)
                                Helpers.Logger.WriteRelative("CLEANER", ex.ToString, , 6)
                            End Try
                        Next
                    Catch ex As Exception
                        Helpers.Logger.WriteErrorRelative("CLEANER", "fail", 4)
                        Helpers.Logger.WriteErrorRelative("CLEANER", ex.ToString, 5)
                    End Try
                Next
            End If
        Next

        Return True
    End Function

    Private Function _DisableWindowsStartup() As Boolean
        Dim rk_STARTUP As RegistryKey
        Dim sFullKey As String, sTmp() As String, sKey As String, sValue As String

        Helpers.Logger.WriteRelative("CLEANER", "removing found startup keys", , 1)

        For Each sFullKey In mStartupsKeys
            Helpers.Logger.WriteRelative("CLEANER", "key: " & sFullKey, , 2)

            sTmp = Split(sFullKey, "!", 2)
            If sTmp.Length < 2 Then
                Helpers.Logger.WriteRelative("CLEANER", "invalid key", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 3)
            Else
                sKey = sTmp(0)
                sValue = sTmp(1)

                Helpers.Logger.WriteRelative("CLEANER", "deleting", , 3)
                Try
                    rk_STARTUP = Registry.LocalMachine.OpenSubKey(sKey, True)

                    If rk_STARTUP Is Nothing Then
                        Helpers.Logger.WriteRelative("CLEANER", "not found", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                    Else
                        Try
                            rk_STARTUP.DeleteValue(sValue, True)
                            Helpers.Logger.WriteRelative("CLEANER", "ok", , 4)
                        Catch ex As Exception
                            Helpers.Logger.WriteRelative("CLEANER", "failed", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                            Helpers.Logger.WriteRelative(ex.ToString, , 5)
                        End Try
                    End If
                Catch ex As Exception
                    Helpers.Logger.WriteErrorRelative("CLEANER", "fail", 4)
                    Helpers.Logger.WriteErrorRelative("CLEANER", ex.ToString, 5)

                    Return False
                End Try
            End If
        Next

        Return True
    End Function

    Private Function _DisableRegistryKeys(ByVal oRegistry As Cleanup_Registry) As Boolean
        Helpers.Logger.WriteRelative("CLEANER", "changing " & oRegistry.KeyName & "!" & oRegistry.ValueName, , 1)

        Try
            If oRegistry.Type = RegistryValueKind.MultiString Then
                Registry.SetValue(oRegistry.KeyName, oRegistry.ValueName, oRegistry.Value.Split(";"), oRegistry.Type)
            Else
                Registry.SetValue(oRegistry.KeyName, oRegistry.ValueName, oRegistry.Value, oRegistry.Type)
            End If

            mCountRegistry += 1

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteRelative("CLEANER", "ERROR", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.WriteRelative("CLEANER", "ex.Message=" & ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.WriteRelative("CLEANER", "ex.Source =" & ex.Source, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function

    Private Function _DeleteScheduledTask(_task As TaskScheduler.Task, ScheduleName As String) As Boolean
        Helpers.Logger.WriteMessageRelative("CLEANER", "task '" & _task.Name & "' matches '" & ScheduleName & "'", 2)
        Try
            Helpers.Logger.WriteMessageRelative("CLEANER", "deleting", 3)
            TaskScheduler.TaskService.Instance.RootFolder.DeleteTask(_task.Path)
            Helpers.Logger.WriteMessageRelative("CLEANER", "ok", 4)

            mCountScheduledTasks += 1

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("CLEANER", "failed", 4)
            Helpers.Logger.WriteErrorRelative("CLEANER", ex.Message, 5)
        End Try

        Return False
    End Function
#End Region
#End Region

#Region "private classes"
    Private Class Cleanup_Service
        Private mName As String
        Private mFound As Boolean
        Private mDisabled As Boolean

        Public Sub New()
            mName = ""
            mFound = False
            mDisabled = False
        End Sub

        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Public Property Found() As Boolean
            Get
                Return mFound
            End Get
            Set(ByVal value As Boolean)
                mFound = value
            End Set
        End Property

        Public Property Disabled() As Boolean
            Get
                Return mDisabled
            End Get
            Set(ByVal value As Boolean)
                mDisabled = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Startup
        Private mName As String
        Private mFound As Boolean

        Public Sub New()
            mName = ""
        End Sub

        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Public Property Found() As Boolean
            Get
                Return mFound
            End Get
            Set(ByVal value As Boolean)
                mFound = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Folder
        Private mPath As String
        Private mRecursive As Boolean
        Private mFileNamePattern As String
        Private mMaxFileAge As Integer

        Private mFound As Integer
        Private mDeleted As Integer
        Private mDelayed As Integer

        Private mTotalFileSize As Long
        Private mTotalDeletedFileSize As Long
        Private mTotalDelayedFileSize As Long

        Public Property Path() As String
            Get
                Return mPath
            End Get
            Set(ByVal value As String)
                mPath = value
            End Set
        End Property

        Public Property Recursive() As Boolean
            Get
                Return mRecursive
            End Get
            Set(ByVal value As Boolean)
                mRecursive = value
            End Set
        End Property

        Public Property FileNamePattern() As String
            Get
                Return mFileNamePattern
            End Get
            Set(ByVal value As String)
                mFileNamePattern = value
            End Set
        End Property

        Public Property MaxFileAge() As Integer
            Get
                Return mMaxFileAge
            End Get
            Set(ByVal value As Integer)
                mMaxFileAge = value
            End Set
        End Property

        Public Property Found() As Integer
            Get
                Return mFound
            End Get
            Set(ByVal value As Integer)
                mFound = value
            End Set
        End Property

        Public Property Deleted() As Integer
            Get
                Return mDeleted
            End Get
            Set(ByVal value As Integer)
                mDeleted = value
            End Set
        End Property

        Public Property Delayed() As Integer
            Get
                Return mDelayed
            End Get
            Set(ByVal value As Integer)
                mDelayed = value
            End Set
        End Property

        Public Property TotalFileSize() As Long
            Get
                Return mTotalFileSize
            End Get
            Set(ByVal value As Long)
                mTotalFileSize = value
            End Set
        End Property

        Public Property TotalDeletedFileSize() As Long
            Get
                Return mTotalDeletedFileSize
            End Get
            Set(ByVal value As Long)
                mTotalDeletedFileSize = value
            End Set
        End Property

        Public Property TotalDelayedFileSize() As Long
            Get
                Return mTotalDelayedFileSize
            End Get
            Set(ByVal value As Long)
                mTotalDelayedFileSize = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Registry
        Private mKeyName As String
        Private mValueName As String
        Private mValue As String

        Private mType As RegistryValueKind

        Public Sub New()

        End Sub

        Public Property KeyName() As String
            Get
                Return mKeyName
            End Get
            Set(ByVal value As String)
                mKeyName = value
            End Set
        End Property

        Public Property ValueName() As String
            Get
                Return mValueName
            End Get
            Set(ByVal value As String)
                mValueName = value
            End Set
        End Property

        Public Property Value() As String
            Get
                Return mValue
            End Get
            Set(ByVal _value As String)
                mValue = _value
            End Set
        End Property

        Public Property Type() As RegistryValueKind
            Get
                Return mType
            End Get
            Set(ByVal value As RegistryValueKind)
                mType = value
            End Set
        End Property
    End Class

    Private Class Cleanup_ScheduledTask
        Public Sub New()

        End Sub

        Private mScheduleName As String
        Public Property ScheduleName() As String
            Get
                Return mScheduleName
            End Get
            Set(ByVal value As String)
                mScheduleName = value
            End Set
        End Property

        Private mInvalid As Boolean
        Public Property IsInvalid() As Boolean
            Get
                Return mInvalid
            End Get
            Set(ByVal value As Boolean)
                mInvalid = value
            End Set
        End Property
    End Class
#End Region
End Class
