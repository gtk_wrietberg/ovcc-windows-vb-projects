Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCC_Janitor_Service")>
<Assembly: AssemblyDescription("Scheduled cleaning")>
<Assembly: AssemblyCompany("GuestTek")>
<Assembly: AssemblyProduct("OVCC_Janitor")>
<Assembly: AssemblyCopyright("Copyright ©  2024")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("a79f0c59-b436-43d1-86f8-66df450c9029")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'

<Assembly: AssemblyVersion("1.0.25028.1501")>
<Assembly: AssemblyFileVersion("1.0.25028.1501")>
