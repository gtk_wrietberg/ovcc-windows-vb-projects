﻿Imports System.IO
Imports System.Net
Imports System.Net.Http
Imports System.Text
Imports System.Xml

Public Class Thread__Update
    Private Shared mIsActive As Boolean = False
    Public Shared ReadOnly Property IsActive() As Boolean
        Get
            Return mIsActive
        End Get
    End Property

    Private Shared mIsRunning As Boolean = False
    Public Shared ReadOnly Property IsRunning() As Boolean
        Get
            Return mIsRunning
        End Get
    End Property

    Private Shared mTriggered As Boolean = False
    Public Shared Property Triggered() As Boolean
        Get
            Return mTriggered
        End Get
        Set(ByVal value As Boolean)
            mTriggered = value
        End Set
    End Property

    Private Shared mForced As Boolean = False
    Public Shared Property Forced() As Boolean
        Get
            Return mForced
        End Get
        Set(ByVal value As Boolean)
            mForced = value
        End Set
    End Property

    Private Shared mLoopActive As Boolean = False
    Public Shared Sub [Stop]()
        mLoopActive = False
    End Sub


    Public Shared Sub Main()
        mIsRunning = True
        mLoopActive = True

        Do
            Helpers.Logger.WriteMessage("UPDATE", "Waiting for timer thread", 0)
            Do
                Threading.Thread.Sleep(Settings.Constants.Timeouts.MILLISECONDS_IN_ONE_SECOND)
            Loop While Thread__Timer.IsActive
            Helpers.Logger.WriteMessage("UPDATE", "ok", 1)


            Helpers.Logger.WriteMessage("UPDATE", "Waiting for update trigger", 0)
            Do
                Threading.Thread.Sleep(Settings.Constants.Timeouts.MILLISECONDS_IN_ONE_SECOND)
            Loop While Not (mTriggered Or mForced)
            Helpers.Logger.WriteMessage("UPDATE", "triggered", 0)


            mTriggered = False
            mIsActive = True


            Dim theResponse As HttpWebResponse
            Dim theRequest As HttpWebRequest

            Dim bNotActuallyAnError As Boolean = False
            Dim bSkipLastModifiedCheck As Boolean = False

            Dim lastMod As DateTimeOffset

            Dim hash As New HashFile()


            Try
                Helpers.Logger.WriteMessage("UPDATE", "do we need to download?", 0)

                If mForced Then
                    mForced = False

                    Helpers.Logger.WriteMessage("UPDATE", "yes, forced by registry", 1)
                Else
                    If IO.File.Exists(Settings.Constants.Files.CLEANER_CONFIG_PATH) Then
                        hash.FilePath = Settings.Constants.Files.CLEANER_CONFIG_PATH

                        Helpers.Logger.WriteDebug("UPDATE", "checking config integrity", 1)

                        If Not Settings.XML.Verify() Then
                            Helpers.Logger.WriteWarning("UPDATE", "yes, integrity compromised", 1)

                            bSkipLastModifiedCheck = True 'no need to do the last modification check, we're downloading already
                        Else
                            Helpers.Logger.WriteDebug("UPDATE", "config integrity ok", 2)
                        End If


                        If Not bSkipLastModifiedCheck Then
                            Helpers.Logger.WriteDebug("UPDATE", "checking timestamp", 1)

                            Dim client As HttpClient = New HttpClient()
                            Dim msg As HttpRequestMessage = New HttpRequestMessage(HttpMethod.Head, Settings.Constants.Urls.UPDATE & Settings.Constants.Files.CLEANER_CONFIG_FILE)
                            Dim resp As HttpResponseMessage = client.SendAsync(msg).Result
                            lastMod = resp.Content.Headers.LastModified

                            Dim lLastUpdate As Long = Settings.Registry.Get.Long(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__LastUpdate, 0)

                            Helpers.Logger.WriteDebug("UPDATE", "last modified", 2)
                            Helpers.Logger.WriteDebug("UPDATE", "local", 3)
                            Helpers.Logger.WriteDebug("UPDATE", (New DateTime(lLastUpdate)).ToString, 4)
                            Helpers.Logger.WriteDebug("UPDATE", "online", 3)
                            Helpers.Logger.WriteDebug("UPDATE", (New DateTime(lastMod.Ticks)).ToString, 4)

                            If lastMod.Ticks <= lLastUpdate Then
                                Helpers.Logger.WriteDebug("UPDATE", "timestamp ok", 2)
                                bNotActuallyAnError = True

                                Throw New Exception("I'll throw myself out")
                            End If

                            Helpers.Logger.WriteWarning("UPDATE", "yes, newer config found", 1)
                        End If
                    Else
                        Helpers.Logger.WriteWarning("UPDATE", "yes, file does not exist", 1)
                    End If
                End If


                '----------------------------------------------------------------------------------------------------------------------------
                Dim sServerUrlObfuscated As String = Settings.Registry.Get.String(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ServerUrl, "")
                Dim sServerUrl As String = Helpers.XOrObfuscation_v2.Deobfuscate(sServerUrlObfuscated)

                If sServerUrl.Equals("") Then
                    sServerUrl = Settings.Constants.Urls.UPDATE
                End If
                If Not sServerUrl.EndsWith("/") Then
                    sServerUrl = sServerUrl & "/"
                End If

                Settings.Registry.Set.String(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ServerUrl, Helpers.XOrObfuscation_v2.Obfuscate(sServerUrl))


                Helpers.Logger.WriteMessage("UPDATE", "getting latest config", 0)
                Helpers.Logger.WriteDebug("UPDATE", "url", 1)
                Helpers.Logger.WriteDebug("UPDATE", sServerUrl & Settings.Constants.Files.CLEANER_CONFIG_FILE, 2)


                theRequest = WebRequest.Create(sServerUrl & Settings.Constants.Files.CLEANER_CONFIG_FILE)
                theResponse = theRequest.GetResponse()

                Dim streamResponse As New StreamReader(theResponse.GetResponseStream, Encoding.ASCII)
                Dim sContents As String = streamResponse.ReadToEnd

                theResponse.Close()
                streamResponse.Close()


                Helpers.Logger.WriteMessage("UPDATE", "received data", 1)
                Helpers.Logger.WriteMessage("UPDATE", sContents.Length & " bytes", 2)


                Helpers.Logger.WriteMessage("writing tmp file", 1)
                Helpers.Logger.WriteMessage("UPDATE", Settings.Constants.Files.CLEANER_CONFIG_TMP_PATH, 2)
                IO.File.WriteAllText(Settings.Constants.Files.CLEANER_CONFIG_TMP_PATH, sContents)
                Helpers.Logger.WriteMessage("UPDATE", "ok", 3)


                Helpers.Logger.WriteMessage("UPDATE", "checking config", 1)
                If Settings.XML.Test() Then
                    Helpers.Logger.WriteMessage("UPDATE", "ok", 2)

                    Helpers.Logger.WriteMessage("UPDATE", "writing config", 1)
                    Helpers.Logger.WriteMessage("UPDATE", Settings.Constants.Files.CLEANER_CONFIG_PATH, 2)

                    IO.File.WriteAllText(Settings.Constants.Files.CLEANER_CONFIG_PATH, sContents)
                    IO.File.Delete(Settings.Constants.Files.CLEANER_CONFIG_TMP_PATH)

                    Dim _hash As String = Settings.XML.Hash()
                    Settings.Registry.Set.String(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ConfigHash, _hash)

                    Helpers.Logger.WriteMessage("UPDATE", "ok", 3)


                    Settings.Registry.Set.Long(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__LastUpdate, DateTime.Now.Ticks)
                Else
                    Helpers.Logger.WriteError("UPDATE", "new config is invalid, ignored!", 2)
                End If
            Catch ex As Exception
                If Not bNotActuallyAnError Then 'only log actual errors, not the very genius and not at all lazy throw/goto construction
                    Helpers.Logger.WriteError("UPDATE", "ERROR", 1)
                    Helpers.Logger.WriteError("UPDATE", ex.Message, 2)
                Else
                    Helpers.Logger.WriteMessage("UPDATE", "no", 1)
                End If
            End Try

            mIsActive = False
        Loop While mLoopActive


        mIsRunning = False
    End Sub
End Class
