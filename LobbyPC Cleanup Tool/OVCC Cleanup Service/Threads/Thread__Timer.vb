﻿Imports System.Net.Http

Public Class Thread__Timer
    Private Shared mIsActive As Boolean = False
    Public Shared ReadOnly Property IsActive() As Boolean
        Get
            Return mIsActive
        End Get
    End Property

    Private Shared mIsRunning As Boolean = False
    Public Shared ReadOnly Property IsRunning() As Boolean
        Get
            Return mIsRunning
        End Get
    End Property

    Private Shared mLoopActive As Boolean = False
    Public Shared Sub [Stop]()
        mLoopActive = False
    End Sub


    Private Shared ReadOnly DEFAULT_ActivationType As String = "weekly"
    Private Shared ReadOnly DEFAULT_ActivationHour As Integer = 0

    Private Shared mActivationType As String = ""
    Private Shared mActivationHour As Integer = 0
    Private Shared mLastActivation As Long = 0

    Public Shared Sub Main()
        mIsRunning = True
        mLoopActive = True

        Helpers.Logger.WriteMessage("TIMER", "Timer thread started", 0)

        Dim dNow As DateTime
        Dim dLastActivation As DateTime
        Dim dNextActivation As DateTime
        Dim dDiff As TimeSpan
        Dim bCleaningTime As Boolean = False


        Do
            Helpers.Logger.WriteMessage("TIMER", "Waiting for update thread", 0)
            Do
                Threading.Thread.Sleep(Settings.Constants.Timeouts.MILLISECONDS_IN_ONE_SECOND)
            Loop While Thread__Update.IsActive
            Helpers.Logger.WriteMessage("TIMER", "ok", 1)


            Helpers.Logger.WriteMessage("TIMER", "Timer thread active", 0)
            mIsActive = True

            dNow = DateTime.Now

            mActivationType = Settings.Registry.Get.String(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ActivationType, "(not set))")
            mActivationHour = Settings.Registry.Get.Integer(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ActivationHour, -1)
            mLastActivation = Settings.Registry.Get.Long(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__LastActivation, 0)


            dLastActivation = New DateTime(mLastActivation)

            If Not IsActivationTypeValid() Then
                Helpers.Logger.WriteError("TIMER", "invalid activation type '" & mActivationType & "', defaulting to '" & DEFAULT_ActivationType & "'", 0)
                mActivationType = DEFAULT_ActivationType

                Settings.Registry.Set.String(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ActivationType, DEFAULT_ActivationType)
            End If

            If mActivationHour < 0 Or mActivationHour > 23 Then
                Helpers.Logger.WriteError("TIMER", "invalid activation hour '" & mActivationHour & "', defaulting to '" & DEFAULT_ActivationHour & "'", 0)
                mActivationHour = DEFAULT_ActivationHour

                Settings.Registry.Set.Integer(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ActivationHour, DEFAULT_ActivationHour)
            End If


            '--------------------------------------------------------------------------------------------------------
            If Settings.Registry.Get.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ForcedCleaning, False) Then
                Helpers.Logger.WriteMessage("TIMER", "Forced cleaning!", 1)

                If Settings.Registry.Get.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ForcedUpdate, False) Then
                    Helpers.Logger.WriteMessage("TIMER", "Waiting for forced update", 2)
                Else
                    bCleaningTime = True
                End If
            End If


            If Not bCleaningTime Then
                Helpers.Logger.WriteMessage("TIMER", "Is it time for the " & mActivationType & " cleaning?", 0)


                Helpers.Logger.WriteDebug("TIMER", "Now", 1)
                Helpers.Logger.WriteDebug("TIMER", dNow.ToString, 2)


                Helpers.Logger.WriteDebug("TIMER", "Last clean", 1)
                If mLastActivation > 0 Then
                    Helpers.Logger.WriteDebug("TIMER", dLastActivation.ToString, 2)
                Else
                    Helpers.Logger.WriteDebug("TIMER", "never cleaned yet", 2)
                End If


                Dim dTmpActivation As DateTime
                dTmpActivation = dLastActivation


                Select Case mActivationType
                    Case "daily"
                        Dim dTmp As DateTime = dTmpActivation.AddDays(1)
                        dNextActivation = New DateTime(dTmp.Year, dTmp.Month, dTmp.Day, mActivationHour, 0, 0)
                    Case "weekly"
                        Dim dTmp As DateTime = dTmpActivation.AddDays(7 - dTmpActivation.DayOfWeek)
                        dNextActivation = New DateTime(dTmp.Year, dTmp.Month, dTmp.Day, mActivationHour, 0, 0)
                    Case "monthly"
                        Dim dTmp As DateTime = dTmpActivation.AddMonths(1)
                        dNextActivation = New DateTime(dTmp.Year, dTmp.Month, 1, mActivationHour, 0, 0)
                End Select

                Helpers.Logger.WriteMessage("TIMER", "Next scheduled clean", 1)
                Helpers.Logger.WriteMessage("TIMER", dNextActivation.ToString, 2)


                'Check if it's time to do some cleaning
                dDiff = dNow.Subtract(dNextActivation)


                If dDiff.TotalMinutes > 0 Then
                    'Cleaning time!
                    Helpers.Logger.WriteMessage("TIMER", "Cleaning time!", 1)

                    If Thread__Cleaner.IsActive Then
                        Helpers.Logger.WriteWarning("TIMER", "Cleaner is already running!", 2)
                    Else
                        bCleaningTime = True
                    End If
                Else
                    Helpers.Logger.WriteMessage("TIMER", "no", 1)
                End If
            End If


            If bCleaningTime Then
                bCleaningTime = False

                If Settings.XML.Verify() Then
                    Helpers.Logger.WriteMessage("TIMER", "Starting Cleaner thread", 1)

                    Settings.Registry.Set.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ForcedCleaning, False)


                    StartCleaner()
                    Settings.Registry.Set.Long(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__LastActivation, dNow.Ticks)
                Else
                    Helpers.Logger.WriteMessage("TIMER", "Config appears to be corrupt, triggering update", 1)

                    Thread__Update.Triggered = True
                    Threading.Thread.Sleep(5 * Settings.Constants.Timeouts.MILLISECONDS_IN_ONE_SECOND)

                    mIsActive = False

                    Continue Do
                End If
            End If


            mIsActive = False


            '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            For iInternalLoopCounter As Integer = 1 To Settings.Constants.Timeouts.TIMER_LOOP_DELAY__SECONDS
                If Not mLoopActive Then Exit For

                If Settings.Registry.Get.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ForcedUpdate, False) Then
                    Helpers.Logger.WriteWarning("TIMER", "Forced update!", 1)
                    Settings.Registry.Set.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ForcedUpdate, False)

                    Helpers.Logger.WriteWarning("TIMER", "Pausing timer", 2)
                    mIsActive = False

                    Helpers.Logger.WriteWarning("TIMER", "Triggering update", 2)
                    Thread__Update.Forced = True
                    Thread__Update.Triggered = True


                    Threading.Thread.Sleep(5 * Settings.Constants.Timeouts.MILLISECONDS_IN_ONE_SECOND)

                    Exit For
                End If

                If Settings.Registry.Get.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ForcedCleaning, False) Then
                    Helpers.Logger.WriteWarning("TIMER", "Forced cleaning!", 1)

                    Exit For
                End If


                Threading.Thread.Sleep(Settings.Constants.Timeouts.MILLISECONDS_IN_ONE_SECOND)
            Next
        Loop While mLoopActive

        Helpers.Logger.WriteMessage("TIMER", "No more cleaning time", 0)

        mIsRunning = False
    End Sub

    Private Shared Function IsActivationTypeValid() As Boolean 'lol
        Select Case mActivationType
            Case "daily"
                Return True
            Case "weekly"
                Return True
            Case "monthly"
                Return True
            Case Else
                Return False
        End Select
    End Function

    Private Shared Sub StartCleaner()
        Threads.Cleaner = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Cleaner.Main))
        Threads.Cleaner.Start()
    End Sub
End Class
