﻿Imports System.Xml
Imports Microsoft.Win32.TaskScheduler

Public Class Thread__Cleaner
    Private Shared mIsActive As Boolean
    Public Shared ReadOnly Property IsActive() As Boolean
        Get
            Return mIsActive
        End Get
    End Property

    Private Shared mLoopActive As Boolean = True
    Public Shared Sub [Stop]()
        mLoopActive = False
    End Sub

    '-------------------------------------------
    Public Shared Sub Main()
        mIsActive = True

        Helpers.Logger.WriteMessage("CLEANER", "Cleaner started", 0)
        If Not Settings.XML.Load() Then
            mIsActive = False

            Exit Sub
        End If


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("CLEANER", "Disabling services", 1)

        If Settings.CleanupContainer.CleanUp_Services Then
            Helpers.Logger.WriteMessage("CLEANER", "done", 2)
        Else
            Helpers.Logger.WriteWarning("CLEANER", "done, but with errors", 2)
        End If


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("CLEANER", "Removing startups", 1)

        If Settings.CleanupContainer.CleanUp_Startups__LOCAL_MACHINE Then
            Helpers.Logger.WriteMessage("CLEANER", "done", 2)
        Else
            Helpers.Logger.WriteWarning("CLEANER", "done, but with errors", 2)
        End If


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("CLEANER", "Cleaning folders", 1)

        If Settings.CleanupContainer.CleanUp_Folders() Then
            Helpers.Logger.WriteMessage("CLEANER", "done", 2)
        Else
            Helpers.Logger.WriteWarning("CLEANER", "done, but with errors", 2)
        End If

        Helpers.Logger.WriteMessage("CLEANER", "Bytes found", 3)
        Helpers.Logger.WriteMessage("CLEANER", Settings.CleanupContainer.TotalFileSize.ToString, 4)
        Helpers.Logger.WriteMessage("CLEANER", "Bytes deleted", 3)
        Helpers.Logger.WriteMessage("CLEANER", Settings.CleanupContainer.TotalDeletedFileSize.ToString, 4)
        Helpers.Logger.WriteMessage("CLEANER", "Bytes delayed", 3)
        Helpers.Logger.WriteMessage("CLEANER", Settings.CleanupContainer.TotalDelayedFileSize.ToString, 4)


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("CLEANER", "Cleaning registry", 1)
        If Settings.CleanupContainer.CleanUp_RegistryKeys() Then
            Helpers.Logger.WriteMessage("CLEANER", "done", 2)
        Else
            Helpers.Logger.WriteWarning("CLEANER", "done, but with errors", 2)
        End If


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("CLEANER", "Cleaning scheduled tasks", 1)
        If Settings.CleanupContainer.Cleanup_ScheduledTasks() Then
            Helpers.Logger.WriteMessage("CLEANER", "done", 2)
        Else
            Helpers.Logger.WriteWarning("CLEANER", "done, but with errors", 2)
        End If


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("CLEANER", "Starting Helper task", 1)

        Dim tTF As TaskFolder = TaskService.Instance.GetFolder(Settings.Constants.Helper.ScheduledTask.FOLDER)
        Dim _task As Task = Nothing
        Dim sRandomArgument As String = Helpers.Types.RandomString(8)

        If tTF IsNot Nothing Then
            Dim lTasks As List(Of Task) = tTF.AllTasks.ToList

            For Each _task In lTasks
                If _task.Name.Equals(Settings.Constants.Helper.ScheduledTask.NAME) Then
                    Helpers.Logger.WriteMessage("CLEANER", "task found", 2)
                    Exit For
                End If
            Next
        End If

        If _task IsNot Nothing Then
            Helpers.Logger.WriteMessage("CLEANER", "starting", 2)

            Try
                If _task.Definition.Actions.Count > 0 Then
                    Dim _task_action As Action = _task.Definition.Actions(0)
                    If _task_action.ActionType = TaskActionType.Execute Then
                        Dim _task_execaction As ExecAction = _task_action
                        Dim _task_new_execaction As New ExecAction With {
                            .Path = _task_execaction.Path,
                            .WorkingDirectory = _task_execaction.WorkingDirectory,
                            .Arguments = sRandomArgument
                        }

                        Helpers.Logger.WriteMessage("CLEANER", _task_execaction.Path & " " & sRandomArgument, 3)

                        _task.Definition.Actions(0) = _task_new_execaction
                        _task.RegisterChanges()
                    End If
                End If
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("CLEANER", "EXCEPTION", 1)
                Helpers.Logger.WriteErrorRelative("CLEANER", ex.GetType.Name, 2)
                Helpers.Logger.WriteErrorRelative("CLEANER", ex.Message, 3)
            End Try


            'wait for task to finish
            Dim _task_running As RunningTask = _task.Run()
            Dim _task_running_counter As Integer = 0
            Do
                Helpers.Logger.WriteMessage("CLEANER", "running", 2)
                Helpers.Logger.WriteMessage("CLEANER", "task result code: " & ScheduledTaskHelper.GetResultCodeAsString(_task_running.LastTaskResult), 3)

                _task_running_counter += 1
                Threading.Thread.Sleep(1000)
            Loop While _task_running.IsActive And _task_running_counter < Settings.Constants.Timeouts.HELPER_TASK


            'wait for Helper to finish
            Dim bProcessRunning As Boolean = False
            Dim _process_running_counter As Integer = 0
            Do
                Helpers.Logger.WriteMessage("CLEANER", "waiting", 2)

                bProcessRunning = Helpers.Processes.IsProcessRunning(Settings.Constants.Helper.FILE)

                _process_running_counter += 1
                Threading.Thread.Sleep(1000)
            Loop While bProcessRunning And _process_running_counter < Settings.Constants.Timeouts.HELPER_PROCESS


            'see if there is a logfile from the helper
            Dim sHelperLogfile As String = IO.Path.Combine(Settings.Constants.Helper.LOG_PATH, Settings.Constants.Helper.LOG_NAME & "." & sRandomArgument)

            Helpers.Logger.WriteMessage("CLEANER", "helper log", 2)
            Helpers.Logger.WriteMessage("CLEANER", sHelperLogfile, 3)

            If IO.File.Exists(sHelperLogfile) Then
                Dim sHelperLogfileContents As String = My.Computer.FileSystem.ReadAllText(sHelperLogfile)

                Helpers.Logger.WriteWithoutDate(" ")
                Helpers.Logger.WriteWithoutDate(New String("=", 50))
                Helpers.Logger.WriteWithoutDate(" ")
                Helpers.Logger.WriteWithoutDate(sHelperLogfileContents)
                Helpers.Logger.WriteWithoutDate(" ")
                Helpers.Logger.WriteWithoutDate(New String("=", 50))
                Helpers.Logger.WriteWithoutDate(" ")
            Else
                Helpers.Logger.WriteError("CLEANER", "not found!", 4)
            End If
        Else
            Helpers.Logger.WriteError("CLEANER", "Task '" & Settings.Constants.Helper.ScheduledTask.PATH & "' not found!", 1)
        End If


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("CLEANER", "done", 1)
        mIsActive = False
    End Sub
End Class
