﻿Public Class ServiceMain
    Protected Overrides Sub OnStart(ByVal args() As String)
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Debugging = Settings.Registry.Get.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__Debug, False)
        Helpers.Logger.Write(New String("*", 50), 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.Write("started", 0)


        Helpers.Logger.Write("Zipping old logs", 0)
        LoggerZipper.ZipLogs()


        '---------------------------------------------------------
        StartThreads()
    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()
    End Sub


    '------------------------
    Private Sub StartThreads()
        Threads.Update = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Update.Main))
        Threads.Update.Start()
        Thread__Update.Triggered = True

        Threading.Thread.Sleep(5 * Settings.Constants.Timeouts.MILLISECONDS_IN_ONE_SECOND)

        Threads.Timer = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Timer.Main))
        Threads.Timer.Start()
    End Sub

    Private Sub KillThreads()
        Try
            Thread__Update.Stop()
            Thread__Timer.Stop()
        Catch ex As Exception

        End Try
    End Sub
End Class
