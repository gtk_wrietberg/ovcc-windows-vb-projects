Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Xml

Public Class Report
    Private mXmlReport As XmlDocument

    Public ReadOnly Property Xml() As String
        Get
            Return mXmlReport.InnerXml
        End Get
    End Property

    Public Function CreateReportTemplate() As Boolean
        Dim xmlRootNode As XmlElement
        Dim xmlNode As XmlElement

        mXmlReport = New XmlDocument

        Dim xmlDec As XmlDeclaration = mXmlReport.CreateXmlDeclaration("1.0", Nothing, Nothing)
        mXmlReport.AppendChild(xmlDec)

        xmlRootNode = mXmlReport.CreateElement("cleanuptool-report")
        mXmlReport.AppendChild(xmlRootNode)

        xmlNode = mXmlReport.CreateElement("lobbypc-name")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("services")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("startups")
        xmlRootNode.AppendChild(xmlNode)

        xmlNode = mXmlReport.CreateElement("files-and-folders")
        xmlRootNode.AppendChild(xmlNode)

        Return True
    End Function

    Public Sub AddComputerName(ByVal sName As String)
        Dim xmlNode As XmlElement

        xmlNode = mXmlReport.SelectSingleNode("/cleanuptool-report/lobbypc-name")
        xmlNode.AppendChild(mXmlReport.CreateTextNode(sName))
    End Sub

    Public Sub AddService(ByVal sName As String, ByVal bFound As Boolean, ByVal bDisabled As Boolean)
        Dim xmlNode As XmlElement, xmlSubNode As XmlElement
        Dim xmlAttrib As XmlAttribute

        xmlNode = mXmlReport.SelectSingleNode("/cleanuptool-report/services")

        xmlSubNode = mXmlReport.CreateElement("service")

        xmlAttrib = mXmlReport.CreateAttribute("name")
        xmlAttrib.Value = sName
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlAttrib = mXmlReport.CreateAttribute("found")
        xmlAttrib.Value = bFound.ToString.ToLower
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlAttrib = mXmlReport.CreateAttribute("disabled")
        xmlAttrib.Value = bDisabled.ToString.ToLower
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlNode.AppendChild(xmlSubNode)
    End Sub

    Public Sub AddStartup(ByVal sName As String, ByVal bFound As Boolean, ByVal bDeleted As Boolean)
        Dim xmlNode As XmlElement, xmlSubNode As XmlElement
        Dim xmlAttrib As XmlAttribute

        xmlNode = mXmlReport.SelectSingleNode("/cleanuptool-report/startups")

        xmlSubNode = mXmlReport.CreateElement("startup")

        xmlAttrib = mXmlReport.CreateAttribute("name")
        xmlAttrib.Value = sName
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlAttrib = mXmlReport.CreateAttribute("found")
        xmlAttrib.Value = bFound.ToString.ToLower
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlAttrib = mXmlReport.CreateAttribute("deleted")
        xmlAttrib.Value = bDeleted.ToString.ToLower
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlNode.AppendChild(xmlSubNode)
    End Sub

    Public Sub AddFolder(ByVal sName As String, ByVal iFound As Integer, ByVal iDeleted As Integer, ByVal iDelayed As Integer)
        Dim xmlNode As XmlElement, xmlSubNode As XmlElement
        Dim xmlAttrib As XmlAttribute

        xmlNode = mXmlReport.SelectSingleNode("/cleanuptool-report/files-and-folders")

        xmlSubNode = mXmlReport.CreateElement("location")

        xmlAttrib = mXmlReport.CreateAttribute("path")
        xmlAttrib.Value = sName
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlAttrib = mXmlReport.CreateAttribute("found")
        xmlAttrib.Value = iFound.ToString
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlAttrib = mXmlReport.CreateAttribute("deleted")
        xmlAttrib.Value = iDeleted.ToString
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlAttrib = mXmlReport.CreateAttribute("delayed")
        xmlAttrib.Value = iDelayed.ToString
        xmlSubNode.Attributes.Append(xmlAttrib)

        xmlNode.AppendChild(xmlSubNode)
    End Sub

    Public Function Send() As Boolean
        Dim request As WebRequest = WebRequest.Create(cReportUrl)

        request.Method = "POST"

        Dim postData As String
        postData = mXmlReport.InnerXml

        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)

        request.ContentType = "application/x-www-form-urlencoded"
        request.ContentLength = byteArray.Length

        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()

        Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)

        dataStream = response.GetResponseStream()

        Dim reader As New StreamReader(dataStream)
        Dim responseFromServer As String = reader.ReadToEnd()

        Console.WriteLine(responseFromServer)

        reader.Close()
        dataStream.Close()
        response.Close()
    End Function
End Class
