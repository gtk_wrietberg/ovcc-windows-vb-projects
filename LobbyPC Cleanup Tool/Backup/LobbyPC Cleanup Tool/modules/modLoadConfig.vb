Imports System.IO
Imports System.Xml

Module modLoadConfig
    Public Function LoadConfiguration() As Boolean
        Try
            Dim xmlDoc As XmlDocument
            Dim xmlNodelist As XmlNodeList
            Dim xmlNodelist2 As XmlNodeList
            Dim xmlNode As XmlNode
            Dim xmlNode2 As XmlNode
            Dim sTmp As String
            Dim sTmpName As String, bTmpRecursive As Boolean, sTmpPattern As String, iTmpMaxAge As Integer
            Dim sKeyName As String, sValueName As String, sValue As String, sType As String, oType As Microsoft.Win32.RegistryValueKind

            xmlDoc = New XmlDocument()
            oCleanup = New Cleanup

            '------------
            oLogger.WriteToLogRelative("loading config XML file", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            xmlDoc.Load("LobbyPC_Cleanup_Tool_config.xml")
            oLogger.WriteToLogRelative("ok", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            '------------
            oLogger.WriteToLogRelative("loading services", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            xmlNodelist = xmlDoc.SelectNodes("/cleanuptool/config/services/service")
            For Each xmlNode In xmlNodelist
                sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                oLogger.WriteToLogRelative(sTmpName, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                oCleanup.AddService(sTmpName)
            Next

            '------------
            oLogger.WriteToLogRelative("loading startup apps", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            xmlNodelist = xmlDoc.SelectNodes("/cleanuptool/config/startups/application")
            For Each xmlNode In xmlNodelist
                sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                oLogger.WriteToLogRelative(sTmpName, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                oCleanup.AddStartup(sTmpName)
            Next

            '------------
            oLogger.WriteToLogRelative("loading files and folders", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            xmlNodelist = xmlDoc.SelectNodes("/cleanuptool/config/files-and-folders/location")
            For Each xmlNode In xmlNodelist
                sTmpName = xmlNode.Attributes.GetNamedItem("path").Value
                oLogger.WriteToLogRelative(sTmpName, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

                oLogger.WriteToLogRelative("params", Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)

                sTmp = xmlNode.ChildNodes.Item(0).InnerText
                If sTmp = "yes" Then
                    bTmpRecursive = True
                Else
                    bTmpRecursive = False
                End If
                oLogger.WriteToLogRelative("recursive: " & bTmpRecursive.ToString, Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

                sTmpPattern = xmlNode.ChildNodes.Item(1).InnerText
                oLogger.WriteToLogRelative("pattern: " & sTmpPattern, Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

                sTmp = xmlNode.ChildNodes.Item(2).InnerText
                If IsNumeric(sTmp) Then
                    Try
                        iTmpMaxAge = Integer.Parse(sTmp)
                    Catch ex As Exception
                        iTmpMaxAge = 0
                    End Try
                Else
                    iTmpMaxAge = 0
                End If
                oLogger.WriteToLogRelative("max age: " & iTmpMaxAge.ToString, Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

                oCleanup.AddLocation(sTmpName, bTmpRecursive, sTmpPattern, iTmpMaxAge)
            Next

            '------------
            oLogger.WriteToLogRelative("loading registry keys", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            xmlNodelist = xmlDoc.SelectNodes("/cleanuptool/config/registry/key")
            For Each xmlNode In xmlNodelist
                sKeyName = xmlNode.Attributes.GetNamedItem("name").Value

                xmlNodelist2 = xmlNode.SelectNodes("value")
                For Each xmlNode2 In xmlNodelist2
                    sValueName = xmlNode2.Attributes.GetNamedItem("name").Value
                    sType = xmlNode2.Attributes.GetNamedItem("type").Value
                    sValue = xmlNode2.InnerText

                    Select Case sType.ToLower
                        Case "DWord".ToLower
                            oType = Microsoft.Win32.RegistryValueKind.DWord
                        Case "MultiString".ToLower
                            oType = Microsoft.Win32.RegistryValueKind.MultiString
                        Case Else
                            sType = "String"
                            oType = Microsoft.Win32.RegistryValueKind.String
                    End Select

                    oLogger.WriteToLogRelative(sKeyName & "!" & sValueName & " = " & sValue & " (" & sType & ")", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                    oCleanup.AddRegistry(sKeyName, sValueName, sValue, oType)
                Next
            Next

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function
End Module
