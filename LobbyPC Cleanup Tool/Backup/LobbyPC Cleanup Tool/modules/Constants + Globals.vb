Module Constants___Globals
    'Constants
    Public Const cServiceDisabledValue As Long = 4&


    Public Const cReportUrl As String = "http://eureport.ibahn.com/LPC_Cleanup_Tool/report.asp"

    Public Enum ExitCodes As Integer
        Ok = 0
        SafetyNotDisabled = 1
        ConfigurationError = 2
        MiscError = 3
    End Enum

    'Globals. Globals bad, but me are lazy
    Public oCleanup As Cleanup
    Public oLogger As Logger
    Public oReport As Report
    Public oComputerName As ComputerName
End Module
