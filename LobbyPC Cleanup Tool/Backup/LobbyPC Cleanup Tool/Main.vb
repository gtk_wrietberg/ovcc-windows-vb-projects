Module Main
    Public Sub Main()
        Dim bSafetyOff As Boolean = False

        oLogger = New Logger
        oComputerName = New ComputerName
        oCleanup = New Cleanup
        oReport = New Report

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLogWithoutDate(New String("*", 100))
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg = "--safety-off" Then
                bSafetyOff = True
            End If
        Next

        If Not bSafetyOff Then
            oLogger.WriteToLog("Safety not disabled", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
            ExitApplication(ExitCodes.SafetyNotDisabled)

            Exit Sub
        End If


        oLogger.WriteToLog("Loading configuration", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        If Not LoadConfiguration() Then
            oLogger.WriteToLog("CONFIG LOADING FAILED", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            ExitApplication(ExitCodes.ConfigurationError)
            Exit Sub
        End If

        oLogger.WriteToLog(New String("-", 20), , 0)

        CleaningDepartement()
    End Sub

    Private Sub CleaningDepartement()
        oLogger.WriteToLog("Disabling services", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)

        If oCleanup.CleanUp_Services Then
            oLogger.WriteToLog("done", , 1)
        Else
            oLogger.WriteToLog("done, but with errors", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If

        '------------
        oLogger.WriteToLog("Removing startups", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)

        If oCleanup.CleanUp_Startups Then
            oLogger.WriteToLog("done", , 1)
        Else
            oLogger.WriteToLog("done, but with errors", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If

        '------------
        oLogger.WriteToLog("Cleaning folders", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        oCleanup.CleanUp_Locations()

        oLogger.WriteToLog("Bytes found", , 1)
        oLogger.WriteToLog(oCleanup.TotalFileSize, , 2)
        oLogger.WriteToLog("Bytes deleted", , 1)
        oLogger.WriteToLog(oCleanup.TotalDeletedFileSize, , 2)
        oLogger.WriteToLog("Bytes delayed", , 1)
        oLogger.WriteToLog(oCleanup.TotalDelayedFileSize, , 2)

        '------------
        oLogger.WriteToLog("Cleaning registry", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        oCleanup.CleanUp_RegistryKeys()

        '------------
        'If bTmp Then
        '    oLogger.WriteToLog("done", , 1)
        'Else
        '    oLogger.WriteToLog("done, but with errors", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        'End If
        oLogger.WriteToLog("done", , 1)

        ExitApplication(ExitCodes.Ok)
    End Sub

    'Private Sub Reporting()
    '    Dim sName As String = "", bFound As Boolean, bDisabled As Boolean, bDeleted As Boolean
    '    Dim sPath As String = "", iFound As Integer, iDeleted As Integer, iDelayed As Integer

    '    oLogger.WriteToLog("Send the report", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
    '    oLogger.WriteToLog("creating report", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

    '    oReport.CreateReportTemplate()

    '    oLogger.WriteToLog("adding services results", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
    '    Do While oCleanup.GetNextService(sName, bFound, bDisabled)
    '        oReport.AddService(sName, bFound, bDisabled)
    '    Loop

    '    oLogger.WriteToLog("adding startups results", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
    '    Do While oCleanup.GetNextStartup(sName, bFound, bDeleted)
    '        oReport.AddStartup(sName, bFound, bDeleted)
    '    Loop

    '    oLogger.WriteToLog("adding locations results", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
    '    Do While oCleanup.GetNextLocation(sPath, iFound, iDeleted, iDelayed)
    '        oReport.AddFolder(sPath, iFound, iDeleted, iDelayed)
    '    Loop

    '    oLogger.WriteToLogWithoutDate(" ")
    '    oLogger.WriteToLogWithoutDate(" ")
    '    oLogger.WriteToLogWithoutDate(oReport.Xml)
    'End Sub

    Public Sub ExitApplication(ByVal iExitCode As ExitCodes)
        oLogger.WriteToLog("Exiting...", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        oLogger.WriteToLog("exit code: " & iExitCode & " (" & iExitCode.ToString & ")", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLog("bye", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLogWithoutDate(New String("=", 100))

        System.Environment.Exit(iExitCode)
    End Sub
End Module
