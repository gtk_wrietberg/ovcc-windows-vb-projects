﻿Imports System.Xml

Module modMain
    Public Sub Main()
        'command line params
        Dim sArgs As String() = Environment.GetCommandLineArgs()
        Dim sRandomArgument As String = ""

        If sArgs.Count > 1 Then
            sRandomArgument = sArgs(1)
        End If


        Helpers.Logger.InitialiseLogger(Settings.Constants.Helper.LOG_PATH, False)
        Helpers.Logger.LogFileName = Settings.Constants.Helper.LOG_NAME
        Helpers.Logger.LogExtension = sRandomArgument

        Helpers.Logger.Debugging = Settings.Registry.Get.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__Debug, False)
        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.Write("started")


        'load cleaning settings
        LoadSettings()


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Removing startups", 1)

        If Settings.CleanupContainer.CleanUp_Startups__CURRENT_USER Then
            Helpers.Logger.WriteMessage("done", 2)
        Else
            Helpers.Logger.WriteWarning("done, but with errors", 2)
        End If


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("CLEANER", "Cleaning folders", 1)

        If Settings.CleanupContainer.CleanUp_Folders() Then
            Helpers.Logger.WriteMessage("CLEANER", "done", 2)
        Else
            Helpers.Logger.WriteWarning("CLEANER", "done, but with errors", 2)
        End If

        Helpers.Logger.WriteMessage("CLEANER", "Bytes found", 3)
        Helpers.Logger.WriteMessage("CLEANER", Settings.CleanupContainer.TotalFileSize.ToString, 4)
        Helpers.Logger.WriteMessage("CLEANER", "Bytes deleted", 3)
        Helpers.Logger.WriteMessage("CLEANER", Settings.CleanupContainer.TotalDeletedFileSize.ToString, 4)
        Helpers.Logger.WriteMessage("CLEANER", "Bytes delayed", 3)
        Helpers.Logger.WriteMessage("CLEANER", Settings.CleanupContainer.TotalDelayedFileSize.ToString, 4)


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Cleaning registry", 1)
        If Settings.CleanupContainer.CleanUp_RegistryKeys() Then
            Helpers.Logger.WriteMessage("done", 2)
        Else
            Helpers.Logger.WriteWarning("done, but with errors", 2)
        End If


        '---------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("done", 1)
    End Sub


    Private Function LoadSettings() As Boolean
        Helpers.Logger.WriteMessageRelative("MAIN", "loading cleaner settings", 1)

        Try
            Dim xmlDoc As XmlDocument
            Dim xmlNodelist As XmlNodeList
            Dim xmlNodelist2 As XmlNodeList
            Dim xmlNode As XmlNode
            Dim xmlNode2 As XmlNode
            Dim sTmp As String
            Dim sTmpName As String, bTmpRecursive As Boolean, sTmpPattern As String, iTmpMaxAge As Integer
            Dim sKeyName As String, sValueName As String, sValue As String, sType As String, oType As Microsoft.Win32.RegistryValueKind

            xmlDoc = New XmlDocument()
            Settings.CleanupContainer = New Cleanup


            '------------------------------------------------------------------------------------------------------------------------------------
            Helpers.Logger.WriteMessageRelative("loading config XML file", 2)
            Helpers.Logger.WriteMessageRelative(Settings.Constants.Files.CLEANER_CONFIG_PATH, 3)
            xmlDoc.Load(Settings.Constants.Files.CLEANER_CONFIG_PATH)
            Helpers.Logger.WriteMessageRelative("ok", 4)


            '------------------------------------------------------------------------------------------------------------------------------------
            Helpers.Logger.WriteMessageRelative("loading startup apps", 3)
            xmlNodelist = xmlDoc.SelectNodes(Settings.Constants.Config.ROOT & Settings.Constants.Config.USER & "/startups/startup")
            For Each xmlNode In xmlNodelist
                sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                Helpers.Logger.WriteMessageRelative(sTmpName, 4)
                Settings.CleanupContainer.AddStartup(sTmpName)
            Next


            '------------------------------------------------------------------------------------------------------------------------------------
            Helpers.Logger.WriteMessageRelative("SETTINGS", "files and folders", 3)
            xmlNodelist = xmlDoc.SelectNodes(Settings.Constants.Config.ROOT & Settings.Constants.Config.USER & "/files-and-folders/location")
            For Each xmlNode In xmlNodelist
                sTmpName = xmlNode.Attributes.GetNamedItem("path").Value

                Helpers.Logger.WriteDebugRelative("SETTINGS", sTmpName, 4)
                Helpers.Logger.WriteDebugRelative("SETTINGS", "params", 5)

                sTmp = xmlNode.ChildNodes.Item(0).InnerText
                If sTmp = "yes" Then
                    bTmpRecursive = True
                Else
                    bTmpRecursive = False
                End If
                Helpers.Logger.WriteDebugRelative("SETTINGS", "recursive", 6)
                Helpers.Logger.WriteDebugRelative("SETTINGS", bTmpRecursive.ToString, 7)

                sTmpPattern = xmlNode.ChildNodes.Item(1).InnerText
                Helpers.Logger.WriteDebugRelative("SETTINGS", "pattern", 6)
                Helpers.Logger.WriteDebugRelative("SETTINGS", sTmpPattern, 7)

                sTmp = xmlNode.ChildNodes.Item(2).InnerText
                If IsNumeric(sTmp) Then
                    Try
                        iTmpMaxAge = Integer.Parse(sTmp)
                    Catch ex As Exception
                        iTmpMaxAge = 0
                    End Try
                Else
                    iTmpMaxAge = 0
                End If
                Helpers.Logger.WriteDebugRelative("SETTINGS", "max age", 6)
                Helpers.Logger.WriteDebugRelative("SETTINGS", iTmpMaxAge.ToString, 7)

                Settings.CleanupContainer.AddFolder(sTmpName, bTmpRecursive, sTmpPattern, iTmpMaxAge)
            Next


            '------------------------------------------------------------------------------------------------------------------------------------
            Helpers.Logger.WriteMessageRelative("loading registry keys", 3)
            xmlNodelist = xmlDoc.SelectNodes(Settings.Constants.Config.ROOT & Settings.Constants.Config.USER & "/registry/key")
            For Each xmlNode In xmlNodelist
                sKeyName = xmlNode.Attributes.GetNamedItem("name").Value

                xmlNodelist2 = xmlNode.SelectNodes("value")
                For Each xmlNode2 In xmlNodelist2
                    sValueName = xmlNode2.Attributes.GetNamedItem("name").Value
                    sType = xmlNode2.Attributes.GetNamedItem("type").Value
                    sValue = xmlNode2.InnerText

                    Select Case sType.ToLower
                        Case "DWord".ToLower
                            oType = Microsoft.Win32.RegistryValueKind.DWord
                        Case "MultiString".ToLower
                            oType = Microsoft.Win32.RegistryValueKind.MultiString
                        Case Else
                            sType = "String"
                            oType = Microsoft.Win32.RegistryValueKind.String
                    End Select

                    Helpers.Logger.WriteMessageRelative("key", 4)
                    Helpers.Logger.WriteMessageRelative(sKeyName, 5)
                    Helpers.Logger.WriteMessageRelative("value name", 4)
                    Helpers.Logger.WriteMessageRelative(sValueName, 5)
                    Helpers.Logger.WriteMessageRelative("value", 4)
                    Helpers.Logger.WriteMessageRelative(sValue, 5)
                    Helpers.Logger.WriteMessageRelative("type", 4)
                    Helpers.Logger.WriteMessageRelative(sType, 5)

                    Settings.CleanupContainer.AddRegistry(sKeyName, sValueName, sValue, oType)
                Next
            Next

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("ERROR", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            Return False
        End Try
    End Function
End Module
