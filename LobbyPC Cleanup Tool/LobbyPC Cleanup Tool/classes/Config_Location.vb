Public Class Config_Location
    Private mPath As String
    Private mRecursive As Boolean
    Private mFileNamePattern As String
    Private mMaxFileAge As Integer

    Public Property Path() As String
        Get
            Return mPath
        End Get
        Set(ByVal value As String)
            mPath = value
        End Set
    End Property

    Public Property Recursive() As Boolean
        Get
            Return mRecursive
        End Get
        Set(ByVal value As Boolean)
            mRecursive = value
        End Set
    End Property

    Public Property FileNamePattern() As String
        Get
            Return mFileNamePattern
        End Get
        Set(ByVal value As String)
            mFileNamePattern = value
        End Set
    End Property

    Public Property MaxFileAge() As Integer
        Get
            Return mMaxFileAge
        End Get
        Set(ByVal value As Integer)
            mMaxFileAge = value
        End Set
    End Property

End Class
