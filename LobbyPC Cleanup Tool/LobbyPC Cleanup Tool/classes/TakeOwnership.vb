﻿Public Class TakeOwnership
    Private Shared ReadOnly cTakeOwnExe As String = "takeown.exe"

    Public Shared Function Directory(sDirectory As String, Optional Recursive As Boolean = True) As Boolean
        Return _TakeOwn(sDirectory, Recursive)
    End Function

    Public Shared Function File(sFile As String) As Integer
        Return _TakeOwn(sFile, False)
    End Function

    Private Shared Function _TakeOwn(sPath As String, bRecursive As Boolean) As Integer
        Dim arguments As String = ""
        Dim iExitCode As Integer = -666
        Dim myProcess As Process = Nothing

        If bRecursive Then
            arguments = String.Format("/F ""{0}"" /R /D Y", sPath)
        Else
            arguments = String.Format("/f ""{0}""", sPath)
        End If

        Try
            myProcess = Process.Start(cTakeOwnExe, arguments)

            Do
                'loop loop loop loop loop
            Loop While Not myProcess.WaitForExit(500)

            iExitCode = myProcess.ExitCode
        Finally
            If Not myProcess Is Nothing Then
                myProcess.Close()
            End If
        End Try

        Return iExitCode
    End Function
End Class
