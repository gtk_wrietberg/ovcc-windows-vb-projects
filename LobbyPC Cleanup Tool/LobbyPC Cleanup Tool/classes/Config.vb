Public Class Config
    Private mServices As New List(Of Config_Service)
    Private mStartups As New List(Of Config_Startup)
    Private mLocations As New List(Of Config_Location)

    Public Sub New()
        mServices = New List(Of Config_Service)
        mStartups = New List(Of Config_Startup)
        mLocations = New List(Of Config_Location)
    End Sub

    Public Sub AddService(ByVal sServiceName As String)
        Dim oConfig_Service As New Config_Service

        oConfig_Service.Name = sServiceName

        mServices.Add(oConfig_Service)
    End Sub

    Public Sub AddStartup(ByVal sStartupName As String)
        Dim oConfig_Startup As New Config_Startup

        oConfig_Startup.Name = sStartupName

        mStartups.Add(oConfig_Startup)
    End Sub

    Public Sub AddLocation(ByVal sPath As String, ByVal bRecursive As Boolean, ByVal sFileNamePattern As String, ByVal iMaxFileAge As Integer)
        Dim oConfig_Location As New Config_Location

        oConfig_Location.Path = sPath
        oConfig_Location.Recursive = bRecursive
        oConfig_Location.FileNamePattern = sFileNamePattern
        oConfig_Location.MaxFileAge = iMaxFileAge

        mLocations.Add(oConfig_Location)
    End Sub
End Class
