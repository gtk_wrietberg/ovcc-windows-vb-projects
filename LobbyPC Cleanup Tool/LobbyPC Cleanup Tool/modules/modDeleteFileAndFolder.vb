Imports System.IO

Module modDeleteFileAndFolder
    Private ReadOnly MOVEFILE_DELAY_UNTIL_REBOOT As Integer = 4

    <Runtime.InteropServices.DllImport("kernel32", SetLastError:=True)> _
    Private Function MoveFileEx(ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal dwFlags As Integer) As Boolean
    End Function

    Public Enum DeleteReturnValue
        FileDeleted = 0
        FileDeletedNextBoot = 1
        FileNotDeletedError = 2
    End Enum

    Public Function DeleteFolder(ByVal sFolder As String) As DeleteReturnValue
        Return _DeleteFolder(sFolder)
    End Function

    Public Function DeleteFile(ByVal sFile As String) As DeleteReturnValue
        Return _DeleteFile(sFile)
    End Function

    Private Function _DeleteFolder(ByVal sFolder As String) As DeleteReturnValue
        Dim iRet As DeleteReturnValue = DeleteReturnValue.FileNotDeletedError

        oLogger.WriteToLogRelative("deleting folder: " & sFolder, , 1)
        If Directory.Exists(sFolder) Then
            Try
                _SetFileAttributesToNormal(sFolder)
                Directory.Delete(sFolder, True)
            Catch ex As Exception
                oLogger.WriteToLogRelative("error", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End Try
            If Directory.Exists(sFolder) Then
                oLogger.WriteToLogRelative("still there", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                oLogger.WriteToLogRelative("marking for delete on reboot", , 2)
                If _DeleteOnReboot(sFolder) Then
                    oLogger.WriteToLogRelative("ok", , 3)
                    iRet = DeleteReturnValue.FileDeletedNextBoot
                Else
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                End If

            Else
                oLogger.WriteToLogRelative("gone", , 2)
                iRet = DeleteReturnValue.FileDeleted
            End If
        Else
            oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If

        Return iRet
    End Function

    Private Function _DeleteFile(ByVal sFile As String) As DeleteReturnValue
        Dim iRet As DeleteReturnValue = DeleteReturnValue.FileNotDeletedError

        oLogger.WriteToLogRelative("deleting file: " & sFile, , 1)
        If File.Exists(sFile) Then
            Try
                _SetFileAttributesToNormal(sFile)
                File.Delete(sFile)
            Catch ex As Exception
                oLogger.WriteToLogRelative("error", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End Try
            If File.Exists(sFile) Then
                oLogger.WriteToLogRelative("still there", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                oLogger.WriteToLogRelative("marking for delete on reboot", , 2)
                If _DeleteOnReboot(sFile) Then
                    oLogger.WriteToLogRelative("ok", , 3)
                    iRet = DeleteReturnValue.FileDeletedNextBoot
                Else
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                End If

            Else
                oLogger.WriteToLogRelative("gone", , 2)
                iRet = DeleteReturnValue.FileDeleted
            End If
        Else
            oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If

        Return iRet
    End Function

    Private Sub _SetFileAttributesToNormal(ByVal sPath As String)
        If Directory.Exists(sPath) Then
            Dim sSubFolders() As String = Directory.GetDirectories(sPath)
            Dim sSubFolder As String

            For Each sSubFolder In sSubFolders
                Try
                    File.SetAttributes(sSubFolder, FileAttributes.Normal)
                Catch ex As Exception

                End Try

                _SetFileAttributesToNormal(sSubFolder)
            Next

            Dim sFiles() As String = Directory.GetFiles(sPath)
            Dim sFile As String

            For Each sFile In sFiles
                Try
                    File.SetAttributes(sFile, FileAttributes.Normal)
                Catch ex As Exception

                End Try
            Next
        ElseIf File.Exists(sPath) Then
            Try
                File.SetAttributes(sPath, FileAttributes.Normal)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Public Function _DeleteOnReboot(ByVal sPath As String) As Boolean
        Dim bReturn As Boolean

        Try
            bReturn = MoveFileEx(sPath, Nothing, MOVEFILE_DELAY_UNTIL_REBOOT)
        Catch ex As Exception
            bReturn = False
        End Try

        Return bReturn
    End Function
End Module
