Imports Microsoft.Win32

Module modCleanRegistry
    Public Sub CleanRegistry()
        oLogger.WriteToLog("disabling MS Security Centre notifications", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Security Center!AntiVirusDisableNotify", , 2)
        oLogger.WriteToLog("value: 1", , 2)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Security Center!FirewallDisableNotify", , 2)
        oLogger.WriteToLog("value: 1", , 2)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Security Center!UpdatesDisableNotify", , 2)
        oLogger.WriteToLog("value: 1", , 2)
        Try
            Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Security Center", "AntiVirusDisableNotify", 1)
            Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Security Center", "FirewallDisableNotify", 1)
            Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Security Center", "UpdatesDisableNotify", 1)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        oLogger.WriteToLog("setting page file size", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management!PagingFiles", , 2)
        oLogger.WriteToLog("value: C:\pagefile.sys 2072 2072 (REG_MULTI_SZ)", , 2)
        Try
            Dim sValuePageFile As String() = {"C:\pagefile.sys 2072 2072"}

            Registry.SetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management", "PagingFiles", sValuePageFile, RegistryValueKind.MultiString)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        oLogger.WriteToLog("disabling System Restore", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore!DisableSR", , 2)
        oLogger.WriteToLog("value: 1", , 2)
        Try
            Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\SystemRestore", "DisableSR", 1)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub
End Module
