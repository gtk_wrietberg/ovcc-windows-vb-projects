Module Constants___Globals
    'Constants
    Public Const cServiceDisabledValue As Long = 4&

    Public Const cConfigFileName As String = "OVCC_Cleanup_Tool_config.xml"

    'Public Const cReportUrl As String = "http://eureport.ibahn.com/LPC_Cleanup_Tool/report.asp"

    Public Enum ExitCodes As Integer
        Ok = 0
        SafetyNotDisabled = 1
        ConfigurationError = 2
        MiscError = 3
    End Enum

    'Globals
    Public oCleanup As Cleanup
    Public oLogger As Logger
    'Public oReport As Report
    Public oComputerName As ComputerName

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public Sub InitGlobals()
        oComputerName = New ComputerName
        oCleanup = New Cleanup
        'oReport = New Report


        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"


        Try
            System.IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
