Module modCopyFile
    Public Function CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String) As Boolean
        'WTF

        Try
            If Not IO.File.Exists(sSourcePath & "\" & sFileName) Then
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

        Try
            IO.File.Copy(sSourcePath & "\" & sFileName, sDestinationPath & "\" & sFileName, True)
        Catch ex As Exception
            Return False
        End Try

        Try
            If Not IO.File.Exists(sDestinationPath & "\" & sFileName) Then
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function
End Module
