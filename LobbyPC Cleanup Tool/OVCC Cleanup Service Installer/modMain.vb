﻿
Imports Microsoft.Win32.TaskScheduler

Module modMain
    Public Sub Main()
        Dim sStmp As String = ""


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Debugging = Settings.Registry.Get.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__Debug, False)
        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.Write("started")


        '==================================================================================================================================================================================
        'Settings 
        Dim tmpActivationType As String = ""
        Dim tmpActivationHour As Integer = 0
        Dim tmpLastActivation As Long = 0

        tmpActivationType = Settings.Registry.Get.String(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ActivationType, "")
        tmpActivationHour = Settings.Registry.Get.Integer(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ActivationHour, -1)
        tmpLastActivation = Settings.Registry.Get.Long(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__LastActivation, -1)

        If tmpActivationType.Equals("") Then
            Settings.Registry.Set.String(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ActivationType, "weekly")
        End If
        If tmpActivationHour < 0 Or tmpActivationHour > 23 Then
            Settings.Registry.Set.Integer(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ActivationHour, 2)
        End If
        If tmpLastActivation < 0 Then
            Settings.Registry.Set.Long(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__LastActivation, 0)
        End If

        Settings.Registry.Set.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__Debug, False)
        Settings.Registry.Set.String(Settings.Constants.Registry.KEY__OVCCJanitor__Settings, Settings.Constants.Registry.VALUENAME__Settings__ServerUrl, Helpers.XOrObfuscation_v2.Obfuscate(Settings.Constants.Urls.UPDATE))

        Settings.Registry.Set.Long(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__LastUpdate, -1)

        Settings.Registry.Set.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ForcedCleaning, False)
        Settings.Registry.Set.Boolean(Settings.Constants.Registry.KEY__OVCCJanitor, Settings.Constants.Registry.VALUENAME__ForcedUpdate, False)


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("stop service", 0)
        If ServiceInstaller.ServiceIsInstalled(Settings.Constants.Service.NAME) Then
            Helpers.Logger.WriteMessage(Settings.Constants.Service.NAME, 1)
            Helpers.Logger.WriteMessage("stopping", 2)
            sStmp = ServiceInstaller.StopService(Settings.Constants.Service.NAME)
            Helpers.Logger.WriteMessage(sStmp, 3)

            Threading.Thread.Sleep(15000)
        End If


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("copy files", 0)

        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles
        Dim sBackupPath As String = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\_backups\")
        sBackupPath = IO.Path.Combine(sBackupPath, My.Application.Info.ProductName, My.Application.Info.Version.ToString)
        sBackupPath = IO.Path.Combine(sBackupPath, DateTime.Now().ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))

        oCopyFiles = New Helpers.FilesAndFolders.CopyFiles
        oCopyFiles.BackupDirectory = sBackupPath
        oCopyFiles.SourceDirectory = "files"
        oCopyFiles.DestinationDirectory = Settings.Constants.Service.PATH

        oCopyFiles.CopyFiles()


        '==================================================================================================================================================================================
        'Install Helper task
        Helpers.Logger.WriteMessage("create scheduled task", 0)

        Try
            Dim tTF As TaskFolder = TaskService.Instance.GetFolder(Settings.Constants.Helper.ScheduledTask.FOLDER)
            Dim _task As Task = Nothing

            If tTF IsNot Nothing Then
                Dim lTasks As List(Of Task) = tTF.AllTasks.ToList

                For Each _task In lTasks
                    If _task.Name.Equals(Settings.Constants.Helper.ScheduledTask.NAME) Then
                        Exit For
                    End If
                Next
            End If

            If _task IsNot Nothing Then
                Helpers.Logger.WriteMessage("task already exists!", 1)

                Helpers.Logger.WriteMessage("deleting", 2)
                tTF.DeleteTask(_task.Name)
                Helpers.Logger.WriteMessage("ok", 3)
            End If


            Helpers.Logger.WriteMessage("creating", 1)

            Helpers.Logger.WriteMessage("reading template", 2)
            Dim sXml As String = My.Computer.FileSystem.ReadAllText(Settings.Constants.Files.SCHEDULED_TASK_TEMPLATE_FILE)
            Helpers.Logger.WriteMessage("ok", 3)

            Helpers.Logger.WriteMessage("updating template", 2)

            Dim tSID As String = ""
            If Not Helpers.WindowsUser.ConvertUsernameToSid(Settings.Constants.SiteKiosk.USERNAME, tSID) Then
                Throw New Exception("Could not convert username '" & Settings.Constants.SiteKiosk.USERNAME & "' to sid: " & Helpers.Errors.GetLast())
            End If

            Helpers.Logger.WriteMessage("%%%USER_SID%%", 3)
            Helpers.Logger.WriteMessage(tSID, 4)
            sXml = sXml.Replace("%%%USER_SID%%%", tSID)

            Helpers.Logger.WriteMessage("%%%TASK_PATH%%%", 3)
            Helpers.Logger.WriteMessage(Settings.Constants.Helper.FULL_PATH, 4)
            sXml = sXml.Replace("%%%TASK_PATH%%%", Settings.Constants.Helper.FULL_PATH)

            Helpers.Logger.WriteMessage("ok", 3)


            Helpers.Logger.WriteMessage("creating task definition", 2)
            Dim tTD As TaskDefinition = TaskService.Instance.NewTask
            tTD.XmlText = sXml
            Helpers.Logger.WriteMessage("ok", 3)


            Helpers.Logger.WriteMessage("registering task", 2)
            TaskService.Instance.RootFolder.RegisterTaskDefinition(Settings.Constants.Helper.ScheduledTask.PATH, tTD)
            Helpers.Logger.WriteMessage("ok", 3)
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("EXCEPTION", 1)
            Helpers.Logger.WriteErrorRelative(ex.GetType.Name, 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)
        End Try


        '==================================================================================================================================================================================
        'Install Service
        Helpers.Logger.WriteMessage("start service", 0)
        Helpers.Logger.WriteMessage(Settings.Constants.Service.NAME, 1)


        If Not ServiceInstaller.ServiceIsInstalled(Settings.Constants.Service.NAME) Then
            Helpers.Logger.WriteMessage("installing", 2)
            Helpers.Logger.WriteMessage("display name", 3)
            Helpers.Logger.WriteMessage(Settings.Constants.Service.DISPLAY_NAME, 4)
            Helpers.Logger.WriteMessage("path", 3)
            Helpers.Logger.WriteMessage(Settings.Constants.Service.FULL_PATH, 4)
            ServiceInstaller.InstallService(Settings.Constants.Service.NAME, Settings.Constants.Service.DISPLAY_NAME, Settings.Constants.Service.FULL_PATH)

            Threading.Thread.Sleep(5000)
        End If

        Helpers.Logger.WriteMessage("set description", 2)
        ServiceInstaller.ChangeServiceDescription(Settings.Constants.Service.NAME, Settings.Constants.Service.DESCRIPTION)
        Threading.Thread.Sleep(1000)

        Helpers.Logger.WriteMessage("starting", 2)
        ServiceInstaller.StartService(Settings.Constants.Service.NAME)
        Threading.Thread.Sleep(10000)


        Dim tSS As ServiceProcess.ServiceControllerStatus = ServiceInstaller.GetServiceStatus(Settings.Constants.Service.NAME)
        Helpers.Logger.WriteMessage("is running?", 2)
        Helpers.Logger.WriteMessage("service state", 3)
        Helpers.Logger.WriteMessage(tSS.ToString, 4)
        If tSS = ServiceProcess.ServiceControllerStatus.Running Or tSS = ServiceProcess.ServiceControllerStatus.StartPending Then
            Helpers.Logger.WriteMessage("ok", 3)
        Else
            Helpers.Logger.WriteError("FAIL", 3)
        End If


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Done", 0)
        Helpers.Logger.WriteMessage("ok", 1)
        Helpers.Logger.WriteMessage("bye", 2)
    End Sub
End Module
