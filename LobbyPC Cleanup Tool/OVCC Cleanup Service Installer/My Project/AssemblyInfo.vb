Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCC_Janitor_Installer")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("OVCC_Janitor_Installer")>
<Assembly: AssemblyCopyright("Copyright ©  2024")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("90aacf31-8efb-4ab6-bfaf-3d97d64f8b53")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'

<Assembly: AssemblyVersion("1.0.25028.1501")>
<Assembly: AssemblyFileVersion("1.0.25028.1501")>
