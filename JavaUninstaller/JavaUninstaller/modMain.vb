Imports System.Threading

Module modMain
    Private oLogger As Logger
    Private WithEvents oProcess As ProcessRunner

    Private mValue__keep_app As String

    Private mTest As Boolean

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName


        mTest = True

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        '-----------------------------------
        Dim sArg As String

        mValue__keep_app = ""

        oLogger.WriteToLog("Loading command line arguments", , 0)
        If Environment.GetCommandLineArgs.Length <= 1 Then
            oLogger.WriteToLog("none found", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            Bye(1)
            Exit Sub
        End If

        For Each sArg In My.Application.CommandLineArgs
            sArg = sArg.ToLower

            If InStr(sArg, cParams__keep_this_java) > 0 Then
                'mValue__keep_app = LettersAndDigitsOnly(sArg.Replace(cParams__keep_this_java, ""))
                mValue__keep_app = sArg.Replace(cParams__keep_this_java, "")

                oLogger.WriteToLog("Keep app: " & mValue__keep_app, , 2)
            End If

            If sArg = cParams__go_for_it Then
                mTest = False
            End If
        Next


        '-----------------------------------
        oLogger.WriteToLog("Uninstalling", , 0)
        If mValue__keep_app.Length < 2 Then
            oLogger.WriteToLog("Incorrect app name!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            If mTest Then
                MsgBox("App name '" & mValue__keep_app & "' is too short", MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Application.ProductName)
            End If

            Bye(2)
            Exit Sub
        End If


        Dim sId_CheckKeep As String

        sId_CheckKeep = ProductGUID.GetUninstallIdentifier(mValue__keep_app)
        sId_CheckKeep = sId_CheckKeep.ToLower
        If sId_CheckKeep = "" Then
            'Derp. The version we apparently have to keep is not there.
            oLogger.WriteToLog("You want to keep '" & mValue__keep_app & "' but it's not there!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            If mTest Then
                MsgBox("You want to keep '" & mValue__keep_app & "' but it's not there!", MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly, Application.ProductName)
            End If

            Bye(2)
            Exit Sub
        End If

        Dim sNames As New List(Of String), sIds As New List(Of String), iCount As Integer

        iCount = ProductGUID.GetUninstallIdentifiers(cJAVA_STRING, True, sNames, sIds)
        If iCount = 1 Then
            oLogger.WriteToLog("I found " & iCount.ToString & " app with '" & cJAVA_STRING & "' in the name!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        Else
            oLogger.WriteToLog("I found " & iCount.ToString & " apps with '" & cJAVA_STRING & "' in the name!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        End If

        Dim sId As String, sName As String, iRet As Integer
        For i As Integer = 0 To sIds.Count - 1
            sName = sNames.Item(i).ToLower
            sId = sIds.Item(i).ToLower

            oLogger.WriteToLog("Found '" & sName & "'", , 1)
            If sId <> "" Then
                If sId.Equals(sId_CheckKeep) Then
                    oLogger.WriteToLog("Let's keep this one", , 2)
                Else
                    oLogger.WriteToLog("Uninstalling", , 2)
                    iRet = UninstallProcedure(sId)
                    oLogger.WriteToLog("return code", , 2)
                    oLogger.WriteToLog(iRet.ToString, , 3)
                End If
            End If
        Next

        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
        Bye(0)
    End Sub

    Private Function UninstallProcedure(sId As String) As Integer
        Dim sParam As String

        sParam = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        If Not mTest Then
            oLogger.WriteToLogRelative("Starting uninstaller", , 1)
            oLogger.WriteToLogRelative("Path   : " & cUNINSTALL_COMMAND, , 2)
            oLogger.WriteToLogRelative("Params : " & sParam, , 2)
            oLogger.WriteToLogRelative("Timeout: 1800", , 2)

            oProcess = New ProcessRunner
            oProcess.FileName = cUNINSTALL_COMMAND
            oProcess.Arguments = sParam
            oProcess.MaxTimeout = 1800
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()

            Do
                Thread.Sleep(500)
            Loop Until oProcess.IsProcessDone

            Return oProcess.LastErrorCode
        Else
            oLogger.WriteToLogRelative("Not starting uninstaller (test mode)", , 1)
            oLogger.WriteToLogRelative("Path   : " & cUNINSTALL_COMMAND, , 2)
            oLogger.WriteToLogRelative("Params : " & sParam, , 2)
            oLogger.WriteToLogRelative("Timeout: 1800", , 2)

            oLogger.WriteToLog("I would have started the following command:" & vbCrLf & vbCrLf & cUNINSTALL_COMMAND & " " & sParam, Logger.MESSAGE_TYPE.LOG_DEBUG, 1)

            Return 0
        End If

    End Function

    Private Function LettersAndDigitsOnly(ByVal sString As String) As String
        Dim sb As New System.Text.StringBuilder

        For Each ch As Char In sString
            If Char.IsLetterOrDigit(ch) Then
                sb.Append(ch)
            End If
        Next

        Return sb.ToString()
    End Function


    Private Sub Bye(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("Bye (" & iExitCode.ToString & ")", , 0)
        oLogger.WriteToLog(New String("=", 50))

        Environment.ExitCode = iExitCode
        Application.Exit()
    End Sub


#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLog("done", , 2)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 3)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLog("failed", , 2)
        oLogger.WriteToLog(ErrorMessage, , 3)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_KillFailed(ByVal EventProcess As System.Diagnostics.Process, ByVal ErrorMessage As String) Handles oProcess.KillFailed
        oLogger.WriteToLog("killing process failed", , 2)
        oLogger.WriteToLog(ErrorMessage, , 3)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLog("started", , 2)
        oLogger.WriteToLog("id  : " & EventProcess.Id.ToString, , 3)
        oLogger.WriteToLog("name: " & EventProcess.ProcessName, , 3)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLog("timed out", , 2)

        oProcess.ProcessDone()
    End Sub
#End Region

End Module
