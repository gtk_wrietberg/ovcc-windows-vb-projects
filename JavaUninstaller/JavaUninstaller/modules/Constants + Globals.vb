Module Constants___Globals
    'Uninstall
    Public ReadOnly cUNINSTALL_COMMAND As String = "msiexec.exe"
    Public ReadOnly cUNINSTALL_PARAMS As String = "/X{%%APP_IDENTIFIER%%} /quiet /qn /norestart"

    'Java
    'Public ReadOnly cJAVA_STRING As String = "Java(TM)"
    Public ReadOnly cJAVA_STRING As String = "Java"

    'Params
    Public ReadOnly cParams__keep_this_java As String = "--keep:"
    Public ReadOnly cParams__go_for_it As String = "--go-for-it"


    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub

End Module
