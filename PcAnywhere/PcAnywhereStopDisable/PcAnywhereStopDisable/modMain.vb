Imports System.ServiceProcess
Imports System.Threading

Module modMain
    Private oLogger As Logger
    Private oComputerName As ComputerName

    Private myController As ServiceController

    Private ReadOnly c_REGISTRY_SERVICES As String = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\"

    'Private ReadOnly c_SERVICE_NAME As String = "JavaQuickStarterService"
    Private ReadOnly c_SERVICE_NAME As String = "awhost32"

    Private ReadOnly c_StatusTimeout As Integer = 20

    Private ReadOnly c_SERVICE_START_AUTOMATIC As Integer = 2
    Private ReadOnly c_SERVICE_START_MANUAL As Integer = 3
    Private ReadOnly c_SERVICE_START_DISABLED As Integer = 4

    Public Sub Main()
        oLogger = New Logger
        oComputerName = New ComputerName

        oLogger.LogFilePath = "C:\Program Files\iBAHN\"
        oLogger.LogFileName = "PcAnywhereStopDisable.log"

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)


        '------------------------------------------------------
        Dim bCanStop As Boolean
        Dim sStatus As String
        Dim iStatusTimeout As Integer

        myController = New ServiceController(c_SERVICE_NAME)

        oLogger.WriteToLog("Stopping and disabling service", , 0)
        oLogger.WriteToLog("Service name: " & c_SERVICE_NAME, , 1)

        Try
            bCanStop = myController.CanStop
        Catch ex As Exception
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            ExitApplication(1)

            Exit Sub
        End Try


        myController.Refresh()
        sStatus = myController.Status.ToString
        oLogger.WriteToLog("state: " & sStatus, , 2)

        oLogger.WriteToLog("Stopping", , 1)
        If Not myController.Status = ServiceControllerStatus.Stopped Or Not myController.Status = ServiceControllerStatus.StopPending Then
            myController.Stop()

            iStatusTimeout = 0
            Do
                iStatusTimeout += 1

                Thread.Sleep(500)

                myController.Refresh()
            Loop While Not myController.Status = ServiceControllerStatus.Stopped And iStatusTimeout < c_StatusTimeout

            myController.Refresh()
            sStatus = myController.Status.ToString
            oLogger.WriteToLog("state: " & sStatus, , 2)
        Else
            oLogger.WriteToLog("Already stopped or stopping", , 2)
        End If


        '----------------------------------------------
        oLogger.WriteToLog("Disabling", , 1)

        Dim iCurrentValue As Integer

        oLogger.WriteToLog("Getting start value", , 2)

        iCurrentValue = GetCurrentValue()


        If iCurrentValue < 1 Then
            oLogger.WriteToLog("Error", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            ExitApplication(1)

            Exit Sub
        End If

        oLogger.WriteToLog("Current start value: " & ServiceStartValues(iCurrentValue), , 3)


        If iCurrentValue <> c_SERVICE_START_DISABLED Then
            oLogger.WriteToLog("Setting start value", , 2)

            Microsoft.Win32.Registry.SetValue(c_REGISTRY_SERVICES & c_SERVICE_NAME, "Start", c_SERVICE_START_DISABLED, Microsoft.Win32.RegistryValueKind.DWord)

            iCurrentValue = GetCurrentValue()
            oLogger.WriteToLog("Current start value: " & ServiceStartValues(iCurrentValue), , 3)
        End If

        ExitApplication(0)
    End Sub

    Private Function GetCurrentValue() As Integer
        Dim oCurrentValue As Object, iCurrentValue As Integer

        oCurrentValue = Microsoft.Win32.Registry.GetValue(c_REGISTRY_SERVICES & c_SERVICE_NAME, "Start", Nothing)
        If oCurrentValue Is Nothing Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            ExitApplication(2)

            Exit Function
        End If

        Try
            iCurrentValue = Integer.Parse(oCurrentValue.ToString)
        Catch ex As Exception
            iCurrentValue = 0
        End Try

        Return iCurrentValue
    End Function

    Private Function ServiceStartValues(ByVal iStartValue As Integer) As String
        Select Case iStartValue
            Case c_SERVICE_START_AUTOMATIC
                Return "Automatic"
            Case c_SERVICE_START_MANUAL
                Return "Manual"
            Case c_SERVICE_START_DISABLED
                Return "Disabled"
            Case Else
                Return "Unknown"
        End Select
    End Function

    Public Sub ExitApplication(ByVal iExitCode As Integer)
        oLogger.WriteToLog("Exiting...", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        oLogger.WriteToLog("exit code: " & iExitCode, Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLog("bye", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLogWithoutDate(New String("=", 100))

        System.Environment.Exit(iExitCode)
    End Sub
End Module
