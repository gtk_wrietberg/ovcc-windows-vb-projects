﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("PcAnywhereUpdate")> 
<Assembly: AssemblyDescription("Alternate pcAnywhere hotfix (Tech180472)")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("PcAnywhereUpdate")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("a74c6d31-da01-41b2-889e-054080d99f3f")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.1.14.1612")> 
<Assembly: AssemblyFileVersion("1.1.14.1612")> 
