Imports System.ServiceProcess
Imports System.Threading

Module modMain
    Private oLogger As Logger
    Private oComputerName As ComputerName

    Private myController As ServiceController


    'Private ReadOnly c_SERVICE_NAME As String = "JavaQuickStarterService"
    Private ReadOnly c_SERVICE_NAME As String = "awhost32"

    'Private ReadOnly c_SERVICE_PATH As String = "C:\Program Files\Java\jre6\bin\jqs.exe"
    Private ReadOnly c_SERVICE_PATH As String = "C:\Program Files\Symantec\pcAnywhere\awhost32.exe"

    Private ReadOnly c_StatusTimeout As Integer = 60 'seconds


    Public Sub Main()
        oLogger = New Logger
        oComputerName = New ComputerName

        oLogger.LogFilePath = "C:\Program Files\iBAHN\"
        oLogger.LogFileName = "PcAnywhereUpdate.log"

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        '------------------------------------------------------
        Dim bCanStop As Boolean
        Dim sStatus As String
        Dim iStatusTimeout As Integer

        myController = New ServiceController(c_SERVICE_NAME)


        oLogger.WriteToLog("Updating service", , 0)
        oLogger.WriteToLog("Service name: " & c_SERVICE_NAME, , 1)

        Try
            bCanStop = myController.CanStop
        Catch ex As Exception
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            ExitApplication(1)

            Exit Sub
        End Try


        Dim ServiceInfo As FileVersionInfo
        Dim sVersion As String
        Try

            ServiceInfo = FileVersionInfo.GetVersionInfo(c_SERVICE_PATH)
            sVersion = ServiceInfo.FileVersion
        Catch ex As Exception
            sVersion = "(unknown)"
        End Try

        oLogger.WriteToLog("version: " & sVersion, , 2)

        If sVersion = "12.0.2.174" Or sVersion = "12.5.0.442" Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("Incompatible version!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            ExitApplication(2)

            Exit Sub
        End If

        myController.Refresh()
        sStatus = myController.Status.ToString
        oLogger.WriteToLog("state: " & sStatus, , 2)

        '------------------------------------------------------

        oLogger.WriteToLog("Stopping", , 1)
        If Not myController.Status = ServiceControllerStatus.Stopped Then
            myController.Stop()

            iStatusTimeout = 0
            Do
                iStatusTimeout += 1

                Thread.Sleep(1000)

                myController.Refresh()
            Loop While Not myController.Status = ServiceControllerStatus.Stopped And iStatusTimeout < c_StatusTimeout

            myController.Refresh()
            sStatus = myController.Status.ToString
            oLogger.WriteToLog("state: " & sStatus, , 2)

            If Not myController.Status = ServiceControllerStatus.Stopped Then
                oLogger.WriteToLog("Stopping service timed out, exiting...", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                ExitApplication(3)
                Exit Sub
            End If
        Else
            oLogger.WriteToLog("Already stopped", , 2)
        End If

        '-------------------------------------------------------------------
        oLogger.WriteToLog("Updating", , 1)

        Dim iFileCheck As Integer = -1

        If sVersion = "12.0.2.174" Then
            oLogger.WriteToLog("Copying files", , 2)

            iFileCheck = _CopyFiles__12_0_2_174(7)
        End If

        If sVersion = "12.5.0.442" Then
            oLogger.WriteToLog("Copying files", , 2)

            iFileCheck = _CopyFiles__12_5_0_442(8)
        End If

        If iFileCheck < 0 Then
            oLogger.WriteToLog("Rrrrright. Someone forgot to update the compatible version list...", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If
        If iFileCheck = 0 Then
            oLogger.WriteToLog("done", , 2)
        End If
        If iFileCheck > 0 Then
            oLogger.WriteToLog("OOPS, not all files were copied", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            ExitApplication(4)
            Exit Sub
        End If

        '-------------------------------------------------------------------
        oLogger.WriteToLog("Starting", , 1)
        If Not myController.Status = ServiceControllerStatus.Running Then
            myController.Start()

            iStatusTimeout = 0
            Do
                iStatusTimeout += 1

                Thread.Sleep(1000)

                myController.Refresh()
            Loop While Not myController.Status = ServiceControllerStatus.Running And iStatusTimeout < c_StatusTimeout

            myController.Refresh()
            sStatus = myController.Status.ToString
            oLogger.WriteToLog("state: " & sStatus, , 2)

            If Not myController.Status = ServiceControllerStatus.Running Then
                oLogger.WriteToLog("Starting service timed out, exiting...", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                ExitApplication(5)
                Exit Sub
            End If
        Else
            oLogger.WriteToLog("Already stopped", , 2)
        End If


        ExitApplication(0)
    End Sub

    Public Sub ExitApplication(ByVal iExitCode As Integer)
        oLogger.WriteToLog("Exiting...", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        oLogger.WriteToLog("exit code: " & iExitCode, Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLog("bye", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        oLogger.WriteToLogWithoutDate(New String("=", 100))

        System.Environment.Exit(iExitCode)
    End Sub

    Private Function _CopyFiles__12_0_2_174(ByVal _iFileCheck As Integer) As Integer
        Dim iFileCheck As Integer = _iFileCheck

        Try
            oLogger.WriteToLogRelative("copying awcfgmgr.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\awcfgmgr.dll", My.Resources._12_0_2_174_awcfgmgr_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying awcfgmgr.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying awhlogon.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\awhlogon.dll", My.Resources._12_0_2_174_awhlogon_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying awhlogon.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying AWRem32.exe", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\AWRem32.exe", My.Resources._12_0_2_174_AWRem32_exe)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying AWRem32.exe", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying awterm32.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\awterm32.dll", My.Resources._12_0_2_174_awterm32_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying awterm32.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying crypto.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\crypto.dll", My.Resources._12_0_2_174_crypto_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying crypto.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying ShellClient.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\ShellClient.dll", My.Resources._12_0_2_174_ShellClient_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying ShellClient.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying WinAw32.exe", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\WinAw32.exe", My.Resources._12_0_2_174_Winaw32_exe)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying WinAw32.exe", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Return iFileCheck
    End Function

    Private Function _CopyFiles__12_5_0_442(ByVal _iFileCheck As Integer) As Integer
        Dim iFileCheck As Integer = _iFileCheck

        Try
            oLogger.WriteToLogRelative("copying awcfgmgr.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\awcfgmgr.dll", My.Resources._12_5_0_442_awcfgmgr_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying awcfgmgr.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying awhlogon.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\awhlogon.dll", My.Resources._12_5_0_442_awhlogon_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying awhlogon.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying AWRem32.exe", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\AWRem32.exe", My.Resources._12_5_0_442_AWRem32_exe)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying AWRem32.exe", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying awterm32.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\awterm32.dll", My.Resources._12_5_0_442_awterm32_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying awterm32.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying crypto.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\crypto.dll", My.Resources._12_5_0_442_crypto_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying crypto.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying PCAQuickconnectDll.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\PCAQuickconnectDll.dll", My.Resources._12_5_0_442_PCAQuickconnectDll_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying PCAQuickconnectDll.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying ShellClient.dll", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\ShellClient.dll", My.Resources._12_5_0_442_ShellClient_dll)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying ShellClient.dll", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("copying WinAw32.exe", , 1)
            System.IO.File.WriteAllBytes("C:\Program Files\Symantec\pcAnywhere\WinAw32.exe", My.Resources._12_5_0_442_WinAw32_exe)
            oLogger.WriteToLogRelative("ok", , 2)
            iFileCheck -= 1
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error while copying WinAw32.exe", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Return iFileCheck
    End Function
End Module
