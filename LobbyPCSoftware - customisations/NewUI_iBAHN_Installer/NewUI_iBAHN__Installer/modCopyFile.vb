Module modCopyFile
    Private pBackupFolder As String

    Public Function CopyFiles() As Boolean
        InitializeBackupFolder()
    End Function

    Private Function InitializeBackupFolder() As Boolean
        Dim dDate As Date = Now()

        pBackupFolder = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_backups"
        pBackupFolder &= "\" & Application.ProductName & "\" & Application.ProductVersion
        pBackupFolder &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        IO.Directory.CreateDirectory(pBackupFolder)
    End Function

    Private Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String)
        Dim sDir As String
        Dim dDirInfo As IO.DirectoryInfo
        Dim sDirInfo As IO.DirectoryInfo
        Dim sFile As String
        Dim sFileInfo As IO.FileInfo
        Dim dFileInfo As IO.FileInfo

        ' Add trailing separators to the supplied paths if they don't exist.
        If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            sourceDir &= System.IO.Path.DirectorySeparatorChar
        End If
        If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            destDir &= System.IO.Path.DirectorySeparatorChar
        End If

        'If destination directory does not exist, create it.
        dDirInfo = New System.IO.DirectoryInfo(destDir)
        If dDirInfo.Exists = False Then dDirInfo.Create()
        dDirInfo = Nothing


        ' Get a list of directories from the current parent.
        For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
            sDirInfo = New System.IO.DirectoryInfo(sDir)
            dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
            ' Create the directory if it does not exist.
            If dDirInfo.Exists = False Then dDirInfo.Create()
            ' Since we are in recursive mode, copy the children also
            RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName)
            sDirInfo = Nothing
            dDirInfo = Nothing
        Next

        ' Get the files from the current parent.
        For Each sFile In System.IO.Directory.GetFiles(sourceDir)
            sFileInfo = New System.IO.FileInfo(sFile)
            dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

            'If File does exists, backup.
            If dFileInfo.Exists Then
                BackupFile(sFileInfo)
            End If
            sFileInfo.CopyTo(dFileInfo.FullName, True)

            sFileInfo = Nothing
            dFileInfo = Nothing
        Next
    End Sub

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Try
            oFile.CopyTo(pBackupFolder & "\" & oFile.Name, True)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Module
