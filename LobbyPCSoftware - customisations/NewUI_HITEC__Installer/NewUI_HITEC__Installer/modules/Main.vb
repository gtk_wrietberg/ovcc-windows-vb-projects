Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or _
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        oCopyFiles.DestinationDirectory = "c:\"
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Update SiteKiosk config", , 0)
        oSkCfg.Update()


        ''------------------------------------------------------
        'oLogger.WriteToLog("Rebooting", , 0)
        'If g_TESTMODE Then
        '    oLogger.WriteToLog("skipped, in test mode", , 1)
        'Else
        '    oLogger.WriteToLog("bye", , 1)
        '    'WindowsController.ExitWindows(RestartOptions.Reboot, True)
        'End If

        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
