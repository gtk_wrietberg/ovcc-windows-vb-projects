﻿Public Class clsGlobals
    Public Shared oLogger As Logger
    Public Shared oComputerName As ComputerName

    Public Shared oSkCfg As SkCfg

    Public Shared oHotelBrand As clsHotelBrand

    Public Shared g_LogFileDirectory As String
    Public Shared g_LogFileName As String
    Public Shared g_BackupDirectory As String

    Public Shared g_TESTMODE As Boolean

    Public Shared g_OverwriteExistingConfig As Boolean
    Public Shared g_LeaveGlobalSettingsAlone As Boolean

    Public Shared g_Url_Hotel As String = "http://www.hilton.co.uk"
    Public Shared g_Url_Internet As String = "http://www.google.com"
    Public Shared g_Url_Weather As String = "http://www.weather.com"
    Public Shared g_Url_Map As String = "http://maps.google.com"
    Public Shared g_Address_Site As String = "iBAHN, Logie Court, Stirling University Inovation Park, Stirling"

    Public Shared ReadOnly c_FreeUrlZoneName As String = "No Charge (UI)"

    Public Shared ReadOnly c_Template_Guesttek As String = "Guest-tek Startpage"
    Public Shared ReadOnly c_Template_iBAHN As String = "iBAHN Startpage"
    Public Shared ReadOnly c_Template_Hilton As String = "Hilton Startpage"

    Public Shared ReadOnly c_Path_Guesttek__UI As String = "%SiteKioskPath%/Skins/Public/Startpages/Guest-tek/index.html"
    Public Shared ReadOnly c_Path_iBAHN__UI As String = "%SiteKioskPath%/Skins/Public/Startpages/iBAHN/index.html"
    Public Shared ReadOnly c_Path_Hilton__UI As String = "%SiteKioskPath%/Skins/Public/StartPages/Hilton/index.html"

    Public Shared ReadOnly c_Path_Guesttek__screensaver As String = "C:\Program Files (x86)\SiteKiosk\Skins\Public\Startpages\Guest-tek\screensaver\index.html"
    Public Shared ReadOnly c_Path_iBAHN__screensaver As String = "C:\Program Files (x86)\SiteKiosk\Skins\Public\Startpages\iBAHN\screensaver\index.html"
    Public Shared ReadOnly c_Path_Hilton__screensaver As String = "C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\Hilton\screensaver\main.html"

    Public Shared ReadOnly c_Path_SK__paymentdialog As String = "C:\Program Files (x86)\SiteKiosk\SiteCash\Html\PaymentInfoDlg.htm"
    Public Shared ReadOnly c_Path_Hilton__paymentdialog As String = "C:\Program Files (x86)\SiteKiosk\SiteCash\Html\PaymentInfoDlg.HILTON.htm"
    Public Shared ReadOnly c_Path_Default__paymentdialog As String = "C:\Program Files (x86)\SiteKiosk\SiteCash\Html\PaymentInfoDlg.DEFAULT.htm"


    Public Shared Sub InitGlobals()
        Dim dDate As Date = Now()
        Dim sProgFilesPath As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sProgFilesPath.Equals(String.Empty) Then
            sProgFilesPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        g_LogFileDirectory = sProgFilesPath & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = sProgFilesPath & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try

        g_OverwriteExistingConfig = False
        g_LeaveGlobalSettingsAlone = False

        oSkCfg = New SkCfg
    End Sub

End Class
