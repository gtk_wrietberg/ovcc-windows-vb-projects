﻿Public Class Helpers
    Public Shared Function GetHostnameFromUrl(ByVal sUrl As String) As String
        Dim u As New Uri(sUrl)

        Return u.Host
    End Function

    Public Shared Function IsAdmin() As Boolean
        Dim bTmp As Boolean

        Try
            Dim user As Security.Principal.WindowsIdentity = Security.Principal.WindowsIdentity.GetCurrent()
            Dim principal As New Security.Principal.WindowsPrincipal(user)

            bTmp = principal.IsInRole(Security.Principal.WindowsBuiltInRole.Administrator)
        Catch ex As UnauthorizedAccessException
            bTmp = False
        Catch ex As Exception
            bTmp = False
        End Try

        Return bTmp
    End Function
End Class
