﻿Public Class clsHotelBrand
    Private mBrandNumber As Integer
    Public Property BrandNumber() As Integer
        Get
            Return mBrandNumber
        End Get
        Set(ByVal value As Integer)
            mBrandNumber = value
        End Set
    End Property

    Private mBrandiBAHN As Boolean
    Public Property BrandiBAHN() As Boolean
        Get
            Return mBrandiBAHN
        End Get
        Set(ByVal value As Boolean)
            mBrandiBAHN = value
        End Set
    End Property

    Private mBrandGuestTek As Boolean
    Public Property BrandGuestTek() As Boolean
        Get
            Return mBrandGuestTek
        End Get
        Set(ByVal value As Boolean)
            mBrandGuestTek = value
        End Set
    End Property

    Public Sub Reset()
        mBrandNumber = -1
        mBrandGuestTek = False
        mBrandiBAHN = False
    End Sub

    Public Sub New()
        Me.Reset()
    End Sub
End Class
