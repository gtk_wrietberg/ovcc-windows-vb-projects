﻿Public Class clsGlobalSettings
    Public Shared Function ReadGlobalSettings() As Boolean
        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

        If Not IO.File.Exists(sFile) Then
            Return False
        End If

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = line.Replace("var iBAHN_Global_HotelURL=", "")
                        line = CleanText(line)
                        clsGlobals.g_Url_Hotel = line
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = line.Replace("var iBAHN_Global_InternetURL=", "")
                        line = CleanText(line)
                        clsGlobals.g_Url_Internet = line
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = line.Replace("var iBAHN_Global_WeatherURL=", "")
                        line = CleanText(line)
                        clsGlobals.g_Url_Weather = line
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = line.Replace("var iBAHN_Global_MapURL=", "")
                        line = CleanText(line)
                        clsGlobals.g_Url_Map = line
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = line.Replace("var iBAHN_Global_SiteAddress=", "")
                        line = CleanText(line)
                        clsGlobals.g_Address_Site = line
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        line = line.Replace("var iBAHN_Global_Brand=", "")
                        line = CleanText(line)

                        clsGlobals.oHotelBrand.Reset()
                        If IsNumeric(line) Then
                            If line = 666 Then
                                clsGlobals.oHotelBrand.BrandiBAHN = True
                            ElseIf line = 667 Then
                                clsGlobals.oHotelBrand.BrandGuestTek = True
                            Else
                                clsGlobals.oHotelBrand.BrandNumber = line
                            End If
                        End If
                    End If
                End While
            End Using

            Return True
        Catch ex As Exception : Return False : End Try
    End Function

    Private Shared Function CleanText(txt As String) As String
        txt = txt.Replace("""", "")
        txt = txt.Replace(";", "")

        Return txt
    End Function

    Public Shared Function WriteGlobalSettings() As Boolean
        Dim lines As New List(Of String)

        clsGlobals.oLogger.WriteToLogRelative("reading script file", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

        If Not IO.File.Exists(sFile) Then
            clsGlobals.oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        Else
            clsGlobals.oLogger.WriteToLogRelative("ok", , 2)
        End If

        Try
            clsGlobals.oLogger.WriteToLogRelative("setting values", , 1)

            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = "var iBAHN_Global_HotelURL=""" & clsGlobals.g_Url_Hotel & """;"
                        clsGlobals.oLogger.WriteToLogRelative("HotelUrl", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = "var iBAHN_Global_InternetURL=""" & clsGlobals.g_Url_Internet & """;"
                        clsGlobals.oLogger.WriteToLogRelative("InternetURL", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = "var iBAHN_Global_WeatherURL=""" & clsGlobals.g_Url_Weather & """;"
                        clsGlobals.oLogger.WriteToLogRelative("WeatherURL", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = "var iBAHN_Global_MapURL=""" & clsGlobals.g_Url_Map & """;"
                        clsGlobals.oLogger.WriteToLogRelative("MapURL", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = "var iBAHN_Global_SiteAddress=""" & clsGlobals.g_Address_Site & """;"
                        clsGlobals.oLogger.WriteToLogRelative("SiteAddress", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        If clsGlobals.oHotelBrand.BrandGuestTek Then
                            line = "var iBAHN_Global_Brand=667;"
                        ElseIf clsGlobals.oHotelBrand.BrandiBAHN Then
                            line = "var iBAHN_Global_Brand=666;"
                        Else
                            line = "var iBAHN_Global_Brand=" & clsGlobals.oHotelBrand.BrandNumber & ";"
                        End If
                        clsGlobals.oLogger.WriteToLogRelative("Brand", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If

                    lines.Add(line)
                End While
            End Using

            clsGlobals.oLogger.WriteToLogRelative("writing script file", , 1)
            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using

            clsGlobals.oLogger.WriteToLogRelative("ok", , 2)
            Return True
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            clsGlobals.oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End Try
    End Function
End Class
