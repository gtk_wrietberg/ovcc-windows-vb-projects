Public Class SkCfg
    Private mOldSkCfgFile As String
    Private mNewSkCfgFile As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Sub Update(Optional ByVal bOverWriteOldFile As Boolean = False)
        Try
            clsGlobals.oLogger.WriteToLogRelative("finding SiteKiosk install directory", , 1)
            mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            clsGlobals.oLogger.WriteToLogRelative("found: " & mSiteKioskFolder, , 2)

            clsGlobals.oLogger.WriteToLogRelative("finding active skcfg file", , 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            clsGlobals.oLogger.WriteToLogRelative("found: " & mOldSkCfgFile, , 2)



            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            If bOverWriteOldFile Then
                clsGlobals.oLogger.WriteToLogRelative("backing up current config", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)

                Dim sBackupPath As String = oFile.DirectoryName
                Dim sBackupName As String = oFile.Name
                Dim sBackupFile As String
                Dim oDirInfo As System.IO.DirectoryInfo
                sBackupPath = sBackupPath.Replace("C:", "")
                sBackupPath = sBackupPath.Replace("c:", "")
                sBackupPath = sBackupPath.Replace("\\", "\")
                sBackupPath = mBackupFolder & "\" & sBackupPath

                sBackupFile = sBackupPath & "\" & sBackupName

                oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
                If Not oDirInfo.Exists Then oDirInfo.Create()

                oFile = New IO.FileInfo(mOldSkCfgFile)
                oFile.CopyTo(sBackupFile, True)

                clsGlobals.oLogger.WriteToLogRelative("backup: " & mOldSkCfgFile & " => " & sBackupFile, , 2)

                mNewSkCfgFile = mOldSkCfgFile
            Else
                clsGlobals.oLogger.WriteToLogRelative("making copy", , 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)
                mNewSkCfgFile = oFile.DirectoryName & "\iBAHN_newUI_Hilton__" & sDateString & ".skcfg"
                oFile.CopyTo(mNewSkCfgFile, True)

                clsGlobals.oLogger.WriteToLogRelative("copy: " & mNewSkCfgFile, , 2)
            End If


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument

            clsGlobals.oLogger.WriteToLogRelative("updating", , 1)

            clsGlobals.oLogger.WriteToLogRelative("loading", , 2)
            clsGlobals.oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            clsGlobals.oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            clsGlobals.oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                clsGlobals.oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If



            '*************************************************************************************************************
            'SiteCash - replace current url zones
            clsGlobals.oLogger.WriteToLogRelative("SiteCash url zones update", , 2)
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode

            clsGlobals.oLogger.WriteToLogRelative("loading SiteCash node", , 3)
            xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

            If Not xmlNode_SiteCash Is Nothing Then
                clsGlobals.oLogger.WriteToLogRelative("ok", , 4)
            Else
                Throw New Exception("SiteCash node not found")
            End If

            clsGlobals.oLogger.WriteToLogRelative("setting SiteCash state", , 4)
            Try
                'If Not clsGlobals.oHotelBrand.BrandGuestTek And Not clsGlobals.oHotelBrand.BrandiBAHN Then
                '    'Hilton, we need to disable payment
                '    xmlNode_SiteCash.Attributes.GetNamedItem("enabled").Value = "true"
                '    clsGlobals.oLogger.WriteToLogRelative("enabled", , 5)
                'Else
                '    xmlNode_SiteCash.Attributes.GetNamedItem("enabled").Value = "true"
                '    clsGlobals.oLogger.WriteToLogRelative("enabled", , 5)
                'End If

                xmlNode_SiteCash.Attributes.GetNamedItem("enabled").Value = "true"
                clsGlobals.oLogger.WriteToLogRelative("enabled", , 5)
            Catch ex As Exception
                clsGlobals.oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                clsGlobals.oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End Try


            Dim xmlNodeList_UrlZones As Xml.XmlNodeList
            Dim xmlNode_UrlZone As Xml.XmlNode
            Dim xmlNodeList_Urls As Xml.XmlNodeList
            Dim xmlNode_Url As Xml.XmlNode
            Dim xmlNode_UrlClone As Xml.XmlNode
            Dim xmlNode_UrlCloneClone As Xml.XmlNode
            Dim xmlNode_FreeUrlZone As Xml.XmlNode

            clsGlobals.oLogger.WriteToLogRelative("removing '" & clsGlobals.c_FreeUrlZoneName & "' url zone, if there", , 3)
            xmlNodeList_UrlZones = xmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
            For Each xmlNode_UrlZone In xmlNodeList_UrlZones
                If xmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = clsGlobals.c_FreeUrlZoneName Then
                    xmlNode_SiteCash.RemoveChild(xmlNode_UrlZone)
                    clsGlobals.oLogger.WriteToLogRelative("ok", , 4)
                End If
            Next

            clsGlobals.oLogger.WriteToLogRelative("adding '" & clsGlobals.c_FreeUrlZoneName & "' url zone", , 3)
            xmlNode_FreeUrlZone = xmlNode_SiteCash.SelectSingleNode("sk:url-zone", ns).CloneNode(True)
            xmlNode_FreeUrlZone.SelectSingleNode("sk:name", ns).InnerText = clsGlobals.c_FreeUrlZoneName

            xmlNodeList_Urls = xmlNode_FreeUrlZone.SelectNodes("sk:url", ns)
            xmlNode_UrlClone = xmlNode_FreeUrlZone.SelectSingleNode("sk:url", ns).CloneNode(True)
            For Each xmlNode_Url In xmlNodeList_Urls
                xmlNode_FreeUrlZone.RemoveChild(xmlNode_Url)
            Next

            clsGlobals.oLogger.WriteToLogRelative("adding: " & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Hotel), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Hotel)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Hotel)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            clsGlobals.oLogger.WriteToLogRelative("adding: " & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Weather), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Weather)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Weather)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            clsGlobals.oLogger.WriteToLogRelative("adding: " & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Map), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Map)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Map)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            xmlNode_SiteCash.AppendChild(xmlNode_FreeUrlZone)


            '*************************************************************************************************************
            'StartpageConfig 
            clsGlobals.oLogger.WriteToLogRelative("startpageconfig update", , 2)
            Dim xmlNode_StartpageConfig As Xml.XmlNode
            Dim xmlNode_Startpage As Xml.XmlNode
            Dim xmlNode_TemplateName As Xml.XmlNode

            clsGlobals.oLogger.WriteToLogRelative("loading startpageconfig node", , 3)
            xmlNode_StartpageConfig = xmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)
            xmlNode_Startpage = xmlNode_StartpageConfig.SelectSingleNode("sk:startpage", ns)
            xmlNode_TemplateName = xmlNode_StartpageConfig.SelectSingleNode("sk:templatename", ns)

            clsGlobals.oLogger.WriteToLogRelative("setting startpage", , 3)
            If clsGlobals.oHotelBrand.BrandGuestTek Then
                xmlNode_Startpage.InnerText = clsGlobals.c_Path_Guesttek__UI
                xmlNode_TemplateName.InnerText = clsGlobals.c_Template_Guesttek
            ElseIf clsGlobals.oHotelBrand.BrandiBAHN Then
                xmlNode_Startpage.InnerText = clsGlobals.c_Path_iBAHN__UI
                xmlNode_TemplateName.InnerText = clsGlobals.c_Template_iBAHN
            Else
                xmlNode_Startpage.InnerText = clsGlobals.c_Path_Hilton__UI
                xmlNode_TemplateName.InnerText = clsGlobals.c_Template_Hilton
            End If



            '*************************************************************************************************************
            'Screensaver
            Dim xmlNode_Screensaver As Xml.XmlNode

            clsGlobals.oLogger.WriteToLogRelative("Screensaver update", , 2)
            xmlNode_Screensaver = xmlNode_Root.SelectSingleNode("sk:screensaver", ns)

            clsGlobals.oLogger.WriteToLogRelative("making sure screensaver is enabled", , 3)
            xmlNode_Screensaver.Attributes.GetNamedItem("enabled").InnerText = "true"

            clsGlobals.oLogger.WriteToLogRelative("setting screensaver", , 3)
            If clsGlobals.oHotelBrand.BrandGuestTek Then
                If IO.File.Exists(clsGlobals.c_Path_Guesttek__screensaver) Then
                    xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://" & clsGlobals.c_Path_Guesttek__screensaver
                Else
                    xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://" & clsGlobals.c_Path_Guesttek__screensaver.Replace(" (x86)", "")
                End If
            ElseIf clsGlobals.oHotelBrand.BrandiBAHN Then
                If IO.File.Exists(clsGlobals.c_Path_iBAHN__screensaver) Then
                    xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://" & clsGlobals.c_Path_iBAHN__screensaver
                Else
                    xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://" & clsGlobals.c_Path_iBAHN__screensaver.Replace(" (x86)", "")
                End If
            Else
                If IO.File.Exists(clsGlobals.c_Path_Hilton__screensaver) Then
                    xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://" & clsGlobals.c_Path_Hilton__screensaver
                Else
                    xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://" & clsGlobals.c_Path_Hilton__screensaver.Replace(" (x86)", "")
                End If
            End If


            '*************************************************************************************************************
            'Saving
            clsGlobals.oLogger.WriteToLogRelative("saving", , 2)
            clsGlobals.oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Save(mNewSkCfgFile)
            clsGlobals.oLogger.WriteToLogRelative("ok", , 4)

            If Not bOverWriteOldFile Then
                clsGlobals.oLogger.WriteToLogRelative("activating", , 2)
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

                Dim sTempRegVal As String
                sTempRegVal = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

                If sTempRegVal = mNewSkCfgFile Then
                    clsGlobals.oLogger.WriteToLogRelative("ok", , 3)
                Else
                    clsGlobals.oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_WARNING, 3)

                    clsGlobals.oLogger.WriteToLogRelative("trying to fix this by overwriting the existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 4)

                    clsGlobals.oLogger.WriteToLogRelative("backing up existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mOldSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile & "." & sDateString & "." & Application.ProductName & ".backup", True)

                    'Application .ProductName 

                    clsGlobals.oLogger.WriteToLogRelative("overwriting existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mNewSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile, True)
                End If
            End If
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            clsGlobals.oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub
End Class
