﻿Public Class frmMain
    Private Function FindTopLocation(ByVal item As Control) As Integer
        Dim i_top As Integer = 0

        i_top = item.Top

        If Not TypeOf item Is Form Then
            i_top += FindTopLocation(item.Parent)
        End If

        Return i_top
    End Function

    Private Function FindLeftLocation(ByVal item As Control) As Integer
        Dim i_left As Integer = 0

        i_left = item.Left

        If Not TypeOf item Is Form Then
            i_left += FindLeftLocation(item.Parent)
        End If

        Return i_left
    End Function

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        clsGlobals.InitGlobals()

        clsGlobals.oLogger = New Logger

        clsGlobals.oLogger.LogFileDirectory = clsGlobals.g_LogFileDirectory
        clsGlobals.oLogger.LogFileName = clsGlobals.g_LogFileName

        clsGlobals.oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        clsGlobals.oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)



        Me.Size = New Size(631, 580)

        If Not Helpers.IsAdmin Then
            btnSave.Enabled = False
            btnCancel.Enabled = False

            Dim pTmp As New Point
            pTmp.X = pnlButtons.Left + pnlMain.Left + 1
            pTmp.Y = pnlButtons.Top + pnlMain.Top + 1

            pnlDisabled.Location = pTmp
            pnlDisabled.BringToFront()
        End If


        cmbBrand.Items.Clear()

        cmbBrand.Items.Add("Hilton Hotel & Resorts")
        cmbBrand.Items.Add("Garden Inn")
        cmbBrand.Items.Add("Hampton")
        cmbBrand.Items.Add("Waldorf Astoria")
        cmbBrand.Items.Add("Conrad")
        cmbBrand.Items.Add("DoubleTree")



        clsGlobals.oHotelBrand = New clsHotelBrand

        clsGlobalSettings.ReadGlobalSettings()
        txtHotelAddress.Text = clsGlobals.g_Address_Site
        txtHotelinformation.Text = clsGlobals.g_Url_Hotel
        txtInternet.Text = clsGlobals.g_Url_Internet
        txtMap.Text = clsGlobals.g_Url_Map
        txtWeather.Text = clsGlobals.g_Url_Weather
        Try
            cmbBrand.SelectedIndex = clsGlobals.oHotelBrand.BrandNumber
        Catch ex As Exception
        End Try
        If clsGlobals.oHotelBrand.BrandGuestTek Then
            chk_GuestTek.Checked = True
        End If
        If clsGlobals.oHotelBrand.BrandiBAHN Then
            chk_iBAHN.Checked = True
        End If

    End Sub

    Private Sub chk_GuestTek_CheckedChanged(sender As Object, e As EventArgs) Handles chk_GuestTek.CheckedChanged
        If chk_GuestTek.Checked Then
            chk_iBAHN.Checked = False
            cmbBrand.Enabled = False

            clsGlobals.oHotelBrand.BrandGuestTek = True
            clsGlobals.oHotelBrand.BrandiBAHN = False
            clsGlobals.oHotelBrand.BrandNumber = 667
        Else
            clsGlobals.oHotelBrand.BrandGuestTek = False
            cmbBrand.Enabled = True
            clsGlobals.oHotelBrand.BrandNumber = cmbBrand.SelectedIndex
        End If
    End Sub

    Private Sub chk_iBAHN_CheckedChanged(sender As Object, e As EventArgs) Handles chk_iBAHN.CheckedChanged
        If chk_iBAHN.Checked Then
            chk_GuestTek.Checked = False
            cmbBrand.Enabled = False

            clsGlobals.oHotelBrand.BrandGuestTek = False
            clsGlobals.oHotelBrand.BrandiBAHN = True
            clsGlobals.oHotelBrand.BrandNumber = 666
        Else
            clsGlobals.oHotelBrand.BrandiBAHN = False
            cmbBrand.Enabled = True
            clsGlobals.oHotelBrand.BrandNumber = cmbBrand.SelectedIndex
        End If

    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        clsGlobals.g_Address_Site = txtHotelAddress.Text
        clsGlobals.g_Url_Hotel = txtHotelinformation.Text
        clsGlobals.g_Url_Internet = txtInternet.Text
        clsGlobals.g_Url_Map = txtMap.Text
        clsGlobals.g_Url_Weather = txtWeather.Text

        clsGlobals.oLogger.WriteToLog("Update global config", , 0)

        clsGlobalSettings.WriteGlobalSettings()


        '------------------------------------------------------
        clsGlobals.oSkCfg = New SkCfg
        clsGlobals.oSkCfg.BackupFolder = clsGlobals.g_BackupDirectory

        clsGlobals.oLogger.WriteToLog("Update SiteKiosk config", , 0)
        clsGlobals.oSkCfg.Update(True)


        '------------------------------------------------------
        'Update payment dialog
        Try
            clsGlobals.oLogger.WriteToLog("Update payment dialog", , 0)
            If Not IO.File.Exists(clsGlobals.c_Path_Hilton__paymentdialog) Or Not IO.File.Exists(clsGlobals.c_Path_Default__paymentdialog) Then
                'Missing files, not touching it!
                If Not IO.File.Exists(clsGlobals.c_Path_Hilton__paymentdialog) Then
                    clsGlobals.oLogger.WriteToLog("missing file", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Hilton__paymentdialog, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End If
                If Not IO.File.Exists(clsGlobals.c_Path_Default__paymentdialog) Then
                    clsGlobals.oLogger.WriteToLog("missing file", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Default__paymentdialog, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End If
            Else
                clsGlobals.oLogger.WriteToLog("copying", , 1)
                If Not clsGlobals.oHotelBrand.BrandGuestTek And Not clsGlobals.oHotelBrand.BrandiBAHN Then
                    'Hilton
                    IO.File.Copy(clsGlobals.c_Path_Hilton__paymentdialog, clsGlobals.c_Path_SK__paymentdialog, True)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Hilton__paymentdialog & " => " & clsGlobals.c_Path_SK__paymentdialog, , 1)
                Else
                    'normal
                    IO.File.Copy(clsGlobals.c_Path_Default__paymentdialog, clsGlobals.c_Path_SK__paymentdialog, True)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Default__paymentdialog & " => " & clsGlobals.c_Path_SK__paymentdialog, , 1)
                End If
            End If
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            clsGlobals.oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        'done
        clsGlobals.oLogger.WriteToLog("Done", , 0)
        clsGlobals.oLogger.WriteToLog("bye", , 1)
        Me.Close()
    End Sub

    Private Sub btnDisabledClose_Click(sender As Object, e As EventArgs) Handles btnDisabledClose.Click
        Me.Close()
    End Sub

    Private Sub cmbBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBrand.SelectedIndexChanged
        clsGlobals.oHotelBrand.BrandNumber = cmbBrand.SelectedIndex
    End Sub
End Class
