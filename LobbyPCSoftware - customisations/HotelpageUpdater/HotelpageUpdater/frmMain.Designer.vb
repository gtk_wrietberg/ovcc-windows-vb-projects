﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnHotelInformation = New System.Windows.Forms.Button()
        Me.txtHotelinformation = New System.Windows.Forms.TextBox()
        Me.cmbBrand = New System.Windows.Forms.ComboBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.chk_GuestTek = New System.Windows.Forms.CheckBox()
        Me.chk_iBAHN = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtHotelAddress = New System.Windows.Forms.TextBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMap = New System.Windows.Forms.TextBox()
        Me.btnMap = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtWeather = New System.Windows.Forms.TextBox()
        Me.btnWeather = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtInternet = New System.Windows.Forms.TextBox()
        Me.btnInternet = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnlDisabled = New System.Windows.Forms.Panel()
        Me.btnDisabledClose = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.pnlDisabled.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnHotelInformation
        '
        Me.btnHotelInformation.Location = New System.Drawing.Point(502, 23)
        Me.btnHotelInformation.Name = "btnHotelInformation"
        Me.btnHotelInformation.Size = New System.Drawing.Size(75, 25)
        Me.btnHotelInformation.TabIndex = 0
        Me.btnHotelInformation.Text = "test"
        Me.btnHotelInformation.UseVisualStyleBackColor = True
        '
        'txtHotelinformation
        '
        Me.txtHotelinformation.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHotelinformation.Location = New System.Drawing.Point(7, 24)
        Me.txtHotelinformation.Name = "txtHotelinformation"
        Me.txtHotelinformation.Size = New System.Drawing.Size(489, 22)
        Me.txtHotelinformation.TabIndex = 1
        '
        'cmbBrand
        '
        Me.cmbBrand.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBrand.FormattingEnabled = True
        Me.cmbBrand.Location = New System.Drawing.Point(7, 24)
        Me.cmbBrand.Name = "cmbBrand"
        Me.cmbBrand.Size = New System.Drawing.Size(208, 24)
        Me.cmbBrand.TabIndex = 2
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(76, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(505, 54)
        Me.lblTitle.TabIndex = 3
        Me.lblTitle.Text = "OVCC Information Updater"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.lblTitle)
        Me.Panel1.Location = New System.Drawing.Point(12, 13)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(590, 60)
        Me.Panel1.TabIndex = 5
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImage = Global.OVCCInformationUpdater.My.Resources.Resources.guest_tek
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(70, 54)
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = False
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.pnlButtons)
        Me.pnlMain.Controls.Add(Me.Panel8)
        Me.pnlMain.Controls.Add(Me.Panel7)
        Me.pnlMain.Controls.Add(Me.Panel6)
        Me.pnlMain.Controls.Add(Me.Panel5)
        Me.pnlMain.Controls.Add(Me.Panel4)
        Me.pnlMain.Controls.Add(Me.Panel3)
        Me.pnlMain.Location = New System.Drawing.Point(12, 80)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(590, 448)
        Me.pnlMain.TabIndex = 6
        '
        'pnlButtons
        '
        Me.pnlButtons.BackColor = System.Drawing.Color.White
        Me.pnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlButtons.Controls.Add(Me.btnCancel)
        Me.pnlButtons.Controls.Add(Me.btnSave)
        Me.pnlButtons.Location = New System.Drawing.Point(3, 384)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(582, 57)
        Me.pnlButtons.TabIndex = 12
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(293, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(284, 48)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(3, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(284, 48)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.White
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.chk_GuestTek)
        Me.Panel8.Controls.Add(Me.chk_iBAHN)
        Me.Panel8.Controls.Add(Me.Label7)
        Me.Panel8.Controls.Add(Me.cmbBrand)
        Me.Panel8.Location = New System.Drawing.Point(3, 321)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(582, 57)
        Me.Panel8.TabIndex = 11
        '
        'chk_GuestTek
        '
        Me.chk_GuestTek.AutoSize = True
        Me.chk_GuestTek.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_GuestTek.Location = New System.Drawing.Point(303, 26)
        Me.chk_GuestTek.Name = "chk_GuestTek"
        Me.chk_GuestTek.Size = New System.Drawing.Size(82, 20)
        Me.chk_GuestTek.TabIndex = 9
        Me.chk_GuestTek.Text = "GuestTek"
        Me.chk_GuestTek.UseVisualStyleBackColor = True
        '
        'chk_iBAHN
        '
        Me.chk_iBAHN.AutoSize = True
        Me.chk_iBAHN.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_iBAHN.Location = New System.Drawing.Point(231, 26)
        Me.chk_iBAHN.Name = "chk_iBAHN"
        Me.chk_iBAHN.Size = New System.Drawing.Size(66, 20)
        Me.chk_iBAHN.TabIndex = 8
        Me.chk_iBAHN.Text = "iBAHN"
        Me.chk_iBAHN.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(98, 19)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Hotel brand"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.White
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.Controls.Add(Me.txtHotelAddress)
        Me.Panel7.Location = New System.Drawing.Point(3, 257)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(582, 57)
        Me.Panel7.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(115, 19)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Hotel address"
        '
        'txtHotelAddress
        '
        Me.txtHotelAddress.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHotelAddress.Location = New System.Drawing.Point(7, 24)
        Me.txtHotelAddress.Name = "txtHotelAddress"
        Me.txtHotelAddress.Size = New System.Drawing.Size(489, 22)
        Me.txtHotelAddress.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.Label5)
        Me.Panel6.Controls.Add(Me.txtMap)
        Me.Panel6.Controls.Add(Me.btnMap)
        Me.Panel6.Location = New System.Drawing.Point(3, 194)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(582, 57)
        Me.Panel6.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 19)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Map url"
        '
        'txtMap
        '
        Me.txtMap.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMap.Location = New System.Drawing.Point(7, 24)
        Me.txtMap.Name = "txtMap"
        Me.txtMap.Size = New System.Drawing.Size(489, 22)
        Me.txtMap.TabIndex = 1
        '
        'btnMap
        '
        Me.btnMap.Location = New System.Drawing.Point(502, 23)
        Me.btnMap.Name = "btnMap"
        Me.btnMap.Size = New System.Drawing.Size(75, 25)
        Me.btnMap.TabIndex = 0
        Me.btnMap.Text = "test"
        Me.btnMap.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.txtWeather)
        Me.Panel5.Controls.Add(Me.btnWeather)
        Me.Panel5.Location = New System.Drawing.Point(3, 130)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(582, 57)
        Me.Panel5.TabIndex = 11
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 19)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Weather url"
        '
        'txtWeather
        '
        Me.txtWeather.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeather.Location = New System.Drawing.Point(7, 24)
        Me.txtWeather.Name = "txtWeather"
        Me.txtWeather.Size = New System.Drawing.Size(489, 22)
        Me.txtWeather.TabIndex = 1
        '
        'btnWeather
        '
        Me.btnWeather.Location = New System.Drawing.Point(502, 23)
        Me.btnWeather.Name = "btnWeather"
        Me.btnWeather.Size = New System.Drawing.Size(75, 25)
        Me.btnWeather.TabIndex = 0
        Me.btnWeather.Text = "test"
        Me.btnWeather.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.White
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.txtInternet)
        Me.Panel4.Controls.Add(Me.btnInternet)
        Me.Panel4.Location = New System.Drawing.Point(3, 67)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(582, 57)
        Me.Panel4.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 19)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Internet url"
        '
        'txtInternet
        '
        Me.txtInternet.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInternet.Location = New System.Drawing.Point(7, 24)
        Me.txtInternet.Name = "txtInternet"
        Me.txtInternet.Size = New System.Drawing.Size(489, 22)
        Me.txtInternet.TabIndex = 1
        '
        'btnInternet
        '
        Me.btnInternet.Location = New System.Drawing.Point(502, 23)
        Me.btnInternet.Name = "btnInternet"
        Me.btnInternet.Size = New System.Drawing.Size(75, 25)
        Me.btnInternet.TabIndex = 0
        Me.btnInternet.Text = "test"
        Me.btnInternet.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.txtHotelinformation)
        Me.Panel3.Controls.Add(Me.btnHotelInformation)
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(582, 57)
        Me.Panel3.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(164, 19)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Hotel information url"
        '
        'pnlDisabled
        '
        Me.pnlDisabled.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.pnlDisabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlDisabled.Controls.Add(Me.btnDisabledClose)
        Me.pnlDisabled.Controls.Add(Me.Label2)
        Me.pnlDisabled.Location = New System.Drawing.Point(739, 513)
        Me.pnlDisabled.Name = "pnlDisabled"
        Me.pnlDisabled.Size = New System.Drawing.Size(582, 57)
        Me.pnlDisabled.TabIndex = 13
        '
        'btnDisabledClose
        '
        Me.btnDisabledClose.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisabledClose.Location = New System.Drawing.Point(453, 3)
        Me.btnDisabledClose.Name = "btnDisabledClose"
        Me.btnDisabledClose.Size = New System.Drawing.Size(123, 48)
        Me.btnDisabledClose.TabIndex = 9
        Me.btnDisabledClose.Text = "close"
        Me.btnDisabledClose.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(444, 48)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "You need to run this app as an administrator "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1362, 611)
        Me.Controls.Add(Me.pnlDisabled)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.Text = "OVCCInformationUpdater"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        Me.pnlButtons.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.pnlDisabled.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnHotelInformation As System.Windows.Forms.Button
    Friend WithEvents txtHotelinformation As System.Windows.Forms.TextBox
    Friend WithEvents cmbBrand As System.Windows.Forms.ComboBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtInternet As System.Windows.Forms.TextBox
    Friend WithEvents btnInternet As System.Windows.Forms.Button
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtHotelAddress As System.Windows.Forms.TextBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtMap As System.Windows.Forms.TextBox
    Friend WithEvents btnMap As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtWeather As System.Windows.Forms.TextBox
    Friend WithEvents btnWeather As System.Windows.Forms.Button
    Friend WithEvents pnlButtons As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents chk_GuestTek As System.Windows.Forms.CheckBox
    Friend WithEvents chk_iBAHN As System.Windows.Forms.CheckBox
    Friend WithEvents pnlDisabled As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnDisabledClose As System.Windows.Forms.Button

End Class
