﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("NewUI_Hilton__revision3_Patcher")> 
<Assembly: AssemblyDescription("NewUI_Hilton__revision3_Patcher")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("NewUI_Hilton__revision3_Patcher")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("3881c59a-2983-4783-9cf4-8c058c190c9e")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.5.1318")> 
<Assembly: AssemblyFileVersion("1.0.5.1318")> 
