Module Main
    Public Sub main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        '--------------
        Dim jsFile As String = ""

        If IO.File.Exists(JS_FILE__PATH_x86) Then
            jsFile = JS_FILE__PATH_x86
        ElseIf IO.File.Exists(JS_FILE__PATH) Then
            jsFile = JS_FILE__PATH
        Else
            oLogger.WriteToLog("File not found, bye")
            Exit Sub
        End If

        oLogger.WriteToLog("Updating file: " & jsFile)

        Dim lines As New List(Of String)
        Dim bUpdated As Boolean = False
        Try
            Using sr As New IO.StreamReader(jsFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("DeleteTempInternetFiles") Then
                        line = System.Text.RegularExpressions.Regex.Replace(line, "([\s]+)(SiteKiosk\.DeleteTempInternetFiles)(.+)", "$1//$2$3")
                        bUpdated = True
                    End If

                    lines.Add(line)
                End While
            End Using

            If bUpdated Then
                Using sw As New IO.StreamWriter(jsFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using

                oLogger.WriteToLog("Done")
            End If

        Catch ex As Exception

        End Try

        oLogger.WriteToLog("bye")
    End Sub
End Module
