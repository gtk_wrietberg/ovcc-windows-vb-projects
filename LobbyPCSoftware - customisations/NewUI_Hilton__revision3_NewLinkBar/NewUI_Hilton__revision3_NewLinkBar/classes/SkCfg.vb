Public Class SkCfg
    Private mOldSkCfgFile As String
    Private mNewSkCfgFile As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Sub Update(Optional ByVal bOverWriteOldFile As Boolean = False)
        Try
            oLogger.WriteToLogRelative("finding SiteKiosk install directory", , 1)
            mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            oLogger.WriteToLogRelative("found: " & mSiteKioskFolder, , 2)

            oLogger.WriteToLogRelative("finding active skcfg file", , 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            oLogger.WriteToLogRelative("found: " & mOldSkCfgFile, , 2)



            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            If bOverWriteOldFile Then
                oLogger.WriteToLogRelative("backing up current config", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
                oFile = New IO.FileInfo(mOldSkCfgFile)
                oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", True)

                oLogger.WriteToLogRelative("backup: " & mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", , 2)

                mNewSkCfgFile = mOldSkCfgFile
            Else
                oLogger.WriteToLogRelative("making copy", , 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)
                mNewSkCfgFile = oFile.DirectoryName & "\iBAHN_newUI_Hilton__" & sDateString & ".skcfg"
                oFile.CopyTo(mNewSkCfgFile, True)

                oLogger.WriteToLogRelative("copy: " & mNewSkCfgFile, , 2)
            End If


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If



            '*************************************************************************************************************
            'SiteCash - replace current url zones
            oLogger.WriteToLogRelative("SiteCash url zones update", , 2)
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode

            oLogger.WriteToLogRelative("loading SiteCash node", , 3)
            xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

            If Not xmlNode_SiteCash Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                Throw New Exception("SiteCash node not found")
            End If


            Dim xmlNodeList_UrlZones As Xml.XmlNodeList
            Dim xmlNode_UrlZone As Xml.XmlNode
            Dim xmlNodeList_Urls As Xml.XmlNodeList
            Dim xmlNode_Url As Xml.XmlNode
            Dim xmlNode_UrlClone As Xml.XmlNode
            Dim xmlNode_UrlCloneClone As Xml.XmlNode

            oLogger.WriteToLogRelative("removing existing 'No Charge (Hilton Linkbar)' url zone", , 3)
            xmlNodeList_UrlZones = xmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
            For Each xmlNode_UrlZone In xmlNodeList_UrlZones
                If xmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = "No Charge (Hilton Linkbar)" Then
                    xmlNode_SiteCash.RemoveChild(xmlNode_UrlZone)
                    oLogger.WriteToLogRelative("gone", , 4)
                End If
            Next

            oLogger.WriteToLogRelative("adding 'No Charge (Hilton Linkbar)' url zone", , 3)
            xmlNode_UrlZone = xmlNode_SiteCash.SelectSingleNode("sk:url-zone", ns).CloneNode(True)
            xmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = "No Charge (Hilton Linkbar)"

            xmlNodeList_Urls = xmlNode_UrlZone.SelectNodes("sk:url", ns)
            xmlNode_UrlClone = xmlNode_UrlZone.SelectSingleNode("sk:url", ns).CloneNode(True)
            For Each xmlNode_Url In xmlNodeList_Urls
                xmlNode_UrlZone.RemoveChild(xmlNode_Url)
            Next

            oLogger.WriteToLogRelative("adding: '*.hilton.com'", , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://*.hilton.com"
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://*.hilton.com"
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)

            oLogger.WriteToLogRelative("adding: '*.canopybyhilton.com'", , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://*.canopybyhilton.com"
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://*.canopybyhilton.com"
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)

            oLogger.WriteToLogRelative("adding: '*.hiltongrandvacations.com'", , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://*.hiltongrandvacations.com"
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://*.hiltongrandvacations.com"
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)


            xmlNode_SiteCash.AppendChild(xmlNode_UrlZone)


            '*************************************************************************************************************
            'Saving
            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Save(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)

            If Not bOverWriteOldFile Then
                oLogger.WriteToLogRelative("activating", , 2)
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

                Dim sTempRegVal As String
                sTempRegVal = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

                If sTempRegVal = mNewSkCfgFile Then
                    oLogger.WriteToLogRelative("ok", , 3)
                Else
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_WARNING, 3)

                    oLogger.WriteToLogRelative("trying to fix this by overwriting the existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 4)

                    oLogger.WriteToLogRelative("backing up existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mOldSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", True)

                    oLogger.WriteToLogRelative("overwriting existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mNewSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile, True)
                End If
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub
End Class
