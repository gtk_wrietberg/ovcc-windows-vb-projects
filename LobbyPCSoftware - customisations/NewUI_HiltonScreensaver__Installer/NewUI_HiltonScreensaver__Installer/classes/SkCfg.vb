Public Class SkCfg
    Private mSkCfgFile As String
    Private mBackupFolder As String

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Sub Update()
        Try
            oLogger.WriteToLogRelative("finding active skcfg file", , 1)
            mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            oLogger.WriteToLogRelative("found: " & mSkCfgFile, , 2)


            oLogger.WriteToLogRelative("backing up current config", , 1)
            mBackupFolder = Globals.g_BackupDirectory

            Dim oFile As System.IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            oFile = New System.IO.FileInfo(mSkCfgFile)

            Dim sBackupPath As String = oFile.DirectoryName
            Dim sBackupName As String = oFile.Name
            Dim sBackupFile As String
            Dim oDirInfo As System.IO.DirectoryInfo
            sBackupPath = sBackupPath.Replace("C:", "")
            sBackupPath = sBackupPath.Replace("c:", "")
            sBackupPath = sBackupPath.Replace("\\", "\")
            sBackupPath = mBackupFolder & "\" & sBackupPath

            sBackupFile = sBackupPath & "\" & sBackupName

            oLogger.WriteToLogRelative("backup: " & mSkCfgFile & " => " & sBackupFile, , 2)

            oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oFile = New System.IO.FileInfo(mSkCfgFile)
            oFile.CopyTo(sBackupFile, True)

            oLogger.WriteToLogRelative("ok", , 3)



            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(mSkCfgFile, , 3)
            xmlSkCfg.Load(mSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If


            '*************************************************************************************************************
            'Screensaver
            Dim xmlNode_Screensaver As Xml.XmlNode

            oLogger.WriteToLogRelative("Screensaver update", , 2)
            xmlNode_Screensaver = xmlNode_Root.SelectSingleNode("sk:screensaver", ns)

            oLogger.WriteToLogRelative("making sure screensaver is enabled", , 3)
            xmlNode_Screensaver.Attributes.GetNamedItem("enabled").InnerText = "true"

            oLogger.WriteToLogRelative("setting screensaver", , 3)
            xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = c_SCREENSAVER.Replace("%%PROGRAM FILES%%", g_ProgramFiles)


            '*************************************************************************************************************
            'Saving
            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(mSkCfgFile, , 3)
            xmlSkCfg.Save(mSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub
End Class
