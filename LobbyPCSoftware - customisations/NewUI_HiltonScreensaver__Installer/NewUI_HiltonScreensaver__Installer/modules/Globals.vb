Module Globals
    Public oLogger As Logger
    Public oComputerName As ComputerName

    Public oCopyFiles As CopyFiles
    Public oSkCfg As SkCfg

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String
    Public g_ProgramFiles As String

    Public g_TESTMODE As Boolean

    'Public ReadOnly c_SCREENSAVER As String = "file://C:\Program Files\SiteKiosk\Skins\Public\StartPages\Hilton\screensaver\main.html"
    Public ReadOnly c_SCREENSAVER As String = "file://%%PROGRAM FILES%%\SiteKiosk\Skins\Public\Startpages\Hilton\screensaver\02_Hilton_2017-March\main_swf.html"

    ' C:\Users\Wouter\Documents\iBAHN Projects\LobbyPC\LobbyPC Software - customisations\Theme Packs\Hilton screensaver\files\SiteKiosk\Skins\Public\Startpages\Hilton\screensaver\02_Hilton_2017-March
    ' C:\Users\Wouter\Documents\iBAHN Projects\LobbyPC\LobbyPC Software - customisations\Theme Packs\Hilton screensaver\files\
    'SiteKiosk\Skins\Public\Startpages\Hilton\screensaver\01_PF_Hilton_Screensaver_02A_150204


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_ProgramFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
        If g_ProgramFiles.Equals(String.Empty) Then
            g_ProgramFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        g_LogFileDirectory = g_ProgramFiles & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = g_ProgramFiles & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")



        Try
            System.IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try
        Try
            System.IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
