Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or _
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        oCopyFiles.DestinationDirectory = g_ProgramFiles
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Update SiteKiosk config", , 0)
        oSkCfg.Update()


        '-----------------------------------------------
        'SiteKiosk restart
        oLogger.WriteToLog("Restarting SiteKiosk", , 0)
        If g_TESTMODE Then
            oLogger.WriteToLog("not doing anything, we are in TESTMODE, remember?", , 1)
        Else
            Dim iCount As Integer = 0

            For Each p As Process In Process.GetProcessesByName("sitekiosk")
                Try
                    oLogger.WriteToLog("killing", , 1)
                    p.Kill()
                    oLogger.WriteToLog("ok", , 2)
                    iCount += 1
                Catch ex As Exception
                    iCount = -1
                End Try
            Next

            If iCount > 0 Then
                oLogger.WriteToLog("ok", , 1)
            ElseIf iCount = 0 Then
                oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            End If
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
