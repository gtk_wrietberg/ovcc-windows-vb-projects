Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------
        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, "--patch-script") > 0 Then
                g_PatchScript = True
            End If
            If InStr(arg, "--selected-theme:") > 0 Then
                g_SelectedTheme = arg.Replace("--selected-theme:", "")
            End If
        Next


        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or _
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        oLogger.WriteToLog("Patch script", , 0)
        If g_PatchScript Then
            PatchScriptFile()
        Else
            oLogger.WriteToLog("skipped", , 1)
        End If


        '------------------------------------------------------
        Dim bRestartSitekiosk As Boolean = False

        oLogger.WriteToLog("Set theme", , 0)
        If g_SelectedTheme.Length > 0 Then
            oLogger.WriteToLog("Selected theme", , 1)
            oLogger.WriteToLog(g_SelectedTheme, , 2)

            Dim _themePath As String = IO.Path.Combine(c_SK_THEME_BASE_PATH, g_SelectedTheme)

            If IO.Directory.Exists(_themePath) Then
                Dim _themeIndex As String = IO.Path.Combine(_themePath, "index.html")
                Dim _logoutIndex As String = IO.Path.Combine(_themePath, "logout.html")
                Dim _screensaverIndex As String = IO.Path.Combine(_themePath, "screensaver\index.html")

                If IO.File.Exists(_themeIndex) Then
                    Configuration.SkCfg.XML.Lock()
                    Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
                    Configuration.SkCfg.XML.Load()
                    Configuration.SkCfg.XML.SetValue(c_ThemeUrl, _themeIndex)
                    If IO.File.Exists(_logoutIndex) Then
                        Configuration.SkCfg.XML.SetValue(c_LogoutUrl, _logoutIndex)
                    Else
                        oLogger.WriteToLog("theme logout.html doesn't exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    End If
                    If IO.File.Exists(_screensaverIndex) Then
                        Configuration.SkCfg.XML.SetValue(c_ScreensaverUrl, _screensaverIndex)
                    Else
                        oLogger.WriteToLog("screensaver index.html doesn't exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    End If

                    Configuration.SkCfg.XML.Save()

                    bRestartSitekiosk = True
                Else
                    oLogger.WriteToLog("theme index.html doesn't exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                End If
            Else
                oLogger.WriteToLog("doesn't exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End If
        Else
            oLogger.WriteToLog("skipped (no theme selected)", , 1)
        End If


        '------------------------------------------------------
        If bRestartSitekiosk Then
            oLogger.WriteToLog("Killing SiteKiosk", , 0)
            KillSiteKiosk()
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
