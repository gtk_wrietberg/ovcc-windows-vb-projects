Module Globals
    Public oLogger As Logger
    Public oComputerName As ComputerName

    Public oCopyFiles As CopyFiles

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String

    Public g_TESTMODE As Boolean

    Public ReadOnly c_ScriptPatch As String = "iBAHN_functions.js.patch"
    Public ReadOnly c_SK_THEME_BASE_PATH As String = "C:\Program Files (x86)\SiteKiosk\Skins\Public\Startpages"
    Public g_PatchScript As Boolean = False
    Public g_SelectedTheme As String = ""

    Public ReadOnly c_LogoutUrl As String = "/browserbar/logout-navigation/url"
    Public ReadOnly c_ThemeUrl As String = "/startpageconfig/startpage"
    Public ReadOnly c_ScreensaverUrl As String = "/screensaver/url"


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(g_BackupDirectory)
    End Sub


End Module
