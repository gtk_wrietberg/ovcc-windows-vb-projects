Public Class SkCfg
    Private mOldSkCfgFile As String
    Private mNewSkCfgFile As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Sub Update(Optional ByVal bOverWriteOldFile As Boolean = False)
        Try
            oLogger.WriteToLogRelative("finding SiteKiosk install directory", , 1)
            mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            oLogger.WriteToLogRelative("found: " & mSiteKioskFolder, , 2)

            oLogger.WriteToLogRelative("finding active skcfg file", , 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            oLogger.WriteToLogRelative("found: " & mOldSkCfgFile, , 2)



            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            If bOverWriteOldFile Then
                oLogger.WriteToLogRelative("backing up current config", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
                oFile = New IO.FileInfo(mOldSkCfgFile)
                oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", True)

                oLogger.WriteToLogRelative("backup: " & mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", , 2)

                mNewSkCfgFile = mOldSkCfgFile
            Else
                oLogger.WriteToLogRelative("making copy", , 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)
                mNewSkCfgFile = oFile.DirectoryName & "\iBAHN_newUI_Hilton__" & sDateString & ".skcfg"
                oFile.CopyTo(mNewSkCfgFile, True)

                oLogger.WriteToLogRelative("copy: " & mNewSkCfgFile, , 2)
            End If


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            Dim xmlPatchReplacements As Xml.XmlDocument
            Dim xmlNode_Patch_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If


            '*************************************************************************************************************
            'Patch
            oLogger.WriteToLogRelative("loading replacement patches", , 2)
            xmlPatchReplacements = New Xml.XmlDocument
            oLogger.WriteToLogRelative(System.AppDomain.CurrentDomain.BaseDirectory & "\skcfg.replacements.xml", , 3)
            xmlPatchReplacements.Load(System.AppDomain.CurrentDomain.BaseDirectory & "\skcfg.replacements.xml")
            xmlNode_Patch_Root = xmlPatchReplacements.SelectSingleNode("sk:replacements", ns)

            If Not xmlNode_Patch_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If



            '*************************************************************************************************************
            'SiteSkin - remove current section and replace with patch
            oLogger.WriteToLogRelative("SiteSkin update", , 2)
            Dim xmlNode_SiteSkin As Xml.XmlNode

            oLogger.WriteToLogRelative("loading SiteSkin node", , 3)
            xmlNode_SiteSkin = xmlNode_Root.SelectSingleNode("sk:siteskin", ns)

            oLogger.WriteToLogRelative("removing current SiteSkin settings", , 3)
            xmlNode_SiteSkin.RemoveAll()

            Dim xmlNodeList_Patch_SiteSkin As Xml.XmlNodeList
            Dim xmlNode_Patch_SiteSkin As Xml.XmlNode

            oLogger.WriteToLogRelative("loading patch", , 3)
            xmlNode_Patch_SiteSkin = xmlNode_Patch_Root.SelectSingleNode("sk:siteskin", ns)
            xmlNodeList_Patch_SiteSkin = xmlNode_Patch_SiteSkin.ChildNodes

            oLogger.WriteToLogRelative("applying patch", , 3)
            Dim xmlNode_Patch_SiteSkin_SubNode As Xml.XmlNode
            Dim xmlNode_SiteSkinImport As Xml.XmlNode
            For Each xmlNode_Patch_SiteSkin_SubNode In xmlNodeList_Patch_SiteSkin
                xmlNode_SiteSkinImport = xmlSkCfg.ImportNode(xmlNode_Patch_SiteSkin_SubNode, True)
                xmlNode_SiteSkin.AppendChild(xmlNode_SiteSkinImport)
            Next


            '*************************************************************************************************************
            'StartpageConfig - remove current section and replace with patch
            oLogger.WriteToLogRelative("startpageconfig update", , 2)
            Dim xmlNode_StartpageConfig As Xml.XmlNode

            oLogger.WriteToLogRelative("loading startpageconfig node", , 3)
            xmlNode_StartpageConfig = xmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)

            oLogger.WriteToLogRelative("removing current startpageconfig settings", , 3)
            xmlNode_StartpageConfig.RemoveAll()

            Dim xmlNodeList_Patch_StartpageConfig As Xml.XmlNodeList
            Dim xmlNode_Patch_StartpageConfig As Xml.XmlNode

            oLogger.WriteToLogRelative("loading patch", , 3)
            xmlNode_Patch_StartpageConfig = xmlNode_Patch_Root.SelectSingleNode("sk:startpageconfig", ns)
            xmlNodeList_Patch_StartpageConfig = xmlNode_Patch_StartpageConfig.ChildNodes

            oLogger.WriteToLogRelative("applying patch", , 3)
            Dim xmlNode_Patch_StartpageConfig_SubNode As Xml.XmlNode
            Dim xmlNode_StartpageConfigImport As Xml.XmlNode
            For Each xmlNode_Patch_StartpageConfig_SubNode In xmlNodeList_Patch_StartpageConfig
                xmlNode_StartpageConfigImport = xmlSkCfg.ImportNode(xmlNode_Patch_StartpageConfig_SubNode, True)
                xmlNode_StartpageConfig.AppendChild(xmlNode_StartpageConfigImport)
            Next


            '*************************************************************************************************************
            'SiteCash - replace current url zones
            oLogger.WriteToLogRelative("SiteCash url zones update", , 2)
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode

            oLogger.WriteToLogRelative("loading SiteCash node", , 3)
            xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

            If Not xmlNode_SiteCash Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                Throw New Exception("SiteCash node not found")
            End If

            oLogger.WriteToLogRelative("making sure SiteCash is enabled", , 3)
            xmlNode_SiteCash.Attributes.GetNamedItem("enabled").InnerText = "true"

            oLogger.WriteToLogRelative("making applications free of charge", , 3)
            xmlNode_SiteCash.SelectSingleNode("sk:applicationprice", ns).Attributes.GetNamedItem("enabled").InnerText = "false"
            'xmlNode_SiteCash.SelectSingleNode("sk:applicationmultiplier", ns).Attributes.GetNamedItem("enabled").InnerText = "false"

            oLogger.WriteToLogRelative("disabling time warning", , 3)
            xmlNode_SiteCash.SelectSingleNode("sk:warning-seconds", ns).Attributes.GetNamedItem("enabled").InnerText = "false"

            oLogger.WriteToLogRelative("disabling progress bar", , 3)
            xmlNode_SiteCash.SelectSingleNode("sk:progressbar", ns).Attributes.GetNamedItem("display").InnerText = "false"


            Dim xmlNodeList_UrlZones As Xml.XmlNodeList
            Dim xmlNode_UrlZone As Xml.XmlNode
            Dim xmlNodeList_Urls As Xml.XmlNodeList
            Dim xmlNode_Url As Xml.XmlNode
            Dim xmlNode_UrlClone As Xml.XmlNode
            Dim xmlNode_UrlCloneClone As Xml.XmlNode

            oLogger.WriteToLogRelative("removing current url zones", , 3)
            xmlNodeList_UrlZones = xmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
            For Each xmlNode_UrlZone In xmlNodeList_UrlZones
                xmlNode_SiteCash.RemoveChild(xmlNode_UrlZone)
            Next

            Dim xmlNodeList_UrlZonesPatch As Xml.XmlNodeList
            Dim xmlNode_UrlZonesPatch As Xml.XmlNode
            oLogger.WriteToLogRelative("loading patch", , 3)
            xmlNodeList_UrlZonesPatch = xmlNode_Patch_Root.SelectSingleNode("sk:sitecash-plugin", ns).ChildNodes

            oLogger.WriteToLogRelative("applying patch", , 3)
            Dim xmlNode_SiteCashImport As Xml.XmlNode
            For Each xmlNode_UrlZonesPatch In xmlNodeList_UrlZonesPatch
                xmlNode_SiteCashImport = xmlSkCfg.ImportNode(xmlNode_UrlZonesPatch, True)
                xmlNode_SiteCash.AppendChild(xmlNode_SiteCashImport)
            Next

            oLogger.WriteToLogRelative("adding extra url zone", , 3)
            xmlNode_UrlZone = xmlNode_SiteCash.SelectSingleNode("sk:url-zone", ns).CloneNode(True)
            xmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = "No Charge (Extra)"

            xmlNodeList_Urls = xmlNode_UrlZone.SelectNodes("sk:url", ns)
            xmlNode_UrlClone = xmlNode_UrlZone.SelectSingleNode("sk:url", ns).CloneNode(True)
            For Each xmlNode_Url In xmlNodeList_Urls
                xmlNode_UrlZone.RemoveChild(xmlNode_Url)
            Next

            oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Hotel), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Hotel)
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Hotel)
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)

            oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Weather), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Weather)
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Weather)
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)

            oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Map), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Map)
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Map)
            xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)

            xmlNode_SiteCash.AppendChild(xmlNode_UrlZone)

            '*************************************************************************************************************
            'Printing
            Dim xmlNode_Printing As Xml.XmlNode

            oLogger.WriteToLogRelative("Printing update", , 2)
            xmlNode_Printing = xmlNode_Root.SelectSingleNode("sk:printing", ns)

            oLogger.WriteToLogRelative("enabling print button", , 3)
            xmlNode_Printing.SelectSingleNode("sk:show-print-button", ns).InnerText = "true"

            oLogger.WriteToLogRelative("setting print mode", , 3)
            xmlNode_Printing.SelectSingleNode("sk:mode", ns).InnerText = "1"

            oLogger.WriteToLogRelative("disabling print monitoring", , 3)
            xmlNode_Printing.SelectSingleNode("sk:enable-monitoring", ns).InnerText = "false"


            '*************************************************************************************************************
            'Screensaver
            Dim xmlNode_Screensaver As Xml.XmlNode

            oLogger.WriteToLogRelative("Screensaver update", , 2)
            xmlNode_Screensaver = xmlNode_Root.SelectSingleNode("sk:screensaver", ns)

            oLogger.WriteToLogRelative("making sure screensaver is enabled", , 3)
            xmlNode_Screensaver.Attributes.GetNamedItem("enabled").InnerText = "true"

            oLogger.WriteToLogRelative("setting screensaver", , 3)
            If IO.File.Exists("C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\Hilton\screensaver\main.html") Then
                xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\Hilton\screensaver\main.html"
            Else
                xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://C:\Program Files\SiteKiosk\Skins\Public\StartPages\Hilton\screensaver\main.html"
            End If


            '*************************************************************************************************************
            'Logout navigation
            Dim xmlNode_BrowserBar As Xml.XmlNode
            Dim xmlNode_Logout As Xml.XmlNode

            oLogger.WriteToLogRelative("Logout navigation update", , 2)
            xmlNode_BrowserBar = xmlNode_Root.SelectSingleNode("sk:browserbar", ns)
            xmlNode_Logout = xmlNode_BrowserBar.SelectSingleNode("sk:logout-navigation", ns)

            oLogger.WriteToLogRelative("making sure logout navigation is enabled", , 3)
            xmlNode_Logout.Attributes.GetNamedItem("enabled").InnerText = "true"

            oLogger.WriteToLogRelative("setting logout navigation", , 3)
            If IO.File.Exists("C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\Hilton\logout.html") Then
                xmlNode_Logout.SelectSingleNode("sk:url", ns).InnerText = "file://C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\Hilton\logout.html"
            Else
                xmlNode_Logout.SelectSingleNode("sk:url", ns).InnerText = "file://C:\Program Files\SiteKiosk\Skins\Public\StartPages\Hilton\logout.html"
            End If


            '*************************************************************************************************************
            'Saving
            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Save(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)

            If Not bOverWriteOldFile Then
                oLogger.WriteToLogRelative("activating", , 2)
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

                Dim sTempRegVal As String
                sTempRegVal = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

                If sTempRegVal = mNewSkCfgFile Then
                    oLogger.WriteToLogRelative("ok", , 3)
                Else
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_WARNING, 3)

                    oLogger.WriteToLogRelative("trying to fix this by overwriting the existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 4)

                    oLogger.WriteToLogRelative("backing up existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mOldSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", True)

                    oLogger.WriteToLogRelative("overwriting existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mNewSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile, True)
                End If
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub
End Class
