﻿Imports System.Linq

Public Class Configuration
    Public Class SkCfg
        Public Class XML
            Private Shared ReadOnly SK_NAMESPACE As String = "sk"
            Private Shared ReadOnly DT_NAMESPACE As String = "dt"

            Private Shared ReadOnly LOCK_TIMEOUT_MAX_COUNT As Integer = 20

            Public Shared Document As System.Xml.XmlDocument
            Public Shared Node_Root As System.Xml.XmlNode
            Private Shared NameTable As System.Xml.XmlNameTable
            Private Shared NamespaceManager As System.Xml.XmlNamespaceManager

            Public Shared FilePath As String = ""
            Public Shared LastError As String = ""

            Private Shared mNeedsSaving As Boolean = False

            Private Shared mLoaded As Boolean = False
            Private Shared mIsLocked As Boolean = False
            Private Shared mIsLockedBy As String = ""


            Public Shared ReadOnly Property HasError() As Boolean
                Get
                    Return Not LastError.Equals("")
                End Get
            End Property

            Public Shared ReadOnly Property IsLoaded() As Boolean
                Get
                    Return mLoaded
                End Get
            End Property

            Public Shared ReadOnly Property IsLocked() As Boolean
                Get
                    Return mIsLocked
                End Get
            End Property

            Public Shared ReadOnly Property IsLockedBy() As String
                Get
                    Return mIsLockedBy
                End Get
            End Property

            Public Shared Sub Lock()
                Lock("")
            End Sub

            Public Shared Sub Lock(Caller As String)
                mIsLocked = True
                mIsLockedBy = Caller
            End Sub

            Public Shared Sub Unlock()
                mIsLocked = False
                mIsLockedBy = ""
            End Sub

            Public Shared ReadOnly Property NeedsSaving() As Boolean
                Get
                    Return mNeedsSaving
                End Get
            End Property

            Public Shared Function WaitForLock() As Boolean
                Dim iTimeoutCounter As Integer = 0

                Do
                    If Not mIsLocked Then
                        Return True
                    End If

                    Threading.Thread.Sleep(100)

                    iTimeoutCounter += 1
                Loop While iTimeoutCounter < LOCK_TIMEOUT_MAX_COUNT

                Return False
            End Function

            Public Shared Function Load() As Boolean
                mIsLocked = True

                If mLoaded Then
                    Return True
                End If

                Try
                    Document = New System.Xml.XmlDocument
                    Document.Load(FilePath)

                    NameTable = Document.NameTable
                    NamespaceManager = New System.Xml.XmlNamespaceManager(NameTable)
                    NamespaceManager.AddNamespace(SK_NAMESPACE, "urn:schemas-sitekiosk-com:configuration")
                    NamespaceManager.AddNamespace(DT_NAMESPACE, "urn:schemas-microsoft-com:datatypes")

                    Node_Root = Document.SelectSingleNode("sk:sitekiosk-configuration", NamespaceManager)

                    If Node_Root Is Nothing Then
                        Throw New Exception("XmlNodeRoot Is Nothing")
                    End If

                    mLoaded = True
                Catch ex As Exception
                    LastError = ex.Message

                    Unlock()

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function Save() As Boolean
                If mNeedsSaving Then
                    Try
                        Document.Save(FilePath)

                        mLoaded = False
                    Catch ex As Exception
                        LastError = ex.Message

                        Return False
                    End Try
                End If

                Unlock()

                Return True
            End Function

            Public Shared Function GetValue(sPath As String, ByRef sValue As String) As Boolean
                Try
                    Dim mXmlNode As System.Xml.XmlNode

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)
                    sValue = mXmlNode.InnerText
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function SetValue(sPath As String, ByVal sValue As String) As Boolean
                Try
                    Dim sCurrentValue As String = ""

                    GetValue(sPath, sCurrentValue)

                    If Not sCurrentValue.Equals(sValue) Then
                        mNeedsSaving = True
                    End If
                Catch ex As Exception

                End Try

                Try
                    Dim mXmlNode As System.Xml.XmlNode

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)
                    mXmlNode.InnerText = sValue
                Catch ex As Exception
                    LastError = "SetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function AddValue(sPath As String, sValueName As String, sValue As String) As Boolean
                Try
                    Dim mXmlNode As System.Xml.XmlNode
                    Dim mXmlNodeList As System.Xml.XmlNodeList
                    Dim bAlreadyThere As Boolean = False
                    Dim sFullPath As String = ""

                    sFullPath = sPath & "/" & sValueName

                    sPath = _AddNamespaceToXPath(sPath)
                    sFullPath = _AddNamespaceToXPath(sFullPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)
                    mXmlNodeList = Node_Root.SelectNodes(sFullPath, NamespaceManager)

                    For Each mXmlSubNode As System.Xml.XmlNode In mXmlNodeList
                        If mXmlSubNode.InnerText.Equals(sValue) Then
                            bAlreadyThere = True
                            Exit For
                        End If
                    Next


                    If Not bAlreadyThere Then
                        Dim mXmlNode_NewNode As System.Xml.XmlNode

                        mXmlNode_NewNode = Document.CreateNode(System.Xml.XmlNodeType.Element, sValueName, NamespaceManager.LookupNamespace(SK_NAMESPACE))
                        mXmlNode_NewNode.InnerText = sValue

                        mXmlNode.AppendChild(mXmlNode_NewNode)

                        mNeedsSaving = True
                    End If


                    Return True
                Catch ex As Exception
                    LastError = "SetValue: " & ex.Message

                    Return False
                End Try
            End Function

            Public Shared Function GetAttribute(sPath As String, sAttributeName As String, ByRef sAttributeValue As String) As Boolean
                Try
                    Dim mXmlNode As System.Xml.XmlNode

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)

                    sAttributeValue = mXmlNode.Attributes.GetNamedItem(sAttributeName).Value
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function GetAttribute(sPath As String, sSearchAttributeName As String, sSearchAttributeValue As String, sAttributeName As String, ByRef sAttributeValue As String) As Boolean
                Try
                    Dim mXmlNodeList As System.Xml.XmlNodeList

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNodeList = Node_Root.SelectNodes(sPath, NamespaceManager)

                    For Each mXmlNode As System.Xml.XmlNode In mXmlNodeList
                        If Not mXmlNode.Attributes.GetNamedItem(sSearchAttributeName) Is Nothing Then
                            If mXmlNode.Attributes.GetNamedItem(sSearchAttributeName).Value.Equals(sSearchAttributeValue) Then
                                sAttributeValue = mXmlNode.Attributes.GetNamedItem(sAttributeName).Value

                                Return True
                            End If
                        End If
                    Next
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message
                End Try

                Return False
            End Function

            Public Shared Function SetAttribute(sPath As String, sAttributeName As String, ByVal sAttributeValue As String) As Boolean
                Try
                    Dim mXmlNode As System.Xml.XmlNode

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)

                    If mXmlNode.Attributes.GetNamedItem(sAttributeName) Is Nothing Then
                        Dim mXmlAttribute As System.Xml.XmlAttribute

                        mXmlAttribute = Document.CreateAttribute(sAttributeName)
                        mXmlAttribute.Value = sAttributeValue

                        mXmlNode.Attributes.SetNamedItem(mXmlAttribute)

                        mNeedsSaving = True
                    Else
                        Dim sCurrentAttributeValue As String = mXmlNode.Attributes.GetNamedItem(sAttributeName).Value

                        If Not sAttributeValue.Equals(sCurrentAttributeValue) Then
                            mXmlNode.Attributes.GetNamedItem(sAttributeName).Value = sAttributeValue

                            mNeedsSaving = True
                        End If
                    End If
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function SetAttribute(sPath As String, sSearchAttributeName As String, sSearchAttributeValue As String, sAttributeName As String, ByVal sAttributeValue As String) As Boolean
                Dim bRet As Boolean = False

                Try
                    Dim mXmlNodeList As System.Xml.XmlNodeList

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNodeList = Node_Root.SelectNodes(sPath, NamespaceManager)

                    For Each mXmlNode As System.Xml.XmlNode In mXmlNodeList
                        If Not mXmlNode.Attributes.GetNamedItem(sSearchAttributeName) Is Nothing Then
                            If mXmlNode.Attributes.GetNamedItem(sSearchAttributeName).Value.Equals(sSearchAttributeValue) Then
                                If mXmlNode.Attributes.GetNamedItem(sAttributeName) Is Nothing Then
                                    Dim mXmlAttribute As System.Xml.XmlAttribute

                                    mXmlAttribute = Document.CreateAttribute(sAttributeName)
                                    mXmlAttribute.Value = sAttributeValue

                                    mXmlNode.Attributes.SetNamedItem(mXmlAttribute)

                                    mNeedsSaving = True
                                Else
                                    Dim sCurrentAttributeValue As String = mXmlNode.Attributes.GetNamedItem(sAttributeName).Value

                                    If Not sAttributeValue.Equals(sCurrentAttributeValue) Then
                                        mXmlNode.Attributes.GetNamedItem(sAttributeName).Value = sAttributeValue

                                        mNeedsSaving = True
                                    End If
                                End If

                                bRet = True
                                Exit For
                            End If
                        End If
                    Next
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return bRet
                End Try

                Return bRet
            End Function

            Private Shared Function _AddNamespaceToXPath(sPath As String) As String
                Dim aPath As String() = sPath.Split("/")

                For i As Integer = 0 To aPath.Length - 1
                    If Not aPath(i).StartsWith(SK_NAMESPACE & ":") And Not aPath(i).Equals("") Then
                        aPath(i) = SK_NAMESPACE & ":" & aPath(i)
                    End If
                Next
                sPath = String.Join("/", aPath.Skip(1).ToArray())

                Return sPath
            End Function
        End Class
    End Class
End Class
