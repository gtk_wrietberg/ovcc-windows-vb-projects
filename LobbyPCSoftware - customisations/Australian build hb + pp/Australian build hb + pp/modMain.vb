Module modMain
    Private Const c_REG_LPCAGENT As String = "HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPCAgent"
    Private Const c_URL_HBSERVER As String = "https://lpchbserver02.ibahn.com/axis/servlet/"

    Private oLogger As Logger
    Private oComputerName As ComputerName

    Public Sub Main()
        oLogger = New Logger
        oLogger.LogFilePath = "C:\Program Files\iBAHN\"
        oLogger.LogFileName = "AU_cust.log"

        oComputerName = New ComputerName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        oLogger.WriteToLog(New String("-", 10))

        oLogger.WriteToLog("Update registry", , 0)
        oLogger.WriteToLog(c_REG_LPCAGENT & "!HeartBeatServerURL", , 1)
        oLogger.WriteToLog(c_URL_HBSERVER & "HeartbeatReceiverServlet", , 2)
        Microsoft.Win32.Registry.SetValue(c_REG_LPCAGENT, "HeartBeatServerURL", c_URL_HBSERVER & "HeartbeatReceiverServlet", Microsoft.Win32.RegistryValueKind.String)

        oLogger.WriteToLog(c_REG_LPCAGENT & "!LogServerURL", , 1)
        oLogger.WriteToLog(c_URL_HBSERVER & "LogfileLineReceiver", , 2)
        Microsoft.Win32.Registry.SetValue(c_REG_LPCAGENT, "LogServerURL", c_URL_HBSERVER & "LogfileLineReceiver", Microsoft.Win32.RegistryValueKind.String)


        oLogger.WriteToLog("Update ibahn.js", , 0)
        Dim sSiteKioskLocation As String = ""
        sSiteKioskLocation = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        oLogger.WriteToLog("found SiteKiosk folder", , 1)
        oLogger.WriteToLog(sSiteKioskLocation, , 2)

        If IO.Directory.Exists(sSiteKioskLocation) Then
            oLogger.WriteToLog("preparing new file", , 1)
            FindReplaceString("ibahn_template.js", "%%LocationID%%", oComputerName.ComputerName)

            Try
                oLogger.WriteToLog("copying new file", , 1)
                oLogger.WriteToLog("source", , 2)
                oLogger.WriteToLog("ibahn_template.js", , 3)
                oLogger.WriteToLog("destination", , 2)
                oLogger.WriteToLog(sSiteKioskLocation & "skins\default\scripts\ibahn.js", , 3)

                IO.File.Copy("ibahn_template.js", sSiteKioskLocation & "skins\default\scripts\ibahn.js", True)
            Catch ex As Exception
                oLogger.WriteToLog("Copy failed!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        Else
            oLogger.WriteToLog("SiteKiosk folder not found!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Sub FindReplaceString(ByVal fileName As String, ByVal oldValue As String, ByVal newValue As String)
        Using fs As New IO.FileStream(fileName, IO.FileMode.Open, IO.FileAccess.ReadWrite)

            Dim sr As New IO.StreamReader(fs)

            Dim fileContent As String = sr.ReadToEnd()
            fs.SetLength(0)
            sr.Close()

            fileContent = fileContent.Replace(oldValue, newValue)

            Dim sw As New IO.StreamWriter(fileName)
            sw.Write(fileContent)
            sw.Flush()
            sw.Close()
        End Using
    End Sub
End Module
