Module Globals
    Public oLogger As Logger
    Public oComputerName As ComputerName

    Public oCopyFiles As CopyFiles
    Public oSkCfg As SkCfg

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupBaseDirectory As String
    Public g_BackupDirectory As String
    Public g_BackupDate As String

    Public g_TESTMODE As Boolean

    Public g_OverwriteExistingConfig As Boolean

    Public g_BackupEntireSiteKioskFolder As Boolean

    Public g_Url_Hotel As String = "http://www.ibahn_.com"
    Public g_Url_Internet As String = "http://www.google.com"
    Public g_Url_Weather As String = "http://www.weather.com"
    Public g_Url_Map As String = "http://maps.google.com"
    Public g_Address_Site As String = "iBAHN, Logie Court, Stirling University Inovation Park, Stirling, United Kingdom"
    Public g_Brand_Hotel As String = "0"

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_BackupDate = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupBaseDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_backups"
        g_BackupDirectory = g_BackupBaseDirectory & "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & g_BackupDate

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(g_BackupDirectory)

        g_OverwriteExistingConfig = False
        g_BackupEntireSiteKioskFolder = False
    End Sub


End Module
