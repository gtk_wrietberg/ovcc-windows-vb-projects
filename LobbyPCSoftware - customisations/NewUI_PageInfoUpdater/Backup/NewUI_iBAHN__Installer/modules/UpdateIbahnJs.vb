Module UpdateIbahnJs
    Public Function UpdateIbahnJsScriptFile() As Boolean
        Dim line As String
        Dim lines As New List(Of String)
        Dim lines2 As New List(Of String)
        Dim lines3 As New List(Of String)
        Dim patchlines_UIfunctions As New List(Of String)
        Dim patchlines_Airlines As New List(Of String)
        Dim bPatchFound_UIfunctions As Boolean = False
        Dim bPatchFound_Airlines As Boolean = False

        oLogger.WriteToLogRelative("loading files", , 1)

        Dim sFile_iBAHNJS As String
        sFile_iBAHNJS = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile_iBAHNJS &= "\Skins\default\scripts\ibahn.js"

        oLogger.WriteToLogRelative("ibahn.js", , 2)
        oLogger.WriteToLogRelative(sFile_iBAHNJS, , 3)

        If Not IO.File.Exists(sFile_iBAHNJS) Then
            oLogger.WriteToLogRelative("not found", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            Return False
        End If

        oLogger.WriteToLogRelative("ok", , 4)


        oLogger.WriteToLogRelative("loading patch for UI functions", , 1)
        Dim sPatchFile_UIfunctions As String
        sPatchFile_UIfunctions = System.AppDomain.CurrentDomain.BaseDirectory & "\ibahn.js.patch.UIfunctions"

        oLogger.WriteToLogRelative(sPatchFile_UIfunctions, , 2)

        If Not IO.File.Exists(sPatchFile_UIfunctions) Then
            oLogger.WriteToLogRelative("not found", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End If

        oLogger.WriteToLogRelative("ok", , 3)

        oLogger.WriteToLogRelative("loading contents", , 2)
        Try
            Using sr As New IO.StreamReader(sPatchFile_UIfunctions)
                While Not sr.EndOfStream
                    line = sr.ReadLine

                    patchlines_UIfunctions.Add(line)
                End While
            End Using
        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            Return False
        End Try


        oLogger.WriteToLogRelative("loading patch for Airlines", , 1)
        Dim sPatchFile_Airlines As String
        sPatchFile_Airlines = System.AppDomain.CurrentDomain.BaseDirectory & "\ibahn.js.patch.Airlines"

        oLogger.WriteToLogRelative(sPatchFile_Airlines, , 2)

        If Not IO.File.Exists(sPatchFile_Airlines) Then
            oLogger.WriteToLogRelative("not found", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End If

        oLogger.WriteToLogRelative("ok", , 3)

        oLogger.WriteToLogRelative("loading patch", , 2)
        Try
            Using sr As New IO.StreamReader(sPatchFile_Airlines)
                While Not sr.EndOfStream
                    line = sr.ReadLine

                    patchlines_Airlines.Add(line)
                End While
            End Using
        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            Return False
        End Try




        Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

        sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        oLogger.WriteToLogRelative("creating backup", , 1)
        oFile = New IO.FileInfo(sFile_iBAHNJS)
        oFile.CopyTo(sFile_iBAHNJS & "." & sDateString & ".iBAHNnewUI.backup", True)

        oLogger.WriteToLogRelative(sFile_iBAHNJS & "." & sDateString & ".iBAHNnewUI.backup", , 2)


        Dim linecount As Integer = 0

        oLogger.WriteToLogRelative("read current file", , 1)
        Using sr As New IO.StreamReader(sFile_iBAHNJS)
            While Not sr.EndOfStream
                line = sr.ReadLine
                lines.Add(line)
            End While
        End Using

        oLogger.WriteToLogRelative("applying patch", , 1)
        Try
            For Each line In lines
                linecount += 1

                If line.Contains("//New UI 2012 - start") Then
                    oLogger.WriteToLogRelative("existing patch found (line " & linecount & "), will overwrite", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                    bPatchFound_UIfunctions = True
                End If

                If Not bPatchFound_UIfunctions Then
                    lines2.Add(line)
                End If

                If line.Contains("//New UI 2012 - end") Then
                    oLogger.WriteToLogRelative("existing patch ended (line " & linecount & ")", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                    bPatchFound_UIfunctions = False
                End If
            Next

            oLogger.WriteToLogRelative("applying airline patch", , 1)
            Dim skiplinecount As Integer = 30
            Dim bPatchAdded As Boolean = False

            linecount = 0
            For Each line In lines2
                linecount += 1

                If line.Contains("var FAAenabled=") Then
                    oLogger.WriteToLogRelative("existing airlines section found (line " & linecount & "), will overwrite", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                    bPatchFound_Airlines = True
                End If

                If bPatchFound_Airlines And skiplinecount <= 0 Then
                    If line.Length > 12 And Not line.Contains("FAAwebsites[") Then
                        oLogger.WriteToLogRelative("existing airlines section ended (line " & linecount & ")", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                        bPatchFound_Airlines = False
                    End If
                End If

                If Not bPatchFound_Airlines Then
                    lines3.Add(line)
                Else
                    If Not bPatchAdded Then
                        lines3.AddRange(patchlines_Airlines)
                        bPatchAdded = True
                    End If
                    skiplinecount -= 1
                End If
            Next

            Using sw As New IO.StreamWriter(sFile_iBAHNJS)
                For Each line In lines3
                    sw.WriteLine(line)
                Next

                For Each line In patchlines_UIfunctions
                    sw.WriteLine(line)
                Next
            End Using

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        End Try

        oLogger.WriteToLogRelative("done", , 1)
    End Function
End Module
