Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        Dim arg As String, arg_index As Integer
        For arg_index = 1 To Environment.GetCommandLineArgs.Length - 1
            arg = Environment.GetCommandLineArgs(arg_index)

            oLogger.WriteToLog("found: " & arg, , 1)

            If arg = "--overwrite-existing-config" Then
                g_OverwriteExistingConfig = True
            End If

            If arg = "--backup-entire-folder" Then
                g_BackupEntireSiteKioskFolder = True
            End If
        Next



        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or _
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        If g_BackupEntireSiteKioskFolder Then
            oCopyFiles = New CopyFiles
            oCopyFiles.BackupDirectory = g_BackupBaseDirectory & "\_SkFB_\" & g_BackupDate

            If IO.Directory.Exists("C:\Program Files (x86)\") Then
                oCopyFiles.SourceDirectory = "C:\Program Files (x86)\SiteKiosk"
            Else
                oCopyFiles.SourceDirectory = "C:\Program Files\SiteKiosk"
            End If
            oCopyFiles.DestinationDirectory = g_BackupBaseDirectory & "\SiteKioskFullBackup\" & g_BackupDate

            oLogger.WriteToLog("Copying SiteKiosk files", , 0)
            oCopyFiles.CopyFiles()
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()

        '------------------------------------------------------
        oLogger.WriteToLog("Update global settings", , 0)
        UpdateGloballSettings()

        '------------------------------------------------------
        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Update SiteKiosk config", , 0)
        oSkCfg.Update(g_OverwriteExistingConfig)


        '------------------------------------------------------
        oLogger.WriteToLog("Updating ibahn.js script file", , 0)
        UpdateIbahnJsScriptFile()


        '------------------------------------------------------
        oLogger.WriteToLog("Killing SiteKiosk", , 0)
        KillSiteKiosk()

        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
