Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------
        g_CompatibilityView_On = False
        g_CompatibilityView_Set = False

        g_OverwriteExistingConfig = False

        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, "--comp-view:on") > 0 Then
                g_CompatibilityView_On = True
                g_CompatibilityView_Set = True
            End If
            If InStr(arg, "--comp-view:off") > 0 Then
                g_CompatibilityView_On = False
                g_CompatibilityView_Set = True
            End If
            If arg = "--overwrite-existing-config" Then
                g_OverwriteExistingConfig = True
            End If
        Next


        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or _
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        oLogger.WriteToLog("Setting CompatibilityView in SkCfg", , 0)
        If g_CompatibilityView_Set Then
            oSkCfg = New SkCfg
            oSkCfg.BackupFolder = g_BackupDirectory

            oLogger.WriteToLog("Update SiteKiosk config", , 1)
            oSkCfg.Update(g_OverwriteExistingConfig)
        Else
            oLogger.WriteToLog("skipped", , 1)
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Killing SiteKiosk", , 0)
        KillSiteKiosk()


        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
