Public Class SkCfg
    Private mOldSkCfgFile As String
    Private mNewSkCfgFile As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Sub Update(Optional ByVal bOverWriteOldFile As Boolean = False)
        Try
            oLogger.WriteToLogRelative("finding SiteKiosk install directory", , 1)
            mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            oLogger.WriteToLogRelative("found: " & mSiteKioskFolder, , 2)

            oLogger.WriteToLogRelative("finding active skcfg file", , 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            oLogger.WriteToLogRelative("found: " & mOldSkCfgFile, , 2)


            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            If bOverWriteOldFile Then
                oLogger.WriteToLogRelative("backing up current config", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
                oFile = New IO.FileInfo(mOldSkCfgFile)
                oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", True)

                oLogger.WriteToLogRelative("backup: " & mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", , 2)

                mNewSkCfgFile = mOldSkCfgFile
            Else
                oLogger.WriteToLogRelative("making copy", , 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)
                mNewSkCfgFile = oFile.DirectoryName & "\iBAHN_newUI_Hilton__" & sDateString & ".skcfg"
                oFile.CopyTo(mNewSkCfgFile, True)

                oLogger.WriteToLogRelative("copy: " & mNewSkCfgFile, , 2)
            End If


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            Dim mConfigChanged As Boolean = False

            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If


            '*************************************************************************************************************
            'IE Compatibility
            Dim xmlNode_Global As Xml.XmlNode
            Dim xmlNode_IE8CompatibilityView As Xml.XmlNode

            oLogger.WriteToLog("global node", , 1)
            xmlNode_Global = xmlNode_Root.SelectSingleNode("sk:global", ns)
            If Not xmlNode_Global Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_Global Is Nothing")
            End If

            oLogger.WriteToLog("ie8-compatibility-view node", , 1)
            xmlNode_IE8CompatibilityView = xmlNode_Global.SelectSingleNode("sk:ie8-compatibility-view", ns)
            If Not xmlNode_IE8CompatibilityView Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_IE8CompatibilityView Is Nothing")
            End If

            Dim sCurrentValue As String, bCurrentValue As Boolean

            sCurrentValue = xmlNode_IE8CompatibilityView.Attributes.GetNamedItem("enabled").InnerText
            oLogger.WriteToLog("current value", , 2)
            oLogger.WriteToLog(sCurrentValue, , 3)
            bCurrentValue = sCurrentValue.Equals("true", StringComparison.OrdinalIgnoreCase)
            If g_CompatibilityView_On <> bCurrentValue Then
                If g_CompatibilityView_On Then
                    xmlNode_IE8CompatibilityView.Attributes.GetNamedItem("enabled").InnerText = "true"
                    oLogger.WriteToLog("updated to", , 2)
                    oLogger.WriteToLog("true", , 3)
                Else
                    xmlNode_IE8CompatibilityView.Attributes.GetNamedItem("enabled").InnerText = "false"
                    oLogger.WriteToLog("updated to", , 2)
                    oLogger.WriteToLog("false", , 3)
                End If

                mConfigChanged = True
            Else
                oLogger.WriteToLog("no updated needed", , 2)
            End If



            '*************************************************************************************************************
            'Saving
            oLogger.WriteToLogRelative("saving", , 2)
            If mConfigChanged Then
                oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
                xmlSkCfg.Save(mNewSkCfgFile)
                oLogger.WriteToLogRelative("ok", , 4)

                If Not bOverWriteOldFile Then
                    oLogger.WriteToLogRelative("activating", , 2)
                    Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

                    Dim sTempRegVal As String
                    sTempRegVal = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

                    If sTempRegVal = mNewSkCfgFile Then
                        oLogger.WriteToLogRelative("ok", , 3)
                    Else
                        oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_WARNING, 3)

                        oLogger.WriteToLogRelative("trying to fix this by overwriting the existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 4)

                        oLogger.WriteToLogRelative("backing up existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                        oFile = New IO.FileInfo(mOldSkCfgFile)
                        oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", True)

                        oLogger.WriteToLogRelative("overwriting existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                        oFile = New IO.FileInfo(mNewSkCfgFile)
                        oFile.CopyTo(mOldSkCfgFile, True)
                    End If
                End If
            Else
                oLogger.WriteToLogRelative("skipped, nothing was changed", , 3)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub
End Class
