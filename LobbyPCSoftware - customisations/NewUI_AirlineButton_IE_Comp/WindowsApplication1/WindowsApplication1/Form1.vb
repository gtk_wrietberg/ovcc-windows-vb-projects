﻿Imports System.Globalization
Imports System.Threading


Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim culture As CultureInfo

        'culture = CultureInfo.CreateSpecificCulture("en-GB")
        'culture = CultureInfo.InvariantCulture
        'IFormatProvider provider = CultureInfo.InvariantCulture;

        Dim d1 As Double = 0, d2 As Double = 0, d3 As Double = 0, d4 As Double = 0
        Dim s1 As String, s2 As String, s3 As String, s4 As String
        Dim result As String = ""

        s1 = TextBox1.Text
        s2 = TextBox1.Text.Replace(",", ".")
        s3 = s1
        s4 = s2

        culture = CultureInfo.CreateSpecificCulture("en-GB")
        result += "culture: " + culture.Name
        result += vbCrLf
        result += vbCrLf
        Try
            d1 = Double.Parse(s1, NumberStyles.Float, culture)
            result += "s1 = " + s1 + " _ d1 = " + d1.ToString
        Catch ex As Exception
            result += "s1 = " + s1 + " _ d1 = " + ex.Message
        End Try
        result += vbCrLf

        Try
            d2 = Double.Parse(s2, NumberStyles.Float, culture)
            result += "s2 = " + s2 + " _ d2 = " + d2.ToString
        Catch ex As Exception
            result += "s2 = " + s2 + " _ d2 = " + ex.Message
        End Try
        result += vbCrLf

        result += vbCrLf


        culture = CultureInfo.CreateSpecificCulture("fr-FR")
        result += "culture: " + culture.Name
        result += vbCrLf
        result += vbCrLf
        Try
            d3 = Double.Parse(s3, NumberStyles.Float, culture)
            result += "s3 = " + s3 + " _ d3 = " + d3.ToString
        Catch ex As Exception
            result += "s3 = " + s3 + " _ " + ex.Message
        End Try
        result += vbCrLf


        Try
            d4 = Double.Parse(s4, NumberStyles.Float, culture)
            result += "s4 = " + s4 + " _ d4 = " + d4.ToString
        Catch ex As Exception
            result += "s4 = " + s4 + " _ d4 = " + ex.Message
        End Try
        result += vbCrLf


        MessageBox.Show(result)


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim culture As CultureInfo
        Dim t As String = TextBox1.Text.Replace(",", ".")
        Dim d As Double = 0
        Dim result As String = ""

        culture = CultureInfo.CreateSpecificCulture("en-GB")


        Try
            d = Double.Parse(t, NumberStyles.Float, culture)
            result += "t = " + t + " _ d = " + d.ToString
        Catch ex As Exception
            result += "t = " + t + " _ d = " + ex.Message
        End Try

        MessageBox.Show(result)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Thread.CurrentThread.CurrentCulture = New CultureInfo("fr-FR")
    End Sub
End Class
