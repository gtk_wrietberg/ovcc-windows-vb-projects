Module UpdateScriptFile
    Public Function PatchScriptFile() As Boolean
        Dim lines As New List(Of String), patch_lines As New List(Of String)

        Dim sFileScript As String
        sFileScript = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFileScript &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"


        If Not IO.File.Exists(sFileScript) Then
            oLogger.WriteToLogRelative("script file not found", , 1)
            Return False
        Else
            oLogger.WriteToLogRelative(sFileScript, , 1)
        End If


        oLogger.WriteToLogRelative("creating backup", , 1)
        If oCopyFiles.BackupFile(New IO.FileInfo(sFileScript)) Then
            oLogger.WriteToLogRelative("ok", , 2)
        Else
            oLogger.WriteToLogRelative("OOPS", , 2)
            Return False
        End If


        oLogger.WriteToLogRelative("reading patch", , 1)
        If Not IO.File.Exists(c_ScriptPatch) Then
            oLogger.WriteToLogRelative("path file not found", , 2)
            Return False
        End If
        Try
            Using sr As New IO.StreamReader(c_ScriptPatch)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    patch_lines.Add(line)
                End While
            End Using
        Catch ex As Exception
            oLogger.WriteToLogRelative("error while reading patch", , 2)
            oLogger.WriteToLogRelative(ex.Message, , 3)
            Return False
        End Try


        oLogger.WriteToLogRelative("updating", , 1)
        Try
            Using sr As New IO.StreamReader(sFileScript)
                Dim line As String, startpatching As Boolean = False

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If Not startpatching Then lines.Add(line)

                    If line.Contains("var iBAHN_Global_Brand") Then
                        startpatching = True
                    End If
                End While
            End Using

            Using sw As New IO.StreamWriter(sFileScript)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next

                For Each line As String In patch_lines
                    sw.WriteLine(line)
                Next
            End Using
            oLogger.WriteToLogRelative("ok", , 1)
            Return True
        Catch ex As Exception : Return False : End Try
    End Function

End Module
