Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        ExitCode.SetValue(ExitCode.ExitCodes.OK)


        '------------------------------
        Dim args() As String = Environment.GetCommandLineArgs().Skip(1).ToArray 'Skip first element (exe name) ; LINQ
        For Each arg As String In args
            If arg.Equals("--patch-script") Then
                g_PatchScript = True
            End If
            If arg.Equals("--keep-settings") Then
                g_KeepCurrentSettings = True
            End If
            If arg.Equals("--update-screensaver") Then
                g_UpdateScreenSaver = True
            End If
            If arg.StartsWith("--run-information-updater-params:") Then
                ' note: using this arg with the incorrect params for the info updater will result in a running instance of the info updater after this installer exits.
                g_RunInformationUpdater = True
                g_RunInformationUpdaterParams = arg.Replace("--run-information-updater-params:", "")
            End If
            If arg.Equals("--restart-sitekiosk") Then
                g_RestartSitekiosk = True
            End If
            If arg.Equals("--reboot-on-success") Then
                g_Reboot = True
            End If
        Next


        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Get current settings (1)", , 0)
        If Not clsGlobalSettings.ReadGlobalSettings() Then
            oLogger.WriteToLog("!!!FATAL ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 0)

            ExitCode.SetValue(ExitCode.ExitCodes.DATA_READ_FAILURE)

            Environment.ExitCode = ExitCode.GetValue
            Environment.Exit(ExitCode.GetValue)

            Exit Sub
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        oLogger.WriteToLog("Copy files", , 0)
        If IO.Directory.Exists(oCopyFiles.SourceDirectory) Then
            oCopyFiles.CopyFiles()
        Else
            oLogger.WriteToLog("source dir '" & oCopyFiles.SourceDirectory & "' not found!", Logger.MESSAGE_TYPE.LOG_ERROR, 0)

            oLogger.WriteToLog("!!!FATAL ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 0)

            ExitCode.SetValue(ExitCode.ExitCodes.FILE_COPY_FAILURE)

            Environment.ExitCode = ExitCode.GetValue
            Environment.Exit(ExitCode.GetValue)

            Exit Sub
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Patch script", , 0)
        If g_PatchScript Then
            clsGlobalSettings.PatchScriptFile()
        Else
            oLogger.WriteToLog("skipped", , 1)
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Patch sitekiosk config (screensaver)", , 0)
        If g_UpdateScreenSaver Then
            If clsGlobalSettings.IsHilton Then
                oLogger.WriteToLog("Hilton theme found, go ahead", , 1)

                SkCfg.BackupFolder = g_BackupDirectory
                SkCfg.Update()
            Else
                oLogger.WriteToLog("Hilton theme not active, let's not mess with the screen saver", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
            End If
        Else
            oLogger.WriteToLog("skipped", , 1)
        End If



        '------------------------------------------------------
        If g_KeepCurrentSettings Then
            If Not clsGlobalSettings.WriteGlobalSettings Then
                oLogger.WriteToLog("FAIL!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            End If
        Else
            oLogger.WriteToLog("skipped", , 1)
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Get current settings (2)", , 0)
        If Not clsGlobalSettings.ReadGlobalSettings() Then
            oLogger.WriteToLog("FAIL!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            ExitCode.SetValue(ExitCode.ExitCodes.DATA_WRITE_FAILURE)
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Running OVCCInformationUpdater", , 0)
        If g_RunInformationUpdater Then
            Dim sInfoUpdater As String = c_INFOUPDATER.Replace("%%PROGRAMFILESx86%%", Helpers.FilesAndFolders.GetProgramFilesFolder())

            If Not IO.File.Exists(sInfoUpdater) Then
                oLogger.WriteToLog("'" & sInfoUpdater & "' not found!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Else
                Try
                    Dim sParams() As String = g_RunInformationUpdaterParams.Split(",")
                    Dim sFinalParams As String = ""

                    For Each sParam As String In sParams
                        If sParam.StartsWith("--quiet") Then
                            sFinalParams &= " " & sParam
                        End If
                    Next

                    If sFinalParams.Equals("") Then
                        'Not starting info updater without any params, blah
                        oLogger.WriteToLog("Cannot start with these params: " & g_RunInformationUpdaterParams, Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    Else
                        oLogger.WriteToLog("starting", , 1)
                        oLogger.WriteToLog(sInfoUpdater & " " & sFinalParams, , 2)

                        If Helpers.Processes.StartProcess(sInfoUpdater, sFinalParams, True, 30000) Then
                            oLogger.WriteToLog("ok", , 3)
                        Else
                            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                        End If
                    End If
                Catch ex As Exception
                    oLogger.WriteToLog("Error while starting '" & sInfoUpdater & "'!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End Try
            End If
        End If



        '------------------------------------------------------
        oLogger.WriteToLog("Killing SiteKiosk", , 0)
        If g_RestartSitekiosk Then
            KillSiteKiosk()
        Else
            oLogger.WriteToLog("skipped", , 1)
        End If


        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
        oLogger.WriteToLog("exit code", , 2)
        oLogger.WriteToLog(ExitCode.ToString, , 3)


        If g_Reboot Then
            oLogger.WriteToLog("Reboot", , 0)

            If g_TESTMODE Then
                oLogger.WriteToLog("test mode, skipped", , 1)
            Else
                oLogger.WriteToLog("ok", , 1)
                WindowsController.ExitWindows(RestartOptions.Reboot, True)
            End If
        End If

        Environment.ExitCode = ExitCode.GetValue
        Environment.Exit(ExitCode.GetValue)
    End Sub
End Module
