Module Globals
    Public oLogger As Logger
    Public oComputerName As ComputerName

    Public oCopyFiles As CopyFiles

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String
    Public g_ProgramFiles As String
    Public g_RestartSitekiosk As Boolean = False
    Public g_Reboot As Boolean = False


    Public g_TESTMODE As Boolean

    Public ReadOnly c_ScriptPatch As String = "iBAHN_functions.js.patch"
    Public g_PatchScript As Boolean = False
    Public g_KeepCurrentSettings As Boolean = False
    Public g_UpdateScreenSaver As Boolean = False
    Public g_RunInformationUpdater As Boolean = False
    Public g_RunInformationUpdaterParams As String = ""


    Public ReadOnly c_SCREENSAVER As String = "file://%%PROGRAM FILES%%\SiteKiosk\Skins\Public\Startpages\Hilton\screensaver\02_Hilton_2017-March\main_swf.html"

    Public ReadOnly c_INFOUPDATER As String = "%%PROGRAMFILESx86%%\GuestTek\OVCCSoftware\tools\OVCCInformationUpdater.exe"


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_ProgramFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
        If g_ProgramFiles.Equals(String.Empty) Then
            g_ProgramFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub


End Module
