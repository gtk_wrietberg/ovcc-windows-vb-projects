﻿Public Class clsGlobalSettings
    Private Shared ReadOnly cStringId__PhoneNumber As String = "1100000"

    Public Shared HotelURL As String = ""
    Public Shared InternetURL As String = ""
    Public Shared WeatherURL As String = ""
    Public Shared MapURL As String = ""
    Public Shared SiteAddress As String = ""
    Public Shared Brand As String = ""
    Public Shared BrandInteger As Integer = -1

    Private Shared bSettingsRead As Boolean = False


    Public Shared ReadOnly Property IsHilton() As Boolean
        Get
            Return (BrandInteger >= 0 And BrandInteger < 50)
        End Get
    End Property

    Public Shared ReadOnly Property IsResidenceInn As Boolean
        Get
            If BrandInteger = 668 Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property


    Public Shared Function PatchScriptFile() As Boolean
        Dim lines As New List(Of String), patch_lines As New List(Of String)

        Dim sFileScript As String
        sFileScript = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFileScript &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"


        If Not IO.File.Exists(sFileScript) Then
            oLogger.WriteToLogRelative("script file not found", , 1)
            Return False
        Else
            oLogger.WriteToLogRelative(sFileScript, , 1)
        End If


        oLogger.WriteToLogRelative("creating backup", , 1)
        If oCopyFiles.BackupFile(New IO.FileInfo(sFileScript)) Then
            oLogger.WriteToLogRelative("ok", , 2)
        Else
            oLogger.WriteToLogRelative("OOPS", , 2)
            Return False
        End If


        oLogger.WriteToLogRelative("reading patch", , 1)
        If Not IO.File.Exists(c_ScriptPatch) Then
            oLogger.WriteToLogRelative("patch file not found", , 2)
            Return False
        End If
        Try
            Using sr As New IO.StreamReader(c_ScriptPatch)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    patch_lines.Add(line)
                End While
            End Using
        Catch ex As Exception
            oLogger.WriteToLogRelative("error while reading patch", , 2)
            oLogger.WriteToLogRelative(ex.Message, , 3)
            Return False
        End Try


        oLogger.WriteToLogRelative("updating", , 1)
        Try
            Using sr As New IO.StreamReader(sFileScript)
                Dim line As String, startpatching As Boolean = False

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If Not startpatching Then lines.Add(line)

                    If line.Contains("var iBAHN_Global_Brand") Then
                        startpatching = True
                    End If
                End While
            End Using

            Using sw As New IO.StreamWriter(sFileScript)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next

                For Each line As String In patch_lines
                    sw.WriteLine(line)
                Next
            End Using
            oLogger.WriteToLogRelative("ok", , 1)
            Return True
        Catch ex As Exception : Return False : End Try
    End Function

    Public Shared Function ReadGlobalSettings() As Boolean
        oLogger.WriteToLogRelative("reading script file", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

        oLogger.WriteToLogRelative(sFile, , 2)

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLogRelative("NOT FOUND!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        Else
            oLogger.WriteToLogRelative("ok", , 3)
        End If

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = line.Replace("var iBAHN_Global_HotelURL=", "")
                        line = CleanText(line)
                        HotelURL = line

                        oLogger.WriteToLogRelative("found hotel url", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = line.Replace("var iBAHN_Global_InternetURL=", "")
                        line = CleanText(line)
                        InternetURL = line

                        oLogger.WriteToLogRelative("found internet url", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = line.Replace("var iBAHN_Global_WeatherURL=", "")
                        line = CleanText(line)
                        WeatherURL = line

                        oLogger.WriteToLogRelative("found weather url", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = line.Replace("var iBAHN_Global_MapURL=", "")
                        line = CleanText(line)
                        MapURL = line

                        oLogger.WriteToLogRelative("found map url", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = line.Replace("var iBAHN_Global_SiteAddress=", "")
                        line = CleanText(line)
                        SiteAddress = line

                        oLogger.WriteToLogRelative("found site address", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        line = line.Replace("var iBAHN_Global_Brand=", "")
                        line = CleanText(line)

                        oLogger.WriteToLogRelative("found brand index", , 2)
                        oLogger.WriteToLogRelative(line, , 3)

                        Brand = line
                        If Not Int32.TryParse(Brand, BrandInteger) Then
                            BrandInteger = -100
                        End If
                    End If
                End While
            End Using

            bSettingsRead = True

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("Exception", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function

    'Public Shared Function ReadPhoneNumberFromEnglishLanguageFile() As Boolean
    '    ' read english.xml from c_Path_LanguageFiles
    '    Dim bFoundIt As Boolean = False
    '    Dim sFile As String = ""
    '    Dim sPhoneNumber As String = g_Support_PhoneNumber

    '    sFile = c_Path_LanguageFiles & "\english.xml"


    '    Try
    '        Dim xmlSkCfg As Xml.XmlDocument
    '        Dim xmlNode_Root As Xml.XmlNode


    '        xmlSkCfg = New Xml.XmlDocument

    '        oLogger.WriteToLogRelative("loading", , 1)
    '        oLogger.WriteToLogRelative(sFile, , 2)
    '        xmlSkCfg.Load(sFile)
    '        oLogger.WriteToLogRelative("ok", , 3)

    '        oLogger.WriteToLogRelative("loading root node", , 2)
    '        xmlNode_Root = xmlSkCfg.SelectSingleNode("language")

    '        If Not xmlNode_Root Is Nothing Then
    '            oLogger.WriteToLogRelative("ok", , 3)
    '        Else
    '            Throw New Exception("root node (language) not found")
    '        End If


    '        oLogger.WriteToLogRelative("loading string nodelist", , 2)
    '        Dim xmlNodeList_Strings As Xml.XmlNodeList
    '        Dim xmlNode_String As Xml.XmlNode
    '        Dim sTmp As String = ""

    '        xmlNodeList_Strings = xmlNode_Root.SelectNodes("string")
    '        For Each xmlNode_String In xmlNodeList_Strings
    '            If xmlNode_String.Attributes.GetNamedItem("id").InnerText = cStringId__PhoneNumber Then
    '                'found the specific language string id, mark it for a backup
    '                oLogger.WriteToLogRelative("found phone number string", , 3)

    '                'get existing phone number
    '                oLogger.WriteToLogRelative("retrieving current phone number", , 3)

    '                Dim sString As String = xmlNode_String.InnerText
    '                ' <string id="1100000">OneView Connection Centre helpdesk (gratis): 00800 3400 1111</string>

    '                Dim pattern As String = "(.+)(:|：) (.+)" 'Notice the funky semi-colon, thanks China
    '                Dim replacement As String = "$3"
    '                Dim rgx As New System.Text.RegularExpressions.Regex(pattern)
    '                sPhoneNumber = rgx.Replace(sString, replacement)

    '                If Not sPhoneNumber.Equals(sString) Then
    '                    g_Support_PhoneNumber = sPhoneNumber
    '                    oLogger.WriteToLogRelative(sPhoneNumber, , 4)

    '                    bFoundIt = True
    '                Else
    '                    oLogger.WriteToLogRelative("something went wrong, could not extract phone number", , 4)

    '                    Throw New Exception("Error while extracting phone number")
    '                End If
    '            End If
    '        Next

    '        If Not bFoundIt Then
    '            Throw New Exception("Could not find find string id 1100000 in '" & sFile & "'")
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
    '        oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
    '        oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
    '    End Try

    '    Return False
    'End Function

    'Public Shared Function WritePhoneNumberToLanguageFiles() As Boolean
    '    oLogger.WriteToLog("iterating through list of language files, Gotta Catch 'Em All", , 2)

    '    Try
    '        For Each file As String In My.Computer.FileSystem.GetFiles(c_Path_LanguageFiles, FileIO.SearchOption.SearchTopLevelOnly, "*.xml")
    '            oLogger.WriteToLog("found: " & file, , 3)

    '            If _WritePhoneNumberToLanguageFile(file) Then
    '                oLogger.WriteToLog("ok", , 4)
    '            Else
    '                oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
    '            End If
    '        Next
    '    Catch ex As Exception
    '        oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
    '        oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
    '        oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

    '        Return False
    '    End Try

    '    Return True
    'End Function

    'Private Shared Function _WritePhoneNumberToLanguageFile(sFile As String) As Boolean
    '    Try
    '        Dim xmlSkCfg As Xml.XmlDocument
    '        Dim xmlNode_Root As Xml.XmlNode

    '        Dim bFoundIt As Boolean = False
    '        Dim bUpdated As Boolean = False


    '        xmlSkCfg = New Xml.XmlDocument

    '        oLogger.WriteToLogRelative("loading", , 1)
    '        oLogger.WriteToLogRelative(sFile, , 2)
    '        xmlSkCfg.Load(sFile)
    '        oLogger.WriteToLogRelative("ok", , 3)

    '        oLogger.WriteToLogRelative("loading root node", , 2)
    '        xmlNode_Root = xmlSkCfg.SelectSingleNode("language")

    '        If Not xmlNode_Root Is Nothing Then
    '            oLogger.WriteToLogRelative("ok", , 3)
    '        Else
    '            Throw New Exception("root node (language) not found")
    '        End If


    '        oLogger.WriteToLogRelative("loading string nodelist", , 2)
    '        Dim xmlNodeList_Strings As Xml.XmlNodeList
    '        Dim xmlNode_String As Xml.XmlNode

    '        xmlNodeList_Strings = xmlNode_Root.SelectNodes("string")
    '        For Each xmlNode_String In xmlNodeList_Strings
    '            If xmlNode_String.Attributes.GetNamedItem("id").InnerText = cStringId__PhoneNumber Then
    '                'found the specific language string id, mark it for a backup
    '                oLogger.WriteToLogRelative("found phone number string", , 3)
    '                bFoundIt = True

    '                'get existing phone number
    '                oLogger.WriteToLogRelative("updating current phone number", , 3)

    '                Dim sCurrent As String = xmlNode_String.InnerText
    '                Dim sNew As String = ""
    '                ' <string id="1100000">OneView Connection Centre helpdesk (gratis): 00800 3400 1111</string>

    '                Dim pattern As String = "(.+)(:|：) (.+)"
    '                Dim replacement As String = "$1$2 " & g_Support_PhoneNumber
    '                Dim rgx As New System.Text.RegularExpressions.Regex(pattern)
    '                sNew = rgx.Replace(sCurrent, replacement)

    '                If Not sNew.Equals(sCurrent) Then
    '                    oLogger.WriteToLogRelative("old", , 4)
    '                    oLogger.WriteEmptyLineToLog()
    '                    oLogger.WriteToLogWithoutDate(sCurrent)
    '                    oLogger.WriteEmptyLineToLog()
    '                    oLogger.WriteToLogRelative("new", , 4)
    '                    oLogger.WriteEmptyLineToLog()
    '                    oLogger.WriteToLogWithoutDate(sNew)
    '                    oLogger.WriteEmptyLineToLog()

    '                    xmlNode_String.InnerText = sNew
    '                    oLogger.WriteToLogRelative("updated", , 4)
    '                    oLogger.WriteToLogRelative("ok", , 5)

    '                    bUpdated = True

    '                    Exit For
    '                Else
    '                    oLogger.WriteToLogRelative("something went wrong, could not update phone number", , 4)
    '                End If
    '            End If
    '        Next

    '        If bFoundIt And bUpdated Then
    '            MakeBackup(sFile)

    '            oLogger.WriteToLogRelative("saving", , 1)
    '            xmlSkCfg.Save(sFile)
    '            oLogger.WriteToLogRelative("ok", , 2)
    '        Else
    '            If Not bFoundIt Then
    '                oLogger.WriteToLogRelative("String id '" & cStringId__PhoneNumber & "' not found!", Logger.MESSAGE_TYPE.LOG_WARNING, 3)
    '                oLogger.WriteToLogRelative("File not changed!", Logger.MESSAGE_TYPE.LOG_WARNING, 4)
    '            Else
    '                oLogger.WriteToLogRelative("String id '" & cStringId__PhoneNumber & "' was found, but was not updated!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
    '            End If
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
    '        oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
    '        oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
    '    End Try

    '    Return False
    'End Function

    Public Shared Function WriteGlobalSettings() As Boolean
        If Not bSettingsRead Then
            oLogger.WriteToLogRelative("settings were not read!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            Return False
        End If

        Dim lines As New List(Of String)

        oLogger.WriteToLogRelative("reading script file", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

        If Not IO.File.Exists(sFile) Then
            oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        Else
            oLogger.WriteToLogRelative("ok", , 2)
        End If

        Try
            oLogger.WriteToLogRelative("setting values", , 1)

            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = "var iBAHN_Global_HotelURL=""" & HotelURL & """;"
                        oLogger.WriteToLogRelative("HotelUrl", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = "var iBAHN_Global_InternetURL=""" & InternetURL & """;"
                        oLogger.WriteToLogRelative("InternetURL", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = "var iBAHN_Global_WeatherURL=""" & WeatherURL & """;"
                        oLogger.WriteToLogRelative("WeatherURL", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = "var iBAHN_Global_MapURL=""" & MapURL & """;"
                        oLogger.WriteToLogRelative("MapURL", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = "var iBAHN_Global_SiteAddress=""" & SiteAddress & """;"
                        oLogger.WriteToLogRelative("SiteAddress", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        line = "var iBAHN_Global_Brand=" & Brand & ";"
                        oLogger.WriteToLogRelative("Brand", , 2)
                        oLogger.WriteToLogRelative(line, , 3)
                    End If

                    lines.Add(line)
                End While
            End Using

            oLogger.WriteToLogRelative("writing script file", , 1)
            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using

            oLogger.WriteToLogRelative("ok", , 2)
            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End Try
    End Function

    Private Shared Function MakeBackup(sFile As String) As Boolean
        oLogger.WriteToLogRelative("creating backup", , 1)

        Try
            Dim oFile As System.IO.FileInfo

            oFile = New System.IO.FileInfo(sFile)

            Dim sBackupPath As String = oFile.DirectoryName
            Dim sBackupName As String = oFile.Name
            Dim sBackupFile As String
            Dim oDirInfo As System.IO.DirectoryInfo
            sBackupPath = sBackupPath.Replace("C:", "")
            sBackupPath = sBackupPath.Replace("c:", "")
            sBackupPath = sBackupPath.Replace("\\", "\")
            sBackupPath = g_BackupDirectory & "\" & sBackupPath

            sBackupFile = sBackupPath & "\" & sBackupName

            oLogger.WriteToLogRelative("backup: " & sFile & " => " & sBackupFile, , 2)

            oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oFile = New System.IO.FileInfo(sFile)
            oFile.CopyTo(sBackupFile, True)

            oLogger.WriteToLogRelative("ok", , 3)

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            Return False
        End Try
    End Function

    Private Shared Function CleanText(txt As String) As String
        txt = txt.Replace("""", "")
        txt = txt.Replace(";", "")

        Return txt
    End Function
End Class
