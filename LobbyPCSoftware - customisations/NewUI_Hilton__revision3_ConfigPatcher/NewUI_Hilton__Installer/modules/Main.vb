Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        Dim arg As String, arg_index As Integer
        For arg_index = 1 To Environment.GetCommandLineArgs.Length - 1
            arg = Environment.GetCommandLineArgs(arg_index)

            oLogger.WriteToLog("found: " & arg, , 1)

            If arg = "--overwrite-existing-config" Then
                g_OverwriteExistingConfig = True
            End If
        Next



        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or _
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Update SiteKiosk config", , 0)
        oSkCfg.Update(g_OverwriteExistingConfig)


        '------------------------------------------------------
        oLogger.WriteToLog("Killing SiteKiosk", , 0)
        KillSiteKiosk()

        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
