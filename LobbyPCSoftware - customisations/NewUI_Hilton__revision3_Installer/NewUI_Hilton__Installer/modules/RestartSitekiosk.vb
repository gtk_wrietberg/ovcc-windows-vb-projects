Imports System.Diagnostics

Module RestartSitekiosk
    Public Function KillSiteKiosk() As Integer
        Dim iCount As Integer = 0
        Dim iTmp As Integer

        iTmp = Process.GetProcessesByName("sitekiosk").Length

        If iTmp > 0 Then
            oLogger.WriteToLog("found SiteKiosk (" & iTmp & "x)", , 1)
        Else
            oLogger.WriteToLog("found no SiteKiosk processes", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If

        For Each p As Process In Process.GetProcessesByName("sitekiosk")
            Try
                oLogger.WriteToLog("killing", , 2)
                p.Kill()
                oLogger.WriteToLog("ok", , 3)
                iCount += 1
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End Try
        Next

        If iCount > 0 Then
            oLogger.WriteToLog("killed SiteKiosk (" & iCount & "x)", , 1)
        Else
            oLogger.WriteToLog("killed nothing", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If

        Return iCount
    End Function
End Module
