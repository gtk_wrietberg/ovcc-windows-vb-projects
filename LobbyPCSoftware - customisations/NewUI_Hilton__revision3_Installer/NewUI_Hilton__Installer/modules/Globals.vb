Module Globals
    Public oLogger As Logger
    Public oComputerName As ComputerName

    Public oCopyFiles As CopyFiles
    Public oSkCfg As SkCfg

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String

    Public g_TESTMODE As Boolean

    Public g_OverwriteExistingConfig As Boolean
    Public g_LeaveGlobalSettingsAlone As Boolean

    Public g_Url_Hotel As String = "http://www.hilton.co.uk"
    Public g_Url_Internet As String = "http://www.google.com"
    Public g_Url_Weather As String = "http://www.weather.com"
    Public g_Url_Map As String = "http://maps.google.com"
    Public g_Address_Site As String = "iBAHN, Logie Court, Stirling University Inovation Park, Stirling"
    Public g_Brand_Hotel As String = "2"

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(g_BackupDirectory)

        g_OverwriteExistingConfig = False
        g_LeaveGlobalSettingsAlone = False
    End Sub


End Module
