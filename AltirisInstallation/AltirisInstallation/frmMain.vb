Imports System.Threading

Public Class frmMain
    Private WithEvents oProcess As ProcessRunner
    Private oLogger As Logger
    Private installThread As Thread
    Private iApplicationInstallationCount As Integer = 0
    Private ReadOnly iApplicationInstallationCountMax As Integer = 2

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Visible = False
        Me.Opacity = 0.0

        oLogger = New Logger
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(myBuildInfo.FileVersion)

        oProcess = New ProcessRunner

        iApplicationInstallationCount = 0
        ApplicationInstallationSpecialAction()
    End Sub

    Private Sub ApplicationInstallationSpecialAction()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner

        If iApplicationInstallationCount < iApplicationInstallationCountMax Then
            tmrInstallerWait.Enabled = True
        End If

        Select Case iApplicationInstallationCount
            Case 0
                oLogger.WriteToLog("Installing Altiris files")

                Me.installThread = New Thread(New ThreadStart(AddressOf Me.InstallAltiris))
                Me.installThread.Start()
            Case 1
                oLogger.WriteToLog("Importing certificates")

                Me.installThread = New Thread(New ThreadStart(AddressOf Me.InstallAltirisCertificate))
                Me.installThread.Start()
            Case Else
                oLogger.WriteToLog("Done")

                oLogger.WriteToLog(New String("=", 50))
                Environment.Exit(0)
        End Select
    End Sub

    Private Sub InstallAltiris()
        Dim sReturn As String = ""

        oLogger.WriteToLog("Creating folders", , 1)
        If Not IO.Directory.Exists(cAltirisInstallationPath1) Then
            If CreateFolder(cAltirisInstallationPath1) Then
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Else
            oLogger.WriteToLog("already there", , 2)
        End If
        If Not IO.Directory.Exists(cAltirisInstallationPath2) Then
            If CreateFolder(cAltirisInstallationPath2) Then
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Else
            oLogger.WriteToLog("already there", , 2)
        End If

        oLogger.WriteToLog("Copying files", , 1)
        If CopyMultipleFiles(IO.Path.GetDirectoryName(Application.ExecutablePath), cAltirisInstallationPath1, sReturn) Then
            oLogger.WriteToLog("ok", , 2)
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If
        oLogger.WriteToLog(sReturn, , 2)

        oProcess = New ProcessRunner
        oProcess.FileName = cAltirisInstallationPath1 & "\alt_deploy.exe"
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub InstallAltirisCertificate()
        Dim sReturn As String = ""

        oLogger.WriteToLog("importing ""ams01.ibahn.com.root.cer""", , 1)
        'certmgr.exe -add -c "<cert-file>" -s -r localMachine root 

        oProcess = New ProcessRunner
        oProcess.FileName = cAltirisInstallationPath1 & "\certmgr.exe"
        oProcess.Arguments = "-add -c ""ams01.ibahn.com.root.cer"" -s -r localMachine root"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLog("Done - Time elapsed: " & TimeElapsed.ToString, , 1)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Failed
        oLogger.WriteToLog("Process failed", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLog("Process started", , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLog("Process timed out", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        oProcess.ProcessDone()
    End Sub
#End Region

    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            iApplicationInstallationCount += 1
            ApplicationInstallationSpecialAction()
        End If
    End Sub
End Class
