Public Class ComputerName
    Private _computername As String
    Private _domain As String

    Private Const JOIN_DOMAIN = 1
    Private Const ACCT_CREATE = 2
    Private Const ACCT_DELETE = 4
    Private Const WIN9X_UPGRADE = 16
    Private Const DOMAIN_JOIN_IF_JOINED = 32
    Private Const JOIN_UNSECURE = 64
    Private Const MACHINE_PASSWORD_PASSED = 128
    Private Const DEFERRED_SPN_SET = 256
    Private Const INSTALL_INVOCATION = 262144

    Public Sub New()
        _GetComputerNameAndDomain()
    End Sub

    Public Property ComputerName() As String
        Get
            Return _computername
        End Get
        Set(ByVal value As String)
            If _computername <> "" And InStr(_computername, "rietberg") <= 0 Then
                _RenameComputer(value)
            End If
        End Set
    End Property

    Public Property Workgroup() As String
        Get
            Return _domain
        End Get
        Set(ByVal value As String)
            If _computername <> "" And InStr(_computername, "rietberg") <= 0 Then
                _JoinWorkgroup(value)
            End If
        End Set
    End Property

    Private Sub _GetComputerNameAndDomain()
        Dim strComputer As String
        Dim objWMIService As Object
        Dim objComputers As Object, objComputer As Object

        strComputer = "."
        objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
        objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

        For Each objComputer In objComputers
            _computername = objComputer.Name
            _domain = objComputer.Domain
        Next
    End Sub

    Private Sub _JoinWorkgroup(ByVal sDomain As String)
        Dim strComputer As String
        Dim objWMIService As Object
        Dim objComputers As Object, objComputer As Object

        strComputer = "."
        objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
        objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

        For Each objComputer In objComputers
            objComputer.JoinDomainOrWorkGroup(sDomain, , , , )
        Next
    End Sub

    Private Sub _RenameComputer(ByVal sComputerName As String)
        Dim strComputer As String
        Dim objWMIService As Object
        Dim objComputers As Object, objComputer As Object

        strComputer = "."
        objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
        objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

        For Each objComputer In objComputers
            objComputer.Rename(sComputerName)
        Next
    End Sub
End Class
