Imports System.Diagnostics
Imports System.Threading

Public Class ProcessRunner
    Private WithEvents myProcess As New System.Diagnostics.Process
    Private elapsedTime As Double
    Private eventHandled As Boolean


    Private Const cSleepAmount As Double = 0.5#

    Private Const OneSec As Double = 1.0# / (1440.0# * 60.0#)

    Private mMaxTimeout As Double = 30
    Private mProcessStyle As ProcessWindowStyle

    Private mFileName As String
    Private mArguments As String

    Private mIsProcessDone As Boolean

    Public Event Started(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Failed(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event TimeOut(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Done(ByVal EventProcess As Process, ByVal TimeElapsed As Double)

    Public Sub New()
        mMaxTimeout = 30000
        mProcessStyle = ProcessWindowStyle.Normal

        mFileName = ""
        mArguments = ""

        mIsProcessDone = False
    End Sub

    Public Property MaxTimeout() As Double
        Get
            Return mMaxTimeout
        End Get
        Set(ByVal value As Double)
            mMaxTimeout = value
        End Set
    End Property

    Public Property ProcessStyle() As ProcessWindowStyle
        Get
            Return mProcessStyle
        End Get
        Set(ByVal value As ProcessWindowStyle)
            mProcessStyle = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return mFileName
        End Get
        Set(ByVal value As String)
            mFileName = value
        End Set
    End Property

    Public Property Arguments() As String
        Get
            Return mArguments
        End Get
        Set(ByVal value As String)
            mArguments = value
        End Set
    End Property

    Public ReadOnly Property IsProcessDone() As Boolean
        Get
            Return mIsProcessDone
        End Get
    End Property

    Public Sub StartProcess()
        StartProcess(mFileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String)
        StartProcess(FileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String, ByVal Arguments As String)
        elapsedTime = 0.0#
        eventHandled = False

        Try
            myProcess.StartInfo.FileName = FileName
            myProcess.StartInfo.Arguments = Arguments
            myProcess.StartInfo.WindowStyle = mProcessStyle
            myProcess.EnableRaisingEvents = True
            myProcess.Start()

            RaiseEvent Started(myProcess, elapsedTime)
        Catch ex As Exception
            RaiseEvent Failed(myProcess, elapsedTime)

            Exit Sub
        End Try

        Do While Not eventHandled And Not myProcess.HasExited
            elapsedTime += cSleepAmount

            If elapsedTime > mMaxTimeout Then
                RaiseEvent TimeOut(myProcess, elapsedTime)
                StopProcess()
                Exit Do
            End If

            Thread.Sleep(500)
        Loop

        RaiseEvent Done(myProcess, elapsedTime)
    End Sub

    Public Sub StopProcess()
        Try
            myProcess.Kill()
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ProcessDone()
        eventHandled = True

        myProcess.Close()

        mIsProcessDone = True
    End Sub
End Class
