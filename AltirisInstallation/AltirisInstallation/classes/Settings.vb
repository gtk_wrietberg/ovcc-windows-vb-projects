Imports System.Xml

Public Class Settings
    Private mWorkgroup As String

    Private mInstaller_pcAnywhere_Path As String
    Private mInstaller_pcAnywhere_Params As String
    Private mInstaller_pcAnywhere_Timeout As Double
    Private mInstaller_pcAnywhere_WindowStyle As ProcessWindowStyle

    Private mInstaller_Altiris_Path As String
    Private mInstaller_Altiris_Params As String
    Private mInstaller_Altiris_Timeout As Double
    Private mInstaller_Altiris_WindowStyle As ProcessWindowStyle

    Private mInstaller_AltirisCertificate_Path As String
    Private mInstaller_AltirisCertificate_Params As String
    Private mInstaller_AltirisCertificate_Timeout As Double
    Private mInstaller_AltirisCertificate_WindowStyle As ProcessWindowStyle

    Private mInstaller_SiteKiosk_Path As String
    Private mInstaller_SiteKiosk_Params As String
    Private mInstaller_SiteKiosk_Timeout As Double
    Private mInstaller_SiteKiosk_WindowStyle As ProcessWindowStyle

    Public Sub New()
        _LoadSettingsFromXmlFile()
    End Sub

#Region "Properties"
    Public ReadOnly Property Workgroup() As String
        Get
            Return mWorkgroup
        End Get
    End Property

    Public ReadOnly Property Installer_pcAnywhere_Path() As String
        Get
            Return mInstaller_pcAnywhere_Path
        End Get
    End Property

    Public ReadOnly Property Installer_pcAnywhere_Params() As String
        Get
            Return mInstaller_pcAnywhere_Params
        End Get
    End Property

    Public ReadOnly Property Installer_pcAnywhere_Timeout() As Double
        Get
            Return mInstaller_pcAnywhere_Timeout
        End Get
    End Property

    Public ReadOnly Property Installer_pcAnywhere_WindowStyle() As ProcessWindowStyle
        Get
            Return mInstaller_pcAnywhere_WindowStyle
        End Get
    End Property

    Public ReadOnly Property Installer_Altiris_Path() As String
        Get
            Return mInstaller_Altiris_Path
        End Get
    End Property

    Public ReadOnly Property Installer_Altiris_Params() As String
        Get
            Return mInstaller_Altiris_Params
        End Get
    End Property

    Public ReadOnly Property Installer_Altiris_Timeout() As Double
        Get
            Return mInstaller_Altiris_Timeout
        End Get
    End Property

    Public ReadOnly Property Installer_Altiris_WindowStyle() As ProcessWindowStyle
        Get
            Return mInstaller_Altiris_WindowStyle
        End Get
    End Property

    Public ReadOnly Property Installer_AltirisCertificate_Path() As String
        Get
            Return mInstaller_AltirisCertificate_Path
        End Get
    End Property

    Public ReadOnly Property Installer_AltirisCertificate_Params() As String
        Get
            Return mInstaller_AltirisCertificate_Params
        End Get
    End Property

    Public ReadOnly Property Installer_AltirisCertificate_Timeout() As Double
        Get
            Return mInstaller_AltirisCertificate_Timeout
        End Get
    End Property

    Public ReadOnly Property Installer_AltirisCertificate_WindowStyle() As ProcessWindowStyle
        Get
            Return mInstaller_AltirisCertificate_WindowStyle
        End Get
    End Property

    Public ReadOnly Property Installer_SiteKiosk_Path() As String
        Get
            Return mInstaller_SiteKiosk_Path
        End Get
    End Property

    Public ReadOnly Property Installer_SiteKiosk_Params() As String
        Get
            Return mInstaller_SiteKiosk_Params
        End Get
    End Property

    Public ReadOnly Property Installer_SiteKiosk_Timeout() As Double
        Get
            Return mInstaller_SiteKiosk_Timeout
        End Get
    End Property

    Public ReadOnly Property Installer_SiteKiosk_WindowStyle() As ProcessWindowStyle
        Get
            Return mInstaller_SiteKiosk_WindowStyle
        End Get
    End Property
#End Region

    Private Sub _LoadSettingsFromXmlFile()
        Dim xmlDoc As XmlDocument
        Dim xmlSettings As XmlNodeList

        Dim i As Integer

        xmlDoc = New XmlDocument
        xmlDoc.Load(Application.StartupPath & "\LobbyPCStandaloneInstallation.settings.xml")

        xmlSettings = xmlDoc.GetElementsByTagName("settings")
        If xmlSettings.Count > 0 Then
            If xmlSettings(0).HasChildNodes Then
                For i = 0 To xmlSettings(0).ChildNodes.Count - 1
                    If xmlSettings(0).ChildNodes(i).Name = "workgroup" Then
                        mWorkgroup = xmlSettings(0).ChildNodes(i).InnerText
                    End If
                Next
            End If
        End If

        xmlSettings = xmlDoc.GetElementsByTagName("installer")
        If xmlSettings.Count > 0 Then
            For i = 0 To xmlSettings.Count - 1
                If xmlSettings(i).ChildNodes(0).InnerText.ToLower = "pcAnywhere".ToLower Then
                    mInstaller_pcAnywhere_Path = """" & Application.StartupPath & "\" & xmlSettings(i).ChildNodes(1).InnerText & """"
                    mInstaller_pcAnywhere_Params = xmlSettings(i).ChildNodes(2).InnerText
                    mInstaller_pcAnywhere_Timeout = Double.Parse(xmlSettings(i).ChildNodes(3).InnerText)
                    mInstaller_pcAnywhere_WindowStyle = _GetWindowStyle(xmlSettings(i).ChildNodes(4).InnerText)
                End If
                If xmlSettings(i).ChildNodes(0).InnerText.ToLower = "Altiris".ToLower Then
                    mInstaller_Altiris_Path = """" & Application.StartupPath & "\" & xmlSettings(i).ChildNodes(1).InnerText & """"
                    mInstaller_Altiris_Params = xmlSettings(i).ChildNodes(2).InnerText
                    mInstaller_Altiris_Timeout = Double.Parse(xmlSettings(i).ChildNodes(3).InnerText)
                    mInstaller_Altiris_WindowStyle = _GetWindowStyle(xmlSettings(i).ChildNodes(4).InnerText)
                End If
                If xmlSettings(i).ChildNodes(0).InnerText.ToLower = "AltirisCertificate".ToLower Then
                    mInstaller_AltirisCertificate_Path = """" & Application.StartupPath & "\" & xmlSettings(i).ChildNodes(1).InnerText & """"
                    mInstaller_AltirisCertificate_Params = xmlSettings(i).ChildNodes(2).InnerText
                    mInstaller_AltirisCertificate_Timeout = Double.Parse(xmlSettings(i).ChildNodes(3).InnerText)
                    mInstaller_AltirisCertificate_WindowStyle = _GetWindowStyle(xmlSettings(i).ChildNodes(4).InnerText)
                End If
                If xmlSettings(i).ChildNodes(0).InnerText.ToLower = "SiteKiosk".ToLower Then
                    mInstaller_SiteKiosk_Path = """" & Application.StartupPath & "\" & xmlSettings(i).ChildNodes(1).InnerText & """"
                    mInstaller_SiteKiosk_Params = xmlSettings(i).ChildNodes(2).InnerText
                    mInstaller_SiteKiosk_Timeout = Double.Parse(xmlSettings(i).ChildNodes(3).InnerText)
                    mInstaller_SiteKiosk_WindowStyle = _GetWindowStyle(xmlSettings(i).ChildNodes(4).InnerText)
                End If
            Next
        End If

        Try
        Catch ex As Exception
            mWorkgroup = ""
            mInstaller_pcAnywhere_Path = ""
            mInstaller_pcAnywhere_Params = ""
            mInstaller_pcAnywhere_Timeout = 0
            mInstaller_pcAnywhere_WindowStyle = ProcessWindowStyle.Normal
            mInstaller_Altiris_Path = ""
            mInstaller_Altiris_Params = ""
            mInstaller_Altiris_Timeout = 0
            mInstaller_Altiris_WindowStyle = ProcessWindowStyle.Normal
            mInstaller_AltirisCertificate_Path = ""
            mInstaller_AltirisCertificate_Params = ""
            mInstaller_AltirisCertificate_Timeout = 0
            mInstaller_AltirisCertificate_WindowStyle = ProcessWindowStyle.Normal
            mInstaller_SiteKiosk_Path = ""
            mInstaller_SiteKiosk_Params = ""
            mInstaller_SiteKiosk_Timeout = 0
            mInstaller_SiteKiosk_WindowStyle = ProcessWindowStyle.Normal
        End Try
    End Sub

    Private Function _GetWindowStyle(ByVal value As String) As ProcessWindowStyle
        Select Case value.ToLower
            Case "hidden"
                Return ProcessWindowStyle.Hidden
            Case "maximized"
                Return ProcessWindowStyle.Maximized
            Case "minimized"
                Return ProcessWindowStyle.Minimized
            Case Else
                Return ProcessWindowStyle.Normal
        End Select
    End Function
End Class
