Imports System.Threading

Public Class frmMain
    Private iApplicationInstallationCount As Integer = 0
    Private iApplicationInstallationCountMax As Integer = 13

    Private iProgress As Integer = 0
    Private iProgressMax As Integer = 15

    Private WithEvents oProcess As ProcessRunner
    Private oComputerName As ComputerName
    Private installThread As Thread

    Private iActiveGroupBox As Integer

    Private Enum InstallationOrder As Integer
        LogMeIn = 1
        Altiris
        CreateShortcut
        SiteKiosk
        iBAHNUpdate
        NewUI
        AirlineButtonIECompPatch
        LobbyPCAgent
        LobbyPCWatchdog
        Shortcuts
        SiteKioskSecurityUpdater
    End Enum

    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

#Region " ClientAreaMove Handling "
    'Private Const WM_NCHITTEST As Integer = &H84
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    'Private Const HTCLIENT As Integer = &H1
    Private Const HTCAPTION As Integer = &H2
    '    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
    '        Select Case m.Msg
    '            Case WM_NCHITTEST
    '                MyBase.WndProc(m)
    '    'If m.Result = HTCLIENT Then m.Result = HTCAPTION
    '                If m.Result.ToInt32 = HTCLIENT Then m.Result = IntPtr.op_Explicit(HTCAPTION) 'Try this in VS.NET 2002/2003 if the latter line of code doesn't do it... thx to Suhas for the tip.
    '            Case Else
    '    'Make sure you pass unhandled messages back to the default message handler.
    '                MyBase.WndProc(m)
    '        End Select
    '    End Sub
#End Region

#Region "Form Events"
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Don't close the form when we're still busy
        e.Cancel = gBusyInstalling
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActivatePrevInstance(Me.Text)

        oComputerName = New ComputerName
        oSettings = New Settings
        oLogger = New Logger


        If InStr(oComputerName.ComputerName.ToLower, "rietberg".ToLower) > 0 _
        Or InStr(oComputerName.ComputerName.ToLower, "superlekkerding".ToLower) > 0 Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If
        End If

        'GUI
        Me.StartPosition = FormStartPosition.Manual

        Me.Left = (My.Computer.Screen.Bounds.Width - Me.Width) / 2
        Me.Top = (My.Computer.Screen.Bounds.Height - Me.Height) / 2

        Me.Opacity = 1.0
        gHideProgress = False

        If IO.Directory.Exists(cPATHS_iBAHNProgramFilesFolder_x86) Then
            giBAHNProgramFilesFolder = cPATHS_iBAHNProgramFilesFolder_x86
        Else
            giBAHNProgramFilesFolder = cPATHS_iBAHNProgramFilesFolder
        End If

        If Not IO.Directory.Exists(giBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder) Then
            IO.Directory.CreateDirectory(giBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder)
        End If

        oLogger.LogFilePath = giBAHNProgramFilesFolder & "\"

        txtProgress.Clear()
        Me.ControlBox = False
        txtProgress.Font = System.Drawing.SystemFonts.MessageBoxFont


        LogEvent(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = "v" & myBuildInfo.FileMajorPart & "." & myBuildInfo.FileMinorPart

        LogEvent(Application.ProductName & " v" & myBuildInfo.FileVersion)
        LogEvent("Computer name: " & oComputerName.ComputerName)


        progressbarInstaller.Maximum = iProgressMax




        If gTestMode Then
            LogEvent("Running in test mode!!!", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        End If


        gBusyInstalling = False


        'Install count max
        iApplicationInstallationCountMax = InstallationOrder.SiteKioskSecurityUpdater + 1


        tmrStartDeployment.Enabled = True
    End Sub
#End Region

#Region "Private Functions"
#Region "Logging"
    Private Sub LogEvent(ByVal sMessage As String, Optional ByVal cMessageType As Logger.MESSAGE_TYPE = Logger.MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bProgressWindowOnly As Boolean = False)
        Dim sLogText As String

        sLogText = oLogger.WriteToLog(sMessage, cMessageType, iDepth, bProgressWindowOnly)

        If bProgressWindowOnly Then
            LogEventToProgressTextBox(sLogText)
        Else
            LogEventToProgressTextBox(sLogText & vbCrLf)
        End If
    End Sub

    Private Sub LogEventToProgressTextBox(ByVal sMessage As String)
        LogToProcessTextBox(sMessage)
    End Sub
#End Region
#Region "Thread Safe stuff"
    Private Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text])
        End If
    End Sub
#End Region

#Region "Deployment and Installers"
#Region "Deployment"
    Private Sub StartDeployment()
        UpdateProgressBar()

        LogEvent("Loading deployment config")

        LogEvent("default values", , 1)
        gRegion = "eu"
        LogEvent("region: eu", , 2)
        gLicenseRegion = ""
        LogEvent("license: equal to region", , 2)

        gSkipSkype = False
        gSkipLogMeIn = False

        gLogMeInDeployID = ""
        gHiltonPackageDetected = False


        LogEvent("loading command line arguments", , 1)
        For Each arg As String In Environment.GetCommandLineArgs()
            LogEvent("found: " & arg, , 2)

            If InStr(arg, "-license=") > 0 Then
                gLicenseRegion = arg.Replace("-license=", "")
                LogEvent("license: " & gLicenseRegion, , 3)
            End If
            If arg = "-eu" Or arg = "-emea" Then
                gRegion = "eu"
                LogEvent("region: " & gRegion, , 3)
            End If
            If arg = "-us" Or arg = "-yankees" Then
                gRegion = "us"
                LogEvent("region: " & gRegion, , 3)
            End If
            If arg = "-hideprogress" Then
                gHideProgress = True

                Me.Opacity = 0.0

                LogEvent("hide progress: " & gHideProgress.ToString, , 3)
            End If
            If arg = "-skipskype" Then
                gSkipSkype = True

                LogEvent("skip skype: " & gSkipSkype.ToString, , 3)
            End If
            If arg = "-skiplogmein" Then
                gSkipLogMeIn = True

                LogEvent("skip LogMeIn: " & gSkipLogMeIn.ToString, , 3)
            End If
            If InStr(arg, "-logmein-deployid=") > 0 Then
                gLogMeInDeployID = arg.Replace("-logmein-deployid=", "")
                LogEvent("LogMeIn DeployID: " & gLogMeInDeployID, , 3)
            End If
        Next

        If gLicenseRegion = "" Then
            gLicenseRegion = gRegion
        End If


        UpdateProgressBar()
        LogEvent("Storing deployment config in registry")
        LogEvent("blocked drives", , 1)
        Try
            Dim sDrivesToBeBlocked As String
            sDrivesToBeBlocked = GetDrivesToBeBlocked()
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\BlockedDrives", , 3)
            LogEvent("value: " & sDrivesToBeBlocked, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "BlockedDrives", sDrivesToBeBlocked)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        LogEvent("license", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\License", , 3)
            LogEvent("value: " & gLicenseRegion, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "License", gLicenseRegion)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        If gLogMeInDeployID <> "" Then
            LogEvent("LogMeIn DeployID", , 1)
            Try
                LogEvent("Registry", , 2)
                LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\LogMeInDeployID", , 3)
                LogEvent("value: " & gLogMeInDeployID, , 3)
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "LogMeInDeployID", gLogMeInDeployID)
            Catch ex As Exception
                LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If

        LogEvent("region", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\Region", , 3)
            LogEvent("value: " & gRegion, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "Region", gRegion)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        If gSkipSkype Then
            LogEvent("skip skype", , 1)
            Try
                LogEvent("Registry", , 2)
                LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\SkipSkype", , 3)
                LogEvent("value: yes", , 3)
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "SkipSkype", "yes")
            Catch ex As Exception
                LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If


        UpdateProgressBar()
        LogEvent("Backing up some stuff")

        LogEvent("Computer name", , 1)
        LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Backup\Computer Name", , 2)
        LogEvent("value: " & oComputerName.ComputerName, , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Backup", "Computer Name", oComputerName.ComputerName)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogEvent("Workgroup name", , 1)
        LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Backup\Workgroup Name", , 2)
        LogEvent("value: " & oComputerName.Workgroup, , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Backup", "Workgroup Name", oComputerName.Workgroup)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogEvent("Updating default user profile")
        LogEvent("Registry", , 1)
        LogEvent("key  : HKEY_USERS\.DEFAULT\Control Panel\PowerCfg\CurrentPowerPolicy", , 2)
        LogEvent("value: 2", , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_USERS\.DEFAULT\Control Panel\PowerCfg", "CurrentPowerPolicy", "2")
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogEvent("Updating User Agent")
        LogEvent("Registry", , 1)
        LogEvent("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", , 2)
        LogEvent("value: Mozilla/5.0", , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "", "Mozilla/5.0")
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent\Version", , 2)
        LogEvent("value: MSIE 9.0", , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "Version", "MSIE 9.0")
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V HonorAutoRunSetting  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveTypeAutoRun  /T REG_DWORD  /D 0x000000ff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveAutoRun  /T REG_DWORD  /D 0x0003ffff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoAutoRun  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\SYSTEM\CurrentControlSet\Services\Cdrom" /V AutoRun  /T REG_DWORD  /D 0x00000000  /F
        'reg.exe ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping\Autorun.inf" /VE      /T REG_SZ    /D "@SYS:DoesNotExist" /F

        LogEvent("Disabling AutoRun")
        LogEvent("Registry", , 1)
        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!HonorAutoRunSetting", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "HonorAutoRunSetting", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoDriveTypeAutoRun", , 2)
        LogEvent("value: 255", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoDriveTypeAutoRun", 255)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoDriveAutoRun", , 2)
        LogEvent("value: 262143", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoDriveAutoRun", 262143)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoAutoRun", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoAutoRun", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER2 & "!NoAutoplayfornonVolume", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER2, "NoAutoplayfornonVolume", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try




        LogEvent("Storing main installer version number")
        LogEvent("Registry", , 1)
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        LogEvent(cREGKEY_LPCSOFTWARE & "\Version", , 2)
        LogEvent("Installer = '" & myBuildInfo.FileVersion & "'", , 3)
        Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Version", "Installer", myBuildInfo.FileVersion)


        UpdateProgressBar()
        UnpackResources()

        If gHiltonPackageDetected Then
            LogEvent("Hilton package found", , 1)
            Try
                LogEvent("Registry", , 2)
                LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\HiltonPackageDetected", , 3)
                LogEvent("value: yes", , 3)
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "HiltonPackageDetected", "yes")
            Catch ex As Exception
                LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If

        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\IsThisTheSoftwarePackage", , 3)
            LogEvent("value: yes", , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "IsThisTheSoftwarePackage", "yes")
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        NextInstallationStep()
    End Sub

    Private Sub PostDeployment()
        UpdateProgressBar()

        LogEvent("Post deployment steps")
        LogEvent("Registry", , 1)

        LogEvent(cREGKEY_LPCSOFTWARE, , 2)
        LogEvent("Restarted = 'no'", , 3)
        Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE, "Restarted", "no")

        LogEvent("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce", , 2)
        LogEvent("PcHasRebooted = '" & oSettings.Path_PcHasRebooted & "'", , 3)
        Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce", "PcHasRebooted", oSettings.Path_PcHasRebooted)

        LogEvent("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", , 2)
        LogEvent("TaskbarAutohide = '" & oSettings.Path_TaskbarAutohide & "'", , 3)
        Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "TaskbarAutohide", oSettings.Path_TaskbarAutohide)


        'Copy c:\Windows\explorer.exe
        LogEvent("Copy C:\Windows\Explorer.exe to C:\Windows\explorer_.exe", , 1)
        Try
            IO.File.Copy("c:\windows\explorer.exe", "c:\windows\explorer_.exe")
        Catch ex As Exception
            LogEvent("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        If Not IO.File.Exists("c:\windows\explorer_.exe") Then
            LogEvent("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("Destination file does not exist!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            LogEvent("ok", , 2)
        End If

        'Copy iBAHN.bmp
        Dim sUserAccountPicturesFolder As String
        Dim sSiteKioskFolder As String

        If IO.Directory.Exists("C:\ProgramData\Microsoft\User Account Pictures") Then
            sUserAccountPicturesFolder = "C:\ProgramData\Microsoft\User Account Pictures"
        Else
            sUserAccountPicturesFolder = "C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures"
        End If

        If IO.Directory.Exists("C:\Program Files (x86)\SiteKiosk") Then
            sSiteKioskFolder = "C:\Program Files (x86)\SiteKiosk"
        Else
            sSiteKioskFolder = "C:\Program Files\SiteKiosk"
        End If

        LogEvent("Copy " & sSiteKioskFolder & "\Bitmaps\iBAHN.bmp to " & sUserAccountPicturesFolder & "\iBAHN.bmp", , 1)
        Try
            IO.File.Copy(sSiteKioskFolder & "\Bitmaps\iBAHN.bmp", sUserAccountPicturesFolder & "\iBAHN.bmp")
        Catch ex As Exception
            LogEvent("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        If Not IO.File.Exists(sUserAccountPicturesFolder & "\iBAHN.bmp") Then
            LogEvent("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("Destination file does not exist!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            LogEvent("ok", , 2)
        End If


        'LogEvent("Renaming c:\Users\Public to c:\Users\_Public_", , 1)
        'Try
        '    FileIO.FileSystem.RenameDirectory("c:\Users\Public", "_Public_")
        'Catch ex As Exception
        '    LogEvent("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        '    LogEvent("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        'End Try



        LogEvent("DONE")

        Me.ControlBox = True

        If Not gTestMode Then
            tmrExit.Enabled = True
        End If
    End Sub

    Private Sub NextInstallationStep()
        tmrInstallerWait.Enabled = False

        UpdateProgressBar()

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub UpdateProgressBar()
        iProgress += 1

        If iProgress > progressbarInstaller.Maximum Then
            iProgress = progressbarInstaller.Maximum
        End If

        progressbarInstaller.Value = iProgress

        'Try
        '    TaskbarManager.Instance.SetProgressValue(progressbarInstaller.Value, progressbarInstaller.Maximum)
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub InstallationController()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner

        If iApplicationInstallationCount < iApplicationInstallationCountMax Then
            tmrInstallerWait.Enabled = True
        End If

        If gTestMode Then
            'iApplicationInstallationCount = iApplicationInstallationCountMax
        End If


        LogEvent("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)


        If iApplicationInstallationCount = InstallationOrder.NewUI Then
            If Not IO.File.Exists(oSettings.Path_HiltonUI) And Not IO.File.Exists(oSettings.Path_iBAHNUI) And Not IO.File.Exists(oSettings.Path_GuesttekUI) Then
                'Skip new UI's if files are not included
                iApplicationInstallationCount += 1

                LogEvent("skipped", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                LogEvent("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)
            End If
        End If

        If iApplicationInstallationCount = InstallationOrder.AirlineButtonIECompPatch Then
            If Not IO.File.Exists(oSettings.Path_AirlineButtonIECompPatch) Then
                'Skip new AirlineButton patch if file is not included

                LogEvent("skipped", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                LogEvent("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

                iApplicationInstallationCount += 1
            End If
        End If

        If iApplicationInstallationCount = InstallationOrder.LogMeIn And gSkipLogMeIn Then
            'Skip LogMeIn 

            LogEvent("skipped", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            LogEvent("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

            iApplicationInstallationCount += 1
        End If



        Select Case iApplicationInstallationCount
            Case InstallationOrder.LogMeIn
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLogMeIn))
                Me.installThread.Start()
            Case InstallationOrder.Altiris
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallAltiris))
                Me.installThread.Start()
            Case InstallationOrder.LobbyPCAgent
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLobbyPCAgent))
                Me.installThread.Start()
            Case InstallationOrder.LobbyPCWatchdog
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLobbyPCWatchdog))
                Me.installThread.Start()
            Case InstallationOrder.CreateShortcut
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallCreateShortcut))
                Me.installThread.Start()
            Case InstallationOrder.SiteKiosk
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKiosk))
                Me.installThread.Start()
            Case InstallationOrder.iBAHNUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstalliBAHNUpdate))
                Me.installThread.Start()
            Case InstallationOrder.NewUI
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallNewUI))
                Me.installThread.Start()
            Case InstallationOrder.AirlineButtonIECompPatch
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallAirlineButtonIECompPatch))
                Me.installThread.Start()
            Case InstallationOrder.Shortcuts
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallShortCuts))
                Me.installThread.Start()
            Case InstallationOrder.SiteKioskSecurityUpdater
                LogEvent("SiteKioskSecurityUpdater skipped", Logger.MESSAGE_TYPE.LOG_WARNING)

                oProcess.ProcessDone()
                'Me.installThread = New Thread(New ThreadStart(AddressOf Me._SiteKioskSecurity))
                'Me.installThread.Start()
            Case Else
                LogEvent("Done", , 0)

                gBusyInstalling = False

                PostDeployment()
        End Select
    End Sub
#End Region

#Region "Installers"
    Private Sub _InstallLogMeIn()
        Dim sParams As String = ""

        If gLogMeInDeployID <> "" Then
            sParams &= "-spdeployid=" & gLogMeInDeployID
        Else
            If Not IO.File.Exists(oSettings.Path_HiltonUI) Then
                ' Machines to build LMI string
                'sParams &= "-spdeployid=01_rt1b7rkbgp37385pqrdp4xtdbbo5z907p625u" <-- SoftwareOnly string, not for this package
                sParams &= "-spdeployid=01_ji9beh9f0fscc0pjejl0wrdkd4f1q5r8ednq7"
            Else
                ' Hilton LMI string
                sParams &= "-spdeployid=01_ohq4d8ll318nlvencnsq9g6ryb63xumqsv26s"
            End If
        End If

        oLogger.WriteToLog("Installing remote access application")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LogMeIn, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LogMeIn
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "LogMeIn_Installer"
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLobbyPCAgent()
        LogEvent("Installing iBAHN service")
        LogEvent("Path   : " & oSettings.Path_LobbyPCAgent, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCAgent
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "LobbyPCAgentPatchInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLobbyPCWatchdog()
        LogEvent("Installing iBAHN security service")
        LogEvent("Path   : " & oSettings.Path_LobbyPCWatchdog, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCWatchdog
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "LobbyPCWatchDogInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstalliBAHNUpdate()
        LogEvent("Installing iBAHN files")
        LogEvent("Path   : " & oSettings.Path_iBAHNUpdate, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_iBAHNUpdate
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "SiteKiosk7Updater"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallNewUI()
        If oSettings.Path_GuesttekUI <> "" Then
            oLogger.WriteToLog("Installing Guest-tek files")
            oLogger.WriteToLog("Path   : " & oSettings.Path_GuesttekUI, , 1)
            oLogger.WriteToLog("Params : ", , 1)
            oLogger.WriteToLog("Timeout: 300", , 1)

            oProcess = New ProcessRunner
            oProcess.FileName = oSettings.Path_GuesttekUI
            oProcess.Arguments = ""
            oProcess.ExternalAppToWaitFor = "NewUI_iBAHN__Installer"
            oProcess.MaxTimeout = 300
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()
        ElseIf oSettings.Path_iBAHNUI <> "" Then
            oLogger.WriteToLog("Installing iBAHN files")
            oLogger.WriteToLog("Path   : " & oSettings.Path_iBAHNUI, , 1)
            oLogger.WriteToLog("Params : ", , 1)
            oLogger.WriteToLog("Timeout: 300", , 1)

            oProcess = New ProcessRunner
            oProcess.FileName = oSettings.Path_iBAHNUI
            oProcess.Arguments = ""
            oProcess.ExternalAppToWaitFor = "NewUI_iBAHN__Installer"
            oProcess.MaxTimeout = 300
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()
        Else
            oLogger.WriteToLog("Installing Hilton files")
            oLogger.WriteToLog("Path   : " & oSettings.Path_HiltonUI, , 1)
            oLogger.WriteToLog("Params : ", , 1)
            oLogger.WriteToLog("Timeout: 300", , 1)

            oProcess = New ProcessRunner
            oProcess.FileName = oSettings.Path_HiltonUI
            oProcess.Arguments = ""
            oProcess.ExternalAppToWaitFor = "NewUI_Hilton__Installer"
            oProcess.MaxTimeout = 300
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()
        End If
    End Sub

    Private Sub _InstallAirlineButtonIECompPatch()
        LogEvent("Installing AirlineButton IECompatibility Patch")
        LogEvent("Path   : " & oSettings.Path_AirlineButtonIECompPatch, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_AirlineButtonIECompPatch
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "NewUI_AirlineButton_IE_Comp"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallCreateShortcut()
        LogEvent("Installing shortcut creator")
        LogEvent("Path   : " & oSettings.Path_CreateShortcut, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_CreateShortcut
        If gTestMode Then
            oProcess.FileName = "c:\windows\notepad.exe"
        End If
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        If gTestMode Then
            oProcess.ProcessStyle = ProcessWindowStyle.Normal
        End If
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallShortCuts()
        Dim sPath As String
        Dim sTemplate As String
        Dim sShortcut1 As String, sShortcut2 As String, sShortcut3 As String

        'LogEvent(oSettings.Path_CreateShortcut, Logger.MESSAGE_TYPE.LOG_DEBUG)

        'sPath = IO.Path.GetDirectoryName(oSettings.Path_CreateShortcut) & "\shortcuts.template.txt"
        sPath = giBAHNProgramFilesFolder & "\CreateShortcut"
        'sPath = IO.Path.GetDirectoryName(oSettings.Path_CreateShortcut) & "\CreateShortcut"

        'iBAHN\LobbyPCSoftware\CreateShortcut
        Try
            LogEvent("Preparing shortcut creator")

            Dim sr As New IO.StreamReader(sPath & "\shortcuts.template.txt")
            sTemplate = sr.ReadLine()

            sr.Close()

            sShortcut1 = sTemplate

            sShortcut1 = sShortcut1.Replace("%%NAME%%", "Start LobbyPC")
            sShortcut1 = sShortcut1.Replace("%%DESTINATION_FOLDER%%", GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory))
            sShortcut1 = sShortcut1.Replace("%%TARGET_PATH%%", oSettings.Path_AutoStart)
            sShortcut1 = sShortcut1.Replace("%%ARGUMENTS%%", "")
            sShortcut1 = sShortcut1.Replace("%%TARGET_WORKING_DIR%%", IO.Path.GetDirectoryName(oSettings.Path_AutoStart))
            sShortcut1 = sShortcut1.Replace("%%ICON_FILE%%", oSettings.Path_AutoStart)
            sShortcut1 = sShortcut1.Replace("%%ICON_INDEX%%", "0")

            sShortcut2 = sTemplate
            sShortcut2 = sShortcut2.Replace("%%NAME%%", "Start LobbyPC")
            sShortcut2 = sShortcut2.Replace("%%DESTINATION_FOLDER%%", GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu))
            sShortcut2 = sShortcut2.Replace("%%TARGET_PATH%%", oSettings.Path_AutoStart)
            sShortcut2 = sShortcut2.Replace("%%ARGUMENTS%%", "")
            sShortcut2 = sShortcut2.Replace("%%TARGET_WORKING_DIR%%", IO.Path.GetDirectoryName(oSettings.Path_AutoStart))
            sShortcut2 = sShortcut2.Replace("%%ICON_FILE%%", oSettings.Path_AutoStart)
            sShortcut2 = sShortcut2.Replace("%%ICON_INDEX%%", "0")

            sShortcut3 = sTemplate
            sShortcut3 = sShortcut3.Replace("%%NAME%%", "LobbyPC - Set Hotel Information Url")
            sShortcut3 = sShortcut3.Replace("%%DESTINATION_FOLDER%%", GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory))
            sShortcut3 = sShortcut3.Replace("%%TARGET_PATH%%", oSettings.Path_HotelPageUpdater)
            sShortcut3 = sShortcut3.Replace("%%ARGUMENTS%%", "")
            sShortcut3 = sShortcut3.Replace("%%TARGET_WORKING_DIR%%", IO.Path.GetDirectoryName(oSettings.Path_HotelPageUpdater))
            sShortcut3 = sShortcut3.Replace("%%ICON_FILE%%", oSettings.Path_HotelPageUpdater)
            sShortcut3 = sShortcut3.Replace("%%ICON_INDEX%%", "0")


            Dim sw As New IO.StreamWriter(sPath & "\shortcuts.txt", False)
            sw.WriteLine(sShortcut1)
            sw.WriteLine(sShortcut2)
            sw.WriteLine(sShortcut3)
            sw.Close()

            LogEvent("Installing shortcut creator")
            LogEvent("Path   : " & sPath & "\CreateShortcut.exe", , 1)
            LogEvent("Timeout: 300", , 1)

            oProcess = New ProcessRunner
            oProcess.FileName = sPath & "\CreateShortcut.exe"
            oProcess.Arguments = ""
            oProcess.MaxTimeout = 300
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()

        Catch ex As Exception
            LogEvent("CreateShortcut FAILED", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            oProcess.ProcessDone()
        End Try

    End Sub

    Private Sub _InstallAltiris()
        LogEvent("Installing remote administration application")
        LogEvent("Path   : " & oSettings.Path_Altiris, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_Altiris
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "AltirisInstallation"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKiosk()
        LogEvent("Installing kiosk software")
        LogEvent("Path   : " & oSettings.Path_SiteKiosk, , 1)
        LogEvent("Params : /S /V""/qn""", , 1)
        LogEvent("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKiosk
        oProcess.Arguments = "/S /V""/qn"""
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _SiteKioskSecurity()
        LogEvent("Post-SiteKiosk updating")
        LogEvent("Path   : " & oSettings.Path_SiteKioskSecurity, , 1)
        LogEvent("Timeout: 10", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKioskSecurity
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 10
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

#End Region
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationIsCanceled()
        Application.Exit()
        'gBusyInstalling = False

        'MsgBox("Installation cancelled by user", MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation, Application.ProductName)
    End Sub

    Private Sub ApplicationIsExited()
        LogEvent("Exiting application")

        Application.Exit()
    End Sub
#End Region
#End Region

#Region "Events"
#Region "DragWindow events"
    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub picboxOnTopbar_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Process Events"
    Private Sub oProcess_Busy(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Busy
        LogEventToProgressTextBox(".")
    End Sub

    Private Sub oProcess_Test(ByVal EventProcess As System.Diagnostics.Process) Handles oProcess.Test
        LogEvent("test", , 1)
    End Sub

    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("done", , 1)
        LogEvent("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("failed", , 1)
        LogEvent(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        LogEvent("started", , 1)
        LogEvent("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
#End Region

#Region "Timer Events"
    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            NextInstallationStep()
        End If
    End Sub

    Private Sub tmrStartDeployment_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrStartDeployment.Tick
        tmrStartDeployment.Enabled = False

        StartDeployment()
    End Sub

    Private Sub tmrExit_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrExit.Tick
        ApplicationIsExited()
    End Sub
#End Region

    Private Sub txtProgress_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtProgress.MouseDown
        tmrExit.Enabled = False
    End Sub
#End Region
End Class
