Module modAddWindowsUser
    Private Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000

    Public Function AddWindowsUser(ByVal sFullName As String, ByVal sUsername As String, ByVal sPassWord As String, Optional ByVal sGroup As String = "Users") As Boolean
        Try
            Dim oSystem As Object, oUser As Object, oGroup As Object
            Dim sComputerName As String

            sComputerName = Environment.MachineName

            oSystem = GetObject("WinNT://" & sComputerName)
            oUser = oSystem.Create("user", sUsername)
            oUser.FullName = sFullName
            oUser.SetPassword(sPassWord)
            oUser.SetInfo()

            oGroup = GetObject("WinNT://" & sComputerName & "/" & sGroup)
            oGroup.Add("WinNT://" & sComputerName & "/" & sUsername)

            oGroup = Nothing
            oUser = Nothing
            oSystem = Nothing

            Return True

        Catch ex As Exception
            'How's that for exception handling
            Return False
        End Try
    End Function

    Public Function HideWindowsUserInLogonScreen(ByVal sUsername As String) As Boolean
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", sUsername, 0)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function WindowsUserPasswordExpiry(ByVal sUsername As String) As Integer
        Dim oUser As Object
        Dim sComputerName As String
        Dim iPassExp As Integer

        sComputerName = Environment.MachineName

        oUser = GetObject("WinNT://" & sComputerName & "/" & sUsername)

        iPassExp = Int(oUser.MaxPasswordAge / 86400) - Int(oUser.PasswordAge / 86400)

        Return iPassExp
    End Function

    Public Function RemoveWindowsUserPasswordExpiry(ByVal sUsername As String) As Boolean
        Try
            Dim oUser As Object
            Dim iFlagsOld As Integer, iFlagsNew As Integer
            Dim sComputerName As String

            sComputerName = Environment.MachineName

            oUser = GetObject("WinNT://" & sComputerName & "/" & sUsername)

            iFlagsOld = oUser.UserFlags
            iFlagsNew = iFlagsOld Or ADS_UF_DONT_EXPIRE_PASSWD
            oUser.UserFlags = iFlagsNew
            oUser.SetInfo()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ChangeWindowsUserPassword(ByVal sUsername As String, ByVal sPassWord As String) As Boolean
        Try
            Dim oUser As Object

            oUser = GetObject("WinNT://" & Environment.MachineName & "/" & sUsername)
            oUser.SetPassword(sPassWord)

            oUser = Nothing

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function FindWindowsUser(ByVal sUsername As String, Optional ByVal sGroup As String = "Users") As Boolean
        Try
            Dim oUser As Object, oGroup As Object

            oGroup = GetObject("WinNT://" & Environment.MachineName & "/" & sGroup & ",group")

            For Each oUser In oGroup.Members
                If Trim(sUsername) = Trim(oUser.Name) Then
                    Return True
                End If
            Next

            oGroup = Nothing
            oUser = Nothing

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Module
