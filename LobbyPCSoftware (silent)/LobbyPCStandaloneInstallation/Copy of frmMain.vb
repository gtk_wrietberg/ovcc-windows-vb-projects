Imports System.Threading

Public Class frmMain
    Private WizardFrameCollection() As GroupBox

    Private iInstallationStepCount As Integer = 0
    Private iApplicationInstallationCount As Integer = 0
    Private ReadOnly iApplicationInstallationCountMax As Integer = 5

    Private oSettings As Settings
    Private oComputerName As ComputerName
    Private WithEvents oProcess As ProcessRunner
    Private oLogger As Logger

    Private installThread As Thread


    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        WizardFrameCollection = New GroupBox() {Me.grpboxStep1, Me.grpboxStep2, Me.grpboxStep3, Me.grpboxStep4, Me.grpboxStep5}
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Width = 640
        Me.Height = 480

        oLogger = New Logger

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = "v" & myBuildInfo.FileMajorPart & "." & myBuildInfo.FileMinorPart

        oLogger.WriteToLog(myBuildInfo.FileVersion)

        '395; 281
        btnCancel.Top = 405
        btnCancel.Left = 22

        btnPrev.Top = 405
        btnPrev.Left = 455

        btnNext.Top = 405
        btnNext.Left = 535

        '610; 350
        '12; 90
        Dim i As Integer
        For i = 1 To WizardFrameCollection.Length - 1
            WizardFrameCollection(i).Top = WizardFrameCollection(0).Top
            WizardFrameCollection(i).Left = WizardFrameCollection(0).Left
            WizardFrameCollection(i).Width = WizardFrameCollection(0).Width
            WizardFrameCollection(i).Height = WizardFrameCollection(0).Height
            WizardFrameCollection(i).Visible = False
        Next

        btnCancel.BringToFront()
        btnNext.BringToFront()
        btnPrev.BringToFront()

        Dim streamTerms As New IO.StreamReader(New IO.FileStream("TermsAndConditions.txt", IO.FileMode.Open))

        txtTC.Text = ""
        txtTC.AppendText(streamTerms.ReadToEnd)

        'Find the drive letter (usually c:, but you never know)
        gInstallationDrive = Environment.GetFolderPath(Environment.SpecialFolder.System).Chars(0) & ":"

        gAltirisInstallationPath1 = gInstallationDrive & cAltirisInstallationPath1
        gAltirisInstallationPath2 = gInstallationDrive & cAltirisInstallationPath2

        MoveInstallationStep(0)

        oComputerName = New ComputerName
        oSettings = New Settings

        If InStr(oComputerName.ComputerName, "rietberg") > 0 Then
            Button1.Enabled = False
        End If
    End Sub

    Private Sub MoveInstallationStep(Optional ByVal iStep As Integer = 1)
        WizardFrameCollection(iInstallationStepCount).Visible = False
        iInstallationStepCount = iInstallationStepCount + iStep

        If iInstallationStepCount < 0 Then
            iInstallationStepCount = 0
        End If

        If iInstallationStepCount >= WizardFrameCollection.Length Then
            iInstallationStepCount = WizardFrameCollection.Length - 1
        End If

        WizardFrameCollection(iInstallationStepCount).Visible = True

        If iInstallationStepCount >= (WizardFrameCollection.Length - 1) Then
            iInstallationStepCount = WizardFrameCollection.Length - 1
        Else
            btnNext.Text = "Next >"
            btnCancel.Enabled = True

            If iInstallationStepCount <= 0 Then
                iInstallationStepCount = 0

                btnPrev.Enabled = False
            Else
                btnPrev.Enabled = True
            End If
        End If

        InstallationStepSpecialAction()
    End Sub

    Private Sub InstallationStepSpecialAction()
        Select Case iInstallationStepCount
            Case 1
                chkTCAgree.Checked = False
                btnNext.Enabled = False
            Case 2
                txtComputerNameOld.Text = oComputerName.ComputerName
                txtDomainOld.Text = oComputerName.Workgroup
            Case 3
                btnPrev.Visible = False
                btnPrev.Enabled = False
                btnCancel.Enabled = False
                btnCancel.Visible = False
                btnNext.Text = "Finish"
                btnNext.Enabled = False

                iApplicationInstallationCount = 0
                ApplicationInstallationSpecialAction()
            Case 4
                btnNext.Visible = False
                ApplicationIsDone()
        End Select
    End Sub

    Private Sub ApplicationInstallationSpecialAction()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner
        tmrInstallerWait.Enabled = True

        Select Case iApplicationInstallationCount
            Case 0
                Me.installThread = New Thread(New ThreadStart(AddressOf Me.InstallPCAnywhere))
                Me.installThread.Start()
            Case 1
                Me.installThread = New Thread(New ThreadStart(AddressOf Me.InstallAltiris))
                Me.installThread.Start()
            Case 2
                Me.installThread = New Thread(New ThreadStart(AddressOf Me.InstallAltirisCertificate))
                Me.installThread.Start()
            Case 3
                Me.installThread = New Thread(New ThreadStart(AddressOf Me.InstallSiteKiosk))
                Me.installThread.Start()
            Case Else
                btnNext.Enabled = True
        End Select
    End Sub

#Region "Installers"
    Private Sub InstallPCAnywhere()
        LogToProcessTextBox("Installing remote access application")
        LogToProcessTextBox(vbTab & "Path        : " & oSettings.Installer_pcAnywhere_Path)
        LogToProcessTextBox(vbTab & "Params      : " & oSettings.Installer_pcAnywhere_Params)
        LogToProcessTextBox(vbTab & "Timeout     : " & oSettings.Installer_pcAnywhere_Timeout.ToString)
        LogToProcessTextBox(vbTab & "ProcessStyle: " & oSettings.Installer_pcAnywhere_WindowStyle.ToString)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Installer_pcAnywhere_Path
        oProcess.Arguments = oSettings.Installer_pcAnywhere_Params
        oProcess.MaxTimeout = oSettings.Installer_pcAnywhere_Timeout
        oProcess.ProcessStyle = oSettings.Installer_pcAnywhere_WindowStyle
        oProcess.StartProcess()
    End Sub

    Private Sub InstallAltiris()
        Dim sReturn As String = ""

        LogToProcessTextBox("Installing remote administration application")

        LogToProcessTextBox(vbTab & "Creating folders")
        If CreateFolder(gAltirisInstallationPath1) Then
            LogToProcessTextBox(vbTab & vbTab & "ok")
        Else
            LogToProcessTextBox(vbTab & vbTab & "fail")
        End If
        If CreateFolder(gAltirisInstallationPath2) Then
            LogToProcessTextBox(vbTab & vbTab & "ok")
        Else
            LogToProcessTextBox(vbTab & vbTab & "fail")
        End If

        LogToProcessTextBox(vbTab & "Copying files")
        If CopyMultipleFiles(oSettings.Installer_Altiris_Path, gAltirisInstallationPath1, sReturn) Then
            LogToProcessTextBox(vbTab & vbTab & "ok")
        Else
            LogToProcessTextBox(vbTab & vbTab & "fail")
        End If
        LogToProcessTextBox(vbTab & vbTab & sReturn)

        LogToProcessTextBox(vbTab & "Path        : " & gAltirisInstallationPath1 & "\alt_deploy.exe")
        LogToProcessTextBox(vbTab & "Params      : " & oSettings.Installer_Altiris_Params)
        LogToProcessTextBox(vbTab & "Timeout     : " & oSettings.Installer_Altiris_Timeout.ToString)
        LogToProcessTextBox(vbTab & "ProcessStyle: " & oSettings.Installer_Altiris_WindowStyle.ToString)


        oProcess = New ProcessRunner
        oProcess.FileName = gAltirisInstallationPath1 & "\alt_deploy.exe"
        oProcess.Arguments = oSettings.Installer_Altiris_Params
        oProcess.MaxTimeout = oSettings.Installer_Altiris_Timeout
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub InstallAltirisCertificate()
        Dim sReturn As String = ""

        LogToProcessTextBox("Importing certificates")
        'certmgr.exe -add -c "<cert-file>" -s -r localMachine root 
        LogToProcessTextBox(vbTab & "Path   : " & gAltirisInstallationPath1 & "\certmgr.exe")
        LogToProcessTextBox(vbTab & "Params : " & oSettings.Installer_AltirisCertificate_Params)
        LogToProcessTextBox(vbTab & "Timeout: " & oSettings.Installer_AltirisCertificate_Timeout.ToString)

        oProcess = New ProcessRunner
        oProcess.FileName = gAltirisInstallationPath1 & "\certmgr.exe"
        oProcess.Arguments = oSettings.Installer_AltirisCertificate_Params
        oProcess.MaxTimeout = oSettings.Installer_AltirisCertificate_Timeout
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub InstallSiteKiosk()
        LogToProcessTextBox("Installing kiosk software")
        LogToProcessTextBox(vbTab & "Path   : " & oSettings.Installer_SiteKiosk_Path)
        LogToProcessTextBox(vbTab & "Params : " & oSettings.Installer_SiteKiosk_Params)
        LogToProcessTextBox(vbTab & "Timeout: " & oSettings.Installer_SiteKiosk_Timeout.ToString)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Installer_SiteKiosk_Path
        oProcess.Arguments = oSettings.Installer_SiteKiosk_Params
        oProcess.MaxTimeout = oSettings.Installer_SiteKiosk_Timeout
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub
#End Region

    Private Sub ApplicationIsDone()

    End Sub

    Private Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text] & vbCrLf)
        End If
    End Sub

#Region "Button Events"
    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        MoveInstallationStep(1)
    End Sub

    Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        MoveInstallationStep(-1)
    End Sub

    Private Sub btnCancel_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.GotFocus
        btnFocusCatcher.Focus()
    End Sub

    Private Sub btnPrev_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrev.GotFocus
        btnFocusCatcher.Focus()
    End Sub

    Private Sub btnNext_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.GotFocus
        btnFocusCatcher.Focus()
    End Sub
#End Region

    Private Sub chkTCAgree_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTCAgree.CheckedChanged
        btnNext.Enabled = chkTCAgree.Checked
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        oComputerName.ComputerName = txtComputerNameNew.Text
        oComputerName.Workgroup = txtDomainNew.Text
    End Sub

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        LogToProcessTextBox(vbTab & "Done - Time elapsed: " & TimeElapsed.ToString)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Failed
        LogToProcessTextBox(vbTab & "Process failed")
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        LogToProcessTextBox(vbTab & "Process started")
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        LogToProcessTextBox(vbTab & "Process timed out")
        oProcess.ProcessDone()
    End Sub
#End Region

    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            iApplicationInstallationCount += 1
            ApplicationInstallationSpecialAction()

            If iApplicationInstallationCount <= iApplicationInstallationCountMax Then
                progressInstallation.Value = (100 / iApplicationInstallationCountMax) * iApplicationInstallationCount
            End If
        End If
    End Sub
End Class
