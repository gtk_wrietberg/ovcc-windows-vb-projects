Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sSourceFolder As String
        Dim sDestinationFolder As String

        oLogger.WriteToLog("Copying resources")

        sSourceFolder = giBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder
        sDestinationFolder = sSourceFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("Altiris package (US and EMEA use same installer)", , 1)
        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.eu.exe"

        'If gRegion = "US" Then
        '    oLogger.WriteToLog("ALTIRIS PACKAGE (US)", , 1)
        '    oSettings.Path_Altiris = sDestinationFolder & "\ALTIRIS.PACKAGE.US.EXE"
        'Else
        '    oLogger.WriteToLog("ALTIRIS PACKAGE (EU)", , 1)
        '    oSettings.Path_Altiris = sDestinationFolder & "\ALTIRIS.PACKAGE.EU.EXE"
        'End If

        oLogger.WriteToLog("CreateShortcut package", , 1)
        oSettings.Path_CreateShortcut = sDestinationFolder & "\CreateShortcut.package.exe"

        oLogger.WriteToLog("LogMeIn package", , 1)
        oSettings.Path_LogMeIn = sDestinationFolder & "\LogMeIn.deploy.exe"

        oLogger.WriteToLog("iBAHN Updater package (Win7)", , 1)
        oSettings.Path_iBAHNUpdate = sDestinationFolder & "\iBAHNUpdate.package.Win7.exe"

        oLogger.WriteToLog("Hilton Updater package (Win7 - revision 3 - doctyped)", , 1)
        If gRegion = "us" Then
            oSettings.Path_HiltonUI = sDestinationFolder & "\" & cINSTALLERS__HILTON_UI__US
        Else
            oSettings.Path_HiltonUI = sDestinationFolder & "\" & cINSTALLERS__HILTON_UI__EMEA
        End If

        oLogger.WriteToLog("iBAHN UI package", , 1)
        If gRegion = "us" Then
            oSettings.Path_iBAHNUI = sDestinationFolder & "\" & cINSTALLERS__IBAHN_UI__US
        Else
            oSettings.Path_iBAHNUI = sDestinationFolder & "\" & cINSTALLERS__IBAHN_UI__EMEA
        End If

        If IO.File.Exists(oSettings.Path_HiltonUI) Then
            gHiltonPackageDetected = True
        End If

        oLogger.WriteToLog("LobbyPCAgent package", , 1)
        oSettings.Path_LobbyPCAgent = sDestinationFolder & "\LobbyPCAgentPatchInstaller.package.exe"

        oLogger.WriteToLog("LobbyPCWatchdog package", , 1)
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\LobbyPCWatchdog.package.exe"

        oLogger.WriteToLog("SiteKiosk7", , 1)
        oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk7.exe"

        oLogger.WriteToLog("SiteKioskSecurity", , 1)
        oSettings.Path_SiteKioskSecurity = sDestinationFolder & "\SiteKioskSecurityUpdater.exe"

        oLogger.WriteToLog("PcHasRebooted", , 1)
        oSettings.Path_PcHasRebooted = sDestinationFolder & "\PcHasRebooted.exe"


        sDestinationFolder = giBAHNProgramFilesFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("LobbyPCAutoStart", , 1)
        _CopyFile("LobbyPCAutoStart.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("LobbyPCAutoStart.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_AutoStart = sDestinationFolder & "\LobbyPCAutoStart.exe"


        sDestinationFolder = giBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesToolsFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("HotelPageUpdater", , 1)
        _CopyFile("HotelPageUpdater.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("HotelPageUpdater.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_HotelPageUpdater = sDestinationFolder & "\HotelPageUpdater.exe"


        sDestinationFolder = giBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInternalFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("SiteKioskLogoutExtraTasks", , 1)
        _CopyFile("SiteKioskLogoutExtraTasks.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_SiteKioskLogoutExtraTasks = sDestinationFolder & "\SiteKioskLogoutExtraTasks.exe"

        oLogger.WriteToLog("TaskbarAutohide", , 1)
        _CopyFile("TaskbarAutohide.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("TaskbarAutohide.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("TaskbarAutohideEmergency.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("TaskbarAutohideEmergency.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_TaskbarAutohide = sDestinationFolder & "\TaskbarAutohide.exe"
    End Sub

    Private Sub _CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, ByVal iLogLevel As Integer)
        oLogger.WriteToLog("copying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)

        oLogger.WriteToLog("retrying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
    End Sub
End Module
