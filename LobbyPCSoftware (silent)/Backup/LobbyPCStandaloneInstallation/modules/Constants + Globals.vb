Module Constants___Globals
    'Some extra installers
    Public ReadOnly cINSTALLERS__HILTON_UI__EMEA As String = "NewUI.Hilton.revision3.installer.EMEA.exe"
    Public ReadOnly cINSTALLERS__HILTON_UI__US As String = "NewUI.Hilton.revision3.installer.US.exe"
    Public ReadOnly cINSTALLERS__IBAHN_UI__EMEA As String = "NewUI.iBAHN.installer.EMEA.exe"
    Public ReadOnly cINSTALLERS__IBAHN_UI__US As String = "NewUI.iBAHN.installer.US.exe"


    'GUI
    Public ReadOnly cGUI_GroupBoxBorder_visible As Boolean = True
    'Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.FromArgb(158, 19, 14))
    Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.Black)

    'Paths
    Public ReadOnly cPATHS_iBAHNProgramFilesFolder As String = "C:\Program Files\iBAHN\LobbyPCSoftware"
    Public ReadOnly cPATHS_iBAHNProgramFilesFolder_x86 As String = "C:\Program Files (x86)\iBAHN\LobbyPCSoftware"
    Public ReadOnly cPATHS_iBAHNProgramFilesResourcesFolder As String = "\installation_files\Resources"
    Public ReadOnly cPATHS_iBAHNProgramFilesToolsFolder As String = "\tools"
    Public ReadOnly cPATHS_iBAHNProgramFilesInternalFolder As String = "\internal"
    'Public ReadOnly cPATHS_iBAHNProgramFilesInstallationFolder As String = "\installation_files"

    Public ReadOnly cPATHS_ConfigFile As String = "LobbyPCSoftware.config.xml"

    'Debug
    Public ReadOnly cDEBUG_ForcePrerequisiteWindowsVersionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteInternetConnectionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteUserIsAdminError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteAlreadyInstalledError As Boolean = False

    'SiteKiosk
    Public ReadOnly cSITEKIOSK_UserName As String = "sitekiosk"
    Public ReadOnly cSITEKIOSK_PassWord As String = "provisio"

    'Registry
    Public ReadOnly cREGKEY_LPCSOFTWARE As String = "HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPCSoftwareOnly"

    Public ReadOnly cREGKEY_POLICIES_EXPLORER As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
    Public ReadOnly cREGKEY_POLICIES_EXPLORER2 As String = "HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Explorer"

    'Globals. Me like globals, me are lazy
    Public oLogger As Logger
    Public oSettings As Settings

    Public gBusyInstalling As Boolean
    Public gTestMode As Boolean

    Public gAllFilesCopied As Boolean

    Public gHideProgress As Boolean
    Public gRegion As String
    Public gLicenseRegion As String
    Public gSkipSkype As Boolean
    Public gSkipLogMeIn As Boolean

    Public gHiltonPackageDetected As Boolean

    Public gLogMeInDeployID As String

    Public giBAHNProgramFilesFolder As String
End Module
