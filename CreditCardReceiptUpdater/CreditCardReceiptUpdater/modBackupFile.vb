Module modBackupFile
    Public Function BackupFile(ByVal FilePath As String) As Boolean
        Dim sExt As String
        Dim dDate As Date = Now

        sExt = "." & Application.ProductName & " " & dDate.ToShortDateString & " " & dDate.ToLongTimeString & ".bak"
        sExt = sExt.Replace("/", "-")
        sExt = sExt.Replace(":", ".")
        sExt = sExt.Replace(" ", "_")

        Try
            System.IO.File.Copy(FilePath, FilePath & sExt, True)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
End Module
