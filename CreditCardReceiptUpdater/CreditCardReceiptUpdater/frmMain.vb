Public Class frmMain
    Private ReadOnly SCRIPT_FILE_NAME As String = "C:\Program Files\SiteKiosk\SiteCash\Html\CreditCard\success.htm"

    Private ReadOnly STRING_SEARCH As String = "mycfm.CurrencySymbol+SiteKiosk.ScriptDispatch.CustomCreditCardPayment_GetAmount()"
    Private ReadOnly STRING_REPLACE As String = "mycfm.ISOCode+"" ""+SiteKiosk.ScriptDispatch.CustomCreditCardPayment_GetAmount()"

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        UpdateCreditCardReceipt()

        ExitApplication()
    End Sub

    Private Sub ExitApplication(Optional ByVal iExitCode As Integer = 0)
        Environment.Exit(iExitCode)
    End Sub

    Private Sub UpdateCreditCardReceipt()
        If Not System.IO.File.Exists(SCRIPT_FILE_NAME) Then
            ExitApplication(-3)
        End If

        If Not BackupFile(SCRIPT_FILE_NAME) Then
            ExitApplication(-4)
        End If

        Dim sContents As String = ""
        Dim sLine As String
        Dim oRead As New System.IO.StreamReader(SCRIPT_FILE_NAME)

        Do While oRead.Peek() <> -1
            sLine = oRead.ReadLine

            If sLine.IndexOf(STRING_SEARCH) >= 0 Then
                sLine = sLine.Replace(STRING_SEARCH, STRING_REPLACE)
            End If

            sContents = sContents & sLine & vbNewLine
        Loop
        oRead.Close()

        Dim oWrite As New System.IO.StreamWriter(SCRIPT_FILE_NAME)
        oWrite.Write(sContents)
        oWrite.Close()
    End Sub
End Class
