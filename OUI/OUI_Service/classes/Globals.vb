﻿Public Class Globals
    Public Class Constants
        Public Class Registry
            Private Shared ReadOnly OUI_ROOT As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OUI"

            Public Shared ReadOnly KEY__Settings_Heartbeat As String = OUI_ROOT & "\Heartbeat"
            Public Shared ReadOnly VALUE__Settings_Heartbeat_ServerUrl As String = "ServerURL"
            Public Shared ReadOnly DEFAULT__Settings_Heartbeat_ServerUrl As String = "http://172.17.148.139/index.asp"


            Public Shared ReadOnly KEY__Settings_HeartbeatResponse As String = OUI_ROOT & "\Heartbeat\Response"
            Public Shared ReadOnly VALUE__Settings_HeartbeatResponse_LastReturnCode As String = "LastReturnCode"
            Public Shared ReadOnly DEFAULT__Settings_HeartbeatResponse_LastReturnCode As String = ""

            Public Shared ReadOnly VALUE__Settings_HeartbeatResponse_LastReturnMessage As String = "LastReturnMessage"
            Public Shared ReadOnly DEFAULT__Settings_HeartbeatResponse_LastReturnMessage As String = ""

            Public Shared ReadOnly VALUE__Settings_HeartbeatResponse_UpdatesWaiting As String = "UpdatesWaiting"
            Public Shared ReadOnly DEFAULT__Settings_HeartbeatResponse_UpdatesWaiting As String = ""

        End Class
    End Class

    Public Class Settings
        Private Shared mServerUrl_Heartbeat As String
        Public Shared Property ServerUrl_Heartbeat() As String
            Get
                Return mServerUrl_Heartbeat
            End Get
            Set(ByVal value As String)
                mServerUrl_Heartbeat = value
            End Set
        End Property






        '-----------------------------------------------------------------------------------------
        Public Shared Function Load() As Boolean
            mServerUrl_Heartbeat = Helpers.Registry.GetValue_String(Constants.Registry.KEY__Settings_Heartbeat, Constants.Registry.VALUE__Settings_Heartbeat_ServerUrl, Constants.Registry.DEFAULT__Settings_Heartbeat_ServerUrl)

            Return True
        End Function
    End Class
End Class
