﻿Public Class Heartbeat

    Private Shared mReturnCode As String
    Public Shared Property ReturnCode() As String
        Get
            Return mReturnCode
        End Get
        Set(ByVal value As String)
            mReturnCode = value
        End Set
    End Property

    Private Shared mReturnMessage As String
    Public Shared Property ReturnMessage() As String
        Get
            Return mReturnMessage
        End Get
        Set(ByVal value As String)
            mReturnMessage = value
        End Set
    End Property

    Private Shared mUpdatesWaiting As Integer
    Public Shared Property UpdatesWaiting() As Integer
        Get
            Return mUpdatesWaiting
        End Get
        Set(ByVal value As Integer)
            mUpdatesWaiting = value
        End Set
    End Property




    Private Class HeartbeatRequest
        Public Sub New()

        End Sub

        Public Sub New(Version As String, name As String)
            mVersion = Version
            mName = name
        End Sub

        Private mVersion As String
        Public Property Version() As String
            Get
                Return mVersion
            End Get
            Set(ByVal value As String)
                mVersion = value
            End Set
        End Property

        Private mName As String


        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Public ReadOnly Property XML() As String
            Get
                Dim xmlTemplate = <ovcc>
                                      <OUI>
                                          <heartbeat>
                                              <request>
                                                  <client>
                                                      <%= mVersion %>
                                                  </client>
                                                  <name>
                                                      <%= mName %>
                                                  </name>

                                              </request>
                                          </heartbeat>
                                      </OUI>
                                  </ovcc>

                Return xmlTemplate.ToString
            End Get
        End Property
    End Class

    Private Class HeartbeatResponse
        Public Sub New()
            mXML = ""
            mReturnCode = ""
            mReturnMessage = ""
            mUpdatesWaiting = -1
        End Sub

        Private mXML As String
        Public Property XML() As String
            Get
                Return mXML
            End Get
            Set(ByVal value As String)
                mXML = value
            End Set
        End Property

        Private mReturnCode As String
        Public Property ReturnCode() As String
            Get
                Return mReturnCode
            End Get
            Set(ByVal value As String)
                mReturnCode = value
            End Set
        End Property

        Private mReturnMessage As String
        Public Property ReturnMessage() As String
            Get
                Return mReturnMessage
            End Get
            Set(ByVal value As String)
                mReturnMessage = value
            End Set
        End Property

        Private mUpdatesWaiting As Integer
        Public Property UpdatesWaiting() As Integer
            Get
                Return mUpdatesWaiting
            End Get
            Set(ByVal value As Integer)
                mUpdatesWaiting = value
            End Set
        End Property

        Private mLastError As String
        Public Property LastError() As String
            Get
                Return mLastError
            End Get
            Set(ByVal value As String)
                mLastError = value
            End Set
        End Property

        Public Function Parse() As Boolean
            Try
                Dim xmlDoc = System.Xml.Linq.XDocument.Parse(mXML)
                Dim xmlResponseElement = xmlDoc.Root.Element("OUI").Element("heartbeat").Element("response")

                mReturnCode = xmlResponseElement.Element("code").Value
                mReturnMessage = xmlResponseElement.Element("message").Value
                mUpdatesWaiting = Helpers.Types.CastStringToInteger_safe(xmlResponseElement.Element("updates").Value)

                Return True
            Catch ex As Exception
                mLastError = ex.Message
            End Try

            Return False
        End Function
    End Class

    Private Shared mRequest As HeartbeatRequest
    Private Shared mResponse As HeartbeatResponse


    '-------------------------------------
    Public Shared Sub Initialise()
        mRequest = New HeartbeatRequest
        mResponse = New HeartbeatResponse

        mRequest.Version = Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString
        mRequest.Name = Net.Dns.GetHostName
    End Sub

    Public Shared Function Send() As Boolean
        Try
            Dim request As Net.WebRequest = Net.WebRequest.Create(Globals.Settings.ServerUrl_Heartbeat)

            request.Method = "POST"

            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(mRequest.XML)

            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length

            Dim dataStream As IO.Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()


            Dim response As Net.WebResponse = request.GetResponse()

            dataStream = response.GetResponseStream()

            Dim reader As New IO.StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()

            MsgBox(responseFromServer)

            mResponse.XML = responseFromServer
        Catch ex As Exception
            Helpers.Errors.Add(ex.Message)

            Return False
        End Try

        Return True
    End Function

    Public Shared Sub Parse()
        If Not mResponse.Parse() Then
            MsgBox("OOPS")
        End If

        mReturnCode = mResponse.ReturnCode
        mReturnMessage = mResponse.ReturnMessage
        mUpdatesWaiting = mResponse.UpdatesWaiting
    End Sub
End Class
