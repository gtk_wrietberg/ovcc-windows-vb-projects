﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Globals.Settings.Load()

        Heartbeat.Initialise()
        Heartbeat.Send()
        Heartbeat.Parse()

        Helpers.Registry.SetValue_String(Globals.Constants.Registry.KEY__Settings_HeartbeatResponse, Globals.Constants.Registry.VALUE__Settings_HeartbeatResponse_LastReturnCode, Heartbeat.ReturnCode)
        Helpers.Registry.SetValue_String(Globals.Constants.Registry.KEY__Settings_HeartbeatResponse, Globals.Constants.Registry.VALUE__Settings_HeartbeatResponse_LastReturnMessage, Heartbeat.ReturnMessage)
        Helpers.Registry.SetValue_Integer(Globals.Constants.Registry.KEY__Settings_HeartbeatResponse, Globals.Constants.Registry.VALUE__Settings_HeartbeatResponse_UpdatesWaiting, Heartbeat.UpdatesWaiting)
    End Sub

End Class
