Imports System.IO
Imports System.Net
Imports System.Security.Cryptography
Imports System.Text
Imports System.Text.Encoding
Imports System.Text.RegularExpressions
Imports System.Xml

Public Class LicenseKey
    Private mApplicationVersion As Double

    Private mRawServerResponse As String

    Private mLicenseCodeLength As Integer
    Private mLicenseCode As String
    Private mReturnCode As Integer
    Private mReturnMessage As String

    Private mLicenseCount As Integer
    Private mLicenseCountMax As Integer

    Private mWorkgroup As String
    Private mCountryName As String
    Private mBaseName As String

    Private mServerRegion As String

    Private mHotelInformationUrl As String

    Private mSiteKioskLicenseName As String
    Private mSiteKioskLicenseLicensee As String
    Private mSiteKioskLicenseSignature As String

    Private mPrepaidEnabled As Boolean
    Private mCurrencyName As String
    Private mCurrencyCode As String
    Private mHourlyRate As Double

    Private mCreditCardVendorName As String
    Private mCreditCardVendorUser As String
    Private mCreditCardVendorPass As String
    Private mCreditCardVendorSize As String

    Private mCreditCardEnabled As Boolean
    Private mCreditCardMin As Double
    Private mCreditCardMax As Double
    Private mCreditCardStep As Double

    Private mPermitTestLicense As Boolean

    Private ReadOnly cDefaultTimeout = 10000

    Public Event LicenseIsValid()
    Public Event LicenseIsInvalid(ByVal ReturnCode As Integer, ByVal ReturnMessage As String)
    Public Event LicenseIncremented()
    Public Event LicenseNotIncremented(ByVal ReturnCode As Integer, ByVal ReturnMessage As String)

#Region "properties"
    Public Property ApplicationVersion() As Double
        Get
            Return mApplicationVersion
        End Get
        Set(ByVal value As Double)
            mApplicationVersion = value
        End Set
    End Property

    Public Property PermitTestLicense() As Boolean
        Get
            Return mPermitTestLicense
        End Get
        Set(ByVal value As Boolean)
            mPermitTestLicense = value
        End Set
    End Property

    Public ReadOnly Property RawServerResponse() As String
        Get
            Return mRawServerResponse
        End Get
    End Property

    Public Property LicenseCodeLength() As Integer
        Get
            Return mLicenseCodeLength
        End Get
        Set(ByVal value As Integer)
            mLicenseCodeLength = value
        End Set
    End Property

    Public Property LicenseCode() As String
        Get
            Return mLicenseCode
        End Get
        Set(ByVal value As String)
            mLicenseCode = value
        End Set
    End Property

    Public ReadOnly Property ReturnCode() As Integer
        Get
            Return mReturnCode
        End Get
    End Property

    Public ReadOnly Property ReturnMessage() As String
        Get
            Return mReturnMessage
        End Get
    End Property

    Public ReadOnly Property LicenseCount() As Integer
        Get
            Return mLicenseCount
        End Get
    End Property

    Public ReadOnly Property LicenseCountMax() As Integer
        Get
            Return mLicenseCountMax
        End Get
    End Property

    Public ReadOnly Property Workgroup() As String
        Get
            Return mWorkgroup
        End Get
    End Property

    Public ReadOnly Property CountryName() As String
        Get
            Return mCountryName
        End Get
    End Property

    Public ReadOnly Property MachineBaseName() As String
        Get
            Return mBaseName
        End Get
    End Property

    Public ReadOnly Property MachineName() As String
        Get
            Return (mCountryName & mBaseName & "PC" & _zero_padding((mLicenseCount + 1), 3)).ToUpper
        End Get
    End Property

    Public ReadOnly Property ServerRegion() As String
        Get
            Return mServerRegion
        End Get
    End Property

    Public ReadOnly Property HotelInformationUrl() As String
        Get
            Return mHotelInformationUrl
        End Get
    End Property

    Public ReadOnly Property CreditCardVendorName() As String
        Get
            Return mCreditCardVendorName
        End Get
    End Property

    Public ReadOnly Property CreditCardVendorUser() As String
        Get
            Return mCreditCardVendorUser
        End Get
    End Property

    Public ReadOnly Property CreditCardVendorPass() As String
        Get
            Return mCreditCardVendorPass
        End Get
    End Property

    Public ReadOnly Property CreditCardVendorSize() As String
        Get
            Return mCreditCardVendorSize
        End Get
    End Property

    Public ReadOnly Property PrepaidEnabled() As Boolean
        Get
            Return mPrepaidEnabled
        End Get
    End Property

    Public ReadOnly Property CurrencyName() As String
        Get
            Return mCurrencyName
        End Get
    End Property

    Public ReadOnly Property CurrencyCode() As String
        Get
            Return mCurrencyCode
        End Get
    End Property

    Public ReadOnly Property HourlyRate() As Double
        Get
            Return mHourlyRate
        End Get
    End Property

    Public ReadOnly Property SiteKioskLicenseName() As String
        Get
            Return mSiteKioskLicenseName
        End Get
    End Property

    Public ReadOnly Property SiteKioskLicenseLicensee() As String
        Get
            Return mSiteKioskLicenseLicensee
        End Get
    End Property

    Public ReadOnly Property SiteKioskLicenseSignature() As String
        Get
            Return mSiteKioskLicenseSignature
        End Get
    End Property

    Public ReadOnly Property CreditCardEnabled() As Boolean
        Get
            Return mCreditCardEnabled
        End Get
    End Property

    Public ReadOnly Property CreditCardMin() As Double
        Get
            Return mCreditCardMin
        End Get
    End Property

    Public ReadOnly Property CreditCardMax() As Double
        Get
            Return mCreditCardMax
        End Get
    End Property

    Public ReadOnly Property CreditCardStep() As Double
        Get
            Return mCreditCardStep
        End Get
    End Property
#End Region

#Region "privates"
    Private Sub _cleanup()
        Dim re As Regex = New Regex("[^a-z0-9]", RegexOptions.IgnoreCase)

        mLicenseCode = re.Replace(mLicenseCode, "")
    End Sub

    Private Function _sha1(ByVal sText As String) As String
        Dim sha1Obj As New SHA1CryptoServiceProvider
        Dim bytesToHash() As Byte = ASCII.GetBytes(sText)

        bytesToHash = sha1Obj.ComputeHash(bytesToHash)

        Dim strResult As String = ""

        For Each b As Byte In bytesToHash
            strResult += b.ToString("x2")
        Next

        Return strResult
    End Function

    Private Function _validate() As Boolean
        Try
            Dim Request As WebRequest = WebRequest.Create(cLicenseValidationURL)

            Request.Method = "POST"

            Dim PostData As String = ""

            '<iBAHN><SoftwareOnly><Request><License>" & mLicenseCode & "</License></Request></SoftwareOnly></iBAHN>"
            PostData &= "<iBAHN>"
            PostData &= "<SoftwareOnly>"
            PostData &= "<Request>"
            PostData &= "<Version>" & mApplicationVersion.ToString & "</Version>"
            PostData &= "<License>" & mLicenseCode & "</License>"
            PostData &= "</Request>"
            PostData &= "</SoftwareOnly>"
            PostData &= "</iBAHN>"

            Dim ByteArray As Byte() = Encoding.UTF8.GetBytes(PostData)
            Dim Instance As New WebException
            Dim Value As WebExceptionStatus

            Request.ContentType = "text/xml"
            Request.ContentLength = ByteArray.Length
            Request.Timeout = 10000

            Dim dataStream As Stream = Request.GetRequestStream()
            dataStream.Write(ByteArray, 0, ByteArray.Length)
            dataStream.Close()

            Value = Instance.Status

            Dim response As WebResponse = Request.GetResponse()

            dataStream = response.GetResponseStream()

            Dim reader As New StreamReader(dataStream)
            mRawServerResponse = reader.ReadToEnd()

            reader.Close()
            dataStream.Close()
            response.Close()

            Return True
        Catch ex As Exception
            mReturnCode = -1
            mReturnMessage = ex.Message
            mLicenseCount = 0
            mLicenseCountMax = 0

            Return False
        End Try
    End Function

    Private Function _parse_validate() As Boolean
        Try
            Dim xmldoc As New XmlDocument
            Dim xmlRoot As XmlNode
            Dim xmlNodeLicense As XmlNode, xmlNodeResponse As XmlNode, xmlNodeNaming As XmlNode, xmlNodeSettings As XmlNode
            Dim xmlNodeSettingsSiteKioskLicense As XmlNode, xmlNodeSettingsPrepaid As XmlNode
            Dim xmlNodeSettingsCurrency As XmlNode, xmlNodeSettingsCreditCard As XmlNode, xmlNodeSettingsCreditCardVendor As XmlNode

            xmldoc.LoadXml(mRawServerResponse)

            xmlRoot = xmldoc.SelectSingleNode("iBAHN").SelectSingleNode("SoftwareOnly")

            xmlNodeResponse = xmlRoot.SelectSingleNode("Response")
            xmlNodeLicense = xmlRoot.SelectSingleNode("License")
            xmlNodeNaming = xmlRoot.SelectSingleNode("Naming")
            xmlNodeSettings = xmlRoot.SelectSingleNode("Settings")
            xmlNodeSettingsSiteKioskLicense = xmlNodeSettings.SelectSingleNode("SiteKioskLicense")
            xmlNodeSettingsPrepaid = xmlNodeSettings.SelectSingleNode("Prepaid")
            xmlNodeSettingsCurrency = xmlNodeSettings.SelectSingleNode("Currency")
            xmlNodeSettingsCreditCard = xmlNodeSettings.SelectSingleNode("CreditCard")
            xmlNodeSettingsCreditCardVendor = xmlNodeSettingsCreditCard.SelectSingleNode("Vendor")


            '--------------------------------------------------------
            Try
                mReturnCode = Integer.Parse(xmlNodeResponse.SelectSingleNode("ReturnCode").InnerText)
            Catch ex As Exception
                mReturnCode = 666
            End Try

            mReturnMessage = xmlNodeResponse.SelectSingleNode("ReturnMessage").InnerText

            '--------------------------------------------------------
            Try
                mLicenseCount = Integer.Parse(xmlNodeLicense.SelectSingleNode("LicenseCount").InnerText)
            Catch ex As Exception
                mLicenseCount = 0
            End Try

            Try
                mLicenseCountMax = Integer.Parse(xmlNodeLicense.SelectSingleNode("LicenseCountMax").InnerText)
            Catch ex As Exception
                mLicenseCountMax = 0
            End Try

            '--------------------------------------------------------
            mWorkgroup = xmlNodeNaming.SelectSingleNode("Workgroup").InnerText
            mCountryName = xmlNodeNaming.SelectSingleNode("CountryName").InnerText
            mBaseName = xmlNodeNaming.SelectSingleNode("BaseName").InnerText

            '--------------------------------------------------------
            mServerRegion = xmlNodeSettings.SelectSingleNode("ServerRegion").InnerText

            mHotelInformationUrl = xmlNodeSettings.SelectSingleNode("HotelInformationUrl").InnerText

            mSiteKioskLicenseName = xmlNodeSettingsSiteKioskLicense.SelectSingleNode("Name").InnerText
            mSiteKioskLicenseLicensee = xmlNodeSettingsSiteKioskLicense.SelectSingleNode("Licensee").InnerText
            mSiteKioskLicenseSignature = xmlNodeSettingsSiteKioskLicense.SelectSingleNode("Signature").InnerText

            mPrepaidEnabled = True
            Try
                Dim sTmp As String = ""

                sTmp = xmlNodeSettingsPrepaid.SelectSingleNode("Enabled").InnerText.ToLower

                If sTmp = "nope" Or sTmp = "no" Or sTmp = "false" Or sTmp = "0" Then
                    mPrepaidEnabled = False
                End If
            Catch ex As Exception
            End Try

            mCurrencyName = xmlNodeSettingsCurrency.SelectSingleNode("Name").InnerText
            mCurrencyCode = xmlNodeSettingsCurrency.SelectSingleNode("Code").InnerText

            Try
                mHourlyRate = Double.Parse(xmlNodeSettings.SelectSingleNode("HourlyRate").InnerText)
            Catch ex As Exception
                mHourlyRate = 12.0
            End Try

            mCreditCardEnabled = True
            Try
                Dim sTmp As String = ""

                sTmp = xmlNodeSettingsCreditCard.SelectSingleNode("Enabled").InnerText.ToLower

                If sTmp = "nope" Or sTmp = "no" Or sTmp = "false" Or sTmp = "0" Then
                    mCreditCardEnabled = False
                End If
            Catch ex As Exception
            End Try

            mCreditCardVendorName = xmlNodeSettingsCreditCardVendor.SelectSingleNode("Name").InnerText
            mCreditCardVendorUser = xmlNodeSettingsCreditCardVendor.SelectSingleNode("User").InnerText
            mCreditCardVendorPass = xmlNodeSettingsCreditCardVendor.SelectSingleNode("Pass").InnerText
            mCreditCardVendorSize = xmlNodeSettingsCreditCardVendor.SelectSingleNode("Size").InnerText

            Try
                mCreditCardMin = Double.Parse(xmlNodeSettingsCreditCard.SelectSingleNode("AmountMinimum").InnerText)
            Catch ex As Exception
                mCreditCardMin = 5.0
            End Try

            Try
                mCreditCardMax = Double.Parse(xmlNodeSettingsCreditCard.SelectSingleNode("AmountMaximum").InnerText)
            Catch ex As Exception
                mCreditCardMax = 5.0
            End Try

            Try
                mCreditCardStep = Double.Parse(xmlNodeSettingsCreditCard.SelectSingleNode("AmountStep").InnerText)
            Catch ex As Exception
                mCreditCardStep = 1.0
            End Try

            '--------------------------------------------------------
            Return True
        Catch ex As Exception
            mReturnCode = -2
            mReturnMessage = ex.Message
            mLicenseCount = 0
            mLicenseCountMax = 0

            Return False
        End Try
    End Function

    Private Function _increment() As Boolean
        Try
            Dim Request As WebRequest = WebRequest.Create(cLicenseIncrementURL)

            Request.Method = "POST"

            Dim PostData As String = ""

            '<iBAHN><SoftwareOnly><Request><License>" & mLicenseCode & "</License></Request></SoftwareOnly></iBAHN>"
            PostData &= "<iBAHN>"
            PostData &= "<SoftwareOnly>"
            PostData &= "<Request>"
            PostData &= "<Version>" & mApplicationVersion.ToString & "</Version>"
            PostData &= "<License>" & mLicenseCode & "</License>"
            PostData &= "</Request>"
            PostData &= "</SoftwareOnly>"
            PostData &= "</iBAHN>"

            Dim ByteArray As Byte() = Encoding.UTF8.GetBytes(PostData)
            Dim Instance As New WebException
            Dim Value As WebExceptionStatus

            Request.ContentType = "text/xml"
            Request.ContentLength = ByteArray.Length
            Request.Timeout = 10000

            Dim dataStream As Stream = Request.GetRequestStream()
            dataStream.Write(ByteArray, 0, ByteArray.Length)
            dataStream.Close()

            Value = Instance.Status

            Dim response As WebResponse = Request.GetResponse()

            dataStream = response.GetResponseStream()

            Dim reader As New StreamReader(dataStream)
            mRawServerResponse = reader.ReadToEnd()

            reader.Close()
            dataStream.Close()
            response.Close()

            Return True
        Catch ex As Exception
            mReturnCode = -1
            mReturnMessage = ex.Message
            mLicenseCount = 0
            mLicenseCountMax = 0

            Return False
        End Try
    End Function

    Private Function _parse_increment() As Boolean
        Try
            Dim xmldoc As New XmlDocument
            Dim xmlRoot As XmlNode
            Dim xmlNodeResponse As XmlNode

            xmldoc.LoadXml(mRawServerResponse)

            xmlRoot = xmldoc.SelectSingleNode("iBAHN").SelectSingleNode("SoftwareOnly")

            xmlNodeResponse = xmlRoot.SelectSingleNode("Response")

            '--------------------------------------------------------
            Try
                mReturnCode = Integer.Parse(xmlNodeResponse.SelectSingleNode("ReturnCode").InnerText)
            Catch ex As Exception
                mReturnCode = 666
            End Try

            mReturnMessage = xmlNodeResponse.SelectSingleNode("ReturnMessage").InnerText

            '--------------------------------------------------------
            Return True
        Catch ex As Exception
            mReturnCode = -2
            mReturnMessage = ex.Message
            mLicenseCount = 0
            mLicenseCountMax = 0

            Return False
        End Try
    End Function

    Private Function _zero_padding(ByVal iNum As Integer, ByVal iZeros As Integer) As String
        Dim sNum As String

        sNum = iNum.ToString
        If sNum.Length < iZeros Then
            sNum = (New String("0", iZeros - sNum.Length)) & sNum
        End If

        Return sNum
    End Function
#End Region

#Region "publics"
    Public Sub CheckLicense()
        If mPermitTestLicense And mLicenseCode.Equals("00000000000000000000000000000000") Then
            mWorkgroup = "ENG_WORKGROUP"
            mBaseName = "ENG_TEST0"
            mLicenseCount = 0
            mLicenseCountMax = 0
            mHotelInformationUrl = "http://www.ibahn.com"
            mPrepaidEnabled = True
            mCurrencyName = "Euro"
            mCurrencyCode = "EUR"
            mHourlyRate = 12.0
            mCreditCardEnabled = True
            mCreditCardMin = 5.0
            mCreditCardMax = 5.0
            mCreditCardStep = 1.0

            RaiseEvent LicenseIsValid()

            Exit Sub
        End If

        If mLicenseCode.Length <> mLicenseCodeLength Then
            mReturnCode = -1
            mReturnMessage = "Invalid license key"

            RaiseEvent LicenseIsInvalid(mReturnCode, mReturnMessage)

            Exit Sub
        End If

        If _validate() Then
            If _parse_validate() Then
            End If
        End If

        If mReturnCode = 0 Then
            RaiseEvent LicenseIsValid()
        Else
            RaiseEvent LicenseIsInvalid(mReturnCode, mReturnMessage)
        End If
    End Sub

    Public Sub IncrementLicense()
        If _increment() Then
            If _parse_increment() Then
            End If
        End If

        If mReturnCode = 0 Then
            RaiseEvent LicenseIncremented()
        Else
            RaiseEvent LicenseNotIncremented(mReturnCode, mReturnMessage)
        End If
    End Sub
#End Region

    Public Sub New()
        mPermitTestLicense = False
    End Sub
End Class
