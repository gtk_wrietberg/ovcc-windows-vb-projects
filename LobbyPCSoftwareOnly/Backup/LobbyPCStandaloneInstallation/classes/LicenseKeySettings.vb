Public Class LicenseKeySettings
    Private mHotelInformationUrl As String
    Private mServerRegion As String
    Private mSiteKioskLicenseName As String
    Private mSiteKioskLicenseLicensee As String
    Private mSiteKioskLicenseSignature As String
    Private mPrepaidEnabled As Boolean
    Private mCurrencyName As String
    Private mCurrencyCode As String
    Private mHourlyRate As Double
    Private mCreditCardVendorName As String
    Private mCreditCardVendorUser As String
    Private mCreditCardVendorPass As String
    Private mCreditCardVendorSize As String
    Private mCreditCardEnabled As Boolean
    Private mCreditCardMin As Double
    Private mCreditCardMax As Double
    Private mCreditCardStep As Double

    Public Property HotelInformationUrl() As String
        Get
            Return mHotelInformationUrl
        End Get
        Set(ByVal value As String)
            mHotelInformationUrl = value
        End Set
    End Property

    Public Property ServerRegion() As String
        Get
            Return mServerRegion
        End Get
        Set(ByVal value As String)
            mServerRegion = value
        End Set
    End Property

    Public Property SiteKioskLicenseName() As String
        Get
            Return mSiteKioskLicenseName
        End Get
        Set(ByVal value As String)
            mSiteKioskLicenseName = value
        End Set
    End Property

    Public Property SiteKioskLicenseLicensee() As String
        Get
            Return mSiteKioskLicenseLicensee
        End Get
        Set(ByVal value As String)
            mSiteKioskLicenseLicensee = value
        End Set
    End Property

    Public Property SiteKioskLicenseSignature() As String
        Get
            Return mSiteKioskLicenseSignature
        End Get
        Set(ByVal value As String)
            mSiteKioskLicenseSignature = value
        End Set
    End Property

    Public Property PrepaidEnabled() As Boolean
        Get
            Return mPrepaidEnabled
        End Get
        Set(ByVal value As Boolean)
            mPrepaidEnabled = value
        End Set
    End Property

    Public Property CurrencyName() As String
        Get
            Return mCurrencyName
        End Get
        Set(ByVal value As String)
            mCurrencyName = value
        End Set
    End Property

    Public Property CurrencyCode() As String
        Get
            Return mCurrencyCode
        End Get
        Set(ByVal value As String)
            mCurrencyCode = value
        End Set
    End Property

    Public Property HourlyRate() As Double
        Get
            Return mHourlyRate
        End Get
        Set(ByVal value As Double)
            mHourlyRate = value
        End Set
    End Property

    Public Property CreditCardVendorName() As String
        Get
            Return mCreditCardVendorName
        End Get
        Set(ByVal value As String)
            mCreditCardVendorName = value
        End Set
    End Property

    Public Property CreditCardVendorUser() As String
        Get
            Return mCreditCardVendoruser
        End Get
        Set(ByVal value As String)
            mCreditCardVendoruser = value
        End Set
    End Property

    Public Property CreditCardVendorPass() As String
        Get
            Return mCreditCardVendorPass
        End Get
        Set(ByVal value As String)
            mCreditCardVendorPass = value
        End Set
    End Property

    Public Property CreditCardVendorSize() As String
        Get
            Return mCreditCardVendorSize
        End Get
        Set(ByVal value As String)
            mCreditCardVendorSize = value
        End Set
    End Property

    Public Property CreditCardEnabled() As Boolean
        Get
            Return mCreditCardEnabled
        End Get
        Set(ByVal value As Boolean)
            mCreditCardEnabled = value
        End Set
    End Property

    Public Property CreditCardMin() As Double
        Get
            Return mCreditCardMin
        End Get
        Set(ByVal value As Double)
            mCreditCardMin = value
        End Set
    End Property

    Public Property CreditCardMax() As Double
        Get
            Return mCreditCardMax
        End Get
        Set(ByVal value As Double)
            mCreditCardMax = value
        End Set
    End Property

    Public Property CreditCardStep() As Double
        Get
            Return mCreditCardStep
        End Get
        Set(ByVal value As Double)
            mCreditCardStep = value
        End Set
    End Property

    Public Sub New()

    End Sub
End Class
