Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sDestinationFolder As String

        sDestinationFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder

        oLogger.WriteToLog("Altiris package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.exe"

        oLogger.WriteToLog("AdobeReader package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_PdfReader = sDestinationFolder & "\AdobeReader_9.4.package.exe"

        oLogger.WriteToLog("CreateShortcut package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_CreateShortcut = sDestinationFolder & "\CreateShortcut.package.exe"

        oLogger.WriteToLog("pcAnywhere package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_pcAnywhere = sDestinationFolder & "\Engineering.exe"

        oLogger.WriteToLog("FlashPlayer package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_FlashPlayer = sDestinationFolder & "\FlashPlayer.package.exe"

        oLogger.WriteToLog("iBAHN Updater package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_iBAHNUpdate = sDestinationFolder & "\iBAHNUpdate.package.exe"

        oLogger.WriteToLog("LobbyPCAgent package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_LobbyPCAgent = sDestinationFolder & "\LobbyPCAgentPatchInstaller.package.exe"

        oLogger.WriteToLog("LobbyPCWatchdog package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\LobbyPCWatchdog.package.exe"

        oLogger.WriteToLog("PdfProxy package", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_PdfProxy = sDestinationFolder & "\PdfProxy.package.exe"

        oLogger.WriteToLog("SiteKiosk7", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk7.exe"


        sDestinationFolder = cPATHS_iBAHNProgramFilesFolder

        oLogger.WriteToLog("LobbyPCStandalonePostSiteKiosk", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_PostSiteKioskUpdating = sDestinationFolder & "\LobbyPCStandalonePostSiteKiosk.exe"

        oLogger.WriteToLog("PcHasRebooted", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_PcHasRebooted = sDestinationFolder & "\PcHasRebooted.exe"

        oLogger.WriteToLog("SiteKioskAutoStart", , 1)
        oLogger.WriteToLog("path: " & sDestinationFolder, , 2)
        oSettings.Path_AutoStart = sDestinationFolder & "\SiteKioskAutoStart.exe"












        Dim oMyResources As New MyResources

        oMyResources.DestinationFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder


        oLogger.WriteToLog("Unpacking Altiris package", , 1)
        If oMyResources.UnpackResource("Altiris") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_Altiris = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking iBAHNUpdate package", , 1)
        If oMyResources.UnpackResource("iBAHNUpdate") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_iBAHNUpdate = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking LobbyPCAgent package", , 1)
        If oMyResources.UnpackResource("LobbyPCAgent") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_LobbyPCAgent = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking LobbyPCWatchdog package", , 1)
        If oMyResources.UnpackResource("LobbyPCWatchdog") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_LobbyPCWatchdog = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking pcAnywhere package", , 1)
        If oMyResources.UnpackResource("pcAnywhere", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_pcAnywhere = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking shortcut creator", , 1)
        If oMyResources.UnpackResource("CreateShortcut") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_CreateShortcut = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking PdfProxy", , 1)
        If oMyResources.UnpackResource("PdfProxy") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_PdfProxy = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking SiteKiosk package", , 1)
        If oMyResources.UnpackResource("SiteKiosk7", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_SiteKiosk = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking Flash player", , 1)
        If oMyResources.UnpackResource("FlashPlayer", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_FlashPlayer = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking PDF Reader", , 1)
        If oMyResources.UnpackResource("PDFReader", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_PdfReader = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        'This following files go directly into the standard iBAHN folder
        oMyResources.DestinationFolder = cPATHS_iBAHNProgramFilesFolder

        oLogger.WriteToLog("Unpacking LobbyPCAutoStart", , 1)
        If oMyResources.UnpackResource("LobbyPCAutoStart", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_AutoStart = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking PostSiteKioskUpdating", , 1)
        If oMyResources.UnpackResource("LobbyPCStandalonePostSiteKiosk", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_PostSiteKioskUpdating = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        oLogger.WriteToLog("Unpacking PcHasRebooted", , 1)
        If oMyResources.UnpackResource("PcHasRebooted", ".exe") Then
            oLogger.WriteToLog("ok", , 2)
            oLogger.WriteToLog("path: " & oMyResources.LastFullPath, , 2)
            oSettings.Path_PcHasRebooted = oMyResources.LastFullPath
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If
    End Sub
End Module
