<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.tmrInstallerWait = New System.Windows.Forms.Timer(Me.components)
        Me.tmrUnpackWait = New System.Windows.Forms.Timer(Me.components)
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnContinue = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.pnlLanguageSelect = New System.Windows.Forms.Panel()
        Me.lblSelectLanguage = New System.Windows.Forms.Label()
        Me.cmbBoxLanguages = New System.Windows.Forms.ComboBox()
        Me.pnlTermsAndConditions = New System.Windows.Forms.Panel()
        Me.lblTCText = New System.Windows.Forms.Label()
        Me.txtTC = New System.Windows.Forms.RichTextBox()
        Me.chkTCAgree = New System.Windows.Forms.CheckBox()
        Me.pnlLicenseKey = New System.Windows.Forms.Panel()
        Me.grpboxLicenseError = New System.Windows.Forms.GroupBox()
        Me.lblLicenseError = New System.Windows.Forms.Label()
        Me.grpbxLicense = New System.Windows.Forms.GroupBox()
        Me.txtLicenseCode = New System.Windows.Forms.TextBox()
        Me.lblLicenseValidation = New System.Windows.Forms.Label()
        Me.btnClearLicense = New System.Windows.Forms.Button()
        Me.pnlInstallation = New System.Windows.Forms.Panel()
        Me.lblInstallationProgressStep = New System.Windows.Forms.Label()
        Me.lblInstallationProgressText = New System.Windows.Forms.Label()
        Me.progressMarquee = New System.Windows.Forms.ProgressBar()
        Me.progressInstallation = New System.Windows.Forms.ProgressBar()
        Me.txtProgress = New System.Windows.Forms.TextBox()
        Me.pnlDoneAndRestart = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.radRestartLater = New System.Windows.Forms.RadioButton()
        Me.radRestartNow = New System.Windows.Forms.RadioButton()
        Me.lblReboot2 = New System.Windows.Forms.Label()
        Me.lblReboot = New System.Windows.Forms.Label()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.pnlLicenseKeyValidation = New System.Windows.Forms.Panel()
        Me.grpbxLicenseCopy = New System.Windows.Forms.GroupBox()
        Me.pnlLicenseValidationProgress = New System.Windows.Forms.Panel()
        Me.lblLicenseProgress = New System.Windows.Forms.Label()
        Me.txtLicenseCodeCopy = New System.Windows.Forms.TextBox()
        Me.lblLicenseValidationCopy = New System.Windows.Forms.Label()
        Me.pnlPrerequisites = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblPrerequisitesRestart = New System.Windows.Forms.Label()
        Me.lblPrerequisitesError = New System.Windows.Forms.Label()
        Me.pnlBackgroundBorder = New System.Windows.Forms.Panel()
        Me.lblWindowsVersion = New System.Windows.Forms.Label()
        Me.tmrLicenseValidationDelay = New System.Windows.Forms.Timer(Me.components)
        Me.pnlStartScreen = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblStartContinue = New System.Windows.Forms.Label()
        Me.lblStartWarning = New System.Windows.Forms.Label()
        Me.pnlPreInstallWarning = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblInstallationContinueText = New System.Windows.Forms.Label()
        Me.pnlErrorOccurred = New System.Windows.Forms.Panel()
        Me.lblErrorCode = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.picboxLogo = New System.Windows.Forms.PictureBox()
        Me.pnlLanguageSelect.SuspendLayout()
        Me.pnlTermsAndConditions.SuspendLayout()
        Me.pnlLicenseKey.SuspendLayout()
        Me.grpboxLicenseError.SuspendLayout()
        Me.grpbxLicense.SuspendLayout()
        Me.pnlInstallation.SuspendLayout()
        Me.pnlDoneAndRestart.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlLicenseKeyValidation.SuspendLayout()
        Me.grpbxLicenseCopy.SuspendLayout()
        Me.pnlLicenseValidationProgress.SuspendLayout()
        Me.pnlPrerequisites.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.pnlStartScreen.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.pnlPreInstallWarning.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.pnlErrorOccurred.SuspendLayout()
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(124, 2)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(487, 68)
        Me.lblTitle.TabIndex = 3
        Me.lblTitle.Text = "LobbyPC Software installation"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.White
        Me.lblVersion.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(349, 871)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(174, 17)
        Me.lblVersion.TabIndex = 4
        Me.lblVersion.Text = "v1.2.3.4.5"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'tmrInstallerWait
        '
        Me.tmrInstallerWait.Interval = 1000
        '
        'tmrUnpackWait
        '
        Me.tmrUnpackWait.Interval = 500
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnCancel.Location = New System.Drawing.Point(5, 424)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(197, 36)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnContinue
        '
        Me.btnContinue.BackColor = System.Drawing.Color.Transparent
        Me.btnContinue.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnContinue.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnContinue.Location = New System.Drawing.Point(416, 424)
        Me.btnContinue.Name = "btnContinue"
        Me.btnContinue.Size = New System.Drawing.Size(197, 36)
        Me.btnContinue.TabIndex = 3
        Me.btnContinue.Text = "NEXT"
        Me.btnContinue.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.Enabled = False
        Me.btnExit.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnExit.Location = New System.Drawing.Point(428, 501)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(197, 36)
        Me.btnExit.TabIndex = 12
        Me.btnExit.Text = "FINISH"
        Me.btnExit.UseVisualStyleBackColor = False
        Me.btnExit.Visible = False
        '
        'pnlLanguageSelect
        '
        Me.pnlLanguageSelect.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlLanguageSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLanguageSelect.Controls.Add(Me.lblSelectLanguage)
        Me.pnlLanguageSelect.Controls.Add(Me.cmbBoxLanguages)
        Me.pnlLanguageSelect.Location = New System.Drawing.Point(3, 952)
        Me.pnlLanguageSelect.Name = "pnlLanguageSelect"
        Me.pnlLanguageSelect.Size = New System.Drawing.Size(597, 36)
        Me.pnlLanguageSelect.TabIndex = 15
        '
        'lblSelectLanguage
        '
        Me.lblSelectLanguage.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectLanguage.Location = New System.Drawing.Point(10, 6)
        Me.lblSelectLanguage.Name = "lblSelectLanguage"
        Me.lblSelectLanguage.Size = New System.Drawing.Size(305, 20)
        Me.lblSelectLanguage.TabIndex = 14
        Me.lblSelectLanguage.Text = "Please select your language for the installation"
        Me.lblSelectLanguage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbBoxLanguages
        '
        Me.cmbBoxLanguages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBoxLanguages.Enabled = False
        Me.cmbBoxLanguages.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBoxLanguages.FormattingEnabled = True
        Me.cmbBoxLanguages.Location = New System.Drawing.Point(365, 6)
        Me.cmbBoxLanguages.Name = "cmbBoxLanguages"
        Me.cmbBoxLanguages.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbBoxLanguages.Size = New System.Drawing.Size(227, 22)
        Me.cmbBoxLanguages.TabIndex = 13
        Me.cmbBoxLanguages.TabStop = False
        '
        'pnlTermsAndConditions
        '
        Me.pnlTermsAndConditions.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlTermsAndConditions.Controls.Add(Me.lblTCText)
        Me.pnlTermsAndConditions.Controls.Add(Me.txtTC)
        Me.pnlTermsAndConditions.Controls.Add(Me.chkTCAgree)
        Me.pnlTermsAndConditions.Location = New System.Drawing.Point(5, 76)
        Me.pnlTermsAndConditions.Name = "pnlTermsAndConditions"
        Me.pnlTermsAndConditions.Size = New System.Drawing.Size(608, 342)
        Me.pnlTermsAndConditions.TabIndex = 14
        '
        'lblTCText
        '
        Me.lblTCText.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTCText.Location = New System.Drawing.Point(6, 6)
        Me.lblTCText.Name = "lblTCText"
        Me.lblTCText.Size = New System.Drawing.Size(596, 31)
        Me.lblTCText.TabIndex = 5
        Me.lblTCText.Text = "To continue with the LobbyPC installation, you must accept the terms of the Licen" & _
    "se Agreement. To accept the agreement, click the check box below."
        '
        'txtTC
        '
        Me.txtTC.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.txtTC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTC.Location = New System.Drawing.Point(6, 40)
        Me.txtTC.Name = "txtTC"
        Me.txtTC.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.txtTC.Size = New System.Drawing.Size(596, 267)
        Me.txtTC.TabIndex = 3
        Me.txtTC.Text = ""
        '
        'chkTCAgree
        '
        Me.chkTCAgree.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTCAgree.ForeColor = System.Drawing.Color.ForestGreen
        Me.chkTCAgree.Location = New System.Drawing.Point(6, 313)
        Me.chkTCAgree.Name = "chkTCAgree"
        Me.chkTCAgree.Padding = New System.Windows.Forms.Padding(1)
        Me.chkTCAgree.Size = New System.Drawing.Size(395, 22)
        Me.chkTCAgree.TabIndex = 4
        Me.chkTCAgree.Text = "I accept the terms in the License Agreement"
        Me.chkTCAgree.UseVisualStyleBackColor = True
        '
        'pnlLicenseKey
        '
        Me.pnlLicenseKey.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlLicenseKey.Controls.Add(Me.grpboxLicenseError)
        Me.pnlLicenseKey.Controls.Add(Me.grpbxLicense)
        Me.pnlLicenseKey.Location = New System.Drawing.Point(664, 12)
        Me.pnlLicenseKey.Name = "pnlLicenseKey"
        Me.pnlLicenseKey.Size = New System.Drawing.Size(608, 342)
        Me.pnlLicenseKey.TabIndex = 15
        '
        'grpboxLicenseError
        '
        Me.grpboxLicenseError.Controls.Add(Me.lblLicenseError)
        Me.grpboxLicenseError.Location = New System.Drawing.Point(6, 224)
        Me.grpboxLicenseError.Name = "grpboxLicenseError"
        Me.grpboxLicenseError.Size = New System.Drawing.Size(598, 48)
        Me.grpboxLicenseError.TabIndex = 10
        Me.grpboxLicenseError.TabStop = False
        Me.grpboxLicenseError.Visible = False
        '
        'lblLicenseError
        '
        Me.lblLicenseError.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseError.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblLicenseError.Location = New System.Drawing.Point(5, 7)
        Me.lblLicenseError.Name = "lblLicenseError"
        Me.lblLicenseError.Size = New System.Drawing.Size(587, 38)
        Me.lblLicenseError.TabIndex = 0
        Me.lblLicenseError.Text = "Validating license code failed: lblLicenseError lblLicenseError lblLicenseError l" & _
    "blLicenseError lblLicenseError lblLicenseError lblLqgicenseError"
        Me.lblLicenseError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grpbxLicense
        '
        Me.grpbxLicense.Controls.Add(Me.txtLicenseCode)
        Me.grpbxLicense.Controls.Add(Me.lblLicenseValidation)
        Me.grpbxLicense.Location = New System.Drawing.Point(6, 119)
        Me.grpbxLicense.Name = "grpbxLicense"
        Me.grpbxLicense.Size = New System.Drawing.Size(598, 104)
        Me.grpbxLicense.TabIndex = 9
        Me.grpbxLicense.TabStop = False
        Me.grpbxLicense.Text = "License code"
        '
        'txtLicenseCode
        '
        Me.txtLicenseCode.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.txtLicenseCode.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLicenseCode.Location = New System.Drawing.Point(8, 65)
        Me.txtLicenseCode.MaxLength = 32
        Me.txtLicenseCode.Name = "txtLicenseCode"
        Me.txtLicenseCode.Size = New System.Drawing.Size(584, 32)
        Me.txtLicenseCode.TabIndex = 10
        Me.txtLicenseCode.Text = "AAAAAAAA-BBBBBBBB-CCCCCCCC-DDDDDDDD"
        Me.txtLicenseCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblLicenseValidation
        '
        Me.lblLicenseValidation.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseValidation.Location = New System.Drawing.Point(10, 20)
        Me.lblLicenseValidation.Name = "lblLicenseValidation"
        Me.lblLicenseValidation.Size = New System.Drawing.Size(582, 32)
        Me.lblLicenseValidation.TabIndex = 8
        Me.lblLicenseValidation.Text = "In the box below, type your 32-character license key, which you should have recei" & _
    "ved from your Account Executive."
        '
        'btnClearLicense
        '
        Me.btnClearLicense.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnClearLicense.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClearLicense.Location = New System.Drawing.Point(628, 530)
        Me.btnClearLicense.Name = "btnClearLicense"
        Me.btnClearLicense.Size = New System.Drawing.Size(17, 20)
        Me.btnClearLicense.TabIndex = 8
        Me.btnClearLicense.Text = "X"
        Me.btnClearLicense.UseVisualStyleBackColor = False
        '
        'pnlInstallation
        '
        Me.pnlInstallation.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlInstallation.Controls.Add(Me.lblInstallationProgressStep)
        Me.pnlInstallation.Controls.Add(Me.lblInstallationProgressText)
        Me.pnlInstallation.Controls.Add(Me.progressMarquee)
        Me.pnlInstallation.Controls.Add(Me.progressInstallation)
        Me.pnlInstallation.Location = New System.Drawing.Point(633, 271)
        Me.pnlInstallation.Name = "pnlInstallation"
        Me.pnlInstallation.Size = New System.Drawing.Size(608, 385)
        Me.pnlInstallation.TabIndex = 16
        Me.pnlInstallation.UseWaitCursor = True
        '
        'lblInstallationProgressStep
        '
        Me.lblInstallationProgressStep.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallationProgressStep.Location = New System.Drawing.Point(3, 169)
        Me.lblInstallationProgressStep.Name = "lblInstallationProgressStep"
        Me.lblInstallationProgressStep.Size = New System.Drawing.Size(602, 45)
        Me.lblInstallationProgressStep.TabIndex = 7
        Me.lblInstallationProgressStep.Text = "0 / 0"
        Me.lblInstallationProgressStep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblInstallationProgressStep.UseWaitCursor = True
        Me.lblInstallationProgressStep.Visible = False
        '
        'lblInstallationProgressText
        '
        Me.lblInstallationProgressText.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallationProgressText.Location = New System.Drawing.Point(6, 58)
        Me.lblInstallationProgressText.Name = "lblInstallationProgressText"
        Me.lblInstallationProgressText.Size = New System.Drawing.Size(598, 45)
        Me.lblInstallationProgressText.TabIndex = 6
        Me.lblInstallationProgressText.Text = "InstallationProgressText"
        Me.lblInstallationProgressText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblInstallationProgressText.UseWaitCursor = True
        '
        'progressMarquee
        '
        Me.progressMarquee.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.progressMarquee.ForeColor = System.Drawing.Color.Firebrick
        Me.progressMarquee.Location = New System.Drawing.Point(5, 313)
        Me.progressMarquee.Name = "progressMarquee"
        Me.progressMarquee.Size = New System.Drawing.Size(598, 30)
        Me.progressMarquee.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressMarquee.TabIndex = 5
        Me.progressMarquee.UseWaitCursor = True
        '
        'progressInstallation
        '
        Me.progressInstallation.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.progressInstallation.ForeColor = System.Drawing.Color.Firebrick
        Me.progressInstallation.Location = New System.Drawing.Point(5, 350)
        Me.progressInstallation.Name = "progressInstallation"
        Me.progressInstallation.Size = New System.Drawing.Size(598, 30)
        Me.progressInstallation.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressInstallation.TabIndex = 3
        Me.progressInstallation.UseWaitCursor = True
        '
        'txtProgress
        '
        Me.txtProgress.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.txtProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtProgress.Location = New System.Drawing.Point(628, 945)
        Me.txtProgress.Multiline = True
        Me.txtProgress.Name = "txtProgress"
        Me.txtProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtProgress.Size = New System.Drawing.Size(598, 250)
        Me.txtProgress.TabIndex = 4
        '
        'pnlDoneAndRestart
        '
        Me.pnlDoneAndRestart.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlDoneAndRestart.Controls.Add(Me.Panel2)
        Me.pnlDoneAndRestart.Controls.Add(Me.lblReboot2)
        Me.pnlDoneAndRestart.Controls.Add(Me.lblReboot)
        Me.pnlDoneAndRestart.Location = New System.Drawing.Point(606, 965)
        Me.pnlDoneAndRestart.Name = "pnlDoneAndRestart"
        Me.pnlDoneAndRestart.Size = New System.Drawing.Size(608, 342)
        Me.pnlDoneAndRestart.TabIndex = 17
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.radRestartLater)
        Me.Panel2.Controls.Add(Me.radRestartNow)
        Me.Panel2.Location = New System.Drawing.Point(254, 217)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(100, 56)
        Me.Panel2.TabIndex = 6
        '
        'radRestartLater
        '
        Me.radRestartLater.AutoSize = True
        Me.radRestartLater.Location = New System.Drawing.Point(9, 31)
        Me.radRestartLater.Name = "radRestartLater"
        Me.radRestartLater.Size = New System.Drawing.Size(84, 18)
        Me.radRestartLater.TabIndex = 1
        Me.radRestartLater.Text = "Restart later"
        Me.radRestartLater.UseVisualStyleBackColor = True
        '
        'radRestartNow
        '
        Me.radRestartNow.AutoSize = True
        Me.radRestartNow.Checked = True
        Me.radRestartNow.Location = New System.Drawing.Point(9, 7)
        Me.radRestartNow.Name = "radRestartNow"
        Me.radRestartNow.Size = New System.Drawing.Size(85, 18)
        Me.radRestartNow.TabIndex = 0
        Me.radRestartNow.TabStop = True
        Me.radRestartNow.Text = "Restart now"
        Me.radRestartNow.UseVisualStyleBackColor = True
        '
        'lblReboot2
        '
        Me.lblReboot2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReboot2.Location = New System.Drawing.Point(8, 171)
        Me.lblReboot2.Name = "lblReboot2"
        Me.lblReboot2.Size = New System.Drawing.Size(592, 43)
        Me.lblReboot2.TabIndex = 4
        Me.lblReboot2.Text = "You must restart your PC before using the LobbyPC software."
        Me.lblReboot2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblReboot
        '
        Me.lblReboot.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReboot.Location = New System.Drawing.Point(6, 148)
        Me.lblReboot.Name = "lblReboot"
        Me.lblReboot.Size = New System.Drawing.Size(598, 23)
        Me.lblReboot.TabIndex = 2
        Me.lblReboot.Text = "Congratulations! You have successfully installed the LobbyPC software."
        Me.lblReboot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnBack
        '
        Me.btnBack.BackColor = System.Drawing.Color.Transparent
        Me.btnBack.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBack.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnBack.Location = New System.Drawing.Point(15, 501)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(197, 36)
        Me.btnBack.TabIndex = 19
        Me.btnBack.Text = "BACK"
        Me.btnBack.UseVisualStyleBackColor = False
        Me.btnBack.Visible = False
        '
        'pnlLicenseKeyValidation
        '
        Me.pnlLicenseKeyValidation.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlLicenseKeyValidation.Controls.Add(Me.grpbxLicenseCopy)
        Me.pnlLicenseKeyValidation.Location = New System.Drawing.Point(664, 380)
        Me.pnlLicenseKeyValidation.Name = "pnlLicenseKeyValidation"
        Me.pnlLicenseKeyValidation.Size = New System.Drawing.Size(608, 342)
        Me.pnlLicenseKeyValidation.TabIndex = 21
        Me.pnlLicenseKeyValidation.UseWaitCursor = True
        '
        'grpbxLicenseCopy
        '
        Me.grpbxLicenseCopy.Controls.Add(Me.pnlLicenseValidationProgress)
        Me.grpbxLicenseCopy.Controls.Add(Me.txtLicenseCodeCopy)
        Me.grpbxLicenseCopy.Controls.Add(Me.lblLicenseValidationCopy)
        Me.grpbxLicenseCopy.Location = New System.Drawing.Point(6, 119)
        Me.grpbxLicenseCopy.Name = "grpbxLicenseCopy"
        Me.grpbxLicenseCopy.Size = New System.Drawing.Size(598, 104)
        Me.grpbxLicenseCopy.TabIndex = 10
        Me.grpbxLicenseCopy.TabStop = False
        Me.grpbxLicenseCopy.Text = "License code"
        Me.grpbxLicenseCopy.UseWaitCursor = True
        '
        'pnlLicenseValidationProgress
        '
        Me.pnlLicenseValidationProgress.BackColor = System.Drawing.Color.FromArgb(CType(CType(186, Byte), Integer), CType(CType(186, Byte), Integer), CType(CType(186, Byte), Integer))
        Me.pnlLicenseValidationProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLicenseValidationProgress.Controls.Add(Me.lblLicenseProgress)
        Me.pnlLicenseValidationProgress.Location = New System.Drawing.Point(160, 28)
        Me.pnlLicenseValidationProgress.Name = "pnlLicenseValidationProgress"
        Me.pnlLicenseValidationProgress.Size = New System.Drawing.Size(278, 48)
        Me.pnlLicenseValidationProgress.TabIndex = 11
        Me.pnlLicenseValidationProgress.UseWaitCursor = True
        '
        'lblLicenseProgress
        '
        Me.lblLicenseProgress.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseProgress.Location = New System.Drawing.Point(0, 0)
        Me.lblLicenseProgress.Name = "lblLicenseProgress"
        Me.lblLicenseProgress.Size = New System.Drawing.Size(275, 45)
        Me.lblLicenseProgress.TabIndex = 10
        Me.lblLicenseProgress.Text = "Validating license code, please wait..."
        Me.lblLicenseProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblLicenseProgress.UseWaitCursor = True
        '
        'txtLicenseCodeCopy
        '
        Me.txtLicenseCodeCopy.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.txtLicenseCodeCopy.Enabled = False
        Me.txtLicenseCodeCopy.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLicenseCodeCopy.Location = New System.Drawing.Point(8, 65)
        Me.txtLicenseCodeCopy.MaxLength = 32
        Me.txtLicenseCodeCopy.Name = "txtLicenseCodeCopy"
        Me.txtLicenseCodeCopy.Size = New System.Drawing.Size(584, 32)
        Me.txtLicenseCodeCopy.TabIndex = 10
        Me.txtLicenseCodeCopy.Text = "AAAAAAAA-BBBBBBBB-CCCCCCCC-DDDDDDDD"
        Me.txtLicenseCodeCopy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtLicenseCodeCopy.UseWaitCursor = True
        '
        'lblLicenseValidationCopy
        '
        Me.lblLicenseValidationCopy.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseValidationCopy.Location = New System.Drawing.Point(10, 20)
        Me.lblLicenseValidationCopy.Name = "lblLicenseValidationCopy"
        Me.lblLicenseValidationCopy.Size = New System.Drawing.Size(582, 32)
        Me.lblLicenseValidationCopy.TabIndex = 8
        Me.lblLicenseValidationCopy.Text = "Enter your license validation code. This is a unique sixteen-digit code, which ca" & _
    "n be found in your installation pack."
        Me.lblLicenseValidationCopy.UseWaitCursor = True
        '
        'pnlPrerequisites
        '
        Me.pnlPrerequisites.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlPrerequisites.Controls.Add(Me.Panel3)
        Me.pnlPrerequisites.Location = New System.Drawing.Point(3, 591)
        Me.pnlPrerequisites.Name = "pnlPrerequisites"
        Me.pnlPrerequisites.Size = New System.Drawing.Size(608, 342)
        Me.pnlPrerequisites.TabIndex = 22
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblPrerequisitesRestart)
        Me.Panel3.Controls.Add(Me.lblPrerequisitesError)
        Me.Panel3.Location = New System.Drawing.Point(5, 80)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(598, 182)
        Me.Panel3.TabIndex = 1
        '
        'lblPrerequisitesRestart
        '
        Me.lblPrerequisitesRestart.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrerequisitesRestart.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.lblPrerequisitesRestart.Location = New System.Drawing.Point(12, 159)
        Me.lblPrerequisitesRestart.Name = "lblPrerequisitesRestart"
        Me.lblPrerequisitesRestart.Size = New System.Drawing.Size(586, 19)
        Me.lblPrerequisitesRestart.TabIndex = 1
        Me.lblPrerequisitesRestart.Text = "Select FINISH to exit the installation and start again."
        Me.lblPrerequisitesRestart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPrerequisitesError
        '
        Me.lblPrerequisitesError.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrerequisitesError.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.lblPrerequisitesError.Location = New System.Drawing.Point(12, 7)
        Me.lblPrerequisitesError.Name = "lblPrerequisitesError"
        Me.lblPrerequisitesError.Size = New System.Drawing.Size(586, 142)
        Me.lblPrerequisitesError.TabIndex = 0
        Me.lblPrerequisitesError.Text = "Here come the error messages" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lblPrerequisitesError.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlBackgroundBorder
        '
        Me.pnlBackgroundBorder.BackColor = System.Drawing.Color.Transparent
        Me.pnlBackgroundBorder.Location = New System.Drawing.Point(249, 486)
        Me.pnlBackgroundBorder.Name = "pnlBackgroundBorder"
        Me.pnlBackgroundBorder.Size = New System.Drawing.Size(41, 29)
        Me.pnlBackgroundBorder.TabIndex = 23
        '
        'lblWindowsVersion
        '
        Me.lblWindowsVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblWindowsVersion.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWindowsVersion.Location = New System.Drawing.Point(218, 525)
        Me.lblWindowsVersion.Name = "lblWindowsVersion"
        Me.lblWindowsVersion.Size = New System.Drawing.Size(184, 18)
        Me.lblWindowsVersion.TabIndex = 24
        Me.lblWindowsVersion.Text = "WindowsVersion"
        Me.lblWindowsVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tmrLicenseValidationDelay
        '
        Me.tmrLicenseValidationDelay.Interval = 500
        '
        'pnlStartScreen
        '
        Me.pnlStartScreen.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlStartScreen.Controls.Add(Me.GroupBox2)
        Me.pnlStartScreen.Location = New System.Drawing.Point(642, 41)
        Me.pnlStartScreen.Name = "pnlStartScreen"
        Me.pnlStartScreen.Size = New System.Drawing.Size(608, 342)
        Me.pnlStartScreen.TabIndex = 25
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblStartContinue)
        Me.GroupBox2.Controls.Add(Me.lblStartWarning)
        Me.GroupBox2.Location = New System.Drawing.Point(5, 123)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(598, 96)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'lblStartContinue
        '
        Me.lblStartContinue.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartContinue.ForeColor = System.Drawing.Color.Black
        Me.lblStartContinue.Location = New System.Drawing.Point(8, 74)
        Me.lblStartContinue.Name = "lblStartContinue"
        Me.lblStartContinue.Size = New System.Drawing.Size(586, 19)
        Me.lblStartContinue.TabIndex = 1
        Me.lblStartContinue.Text = "Select Next to start the installation."
        Me.lblStartContinue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStartWarning
        '
        Me.lblStartWarning.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartWarning.ForeColor = System.Drawing.Color.Black
        Me.lblStartWarning.Location = New System.Drawing.Point(6, 15)
        Me.lblStartWarning.Name = "lblStartWarning"
        Me.lblStartWarning.Size = New System.Drawing.Size(586, 46)
        Me.lblStartWarning.TabIndex = 0
        Me.lblStartWarning.Text = "Before starting the installation, it is highly recommended that you close all oth" & _
    "er applications."
        Me.lblStartWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlPreInstallWarning
        '
        Me.pnlPreInstallWarning.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlPreInstallWarning.Controls.Add(Me.GroupBox1)
        Me.pnlPreInstallWarning.Location = New System.Drawing.Point(19, 478)
        Me.pnlPreInstallWarning.Name = "pnlPreInstallWarning"
        Me.pnlPreInstallWarning.Size = New System.Drawing.Size(608, 342)
        Me.pnlPreInstallWarning.TabIndex = 27
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblInstallationContinueText)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 123)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(598, 96)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'lblInstallationContinueText
        '
        Me.lblInstallationContinueText.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallationContinueText.ForeColor = System.Drawing.Color.Black
        Me.lblInstallationContinueText.Location = New System.Drawing.Point(6, 39)
        Me.lblInstallationContinueText.Name = "lblInstallationContinueText"
        Me.lblInstallationContinueText.Size = New System.Drawing.Size(586, 19)
        Me.lblInstallationContinueText.TabIndex = 1
        Me.lblInstallationContinueText.Text = "Select Next to start the installation, or cancel to exit."
        Me.lblInstallationContinueText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlErrorOccurred
        '
        Me.pnlErrorOccurred.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlErrorOccurred.Controls.Add(Me.lblErrorCode)
        Me.pnlErrorOccurred.Controls.Add(Me.Label1)
        Me.pnlErrorOccurred.Controls.Add(Me.Label2)
        Me.pnlErrorOccurred.Location = New System.Drawing.Point(346, 511)
        Me.pnlErrorOccurred.Name = "pnlErrorOccurred"
        Me.pnlErrorOccurred.Size = New System.Drawing.Size(608, 342)
        Me.pnlErrorOccurred.TabIndex = 28
        '
        'lblErrorCode
        '
        Me.lblErrorCode.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorCode.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblErrorCode.Location = New System.Drawing.Point(8, 312)
        Me.lblErrorCode.Name = "lblErrorCode"
        Me.lblErrorCode.Size = New System.Drawing.Size(592, 19)
        Me.lblErrorCode.TabIndex = 5
        Me.lblErrorCode.Text = "Error code:  # 3"
        Me.lblErrorCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(8, 171)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(592, 43)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Please exit this installer and try again. If the problem occurs again, please con" & _
    "tact the helpdesk."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(6, 148)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(598, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "A fatal error occurred during the installation of the LobbyPC software!"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picboxLogo
        '
        Me.picboxLogo.BackgroundImage = Global.LobbyPCSoftwareOnly.My.Resources.Resources.logo_Guest_tek_png
        Me.picboxLogo.Location = New System.Drawing.Point(5, 3)
        Me.picboxLogo.Name = "picboxLogo"
        Me.picboxLogo.Size = New System.Drawing.Size(120, 68)
        Me.picboxLogo.TabIndex = 29
        Me.picboxLogo.TabStop = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1276, 745)
        Me.Controls.Add(Me.pnlStartScreen)
        Me.Controls.Add(Me.pnlInstallation)
        Me.Controls.Add(Me.pnlPreInstallWarning)
        Me.Controls.Add(Me.picboxLogo)
        Me.Controls.Add(Me.pnlErrorOccurred)
        Me.Controls.Add(Me.pnlPrerequisites)
        Me.Controls.Add(Me.pnlDoneAndRestart)
        Me.Controls.Add(Me.pnlTermsAndConditions)
        Me.Controls.Add(Me.pnlLicenseKeyValidation)
        Me.Controls.Add(Me.pnlLicenseKey)
        Me.Controls.Add(Me.lblWindowsVersion)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.btnBack)
        Me.Controls.Add(Me.btnClearLicense)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnContinue)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.txtProgress)
        Me.Controls.Add(Me.pnlBackgroundBorder)
        Me.Controls.Add(Me.pnlLanguageSelect)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LobbyPC Software"
        Me.TopMost = True
        Me.pnlLanguageSelect.ResumeLayout(False)
        Me.pnlTermsAndConditions.ResumeLayout(False)
        Me.pnlLicenseKey.ResumeLayout(False)
        Me.grpboxLicenseError.ResumeLayout(False)
        Me.grpbxLicense.ResumeLayout(False)
        Me.grpbxLicense.PerformLayout()
        Me.pnlInstallation.ResumeLayout(False)
        Me.pnlDoneAndRestart.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pnlLicenseKeyValidation.ResumeLayout(False)
        Me.grpbxLicenseCopy.ResumeLayout(False)
        Me.grpbxLicenseCopy.PerformLayout()
        Me.pnlLicenseValidationProgress.ResumeLayout(False)
        Me.pnlPrerequisites.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.pnlStartScreen.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.pnlPreInstallWarning.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.pnlErrorOccurred.ResumeLayout(False)
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents tmrInstallerWait As System.Windows.Forms.Timer
    Friend WithEvents tmrUnpackWait As System.Windows.Forms.Timer
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnContinue As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents pnlLanguageSelect As System.Windows.Forms.Panel
    Friend WithEvents lblSelectLanguage As System.Windows.Forms.Label
    Friend WithEvents cmbBoxLanguages As System.Windows.Forms.ComboBox
    Friend WithEvents pnlTermsAndConditions As System.Windows.Forms.Panel
    Friend WithEvents txtTC As System.Windows.Forms.RichTextBox
    Friend WithEvents chkTCAgree As System.Windows.Forms.CheckBox
    Friend WithEvents pnlLicenseKey As System.Windows.Forms.Panel
    Friend WithEvents grpbxLicense As System.Windows.Forms.GroupBox
    Friend WithEvents btnClearLicense As System.Windows.Forms.Button
    Friend WithEvents lblLicenseValidation As System.Windows.Forms.Label
    Friend WithEvents pnlInstallation As System.Windows.Forms.Panel
    Friend WithEvents progressMarquee As System.Windows.Forms.ProgressBar
    Friend WithEvents txtProgress As System.Windows.Forms.TextBox
    Friend WithEvents progressInstallation As System.Windows.Forms.ProgressBar
    Friend WithEvents pnlDoneAndRestart As System.Windows.Forms.Panel
    Friend WithEvents lblReboot As System.Windows.Forms.Label
    Friend WithEvents btnBack As System.Windows.Forms.Button
    Friend WithEvents lblReboot2 As System.Windows.Forms.Label
    Friend WithEvents pnlLicenseKeyValidation As System.Windows.Forms.Panel
    Friend WithEvents pnlPrerequisites As System.Windows.Forms.Panel
    Friend WithEvents lblLicenseProgress As System.Windows.Forms.Label
    Friend WithEvents lblInstallationProgressText As System.Windows.Forms.Label
    Friend WithEvents lblInstallationProgressStep As System.Windows.Forms.Label
    Friend WithEvents pnlBackgroundBorder As System.Windows.Forms.Panel
    Friend WithEvents lblWindowsVersion As System.Windows.Forms.Label
    Friend WithEvents lblPrerequisitesError As System.Windows.Forms.Label
    Friend WithEvents txtLicenseCode As System.Windows.Forms.TextBox
    Friend WithEvents pnlLicenseValidationProgress As System.Windows.Forms.Panel
    Friend WithEvents grpbxLicenseCopy As System.Windows.Forms.GroupBox
    Friend WithEvents txtLicenseCodeCopy As System.Windows.Forms.TextBox
    Friend WithEvents lblLicenseValidationCopy As System.Windows.Forms.Label
    Friend WithEvents tmrLicenseValidationDelay As System.Windows.Forms.Timer
    Friend WithEvents grpboxLicenseError As System.Windows.Forms.GroupBox
    Friend WithEvents lblLicenseError As System.Windows.Forms.Label
    Friend WithEvents lblPrerequisitesRestart As System.Windows.Forms.Label
    Friend WithEvents lblTCText As System.Windows.Forms.Label
    Friend WithEvents pnlStartScreen As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblStartContinue As System.Windows.Forms.Label
    Friend WithEvents lblStartWarning As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents radRestartLater As System.Windows.Forms.RadioButton
    Friend WithEvents radRestartNow As System.Windows.Forms.RadioButton
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents pnlPreInstallWarning As System.Windows.Forms.Panel
    Friend WithEvents pnlErrorOccurred As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblErrorCode As System.Windows.Forms.Label
    Friend WithEvents picboxLogo As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblInstallationContinueText As System.Windows.Forms.Label

End Class
