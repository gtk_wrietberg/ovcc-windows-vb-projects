Public Class RegistryChanger
    Private oXml As _XmlDoc

    Public Enum RegistryRoot
        ClassesRoot
        CurrentConfig
        CurrentUser
        DynData
        LocalMachine
        PerformanceData
        Users
    End Enum

    Public Sub New()
        oXml = New _XmlDoc

        oXml.XmlFilePath = cPATHS_iBAHNProgramFilesFolder & "\"
    End Sub

    Public Sub Start()
        oXml.Open()
    End Sub

    Public Sub Done()
        oXml.Close()
    End Sub

    Public Function UpdateKey(ByVal keyRoot As Microsoft.Win32.RegistryKey, ByVal keyName As String, ByVal valueName As String, ByVal value As Object, Optional ByVal SkipRollBack As Boolean = False) As Boolean
        Return UpdateKey(keyRoot, keyName, valueName, value, Microsoft.Win32.RegistryValueKind.Unknown, SkipRollBack)
    End Function

    Public Function UpdateKey(ByVal keyRoot As Microsoft.Win32.RegistryKey, ByVal keyName As String, ByVal valueName As String, ByVal valueNew As Object, ByVal forceType As Microsoft.Win32.RegistryValueKind, ByVal SkipRollBack As Boolean) As Boolean
        Dim existingValue As Object, existingValueKind As Microsoft.Win32.RegistryValueKind

        Try
            oLogger.WriteToLogRelative("Registry", , 1)
            oLogger.WriteToLogRelative("root      :" & keyRoot.Name, , 2)
            oLogger.WriteToLogRelative("key       : " & keyName, , 2)
            oLogger.WriteToLogRelative("value name: " & valueName, , 2)
            oLogger.WriteToLogRelative("value     : " & valueNew.ToString, , 2)

            Dim rk As Microsoft.Win32.RegistryKey = keyRoot.OpenSubKey(keyName, True)

            If Not SkipRollBack Then
                If rk Is Nothing Then
                    oXml.Add(keyRoot.Name & "\" & keyName, valueName, "(does not exist)", Microsoft.Win32.RegistryValueKind.Unknown, valueNew, forceType)
                Else
                    existingValue = rk.GetValue(valueName, "(does not exist)")
                    Try
                        existingValueKind = rk.GetValueKind(keyName)
                    Catch ex As Exception
                        existingValueKind = Microsoft.Win32.RegistryValueKind.Unknown
                    End Try

                    oXml.Add(keyRoot.Name & "\" & keyName, valueName, existingValue, existingValueKind, valueNew, forceType)
                    oLogger.WriteToLogRelative("Saved for rollback", , 1)
                End If
            End If

            If Not gTestMode Then
                rk = keyRoot.CreateSubKey(keyName)

                If forceType = Microsoft.Win32.RegistryValueKind.Unknown Then
                    rk.SetValue(valueName, valueNew)
                Else
                    rk.SetValue(valueName, valueNew, forceType)
                End If
            Else
                oLogger.WriteToLogRelative("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Function

    '===============================================================================================================
    Private Class _XmlDoc
        Private ReadOnly DefaultXmlFile As String = "LobbyPCSoftwareOnlyInstallation.registry.backup.xml"

        Private mXmlFilePath As String

        Private mXmlDocument As Xml.XmlDocument
        Private mXmlNodeRoot As Xml.XmlNode
        Private mXmlNodeRegistry As Xml.XmlNode

        Public Property XmlFilePath() As String
            Get
                Return mXmlFilePath
            End Get
            Set(ByVal value As String)
                mXmlFilePath = value
            End Set
        End Property

        Private Function _CreateXml() As Boolean
            Try
                If IO.File.Exists(mXmlFilePath & DefaultXmlFile) Then
                    IO.File.Delete(mXmlFilePath & DefaultXmlFile)
                End If

                mXmlDocument = New Xml.XmlDocument

                mXmlNodeRoot = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "LobbyPCSoftwareOnly", "")

                mXmlNodeRegistry = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "registry-backup", "")

                Return True
            Catch ex As Exception
                oLogger.WriteToLogRelative("ERROR in _CreateXml()", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End Try
        End Function

        Private Function _SaveXml() As Boolean
            Try
                mXmlNodeRoot.AppendChild(mXmlNodeRegistry)
                mXmlDocument.AppendChild(mXmlNodeRoot)

                mXmlDocument.Save(mXmlFilePath & DefaultXmlFile)

                Return True
            Catch ex As Exception
                oLogger.WriteToLogRelative("ERROR in _SaveXml()", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End Try
        End Function

        Public Function Open() As Boolean
            Return _CreateXml()
        End Function

        Public Function Close() As Boolean
            Return _SaveXml()
        End Function

        Public Function Add(ByVal keyName As String, ByVal valueName As String, ByVal valueOld As Object, ByVal valueKindOld As Microsoft.Win32.RegistryValueKind, ByVal valueNew As Object, ByVal valueKindNew As Microsoft.Win32.RegistryValueKind) As Boolean
            Dim xmlNodeKey As Xml.XmlNode, xmlNodeKeyOld As Xml.XmlNode, xmlNodeKeyNew As Xml.XmlNode
            Dim xmlNodeName As Xml.XmlNode, xmlNodeValueName As Xml.XmlNode
            Dim xmlNodeValueOld As Xml.XmlNode, xmlNodeValueKindOld As Xml.XmlNode
            Dim xmlNodeValueNew As Xml.XmlNode, xmlNodeValueKindNew As Xml.XmlNode

            xmlNodeKey = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "key", "")

            xmlNodeName = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "name", "")
            xmlNodeName.InnerText = keyName

            xmlNodeValueName = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "valuename", "")
            xmlNodeValueName.InnerText = valueName

            xmlNodeKeyOld = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "old", "")
            xmlNodeKeyNew = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "new", "")

            xmlNodeValueOld = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "value", "")
            xmlNodeValueKindOld = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "type", "")
            xmlNodeValueNew = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "value", "")
            xmlNodeValueKindNew = mXmlDocument.CreateNode(Xml.XmlNodeType.Element, "", "type", "")

            xmlNodeValueOld.InnerText = valueOld.ToString
            xmlNodeValueKindOld.InnerText = valueKindOld
            xmlNodeValueNew.InnerText = valueNew.ToString
            xmlNodeValueKindNew.InnerText = valueKindNew

            xmlNodeKeyOld.AppendChild(xmlNodeValueOld)
            xmlNodeKeyOld.AppendChild(xmlNodeValueKindOld)
            xmlNodeKeyNew.AppendChild(xmlNodeValueNew)
            xmlNodeKeyNew.AppendChild(xmlNodeValueKindNew)

            xmlNodeKey.AppendChild(xmlNodeName)
            xmlNodeKey.AppendChild(xmlNodeValueName)
            xmlNodeKey.AppendChild(xmlNodeKeyOld)
            xmlNodeKey.AppendChild(xmlNodeKeyNew)

            mXmlNodeRegistry.AppendChild(xmlNodeKey)
        End Function

    End Class

End Class
