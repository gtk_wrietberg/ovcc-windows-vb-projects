﻿Public Class NotificationClient
    Public Shared Function GetNotificationsCount()
        Dim mList As List(Of String) = Helpers.Registry64.FindValues(
                                            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                                            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationType_SEARCHSTRING)

        Return mList.Count
    End Function

    Public Shared Function GetNextNotification(ByRef _notification As NamedPipe.Notification.Notification) As Boolean
        Dim mList As List(Of String) = Helpers.Registry64.FindValues(
                                            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                                            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationType_SEARCHSTRING)


        Dim iMin As Integer = Integer.MaxValue

        For Each _notification_value As String In mList
            Dim sIndex As String = _notification_value.Replace(Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationType_SEARCHSTRING, "")

            Dim iIndex As Integer
            If Integer.TryParse(sIndex, iIndex) Then
                If iIndex < iMin Then
                    iMin = iIndex
                End If
            End If
        Next

        If iMin < Integer.MaxValue Then
            Dim iType As Integer = Settings.Get.Integer(
                                        Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                                        Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationType_SEARCHSTRING & iMin.ToString,
                                        0)

            Dim sText As String = Settings.Get.String(
                                        Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                                        Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationText_SEARCHSTRING & iMin.ToString,
                                        0)


            NamedPipe.Notification.Client.RemoveNotification(iMin)

            _notification = New NamedPipe.Notification.Notification(iMin, iType, sText)

            Return True
        Else
            _notification = Nothing

            Return False
        End If
    End Function
End Class
