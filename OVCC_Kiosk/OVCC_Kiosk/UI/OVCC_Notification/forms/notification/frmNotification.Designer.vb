﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNotification
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlContainer = New Panel()
        picClose = New PictureBox()
        lblText = New Label()
        picIcon = New PictureBox()
        progressAutoClose = New ProgressBar()
        tmrAutoclose = New Timer(components)
        tmrFadeIn = New Timer(components)
        tmrFadeOut = New Timer(components)
        pnlContainer.SuspendLayout()
        CType(picClose, ComponentModel.ISupportInitialize).BeginInit()
        CType(picIcon, ComponentModel.ISupportInitialize).BeginInit()
        SuspendLayout()
        ' 
        ' pnlContainer
        ' 
        pnlContainer.BackColor = Color.FromArgb(CByte(255), CByte(223), CByte(223))
        pnlContainer.BorderStyle = BorderStyle.FixedSingle
        pnlContainer.Controls.Add(picClose)
        pnlContainer.Controls.Add(lblText)
        pnlContainer.Controls.Add(picIcon)
        pnlContainer.Controls.Add(progressAutoClose)
        pnlContainer.Location = New Point(12, 12)
        pnlContainer.Name = "pnlContainer"
        pnlContainer.Size = New Size(506, 72)
        pnlContainer.TabIndex = 0
        ' 
        ' picClose
        ' 
        picClose.Image = My.Resources.Resources.notification_icons__error__close
        picClose.Location = New Point(469, 19)
        picClose.Name = "picClose"
        picClose.Size = New Size(32, 32)
        picClose.SizeMode = PictureBoxSizeMode.StretchImage
        picClose.TabIndex = 1
        picClose.TabStop = False
        ' 
        ' lblText
        ' 
        lblText.Font = New Font("Tahoma", 12F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblText.Location = New Point(73, 3)
        lblText.Name = "lblText"
        lblText.Size = New Size(390, 64)
        lblText.TabIndex = 2
        lblText.Text = "Label1"
        lblText.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' picIcon
        ' 
        picIcon.Image = My.Resources.Resources.notification_icons__error
        picIcon.Location = New Point(3, 3)
        picIcon.Name = "picIcon"
        picIcon.Size = New Size(64, 64)
        picIcon.SizeMode = PictureBoxSizeMode.StretchImage
        picIcon.TabIndex = 0
        picIcon.TabStop = False
        ' 
        ' progressAutoClose
        ' 
        progressAutoClose.Dock = DockStyle.Bottom
        progressAutoClose.Location = New Point(0, 68)
        progressAutoClose.Name = "progressAutoClose"
        progressAutoClose.Size = New Size(504, 2)
        progressAutoClose.Style = ProgressBarStyle.Continuous
        progressAutoClose.TabIndex = 1
        ' 
        ' tmrAutoclose
        ' 
        ' 
        ' tmrFadeIn
        ' 
        tmrFadeIn.Interval = 20
        ' 
        ' tmrFadeOut
        ' 
        tmrFadeOut.Interval = 20
        ' 
        ' frmNotification
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(532, 96)
        ControlBox = False
        Controls.Add(pnlContainer)
        Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmNotification"
        ShowIcon = False
        ShowInTaskbar = False
        Text = "frmNotification"
        pnlContainer.ResumeLayout(False)
        CType(picClose, ComponentModel.ISupportInitialize).EndInit()
        CType(picIcon, ComponentModel.ISupportInitialize).EndInit()
        ResumeLayout(False)
    End Sub

    Friend WithEvents pnlContainer As Panel
    Friend WithEvents picIcon As PictureBox
    Friend WithEvents picClose As PictureBox
    Friend WithEvents lblText As Label
    Friend WithEvents tmrAutoclose As Timer
    Friend WithEvents progressAutoClose As ProgressBar
    Friend WithEvents tmrFadeIn As Timer
    Friend WithEvents tmrFadeOut As Timer
End Class
