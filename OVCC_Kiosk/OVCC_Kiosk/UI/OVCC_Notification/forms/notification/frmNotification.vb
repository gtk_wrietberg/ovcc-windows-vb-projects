﻿Public Class frmNotification
    Public NotificationType As NamedPipe.Notification.NotificationType
    Public NotificationText As String


    Private mFadePositionSteps As Long = 15
    Private mFadePositionStep As Long = 0

    Private mFadePositionXStart As Long = 0
    Private mFadePositionXEnd As Long = 0
    Private mFadePositionX As Long = 0

    Private mFadePositionYStart As Long = 0
    Private mFadePositionYEnd As Long = 0
    Private mFadePositionY As Long = 0


    Private mAutoCloseCounter As Integer = 50


    Private Sub frmNotificationError_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sPosition As String = Settings.Get.String(
            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__NotificationPosition,
            "right")

        mFadePositionSteps = Settings.Get.Integer(
            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__NotificationPositionAnimationSteps,
            15)


        If mFadePositionSteps <= 0 Then
            'nice try, very funny
            mFadePositionSteps = 1
        End If


        Select Case Me.NotificationType
            Case NamedPipe.Notification.NotificationType.Information
                picIcon.Image = My.Resources.notification_icons__info
                picClose.Image = My.Resources.notification_icons__info__close
                pnlContainer.BackColor = Color.FromArgb(223, 239, 255)
            Case NamedPipe.Notification.NotificationType.Warning
                picIcon.Image = My.Resources.notification_icons__warning
                picClose.Image = My.Resources.notification_icons__warning__close
                pnlContainer.BackColor = Color.FromArgb(255, 251, 227)
            Case NamedPipe.Notification.NotificationType.Error
                picIcon.Image = My.Resources.notification_icons__error
                picClose.Image = My.Resources.notification_icons__error__close
                pnlContainer.BackColor = Color.FromArgb(255, 223, 223)
            Case Else
                Me.Close()
                Exit Sub
        End Select

        lblText.Text = Me.NotificationText

        progressAutoClose.Maximum = mAutoCloseCounter
        progressAutoClose.Value = mAutoCloseCounter

        tmrAutoclose.Enabled = False


        Select Case sPosition
            Case "top,left"
                mFadePositionXStart = -Me.Width
                mFadePositionXEnd = 0
                mFadePositionYStart = 0
                mFadePositionYEnd = 0

                mFadePositionStep = Math.Abs((mFadePositionXStart - mFadePositionXEnd) / mFadePositionSteps)
            Case "top,center"
                mFadePositionXStart = (SystemInformation.PrimaryMonitorSize.Width - Me.Width) / 2
                mFadePositionXEnd = (SystemInformation.PrimaryMonitorSize.Width - Me.Width) / 2
                mFadePositionYStart = -Me.Height
                mFadePositionYEnd = 0

                mFadePositionStep = Math.Abs((mFadePositionYStart - mFadePositionYEnd) / mFadePositionSteps)
            Case "top,right"
                mFadePositionXStart = SystemInformation.PrimaryMonitorSize.Width
                mFadePositionXEnd = SystemInformation.PrimaryMonitorSize.Width - Me.Width
                mFadePositionYStart = 0
                mFadePositionYEnd = 0

                mFadePositionStep = Math.Abs((mFadePositionXStart - mFadePositionXEnd) / mFadePositionSteps)
            Case "bottom,left"
                mFadePositionXStart = -Me.Width
                mFadePositionXEnd = 0
                mFadePositionYStart = SystemInformation.PrimaryMonitorSize.Height - Me.Height
                mFadePositionYEnd = SystemInformation.PrimaryMonitorSize.Height - Me.Height

                mFadePositionStep = Math.Abs((mFadePositionXStart - mFadePositionXEnd) / mFadePositionSteps)
            Case "bottom,center"
                mFadePositionXStart = (SystemInformation.PrimaryMonitorSize.Width - Me.Width) / 2
                mFadePositionXEnd = (SystemInformation.PrimaryMonitorSize.Width - Me.Width) / 2
                mFadePositionYStart = SystemInformation.PrimaryMonitorSize.Height
                mFadePositionYEnd = SystemInformation.PrimaryMonitorSize.Height - Me.Height

                mFadePositionStep = Math.Abs((mFadePositionYStart - mFadePositionYEnd) / mFadePositionSteps)
            Case "bottom,right"
                mFadePositionXStart = SystemInformation.PrimaryMonitorSize.Width
                mFadePositionXEnd = SystemInformation.PrimaryMonitorSize.Width - Me.Width
                mFadePositionYStart = SystemInformation.PrimaryMonitorSize.Height - Me.Height
                mFadePositionYEnd = SystemInformation.PrimaryMonitorSize.Height - Me.Height

                mFadePositionStep = Math.Abs((mFadePositionXStart - mFadePositionXEnd) / mFadePositionSteps)

            Case Else 'Default to top,right
                mFadePositionXStart = SystemInformation.PrimaryMonitorSize.Width
                mFadePositionXEnd = SystemInformation.PrimaryMonitorSize.Width - Me.Width
                mFadePositionYStart = 0
                mFadePositionYEnd = 0

                mFadePositionStep = Math.Abs((mFadePositionXStart - mFadePositionXEnd) / mFadePositionSteps)
        End Select

        mFadePositionX = mFadePositionXStart
        mFadePositionY = mFadePositionYStart
        Me.Top = mFadePositionY
        Me.Left = mFadePositionX

        Me.TopMost = True
        Me.TransparencyKey = Color.Lime
        Me.WindowState = FormWindowState.Normal
        Helpers.Forms.TopMost(Me.Handle, True)

        picClose.Enabled = False
        picClose.Visible = False

        tmrFadeIn.Enabled = True
    End Sub

    Private Sub picClose_Click(sender As Object, e As EventArgs) Handles picClose.Click
        DialogClose()
    End Sub

    Private Sub DialogClose()
        tmrFadeOut.Enabled = True
    End Sub


    Private Sub tmrAutoclose_Tick(sender As Object, e As EventArgs) Handles tmrAutoclose.Tick
        mAutoCloseCounter -= 1
        If mAutoCloseCounter >= 0 Then
            progressAutoClose.Value = mAutoCloseCounter
        Else
            tmrAutoclose.Enabled = False

            DialogClose()
        End If
    End Sub

    Private Sub tmrFadeIn_Tick(sender As Object, e As EventArgs) Handles tmrFadeIn.Tick
        Dim iAnimState As Integer = 0

        If mFadePositionXStart > mFadePositionXEnd Then
            mFadePositionX -= mFadePositionStep
            If mFadePositionX < mFadePositionXEnd Then
                mFadePositionX = mFadePositionXEnd
                iAnimState = iAnimState Or 1
            End If
        ElseIf mFadePositionXStart < mFadePositionXEnd Then
            mFadePositionX += mFadePositionStep
            If mFadePositionX > mFadePositionXEnd Then
                mFadePositionX = mFadePositionXEnd
                iAnimState = iAnimState Or 1
            End If
        Else
            iAnimState = iAnimState Or 1
        End If

        If mFadePositionYStart > mFadePositionYEnd Then
            mFadePositionY -= mFadePositionStep
            If mFadePositionY < mFadePositionYEnd Then
                mFadePositionY = mFadePositionYEnd
                iAnimState = iAnimState Or 2
            End If
        ElseIf mFadePositionyStart < mFadePositionyEnd Then
            mFadePositionY += mFadePositionStep
            If mFadePositionY > mFadePositionYEnd Then
                mFadePositionY = mFadePositionYEnd
                iAnimState = iAnimState Or 2
            End If
        Else
            iAnimState = iAnimState Or 2
        End If

        Me.Top = mFadePositionY
        Me.Left = mFadePositionX

        If iAnimState = 3 Then
            tmrFadeIn.Enabled = False
            tmrAutoclose.Enabled = True

            picClose.Enabled = True
            picClose.Visible = True
        End If
    End Sub

    Private Sub tmrFadeOut_Tick(sender As Object, e As EventArgs) Handles tmrFadeOut.Tick
        Dim iAnimState As Integer = 0

        If mFadePositionXStart > mFadePositionXEnd Then
            mFadePositionX += mFadePositionStep
            If mFadePositionX > mFadePositionXStart Then
                mFadePositionX = mFadePositionXStart
                iAnimState = iAnimState Or 1
            End If
        ElseIf mFadePositionXStart < mFadePositionXEnd Then
            mFadePositionX -= mFadePositionStep
            If mFadePositionX < mFadePositionXStart Then
                mFadePositionX = mFadePositionXStart
                iAnimState = iAnimState Or 1
            End If
        Else
            iAnimState = iAnimState Or 1
        End If

        If mFadePositionYStart > mFadePositionYEnd Then
            mFadePositionY += mFadePositionStep
            If mFadePositionY > mFadePositionYStart Then
                mFadePositionY = mFadePositionYStart
                iAnimState = iAnimState Or 2
            End If
        ElseIf mFadePositionYStart < mFadePositionYEnd Then
            mFadePositionY -= mFadePositionStep
            If mFadePositionY < mFadePositionYStart Then
                mFadePositionY = mFadePositionYStart
                iAnimState = iAnimState Or 2
            End If
        Else
            iAnimState = iAnimState Or 2
        End If

        Me.Top = mFadePositionY
        Me.Left = mFadePositionX

        If iAnimState = 3 Then
            tmrFadeOut.Enabled = False
            tmrAutoclose.Enabled = True

            Me.DialogResult = DialogResult.OK
            Me.Close()
        End If
    End Sub
End Class