﻿Imports System.ComponentModel

Public Class frmMain
    Private mThread_NotificationHandler As Threading.Thread
    Private mThreadActive As Boolean = True

    Delegate Sub MeCallback()

    Public Function _ME() As Form
        If Me.InvokeRequired Then
            Dim d As New MeCallback(AddressOf _ME)
            Return Me.Invoke(d)
        Else
            Return Me
        End If
    End Function


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        NamedPipe.Logger.Client.Message(New String("*", 50))
        NamedPipe.Logger.Client.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        NamedPipe.Logger.Client.Message("started")


        Me.FormBorderStyle = FormBorderStyle.None
        Me.BackColor = Color.Lime
        Me.TransparencyKey = Color.Lime
        Me.Top = SystemInformation.PrimaryMonitorSize.Height + Me.Height
        Me.Left = SystemInformation.PrimaryMonitorSize.Width + Me.Width


        'Start notification thread
        mThread_NotificationHandler = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__NotificationHandler))
        mThread_NotificationHandler.Start()
    End Sub

    Private Sub frmMain_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        mThreadActive = True
    End Sub

    Private Sub Thread__NotificationHandler()
        Dim _notification As NamedPipe.Notification.Notification = Nothing

        Dim bNotificationShown As Boolean = False
        Dim bLastNotificationDone As Boolean = False

        Do
            If NotificationClient.GetNextNotification(_notification) Then
                bNotificationShown = True
                NamedPipe.Logger.Client.Debug("#" & _notification.Index.ToString & " - " & _notification.Type.ToString & " - " & _notification.Text)

                If _notification IsNot Nothing Then
                    Dim _frm As frmNotification = New frmNotification

                    _frm.NotificationType = _notification.Type
                    _frm.NotificationText = _notification.Text

                    Dim _dialogResult As DialogResult = _frm.ShowDialog(_ME)
                    _frm.Dispose()
                End If
            Else
                If bNotificationShown Then bLastNotificationDone = True
            End If


            If bLastNotificationDone And bNotificationShown Then
                bLastNotificationDone = False
                bNotificationShown = False
                System.GC.Collect()
            End If


            Threading.Thread.Sleep(1000)
        Loop While mThreadActive
    End Sub
End Class
