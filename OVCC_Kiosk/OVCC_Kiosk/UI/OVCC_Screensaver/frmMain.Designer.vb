﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        webview2_Screensaver = New Microsoft.Web.WebView2.WinForms.WebView2()
        tmrActivity = New Timer(components)
        CType(webview2_Screensaver, ComponentModel.ISupportInitialize).BeginInit()
        SuspendLayout()
        ' 
        ' webview2_Screensaver
        ' 
        webview2_Screensaver.AllowExternalDrop = False
        webview2_Screensaver.CreationProperties = Nothing
        webview2_Screensaver.DefaultBackgroundColor = Color.White
        webview2_Screensaver.Location = New Point(12, 12)
        webview2_Screensaver.Name = "webview2_Screensaver"
        webview2_Screensaver.Size = New Size(310, 92)
        webview2_Screensaver.TabIndex = 0
        webview2_Screensaver.ZoomFactor = 1R
        ' 
        ' tmrActivity
        ' 
        tmrActivity.Enabled = True
        tmrActivity.Interval = 1000
        ' 
        ' frmMain
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(800, 450)
        Controls.Add(webview2_Screensaver)
        FormBorderStyle = FormBorderStyle.None
        Icon = CType(resources.GetObject("$this.Icon"), Icon)
        Name = "frmMain"
        StartPosition = FormStartPosition.CenterScreen
        Text = "screensaver"
        CType(webview2_Screensaver, ComponentModel.ISupportInitialize).EndInit()
        ResumeLayout(False)
    End Sub

    Friend WithEvents webview2_Screensaver As Microsoft.Web.WebView2.WinForms.WebView2
    Friend WithEvents tmrActivity As Timer

End Class
