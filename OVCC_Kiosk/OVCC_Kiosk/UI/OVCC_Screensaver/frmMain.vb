﻿Imports Microsoft.Web.WebView2.Core

Public Class frmMain
    Private WithEvents corewebview2_Screensaver As CoreWebView2


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        NamedPipe.System.Client.StartedScreensaver()

        Initialise()
    End Sub

    Private Sub CloseScreensaver()
        NamedPipe.Session.Client.ScreensaverStopped()

        Me.Close()
    End Sub

    Private Sub Initialise()
        LoadSettings()


        'first logging
        NamedPipe.Logger.Client.Message(New String("*", 50))
        NamedPipe.Logger.Client.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        NamedPipe.Logger.Client.Message("started")
        If Globals.Application.Debug Then
            NamedPipe.Logger.Client.Message("debug mode on")
        End If


        'Set fullscreen etc.
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized
        Me.Location = New Point(0, 0)
        Me.BackColor = Color.White
        Me.Size = SystemInformation.PrimaryMonitorSize
        Helpers.Forms.TopMost(Me.Handle, True)


        Cursor.Hide()


        'webbrowser
        InitialiseMainWebView()
    End Sub

    Private Async Sub InitialiseMainWebView()
        webview2_Screensaver.Dock = DockStyle.Fill

        Dim cwEnvironment As CoreWebView2Environment = Nothing
        Dim sUserDataFolder As String = IO.Path.Combine(Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER, My.Application.Info.ProductName)

        NamedPipe.Logger.Client.Debug("Webview2 user data: " & sUserDataFolder)

        cwEnvironment = Await CoreWebView2Environment.CreateAsync(Nothing, sUserDataFolder, Nothing)

        Await webview2_Screensaver.EnsureCoreWebView2Async(cwEnvironment)


        corewebview2_Screensaver = webview2_Screensaver.CoreWebView2

        'webview2_Screensaver.CoreWebView2.AddHostObjectToScript("OVCC_Kiosk", New WebBrowserScriptInterface())
        webview2_Screensaver.CoreWebView2.Settings.AreDevToolsEnabled = False
        webview2_Screensaver.CoreWebView2.Settings.AreDefaultContextMenusEnabled = False


        Dim sThemeBaseFolder As String = Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Themes, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Themes__BaseFolder, "")
        Dim sThemeName As String = Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Themes, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Themes__ActiveTheme, "")
        Dim sScreensaver As String = "file:///" & IO.Path.Combine(sThemeBaseFolder, sThemeName, "screensaver", "index.html")

        webview2_Screensaver.CoreWebView2.Navigate(sScreensaver)
    End Sub

    Private Sub LoadSettings()
        Globals.Application.Debug = Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__Debug, True)

        Globals.Application.AdminPassword = Helpers.XOrObfuscation_v2.Deobfuscate(
                                                Settings.Get.String(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__AdminPassword,
                                                    "0758405357522b2e72627754664b18465c520b0711575b500b520c16135e0d0f0a570709160453507c78716a7f533c1a19175b530d5a46525c0708550314165a0d590c0557584a0607547a79236324503e4e10405f515855405b09040c040016120f0f5c5e52565b105255527b7b"))


        Globals.Application.GuestPasswordEnabled = Settings.Get.Boolean(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__GuestPasswordEnabled,
                                                    False)

        Globals.Application.ScreensaverTimeout = Settings.Get.Long(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__ScreensaverTimeout,
                                                    300)

    End Sub

    Private Sub tmrActivity_Tick(sender As Object, e As EventArgs) Handles tmrActivity.Tick
        Dim bActive As Boolean = Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsScreensaverActive, False)

        If Not bActive Then
            CloseScreensaver()
        End If
    End Sub

    Private Sub corewebview2_Screensaver_DOMContentLoaded(sender As Object, e As CoreWebView2DOMContentLoadedEventArgs) Handles corewebview2_Screensaver.DOMContentLoaded
        NamedPipe.Session.Client.ScreensaverRunning()
    End Sub

    Private Sub frmMain_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        NamedPipe.Logger.Client.Message("stopped")

        NamedPipe.System.Client.ClosedScreensaver()
    End Sub
End Class
