﻿Imports System.ComponentModel

Public Class frmMain
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Me.BackColor


        'first logging
        NamedPipe.Logger.Client.Message(New String("*", 50))
        NamedPipe.Logger.Client.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        NamedPipe.Logger.Client.Message("started")


        NamedPipe.System.Client.StartedLogout()


        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub frmMain_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        Me.WindowState = FormWindowState.Minimized

        If SessionStuff.IsSessionActive Then
            NamedPipe.Logger.Client.Message("Triggering logout warning in Homepage")

            If Helpers.Processes.IsProcessRunning(Settings.Constants.Paths.Files.Apps.Homepage.Name) Then
                NamedPipe.Session.Client.SessionLogoutRequested()
            Else
                NamedPipe.Logger.Client.Error("skipped, Homepage is not running")
            End If
        End If
    End Sub

    Private Sub frmMain_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        NamedPipe.System.Client.ClosedLogout()
    End Sub
End Class
