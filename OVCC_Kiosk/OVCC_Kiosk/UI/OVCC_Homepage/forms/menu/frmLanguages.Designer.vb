﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLanguages
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlLanguagesContainer = New Panel()
        pnlLanguages = New Panel()
        tmrAutoClose = New Timer(components)
        picLogoutVoid = New PictureBox()
        lblSizeCalculate = New Label()
        pnlLanguagesContainer.SuspendLayout()
        CType(picLogoutVoid, ComponentModel.ISupportInitialize).BeginInit()
        SuspendLayout()
        ' 
        ' pnlLanguagesContainer
        ' 
        pnlLanguagesContainer.BackColor = Color.Transparent
        pnlLanguagesContainer.Controls.Add(pnlLanguages)
        pnlLanguagesContainer.Location = New Point(28, 155)
        pnlLanguagesContainer.Name = "pnlLanguagesContainer"
        pnlLanguagesContainer.Size = New Size(439, 474)
        pnlLanguagesContainer.TabIndex = 0
        ' 
        ' pnlLanguages
        ' 
        pnlLanguages.BackColor = Color.Transparent
        pnlLanguages.Dock = DockStyle.Top
        pnlLanguages.Location = New Point(0, 0)
        pnlLanguages.Name = "pnlLanguages"
        pnlLanguages.Size = New Size(439, 50)
        pnlLanguages.TabIndex = 10
        ' 
        ' tmrAutoClose
        ' 
        tmrAutoClose.Interval = 1000
        ' 
        ' picLogoutVoid
        ' 
        picLogoutVoid.BackColor = Color.Transparent
        picLogoutVoid.Location = New Point(403, 16)
        picLogoutVoid.Name = "picLogoutVoid"
        picLogoutVoid.Size = New Size(64, 64)
        picLogoutVoid.SizeMode = PictureBoxSizeMode.StretchImage
        picLogoutVoid.TabIndex = 3
        picLogoutVoid.TabStop = False
        ' 
        ' lblSizeCalculate
        ' 
        lblSizeCalculate.AutoSize = True
        lblSizeCalculate.Location = New Point(323, 16)
        lblSizeCalculate.Name = "lblSizeCalculate"
        lblSizeCalculate.Size = New Size(74, 13)
        lblSizeCalculate.TabIndex = 7
        lblSizeCalculate.Text = "size calculator"
        lblSizeCalculate.Visible = False
        ' 
        ' frmLanguages
        ' 
        AutoScaleDimensions = New SizeF(6F, 13F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Gray
        ClientSize = New Size(500, 650)
        ControlBox = False
        Controls.Add(lblSizeCalculate)
        Controls.Add(picLogoutVoid)
        Controls.Add(pnlLanguagesContainer)
        Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        Name = "frmLanguages"
        ShowIcon = False
        ShowInTaskbar = False
        Text = "frmMenu"
        pnlLanguagesContainer.ResumeLayout(False)
        CType(picLogoutVoid, ComponentModel.ISupportInitialize).EndInit()
        ResumeLayout(False)
        PerformLayout()

    End Sub

    Friend WithEvents pnlLanguagesContainer As Panel
    Friend WithEvents tmrAutoClose As Timer
    Friend WithEvents pnlLanguages As Panel
    Friend WithEvents picLogoutVoid As PictureBox
    Friend WithEvents lblSizeCalculate As Label
End Class
