﻿
Public Class frmLanguages
    Private mAutoCloseCounterMax As Integer = 5
    Private mAutoCloseCounter As Integer = mAutoCloseCounterMax

    Private mSelectedLanguage As Integer = -1

    Private ReadOnly cPicBoxName As String = "picBox_Language_"
    Private ReadOnly cLabelName As String = "lbl_Language_"
    Private ReadOnly cPanelName As String = "pnl_Language_"

    Private mThread_Highlight As Threading.Thread

    Private ReadOnly TRANSPARENCY_COLOR As Color = Color.Gray


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


#Region "Thread Safe stuff"
    Delegate Function DELEGATE__PointToClient(_p As Point) As Point
    Delegate Sub DELEGATE__CloseMenu()

    Public Function THREADSAFE__PointToClient(_p As Point) As Point
        Try
            If Me.InvokeRequired Then
                Dim d As New DELEGATE__PointToClient(AddressOf THREADSAFE__PointToClient)
                Return Me.Invoke(d, New Object() {_p})
            Else
                Return Me.PointToClient(_p)
            End If
        Catch ex As Exception
            Return New Point(0, 0)
        End Try
    End Function

    Public Sub THREADSAFE__CloseMenu()
        If Me.InvokeRequired Then
            Dim d As New DELEGATE__CloseMenu(AddressOf THREADSAFE__CloseMenu)
            Me.Invoke(d, New Object() {})
        Else
            Me.Close()
        End If
    End Sub
#End Region

    Private Sub frmMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = TRANSPARENCY_COLOR
        Me.Visible = False
        Helpers.Forms.TopMost(Me.Handle, True)


        Me.Top = 0


        If Globals.UI.LOGOUT_BUTTON_VISIBLE Then
            picLogoutVoid.Top = Globals.UI.BUTTON_MARGIN
        Else
            picLogoutVoid.Visible = False
            picLogoutVoid.Enabled = False
        End If
        picLogoutVoid.Left = Me.Width - picLogoutVoid.Width - Globals.UI.BUTTON_MARGIN


        mSelectedLanguage = Localisation.GetActiveLanguageId()

        Dim iMaxLabelSize As Integer = 0
        For i As Integer = 0 To Localisation.LanguagesCount - 1
            Dim _language As Localisation.Language = Localisation.Languages.Item(i)

            lblSizeCalculate.Text = (New String(" ", 12)) & _language.Name
            If lblSizeCalculate.Width > iMaxLabelSize Then
                iMaxLabelSize = lblSizeCalculate.Width
            End If
        Next

        Me.Width = iMaxLabelSize + Globals.UI.BUTTON_MARGIN
        Me.Left = frmMain.Width - Me.Width
        pnlLanguagesContainer.Width = iMaxLabelSize


        Dim _flagPanelTopPosition As Integer = Settings.Constants.UI.LANGUAGE__FLAG_TOP_MARGIN


        For i As Integer = 0 To Localisation.LanguagesCount - 1
            Dim _language As Localisation.Language = Localisation.Languages.Item(i)
            Dim _flag As Image

            If IO.File.Exists(_language.IconAbsolute) Then
                NamedPipe.Logger.Client.Debug("Loading language flag for language id '" & Localisation.GetActiveLanguageId().ToString & "': " & _language.IconAbsolute)
                _flag = Image.FromFile(_language.IconAbsolute)
            Else
                _flag = My.Resources.flag_placeholder
                NamedPipe.Logger.Client.Error("Could not find language flag for language id '" & Localisation.GetActiveLanguageId().ToString & "': " & _language.IconAbsolute)
            End If


            Dim _picbox As New PictureBox With {
                .BorderStyle = BorderStyle.None,
                .SizeMode = PictureBoxSizeMode.AutoSize,
                .Name = cPicBoxName & _language.Id.ToString,
                .Top = Settings.Constants.UI.LANGUAGE__FLAG_MARGIN,
                .Image = _flag,
                .Cursor = Cursors.Hand
            }
            _picbox.Left = pnlLanguages.Width - _picbox.Width


            AddHandler _picbox.MouseClick, AddressOf picBox_Language__Click

            _picbox.Refresh()


            Dim _label As New Label With {
                .AutoSize = False,
                .BackColor = Color.Transparent,
                .Text = _language.Name,
                .Name = cLabelName & _language.Id.ToString,
                .TextAlign = ContentAlignment.TopRight,
                .BorderStyle = BorderStyle.None,
                .ForeColor = Color.White,
                .Top = Settings.Constants.UI.LANGUAGE__FLAG_MARGIN + _picbox.Height,
                .Left = pnlLanguages.Width - iMaxLabelSize + Settings.Constants.UI.LANGUAGE__FLAG_MARGIN,
                .Width = iMaxLabelSize,
                .Cursor = Cursors.Hand
            }
            AddHandler _label.MouseClick, AddressOf lbl_Language__Click


            Dim _panel As New Panel With {
                .BorderStyle = BorderStyle.None,
                .BackColor = Color.Transparent,
                .Name = cPanelName & _language.Id.ToString,
                .Top = _flagPanelTopPosition,
                .Left = 0,
                .Width = pnlLanguages.Width,
                .Height = _picbox.Height + Settings.Constants.UI.LANGUAGE__LABEL_HEIGHT + Settings.Constants.UI.LANGUAGE__FLAG_MARGIN
            }


            _panel.Controls.Add(_picbox)
            _panel.Controls.Add(_label)


            _flagPanelTopPosition += (_picbox.Height + Settings.Constants.UI.LANGUAGE__LABEL_HEIGHT + Settings.Constants.UI.LANGUAGE__FLAG_MARGIN)


            pnlLanguages.Width = iMaxLabelSize
            pnlLanguages.Height = _panel.Height * (i + 1) + Settings.Constants.UI.LANGUAGE__FLAG_MARGIN + Settings.Constants.UI.LANGUAGE__FLAG_TOP_MARGIN
            pnlLanguages.Controls.Add(_panel)
        Next


        If Globals.UI.LOGOUT_BUTTON_VISIBLE Then
            pnlLanguagesContainer.Top = picLogoutVoid.Top + picLogoutVoid.Height + Globals.UI.BUTTON_MARGIN
        Else
            pnlLanguagesContainer.Top = Globals.UI.BUTTON_MARGIN
        End If
        pnlLanguagesContainer.Left = 0
        pnlLanguagesContainer.Height = pnlLanguages.Height


        Me.Height = pnlLanguagesContainer.Top + pnlLanguagesContainer.Height + Globals.UI.BUTTON_MARGIN
        Me.Refresh()
        Me.Visible = True


        tmrAutoClose.Enabled = True


        AddHandler frmMain.MouseClickDetected, AddressOf MouseClickDetected
        AddHandler frmMain.MouseMoveDetected, AddressOf MouseMoveDetected
    End Sub

    Private Sub MouseClickDetected(X As Short, y As Short, Button As UShort)
        Dim _point As Point = THREADSAFE__PointToClient(New Point(X, y))

        If _point.X < 0 Or _point.Y < 0 Then
            CloseMenu()
        End If
    End Sub

    Private Sub MouseMoveDetected(X As Short, y As Short)
        Dim _point As Point = THREADSAFE__PointToClient(New Point(X, y))

        If _point.X >= 0 And _point.Y >= 0 Then
            mAutoCloseCounter = mAutoCloseCounterMax
        End If
    End Sub

    Private Sub CloseMenu()
        NamedPipe.Logger.Client.Message("Setting language to #" & mSelectedLanguage.ToString)
        NamedPipe.Language.Client.SetActiveLanguage(mSelectedLanguage)

        THREADSAFE__CloseMenu()
    End Sub


#Region "Events"
#Region "dynamic picbox & label"
    Private Sub picBox_Language__Click(sender As Object, e As EventArgs)
        Dim _picturebox As PictureBox = CType(sender, PictureBox)
        Dim _pictureboxname = _picturebox.Name
        Dim _index As Integer = -1

        If Integer.TryParse(_pictureboxname.Replace(cPicBoxName, ""), _index) Then
            mSelectedLanguage = _index

            CloseMenu()
        End If
    End Sub

    Private Sub lbl_Language__Click(sender As Object, e As EventArgs)
        Dim _label As Label = CType(sender, Label)
        Dim _labelname = _label.Name
        Dim _index As Integer = -1

        If Integer.TryParse(_labelname.Replace(cLabelName, ""), _index) Then
            mSelectedLanguage = _index

            CloseMenu()
        End If
    End Sub

    Private Sub pnl_Language__Click(sender As Object, e As EventArgs)
        Dim _panel As Panel = CType(sender, Panel)
        Dim _panelname = _panel.Name
        Dim _index As Integer = -1

        If Integer.TryParse(_panelname.Replace(cPanelName, ""), _index) Then
            mSelectedLanguage = _index

            CloseMenu()
        End If
    End Sub
#End Region

#Region "timers"
    Private Sub tmrAutoClose_Tick(sender As Object, e As EventArgs) Handles tmrAutoClose.Tick
        mAutoCloseCounter -= 1
        If mAutoCloseCounter < 0 Then
            tmrAutoClose.Enabled = False

            CloseMenu()
        End If
    End Sub
#End Region
#End Region
End Class