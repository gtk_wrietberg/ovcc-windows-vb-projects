﻿Public Class frmAirlines
    Private ReadOnly cPicBoxName As String = "picBox_Airline_"
    Private ReadOnly cLabelName As String = "lbl_Airline_"
    Private ReadOnly cPanelName As String = "pnl_Airline_"

    Private ReadOnly cColCountMax As Integer = 5
    Private ReadOnly cRowCountMax As Integer = 7
    Private ReadOnly cExtraMarginFactor As Integer = 5

    Private ReadOnly cLetterSeparator As String = " - "

    Private ReadOnly TRANSPARENCY_COLOR As Color = Color.Gray

    Private ReadOnly AIRLINE_BACKGROUND_NORMAL As Color = Color.Transparent
    Private ReadOnly AIRLINE_BACKGROUND_HIGHLIGHT As Color = Color.Gainsboro

    Private iPicsLoading As Integer = 0

    Private mLetters As New List(Of String)

    Private ReadOnly mAutoCloseCounterInterval As Integer = 25
    Private ReadOnly mAutoCloseCounterMAX As Integer = 10 * (1000 / mAutoCloseCounterInterval)
    Private mAutoCloseCounter As Integer = mAutoCloseCounterMAX


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmAirlines_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = TRANSPARENCY_COLOR
        Me.Visible = False
        Helpers.Forms.TopMost(Me.Handle, True)


        NamedPipe.UI.Client.AirlinesDialog()


        '--------------------------------------------------------------------------------------
        Dim bAirlineAvailable As Boolean = False
        Dim iColCounter As Integer = 0
        Dim iRowCounter As Integer = 0

        Airlines.LoadAirlines()
        Airlines.SortByName()


        iPicsLoading = Airlines.AirlinesCount

        For _index As Integer = 0 To Airlines.AirlinesCount - 1
            Dim _panel As New Panel With {
               .BorderStyle = BorderStyle.None,
               .BackColor = AIRLINE_BACKGROUND_NORMAL,
               .Name = cPanelName & _index.ToString,
               .Width = Settings.Constants.UI.AIRLINE__ICON_WIDTH + 2 * Settings.Constants.UI.AIRLINE__ICON_MARGIN,
               .Height = Settings.Constants.UI.AIRLINE__ICON_HEIGHT + Settings.Constants.UI.AIRLINE__LABEL_HEIGHT + 2 * Settings.Constants.UI.AIRLINE__ICON_MARGIN,
               .Top = iRowCounter * (Settings.Constants.UI.AIRLINE__ICON_HEIGHT + Settings.Constants.UI.AIRLINE__LABEL_HEIGHT + 2 * Settings.Constants.UI.AIRLINE__ICON_MARGIN),
               .Left = iColCounter * (Settings.Constants.UI.AIRLINE__ICON_WIDTH + 2 * Settings.Constants.UI.AIRLINE__ICON_MARGIN)
            }


            Dim _picbox As New PictureBox With {
               .BorderStyle = BorderStyle.FixedSingle,
               .SizeMode = PictureBoxSizeMode.StretchImage,
               .Name = cPicBoxName & _index.ToString,
               .Width = Settings.Constants.UI.AIRLINE__ICON_WIDTH,
               .Height = Settings.Constants.UI.AIRLINE__ICON_HEIGHT,
               .Top = Settings.Constants.UI.AIRLINE__ICON_MARGIN,
               .Left = Settings.Constants.UI.AIRLINE__ICON_MARGIN,
               .Cursor = Cursors.Hand
            }
            If IO.File.Exists(Airlines.AirlinesList.Item(_index).IconAbsolute) Then
                _picbox.ImageLocation = Airlines.AirlinesList.Item(_index).IconAbsolute
            Else
                _picbox.Image = My.Resources.airline_placeholder
                NamedPipe.Logger.Client.Error("Could not find airline icon: " & Airlines.AirlinesList.Item(_index).IconAbsolute)
            End If
            AddHandler _picbox.MouseClick, AddressOf picBox_Airline__Click

            _picbox.Refresh()


            Dim _label As New Label With {
                .AutoSize = False,
                .Text = Airlines.AirlinesList.Item(_index).Name,
                .Name = cLabelName & _index.ToString,
                .TextAlign = ContentAlignment.MiddleCenter,
                .BorderStyle = BorderStyle.None,
                .ForeColor = Color.Black,
                .Top = _picbox.Top + _picbox.Height,
                .Left = Settings.Constants.UI.AIRLINE__ICON_MARGIN,
                .Width = Settings.Constants.UI.AIRLINE__ICON_WIDTH,
                .Height = Settings.Constants.UI.AIRLINE__LABEL_HEIGHT,
                .Cursor = Cursors.Hand
            }
            AddHandler _label.MouseClick, AddressOf lbl_Airline__Click


            Airlines.AirlinesList.Item(_index).IconTop = _panel.Top
            Airlines.AirlinesList.Item(_index).IconLeft = _panel.Left


            _panel.Controls.Add(_picbox)
            _panel.Controls.Add(_label)
            pnlAirlines.Controls.Add(_panel)

            '-------------------------------------
            Dim sFirstLetter As String = Airlines.AirlinesList.Item(_index).Name.Substring(0, 1).ToUpper()
            If Not mLetters.Contains(sFirstLetter) Then
                mLetters.Add(sFirstLetter)
            End If


            iColCounter += 1
            If iColCounter >= cColCountMax Then
                iColCounter = 0
                iRowCounter += 1
            End If
        Next


        linklblLetters.Links.Clear()
        linklblLetters.Text = String.Join(cLetterSeparator, mLetters.ToArray)

        For i As Integer = 0 To mLetters.Count - 1
            linklblLetters.Links.Add((cLetterSeparator.Length + 1) * i, 1)
        Next


        pnlAirlines.Left = 0
        pnlAirlines.Top = 0
        pnlAirlines.Width = cColCountMax * (Settings.Constants.UI.AIRLINE__ICON_WIDTH + 2 * Settings.Constants.UI.AIRLINE__ICON_MARGIN) + SystemInformation.VerticalScrollBarWidth
        pnlAirlines.Height = cRowCountMax * (Settings.Constants.UI.AIRLINE__ICON_HEIGHT + Settings.Constants.UI.AIRLINE__LABEL_HEIGHT + 2 * Settings.Constants.UI.AIRLINE__ICON_MARGIN)
        pnlAirlines.AutoScroll = True

        pnlAirlinesContent.Top = pnlAlphabetLinks.Height
        pnlAirlinesContent.Left = 0
        pnlAirlinesContent.Width = pnlAirlines.Width
        pnlAirlinesContent.Height = pnlAirlines.Height ' + Settings.Constants.UI.AIRLINE__ICON_MARGIN


        Me.ClientSize = New Size(pnlAirlinesContent.Width, pnlAirlinesContent.Top + pnlAirlinesContent.Height)

        Me.Left = (frmMain.Width - Me.Width) / 2
        Me.Top = (frmMain.Height - Me.Height) / 2

        Me.Visible = True


        progressPanelAutoclose.BackgroundColor = Color.Transparent
        progressPanelAutoclose.Border = True
        progressPanelAutoclose.ProgressBarColor = Color.Green
        progressPanelAutoclose.ProgressBarBorder = True
        progressPanelAutoclose.AnimationSpeed = 10
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.ValueMin = 0
        progressPanelAutoclose.ValueMax = mAutoCloseCounterMAX
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.Initialise()


        tmrAutoclose.Interval = mAutoCloseCounterInterval
        tmrAutoclose.Enabled = True
    End Sub

    Private Sub FindAirlineAndGo(Index As Integer)
        If Index >= 0 And Index < Airlines.AirlinesCount Then
            NamedPipe.Logger.Client.Message("Loading airline url:" & Airlines.AirlinesList.Item(Index).Url)
            NamedPipe.UI.Client.AirlineSelected(Airlines.AirlinesList.Item(Index).Url)

            SessionStuff.SessionAction.Add_Url(Airlines.AirlinesList.Item(Index).Url)
        Else
            NamedPipe.Logger.Client.Message("Could not find airline url for id " & Index.ToString)
        End If


        CloseAirlines()
    End Sub

    Private Sub CloseAirlines()
        Close()
    End Sub


#Region "Events"
#Region "dynamic picbox & label"
    Private Sub picBox_Airline__Click(sender As Object, e As EventArgs)
        Dim _picturebox As PictureBox = CType(sender, PictureBox)
        Dim _pictureboxname = _picturebox.Name
        Dim _index As Integer = -1

        If Integer.TryParse(_pictureboxname.Replace(cPicBoxName, ""), _index) Then
            FindAirlineAndGo(_index)
        End If
    End Sub

    Private Sub lbl_Airline__Click(sender As Object, e As EventArgs)
        Dim _label As Label = CType(sender, Label)
        Dim _labelname = _label.Name
        Dim _index As Integer = -1

        If Integer.TryParse(_labelname.Replace(cLabelName, ""), _index) Then
            FindAirlineAndGo(_index)
        End If
    End Sub

    Private Sub picClose_Click(sender As Object, e As EventArgs)
        CloseAirlines
    End Sub

    Private Sub linklblLetters_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles linklblLetters.LinkClicked
        Dim sLetter As String = linklblLetters.Text.Substring(e.Link.Start, 1)
        Dim bLetterFound As Boolean = False, bScrollHappened As Boolean = False

        RemoveHighlights()

        For _index As Integer = 0 To Airlines.AirlinesCount - 1
            If Airlines.AirlinesList.Item(_index).Name.ToUpper.StartsWith(sLetter.ToUpper) Then
                bLetterFound = True

                If Not bScrollHappened Then
                    Dim _icon_top As Integer = Airlines.AirlinesList.Item(_index).IconTop
                    Dim _icon_left As Integer = Airlines.AirlinesList.Item(_index).IconTop

                    If _icon_top < pnlAirlines.VerticalScroll.Minimum Then
                        _icon_top = pnlAirlines.VerticalScroll.Minimum
                    End If
                    If _icon_top > pnlAirlines.VerticalScroll.Maximum Then
                        _icon_top = pnlAirlines.VerticalScroll.Maximum
                    End If

                    pnlAirlines.VerticalScroll.Value = _icon_top
                    pnlAirlines.PerformLayout()

                    bScrollHappened = True
                End If

                'highlight
                Dim _panel As Panel = CType(pnlAirlines.Controls.Item(cPanelName & _index.ToString), Panel)
                If _panel IsNot Nothing Then
                    _panel.BackColor = AIRLINE_BACKGROUND_HIGHLIGHT
                End If
            Else
                If bLetterFound Then
                    Exit For
                End If
            End If
        Next

        tmrAirlinesHighlight.Stop()
        tmrAirlinesHighlight.Start()
    End Sub

    Private Sub RemoveHighlights()
        For Each _control As Control In pnlAirlines.Controls
            If TypeOf _control Is Panel Then
                If _control.Name.StartsWith(cPanelName) Then
                    _control.BackColor = AIRLINE_BACKGROUND_NORMAL
                End If
            End If
        Next
    End Sub

    Private Sub tmrAirlinesHighlight_Tick(sender As Object, e As EventArgs) Handles tmrAirlinesHighlight.Tick
        tmrAirlinesHighlight.Stop()
        RemoveHighlights()
    End Sub

    Private Sub tmrAutoclose_Tick(sender As Object, e As EventArgs) Handles tmrAutoclose.Tick
        If InputCheck.HasActivity Then
            mAutoCloseCounter = mAutoCloseCounterMAX
        End If

        redrawFakeProgressBar()
        mAutoCloseCounter -= 1
        If mAutoCloseCounter < 0 Then
            NamedPipe.UI.Client.AirlinesTimeout()
            CloseAirlines()
        End If
    End Sub

    Private Sub redrawFakeProgressBar()
        If mAutoCloseCounter < 0 Then Exit Sub

        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.ProgressBarColor = Helpers.UI.ProgressColor(mAutoCloseCounter, mAutoCloseCounterMAX)
    End Sub

    Private Sub frmAirlines_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        NamedPipe.UI.Client.AirlinesClosed()
    End Sub
#End Region
#End Region
End Class