﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAirlines
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlAirlinesContent = New Panel()
        pnlAirlines = New Panel()
        pnlAlphabetLinks = New Panel()
        linklblLetters = New LinkLabel()
        tmrAirlinesHighlight = New Timer(components)
        tmrAutoclose = New Timer(components)
        progressPanelAutoclose = New ProgressPanel()
        pnlAirlinesContent.SuspendLayout()
        pnlAlphabetLinks.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlAirlinesContent
        ' 
        pnlAirlinesContent.BackColor = Color.Transparent
        pnlAirlinesContent.Controls.Add(pnlAirlines)
        pnlAirlinesContent.Location = New Point(55, 178)
        pnlAirlinesContent.Name = "pnlAirlinesContent"
        pnlAirlinesContent.Size = New Size(693, 313)
        pnlAirlinesContent.TabIndex = 0
        ' 
        ' pnlAirlines
        ' 
        pnlAirlines.BackColor = Color.Transparent
        pnlAirlines.Location = New Point(4, 36)
        pnlAirlines.Name = "pnlAirlines"
        pnlAirlines.Size = New Size(647, 267)
        pnlAirlines.TabIndex = 4
        ' 
        ' pnlAlphabetLinks
        ' 
        pnlAlphabetLinks.Controls.Add(linklblLetters)
        pnlAlphabetLinks.Dock = DockStyle.Top
        pnlAlphabetLinks.Location = New Point(0, 0)
        pnlAlphabetLinks.Name = "pnlAlphabetLinks"
        pnlAlphabetLinks.Size = New Size(880, 18)
        pnlAlphabetLinks.TabIndex = 2
        ' 
        ' linklblLetters
        ' 
        linklblLetters.ActiveLinkColor = Color.Navy
        linklblLetters.Dock = DockStyle.Fill
        linklblLetters.Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        linklblLetters.LinkColor = Color.Black
        linklblLetters.Location = New Point(0, 0)
        linklblLetters.Name = "linklblLetters"
        linklblLetters.Size = New Size(880, 18)
        linklblLetters.TabIndex = 0
        linklblLetters.TabStop = True
        linklblLetters.Text = "LinkLabel1"
        linklblLetters.TextAlign = ContentAlignment.MiddleCenter
        linklblLetters.VisitedLinkColor = Color.Black
        ' 
        ' tmrAirlinesHighlight
        ' 
        tmrAirlinesHighlight.Interval = 3000
        ' 
        ' tmrAutoclose
        ' 
        tmrAutoclose.Interval = 50
        ' 
        ' progressPanelAutoclose
        ' 
        progressPanelAutoclose.AnimationSpeed = 50R
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.BackColor = Color.Transparent
        progressPanelAutoclose.BackgroundColor = Color.Empty
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.Dock = DockStyle.Bottom
        progressPanelAutoclose.Location = New Point(0, 573)
        progressPanelAutoclose.Name = "progressPanelAutoclose"
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.ProgressBarColor = Color.Empty
        progressPanelAutoclose.Size = New Size(880, 4)
        progressPanelAutoclose.TabIndex = 9
        progressPanelAutoclose.Value = 0R
        progressPanelAutoclose.ValueMax = 100R
        progressPanelAutoclose.ValueMin = 0R
        ' 
        ' frmAirlines
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.WhiteSmoke
        ClientSize = New Size(880, 577)
        Controls.Add(progressPanelAutoclose)
        Controls.Add(pnlAlphabetLinks)
        Controls.Add(pnlAirlinesContent)
        Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.FixedDialog
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmAirlines"
        ShowIcon = False
        ShowInTaskbar = False
        pnlAirlinesContent.ResumeLayout(False)
        pnlAlphabetLinks.ResumeLayout(False)
        ResumeLayout(False)
    End Sub
    Friend WithEvents pnlAirlinesContent As Panel
    Friend WithEvents pnlAirlines As Panel
    Friend WithEvents pnlAlphabetLinks As Panel
    Friend WithEvents linklblLetters As LinkLabel
    Friend WithEvents tmrAirlinesHighlight As Timer
    Friend WithEvents tmrAutoclose As Timer
    Friend WithEvents progressPanelAutoclose As ProgressPanel
End Class
