﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        btnEmergencyClose = New Button()
        txtDebug = New TextBox()
        btnTestTerms = New Button()
        tmrLoadingOverlayFadeout = New Timer(components)
        tmrSessionState = New Timer(components)
        tmrDesktopCheck = New Timer(components)
        btnForceUnlock = New Button()
        picActiveLanguageFlag = New PictureBox()
        pnlInformation = New Panel()
        linklabelInformation_Helpdesk = New LinkLabel()
        lblInformation_SystemInfo = New Label()
        btnHelpdeskTest = New Button()
        tmrLoadingOverlayDelay = New Timer(components)
        lblDebug_Idle = New Label()
        lblDebug_SessionAge = New Label()
        webview2_Main = New Microsoft.Web.WebView2.WinForms.WebView2()
        lblDebug_Foreground = New Label()
        picLogout = New PictureBox()
        tmrSecret = New Timer(components)
        pnlSecret_Left = New Panel()
        pnlSecret_Right = New Panel()
        pnlDebugMode = New Panel()
        lblDebugMode = New Label()
        lblDebug_MousePosition = New Label()
        pnlActiveLanguageFlagShadow = New Panel()
        CType(picActiveLanguageFlag, ComponentModel.ISupportInitialize).BeginInit()
        pnlInformation.SuspendLayout()
        CType(webview2_Main, ComponentModel.ISupportInitialize).BeginInit()
        CType(picLogout, ComponentModel.ISupportInitialize).BeginInit()
        pnlDebugMode.SuspendLayout()
        SuspendLayout()
        ' 
        ' btnEmergencyClose
        ' 
        btnEmergencyClose.Location = New Point(1235, 217)
        btnEmergencyClose.Name = "btnEmergencyClose"
        btnEmergencyClose.Size = New Size(159, 54)
        btnEmergencyClose.TabIndex = 2
        btnEmergencyClose.Text = "emergency close button"
        btnEmergencyClose.UseVisualStyleBackColor = True
        ' 
        ' txtDebug
        ' 
        txtDebug.Font = New Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        txtDebug.Location = New Point(1034, 354)
        txtDebug.Multiline = True
        txtDebug.Name = "txtDebug"
        txtDebug.ScrollBars = ScrollBars.Vertical
        txtDebug.Size = New Size(257, 244)
        txtDebug.TabIndex = 4
        ' 
        ' btnTestTerms
        ' 
        btnTestTerms.Location = New Point(1246, 277)
        btnTestTerms.Name = "btnTestTerms"
        btnTestTerms.Size = New Size(148, 54)
        btnTestTerms.TabIndex = 6
        btnTestTerms.Text = "Test Terms"
        btnTestTerms.UseVisualStyleBackColor = True
        ' 
        ' tmrLoadingOverlayFadeout
        ' 
        ' 
        ' tmrSessionState
        ' 
        tmrSessionState.Interval = 1000
        ' 
        ' tmrDesktopCheck
        ' 
        tmrDesktopCheck.Interval = 1000
        ' 
        ' btnForceUnlock
        ' 
        btnForceUnlock.Location = New Point(1310, 337)
        btnForceUnlock.Name = "btnForceUnlock"
        btnForceUnlock.Size = New Size(84, 53)
        btnForceUnlock.TabIndex = 8
        btnForceUnlock.Text = "Force Unlock"
        btnForceUnlock.UseVisualStyleBackColor = True
        ' 
        ' picActiveLanguageFlag
        ' 
        picActiveLanguageFlag.BackColor = Color.Transparent
        picActiveLanguageFlag.BorderStyle = BorderStyle.FixedSingle
        picActiveLanguageFlag.Cursor = Cursors.Hand
        picActiveLanguageFlag.Enabled = False
        picActiveLanguageFlag.Image = My.Resources.Resources.flag_placeholder
        picActiveLanguageFlag.Location = New Point(1394, 82)
        picActiveLanguageFlag.Name = "picActiveLanguageFlag"
        picActiveLanguageFlag.Size = New Size(34, 22)
        picActiveLanguageFlag.SizeMode = PictureBoxSizeMode.AutoSize
        picActiveLanguageFlag.TabIndex = 12
        picActiveLanguageFlag.TabStop = False
        picActiveLanguageFlag.Visible = False
        ' 
        ' pnlInformation
        ' 
        pnlInformation.BackColor = Color.WhiteSmoke
        pnlInformation.BorderStyle = BorderStyle.FixedSingle
        pnlInformation.Controls.Add(linklabelInformation_Helpdesk)
        pnlInformation.Controls.Add(lblInformation_SystemInfo)
        pnlInformation.Location = New Point(708, 646)
        pnlInformation.Name = "pnlInformation"
        pnlInformation.Size = New Size(718, 36)
        pnlInformation.TabIndex = 13
        ' 
        ' linklabelInformation_Helpdesk
        ' 
        linklabelInformation_Helpdesk.AutoSize = True
        linklabelInformation_Helpdesk.Font = New Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        linklabelInformation_Helpdesk.LinkColor = Color.Black
        linklabelInformation_Helpdesk.Location = New Point(652, 2)
        linklabelInformation_Helpdesk.Name = "linklabelInformation_Helpdesk"
        linklabelInformation_Helpdesk.Size = New Size(61, 13)
        linklabelInformation_Helpdesk.TabIndex = 14
        linklabelInformation_Helpdesk.TabStop = True
        linklabelInformation_Helpdesk.Text = "LinkLabel1"
        linklabelInformation_Helpdesk.TextAlign = ContentAlignment.TopRight
        ' 
        ' lblInformation_SystemInfo
        ' 
        lblInformation_SystemInfo.AutoSize = True
        lblInformation_SystemInfo.Font = New Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblInformation_SystemInfo.Location = New Point(673, 19)
        lblInformation_SystemInfo.Name = "lblInformation_SystemInfo"
        lblInformation_SystemInfo.Size = New Size(40, 13)
        lblInformation_SystemInfo.TabIndex = 15
        lblInformation_SystemInfo.Text = "Label1"
        lblInformation_SystemInfo.TextAlign = ContentAlignment.MiddleRight
        ' 
        ' btnHelpdeskTest
        ' 
        btnHelpdeskTest.Location = New Point(1310, 396)
        btnHelpdeskTest.Name = "btnHelpdeskTest"
        btnHelpdeskTest.Size = New Size(84, 53)
        btnHelpdeskTest.TabIndex = 14
        btnHelpdeskTest.Text = "helpdesk"
        btnHelpdeskTest.UseVisualStyleBackColor = True
        ' 
        ' tmrLoadingOverlayDelay
        ' 
        tmrLoadingOverlayDelay.Interval = 1000
        ' 
        ' lblDebug_Idle
        ' 
        lblDebug_Idle.AutoSize = True
        lblDebug_Idle.BackColor = Color.IndianRed
        lblDebug_Idle.BorderStyle = BorderStyle.FixedSingle
        lblDebug_Idle.ForeColor = Color.White
        lblDebug_Idle.Location = New Point(8, 288)
        lblDebug_Idle.Name = "lblDebug_Idle"
        lblDebug_Idle.Size = New Size(72, 16)
        lblDebug_Idle.TabIndex = 16
        lblDebug_Idle.Text = "Debug_Idle"
        ' 
        ' lblDebug_SessionAge
        ' 
        lblDebug_SessionAge.AutoSize = True
        lblDebug_SessionAge.BackColor = Color.IndianRed
        lblDebug_SessionAge.BorderStyle = BorderStyle.FixedSingle
        lblDebug_SessionAge.ForeColor = Color.White
        lblDebug_SessionAge.Location = New Point(8, 337)
        lblDebug_SessionAge.Name = "lblDebug_SessionAge"
        lblDebug_SessionAge.Size = New Size(114, 16)
        lblDebug_SessionAge.TabIndex = 17
        lblDebug_SessionAge.Text = "Debug_SessionAge"
        ' 
        ' webview2_Main
        ' 
        webview2_Main.AllowExternalDrop = False
        webview2_Main.BackColor = Color.White
        webview2_Main.CreationProperties = Nothing
        webview2_Main.DefaultBackgroundColor = Color.White
        webview2_Main.Location = New Point(8, 8)
        webview2_Main.Name = "webview2_Main"
        webview2_Main.Size = New Size(459, 247)
        webview2_Main.TabIndex = 18
        webview2_Main.ZoomFactor = 1R
        ' 
        ' lblDebug_Foreground
        ' 
        lblDebug_Foreground.AutoSize = True
        lblDebug_Foreground.BackColor = Color.IndianRed
        lblDebug_Foreground.BorderStyle = BorderStyle.FixedSingle
        lblDebug_Foreground.ForeColor = Color.White
        lblDebug_Foreground.Location = New Point(8, 356)
        lblDebug_Foreground.Name = "lblDebug_Foreground"
        lblDebug_Foreground.Size = New Size(115, 16)
        lblDebug_Foreground.TabIndex = 19
        lblDebug_Foreground.Text = "Debug_Foreground"
        ' 
        ' picLogout
        ' 
        picLogout.BackColor = Color.Transparent
        picLogout.Cursor = Cursors.Hand
        picLogout.Enabled = False
        picLogout.Image = My.Resources.Resources.logout_bw_64x64__2
        picLogout.Location = New Point(1362, 12)
        picLogout.Name = "picLogout"
        picLogout.Size = New Size(64, 64)
        picLogout.SizeMode = PictureBoxSizeMode.StretchImage
        picLogout.TabIndex = 20
        picLogout.TabStop = False
        picLogout.Visible = False
        ' 
        ' tmrSecret
        ' 
        tmrSecret.Interval = 2000
        ' 
        ' pnlSecret_Left
        ' 
        pnlSecret_Left.Location = New Point(198, 288)
        pnlSecret_Left.Name = "pnlSecret_Left"
        pnlSecret_Left.Size = New Size(92, 60)
        pnlSecret_Left.TabIndex = 22
        ' 
        ' pnlSecret_Right
        ' 
        pnlSecret_Right.Location = New Point(314, 288)
        pnlSecret_Right.Name = "pnlSecret_Right"
        pnlSecret_Right.Size = New Size(92, 60)
        pnlSecret_Right.TabIndex = 23
        ' 
        ' pnlDebugMode
        ' 
        pnlDebugMode.BackColor = Color.Red
        pnlDebugMode.Controls.Add(lblDebugMode)
        pnlDebugMode.Location = New Point(493, 8)
        pnlDebugMode.Name = "pnlDebugMode"
        pnlDebugMode.Size = New Size(329, 25)
        pnlDebugMode.TabIndex = 23
        ' 
        ' lblDebugMode
        ' 
        lblDebugMode.Font = New Font("Tahoma", 9F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        lblDebugMode.ForeColor = Color.WhiteSmoke
        lblDebugMode.Location = New Point(51, 0)
        lblDebugMode.Name = "lblDebugMode"
        lblDebugMode.Size = New Size(155, 24)
        lblDebugMode.TabIndex = 0
        lblDebugMode.Text = "!!! Debug mode !!!"
        lblDebugMode.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' lblDebug_MousePosition
        ' 
        lblDebug_MousePosition.AutoSize = True
        lblDebug_MousePosition.BackColor = Color.IndianRed
        lblDebug_MousePosition.BorderStyle = BorderStyle.FixedSingle
        lblDebug_MousePosition.ForeColor = Color.White
        lblDebug_MousePosition.Location = New Point(8, 315)
        lblDebug_MousePosition.Name = "lblDebug_MousePosition"
        lblDebug_MousePosition.Size = New Size(129, 16)
        lblDebug_MousePosition.TabIndex = 24
        lblDebug_MousePosition.Text = "Debug_MousePosition"
        ' 
        ' pnlActiveLanguageFlagShadow
        ' 
        pnlActiveLanguageFlagShadow.BackColor = Color.Black
        pnlActiveLanguageFlagShadow.Location = New Point(1330, 142)
        pnlActiveLanguageFlagShadow.Name = "pnlActiveLanguageFlagShadow"
        pnlActiveLanguageFlagShadow.Size = New Size(44, 30)
        pnlActiveLanguageFlagShadow.TabIndex = 25
        ' 
        ' frmMain
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Silver
        BackgroundImageLayout = ImageLayout.Center
        ClientSize = New Size(1438, 694)
        ControlBox = False
        Controls.Add(lblDebug_MousePosition)
        Controls.Add(pnlDebugMode)
        Controls.Add(pnlSecret_Right)
        Controls.Add(pnlSecret_Left)
        Controls.Add(picLogout)
        Controls.Add(lblDebug_Foreground)
        Controls.Add(webview2_Main)
        Controls.Add(lblDebug_SessionAge)
        Controls.Add(lblDebug_Idle)
        Controls.Add(btnHelpdeskTest)
        Controls.Add(pnlInformation)
        Controls.Add(picActiveLanguageFlag)
        Controls.Add(btnForceUnlock)
        Controls.Add(btnTestTerms)
        Controls.Add(txtDebug)
        Controls.Add(btnEmergencyClose)
        Controls.Add(pnlActiveLanguageFlagShadow)
        Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        Icon = CType(resources.GetObject("$this.Icon"), Icon)
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmMain"
        ShowInTaskbar = False
        Text = "OVCC Kiosk"
        CType(picActiveLanguageFlag, ComponentModel.ISupportInitialize).EndInit()
        pnlInformation.ResumeLayout(False)
        pnlInformation.PerformLayout()
        CType(webview2_Main, ComponentModel.ISupportInitialize).EndInit()
        CType(picLogout, ComponentModel.ISupportInitialize).EndInit()
        pnlDebugMode.ResumeLayout(False)
        ResumeLayout(False)
        PerformLayout()

    End Sub
    Friend WithEvents btnEmergencyClose As Button
    Friend WithEvents txtDebug As TextBox
    Friend WithEvents btnTestTerms As Button
    Friend WithEvents tmrLoadingOverlayFadeout As Timer
    Friend WithEvents tmrSessionState As Timer
    Friend WithEvents tmrDesktopCheck As Timer
    Friend WithEvents btnForceUnlock As Button
    Friend WithEvents picActiveLanguageFlag As PictureBox
    Friend WithEvents pnlInformation As Panel
    Friend WithEvents lblInformation_SystemInfo As Label
    Friend WithEvents btnHelpdeskTest As Button
    Friend WithEvents tmrLoadingOverlayDelay As Timer
    Friend WithEvents linklabelInformation_Helpdesk As LinkLabel
    Friend WithEvents lblDebug_Idle As Label
    Friend WithEvents lblDebug_SessionAge As Label
    Friend WithEvents webview2_Main As Microsoft.Web.WebView2.WinForms.WebView2
    Friend WithEvents lblDebug_Foreground As Label
    Friend WithEvents picLogout As PictureBox
    Friend WithEvents tmrSecret As Timer
    Friend WithEvents pnlSecret_Left As Panel
    Friend WithEvents pnlSecret_Right As Panel
    Friend WithEvents pnlDebugMode As Panel
    Friend WithEvents lblDebugMode As Label
    Friend WithEvents lblDebug_MousePosition As Label
    Friend WithEvents pnlActiveLanguageFlagShadow As Panel
End Class
