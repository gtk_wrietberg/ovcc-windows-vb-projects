﻿Imports System.ComponentModel
Imports System.IO
Imports Microsoft.Web.WebView2.Core
Imports SharpHook

#Disable Warning CA1416

Public Class frmMain
    Private WithEvents _processWatcher As New ProcessWatcher

    Private _frmLoadingOverlay As New frmLoadingOverlay
    Private _frmTermsOverlay As New frmTermsOverlay
    Private _frmAirlinesOverlay As New frmAirlinesOverlay
    Private _frmOfficeOverlay As New frmOfficeOverlay
    Private _frmLogoutOverlay As New frmLogoutOverlay
    Private _frmAdminPasswordOverlay As New frmAdminPasswordOverlay
    Private _frmAdminOverlay As New frmAdminOverlay
    Private _frmHelpdeskOverlay As New frmHelpdeskOverlay
    Private _frmGuestPasswordOverlay As New frmGuestPasswordOverlay
    Private _frmSessionIdleWarning As New frmSessionIdleWarning
    Private _frmLanguagesOverlay As New frmLanguagesOverlay
    Private _frmCustomOverlay As New frmCustomOverlay


    Private mLoadingOverlayOpacity As Double = 0.0
    Private mLoadingOverlayOpacityFadeoutSpeed As Double = 0.2
    Private mLoadingOverlayFakeDelay As Long = 4 'seconds

    Private mCurrentSessionState As Integer = -1

    Private mWebviewLoaded As Boolean = False

    Private WithEvents corewebview2_Main As CoreWebView2
    Private WithEvents webDoc As HtmlDocument


    Private mThreadsActive As Boolean = True
    Private mThread__IdleTime As Threading.Thread
    Private mThread__SessionAge As Threading.Thread
    Private mThread__Hook As Threading.Thread


#Region "Thread Safe stuff"
    Delegate Sub DebugToTextBoxCallback(ByVal [text] As String)
    Delegate Sub DebugToLabelCallback(ByVal [text] As String)
    Delegate Sub ShowFormCallback()

    Public Sub THREADSAFE__DebugToTextBox(ByVal [text] As String)
        Try
            If Me.txtDebug.InvokeRequired Then
                Dim d As New DebugToTextBoxCallback(AddressOf THREADSAFE__DebugToTextBox)
                Me.Invoke(d, New Object() {[text]})
            Else
                Me.txtDebug.AppendText(Trim([text]) & vbCrLf)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub THREADSAFE__DebugToLabel_Idle(ByVal [text] As String)
        Try
            If Me.lblDebug_Idle.InvokeRequired Then
                Dim d As New DebugToLabelCallback(AddressOf THREADSAFE__DebugToLabel_Idle)
                Me.Invoke(d, New Object() {[text]})
            Else
                Me.lblDebug_Idle.Text = [text]
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub THREADSAFE__DebugToLabel_MousePosition(ByVal [text] As String)
        Try
            If Me.lblDebug_MousePosition.InvokeRequired Then
                Dim d As New DebugToLabelCallback(AddressOf THREADSAFE__DebugToLabel_MousePosition)
                Me.Invoke(d, New Object() {[text]})
            Else
                Me.lblDebug_MousePosition.Text = [text]
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub THREADSAFE__DebugToLabel_SessionAge(ByVal [text] As String)
        Try
            If Me.lblDebug_SessionAge.InvokeRequired Then
                Dim d As New DebugToLabelCallback(AddressOf THREADSAFE__DebugToLabel_SessionAge)
                Me.Invoke(d, New Object() {[text]})
            Else
                Me.lblDebug_SessionAge.Text = "Session age: " & [text]
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub THREADSAFE__DebugToLabel_Foreground(ByVal [text] As String)
        Try
            If Me.lblDebug_Foreground.InvokeRequired Then
                Dim d As New DebugToLabelCallback(AddressOf THREADSAFE__DebugToLabel_Foreground)
                Me.Invoke(d, New Object() {[text]})
            Else
                Me.lblDebug_Foreground.Text = "Is foreground: " & [text]
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub THREADSAFE__ShowForm__SessionIdleWarning()
        Try
            If Me.InvokeRequired Then
                Dim d As New ShowFormCallback(AddressOf THREADSAFE__ShowForm__SessionIdleWarning)
                Me.Invoke(d, New Object() {})
            Else
                Me.Activate()

                CloseAllDialogs()

                Me._frmSessionIdleWarning = New frmSessionIdleWarning
                Dim dialogResult As DialogResult = Me._frmSessionIdleWarning.ShowDialog(Me)
                Me._frmSessionIdleWarning.Dispose()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Hotkey"
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        'prevent form moving
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If


        'hotkey
        If m.Msg = Hotkey.WM_HOTKEY Then
            HotKeyHandler(m.WParam)
        End If

        MyBase.WndProc(m)
    End Sub

    Private Sub HotKeyHandler(Id As IntPtr)
        Select Case Id
            Case Hotkey.HOTKEYS.ADMIN_DIALOG
                ShowAdminPasswordDialog()
            Case 2

            Case 3

            Case Else

        End Select

        'MsgBox("HOTKEY: " & Id.ToString)
    End Sub
#End Region

#Region "keyboard/mouse hook"
    Public Event MouseMoveDetected(ByVal X As Short, ByVal Y As Short)
    Public Event MouseClickDetected(ByVal X As Short, ByVal Y As Short, ByVal Button As UShort)
    Public Event KeyTypedDetected(ByVal KeyCode As UShort, ByVal KeyChar As Char)


    'Keyboard/mouse hook
    Private WithEvents mHook As TaskPoolGlobalHook

    Public Sub Hook__Start()
        Hook__Stop()

        Try
            NamedPipe.Logger.Client.Message("HOOK", "Starting")

            mHook = New TaskPoolGlobalHook
            mHook.RunAsync()
        Catch ex As Exception
            NamedPipe.Logger.Client.Error("HOOK", "Starting failed: " & ex.Message)
        End Try
    End Sub

    Public Sub Hook__Stop()
        If mHook IsNot Nothing Then
            If Not mHook.IsDisposed Then
                Try
                    NamedPipe.Logger.Client.Message("HOOK", "Stopping")
                    mHook.Dispose()
                Catch ex As Exception
                    NamedPipe.Logger.Client.Error("HOOK", "Stopping failed: " & ex.Message)
                End Try
            Else
                NamedPipe.Logger.Client.Debug("HOOK", "Stopping skipped, hook already disposed")
            End If
        Else
            NamedPipe.Logger.Client.Debug("HOOK", "Stopping skipped, hook not set")
        End If
    End Sub

#Region "events"
    Private Sub mHook_MouseMoved(sender As Object, e As MouseHookEventArgs) Handles mHook.MouseMoved
        THREADSAFE__DebugToLabel_MousePosition(e.Data.X & ":" & e.Data.Y)

        RaiseEvent MouseMoveDetected(e.Data.X, e.Data.Y)
    End Sub

    Private Sub mHook_MouseClicked(sender As Object, e As MouseHookEventArgs) Handles mHook.MouseClicked
        RaiseEvent MouseClickDetected(e.Data.X, e.Data.Y, e.Data.Button)
    End Sub

    Private Sub mHook_KeyTyped(sender As Object, e As KeyboardHookEventArgs) Handles mHook.KeyTyped
        RaiseEvent KeyTypedDetected(e.Data.KeyCode, e.Data.KeyChar)
    End Sub
#End Region
#End Region


    Private Sub Initialise()
        NamedPipe.Logger.Client.Message("initialising")


        LoadSettings()

        If Globals.Application.Debug Then
            NamedPipe.Logger.Client.Message("debug mode on")
        End If


        'Set fullscreen etc.
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Normal
        Me.Location = New Point(0, 0)
        Me.BackColor = Color.White
        Me.Size = SystemInformation.PrimaryMonitorSize
        Helpers.Forms.TopMost(Me.Handle, Not Globals.Application.IsDevMachineOrDebugging)


        'logout button
        picLogout.Top = Globals.UI.BUTTON_MARGIN
        picLogout.Left = Me.Width - picLogout.Width - Globals.UI.BUTTON_MARGIN
        picLogout.BringToFront()


        'menu button
        picActiveLanguageFlag.Width = 1
        picActiveLanguageFlag.Height = 1
        picActiveLanguageFlag.Top = picLogout.Top + picLogout.Height + Globals.UI.BUTTON_MARGIN
        picActiveLanguageFlag.Left = Me.Width - picActiveLanguageFlag.Width - Globals.UI.BUTTON_MARGIN
        picActiveLanguageFlag.BringToFront()


        'info box
        pnlInformation.Top = Me.Height - pnlInformation.Height - Globals.UI.BUTTON_MARGIN
        pnlInformation.Left = Me.Width - pnlInformation.Width - Globals.UI.BUTTON_MARGIN
        pnlInformation.BringToFront()


        'secret click boxes
        pnlSecret_Left.Width = Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Left.Height = Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Left.Top = Me.Height - Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Left.Left = 0
        pnlSecret_Left.BackColor = If(Globals.Application.Debug, Color.Red, Color.Transparent)
        pnlSecret_Left.BorderStyle = If(Globals.Application.Debug, BorderStyle.FixedSingle, BorderStyle.None)
        pnlSecret_Left.BringToFront()

        pnlSecret_Right.Width = Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Right.Height = Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Right.Top = Me.Height - Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Right.Left = Me.Width - Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Right.BackColor = If(Globals.Application.Debug, Color.Red, Color.Transparent)
        pnlSecret_Right.BorderStyle = If(Globals.Application.Debug, BorderStyle.FixedSingle, BorderStyle.None)
        pnlSecret_Right.BringToFront()

        tmrSecret.Interval = Globals.Secret.SECRET_BOX_DELAY


        'hotkeys
        Hotkey.FormHandle = Me.Handle
        Hotkey.Register(Hotkey.HOTKEYS.ADMIN_DIALOG, Keys.D0, Hotkey.KeyModifier.Control Or Hotkey.KeyModifier.Alt Or Hotkey.KeyModifier.Shift)
        'Hotkey.Register(2, 50, Hotkey.KeyModifier.Control) ' 50 = '2'
        'Hotkey.Register(3, 51, Hotkey.KeyModifier.Control) ' 51 = '3'


        'Start ProcessWatcher
        _processWatcher.Start()


        'Start keyboard/mouse hook
        Hook__Start()


        'Logoff detection
        'AddHandler Microsoft.Win32.SystemEvents.SessionEnding, AddressOf SessionEnding


        'Language stuff
        Localisation.LoadLanguages()
        NamedPipe.Language.Client.DefaultLanguage()
        UpdateLanguageStringsHandler(False)


        'airline stuff
        Airlines.LoadAirlines()


        'Start Timers
        tmrSessionState.Enabled = True
        tmrDesktopCheck.Enabled = True


        'Debug stuff
        DrawDebugStuff()


        'Redraw controls
        RedrawControls()


        'Redraw language flag
        RedrawLanguageFlag()


        'Start idle time thread
        mThread__IdleTime = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__IdleTime))
        mThread__IdleTime.Start()


        'Start session age thread
        mThread__SessionAge = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__SessionAge))
        mThread__SessionAge.Start()


        'fake loading delay
        tmrLoadingOverlayDelay.Enabled = True
    End Sub

    Private Sub DrawDebugStuff()
        pnlDebugMode.Top = 0
        pnlDebugMode.Width = Me.Width / 2
        pnlDebugMode.Left = (Me.Width - pnlDebugMode.Width) / 2
        pnlDebugMode.Visible = Globals.Application.Debug
        pnlDebugMode.Enabled = Globals.Application.Debug
        pnlDebugMode.BringToFront()

        lblDebugMode.Top = 0
        lblDebugMode.Left = 0
        lblDebugMode.Width = pnlDebugMode.Width
        lblDebugMode.Visible = Globals.Application.Debug
        lblDebugMode.Enabled = Globals.Application.Debug
        lblDebugMode.BringToFront()

        lblDebug_Idle.Top = pnlDebugMode.Height + 3
        lblDebug_Idle.Left = Me.Width / 2
        lblDebug_Idle.Visible = Globals.Application.Debug
        lblDebug_Idle.Enabled = Globals.Application.Debug
        lblDebug_Idle.BringToFront()

        lblDebug_SessionAge.Top = pnlDebugMode.Height + 3 + lblDebug_Idle.Height + 3
        lblDebug_SessionAge.Left = Me.Width / 2
        lblDebug_SessionAge.Visible = Globals.Application.Debug
        lblDebug_SessionAge.Enabled = Globals.Application.Debug
        lblDebug_SessionAge.BringToFront()

        lblDebug_Foreground.Top = pnlDebugMode.Height + 3 + lblDebug_Idle.Height + 3 + lblDebug_SessionAge.Height + 3
        lblDebug_Foreground.Left = Me.Width / 2
        lblDebug_Foreground.Visible = Globals.Application.Debug
        lblDebug_Foreground.Enabled = Globals.Application.Debug
        lblDebug_Foreground.BringToFront()

        lblDebug_MousePosition.Top = pnlDebugMode.Height + 3 + lblDebug_Idle.Height + 3 + lblDebug_SessionAge.Height + 3 + lblDebug_Foreground.Height + 3
        lblDebug_MousePosition.Left = Me.Width / 2
        lblDebug_MousePosition.Visible = Globals.Application.Debug
        lblDebug_MousePosition.Enabled = Globals.Application.Debug
        lblDebug_MousePosition.BringToFront()


        btnEmergencyClose.Top = 300
        btnEmergencyClose.Left = Me.Width - btnEmergencyClose.Width - 12
        btnEmergencyClose.Visible = Globals.Application.Debug
        btnEmergencyClose.Enabled = Globals.Application.Debug
        btnEmergencyClose.BringToFront()

        btnTestTerms.Top = 3 + btnEmergencyClose.Top + btnEmergencyClose.Height
        btnTestTerms.Left = Me.Width - btnTestTerms.Width - 12
        btnTestTerms.Visible = Globals.Application.Debug
        btnTestTerms.Enabled = Globals.Application.Debug
        btnTestTerms.BringToFront()

        btnForceUnlock.Top = 3 + btnTestTerms.Top + btnTestTerms.Height
        btnForceUnlock.Left = Me.Width - btnForceUnlock.Width - 12
        btnForceUnlock.Visible = Globals.Application.Debug
        btnForceUnlock.Enabled = Globals.Application.Debug
        btnForceUnlock.BringToFront()

        btnHelpdeskTest.Top = 3 + btnForceUnlock.Top + btnForceUnlock.Height
        btnHelpdeskTest.Left = Me.Width - btnHelpdeskTest.Width - 12
        btnHelpdeskTest.Visible = Globals.Application.Debug
        btnHelpdeskTest.Enabled = Globals.Application.Debug
        btnHelpdeskTest.BringToFront()

        txtDebug.Clear()
        'txtDebug.Top = 3 + btnForceUnlock.Top + btnForceUnlock.Height + btnHelpdeskTest.Height
        txtDebug.Width = 400
        txtDebug.Height = 500
        txtDebug.Top = btnEmergencyClose.Top
        'txtDebug.Left = Me.Width - txtDebug.Width - 12
        txtDebug.Left = btnEmergencyClose.Left - txtDebug.Width - 6
        txtDebug.Visible = Globals.Application.Debug
        txtDebug.Enabled = Globals.Application.Debug
        txtDebug.BringToFront()
        txtDebug.AppendText("btnEmergencyClose.Top=" & btnEmergencyClose.Top.ToString & vbCrLf)
        txtDebug.AppendText("btnEmergencyClose.Left=" & btnEmergencyClose.Left.ToString & vbCrLf)
        txtDebug.AppendText("btnEmergencyClose.Visible=" & btnEmergencyClose.Visible.ToString & vbCrLf)
    End Sub

    Private Sub Thread__IdleTime()
        Dim sIdle As String = ""
        Dim lIdle As Long = -1
        Dim lSessionAge As Long = -1
        Dim bScreensaverActive As Boolean = False
        Dim iDebugCounter As Integer = 0

        Do
            THREADSAFE__DebugToLabel_Foreground(Helpers.Forms.IsForeground(Globals.Application.MainFormHandle).ToString)


            lIdle = InputCheck.SystemIdleTime(sIdle)
            bScreensaverActive = Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsScreensaverActive, False)

            If iDebugCounter = 0 Then
                NamedPipe.Logger.Client.Debug("Idle time debug: " & sIdle)
            End If

            THREADSAFE__DebugToLabel_Idle("idle: " & lIdle & " - screensaver timeout: " & Globals.Application.ScreensaverTimeout & " - session timeout: " & Globals.Application.SessionTimeout)

            NamedPipe.Session.Client.IdleTime(lIdle)

#Region "session"
            If SessionStuff.IsSessionActive Then
                If lIdle > Globals.Application.SessionTimeout Then
                    If Not Globals.Application.SessionTimeoutWarningActive Then
                        NamedPipe.Logger.Client.Message("Session is inactive for too long, showing warning")
                        NamedPipe.Session.Client.IdleWarning()

                        Globals.Application.SessionTimeoutWarningActive = True

                        THREADSAFE__ShowForm__SessionIdleWarning()
                    End If

                    If lIdle > (Globals.Application.SessionTimeout + Globals.Application.SessionTimeoutWarningDuration) Then
                        NamedPipe.Session.Client.SessionEndForced()
                    End If

                    If bScreensaverActive Then
                        'Screensaver is active, let's kill it
                        NamedPipe.Logger.Client.Message("Stopping screensaver: " & Settings.Constants.Paths.Files.Apps.Screensaver.Path)
                        NamedPipe.Session.Client.ScreensaverKilled()
                    End If
                Else
                    Globals.Application.SessionTimeoutWarningActive = False
                End If
            Else
                Globals.Application.SessionTimeoutWarningActive = False
            End If
#End Region

#Region "screensaver"
            If lIdle > Globals.Application.ScreensaverTimeout Then
                If Not Globals.Application.SessionTimeoutWarningActive Then
                    If iDebugCounter = 0 Then
                        NamedPipe.Logger.Client.Debug("Idle too long, screensaver time")
                    End If



                    If Not bScreensaverActive Then
                        NamedPipe.Logger.Client.Message("Starting screensaver: " & Settings.Constants.Paths.Files.Apps.Screensaver.Path)

                        NamedPipe.Session.Client.ScreensaverTriggered()
                        If Helpers.Processes.StartProcess(Settings.Constants.Paths.Files.Apps.Screensaver.Path,, False) Then
                            NamedPipe.Session.Client.ScreensaverStarted()
                        Else
                            NamedPipe.Session.Client.ScreensaverFailed()
                        End If
                    Else
                        If iDebugCounter = 0 Then
                            NamedPipe.Logger.Client.Debug("Screensaver already active")
                        End If
                    End If
                Else
                    If iDebugCounter = 0 Then
                        NamedPipe.Logger.Client.Debug("Idle too long, but session warning is active")
                    End If
                End If
            Else
                If bScreensaverActive Then
                    NamedPipe.Logger.Client.Message("Stopping screensaver: " & Settings.Constants.Paths.Files.Apps.Screensaver.Path)
                    NamedPipe.Session.Client.ScreensaverKilled()
                End If
            End If
#End Region


            iDebugCounter += 1
            If iDebugCounter > 10 Then
                iDebugCounter = 0
            End If

            Threading.Thread.Sleep(1000)
        Loop While mThreadsActive
    End Sub

    Private Sub Thread__SessionAge()
        Dim lAge As Long = -1
        Dim bActive As Boolean = False
        Dim iDebugCounterMax As Integer = 10
        Dim iDebugCounter As Integer = iDebugCounterMax

        Do
            If SessionStuff.IsSessionActive Then
                lAge = SessionStuff.GetSessionAge()
                If iDebugCounter = 0 Then
                    NamedPipe.Logger.Client.Debug("Session age debug: " & lAge)
                End If

                THREADSAFE__DebugToLabel_SessionAge(lAge)
            Else
                THREADSAFE__DebugToLabel_SessionAge("-")
            End If



            iDebugCounter += 1
            If iDebugCounter > iDebugCounterMax Then
                iDebugCounter = 0
            End If

            Threading.Thread.Sleep(1000)
        Loop While mThreadsActive
    End Sub

    Private Sub CleanWebViewUserData()
        Try
            NamedPipe.Logger.Client.Message("cleaning " & Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER)

            IO.Directory.Delete(Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER, True)
        Catch ex As Exception
            NamedPipe.Logger.Client.Error("cleaning failed: " & ex.Message)
        End Try

        Try
            NamedPipe.Logger.Client.Message("creating " & Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER)

            IO.Directory.CreateDirectory(Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER)
        Catch ex As Exception
            NamedPipe.Logger.Client.Error("creating failed: " & ex.Message)
        End Try
    End Sub

    Private Async Sub InitialiseMainWebView()
        'Await InitialiseCoreWebView2Async()

        Dim cwEnvironment As CoreWebView2Environment = Nothing
        Dim sUserDataFolder As String = IO.Path.Combine(Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER, My.Application.Info.ProductName)

        NamedPipe.Logger.Client.Debug("Webview2 user data: " & sUserDataFolder)

        cwEnvironment = Await CoreWebView2Environment.CreateAsync(Nothing, sUserDataFolder, Nothing)

        Await webview2_Main.EnsureCoreWebView2Async(cwEnvironment)



        webview2_Main.Dock = DockStyle.Fill

        corewebview2_Main = webview2_Main.CoreWebView2

        webview2_Main.CoreWebView2.AddHostObjectToScript(Settings.Constants.Applications.Homepage.WebBrowserScriptInterface_Name, New WebBrowserScriptInterface())

        webview2_Main.CoreWebView2.Settings.AreDevToolsEnabled = Globals.Application.Debug
        webview2_Main.CoreWebView2.Settings.AreDefaultContextMenusEnabled = Globals.Application.Debug


        Dim sThemeBaseFolder As String = Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Themes, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Themes__BaseFolder, "")
        Dim sThemeName As String = Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Themes, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Themes__ActiveTheme, "")
        Dim sTheme As String = "file:///" & IO.Path.Combine(sThemeBaseFolder, sThemeName, "theme", "index.html")

        webview2_Main.CoreWebView2.Navigate(sTheme)
    End Sub

    Private Sub LoadSettings()
        NamedPipe.Logger.Client.Message("Loading settings")


        Globals.Application.Debug = Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__Debug, True)

        Globals.Application.AdminPassword = Helpers.XOrObfuscation_v2.Deobfuscate(
                                                Settings.Get.String(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__AdminPassword,
                                                    "0758405357522b2e72627754664b18465c520b0711575b500b520c16135e0d0f0a570709160453507c78716a7f533c1a19175b530d5a46525c0708550314165a0d590c0557584a0607547a79236324503e4e10405f515855405b09040c040016120f0f5c5e52565b105255527b7b"))


        Globals.Application.GuestPasswordEnabled = Settings.Get.Boolean(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__GuestPasswordEnabled,
                                                    False)

        Globals.Application.ScreensaverTimeout = Settings.Get.Long(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__ScreensaverTimeout,
                                                    300)

        Globals.Application.SessionTimeout = Settings.Get.Long(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__SessionTimeout,
                                                    600)

        Globals.Application.SessionTimeoutWarningDuration = Settings.Get.Long(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__SessionTimeoutWarningDuration,
                                                    30)


        Dim tmpPermittedScriptingHosts As String = Settings.Get.String(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__PermittedScriptingHosts,
                                                    "")
        Globals.Security.PermittedScriptingHosts.Load(tmpPermittedScriptingHosts)


        NamedPipe.Logger.Client.Message("Debug                         =" & Globals.Application.Debug.ToString)
        NamedPipe.Logger.Client.Message("GuestPasswordEnabled          =" & Globals.Application.GuestPasswordEnabled.ToString)
        NamedPipe.Logger.Client.Message("ScreensaverTimeout            =" & Globals.Application.ScreensaverTimeout.ToString)
        NamedPipe.Logger.Client.Message("SessionTimeout                =" & Globals.Application.SessionTimeout.ToString)
        NamedPipe.Logger.Client.Message("SessionTimeoutWarningDuration =" & Globals.Application.SessionTimeoutWarningDuration.ToString)
        NamedPipe.Logger.Client.Debug("PermittedScriptingHosts       =" & tmpPermittedScriptingHosts)


        '-----------------------------------------------------
        'External applications
        Globals.ExternalApplications.Reset()


        Dim mList As List(Of String) = Helpers.Registry64.FindValues(
                                            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_ExternalApplications,
                                            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__FINDVALUES_SEARCHSTRING)


        For Each _external_app_regkey As String In mList
            If _external_app_regkey.EndsWith(Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__Name_FINDVALUES_SEARCHSTRING) Then
                Dim sIndex As String = Globals.Application.RegularExpressions.EXTERNAL_APPLICATION.Replace(_external_app_regkey, "$1")
                Dim iIndex As Integer = -1
                Dim sName As String = ""
                Dim sPath As String = ""
                Dim sParams As String = ""

                If Integer.TryParse(sIndex, iIndex) Then
                    sName = Settings.Get.String(
                                Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_ExternalApplications,
                                Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__Name_SEARCHSTRING.Replace("%%", sIndex),
                                "")
                    sPath = Settings.Get.String(
                                Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_ExternalApplications,
                                Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__Path_SEARCHSTRING.Replace("%%", sIndex),
                                "")
                    sParams = Settings.Get.String(
                                Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_ExternalApplications,
                                Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__Params_SEARCHSTRING.Replace("%%", sIndex),
                                "")

                    NamedPipe.Logger.Client.Message("External application")
                    NamedPipe.Logger.Client.Message("id    =" & iIndex.ToString, 1)
                    NamedPipe.Logger.Client.Message("name  =" & sName, 1)
                    NamedPipe.Logger.Client.Message("path  =" & sPath, 1)
                    NamedPipe.Logger.Client.Message("params=" & sParams, 1)

                    Globals.ExternalApplications.Add(iIndex, sName, sPath, sParams)
                End If
            End If


            'Dim sIndex As String = _external_app_regkey.Replace(Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__Name_SEARCHSTRING, "")

            'Dim iIndex As Integer = -1
            'Dim sName As String = ""
            'Dim sPath As String = ""
            'Dim sParams As String = ""

            'If Integer.TryParse(sIndex, iIndex) Then
            '    sName = Settings.Get.String(
            '                Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_ExternalApplications,
            '                Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__Name_SEARCHSTRING & sIndex,
            '                "")
            '    sPath = Settings.Get.String(
            '                Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_ExternalApplications,
            '                Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__Path_SEARCHSTRING & sIndex,
            '                "")
            '    sParams = Settings.Get.String(
            '                Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_ExternalApplications,
            '                Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_ExternalApplication__Params_SEARCHSTRING & sIndex,
            '                "")

            '    NamedPipe.Logger.Client.Message("External application id       =" & iIndex.ToString)
            '    NamedPipe.Logger.Client.Message("External application name     =" & sName)
            '    NamedPipe.Logger.Client.Message("External application path     =" & sPath)
            '    NamedPipe.Logger.Client.Message("External application params   =" & sParams)
            '    Globals.ExternalApplications.Add(iIndex, sName, sPath, sParams)
            'End If
        Next


        '-----------------------------------------------------
        'secret box
        Globals.Secret.SECRET_BOX_SIZE = Settings.Get.Integer(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__SecretBoxSize, 100)
        Globals.Secret.SECRET_BOX_DELAY = Settings.Get.Integer(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__SecretBoxDelay, 5000)
    End Sub


    Private Function ApplicationExit(Optional reason As String = "") As Boolean
        If SessionStuff.HomepageKeepAlive And Not Globals.Application.EmergencyExit Then
            If reason <> "" Then
                NamedPipe.Logger.Client.Warning("exit with reason '" & reason & "' cancelled")
            Else
                NamedPipe.Logger.Client.Warning("exit cancelled")
            End If

            Return False
        End If


        NamedPipe.System.Client.ClosedHomepage()


        mThreadsActive = False
        Globals.Application.IsClosing = True
        Unlock()
        Hotkey.Unregister()
        _processWatcher.Stop()
        Hook__Stop()


        NamedPipe.Logger.Client.Message("exiting")
        If reason <> "" Then
            NamedPipe.Logger.Client.Message("exit reason: " & reason)
        End If

        NamedPipe.Logger.Client.Message("bye")


        Dim frms As New List(Of Form)
        For Each frm As Form In My.Application.OpenForms
            frms.Add(frm)
        Next

        For Each frm As Form In frms
            If Not frm Is Nothing Then
                frm.Close()
            End If
        Next

        Return True
    End Function

    Private Sub SessionStateCheck(Optional Forced As Boolean = False)
        'THREADSAFE__DebugToTextBox("SessionStateCheck" & " - " & Now.ToLongTimeString & " - start")

        If Not Globals.Application.Dialogs.IsDialogShown Then
            Dim currentSessionActiveState As Boolean = SessionStuff.IsSessionActive

            If SessionStuff.IsSessionActive Then
                If Globals.Application.IsLocked Then
                    THREADSAFE__DebugToTextBox("SessionStateCheck" & " - " & Now.ToLongTimeString & " - unlock")
                End If

                Unlock(Forced)

                SessionStuff.SessionAction.ExecuteNext()
            Else
                If SessionStuff.SessionAction.HasActions() Then
                    THREADSAFE__DebugToTextBox("SessionStateCheck" & " - " & Now.ToLongTimeString & " - HasActions")
                    ShowTerms()
                Else
                    If Not Globals.Application.IsLocked Then
                        THREADSAFE__DebugToTextBox("SessionStateCheck" & " - " & Now.ToLongTimeString & " - lock")
                    End If

                    Lock(Forced)
                End If

                'Lock(Forced)

                'If SessionStuff.IsTermsRequested And Not Globals.Application.Dialogs.IsDialogShown_Terms Then
                '    ShowTerms()
                'End If
            End If
        End If


        If SessionStuff.IsLogoutRequested Then
            THREADSAFE__DebugToTextBox("SessionStateCheck -  IsLogoutRequested")

            If Not Globals.Application.Dialogs.IsDialogShown_Logout Then
                THREADSAFE__DebugToTextBox("SessionStateCheck -  IsDialogShown_Logout")

                ForegroundWindowHandler__SetForegroundWindow(Me.Handle)

                CloseAllSubForms()

                ShowLogoutDialog()
            End If
        End If


        If Globals.Session.AirlinesDialogRequested Then
            Globals.Session.AirlinesDialogRequested = False

            If Not Globals.Application.Dialogs.IsDialogShown_Airlines Then
                ShowAirlines()
            End If
        End If

        If Globals.Session.OfficeDialogRequested Then
            Globals.Session.OfficeDialogRequested = False

            If Not Globals.Application.Dialogs.IsDialogShown_Office Then
                ShowOffice()
            End If
        End If

        If Globals.Session.CustomDialogRequested Then
            Globals.Session.CustomDialogRequested = False

            If Not Globals.Application.Dialogs.IsDialogShown_Custom Then
                ShowCustom()
            End If
        End If


        '-------------------------------------
        'show/hide menu/button/info panel
        'THREADSAFE__DebugToTextBox("Globals.UI.CURRENT_VALUE=" & Globals.UI.CURRENT_VALUE)
        If Globals.UI.HAS_CHANGED Then
            NamedPipe.Logger.Client.Debug("UI redraw triggered")
            NamedPipe.Logger.Client.Debug("Globals.UI.LOGOUT_BUTTON_VISIBLE:" & Globals.UI.LOGOUT_BUTTON_VISIBLE)

            picLogout.Enabled = Globals.UI.LOGOUT_BUTTON_VISIBLE
            picLogout.Visible = Globals.UI.LOGOUT_BUTTON_VISIBLE

            picActiveLanguageFlag.Enabled = Globals.UI.HOMEPAGE_MENU_LANGUAGES_VISIBLE
            picActiveLanguageFlag.Visible = Globals.UI.HOMEPAGE_MENU_LANGUAGES_VISIBLE
            If Globals.UI.LOGOUT_BUTTON_VISIBLE Then
                picActiveLanguageFlag.Top = Globals.UI.BUTTON_MARGIN + picLogout.Height + Globals.UI.BUTTON_MARGIN
            Else
                picActiveLanguageFlag.Top = Globals.UI.BUTTON_MARGIN
            End If

            pnlInformation.Enabled = Globals.UI.INFORMATION_PANEL_VISIBLE
            pnlInformation.Visible = Globals.UI.INFORMATION_PANEL_VISIBLE


            RedrawControls()
            RedrawLanguageFlag()
        End If
    End Sub

    Private Sub Lock(Optional Forced As Boolean = False)
        If Not Globals.Application.IsLocked Then
            Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes = 0
        End If
        If Not Globals.Application.IsLocked Or Not Globals.Application.IsLockSet Or Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes < 2 Or Forced Then
            NamedPipe.Logger.Client.Message("locking (#" & Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes & ")")

            Globals.Application.IsLocked = True
            If Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes >= Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes_MAX And Not Forced Then
                Exit Sub
            Else
                Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes += 1
            End If


            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
            Me.WindowState = FormWindowState.Maximized
            Me.Location = New Point(0, 0)
            Me.Size = SystemInformation.PrimaryMonitorSize
            Helpers.Forms.TopMost(Me.Handle, True)
            Helpers.Windows.TaskBar.Hide()

            RedrawControls()
        Else
            If Globals.Application.Debug Then
                If Globals.Application.IsLocked And Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes < Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes_MAX Then
                    NamedPipe.Logger.Client.Debug("locking skipped: already locked")
                Else
                    NamedPipe.Logger.Client.Debug("locking skipped")
                End If
            End If
        End If
    End Sub

    Private Sub Unlock(Optional Forced As Boolean = False)
        If Globals.Application.IsLocked Then
            Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes = 0
        End If
        If Globals.Application.IsLocked Or Not Globals.Application.IsLockSet Or Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes < Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes_MAX Or Forced Then
            NamedPipe.Logger.Client.Debug("unlocking (#" & Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes & ")")

            Globals.Application.IsLocked = False

            If Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes >= Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes_MAX And Not Forced Then
                Exit Sub
            Else
                Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes += 1
            End If

            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
            Me.WindowState = FormWindowState.Normal
            Me.Location = New Point(0, 0)
            Me.Size = SystemInformation.WorkingArea.Size
            Helpers.Forms.TopMost(Me.Handle, False)
            Helpers.Windows.TaskBar.Show()

            RedrawControls()
        Else
            If Globals.Application.Debug Then
                If Not Globals.Application.IsLocked And Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes < Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes_MAX Then
                    NamedPipe.Logger.Client.Debug("unlocking skipped: already unlocked")
                Else
                    NamedPipe.Logger.Client.Debug("unlocking skipped")
                End If
            End If
        End If
    End Sub

    Private Sub BringHomepageToFront()
        Me.Activate()
    End Sub

    Private Sub RedrawLanguageFlag()
        Dim sLanguageFlagPath As String = Localisation.GetFlagImagePath()
        Dim imageLanguageFlag As Image

        THREADSAFE__DebugToTextBox("RedrawLanguageFlag - Globals.UI.LOGOUT_BUTTON_VISIBLE:" & Globals.UI.LOGOUT_BUTTON_VISIBLE)

        If IO.File.Exists(sLanguageFlagPath) Then
            imageLanguageFlag = Image.FromFile(sLanguageFlagPath)
        Else
            imageLanguageFlag = My.Resources.flag_placeholder
            NamedPipe.Logger.Client.Error("Could not find language flag for language id '" & Localisation.GetActiveLanguageId().ToString & "': " & sLanguageFlagPath)
        End If

        picActiveLanguageFlag.Image = imageLanguageFlag
        THREADSAFE__DebugToTextBox("Flag width:" & picActiveLanguageFlag.Width & " - Flag height:" & picActiveLanguageFlag.Height)
        If Globals.UI.LOGOUT_BUTTON_VISIBLE Then
            picActiveLanguageFlag.Top = picLogout.Top + picLogout.Height + Globals.UI.BUTTON_MARGIN
        Else
            picActiveLanguageFlag.Top = Globals.UI.BUTTON_MARGIN
        End If
        picActiveLanguageFlag.Left = Me.Width - picActiveLanguageFlag.Width - Globals.UI.BUTTON_MARGIN

        pnlActiveLanguageFlagShadow.Width = picActiveLanguageFlag.Width
        pnlActiveLanguageFlagShadow.Height = picActiveLanguageFlag.Height
        pnlActiveLanguageFlagShadow.Top = picActiveLanguageFlag.Top + Globals.UI.SHADOW_MARGIN
        pnlActiveLanguageFlagShadow.Left = picActiveLanguageFlag.Left + Globals.UI.SHADOW_MARGIN
        pnlActiveLanguageFlagShadow.BringToFront()
        picActiveLanguageFlag.BringToFront()
    End Sub

    Private Sub RedrawControls()
        Dim infobox_Helpdesk_text As String = Localisation.LoadString(1100000)
        Dim infobox_Helpdesk_link As String = Localisation.LoadString(1100002)

        linklabelInformation_Helpdesk.Links.Clear()
        linklabelInformation_Helpdesk.Text = infobox_Helpdesk_text & " " & infobox_Helpdesk_link
        linklabelInformation_Helpdesk.Links.Add(infobox_Helpdesk_text.Length + 1, infobox_Helpdesk_link.Length)


        Dim tmpVersion As String
        If Globals.Application.Debug Then
            tmpVersion = My.Application.Info.Version.ToString
        Else
            tmpVersion = My.Application.Info.Version.ToString(2)
        End If

        lblInformation_SystemInfo.Text = Localisation.LoadString(1100010) & ": " & Environment.MachineName & " - " & Localisation.LoadString(1100020) & ": " & tmpVersion

        pnlInformation.Width = Math.Max(linklabelInformation_Helpdesk.Width, lblInformation_SystemInfo.Width) + 2 * 6

        linklabelInformation_Helpdesk.Left = pnlInformation.Width - linklabelInformation_Helpdesk.Width - 6
        lblInformation_SystemInfo.Left = pnlInformation.Width - lblInformation_SystemInfo.Width - 6


        pnlInformation.Top = Me.Height - pnlInformation.Height - Globals.UI.BUTTON_MARGIN
        pnlInformation.Left = Me.Width - pnlInformation.Width - Globals.UI.BUTTON_MARGIN
        pnlInformation.BringToFront()


        '-----------------------------------------------------------------------------
        'secret click boxes
        pnlSecret_Left.Width = Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Left.Height = Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Left.Top = Me.Height - Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Left.Left = 0
        pnlSecret_Left.BackColor = If(Globals.Application.Debug, Color.Red, Color.Transparent)
        pnlSecret_Left.BorderStyle = If(Globals.Application.Debug, BorderStyle.FixedSingle, BorderStyle.None)
        pnlSecret_Left.BringToFront()

        pnlSecret_Right.Width = Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Right.Height = Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Right.Top = Me.Height - Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Right.Left = Me.Width - Globals.Secret.SECRET_BOX_SIZE
        pnlSecret_Right.BackColor = If(Globals.Application.Debug, Color.Red, Color.Transparent)
        pnlSecret_Right.BorderStyle = If(Globals.Application.Debug, BorderStyle.FixedSingle, BorderStyle.None)
        pnlSecret_Right.BringToFront()
    End Sub

    Private Sub VerifyAdminPassword()
        If Globals.Application.AdminPasswordTyped.Equals(Globals.Application.AdminPassword) Then
            NamedPipe.Session.Client.SessionActivity("admin access granted")
            ShowAdminDialog()


            LoadSettings()

            RedrawControls()
            DrawDebugStuff()
        Else
            NamedPipe.Logger.Client.Warning("Invalid admin password!")
            NamedPipe.Session.Client.SessionActivity("admin access denied")
        End If
    End Sub


#Region "language stuff"
    Private Sub UpdateLanguageStringsHandler(bReloadTheme As Boolean)
        If Globals.Application.Languages.ActiveLanguageId = Localisation.GetActiveLanguageId Then
            Exit Sub
        End If


        Globals.Application.Languages.ActiveLanguageId = Localisation.GetActiveLanguageId
        Globals.Application.Languages.TermsFile = Localisation.GetTermsFile


        btnEmergencyClose.Text = Localisation.LoadString(100000) & vbCrLf & "(emergency exit)"
        btnTestTerms.Text = Localisation.LoadString(101) & vbCrLf & "(test terms)"


        RedrawLanguageFlag()


        If bReloadTheme Then webview2_Main.CoreWebView2.Reload()


        NamedPipe.Logger.Client.Debug("Updating language strings")
    End Sub
#End Region


#Region "dialogs & forms"
    Private Sub CloseAllSubForms()
        Dim lFormsToClose As New List(Of Form)

        For Each _frm As Form In Application.OpenForms
            If Not _frm.Name.Equals(Me.Name) Then
                THREADSAFE__DebugToTextBox("About to close: " & _frm.Name)
                lFormsToClose.Add(_frm)
            End If
        Next

        For Each _frm As Form In lFormsToClose
            Try
                THREADSAFE__DebugToTextBox("Closing: " & _frm.Name)
                _frm.Close()

                THREADSAFE__DebugToTextBox("Disposing: " & _frm.Name)
                _frm.Dispose()
            Catch ex As Exception
                THREADSAFE__DebugToTextBox("CloseAllSubForms exception: " & ex.Message)
            End Try
        Next
    End Sub

    Private Sub ShowHelpdesk()
        Globals.Application.Dialogs.IsDialogShown_Helpdesk = True

        _frmHelpdeskOverlay = New frmHelpdeskOverlay
        _frmHelpdeskOverlay.ShowDialog()
        _frmHelpdeskOverlay.Dispose()

        Globals.Application.Dialogs.IsDialogShown_Helpdesk = False
    End Sub

    Private Sub ShowAirlines()
        Globals.Application.Dialogs.IsDialogShown_Airlines = True

        _frmAirlinesOverlay = New frmAirlinesOverlay
        _frmAirlinesOverlay.ShowDialog()
        _frmAirlinesOverlay.Dispose()

        Globals.Application.Dialogs.IsDialogShown_Airlines = False
    End Sub

    Private Sub ShowOffice()
        Globals.Application.Dialogs.IsDialogShown_Office = True

        _frmOfficeOverlay = New frmOfficeOverlay
        _frmOfficeOverlay.ShowDialog()
        _frmOfficeOverlay.Dispose()

        Globals.Application.Dialogs.IsDialogShown_Office = False
    End Sub

    Private Sub ShowMenu_Languages()
        Globals.Application.Dialogs.IsDialogShown_Menu_Languages = True

        picLogout.Image = My.Resources.logout_bw_64x64__2__transparent

        Me.Enabled = False

        '_frmLanguages = New frmLanguages
        '_frmLanguages.ShowDialog(Me)
        '_frmLanguages.Dispose()
        _frmLanguagesOverlay = New frmLanguagesOverlay
        _frmLanguagesOverlay.ShowDialog()
        _frmLanguagesOverlay.Dispose()

        Me.Enabled = True

        picLogout.Image = My.Resources.logout_bw_64x64__2

        UpdateLanguageStringsHandler(True)

        Globals.Application.Dialogs.IsDialogShown_Menu_Languages = False
    End Sub

    Private Sub ShowLogoutDialog()
        Globals.Application.Dialogs.IsDialogShown_Logout = True

        _frmLogoutOverlay = New frmLogoutOverlay
        Dim dialogResult As DialogResult = _frmLogoutOverlay.ShowDialog()
        _frmLogoutOverlay.Dispose()


        If dialogResult = DialogResult.Yes Then
            NamedPipe.Session.Client.SessionEnd()
        Else
            NamedPipe.Session.Client.SessionLogoutRequestCancelled()
        End If

        Globals.Application.Dialogs.IsDialogShown_Logout = False
    End Sub

    Private Sub ShowGuestPasswordDialog()
        Globals.Application.Dialogs.IsDialogShown_GuestPassword = True

        _frmGuestPasswordOverlay = New frmGuestPasswordOverlay
        Dim dialogResult As DialogResult = _frmGuestPasswordOverlay.ShowDialog()
        _frmGuestPasswordOverlay.Dispose()


        If dialogResult = DialogResult.Yes Then
            NamedPipe.Session.Client.GuestPasswordAccepted()
            NamedPipe.Session.Client.SessionStart()
        ElseIf dialogResult = DialogResult.Abort Then
            SessionStuff.SessionAction.Reset()
            NamedPipe.Session.Client.SessionTermsCancelled()
            NamedPipe.Session.Client.GuestPasswordCancelled()
        ElseIf dialogResult = DialogResult.No Then
            SessionStuff.SessionAction.Reset()
            NamedPipe.Session.Client.SessionTermsCancelled()
            NamedPipe.Session.Client.GuestPasswordCancelled()
        Else
            SessionStuff.SessionAction.Reset()
            NamedPipe.Session.Client.SessionTermsCancelled()
            NamedPipe.Session.Client.GuestPasswordTimedout()
        End If

        Globals.Application.Dialogs.IsDialogShown_GuestPassword = False
    End Sub

    Private Sub ShowAdminPasswordDialog()
        Globals.Application.Dialogs.IsDialogShown_AdminPassword = True

        _frmAdminPasswordOverlay = New frmAdminPasswordOverlay
        _frmAdminPasswordOverlay.ShowDialog()
        _frmAdminPasswordOverlay.Dispose()

        Globals.Application.Dialogs.IsDialogShown_AdminPassword = False

        VerifyAdminPassword()
    End Sub

    Private Sub ShowAdminDialog()
        Globals.Application.Dialogs.IsDialogShown_Admin = True

        _frmAdminOverlay = New frmAdminOverlay
        _frmAdminOverlay.ShowDialog()
        _frmAdminOverlay.Dispose()

        Globals.Application.Dialogs.IsDialogShown_Admin = False
    End Sub

    Private Sub ShowLoadingOverlay(_opacity As Double)
        THREADSAFE__DebugToTextBox("ShowLoadingOverlay")
        Globals.Application.Dialogs.IsDialogShown_Loading = True

        mLoadingOverlayOpacity = _opacity

        'Helpers.Forms.TopMost(Me.Handle, False)

        _frmLoadingOverlay = New frmLoadingOverlay
        _frmLoadingOverlay.Opacity = _opacity
        _frmLoadingOverlay.ShowDialog(Me)
        _frmLoadingOverlay.Dispose()

        Globals.Application.Dialogs.IsDialogShown_Loading = False
    End Sub

    Private Sub CloseAllDialogs()
        NamedPipe.Logger.Client.Message("Closing all dialogs")


        Dim _frm2close As New List(Of Form)

        For Each _frm As Form In Application.OpenForms
            If _frm.Name = "frmMain" Then
                Continue For
            End If

            _frm2close.Add(_frm)
        Next

        For Each _frm In _frm2close
            Try
                NamedPipe.Logger.Client.Message("Closing dialog: " & _frm.Name)
                _frm.Dispose()
            Catch ex As Exception

            End Try
        Next
    End Sub

    Private Sub DisposeLoadingOverlay()
        THREADSAFE__DebugToTextBox("DisposeLoadingOverlay")
        _frmLoadingOverlay.Dispose()

        Helpers.Forms.TopMost(Me.Handle, True)

        SessionStateCheck(True)
    End Sub

    Private Sub ShowTerms()
        Globals.Application.Dialogs.IsDialogShown_Terms = True

        _frmTermsOverlay = New frmTermsOverlay
        Dim dialogResult As DialogResult = _frmTermsOverlay.ShowDialog(Me)
        _frmTermsOverlay.Dispose()

        If dialogResult = DialogResult.Yes Then
            NamedPipe.Session.Client.SessionTermsAccepted()

            If Globals.Application.GuestPasswordEnabled Then
                ShowGuestPasswordDialog()
            Else
                NamedPipe.Session.Client.SessionStart()
            End If
        ElseIf dialogResult = DialogResult.No Then
            SessionStuff.SessionAction.Reset()
            NamedPipe.Session.Client.SessionTermsDeclined()
        Else
            SessionStuff.SessionAction.Reset()
            NamedPipe.Session.Client.SessionTermsTimeout()
        End If

        Globals.Application.Dialogs.IsDialogShown_Terms = False
    End Sub

    Private Sub ShowCustom()
        Globals.Application.Dialogs.IsDialogShown_Custom = True

        _frmCustomOverlay = New frmCustomOverlay
        _frmCustomOverlay.ShowDialog()
        _frmCustomOverlay.Dispose()

        Globals.Application.Dialogs.IsDialogShown_Custom = False
    End Sub
#End Region


#Region "Events"
#Region "WebBrowserScriptInterface"
    Private Sub WebBrowserScriptInterface_OnCloseUI()
        'MessageBox.Show("WebBrowserScriptInterface_OnCloseUI")
        'ApplicationExit("Closed by UI")
    End Sub

    Private Sub WebBrowserScriptInterface_OnOpenPasswordDialog()
        'MessageBox.Show("WebBrowserScriptInterface_OnOpenPasswordDialog")
        ShowAdminPasswordDialog()
    End Sub
#End Region

    '=================================================================================================================================================================================

#Region "form"
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not NamedPipe.Session.Client.CommTest() Then
            MessageBox.Show("Backend communication failed", "FATAL ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Globals.Application.EmergencyExit = True
            ApplicationExit("fatal error - backend")
            Exit Sub
        End If


        '----------------------------------------------------------------------
        NamedPipe.Logger.Client.Message(New String("*", 50))
        NamedPipe.Logger.Client.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        NamedPipe.Logger.Client.Message("started")


        Console.WriteLine("frmMain_Load")
        Me.Visible = False

        Globals.Application.MainFormHandle = Me.Handle

        NamedPipe.System.Client.StartedHomepage()


        '----------------------
        CleanWebViewUserData()


        '----------------------
        Initialise()


        'webbrowser
        InitialiseMainWebView()
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        'Overlay
        ShowLoadingOverlay(1.0)


        Console.WriteLine("frmMain_Shown")
    End Sub

    Private Sub frmMain_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If Not Globals.Application.IsClosing Then
            If Not ApplicationExit("Closing event") Then
                NamedPipe.System.Client.ClosedHomepageBlocked()
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmMain_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Console.WriteLine("frmMain_Closed")
    End Sub

    Private Sub frmMain_Deactivate(sender As Object, e As EventArgs) Handles Me.Deactivate
        Console.WriteLine("frmMain_Deactivate")
    End Sub

    Private Sub frmMain_Activated(sender As Object, e As EventArgs) Handles Me.Activated

    End Sub

    Private Sub frmMain_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        'Console.WriteLine("frmMain_Resize")
    End Sub

    Private Sub frmMain_SizeChanged(sender As Object, e As EventArgs) Handles Me.SizeChanged
        'Console.WriteLine("frmMain_SizeChanged")
    End Sub

    Private Sub frmMain_ResizeBegin(sender As Object, e As EventArgs) Handles Me.ResizeBegin
        'Console.WriteLine("frmMain_ResizeBegin")
    End Sub

    Private Sub frmMain_ResizeEnd(sender As Object, e As EventArgs) Handles Me.ResizeEnd
        'Console.WriteLine("frmMain_ResizeEnd")
    End Sub
#End Region

    '=================================================================================================================================================================================

#Region "buttons & links"
    Private Sub btnEmergencyClose_Click(sender As Object, e As EventArgs) Handles btnEmergencyClose.Click
        Globals.Application.EmergencyExit = True
        ApplicationExit("emergency close")
    End Sub

    Private Sub btnTestTerms_Click(sender As Object, e As EventArgs) Handles btnTestTerms.Click
        ShowTerms()
    End Sub

    Private Sub btnForceUnlock_Click(sender As Object, e As EventArgs) Handles btnForceUnlock.Click
        Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes = 0
        Unlock(True)
    End Sub

    Private Sub picActiveLanguageFlag_Click(sender As Object, e As EventArgs) Handles picActiveLanguageFlag.Click
        ShowMenu_Languages()

        Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes = 0
        SessionStateCheck(True)
    End Sub

    Private Sub btnHelpdeskTest_Click(sender As Object, e As EventArgs) Handles btnHelpdeskTest.Click
        ShowHelpdesk()

        Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes = 0
        SessionStateCheck(True)
    End Sub

    Private Sub picLogout_Click(sender As Object, e As EventArgs) Handles picLogout.Click
        NamedPipe.Session.Client.SessionLogoutRequested()
    End Sub

    Private Sub linklabelInformation_Helpdesk_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles linklabelInformation_Helpdesk.LinkClicked
        ShowHelpdesk()

        Globals.Application.ForSomeReasonWeNeedToChangeLockStatusMultipleTimes = 0
        SessionStateCheck(True)
    End Sub

    Private Sub pnlSecret_Left_MouseClick(sender As Object, e As MouseEventArgs) Handles pnlSecret_Left.MouseClick
        If Not tmrSecret.Enabled Then
            tmrSecret.Enabled = True
        End If

        Globals.Secret.SECRET_BOX_LEFT_CLICKED = True

        THREADSAFE__DebugToTextBox("Secret click left")
    End Sub

    Private Sub pnlSecret_Right_MouseClick(sender As Object, e As MouseEventArgs) Handles pnlSecret_Right.MouseClick
        If Globals.Secret.SECRET_BOX_LEFT_CLICKED Then
            THREADSAFE__DebugToTextBox("Secret click right")

            ShowAdminPasswordDialog()
        End If
    End Sub
#End Region

    '=================================================================================================================================================================================

#Region "webMain"
    Private Sub corewebview2_Main_DOMContentLoaded(sender As Object, e As CoreWebView2DOMContentLoadedEventArgs) Handles corewebview2_Main.DOMContentLoaded
        mWebviewLoaded = True

        'HideLoadingOverlay()

        NamedPipe.Logger.Client.Debug("corewebview2_Main_DOMContentLoaded: " & webview2_Main.CoreWebView2.Source)

        If Globals.Security.PermittedScriptingHosts.Exists(webview2_Main.CoreWebView2.Source) Then
            NamedPipe.Logger.Client.Debug("Script interface permitted")
        Else
            NamedPipe.Logger.Client.Warning("Script interface not permitted from url: " & webview2_Main.CoreWebView2.Source)

            webview2_Main.CoreWebView2.RemoveHostObjectFromScript(Settings.Constants.Applications.Homepage.WebBrowserScriptInterface_Name)
        End If
    End Sub

    Private Sub corewebview2_Main_NewWindowRequested(sender As Object, e As CoreWebView2NewWindowRequestedEventArgs) Handles corewebview2_Main.NewWindowRequested
        e.Handled = True
    End Sub
#End Region

    '=================================================================================================================================================================================

#Region "process watcher"
    Private Sub _processWatcher_Event_DEBUG(DebugMessage As String) Handles _processWatcher.Event__DEBUG
        NamedPipe.Logger.Client.Debug("_processWatcher_Event_DEBUG", DebugMessage)
        THREADSAFE__DebugToTextBox("_processWatcher: " & DebugMessage & vbCrLf)
    End Sub

    Private Sub _processWatcher_Event__AppStarted_System(ProcessName As String, ProcessId As Integer, Owner As String) Handles _processWatcher.Event__AppStarted_System
        THREADSAFE__DebugToTextBox("AppStarted (System): " & ProcessName & " - " & ProcessId & " - " & Owner & vbCrLf)

        NamedPipe.Logger.Client.Message("(system) started '" & ProcessName & "'")
        NamedPipe.Session.Client.SessionActivity("started '" & ProcessName & "'")

        If Owner.Equals(Globals.Application.FullAccountName) Then
            NamedPipe.Logger.Client.Message(Environment.UserDomainName & "\" & Environment.UserName)
        End If

        If Not SessionStuff.IsSessionActive Then
            Helpers.Processes.KillProcess(ProcessId)
        End If
    End Sub

    Private Sub _processWatcher_Event__AppStarted_Homepage(ProcessName As String, ProcessId As Integer, Owner As String) Handles _processWatcher.Event__AppStarted_Homepage
        THREADSAFE__DebugToTextBox("AppStarted (Homepage): " & ProcessName & " - " & ProcessId & " - " & Owner & vbCrLf)

        NamedPipe.Logger.Client.Message("(homepage) started '" & ProcessName & "'")
        NamedPipe.Session.Client.SessionActivity("started '" & ProcessName & "'")

        If Not SessionStuff.IsSessionActive Then
            Helpers.Processes.KillProcess(ProcessId)
        End If
    End Sub

    Private Sub _processWatcher_Event__AppStarted_Unknown(ProcessName As String, ProcessId As Integer, Owner As String) Handles _processWatcher.Event__AppStarted_Unknown
        THREADSAFE__DebugToTextBox("AppStarted (unknown): " & ProcessName & " - " & ProcessId & " - " & Owner & vbCrLf)

        NamedPipe.Logger.Client.Message("(unknown) started '" & ProcessName & "'")
    End Sub

    Private Sub _processWatcher_Event_AppStoppedd(ProcessName As String, ProcessId As Integer) Handles _processWatcher.Event__AppStopped
        THREADSAFE__DebugToTextBox("AppStopped: " & ProcessName & " - " & ProcessId & vbCrLf)

        NamedPipe.Logger.Client.Message("started '" & ProcessName & "'")
        NamedPipe.Session.Client.SessionActivity("stopped '" & ProcessName & "'")
    End Sub

    Private Sub _processWatcher_Event_AppStartedException(ErrorMessage As String) Handles _processWatcher.Event__AppStarted_Exception
        NamedPipe.Logger.Client.Error("ProcessWatcher", "Event_AppStartedException: " & ErrorMessage)
    End Sub

    Private Sub _processWatcher_Event_AppStoppedException(ErrorMessage As String) Handles _processWatcher.Event__AppStopped_Exception
        NamedPipe.Logger.Client.Error("ProcessWatcher", "Event_AppStoppedException: " & ErrorMessage)
    End Sub
#End Region

    '=================================================================================================================================================================================

#Region "timers"
    Private Sub tmrLoadingOverlayFadeout_Tick(sender As Object, e As EventArgs) Handles tmrLoadingOverlayFadeout.Tick
        THREADSAFE__DebugToTextBox("tmrLoadingOverlayFadeout.Tick - " & mLoadingOverlayOpacity)
        mLoadingOverlayOpacity -= mLoadingOverlayOpacityFadeoutSpeed
        If mLoadingOverlayOpacity <= 0 Then
            tmrLoadingOverlayFadeout.Enabled = False
            DisposeLoadingOverlay()
            Exit Sub
        End If

        _frmLoadingOverlay.Opacity = mLoadingOverlayOpacity
    End Sub

    Private Sub tmrSessionState_Tick(sender As Object, e As EventArgs) Handles tmrSessionState.Tick
        SessionStateCheck()
    End Sub

    Private Sub tmrDesktopCheck_Tick(sender As Object, e As EventArgs) Handles tmrDesktopCheck.Tick
        Dim oForegroundWindow As ForegroundWindow = ForegroundWindowHandler__GetDetails()

        If oForegroundWindow.Title.Equals("") And oForegroundWindow.ClassName.Equals("WorkerW") Then
            NamedPipe.Logger.Client.Debug("In desktop")

            ForegroundWindowHandler__SetForegroundWindow(Me.Handle)
        End If
    End Sub

    Private Sub tmrLoadingOverlayDelay_Tick(sender As Object, e As EventArgs) Handles tmrLoadingOverlayDelay.Tick
        THREADSAFE__DebugToTextBox("tmrLoadingOverlayDelay.Tick: mLoadingOverlayFakeDelay=" & mLoadingOverlayFakeDelay & " ; mWebviewLoaded=" & mWebviewLoaded)

        mLoadingOverlayFakeDelay -= 1
        If mLoadingOverlayFakeDelay < 0 And mWebviewLoaded Then
            Me.Visible = True

            tmrLoadingOverlayDelay.Enabled = False
            tmrLoadingOverlayFadeout.Enabled = True
        End If
    End Sub

    Private Sub tmrSecret_Tick(sender As Object, e As EventArgs) Handles tmrSecret.Tick
        Globals.Secret.SECRET_BOX_LEFT_CLICKED = False
        tmrSecret.Enabled = False
    End Sub
#End Region
#End Region
End Class
