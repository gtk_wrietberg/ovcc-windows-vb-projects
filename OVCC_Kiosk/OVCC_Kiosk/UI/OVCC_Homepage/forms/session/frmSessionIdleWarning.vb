﻿Imports System.ComponentModel

Public Class frmSessionIdleWarning
    Private ReadOnly BORDER_WIDTH As Integer = 3

    Private mCountDownSpeed As Long = 10
    Private mCountDownValue As Long = (Globals.Application.SessionTimeoutWarningDuration + 1) * mCountDownSpeed


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


#Region "Thread Safe stuff"
    Delegate Sub CountdownvalueCallback()

    Public Sub THREADSAFE__Countdownvalue()
        If Me.lblCountdownValue.InvokeRequired Then
            Dim d As New CountdownvalueCallback(AddressOf THREADSAFE__Countdownvalue)
            Me.Invoke(d, New Object() {})
        Else
            Dim s As String
            Dim l As Long

            l = CLng(mCountDownValue / mCountDownSpeed)

            If l = 1 Then
                s = Localisation.LoadString(1001)
            Else
                s = Localisation.LoadString(1000)
            End If
            s = s.Replace("??", l.ToString)

            Me.lblCountdownValue.Text = s


            progressPanelAutoclose.Value = mCountDownValue
        End If
    End Sub
#End Region

    Private Sub frmSessionActivityWarning_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime
        Me.Opacity = 1.0

        'Me.ClientSize = New Size(pnlMain.Width + (2 * pnlMain.Left), pnlMain.Height + (2 * pnlMain.Top))
        'Me.Left = (frmMain.Width - Me.Width) / 2
        'Me.Top = (frmMain.Height - Me.Height) / 2

        pnlMain.Left = (Me.Width - pnlMain.Width) / 2
        pnlMain.Top = (Me.Height - pnlMain.Height) / 2

        pnlBorder.Left = pnlMain.Left - BORDER_WIDTH
        pnlBorder.Top = pnlMain.Top - BORDER_WIDTH
        pnlBorder.Width = pnlMain.Width + 2 * BORDER_WIDTH
        pnlBorder.Height = pnlMain.Height + 2 * BORDER_WIDTH
        pnlBorder.SendToBack()


        lblWarning.Text = Localisation.LoadString(1020002)

        btnEmergencyClose.Enabled = Globals.Application.Debug
        btnEmergencyClose.Visible = Globals.Application.Debug


        progressPanelAutoclose.BackgroundColor = Color.Transparent
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.ProgressBarColor = Color.Black
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.AnimationSpeed = 10
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.ValueMin = 0
        progressPanelAutoclose.ValueMax = mCountDownValue
        progressPanelAutoclose.Value = mCountDownValue
        progressPanelAutoclose.Initialise()


        '-----------------------------------
        bgworker_CheckIdle.RunWorkerAsync()
    End Sub

    Private Sub bgworker_CheckIdle_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworker_CheckIdle.DoWork
        Do
            Dim sVoid As String = ""
            Dim lIdle As Long = InputCheck.SystemIdleTime(sVoid)

            If lIdle < Globals.Application.SessionTimeout Then
                e.Result = "activity"
                Exit Do
            End If

            mCountDownValue -= 1
            If mCountDownValue <= 0 Then
                e.Result = "timeout"
                Exit Do
            End If

            THREADSAFE__Countdownvalue()

            Threading.Thread.Sleep(1000 / mCountDownSpeed)
        Loop
    End Sub

    Private Sub bgworker_CheckIdle_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworker_CheckIdle.RunWorkerCompleted
        Me.Close()
    End Sub

    Private Sub btnEmergencyClose_Click(sender As Object, e As EventArgs) Handles btnEmergencyClose.Click
        Me.Close()
    End Sub
End Class