﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSessionIdleWarning
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        pnlBorder = New Panel()
        pnlMain = New Panel()
        lblCountdownValue = New Label()
        lblWarning = New Label()
        btnEmergencyClose = New Button()
        bgworker_CheckIdle = New ComponentModel.BackgroundWorker()
        progressPanelAutoclose = New ProgressPanel()
        pnlMain.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlBorder
        ' 
        pnlBorder.BackColor = Color.DarkRed
        pnlBorder.Location = New Point(370, 24)
        pnlBorder.Name = "pnlBorder"
        pnlBorder.Size = New Size(287, 62)
        pnlBorder.TabIndex = 0
        ' 
        ' pnlMain
        ' 
        pnlMain.BackColor = Color.Red
        pnlMain.BorderStyle = BorderStyle.FixedSingle
        pnlMain.Controls.Add(lblCountdownValue)
        pnlMain.Controls.Add(progressPanelAutoclose)
        pnlMain.Controls.Add(lblWarning)
        pnlMain.Location = New Point(12, 122)
        pnlMain.Name = "pnlMain"
        pnlMain.Size = New Size(1056, 143)
        pnlMain.TabIndex = 1
        ' 
        ' lblCountdownValue
        ' 
        lblCountdownValue.BackColor = Color.Transparent
        lblCountdownValue.Dock = DockStyle.Bottom
        lblCountdownValue.Font = New Font("Tahoma", 9.75F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblCountdownValue.ForeColor = Color.White
        lblCountdownValue.Location = New Point(0, 114)
        lblCountdownValue.Name = "lblCountdownValue"
        lblCountdownValue.Size = New Size(1054, 23)
        lblCountdownValue.TabIndex = 1
        lblCountdownValue.Text = "(closes in ?? seconds)"
        lblCountdownValue.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' lblWarning
        ' 
        lblWarning.BackColor = Color.Transparent
        lblWarning.Dock = DockStyle.Top
        lblWarning.Font = New Font("Tahoma", 15.75F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        lblWarning.ForeColor = Color.White
        lblWarning.Location = New Point(0, 0)
        lblWarning.Name = "lblWarning"
        lblWarning.Size = New Size(1054, 130)
        lblWarning.TabIndex = 0
        lblWarning.Text = "The session will be logged out automatically soon."
        lblWarning.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' btnEmergencyClose
        ' 
        btnEmergencyClose.Location = New Point(13, 12)
        btnEmergencyClose.Name = "btnEmergencyClose"
        btnEmergencyClose.Size = New Size(236, 30)
        btnEmergencyClose.TabIndex = 2
        btnEmergencyClose.Text = "emergency exit"
        btnEmergencyClose.UseVisualStyleBackColor = True
        ' 
        ' bgworker_CheckIdle
        ' 
        bgworker_CheckIdle.WorkerReportsProgress = True
        bgworker_CheckIdle.WorkerSupportsCancellation = True
        ' 
        ' progressPanelAutoclose
        ' 
        progressPanelAutoclose.AnimationSpeed = 50R
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.BackColor = Color.IndianRed
        progressPanelAutoclose.BackgroundColor = Color.Empty
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.Dock = DockStyle.Bottom
        progressPanelAutoclose.Location = New Point(0, 137)
        progressPanelAutoclose.Name = "progressPanelAutoclose"
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.ProgressBarColor = Color.Empty
        progressPanelAutoclose.Size = New Size(1054, 4)
        progressPanelAutoclose.TabIndex = 3
        progressPanelAutoclose.Value = 0R
        progressPanelAutoclose.ValueMax = 100R
        progressPanelAutoclose.ValueMin = 0R
        ' 
        ' frmSessionIdleWarning
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(1080, 395)
        ControlBox = False
        Controls.Add(btnEmergencyClose)
        Controls.Add(pnlMain)
        Controls.Add(pnlBorder)
        Font = New Font("Tahoma", 9F)
        FormBorderStyle = FormBorderStyle.None
        MinimizeBox = False
        Name = "frmSessionIdleWarning"
        StartPosition = FormStartPosition.CenterScreen
        Text = "Idle warning"
        pnlMain.ResumeLayout(False)
        ResumeLayout(False)
    End Sub

    Friend WithEvents pnlBorder As Panel
    Friend WithEvents pnlMain As Panel
    Friend WithEvents lblWarning As Label
    Friend WithEvents bgworker_CheckIdle As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblCountdownValue As Label
    Friend WithEvents btnEmergencyClose As Button
    Friend WithEvents progressPanelAutoclose As ProgressPanel
End Class
