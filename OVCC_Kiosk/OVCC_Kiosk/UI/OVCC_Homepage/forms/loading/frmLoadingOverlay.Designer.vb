﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmLoadingOverlay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoadingOverlay))
        btnEmergencyClose = New Button()
        lblLoadingVersion = New Label()
        pnlLoading = New Panel()
        picLogo = New PictureBox()
        lblLoading = New Label()
        progressPanelLoading = New ProgressPanel()
        pnlLoadingBorder = New Panel()
        pnlLoading.SuspendLayout()
        CType(picLogo, ComponentModel.ISupportInitialize).BeginInit()
        SuspendLayout()
        ' 
        ' btnEmergencyClose
        ' 
        btnEmergencyClose.Location = New Point(12, 12)
        btnEmergencyClose.Name = "btnEmergencyClose"
        btnEmergencyClose.Size = New Size(78, 58)
        btnEmergencyClose.TabIndex = 3
        btnEmergencyClose.Text = "emergency close button"
        btnEmergencyClose.UseVisualStyleBackColor = True
        ' 
        ' lblLoadingVersion
        ' 
        lblLoadingVersion.BackColor = Color.Gainsboro
        lblLoadingVersion.Dock = DockStyle.Bottom
        lblLoadingVersion.Font = New Font("Segoe UI", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblLoadingVersion.Location = New Point(0, 116)
        lblLoadingVersion.Name = "lblLoadingVersion"
        lblLoadingVersion.Size = New Size(560, 19)
        lblLoadingVersion.TabIndex = 6
        lblLoadingVersion.Text = "version number goes here"
        lblLoadingVersion.TextAlign = ContentAlignment.MiddleRight
        ' 
        ' pnlLoading
        ' 
        pnlLoading.BackColor = Color.WhiteSmoke
        pnlLoading.BorderStyle = BorderStyle.FixedSingle
        pnlLoading.Controls.Add(picLogo)
        pnlLoading.Controls.Add(lblLoading)
        pnlLoading.Controls.Add(progressPanelLoading)
        pnlLoading.Controls.Add(lblLoadingVersion)
        pnlLoading.Location = New Point(191, 120)
        pnlLoading.Name = "pnlLoading"
        pnlLoading.Size = New Size(562, 137)
        pnlLoading.TabIndex = 7
        ' 
        ' picLogo
        ' 
        picLogo.Dock = DockStyle.Top
        picLogo.Image = My.Resources.Resources.GuestTek_Header_Logo
        picLogo.Location = New Point(0, 0)
        picLogo.Name = "picLogo"
        picLogo.Size = New Size(560, 87)
        picLogo.SizeMode = PictureBoxSizeMode.CenterImage
        picLogo.TabIndex = 8
        picLogo.TabStop = False
        ' 
        ' lblLoading
        ' 
        lblLoading.Dock = DockStyle.Bottom
        lblLoading.Font = New Font("Segoe UI", 8.25F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        lblLoading.Location = New Point(0, 91)
        lblLoading.Name = "lblLoading"
        lblLoading.Size = New Size(560, 19)
        lblLoading.TabIndex = 8
        lblLoading.Text = "Loading, please wait"
        lblLoading.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' progressPanelLoading
        ' 
        progressPanelLoading.AnimationSpeed = 50R
        progressPanelLoading.AnimationType = ProgressPanel.ANIMATION_TYPES.KITT
        progressPanelLoading.AnimationWidth = 25
        progressPanelLoading.BackColor = Color.Transparent
        progressPanelLoading.BackgroundColor = Color.Empty
        progressPanelLoading.Border = False
        progressPanelLoading.Dock = DockStyle.Bottom
        progressPanelLoading.Location = New Point(0, 110)
        progressPanelLoading.Name = "progressPanelLoading"
        progressPanelLoading.ProgressBarBorder = False
        progressPanelLoading.ProgressBarColor = Color.Empty
        progressPanelLoading.Size = New Size(560, 6)
        progressPanelLoading.TabIndex = 7
        progressPanelLoading.Value = 0R
        progressPanelLoading.ValueMax = 100R
        progressPanelLoading.ValueMin = 0R
        ' 
        ' pnlLoadingBorder
        ' 
        pnlLoadingBorder.BackColor = Color.FromArgb(CByte(140), CByte(140), CByte(140))
        pnlLoadingBorder.Location = New Point(435, 358)
        pnlLoadingBorder.Name = "pnlLoadingBorder"
        pnlLoadingBorder.Size = New Size(185, 82)
        pnlLoadingBorder.TabIndex = 8
        ' 
        ' frmLoadingOverlay
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Black
        ClientSize = New Size(933, 519)
        ControlBox = False
        Controls.Add(pnlLoading)
        Controls.Add(btnEmergencyClose)
        Controls.Add(pnlLoadingBorder)
        Font = New Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        Icon = CType(resources.GetObject("$this.Icon"), Icon)
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmLoadingOverlay"
        Text = "frmLoadingOverlay"
        TransparencyKey = Color.Maroon
        pnlLoading.ResumeLayout(False)
        CType(picLogo, ComponentModel.ISupportInitialize).EndInit()
        ResumeLayout(False)

    End Sub
    Friend WithEvents btnEmergencyClose As Button
    Friend WithEvents progressLoading As ProgressBar
    Friend WithEvents lblLoadingVersion As Label
    Friend WithEvents pnlLoading As Panel
    Friend WithEvents lblLoading As Label
    Friend WithEvents picLogo As PictureBox
    Friend WithEvents pnlLoadingBorder As Panel
    Friend WithEvents progressPanelLoading As ProgressPanel
End Class
