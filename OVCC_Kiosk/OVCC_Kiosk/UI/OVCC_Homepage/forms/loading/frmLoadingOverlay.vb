﻿Public Class frmLoadingOverlay
    Private ReadOnly BORDER_WIDTH As Integer = 3


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmOverlay_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Set fullscreen etc.
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized
        Me.Location = New Point(0, 0)
        Me.Size = SystemInformation.PrimaryMonitorSize
        Me.BackColor = Color.Black
        Helpers.Forms.TopMost(Me.Handle, Not Globals.Application.IsDevMachineOrDebugging)


        'Emergency close
        btnEmergencyClose.Top = 3
        btnEmergencyClose.Left = Me.Width - btnEmergencyClose.Width - 3
        btnEmergencyClose.Visible = Globals.Application.IsDevMachineOrDebugging
        btnEmergencyClose.Enabled = Globals.Application.IsDevMachineOrDebugging
        btnEmergencyClose.BringToFront()


        'loading
        lblLoadingVersion.Text = My.Application.Info.Version.ToString
        pnlLoading.Top = (Me.Height - pnlLoading.Height) / 2
        pnlLoading.Left = (Me.Width - pnlLoading.Width) / 2
        pnlLoading.BringToFront()


        'loading border
        pnlLoadingBorder.Top = pnlLoading.Top - BORDER_WIDTH
        pnlLoadingBorder.Left = pnlLoading.Left - BORDER_WIDTH
        pnlLoadingBorder.Width = pnlLoading.Width + 2 * BORDER_WIDTH
        pnlLoadingBorder.Height = pnlLoading.Height + 2 * BORDER_WIDTH


        'custom progress bar
        progressPanelLoading.BackgroundColor = Color.Transparent
        progressPanelLoading.Border = False
        progressPanelLoading.ProgressBarColor = Color.Green
        progressPanelLoading.ProgressBarBorder = False
        progressPanelLoading.AnimationSpeed = 12
        progressPanelLoading.AnimationWidth = 20
        progressPanelLoading.AnimationType = ProgressPanel.ANIMATION_TYPES.KITT
        progressPanelLoading.Initialise()
        progressPanelLoading.StartAnimation()


        '----------------------
        lblLoading.Text = Localisation.LoadString(1100021)
    End Sub


    Private Sub btnEmergencyClose_Click(sender As Object, e As EventArgs) Handles btnEmergencyClose.Click
        Me.Close()
    End Sub

    Private Sub frmLoadingOverlay_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        frmMain.Visible = True
    End Sub
End Class