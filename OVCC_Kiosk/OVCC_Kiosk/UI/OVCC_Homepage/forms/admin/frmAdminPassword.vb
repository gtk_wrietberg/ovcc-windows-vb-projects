﻿Public Class frmAdminPassword
    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmPassword_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime

        Helpers.Forms.TopMost(Me.Handle, True)

        'tmrAutoClose.Enabled = True

        txtPassword.Text = ""
    End Sub

    Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPassword.KeyDown
        If e.KeyCode = Keys.Enter Then
            Globals.Application.AdminPasswordTyped = Trim(txtPassword.Text)
            DialogResult = DialogResult.Yes
            Me.Close()
        ElseIf e.KeyCode = Keys.Escape Then
            Globals.Application.AdminPasswordTyped = ""
            DialogResult = DialogResult.No
            Me.Close()
        End If
    End Sub

    Private Sub tmrAutoClose_Tick(sender As Object, e As EventArgs) Handles tmrAutoClose.Tick
        DialogResult = DialogResult.No
        Me.Close()
    End Sub
End Class