﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdmin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        btnCancel = New Button()
        groupbox_Applications = New GroupBox()
        lblStatus_Application_Logout = New Label()
        picStatus_Application_Logout = New PictureBox()
        lblStatus_Application_Notification = New Label()
        picStatus_Application_Notification = New PictureBox()
        groupbox_Services = New GroupBox()
        lblStatus_Service_Watchdog = New Label()
        picStatus_Service_Plumber = New PictureBox()
        picStatus_Service_Watchdog = New PictureBox()
        lblStatus_Service_Plumber = New Label()
        pnlAdminOptions = New Panel()
        btnSave = New Button()
        groupbox_UI = New GroupBox()
        chkDebug = New CheckBox()
        pnlMain = New Panel()
        pnlSystemStatus = New Panel()
        lblSystemStatus = New Label()
        lblAdminOptions = New Label()
        bgworker_CheckSystem = New ComponentModel.BackgroundWorker()
        pnlShadow = New Panel()
        groupbox_AutoStart = New GroupBox()
        btnAutoStart_Stop = New Button()
        btnAutoStart_Reboot = New Button()
        pnlSystemStatusContainer = New Panel()
        pnlAdminOptionsContainer = New Panel()
        groupbox_Applications.SuspendLayout()
        CType(picStatus_Application_Logout, ComponentModel.ISupportInitialize).BeginInit()
        CType(picStatus_Application_Notification, ComponentModel.ISupportInitialize).BeginInit()
        groupbox_Services.SuspendLayout()
        CType(picStatus_Service_Plumber, ComponentModel.ISupportInitialize).BeginInit()
        CType(picStatus_Service_Watchdog, ComponentModel.ISupportInitialize).BeginInit()
        pnlAdminOptions.SuspendLayout()
        groupbox_UI.SuspendLayout()
        pnlMain.SuspendLayout()
        pnlSystemStatus.SuspendLayout()
        groupbox_AutoStart.SuspendLayout()
        pnlSystemStatusContainer.SuspendLayout()
        pnlAdminOptionsContainer.SuspendLayout()
        SuspendLayout()
        ' 
        ' btnCancel
        ' 
        btnCancel.Location = New Point(195, 199)
        btnCancel.Name = "btnCancel"
        btnCancel.Size = New Size(186, 38)
        btnCancel.TabIndex = 0
        btnCancel.Text = "Cancel"
        btnCancel.UseVisualStyleBackColor = True
        ' 
        ' groupbox_Applications
        ' 
        groupbox_Applications.Controls.Add(lblStatus_Application_Logout)
        groupbox_Applications.Controls.Add(picStatus_Application_Logout)
        groupbox_Applications.Controls.Add(lblStatus_Application_Notification)
        groupbox_Applications.Controls.Add(picStatus_Application_Notification)
        groupbox_Applications.Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        groupbox_Applications.Location = New Point(195, 3)
        groupbox_Applications.Name = "groupbox_Applications"
        groupbox_Applications.Size = New Size(186, 63)
        groupbox_Applications.TabIndex = 6
        groupbox_Applications.TabStop = False
        groupbox_Applications.Text = "Applications"
        ' 
        ' lblStatus_Application_Logout
        ' 
        lblStatus_Application_Logout.BackColor = Color.WhiteSmoke
        lblStatus_Application_Logout.Font = New Font("Tahoma", 8.25F, FontStyle.Bold)
        lblStatus_Application_Logout.Location = New Point(6, 39)
        lblStatus_Application_Logout.Name = "lblStatus_Application_Logout"
        lblStatus_Application_Logout.Size = New Size(156, 18)
        lblStatus_Application_Logout.TabIndex = 7
        lblStatus_Application_Logout.Text = "Logout shortcut"
        lblStatus_Application_Logout.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' picStatus_Application_Logout
        ' 
        picStatus_Application_Logout.Image = My.Resources.Resources.status_nok__small
        picStatus_Application_Logout.Location = New Point(163, 39)
        picStatus_Application_Logout.Name = "picStatus_Application_Logout"
        picStatus_Application_Logout.Size = New Size(18, 18)
        picStatus_Application_Logout.SizeMode = PictureBoxSizeMode.StretchImage
        picStatus_Application_Logout.TabIndex = 6
        picStatus_Application_Logout.TabStop = False
        ' 
        ' lblStatus_Application_Notification
        ' 
        lblStatus_Application_Notification.BackColor = Color.WhiteSmoke
        lblStatus_Application_Notification.Font = New Font("Tahoma", 8.25F, FontStyle.Bold)
        lblStatus_Application_Notification.Location = New Point(6, 18)
        lblStatus_Application_Notification.Name = "lblStatus_Application_Notification"
        lblStatus_Application_Notification.Size = New Size(156, 18)
        lblStatus_Application_Notification.TabIndex = 5
        lblStatus_Application_Notification.Text = "Notification"
        lblStatus_Application_Notification.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' picStatus_Application_Notification
        ' 
        picStatus_Application_Notification.Image = My.Resources.Resources.status_nok__small
        picStatus_Application_Notification.Location = New Point(163, 18)
        picStatus_Application_Notification.Name = "picStatus_Application_Notification"
        picStatus_Application_Notification.Size = New Size(18, 18)
        picStatus_Application_Notification.SizeMode = PictureBoxSizeMode.StretchImage
        picStatus_Application_Notification.TabIndex = 4
        picStatus_Application_Notification.TabStop = False
        ' 
        ' groupbox_Services
        ' 
        groupbox_Services.Controls.Add(lblStatus_Service_Watchdog)
        groupbox_Services.Controls.Add(picStatus_Service_Plumber)
        groupbox_Services.Controls.Add(picStatus_Service_Watchdog)
        groupbox_Services.Controls.Add(lblStatus_Service_Plumber)
        groupbox_Services.Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        groupbox_Services.Location = New Point(3, 3)
        groupbox_Services.Name = "groupbox_Services"
        groupbox_Services.Size = New Size(186, 63)
        groupbox_Services.TabIndex = 4
        groupbox_Services.TabStop = False
        groupbox_Services.Text = "Services"
        ' 
        ' lblStatus_Service_Watchdog
        ' 
        lblStatus_Service_Watchdog.BackColor = Color.WhiteSmoke
        lblStatus_Service_Watchdog.Font = New Font("Tahoma", 8.25F, FontStyle.Bold)
        lblStatus_Service_Watchdog.Location = New Point(6, 39)
        lblStatus_Service_Watchdog.Name = "lblStatus_Service_Watchdog"
        lblStatus_Service_Watchdog.Size = New Size(156, 18)
        lblStatus_Service_Watchdog.TabIndex = 5
        lblStatus_Service_Watchdog.Text = "Watchdog"
        lblStatus_Service_Watchdog.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' picStatus_Service_Plumber
        ' 
        picStatus_Service_Plumber.Image = My.Resources.Resources.status_ok__small
        picStatus_Service_Plumber.Location = New Point(163, 18)
        picStatus_Service_Plumber.Name = "picStatus_Service_Plumber"
        picStatus_Service_Plumber.Size = New Size(18, 18)
        picStatus_Service_Plumber.SizeMode = PictureBoxSizeMode.StretchImage
        picStatus_Service_Plumber.TabIndex = 0
        picStatus_Service_Plumber.TabStop = False
        ' 
        ' picStatus_Service_Watchdog
        ' 
        picStatus_Service_Watchdog.Image = My.Resources.Resources.status_nok__small
        picStatus_Service_Watchdog.Location = New Point(163, 39)
        picStatus_Service_Watchdog.Name = "picStatus_Service_Watchdog"
        picStatus_Service_Watchdog.Size = New Size(18, 18)
        picStatus_Service_Watchdog.SizeMode = PictureBoxSizeMode.StretchImage
        picStatus_Service_Watchdog.TabIndex = 4
        picStatus_Service_Watchdog.TabStop = False
        ' 
        ' lblStatus_Service_Plumber
        ' 
        lblStatus_Service_Plumber.BackColor = Color.WhiteSmoke
        lblStatus_Service_Plumber.Font = New Font("Tahoma", 8.25F, FontStyle.Bold)
        lblStatus_Service_Plumber.Location = New Point(6, 18)
        lblStatus_Service_Plumber.Name = "lblStatus_Service_Plumber"
        lblStatus_Service_Plumber.Size = New Size(156, 18)
        lblStatus_Service_Plumber.TabIndex = 3
        lblStatus_Service_Plumber.Text = "Plumber"
        lblStatus_Service_Plumber.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' pnlAdminOptions
        ' 
        pnlAdminOptions.BackColor = Color.White
        pnlAdminOptions.BorderStyle = BorderStyle.FixedSingle
        pnlAdminOptions.Controls.Add(groupbox_AutoStart)
        pnlAdminOptions.Controls.Add(btnSave)
        pnlAdminOptions.Controls.Add(groupbox_UI)
        pnlAdminOptions.Controls.Add(btnCancel)
        pnlAdminOptions.Location = New Point(4, 40)
        pnlAdminOptions.Name = "pnlAdminOptions"
        pnlAdminOptions.Size = New Size(386, 242)
        pnlAdminOptions.TabIndex = 3
        ' 
        ' btnSave
        ' 
        btnSave.Location = New Point(3, 199)
        btnSave.Name = "btnSave"
        btnSave.Size = New Size(186, 38)
        btnSave.TabIndex = 3
        btnSave.Text = "Save"
        btnSave.UseVisualStyleBackColor = True
        ' 
        ' groupbox_UI
        ' 
        groupbox_UI.Controls.Add(chkDebug)
        groupbox_UI.Location = New Point(2, 3)
        groupbox_UI.Name = "groupbox_UI"
        groupbox_UI.Size = New Size(186, 42)
        groupbox_UI.TabIndex = 2
        groupbox_UI.TabStop = False
        groupbox_UI.Text = "UI options"
        ' 
        ' chkDebug
        ' 
        chkDebug.AutoSize = True
        chkDebug.Font = New Font("Tahoma", 8.25F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        chkDebug.Location = New Point(6, 21)
        chkDebug.Name = "chkDebug"
        chkDebug.Size = New Size(97, 17)
        chkDebug.TabIndex = 1
        chkDebug.Text = "Debug mode"
        chkDebug.UseVisualStyleBackColor = True
        ' 
        ' pnlMain
        ' 
        pnlMain.BackColor = Color.Gainsboro
        pnlMain.BorderStyle = BorderStyle.FixedSingle
        pnlMain.Controls.Add(pnlAdminOptionsContainer)
        pnlMain.Controls.Add(pnlSystemStatusContainer)
        pnlMain.Location = New Point(12, 12)
        pnlMain.Name = "pnlMain"
        pnlMain.Size = New Size(396, 389)
        pnlMain.TabIndex = 4
        ' 
        ' pnlSystemStatus
        ' 
        pnlSystemStatus.BackColor = Color.White
        pnlSystemStatus.BorderStyle = BorderStyle.FixedSingle
        pnlSystemStatus.Controls.Add(groupbox_Applications)
        pnlSystemStatus.Controls.Add(groupbox_Services)
        pnlSystemStatus.Location = New Point(4, 28)
        pnlSystemStatus.Name = "pnlSystemStatus"
        pnlSystemStatus.Size = New Size(386, 70)
        pnlSystemStatus.TabIndex = 3
        ' 
        ' lblSystemStatus
        ' 
        lblSystemStatus.BackColor = Color.Transparent
        lblSystemStatus.Font = New Font("Tahoma", 15.75F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblSystemStatus.ForeColor = Color.Black
        lblSystemStatus.Location = New Point(4, 0)
        lblSystemStatus.Name = "lblSystemStatus"
        lblSystemStatus.Size = New Size(386, 25)
        lblSystemStatus.TabIndex = 5
        lblSystemStatus.Text = "System status"
        lblSystemStatus.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' lblAdminOptions
        ' 
        lblAdminOptions.BackColor = Color.Transparent
        lblAdminOptions.Font = New Font("Tahoma", 15.75F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblAdminOptions.ForeColor = Color.Black
        lblAdminOptions.Location = New Point(4, 0)
        lblAdminOptions.Name = "lblAdminOptions"
        lblAdminOptions.Size = New Size(384, 37)
        lblAdminOptions.TabIndex = 4
        lblAdminOptions.Text = "Admin options"
        lblAdminOptions.TextAlign = ContentAlignment.BottomCenter
        ' 
        ' bgworker_CheckSystem
        ' 
        bgworker_CheckSystem.WorkerReportsProgress = True
        bgworker_CheckSystem.WorkerSupportsCancellation = True
        ' 
        ' pnlShadow
        ' 
        pnlShadow.BackColor = Color.Black
        pnlShadow.Location = New Point(949, 513)
        pnlShadow.Name = "pnlShadow"
        pnlShadow.Size = New Size(91, 65)
        pnlShadow.TabIndex = 5
        ' 
        ' groupbox_AutoStart
        ' 
        groupbox_AutoStart.Controls.Add(btnAutoStart_Reboot)
        groupbox_AutoStart.Controls.Add(btnAutoStart_Stop)
        groupbox_AutoStart.Location = New Point(195, 3)
        groupbox_AutoStart.Name = "groupbox_AutoStart"
        groupbox_AutoStart.Size = New Size(186, 80)
        groupbox_AutoStart.TabIndex = 3
        groupbox_AutoStart.TabStop = False
        groupbox_AutoStart.Text = "Auto-start options"
        ' 
        ' btnAutoStart_Stop
        ' 
        btnAutoStart_Stop.Location = New Point(6, 48)
        btnAutoStart_Stop.Name = "btnAutoStart_Stop"
        btnAutoStart_Stop.Size = New Size(175, 24)
        btnAutoStart_Stop.TabIndex = 6
        btnAutoStart_Stop.Text = "Stop auto-start and logoff"
        btnAutoStart_Stop.UseVisualStyleBackColor = True
        ' 
        ' btnAutoStart_Reboot
        ' 
        btnAutoStart_Reboot.Location = New Point(6, 18)
        btnAutoStart_Reboot.Name = "btnAutoStart_Reboot"
        btnAutoStart_Reboot.Size = New Size(175, 24)
        btnAutoStart_Reboot.TabIndex = 7
        btnAutoStart_Reboot.Text = "Reboot machine"
        btnAutoStart_Reboot.UseVisualStyleBackColor = True
        ' 
        ' pnlSystemStatusContainer
        ' 
        pnlSystemStatusContainer.Controls.Add(pnlSystemStatus)
        pnlSystemStatusContainer.Controls.Add(lblSystemStatus)
        pnlSystemStatusContainer.Dock = DockStyle.Top
        pnlSystemStatusContainer.Location = New Point(0, 0)
        pnlSystemStatusContainer.Name = "pnlSystemStatusContainer"
        pnlSystemStatusContainer.Size = New Size(394, 102)
        pnlSystemStatusContainer.TabIndex = 6
        ' 
        ' pnlAdminOptionsContainer
        ' 
        pnlAdminOptionsContainer.Controls.Add(pnlAdminOptions)
        pnlAdminOptionsContainer.Controls.Add(lblAdminOptions)
        pnlAdminOptionsContainer.Dock = DockStyle.Bottom
        pnlAdminOptionsContainer.Location = New Point(0, 101)
        pnlAdminOptionsContainer.Name = "pnlAdminOptionsContainer"
        pnlAdminOptionsContainer.Size = New Size(394, 286)
        pnlAdminOptionsContainer.TabIndex = 6
        ' 
        ' frmAdmin
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(1099, 609)
        Controls.Add(pnlMain)
        Controls.Add(pnlShadow)
        Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmAdmin"
        StartPosition = FormStartPosition.CenterScreen
        Text = "frmAdmin"
        groupbox_Applications.ResumeLayout(False)
        CType(picStatus_Application_Logout, ComponentModel.ISupportInitialize).EndInit()
        CType(picStatus_Application_Notification, ComponentModel.ISupportInitialize).EndInit()
        groupbox_Services.ResumeLayout(False)
        CType(picStatus_Service_Plumber, ComponentModel.ISupportInitialize).EndInit()
        CType(picStatus_Service_Watchdog, ComponentModel.ISupportInitialize).EndInit()
        pnlAdminOptions.ResumeLayout(False)
        groupbox_UI.ResumeLayout(False)
        groupbox_UI.PerformLayout()
        pnlMain.ResumeLayout(False)
        pnlSystemStatus.ResumeLayout(False)
        groupbox_AutoStart.ResumeLayout(False)
        pnlSystemStatusContainer.ResumeLayout(False)
        pnlAdminOptionsContainer.ResumeLayout(False)
        ResumeLayout(False)
    End Sub

    Friend WithEvents btnCancel As Button
    Friend WithEvents pnlAdminOptions As Panel
    Friend WithEvents picStatus_Service_Plumber As PictureBox
    Friend WithEvents lblStatus_Service_Plumber As Label
    Friend WithEvents groupbox_Services As GroupBox
    Friend WithEvents lblStatus_Service_Watchdog As Label
    Friend WithEvents picStatus_Service_Watchdog As PictureBox
    Friend WithEvents groupbox_Applications As GroupBox
    Friend WithEvents lblStatus_Application_Notification As Label
    Friend WithEvents picStatus_Application_Notification As PictureBox
    Friend WithEvents pnlMain As Panel
    Friend WithEvents lblAdminOptions As Label
    Friend WithEvents bgworker_CheckSystem As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlSystemStatus As Panel
    Friend WithEvents lblSystemStatus As Label
    Friend WithEvents pnlShadow As Panel
    Friend WithEvents lblStatus_Application_Logout As Label
    Friend WithEvents picStatus_Application_Logout As PictureBox
    Friend WithEvents groupbox_UI As GroupBox
    Friend WithEvents chkDebug As CheckBox
    Friend WithEvents btnSave As Button
    Friend WithEvents groupbox_AutoStart As GroupBox
    Friend WithEvents btnAutoStart_Reboot As Button
    Friend WithEvents btnAutoStart_Stop As Button
    Friend WithEvents pnlAdminOptionsContainer As Panel
    Friend WithEvents pnlSystemStatusContainer As Panel
End Class
