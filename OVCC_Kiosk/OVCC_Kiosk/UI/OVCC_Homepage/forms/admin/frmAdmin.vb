﻿Public Class frmAdmin
    Private bStarted As Boolean = False


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmAdmin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime

        pnlShadow.Width = pnlMain.Width
        pnlShadow.Height = pnlMain.Height
        pnlShadow.Top = pnlMain.Top + 6
        pnlShadow.Left = pnlMain.Left + 6

        Me.Width = pnlMain.Width + 2 * pnlMain.Left
        Me.Height = pnlMain.Height + 2 * pnlMain.Top

        CenterForm()

        Helpers.Forms.TopMost(Me.Handle, True)


        '---------------------------------------
        chkDebug.Checked = Globals.Application.Debug
    End Sub

    Private Sub CenterForm()
        Me.Top = (SystemInformation.PrimaryMonitorSize.Height - Me.Height) / 2
        Me.Left = (SystemInformation.PrimaryMonitorSize.Width - Me.Width) / 2
    End Sub

    Private Sub frmAdmin_Move(sender As Object, e As EventArgs) Handles Me.Move
        CenterForm()
    End Sub


    Private Sub frmAdmin_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If Not bStarted Then
            bStarted = True

            bgworker_CheckSystem.RunWorkerAsync()
        End If
    End Sub

    Private Sub CloseDialog()
        Me.Close()
    End Sub

    Private Sub bgworker_CheckSystem_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworker_CheckSystem.DoWork
        CheckApplications()
    End Sub


    '----------------------------------------------------------------
    Private Sub CheckApplications()
        If Helpers.Processes.IsProcessRunning(Settings.Constants.Services.Plumber.Name) Then
            picStatus_Service_Plumber.Image = My.Resources.status_ok__small
        Else
            picStatus_Service_Plumber.Image = My.Resources.status_nok__small
        End If

        If Helpers.Processes.IsProcessRunning(Settings.Constants.Services.Watchdog.Name) Then
            picStatus_Service_Watchdog.Image = My.Resources.status_ok__small
        Else
            picStatus_Service_Watchdog.Image = My.Resources.status_nok__small
        End If


        If Helpers.Processes.IsProcessRunning(Settings.Constants.Paths.Files.Apps.Notification.Name) Then
            picStatus_Application_Notification.Image = My.Resources.status_ok__small
        Else
            picStatus_Application_Notification.Image = My.Resources.status_nok__small
        End If

        If Helpers.Processes.IsProcessRunning(Settings.Constants.Paths.Files.Apps.Logout.Name) Then
            picStatus_Application_Logout.Image = My.Resources.status_ok__small
        Else
            picStatus_Application_Logout.Image = My.Resources.status_nok__small
        End If
    End Sub


    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If chkDebug.Checked Then
            NamedPipe.UI.Client.DebuggingOn()
        Else
            NamedPipe.UI.Client.DebuggingOff()
        End If

        CloseDialog()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        CloseDialog()
    End Sub
End Class