﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdminPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        txtPassword = New TextBox()
        pnlPasswordPlease = New Panel()
        picLock = New PictureBox()
        tmrAutoClose = New Timer(components)
        pnlPasswordPlease.SuspendLayout()
        CType(picLock, ComponentModel.ISupportInitialize).BeginInit()
        SuspendLayout()
        ' 
        ' txtPassword
        ' 
        txtPassword.BorderStyle = BorderStyle.FixedSingle
        txtPassword.Font = New Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        txtPassword.Location = New Point(37, 3)
        txtPassword.Name = "txtPassword"
        txtPassword.PasswordChar = "*"c
        txtPassword.Size = New Size(439, 39)
        txtPassword.TabIndex = 1
        txtPassword.TextAlign = HorizontalAlignment.Center
        ' 
        ' pnlPasswordPlease
        ' 
        pnlPasswordPlease.BackColor = Color.Red
        pnlPasswordPlease.BorderStyle = BorderStyle.FixedSingle
        pnlPasswordPlease.Controls.Add(picLock)
        pnlPasswordPlease.Controls.Add(txtPassword)
        pnlPasswordPlease.Location = New Point(12, 12)
        pnlPasswordPlease.Name = "pnlPasswordPlease"
        pnlPasswordPlease.Size = New Size(481, 47)
        pnlPasswordPlease.TabIndex = 2
        ' 
        ' picLock
        ' 
        picLock.BackColor = Color.Transparent
        picLock.Image = My.Resources.Resources.lock
        picLock.Location = New Point(3, 3)
        picLock.Name = "picLock"
        picLock.Size = New Size(28, 39)
        picLock.SizeMode = PictureBoxSizeMode.AutoSize
        picLock.TabIndex = 3
        picLock.TabStop = False
        ' 
        ' tmrAutoClose
        ' 
        tmrAutoClose.Interval = 10000
        ' 
        ' frmAdminPassword
        ' 
        AutoScaleDimensions = New SizeF(6F, 13F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(505, 71)
        ControlBox = False
        Controls.Add(pnlPasswordPlease)
        Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmAdminPassword"
        ShowIcon = False
        ShowInTaskbar = False
        StartPosition = FormStartPosition.CenterScreen
        Text = "Password please"
        pnlPasswordPlease.ResumeLayout(False)
        pnlPasswordPlease.PerformLayout()
        CType(picLock, ComponentModel.ISupportInitialize).EndInit()
        ResumeLayout(False)

    End Sub
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents pnlPasswordPlease As Panel
    Friend WithEvents tmrAutoClose As Timer
    Friend WithEvents picLock As PictureBox
End Class
