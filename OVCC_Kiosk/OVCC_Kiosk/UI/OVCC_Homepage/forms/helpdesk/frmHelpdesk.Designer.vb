﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHelpdesk
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlLoading = New Panel()
        progressPanelLoading = New ProgressPanel()
        webview2_Helpdesk = New Microsoft.Web.WebView2.WinForms.WebView2()
        tmrAutoclose = New Timer(components)
        pnlOpenInDefaultBrowser = New Panel()
        lblOpenHelpdeskInExternalBrowser = New Label()
        progressPanelAutoclose = New ProgressPanel()
        pnlLoading.SuspendLayout()
        CType(webview2_Helpdesk, ComponentModel.ISupportInitialize).BeginInit()
        pnlOpenInDefaultBrowser.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlLoading
        ' 
        pnlLoading.BorderStyle = BorderStyle.FixedSingle
        pnlLoading.Controls.Add(progressPanelLoading)
        pnlLoading.Location = New Point(398, 448)
        pnlLoading.Name = "pnlLoading"
        pnlLoading.Size = New Size(590, 20)
        pnlLoading.TabIndex = 1
        ' 
        ' progressPanelLoading
        ' 
        progressPanelLoading.AnimationSpeed = 50R
        progressPanelLoading.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelLoading.AnimationWidth = 25
        progressPanelLoading.BackColor = Color.Transparent
        progressPanelLoading.BackgroundColor = Color.Empty
        progressPanelLoading.Border = True
        progressPanelLoading.Dock = DockStyle.Fill
        progressPanelLoading.Location = New Point(0, 0)
        progressPanelLoading.Name = "progressPanelLoading"
        progressPanelLoading.ProgressBarBorder = False
        progressPanelLoading.ProgressBarColor = Color.Empty
        progressPanelLoading.Size = New Size(588, 18)
        progressPanelLoading.TabIndex = 11
        progressPanelLoading.Value = 0R
        progressPanelLoading.ValueMax = 100R
        progressPanelLoading.ValueMin = 0R
        ' 
        ' webview2_Helpdesk
        ' 
        webview2_Helpdesk.AllowExternalDrop = False
        webview2_Helpdesk.CreationProperties = Nothing
        webview2_Helpdesk.DefaultBackgroundColor = Color.White
        webview2_Helpdesk.Location = New Point(59, 117)
        webview2_Helpdesk.Name = "webview2_Helpdesk"
        webview2_Helpdesk.Size = New Size(638, 298)
        webview2_Helpdesk.TabIndex = 0
        webview2_Helpdesk.ZoomFactor = 1R
        ' 
        ' tmrAutoclose
        ' 
        tmrAutoclose.Interval = 50
        ' 
        ' pnlOpenInDefaultBrowser
        ' 
        pnlOpenInDefaultBrowser.Controls.Add(lblOpenHelpdeskInExternalBrowser)
        pnlOpenInDefaultBrowser.Dock = DockStyle.Top
        pnlOpenInDefaultBrowser.Location = New Point(3, 3)
        pnlOpenInDefaultBrowser.Name = "pnlOpenInDefaultBrowser"
        pnlOpenInDefaultBrowser.Size = New Size(1258, 23)
        pnlOpenInDefaultBrowser.TabIndex = 3
        ' 
        ' lblOpenHelpdeskInExternalBrowser
        ' 
        lblOpenHelpdeskInExternalBrowser.AutoSize = True
        lblOpenHelpdeskInExternalBrowser.Font = New Font("Tahoma", 8.25F, FontStyle.Underline, GraphicsUnit.Point, CByte(0))
        lblOpenHelpdeskInExternalBrowser.ForeColor = Color.DarkOrange
        lblOpenHelpdeskInExternalBrowser.Location = New Point(1049, 0)
        lblOpenHelpdeskInExternalBrowser.Name = "lblOpenHelpdeskInExternalBrowser"
        lblOpenHelpdeskInExternalBrowser.Size = New Size(123, 13)
        lblOpenHelpdeskInExternalBrowser.TabIndex = 0
        lblOpenHelpdeskInExternalBrowser.Text = "Open in default browser"
        lblOpenHelpdeskInExternalBrowser.TextAlign = ContentAlignment.MiddleRight
        ' 
        ' progressPanelAutoclose
        ' 
        progressPanelAutoclose.AnimationSpeed = 50R
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.BackColor = Color.Transparent
        progressPanelAutoclose.BackgroundColor = Color.Empty
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.Dock = DockStyle.Bottom
        progressPanelAutoclose.Location = New Point(3, 674)
        progressPanelAutoclose.Name = "progressPanelAutoclose"
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.ProgressBarColor = Color.Empty
        progressPanelAutoclose.Size = New Size(1258, 4)
        progressPanelAutoclose.TabIndex = 10
        progressPanelAutoclose.Value = 0R
        progressPanelAutoclose.ValueMax = 100R
        progressPanelAutoclose.ValueMin = 0R
        ' 
        ' frmHelpdesk
        ' 
        AutoScaleDimensions = New SizeF(6F, 13F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(1264, 681)
        Controls.Add(progressPanelAutoclose)
        Controls.Add(pnlOpenInDefaultBrowser)
        Controls.Add(pnlLoading)
        Controls.Add(webview2_Helpdesk)
        Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.FixedDialog
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmHelpdesk"
        Padding = New Padding(3)
        ShowIcon = False
        ShowInTaskbar = False
        StartPosition = FormStartPosition.CenterScreen
        Text = "frmHelpdesk"
        pnlLoading.ResumeLayout(False)
        CType(webview2_Helpdesk, ComponentModel.ISupportInitialize).EndInit()
        pnlOpenInDefaultBrowser.ResumeLayout(False)
        pnlOpenInDefaultBrowser.PerformLayout()
        ResumeLayout(False)

    End Sub

    Friend WithEvents webview2_Helpdesk As Microsoft.Web.WebView2.WinForms.WebView2
    Friend WithEvents pnlLoading As Panel
    Friend WithEvents tmrAutoclose As Timer
    Friend WithEvents pnlOpenInDefaultBrowser As Panel
    Friend WithEvents lblOpenHelpdeskInExternalBrowser As Label
    Friend WithEvents progressPanelAutoclose As ProgressPanel
    Friend WithEvents progressPanelLoading As ProgressPanel
End Class
