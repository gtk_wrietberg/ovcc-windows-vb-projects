﻿Public Class frmHelpdeskOverlay
    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Public Sub ShowHelpdesk()
        Dim _frmHelpdesk As New frmHelpdesk
        DialogResult = _frmHelpdesk.ShowDialog(frmMain)
        _frmHelpdesk.Dispose()
        Me.Close()
    End Sub

    Private Sub frmHelpdeskOverlay_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Set fullscreen etc.
        Me.FormBorderStyle = FormBorderStyle.None
        'Me.WindowState = FormWindowState.Maximized
        Me.Location = New Point(0, 0)
        Me.Size = New Size(frmMain.Width, frmMain.Height)
        ' Me.Size = frmMain.Size
        Me.BackColor = Color.Black
        Helpers.Forms.TopMost(Me.Handle, True)

        ShowHelpdesk()
    End Sub
End Class