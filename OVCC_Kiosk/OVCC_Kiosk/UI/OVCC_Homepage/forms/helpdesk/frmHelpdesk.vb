﻿Imports Microsoft.Web.WebView2.Core

Public Class frmHelpdesk
    Private WithEvents webview2_Helpdesk__CoreWebView2 As CoreWebView2

    Private mNavigationCancelled As Boolean = False
    Private mPermittedHost As String = ""

    Private ReadOnly mAutoCloseCounterInterval As Integer = 25
    Private ReadOnly mAutoCloseCounterMAX As Integer = 30 * (1000 / mAutoCloseCounterInterval)
    Private mAutoCloseCounter As Integer = mAutoCloseCounterMAX


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmHelpdesk_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = Localisation.LoadString(1100000)
        Helpers.Forms.TopMost(Me.Handle, True)

        pnlLoading.Left = (Me.Width - pnlLoading.Width) / 2
        pnlLoading.Top = (Me.Height - pnlLoading.Height) / 2
        pnlLoading.BringToFront()

        tmrAutoclose.Interval = mAutoCloseCounterInterval
        tmrAutoclose.Enabled = True

        If SessionStuff.IsSessionActive Then
            pnlOpenInDefaultBrowser.Visible = True
            lblOpenHelpdeskInExternalBrowser.Text = Localisation.LoadString(2000)
            lblOpenHelpdeskInExternalBrowser.Cursor = Cursors.Hand
            lblOpenHelpdeskInExternalBrowser.Top = (pnlOpenInDefaultBrowser.Height - lblOpenHelpdeskInExternalBrowser.Height) / 2
            lblOpenHelpdeskInExternalBrowser.Left = pnlOpenInDefaultBrowser.Width - lblOpenHelpdeskInExternalBrowser.Width
        Else
            pnlOpenInDefaultBrowser.Visible = False
        End If


        'custom progress bar
        progressPanelLoading.BackgroundColor = Color.Transparent
        progressPanelLoading.Border = True
        progressPanelLoading.ProgressBarColor = Color.Green
        progressPanelLoading.ProgressBarBorder = True
        progressPanelLoading.AnimationSpeed = 10
        progressPanelLoading.AnimationWidth = 25
        progressPanelLoading.AnimationType = ProgressPanel.ANIMATION_TYPES.KITT
        progressPanelLoading.Initialise()
        progressPanelLoading.StartAnimation()


        progressPanelAutoclose.BackgroundColor = Color.Transparent
        progressPanelAutoclose.Border = True
        progressPanelAutoclose.ProgressBarColor = Color.Green
        progressPanelAutoclose.ProgressBarBorder = True
        progressPanelAutoclose.AnimationSpeed = 10
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.ValueMin = 0
        progressPanelAutoclose.ValueMax = mAutoCloseCounterMAX
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.Initialise()


        InitialiseMainWebView()
    End Sub

    Private Async Sub InitialiseMainWebView()
        webview2_Helpdesk.Dock = DockStyle.Fill

        Dim cwEnvironment As CoreWebView2Environment = Nothing
        Dim sUserDataFolder As String = IO.Path.Combine(Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER, My.Application.Info.ProductName)

        NamedPipe.Logger.Client.Debug("HELPDESK", "Webview2 user data: " & sUserDataFolder)

        cwEnvironment = Await CoreWebView2Environment.CreateAsync(Nothing, sUserDataFolder, Nothing)

        Await webview2_Helpdesk.EnsureCoreWebView2Async(cwEnvironment)


        webview2_Helpdesk__CoreWebView2 = webview2_Helpdesk.CoreWebView2
        'webview2_Helpdesk.CoreWebView2.AddHostObjectToScript("test", New WebBrowserScriptInterface())
        webview2_Helpdesk.CoreWebView2.Settings.AreDevToolsEnabled = False
        webview2_Helpdesk.CoreWebView2.Settings.AreDefaultContextMenusEnabled = False


        Dim sHelpdesk As String = Localisation.LoadString(1100001)

        Dim tmpUri As New Uri(sHelpdesk)
        mPermittedHost = tmpUri.Host

        NamedPipe.Logger.Client.Message("HELPDESK", "Loading page: " & sHelpdesk)

        webview2_Helpdesk.CoreWebView2.Navigate(sHelpdesk)
    End Sub

    Private Sub webview2_Helpdesk__CoreWebView2_DOMContentLoaded(sender As Object, e As CoreWebView2DOMContentLoadedEventArgs) Handles webview2_Helpdesk__CoreWebView2.DOMContentLoaded
        HideLoadingAnimation()

        NamedPipe.UI.Client.HelpdeskDialog()

        Try
            ScrollToDropdown()
        Catch ex As Exception

        End Try
    End Sub

    Private Async Sub ScrollToDropdown()
        Await webview2_Helpdesk__CoreWebView2.ExecuteScriptAsync("document.getElementById(""inner-content"").scrollIntoView()")
        Await webview2_Helpdesk__CoreWebView2.ExecuteScriptAsync("window.scrollBy(0, 100)")
    End Sub

    Private Sub frmHelpdesk_LostFocus(sender As Object, e As EventArgs) Handles Me.LostFocus
        NamedPipe.Logger.Client.Debug("HELPDESK", "frmHelpdesk_LostFocus")
    End Sub

    Private Sub webview2_Helpdesk__CoreWebView2_NavigationStarting(sender As Object, e As CoreWebView2NavigationStartingEventArgs) Handles webview2_Helpdesk__CoreWebView2.NavigationStarting
        Dim _uri As New Uri(e.Uri)

        If Not _uri.Host.Equals(mPermittedHost) Then
            mNavigationCancelled = True
            NamedPipe.Logger.Client.Warning("HELPDESK", "Invalid navigation attempt outside '" & mPermittedHost & "' (" & _uri.ToString & ")")
            e.Cancel = True
        Else
            ShowLoadingAnimation()
        End If
    End Sub

    Private Sub webview2_Helpdesk_NavigationCompleted(sender As Object, e As CoreWebView2NavigationCompletedEventArgs) Handles webview2_Helpdesk__CoreWebView2.NavigationCompleted
        If mNavigationCancelled Then
            mNavigationCancelled = False
            HideLoadingAnimation()
        End If
    End Sub

    Private Sub webview2_Helpdesk__CoreWebView2_NewWindowRequested(sender As Object, e As CoreWebView2NewWindowRequestedEventArgs) Handles webview2_Helpdesk__CoreWebView2.NewWindowRequested
        NamedPipe.Logger.Client.Warning("HELPDESK", "New window blocked: " & e.Uri)
        e.Handled = True
    End Sub

    Private Sub ShowLoadingAnimation()
        progressPanelLoading.StartAnimation()
        pnlLoading.Visible = True
    End Sub

    Private Sub HideLoadingAnimation()
        progressPanelLoading.StopAnimation()
        pnlLoading.Visible = False
    End Sub

    Private Sub CloseDialog()
        Me.Close()
    End Sub

    Private Sub tmrAutoclose_Tick(sender As Object, e As EventArgs) Handles tmrAutoclose.Tick
        If InputCheck.HasActivity Then
            mAutoCloseCounter = mAutoCloseCounterMAX
        End If

        redrawFakeProgressBar()
        mAutoCloseCounter -= 1
        If mAutoCloseCounter < 0 Then
            NamedPipe.UI.Client.HelpdeskTimeout()
            CloseDialog()
        End If
    End Sub

    Private Sub redrawFakeProgressBar()
        If mAutoCloseCounter < 0 Then Exit Sub

        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.ProgressBarColor = Helpers.UI.ProgressColor(mAutoCloseCounter, mAutoCloseCounterMAX)
    End Sub

    Private Sub frmHelpdesk_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        NamedPipe.UI.Client.HelpdeskClosed()
    End Sub

    Private Sub lblOpenHelpdeskInExternalBrowser_Click(sender As Object, e As EventArgs) Handles lblOpenHelpdeskInExternalBrowser.Click
        SessionStuff.SessionAction.Add_Url(Localisation.LoadString(1100001))

        CloseDialog()
    End Sub
End Class