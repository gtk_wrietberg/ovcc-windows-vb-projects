﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmTerms
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlTerms = New Panel()
        webview2_Terms = New Microsoft.Web.WebView2.WinForms.WebView2()
        pnlTermsButtons = New Panel()
        progressPanelAutoclose = New ProgressPanel()
        btnDecline = New Button()
        btnAccept = New Button()
        tmrAutoClose = New Timer(components)
        pnlShadow = New Panel()
        pnlTermsContainer = New Panel()
        pnlTerms.SuspendLayout()
        CType(webview2_Terms, ComponentModel.ISupportInitialize).BeginInit()
        pnlTermsButtons.SuspendLayout()
        pnlTermsContainer.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlTerms
        ' 
        pnlTerms.BackColor = Color.WhiteSmoke
        pnlTerms.BorderStyle = BorderStyle.FixedSingle
        pnlTerms.Controls.Add(webview2_Terms)
        pnlTerms.Location = New Point(3, 3)
        pnlTerms.Name = "pnlTerms"
        pnlTerms.Size = New Size(640, 480)
        pnlTerms.TabIndex = 1
        ' 
        ' webview2_Terms
        ' 
        webview2_Terms.AllowExternalDrop = False
        webview2_Terms.CreationProperties = Nothing
        webview2_Terms.DefaultBackgroundColor = Color.White
        webview2_Terms.Dock = DockStyle.Fill
        webview2_Terms.Location = New Point(0, 0)
        webview2_Terms.Name = "webview2_Terms"
        webview2_Terms.Size = New Size(638, 478)
        webview2_Terms.TabIndex = 0
        webview2_Terms.ZoomFactor = 1R
        ' 
        ' pnlTermsButtons
        ' 
        pnlTermsButtons.BackColor = Color.WhiteSmoke
        pnlTermsButtons.BorderStyle = BorderStyle.FixedSingle
        pnlTermsButtons.Controls.Add(progressPanelAutoclose)
        pnlTermsButtons.Controls.Add(btnDecline)
        pnlTermsButtons.Controls.Add(btnAccept)
        pnlTermsButtons.Location = New Point(3, 486)
        pnlTermsButtons.Name = "pnlTermsButtons"
        pnlTermsButtons.Size = New Size(640, 87)
        pnlTermsButtons.TabIndex = 2
        ' 
        ' progressPanelAutoclose
        ' 
        progressPanelAutoclose.AnimationSpeed = 50R
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.BackColor = Color.IndianRed
        progressPanelAutoclose.BackgroundColor = Color.Empty
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.Dock = DockStyle.Bottom
        progressPanelAutoclose.Location = New Point(0, 81)
        progressPanelAutoclose.Name = "progressPanelAutoclose"
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.ProgressBarColor = Color.Empty
        progressPanelAutoclose.Size = New Size(638, 4)
        progressPanelAutoclose.TabIndex = 5
        progressPanelAutoclose.Value = 0R
        progressPanelAutoclose.ValueMax = 100R
        progressPanelAutoclose.ValueMin = 0R
        ' 
        ' btnDecline
        ' 
        btnDecline.BackColor = Color.Gainsboro
        btnDecline.FlatAppearance.BorderColor = Color.Black
        btnDecline.FlatAppearance.BorderSize = 2
        btnDecline.FlatStyle = FlatStyle.Flat
        btnDecline.Font = New Font("Tahoma", 12F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        btnDecline.Location = New Point(323, 6)
        btnDecline.Name = "btnDecline"
        btnDecline.Size = New Size(309, 70)
        btnDecline.TabIndex = 1
        btnDecline.Text = "Decline"
        btnDecline.UseVisualStyleBackColor = False
        ' 
        ' btnAccept
        ' 
        btnAccept.BackColor = Color.Gainsboro
        btnAccept.FlatAppearance.BorderColor = Color.Black
        btnAccept.FlatAppearance.BorderSize = 2
        btnAccept.FlatStyle = FlatStyle.Flat
        btnAccept.Font = New Font("Tahoma", 12F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        btnAccept.Location = New Point(6, 6)
        btnAccept.Name = "btnAccept"
        btnAccept.Size = New Size(309, 70)
        btnAccept.TabIndex = 0
        btnAccept.Text = "Accept"
        btnAccept.UseVisualStyleBackColor = False
        ' 
        ' tmrAutoClose
        ' 
        tmrAutoClose.Enabled = True
        tmrAutoClose.Interval = 500
        ' 
        ' pnlShadow
        ' 
        pnlShadow.BackColor = Color.Black
        pnlShadow.Location = New Point(736, 74)
        pnlShadow.Name = "pnlShadow"
        pnlShadow.Size = New Size(35, 62)
        pnlShadow.TabIndex = 3
        ' 
        ' pnlTermsContainer
        ' 
        pnlTermsContainer.BackColor = Color.RoyalBlue
        pnlTermsContainer.BorderStyle = BorderStyle.FixedSingle
        pnlTermsContainer.Controls.Add(pnlTerms)
        pnlTermsContainer.Controls.Add(pnlTermsButtons)
        pnlTermsContainer.Location = New Point(12, 15)
        pnlTermsContainer.Name = "pnlTermsContainer"
        pnlTermsContainer.Size = New Size(648, 578)
        pnlTermsContainer.TabIndex = 4
        ' 
        ' frmTerms
        ' 
        AutoScaleDimensions = New SizeF(6F, 13F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(1439, 622)
        Controls.Add(pnlTermsContainer)
        Controls.Add(pnlShadow)
        Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmTerms"
        ShowIcon = False
        ShowInTaskbar = False
        StartPosition = FormStartPosition.CenterScreen
        Text = "frmTerms"
        pnlTerms.ResumeLayout(False)
        CType(webview2_Terms, ComponentModel.ISupportInitialize).EndInit()
        pnlTermsButtons.ResumeLayout(False)
        pnlTermsContainer.ResumeLayout(False)
        ResumeLayout(False)

    End Sub
    Friend WithEvents pnlTerms As Panel
    Friend WithEvents pnlTermsButtons As Panel
    Friend WithEvents btnDecline As Button
    Friend WithEvents btnAccept As Button
    Friend WithEvents tmrAutoClose As Timer
    Friend WithEvents pnlShadow As Panel
    Friend WithEvents pnlTermsContainer As Panel
    Friend WithEvents webview2_Terms As Microsoft.Web.WebView2.WinForms.WebView2
    Friend WithEvents progressPanelAutoclose As ProgressPanel
End Class
