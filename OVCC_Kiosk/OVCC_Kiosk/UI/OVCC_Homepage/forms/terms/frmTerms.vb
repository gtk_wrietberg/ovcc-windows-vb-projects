﻿Imports Microsoft.Web.WebView2.Core

Public Class frmTerms
    Private ReadOnly mAutoCloseCounterMAX As Integer = 300
    Private mAutoCloseCounter As Integer = mAutoCloseCounterMAX


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmTerms_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime
        Me.ClientSize = New Size(pnlTermsContainer.Width + (2 * pnlTermsContainer.Left), pnlTermsContainer.Height + (2 * pnlTermsContainer.Top))
        Me.Left = (frmMain.Width - Me.Width) / 2
        Me.Top = (frmMain.Height - Me.Height) / 2


        Helpers.Forms.TopMost(Me.Handle, True)

        pnlShadow.Width = pnlTermsContainer.Width
        pnlShadow.Height = pnlTermsContainer.Height
        pnlShadow.Top = pnlTermsContainer.Top + 6
        pnlShadow.Left = pnlTermsContainer.Left + 6
        pnlShadow.SendToBack()


        btnAccept.Text = Localisation.LoadString(102)
        btnDecline.Text = Localisation.LoadString(103)


        progressPanelAutoclose.BackgroundColor = Color.Transparent
        progressPanelAutoclose.Border = True
        progressPanelAutoclose.ProgressBarColor = Color.Green
        progressPanelAutoclose.ProgressBarBorder = True
        progressPanelAutoclose.AnimationSpeed = 10
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.ValueMin = 0
        progressPanelAutoclose.ValueMax = mAutoCloseCounterMAX
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.Initialise()


        InitialiseMainWebView()
    End Sub

    Private Async Sub InitialiseMainWebView()
        webview2_Terms.Dock = DockStyle.Fill

        Dim cwEnvironment As CoreWebView2Environment = Nothing
        Dim sUserDataFolder As String = IO.Path.Combine(Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER, My.Application.Info.ProductName)

        NamedPipe.Logger.Client.Debug("Webview2 user data: " & sUserDataFolder)

        cwEnvironment = Await CoreWebView2Environment.CreateAsync(Nothing, sUserDataFolder, Nothing)

        Await webview2_Terms.EnsureCoreWebView2Async(cwEnvironment)

        'webview2_Terms.CoreWebView2.AddHostObjectToScript("test", New WebBrowserScriptInterface())
        webview2_Terms.CoreWebView2.Settings.AreDevToolsEnabled = False
        webview2_Terms.CoreWebView2.Settings.AreDefaultContextMenusEnabled = False


        webview2_Terms.CoreWebView2.Navigate(Localisation.GetTermsFile())
    End Sub

    Private Sub webview2_Terms_NavigationStarting(sender As Object, e As CoreWebView2NavigationStartingEventArgs) Handles webview2_Terms.NavigationStarting

    End Sub

    Private Sub btnAccept_Click(sender As Object, e As EventArgs) Handles btnAccept.Click
        DialogResult = DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub btnDecline_Click(sender As Object, e As EventArgs) Handles btnDecline.Click
        DialogResult = DialogResult.No
        Me.Close()
    End Sub

    Private Sub tmrAutoClose_Tick(sender As Object, e As EventArgs) Handles tmrAutoClose.Tick
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.ProgressBarColor = Helpers.UI.ProgressColor(mAutoCloseCounter, mAutoCloseCounterMAX)
        mAutoCloseCounter -= 1
        If mAutoCloseCounter < 0 Then
            DialogResult = DialogResult.Ignore
            Me.Close()
        End If
    End Sub

    Private Sub webview2_Terms_MouseWheel(sender As Object, e As EventArgs) Handles webview2_Terms.MouseHover
        mAutoCloseCounter = mAutoCloseCounterMAX
    End Sub
End Class