﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlLogoutContainer = New Panel()
        pnlLogout = New Panel()
        progressPanelAutoclose = New ProgressPanel()
        lblLogoutWarning = New Label()
        btnLogout = New Button()
        btnCancel = New Button()
        pnlShadow = New Panel()
        tmrAutoclose = New Timer(components)
        pnlButtons = New Panel()
        pnlLogoutContainer.SuspendLayout()
        pnlLogout.SuspendLayout()
        pnlButtons.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlLogoutContainer
        ' 
        pnlLogoutContainer.BackColor = Color.Red
        pnlLogoutContainer.BorderStyle = BorderStyle.FixedSingle
        pnlLogoutContainer.Controls.Add(pnlLogout)
        pnlLogoutContainer.Location = New Point(12, 12)
        pnlLogoutContainer.Name = "pnlLogoutContainer"
        pnlLogoutContainer.Size = New Size(677, 165)
        pnlLogoutContainer.TabIndex = 2
        ' 
        ' pnlLogout
        ' 
        pnlLogout.BackColor = Color.WhiteSmoke
        pnlLogout.BorderStyle = BorderStyle.FixedSingle
        pnlLogout.Controls.Add(pnlButtons)
        pnlLogout.Controls.Add(progressPanelAutoclose)
        pnlLogout.Controls.Add(lblLogoutWarning)
        pnlLogout.Location = New Point(3, 3)
        pnlLogout.Name = "pnlLogout"
        pnlLogout.Size = New Size(668, 156)
        pnlLogout.TabIndex = 0
        ' 
        ' progressPanelAutoclose
        ' 
        progressPanelAutoclose.AnimationSpeed = 50R
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.BackColor = Color.Transparent
        progressPanelAutoclose.BackgroundColor = Color.Empty
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.Dock = DockStyle.Bottom
        progressPanelAutoclose.Location = New Point(0, 150)
        progressPanelAutoclose.Name = "progressPanelAutoclose"
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.ProgressBarColor = Color.Empty
        progressPanelAutoclose.Size = New Size(666, 4)
        progressPanelAutoclose.TabIndex = 4
        progressPanelAutoclose.Value = 0R
        progressPanelAutoclose.ValueMax = 100R
        progressPanelAutoclose.ValueMin = 0R
        ' 
        ' lblLogoutWarning
        ' 
        lblLogoutWarning.Dock = DockStyle.Top
        lblLogoutWarning.Font = New Font("Tahoma", 12F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        lblLogoutWarning.Location = New Point(0, 0)
        lblLogoutWarning.Name = "lblLogoutWarning"
        lblLogoutWarning.Size = New Size(666, 97)
        lblLogoutWarning.TabIndex = 3
        lblLogoutWarning.Text = "Logging out will close this session." & vbCrLf & "Any open files not saved on removable media will be deleted." & vbCrLf & vbCrLf & "Are you sure?"
        lblLogoutWarning.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' btnLogout
        ' 
        btnLogout.BackColor = Color.White
        btnLogout.Font = New Font("Tahoma", 12F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        btnLogout.Location = New Point(3, 3)
        btnLogout.Name = "btnLogout"
        btnLogout.Size = New Size(240, 45)
        btnLogout.TabIndex = 1
        btnLogout.Text = ">>logout<<"
        btnLogout.UseVisualStyleBackColor = False
        ' 
        ' btnCancel
        ' 
        btnCancel.BackColor = Color.White
        btnCancel.Font = New Font("Tahoma", 12F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        btnCancel.Location = New Point(416, 3)
        btnCancel.Name = "btnCancel"
        btnCancel.Size = New Size(240, 45)
        btnCancel.TabIndex = 2
        btnCancel.Text = ">>cancel<<"
        btnCancel.UseVisualStyleBackColor = False
        ' 
        ' pnlShadow
        ' 
        pnlShadow.BackColor = Color.Black
        pnlShadow.Location = New Point(18, 18)
        pnlShadow.Name = "pnlShadow"
        pnlShadow.Size = New Size(677, 165)
        pnlShadow.TabIndex = 3
        ' 
        ' tmrAutoclose
        ' 
        tmrAutoclose.Enabled = True
        tmrAutoclose.Interval = 500
        ' 
        ' pnlButtons
        ' 
        pnlButtons.Controls.Add(btnLogout)
        pnlButtons.Controls.Add(btnCancel)
        pnlButtons.Location = New Point(3, 100)
        pnlButtons.Name = "pnlButtons"
        pnlButtons.Size = New Size(659, 51)
        pnlButtons.TabIndex = 4
        ' 
        ' frmLogout
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(1012, 552)
        ControlBox = False
        Controls.Add(pnlLogoutContainer)
        Controls.Add(pnlShadow)
        FormBorderStyle = FormBorderStyle.None
        Name = "frmLogout"
        Text = "frmLogout"
        pnlLogoutContainer.ResumeLayout(False)
        pnlLogout.ResumeLayout(False)
        pnlButtons.ResumeLayout(False)
        ResumeLayout(False)
    End Sub

    Friend WithEvents pnlLogoutContainer As Panel
    Friend WithEvents pnlLogout As Panel
    Friend WithEvents lblLogoutWarning As Label
    Friend WithEvents btnLogout As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents pnlShadow As Panel
    Friend WithEvents tmrAutoclose As Timer
    Friend WithEvents progressPanelAutoclose As ProgressPanel
    Friend WithEvents pnlButtons As Panel
End Class
