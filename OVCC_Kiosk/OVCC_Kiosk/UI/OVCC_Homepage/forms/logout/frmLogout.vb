﻿Public Class frmLogout
    Private ReadOnly mAutoCloseCounterMAX As Integer = 30
    Private mAutoCloseCounter As Integer = mAutoCloseCounterMAX


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmLogout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime
        Me.ClientSize = New Size(pnlLogoutContainer.Width + (2 * pnlLogoutContainer.Left), pnlLogoutContainer.Height + (2 * pnlLogoutContainer.Top))
        Me.Left = (frmMain.Width - Me.Width) / 2
        Me.Top = (frmMain.Height - Me.Height) / 2

        Helpers.Forms.TopMost(Me.Handle, True)


        lblLogoutWarning.Text = Localisation.LoadString(1020000).Replace("\n", vbCrLf)
        btnLogout.Text = Localisation.LoadString(300)
        btnCancel.Text = Localisation.LoadString(301)


        progressPanelAutoclose.BackgroundColor = Color.Transparent
        progressPanelAutoclose.Border = True
        progressPanelAutoclose.ProgressBarColor = Color.Green
        progressPanelAutoclose.ProgressBarBorder = True
        progressPanelAutoclose.AnimationSpeed = 10
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.ValueMin = 0
        progressPanelAutoclose.ValueMax = mAutoCloseCounterMAX
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.Initialise()
    End Sub

    Private Sub btnLogout_Click(sender As Object, e As EventArgs) Handles btnLogout.Click
        Logout()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Cancel()
    End Sub

    Private Sub Logout()
        DialogResult = DialogResult.Yes
        _Close()
    End Sub

    Private Sub Cancel()
        DialogResult = DialogResult.No
        _Close()
    End Sub

    Private Sub _Close()
        Me.Close()
    End Sub

    Private Sub tmrAutoclose_Tick(sender As Object, e As EventArgs) Handles tmrAutoclose.Tick
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.ProgressBarColor = Helpers.UI.ProgressColor(mAutoCloseCounter, mAutoCloseCounterMAX)
        mAutoCloseCounter -= 1
        If mAutoCloseCounter < 0 Then
            Logout()
        End If
    End Sub
End Class
