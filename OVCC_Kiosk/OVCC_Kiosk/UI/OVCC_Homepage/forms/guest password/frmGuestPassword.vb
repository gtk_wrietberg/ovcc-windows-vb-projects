﻿Public Class frmGuestPassword
    Private _GuestPassword As String = ""
    Private iGuesses As Integer = 0

    Private ReadOnly mAutoCloseCounterMAX As Integer = 300
    Private mAutoCloseCounter As Integer = mAutoCloseCounterMAX


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmGuestPassword_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime
        Me.ClientSize = New Size(pnlGuestPasswordContainer.Width + (2 * pnlGuestPasswordContainer.Left), pnlGuestPasswordContainer.Height + (2 * pnlGuestPasswordContainer.Top))
        Me.Left = (frmMain.Width - Me.Width) / 2
        Me.Top = (frmMain.Height - Me.Height) / 2

        Helpers.Forms.TopMost(Me.Handle, True)

        lblGuestPassword.Text = Localisation.LoadString(104)
        btnContinue.Text = Localisation.LoadString(105)
        btnCancel.Text = Localisation.LoadString(106)
        txtGuestPassword.Text = ""


        _GuestPassword = Helpers.XOrObfuscation_v2.Deobfuscate(
                                                Settings.Get.String(
                                                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                                                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__GuestPassword,
                                                    ""))

        iGuesses = 0
        lblInvalid.Visible = False
        txtGuestPassword.Enabled = True
        btnContinue.Enabled = True
        btnContinue.Visible = True


        progressPanelAutoclose.BackgroundColor = Color.Transparent
        progressPanelAutoclose.Border = True
        progressPanelAutoclose.ProgressBarColor = Color.Green
        progressPanelAutoclose.ProgressBarBorder = True
        progressPanelAutoclose.AnimationSpeed = 10
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.ValueMin = 0
        progressPanelAutoclose.ValueMax = mAutoCloseCounterMAX
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.Initialise()
    End Sub

    Private Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        _Continue()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If iGuesses < 3 Then
            _Cancel()
        Else
            _Failed()
        End If
    End Sub

    Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtGuestPassword.KeyDown
        If e.KeyCode = Keys.Enter Then
            _Continue()
        ElseIf e.KeyCode = Keys.Escape Then
            _Cancel()
        End If
    End Sub

    Private Sub tmrAutoClose_Tick(sender As Object, e As EventArgs) Handles tmrAutoclose.Tick
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.ProgressBarColor = Helpers.UI.ProgressColor(mAutoCloseCounter, mAutoCloseCounterMAX)

        mAutoCloseCounter -= 1
        If mAutoCloseCounter < 0 Then
            _Timeout()
        End If
    End Sub

    Private Sub _Continue()
        If _GuestPassword.Equals(Trim(txtGuestPassword.Text)) Then
            DialogResult = DialogResult.Yes
            _Close()
            Exit Sub
        End If

        txtGuestPassword.Text = ""

        iGuesses += 1

        NamedPipe.Session.Client.GuestPasswordDenied()

        If iGuesses < 3 Then
            lblInvalid.Text = Localisation.LoadString(1010)
            lblInvalid.Visible = True
        Else
            lblInvalid.Text = Localisation.LoadString(1011)
            lblInvalid.Visible = True

            txtGuestPassword.Enabled = False
            btnContinue.Enabled = False
            btnContinue.Visible = False
        End If
    End Sub

    Private Sub _Failed()
        DialogResult = DialogResult.Abort
        _Close()
    End Sub

    Private Sub _Cancel()
        DialogResult = DialogResult.No
        _Close()
    End Sub

    Private Sub _Timeout()
        DialogResult = DialogResult.Ignore
        _Close()
    End Sub

    Private Sub _Close()
        txtGuestPassword.Text = ""
        Me.Close()
    End Sub

    Private Sub txtGuestPassword_TextChanged(sender As Object, e As EventArgs) Handles txtGuestPassword.TextChanged
        lblInvalid.Visible = False
    End Sub
End Class