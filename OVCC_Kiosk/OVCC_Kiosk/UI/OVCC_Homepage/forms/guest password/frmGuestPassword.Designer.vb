﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGuestPassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlGuestPasswordContainer = New Panel()
        pnlGuestPassword = New Panel()
        progressPanelAutoclose = New ProgressPanel()
        lblInvalid = New Label()
        txtGuestPassword = New TextBox()
        lblGuestPassword = New Label()
        btnContinue = New Button()
        btnCancel = New Button()
        tmrAutoclose = New Timer(components)
        pnlShadow = New Panel()
        pnlButtons = New Panel()
        pnlGuestPasswordContainer.SuspendLayout()
        pnlGuestPassword.SuspendLayout()
        pnlButtons.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlGuestPasswordContainer
        ' 
        pnlGuestPasswordContainer.BackColor = Color.Orange
        pnlGuestPasswordContainer.BorderStyle = BorderStyle.FixedSingle
        pnlGuestPasswordContainer.Controls.Add(pnlGuestPassword)
        pnlGuestPasswordContainer.Location = New Point(12, 12)
        pnlGuestPasswordContainer.Name = "pnlGuestPasswordContainer"
        pnlGuestPasswordContainer.Size = New Size(416, 164)
        pnlGuestPasswordContainer.TabIndex = 0
        ' 
        ' pnlGuestPassword
        ' 
        pnlGuestPassword.BackColor = Color.WhiteSmoke
        pnlGuestPassword.BorderStyle = BorderStyle.FixedSingle
        pnlGuestPassword.Controls.Add(pnlButtons)
        pnlGuestPassword.Controls.Add(progressPanelAutoclose)
        pnlGuestPassword.Controls.Add(lblInvalid)
        pnlGuestPassword.Controls.Add(txtGuestPassword)
        pnlGuestPassword.Controls.Add(lblGuestPassword)
        pnlGuestPassword.Location = New Point(3, 3)
        pnlGuestPassword.Name = "pnlGuestPassword"
        pnlGuestPassword.Size = New Size(408, 156)
        pnlGuestPassword.TabIndex = 0
        ' 
        ' progressPanelAutoclose
        ' 
        progressPanelAutoclose.AnimationSpeed = 50R
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.BackColor = Color.Transparent
        progressPanelAutoclose.BackgroundColor = Color.FromArgb(CByte(255), CByte(128), CByte(128))
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.Dock = DockStyle.Bottom
        progressPanelAutoclose.Location = New Point(0, 150)
        progressPanelAutoclose.Name = "progressPanelAutoclose"
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.ProgressBarColor = Color.Empty
        progressPanelAutoclose.Size = New Size(406, 4)
        progressPanelAutoclose.TabIndex = 2
        progressPanelAutoclose.Value = 0R
        progressPanelAutoclose.ValueMax = 100R
        progressPanelAutoclose.ValueMin = 0R
        ' 
        ' lblInvalid
        ' 
        lblInvalid.BackColor = Color.Transparent
        lblInvalid.ForeColor = Color.Red
        lblInvalid.Location = New Point(3, 38)
        lblInvalid.Name = "lblInvalid"
        lblInvalid.Size = New Size(400, 20)
        lblInvalid.TabIndex = 1
        lblInvalid.Text = ">>string 1010<<"
        lblInvalid.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' txtGuestPassword
        ' 
        txtGuestPassword.BackColor = Color.White
        txtGuestPassword.BorderStyle = BorderStyle.FixedSingle
        txtGuestPassword.Font = New Font("Tahoma", 15.75F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        txtGuestPassword.Location = New Point(3, 59)
        txtGuestPassword.Name = "txtGuestPassword"
        txtGuestPassword.PasswordChar = "*"c
        txtGuestPassword.Size = New Size(400, 33)
        txtGuestPassword.TabIndex = 4
        txtGuestPassword.TextAlign = HorizontalAlignment.Center
        ' 
        ' lblGuestPassword
        ' 
        lblGuestPassword.Font = New Font("Tahoma", 12F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        lblGuestPassword.Location = New Point(3, 0)
        lblGuestPassword.Name = "lblGuestPassword"
        lblGuestPassword.Size = New Size(400, 36)
        lblGuestPassword.TabIndex = 3
        lblGuestPassword.Text = ">>string 104<<"
        lblGuestPassword.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' btnContinue
        ' 
        btnContinue.BackColor = Color.White
        btnContinue.Font = New Font("Tahoma", 12F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        btnContinue.Location = New Point(3, 1)
        btnContinue.Name = "btnContinue"
        btnContinue.Size = New Size(180, 49)
        btnContinue.TabIndex = 1
        btnContinue.Text = ">>continue<<"
        btnContinue.UseVisualStyleBackColor = False
        ' 
        ' btnCancel
        ' 
        btnCancel.BackColor = Color.White
        btnCancel.Font = New Font("Tahoma", 12F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        btnCancel.Location = New Point(216, 1)
        btnCancel.Name = "btnCancel"
        btnCancel.Size = New Size(180, 49)
        btnCancel.TabIndex = 2
        btnCancel.Text = ">>cancel<<"
        btnCancel.UseVisualStyleBackColor = False
        ' 
        ' tmrAutoclose
        ' 
        tmrAutoclose.Enabled = True
        tmrAutoclose.Interval = 500
        ' 
        ' pnlShadow
        ' 
        pnlShadow.BackColor = Color.Black
        pnlShadow.Location = New Point(18, 18)
        pnlShadow.Name = "pnlShadow"
        pnlShadow.Size = New Size(416, 164)
        pnlShadow.TabIndex = 1
        ' 
        ' pnlButtons
        ' 
        pnlButtons.Controls.Add(btnContinue)
        pnlButtons.Controls.Add(btnCancel)
        pnlButtons.Location = New Point(4, 98)
        pnlButtons.Name = "pnlButtons"
        pnlButtons.Size = New Size(399, 53)
        pnlButtons.TabIndex = 3
        ' 
        ' frmGuestPassword
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(800, 420)
        ControlBox = False
        Controls.Add(pnlGuestPasswordContainer)
        Controls.Add(pnlShadow)
        Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        Name = "frmGuestPassword"
        Text = "frmGuestPassword"
        pnlGuestPasswordContainer.ResumeLayout(False)
        pnlGuestPassword.ResumeLayout(False)
        pnlGuestPassword.PerformLayout()
        pnlButtons.ResumeLayout(False)
        ResumeLayout(False)
    End Sub

    Friend WithEvents pnlGuestPasswordContainer As Panel
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnContinue As Button
    Friend WithEvents pnlGuestPassword As Panel
    Friend WithEvents lblGuestPassword As Label
    Friend WithEvents txtGuestPassword As TextBox
    Friend WithEvents tmrAutoclose As Timer
    Friend WithEvents lblInvalid As Label
    Friend WithEvents pnlShadow As Panel
    Friend WithEvents progressPanelAutoclose As ProgressPanel
    Friend WithEvents pnlButtons As Panel
End Class
