﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOffice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlIcon1 = New Panel()
        lblIcon1 = New Label()
        picIcon1 = New PictureBox()
        pnlIcon2 = New Panel()
        lblIcon2 = New Label()
        picIcon2 = New PictureBox()
        pnlIcon3 = New Panel()
        lblIcon3 = New Label()
        picIcon3 = New PictureBox()
        tmrAutoclose = New Timer(components)
        progressPanelAutoclose = New ProgressPanel()
        pnlIcons = New Panel()
        pnlIcon1.SuspendLayout()
        CType(picIcon1, ComponentModel.ISupportInitialize).BeginInit()
        pnlIcon2.SuspendLayout()
        CType(picIcon2, ComponentModel.ISupportInitialize).BeginInit()
        pnlIcon3.SuspendLayout()
        CType(picIcon3, ComponentModel.ISupportInitialize).BeginInit()
        pnlIcons.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlIcon1
        ' 
        pnlIcon1.Controls.Add(lblIcon1)
        pnlIcon1.Controls.Add(picIcon1)
        pnlIcon1.Location = New Point(12, 12)
        pnlIcon1.Name = "pnlIcon1"
        pnlIcon1.Size = New Size(134, 159)
        pnlIcon1.TabIndex = 4
        ' 
        ' lblIcon1
        ' 
        lblIcon1.Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblIcon1.Location = New Point(3, 134)
        lblIcon1.Name = "lblIcon1"
        lblIcon1.Size = New Size(128, 19)
        lblIcon1.TabIndex = 1
        lblIcon1.Text = "1_desc"
        lblIcon1.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' picIcon1
        ' 
        picIcon1.Cursor = Cursors.Hand
        picIcon1.Location = New Point(3, 3)
        picIcon1.Name = "picIcon1"
        picIcon1.Size = New Size(128, 128)
        picIcon1.TabIndex = 0
        picIcon1.TabStop = False
        ' 
        ' pnlIcon2
        ' 
        pnlIcon2.Controls.Add(lblIcon2)
        pnlIcon2.Controls.Add(picIcon2)
        pnlIcon2.Location = New Point(156, 12)
        pnlIcon2.Name = "pnlIcon2"
        pnlIcon2.Size = New Size(134, 159)
        pnlIcon2.TabIndex = 5
        ' 
        ' lblIcon2
        ' 
        lblIcon2.Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblIcon2.Location = New Point(3, 134)
        lblIcon2.Name = "lblIcon2"
        lblIcon2.Size = New Size(128, 19)
        lblIcon2.TabIndex = 1
        lblIcon2.Text = "2_desc"
        lblIcon2.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' picIcon2
        ' 
        picIcon2.Cursor = Cursors.Hand
        picIcon2.Location = New Point(3, 3)
        picIcon2.Name = "picIcon2"
        picIcon2.Size = New Size(128, 128)
        picIcon2.TabIndex = 0
        picIcon2.TabStop = False
        ' 
        ' pnlIcon3
        ' 
        pnlIcon3.Controls.Add(lblIcon3)
        pnlIcon3.Controls.Add(picIcon3)
        pnlIcon3.Location = New Point(300, 12)
        pnlIcon3.Name = "pnlIcon3"
        pnlIcon3.Size = New Size(134, 159)
        pnlIcon3.TabIndex = 6
        ' 
        ' lblIcon3
        ' 
        lblIcon3.Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        lblIcon3.Location = New Point(3, 134)
        lblIcon3.Name = "lblIcon3"
        lblIcon3.Size = New Size(128, 19)
        lblIcon3.TabIndex = 1
        lblIcon3.Text = "3_desc"
        lblIcon3.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' picIcon3
        ' 
        picIcon3.Cursor = Cursors.Hand
        picIcon3.Location = New Point(3, 3)
        picIcon3.Name = "picIcon3"
        picIcon3.Size = New Size(128, 128)
        picIcon3.TabIndex = 0
        picIcon3.TabStop = False
        ' 
        ' tmrAutoclose
        ' 
        tmrAutoclose.Interval = 50
        ' 
        ' progressPanelAutoclose
        ' 
        progressPanelAutoclose.AnimationSpeed = 50R
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.BackColor = Color.IndianRed
        progressPanelAutoclose.BackgroundColor = Color.Empty
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.Dock = DockStyle.Bottom
        progressPanelAutoclose.Location = New Point(0, 179)
        progressPanelAutoclose.Name = "progressPanelAutoclose"
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.ProgressBarColor = Color.Empty
        progressPanelAutoclose.Size = New Size(442, 4)
        progressPanelAutoclose.TabIndex = 7
        progressPanelAutoclose.Value = 0R
        progressPanelAutoclose.ValueMax = 100R
        progressPanelAutoclose.ValueMin = 0R
        ' 
        ' pnlIcons
        ' 
        pnlIcons.Controls.Add(pnlIcon1)
        pnlIcons.Controls.Add(pnlIcon2)
        pnlIcons.Controls.Add(pnlIcon3)
        pnlIcons.Dock = DockStyle.Top
        pnlIcons.Location = New Point(0, 0)
        pnlIcons.Name = "pnlIcons"
        pnlIcons.Size = New Size(442, 179)
        pnlIcons.TabIndex = 8
        ' 
        ' frmOffice
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.WhiteSmoke
        ClientSize = New Size(442, 183)
        Controls.Add(pnlIcons)
        Controls.Add(progressPanelAutoclose)
        Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.FixedDialog
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmOffice"
        ShowIcon = False
        ShowInTaskbar = False
        StartPosition = FormStartPosition.CenterScreen
        pnlIcon1.ResumeLayout(False)
        CType(picIcon1, ComponentModel.ISupportInitialize).EndInit()
        pnlIcon2.ResumeLayout(False)
        CType(picIcon2, ComponentModel.ISupportInitialize).EndInit()
        pnlIcon3.ResumeLayout(False)
        CType(picIcon3, ComponentModel.ISupportInitialize).EndInit()
        pnlIcons.ResumeLayout(False)
        ResumeLayout(False)
    End Sub
    Friend WithEvents pnlIcon1 As Panel
    Friend WithEvents picIcon1 As PictureBox
    Friend WithEvents lblIcon1 As Label
    Friend WithEvents pnlIcon2 As Panel
    Friend WithEvents lblIcon2 As Label
    Friend WithEvents picIcon2 As PictureBox
    Friend WithEvents pnlIcon3 As Panel
    Friend WithEvents lblIcon3 As Label
    Friend WithEvents picIcon3 As PictureBox
    Friend WithEvents tmrAutoclose As Timer
    Friend WithEvents progressPanelAutoclose As ProgressPanel
    Friend WithEvents pnlIcons As Panel
End Class
