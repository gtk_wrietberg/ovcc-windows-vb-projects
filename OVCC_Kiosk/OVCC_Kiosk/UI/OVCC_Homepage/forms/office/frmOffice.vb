﻿Public Class frmOffice
    Private ReadOnly TRANSPARENCY_COLOR As Color = Color.Gray

    Private officeAppCount As Integer = 0

    Private ReadOnly mAutoCloseCounterInterval As Integer = 25
    Private ReadOnly mAutoCloseCounterMAX As Integer = 10 * (1000 / mAutoCloseCounterInterval)
    Private mAutoCloseCounter As Integer = mAutoCloseCounterMAX


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub frmOffice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = TRANSPARENCY_COLOR
        Me.Visible = False
        Helpers.Forms.TopMost(Me.Handle, True)


        NamedPipe.UI.Client.OfficeDialog()


        '-----------------
        Me.Left = (frmMain.Width - Me.Width) / 2
        Me.Top = (frmMain.Height - Me.Height) / 2


        '-----------------
        Dim _bitmaps As New List(Of Office._office_info)

        _bitmaps.Add(Office.GetInfo_Word())
        _bitmaps.Add(Office.GetInfo_Excel())
        _bitmaps.Add(Office.GetInfo_Powerpoint())


        officeAppCount = _bitmaps.AsEnumerable().Count(Function(_value) _value.Icon IsNot Nothing)

        If officeAppCount <= 0 Then
            NamedPipe.Logger.Client.Error("No office apps found for office dialog!")
            CloseOffice()
            Exit Sub
        End If


        Dim _margin As Integer = (Me.Width - (officeAppCount * pnlIcon1.Width)) / (officeAppCount + 1)

        Select Case officeAppCount
            Case 1
                pnlIcon1.Visible = True
                pnlIcon2.Visible = False
                pnlIcon3.Visible = False

                pnlIcon1.Top = Settings.Constants.UI.OFFICE__ICON_TOP
                pnlIcon1.Left = _margin
                picIcon1.Image = _bitmaps.Item(0).Icon
                lblIcon1.Text = _bitmaps.Item(0).Description
                AddHandler picIcon1.MouseClick, Sub(_sender, _e) picIcon_Clicked(_sender, _e, _bitmaps.Item(0))
            Case 2
                pnlIcon1.Visible = True
                pnlIcon2.Visible = True
                pnlIcon3.Visible = False

                pnlIcon1.Top = Settings.Constants.UI.OFFICE__ICON_TOP
                pnlIcon1.Left = _margin
                picIcon1.Image = _bitmaps.Item(0).Icon
                lblIcon1.Text = _bitmaps.Item(0).Description
                AddHandler picIcon1.MouseClick, Sub(_sender, _e) picIcon_Clicked(_sender, _e, _bitmaps.Item(0))

                pnlIcon2.Top = Settings.Constants.UI.OFFICE__ICON_TOP
                pnlIcon2.Left = _margin + pnlIcon1.Width + _margin
                picIcon2.Image = _bitmaps.Item(1).Icon
                lblIcon2.Text = _bitmaps.Item(1).Description
                AddHandler picIcon2.MouseClick, Sub(_sender, _e) picIcon_Clicked(_sender, _e, _bitmaps.Item(1))
            Case 3
                pnlIcon1.Visible = True
                pnlIcon2.Visible = True
                pnlIcon3.Visible = True

                pnlIcon1.Top = Settings.Constants.UI.OFFICE__ICON_TOP
                pnlIcon1.Left = _margin
                picIcon1.Image = _bitmaps.Item(0).Icon
                lblIcon1.Text = _bitmaps.Item(0).Description
                AddHandler picIcon1.MouseClick, Sub(_sender, _e) picIcon_Clicked(_sender, _e, _bitmaps.Item(0))

                pnlIcon2.Top = Settings.Constants.UI.OFFICE__ICON_TOP
                pnlIcon2.Left = _margin + pnlIcon1.Width + _margin
                picIcon2.Image = _bitmaps.Item(1).Icon
                lblIcon2.Text = _bitmaps.Item(1).Description
                AddHandler picIcon2.MouseClick, Sub(_sender, _e) picIcon_Clicked(_sender, _e, _bitmaps.Item(1))

                pnlIcon3.Top = Settings.Constants.UI.OFFICE__ICON_TOP
                pnlIcon3.Left = _margin + pnlIcon1.Width + _margin + pnlIcon2.Width + _margin
                picIcon3.Image = _bitmaps.Item(2).Icon
                lblIcon3.Text = _bitmaps.Item(2).Description
                AddHandler picIcon3.MouseClick, Sub(_sender, _e) picIcon_Clicked(_sender, _e, _bitmaps.Item(2))
        End Select


        progressPanelAutoclose.BackgroundColor = Color.Transparent
        progressPanelAutoclose.Border = True
        progressPanelAutoclose.ProgressBarColor = Color.Green
        progressPanelAutoclose.ProgressBarBorder = True
        progressPanelAutoclose.AnimationSpeed = 10
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.ValueMin = 0
        progressPanelAutoclose.ValueMax = mAutoCloseCounterMAX
        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.Initialise()


        tmrAutoclose.Interval = mAutoCloseCounterInterval
        tmrAutoclose.Enabled = True
    End Sub

    Private Sub picIcon_Clicked(sender As Object, e As EventArgs, oOffice As Office._office_info)
        NamedPipe.Logger.Client.Message("Loading office app:" & oOffice.Path)
        NamedPipe.UI.Client.OfficeSelected(oOffice.Description)

        SessionStuff.SessionAction.Add_AppByPath(oOffice.Path)


        CloseOffice()
    End Sub

    Private Sub CloseOffice()
        Close()
    End Sub

    Private Sub picClose_Click(sender As Object, e As EventArgs)
        CloseOffice
    End Sub


    Private Sub tmrAutoclose_Tick(sender As Object, e As EventArgs) Handles tmrAutoclose.Tick
        If InputCheck.HasActivity Then
            mAutoCloseCounter = mAutoCloseCounterMAX
        End If

        redrawFakeProgressBar()
        mAutoCloseCounter -= 1
        If mAutoCloseCounter < 0 Then
            NamedPipe.UI.Client.OfficeTimeout()
            CloseOffice()
        End If
    End Sub

    Private Sub redrawFakeProgressBar()
        If mAutoCloseCounter < 0 Then Exit Sub

        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.ProgressBarColor = Helpers.UI.ProgressColor(mAutoCloseCounter, mAutoCloseCounterMAX)
    End Sub

    Private Sub frmOffice_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        NamedPipe.UI.Client.OfficeClosed()
    End Sub
End Class