﻿Imports System.Windows.Forms.VisualStyles.VisualStyleElement.Window
Imports Microsoft.Web.WebView2.Core

Public Class frmCustom
    Private WithEvents corewebview2_Custom As CoreWebView2
    Private mNavigationCancelled As Boolean = False

    Private mWebviewLoaded As Boolean = False

    Private ReadOnly mAutoCloseCounterInterval As Integer = 25
    Private mAutoCloseCounterMAX As Integer = Globals.Session.CustomDialog.AutoCloseDelay * (1000 / mAutoCloseCounterInterval)
    Private mAutoCloseCounter As Integer = mAutoCloseCounterMAX


    '--------------------------------------------------------------------------------------------------
    'prevent form moving
    Protected Overrides Sub WndProc(ByRef m As Message)
        Const WM_NCLBUTTONDOWN As Integer = 161
        Const WM_SYSCOMMAND As Integer = 274
        Const HTCAPTION As Integer = 2
        Const SC_MOVE As Integer = 61456

        If (m.Msg = WM_SYSCOMMAND) And (m.WParam.ToInt32() = SC_MOVE) Then
            Return
        End If

        If (m.Msg = WM_NCLBUTTONDOWN) And (m.WParam.ToInt32() = HTCAPTION) Then
            Return
        End If

        MyBase.WndProc(m)
    End Sub
    '--------------------------------------------------------------------------------------------------


    Private Sub CloseCustom()
        NamedPipe.UI.Client.CustomClosed()

        Close()
    End Sub

    Private Sub picClose_Click(sender As Object, e As EventArgs)
        CloseCustom()
    End Sub

    Private Sub frmCustom_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Visible = False
        Helpers.Forms.TopMost(Me.Handle, True)


        NamedPipe.UI.Client.CustomDialog()

        NamedPipe.Logger.Client.Debug("CUSTOM", Globals.Session.CustomDialog.DebugString)
    End Sub

    Public Sub Initialise()
        If Globals.Session.CustomDialog.FormWidth < 240 Then Globals.Session.CustomDialog.FormWidth = 240
        If Globals.Session.CustomDialog.FormWidth > Screen.PrimaryScreen.Bounds.Width Then Globals.Session.CustomDialog.FormWidth = Screen.PrimaryScreen.Bounds.Width
        If Globals.Session.CustomDialog.FormHeight < 160 Then Globals.Session.CustomDialog.FormHeight = 160
        If Globals.Session.CustomDialog.FormHeight > Screen.PrimaryScreen.Bounds.Height Then Globals.Session.CustomDialog.FormHeight = Screen.PrimaryScreen.Bounds.Height


        Me.Width = Globals.Session.CustomDialog.FormWidth
        Me.Height = Globals.Session.CustomDialog.FormHeight
        Me.Left = (frmMain.Width - Me.Width) / 2
        Me.Top = (frmMain.Height - Me.Height) / 2

        pnlContent.Dock = DockStyle.Fill
        webview2_CustomContent.Dock = DockStyle.Fill


        If Globals.Session.CustomDialog.AutoCloseDelay > 0 Then
            progressPanelAutoclose.BackgroundColor = Color.Transparent
            progressPanelAutoclose.Border = True
            progressPanelAutoclose.ProgressBarColor = Color.Green
            progressPanelAutoclose.ProgressBarBorder = True
            progressPanelAutoclose.AnimationSpeed = 10
            progressPanelAutoclose.AnimationWidth = 25
            progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
            progressPanelAutoclose.ValueMin = 0
            progressPanelAutoclose.ValueMax = mAutoCloseCounterMAX
            progressPanelAutoclose.Value = mAutoCloseCounter
            progressPanelAutoclose.Initialise()

            mAutoCloseCounterMAX = Globals.Session.CustomDialog.AutoCloseDelay * (1000 / mAutoCloseCounterInterval)
            mAutoCloseCounter = mAutoCloseCounterMAX
            tmrAutoclose.Interval = mAutoCloseCounterInterval
            tmrAutoclose.Enabled = True
        Else
            progressPanelAutoclose.Visible = False
        End If


        Me.Text = Globals.Session.CustomDialog.FormTitle


        InitialiseMainWebView()
    End Sub

    Private Async Sub InitialiseMainWebView()
        Dim cwEnvironment As CoreWebView2Environment = Nothing
        Dim sUserDataFolder As String = IO.Path.Combine(Settings.Constants.Paths.Files.Data.USER_DATA_FOLDER, My.Application.Info.ProductName)

        NamedPipe.Logger.Client.Debug("CUSTOM", "Webview2 user data: " & sUserDataFolder)

        cwEnvironment = Await CoreWebView2Environment.CreateAsync(Nothing, sUserDataFolder, Nothing)

        Await webview2_CustomContent.EnsureCoreWebView2Async(cwEnvironment)


        corewebview2_Custom = webview2_CustomContent.CoreWebView2
        webview2_CustomContent.CoreWebView2.AddHostObjectToScript(Settings.Constants.Applications.Homepage.WebBrowserScriptInterface_Name, New WebBrowserScriptInterface())
        webview2_CustomContent.CoreWebView2.Settings.AreDevToolsEnabled = False
        webview2_CustomContent.CoreWebView2.Settings.AreDefaultContextMenusEnabled = False


        NamedPipe.Logger.Client.Message("CUSTOM", "Loading page: " & Globals.Session.CustomDialog.ContentUrl)

        webview2_CustomContent.CoreWebView2.Navigate(Globals.Session.CustomDialog.ContentUrl)
    End Sub

#Region "events"
#Region "webview2"
    Private Sub corewebview2_Custom_DOMContentLoaded(sender As Object, e As CoreWebView2DOMContentLoadedEventArgs) Handles corewebview2_Custom.DOMContentLoaded
        mWebviewLoaded = True

        Me.Visible = True


        NamedPipe.Logger.Client.Debug("CUSTOM", "corewebview2_Custom_DOMContentLoaded: " & webview2_CustomContent.CoreWebView2.Source)

        If Globals.Security.PermittedScriptingHosts.Exists(webview2_CustomContent.CoreWebView2.Source) Then
            NamedPipe.Logger.Client.Debug("CUSTOM", "Script interface permitted")
        Else
            NamedPipe.Logger.Client.Warning("CUSTOM", "Script interface not permitted from url: " & webview2_CustomContent.CoreWebView2.Source)

            webview2_CustomContent.CoreWebView2.RemoveHostObjectFromScript(Settings.Constants.Applications.Homepage.WebBrowserScriptInterface_Name)
        End If
    End Sub

    Private Sub corewebview2_Main_NewWindowRequested(sender As Object, e As CoreWebView2NewWindowRequestedEventArgs) Handles corewebview2_Custom.NewWindowRequested
        e.Handled = True
    End Sub
#End Region

#Region "timer"
    Private Sub tmrAutoclose_Tick(sender As Object, e As EventArgs) Handles tmrAutoclose.Tick
        If InputCheck.HasActivity Then
            mAutoCloseCounter = mAutoCloseCounterMAX
        End If

        redrawFakeProgressBar()
        mAutoCloseCounter -= 1
        If mAutoCloseCounter < 0 Then
            NamedPipe.UI.Client.CustomTimeout()
            CloseCustom()
        End If
    End Sub

    Private Sub redrawFakeProgressBar()
        If mAutoCloseCounter < 0 Then Exit Sub

        progressPanelAutoclose.Value = mAutoCloseCounter
        progressPanelAutoclose.ProgressBarColor = Helpers.UI.ProgressColor(mAutoCloseCounter, mAutoCloseCounterMAX)
    End Sub
#End Region
#End Region
End Class