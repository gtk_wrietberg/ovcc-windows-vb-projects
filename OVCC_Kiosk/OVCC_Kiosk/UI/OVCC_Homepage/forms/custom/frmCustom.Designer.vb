﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCustom
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        tmrAutoclose = New Timer(components)
        progressPanelAutoclose = New ProgressPanel()
        pnlContent = New Panel()
        webview2_CustomContent = New Microsoft.Web.WebView2.WinForms.WebView2()
        pnlContent.SuspendLayout()
        CType(webview2_CustomContent, ComponentModel.ISupportInitialize).BeginInit()
        SuspendLayout()
        ' 
        ' tmrAutoclose
        ' 
        tmrAutoclose.Interval = 50
        ' 
        ' progressPanelAutoclose
        ' 
        progressPanelAutoclose.AnimationSpeed = 50R
        progressPanelAutoclose.AnimationType = ProgressPanel.ANIMATION_TYPES.VALUE
        progressPanelAutoclose.AnimationWidth = 25
        progressPanelAutoclose.BackColor = Color.Transparent
        progressPanelAutoclose.BackgroundColor = Color.Empty
        progressPanelAutoclose.Border = False
        progressPanelAutoclose.Dock = DockStyle.Bottom
        progressPanelAutoclose.Location = New Point(3, 570)
        progressPanelAutoclose.Name = "progressPanelAutoclose"
        progressPanelAutoclose.ProgressBarBorder = False
        progressPanelAutoclose.ProgressBarColor = Color.Empty
        progressPanelAutoclose.Size = New Size(874, 4)
        progressPanelAutoclose.TabIndex = 9
        progressPanelAutoclose.Value = 0R
        progressPanelAutoclose.ValueMax = 100R
        progressPanelAutoclose.ValueMin = 0R
        ' 
        ' pnlContent
        ' 
        pnlContent.Controls.Add(webview2_CustomContent)
        pnlContent.Location = New Point(0, 37)
        pnlContent.Name = "pnlContent"
        pnlContent.Size = New Size(753, 536)
        pnlContent.TabIndex = 10
        ' 
        ' webview2_CustomContent
        ' 
        webview2_CustomContent.AllowExternalDrop = True
        webview2_CustomContent.CreationProperties = Nothing
        webview2_CustomContent.DefaultBackgroundColor = Color.White
        webview2_CustomContent.Dock = DockStyle.Fill
        webview2_CustomContent.Location = New Point(0, 0)
        webview2_CustomContent.Name = "webview2_CustomContent"
        webview2_CustomContent.Size = New Size(753, 536)
        webview2_CustomContent.TabIndex = 0
        webview2_CustomContent.ZoomFactor = 1R
        ' 
        ' frmCustom
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.WhiteSmoke
        ClientSize = New Size(880, 577)
        Controls.Add(pnlContent)
        Controls.Add(progressPanelAutoclose)
        Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.FixedDialog
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmCustom"
        Padding = New Padding(3)
        ShowIcon = False
        ShowInTaskbar = False
        StartPosition = FormStartPosition.CenterScreen
        pnlContent.ResumeLayout(False)
        CType(webview2_CustomContent, ComponentModel.ISupportInitialize).EndInit()
        ResumeLayout(False)
    End Sub
    Friend WithEvents tmrAutoclose As Timer
    Friend WithEvents progressPanelAutoclose As ProgressPanel
    Friend WithEvents pnlContent As Panel
    Friend WithEvents webview2_CustomContent As Microsoft.Web.WebView2.WinForms.WebView2
End Class
