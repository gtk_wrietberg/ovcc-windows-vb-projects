﻿Imports System.ComponentModel
Imports System.Reflection.Emit

Public Class ProgressPanel
    Private ReadOnly PROGRESSBAR_MARGIN As Integer = 2
    Private ReadOnly DEFAULT_AnimationSpeed As Double = 50.0
    Private ReadOnly DEFAULT_AnimationWidth As Integer = 25


    Public Enum ANIMATION_TYPES As Integer
        VALUE = 0
        LEFT_RIGHT = 1
        RIGHT_LEFT = 2
        KITT = 3
    End Enum


#Region "properties"
    Private mBackgroundColor As Color
    <Browsable(True)>
    Public Property BackgroundColor() As Color
        Get
            Return mBackgroundColor
        End Get
        Set(ByVal value As Color)
            mBackgroundColor = value
            '_Draw_Value()
        End Set
    End Property

    Private mProgressBarColor As Color
    <Browsable(True)>
    Public Property ProgressBarColor() As Color
        Get
            Return mProgressBarColor
        End Get
        Set(ByVal value As Color)
            mProgressBarColor = value
            '_Draw_Value()
        End Set
    End Property

    Private mBorder As Boolean
    <Browsable(True)>
    Public Property Border() As Boolean
        Get
            Return mBorder
        End Get
        Set(ByVal value As Boolean)
            mBorder = value
            '_Draw_Value()
        End Set
    End Property

    Private mProgressBarBorder As Boolean
    <Browsable(True)>
    Public Property ProgressBarBorder() As Boolean
        Get
            Return mProgressBarBorder
        End Get
        Set(ByVal value As Boolean)
            mProgressBarBorder = value
            '_Draw_Value()
        End Set
    End Property

    Private mValue As Double
    <Browsable(True)>
    Public Property Value() As Double
        Get
            Return mValue
        End Get
        Set(ByVal value As Double)
            mValue = value
            _Draw_Value()
        End Set
    End Property

    Private mValueMin As Double = 0.0
    <Browsable(True)>
    Public Property ValueMin() As Double
        Get
            Return mValueMin
        End Get
        Set(ByVal value As Double)
            mValueMin = value
            '_Draw_Value()
        End Set
    End Property

    Private mValueMax As Double = 100.0
    <Browsable(True)>
    Public Property ValueMax() As Double
        Get
            Return mValueMax
        End Get
        Set(ByVal value As Double)
            mValueMax = value
            '_Draw_Value()
        End Set
    End Property

    Private mAnimationType As ANIMATION_TYPES
    <Browsable(True)>
    Public Property AnimationType() As ANIMATION_TYPES
        Get
            Return mAnimationType
        End Get
        Set(ByVal value As ANIMATION_TYPES)
            mAnimationType = value
        End Set
    End Property

    Private mAnimationSpeed As Double = DEFAULT_AnimationSpeed
    <Browsable(True)>
    Public Property AnimationSpeed() As Double
        Get
            Return mAnimationSpeed
        End Get
        Set(ByVal value As Double)
            mAnimationSpeed = value
        End Set
    End Property

    Private mAnimationDirectory As Integer = 1
    Public ReadOnly Property AnimationDirectory() As Integer
        Get
            Return mAnimationDirectory
        End Get
    End Property

    Private mAnimationWidth As Integer = DEFAULT_AnimationWidth
    <Browsable(True)>
    Public Property AnimationWidth() As Integer
        Get
            Return mAnimationWidth
        End Get
        Set(ByVal value As Integer)
            mAnimationWidth = value
        End Set
    End Property

    Public ReadOnly Property IsFull() As Boolean
        Get
            Return IIf(Me.Value >= 100, True, False)
        End Get
    End Property

    Public ReadOnly Property IsEmpty() As Boolean
        Get
            Return IIf(Me.Value <= 0, True, False)
        End Get
    End Property

    Private mIsAnimated As Boolean = False
    Public ReadOnly Property IsAnimated() As Boolean
        Get
            Return mIsAnimated
        End Get
    End Property
#End Region


    '--------------------------------------------------------------------------------------------------------------------------------------------------
    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Initialise()
    End Sub

    Public Sub Initialise()
        _ResizeControls()

        mValue = 0
        _Draw_Value()
    End Sub

    Public Sub StartAnimation()
        mIsAnimated = True
        _Animate_reset()

        mValue = 0

        Me.tmrAnimate.Enabled = True
    End Sub

    Public Sub StopAnimation()
        Me.tmrAnimate.Enabled = False
        _Animate_reset()
        _Draw_Value()

        mIsAnimated = False
    End Sub


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Private Sub _Animate()
        If mAnimationSpeed < 1.0 Then mAnimationSpeed = 1.0
        If mAnimationSpeed > 20.0 Then mAnimationSpeed = 20.0

        Select Case mAnimationType
            Case ANIMATION_TYPES.LEFT_RIGHT
                _Animate__LEFT_RIGHT()
                _Draw_Animation()
            Case ANIMATION_TYPES.RIGHT_LEFT
                _Animate__RIGHT_LEFT()
                _Draw_Animation()
            Case ANIMATION_TYPES.KITT
                _Animate__KITT()
                _Draw_Animation()
            Case Else
                Me.StopAnimation()
        End Select
    End Sub

    Private Sub _Animate_reset()
        mValue = 0
        mValueMin = 0
        mValueMax = 100
        mAnimationDirectory = 1
    End Sub

    Private Sub _Animate__LEFT_RIGHT()
        mValue += (mAnimationSpeed / 10)

        If mValue > mValueMax Then mValue = mValueMin
    End Sub

    Private Sub _Animate__RIGHT_LEFT()
        mValue -= (mAnimationSpeed / 10)

        If mValue < mValueMin Then mValue = mValueMax
    End Sub

    Private Sub _Animate__KITT()
        mValue += ((mAnimationSpeed / 10) * mAnimationDirectory)

        If mValue > mValueMax Then
            mValue = mValueMax
            mAnimationDirectory = -mAnimationDirectory
        End If
        If mValue < mValueMin Then
            mValue = mValueMin
            mAnimationDirectory = -mAnimationDirectory
        End If
    End Sub

    Private Sub _ResizeControls()
        Me.pnlBar.Top = 0
        Me.pnlBar.Left = 0
        Me.pnlBar.Height = Me.Height
    End Sub

    Private Sub _Draw_Value()
        If mIsAnimated Then Exit Sub

        If mValue < mValueMin Then mValue = mValueMin
        If mValue > mValueMax Then mValue = mValueMax

        Me.pnlBar.Left = 0
        Me.pnlBar.Width = CInt(Me.Width * (mValue / mValueMax))

        _Draw()
    End Sub

    Private Sub _Draw_Animation()
        Dim _width As Integer = CInt((mAnimationWidth / mValueMax) * Me.Width)
        If _width < 0 Then _width = 0
        Dim _left As Integer = CInt((Me.Width + _width) * (mValue / mValueMax)) - _width

        Me.pnlBar.Left = _left
        Me.pnlBar.Width = _width

        _Draw()
    End Sub

    Private Sub _Draw()
        pnlContainer.BorderStyle = IIf(mBorder, BorderStyle.FixedSingle, BorderStyle.None)
        pnlContainer.BackColor = mBackgroundColor

        Me.pnlBar.BackColor = mProgressBarColor
        Me.pnlBar.BorderStyle = IIf(mProgressBarBorder, BorderStyle.FixedSingle, BorderStyle.None)
        Me.BackColor = Color.Transparent
    End Sub

    Private Sub CustomProgressBar_Paint(sender As Object, e As PaintEventArgs) Handles Me.Paint
        '_ResizeControls()
    End Sub

    Private Sub tmrAnimate_Tick(sender As Object, e As EventArgs) Handles tmrAnimate.Tick
        If Not mIsAnimated Then
            tmrAnimate.Enabled = False
            Exit Sub
        End If

        _Animate()
    End Sub
End Class
