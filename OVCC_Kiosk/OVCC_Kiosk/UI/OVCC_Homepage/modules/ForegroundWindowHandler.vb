﻿Module ForegroundWindowHandler
    <Runtime.InteropServices.DllImport("user32.dll")>
    Private Function SetForegroundWindow(ByVal hWnd As IntPtr) As Boolean
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")>
    Private Function GetForegroundWindow() As IntPtr
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")>
    Private Function GetWindowText(ByVal hWnd As IntPtr, ByVal lpWindowText As System.Text.StringBuilder,
    ByVal nMaxCount As Integer) As Integer
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")>
    Private Function GetClassName(ByVal hWnd As IntPtr, ByVal lpWindowText As System.Text.StringBuilder,
    ByVal nMaxCount As Integer) As Integer
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")>
    Private Function GetActiveWindow() As IntPtr
    End Function


    Public Function ForegroundWindowHandler__GetWindowClassname() As String
        Dim classname As New System.Text.StringBuilder(255)
        Dim classnameLength As Integer = GetClassName(GetForegroundWindow(), classname, classname.Capacity + 1)
        classname.Length = classnameLength

        Return classname.ToString
    End Function


    Public Function ForegroundWindowHandler__GetDetails() As ForegroundWindow
        Dim classname As New System.Text.StringBuilder(255)
        Dim classnameLength As Integer = GetClassName(GetForegroundWindow(), classname, classname.Capacity + 1)
        classname.Length = classnameLength

        Dim title As New System.Text.StringBuilder(255)
        Dim titleLength As Integer = GetWindowText(GetForegroundWindow(), title, title.Capacity + 1)
        title.Length = titleLength

        Dim w As New ForegroundWindow
        w.Title = title.ToString()
        w.ClassName = classname.ToString
        w.Handle = GetForegroundWindow()
        Return w
    End Function

    Public Function ForegroundWindowHandler__SetForegroundWindow(ByVal hWnd As IntPtr) As Boolean
        Return SetForegroundWindow(hWnd)
    End Function


    ''' <summary>
    ''' Represents an open, visible window
    ''' </summary>
    Public Structure ForegroundWindow
        Public Handle As IntPtr
        Public Title As String
        Public ClassName As String
    End Structure

    Public Function ForegroundWindowHandler__IsCurrentWindowActive(WindowHandle As IntPtr) As Boolean
        Return GetActiveWindow() = WindowHandle
    End Function
End Module
