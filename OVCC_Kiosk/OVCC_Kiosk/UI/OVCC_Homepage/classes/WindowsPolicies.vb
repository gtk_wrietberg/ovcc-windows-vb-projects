﻿
Public Class WindowsPolicies
    Private Shared Sub _toggle_reg_key(KeyName As String, ValueName As String, Value As Boolean)
        Helpers.Registry.SetValue_Integer(KeyName, ValueName, If(Value, 1, 0))
    End Sub

    Private Class _regkeys
        Public Class TaskManager
            Public Shared Keys As String() = {
                "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies",
                "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System"
            }

            Public Shared Values As String() = {
                "DisableTaskMgr",
                "DisableTaskMgr"
            }
        End Class

        Public Class ControlPanel
            Public Shared Keys As String() = {
                "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
            }

            Public Shared Values As String() = {
                "NoControlPanel"
            }
        End Class

        Public Class WindowsScreensaver
            Public Shared Keys As String() = {
                "HKEY_CURRENT_USER\Software\Policies\Microsoft\Windows\Control Panel\Desktop",
                "HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\System"
            }

            Public Shared Values As String() = {
                "ScreenSaveActive",
                "NoDispScrSavPage"
            }
        End Class
    End Class

    Public Class Toggler
        Private mDisabled As Boolean = False
        Public ReadOnly Property IsDisabled() As Boolean
            Get
                Return mDisabled
            End Get
        End Property

        Private mKeys As String()
        Private mValues As String()

        Public Sub New(_keys As String(), _values As String())
            mKeys = _keys
            mValues = _values
        End Sub

        Public Sub Enable()
            mDisabled = False

            _toggle()
        End Sub

        Public Sub Disable()
            mDisabled = True

            _toggle()
        End Sub

        Private Sub _toggle()
            For i As Integer = 0 To mKeys.Length - 1
                Helpers.Registry.SetValue_Integer(mKeys(i), mValues(i), If(mDisabled, 1, 0))
            Next
        End Sub
    End Class

    Public Shared TaskManager As New Toggler(_regkeys.TaskManager.Keys, _regkeys.TaskManager.Values)
    Public Shared ControlPanel As New Toggler(_regkeys.ControlPanel.Keys, _regkeys.ControlPanel.Values)
    Public Shared WindowsScreensaver As New Toggler(_regkeys.WindowsScreensaver.Keys, _regkeys.WindowsScreensaver.Values)
End Class
