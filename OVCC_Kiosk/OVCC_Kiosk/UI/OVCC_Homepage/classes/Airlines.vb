﻿Public Class Airlines
    Public Class Airline
        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mUrl As String
        Public Property Url() As String
            Get
                Return mUrl
            End Get
            Set(ByVal value As String)
                mUrl = value
            End Set
        End Property

        Private mIcon As String
        Public Property Icon() As String
            Get
                Return mIcon
            End Get
            Set(ByVal value As String)
                mIcon = value
            End Set
        End Property

        Private mIconAbsolute As String
        Public Property IconAbsolute() As String
            Get
                Return mIconAbsolute
            End Get
            Set(ByVal value As String)
                mIconAbsolute = value
            End Set
        End Property

        Private mIconTop As Integer
        Public Property IconTop() As Integer
            Get
                Return mIconTop
            End Get
            Set(ByVal value As Integer)
                mIconTop = value
            End Set
        End Property

        Private mIconLeft As Integer
        Public Property IconLeft() As Integer
            Get
                Return mIconLeft
            End Get
            Set(ByVal value As Integer)
                mIconLeft = value
            End Set
        End Property
    End Class

    Public Shared AirlinesList As New List(Of Airline)

    Public Shared Function LoadAirlines() As Boolean
        Try
            AirlinesList = New List(Of Airline)

            Dim mDocument_Airlines As System.Xml.XmlDocument = New System.Xml.XmlDocument
            mDocument_Airlines.LoadXml(_File2String(Settings.Constants.Paths.Files.Data.AIRLINES_FILE))

            Dim mAirlineList As System.Xml.XmlNodeList = mDocument_Airlines.SelectNodes("/airlines/airline")

            'Console.WriteLine(mLanguageList.Count)

            For Each mAirline As System.Xml.XmlNode In mAirlineList
                Dim _airline As New Airline

                _airline.Name = mAirline.SelectSingleNode("name").InnerText
                _airline.Url = mAirline.SelectSingleNode("url").InnerText
                _airline.Icon = mAirline.SelectSingleNode("icon").InnerText
                _airline.IconAbsolute = IO.Path.Combine(Settings.Constants.Paths.Folders.Kiosk_AirlineImages, mAirline.SelectSingleNode("icon").InnerText)
                _airline.IconTop = 0
                _airline.IconLeft = 0

                AirlinesList.Add(_airline)
            Next


            Return True
        Catch ex As Exception
            NamedPipe.Logger.Client.Error("LoadAirlines() error: " & ex.Message)
            mlastError = ex.Message
            Return False
        End Try
    End Function

    Public Shared Sub SortByName()
        AirlinesList = AirlinesList.OrderBy(Function(x) x.Name).ToList()
    End Sub

    Public Shared ReadOnly Property AirlinesCount As Integer
        Get
            Return AirlinesList.Count
        End Get
    End Property

    Private Shared mlastError As String
    Public Shared ReadOnly Property LastError() As String
        Get
            Return mlastError
        End Get
    End Property

    Private Shared Function _String2Int(s As String) As Integer
        Dim _tmp As Integer = 0
        If Integer.TryParse(s, _tmp) Then

        End If

        Return _tmp
    End Function

    Private Shared Function _File2String(f As String) As String
        Dim fileBytes As Byte()

        fileBytes = System.IO.File.ReadAllBytes(f)

        Return System.Text.Encoding.UTF8.GetString(fileBytes)
    End Function
End Class
