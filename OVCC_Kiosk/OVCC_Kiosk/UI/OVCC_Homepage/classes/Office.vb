﻿Imports System.Windows.Forms.VisualStyles.VisualStyleElement

Public Class Office
    Public Class _office_info
        Private mBitmap As Bitmap
        Public Property Icon() As Bitmap
            Get
                Return mBitmap
            End Get
            Set(ByVal value As Bitmap)
                mBitmap = value
            End Set
        End Property

        Private mDescription As String
        Public Property Description() As String
            Get
                Return mDescription
            End Get
            Set(ByVal value As String)
                mDescription = value
            End Set
        End Property

        Private mPath As String
        Public Property Path() As String
            Get
                Return mPath
            End Get
            Set(ByVal value As String)
                mPath = value
            End Set
        End Property

        Public Sub New()
            mBitmap = Nothing
            mDescription = ""
            mPath = ""
        End Sub

        Public Sub New(oBitmap As Bitmap, sDescription As String, sPath As String)
            mBitmap = oBitmap
            mDescription = sDescription
            mPath = sPath
        End Sub
    End Class

    Public Shared Function IsOfficeInstalled() As Boolean
        Dim extensions_word As String = Settings.Get.String(
            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__OfficeExtensions_Word,
            Settings.Constants.Registry.OVCC.Kiosk.DEFAULTVALUE__Settings__OfficeExtensions_Word)

        Dim extensions_excel As String = Settings.Get.String(
            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__OfficeExtensions_Excel,
            Settings.Constants.Registry.OVCC.Kiosk.DEFAULTVALUE__Settings__OfficeExtensions_Excel)

        Dim extensions_powerpoint As String = Settings.Get.String(
            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__OfficeExtensions_Powerpoint,
            Settings.Constants.Registry.OVCC.Kiosk.DEFAULTVALUE__Settings__OfficeExtensions_Powerpoint)

        Dim extensions As String = extensions_word & Globals.ExternalApplications.OfficeExtensionsSeparatorCharacter & extensions_excel & Globals.ExternalApplications.OfficeExtensionsSeparatorCharacter & extensions_powerpoint

        Dim _hasOffice As Boolean = LinkExtensionToApplication.HasApplication(extensions)
        NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "IsOfficeInstalled(): " & _hasOffice.ToString)

        Return _hasOffice
    End Function


    Public Shared Function GetInfo_Word() As _office_info
        Return _GetInfo("word")
    End Function

    Public Shared Function GetInfo_Excel() As _office_info
        Return _GetInfo("excel")
    End Function

    Public Shared Function GetInfo_Powerpoint() As _office_info
        Return _GetInfo("powerpoint")
    End Function

    Private Shared Function _GetInfo(Name As String) As _office_info
        Dim _info As New _office_info

        Dim _name As String = ""
        Dim _application As String = ""
        Dim _extensions As String = ""

        Select Case Name
            Case "word"
                _extensions = Settings.Get.String(
                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__OfficeExtensions_Word,
                    Settings.Constants.Registry.OVCC.Kiosk.DEFAULTVALUE__Settings__OfficeExtensions_Word)
            Case "excel"
                _extensions = Settings.Get.String(
                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__OfficeExtensions_Excel,
                    Settings.Constants.Registry.OVCC.Kiosk.DEFAULTVALUE__Settings__OfficeExtensions_Excel)
            Case "powerpoint"
                _extensions = Settings.Get.String(
                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings,
                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__OfficeExtensions_Powerpoint,
                    Settings.Constants.Registry.OVCC.Kiosk.DEFAULTVALUE__Settings__OfficeExtensions_Powerpoint)
            Case Else
                Return _info
        End Select


        Dim aExtensions As String() = _extensions.Split(Globals.ExternalApplications.OfficeExtensionsSeparatorCharacter)
        Dim bRet As Boolean = False

        For Each _extension In aExtensions
            If LinkExtensionToApplication.GetInfo(_extension, _name, _application) Then
                Dim _icon As Icon = Icon.ExtractIcon(_application, 0, Settings.Constants.UI.OFFICE__ICON_SIZE)

                _info.Icon = _icon.ToBitmap
                _info.Description = _name
                _info.Path = _application

                Return _info
            End If
        Next

        Return _info
    End Function
End Class
