﻿Public Class Hotkey
    Private Declare Function RegisterHotKey Lib "user32" (ByVal hwnd As IntPtr, ByVal id As Integer, ByVal fsModifiers As Integer, ByVal vk As Integer) As Integer
    Private Declare Function UnregisterHotKey Lib "user32" (ByVal hwnd As IntPtr, ByVal id As Integer) As Integer

    Public Const WM_HOTKEY As Integer = &H312


    Public Enum HOTKEYS
        ADMIN_DIALOG = 1

    End Enum


    Enum KeyModifier
        None = 0
        Alt = &H1
        Control = &H2
        Shift = &H4
        Winkey = &H8
    End Enum

    Private Shared mHotkeyList As New List(Of Integer)

    Private Shared mFormHandle As IntPtr
    Public Shared Property FormHandle() As IntPtr
        Get
            Return mFormHandle
        End Get
        Set(ByVal value As IntPtr)
            mFormHandle = value
        End Set
    End Property

    Private Shared Function _Exists(Id As Integer) As Boolean
        For Each _id As Integer In mHotkeyList
            If _id = Id Then
                Return True
            End If
        Next

        Return False
    End Function

    ''' <summary>
    ''' Registers hotkey
    ''' </summary>
    ''' <param name="Id">hotkey id</param>
    ''' <param name="KeyCode">key code</param>
    ''' <param name="Modifier">modifier enum</param>
    ''' <returns></returns>
    Public Shared Function Register(Id As Integer, KeyCode As Integer, Modifier As KeyModifier) As Boolean
        If _Exists(Id) Then
            Unregister(Id, True)
        End If

        RegisterHotKey(mFormHandle, Id, Modifier, KeyCode)

        mHotkeyList.Add(Id)

        Return True
    End Function

    ''' <summary>
    ''' Unregister all hotkeys
    ''' </summary>
    Public Shared Sub Unregister()
        For Each _id As Integer In mHotkeyList.ToList
            Unregister(_id, False)
        Next

        mHotkeyList.Clear()
    End Sub

    ''' <summary>
    ''' Unregister hotkey by id, and remove from internal list
    ''' </summary>
    ''' <param name="Id">hotkey id</param>
    Public Shared Sub Unregister(Id As Integer)
        Unregister(Id, True)
    End Sub

    ''' <summary>
    ''' Unregister hotkey by id
    ''' </summary>
    ''' <param name="Id">hotkey id</param>
    ''' <param name="RemoveFromList">remove from list or not</param>
    Public Shared Sub Unregister(Id As Integer, RemoveFromList As Boolean)
        UnregisterHotKey(mFormHandle, Id)

        If RemoveFromList Then
            For _i As Integer = 0 To mHotkeyList.Count - 1
                If mHotkeyList.Item(_i) = Id Then
                    mHotkeyList.RemoveAt(_i)
                End If
            Next
        End If
    End Sub
End Class