﻿Imports System.Management
Imports System.Runtime.InteropServices
Imports System.Security
Imports System.Text

Public Class ProcessWatcher
    Private WithEvents oProcessStartWatcher As ManagementEventWatcher
    Private WithEvents oProcessStopWatcher As ManagementEventWatcher

    Private pidExplorer As Integer = -1
    Private pidHomepage As Integer = -1
    Private mLoopEnabled As Boolean = False

    Private mIsStarted As Boolean = False

    Private mThread_ExplorerProcessWatcher As Threading.Thread


    Public Event Event__ProcessStartWatcher_Started()
    Public Event Event__ProcessStartWatcher_Stopped()

    Public Event Event__ProcessStopWatcher_Started()
    Public Event Event__ProcessStopWatcher_Stopped()

    Public Event Event__Explorer_IdChanged(ByVal PreviousProcessId As Integer, ByVal ProcessId As Integer)
    Public Event Event__Explorer_NotFound()
    Public Event Event__Explorer_Exception(ByVal ErrorMessage As String)

    Public Event Event__AppStarted_System(ByVal ProcessName As String, ByVal ProcessId As Integer, ByVal ProcessOwner As String)
    Public Event Event__AppStarted_Homepage(ByVal ProcessName As String, ByVal ProcessId As Integer, ByVal ProcessOwner As String)
    Public Event Event__AppStarted_Unknown(ByVal ProcessName As String, ByVal ProcessId As Integer, ByVal ProcessOwner As String)

    Public Event Event__AppStopped(ByVal ProcessName As String, ByVal ProcessId As Integer)

    Public Event Event__AppStarted_Exception(ByVal ErrorMessage As String)
    Public Event Event__AppStopped_Exception(ByVal ErrorMessage As String)

    Public Event Event__AppOwner_Exception(ByVal ProcessName As String, ByVal ProcessId As Integer, ByVal ErrorMessage As String)

    Public Event Event__DEBUG(DebugMessage As String)


    Public ReadOnly Property IsRunning() As String
        Get
            Return mLoopEnabled
        End Get
    End Property

    Public Sub Start()
        mLoopEnabled = True

        mThread_ExplorerProcessWatcher = New Threading.Thread(New Threading.ThreadStart(AddressOf _ExplorerProcessWatcher))
        mThread_ExplorerProcessWatcher.Start()

        oProcessStartWatcher = New ManagementEventWatcher("\\.\root\CIMV2", "SELECT * FROM __InstanceCreationEvent WITHIN .025 WHERE TargetInstance ISA 'Win32_Process'")
        oProcessStartWatcher.Start()
        RaiseEvent Event__ProcessStartWatcher_Started()

        oProcessStopWatcher = New ManagementEventWatcher("\\.\root\CIMV2", "SELECT * FROM __InstanceDeletionEvent  WITHIN .025 WHERE TargetInstance ISA 'Win32_Process'")
        oProcessStopWatcher.Start()
        RaiseEvent Event__ProcessStopWatcher_Started()


        mIsStarted = True
        RaiseEvent Event__DEBUG("started")
    End Sub

    Public Sub [Stop]()
        If mIsStarted Then
            oProcessStartWatcher.Stop()
            RaiseEvent Event__ProcessStartWatcher_Stopped()

            oProcessStopWatcher.Stop()
            RaiseEvent Event__ProcessStopWatcher_Stopped()
        End If

        mLoopEnabled = False
    End Sub

    Private Sub _ExplorerProcessWatcher()
        Do
            Try
                Dim pProcesses As Process() = Process.GetProcessesByName(Settings.Constants.Paths.Files.Misc.EXPLORER_PROCESS_NAME)

                RaiseEvent Event__DEBUG("explorer processes: " & pProcesses.Count.ToString)

                If pProcesses.Count > 0 Then
                    Dim pidExplorerPrev As Integer = pidExplorer
                    pidExplorer = pProcesses.First.Id

                    RaiseEvent Event__DEBUG("explorer pid: " & pidExplorer.ToString)
                    RaiseEvent Event__Explorer_IdChanged(pidExplorerPrev, pidExplorer)
                Else
                    RaiseEvent Event__DEBUG("explorer pid: not found")
                    RaiseEvent Event__Explorer_NotFound()
                End If
            Catch ex As Exception
                RaiseEvent Event__Explorer_Exception(ex.Message)
            End Try


            Try
                pidHomepage = Environment.ProcessId
            Catch ex As Exception

            End Try


            For i As Integer = 0 To 20
                If Not mLoopEnabled Then
                    Exit Do
                End If
                Threading.Thread.Sleep(500)
            Next
        Loop While mLoopEnabled
    End Sub

    Private Sub ProcessStartWatcher_EventArrived(sender As Object, e As System.Management.EventArrivedEventArgs) Handles oProcessStartWatcher.EventArrived
        Try
            Dim targetInstance As ManagementBaseObject = e.NewEvent.Properties("TargetInstance").Value

            Dim Caption As String = targetInstance.Properties("Caption").Value
            Dim PID As Integer = targetInstance.Properties("ProcessID").Value
            Dim ParentID As Integer = targetInstance.Properties("ParentProcessID").Value
            Dim Owner As String = "", ErrorMessage As String = ""
            If Not _ProcessOwner.GetUsernameFromProcessID(PID, Owner, ErrorMessage) Then
                Owner = ""
                RaiseEvent Event__AppOwner_Exception(Caption, PID, ErrorMessage)
            End If


            RaiseEvent Event__DEBUG("ProcessStartWatcher_EventArrived: Caption='" & Caption & " ' - PID=" & PID.ToString & " - ; ParentId=" & ParentID.ToString & " - ; Owner=" & Owner)


            If ParentID = pidExplorer Then
                RaiseEvent Event__AppStarted_System(Caption, PID, Owner)
            ElseIf ParentID = pidHomepage Then
                RaiseEvent Event__AppStarted_Homepage(Caption, PID, Owner)
            Else
                RaiseEvent Event__AppStarted_Unknown(Caption, PID, Owner)
            End If
        Catch ex As Exception
            RaiseEvent Event__AppStarted_Exception(ex.Message)
        End Try
    End Sub

    Private Sub ProcessStopWatcher_EventArrived(sender As Object, e As System.Management.EventArrivedEventArgs) Handles oProcessStopWatcher.EventArrived
        Try
            Dim targetInstance As ManagementBaseObject = e.NewEvent.Properties("TargetInstance").Value

            Dim Caption As String = targetInstance.Properties("Caption").Value
            Dim PID As Integer = targetInstance.Properties("ProcessID").Value
            Dim ParentID As Integer = targetInstance.Properties("ParentProcessID").Value

            RaiseEvent Event__DEBUG("ProcessStopWatcher_EventArrived: Caption='" & Caption & " ' - PID=" & PID.ToString & " - ; ParentId=" & ParentID.ToString)

            If ParentID = pidExplorer Then
                RaiseEvent Event__AppStopped(Caption, PID)
            End If
        Catch ex As Exception
            RaiseEvent Event__AppStopped_Exception(ex.Message)
        End Try
    End Sub

    Private Class _ProcessOwner
        <DllImport("kernel32.dll")>
        Public Shared Function OpenProcess(processAccess As ProcessAccessFlags, bInheritHandle As Boolean, processId As Integer) As Integer
        End Function

        <DllImport("advapi32.dll", SetLastError:=True)>
        Public Shared Function OpenProcessToken(ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("advapi32.dll", SetLastError:=True)>
        Public Shared Function GetTokenInformation(ByVal TokenHandle As IntPtr, ByVal TokenInformationClass As TOKEN_INFORMATION_CLASS, ByVal TokenInformation As IntPtr, ByVal TokenInformationLength As System.UInt32, ByRef ReturnLength As System.UInt32) As Boolean
        End Function

        <DllImport("advapi32.dll", SetLastError:=True)>
        Public Shared Function ConvertSidToStringSid(ByVal psid As IntPtr, ByRef pStringSid As String) As Boolean
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)>
        Public Shared Function GetExitCodeProcess(ByVal hProcess As IntPtr, ByRef lpExitCode As System.UInt32) As Boolean
        End Function


        Public Structure SID_AND_ATTRIBUTES
            Public Sid As IntPtr
            Public Attributes As Int32
        End Structure

        Public Structure TOKEN_USER
            Public User As SID_AND_ATTRIBUTES
        End Structure

        Enum ProcessAccessFlags
            All = &H1F0FFF
            Terminate = &H1
            CreateThread = &H2
            VirtualMemoryOperation = &H8
            VirtualMemoryRead = &H10
            VirtualMemoryWrite = &H20
            DuplicateHandle = &H40
            CreateProcess = &H80
            SetQuota = &H100
            SetInformation = &H200
            QueryInformation = &H400
            QueryLimitedInformation = &H1000
            Synchronize = &H100000
        End Enum

        Enum TOKEN_INFORMATION_CLASS
            TokenUser = 1
            TokenGroups
            TokenPrivileges
            TokenOwner
            TokenPrimaryGroup
            TokenDefaultDacl
            TokenSource
            TokenType
            TokenImpersonationLevel
            TokenStatistics
            TokenRestrictedSids
            TokenSessionId
            TokenGroupsAndPrivileges
            TokenSessionReference
            TokenSandBoxInert
            TokenAuditPolicy
            TokenOrigin
        End Enum

        Public Shared Function GetUsernameFromProcessID(pId As Long, ByRef UserName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim pHandle As Integer = OpenProcess(ProcessAccessFlags.QueryInformation, False, pId)

                If pHandle <> 0 Then
                    Dim tHandle As Integer
                    OpenProcessToken(pHandle, Principal.TokenAccessLevels.Query, tHandle)

                    Dim TokenUser As TOKEN_USER
                    Dim TokenInfLength As Integer = 0
                    Dim sUserSid As String = ""
                    Dim TokenInformation As IntPtr

                    GetTokenInformation(tHandle, TOKEN_INFORMATION_CLASS.TokenUser, IntPtr.Zero, 0, TokenInfLength)
                    TokenInformation = Marshal.AllocHGlobal(TokenInfLength)
                    GetTokenInformation(tHandle, TOKEN_INFORMATION_CLASS.TokenUser, TokenInformation, TokenInfLength, TokenInfLength)

                    TokenUser = DirectCast(Marshal.PtrToStructure(TokenInformation, TokenUser.GetType), TOKEN_USER)

                    ConvertSidToStringSid(TokenUser.User.Sid, sUserSid)

                    UserName = New Principal.SecurityIdentifier(sUserSid).Translate(GetType(Principal.NTAccount)).ToString()
                    ErrorMessage = ""

                    Return True
                Else
                    ErrorMessage = "<access denied>"
                    Return False
                End If
            Catch ex As Exception
                ErrorMessage = ex.Message
                Return False
            End Try

            Return False
        End Function
    End Class
End Class
