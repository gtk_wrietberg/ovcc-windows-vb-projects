﻿Imports System.Runtime.InteropServices
Imports System.Text

Public Class LinkExtensionToApplication
    <DllImport("Shlwapi.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Private Shared Function AssocQueryString(ByVal flags As UInteger, ByVal str As UInteger, ByVal pszAssoc As String, ByVal pszExtra As String, ByVal pszOut As StringBuilder, ByRef pcchOut As UInteger) As UInteger
    End Function

    Enum AssocF As UInteger
        None = 0
        Init_NoRemapCLSID = &H1
        Init_ByExeName = &H2
        Open_ByExeName = &H2
        Init_DefaultToStar = &H4
        Init_DefaultToFolder = &H8
        NoUserSettings = &H10
        NoTruncate = &H20
        Verify = &H40
        RemapRunDll = &H80
        NoFixUps = &H100
        IgnoreBaseClass = &H200
        Init_IgnoreUnknown = &H400
        Init_FixedProgId = &H800
        IsProtocol = &H1000
        InitForFile = &H2000
    End Enum

    Enum AssocStr
        Command = 1
        Executable
        FriendlyDocName
        FriendlyAppName
        NoOpen
        ShellNewValue
        DDECommand
        DDEIfExec
        DDEApplication
        DDETopic
        InfoTip
        QuickTip
        TileInfo
        ContentType
        DefaultIcon
        ShellExtension
        DropTarget
        DelegateExecute
        SupportedUriProtocols
        Max
    End Enum

    Private Shared mLastError As String
    Public Shared ReadOnly Property LastError() As String
        Get
            Return mLastError
        End Get
    End Property

    Private Shared Sub _ResetLastError()
        mLastError = ""
    End Sub


    Private Shared Function _AssocQueryString(association As AssocStr, extension As String) As String
        Const S_OK As Integer = 0
        Const S_FALSE As Integer = 1

        Dim length As UInteger = 0
        Dim ret As UInteger = AssocQueryString(AssocF.None, association, extension, Nothing, Nothing, length)
        If ret <> S_FALSE Then
            Throw New InvalidOperationException("Could not determine associated string (1 ; {" & ret.ToString() & "})")
        End If

        Dim sb = New StringBuilder(CInt(length))
        ' (length-1) will probably work too as the marshaller adds null termination
        ret = AssocQueryString(AssocF.None, association, extension, Nothing, sb, length)
        If ret <> S_OK Then
            Throw New InvalidOperationException("Could not determine associated string (2 ; {" & ret.ToString() & "})")
        End If

        Return sb.ToString()
    End Function

    Public Shared Function HasApplication(extensions As String) As Boolean
        Dim aExtensions As String() = extensions.Split(Globals.ExternalApplications.OfficeExtensionsSeparatorCharacter)
        Dim bRet As Boolean = False

        For Each _extension In aExtensions
            bRet = bRet Or _HasApplication(_extension)
        Next

        Return bRet
    End Function

    Private Shared Function _HasApplication(extension As String) As Boolean
        _ResetLastError()


        Try
            Dim _application As String = _AssocQueryString(AssocStr.Executable, extension)

            NamedPipe.Logger.Client.Debug("LinkExtensionToApplication", "extension '" & extension & "' links to '" & _application & "'")

            If Not _application.EndsWith("OpenWith.exe") Then
                Return True
            End If
        Catch ex As Exception
            mLastError = ex.Message
        End Try

        Return False
    End Function

    Public Shared Function GetInfo(extension As String, ByRef name As String, ByRef application As String) As Boolean
        _ResetLastError()


        Dim bRet As Boolean = True

        Try
            application = _AssocQueryString(AssocStr.Executable, extension)

            If application.EndsWith("OpenWith.exe") Then
                bRet = False
            End If
        Catch ex As Exception
            mLastError = ex.Message

            bRet = False
        End Try

        Try
            name = _AssocQueryString(AssocStr.FriendlyAppName, extension)
        Catch ex As Exception
            mLastError = ex.Message

            bRet = False
        End Try

        Return bRet
    End Function
End Class
