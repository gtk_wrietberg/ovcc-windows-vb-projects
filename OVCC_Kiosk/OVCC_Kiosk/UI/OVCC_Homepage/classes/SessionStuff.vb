﻿Imports System.Runtime.CompilerServices
Imports System.Runtime.Intrinsics

Public Class SessionStuff
    Public Class SessionAction
        Private Const ACTION__OPEN_URL = "open_url"
        Private Const ACTION__OPEN_APP = "open_app"
        Private Const ACTION__OPEN_APP_BY_PATH = "open_app_by_path"
        Private Const ACTION__OPEN_AIRLINES = "open_airlines"
        Private Const ACTION__OPEN_OFFICE = "open_office"

        Private Class _action
            Private mType As String
            Public Property Type() As String
                Get
                    Return mType
                End Get
                Set(ByVal value As String)
                    mType = value
                End Set
            End Property

            Private mParam As String
            Public Property Param() As String
                Get
                    Return mParam
                End Get
                Set(ByVal value As String)
                    mParam = value
                End Set
            End Property

            Public Sub New()
                mType = ""
                mParam = ""
            End Sub

            Public Sub New([Type] As String, Param As String)
                mType = [Type]
                mParam = Param
            End Sub
        End Class

        Private Shared _actions As New List(Of _action)

        Private Shared _type As String = ""
        Private Shared _param As String = ""

        Public Shared Sub Add_Url(Param As String)
            _add(ACTION__OPEN_URL, Param)
        End Sub

        Public Shared Sub Add_App(Param As String)
            _add(ACTION__OPEN_APP, Param)
        End Sub

        Public Shared Sub Add_AppByPath(Param As String)
            _add(ACTION__OPEN_APP_BY_PATH, Param)
        End Sub

        Public Shared Sub Add_AirlinesDialog()
            _add(ACTION__OPEN_AIRLINES)
        End Sub

        Public Shared Sub Add_OfficeDialog()
            _add(ACTION__OPEN_OFFICE)
        End Sub

        Private Shared Sub _add(Type As String)
            _add(Type, "")
        End Sub

        Private Shared Sub _add(Type As String, Param As String)
            _actions.Add(New _action([Type], Param))
        End Sub

        Public Shared Sub Reset()
            _actions.Clear()
        End Sub

        Public Shared Function HasActions() As Boolean
            Return _actions.Count > 0
        End Function

        Public Shared Function ExecuteNext() As Boolean
            If _actions.Count > 0 Then
                Dim _current_action As _action = _actions.Item(0)
                _actions = _actions.Skip(1).ToList

                If _current_action.Type.Equals(ACTION__OPEN_URL) Then
                    NamedPipe.Session.Client.SessionActivity("opening url '" & _current_action.Param & "'")

                    _OpenDefaultBrowser(_current_action.Param)
                End If
                If _current_action.Type.Equals(ACTION__OPEN_APP) Then
                    NamedPipe.Session.Client.SessionActivity("opening app '" & _current_action.Param & "'")

                    _OpenApp(_current_action.Param)
                End If
                If _current_action.Type.Equals(ACTION__OPEN_APP_BY_PATH) Then
                    NamedPipe.Session.Client.SessionActivity("opening app '" & _current_action.Param & "'")

                    _OpenAppByPath(_current_action.Param)
                End If
                If _current_action.Type.Equals(ACTION__OPEN_AIRLINES) Then
                    NamedPipe.Session.Client.SessionActivity("opening airlines")

                    Globals.Session.AirlinesDialogRequested = True
                End If
                If _current_action.Type.Equals(ACTION__OPEN_OFFICE) Then
                    NamedPipe.Session.Client.SessionActivity("opening office")

                    Globals.Session.OfficeDialogRequested = True
                End If

                Return True
            Else
                Return False
            End If
        End Function


        '--------------------------------------------------
        Private Shared Function _OpenDefaultBrowser(Url As String) As Boolean
            Dim bRet As Boolean = False

            NamedPipe.Logger.Client.Message("Opening url: " & Url)

            Try
                Dim p As New Process

                p.StartInfo.UseShellExecute = True
                p.StartInfo.FileName = Url

                bRet = p.Start()
            Catch ex As Exception
                NamedPipe.Logger.Client.Error("Error while opening url: " & ex.Message)
            End Try


            Return bRet
        End Function

        Private Shared Function _OpenApp(AppName As String) As Boolean
            Dim bRet As Boolean = False

            NamedPipe.Logger.Client.Message("Opening app: " & AppName)

            Try
                Dim sPath As String = Globals.ExternalApplications.GetPath(AppName)
                If sPath.Equals("") Then
                    Throw New Exception("Nothing found for app '" & AppName & "'")
                End If

                NamedPipe.Logger.Client.Message("Opening path: " & sPath)

                Dim p As New Process

                p.StartInfo.UseShellExecute = True
                p.StartInfo.FileName = sPath

                bRet = p.Start()
            Catch ex As Exception
                NamedPipe.Logger.Client.Error("Error while opening app: " & ex.Message)
            End Try


            Return bRet
        End Function

        Private Shared Function _OpenAppByPath(AppPath As String) As Boolean
            Dim bRet As Boolean = False

            NamedPipe.Logger.Client.Message("Opening app: " & AppPath)

            Try
                NamedPipe.Logger.Client.Message("Opening path: " & AppPath)

                Dim p As New Process

                p.StartInfo.UseShellExecute = True
                p.StartInfo.FileName = AppPath

                bRet = p.Start()
            Catch ex As Exception
                NamedPipe.Logger.Client.Error("Error while opening app: " & ex.Message)
            End Try


            Return bRet
        End Function
    End Class

    Public Shared Sub AcceptTerms()
        NamedPipe.Session.Client.SessionTermsAccepted()
    End Sub

    Public Shared Sub DeclineTerms()
        NamedPipe.Session.Client.SessionTermsDeclined()
    End Sub

    Public Shared Sub TimeoutTerms()
        NamedPipe.Session.Client.SessionTermsTimeout()
    End Sub

    Public Shared Sub StartSession()
        NamedPipe.Session.Client.SessionStart()
    End Sub

    Public Shared Function IsSessionActive() As Boolean
        Return Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsActive, False)
    End Function

    Public Shared Function GetSessionStart() As DateTime
        Try
            Dim l As Long = Settings.Get.Long(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__StartTimestamp, 0)
            Dim d As New DateTime(l)

            Return d
        Catch ex As Exception
            Return New DateTime(0)
        End Try
    End Function

    Public Shared Function GetSessionAge() As Long
        Dim l As Long = Settings.Get.Long(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__StartTimestamp, 0)

        Return CLng(TimeSpan.FromTicks(DateTime.Now.Ticks - l).TotalSeconds)
    End Function

    Public Shared Function GetSessionId() As String
        Return Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__SessionId, "")
    End Function

    Public Shared Function IsTermsAccepted() As Boolean
        Return Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsAccepted, False)
    End Function

    Public Shared Function IsTermsRequested() As Boolean
        Return Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsRequested, False)
    End Function

    Public Shared Function IsLogoutRequested() As Boolean
        Return Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsLogoutRequested, False)
    End Function

    Public Shared Function HomepageKeepAlive() As Boolean
        Return Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__HomepageKeepAlive, True)
    End Function

    Public Shared Function IsScreensaverActive() As Boolean
        Return Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsScreensaverActive, False)
    End Function
End Class
