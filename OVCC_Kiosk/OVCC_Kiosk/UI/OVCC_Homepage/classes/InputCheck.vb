﻿Imports System.Runtime.InteropServices

Public Class InputCheck
    <System.Runtime.InteropServices.StructLayout(Runtime.InteropServices.LayoutKind.Sequential)>
    Private Structure MOUSE_POINTER
        Dim x As Integer
        Dim y As Integer
    End Structure

    <DllImport("user32.dll", ExactSpelling:=True, SetLastError:=True)>
    Private Shared Function GetCursorPos(ByRef lpPoint As MOUSE_POINTER) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Private Shared current__MOUSE_POINTER As MOUSE_POINTER


    Private Structure LASTINPUTINFO
        Public cbSize As Integer
        Public dwTime As Integer
    End Structure

    <Runtime.InteropServices.DllImport("user32.dll")>
    Private Shared Function GetLastInputInfo(ByRef lii As LASTINPUTINFO) As Boolean
    End Function

    Private Shared Function GetTickCount() As Long
        Return Environment.TickCount
    End Function

    Private Shared current__LASTINPUTINFO_dwTime As Integer


    '------------------------------------------------------------------------------------------
    Public Shared Function SystemIdleTime(ByRef ReturnMessage As String) As Long
        Dim mMessage As String = ""
        Dim mIdleTime As Integer = -1

        Try
            Dim mLastInputInfo As LASTINPUTINFO = New LASTINPUTINFO

            Try
                mLastInputInfo.cbSize = Runtime.InteropServices.Marshal.SizeOf(mLastInputInfo)
            Catch ex As Exception
                mIdleTime = -3
            End Try

            ' get last input info from Windows
            If GetLastInputInfo(mLastInputInfo) Then     ' if we have an input info     
                ' compute idle time
                Try
                    mMessage = "mIdleTime calculation"

                    mIdleTime = (GetTickCount() - mLastInputInfo.dwTime) / 1000
                    mMessage = "mIdleTime = (GetTickCount() - mLastInputInfo.dwTime) / 1000 = (" & GetTickCount().ToString & " - " & mLastInputInfo.dwTime.ToString & ") / 1000 = " & mIdleTime.ToString

                Catch ex As Exception
                    mIdleTime = -4
                End Try
            Else
                mIdleTime = -5
            End If
        Catch ex As Exception
            mIdleTime = -2
        End Try


        ReturnMessage = mMessage
        Return mIdleTime
    End Function

    Public Shared Function HasActivity() As Boolean
        Dim _ret As Boolean = False
        Dim _lpi As New LASTINPUTINFO

        Try
            _lpi.cbSize = Runtime.InteropServices.Marshal.SizeOf(_lpi)

            If GetLastInputInfo(_lpi) Then
                If _lpi.dwTime > current__LASTINPUTINFO_dwTime Then
                    current__LASTINPUTINFO_dwTime = _lpi.dwTime

                    _ret = True
                End If
            Else
                NamedPipe.Logger.Client.Debug("GetLastInputInfo failed?!? " & Err.LastDllError.ToString)
            End If
        Catch ex As Exception

        End Try


        Return _ret
    End Function


    '------------------------------------------------------------------------------------------
    Public Shared Function HasMouseMoved() As Boolean
        Dim _mp As MOUSE_POINTER
        GetCursorPos(_mp)

        If _mp.x <> current__MOUSE_POINTER.x Or _mp.y <> current__MOUSE_POINTER.y Then
            current__MOUSE_POINTER.x = _mp.x
            current__MOUSE_POINTER.y = _mp.y

            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function MousePosition() As Point
        Dim _mp As MOUSE_POINTER
        GetCursorPos(_mp)

        Return New Point(_mp.x, _mp.y)
    End Function

    Public Shared Function MousePositionX() As Integer
        Return current__MOUSE_POINTER.x
    End Function

    Public Shared Function MousePositionY() As Integer
        Return current__MOUSE_POINTER.y
    End Function
End Class
