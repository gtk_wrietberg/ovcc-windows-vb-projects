﻿Imports System.Text

Public Class Globals
    Private Enum LockState
        [UNSET] = 0
        LOCKED = 1
        UNLOCKED = 2
    End Enum


    Public Class Machine
        Public Shared ReadOnly Name As String = Environment.MachineName
    End Class


    Public Class UI
        Public Shared ReadOnly BUTTON_MARGIN As Integer = 12
        Public Shared ReadOnly SHADOW_MARGIN As Integer = 3

        Public Shared HOMEPAGE_MENU_LANGUAGES_VISIBLE As Boolean = True
        Public Shared LOGOUT_BUTTON_VISIBLE As Boolean = True
        Public Shared INFORMATION_PANEL_VISIBLE As Boolean = True

        'Some over-complicated stuff to prevent repeated redrawing of controls
        Public Shared PREVIOUS_VALUE As Integer = -1
        Public Shared ReadOnly Property CURRENT_VALUE() As Integer
            Get
                Dim _tmp As Integer = 0
                _tmp += CInt(IIf(HOMEPAGE_MENU_LANGUAGES_VISIBLE, 1, 0))
                _tmp += CInt(IIf(LOGOUT_BUTTON_VISIBLE, 2, 0))
                _tmp += CInt(IIf(INFORMATION_PANEL_VISIBLE, 4, 0))
                Return _tmp
            End Get
        End Property
        Public Shared ReadOnly Property HAS_CHANGED() As Boolean
            Get
                Dim _b As Boolean = (PREVIOUS_VALUE <> CURRENT_VALUE)
                PREVIOUS_VALUE = CURRENT_VALUE
                Return _b
            End Get
        End Property
    End Class


    Public Class Secret
        Public Shared SECRET_BOX_DELAY As Integer = 5000
        Public Shared SECRET_BOX_SIZE As Integer = 100
        Public Shared SECRET_BOX_LEFT_CLICKED As Boolean = False
    End Class


    Public Class Session
        Public Shared AirlinesDialogRequested As Boolean = False
        Public Shared OfficeDialogRequested As Boolean = False
        Public Shared CustomDialogRequested As Boolean = False

        Public Class CustomDialog
            Public Shared FormTitle As String = ""
            Public Shared FormWidth As Integer = 0
            Public Shared FormHeight As Integer = 0
            Public Shared ContentUrl As String = ""
            Public Shared AutoCloseDelay As Integer = 30

            Public Shared Function DebugString() As String
                Dim _sb As New StringBuilder

                _sb.Append("FormTitle=" & FormTitle)
                _sb.Append(";")
                _sb.Append("FormWidth=" & FormWidth)
                _sb.Append(";")
                _sb.Append("FormHeight=" & FormHeight)
                _sb.Append(";")
                _sb.Append("ContentUrl=" & ContentUrl)
                _sb.Append(";")
                _sb.Append("AutoCloseDelay=" & AutoCloseDelay)

                Return _sb.ToString
            End Function
        End Class
    End Class


    Public Class Application
        Public Shared Debug As Boolean = False

        Public Shared ReadOnly Property IsDevMachineOrDebugging() As Boolean
            Get
                Return Helpers.Generic.IsDevMachine Or Debug
            End Get
        End Property

        Public Shared MainFormHandle = -1

        Public Shared AdminPassword As String = ""

        Public Shared GuestPasswordEnabled As Boolean = False

        Public Shared ScreensaverTimeout As Long = 300

        Public Shared SessionTimeout As Long = 600
        Public Shared SessionTimeoutWarningDuration As Long = 30
        Public Shared SessionTimeoutWarningActive As Boolean = False

        Public Shared EmergencyExit As Boolean = False
        Public Shared ForSomeReasonWeNeedToChangeLockStatusMultipleTimes As Integer = 0
        Public Shared ReadOnly ForSomeReasonWeNeedToChangeLockStatusMultipleTimes_MAX As Integer = 5

        Public Shared FullAccountName As String = Environment.UserDomainName & "\" & Environment.UserName


        Public Shared AdminPasswordTyped As String = ""

        Private Shared mLockedState As LockState = LockState.UNSET
        Public Shared ReadOnly Property IsLockSet() As Boolean
            Get
                Return Not mLockedState = LockState.UNSET
            End Get
        End Property
        Public Shared Property IsLocked() As Boolean
            Get
                Return mLockedState = LockState.LOCKED
            End Get
            Set(value As Boolean)
                If value Then
                    mLockedState = LockState.LOCKED
                Else
                    mLockedState = LockState.UNLOCKED
                End If
            End Set
        End Property


        Public Shared IsClosing As Boolean = False


        Public Class RegularExpressions
            Public Shared EXTERNAL_APPLICATION As New System.Text.RegularExpressions.Regex(Settings.Constants.RegularExpressions.EXTERNAL_APPLICATION)
        End Class


        Public Class Languages
            Public Shared ActiveLanguageId As Integer = -1
            Public Shared TermsFile As String
        End Class


        Public Class Dialogs
            Public Shared IsDialogShown_Loading As Boolean = False
            Public Shared IsDialogShown_Terms As Boolean = False
            Public Shared IsDialogShown_Logout As Boolean = False
            Public Shared IsDialogShown_AdminPassword As Boolean = False
            Public Shared IsDialogShown_Admin As Boolean = False
            Public Shared IsDialogShown_Menu_Languages As Boolean = False
            Public Shared IsDialogShown_Airlines As Boolean = False
            Public Shared IsDialogShown_Office As Boolean = False
            Public Shared IsDialogShown_Helpdesk As Boolean = False
            Public Shared IsDialogShown_GuestPassword As Boolean = False
            Public Shared IsDialogShown_Custom As Boolean = False

            Public Shared ReadOnly Property IsDialogShown() As Boolean
                Get
                    Return IsDialogShown_Loading Or
                        IsDialogShown_Terms Or
                        IsDialogShown_Logout Or
                        IsDialogShown_AdminPassword Or
                        IsDialogShown_Admin Or
                        IsDialogShown_Menu_Languages Or
                        IsDialogShown_Helpdesk Or
                        IsDialogShown_GuestPassword Or
                        IsDialogShown_Airlines Or
                        IsDialogShown_Office Or
                        IsDialogShown_Custom
                End Get
            End Property
        End Class
    End Class


    Public Class Security
        Public Class PermittedScriptingHosts
            Private Shared mList As New List(Of String)

            Public Shared Sub Load(ListAsString As String)
                mList = New List(Of String)
                mList = ListAsString.Split("|").ToList
            End Sub

            Public Shared Function Exists(HostString As String) As Boolean
                For Each _s In mList
                    If HostString.StartsWith(_s) Then
                        Return True
                    End If
                Next

                Return False
            End Function
        End Class
    End Class


    Public Class Url
        Public Shared Function Path2Url(sPath As String) As String
            Return "file:///" & sPath
        End Function
    End Class


    Public Class ExternalApplications
        Public Shared ReadOnly OfficeExtensionsSeparatorCharacter As String = ";"

        Private Class _external_app
            Private mId As Integer
            Public Property ID() As Integer
                Get
                    Return mId
                End Get
                Set(ByVal value As Integer)
                    mId = value
                End Set
            End Property

            Private mName As String
            Public Property Name() As String
                Get
                    Return mName
                End Get
                Set(ByVal value As String)
                    mName = value
                End Set
            End Property

            Private mPath As String
            Public Property Path() As String
                Get
                    Return mPath
                End Get
                Set(ByVal value As String)
                    mPath = value
                End Set
            End Property

            Private mParams As String
            Public Property Params() As String
                Get
                    Return mParams
                End Get
                Set(ByVal value As String)
                    mParams = value
                End Set
            End Property

            Public Sub New(ID As Integer, Name As String, Path As String, Params As String)
                mId = ID
                mName = Name
                mPath = Path
                mParams = Params
            End Sub
        End Class

        Private Shared _external_apps As New List(Of _external_app)

        Public Shared Sub Add(ID As Integer, Name As String, Path As String, Params As String)
            _external_apps.Add(New _external_app(ID, Name, Path, Params))
        End Sub

        Public Shared Sub Reset()
            _external_apps.Clear()
        End Sub

        Public Shared Function Exists(ID As Integer) As Boolean
            Return Not GetPath(ID).Equals("")
        End Function

        Public Shared Function Exists(Name As String) As Boolean
            Return Not GetPath(Name).Equals("")
        End Function

        Public Shared Function GetPath(ID As Integer) As String
            NamedPipe.Logger.Client.Debug("GetPath(" & ID.ToString & ")")

            For Each _app As _external_app In _external_apps
                NamedPipe.Logger.Client.Debug("_app.ID=" & _app.ID)
                If _app.ID = ID Then
                    Return _app.Path
                End If
            Next

            Return ""
        End Function

        Public Shared Function GetPath(Name As String) As String
            NamedPipe.Logger.Client.Debug("GetPath('" & Name & "')")

            For Each _app As _external_app In _external_apps
                NamedPipe.Logger.Client.Debug("_app.Name=" & _app.Name)
                If _app.Name.Equals(Name) Then
                    Return _app.Path
                End If
            Next

            Return ""
        End Function

        Public Shared Function GetParams(ID As Integer) As String
            For Each _app As _external_app In _external_apps
                If _app.ID = ID Then
                    Return _app.Params
                End If
            Next

            Return ""
        End Function

        Public Shared Function GetParams(Name As String) As String
            For Each _app As _external_app In _external_apps
                If _app.Name.Equals(Name) Then
                    Return _app.Params
                End If
            Next

            Return ""
        End Function
    End Class
End Class
