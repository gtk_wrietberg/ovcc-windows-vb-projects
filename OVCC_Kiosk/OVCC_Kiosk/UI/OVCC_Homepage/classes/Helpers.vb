Imports System.Globalization
Imports System.Runtime.InteropServices
Imports System.Security.Principal
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Web
Imports System.Drawing
Imports Microsoft.Win32

Public Class Helpers
#Region "errors"
    Public Class Errors
        Public Class ErrorData
            Private mError_Timestamp As Date
            Private mError_Description As String
            Private mError_Caller As String

            Public Property Timestamp() As Date
                Get
                    Return mError_Timestamp
                End Get
                Set(ByVal value As Date)
                    mError_Timestamp = value
                End Set
            End Property

            Public Property Description() As String
                Get
                    Return mError_Description
                End Get
                Set(ByVal value As String)
                    mError_Description = value
                End Set
            End Property

            Public Property Caller() As String
                Get
                    Return mError_Caller
                End Get
                Set(ByVal value As String)
                    mError_Caller = value
                End Set
            End Property

            Public Overloads Function ToString() As String
                Return ToString(True, True)
            End Function

            Public Overloads Function ToString(IncludeTimestamp As Boolean) As String
                Return ToString(IncludeTimestamp, True)
            End Function

            Public Overloads Function ToString(IncludeTimestamp As Boolean, IncludeCaller As Boolean) As String
                Dim sResult As String = ""

                If IncludeTimestamp Then
                    sResult = mError_Timestamp.ToString & " - " & mError_Description
                Else
                    sResult = mError_Description
                End If

                If IncludeCaller Then
                    sResult = sResult & " - " & mError_Caller
                End If

                Return sResult
            End Function
        End Class

        Private Shared mMaxHistory As Integer = 25
        Private Shared lstErrors As New List(Of ErrorData)(mMaxHistory)

        Public Shared Sub Add(Description As String)
            Dim newError As New ErrorData

            newError.Description = Description
            newError.Timestamp = Date.Now

            Dim stackTrace As New StackTrace()

            newError.Caller = stackTrace.GetFrame(1).GetMethod().Name


            lstErrors.Insert(0, newError)

            If lstErrors.Count > mMaxHistory Then
                lstErrors.RemoveRange(mMaxHistory, lstErrors.Count - mMaxHistory)
            End If
        End Sub

        Public Shared Function GetLast() As String
            Return GetLast(True, True)
        End Function

        Public Shared Function GetLast(IncludeTimestamp As Boolean) As String
            Return GetLast(IncludeTimestamp, True)
        End Function

        Public Shared Function GetLast(IncludeTimestamp As Boolean, IncludeCaller As Boolean) As String
            If lstErrors.Count > 0 Then
                Return lstErrors.Item(0).ToString(IncludeTimestamp, IncludeCaller)
            Else
                Return ""
            End If
        End Function

        Public Overloads Shared Function ToString(Optional IncludeTimestamp As Boolean = True, Optional AllErrors As Boolean = False) As String
            Dim sResult As String = ""
            Dim currentError As New ErrorData

            If lstErrors.Count = 0 Then
                Return ""
            End If

            If lstErrors.Count = 1 Then
                AllErrors = False
            End If

            If AllErrors Then
                For i As Integer = 0 To lstErrors.Count - 2
                    currentError = lstErrors.Item(i)
                    sResult &= currentError.ToString & vbCrLf
                Next

                currentError = lstErrors.Item(lstErrors.Count - 1)
                sResult &= currentError.ToString
            Else
                currentError = lstErrors.Item(0)
                sResult = currentError.ToString
            End If

            Return sResult
        End Function
    End Class
#End Region


#Region "Windows"
    Public Class Windows
        Public Class Programs
            Public Class FileInfo
                Public Shared Function GetFileDescription(sFilePath As String) As String
                    If Not IO.File.Exists(sFilePath) Then
                        Return "?" & IO.Path.GetFileNameWithoutExtension(sFilePath) & "?"
                    End If

                    Try
                        Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(sFilePath)

                        Return myFileVersionInfo.FileDescription
                    Catch ex As Exception
                        Return "?!" & IO.Path.GetFileNameWithoutExtension(sFilePath) & "!?"
                    End Try
                End Function
            End Class

            Public Class ProductGUID
                Public Shared Function GetUninstallIdentifier(ByVal sSearchForApplication As String) As String
                    Dim KeyName As String
                    Dim Value_DisplayName As String
                    Dim Value_UninstallString As String = ""
                    Dim UninstallRegKey As Microsoft.Win32.RegistryKey
                    Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

                    UninstallRegKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", False)

                    If sSearchForApplication = "" Then
                        'Yes that's right
                        sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
                    End If

                    sSearchForApplication = sSearchForApplication.ToLower

                    For Each KeyName In UninstallRegKey.GetSubKeyNames
                        ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

                        Try
                            Value_DisplayName = ApplicationRegKey.GetValue("DisplayName", "").ToString.ToLower
                        Catch ex As Exception
                            Value_DisplayName = ""
                        End Try

                        If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                            Try
                                Value_UninstallString = ApplicationRegKey.GetValue("UninstallString", "").ToString
                            Catch ex As Exception
                                Value_UninstallString = ""
                            End Try

                            Value_UninstallString = System.Text.RegularExpressions.Regex.Replace(Value_UninstallString, ".*\{(.+)\}.*", "$1")

                            Exit For
                        End If
                    Next

                    Return Value_UninstallString
                End Function

                Public Shared Function GetProductIdentifier(ByVal sSearchForApplication As String) As String
                    Dim KeyName As String
                    Dim Value_DisplayName As String
                    Dim Value_UninstallString As String = ""
                    Dim UninstallRegKey As Microsoft.Win32.RegistryKey
                    Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

                    UninstallRegKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Installer\Products", False)

                    If sSearchForApplication = "" Then
                        'Yes that's right
                        sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
                    End If

                    sSearchForApplication = sSearchForApplication.ToLower

                    For Each KeyName In UninstallRegKey.GetSubKeyNames
                        ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

                        Try
                            Value_DisplayName = ApplicationRegKey.GetValue("ProductName", "").ToString.ToLower
                        Catch ex As Exception
                            Value_DisplayName = ""
                        End Try

                        If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                            Try
                                Value_UninstallString = KeyName
                            Catch ex As Exception
                                Value_UninstallString = ""
                            End Try

                            Exit For
                        End If
                    Next

                    Return Value_UninstallString
                End Function

            End Class
        End Class

        Public Shared Function IsRunningAsAdmin() As Boolean
            Dim bTmp As Boolean

            Try
                Dim user As WindowsIdentity = WindowsIdentity.GetCurrent()
                Dim principal As New WindowsPrincipal(user)

                bTmp = principal.IsInRole(WindowsBuiltInRole.Administrator)
            Catch ex As UnauthorizedAccessException
                bTmp = False
            Catch ex As Exception
                bTmp = False
            End Try

            Return bTmp
        End Function

        Public Class TaskBar
            Public Shared Sub Show()
                _TaskBar(True)
            End Sub

            Public Shared Sub Hide()
                _TaskBar(False)
            End Sub

            Private Declare Auto Function ShowWindow Lib "user32.dll" (ByVal hWnd As Long, ByVal nCmdShow As Long) As Long
            Private Declare Auto Function FindWindow Lib "user32.dll" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
            Private Declare Auto Function EnableWindow Lib "user32.dll" (ByVal hWnd As Long, ByVal fEnable As Long) As Long

            Private Const SW_HIDE = 0
            Private Const SW_SHOW = 5

            Private Shared Sub _TaskBar(Visible As Boolean)
                Dim hWnd As Long

                hWnd = FindWindow("Shell_TrayWnd", "")
                If Visible Then
                    ShowWindow(hWnd, SW_SHOW)
                Else
                    ShowWindow(hWnd, SW_HIDE)
                End If

                EnableWindow(hWnd, Visible)
            End Sub
        End Class

        Public Class Startup
            Private Shared ReadOnly StartupApproved_enabled_bytes As Byte() = {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            Private Shared ReadOnly StartupApproved_disabled_bytes As Byte() = {3, 0, 29, 16, 113, 209, 7, 29, 16, 113, 209, 7}

            Private Shared ReadOnly KEY_Windows_RunAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run"
            Private Shared ReadOnly KEY_Windows_StartupApproved As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run"

            Public Shared Sub SetStartupProgram(AppName As String, AppPath As String)
                If Environment.Is64BitOperatingSystem Then
                    Registry.SetValue_String(KEY_Windows_RunAtStartup, AppName, "")

                    Registry64.SetValue_String(KEY_Windows_RunAtStartup, AppName, AppPath)
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                Else
                    Registry64.SetValue_String(KEY_Windows_RunAtStartup, AppName, "")

                    Registry.SetValue_String(KEY_Windows_RunAtStartup, AppName, AppPath)
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                End If
            End Sub

            Public Shared Sub EnableStartupProgram(AppName As String)
                If Environment.Is64BitOperatingSystem Then
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                Else
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                End If
            End Sub

            Public Shared Sub DisableStartupProgram(AppName As String)
                If Environment.Is64BitOperatingSystem Then
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_disabled_bytes)
                Else
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_disabled_bytes)
                End If
            End Sub

        End Class
    End Class
#End Region


#Region "Forms"
    Public Class Forms
        <DllImport("user32.dll", SetLastError:=True)>
        Private Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As Integer) As Boolean
        End Function

        <DllImport("user32", CharSet:=CharSet.Auto, SetLastError:=True)>
        Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As UInt32, ByVal wParam As Integer, ByVal lParam As IntPtr) As Integer
        End Function

        Private Const BCM_SETSHIELD As UInt32 = &H160C

        Public Shared Sub AddUACShield(handle As IntPtr)
            SendMessage(handle, BCM_SETSHIELD, 0, New IntPtr(1))
        End Sub


        Public Class OnTop
            Private Const SWP_NOSIZE As Integer = &H1
            Private Const SWP_NOMOVE As Integer = &H2

            Private Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
            Private Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)

            Public Shared Sub Put(FormHandle As System.IntPtr)
                SetWindowPos(FormHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End Sub

            Public Shared Sub Remove(FormHandle As System.IntPtr)
                SetWindowPos(FormHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End Sub
        End Class

        Private Const SWP_NOSIZE As Integer = &H1
        Private Const SWP_NOMOVE As Integer = &H2

        Private Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
        Private Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)

        Public Shared Sub TopMost(FormHandle As System.IntPtr, bTopMost As Boolean)
            If bTopMost Then
                SetWindowPos(FormHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            Else
                SetWindowPos(FormHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End If
        End Sub


        <DllImport("user32", CharSet:=CharSet.Auto, SetLastError:=True)>
        Private Shared Function GetForegroundWindow() As IntPtr
        End Function

        Public Shared Function IsForeground(FormHandle As IntPtr) As Boolean
            Dim _fg As IntPtr = GetForegroundWindow()

            Return _fg.Equals(FormHandle)
        End Function
    End Class
#End Region


#Region "XOrObfuscation"
    Public Class XOrObfuscation
        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "$#(98)&_Ndf9m0987e5WRW#%rtefgm03947"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Private Shared mLastError As String
        Public Shared ReadOnly Property LastError() As String
            Get
                Return mLastError
            End Get
        End Property

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            mLastError = ""

            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            Try
                If PlainText.Length > 1000 Then
                    Throw New Exception("input string too long (max 1000)")
                End If

                InitializePasshrases()


                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    If AscW(Mid(PlainText, i + 1, 1)) > 255 Then
                        'uh oh, Unicode
                        Throw New Exception("Only ASCII is supported!")
                    End If

                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(AscW(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(AscW(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""

                mLastError = ex.Message
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String


            Try
                InitializePasshrases()

                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & ChrW(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & ChrW(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                PlainText_RandomPassphrase = ""

                mLastError = ex.Message
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = AscW(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            temp = _HexToNo(Mid(BinVal, 2, 1))
            iVal = iVal + temp
            temp = _HexToNo(Mid(BinVal, 1, 1))
            iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class

    Public Class XOrObfuscation_v2
        Private Shared ReadOnly c_MAXLENGTH As Integer = 2000
        Private Shared ReadOnly c_PADDINGLENGTH As Integer = 32

        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "ajsgbdJOASGd_()#hfibsbjbn34#$jilhf"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Private Shared mLastError As String
        Public Shared ReadOnly Property LastError() As String
            Get
                Return mLastError
            End Get
        End Property

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            mLastError = ""

            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            Try
                If PlainText.Length > c_MAXLENGTH Then
                    Throw New Exception("input string too long (max " & c_MAXLENGTH.ToString & ")")
                End If

                InitializePasshrases()


                'add length to string
                PlainText = _LeadingZero(_D2H(PlainText.Length), 3) & PlainText

                'let the padding begin
                Dim paddinglength As Integer = c_PADDINGLENGTH - (PlainText.Length Mod c_PADDINGLENGTH)
                PlainText &= Types.RandomString(paddinglength)


                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    If AscW(Mid(PlainText, i + 1, 1)) > 255 Then
                        'uh oh, Unicode
                        Throw New Exception("Only ASCII is supported!")
                    End If

                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(AscW(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(AscW(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""

                mLastError = ex.Message
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String


            Try
                InitializePasshrases()

                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & ChrW(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & ChrW(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                'we need to remove the padding, first 3 chars are length in hex
                Dim firstthreechars As String = PlainText_RandomPassphrase.Substring(0, 3)
                Dim strlength As Integer = _H2D(firstthreechars)

                PlainText_RandomPassphrase = PlainText_RandomPassphrase.Substring(3, strlength)
            Catch ex As Exception
                PlainText_RandomPassphrase = ""

                mLastError = ex.Message
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = AscW(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            For i As Integer = BinVal.Length To 1 Step -1
                temp = _HexToNo(Mid(BinVal, i, 1))
                iVal = iVal + (temp * (16 ^ (BinVal.Length - i)))
            Next

            'temp = _HexToNo(Mid(BinVal, 2, 1))
            'iVal = iVal + temp
            'temp = _HexToNo(Mid(BinVal, 1, 1))
            'iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _LeadingZero(sNum As String, Optional len As Integer = 3) As String
            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class
#End Region


#Region "misc helpers"
    Public Class Generic
        Public Shared Function IsDevMachine() As Boolean
            Dim sComputerName As String = Environment.MachineName.ToLower
            Dim bRet As Boolean = False

            If InStr(sComputerName, "rietberg") > 0 _
            Or InStr(sComputerName, "superlekkerding") > 0 _
            Or InStr(sComputerName, "dragon-bafstaaf") > 0 _
            Or InStr(sComputerName, "wrdev") > 0 Then
                bRet = True
            End If

            Return bRet
        End Function

        Public Shared Function GetCurrentUser() As String
            Return Environment.UserName
        End Function

        Public Shared Function GetHostnameFromUrl(ByVal sUrl As String) As String
            Dim u As New Uri(sUrl)

            Return u.Host
        End Function

        Public Shared Function HtmlEncode(str As String) As String
            Return HttpUtility.HtmlEncode(str)
        End Function

        Public Shared Function HtmlDecode(str As String) As String
            Return HttpUtility.HtmlDecode(str)
        End Function

        Public Shared Function JavaScriptStringEncode(str As String) As String
            Return HttpUtility.JavaScriptStringEncode(str)
        End Function

        Public Shared Function JavaScriptStringDecode(str As String) As String
            ' Replace some chars.
            Dim decoded = str.Replace("\'", "'").Replace("\""", """").Replace("\/", "/").Replace("\\", "\").Replace("\t", vbTab).Replace("\r\n", vbCrLf).Replace("\r", vbCr).Replace("\n", vbLf)

            ' Replace unicode escaped text.
            Dim rx = New Regex("\\[uU]([0-9A-F]{4})")
            Dim evaluator As MatchEvaluator = AddressOf ASCII_to_CHAR

            decoded = rx.Replace(decoded, evaluator)

            Return decoded
        End Function

        Private Shared Function ASCII_to_CHAR(match As Match) As String
            Return ChrW(Int32.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString(CultureInfo.InvariantCulture)
        End Function

        Public Shared Function NumbersOnly(str As String) As String
            Dim nonNumericCharacters As New Regex("[^0-9]")

            Return nonNumericCharacters.Replace(str, String.Empty)
        End Function

        Public Shared Function RandomInteger(lowerbound As Integer, upperbound As Integer, upperbound_inclusive As Boolean)
            Static Generator As System.Random = New System.Random()

            If upperbound < lowerbound Then
                Dim tmp As Integer = upperbound
                upperbound = lowerbound
                lowerbound = tmp
            End If

            If upperbound_inclusive Then
                Return Generator.Next(lowerbound, upperbound + 1)
            Else
                Return Generator.Next(lowerbound, upperbound)
            End If
        End Function
    End Class
#End Region


#Region "types"
    Public Class Types
        Public Shared Function CastStringToInteger_safe(s As String) As Integer
            Dim iTmp As Integer = -1

            If Integer.TryParse(s, iTmp) Then

            End If

            Return iTmp
        End Function

        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function LeadingZero(num As Integer, Optional len As Integer = 2) As String
            Dim sNum As String = num.ToString

            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Public Shared Function RandomString(Length As Integer, AddSpaces As Boolean) As String
            Return RandomString(Length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", AddSpaces)
        End Function

        Public Shared Function RandomString(Length As Integer) As String
            Return RandomString(Length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", False)
        End Function

        Public Shared Function RandomString(Length As Integer, CharsToChooseFrom As String, AddSpaces As Boolean) As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim _sb As New StringBuilder
            Dim iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())

                _sb.Append(Mid(CharsToChooseFrom, iRnd + 1, 1))

                If AddSpaces And Rnd() > 0.85 Then
                    _sb.Append(" ")
                End If
            Next

            Return _sb.ToString().Trim()
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function

        Public Shared Function FromNormalDateToHBDate(Optional WithTime As Boolean = False) As String
            Return FromNormalDateToHBDate(Now, WithTime)
        End Function

        Public Shared Function FromNormalDateToHBDate(d As Date, Optional WithTime As Boolean = False) As String
            Dim sTmp As String = d.ToString("yyyy-MM-dd")

            If WithTime Then
                sTmp &= " " & d.ToString("HH:mm:ss")
            End If

            Return sTmp
        End Function

        Public Shared Function FromHBDateToNormalDate(s As String) As Date
            Dim d As Date

            Try
                d = Date.Parse(s)
            Catch ex As Exception
                d = Nothing
            End Try

            Return d
        End Function

        Public Shared Function IsValidHBDate(s As String) As Boolean
            Dim d As Date

            Try
                d = Date.Parse(s)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function MaskString(s As String, Optional Mask As String = "*", Optional ShowCharsAtStartCount As Integer = 2, Optional ShowCharsAtEndCound As Integer = 0) As String
            If s.Length > (ShowCharsAtStartCount + ShowCharsAtEndCound) Then
                Return String.Concat(s.Substring(0, ShowCharsAtStartCount), "".PadLeft(s.Length - (ShowCharsAtStartCount + ShowCharsAtEndCound), Mask), s.Substring(s.Length - ShowCharsAtEndCound, ShowCharsAtEndCound))
            Else
                Return s
            End If
        End Function

        Public Shared Function String2Boolean(s As String) As Boolean
            s = s.ToLower

            Return (s.Equals("yes") Or s.Equals("true") Or s.Equals("1"))
        End Function
    End Class
#End Region


#Region "time and date"
    Public Class TimeDate
        Public Shared Function UnixTimestamp() As Long
            Return Convert.ToInt64((DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)
        End Function

        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Return FromUnix(UnixTime, True)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long, bDaylightSavingCorrection As Boolean) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime And bDaylightSavingCorrection Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now, False)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Return ToUnix(dTmp, False)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date, bDaylightSavingCorrection As Boolean) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime And bDaylightSavingCorrection Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function

        Public Class Timezone
            Public Shared Function getName() As String
                Dim zone As TimeZoneInfo = TimeZoneInfo.Local

                Return zone.DisplayName
            End Function

            Public Shared Function getOffset() As Double
                Dim dOffset As Double = 0.0

                Dim zone As TimeZoneInfo = TimeZoneInfo.Local
                Dim offset As TimeSpan = zone.GetUtcOffset(DateTime.Now)

                dOffset += offset.Hours
                dOffset += (offset.Minutes / 60)

                Return dOffset
            End Function
        End Class
    End Class
#End Region


#Region "registry"
    Public Class Registry '32-bit 
        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function KeyExists_CU(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.CurrentUser, keyName)
        End Function

        Public Shared Function KeyExists_LM(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.LocalMachine, keyName)
        End Function

        Private Shared Function _KeyExists(keyHive As Microsoft.Win32.RegistryHive, keyName As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.LocalMachine Then
                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyHive = RegistryHive.CurrentUser Then
                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                Errors.Add(ex.Message)

                Return False
            End Try
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
            Return FindKeyByString(keyName, SearchString, "", ErrorMessage)
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, SearchInValueName As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchInValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchInValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchInValueName).ToString
                            End If


                            subkey.Close()

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                bRet = True

                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function FindValueByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Object
            Dim oRet As Object = Nothing
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each valueName As String In key.GetValueNames
                    If valueName.ToLower.Equals(SearchString.ToLower) Then
                        oRet = key.GetValue(valueName)

                        Exit For
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                oRet = Nothing
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return oRet
        End Function

        Public Shared Function FindKeyBySubValueAndReturnValue(keyName As String, SearchString As String, SearchValueName As String, ReturnValueName As String, ByRef ReturnValue As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"
            ReturnValue = "not found"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchValueName).ToString
                            End If

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                If Not subkey.GetValue(ReturnValueName) Is Nothing Then
                                    ReturnValue = subkey.GetValue(ReturnValueName).ToString

                                    bRet = True
                                End If
                            End If

                            subkey.Close()

                            If bRet Then
                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp.ToLower.Equals("yes") Or sTmp.ToLower.Equals("true") Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function SetValue_Boolean(keyName As String, valueName As String, value As Boolean) As Boolean
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "true", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "false", Microsoft.Win32.RegistryValueKind.String)
                End If

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "binary"
        Public Shared Function GetValue_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_Binary(keyName As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

        Public Shared Sub SetValue_Binary(keyName As String, valueName As String, value As Object)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.Binary)

            Catch ex As Exception

            End Try
        End Sub

        Public Shared Function GetValue_ReadOnly_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_ReadOnly_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_ReadOnly_Binary(keyname As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyname, valueName, defaultValue)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

#End Region

#Region "string" ' Microsoft.Win32.RegistryKey.OpenSubKey(String name, Boolean writable)
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                sTmp = defaultValue
            Else
                sTmp = Convert.ToString(oTmp)
            End If

            Return sTmp
        End Function

        Public Shared Function SetValue_String(keyName As String, valueName As String, value As String) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function


        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String, defaultValue As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, defaultValue)
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(hiveName, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyname As String, valueName As String, defaultValue As String) As String
            Try
                Dim oTmp As Object, sTmp As String
                Dim r As RegistryKey

                If hiveName = RegistryHive.LocalMachine Then
                    r = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyname, False)
                ElseIf hiveName = RegistryHive.CurrentUser Then
                    r = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyname, False)
                Else
                    Return defaultValue
                End If

                oTmp = r.GetValue(valueName, defaultValue)
                sTmp = Convert.ToString(oTmp)

                Return sTmp
            Catch ex As Exception
                Errors.Add("GetValue_ReadOnly_String(): " & ex.Message)

                Return "GetValue_ReadOnly_String(): " & ex.Message
            End Try
        End Function
#End Region

#Region "long"
        Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
            Return GetValue_Long(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
            Dim oTmp As Object, iTmp As Long

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                iTmp = defaultValue
            Else
                iTmp = Helpers.Types.CastToLong_safe(oTmp)
            End If


            Return iTmp
        End Function

        Public Shared Function SetValue_Long(keyName As String, valueName As String, value As Long) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.QWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                iTmp = defaultValue
            Else
                iTmp = Helpers.Types.CastToInteger_safe(oTmp)
            End If


            Return iTmp
        End Function

        Public Shared Function SetValue_Integer(keyName As String, valueName As String, value As Integer) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "misc"
        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, True)
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Public Shared Function DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(keyHive, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Private Shared Function _DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Try
                Dim regKey As RegistryKey

                If keyHive = RegistryHive.CurrentUser Then
                    regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyRoot, True)
                ElseIf keyHive = RegistryHive.LocalMachine Then
                    regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyRoot, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                'delete all subkeys
                regKey.DeleteSubKeyTree(keyToDelete)

                If bKeepKeyToDelete Then
                    'put back the root sub key
                    regKey.CreateSubKey(keyToDelete)
                End If

                Return True
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return False
        End Function
#End Region
    End Class

    Public Class Registry64
        Private Shared Function _String2RegKey(keyName As String) As Microsoft.Win32.RegistryKey
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return key
            Catch ex As Exception
                Errors.Add(ex.Message)

                Return Nothing
            End Try
        End Function

        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function KeyExists_CU(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.CurrentUser, keyName)
        End Function

        Public Shared Function KeyExists_LM(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.LocalMachine, keyName)
        End Function

        Private Shared Function _KeyExists(keyHive As Microsoft.Win32.RegistryHive, keyName As String) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.LocalMachine Or keyHive = RegistryHive.CurrentUser Then
                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(keyHive, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                Errors.Add(ex.Message)

                Return False
            End Try
        End Function


        Public Shared Function FindValues(KeyName As String, SearchString As String) As List(Of String)
            Dim mList As New List(Of String)

            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey


            Try
                If SearchString.Length < 3 Then
                    Throw New Exception("SearchString too short")
                End If

                If KeyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    KeyName = KeyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf KeyName.StartsWith("HKEY_CURRENT_USER\") Then
                    KeyName = KeyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(KeyName, False)
                If key Is Nothing Then
                    Throw New Exception(KeyName & " not found!")
                End If

                For Each valueName As String In key.GetValueNames
                    If valueName.Contains(SearchString) Then
                        mList.Add(valueName)
                    End If
                Next
            Catch ex As Exception

            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return mList
        End Function



        Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
            Return FindKeyByString(keyName, SearchString, "", ErrorMessage)
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, SearchInValueName As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchInValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchInValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchInValueName).ToString
                            End If


                            subkey.Close()

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                bRet = True

                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function FindKeyBySubValueAndReturnValue(keyName As String, SearchString As String, SearchValueName As String, ReturnValueName As String, ByRef ReturnValue As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"
            ReturnValue = "not found"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchValueName).ToString
                            End If

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                If Not subkey.GetValue(ReturnValueName) Is Nothing Then
                                    ReturnValue = subkey.GetValue(ReturnValueName).ToString

                                    bRet = True
                                End If
                            End If

                            subkey.Close()

                            If bRet Then
                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function


        'Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
        '    Dim bRet As Boolean = False

        '    ErrorMessage = "ok"

        '    Try
        '        Dim key_root As Microsoft.Win32.RegistryKey
        '        Dim key As Microsoft.Win32.RegistryKey

        '        If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
        '            keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

        '            key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
        '            key = key_root.OpenSubKey(keyName, False)
        '            If key Is Nothing Then
        '                Throw New Exception(keyName & " not found!")
        '            End If
        '        ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
        '            keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

        '            key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
        '            key = key_root.OpenSubKey(keyName, False)
        '            If key Is Nothing Then
        '                Throw New Exception(keyName & " not found!")
        '            End If
        '        Else
        '            Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
        '        End If

        '        For Each subKeyName As String In key.GetSubKeyNames()
        '            If subKeyName.ToLower.Contains(SearchString.ToLower) Then
        '                bRet = True

        '                Exit For
        '            End If
        '        Next
        '    Catch ex As Exception
        '        ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

        '        bRet = False
        '    End Try

        '    Return bRet
        'End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "_GetValue & _SetValue & _DeleteValue"
        Private Shared Function _DeleteValue(keyName As String, valueName As String, thrownOnMissingValue As Boolean) As Boolean
            Dim ret As Boolean = False

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key.DeleteValue(valueName, thrownOnMissingValue)
                ret = True
            Catch ex As Exception
                Errors.Add(ex.Message)
                ret = False
            End Try

            Return ret
        End Function

        Private Shared Function _GetValue(keyName As String, valueName As String) As Object
            Return _GetValue(keyName, valueName, True)
        End Function

        Private Shared Function _GetValue(keyName As String, valueName As String, writable As Boolean) As Object
            Dim oTmp As Object

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                oTmp = key.GetValue(valueName, Nothing)
            Catch ex As Exception
                oTmp = Nothing
            End Try

            Return oTmp
        End Function

        Private Shared Function _SetValue(keyName As String, valueName As String, value As Object, valueKind As RegistryValueKind) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey


                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key.SetValue(valueName, value, valueKind)

                Return True
            Catch ex As Exception
                Errors.Add(ex.Message)
                Return False
            End Try
        End Function


#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String, bRet As Boolean

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    bRet = defaultValue
                End If

                sTmp = Convert.ToString(oTmp)
                If sTmp.Equals("yes") Or sTmp.Equals("no") Then
                    bRet = sTmp.Equals("yes")
                Else
                    bRet = defaultValue
                End If
            Catch ex As Exception
                bRet = defaultValue
            End Try

            Return bRet
        End Function

        Public Shared Function SetValue_Boolean(keyName As String, valueName As String, value As Boolean) As Boolean
            If value Then
                Return SetValue_String(keyName, valueName, "yes")
            Else
                Return SetValue_String(keyName, valueName, "no")
            End If
        End Function
#End Region

#Region "binary"
        Public Shared Function GetValue_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_Binary(keyName As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

        Public Shared Function SetValue_Binary(keyName As String, valueName As String, value As Object) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.Binary)
        End Function


        Public Shared Function GetValue_ReadOnly_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_ReadOnly_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_ReadOnly_Binary(keyname As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = _GetValue(keyname, valueName, False)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

#End Region

#Region "string"
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function

        Public Shared Function SetValue_String(keyName As String, valueName As String, value As String) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.String)
        End Function


        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(keyname As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyname, valueName, False)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function
#End Region

#Region "expand string"
        Public Shared Function GetValue_ExpandString(keyName As String, valueName As String) As String
            Return GetValue_ExpandString(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ExpandString(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function

        Public Shared Function SetValue_ExpandString(keyName As String, valueName As String, value As String) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.ExpandString)
        End Function


        Public Shared Function GetValue_ReadOnly_ExpandString(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_ExpandString(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_ExpandString(keyname As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyname, valueName, False)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    iTmp = defaultValue
                Else
                    iTmp = Helpers.Types.CastToInteger_safe(oTmp)
                End If
            Catch ex As Exception
                iTmp = defaultValue
            End Try

            Return iTmp
        End Function

        Public Shared Function SetValue_Integer(keyName As String, valueName As String, value As Integer) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.DWord)
        End Function
#End Region

#Region "delete value"
        Public Shared Function DeleteValue(keyName As String, valueName As String) As Boolean
            Return DeleteValue(keyName, valueName, True)
        End Function

        Public Shared Function DeleteValue(keyName As String, valueName As String, thrownOnMissingValue As Boolean) As Boolean
            Return _DeleteValue(keyName, valueName, thrownOnMissingValue)
        End Function
#End Region
#End Region

#Region "misc"
        Public Shared Function CreateKey(keyName As String) As Boolean
            Dim bRet As Boolean = False

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key_new As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key_new = key_root.CreateSubKey(keyName)

                If Not key_new Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return bRet
        End Function

        Public Shared Function DeleteSubKey(keyName As String, keyToDelete As String) As Boolean
            If keyName.StartsWith("HKEY_CURRENT_USER\") Then
                keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                Return _DeleteSubKey(RegistryHive.CurrentUser, keyName, keyToDelete, True)
            ElseIf keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                Return _DeleteSubKey(RegistryHive.LocalMachine, keyName, keyToDelete, True)
            Else
                Return False
            End If
        End Function

        Public Shared Function DeleteSubKey(keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyName, keyToDelete, bKeepKeyToDelete)
        End Function

        Public Shared Function DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(keyHive, keyName, keyToDelete, bKeepKeyToDelete)
        End Function

        Private Shared Function _DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.CurrentUser Or keyHive = RegistryHive.LocalMachine Then
                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(keyHive, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = key_root.OpenSubKey(keyName, True)

                'delete all subkeys
                key.DeleteSubKeyTree(keyToDelete)

                If bKeepKeyToDelete Then
                    'put back the root sub key
                    key.CreateSubKey(keyToDelete)
                End If

                Return True
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return False
        End Function
#End Region
    End Class
#End Region


#Region "Processes"
    Public Class Processes
        Public Class HelpersProcessesException
            Inherits Exception

            Public Sub New()
            End Sub

            Public Sub New(ByVal message As String)
            End Sub

            Public Sub New(ByVal message As String, ByVal inner As Exception)
            End Sub
        End Class

        <DllImport("advapi32.dll", SetLastError:=True)>
        Private Shared Function OpenProcessToken(ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)>
        Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        Private Shared mLastError As String = ""
        Public Shared ReadOnly Property LastError As String
            Get
                Dim sTmp As String = mLastError

                mLastError = ""

                Return sTmp
            End Get
        End Property

        Private Shared Function ConvertPasswordStringToSecureString(ByVal str As String) As System.Security.SecureString
            Dim password As New System.Security.SecureString

            For Each c As Char In str.ToCharArray
                password.AppendChar(c)
            Next

            Return password
        End Function

        Public Shared Function IsProcessRunningExactMatch(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String


            sSearchProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

            If Not sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower & ".exe"
            End If

            Try
                For Each p As Process In Process.GetProcesses()
                    Try
                        sProcessName__ToLower = IO.Path.GetFileName(p.Modules(0).FileName).ToLower
                    Catch ex As Exception
                        sProcessName__ToLower = ""
                    End Try

                    If sSearchProcessName__ToLower.Equals(sProcessName__ToLower) Then
                        bRet = True
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function IsProcessRunning(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sProcessName__ToLower As String

            Try
                sProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

                If sProcessName.EndsWith(".exe") Then
                    sProcessName__ToLower = sProcessName__ToLower.Replace(".exe", "")
                End If


                Dim p As Process() = Process.GetProcessesByName(sProcessName__ToLower)

                If p.Count > 0 Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function IsProcessRunning_broken(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String


            sSearchProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            Try
                For Each p As Process In Process.GetProcesses
                    sProcessName__ToLower = p.ProcessName.ToLower
                    If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                        bRet = True
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function KillProcess(Pid As Integer) As Boolean
            Try
                Dim p As Process = Process.GetProcessById(Pid)

                If p IsNot Nothing Then
                    p.Kill()
                End If

                Return True
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Function KillProcesses(sProcessName As String) As Integer
            Dim sSearchProcessName__ToLower As String, sProcessName__ToLower As String
            Dim iCount As Integer = 0

            sSearchProcessName__ToLower = sProcessName.ToLower

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            Try
                For Each p As Process In Process.GetProcesses
                    sProcessName__ToLower = p.ProcessName.ToLower
                    If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                        Try
                            p.Kill()
                            iCount += 1
                        Catch ex As Exception

                        End Try
                    End If
                Next
            Catch ex As Exception
                iCount = -1
            End Try

            Return iCount
        End Function

        Public Shared Function IsUserLoggedIn(UserName As String, Optional CompletelyArbitraryProcessCountThreshold As Integer = 2) As Boolean
            Dim iCount As Integer

            iCount = CountProcessesOwnedByUser(UserName)

            If iCount >= CompletelyArbitraryProcessCountThreshold Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
            Dim wiName As String, lCount As Long

            lCount = 0

            For Each p As Process In Process.GetProcesses
                Try
                    Dim hToken As IntPtr
                    If OpenProcessToken(p.Handle, TokenAccessLevels.Query, hToken) Then
                        Using wi As New WindowsIdentity(hToken)
                            wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                            If wiName.ToLower = sUserNameToCheck.ToLower Then
                                lCount += 1
                            End If
                        End Using
                        CloseHandle(hToken)
                    End If
                Catch ex As Exception

                End Try
            Next

            Return lCount
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String) As Process
            Dim p As New Process

            If _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, sWorkingDirectory, False, p) Then
                Return p
            Else
                'wow, this makes sense
                Return p
            End If
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String, bHidden As Boolean) As Process
            Dim p As New Process

            If _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, sWorkingDirectory, bHidden, p) Then
                Return p
            Else
                'wow, this makes sense
                Return p
            End If
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOutMs As Integer) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, "", waitForExit, timeOutMs, False)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, "", waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, sWorkingDirectory, waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, bLoadUserProfile As Boolean, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, bLoadUserProfile, cmdArguments, sWorkingDirectory, waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcess(cmdLine As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOutMs As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, "", "", False, cmdArguments, "", waitForExit, timeOutMs, False)
        End Function

        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, bLoadUserProfile As Boolean, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            pInfo.WorkingDirectory = sWorkingDirectory
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = bLoadUserProfile
                pInfo.Verb = "runas"
                If hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOutMs)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        Errors.Add("process timed out ('" & pInfo.FileName & "')")

                        Return False
                    Else
                        Dim ret As Integer = -1
                        Try
                            ret = p.ExitCode
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function

        Public Shared Function StartProcessAsUserWithExitcode(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer, ByRef ExitCode As Integer) As Integer
            Return _StartProcessWithExitcode(cmdLine, sUserName, sPassword, cmdArguments, waitForExit, timeOut, False, ExitCode)
        End Function

        Private Shared Function _StartProcessWithExitcode(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer, hidden As Boolean, ByRef ExitCode As Integer) As Integer
            Dim pInfo As New ProcessStartInfo()
            Dim iExitCode As Integer = -1

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = True
                pInfo.Verb = "runas"
                If hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOut)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        Errors.Add("process timed out ('" & pInfo.FileName & "')")

                        Return False
                    Else
                        Dim ret As Integer = -1
                        Try
                            iExitCode = p.ExitCode
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function


        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, WorkingDirectory As String, Hidden As Boolean, ByRef proc As Process) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = True
                pInfo.WorkingDirectory = WorkingDirectory
                pInfo.Verb = "runas"
                If Hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            proc = New Process

            Try
                proc = Process.Start(pInfo)

                If proc.Id <= 0 Then
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function

        Public Shared Function IsAppAlreadyRunning() As Boolean
            Dim appProc() As Process
            Dim strModName, strProcName As String

            Try
                strModName = Process.GetCurrentProcess.MainModule.ModuleName
                strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

                appProc = Process.GetProcessesByName(strProcName)

                If appProc.Length > 1 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Function KillPreviousInstances() As Integer
            Dim thisProcess As Process = Process.GetCurrentProcess()
            Dim thisExe As String = IO.Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().Location)
            Dim pList() As System.Diagnostics.Process = System.Diagnostics.Process.GetProcessesByName(thisExe)
            Dim killCount As Integer = 0

            Try
                For Each eachProcess As Process In pList
                    If (Not thisProcess.Equals(eachProcess)) Then
                        killCount += 1
                        eachProcess.Kill()
                    End If
                Next
            Catch ex As Exception
                Errors.Add(ex.Message)
                killCount = -1
            End Try

            Return killCount
        End Function
    End Class
#End Region


#Region "Windows User"
    Public Class WindowsUser
        Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String], ByVal lpszDomain As [String], ByVal lpszPassword As [String], ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Boolean
        <DllImport("kernel32.dll")> Public Shared Function FormatMessage(ByVal dwFlags As Integer, ByRef lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByRef lpBuffer As [String], ByVal nSize As Integer, ByRef Arguments As IntPtr) As Integer
        End Function

        Public Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean

        Public Shared Function ConvertUsernameToSid(UserName As String, ByRef sId As String) As Boolean
            Try
                Dim f As New NTAccount(UserName)
                Dim s As SecurityIdentifier = DirectCast(f.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                sId = s.ToString()

                Return True
            Catch ex As Exception
                sId = ""

                Errors.Add(ex.Message)

                Return False
            End Try
        End Function

        Public Shared Function GetProfileDirectory(UserName As String, ByRef ProfileDir As String) As Boolean
            Dim sSid As String = "", sDir As String = ""

            If ConvertUsernameToSid(UserName, sSid) Then
                ProfileDir = Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\" & sSid, "ProfileImagePath")

                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function GetProfileTempDirectory(UserName As String) As String
            Dim sDir As String = ""

            GetProfileTempDirectory(UserName, sDir)

            Return sDir
        End Function

        Public Shared Function GetProfileTempDirectory(UserName As String, ByRef ProfileTempDir As String) As Boolean
            Dim sDir As String = ""

            If GetProfileDirectory(UserName, sDir) Then
                ProfileTempDir = "%USERPROFILE%\AppData\Local\Temp".Replace("%USERPROFILE%", sDir)

                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CheckLogon(sUsername As String, sPassword As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False

            Try
                Dim tokenHandle As New IntPtr(0)
                Dim UserName, MachineName, Pwd As String
                'The MachineName property gets the name of your computer.
                MachineName = System.Environment.MachineName
                UserName = sUsername
                Pwd = sPassword

                Const LOGON32_PROVIDER_DEFAULT As Integer = 0
                Const LOGON32_LOGON_INTERACTIVE As Integer = 2
                tokenHandle = IntPtr.Zero
                'Call the LogonUser function to obtain a handle to an access token.
                Dim returnValue As Boolean = LogonUser(UserName, MachineName, Pwd, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, tokenHandle)

                If returnValue = False Then
                    'This function returns the error code that the last unmanaged function returned.
                    Dim ret As Integer = Marshal.GetLastWin32Error()
                    ErrorMessage = GetErrorMessage(ret)
                    bRet = False
                Else
                    'Create the WindowsIdentity object for the Windows user account that is
                    'represented by the tokenHandle token.
                    Dim newId As New WindowsIdentity(tokenHandle)
                    Dim userperm As New WindowsPrincipal(newId)
                    'Verify whether the Windows user has administrative credentials.
                    If userperm.IsInRole(WindowsBuiltInRole.Administrator) Then
                        ErrorMessage = "ok"
                    Else
                        ErrorMessage = "ok, but not an admin"
                    End If

                    bRet = True
                End If

                'Free the access token.
                If Not System.IntPtr.op_Equality(tokenHandle, IntPtr.Zero) Then
                    CloseHandle(tokenHandle)
                End If
            Catch ex As Exception
                ErrorMessage = "Exception occurred. " + ex.Message
            End Try

            Return bRet
        End Function

        Private Shared Function GetErrorMessage(ByVal errorCode As Integer) As String
            Dim FORMAT_MESSAGE_ALLOCATE_BUFFER As Integer = &H100
            Dim FORMAT_MESSAGE_IGNORE_INSERTS As Integer = &H200
            Dim FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000

            Dim msgSize As Integer = 255
            Dim lpMsgBuf As String = ""
            Dim dwFlags As Integer = FORMAT_MESSAGE_ALLOCATE_BUFFER Or FORMAT_MESSAGE_FROM_SYSTEM Or FORMAT_MESSAGE_IGNORE_INSERTS

            Dim lpSource As IntPtr = IntPtr.Zero
            Dim lpArguments As IntPtr = IntPtr.Zero
            'Call the FormatMessage function to format the message.
            Dim returnVal As Integer = FormatMessage(dwFlags, lpSource, errorCode, 0, lpMsgBuf,
                    msgSize, lpArguments)
            If returnVal = 0 Then
                Throw New Exception("Failed to format message for error code " + errorCode.ToString() + ". ")
            End If

            Return lpMsgBuf
        End Function

        Public Shared Function FindUser(ByVal sUsername As String, Optional ByVal sGroup As String = "Users") As Boolean
            Try
                Dim oUser As Object, oGroup As Object

                oGroup = GetObject("WinNT://" & Environment.MachineName & "/" & sGroup)

                For Each oUser In oGroup.Members
                    Try
                        If Trim(sUsername.ToLower) = Trim(oUser.Name.ToString().ToLower) Then
                            Return True
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                Next

                oGroup = Nothing
                oUser = Nothing

                'Return True
            Catch ex As Exception
                'Return False
            End Try

            Return False
        End Function
    End Class
#End Region


#Region "UI"
    Public Class UI
        Public Shared Function ProgressColor(Value As Integer, MaxValue As Integer) As Color
            Return ProgressColor(CInt(100 * Value / MaxValue))
        End Function

        Public Shared Function ProgressColor(Percentage As Integer) As Color
            If Percentage > 100 Then Percentage = 100
            If Percentage < 0 Then Percentage = 0

            Dim _r As Integer = CInt(255 * (1 - (Percentage / 100)))
            Dim _g As Integer = CInt(255 * (Percentage / 100))
            Dim _b As Integer = 0

            Return Color.FromArgb(_r, _g, _b)
        End Function
    End Class
#End Region
End Class