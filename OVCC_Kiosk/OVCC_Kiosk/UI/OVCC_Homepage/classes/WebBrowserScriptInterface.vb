﻿Imports System.Runtime.InteropServices

<Assembly: ClassInterface(ClassInterfaceType.AutoDual)>
<ComVisible(True)>
Public Class WebBrowserScriptInterface
    Public UI As New _UI
    Public Session As New _Session
    Public Languages As New _Languages
    Public Logger As New _Logger

    Public Sub New()
        UI = New _UI
        Session = New _Session
        Languages = New _Languages
        Logger = New _Logger
    End Sub

    Public Function InitialiseScriptInterface() As String
        Return "ok"
    End Function


    '-------------------------------------------------------------------------------------------------------------
    <System.Runtime.InteropServices.ComVisible(True)>
    Public Class _UI
        Public Sub CloseAll()
            'OnCloseUI(Me)
        End Sub

        Public Sub OpenPasswordDialog()
            'OnOpenPasswordDialog(Me)
        End Sub

        Public Sub OpenUrl(Url As String)
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "OpenUrl('" & Url & "')")

            SessionStuff.SessionAction.Add_Url(Url)
        End Sub

        Public Function OpenUrlByName(UrlName As String) As Boolean
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "OpenUrlByName('" & UrlName & "')")

            Dim sErrorMessage As String = ""
            Dim oRet As Object = Helpers.Registry.FindValueByString(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Urls, UrlName, sErrorMessage)

            If oRet Is Nothing Then
                NamedPipe.Logger.Client.Error("WebBrowserScriptInterface", "OpenUrlByName('" & UrlName & "') error: " & sErrorMessage)

                Return False
            Else
                SessionStuff.SessionAction.Add_Url(oRet.ToString)

                Return True
            End If
        End Function

        Public Sub OpenApp(AppName As String)
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "OpenApp('" & AppName & "')")

            SessionStuff.SessionAction.Add_App(AppName)
        End Sub

        Public Sub OpenAirlinesDialog()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "OpenAirlinesDialog()")

            SessionStuff.SessionAction.Add_AirlinesDialog()
        End Sub

        Public Sub OpenOfficeDialog()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "OpenOfficeDialog()")

            SessionStuff.SessionAction.Add_OfficeDialog()
        End Sub

        Public Sub ShowHomepageMenu_Languages()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "ShowHomepageMenu_Languages()")

            Globals.UI.HOMEPAGE_MENU_LANGUAGES_VISIBLE = True
        End Sub

        Public Sub HideHomepageMenu()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "HideHomepageMenu()")

            Globals.UI.HOMEPAGE_MENU_LANGUAGES_VISIBLE = False
        End Sub

        Public Function IsHomepageMenuVisible() As Boolean
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "IsHomepageMenuVisible()")

            Return Globals.UI.HOMEPAGE_MENU_LANGUAGES_VISIBLE
        End Function


        Public Sub ShowInformationPanel()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "ShowInformationPanel()")

            Globals.UI.INFORMATION_PANEL_VISIBLE = True
        End Sub

        Public Sub HideInformationPanel()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "HideInformationPanel()")

            Globals.UI.INFORMATION_PANEL_VISIBLE = False
        End Sub

        Public Function IsInformationPanelVisible() As Boolean
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "IsInformationPanelVisible()")

            Return Globals.UI.INFORMATION_PANEL_VISIBLE
        End Function


        Public Sub ShowLogoutButton()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "ShowLogoutButton()")

            Globals.UI.LOGOUT_BUTTON_VISIBLE = True
        End Sub

        Public Sub HideLogoutButton()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "HideLogoutButton()")

            Globals.UI.LOGOUT_BUTTON_VISIBLE = False
        End Sub

        Public Function IsLogoutButtonVisible() As Boolean
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "IsLogoutButtonVisible()")

            Return Globals.UI.LOGOUT_BUTTON_VISIBLE
        End Function


        Public Function IsOfficeInstalled() As Boolean
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "IsOfficeInstalled()")

            Return Office.IsOfficeInstalled()
        End Function


        Public Sub OpenCustomDialog(pFormTitle As String, pFormWidth As Integer, pFormHeight As Integer, pContentUrl As String, pAutoCloseDelay As Integer)
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "OpenCustomDialog('" & pFormTitle & "'," & pFormWidth & "," & pFormHeight & ",'" & pContentUrl & "'," & pAutoCloseDelay & ")")

            Globals.Session.CustomDialog.FormTitle = pFormTitle
            Globals.Session.CustomDialog.FormWidth = pFormWidth
            Globals.Session.CustomDialog.FormHeight = pFormHeight
            Globals.Session.CustomDialog.ContentUrl = pContentUrl
            Globals.Session.CustomDialog.AutoCloseDelay = pAutoCloseDelay

            Globals.Session.CustomDialogRequested = True
        End Sub
    End Class


    '-------------------------------------------------------------------------------------------------------------
    <System.Runtime.InteropServices.ComVisible(True)>
    Public Class _Session
        Public Sub TriggerTerms()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "TriggerTerms()")

            If Not SessionStuff.IsTermsAccepted Then
                NamedPipe.Session.Client.SessionTermsRequested()
            End If
        End Sub

        Public Function IsActive() As Boolean
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "IsActive()")

            Return SessionStuff.IsSessionActive
        End Function

        Public Function SessionId() As String
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "SessionId()")

            Return SessionStuff.GetSessionId
        End Function

        Public Sub SessionLogout()
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "SessionLogout()")

            NamedPipe.Session.Client.SessionLogoutRequested()
        End Sub
    End Class


    '-------------------------------------------------------------------------------------------------------------
    <System.Runtime.InteropServices.ComVisible(True)>
    Public Class _Languages
        Public Sub SetActiveLanguage(LanguageId As Integer)
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "SetActiveLanguage(" & LanguageId & ")")

            NamedPipe.Language.Client.SetActiveLanguage(LanguageId)
        End Sub

        Public Function GetLanguagesAsJson() As String
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "GetLanguagesAsJson()")

            Return Localisation.LanguagesAsJson()
        End Function

        Public Function GetLanguageStringsAsJson() As String
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "GetLanguageStringsAsJson()")

            Return Localisation.LoadStringsAsJson()
        End Function

        Public Function LoadString(StringId As Integer) As String
            NamedPipe.Logger.Client.Debug("WebBrowserScriptInterface", "LoadString(" & StringId & ")")

            Return Localisation.LoadString(StringId)
        End Function
    End Class


    '-------------------------------------------------------------------------------------------------------------
    <System.Runtime.InteropServices.ComVisible(True)>
    Public Class _Logger
        Public Sub Message(sMessage As String)
            NamedPipe.Logger.Client.Message("THEME", sMessage)
        End Sub

        Public Sub Warning(sMessage As String)
            NamedPipe.Logger.Client.Warning("THEME", sMessage)
        End Sub

        Public Sub [Error](sMessage As String)
            NamedPipe.Logger.Client.Error("THEME", sMessage)
        End Sub

        Public Sub Debug(sMessage As String)
            NamedPipe.Logger.Client.Debug("THEME", sMessage)
        End Sub
    End Class
End Class
