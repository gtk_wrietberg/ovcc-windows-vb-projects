﻿Public Class Localisation
    Private Shared ReadOnly DEFAULT_TERMS_FILE As String = Globals.Url.Path2Url(IO.Path.Combine(Settings.Constants.Paths.Folders.Kiosk_Terms, "tandc_content_english.html"))

    Public Class Language
        Private mId As Integer
        Public Property Id() As Integer
            Get
                Return mId
            End Get
            Set(ByVal value As Integer)
                mId = value
            End Set
        End Property

        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mDescription As String
        Public Property Description() As String
            Get
                Return mDescription
            End Get
            Set(ByVal value As String)
                mDescription = value
            End Set
        End Property

        Private mFile As String
        Public Property File() As String
            Get
                Return mFile
            End Get
            Set(ByVal value As String)
                mFile = value
            End Set
        End Property

        Private mTermsFile As String
        Public Property TermsFile() As String
            Get
                Return mTermsFile
            End Get
            Set(ByVal value As String)
                mTermsFile = value
            End Set
        End Property

        Private mIcon As String
        Public Property Icon() As String
            Get
                Return mIcon
            End Get
            Set(ByVal value As String)
                mIcon = value
            End Set
        End Property

        Private mIconAbsolute As String
        Public Property IconAbsolute() As String
            Get
                Return mIconAbsolute
            End Get
            Set(ByVal value As String)
                mIconAbsolute = value
            End Set
        End Property

        'Special properties for javascript
        Public ReadOnly Property value() As String
            Get
                Return mId
            End Get
        End Property

        Public ReadOnly Property text() As String
            Get
                Return mName
            End Get
        End Property

        Public ReadOnly Property innerHTML() As String
            Get
                Return mName
            End Get
        End Property

        Public ReadOnly Property data_image() As String
            Get
                Return mIcon
            End Get
        End Property

        '----------------------------
        'language strings
        Public Class LanguageString
            Private mId As Integer
            Public Property Id() As Integer
                Get
                    Return mId
                End Get
                Set(ByVal value As Integer)
                    mId = value
                End Set
            End Property

            Private mString As String
            Public Property [String]() As String
                Get
                    Return mString
                End Get
                Set(ByVal value As String)
                    mString = value
                End Set
            End Property
        End Class

        Private mLanguageStrings As New List(Of LanguageString)
        Public Sub ClearStrings()
            mLanguageStrings.Clear()
        End Sub

        Public ReadOnly Property LanguageStrings() As List(Of LanguageString)
            Get
                Return mLanguageStrings
            End Get
        End Property

        Public Sub AddString(StringId As Integer, StringValue As String)
            Dim _ls As New LanguageString
            _ls.Id = StringId
            _ls.String = StringValue
            mLanguageStrings.Add(_ls)
        End Sub

        Public Function GetString(StringId As Integer) As String
            For Each _ls As LanguageString In mLanguageStrings
                If _ls.Id = StringId Then
                    Return _ls.String
                End If
            Next

            Return "<String:" & mId & "," & StringId & ">"
        End Function
    End Class

    Private Shared mLanguages As New List(Of Language)

    Public Shared Function LoadLanguages() As Boolean
        Try
            mLanguages = New List(Of Language)

            Dim mDocument_Languages As System.Xml.XmlDocument = New System.Xml.XmlDocument
            mDocument_Languages.LoadXml(_File2String(Settings.Constants.Paths.Files.Data.LANGUAGES_FILE))

            Dim mLanguageList As System.Xml.XmlNodeList = mDocument_Languages.SelectNodes("/languages/language")

            'Console.WriteLine(mLanguageList.Count)

            For Each mLanguage As System.Xml.XmlNode In mLanguageList
                Dim _language As New Language
                'Console.WriteLine(mLanguage.InnerXml)
                _language.Id = _String2Int(mLanguage.SelectSingleNode("id").InnerText)
                _language.Name = mLanguage.SelectSingleNode("name").InnerText
                _language.Description = mLanguage.SelectSingleNode("description").InnerText
                _language.File = mLanguage.SelectSingleNode("file").InnerText
                _language.TermsFile = Globals.Url.Path2Url(IO.Path.Combine(Settings.Constants.Paths.Folders.Kiosk_Terms, mLanguage.SelectSingleNode("terms_file").InnerText))
                _language.Icon = Globals.Url.Path2Url(IO.Path.Combine(Settings.Constants.Paths.Folders.Kiosk_LanguageFlags, mLanguage.SelectSingleNode("icon").InnerText))
                _language.IconAbsolute = IO.Path.Combine(Settings.Constants.Paths.Folders.Kiosk_LanguageFlags, mLanguage.SelectSingleNode("icon").InnerText)


                _language.ClearStrings()
                Dim sLanguageFile As String = IO.Path.Combine(Settings.Constants.Paths.Folders.Kiosk_Config_Languages, mLanguage.SelectSingleNode("file").InnerText)

                If IO.File.Exists(sLanguageFile) Then
                    Try
                        Dim mDocument_Strings As System.Xml.XmlDocument = New System.Xml.XmlDocument
                        mDocument_Strings.LoadXml(_File2String(sLanguageFile))

                        Dim mStringList As System.Xml.XmlNodeList = mDocument_Strings.SelectNodes("/language/string")
                        For Each mString As System.Xml.XmlNode In mStringList
                            _language.AddString(_String2Int(mString.Attributes.GetNamedItem("id").Value), mString.InnerText)
                        Next
                    Catch ex As Exception
                        _language.ClearStrings()

                        NamedPipe.Logger.Client.Error("language file '" & sLanguageFile & "' error: " & ex.Message)
                    End Try
                Else
                    NamedPipe.Logger.Client.Error("language file '" & sLanguageFile & "' not found")
                End If

                mLanguages.Add(_language)
            Next


            Return True
        Catch ex As Exception
            NamedPipe.Logger.Client.Error("LoadLanguages() error: " & ex.Message)
            Console.WriteLine("language error: " & ex.Message)
            mlastError = ex.Message
            Return False
        End Try
    End Function


    Public Shared ReadOnly Property Languages() As List(Of Language)
        Get
            Return mLanguages
        End Get
    End Property

    Public Shared ReadOnly Property LanguagesCount As Integer
        Get
            Return mLanguages.Count
        End Get
    End Property

    Public Shared ReadOnly Property LanguagesAsJson() As String
        Get
            Return Newtonsoft.Json.JsonConvert.SerializeObject(mLanguages)
        End Get
    End Property

    Public Shared Function GetActiveLanguageId() As Integer
        Return Settings.Get.Integer(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Languages, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Languages__ActiveLanguageId, 0)
    End Function

    Public Shared Function GetDefaultLanguageId() As Integer
        Return Settings.Get.Integer(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__DefaultLanguageId, 0)
    End Function

    Public Shared Function LoadString(StringId As Integer) As String
        Dim activeLanguageId As Integer = GetActiveLanguageId()

        Return LoadString(activeLanguageId, StringId)
    End Function

    Public Shared Function LoadString(LanguageId As Integer, StringId As Integer) As String
        For Each _language As Language In mLanguages
            If _language.Id = LanguageId Then
                Return _language.GetString(StringId)
            End If
        Next

        Return "<String:" & LanguageId & "," & StringId & ">"
    End Function

    Public Shared Function LoadStringsAsJson() As String
        Dim activeLanguageId As Integer = GetActiveLanguageId()

        For Each _language As Language In mLanguages
            If _language.Id = activeLanguageId Then
                Return Newtonsoft.Json.JsonConvert.SerializeObject(_language.LanguageStrings)
            End If
        Next

        Return ""
    End Function


    Public Shared Function GetTermsFile() As String
        Dim activeLanguageId As Integer = GetActiveLanguageId()

        For Each _language As Language In mLanguages
            If _language.Id = activeLanguageId Then
                Return _language.TermsFile
            End If
        Next

        Return DEFAULT_TERMS_FILE
    End Function

    Public Shared Function GetFlagImagePath() As String
        Dim activeLanguageId As Integer = GetActiveLanguageId()

        For Each _language As Language In mLanguages
            If _language.Id = activeLanguageId Then
                Return _language.IconAbsolute
            End If
        Next

        Return ""
    End Function


    Private Shared mlastError As String
    Public Shared ReadOnly Property LastError() As String
        Get
            Return mlastError
        End Get
    End Property

    Private Shared Function _String2Int(s As String) As Integer
        Dim _tmp As Integer = 0
        If Integer.TryParse(s, _tmp) Then

        End If

        Return _tmp
    End Function

    Private Shared Function _File2String(f As String) As String
        Dim fileBytes As Byte()

        fileBytes = System.IO.File.ReadAllBytes(f)

        Return System.Text.Encoding.UTF8.GetString(fileBytes)
    End Function
End Class
