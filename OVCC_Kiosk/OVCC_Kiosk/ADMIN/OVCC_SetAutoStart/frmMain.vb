﻿Imports System.ComponentModel

Public Class frmMain
    Private Class _UserState
        Private mName As String = ""
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mStatus As Boolean
        Public Property Status() As Boolean
            Get
                Return mStatus
            End Get
            Set(ByVal value As Boolean)
                mStatus = value
            End Set
        End Property

        Private mErrorMessage As String = ""
        Public Property ErrorMessage() As String
            Get
                Return mErrorMessage
            End Get
            Set(ByVal value As String)
                mErrorMessage = value
            End Set
        End Property

        Public ReadOnly Property HasError() As Boolean
            Get
                Return Not mErrorMessage.Equals("")
            End Get
        End Property
    End Class


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime

        'first logging
        NamedPipe.Logger.Client.Message(New String("*", 50))
        NamedPipe.Logger.Client.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        NamedPipe.Logger.Client.Message("started")

        LoadSettings()

        If Globals.Application.Debug Then
            NamedPipe.Logger.Client.Message("debug mode on")
        End If


        If Environment.UserName.Equals(Settings.Constants.Users.Usernames.KIOSK_USER) Then
            NamedPipe.Logger.Client.Error("Please don't run as '" & Settings.Constants.Users.Usernames.KIOSK_USER & "'")
            Me.Close()
        End If


        lblResult.Text = ""


        bgWorker_Status.RunWorkerAsync()
    End Sub

    Private Sub LoadSettings()
        Globals.Application.Debug = Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__Debug, True)
    End Sub

    Private Sub ApplicationExit(Optional reason As String = "")
        NamedPipe.Logger.Client.Message("exiting")
        If reason <> "" Then
            NamedPipe.Logger.Client.Message("exit reason: " & reason)
        End If

        NamedPipe.Logger.Client.Message("bye")


        Dim frms As New List(Of Form)
        For Each frm As Form In My.Application.OpenForms
            frms.Add(frm)
        Next

        For Each frm As Form In frms
            If frm IsNot Nothing Then
                frm.Close()
            End If
        Next
    End Sub


    Private Sub picClose_MouseEnter(sender As Object, e As EventArgs) Handles picClose.MouseEnter
        picClose.Image = My.Resources.topbar_buttons__close_hover
    End Sub

    Private Sub picClose_MouseLeave(sender As Object, e As EventArgs) Handles picClose.MouseLeave
        picClose.Image = My.Resources.topbar_buttons__close_on
    End Sub


#Region "fake title drag"
    Private isMouseDown As Boolean = False
    Private mouseOffset As Point

    Private Sub picTopBarLeft_MouseDown(sender As Object, e As MouseEventArgs) Handles pnlTopBarLeft.MouseDown
        If e.Button = MouseButtons.Left Then
            ' Get the new position
            mouseOffset = New Point(-e.X - pnlContainer.Left, -e.Y - pnlContainer.Top)
            ' Set that left button is pressed
            isMouseDown = True
        End If
    End Sub

    Private Sub picTopBarLeft_MouseMove(sender As Object, e As MouseEventArgs) Handles pnlTopBarLeft.MouseMove
        If isMouseDown Then
            Dim mousePos As Point = Control.MousePosition
            ' Get the new form position
            mousePos.Offset(mouseOffset.X, mouseOffset.Y)
            Me.Location = mousePos
        End If
    End Sub

    Private Sub picTopBarLeft_MouseUp(sender As Object, e As MouseEventArgs) Handles pnlTopBarLeft.MouseUp
        If e.Button = MouseButtons.Left Then
            isMouseDown = False
        End If
    End Sub

    Private Sub lblTitle_MouseDown(sender As Object, e As MouseEventArgs) Handles lblTitle.MouseDown
        If e.Button = MouseButtons.Left Then
            ' Get the new position
            mouseOffset = New Point(-e.X - pnlContainer.Left, -e.Y - pnlContainer.Top)
            ' Set that left button is pressed
            isMouseDown = True
        End If
    End Sub

    Private Sub lblTitle_MouseMove(sender As Object, e As MouseEventArgs) Handles lblTitle.MouseMove
        If isMouseDown Then
            Dim mousePos As Point = Control.MousePosition
            ' Get the new form position
            mousePos.Offset(mouseOffset.X, mouseOffset.Y)
            Me.Location = mousePos
        End If
    End Sub

    Private Sub lblTitle_MouseUp(sender As Object, e As MouseEventArgs) Handles lblTitle.MouseUp
        If e.Button = MouseButtons.Left Then
            isMouseDown = False
        End If
    End Sub

    Private Sub picLogo_MouseDown(sender As Object, e As MouseEventArgs) Handles picLogo.MouseDown
        If e.Button = MouseButtons.Left Then
            ' Get the new position
            mouseOffset = New Point(-e.X - pnlContainer.Left, -e.Y - pnlContainer.Top)
            ' Set that left button is pressed
            isMouseDown = True
        End If
    End Sub

    Private Sub picLogo_MouseMove(sender As Object, e As MouseEventArgs) Handles picLogo.MouseMove
        If isMouseDown Then
            Dim mousePos As Point = Control.MousePosition
            ' Get the new form position
            mousePos.Offset(mouseOffset.X, mouseOffset.Y)
            Me.Location = mousePos
        End If
    End Sub

    Private Sub picLogo_MouseUp(sender As Object, e As MouseEventArgs) Handles picLogo.MouseUp
        If e.Button = MouseButtons.Left Then
            isMouseDown = False
        End If
    End Sub
#End Region


    Private Sub btnSetAutoStart_Click(sender As Object, e As EventArgs) Handles btnSetAutoStart.Click
        NamedPipe.Logger.Client.Message("set autostart")


        btnSetAutoStart.Enabled = False
        Me.Cursor = Cursors.WaitCursor

        bgWorkerToggleUser.RunWorkerAsync()
    End Sub

    Private Sub bgWorkerToggleUser_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorkerToggleUser.DoWork
        Dim sError As String = ""
        Dim oUserState As _UserState

        oUserState = New _UserState
        oUserState.Name = "User toggle"

        Dim sRet As String = WindowsUser.User2Guest(Settings.Constants.Users.Usernames.KIOSK_USER, sError)

        oUserState.ErrorMessage = sError

        bgWorkerToggleUser.ReportProgress(0, oUserState)
    End Sub

    Private Sub bgWorkerToggleUser_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bgWorkerToggleUser.ProgressChanged
        If e.UserState.HasError Then
            lblResult.ForeColor = Color.DarkRed
            lblResult.Text = e.UserState.ErrorMessage

            NamedPipe.Logger.Client.Error(e.UserState.ErrorMessage)
        Else
            lblResult.ForeColor = Color.DarkGreen
            lblResult.Text = "Ok"

            Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__AutoStartActive, True)
            WindowsUser.RemovePasswordRestrictions(Settings.Constants.Users.Usernames.KIOSK_USER)

            NamedPipe.Logger.Client.Message("ok")
        End If

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub bgWorker_Status_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgWorker_Status.DoWork
        Threading.Thread.Sleep(1000)


        Dim oUserState As _UserState


        oUserState = New _UserState
        oUserState.Name = "Plumber"
        If ServiceInstaller.GetServiceStatus(Settings.Constants.Services.Plumber.Name) = ServiceProcess.ServiceControllerStatus.Running Then
            oUserState.Status = True
        Else
            oUserState.Status = False
        End If
        bgWorker_Status.ReportProgress(0, oUserState)


        oUserState = New _UserState
        oUserState.Name = "Watchdog"
        If ServiceInstaller.GetServiceStatus(Settings.Constants.Services.Watchdog.Name) = ServiceProcess.ServiceControllerStatus.Running Then
            oUserState.Status = True
        Else
            oUserState.Status = False
        End If
        bgWorker_Status.ReportProgress(0, oUserState)


        oUserState = New _UserState
        oUserState.Name = "Communication"
        If NamedPipe.Session.Client.CommTest() Then
            oUserState.Status = True
        Else
            oUserState.Status = False
        End If
        bgWorker_Status.ReportProgress(0, oUserState)
    End Sub

    Private Sub bgWorker_Status_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bgWorker_Status.ProgressChanged
        Select Case e.UserState.Name
            Case "Plumber"
                If e.UserState.Status Then
                    picStatus_Plumber.Image = My.Resources.ok
                Else
                    picStatus_Plumber.Image = My.Resources.nok
                End If
            Case "Watchdog"
                If e.UserState.Status Then
                    picStatus_Watchdog.Image = My.Resources.ok
                Else
                    picStatus_Watchdog.Image = My.Resources.nok
                End If
            Case "Communication"
                If e.UserState.Status Then
                    picStatus_Communication.Image = My.Resources.ok
                Else
                    picStatus_Communication.Image = My.Resources.nok
                End If
        End Select
    End Sub

    Private Sub picClose_Click(sender As Object, e As EventArgs) Handles picClose.Click
        If Not bgWorkerToggleUser.IsBusy And Not bgWorker_Status.IsBusy Then
            ApplicationExit("close button")
        End If
    End Sub
End Class
