﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        pnlHeader = New Panel()
        picClose = New PictureBox()
        pnlTopBarLeft = New Panel()
        lblTitle = New Label()
        picLogo = New PictureBox()
        pnlContainer = New Panel()
        grpAutoStart = New GroupBox()
        lblAutoStartTitle = New Label()
        lblResult = New Label()
        btnSetAutoStart = New Button()
        groupStatus = New GroupBox()
        picStatus_Communication = New PictureBox()
        lblStatus_Communication = New Label()
        picStatus_Watchdog = New PictureBox()
        lblStatus_Watchdog = New Label()
        picStatus_Plumber = New PictureBox()
        lblStatus_Plumber = New Label()
        bgWorkerToggleUser = New ComponentModel.BackgroundWorker()
        bgWorker_Status = New ComponentModel.BackgroundWorker()
        pnlBorderBackground = New Panel()
        pnlHeader.SuspendLayout()
        CType(picClose, ComponentModel.ISupportInitialize).BeginInit()
        pnlTopBarLeft.SuspendLayout()
        CType(picLogo, ComponentModel.ISupportInitialize).BeginInit()
        pnlContainer.SuspendLayout()
        grpAutoStart.SuspendLayout()
        groupStatus.SuspendLayout()
        CType(picStatus_Communication, ComponentModel.ISupportInitialize).BeginInit()
        CType(picStatus_Watchdog, ComponentModel.ISupportInitialize).BeginInit()
        CType(picStatus_Plumber, ComponentModel.ISupportInitialize).BeginInit()
        pnlBorderBackground.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlHeader
        ' 
        pnlHeader.BackColor = Color.Gainsboro
        pnlHeader.Controls.Add(picClose)
        pnlHeader.Controls.Add(pnlTopBarLeft)
        pnlHeader.Dock = DockStyle.Top
        pnlHeader.Location = New Point(0, 0)
        pnlHeader.Name = "pnlHeader"
        pnlHeader.Size = New Size(392, 40)
        pnlHeader.TabIndex = 0
        ' 
        ' picClose
        ' 
        picClose.Dock = DockStyle.Right
        picClose.Image = My.Resources.Resources.topbar_buttons__close_on
        picClose.Location = New Point(347, 0)
        picClose.Name = "picClose"
        picClose.Size = New Size(45, 40)
        picClose.TabIndex = 0
        picClose.TabStop = False
        ' 
        ' pnlTopBarLeft
        ' 
        pnlTopBarLeft.Controls.Add(lblTitle)
        pnlTopBarLeft.Controls.Add(picLogo)
        pnlTopBarLeft.Dock = DockStyle.Left
        pnlTopBarLeft.Location = New Point(0, 0)
        pnlTopBarLeft.Name = "pnlTopBarLeft"
        pnlTopBarLeft.Size = New Size(379, 40)
        pnlTopBarLeft.TabIndex = 2
        ' 
        ' lblTitle
        ' 
        lblTitle.Dock = DockStyle.Left
        lblTitle.Font = New Font("Segoe UI", 9.75F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        lblTitle.Location = New Point(34, 0)
        lblTitle.Name = "lblTitle"
        lblTitle.Size = New Size(254, 40)
        lblTitle.TabIndex = 1
        lblTitle.Text = "   OVCC AutoStart"
        lblTitle.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' picLogo
        ' 
        picLogo.Dock = DockStyle.Left
        picLogo.Image = My.Resources.Resources.guest_tek
        picLogo.Location = New Point(0, 0)
        picLogo.Name = "picLogo"
        picLogo.Size = New Size(34, 40)
        picLogo.SizeMode = PictureBoxSizeMode.Zoom
        picLogo.TabIndex = 2
        picLogo.TabStop = False
        ' 
        ' pnlContainer
        ' 
        pnlContainer.BackColor = Color.White
        pnlContainer.BorderStyle = BorderStyle.FixedSingle
        pnlContainer.Controls.Add(grpAutoStart)
        pnlContainer.Controls.Add(groupStatus)
        pnlContainer.Controls.Add(pnlHeader)
        pnlContainer.Location = New Point(3, 3)
        pnlContainer.Name = "pnlContainer"
        pnlContainer.Size = New Size(394, 152)
        pnlContainer.TabIndex = 1
        ' 
        ' grpAutoStart
        ' 
        grpAutoStart.Controls.Add(lblAutoStartTitle)
        grpAutoStart.Controls.Add(lblResult)
        grpAutoStart.Controls.Add(btnSetAutoStart)
        grpAutoStart.Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        grpAutoStart.Location = New Point(3, 46)
        grpAutoStart.Name = "grpAutoStart"
        grpAutoStart.Size = New Size(216, 102)
        grpAutoStart.TabIndex = 3
        grpAutoStart.TabStop = False
        ' 
        ' lblAutoStartTitle
        ' 
        lblAutoStartTitle.Font = New Font("Tahoma", 8.25F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        lblAutoStartTitle.Location = New Point(6, 12)
        lblAutoStartTitle.Name = "lblAutoStartTitle"
        lblAutoStartTitle.Size = New Size(204, 21)
        lblAutoStartTitle.TabIndex = 3
        lblAutoStartTitle.Text = "Set autostart, and reboot"
        lblAutoStartTitle.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' lblResult
        ' 
        lblResult.Location = New Point(6, 78)
        lblResult.Name = "lblResult"
        lblResult.Size = New Size(204, 21)
        lblResult.TabIndex = 2
        lblResult.Text = ">>result<<"
        lblResult.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' btnSetAutoStart
        ' 
        btnSetAutoStart.Location = New Point(6, 37)
        btnSetAutoStart.Name = "btnSetAutoStart"
        btnSetAutoStart.Size = New Size(204, 38)
        btnSetAutoStart.TabIndex = 1
        btnSetAutoStart.Text = "Set autostart"
        btnSetAutoStart.UseVisualStyleBackColor = True
        ' 
        ' groupStatus
        ' 
        groupStatus.Controls.Add(picStatus_Communication)
        groupStatus.Controls.Add(lblStatus_Communication)
        groupStatus.Controls.Add(picStatus_Watchdog)
        groupStatus.Controls.Add(lblStatus_Watchdog)
        groupStatus.Controls.Add(picStatus_Plumber)
        groupStatus.Controls.Add(lblStatus_Plumber)
        groupStatus.Font = New Font("Tahoma", 8.25F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        groupStatus.Location = New Point(225, 46)
        groupStatus.Name = "groupStatus"
        groupStatus.Size = New Size(164, 102)
        groupStatus.TabIndex = 2
        groupStatus.TabStop = False
        groupStatus.Text = "Status"
        ' 
        ' picStatus_Communication
        ' 
        picStatus_Communication.Location = New Point(133, 71)
        picStatus_Communication.Name = "picStatus_Communication"
        picStatus_Communication.Size = New Size(24, 24)
        picStatus_Communication.SizeMode = PictureBoxSizeMode.CenterImage
        picStatus_Communication.TabIndex = 5
        picStatus_Communication.TabStop = False
        ' 
        ' lblStatus_Communication
        ' 
        lblStatus_Communication.Location = New Point(6, 71)
        lblStatus_Communication.Name = "lblStatus_Communication"
        lblStatus_Communication.Size = New Size(121, 24)
        lblStatus_Communication.TabIndex = 4
        lblStatus_Communication.Text = "Communication"
        lblStatus_Communication.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' picStatus_Watchdog
        ' 
        picStatus_Watchdog.Location = New Point(133, 44)
        picStatus_Watchdog.Name = "picStatus_Watchdog"
        picStatus_Watchdog.Size = New Size(24, 24)
        picStatus_Watchdog.SizeMode = PictureBoxSizeMode.CenterImage
        picStatus_Watchdog.TabIndex = 3
        picStatus_Watchdog.TabStop = False
        ' 
        ' lblStatus_Watchdog
        ' 
        lblStatus_Watchdog.Location = New Point(6, 44)
        lblStatus_Watchdog.Name = "lblStatus_Watchdog"
        lblStatus_Watchdog.Size = New Size(121, 24)
        lblStatus_Watchdog.TabIndex = 2
        lblStatus_Watchdog.Text = "Watchdog service"
        lblStatus_Watchdog.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' picStatus_Plumber
        ' 
        picStatus_Plumber.Location = New Point(133, 17)
        picStatus_Plumber.Name = "picStatus_Plumber"
        picStatus_Plumber.Size = New Size(24, 24)
        picStatus_Plumber.SizeMode = PictureBoxSizeMode.CenterImage
        picStatus_Plumber.TabIndex = 1
        picStatus_Plumber.TabStop = False
        ' 
        ' lblStatus_Plumber
        ' 
        lblStatus_Plumber.Location = New Point(6, 17)
        lblStatus_Plumber.Name = "lblStatus_Plumber"
        lblStatus_Plumber.Size = New Size(121, 24)
        lblStatus_Plumber.TabIndex = 0
        lblStatus_Plumber.Text = "Communication service"
        lblStatus_Plumber.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' bgWorkerToggleUser
        ' 
        bgWorkerToggleUser.WorkerReportsProgress = True
        bgWorkerToggleUser.WorkerSupportsCancellation = True
        ' 
        ' bgWorker_Status
        ' 
        bgWorker_Status.WorkerReportsProgress = True
        bgWorker_Status.WorkerSupportsCancellation = True
        ' 
        ' pnlBorderBackground
        ' 
        pnlBorderBackground.BackColor = Color.Black
        pnlBorderBackground.Controls.Add(pnlContainer)
        pnlBorderBackground.Location = New Point(12, 12)
        pnlBorderBackground.Name = "pnlBorderBackground"
        pnlBorderBackground.Size = New Size(400, 158)
        pnlBorderBackground.TabIndex = 2
        ' 
        ' frmMain
        ' 
        AutoScaleDimensions = New SizeF(7F, 14F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(425, 183)
        ControlBox = False
        Controls.Add(pnlBorderBackground)
        Font = New Font("Tahoma", 9F, FontStyle.Regular, GraphicsUnit.Point, CByte(0))
        FormBorderStyle = FormBorderStyle.None
        Icon = CType(resources.GetObject("$this.Icon"), Icon)
        MaximizeBox = False
        MinimizeBox = False
        Name = "frmMain"
        StartPosition = FormStartPosition.CenterScreen
        Text = "OVCC AutoStart"
        pnlHeader.ResumeLayout(False)
        CType(picClose, ComponentModel.ISupportInitialize).EndInit()
        pnlTopBarLeft.ResumeLayout(False)
        CType(picLogo, ComponentModel.ISupportInitialize).EndInit()
        pnlContainer.ResumeLayout(False)
        grpAutoStart.ResumeLayout(False)
        groupStatus.ResumeLayout(False)
        CType(picStatus_Communication, ComponentModel.ISupportInitialize).EndInit()
        CType(picStatus_Watchdog, ComponentModel.ISupportInitialize).EndInit()
        CType(picStatus_Plumber, ComponentModel.ISupportInitialize).EndInit()
        pnlBorderBackground.ResumeLayout(False)
        ResumeLayout(False)
    End Sub

    Friend WithEvents pnlHeader As Panel
    Friend WithEvents picClose As PictureBox
    Friend WithEvents pnlContainer As Panel
    Friend WithEvents lblTitle As Label
    Friend WithEvents picLogo As PictureBox
    Friend WithEvents pnlTopBarLeft As Panel
    Friend WithEvents btnSetAutoStart As Button
    Friend WithEvents bgWorkerToggleUser As System.ComponentModel.BackgroundWorker
    Friend WithEvents groupStatus As GroupBox
    Friend WithEvents lblStatus_Plumber As Label
    Friend WithEvents picStatus_Watchdog As PictureBox
    Friend WithEvents lblStatus_Watchdog As Label
    Friend WithEvents picStatus_Plumber As PictureBox
    Friend WithEvents picStatus_Communication As PictureBox
    Friend WithEvents lblStatus_Communication As Label
    Friend WithEvents bgWorker_Status As System.ComponentModel.BackgroundWorker
    Friend WithEvents grpAutoStart As GroupBox
    Friend WithEvents lblResult As Label
    Friend WithEvents lblAutoStartTitle As Label
    Friend WithEvents pnlBorderBackground As Panel
End Class
