﻿Public Class frmMain
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime


        'first logging
        NamedPipe.Logger.Client.Message(New String("*", 50))
        NamedPipe.Logger.Client.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        NamedPipe.Logger.Client.Message("started")


        NamedPipe.System.Client.StartedAppStarter()


        LoadSettings()


        If Globals.Application.Debug Then
            NamedPipe.Logger.Client.Message("debug mode on")
        End If
    End Sub

    Private Sub ApplicationExit(Optional reason As String = "")
        NamedPipe.Logger.Client.Message("exiting")
        If reason <> "" Then
            NamedPipe.Logger.Client.Message("exit reason: " & reason)
        End If

        NamedPipe.Logger.Client.Message("bye")


        Dim frms As New List(Of Form)
        For Each frm As Form In My.Application.OpenForms
            frms.Add(frm)
        Next

        For Each frm As Form In frms
            If frm IsNot Nothing Then
                frm.Close()
            End If
        Next
    End Sub

    Private Sub LoadSettings()
        Globals.Application.Debug = Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__Debug, True)
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        tmrStart.Enabled = True
    End Sub

    Private Sub DoStuff()
        If Not Environment.UserName.EndsWith(Settings.Constants.Users.Usernames.KIOSK_USER) Then
            ApplicationExit("not needed for user '" & Environment.UserName & "'")
            Exit Sub
        End If


        NamedPipe.Logger.Client.Message("starting " & Settings.Constants.Paths.Files.Apps.Notification.Name & " (" & Settings.Constants.Paths.Files.Apps.Notification.Path & ")")
        Helpers.Processes.StartProcess(Settings.Constants.Paths.Files.Apps.Notification.Path,, False)


        NamedPipe.Logger.Client.Message("starting " & Settings.Constants.Paths.Files.Apps.Homepage.Name & " (" & Settings.Constants.Paths.Files.Apps.Homepage.Path & ")")
        Helpers.Processes.StartProcess(Settings.Constants.Paths.Files.Apps.Homepage.Path,, False)


        NamedPipe.Logger.Client.Message("starting " & Settings.Constants.Paths.Files.Apps.Logout.Name & " (" & Settings.Constants.Paths.Files.Apps.Logout.Path & ")")
        Helpers.Processes.StartProcess(Settings.Constants.Paths.Files.Apps.Logout.Path,, False)


        ApplicationExit()
    End Sub

    Private Sub tmrStart_Tick(sender As Object, e As EventArgs) Handles tmrStart.Tick
        tmrStart.Enabled = False

        DoStuff()
    End Sub

    Private Sub frmMain_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        NamedPipe.System.Client.ClosedAppStarter()
    End Sub
End Class
