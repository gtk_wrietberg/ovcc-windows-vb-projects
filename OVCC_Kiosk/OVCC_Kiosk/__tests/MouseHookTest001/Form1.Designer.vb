﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Button1 = New Button()
        txtDebug = New TextBox()
        Panel1 = New Panel()
        VScrollBar1 = New VScrollBar()
        Label1 = New Label()
        Label2 = New Label()
        Label3 = New Label()
        Label4 = New Label()
        Label5 = New Label()
        Label6 = New Label()
        Label7 = New Label()
        Label8 = New Label()
        Label9 = New Label()
        Label10 = New Label()
        Label11 = New Label()
        Label12 = New Label()
        Label13 = New Label()
        Label14 = New Label()
        Label15 = New Label()
        Label16 = New Label()
        Label17 = New Label()
        Label18 = New Label()
        Label19 = New Label()
        Label20 = New Label()
        Label21 = New Label()
        Label22 = New Label()
        Label23 = New Label()
        Label24 = New Label()
        Panel1.SuspendLayout()
        SuspendLayout()
        ' 
        ' Button1
        ' 
        Button1.Location = New Point(12, 12)
        Button1.Name = "Button1"
        Button1.Size = New Size(190, 125)
        Button1.TabIndex = 0
        Button1.Text = "Button1"
        Button1.UseVisualStyleBackColor = True
        ' 
        ' txtDebug
        ' 
        txtDebug.Location = New Point(208, 12)
        txtDebug.Multiline = True
        txtDebug.Name = "txtDebug"
        txtDebug.ScrollBars = ScrollBars.Vertical
        txtDebug.Size = New Size(740, 139)
        txtDebug.TabIndex = 1
        ' 
        ' Panel1
        ' 
        Panel1.BorderStyle = BorderStyle.FixedSingle
        Panel1.Controls.Add(Label17)
        Panel1.Controls.Add(Label18)
        Panel1.Controls.Add(Label19)
        Panel1.Controls.Add(Label20)
        Panel1.Controls.Add(Label21)
        Panel1.Controls.Add(Label22)
        Panel1.Controls.Add(Label23)
        Panel1.Controls.Add(Label24)
        Panel1.Controls.Add(Label9)
        Panel1.Controls.Add(Label10)
        Panel1.Controls.Add(Label11)
        Panel1.Controls.Add(Label12)
        Panel1.Controls.Add(Label13)
        Panel1.Controls.Add(Label14)
        Panel1.Controls.Add(Label15)
        Panel1.Controls.Add(Label16)
        Panel1.Controls.Add(Label5)
        Panel1.Controls.Add(Label6)
        Panel1.Controls.Add(Label7)
        Panel1.Controls.Add(Label8)
        Panel1.Controls.Add(Label3)
        Panel1.Controls.Add(Label4)
        Panel1.Controls.Add(Label2)
        Panel1.Controls.Add(Label1)
        Panel1.Location = New Point(229, 215)
        Panel1.Name = "Panel1"
        Panel1.Size = New Size(354, 189)
        Panel1.TabIndex = 2
        ' 
        ' VScrollBar1
        ' 
        VScrollBar1.Location = New Point(586, 215)
        VScrollBar1.Name = "VScrollBar1"
        VScrollBar1.Size = New Size(11, 189)
        VScrollBar1.TabIndex = 3
        ' 
        ' Label1
        ' 
        Label1.AutoSize = True
        Label1.Location = New Point(3, 0)
        Label1.Name = "Label1"
        Label1.Size = New Size(41, 15)
        Label1.TabIndex = 0
        Label1.Text = "Label1"
        ' 
        ' Label2
        ' 
        Label2.AutoSize = True
        Label2.Location = New Point(3, 15)
        Label2.Name = "Label2"
        Label2.Size = New Size(41, 15)
        Label2.TabIndex = 1
        Label2.Text = "Label2"
        ' 
        ' Label3
        ' 
        Label3.AutoSize = True
        Label3.Location = New Point(3, 45)
        Label3.Name = "Label3"
        Label3.Size = New Size(41, 15)
        Label3.TabIndex = 3
        Label3.Text = "Label3"
        ' 
        ' Label4
        ' 
        Label4.AutoSize = True
        Label4.Location = New Point(3, 30)
        Label4.Name = "Label4"
        Label4.Size = New Size(41, 15)
        Label4.TabIndex = 2
        Label4.Text = "Label4"
        ' 
        ' Label5
        ' 
        Label5.AutoSize = True
        Label5.Location = New Point(3, 108)
        Label5.Name = "Label5"
        Label5.Size = New Size(41, 15)
        Label5.TabIndex = 7
        Label5.Text = "Label5"
        ' 
        ' Label6
        ' 
        Label6.AutoSize = True
        Label6.Location = New Point(3, 93)
        Label6.Name = "Label6"
        Label6.Size = New Size(41, 15)
        Label6.TabIndex = 6
        Label6.Text = "Label6"
        ' 
        ' Label7
        ' 
        Label7.AutoSize = True
        Label7.Location = New Point(3, 78)
        Label7.Name = "Label7"
        Label7.Size = New Size(41, 15)
        Label7.TabIndex = 5
        Label7.Text = "Label7"
        ' 
        ' Label8
        ' 
        Label8.AutoSize = True
        Label8.Location = New Point(3, 63)
        Label8.Name = "Label8"
        Label8.Size = New Size(41, 15)
        Label8.TabIndex = 4
        Label8.Text = "Label8"
        ' 
        ' Label9
        ' 
        Label9.AutoSize = True
        Label9.Location = New Point(3, 234)
        Label9.Name = "Label9"
        Label9.Size = New Size(41, 15)
        Label9.TabIndex = 15
        Label9.Text = "Label9"
        ' 
        ' Label10
        ' 
        Label10.AutoSize = True
        Label10.Location = New Point(3, 219)
        Label10.Name = "Label10"
        Label10.Size = New Size(47, 15)
        Label10.TabIndex = 14
        Label10.Text = "Label10"
        ' 
        ' Label11
        ' 
        Label11.AutoSize = True
        Label11.Location = New Point(3, 204)
        Label11.Name = "Label11"
        Label11.Size = New Size(47, 15)
        Label11.TabIndex = 13
        Label11.Text = "Label11"
        ' 
        ' Label12
        ' 
        Label12.AutoSize = True
        Label12.Location = New Point(3, 189)
        Label12.Name = "Label12"
        Label12.Size = New Size(47, 15)
        Label12.TabIndex = 12
        Label12.Text = "Label12"
        ' 
        ' Label13
        ' 
        Label13.AutoSize = True
        Label13.Location = New Point(3, 171)
        Label13.Name = "Label13"
        Label13.Size = New Size(47, 15)
        Label13.TabIndex = 11
        Label13.Text = "Label13"
        ' 
        ' Label14
        ' 
        Label14.AutoSize = True
        Label14.Location = New Point(3, 156)
        Label14.Name = "Label14"
        Label14.Size = New Size(47, 15)
        Label14.TabIndex = 10
        Label14.Text = "Label14"
        ' 
        ' Label15
        ' 
        Label15.AutoSize = True
        Label15.Location = New Point(3, 141)
        Label15.Name = "Label15"
        Label15.Size = New Size(47, 15)
        Label15.TabIndex = 9
        Label15.Text = "Label15"
        ' 
        ' Label16
        ' 
        Label16.AutoSize = True
        Label16.Location = New Point(3, 126)
        Label16.Name = "Label16"
        Label16.Size = New Size(47, 15)
        Label16.TabIndex = 8
        Label16.Text = "Label16"
        ' 
        ' Label17
        ' 
        Label17.AutoSize = True
        Label17.Location = New Point(3, 359)
        Label17.Name = "Label17"
        Label17.Size = New Size(47, 15)
        Label17.TabIndex = 23
        Label17.Text = "Label17"
        ' 
        ' Label18
        ' 
        Label18.AutoSize = True
        Label18.Location = New Point(3, 344)
        Label18.Name = "Label18"
        Label18.Size = New Size(47, 15)
        Label18.TabIndex = 22
        Label18.Text = "Label18"
        ' 
        ' Label19
        ' 
        Label19.AutoSize = True
        Label19.Location = New Point(3, 329)
        Label19.Name = "Label19"
        Label19.Size = New Size(47, 15)
        Label19.TabIndex = 21
        Label19.Text = "Label19"
        ' 
        ' Label20
        ' 
        Label20.AutoSize = True
        Label20.Location = New Point(3, 314)
        Label20.Name = "Label20"
        Label20.Size = New Size(47, 15)
        Label20.TabIndex = 20
        Label20.Text = "Label20"
        ' 
        ' Label21
        ' 
        Label21.AutoSize = True
        Label21.Location = New Point(3, 296)
        Label21.Name = "Label21"
        Label21.Size = New Size(47, 15)
        Label21.TabIndex = 19
        Label21.Text = "Label21"
        ' 
        ' Label22
        ' 
        Label22.AutoSize = True
        Label22.Location = New Point(3, 281)
        Label22.Name = "Label22"
        Label22.Size = New Size(47, 15)
        Label22.TabIndex = 18
        Label22.Text = "Label22"
        ' 
        ' Label23
        ' 
        Label23.AutoSize = True
        Label23.Location = New Point(3, 266)
        Label23.Name = "Label23"
        Label23.Size = New Size(47, 15)
        Label23.TabIndex = 17
        Label23.Text = "Label23"
        ' 
        ' Label24
        ' 
        Label24.AutoSize = True
        Label24.Location = New Point(3, 251)
        Label24.Name = "Label24"
        Label24.Size = New Size(47, 15)
        Label24.TabIndex = 16
        Label24.Text = "Label24"
        ' 
        ' Form1
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(1406, 525)
        Controls.Add(VScrollBar1)
        Controls.Add(Panel1)
        Controls.Add(txtDebug)
        Controls.Add(Button1)
        Name = "Form1"
        Text = "Form1"
        Panel1.ResumeLayout(False)
        Panel1.PerformLayout()
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents txtDebug As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents VScrollBar1 As VScrollBar

End Class
