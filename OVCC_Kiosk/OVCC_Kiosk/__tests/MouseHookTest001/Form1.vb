﻿Imports System.ComponentModel
Imports SharpHook

Public Class Form1
    Private WithEvents mHook As TaskPoolGlobalHook

    Delegate Sub DebugToTextBoxCallback(ByVal [text] As String)

    Public Sub THREADSAFE__DebugToTextBox(ByVal [text] As String)
        If Me.txtDebug.InvokeRequired Then
            Dim d As New DebugToTextBoxCallback(AddressOf THREADSAFE__DebugToTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtDebug.AppendText(Trim([text]))
        End If
    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtDebug.Clear()

        mHook = New TaskPoolGlobalHook

        mHook.RunAsync()
    End Sub

    Private Sub Log(s As String)
        THREADSAFE__DebugToTextBox(s & vbCrLf)
    End Sub

    Private Sub mHook_HookEnabled(sender As Object, e As HookEventArgs) Handles mHook.HookEnabled
        Log("HookEnabled")
    End Sub

    Private Sub mHook_MouseMoved(sender As Object, e As MouseHookEventArgs) Handles mHook.MouseMoved
        Dim s As String = ""

        s = "x=" & e.Data.X.ToString & ";y=" & e.Data.Y.ToString
        Log(s)
    End Sub

    Private Sub Form1_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        mHook.Dispose()
    End Sub
End Class
