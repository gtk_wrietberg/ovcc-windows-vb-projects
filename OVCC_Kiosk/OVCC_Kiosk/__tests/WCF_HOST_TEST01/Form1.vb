﻿Imports System.ServiceModel

Public Class Form1
    Private hostWhistle As ServiceHost

    Private Sub AddLog(s As String)
        TextBox1.AppendText(s & vbCrLf)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Button1.Enabled = False

        Try
            AddLog("servicehost")
            hostWhistle = New ServiceHost(GetType(ServiceCommunication), New Uri() {
                                          New Uri("net.pipe://localhost")
                                          })


            AddLog("DogWhistlePipe")
            hostWhistle.AddServiceEndpoint(GetType(IServiceCommunication), New NetNamedPipeBinding(), "DogWhistlePipe")

            AddLog("hostWhistle.Open")
            hostWhistle.Open()


        Catch ex As Exception
            AddLog("ex: " & ex.Message)
        End Try

        Button2.Enabled = True
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Button2.Enabled = False

        Try

            If Not hostWhistle Is Nothing Then
                AddLog("hostWhistle.Close()")

                hostWhistle.Close()
            Else
                AddLog("hostWhistle.Close() not needed")
            End If

        Catch ex As Exception
            AddLog("ex: " & ex.Message)
        End Try

        Button1.Enabled = True
    End Sub
End Class
