﻿Imports Microsoft.Win32
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.Security.Principal
Imports System.Security
Imports Microsoft.Win32.SafeHandles
Imports System.Runtime.ConstrainedExecution
Imports System.Security.AccessControl
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports System.Linq
Imports System.Data.SqlTypes

Public Class Helpers
#Region "errors"
    Public Class Errors
        Public Class ErrorData
            Private mError_Timestamp As Date
            Private mError_Description As String
            Private mError_Caller As String

            Public Property Timestamp() As Date
                Get
                    Return mError_Timestamp
                End Get
                Set(ByVal value As Date)
                    mError_Timestamp = value
                End Set
            End Property

            Public Property Description() As String
                Get
                    Return mError_Description
                End Get
                Set(ByVal value As String)
                    mError_Description = value
                End Set
            End Property

            Public Property Caller() As String
                Get
                    Return mError_Caller
                End Get
                Set(ByVal value As String)
                    mError_Caller = value
                End Set
            End Property

            Public Overloads Function ToString() As String
                Return ToString(True, True)
            End Function

            Public Overloads Function ToString(IncludeTimestamp As Boolean) As String
                Return ToString(IncludeTimestamp, True)
            End Function

            Public Overloads Function ToString(IncludeTimestamp As Boolean, IncludeCaller As Boolean) As String
                Dim sResult As String = ""

                If IncludeTimestamp Then
                    sResult = mError_Timestamp.ToString & " - " & mError_Description
                Else
                    sResult = mError_Description
                End If

                If IncludeCaller Then
                    sResult = sResult & " - " & mError_Caller
                End If

                Return sResult
            End Function
        End Class

        Private Shared mMaxHistory As Integer = 25
        Private Shared lstErrors As New List(Of ErrorData)(mMaxHistory)

        Public Shared Sub Add(Description As String)
            Dim newError As New ErrorData

            newError.Description = Description
            newError.Timestamp = Date.Now

            Dim stackTrace As New StackTrace()

            newError.Caller = stackTrace.GetFrame(1).GetMethod().Name


            lstErrors.Insert(0, newError)

            If lstErrors.Count > mMaxHistory Then
                lstErrors.RemoveRange(mMaxHistory, lstErrors.Count - mMaxHistory)
            End If
        End Sub

        Public Shared Function GetLast() As String
            Return GetLast(True, True)
        End Function

        Public Shared Function GetLast(IncludeTimestamp As Boolean) As String
            Return GetLast(IncludeTimestamp, True)
        End Function

        Public Shared Function GetLast(IncludeTimestamp As Boolean, IncludeCaller As Boolean) As String
            If lstErrors.Count > 0 Then
                Return lstErrors.Item(0).ToString(IncludeTimestamp, IncludeCaller)
            Else
                Return ""
            End If
        End Function

        Public Overloads Shared Function ToString(Optional IncludeTimestamp As Boolean = True, Optional AllErrors As Boolean = False) As String
            Dim sResult As String = ""
            Dim currentError As New ErrorData

            If lstErrors.Count = 0 Then
                Return ""
            End If

            If lstErrors.Count = 1 Then
                AllErrors = False
            End If

            If AllErrors Then
                For i As Integer = 0 To lstErrors.Count - 2
                    currentError = lstErrors.Item(i)
                    sResult &= currentError.ToString & vbCrLf
                Next

                currentError = lstErrors.Item(lstErrors.Count - 1)
                sResult &= currentError.ToString
            Else
                currentError = lstErrors.Item(0)
                sResult = currentError.ToString
            End If

            Return sResult
        End Function
    End Class
#End Region

#Region "Logging"
    Public Class Logger
        Public Shared Function InitialiseLogger() As Boolean
            Dim sPath As String = ""

            sPath = FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_logs"

            Return _InitialiseLogger(sPath, True)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String) As Boolean
            Return _InitialiseLogger(LogFilePath, True)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String, bAddProductNameAndVersion As Boolean) As Boolean
            Return _InitialiseLogger(LogFilePath, bAddProductNameAndVersion)
        End Function

        Private Shared Function _InitialiseLogger(LogFilePath As String, bAddProductNameAndVersion As Boolean) As Boolean
            Dim dDate As Date = Now()


            mLogFilePath_ROOT = LogFilePath
            If bAddProductNameAndVersion Then
                mLogFilePath = LogFilePath & "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
                mLogFileBasePath = LogFilePath & "\" & My.Application.Info.ProductName
            Else
                mLogFilePath = LogFilePath
                mLogFileBasePath = LogFilePath
            End If
            mLogFile = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            Try
                If Not IO.Directory.Exists(mLogFilePath) Then
                    IO.Directory.CreateDirectory(mLogFilePath)
                End If
            Catch ex As Exception

            End Try

            Return IO.Directory.Exists(mLogFilePath)
        End Function

        Public Shared ReadOnly Property LogFilePath_ROOT As String
            Get
                Return mLogFilePath_ROOT
            End Get
        End Property


        Public Shared ReadOnly Property LogFileBasePath() As String
            Get
                Return mLogFileBasePath
            End Get
        End Property


        Private Shared mLogFilePath_ROOT As String
        Private Shared mLogFilePath As String
        Private Shared mLogFileBasePath As String
        Private Shared mLogFile As String = ""
        Private Shared pPrevDepth As Integer = 0
        Private Shared pDoNotStoreLogLevel As Boolean = False

        Private Shared mLogExtension As String = "log"
        Public Shared Property LogExtension() As String
            Get
                Return mLogExtension
            End Get
            Set(ByVal value As String)
                mLogExtension = value
            End Set
        End Property

        Private Shared mLogNameSuffix As String = ""
        Public Shared Property LogNameSuffix() As String
            Get
                Return mLogNameSuffix
            End Get
            Set(ByVal value As String)
                mLogNameSuffix = value
            End Set
        End Property

        Private Shared mDebugging As Boolean = True
        Public Shared Property Debugging() As Boolean
            Get
                Return mDebugging
            End Get
            Set(ByVal value As Boolean)
                mDebugging = value
            End Set
        End Property

        Private Shared mIncludeCaller As Boolean = False
        Public Shared Property IncludeCaller() As Boolean
            Get
                Return mIncludeCaller
            End Get
            Set(ByVal value As Boolean)
                mIncludeCaller = value
            End Set
        End Property

        Private Shared mExtraPrefix As String
        Public Shared Property ExtraPrefix() As String
            Get
                Return mExtraPrefix
            End Get
            Set(ByVal value As String)
                mExtraPrefix = value
            End Set
        End Property

        Public Enum MESSAGE_TYPE
            LOG_DEFAULT = 0
            LOG_WARNING = 1
            LOG_ERROR = 2
            LOG_DEBUG = 6
        End Enum

        Public Shared Property LogFilePath() As String
            Get
                Return mLogFilePath
            End Get
            Set(ByVal value As String)
                If value.EndsWith("\") Then
                    mLogFilePath = value
                Else
                    mLogFilePath = value & "\"
                End If
            End Set
        End Property

        Public Shared Property LogFileName() As String
            Get
                Return mLogFile
            End Get
            Set(ByVal value As String)
                mLogFile = value
            End Set
        End Property

        Public Shared Function WriteMessage(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteMessageRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteWarning(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteWarningRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteError(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteErrorRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteDebug(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteDebugRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return WriteRelative(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function WriteRelative(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iRelativeDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            pDoNotStoreLogLevel = True
            Return Write(sMessage, cMessageType, pPrevDepth + iRelativeDepth)
        End Function

        Public Shared Function WriteRelative(ByVal sMessagePrefix As String, ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iRelativeDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            pDoNotStoreLogLevel = True
            Return Write(sMessagePrefix, sMessage, cMessageType, pPrevDepth + iRelativeDepth)
        End Function

        Public Shared Function Write(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Return Write("", sMessage, cMessageType, iDepth, bDoNotWriteToFile)
        End Function

        Public Shared Function Write(ByVal sMessagePrefix As String, ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
            Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String, sMsgCallerSuffix As String, iDepthStep As Integer

            If iDepth < 0 Then
                iDepth = 0
            End If

            If Not mDebugging And cMessageType = MESSAGE_TYPE.LOG_DEBUG Then
                bDoNotWriteToFile = True
            End If

            sMessage = Trim(sMessage)

            Select Case cMessageType
                Case MESSAGE_TYPE.LOG_WARNING
                    sMsgTypePrefix = "[*] "
                Case MESSAGE_TYPE.LOG_ERROR
                    sMsgTypePrefix = "[!] "
                Case MESSAGE_TYPE.LOG_DEBUG
                    sMsgTypePrefix = "[#] "
                Case MESSAGE_TYPE.LOG_DEFAULT
                    sMsgTypePrefix = "[.] "
                Case Else
                    sMsgTypePrefix = "[?] "
            End Select


            If mIncludeCaller Then
                Try
                    Dim stackTrace As New StackTrace()

                    sMsgCallerSuffix = " ; (" & stackTrace.GetFrame(1).GetMethod().Name & ")"
                Catch ex As Exception
                    sMsgCallerSuffix = " ; (error getting caller: " & ex.Message & ")"
                End Try
            Else
                sMsgCallerSuffix = ""
            End If


            sMsgDatePrefix = Now.ToString & " - "

            sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

            If Not sMessagePrefix.Equals("") Then
                sMsgPrefix = sMsgPrefix & "[" & sMessagePrefix & "] - "
            End If

            'If iDepth < pPrevDepth Then
            '    For iDepthStep = 1 To iDepth
            '        sMsgPrefix = sMsgPrefix & "| "
            '    Next

            '    sMsgPrefix = sMsgPrefix & vbCrLf & sMsgPrefix
            'End If

            'If iDepth > 0 Then
            '    For iDepthStep = 1 To iDepth - 1
            '        sMsgPrefix = sMsgPrefix & "| "
            '    Next

            '    sMsgPrefix = sMsgPrefix & "|-"
            'End If

            If iDepth > 0 Then
                For iDepthStep = 1 To iDepth - 1
                    sMsgPrefix = sMsgPrefix & "| "
                Next

                sMsgPrefix = sMsgPrefix & "|-"
            End If

            If Not bDoNotWriteToFile Then
                UpdateLogfile(sMsgPrefix & sMessage & sMsgCallerSuffix)
            End If

            If Not pDoNotStoreLogLevel Then
                pPrevDepth = iDepth
            End If

            pDoNotStoreLogLevel = False

            Return sMsgPrefix & sMessage & sMsgCallerSuffix
        End Function

        Public Shared Sub WriteWithoutDate(ByVal sMessage As String)
            UpdateLogfile(sMessage)
        End Sub

        Public Shared Sub WriteEmptyLine()
            WriteWithoutDate(" ")
        End Sub

        Private Shared Sub UpdateLogfile(ByVal sString As String)
            Try
                Dim sFile As String = mLogFilePath & "\" & mLogFile & "." & mLogNameSuffix & "." & mLogExtension

                sFile = sFile.Replace("..", ".")

                Using sw As New IO.StreamWriter(sFile, True)
                    sw.WriteLine(sString)
                End Using

                mLogNameSuffix = ""
            Catch ex As Exception

            End Try
        End Sub
    End Class

    Public Class Logging
        Public Shared Function InitialiseLogger() As Boolean
            Dim sPath As String = ""

            sPath = FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_logs"

            Return _InitialiseLogger(sPath, True)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String) As Boolean
            Return _InitialiseLogger(LogFilePath, True)
        End Function

        Public Shared Function InitialiseLogger(LogFilePath As String, bAddProductNameAndVersion As Boolean) As Boolean
            Return _InitialiseLogger(LogFilePath, bAddProductNameAndVersion)
        End Function

        Private Shared Function _InitialiseLogger(LogFilePath As String, bAddProductNameAndVersion As Boolean) As Boolean
            Dim dDate As Date = Now()

            If bAddProductNameAndVersion Then
                mLogFilePath = LogFilePath & "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
            Else
                mLogFilePath = LogFilePath
            End If
            mLogFile = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            Try
                If Not IO.Directory.Exists(mLogFilePath) Then
                    IO.Directory.CreateDirectory(mLogFilePath)
                End If
            Catch ex As Exception

            End Try

            Return IO.Directory.Exists(mLogFilePath)
        End Function


        Private Shared mLogFilePath As String
        Private Shared mLogFile As String = ""

        Private Shared mLogExtension As String = "log"
        Public Shared Property LogExtension() As String
            Get
                Return mLogExtension
            End Get
            Set(ByVal value As String)
                mLogExtension = value
            End Set
        End Property

        Private Shared mLogNameSuffix As String = ""
        Public Shared Property LogNameSuffix() As String
            Get
                Return mLogNameSuffix
            End Get
            Set(ByVal value As String)
                mLogNameSuffix = value
            End Set
        End Property

        Public Enum MESSAGE_TYPE
            LOG_DEFAULT = 0
            LOG_WARNING = 1
            LOG_ERROR = 2
            LOG_DEBUG = 6
        End Enum

        Public Shared Property LogFilePath() As String
            Get
                Return mLogFilePath
            End Get
            Set(ByVal value As String)
                If value.EndsWith("\") Then
                    mLogFilePath = value
                Else
                    mLogFilePath = value & "\"
                End If
            End Set
        End Property

        Public Shared Property LogFileName() As String
            Get
                Return mLogFile
            End Get
            Set(ByVal value As String)
                mLogFile = value
            End Set
        End Property

        Public Shared Function WriteMessage(ByVal sMessage As String) As String
            Return Write("", sMessage, MESSAGE_TYPE.LOG_DEFAULT)
        End Function

        Public Shared Function WriteWarning(ByVal sMessage As String) As String
            Return Write("", sMessage, MESSAGE_TYPE.LOG_WARNING)
        End Function

        Public Shared Function WriteError(ByVal sMessage As String) As String
            Return Write("", sMessage, MESSAGE_TYPE.LOG_ERROR)
        End Function

        Public Shared Function WriteDebug(ByVal sMessage As String) As String
            Return Write("", sMessage, MESSAGE_TYPE.LOG_DEBUG)
        End Function

        Public Shared Function WriteMessage(ByVal sClient As String, ByVal sMessage As String) As String
            Return Write(sClient, sMessage, MESSAGE_TYPE.LOG_DEFAULT)
        End Function

        Public Shared Function WriteWarning(ByVal sClient As String, ByVal sMessage As String) As String
            Return Write(sClient, sMessage, MESSAGE_TYPE.LOG_WARNING)
        End Function

        Public Shared Function WriteError(ByVal sClient As String, ByVal sMessage As String) As String
            Return Write(sClient, sMessage, MESSAGE_TYPE.LOG_ERROR)
        End Function

        Public Shared Function WriteDebug(ByVal sClient As String, ByVal sMessage As String) As String
            Return Write(sClient, sMessage, MESSAGE_TYPE.LOG_DEBUG)
        End Function



        Public Shared Function Write(ByVal sClient As String, ByVal sMessage As String, ByVal cMessageType As MESSAGE_TYPE) As String
            Dim sMsgTypePrefix As String = "", sFinal As String = ""


            sMessage = Trim(sMessage)

            Select Case cMessageType
                Case MESSAGE_TYPE.LOG_WARNING
                    sMsgTypePrefix = "[*] "
                Case MESSAGE_TYPE.LOG_ERROR
                    sMsgTypePrefix = "[!] "
                Case MESSAGE_TYPE.LOG_DEBUG
                    sMsgTypePrefix = "[#] "
                Case MESSAGE_TYPE.LOG_DEFAULT
                    sMsgTypePrefix = "[.] "
                Case Else
                    sMsgTypePrefix = "[?] "
            End Select


            sFinal &= sMsgTypePrefix
            sFinal &= " "
            sFinal &= Now.ToString
            sFinal &= " "

            If Not sClient.Equals("") Then
                sFinal &= "[" & sClient & "]"
                sFinal &= " "
            End If

            sFinal &= sMessage


            UpdateLogfile(sFinal)

            Return sFinal
        End Function

        Public Shared Sub WriteWithoutDate(ByVal sMessage As String)
            UpdateLogfile(sMessage)
        End Sub

        Public Shared Sub WriteEmptyLine()
            WriteWithoutDate(" ")
        End Sub

        Private Shared Sub UpdateLogfile(ByVal sString As String)
            Try
                Dim sFile As String = mLogFilePath & "\" & mLogFile & "." & mLogNameSuffix & "." & mLogExtension

                sFile = sFile.Replace("..", ".")

                Dim sw As New IO.StreamWriter(sFile, True)
                sw.WriteLine(sString)
                sw.Close()

                mLogNameSuffix = ""
            Catch ex As Exception
                Using sw As New IO.StreamWriter("C:\Temp\err.txt", True)
                    sw.WriteLine("-------------------------------------")
                    sw.WriteLine("error while writing log")
                    sw.WriteLine("sString:")
                    sw.WriteLine(sString)
                    sw.WriteLine("Error:")
                    sw.WriteLine(ex.Message)
                End Using
            End Try
        End Sub
    End Class

#End Region

#Region "Forms"
    Public Class Forms
        <DllImport("user32.dll", SetLastError:=True)>
        Private Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As Integer) As Boolean
        End Function

        <DllImport("user32", CharSet:=CharSet.Auto, SetLastError:=True)>
        Private Shared Function SendMessage(ByVal hWnd As IntPtr, ByVal Msg As UInt32, ByVal wParam As Integer, ByVal lParam As IntPtr) As Integer
        End Function

        Private Const BCM_SETSHIELD As UInt32 = &H160C

        Public Shared Sub AddUACShield(handle As IntPtr)
            SendMessage(handle, BCM_SETSHIELD, 0, New IntPtr(1))
        End Sub


        Public Class OnTop
            Private Const SWP_NOSIZE As Integer = &H1
            Private Const SWP_NOMOVE As Integer = &H2

            Private Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
            Private Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)

            Public Shared Sub Put(FormHandle As System.IntPtr)
                SetWindowPos(FormHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End Sub

            Public Shared Sub Remove(FormHandle As System.IntPtr)
                SetWindowPos(FormHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End Sub
        End Class

        Private Const SWP_NOSIZE As Integer = &H1
        Private Const SWP_NOMOVE As Integer = &H2

        Private Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
        Private Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)

        Public Shared Sub TopMost(FormHandle As System.IntPtr, bTopMost As Boolean)
            If bTopMost Then
                SetWindowPos(FormHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            Else
                SetWindowPos(FormHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End If
        End Sub

    End Class
#End Region

#Region "PuppyPower"
    Public Class PuppyPower
        Private Shared ReadOnly KEY__Puppypower As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\PuppyPower"
        Private Shared ReadOnly KEY_OLD__Puppypower As String = "HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\Watchdog\PuppyPower"
        Private Shared ReadOnly VALUE__PuppyPowerUserName As String = "username"
        Private Shared ReadOnly VALUE__PuppyPowerPassWord As String = "password"

        Public Shared ReadOnly Property Username As String
            Get
                Dim sTmp As String = "", sVoid As String = ""

                If Registry.KeyExists(KEY__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerUserName, "")
                ElseIf Registry.KeyExists(KEY_OLD__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerUserName, "")
                Else
                    sTmp = ""
                End If

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
        End Property

        Public Shared ReadOnly Property Password As String
            Get
                Dim sTmp As String = "", sVoid As String = ""

                If Registry.KeyExists(KEY__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerPassWord, "")
                ElseIf Registry.KeyExists(KEY_OLD__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerPassWord, "")
                Else
                    sTmp = ""
                End If

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
        End Property
    End Class

    Public Class PuppyPower_v2
        Private Shared ReadOnly KEY__Puppypower As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\PuppyPower"
        Private Shared ReadOnly VALUE__PuppyPowerUserName As String = "username"
        Private Shared ReadOnly VALUE__PuppyPowerPassWord As String = "password"

        Public Shared Property Username As String
            Get
                Dim sTmp As String = "", sVoid As String = ""

                If Registry.KeyExists(KEY__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerUserName, "")
                Else
                    sTmp = ""
                End If

                Return XOrObfuscation_v2.Deobfuscate(sTmp)
            End Get
            Set(value As String)
                Registry.SetValue_String(KEY__Puppypower, VALUE__PuppyPowerUserName, XOrObfuscation_v2.Obfuscate(value))
            End Set
        End Property

        Public Shared Property Password As String
            Get
                Dim sTmp As String = "", sVoid As String = ""

                If Registry.KeyExists(KEY__Puppypower, sVoid) Then
                    sTmp = Registry.GetValue_String(KEY__Puppypower, VALUE__PuppyPowerPassWord, "")
                Else
                    sTmp = ""
                End If

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
            Set(value As String)
                Registry.SetValue_String(KEY__Puppypower, VALUE__PuppyPowerPassWord, XOrObfuscation_v2.Obfuscate(value))
            End Set
        End Property
    End Class

#End Region

#Region "XOrObfuscation"
    Public Class XOrObfuscation
        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "$#(98)&_Ndf9m0987e5WRW#%rtefgm03947"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Private Shared mLastError As String
        Public Shared ReadOnly Property LastError() As String
            Get
                Return mLastError
            End Get
        End Property

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            mLastError = ""

            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            Try
                If PlainText.Length > 1000 Then
                    Throw New Exception("input string too long (max 1000)")
                End If

                InitializePasshrases()


                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    If AscW(Mid(PlainText, i + 1, 1)) > 255 Then
                        'uh oh, Unicode
                        Throw New Exception("Only ASCII is supported!")
                    End If

                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(AscW(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(AscW(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""

                mLastError = ex.Message
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String


            Try
                InitializePasshrases()

                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & ChrW(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & ChrW(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                PlainText_RandomPassphrase = ""

                mLastError = ex.Message
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = AscW(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            temp = _HexToNo(Mid(BinVal, 2, 1))
            iVal = iVal + temp
            temp = _HexToNo(Mid(BinVal, 1, 1))
            iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class

    Public Class XOrObfuscation_v2
        Private Shared ReadOnly c_MAXLENGTH As Integer = 2000
        Private Shared ReadOnly c_PADDINGLENGTH As Integer = 32

        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "ajsgbdJOASGd_()#hfibsbjbn34#$jilhf"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Private Shared mLastError As String
        Public Shared ReadOnly Property LastError() As String
            Get
                Return mLastError
            End Get
        End Property

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            mLastError = ""

            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            Try
                If PlainText.Length > c_MAXLENGTH Then
                    Throw New Exception("input string too long (max " & c_MAXLENGTH.ToString & ")")
                End If

                InitializePasshrases()


                'add length to string
                PlainText = _LeadingZero(_D2H(PlainText.Length), 3) & PlainText

                'let the padding begin
                Dim paddinglength As Integer = c_PADDINGLENGTH - (PlainText.Length Mod c_PADDINGLENGTH)
                PlainText &= Types.RandomString(paddinglength)


                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    If AscW(Mid(PlainText, i + 1, 1)) > 255 Then
                        'uh oh, Unicode
                        Throw New Exception("Only ASCII is supported!")
                    End If

                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(AscW(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(AscW(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""

                mLastError = ex.Message
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String


            Try
                InitializePasshrases()

                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & ChrW(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & ChrW(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                'we need to remove the padding, first 3 chars are length in hex
                Dim firstthreechars As String = PlainText_RandomPassphrase.Substring(0, 3)
                Dim strlength As Integer = _H2D(firstthreechars)

                PlainText_RandomPassphrase = PlainText_RandomPassphrase.Substring(3, strlength)
            Catch ex As Exception
                PlainText_RandomPassphrase = ""

                mLastError = ex.Message
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = AscW(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            For i As Integer = BinVal.Length To 1 Step -1
                temp = _HexToNo(Mid(BinVal, i, 1))
                iVal = iVal + (temp * (16 ^ (BinVal.Length - i)))
            Next

            'temp = _HexToNo(Mid(BinVal, 2, 1))
            'iVal = iVal + temp
            'temp = _HexToNo(Mid(BinVal, 1, 1))
            'iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _LeadingZero(sNum As String, Optional len As Integer = 3) As String
            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class
#End Region

#Region "impersonate"
    Public Class Impersonate
        Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String], ByVal lpszDomain As [String], ByVal lpszPassword As [String], ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, <Out()> ByRef phToken As SafeTokenHandle) As Boolean
        Private Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean

        Public Shared impersonatedUser As WindowsImpersonationContext

        Private Shared mActive As Boolean
        Public Shared ReadOnly Property Active() As Boolean
            Get
                Return mActive
            End Get
        End Property

        Private Shared mUserName As String
        Public Shared Property UserName() As String
            Get
                Return mUserName
            End Get
            Set(ByVal value As String)
                mUserName = value
            End Set
        End Property

        Private Shared mPassWord As String
        Public Shared Property PassWord() As String
            Get
                Return mPassWord
            End Get
            Set(ByVal value As String)
                mPassWord = value
            End Set
        End Property

        Public Shared ReadOnly Property CurrentUser() As String
            Get
                Return WindowsIdentity.GetCurrent().Name
            End Get
        End Property

        Public Shared Function StartImpersonation(Username As String, Password As String) As Boolean
            mUserName = Username
            mPassWord = Password

            Return StartImpersonation()
        End Function

        Public Shared Function StartImpersonation() As Boolean
            Dim safeTokenHandle As SafeTokenHandle
            Dim tokenHandle As New IntPtr(0)
            Dim bRet As Boolean = False

            Try
                Dim domainName As String

                domainName = Environment.UserDomainName

                Const LOGON32_PROVIDER_DEFAULT As Integer = 0
                Const LOGON32_LOGON_INTERACTIVE As Integer = 2

#Disable Warning BC42030 ' Variable is passed by reference before it has been assigned a value
                Dim returnValue As Boolean = LogonUser(mUserName, domainName, mPassWord, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, safeTokenHandle)
#Enable Warning BC42030 ' Variable is passed by reference before it has been assigned a value

                If False = returnValue Then
                    Dim ret As Integer = Marshal.GetLastWin32Error()

                    Throw New System.ComponentModel.Win32Exception(ret)

                    Return False
                End If

                ' Use the token handle returned by LogonUser.
                impersonatedUser = WindowsIdentity.Impersonate(safeTokenHandle.DangerousGetHandle())

                bRet = True
                mActive = True
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return bRet
        End Function

        Public Shared Function StopImpersonation() As Boolean
            Dim bRet As Boolean = False

            Try
                impersonatedUser.Dispose()

                bRet = True
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return bRet
        End Function

        Private NotInheritable Class SafeTokenHandle
            Inherits SafeHandleZeroOrMinusOneIsInvalid

            Private Sub New()
                MyBase.New(True)

            End Sub 'NewNew

            Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String],
                    ByVal lpszDomain As [String], ByVal lpszPassword As [String],
                    ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer,
                    ByRef phToken As IntPtr) As Boolean
            <DllImport("kernel32.dll"), ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success), SuppressUnmanagedCodeSecurity()>
            Private Shared Function CloseHandle(ByVal handle As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean

            End Function
            Protected Overrides Function ReleaseHandle() As Boolean
                Return CloseHandle(handle)

            End Function 'ReleaseHandle
        End Class 'SafeTokenHandle
    End Class
#End Region

#Region "misc helpers"
    Public Class Generic
        Public Shared Function IsDevMachine() As Boolean
            Dim sComputerName As String = Environment.MachineName.ToLower
            Dim bRet As Boolean = False

            If InStr(sComputerName, "rietberg") > 0 _
            Or InStr(sComputerName, "superlekkerding") > 0 _
            Or InStr(sComputerName, "dragon-bafstaaf") > 0 _
            Or InStr(sComputerName, "wrdev") > 0 Then
                bRet = True
            End If

            Return bRet
        End Function

        Public Shared Function IsRunningAsAdmin() As Boolean
            Return My.User.IsInRole(ApplicationServices.BuiltInRole.Administrator)
        End Function

        Public Shared Function IsMSOfficeInstalled() As Boolean
            Dim typOffice As Type = Type.GetTypeFromProgID("Excel.Application")

            If Not typOffice Is Nothing Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function IsFirefoxInstalled() As Boolean
            Dim sError As String = ""
            Dim bRet_32 As Boolean = False
            Dim bRet_64 As Boolean = False

            'bRet = Registry.FindKeyByString("HKEY_LOCAL_MACHINE\SOFTWARE\Clients\StartMenuInternet", "firefox", sError)
            'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{F99F24BF-0B90-463E-9658-3FD2EFC3C992}
            bRet_32 = Registry.FindKeyByString("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", "firefox", "DisplayName", sError)
            bRet_64 = Registry64.FindKeyByString("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", "firefox", "DisplayName", sError)

            If Not sError.Equals("") Then
                Errors.Add(sError)
            End If

            Return bRet_32 Or bRet_64
        End Function

        Public Shared Function KillAllFirefox() As Integer
            Dim _ps() As Process = Process.GetProcesses
            Dim _pc As Integer = 0

            Try
                For Each _p As Process In _ps
                    If _p.ProcessName.ToLower.Equals("firefox") Then
                        Try
                            _p.Kill()
                            _pc += 1
                        Catch ex As Exception

                        End Try
                    End If
                Next
            Catch ex As Exception

            End Try


            Return _pc
        End Function

        Public Shared Function GetCurrentUser() As String
            Return Environment.UserName
        End Function

        Public Shared Function GetHostnameFromUrl(ByVal sUrl As String) As String
            Dim u As New Uri(sUrl)

            Return u.Host
        End Function

        Public Shared Function GetResponseHostname(sUrl As String) As String
            Dim resUri As String = ""

            Try
                Dim req As Net.HttpWebRequest = DirectCast(Net.HttpWebRequest.Create(sUrl), Net.HttpWebRequest)
                Dim response As Net.HttpWebResponse

                response = req.GetResponse
                resUri = response.ResponseUri.Host
            Catch ex As Exception
                'Yeah, wtf
            End Try

            Return resUri
        End Function

        Public Shared Function HtmlEncode(str As String) As String
            Return HttpUtility.HtmlEncode(str)
        End Function

        Public Shared Function HtmlDecode(str As String) As String
            Return HttpUtility.HtmlDecode(str)
        End Function

        Public Shared Function JavaScriptStringEncode(str As String) As String
            Return HttpUtility.JavaScriptStringEncode(str)
        End Function

        Public Shared Function JavaScriptStringDecode(str As String) As String
            ' Replace some chars.
            Dim decoded = str.Replace("\'", "'").Replace("\""", """").Replace("\/", "/").Replace("\\", "\").Replace("\t", vbTab).Replace("\r\n", vbCrLf).Replace("\r", vbCr).Replace("\n", vbLf)

            ' Replace unicode escaped text.
            Dim rx = New Regex("\\[uU]([0-9A-F]{4})")
            Dim evaluator As MatchEvaluator = AddressOf ASCII_to_CHAR

            decoded = rx.Replace(decoded, evaluator)

            Return decoded
        End Function

        Private Shared Function ASCII_to_CHAR(match As Match) As String
            Return ChrW(Int32.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString(CultureInfo.InvariantCulture)
        End Function

        Public Shared Function NumbersOnly(str As String) As String
            Dim nonNumericCharacters As New Regex("[^0-9]")

            Return nonNumericCharacters.Replace(str, String.Empty)
        End Function

        Public Shared Function RandomInteger(lowerbound As Integer, upperbound As Integer, upperbound_inclusive As Boolean)
            Static Generator As System.Random = New System.Random()

            If upperbound < lowerbound Then
                Dim tmp As Integer = upperbound
                upperbound = lowerbound
                lowerbound = tmp
            End If

            If upperbound_inclusive Then
                Return Generator.Next(lowerbound, upperbound + 1)
            Else
                Return Generator.Next(lowerbound, upperbound)
            End If
        End Function
    End Class
#End Region

#Region "network"
    Public Class Network
        Public Class NetStat
            Private Const AF_INET As Integer = 2

            <StructLayout(LayoutKind.Sequential)>
            Private Structure MIB_TCPTABLE_OWNER_PID
                Public dwNumEntries As UInteger
                Private table As MIB_TCPROW_OWNER_PID
            End Structure

            Private Enum TCP_TABLE_CLASS
                TCP_TABLE_BASIC_LISTENER
                TCP_TABLE_BASIC_CONNECTIONS
                TCP_TABLE_BASIC_ALL
                TCP_TABLE_OWNER_PID_LISTENER
                TCP_TABLE_OWNER_PID_CONNECTIONS
                TCP_TABLE_OWNER_PID_ALL
                TCP_TABLE_OWNER_MODULE_LISTENER
                TCP_TABLE_OWNER_MODULE_CONNECTIONS
                TCP_TABLE_OWNER_MODULE_ALL
            End Enum

            <DllImport("iphlpapi.dll", SetLastError:=True)>
            Private Shared Function GetExtendedTcpTable(ByVal tcpTable As IntPtr, ByRef tcpTableLength As Integer, ByVal sort As Boolean, ByVal ipVersion As Integer, ByVal tcpTableType As TCP_TABLE_CLASS, ByVal reserved As Integer) As UInteger
            End Function

            <StructLayout(LayoutKind.Sequential)>
            Public Structure MIB_TCPROW_OWNER_PID
                ' DWORD is System.UInt32 in C#
                <MarshalAs(UnmanagedType.U4)> Public state As UInt32
                <MarshalAs(UnmanagedType.U4)> Public localAddr As UInt32
                <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> Public m_localPort As Byte()
                <MarshalAs(UnmanagedType.U4)> Public remoteAddr As UInt32
                <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> Public m_remotePort As Byte()
                <MarshalAs(UnmanagedType.U4)> Public owningPid As UInt32

                Public ReadOnly Property LocalAddress() As IPAddress
                    Get
                        Return New IPAddress(localAddr)
                    End Get
                End Property

                Public ReadOnly Property LocalPort() As UShort
                    Get
                        Return BitConverter.ToUInt16(New Byte(1) {m_localPort(1), m_localPort(0)}, 0)
                    End Get
                End Property

                Public ReadOnly Property RemoteAddress() As IPAddress
                    Get
                        Return New IPAddress(remoteAddr)
                    End Get
                End Property

                Public ReadOnly Property RemotePort() As UShort
                    Get
                        Return BitConverter.ToUInt16(New Byte(1) {m_remotePort(1), m_remotePort(0)}, 0)
                    End Get
                End Property
            End Structure

            Public Class TcpRow
                Private ReadOnly _localEndPoint As IPEndPoint
                Private ReadOnly _remoteEndPoint As IPEndPoint
                Private ReadOnly _state As TcpState
                Private ReadOnly _process As Process

                Public Sub New(ByVal tcpRow As MIB_TCPROW_OWNER_PID)
                    _state = tcpRow.state
                    _process = Process.GetProcessById(tcpRow.owningPid)

                    Dim localPort As Integer = tcpRow.LocalPort
                    Dim localAddress As IPAddress = tcpRow.LocalAddress
                    _localEndPoint = New IPEndPoint(localAddress, localPort)

                    Dim remotePort As Integer = tcpRow.RemotePort
                    Dim remoteAddress As IPAddress = tcpRow.RemoteAddress
                    _remoteEndPoint = New IPEndPoint(remoteAddress, remotePort)
                End Sub

                Public ReadOnly Property LocalEndPoint() As IPEndPoint
                    Get
                        Return _localEndPoint
                    End Get
                End Property

                Public ReadOnly Property RemoteEndPoint() As IPEndPoint
                    Get
                        Return _remoteEndPoint
                    End Get
                End Property

                Public ReadOnly Property State() As TcpState
                    Get
                        Return _state
                    End Get
                End Property

                Public ReadOnly Property ProcessId() As Integer
                    Get
                        Return _process.Id
                    End Get
                End Property

                Public ReadOnly Property ProcessName() As String
                    Get
                        Return _process.ProcessName
                    End Get
                End Property

                Public Overrides Function ToString() As String
                    Return String.Format("TCP    {0,-23}{1, -23}{2,-14}{3,-6}{4}", LocalEndPoint, RemoteEndPoint, State, ProcessId, ProcessName)
                End Function
            End Class

            Public Shared Function GetTcpTable(Optional ByVal sorted As Boolean = False) As List(Of TcpRow)
                Dim tcpRows As New List(Of TcpRow)

                Dim tcpTable As IntPtr = IntPtr.Zero
                Dim tcpTableLength As Integer = 0
                Dim retGetExtendedTcpTable As UInteger

                retGetExtendedTcpTable = GetExtendedTcpTable(tcpTable, tcpTableLength, sorted, AF_INET, TCP_TABLE_CLASS.TCP_TABLE_OWNER_PID_ALL, 0)
                If retGetExtendedTcpTable <> 0 Then
                    Try
                        tcpTable = Marshal.AllocHGlobal(tcpTableLength)
                        If GetExtendedTcpTable(tcpTable, tcpTableLength, True, AF_INET, TCP_TABLE_CLASS.TCP_TABLE_OWNER_PID_ALL, 0) = 0 Then
                            Dim table As MIB_TCPTABLE_OWNER_PID = CType(Marshal.PtrToStructure(tcpTable, GetType(MIB_TCPTABLE_OWNER_PID)), MIB_TCPTABLE_OWNER_PID)

                            Dim rowPtr As IntPtr = New IntPtr(tcpTable.ToInt64() + Marshal.SizeOf(table.dwNumEntries))
                            For i As Integer = 0 To table.dwNumEntries - 1
                                tcpRows.Add(New TcpRow(CType(Marshal.PtrToStructure(rowPtr, GetType(MIB_TCPROW_OWNER_PID)), MIB_TCPROW_OWNER_PID)))
                                rowPtr = New IntPtr(rowPtr.ToInt64() + Marshal.SizeOf(GetType(MIB_TCPROW_OWNER_PID)))
                            Next
                        End If
                    Finally
                        If tcpTable <> IntPtr.Zero Then
                            Marshal.FreeHGlobal(tcpTable)
                        End If
                    End Try
                Else
                    Console.WriteLine("GetExtendedTcpTable returned " & retGetExtendedTcpTable.ToString)
                End If

                Return tcpRows
            End Function
        End Class

        Public Class IP
            Public Shared Function GetLocalIPs_List() As List(Of String)
                Dim strIP As String
                Dim lIp As List(Of String)
                Dim regexIP As System.Text.RegularExpressions.Regex

                regexIP = New System.Text.RegularExpressions.Regex("^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$")

                lIp = New List(Of String)

                For Each ip As Net.IPAddress In Net.Dns.GetHostEntry(Net.Dns.GetHostName).AddressList
                    strIP = regexIP.Match(ip.ToString).Value

                    If Not strIP.Equals("") Then
                        lIp.Add(strIP)
                    End If
                Next

                Return lIp
            End Function

            Public Shared Function GetLocalIPs_CommaDelimited() As String
                Return String.Join(",", GetLocalIPs_List())
            End Function
        End Class

        Public Class DomainWorkgroup
            Private Enum JoinStatus
                Unknown = 0
                UnJoined = 1
                Workgroup = 2
                Domain = 3
            End Enum

            <DllImport("netapi32.dll", CharSet:=CharSet.Unicode, SetLastError:=True)>
            Private Shared Function NetGetJoinInformation(
            ByVal computerName As String,
            ByRef buffer As IntPtr,
            ByRef status As JoinStatus) As Integer
            End Function

            <DllImport("netapi32.dll", SetLastError:=True)>
            Private Shared Function NetApiBufferFree(ByVal buffer As IntPtr) As Integer
            End Function

            Public Shared Function GetName() As String
                Dim pBuffer As IntPtr = IntPtr.Zero
                Dim mStatus As JoinStatus, mDomainName As String = ""

                Try
                    Dim result As Integer = NetGetJoinInformation(Environment.MachineName, pBuffer, mStatus)
                    If 0 <> result Then Throw New Exception("error")

                    mDomainName = Marshal.PtrToStringUni(pBuffer)
                Catch ex As Exception
                    mDomainName = "error: " & ex.Message
                Finally
                    If Not IntPtr.Zero.Equals(pBuffer) Then
                        NetApiBufferFree(pBuffer)
                    End If
                End Try

                Return mDomainName
            End Function
        End Class

        Public Class LogMeIn
            Public Shared Function GetConnectionCount() As Integer
                Dim iCount As Integer = 0

                For Each tcpr As NetStat.TcpRow In NetStat.GetTcpTable()
                    If tcpr.ProcessName.Equals("LogMeIn") And tcpr.State = TcpState.Established And Not tcpr.RemoteEndPoint.Address.ToString.Equals("127.0.0.1") Then
                        iCount += 1
                    End If
                Next

                Return iCount
            End Function
        End Class

        Public Class TakeControl
            Public Shared Function GetConnectionCount() As Integer
                Dim iCount As Integer = 0

                For Each tcpr As NetStat.TcpRow In NetStat.GetTcpTable()
                    If tcpr.ProcessName.Equals("BASupSrvc") And tcpr.State = TcpState.Established And Not tcpr.RemoteEndPoint.Address.ToString.Equals("127.0.0.1") Then
                        iCount += 1
                    End If
                Next

                Return iCount
            End Function
        End Class
    End Class
#End Region

#Region "OS version"
    Public Class OSVersion
#Region "OSVERSIONINFOEX"
        <StructLayout(LayoutKind.Sequential)>
        Private Structure OSVERSIONINFOEX
            Public dwOSVersionInfoSize As Integer
            Public dwMajorVersion As Integer
            Public dwMinorVersion As Integer
            Public dwBuildNumber As Integer
            Public dwPlatformId As Integer
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=128)>
            Public szCSDVersion As String
            Public wServicePackMajor As Short
            Public wServicePackMinor As Short
            Public wSuiteMask As Short
            Public wProductType As Byte
            Public wReserved As Byte
        End Structure
#End Region

#Region "64 BIT OS DETECTION"
        Private Delegate Function IsWow64ProcessDelegate(<[In]> handle As IntPtr, <Out> ByRef isWow64Process As Boolean) As Boolean

        <DllImport("kernel32", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)>
        Public Shared Function LoadLibrary(libraryName As String) As IntPtr
        End Function

        <DllImport("kernel32", SetLastError:=True, CallingConvention:=CallingConvention.Winapi)>
        Public Shared Function GetProcAddress(hwnd As IntPtr, procedureName As String) As IntPtr
        End Function

        Private Shared Function GetIsWow64ProcessDelegate() As IsWow64ProcessDelegate
            Dim handle As IntPtr = LoadLibrary("kernel32")

            If handle <> IntPtr.Zero Then
                Dim fnPtr As IntPtr = GetProcAddress(handle, "IsWow64Process")

                If fnPtr <> IntPtr.Zero Then
#Disable Warning BC42349 ' Using DirectCast operator to cast a value-type to the same type is obsolete
                    Return DirectCast(Marshal.GetDelegateForFunctionPointer(DirectCast(fnPtr, IntPtr), GetType(IsWow64ProcessDelegate)), IsWow64ProcessDelegate)
#Enable Warning BC42349 ' Using DirectCast operator to cast a value-type to the same type is obsolete
                End If
            End If

            Return Nothing
        End Function

        Private Shared Function Is32BitProcessOn64BitProcessor() As Boolean
            Dim fnDelegate As IsWow64ProcessDelegate = GetIsWow64ProcessDelegate()

            If fnDelegate Is Nothing Then
                Return False
            End If

            Dim isWow64 As Boolean
            Dim retVal As Boolean = fnDelegate.Invoke(Process.GetCurrentProcess().Handle, isWow64)

            If retVal = False Then
                Return False
            End If

            Return isWow64
        End Function
#End Region

        Public Shared Function OSFull() As String
            Return OSName() & " (" & OSBits() & ")"
        End Function

        Public Shared Function OSBits() As String
            Dim sBits As String = "x??"

            Select Case IntPtr.Size * 8
                Case 64
                    sBits = "x64"
                Case 32
                    If Is32BitProcessOn64BitProcessor() Then
                        sBits = "x64"
                    Else
                        sBits = "x32"
                    End If
                Case Else
                    sBits = "x??"
            End Select

            Return sBits
        End Function

        Public Shared Function OSName() As String
            Dim sName As String = "unknown"

            Try
                Dim osVersion As OperatingSystem = Environment.OSVersion
                Dim majorVersion As Integer = osVersion.Version.Major
                Dim minorVersion As Integer = osVersion.Version.Minor

                Dim osVersionInfo As New OSVERSIONINFOEX()
                osVersionInfo.dwOSVersionInfoSize = Marshal.SizeOf(GetType(OSVERSIONINFOEX))
                Dim productType As Byte = osVersionInfo.wProductType

                sName = "??? (" & majorVersion.ToString & "." & minorVersion.ToString & "." & productType.ToString & ")"

                Select Case osVersion.Platform
                    Case PlatformID.Win32NT
                        Select Case majorVersion
                            Case 3
                                sName = "Windows NT 3.51"
                            Case 4
                                sName = "Windows NT 4.0"
                            Case 5
                                Select Case minorVersion
                                    Case 0
                                        sName = "Windows 2000"
                                    Case 1
                                        sName = "Windows XP"
                                    Case 2
                                        sName = "Windows Server 2003"
                                End Select
                            Case 6
                                Select Case minorVersion
                                    Case 0
                                        sName = "Windows Vista"
                                    Case 1
                                        sName = "Windows 7"
                                    Case 2
                                        sName = "Windows 8"
                                    Case 3
                                        sName = "Windows 8.1"
                                End Select
                            Case 10
                                sName = "Windows 10"
                        End Select
                End Select

            Catch ex As Exception

            End Try

            Return sName
        End Function
    End Class
#End Region

#Region "XML"
    Public Class XML
        Public Shared Function Beautify(sIn As String) As String
            Dim sRet As String = ""

            Try
                Dim xmlDoc As System.Xml.XmlDocument

                xmlDoc = New System.Xml.XmlDocument
                xmlDoc.LoadXml(sIn)

                sRet = Beautify(xmlDoc)
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Public Shared Function Beautify(xmlDoc As System.Xml.XmlDocument) As String
            Dim sRet As String = ""

            Try
                Dim sb As New System.Text.StringBuilder()
                Dim settings As New System.Xml.XmlWriterSettings()

                settings.Indent = True
                settings.IndentChars = "    "
                settings.NewLineChars = vbCrLf
                settings.NewLineHandling = System.Xml.NewLineHandling.Replace

                Using writer As System.Xml.XmlWriter = System.Xml.XmlWriter.Create(sb, settings)
                    xmlDoc.Save(writer)
                End Using

                sRet = sb.ToString()
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

    End Class
#End Region

#Region "types"
    Public Class Types
        Public Shared Function CastStringToInteger_safe(s As String) As Integer
            Dim iTmp As Integer = -1

            If Integer.TryParse(s, iTmp) Then

            End If

            Return iTmp
        End Function

        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function LeadingZero(num As Integer, Optional len As Integer = 2) As String
            Dim sNum As String = num.ToString

            If sNum.Length < len Then
                sNum = (New String("0", (len - sNum.Length))) & sNum
            End If

            Return sNum
        End Function

        Public Shared Function RandomString(Length As Integer) As String
            Return RandomString(Length, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        End Function

        Public Shared Function RandomString(Length As Integer, CharsToChooseFrom As String) As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function

        Public Shared Function FromNormalDateToHBDate(Optional WithTime As Boolean = False) As String
            Return FromNormalDateToHBDate(Now, WithTime)
        End Function

        Public Shared Function FromNormalDateToHBDate(d As Date, Optional WithTime As Boolean = False) As String
            Dim sTmp As String = d.ToString("yyyy-MM-dd")

            If WithTime Then
                sTmp &= " " & d.ToString("HH:mm:ss")
            End If

            Return sTmp
        End Function

        Public Shared Function FromHBDateToNormalDate(s As String) As Date
            Dim d As Date

            Try
                d = Date.Parse(s)
            Catch ex As Exception
                d = Nothing
            End Try

            Return d
        End Function

        Public Shared Function IsValidHBDate(s As String) As Boolean
            Dim d As Date

            Try
                d = Date.Parse(s)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function MaskString(s As String, Optional Mask As String = "*", Optional ShowCharsAtStartCount As Integer = 2, Optional ShowCharsAtEndCound As Integer = 0) As String
            If s.Length > (ShowCharsAtStartCount + ShowCharsAtEndCound) Then
                Return String.Concat(s.Substring(0, ShowCharsAtStartCount), "".PadLeft(s.Length - (ShowCharsAtStartCount + ShowCharsAtEndCound), Mask), s.Substring(s.Length - ShowCharsAtEndCound, ShowCharsAtEndCound))
            Else
                Return s
            End If
        End Function

        Public Shared Function String2Boolean(s As String) As Boolean
            s = s.ToLower

            Return (s.Equals("yes") Or s.Equals("true") Or s.Equals("1"))
        End Function
    End Class
#End Region

#Region "time and date"
    Public Class TimeDate
        Public Shared Function UnixTimestamp() As Long
            Return Convert.ToInt64((DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds)
        End Function

        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Return FromUnix(UnixTime, True)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long, bDaylightSavingCorrection As Boolean) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime And bDaylightSavingCorrection Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now, False)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Return ToUnix(dTmp, False)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date, bDaylightSavingCorrection As Boolean) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime And bDaylightSavingCorrection Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function

        Public Class Timezone
            Public Shared Function getName() As String
                Dim zone As TimeZoneInfo = TimeZoneInfo.Local

                Return zone.DisplayName
            End Function

            Public Shared Function getOffset() As Double
                Dim dOffset As Double = 0.0

                Dim zone As TimeZoneInfo = TimeZoneInfo.Local
                Dim offset As TimeSpan = zone.GetUtcOffset(DateTime.Now)

                dOffset += offset.Hours
                dOffset += (offset.Minutes / 60)

                Return dOffset
            End Function
        End Class
    End Class
#End Region

#Region "registry"
    Public Class Registry '32-bit 
        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function KeyExists_CU(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.CurrentUser, keyName)
        End Function

        Public Shared Function KeyExists_LM(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.LocalMachine, keyName)
        End Function

        Private Shared Function _KeyExists(keyHive As Microsoft.Win32.RegistryHive, keyName As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.LocalMachine Then
                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyHive = RegistryHive.CurrentUser Then
                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                Errors.Add(ex.Message)

                Return False
            End Try
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
            Return FindKeyByString(keyName, SearchString, "", ErrorMessage)
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, SearchInValueName As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchInValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchInValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchInValueName).ToString
                            End If


                            subkey.Close()

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                bRet = True

                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function FindValueByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Object
            Dim oRet As Object = Nothing
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each valueName As String In key.GetValueNames
                    If valueName.ToLower.Equals(SearchString.ToLower) Then
                        oRet = key.GetValue(valueName)

                        Exit For
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                oRet = Nothing
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return oRet
        End Function

        Public Shared Function FindKeyBySubValueAndReturnValue(keyName As String, SearchString As String, SearchValueName As String, ReturnValueName As String, ByRef ReturnValue As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"
            ReturnValue = "not found"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.Registry.LocalMachine
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.Registry.CurrentUser
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchValueName).ToString
                            End If

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                If Not subkey.GetValue(ReturnValueName) Is Nothing Then
                                    ReturnValue = subkey.GetValue(ReturnValueName).ToString

                                    bRet = True
                                End If
                            End If

                            subkey.Close()

                            If bRet Then
                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp.ToLower.Equals("yes") Or sTmp.ToLower.Equals("true") Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SetValue_Boolean(keyName As String, valueName As String, value As Boolean)
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "true", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "false", Microsoft.Win32.RegistryValueKind.String)
                End If
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "binary"
        Public Shared Function GetValue_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_Binary(keyName As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

        Public Shared Sub SetValue_Binary(keyName As String, valueName As String, value As Object)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.Binary)

            Catch ex As Exception

            End Try
        End Sub

        Public Shared Function GetValue_ReadOnly_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_ReadOnly_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_ReadOnly_Binary(keyname As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = Microsoft.Win32.Registry.GetValue(keyname, valueName, defaultValue)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

#End Region

#Region "string" ' Microsoft.Win32.RegistryKey.OpenSubKey(String name, Boolean writable)
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                sTmp = defaultValue
            Else
                sTmp = Convert.ToString(oTmp)
            End If

            Return sTmp
        End Function

        Public Shared Function SetValue_String(keyName As String, valueName As String, value As String) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function


        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String, defaultValue As String) As String
            Return GetValue_ReadOnly_String(RegistryHive.CurrentUser, keyName, valueName, defaultValue)
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(hiveName, keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(hiveName As RegistryHive, keyname As String, valueName As String, defaultValue As String) As String
            Try
                Dim oTmp As Object, sTmp As String
                Dim r As RegistryKey

                If hiveName = RegistryHive.LocalMachine Then
                    r = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyname, False)
                ElseIf hiveName = RegistryHive.CurrentUser Then
                    r = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyname, False)
                Else
                    Return defaultValue
                End If

                oTmp = r.GetValue(valueName, defaultValue)
                sTmp = Convert.ToString(oTmp)

                Return sTmp
            Catch ex As Exception
                Errors.Add("GetValue_ReadOnly_String(): " & ex.Message)

                Return "GetValue_ReadOnly_String(): " & ex.Message
            End Try
        End Function
#End Region

#Region "long"
        Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
            Return GetValue_Long(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
            Dim oTmp As Object, iTmp As Long

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                iTmp = defaultValue
            Else
                iTmp = Helpers.Types.CastToLong_safe(oTmp)
            End If


            Return iTmp
        End Function

        Public Shared Function SetValue_Long(keyName As String, valueName As String, value As Long) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.QWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)

            If oTmp Is Nothing Then
                iTmp = defaultValue
            Else
                iTmp = Helpers.Types.CastToInteger_safe(oTmp)
            End If


            Return iTmp
        End Function

        Public Shared Function SetValue_Integer(keyName As String, valueName As String, value As Integer) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
#End Region

#Region "misc"
        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, True)
        End Function

        Public Shared Function DeleteSubKey(keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Public Shared Function DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(keyHive, keyRoot, keyToDelete, bKeepKeyToDelete)
        End Function

        Private Shared Function _DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyRoot As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Try
                Dim regKey As RegistryKey

                If keyHive = RegistryHive.CurrentUser Then
                    regKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(keyRoot, True)
                ElseIf keyHive = RegistryHive.LocalMachine Then
                    regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(keyRoot, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                'delete all subkeys
                regKey.DeleteSubKeyTree(keyToDelete)

                If bKeepKeyToDelete Then
                    'put back the root sub key
                    regKey.CreateSubKey(keyToDelete)
                End If

                Return True
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return False
        End Function
#End Region
    End Class

    Public Class Registry64
        Private Shared Function _String2RegKey(keyName As String) As Microsoft.Win32.RegistryKey
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return key
            Catch ex As Exception
                Errors.Add(ex.Message)

                Return Nothing
            End Try
        End Function

        Public Shared Function KeyExists(keyName As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = "ok"

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)
                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function KeyExists_CU(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.CurrentUser, keyName)
        End Function

        Public Shared Function KeyExists_LM(keyName As String) As Boolean
            Return _KeyExists(RegistryHive.LocalMachine, keyName)
        End Function

        Private Shared Function _KeyExists(keyHive As Microsoft.Win32.RegistryHive, keyName As String) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.LocalMachine Or keyHive = RegistryHive.CurrentUser Then
                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(keyHive, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, False)

                    If key Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                Return False
            Catch ex As Exception
                Errors.Add(ex.Message)

                Return False
            End Try
        End Function


        Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
            Return FindKeyByString(keyName, SearchString, "", ErrorMessage)
        End Function

        Public Shared Function FindKeyByString(keyName As String, SearchString As String, SearchInValueName As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchInValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchInValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchInValueName).ToString
                            End If


                            subkey.Close()

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                bRet = True

                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function FindKeyBySubValueAndReturnValue(keyName As String, SearchString As String, SearchValueName As String, ReturnValueName As String, ByRef ReturnValue As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False
            Dim rootkey As Microsoft.Win32.RegistryKey, key As Microsoft.Win32.RegistryKey, subkey As Microsoft.Win32.RegistryKey

            ErrorMessage = "ok"
            ReturnValue = "not found"

            Try
                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")
                    rootkey = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key = rootkey.OpenSubKey(keyName, False)
                If key Is Nothing Then
                    Throw New Exception(keyName & " not found!")
                End If

                For Each subKeyName As String In key.GetSubKeyNames()
                    If SearchValueName.Equals("") Then
                        If subKeyName.ToLower.Contains(SearchString.ToLower) Then
                            bRet = True

                            Exit For
                        End If
                    Else
                        Try
                            Dim searchValue As String = ""

                            subkey = rootkey.OpenSubKey(keyName & "\" & subKeyName, False)
                            If Not subkey.GetValue(SearchValueName) Is Nothing Then
                                searchValue = subkey.GetValue(SearchValueName).ToString
                            End If

                            If searchValue.ToLower.Contains(SearchString.ToLower) Then
                                If Not subkey.GetValue(ReturnValueName) Is Nothing Then
                                    ReturnValue = subkey.GetValue(ReturnValueName).ToString

                                    bRet = True
                                End If
                            End If

                            subkey.Close()

                            If bRet Then
                                Exit For
                            End If
                        Catch ex As Exception
                            ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                            bRet = False
                        End Try
                    End If
                Next
            Catch ex As Exception
                ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

                bRet = False
            End Try


            Try
#Disable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
                key.Close()
#Enable Warning BC42104 ' Variable is passed by reference before it has been assigned a value
            Catch ex As Exception

            End Try


            Return bRet
        End Function


        'Public Shared Function FindKeyByString(keyName As String, SearchString As String, ByRef ErrorMessage As String) As Boolean
        '    Dim bRet As Boolean = False

        '    ErrorMessage = "ok"

        '    Try
        '        Dim key_root As Microsoft.Win32.RegistryKey
        '        Dim key As Microsoft.Win32.RegistryKey

        '        If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
        '            keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

        '            key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
        '            key = key_root.OpenSubKey(keyName, False)
        '            If key Is Nothing Then
        '                Throw New Exception(keyName & " not found!")
        '            End If
        '        ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
        '            keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

        '            key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
        '            key = key_root.OpenSubKey(keyName, False)
        '            If key Is Nothing Then
        '                Throw New Exception(keyName & " not found!")
        '            End If
        '        Else
        '            Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
        '        End If

        '        For Each subKeyName As String In key.GetSubKeyNames()
        '            If subKeyName.ToLower.Contains(SearchString.ToLower) Then
        '                bRet = True

        '                Exit For
        '            End If
        '        Next
        '    Catch ex As Exception
        '        ErrorMessage = "Error while checking '" & keyName & "' - " & ex.Message

        '        bRet = False
        '    End Try

        '    Return bRet
        'End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String, ByRef ErrorMessage As String) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                    key.DeleteValue(valueName)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                ErrorMessage = "ok"
                Return True
            Catch ex As Exception
                ErrorMessage = "Error while removing '" & keyName & "\" & valueName & "' - " & ex.Message
                Return False
            End Try
        End Function

        Public Shared Function RemoveValue(keyName As String, valueName As String) As Boolean
            Dim sVoid As String = ""

            Return RemoveValue(keyName, valueName, sVoid)
        End Function

#Region "_GetValue & _SetValue & _DeleteValue"
        Private Shared Function _DeleteValue(keyName As String, valueName As String, thrownOnMissingValue As Boolean) As Boolean
            Dim ret As Boolean = False

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key.DeleteValue(valueName, thrownOnMissingValue)
                ret = True
            Catch ex As Exception
                Errors.Add(ex.Message)
                ret = False
            End Try

            Return ret
        End Function

        Private Shared Function _GetValue(keyName As String, valueName As String) As Object
            Return _GetValue(keyName, valueName, True)
        End Function

        Private Shared Function _GetValue(keyName As String, valueName As String, writable As Boolean) As Object
            Dim oTmp As Object

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                oTmp = key.GetValue(valueName, Nothing)
            Catch ex As Exception
                oTmp = Nothing
            End Try

            Return oTmp
        End Function

        Private Shared Function _SetValue(keyName As String, valueName As String, value As Object, valueKind As RegistryValueKind) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey


                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                    key = key_root.OpenSubKey(keyName, True)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If


                key.SetValue(valueName, value, valueKind)

                Return True
            Catch ex As Exception
                Errors.Add(ex.Message)
                Return False
            End Try
        End Function


#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String, bRet As Boolean

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    bRet = defaultValue
                End If

                sTmp = Convert.ToString(oTmp)
                If sTmp.Equals("yes") Or sTmp.Equals("no") Then
                    bRet = sTmp.Equals("yes")
                Else
                    bRet = defaultValue
                End If
            Catch ex As Exception
                bRet = defaultValue
            End Try

            Return bRet
        End Function

        Public Shared Function SetValue_Boolean(keyName As String, valueName As String, value As Boolean) As Boolean
            If value Then
                Return SetValue_String(keyName, valueName, "yes")
            Else
                Return SetValue_String(keyName, valueName, "no")
            End If
        End Function
#End Region

#Region "binary"
        Public Shared Function GetValue_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_Binary(keyName As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

        Public Shared Function SetValue_Binary(keyName As String, valueName As String, value As Object) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.Binary)
        End Function


        Public Shared Function GetValue_ReadOnly_Binary(keyName As String, valueName As String) As Byte()
            Return GetValue_ReadOnly_Binary(keyName, valueName, Nothing)
        End Function

        Public Shared Function GetValue_ReadOnly_Binary(keyname As String, valueName As String, defaultValue As Byte()) As Byte()
            Dim oTmp As Object
            Dim _bytes As Byte()

            Try
                oTmp = _GetValue(keyname, valueName, False)

                If oTmp Is Nothing Then
                    _bytes = defaultValue
                Else
                    _bytes = CType(oTmp, Byte())
                End If
            Catch ex As Exception
                _bytes = defaultValue
            End Try

            Return _bytes
        End Function

#End Region

#Region "string"
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function

        Public Shared Function SetValue_String(keyName As String, valueName As String, value As String) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.String)
        End Function


        Public Shared Function GetValue_ReadOnly_String(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_String(keyname As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyname, valueName, False)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function
#End Region

#Region "expand string"
        Public Shared Function GetValue_ExpandString(keyName As String, valueName As String) As String
            Return GetValue_ExpandString(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ExpandString(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function

        Public Shared Function SetValue_ExpandString(keyName As String, valueName As String, value As String) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.ExpandString)
        End Function


        Public Shared Function GetValue_ReadOnly_ExpandString(keyName As String, valueName As String) As String
            Return GetValue_ReadOnly_ExpandString(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_ReadOnly_ExpandString(keyname As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            Try
                oTmp = _GetValue(keyname, valueName, False)

                If oTmp Is Nothing Then
                    sTmp = defaultValue
                Else
                    sTmp = Convert.ToString(oTmp)
                End If
            Catch ex As Exception
                sTmp = defaultValue
            End Try

            Return sTmp
        End Function
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Integer) As Integer
            Dim oTmp As Object, iTmp As Integer

            Try
                oTmp = _GetValue(keyName, valueName)

                If oTmp Is Nothing Then
                    iTmp = defaultValue
                Else
                    iTmp = Helpers.Types.CastToInteger_safe(oTmp)
                End If
            Catch ex As Exception
                iTmp = defaultValue
            End Try

            Return iTmp
        End Function

        Public Shared Function SetValue_Integer(keyName As String, valueName As String, value As Integer) As Boolean
            Return _SetValue(keyName, valueName, value, RegistryValueKind.DWord)
        End Function
#End Region

#Region "delete value"
        Public Shared Function DeleteValue(keyName As String, valueName As String) As Boolean
            Return DeleteValue(keyName, valueName, True)
        End Function

        Public Shared Function DeleteValue(keyName As String, valueName As String, thrownOnMissingValue As Boolean) As Boolean
            Return _DeleteValue(keyName, valueName, thrownOnMissingValue)
        End Function
#End Region
#End Region

#Region "misc"
        Public Shared Function CreateKey(keyName As String) As Boolean
            Dim bRet As Boolean = False

            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key_new As Microsoft.Win32.RegistryKey

                If keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64)
                ElseIf keyName.StartsWith("HKEY_CURRENT_USER\") Then
                    keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key_new = key_root.CreateSubKey(keyName)

                If Not key_new Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return bRet
        End Function

        Public Shared Function DeleteSubKey(keyName As String, keyToDelete As String) As Boolean
            If keyName.StartsWith("HKEY_CURRENT_USER\") Then
                keyName = keyName.Replace("HKEY_CURRENT_USER\", "")

                Return _DeleteSubKey(RegistryHive.CurrentUser, keyName, keyToDelete, True)
            ElseIf keyName.StartsWith("HKEY_LOCAL_MACHINE\") Then
                keyName = keyName.Replace("HKEY_LOCAL_MACHINE\", "")

                Return _DeleteSubKey(RegistryHive.LocalMachine, keyName, keyToDelete, True)
            Else
                Return False
            End If
        End Function

        Public Shared Function DeleteSubKey(keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(RegistryHive.CurrentUser, keyName, keyToDelete, bKeepKeyToDelete)
        End Function

        Public Shared Function DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Return _DeleteSubKey(keyHive, keyName, keyToDelete, bKeepKeyToDelete)
        End Function

        Private Shared Function _DeleteSubKey(keyHive As Microsoft.Win32.RegistryHive, keyName As String, keyToDelete As String, bKeepKeyToDelete As Boolean) As Boolean
            Try
                Dim key_root As Microsoft.Win32.RegistryKey
                Dim key As Microsoft.Win32.RegistryKey

                If keyHive = RegistryHive.CurrentUser Or keyHive = RegistryHive.LocalMachine Then
                    key_root = Microsoft.Win32.RegistryKey.OpenBaseKey(keyHive, RegistryView.Registry64)
                Else
                    Throw New Exception("Only for LOCAL_MACHINE or CURRENT_USER")
                End If

                key = key_root.OpenSubKey(keyName, True)

                'delete all subkeys
                key.DeleteSubKeyTree(keyToDelete)

                If bKeepKeyToDelete Then
                    'put back the root sub key
                    key.CreateSubKey(keyToDelete)
                End If

                Return True
            Catch ex As Exception
                Errors.Add(ex.Message)
            End Try

            Return False
        End Function
#End Region
    End Class
#End Region

#Region "sitekiosk"
    Public Class SiteKiosk
        Public Shared ReadOnly RootRegistryKey As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk"


        Public Shared Function IsInstalled() As Boolean
            Dim sPath_SiteKiosk As String = GetSiteKioskInstallFolder()
            Dim sFile_SKConfig As String = GetSiteKioskActiveConfig()

            If Not IO.Directory.Exists(sPath_SiteKiosk) Then
                Return False
            End If

            If Not IO.File.Exists(sFile_SKConfig) Then
                Return False
            End If

            Return True
        End Function

        Public Shared Function GetSiteKioskVersion(Optional bShort As Boolean = False) As String
            Dim sTmp As String = Registry.GetValue_String(RootRegistryKey, "Build", "0.0")

            If bShort Then
                Return sTmp.Split(".")(0)
            Else
                Return sTmp
            End If
        End Function

        Public Class License
            Public Shared Function GetLicensee() As String
                Return Registry.GetValue_String(RootRegistryKey, "Licensee", "")
            End Function

            Public Shared Function GetSignature() As String
                Return Registry.GetValue_String(RootRegistryKey, "Signature", "")
            End Function

            Public Shared Sub SetLicensee(NewLicensee As String)
                Registry.SetValue_String(RootRegistryKey, "Licensee", NewLicensee)
            End Sub

            Public Shared Sub SetSignature(NewSignature As String)
                Registry.SetValue_String(RootRegistryKey, "Signature", NewSignature)
            End Sub
        End Class

        Public Class SkCfg
            'Public Shared Function FindViewableFolders(Name As String) As Hashtable
            '    Dim bRet As Boolean = False

            '    Try
            '        Dim mSkCfgFile As String = GetSiteKioskActiveConfig()

            '        Dim nt As System.Xml.XmlNameTable
            '        Dim ns As System.Xml.XmlNamespaceManager
            '        Dim mSkCfgXml As System.Xml.XmlDocument
            '        Dim mXmlNode_Root As System.Xml.XmlNode
            '        Dim sTmpFilename As String = ""
            '        Dim sTmpDescription As String = ""


            '        mSkCfgXml = New System.Xml.XmlDocument

            '        mSkCfgXml.Load(mSkCfgFile)

            '        nt = mSkCfgXml.NameTable
            '        ns = New System.Xml.XmlNamespaceManager(nt)
            '        ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            '        ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            '        mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            '        If Not mXmlNode_Root Is Nothing Then

            '        Else
            '            Throw New Exception("XmlNodeRoot Is Nothing")
            '        End If


            '        Dim mXmlNode_Programs As System.Xml.XmlNode
            '        Dim mXmlNode_Files As System.Xml.XmlNodeList
            '        Dim mXmlNode_File As System.Xml.XmlNode


            '        mXmlNode_Programs = mXmlNode_Root.SelectSingleNode("sk:programs", ns)
            '        mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)
            '        For Each mXmlNode_File In mXmlNode_Files
            '            sTmpFilename = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower
            '            sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("description").Value.ToLower
            '            If sTmpDescription.Equals("") Then
            '                sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("title").Value.ToLower
            '            End If

            '            If sTmpFilename.Contains(Name) Or sTmpDescription.Contains(Name) Then
            '                bRet = True

            '                Exit For
            '            End If
            '        Next
            '    Catch ex As Exception
            '        Errors.Add(ex.Message)
            '    End Try

            '    Return bRet
            'End Function

            Public Shared Function GetDownloadFolder() As String
                Dim sRet As String = ""

                Try
                    Dim mSkCfgFile As String = SiteKiosk.GetSiteKioskActiveConfig()

                    Dim nt As System.Xml.XmlNameTable
                    Dim ns As System.Xml.XmlNamespaceManager
                    Dim mSkCfgXml As System.Xml.XmlDocument
                    Dim mXmlNode_Root As System.Xml.XmlNode
                    Dim sTmpFilename As String = ""
                    Dim sTmpDescription As String = ""


                    mSkCfgXml = New System.Xml.XmlDocument

                    mSkCfgXml.Load(mSkCfgFile)

                    nt = mSkCfgXml.NameTable
                    ns = New System.Xml.XmlNamespaceManager(nt)
                    ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
                    ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

                    mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

                    If Not mXmlNode_Root Is Nothing Then

                    Else
                        Throw New Exception("XmlNodeRoot Is Nothing")
                    End If


                    Dim mXmlNode_DownloadManager As System.Xml.XmlNode
                    Dim mXmlNode_DownloadFolder As System.Xml.XmlNode


                    mXmlNode_DownloadManager = mXmlNode_Root.SelectSingleNode("sk:download-manager", ns)
                    mXmlNode_DownloadFolder = mXmlNode_DownloadManager.SelectSingleNode("sk:download-folder", ns)


                    If mXmlNode_DownloadFolder Is Nothing Then
                        Throw New Exception("mXmlNode_DownloadFolder Is Nothing")
                    End If

                    sRet = mXmlNode_DownloadFolder.InnerText
                Catch ex As Exception
                    Errors.Add(ex.Message)
                End Try

                Return sRet
            End Function


            Public Shared Function FindExternalApplication(Name As String) As Boolean
                Dim bRet As Boolean = False

                Try
                    Dim mSkCfgFile As String = SiteKiosk.GetSiteKioskActiveConfig()

                    Dim nt As System.Xml.XmlNameTable
                    Dim ns As System.Xml.XmlNamespaceManager
                    Dim mSkCfgXml As System.Xml.XmlDocument
                    Dim mXmlNode_Root As System.Xml.XmlNode
                    Dim sTmpFilename As String = ""
                    Dim sTmpDescription As String = ""


                    mSkCfgXml = New System.Xml.XmlDocument

                    mSkCfgXml.Load(mSkCfgFile)

                    nt = mSkCfgXml.NameTable
                    ns = New System.Xml.XmlNamespaceManager(nt)
                    ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
                    ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

                    mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

                    If Not mXmlNode_Root Is Nothing Then

                    Else
                        Throw New Exception("XmlNodeRoot Is Nothing")
                    End If


                    Dim mXmlNode_Programs As System.Xml.XmlNode
                    Dim mXmlNode_Files As System.Xml.XmlNodeList
                    Dim mXmlNode_File As System.Xml.XmlNode


                    mXmlNode_Programs = mXmlNode_Root.SelectSingleNode("sk:programs", ns)
                    mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)
                    For Each mXmlNode_File In mXmlNode_Files
                        sTmpFilename = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower
                        sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("description").Value.ToLower
                        If sTmpDescription.Equals("") Then
                            sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("title").Value.ToLower
                        End If

                        If sTmpFilename.Contains(Name) Or sTmpDescription.Contains(Name) Then
                            bRet = True

                            Exit For
                        End If
                    Next
                Catch ex As Exception
                    Errors.Add(ex.Message)
                End Try

                Return bRet
            End Function
        End Class

        Public Shared Function GetSiteKioskInstallFolder() As String
            Return Registry.GetValue_String(RootRegistryKey, "InstallDir", "")
        End Function

        Public Shared Function GetSiteKioskActiveConfig() As String
            Return Registry.GetValue_String(RootRegistryKey, "LastCfg", "")
        End Function

        Public Shared Function SetSiteKioskActiveConfig(SkCfg As String) As Boolean
            Registry.SetValue_String(RootRegistryKey, "LastCfg", SkCfg)

            Return SkCfg.Equals(GetSiteKioskActiveConfig())
        End Function

        Public Shared Function GetSiteKioskLogfileFolder() As String
            Dim _s As String = Helpers.SiteKiosk.GetSiteKioskInstallFolder

            If _s.Equals("") Then
                Return ""
            Else
                Return IO.Path.Combine(_s, "logfiles")
            End If
        End Function

        Public Shared Function GetMachineNameFromScript() As String
            Dim sRet As String = ""
            Dim sFileBase As String, sFile As String
            sFileBase = GetSiteKioskInstallFolder()
            sFileBase = sFileBase & "\skins\default\scripts"

            sFile = sFileBase & "\guesttek.js"
            If Not IO.File.Exists(sFile) Then
                sFile = sFileBase & "\guest-tek.js"
                If Not IO.File.Exists(sFile) Then
                    sFile = sFileBase & "\ibahn.js"
                    If Not IO.File.Exists(sFile) Then
                        sFile = sFileBase & "\mycall.js"
                        If Not IO.File.Exists(sFile) Then
                            Return "NO_SCRIPT_FOUND"
                        End If
                    End If
                End If
            End If

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var LocationID=") Then
                            line = line.Replace("var LocationID=", "")
                            line = line.Replace("""", "")
                            line = line.Replace(";", "")

                            sRet = line

                            Exit While
                        End If
                    End While
                End Using
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Private Shared ReadOnly _sReplacements() As String = {
            "[SiteKiosk] Notification: [Memory Status]",
            "[SiteKiosk] Notification: [Process Status]",
            ""}

        Public Shared Function CleanSiteKioskLogLine(ByVal input As String) As String
            Dim result As String = input

            For Each item As String In _sReplacements
                If Not item.Equals("") Then
                    If result.Contains(item) Then
                        result = ""
                        Exit For
                    End If
                End If
            Next

            If result <> "" Then
                'remove unwanted chars
                For i As Integer = 0 To 31
                    result = result.Replace(Chr(i), "")
                Next
                For i As Integer = 127 To 255
                    result = result.Replace(Chr(i), "")
                Next
                'Additionally, let's remove </ < and >
                result = result.Replace("</", "[")
                result = result.Replace("<", "[")
                result = result.Replace("/>", "]")
                result = result.Replace(">", "]")
            End If

            Return result
        End Function

        Public Shared Function TranslateSiteKioskPathToNormalPath(sSkPath As String) As String
            Dim sPath As String = sSkPath

            If sPath.StartsWith("%SiteKioskPath%") Then
                sPath = sPath.Replace("%SiteKioskPath%", GetSiteKioskInstallFolder)
                sPath = sPath.Replace("%%PROGRAMFILES%%/SiteKiosk", GetSiteKioskInstallFolder)
                sPath = sPath.Replace("/", "\")
            ElseIf sPath.StartsWith("file://") Then
                sPath = sPath.Replace("file:///", "")
                sPath = sPath.Replace("file://", "")
                sPath = sPath.Replace("/", "\")
            Else

            End If

            Return sPath
        End Function

        Public Shared Function TranslateNormalPathToSiteKioskPath(sPath As String) As String
            Return "file://" & sPath
        End Function

        Public Shared Function Restart() As Integer
            Return Application.Kill
        End Function

        Public Class Application
            Public Shared Function Kill() As Integer
                Return Processes.KillProcesses("sitekiosk")
            End Function
        End Class
    End Class
#End Region

#Region "files and folders"
    Public Class FilesAndFolders
        Public Shared Function GetFileVersion(sFile As String, Optional iParts As Integer = -1) As String
            If Not IO.File.Exists(sFile) Then
                Return ""
            End If

            Dim sVersion As String = ""

            Try
                sVersion = FileVersionInfo.GetVersionInfo(sFile).FileVersion
                sVersion = ParseFileVersion(sVersion, iParts)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

            Return sVersion
        End Function

        Public Shared Function ParseFileVersion(sVersion As String, Optional iParts As Integer = -1) As String
            Try
                Dim sVersionParts As String()

                If sVersion Is Nothing Then
                    sVersion = ""
                End If

                sVersion = sVersion.Split(" ")(0)

                If iParts > 0 Then
                    sVersionParts = sVersion.Split(".")

                    If sVersionParts.Length > iParts Then
                        sVersion = String.Join(".", sVersionParts.Take(iParts))
                    End If
                End If

                Return sVersion
            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

            Return ""
        End Function


        Public Shared Function GetFileCompileDate(sFile As String) As String
            Return GetFileCompileDate(sFile, "yyyy-MM-dd HH:mm:ss")
        End Function

        Public Shared Function GetFileCompileDate(sFile As String, DateFormat As String) As String
            Try
                Const PeHeaderOffset As Integer = 60
                Const LinkerTimestampOffset As Integer = 8

                Dim b(2047) As Byte
                'Dim s As IO.Stream
                Dim s As New Object

                Try
                    s = New IO.FileStream(sFile, IO.FileMode.Open, IO.FileAccess.Read)
                    s.Read(b, 0, 2048)
                Finally
                    Try
                        If Not s Is Nothing Then s.Close()
                    Catch ex As Exception

                    End Try
                End Try


                Dim i As Integer = BitConverter.ToInt32(b, PeHeaderOffset)

                Dim SecondsSince1970 As Integer = BitConverter.ToInt32(b, i + LinkerTimestampOffset)
                Dim dt As New DateTime(1970, 1, 1, 0, 0, 0)
                dt = dt.AddSeconds(SecondsSince1970)
                dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours)

                Return dt.ToString(DateFormat)
            Catch ex As Exception
                Return GetFileCreationDate(sFile, DateFormat)
            End Try

        End Function

        Public Shared Function GetFileCreationDate(sFile As String) As String
            Return GetFileCreationDate(sFile, "yyyy-MM-dd")
        End Function

        Public Shared Function GetFileCreationDate(sFile As String, DateFormat As String) As String
            Try
                Dim dDate As Date = IO.File.GetCreationTime(sFile)

                Return dDate.ToString(DateFormat)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String) As String
            Return GetFileLastModifiedDate(sFile, "yyyy-MM-dd")
        End Function

        Public Shared Function GetFileLastModifiedDate(sFile As String, DateFormat As String) As String
            Try
                Dim dDate As Date = IO.File.GetLastWriteTime(sFile)

                Return dDate.ToString(DateFormat)
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetFileLastModifiedDateAsDate(sFile As String) As Date
            Dim dDate As New Date(0)

            Try
                dDate = IO.File.GetLastWriteTime(sFile)
            Catch ex As Exception

            End Try

            Return dDate
        End Function

        Public Shared Function GetFileTypeFromRegistry(FileExtension As String) As String
            Try
                If FileExtension.Contains(" ") Then
                    FileExtension = ""
                    Throw New Exception("space in extension")
                End If

                Dim reg1 As Object = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & FileExtension, "", FileExtension)

                If reg1 Is Nothing Then
                    Throw New Exception("Unknown file type")
                End If

                If reg1.ToString.Equals(FileExtension) Then
                    Throw New Exception("Unknown file type")
                End If


                Dim reg2 As Object = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & reg1, "", FileExtension)

                If reg2 Is Nothing Then
                    Throw New Exception("Unknown file type")
                End If

                If reg2.ToString.Equals(FileExtension) Then
                    Throw New Exception("Unknown file type")
                End If

                Return reg2
            Catch ex As Exception
                If FileExtension.StartsWith(".") Then
                    FileExtension = FileExtension.Substring(1, FileExtension.Length - 1)
                End If

                If FileExtension.Equals("") Then
                    Return "File"
                Else
                    Return FileExtension & " File"
                End If
            End Try

            'Return My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" &
            '                                     My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & FileExtension, "", FileExtension).ToString, "", FileExtension).ToString
        End Function

        Public Shared Function GetFileDescription(FilePath As String) As String
            Dim _s As String = ""

            If Not IO.File.Exists(FilePath) Then
                Return "(" & FilePath & ")"
            End If

            Try
                Dim fi As FileVersionInfo = FileVersionInfo.GetVersionInfo(FilePath)

                _s = fi.FileDescription
            Catch ex As Exception
                _s = "(" & FilePath & ")"
            End Try

            Return _s
        End Function

        Public Class FileSize
            Public Enum HumanReadable
                Use1024 = 0
                Use1024i = 1
                Use1000 = 2
            End Enum

            Public Shared Function GetHumanReadable(FileSize As Long) As String
                Return GetHumanReadable(FileSize, HumanReadable.Use1024)
            End Function

            Public Shared Function GetHumanReadable(FileSize As Long, Base1024 As HumanReadable) As String
                If FileSize < 0 Then
                    Return ""
                End If

                Select Case Base1024
                    Case HumanReadable.Use1000
                        Select Case FileSize
                            Case > (1000 * 1000 * 1000)
                                Return CInt(FileSize / (1000 * 1000 * 1000)).ToString & "GB"
                            Case > (1000 * 1000)
                                Return CInt(FileSize / (1000 * 1000)).ToString & "MB"
                            Case > 1000
                                Return CInt(FileSize / 1000).ToString & "kB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case HumanReadable.Use1024
                        Select Case FileSize
                            Case > (1024 * 1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024 * 1024)).ToString & "GB"
                            Case > (1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024)).ToString & "MB"
                            Case > 1024
                                Return CInt(FileSize / 1024).ToString & "KB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case HumanReadable.Use1024i
                        Select Case FileSize
                            Case > (1024 * 1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024 * 1024)).ToString & "GiB"
                            Case > (1024 * 1024)
                                Return CInt(FileSize / (1024 * 1024)).ToString & "MiB"
                            Case > 1024
                                Return CInt(FileSize / 1024).ToString & "KiB"
                            Case Else
                                Return FileSize.ToString & "B"
                        End Select
                    Case Else
                        Return "?B"
                End Select
            End Function
        End Class

        Public Shared Function GetProgramFilesFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sPath.Equals(String.Empty) Then
                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetProgramFilesFolder64bit(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetGuestTekFolder() As String
            Return IO.Path.Combine(GetProgramFilesFolder, "GuestTek")
        End Function

        Public Shared Function GetDefaultBackupFolder() As String
            Dim sTmp As String = IO.Path.Combine(GetProgramFilesFolder, "GuestTek\_backups")

            sTmp &= "\%%Application.ProductName%%\%%Application.ProductVersion%%\%%Backup.Date%%"
            sTmp = sTmp.Replace("%%Application.ProductName%%", My.Application.Info.ProductName)
            sTmp = sTmp.Replace("%%Application.ProductVersion%%", My.Application.Info.Version.ToString)
            sTmp = sTmp.Replace("%%Backup.Date%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))

            Return sTmp
        End Function

        Public Shared Function GetWindowsFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.Windows)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetAppDataLocalFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function


        Public Shared Function GetCurrentFolder() As String
            Dim P As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)

            Return New Uri(P).LocalPath
        End Function

        Public Shared Function GetExecutablePath() As String
            Dim sPath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)

            sPath = New Uri(sPath).LocalPath

            Return sPath
        End Function

        Public Shared Function GetNewestFile(Path As String) As String
            Return GetNewestFile(Path, "*.*", False)
        End Function

        Public Shared Function GetNewestFile(Path As String, SearchPattern As String) As String
            Return GetNewestFile(Path, SearchPattern, False)
        End Function

        Public Shared Function GetNewestFile(Path As String, SearchPattern As String, SearchPatternIsRegularExpression As Boolean) As String
            If Not IO.Directory.Exists(Path) Then
                Return ""
            End If


            If SearchPatternIsRegularExpression Then
                Try
                    Dim _regex = New Regex(SearchPattern)


                    Return Directory.GetFiles(Path).Where(Function(f) _regex.IsMatch(IO.Path.GetFileName(f))).OrderByDescending(Function(f) New FileInfo(f).LastWriteTime).FirstOrDefault()
                Catch ex As Exception
                    'invalid searchpattern

                    Return ex.Message
                End Try
            Else
                Return Directory.GetFiles(Path, SearchPattern).OrderByDescending(Function(f) New FileInfo(f).LastWriteTime).FirstOrDefault()
            End If
        End Function

        Public Shared Function GetNewestFolder(Path As String) As String
            If IO.Directory.Exists(Path) Then
                Return Directory.GetDirectories(Path).OrderByDescending(Function(d) New DirectoryInfo(d).LastWriteTime).FirstOrDefault()
            Else
                Return ""
            End If
        End Function

        Public Class Folder
            Public Shared Function IsRootFolder(_dir As String) As Boolean
                Dim _di As New DirectoryInfo(_dir)

                If _di.Parent Is Nothing Then
                    Return True
                Else
                    Return False
                End If
            End Function

            Public Shared Function FindRecursive(rootdir As String) As List(Of String)
                ' This list stores the results.
                Dim result As New List(Of String)

                ' This stack stores the directories to process.
                Dim stack As New Stack(Of String)

                ' Add the initial directory
                stack.Push(rootdir)

                ' Continue processing for each stacked directory
                Do While (stack.Count > 0)
                    ' Get top directory string
                    Dim dir As String = stack.Pop
                    Try
                        ' Loop through all subdirectories and add them to the stack.
                        Dim directoryName As String
                        For Each directoryName In Directory.GetDirectories(dir)
                            result.Add(directoryName)
                            stack.Push(directoryName)
                        Next

                    Catch ex As Exception
                    End Try
                Loop

                ' Return the list
                Return result
            End Function

            Public Shared Function CleanFolder(folder As String, security_string As String) As Long
                If Not security_string.Equals("I_am_sure") Then
                    Return -1
                End If

                'DANGER DANGER
                Dim iErrors As Long = 0

                For Each d As String In IO.Directory.GetDirectories(folder)
                    Try
                        IO.Directory.Delete(d, True)
                    Catch ex As Exception
                        iErrors += 1
                    End Try
                Next

                For Each f As String In Directory.GetFiles(folder)
                    Try
                        IO.File.Delete(f)
                    Catch ex As Exception
                        iErrors += 1
                    End Try
                Next

                Return iErrors
            End Function

            Public Shared Function DirectoryFileCount(ByVal sPath As String, Optional ByVal bRecursive As Boolean = False) As Long
                Dim Count As Long = 0
                Dim diDir As New IO.DirectoryInfo(sPath)

                Try
                    Dim fil As FileInfo

                    For Each fil In diDir.GetFiles()
                        Count += 1
                    Next fil

                    If bRecursive = True Then
                        Dim diSubDir As DirectoryInfo

                        For Each diSubDir In diDir.GetDirectories()
                            Count += DirectoryFileCount(diSubDir.FullName, True)
                        Next diSubDir
                    End If

                    Return Count
                Catch ex As System.IO.FileNotFoundException
                    Return 0
                Catch exx As Exception
                    Return 0
                End Try

                Return Count
            End Function

            Public Shared Function DirectorySize(ByVal sPath As String, Optional ByVal bRecursive As Boolean = False) As Long
                Dim Size As Long = 0
                Dim diDir As New IO.DirectoryInfo(sPath)

                Try
                    Dim fil As FileInfo

                    For Each fil In diDir.GetFiles()
                        Size += fil.Length
                    Next fil

                    If bRecursive = True Then
                        Dim diSubDir As DirectoryInfo

                        For Each diSubDir In diDir.GetDirectories()
                            Size += DirectorySize(diSubDir.FullName, True)
                        Next diSubDir
                    End If

                    Return Size
                Catch ex As System.IO.FileNotFoundException
                    Return 0
                Catch exx As Exception
                    Return 0
                End Try

                Return Size
            End Function
        End Class

        Public Class File
            Public Shared Function Find(rootdir As String, searchPattern As String) As List(Of IO.FileInfo)
                Try
                    Dim lList As New List(Of IO.FileInfo)

                    If Not IO.Directory.Exists(rootdir) Then
                        Throw New Exception("'" & rootdir & "' not found")
                    End If

                    Dim files() As String = Directory.GetFiles(rootdir, searchPattern)

                    For Each file As String In files
                        lList.Add(New IO.FileInfo(file))
                    Next

                    Return lList
                Catch ex As Exception
                    Errors.Add(ex.Message)
                    Return Nothing
                End Try
            End Function

            Public Shared Function FindRecursive(rootdir As String, searchPattern As String) As List(Of String)
                ' This list stores the results.
                Dim result As New List(Of String)

                ' This stack stores the directories to process.
                Dim stack As New Stack(Of String)

                ' Add the initial directory
                stack.Push(rootdir)

                ' Continue processing for each stacked directory
                Do While (stack.Count > 0)
                    ' Get top directory string
                    Dim dir As String = stack.Pop
                    Try
                        ' Add all immediate file paths
                        result.AddRange(Directory.GetFiles(dir, searchPattern))

                        ' Loop through all subdirectories and add them to the stack.
                        Dim directoryName As String
                        For Each directoryName In Directory.GetDirectories(dir)
                            stack.Push(directoryName)
                        Next

                    Catch ex As Exception
                    End Try
                Loop

                ' Return the list
                Return result
            End Function
        End Class

        Public Class Permissions
            Public Shared Function GetSecurityIdentifier(UserName As String) As SecurityIdentifier
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return secid
            End Function

            Public Shared Function FileSecurity_ADD__ReadExecute(FileName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFileSecurity(FileName, secid, FileSystemRights.ReadAndExecute, AccessControlType.Allow, False, False)
            End Function

            Public Shared Function FileSecurity_REPLACE__Full_Everyone(FileName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFileSecurity(FileName, secid, FileSystemRights.FullControl, AccessControlType.Allow, True, False)
            End Function

            Public Shared Function FileSecurity_REPLACEandPURGE__Full_Everyone(FileName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFileSecurity(FileName, secid, FileSystemRights.FullControl, AccessControlType.Allow, True, True)
            End Function

            Public Shared Function FileSecurity_ADD(ByVal FileName As String, ByVal UserName As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFileSecurity(FileName, secid, rights, controlType, DisableInheritance, False)
            End Function


            Public Shared Function FolderSecurity_ADD__ReadExecute(FolderName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.ReadAndExecute, AccessControlType.Allow, False, False, False)
            End Function

            Public Shared Function FolderSecurity_REPLACE__Full(FolderName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, False, True, False)
            End Function

            Public Shared Function FolderSecurity_REPLACEandPURGE__Full(FolderName As String, UserName As String) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, False, True, True)
            End Function

            Public Shared Function FolderSecurity_REPLACE__Full_Everyone(FolderName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, False, True, False)
            End Function

            Public Shared Function FolderSecurity_REPLACEandPURGE__Full_Everyone(FolderName As String) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, FileSystemRights.FullControl, AccessControlType.Allow, False, True, True)
            End Function

            Public Shared Function FolderSecurity_ADD(ByVal FolderName As String, ByVal UserName As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean) As Boolean
                Dim account As New NTAccount(UserName)
                Dim secid As SecurityIdentifier = DirectCast(account.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                Return _AddFolderSecurity(FolderName, secid, rights, controlType, False, DisableInheritance, False)
            End Function

            Public Shared Function FolderSecurity_ADD__Everyone(ByVal FolderName As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, rights, controlType, False, DisableInheritance, False)
            End Function

            Public Shared Function FolderSecurity_ADDandINHERIT__Everyone(ByVal FolderName As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean) As Boolean
                Dim secid As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)

                Return _AddFolderSecurity(FolderName, secid, rights, controlType, True, DisableInheritance, False)
            End Function



            Private Shared Function _AddFileSecurity(ByVal fileName As String, ByVal SecurityIdentifier As IdentityReference, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, DisableInheritance As Boolean, PurgeAllOtherRules As Boolean) As Boolean
                Try
                    Dim fSecurity As FileSecurity = IO.File.GetAccessControl(fileName)
                    Dim accessRule As FileSystemAccessRule = New FileSystemAccessRule(SecurityIdentifier, rights, controlType)

                    If PurgeAllOtherRules Then
                        DisableInheritance = True

                        Dim rules As AuthorizationRuleCollection = fSecurity.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
                        For Each rule As FileSystemAccessRule In rules
                            fSecurity.RemoveAccessRule(rule)
                        Next
                    End If

                    If DisableInheritance Then
                        fSecurity.SetAccessRuleProtection(True, False)
                    End If


                    fSecurity.AddAccessRule(accessRule)

                    IO.File.SetAccessControl(fileName, fSecurity)

                    Return True
                Catch ex As Exception
                    Errors.Add(ex.Message)

                    Return False
                End Try
            End Function

            Private Shared Function _AddFolderSecurity(ByVal folderName As String, ByVal SecurityIdentifier As IdentityReference, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType, SetInheritance As Boolean, DisableInheritance As Boolean, PurgeAllOtherRules As Boolean) As Boolean
                Try
                    Dim fSecurity As DirectorySecurity = IO.Directory.GetAccessControl(folderName)
                    'Dim accessRule As FileSystemAccessRule = New FileSystemAccessRule(SecurityIdentifier, rights, controlType)
                    Dim accessRule As FileSystemAccessRule


                    If SetInheritance Then
                        PurgeAllOtherRules = False

                        accessRule = New FileSystemAccessRule(SecurityIdentifier, rights, InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, PropagationFlags.InheritOnly, controlType)
                    Else
                        accessRule = New FileSystemAccessRule(SecurityIdentifier, rights, controlType)
                    End If

                    If PurgeAllOtherRules Then
                        DisableInheritance = True

                        Dim rules As AuthorizationRuleCollection = fSecurity.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
                        For Each rule As FileSystemAccessRule In rules
                            fSecurity.RemoveAccessRule(rule)
                        Next
                    End If

                    If DisableInheritance Then
                        fSecurity.SetAccessRuleProtection(True, False)
                    End If

                    fSecurity.AddAccessRule(accessRule)

                    IO.Directory.SetAccessControl(folderName, fSecurity)

                    Return True
                Catch ex As Exception
                    Errors.Add(ex.Message)

                    Return False
                End Try
            End Function
        End Class

        Public Class CopyFiles
            Private pBackupFolder As String
            Private pSourceDir As String
            Private pDestDir As String
            Private pSuppressCopyLogging As Boolean = False
            Private pSkipBackup As Boolean = False
            Private pLogDepth As Integer = 0

            Public Property LogDepth() As Integer
                Get
                    Return pLogDepth
                End Get
                Set(value As Integer)
                    pLogDepth = value
                End Set
            End Property

            Public Property BackupDirectory() As String
                Get
                    Return pBackupFolder
                End Get
                Set(ByVal value As String)
                    pBackupFolder = value
                End Set
            End Property

            Public Property SourceDirectory() As String
                Get
                    Return pSourceDir
                End Get
                Set(ByVal value As String)
                    pSourceDir = value
                End Set
            End Property

            Public Property DestinationDirectory() As String
                Get
                    Return pDestDir
                End Get
                Set(ByVal value As String)
                    pDestDir = value
                End Set
            End Property

            Public Property SuppressCopyLogging() As Boolean
                Get
                    Return pSuppressCopyLogging
                End Get
                Set(value As Boolean)
                    pSuppressCopyLogging = value
                End Set
            End Property

            Public Property SkipBackup() As Boolean
                Get
                    Return pSkipBackup
                End Get
                Set(value As Boolean)
                    pSkipBackup = value
                End Set
            End Property

            Private pFileCount_total As Long
            Public Property FileCount_total() As Long
                Get
                    Return pFileCount_total
                End Get
                Set(ByVal value As Long)
                    pFileCount_total = value
                End Set
            End Property

            Private pFileCount_copied As Long
            Public Property FileCount_copied() As Long
                Get
                    Return pFileCount_copied
                End Get
                Set(ByVal value As Long)
                    pFileCount_copied = value
                End Set
            End Property

            Private pFileCount_failed As Long
            Public Property FileCount_failed() As Long
                Get
                    Return pFileCount_failed
                End Get
                Set(ByVal value As Long)
                    pFileCount_failed = value
                End Set
            End Property

            Public Function CopyFiles() As Boolean
                If pSuppressCopyLogging Then
                    Logger.WriteMessageRelative("starting file copy", 1 + pLogDepth)
                    Logger.WriteMessageRelative("source: " & pSourceDir, 2 + pLogDepth)
                    Logger.WriteMessageRelative("dest  : " & pDestDir, 2 + pLogDepth)

                    Logger.WriteMessageRelative("copy logging disabled", 2 + pLogDepth)
                End If

                If Not IO.Directory.Exists(pSourceDir) Then
                    Logger.WriteMessageRelative("source not found!", 3 + pLogDepth)

                    Return False
                End If

                pFileCount_copied = 0
                pFileCount_failed = 0
                pFileCount_total = 0

                RecursiveDirectoryCopy(pSourceDir, pDestDir)

                Return True
            End Function

            Public Function CopyFiles(ByVal SourceDir As String, ByVal DestinationDir As String) As Boolean
                pSourceDir = SourceDir
                pDestDir = DestinationDir

                Return CopyFiles()
            End Function

            Private Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String)
                Dim sDir As String
                Dim dDirInfo As IO.DirectoryInfo
                Dim sDirInfo As IO.DirectoryInfo
                Dim sFile As String
                Dim sFileInfo As IO.FileInfo
                Dim dFileInfo As IO.FileInfo

                ' Add trailing separators to the supplied paths if they don't exist.
                If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                    sourceDir &= System.IO.Path.DirectorySeparatorChar
                End If
                If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
                    destDir &= System.IO.Path.DirectorySeparatorChar
                End If

                'If destination directory does not exist, create it.
                dDirInfo = New System.IO.DirectoryInfo(destDir)
                If dDirInfo.Exists = False Then dDirInfo.Create()
                dDirInfo = Nothing


                ' Get a list of directories from the current parent.
                For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
                    sDirInfo = New System.IO.DirectoryInfo(sDir)
                    dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
                    ' Create the directory if it does not exist.
                    If dDirInfo.Exists = False Then dDirInfo.Create()
                    ' Since we are in recursive mode, copy the children also
                    RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName)
                    sDirInfo = Nothing
                    dDirInfo = Nothing
                Next

                ' Get the files from the current parent.
                For Each sFile In System.IO.Directory.GetFiles(sourceDir)
                    sFileInfo = New System.IO.FileInfo(sFile)
                    dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

                    If Not pSuppressCopyLogging Then
                        Logger.WriteMessageRelative("copying", 1 + pLogDepth)
                        Logger.WriteMessageRelative("source: " & sFileInfo.FullName, 2 + pLogDepth)
                        Logger.WriteMessageRelative("dest  : " & dFileInfo.FullName, 2 + pLogDepth)
                    End If


                    'If File does exists, backup.
                    If dFileInfo.Exists Then
                        If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("already exists", 3 + pLogDepth)

                        If pSkipBackup Then
                            If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("backup skipped", 4 + pLogDepth)
                        Else
                            If dFileInfo.IsReadOnly Then
                                If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("file is read-only, fixing...", 4 + pLogDepth)
                                dFileInfo.IsReadOnly = False
                            End If
                            If Not System.IO.Directory.Exists(pBackupFolder) Then
                                If Not pSuppressCopyLogging Then
                                    Logger.WriteMessageRelative("creating backup failed, backup folder not valid", 3 + pLogDepth)
                                End If
                            Else
                                If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("creating backup", 3 + pLogDepth)
                                If BackupFile(dFileInfo) Then

                                Else

                                End If
                            End If
                        End If
                    End If

                    Try
                        pFileCount_total += 1

                        sFileInfo.CopyTo(dFileInfo.FullName, True)
                        If IO.File.Exists(dFileInfo.FullName) Then
                            If Not pSuppressCopyLogging Then Logger.WriteMessageRelative("ok", 2 + pLogDepth)

                            pFileCount_copied += 1
                        Else
                            If Not pSuppressCopyLogging Then Logger.WriteErrorRelative("fail", 2 + pLogDepth)

                            pFileCount_failed += 1
                        End If
                    Catch ex As Exception
                        If pSuppressCopyLogging Then
                            Logger.WriteMessageRelative("copying", 1)
                            Logger.WriteMessageRelative("source: " & sFileInfo.FullName, 2 + pLogDepth)
                            Logger.WriteMessageRelative("dest  : " & dFileInfo.FullName, 2 + pLogDepth)
                        End If

                        Logger.WriteErrorRelative("fail", 3)
                        Logger.WriteErrorRelative("ex.Message=" & ex.Message, 4 + pLogDepth)

                        pFileCount_failed += 1
                    End Try

                    sFileInfo = Nothing
                    dFileInfo = Nothing
                Next
            End Sub

            Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
                Dim sPath As String, sName As String, sFullDest As String
                Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
                Dim bRet As Boolean = False

                Try
                    sName = oFile.Name

                    sPath = oFile.DirectoryName
                    sPath = sPath.Replace("C:", "")
                    sPath = sPath.Replace("c:", "")
                    sPath = sPath.Replace("\\", "\")
                    sPath = pBackupFolder & "\" & sPath

                    sFullDest = sPath & "\" & sName

                    oDirInfo = New System.IO.DirectoryInfo(sPath)
                    If Not oDirInfo.Exists Then oDirInfo.Create()

                    Logger.WriteMessageRelative("backup: " & sFullDest, 4 + pLogDepth)

                    oFile.CopyTo(sFullDest, True)

                    oFileInfo = New System.IO.FileInfo(sFullDest)
                    If oFileInfo.Exists Then
                        Logger.WriteMessageRelative("ok", 5 + pLogDepth)
                        bRet = True
                    Else
                        Logger.WriteErrorRelative("fail", 5 + pLogDepth)
                    End If
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 5 + pLogDepth)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 6 + pLogDepth)
                End Try

                oDirInfo = Nothing
                oFileInfo = Nothing

                Return bRet
            End Function
        End Class

        Public Class Backup
            Public Shared Function File(filePath As String, logLevel As Integer, prefix As String) As Boolean
                Return File(filePath, "", logLevel, prefix)
            End Function

            Public Shared Function File(filePath As String, logLevel As Integer) As Boolean
                Return File(filePath, "", logLevel, "")
            End Function

            Public Shared Function File(filePath As String, backupFolder As String) As Boolean
                Return File(filePath, backupFolder, 0, "")
            End Function

            Public Shared Function File(filePath As String, backupFolder As String, logLevel As Integer) As Boolean
                Return File(filePath, backupFolder, logLevel, "")
            End Function

            Public Shared Function File(filePath As String, backupFolder As String, logLevel As Integer, prefix As String) As Boolean
                Logger.WriteMessageRelative("writing backup", 1 + logLevel)
                Logger.WriteMessageRelative(filePath, 2 + logLevel)

                If Not backupFolder.Equals("") Then
                    If Not IO.Directory.Exists(backupFolder) Then
                        Logger.WriteErrorRelative("backup root folder does not exist", 3 + logLevel)

                        Return False
                    End If
                End If


                If Not IO.File.Exists(filePath) Then
                    Logger.WriteErrorRelative("does not exist", 3 + logLevel)

                    Return False
                End If


                Dim fI As IO.FileInfo = New System.IO.FileInfo(filePath)

                Return _BackupFile(fI, backupFolder, logLevel, prefix)
            End Function

            Private Shared Function _BackupFile(ByVal oFile As IO.FileInfo, backupFolder As String, logLevel As Integer, prefix As String) As Boolean
                Dim sPath As String, sName As String, sFullDest As String
                Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
                Dim bRet As Boolean = False

                Try
                    If backupFolder.Equals("") Then
                        If prefix.Equals("") Then
                            sName = IO.Path.GetFileNameWithoutExtension(oFile.Name) &
                            "_" & Date.Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") &
                            IO.Path.GetExtension(oFile.Name)
                        Else
                            sName = IO.Path.GetFileNameWithoutExtension(oFile.Name) &
                            "__" & prefix & "_" & Date.Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") &
                            IO.Path.GetExtension(oFile.Name)
                        End If

                        sPath = oFile.DirectoryName
                    Else
                        sName = oFile.Name

                        sPath = oFile.DirectoryName
                        sPath = sPath.Replace("C:", "")
                        sPath = sPath.Replace("c:", "")
                        sPath = sPath.Replace("\\", "\")
                        sPath = backupFolder & "\" & sPath
                    End If


                    sFullDest = IO.Path.Combine(sPath, sName)

                    Logger.WriteMessageRelative("backup: " & sFullDest, 3 + logLevel)


                    oDirInfo = New System.IO.DirectoryInfo(sPath)
                    If Not oDirInfo.Exists Then oDirInfo.Create()



                    oFile.CopyTo(sFullDest, True)

                    oFileInfo = New System.IO.FileInfo(sFullDest)
                    If oFileInfo.Exists Then
                        Logger.WriteMessageRelative("ok", 4 + logLevel)
                        bRet = True
                    Else
                        Logger.WriteErrorRelative("fail", 4 + logLevel)
                    End If
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 4 + logLevel)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 5 + logLevel)
                End Try

                oDirInfo = Nothing
                oFileInfo = Nothing

                Return bRet
            End Function
        End Class

        Public Class IniFile
#Region " Instance "
            ''' <summary>
            ''' Internal list of <see cref="Section"/>s in the file.
            ''' </summary>
            ''' <remarks></remarks>
            Private _sections As List(Of Section)

            ''' <summary>
            ''' Specifies whether the key name portion of the key is whitespace sensitive.
            ''' </summary>
            ''' <remarks></remarks>
            Private _nameWhitespaceLiteral As Boolean
            ''' <summary>
            ''' Specifies whether the value portion of the key is whitespace sensitive.
            ''' </summary>
            ''' <remarks></remarks>
            Private _valueWhitespaceLiteral As Boolean

            ''' <summary>
            ''' Token at the beginning of a line which denotes a comment. (';' character.)
            ''' </summary>
            ''' <remarks></remarks>
            Private Const COMMENT As String = ";"
            ''' <summary>
            ''' Token at the beginning of a line which denotes a <see cref="Section"/> name start. ('[' character.)
            ''' </summary>
            ''' <remarks></remarks>
            Private Const SECTION_BEGIN As Char = "["
            ''' <summary>
            ''' Token at the end of a line which denotes a <see cref="Section"/> name end. (']' character.)
            ''' </summary>
            ''' <remarks></remarks>
            Private Const SECTION_END As Char = "]"
            ''' <summary>
            ''' Token within a <see cref="Key"/> which defines the separation of the <see cref="Key.Name"/> and <see cref="Key.Value"/>. ('=' character.).
            ''' Only the first token encountered in a line is processed (so the <see cref="Key.Value"/> can contain this token in its text).
            ''' </summary>
            ''' <remarks></remarks>
            Private Const KEY_VALUE_SEPARATOR As Char = "="


            ''' <summary>
            ''' Describes an Ini file section.
            ''' </summary>
            ''' <remarks></remarks>
            Public Structure Section
                ''' <summary>
                ''' Section name defined between the <see cref="SECTION_BEGIN"/> and <see cref="SECTION_END"/> tokens.
                ''' </summary>
                ''' <remarks></remarks>
                Dim Name As String

                ''' <summary>
                ''' Text after the <see cref="COMMENT"/> token appearing immediately above the section name.
                ''' </summary>
                ''' <remarks></remarks>
                Dim Comments As String

                ''' <summary>
                ''' Sequential order in the file.
                ''' </summary>
                ''' <remarks></remarks>
                Dim Order As Integer

                ''' <summary>
                ''' <see cref="Key"/> data.
                ''' </summary>
                ''' <remarks></remarks>
                Dim Keys As List(Of Key)
            End Structure

            ''' <summary>
            ''' Describes an Ini file key.
            ''' </summary>
            ''' <remarks></remarks>
            Public Structure Key
                ''' <summary>
                ''' Key name.
                ''' </summary>
                ''' <remarks></remarks>
                Dim Name As String

                ''' <summary>
                ''' Text appearing after the <see cref="KEY_VALUE_SEPARATOR"/>.
                ''' </summary>
                ''' <remarks></remarks>
                Dim Value As String

                ''' <summary>
                ''' Text after the <see cref="COMMENT"/> token appearing immediately above the key.
                ''' </summary>
                ''' <remarks></remarks>
                Dim Comments As String

                ''' <summary>
                ''' Sequential order in the containing <see cref="Section"/>.
                ''' </summary>
                ''' <remarks></remarks>
                Dim Order As Integer
            End Structure

            ''' <summary>
            ''' Creates a new object with the read/write path set by <paramref name="fileName"/>.
            ''' </summary>
            ''' <param name="fileName">Target file name which will be read on instantiation and written to on save.
            ''' This file does not need to exist at instantiation time.</param>
            ''' <param name="keyNameWhitespaceLiteral">Sets the <see cref="KeyNameWhitespaceLiteral"/> property.</param>
            ''' <param name="keyValueWhitespaceLiteral">Sets the <see cref="KeyValueWhitespaceLiteral"/> property.</param>
            ''' <remarks></remarks>
            Public Sub New(fileName As String,
                   Optional ByVal keyNameWhitespaceLiteral As Boolean = False,
                   Optional ByVal keyValueWhitespaceLiteral As Boolean = False)

                Me.FileName = fileName

                _sections = New List(Of Section)
                _nameWhitespaceLiteral = keyNameWhitespaceLiteral
                _valueWhitespaceLiteral = keyValueWhitespaceLiteral

                If My.Computer.FileSystem.FileExists(fileName) Then
                    ReadFile()
                End If
            End Sub

#End Region


#Region " I/O "

            ''' <summary>
            ''' Returns the current object content as a single string.
            ''' </summary>
            ''' <param name="preserveOrders">Determines if the section and key order settings should be honored.
            ''' On large ini files, this may incur a performance hit.</param>
            ''' <returns>String containing the entire Ini contents.</returns>
            ''' <remarks>The output of this function is the same as what is written in <see cref="SaveFile"/>.</remarks>
            Public Function GetOutputString(Optional preserveOrders As Boolean = False) As String

                Return GetOutputString(_sections, preserveOrders, preserveOrders)

            End Function

            ''' <summary>
            ''' Helper method which generates the output content.
            ''' </summary>
            ''' <param name="sections">Sections to process for writing.</param>
            ''' <param name="preserveSectionOrders">Determines if the section order settings should be honored.</param>
            ''' <param name="preserveKeyOrders">Determines if the key order settings should be honored.</param>
            ''' <remarks></remarks>
            Private Function GetOutputString(sections As List(Of Section),
                                     preserveSectionOrders As Boolean,
                                     preserveKeyOrders As Boolean) As String

                If Not preserveSectionOrders Then
                    ' Order is not a consideration for sections, just dump the data.

                    Dim output As New System.Text.StringBuilder

                    For Each nameSection As Section In sections
                        output.Append(GetSectionOutput(nameSection,
                                               preserveOrders:=preserveKeyOrders))
                    Next

                    Return output.ToString

                Else
                    ' Order matters, sort the sections before outputing.
                    Dim sortedSections As New List(Of Section)

                    For Each nameSection As Section In _sections
                        Dim order As Integer = nameSection.Order

                        ' When there is nothing yet added or the order is the max integer value, append to the end.
                        If sortedSections.Count = 0 OrElse
                    order = Integer.MaxValue Then

                            sortedSections.Add(nameSection)

                        Else
                            ' Explicit order is provided.
                            ' Locate where it should go with respect to other sections.
                            Dim insertBefore As Section = sortedSections.Find(
                        Function(sorted) sorted.Order > order)

                            ' Check that the section found contains data.
                            If Not SectionHasData(insertBefore) Then
                                ' No lower order found, add to the end.
                                sortedSections.Add(nameSection)

                            Else
                                ' The current section will be inserted at the index where section with the
                                ' higher order value was found.
                                Dim insertAtIndex As Integer = sortedSections.IndexOf(insertBefore)

                                sortedSections.Insert(insertAtIndex, nameSection)
                            End If

                        End If
                    Next

                    ' Recursive call to produce the output.
                    Return GetOutputString(sortedSections, False, preserveKeyOrders)

                End If

            End Function


            ''' <summary>
            ''' Reads the current contents of <see cref="FileName"/> into the object.
            ''' </summary>
            ''' <remarks>
            ''' <para>Regarding comments and association with <see cref="Section"/>s or <see cref="Key"/>s:</para>
            ''' <para>All comments appearing immediately above a section (regardless of whitespace) will be associated with the respective section.
            ''' The same rule applies for <see cref="Key"/>s as well.</para>
            ''' <para>
            ''' The one exception is when comments appear at the end of a file where there is no section or key below it to assign to.
            ''' When this happens, a blank <see cref="Key"/> is generated with an empty <see cref="Key.Name"/> and <see cref="Key.Value"/> property, but will have the <see cref="Key.Comments"/> property containing the comments.</para>
            ''' <para>To avoid pulling this "placeholder" key, use the <see cref="GetAllKeysWithAName"/> method.</para>
            ''' </remarks>
            Public Sub ReadFile()

                ' Clear all existing data.
                _sections = New List(Of Section)

                Dim comments As New List(Of String)

                ' Create the default section.
                Dim currentSection As Section = IniFile.MakeSection("", order:=0)

                ' Detect presense of data within the section to be added.
                ' Return if the section was actually added.
                Dim _addSection = Function()
                                      If SectionHasData(currentSection) Then
                                          _sections.Add(currentSection)
                                          Return True
                                      End If
                                      Return False
                                  End Function

                Using iniFileText As IO.TextReader = My.Computer.FileSystem.OpenTextFileReader(FileName)
                    With iniFileText

                        ' Check for the end of the file.
                        While .Peek <> -1

                            ' Grab the next line.
                            Dim lineRaw As String = .ReadLine
                            Dim lineTrim As String = lineRaw.Trim

                            If lineTrim.StartsWith(COMMENT) Then
                                ' Remove the comment token and add the remaining text as a comment.
                                comments.Add(lineTrim.Substring(COMMENT.Length).Trim)


                                ' Section check.
                            ElseIf lineTrim.StartsWith(SECTION_BEGIN) AndAlso
                        lineTrim.EndsWith(SECTION_END) Then

                                ' If a section was previously being built then add it to the object collection.
                                If SectionHasData(currentSection) Then
                                    _sections.Add(currentSection)
                                End If
                                '_addSection()


                                ' Create a new section.
                                ' Take everything between the first and last char as the name.
                                currentSection = IniFile.MakeSection(
                            lineTrim.Substring(1, lineTrim.Length - 2),
                            comments:=String.Join(ControlChars.NewLine, comments.ToArray),
                            order:=_sections.Count)

                                ' Reset comments as new comments will apply to the next component.
                                comments = New List(Of String)


                                ' Key check.
                            ElseIf lineTrim.Contains(KEY_VALUE_SEPARATOR) Then

                                ' Grab the components into an array.
                                Dim components As String() = lineRaw.Split({KEY_VALUE_SEPARATOR}, 2)

                                Dim keyName As String = components(0)
                                Dim keyValue As String = components(1)

                                Dim key As Key = IniFile.MakeKey(
                            If(KeyNameWhitespaceLiteral, keyName, keyName.Trim),
                            If(KeyValueWhitespaceLiteral, keyValue, keyValue.Trim),
                            comments:=String.Join(ControlChars.NewLine, comments.ToArray),
                            order:=currentSection.Keys.Count)

                                ' Reset comments as new comments will apply to the next component.
                                comments = New List(Of String)

                                ' Add the new key to the current section being built.
                                currentSection.Keys.Add(key)


                            Else
                                ' Unknown line. Ignore it.
                            End If

                        End While

                        ' Check if there are comments which are not assigned.
                        If comments.Count > 0 Then
                            ' "Orphaned" comments. Create a blank key to hold them.
                            currentSection.Keys.Add(
                        MakeKey("", "",
                                comments:=String.Join(ControlChars.NewLine, comments.ToArray),
                                order:=currentSection.Keys.Count))
                        End If

                        ' Add the section which was being built at the time the file ended.
                        If SectionHasData(currentSection) Then
                            _sections.Add(currentSection)
                        End If
                        'addSection()

                    End With
                End Using

            End Sub

            ''' <summary>
            ''' Writes the current object content to <see cref="FileName"/>.
            ''' </summary>
            ''' <param name="preserveOrders">Determines if the section and key order settings should be honored.
            ''' On large ini files, this may incur a performance hit.</param>
            ''' <param name="reloadAfterSave">Reloads the ini file after the save completes.
            ''' This action essentially rebuilds the section and key ordering according to any changes.</param>
            ''' <exception cref="IO.FileNotFoundException">Thrown when the <see cref="FileName"/> property is empty.</exception>
            Public Sub SaveFile(Optional preserveOrders As Boolean = False,
                        Optional reloadAfterSave As Boolean = True)

                If FileName.Trim = "" Then
                    Throw New IO.FileNotFoundException("A file name is not assigned.")
                End If

                ' Write the output to the current file.
                My.Computer.FileSystem.WriteAllText(FileName, GetOutputString(_sections, preserveOrders, preserveOrders), False)

                If reloadAfterSave Then
                    ReadFile()
                End If

            End Sub

#End Region


#Region " Properties "

            ''' <summary>
            ''' The file path where the object will be read from and written to.
            ''' </summary>
            ''' <value>New full file path location.</value>
            ''' <returns>Full file path and name of the current object.</returns>
            ''' <remarks></remarks>
            Public Property FileName As String

            ''' <summary>
            ''' Raw section data stored by the object.
            ''' </summary>
            ''' <returns>Section data as stored internally.</returns>
            Public ReadOnly Property Sections As List(Of Section)
                Get
                    Return _sections
                End Get
            End Property

            ''' <summary>
            ''' Specifies if the parser includes leading and trailing whitespaces from the source in the <see cref="Key.Name"/> property.
            ''' </summary>
            ''' <returns>Boolean indicating leading and trailing whitespace sensitivity.</returns>
            ''' <remarks>This property only applies to the reading of the ini file.
            ''' It does not modify the output format.</remarks>
            Public ReadOnly Property KeyNameWhitespaceLiteral As Boolean
                Get
                    Return _nameWhitespaceLiteral
                End Get
            End Property

            ''' <summary>
            ''' Specifies if the parser includes leading and trailing whitespaces from the source in the <see cref="Key.Value"/> property.
            ''' </summary>
            ''' <returns>Boolean indicating leading and trailing whitespace sensitivity.</returns>
            ''' <remarks>This property only applies to the reading of the ini file.
            ''' It does not modify the output format.</remarks>
            Public ReadOnly Property KeyValueWhitespaceLiteral As Boolean
                Get
                    Return _valueWhitespaceLiteral
                End Get
            End Property

#End Region


#Region " Accessors "

            ''' <summary>
            ''' Returns the all the keys within <paramref name="sectionName"/> which have a non-empty <see cref="Key.Name"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <returns>Keys within the provided section which have a non-empty <see cref="Key.Name"/> property.</returns>
            Public Function GetAllKeysWithAName(sectionName As String) As Key()

                Dim keys As New List(Of Key)
                keys.AddRange(GetSectionKeys(sectionName))

                Return keys.FindAll(Function(x) x.Name <> "").ToArray

            End Function

            ''' <summary>
            ''' Returns the all the keys matching <paramref name="keyName"/> in the specified <paramref name="sectionName"/> which have a non-empty <see cref="Key.Value"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">If provided, only the keys matching the specified value are returned.
            ''' If not provided, all keys in the section are returned.</param>
            ''' <returns>Keys within the provided section which have a non-empty <see cref="Key.Value"/> property.</returns>
            Public Function GetAllKeysWithAValue(sectionName As String,
                                         Optional keyName As String = "") As Key()

                Dim keys As New List(Of Key)
                keys.AddRange(GetAllKeys(sectionName, keyName:=keyName))

                Return keys.FindAll(Function(x) x.Value <> "").ToArray

            End Function

            ''' <summary>
            ''' Returns the all the keys matching <paramref name="keyName"/> in the specified <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">If provided, only the keys matching the specified value are returned.
            ''' If not provided, all keys in the section are returned.</param>
            ''' <returns>Key data within the provided section.</returns>
            Public Function GetAllKeys(sectionName As String,
                               Optional keyName As String = "") As Key()

                If Not SectionNameExists(sectionName) Then
                    Return New Key() {}

                Else
                    ' Section exists, extract the keys.
                    Dim section As Section = GetSection(sectionName)

                    If keyName = "" Then
                        ' No name specified, return all.
                        Return section.Keys.ToArray

                    Else
                        ' Find all keys matching the provided name.
                        Return section.Keys.FindAll(
                    Function(checkKey) checkKey.Name = keyName).ToArray
                    End If
                End If

            End Function

            ''' <summary>
            ''' Returns the all the sections with the specified <paramref name="name"/> in the object.
            ''' </summary>
            ''' <param name="name">If provided, only the sections matching the specified value are returned.
            ''' If not provided, all sections are returned.</param>
            ''' <returns>Section data.</returns>
            Public Function GetAllSections(Optional name As String = "") As Section()

                If name = "" Then
                    ' No name specified, return all.
                    Return _sections.ToArray

                Else
                    ' Find all sections matching the provided name.
                    Return _sections.FindAll(
                Function(checkSection) checkSection.Name = name).ToArray
                End If

            End Function


            ''' <summary>
            ''' Returns the full key within the specified <paramref name="sectionName"/>. If the key does not exist, an empty <see cref="IniFile.Key"/> is returned.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">Name of the target key.</param>
            ''' <returns>Entire key of the first occurance of <paramref name="keyName"/> within <paramref name="sectionName"/>.</returns>
            ''' <remarks>In the event of multiple <paramref name="keyName"/>s within <paramref name="sectionName"/>, only the first occurrance is returned.</remarks>
            Public Function GetKey(sectionName As String, keyName As String) As Key

                If Not KeyNameExists(sectionName, keyName) Then
                    Return New Key

                Else
                    ' Return the first key with the matching name.
                    Return GetSection(sectionName).Keys.Find(
                Function(searchKey) searchKey.Name = keyName)
                End If

            End Function

            ''' <summary>
            ''' Returns the comments of <paramref name="keyName"/> within the specified <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">Name of the target key.</param>
            ''' <returns>Comments of the first occurance of the <paramref name="keyName"/>s within <paramref name="sectionName"/>.</returns>
            ''' <remarks>In the event of multiple <paramref name="keyName"/>s within <paramref name="sectionName"/>, only the comments of the first occurrance is returned.</remarks>
            Public Function GetKeyComments(sectionName As String, keyName As String) As String

                Dim key As Key = GetKey(sectionName, keyName)

                Return If(KeyHasData(key), key.Comments, "")

            End Function

            ''' <summary>
            ''' Gets the order of <paramref name="keyName"/> in the specified <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">Name of the target key.</param>
            ''' <returns>Order of the first occurance of the <paramref name="keyName"/>s within <paramref name="sectionName"/>.</returns>
            ''' <remarks>In the event of multiple <paramref name="keyName"/>s within <paramref name="sectionName"/>, only the order of the first occurrance is returned.</remarks>
            Public Function GetKeyOrder(sectionName As String, keyName As String) As Integer

                Dim key As Key = GetKey(sectionName, keyName)

                Return If(KeyHasData(key), key.Order, -1)

            End Function

            ''' <summary>
            ''' Gets the value of <paramref name="keyName"/> in the specified <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">Name of the target key.</param>
            ''' <param name="defaultValue">The value returned in the event the key does not exist.</param>
            ''' <returns>Value of the first occurance of the <paramref name="keyName"/>s within <paramref name="sectionName"/>.</returns>
            ''' <remarks>In the event of multiple <paramref name="keyName"/>s within <paramref name="sectionName"/>, only the value of the first occurrance is returned.</remarks>
            Public Function GetKeyValue(sectionName As String, keyName As String,
                                Optional defaultValue As String = "") As String

                Dim key As Key = GetKey(sectionName, keyName)

                Return If(KeyHasData(key), key.Value, defaultValue)

            End Function


            ''' <summary>
            ''' Returns the section with the specified <paramref name="name"/>. If the section does not exist, an empty <see cref="IniFile.Section"/> is returned.
            ''' </summary>
            ''' <param name="name">Section name to get.</param>
            ''' <returns>Entire section of the first occurance of the section with the specified <paramref name="name"/>.</returns>
            ''' <remarks>In the event of multiple sections with the specified <paramref name="name"/>, only the first occurrance is returned.</remarks>
            Public Function GetSection(name As String) As Section

                If Not SectionNameExists(name) Then
                    Return New Section

                Else
                    ' Return the first section with the matching name.
                    Return _sections.Find(
                Function(searchSection) searchSection.Name = name)
                End If

            End Function

            ''' <summary>
            ''' Returns the comments of the section with the specified <paramref name="name"/>.
            ''' </summary>
            ''' <param name="name">Name of the target section.</param>
            ''' <returns>Comments of the first occurance of the section with the specified <paramref name="name"/>.</returns>
            ''' <remarks>In the event of multiple sections with the specified <paramref name="name"/>, only the comments of the first occurrance is returned.</remarks>
            Public Function GetSectionComments(name As String) As String

                Dim section As Section = GetSection(name)

                Return If(SectionHasData(section), section.Comments, "")

            End Function

            ''' <summary>
            ''' Returns the order of the section with the specified <paramref name="name"/>.
            ''' </summary>
            ''' <param name="name">Name of the target section.</param>
            ''' <returns>Order of the first occurance of the section with the specified <paramref name="name"/>.</returns>
            ''' <remarks>In the event of multiple sections with the specified <paramref name="name"/>, only the order of the first occurrance is returned.</remarks>
            Public Function GetSectionOrder(name As String) As Integer

                Dim section As Section = GetSection(name)

                Return If(SectionHasData(section), section.Order, -1)

            End Function

            ''' <summary>
            ''' Returns the all the keys in the section with the specified <paramref name="name"/>.
            ''' </summary>
            ''' <param name="name">Name of the target section.</param>
            ''' <returns>Keys of the first occurance of the section with the specified <paramref name="name"/>.</returns>
            ''' <remarks>Alternate way to call <see cref="GetAllKeys"/>.</remarks>
            Public Function GetSectionKeys(name As String) As Key()

                Return GetAllKeys(name)

            End Function


            ''' <summary>
            ''' Returns the count of keys with the name <paramref name="keyName"/> appearing in <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">Target key name to count.</param>
            ''' <returns>Number of times <paramref name="keyName"/> appears in the first occurrance of section <paramref name="sectionName"/>.</returns>
            ''' <remarks>In the event of multiple sections named <paramref name="sectionName"/>, only the first occurance is searched.</remarks>
            Public Function KeyNameCount(sectionName As String, keyName As String) As Integer

                If Not KeyNameExists(sectionName, keyName) Then
                    Return 0

                Else
                    ' The key exists at least once.
                    ' Pull all matches of the key name and return the count.
                    Return GetSection(sectionName).Keys.FindAll(
                Function(searchKey) searchKey.Name = keyName).Count
                End If

            End Function

            ''' <summary>
            ''' Returns whether or not a key with the name <paramref name="keyName"/> exists in <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">Target key name to find.</param>
            ''' <returns>Whether a key with the specified <paramref name="keyName"/> exists within <paramref name="sectionName"/>.</returns>
            Public Function KeyNameExists(sectionName As String, keyName As String) As Boolean

                Dim section As Section = GetSection(sectionName)

                If Not SectionHasData(section) Then
                    Return False

                Else
                    ' Pull the first key with the matching name.
                    Dim key As Key = section.Keys.Find(
                Function(searchKey) searchKey.Name = keyName)

                    Return KeyHasData(key)
                End If

            End Function

            ''' <summary>
            ''' Returns the count of sections with the specified <paramref name="name"/>.
            ''' </summary>
            ''' <param name="name">Target section name to count.</param>
            ''' <returns>Number of sections with the specified <paramref name="name"/>.</returns>
            Public Function SectionNameCount(name As String) As Integer

                If Not SectionNameExists(name) Then
                    Return 0

                Else
                    ' The section exists at least once.
                    ' Pull all matches of the section name and return the count.
                    Return _sections.FindAll(
                Function(searchSection) searchSection.Name = name).Count
                End If
            End Function

            ''' <summary>
            ''' Returns whether or not a section with the specified <paramref name="name"/> exists.
            ''' </summary>
            ''' <param name="name">Target section name to find.</param>
            ''' <returns>Whether a section with the specified <paramref name="name"/> exists.</returns>
            Public Function SectionNameExists(name As String) As Boolean

                ' Pull the first section with the matching name.
                Dim section As Section = _sections.Find(
            Function(searchSection) searchSection.Name = name)

                Return SectionHasData(section)

            End Function

#End Region


#Region " Modifiers "

            ''' <summary>
            ''' Creates a new <see cref="IniFile.Key"/> with the passed in values and adds it to the specified <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section.</param>
            ''' <param name="keyName">Name of the new key.</param>
            ''' <param name="value">Value of the new key.</param>
            ''' <param name="comments">Comments for the new key.</param>
            ''' <param name="order">Order of the new key respective to the existing keys in the specified <paramref name="sectionName"/>.</param>
            ''' <returns>Whether or not the resulting <see cref="IniFile.Key"/> was successfully added.</returns>
            Public Function AddKey(sectionName As String, keyName As String, value As String,
                           Optional comments As String = "",
                           Optional order As Integer = Integer.MaxValue) As Boolean

                Return AddKey(sectionName,
                      MakeKey(keyName, value, comments:=comments, order:=order))

            End Function

            ''' <summary>
            ''' Adds the <paramref name="key"/> to the specified <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section.</param>
            ''' <param name="key">Full <see cref="IniFile.Key"/> to be added.</param>
            ''' <returns>Whether or not the <paramref name="key"/> was successfully added.</returns>
            Public Function AddKey(sectionName As String, key As Key) As Boolean

                If Not SectionNameExists(sectionName) Then
                    Return False
                Else
                    GetSection(sectionName).Keys.Add(key)
                    Return True
                End If

            End Function

            ''' <summary>
            ''' Creates a new <see cref="IniFile.Section"/> with the passed in values.
            ''' </summary>
            ''' <param name="name">Name of the new section.</param>
            ''' <param name="comments">Comments for the new section.</param>
            ''' <param name="order">Order of the new section respective to the existing sections.</param>
            ''' <returns>Whether or not the resulting <see cref="IniFile.Section"/> was successfully added.</returns>
            Public Function AddSection(name As String,
                               Optional comments As String = "",
                               Optional order As Integer = Integer.MaxValue) As Boolean

                Return AddSection(MakeSection(name, comments:=comments, order:=order))

            End Function

            ''' <summary>
            ''' Adds the <paramref name="section"/> to the object.
            ''' </summary>
            ''' <param name="section">Full <see cref="IniFile.Section"/> to be added.</param>
            ''' <returns>Whether or not the <paramref name="section"/> was successfully added.</returns>
            Public Function AddSection(section As Section) As Boolean

                _sections.Add(section)
                Return True

            End Function


            ''' <summary>
            ''' Deletes the key with the name <paramref name="keyName"/> from the specified <paramref name="sectionName"/>.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">Name of the target key to delete.</param>
            ''' <returns>Whether or not the <paramref name="keyName"/> was deleted.</returns>
            ''' <remarks>In the event of multiple <paramref name="keyName"/>s within <paramref name="sectionName"/>, only the first occurrance is deleted.</remarks>
            Public Function DeleteKey(sectionName As String, keyName As String) As Boolean

                If Not KeyNameExists(sectionName, keyName) Then
                    Return False
                Else
                    GetSection(sectionName).Keys.Remove(GetKey(sectionName, keyName))
                    Return True
                End If

            End Function

            ''' <summary>
            ''' Deletes the section with the specified <paramref name="name"/>.
            ''' </summary>
            ''' <param name="name">Name of the target section to delete.</param>
            ''' <returns>Whether or not the section was deleted.</returns>
            ''' <remarks>In the event of multiple sections matching <paramref name="name"/>, only the first occurrance is deleted.</remarks>
            Public Function DeleteSection(name As String) As Boolean

                If Not SectionNameExists(name) Then
                    Return False
                Else
                    _sections.Remove(GetSection(name))
                    Return True
                End If

            End Function

            ''' <summary>
            ''' Deletes all keys from the section with the specified <paramref name="name"/>.
            ''' </summary>
            ''' <param name="name">Name of the target section to delete the keys from.</param>
            ''' <returns>Whether or not the keys were deleted.</returns>
            ''' <remarks>In the event of multiple sections matching <paramref name="name"/>, only the keys from the first occurrance are deleted.</remarks>
            Public Function DeleteSectionKeys(name As String) As Boolean

                If SectionNameExists(name) Then
                    Return False
                Else
                    GetSection(name).Keys.Clear()
                    Return True
                End If

            End Function


            ''' <summary>
            ''' Sets the data of the key with <paramref name="keyName"/> within the specified <see cref="Section"/>.
            ''' Optionally, this method will create the section and/or key as needed.
            ''' </summary>
            ''' <param name="sectionName">Parent section to search.</param>
            ''' <param name="keyName">Name of the target key.</param>
            ''' <param name="value">Value to set.</param>
            ''' <param name="order">Order to set.</param>
            ''' <param name="comments">Comments to set.</param>
            ''' <param name="createIfNotExist">When True, if the target path of <paramref name="sectionName"/> and/or <paramref name="keyName"/> does not exist, it is created as needed.
            ''' When False, if the path does not exist, no action is taken.</param>
            ''' <returns>Whether the specified key was updated or created.
            ''' False should only be returned in calls where <paramref name="createIfNotExist"/> is set to False.</returns>
            ''' <remarks>In the event of multiple <paramref name="keyName"/>s within <paramref name="sectionName"/>, only the first occurrance is updated.</remarks>
            Public Function SetKeyData(sectionName As String, keyName As String,
                               Optional value As String = Nothing,
                               Optional order As Integer = Integer.MaxValue,
                               Optional comments As String = Nothing,
                               Optional createIfNotExist As Boolean = True) As Boolean

                Dim section As Section

                If Not KeyNameExists(sectionName, keyName) Then

                    ' Check if the key should be created.
                    If Not createIfNotExist Then
                        Return False

                    Else
                        section = GetSection(sectionName)

                        ' Check if the section pulled is valid.
                        If Not SectionHasData(section) Then
                            ' Section does not exist, create it.
                            section = MakeSection(sectionName)

                            _sections.Add(section)
                        End If

                        ' Create the key.
                        section.Keys.Add(MakeKey(keyName,
                                         If(value IsNot Nothing, value, ""),
                                         comments:=If(comments IsNot Nothing, comments, ""),
                                         order:=order))

                        Return True
                    End If

                Else
                    section = GetSection(sectionName)
                    Dim currentKey As Key = GetKey(sectionName, keyName)

                    Dim newKey As Key = MakeKey(
                keyName,
                If(value IsNot Nothing, value, currentKey.Value),
                comments:=If(comments IsNot Nothing, comments, currentKey.Comments),
                order:=If(order = Integer.MaxValue, currentKey.Order, order))


                    Dim currentKeyIndex As Integer = section.Keys.IndexOf(currentKey)

                    ' Add the new key in the index of the existing key.
                    section.Keys.Insert(currentKeyIndex, newKey)

                    ' The old key is now one position higher.
                    section.Keys.RemoveAt(currentKeyIndex + 1)

                    Return True
                End If

            End Function

            ''' <summary>
            ''' Sets the comments of the section with the specified <paramref name="name"/>.
            ''' </summary>
            ''' <param name="name">Name of the target section.</param>
            ''' <param name="comments">Comments to set.</param>
            ''' <param name="order">Order to set.</param>
            ''' <param name="keys">Collection of full keys to set.</param>
            ''' <param name="createIfNotExist">When True, if the target section with the specified <paramref name="name"/> does not exist, it is created.
            ''' When False, if the section does not exist, no action is taken.</param>
            ''' <returns>Whether the specified section was updated or created.
            ''' False should only be returned in calls where <paramref name="createIfNotExist"/> is set to False.</returns>
            ''' <remarks>In the event of multiple sections with the same <paramref name="name"/>, only the first occurrance is updated.</remarks>
            Public Function SetSectionData(name As String,
                                   Optional order As Integer = Integer.MaxValue,
                                   Optional comments As String = Nothing,
                                   Optional keys As List(Of Key) = Nothing,
                                   Optional createIfNotExist As Boolean = True) As Boolean

                If Not SectionNameExists(name) Then

                    ' Check if the section should be created.
                    If Not createIfNotExist Then
                        Return False

                    Else
                        ' Create the section.
                        _sections.Add(MakeSection(name,
                                          comments:=If(comments IsNot Nothing, comments, ""),
                                          order:=order,
                                          keys:=keys))

                        Return True
                    End If

                Else
                    Dim currentSection As Section = GetSection(name)

                    Dim newSection As Section = MakeSection(
                name,
                comments:=If(comments IsNot Nothing, comments, currentSection.Comments),
                order:=If(order = Integer.MaxValue, currentSection.Order, order),
                keys:=If(keys IsNot Nothing, keys, currentSection.Keys))


                    Dim currentSectionIndex As Integer = _sections.IndexOf(currentSection)

                    ' Add the new section in the index of the existing key.
                    _sections.Insert(currentSectionIndex, newSection)

                    ' The old section is now one position higher.
                    _sections.RemoveAt(currentSectionIndex + 1)

                    Return True
                End If

            End Function

#End Region


#Region " Shared "

            ''' <summary>
            ''' Creates a new object from the provided text <paramref name="contents"/>.
            ''' </summary>
            ''' <param name="contents">Ini text.</param>
            ''' <param name="fileName">Destination file for the new object. If this file exists, it will be overwritten.
            ''' If a value is not provided the returned object will not contain a value in <see cref="FileName"/> property (it can be set if needed, however).</param>
            ''' <returns>New object with the <paramref name="contents"/> assigned.</returns>
            ''' <remarks></remarks>
            Public Shared Function CreateFromString(contents As String, Optional fileName As String = "") As IniFile

                Dim isTempFile As Boolean = False

                ' Write the contents to a temp file if data is present.
                If Not String.IsNullOrEmpty(contents) Then
                    If fileName.Trim = "" Then
                        ' Use a file in the temp directory.
                        fileName = My.Computer.FileSystem.GetTempFileName
                        isTempFile = True
                    End If

                    My.Computer.FileSystem.WriteAllText(fileName, contents, False)
                End If

                ' Return the new object.
                Dim ini As New IniFile(fileName)

                ' Remove temp files and reset the file name of the current object.
                If isTempFile AndAlso My.Computer.FileSystem.FileExists(fileName) Then
                    My.Computer.FileSystem.DeleteFile(fileName)
                    ini.FileName = ""
                End If

                Return ini

            End Function

            ''' <summary>
            ''' Gets the value of <paramref name="keyName"/> in the specified <paramref name="section"/>.
            ''' </summary>
            ''' <param name="section">Section to search.</param>
            ''' <param name="keyName">Name of the target key.</param>
            ''' <param name="defaultValue">The value returned in the event the key does not exist.</param>
            ''' <returns>Value of the first occurance of the <paramref name="keyName"/>s within <paramref name="section"/>.</returns>
            ''' <remarks>In the event of multiple <paramref name="keyName"/>s within <paramref name="section"/>, only the value of the first occurrance is returned.</remarks>
            Public Shared Function GetKeyValue(section As Section, keyName As String,
                                       Optional defaultValue As String = "") As String

                Dim key As Key = section.Keys.Find(
            Function(searchKey) searchKey.Name = keyName)

                Return If(KeyHasData(key), key.Value, defaultValue)

            End Function


            ''' <summary>
            ''' Gets the comment output for writing to a file.
            ''' </summary>
            ''' <param name="comment">The comment for any component.</param>
            ''' <returns></returns>
            Public Shared Function GetCommentOutput(comment As String) As String

                Dim commentPrefix As String = IniFile.COMMENT & " "
                comment = comment.Trim

                ' If there is a comment value supplied, append the token to be beginning of each line and return.
                ' For empty/whitespace only comments, return an empty string.
                Return If(comment = "", "",
                  commentPrefix & comment.Replace(ControlChars.NewLine, ControlChars.NewLine & commentPrefix))

            End Function

            ''' <summary>
            ''' Gets the <see cref="Key"/> output which is written to the ini file.
            ''' </summary>
            ''' <param name="key">The key to get output for.</param>
            ''' <returns>Format:
            ''' {Comment}
            ''' {Name}={Value}
            ''' </returns>
            Public Shared Function GetKeyOutput(key As Key) As String

                ' For readability, add line breaks before any keys with comments.
                Dim commentsSpacer As String = If(key.Comments.Trim = "", "",
                                          Space(1).Replace(" ", ControlChars.NewLine))

                If KeyHasData(key) Then
                    Return commentsSpacer &
                String.Format("{3}{0}{1}={2}",
                              ControlChars.NewLine,
                              key.Name, key.Value, GetCommentOutput(key.Comments)).Trim

                    ' "Orphaned" key condition.
                ElseIf key.Comments.Trim <> "" Then
                    Return commentsSpacer & GetCommentOutput(key.Comments).Trim

                    ' Empty key.
                Else
                    Return ""
                End If

            End Function


            ''' <summary>
            ''' Gets the <see cref="Section"/> output which is written to the ini file.
            ''' </summary>
            ''' <param name="section">The section to get output for.</param>
            ''' <param name="preserveOrders">Whether or not the ordering of the keys should be honored.
            ''' For large sections, this may incur a peformance hit.</param>
            ''' <returns>Format:
            ''' {Comment}
            ''' [{Section}]
            ''' {Key1}
            ''' {...}
            ''' {KeyN}
            ''' {carriage return}
            ''' {carriage return}
            ''' </returns>
            ''' <remarks>Using LINQ would make the key sorting easier.</remarks>
            Public Shared Function GetSectionOutput(section As Section,
                                            Optional preserveOrders As Boolean = False) As String

                If Not preserveOrders Then
                    ' Order is not a consideration, just dump the data.

                    ' Section header.
                    Dim output As New System.Text.StringBuilder(
                String.Format("{2}{0}[{1}]",
                              ControlChars.NewLine,
                              section.Name, GetCommentOutput(section.Comments)).Trim)
                    output.AppendLine("")

                    For Each nameKey As Key In section.Keys
                        ' Each key on its own line.
                        output.AppendLine(GetKeyOutput(nameKey))
                    Next

                    ' Add blank lines to the end.
                    output.Append(String.Format("{0}{0}", ControlChars.NewLine))

                    Return output.ToString


                Else
                    ' Order matters, sort the keys before outputing.
                    Dim sortedKeys As New List(Of Key)

                    For Each nameKey As Key In section.Keys
                        Dim order As Integer = nameKey.Order

                        ' When there is nothing yet added or the order is the max integer value, append to the end.
                        If sortedKeys.Count = 0 OrElse
                    order = Integer.MaxValue Then

                            sortedKeys.Add(nameKey)

                        Else
                            ' Explicit order is provided.
                            ' Locate where it should go with respect to other keys.
                            Dim insertBefore As Key = sortedKeys.Find(
                        Function(sorted) sorted.Order > order)

                            ' Check that the key found contains data.
                            If Not KeyHasData(insertBefore) Then
                                ' No lower order found, add to the end.
                                sortedKeys.Add(nameKey)

                            Else
                                ' The current key will be inserted at the index where key with the
                                ' higher order value was found.
                                Dim insertAtIndex As Integer = sortedKeys.IndexOf(insertBefore)

                                sortedKeys.Insert(insertAtIndex, nameKey)
                            End If

                        End If
                    Next


                    ' Wrap the keys in a new section so a recursive call can produce the output.
                    Dim sectionOutput As Section = MakeSection(section.Name, section.Comments,
                                                       keys:=sortedKeys)

                    Return GetSectionOutput(sectionOutput)

                End If

            End Function

            ''' <summary>
            ''' Creates a new unattached <see cref="IniFile.Section"/>.
            ''' </summary>
            ''' <param name="name">Name of the section.</param>
            ''' <param name="comments">Comments for the section.</param>
            ''' <param name="order">Sequential placement with respect to existing sections.</param>
            ''' <param name="keys">Keys for the section.</param>
            ''' <returns>New section with the specified properties assigned.</returns>
            ''' <remarks></remarks>
            Public Shared Function MakeSection(name As String,
                                       Optional comments As String = "",
                                       Optional order As Integer = Integer.MaxValue,
                                       Optional keys As List(Of Key) = Nothing) As Section

                If keys Is Nothing Then
                    keys = New List(Of Key)
                End If

                Return New Section With {.Name = name,
                                 .Comments = comments,
                                 .Order = order,
                                 .Keys = keys}
            End Function

            ''' <summary>
            ''' Creates a new unattached <see cref="IniFile.Key"/>.
            ''' </summary>
            ''' <param name="name">Name of the key.</param>
            ''' <param name="value">Value assigned to the key.</param>
            ''' <param name="comments">Comments for the key.</param>
            ''' <param name="order">Sequential placement with respect to existing keys.</param>
            ''' <returns>New key with the specified properties assigned.</returns>
            ''' <remarks></remarks>
            Public Shared Function MakeKey(name As String, value As String,
                                   Optional comments As String = "",
                                   Optional order As Integer = Integer.MaxValue) As Key

                Return New Key With {.Name = name,
                             .Value = value,
                             .Comments = comments,
                             .Order = order}
            End Function


            ''' <summary>
            ''' Returns whether or not the supplied <paramref name="key"/> contains data.
            ''' </summary>
            ''' <param name="key">Key to evaluate.</param>
            ''' <returns>True when the <paramref name="key"/> has a value for the <see cref="IniFile.Key.Name"/> property, otherwise False.</returns>
            Public Shared Function KeyHasData(key As Key) As Boolean

                Return Not String.IsNullOrEmpty(key.Name)

            End Function

            ''' <summary>
            ''' Returns whether or not the supplied <paramref name="section"/> contains data.
            ''' </summary>
            ''' <param name="section">Key to evaluate.</param>
            ''' <returns><c>True</c> when the <paramref name="section"/> has a value for one of the following:
            ''' <list type="bullet">
            ''' <item><see cref="IniFile.Section.Name"/></item>
            ''' <item><see cref="IniFile.Section.Keys"/> (1 or more)</item>
            ''' <item><see cref="IniFile.Section.Comments"/></item>
            ''' </list>
            ''' Otherwise <c>False</c>.</returns>
            Public Shared Function SectionHasData(section As Section) As Boolean

                Return Not String.IsNullOrEmpty(section.Name) OrElse
            (section.Keys IsNot Nothing AndAlso section.Keys.Count > 0) OrElse
            Not String.IsNullOrEmpty(section.Comments)

            End Function

#End Region


        End Class

    End Class
#End Region

#Region "Crypto"
    Public Class Crypto
        Public Class SHA512
            Private Shared ReadOnly _sha1 As Cryptography.SHA512 = Cryptography.SHA512.Create()

            Public Shared Function GetHash(source As String) As String
                Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
                Dim sb As New System.Text.StringBuilder()

                Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

                Return sb.ToString()
            End Function

            Public Shared Function VerifyHash(source As String, hash As String) As Boolean
                Dim sourceHash = GetHash(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function

            Public Shared Function GetHashBase64(source As String) As String
                Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

                Return Convert.ToBase64String(data)
            End Function

            Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
                Dim sourceHash = GetHashBase64(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function
        End Class

        Public Class MD5
            Private Shared ReadOnly _md5 As Cryptography.MD5 = Cryptography.MD5.Create()

            Public Shared Function GetHash(source As String) As String
                Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
                Dim sb As New System.Text.StringBuilder()

                Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

                Return sb.ToString()
            End Function

            Public Shared Function VerifyHash(source As String, hash As String) As Boolean
                Dim sourceHash = GetHash(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function

            Public Shared Function GetHashBase64(source As String) As String
                Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

                Return Convert.ToBase64String(data)
            End Function

            Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
                Dim sourceHash = GetHashBase64(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function
        End Class
    End Class
#End Region

#Region "prerequisites"
    Public Class Prerequisites
        Public Class DomainOrWorkgroup
            Public Shared Function IsOnDomain() As Boolean
                Try
                    Return Not Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.Equals(String.Empty)
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 1)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 2)

                    Return False
                End Try
            End Function

            Public Shared Function GetDomainName() As String
                Dim sDomain As String = ""

                Try
                    sDomain = Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName
                Catch ex As Exception
                    Logger.WriteErrorRelative("fail", 1)
                    Logger.WriteErrorRelative("ex.Message=" & ex.Message, 2)
                End Try

                Return sDomain
            End Function
        End Class

        Public Class IE
            Public Enum IEVersions As Integer 'useless ENUM
                NONE = 0
                IE5 = 5
                IE6 = 6
                IE7 = 7
                IE8 = 8
                IE9 = 9
                IE10 = 10
                IE11 = 11
                IE12 = 12
            End Enum

            Public Shared Function IsRunningVersion(version As IEVersions) As Boolean
                Return (version <= _GetIEFromRegistry())
            End Function

            Public Shared Function Version() As String
                Dim sTmp As String = ""

                Try
                    Using ieKey As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Internet Explorer\")
                        If ieKey IsNot Nothing AndAlso ieKey.GetValue("svcVersion") IsNot Nothing Then
                            sTmp = DirectCast(ieKey.GetValue("svcVersion", ""), String)
                        End If
                    End Using
                Catch ex As Exception
                    sTmp = ex.Message
                End Try

                Return sTmp
            End Function

            Private Shared Function _GetIEFromRegistry() As IEVersions
                Dim iVersion As Integer = IEVersions.NONE

                Try
                    Using ieKey As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Internet Explorer\")
                        If ieKey IsNot Nothing AndAlso ieKey.GetValue("svcVersion") IsNot Nothing Then
                            Dim sTmp As String = DirectCast(ieKey.GetValue("svcVersion", ""), String)

                            Try
                                iVersion = CInt(sTmp.Substring(0, sTmp.IndexOf(".")))
                            Catch ex As Exception

                            End Try
                        End If
                    End Using
                Catch ex As Exception

                End Try

                Return iVersion
            End Function
        End Class

        Public Class DotNet
            Public Enum DotNetVersions As Integer
                NONE = 0
                v2 = 1
                v3 = 2
                v4 = 4
                v4_5 = 8
                v4_5_1 = 16
                v4_5_2 = 32
                v4_6 = 64
                v4_6_1 = 128
                v4_6_2 = 256
            End Enum

            Public Shared Function IsRunningVersion(version As DotNetVersions) As Boolean
                Dim fullVersion As Integer = GetVersion()

                Return (version <= fullVersion)
            End Function

            Public Shared Function GetVersion() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                iVersion = iVersion Or _Get4orLowerFromRegistry()
                iVersion = iVersion Or _Get45or451FromRegistry()

                Return iVersion
            End Function

            Private Shared Function _Get4orLowerFromRegistry() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                Using ndpKey As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\")
                    For Each versionKeyName As String In ndpKey.GetSubKeyNames()
                        Try
                            Dim versionKey As RegistryKey = ndpKey.OpenSubKey(versionKeyName)
                            Dim name As String = DirectCast(versionKey.GetValue("Version", ""), String)
                            Dim sp As String = versionKey.GetValue("SP", "").ToString()
                            Dim install As String = versionKey.GetValue("Install", "").ToString()

                            If sp <> "" AndAlso install = "1" Then
                                Select Case True
                                    Case versionKeyName.StartsWith("v2")
                                        iVersion = iVersion Or DotNetVersions.v2
                                    Case versionKeyName.StartsWith("v3")
                                        iVersion = iVersion Or DotNetVersions.v3
                                    Case versionKeyName.StartsWith("v4.0")
                                        iVersion = iVersion Or DotNetVersions.v4
                                End Select
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                End Using

                Return iVersion
            End Function

            Private Shared Function _Get45or451FromRegistry() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                Try
                    Using ndpKey As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\")
                        If ndpKey IsNot Nothing AndAlso ndpKey.GetValue("Release") IsNot Nothing Then
                            iVersion = _CheckFor45DotVersion(CInt(ndpKey.GetValue("Release")))
                        End If
                    End Using
                Catch ex As Exception

                End Try

                Return iVersion
            End Function

            Private Shared Function _CheckFor45DotVersion(releaseKey As Integer) As Integer
                If releaseKey >= 394747 Then
                    Return DotNetVersions.v4_6_2
                End If
                If releaseKey >= 394254 Then
                    Return DotNetVersions.v4_6_1
                End If
                If releaseKey >= 393295 Then
                    Return DotNetVersions.v4_6
                End If
                If releaseKey >= 379893 Then
                    Return DotNetVersions.v4_5_2
                End If
                If releaseKey >= 378675 Then
                    Return DotNetVersions.v4_5_1
                End If
                If releaseKey >= 378389 Then
                    Return DotNetVersions.v4_5
                End If

                ' This line should never execute. A non-null release key should mean
                ' that 4.5 or later is installed.
                Return DotNetVersions.NONE
            End Function
        End Class
    End Class
#End Region

#Region "Windows"
    Public Class Windows
        Public Class Programs
            Public Class ProductGUID
                Public Shared Function GetUninstallIdentifier(ByVal sSearchForApplication As String) As String
                    Dim KeyName As String
                    Dim Value_DisplayName As String
                    Dim Value_UninstallString As String = ""
                    Dim UninstallRegKey As Microsoft.Win32.RegistryKey
                    Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

                    UninstallRegKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", False)

                    If sSearchForApplication = "" Then
                        'Yes that's right
                        sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
                    End If

                    sSearchForApplication = sSearchForApplication.ToLower

                    For Each KeyName In UninstallRegKey.GetSubKeyNames
                        ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

                        Try
                            Value_DisplayName = ApplicationRegKey.GetValue("DisplayName", "").ToString.ToLower
                        Catch ex As Exception
                            Value_DisplayName = ""
                        End Try

                        If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                            Try
                                Value_UninstallString = ApplicationRegKey.GetValue("UninstallString", "").ToString
                            Catch ex As Exception
                                Value_UninstallString = ""
                            End Try

                            Value_UninstallString = System.Text.RegularExpressions.Regex.Replace(Value_UninstallString, ".*\{(.+)\}.*", "$1")

                            Exit For
                        End If
                    Next

                    Return Value_UninstallString
                End Function

                Public Shared Function GetProductIdentifier(ByVal sSearchForApplication As String) As String
                    Dim KeyName As String
                    Dim Value_DisplayName As String
                    Dim Value_UninstallString As String = ""
                    Dim UninstallRegKey As Microsoft.Win32.RegistryKey
                    Dim ApplicationRegKey As Microsoft.Win32.RegistryKey

                    UninstallRegKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Installer\Products", False)

                    If sSearchForApplication = "" Then
                        'Yes that's right
                        sSearchForApplication = ")(*&$(B%V&(N#E$(^V&M&(#*$M&^(V&$^VM$&^^(#&^(#MV(&#(M^"
                    End If

                    sSearchForApplication = sSearchForApplication.ToLower

                    For Each KeyName In UninstallRegKey.GetSubKeyNames
                        ApplicationRegKey = UninstallRegKey.OpenSubKey(KeyName)

                        Try
                            Value_DisplayName = ApplicationRegKey.GetValue("ProductName", "").ToString.ToLower
                        Catch ex As Exception
                            Value_DisplayName = ""
                        End Try

                        If Value_DisplayName.IndexOf(sSearchForApplication) > -1 Then
                            Try
                                Value_UninstallString = KeyName
                            Catch ex As Exception
                                Value_UninstallString = ""
                            End Try

                            Exit For
                        End If
                    Next

                    Return Value_UninstallString
                End Function

            End Class
        End Class

        Public Shared Function IsRunningAsAdmin() As Boolean
            Dim bTmp As Boolean

            Try
                Dim user As WindowsIdentity = WindowsIdentity.GetCurrent()
                Dim principal As New WindowsPrincipal(user)

                bTmp = principal.IsInRole(WindowsBuiltInRole.Administrator)
            Catch ex As UnauthorizedAccessException
                bTmp = False
            Catch ex As Exception
                bTmp = False
            End Try

            Return bTmp
        End Function

        Public Class TaskBar
            Public Shared Sub Show()
                _TaskBar(True)
            End Sub

            Public Shared Sub Hide()
                _TaskBar(False)
            End Sub

            Private Declare Auto Function ShowWindow Lib "user32.dll" (ByVal hWnd As Long, ByVal nCmdShow As Long) As Long
            Private Declare Auto Function FindWindow Lib "user32.dll" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
            Private Declare Auto Function EnableWindow Lib "user32.dll" (ByVal hWnd As Long, ByVal fEnable As Long) As Long

            Private Const SW_HIDE = 0
            Private Const SW_SHOW = 5

            Private Shared Sub _TaskBar(Visible As Boolean)
                Dim hWnd As Long

                hWnd = FindWindow("Shell_TrayWnd", "")
                If Visible Then
                    ShowWindow(hWnd, SW_SHOW)
                Else
                    ShowWindow(hWnd, SW_HIDE)
                End If

                EnableWindow(hWnd, Visible)
            End Sub
        End Class

        Public Class RegisteredFileType
            Public Class Icon
                Public Shared Function FromFilePath(filePath As String) As System.Drawing.Icon
                    Dim result As System.Drawing.Icon = Nothing


                    Try
                        result = System.Drawing.Icon.ExtractAssociatedIcon(filePath)
                    Catch
                        Try
                            result = New Drawing.Icon(My.Resources.ResourceManager.GetStream("ico_unknownfile"))
                        Catch ex As Exception
                            ''# swallow and return nothing. You could supply a default Icon here as well
                        End Try
                    End Try

                    Return result
                End Function
            End Class

            Public Class Application
                <DllImport("Shlwapi.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
                Private Shared Function AssocQueryString(ByVal flags As UInteger, ByVal str As UInteger, ByVal pszAssoc As String, ByVal pszExtra As String, ByVal pszOut As System.Text.StringBuilder, ByRef pcchOut As UInteger) As UInteger
                End Function

                Enum AssocF As UInteger
                    None = 0
                    Init_NoRemapCLSID = &H1
                    Init_ByExeName = &H2
                    Open_ByExeName = &H2
                    Init_DefaultToStar = &H4
                    Init_DefaultToFolder = &H8
                    NoUserSettings = &H10
                    NoTruncate = &H20
                    Verify = &H40
                    RemapRunDll = &H80
                    NoFixUps = &H100
                    IgnoreBaseClass = &H200
                    Init_IgnoreUnknown = &H400
                    Init_FixedProgId = &H800
                    IsProtocol = &H1000
                    InitForFile = &H2000
                End Enum

                Enum AssocStr
                    Command = 1
                    Executable
                    FriendlyDocName
                    FriendlyAppName
                    NoOpen
                    ShellNewValue
                    DDECommand
                    DDEIfExec
                    DDEApplication
                    DDETopic
                    InfoTip
                    QuickTip
                    TileInfo
                    ContentType
                    DefaultIcon
                    ShellExtension
                    DropTarget
                    DelegateExecute
                    SupportedUriProtocols
                    Max
                End Enum

                Private Shared Function AssocQueryString(association As AssocStr, extension As String) As String
                    Const S_OK As Integer = 0
                    Const S_FALSE As Integer = 1

                    Dim length As UInteger = 0
                    Dim ret As UInteger = AssocQueryString(AssocF.None, association, extension, Nothing, Nothing, length)
                    If ret <> S_FALSE Then
                        Throw New InvalidOperationException("Could not determine associated string (1 ; {" & ret.ToString() & "})")
                    End If

                    Dim sb = New System.Text.StringBuilder(CInt(length))
                    ' (length-1) will probably work too as the marshaller adds null termination
                    ret = AssocQueryString(AssocF.None, association, extension, Nothing, sb, length)
                    If ret <> S_OK Then
                        Throw New InvalidOperationException("Could not determine associated string (2 ; {" & ret.ToString() & "})")
                    End If

                    Return sb.ToString()
                End Function

                Public Shared Function HasAssociatedApp(extension As String) As Boolean
                    Dim _s As String = ""

                    If GetAnything(extension, AssocStr.SupportedUriProtocols, _s) Then
                        Return Not _s.StartsWith("*")
                    Else
                        Return False
                    End If
                End Function

                Public Shared Function GetAppName(extension As String, ByRef appname As String) As Boolean
                    Try
                        appname = AssocQueryString(AssocStr.FriendlyAppName, extension)

                        Return True
                    Catch ex As Exception
                        appname = ex.Message
                    End Try

                    Return False
                End Function

                Public Shared Function GetExecutable(extension As String, ByRef application As String) As Boolean
                    Try
                        application = AssocQueryString(AssocStr.Executable, extension)

                        Return True
                    Catch ex As Exception
                        application = ex.Message
                    End Try

                    Return False
                End Function

                Public Shared Function GetName(extension As String, ByRef name As String) As Boolean
                    Try
                        name = AssocQueryString(AssocStr.FriendlyAppName, extension)

                        Return True
                    Catch ex As Exception
                        name = ex.Message
                    End Try

                    Return False
                End Function

                Public Shared Function GetAnything(extension As String, _AssocStr As AssocStr, ByRef _return_string As String) As Boolean
                    Try
                        _return_string = AssocQueryString(_AssocStr, extension)

                        Return True
                    Catch ex As Exception
                        _return_string = ex.Message
                    End Try

                    Return False
                End Function


            End Class
        End Class

        Public Class Startup
            Private Shared ReadOnly StartupApproved_enabled_bytes As Byte() = {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
            Private Shared ReadOnly StartupApproved_disabled_bytes As Byte() = {3, 0, 29, 16, 113, 209, 7, 29, 16, 113, 209, 7}

            Private Shared ReadOnly KEY_Windows_RunAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run"
            Private Shared ReadOnly KEY_Windows_StartupApproved As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run"

            Public Shared Sub SetStartupProgram(AppName As String, AppPath As String)
                If Environment.Is64BitOperatingSystem Then
                    Registry.SetValue_String(KEY_Windows_RunAtStartup, AppName, "")

                    Registry64.SetValue_String(KEY_Windows_RunAtStartup, AppName, AppPath)
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                Else
                    Registry64.SetValue_String(KEY_Windows_RunAtStartup, AppName, "")

                    Registry.SetValue_String(KEY_Windows_RunAtStartup, AppName, AppPath)
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                End If
            End Sub

            Public Shared Sub EnableStartupProgram(AppName As String)
                If Environment.Is64BitOperatingSystem Then
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                Else
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_enabled_bytes)
                End If
            End Sub

            Public Shared Sub DisableStartupProgram(AppName As String)
                If Environment.Is64BitOperatingSystem Then
                    Registry64.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_disabled_bytes)
                Else
                    Registry.SetValue_Binary(KEY_Windows_StartupApproved, AppName, StartupApproved_disabled_bytes)
                End If
            End Sub

        End Class
    End Class
#End Region

#Region "Windows User"
    Public Class WindowsUser
        Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String], ByVal lpszDomain As [String], ByVal lpszPassword As [String], ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Boolean
        <DllImport("kernel32.dll")> Public Shared Function FormatMessage(ByVal dwFlags As Integer, ByRef lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByRef lpBuffer As [String], ByVal nSize As Integer, ByRef Arguments As IntPtr) As Integer
        End Function

        Public Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean

        Public Shared Function ConvertUsernameToSid(UserName As String, ByRef sId As String) As Boolean
            Try
                Dim f As New NTAccount(UserName)
                Dim s As SecurityIdentifier = DirectCast(f.Translate(GetType(SecurityIdentifier)), SecurityIdentifier)

                sId = s.ToString()

                Return True
            Catch ex As Exception
                sId = ""

                Errors.Add(ex.Message)

                Return False
            End Try
        End Function

        Public Shared Function GetProfileDirectory(UserName As String, ByRef ProfileDir As String) As Boolean
            Dim sSid As String = "", sDir As String = ""

            If ConvertUsernameToSid(UserName, sSid) Then
                ProfileDir = Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\" & sSid, "ProfileImagePath")

                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function GetProfileTempDirectory(UserName As String) As String
            Dim sDir As String = ""

            GetProfileTempDirectory(UserName, sDir)

            Return sDir
        End Function

        Public Shared Function GetProfileTempDirectory(UserName As String, ByRef ProfileTempDir As String) As Boolean
            Dim sDir As String = ""

            If GetProfileDirectory(UserName, sDir) Then
                ProfileTempDir = "%USERPROFILE%\AppData\Local\Temp".Replace("%USERPROFILE%", sDir)

                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CheckLogon(sUsername As String, sPassword As String, ByRef ErrorMessage As String) As Boolean
            Dim bRet As Boolean = False

            Try
                Dim tokenHandle As New IntPtr(0)
                Dim UserName, MachineName, Pwd As String
                'The MachineName property gets the name of your computer.
                MachineName = System.Environment.MachineName
                UserName = sUsername
                Pwd = sPassword

                Const LOGON32_PROVIDER_DEFAULT As Integer = 0
                Const LOGON32_LOGON_INTERACTIVE As Integer = 2
                tokenHandle = IntPtr.Zero
                'Call the LogonUser function to obtain a handle to an access token.
                Dim returnValue As Boolean = LogonUser(UserName, MachineName, Pwd, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, tokenHandle)

                If returnValue = False Then
                    'This function returns the error code that the last unmanaged function returned.
                    Dim ret As Integer = Marshal.GetLastWin32Error()
                    ErrorMessage = GetErrorMessage(ret)
                    bRet = False
                Else
                    'Create the WindowsIdentity object for the Windows user account that is
                    'represented by the tokenHandle token.
                    Dim newId As New WindowsIdentity(tokenHandle)
                    Dim userperm As New WindowsPrincipal(newId)
                    'Verify whether the Windows user has administrative credentials.
                    If userperm.IsInRole(WindowsBuiltInRole.Administrator) Then
                        ErrorMessage = "ok"
                    Else
                        ErrorMessage = "ok, but not an admin"
                    End If

                    bRet = True
                End If

                'Free the access token.
                If Not System.IntPtr.op_Equality(tokenHandle, IntPtr.Zero) Then
                    CloseHandle(tokenHandle)
                End If
            Catch ex As Exception
                ErrorMessage = "Exception occurred. " + ex.Message
            End Try

            Return bRet
        End Function

        Private Shared Function GetErrorMessage(ByVal errorCode As Integer) As String
            Dim FORMAT_MESSAGE_ALLOCATE_BUFFER As Integer = &H100
            Dim FORMAT_MESSAGE_IGNORE_INSERTS As Integer = &H200
            Dim FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000

            Dim msgSize As Integer = 255
            Dim lpMsgBuf As String = ""
            Dim dwFlags As Integer = FORMAT_MESSAGE_ALLOCATE_BUFFER Or FORMAT_MESSAGE_FROM_SYSTEM Or FORMAT_MESSAGE_IGNORE_INSERTS

            Dim lpSource As IntPtr = IntPtr.Zero
            Dim lpArguments As IntPtr = IntPtr.Zero
            'Call the FormatMessage function to format the message.
            Dim returnVal As Integer = FormatMessage(dwFlags, lpSource, errorCode, 0, lpMsgBuf,
                    msgSize, lpArguments)
            If returnVal = 0 Then
                Throw New Exception("Failed to format message for error code " + errorCode.ToString() + ". ")
            End If

            Return lpMsgBuf
        End Function

        Public Shared Function FindUser(ByVal sUsername As String, Optional ByVal sGroup As String = "Users") As Boolean
            Try
                Dim oUser As Object, oGroup As Object

                oGroup = GetObject("WinNT://" & Environment.MachineName & "/" & sGroup)

                For Each oUser In oGroup.Members
                    Try
                        If Trim(sUsername.ToLower) = Trim(oUser.Name.ToString().ToLower) Then
                            Return True
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                Next

                oGroup = Nothing
                oUser = Nothing

                'Return True
            Catch ex As Exception
                'Return False
            End Try

            Return False
        End Function
    End Class
#End Region

#Region "Processes"
    Public Class Processes
        Public Class HelpersProcessesException
            Inherits Exception

            Public Sub New()
            End Sub

            Public Sub New(ByVal message As String)
            End Sub

            Public Sub New(ByVal message As String, ByVal inner As Exception)
            End Sub
        End Class

        <DllImport("advapi32.dll", SetLastError:=True)>
        Private Shared Function OpenProcessToken(ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)>
        Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        Private Shared mLastError As String = ""
        Public Shared ReadOnly Property LastError As String
            Get
                Dim sTmp As String = mLastError

                mLastError = ""

                Return sTmp
            End Get
        End Property

        Private Shared Function ConvertPasswordStringToSecureString(ByVal str As String) As System.Security.SecureString
            Dim password As New System.Security.SecureString

            For Each c As Char In str.ToCharArray
                password.AppendChar(c)
            Next

            Return password
        End Function

        Public Shared Function IsProcessRunningExactMatch(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String


            sSearchProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

            If Not sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower & ".exe"
            End If

            Try
                For Each p As Process In Process.GetProcesses()
                    Try
                        sProcessName__ToLower = IO.Path.GetFileName(p.Modules(0).FileName).ToLower
                    Catch ex As Exception
                        sProcessName__ToLower = ""
                    End Try

                    If sSearchProcessName__ToLower.Equals(sProcessName__ToLower) Then
                        bRet = True
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function IsProcessRunning(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sProcessName__ToLower As String

            Try
                sProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

                If sProcessName.EndsWith(".exe") Then
                    sProcessName__ToLower = sProcessName__ToLower.Replace(".exe", "")
                End If


                Dim p As Process() = Process.GetProcessesByName(sProcessName__ToLower)

                If p.Count > 0 Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try


            Return bRet
        End Function

        Public Shared Function IsProcessRunning_broken(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String


            sSearchProcessName__ToLower = IO.Path.GetFileName(sProcessName).ToLower()

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            Try
                For Each p As Process In Process.GetProcesses
                    sProcessName__ToLower = p.ProcessName.ToLower
                    If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                        bRet = True
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function KillProcesses(sProcessName As String) As Integer
            Dim sSearchProcessName__ToLower As String, sProcessName__ToLower As String
            Dim iCount As Integer = 0

            sSearchProcessName__ToLower = sProcessName.ToLower

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            Try
                For Each p As Process In Process.GetProcesses
                    sProcessName__ToLower = p.ProcessName.ToLower
                    If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                        Try
                            p.Kill()
                            iCount += 1
                        Catch ex As Exception

                        End Try
                    End If
                Next
            Catch ex As Exception
                iCount = -1
            End Try

            Return iCount
        End Function

        Public Shared Function IsUserLoggedIn(UserName As String, Optional CompletelyArbitraryProcessCountThreshold As Integer = 2) As Boolean
            Dim iCount As Integer

            iCount = CountProcessesOwnedByUser(UserName)

            If iCount >= CompletelyArbitraryProcessCountThreshold Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
            Dim wiName As String, lCount As Long

            lCount = 0

            For Each p As Process In Process.GetProcesses
                Try
                    Dim hToken As IntPtr
                    If OpenProcessToken(p.Handle, TokenAccessLevels.Query, hToken) Then
                        Using wi As New WindowsIdentity(hToken)
                            wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                            If wiName.ToLower = sUserNameToCheck.ToLower Then
                                lCount += 1
                            End If
                        End Using
                        CloseHandle(hToken)
                    End If
                Catch ex As Exception

                End Try
            Next

            Return lCount
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String) As Process
            Dim p As New Process

            If _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, sWorkingDirectory, False, p) Then
                Return p
            Else
                'wow, this makes sense
                Return p
            End If
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String, bHidden As Boolean) As Process
            Dim p As New Process

            If _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, sWorkingDirectory, bHidden, p) Then
                Return p
            Else
                'wow, this makes sense
                Return p
            End If
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOutMs As Integer) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, "", waitForExit, timeOutMs, False)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, "", waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, False, cmdArguments, sWorkingDirectory, waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, bLoadUserProfile As Boolean, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, bLoadUserProfile, cmdArguments, sWorkingDirectory, waitForExit, timeOutMs, hidden)
        End Function

        Public Shared Function StartProcess(cmdLine As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOutMs As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, "", "", False, cmdArguments, "", waitForExit, timeOutMs, False)
        End Function

        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, bLoadUserProfile As Boolean, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            pInfo.WorkingDirectory = sWorkingDirectory
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = bLoadUserProfile
                pInfo.Verb = "runas"
                If hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOutMs)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        Errors.Add("process timed out ('" & pInfo.FileName & "')")

                        Return False
                    Else
                        Dim ret As Integer = -1
                        Try
                            ret = p.ExitCode
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function

        Public Shared Function StartProcessAsUserWithExitcode(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer, ByRef ExitCode As Integer) As Integer
            Return _StartProcessWithExitcode(cmdLine, sUserName, sPassword, cmdArguments, waitForExit, timeOut, False, ExitCode)
        End Function

        Private Shared Function _StartProcessWithExitcode(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer, hidden As Boolean, ByRef ExitCode As Integer) As Integer
            Dim pInfo As New ProcessStartInfo()
            Dim iExitCode As Integer = -1

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = True
                pInfo.Verb = "runas"
                If hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOut)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        Errors.Add("process timed out ('" & pInfo.FileName & "')")

                        Return False
                    Else
                        Dim ret As Integer = -1
                        Try
                            iExitCode = p.ExitCode
                        Catch ex As Exception

                        End Try
                    End If
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function


        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, WorkingDirectory As String, Hidden As Boolean, ByRef proc As Process) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
                pInfo.LoadUserProfile = True
                pInfo.WorkingDirectory = WorkingDirectory
                pInfo.Verb = "runas"
                If Hidden Then
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            proc = New Process

            Try
                proc = Process.Start(pInfo)

                If proc.Id <= 0 Then
                    'wtf, not started
                    Errors.Add("process not started, pid<0 ('" & pInfo.FileName & "')")

                    Return False
                End If
            Catch ex As Exception
                Errors.Add("_StartProcess exception ('" & pInfo.FileName & "'): " & ex.Message)

                Return False
            End Try

            Return True
        End Function

        Public Shared Function IsAppAlreadyRunning() As Boolean
            Dim appProc() As Process
            Dim strModName, strProcName As String

            Try
                strModName = Process.GetCurrentProcess.MainModule.ModuleName
                strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

                appProc = Process.GetProcessesByName(strProcName)

                If appProc.Length > 1 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Sub SelfElevate(Optional Argument As String = "--elevated")
            Dim proc As New ProcessStartInfo
            proc.UseShellExecute = True
            proc.WorkingDirectory = Environment.CurrentDirectory
            proc.FileName = FilesAndFolders.GetExecutablePath()
            proc.Verb = "runas"
            proc.Arguments = Argument

            Try
                Process.Start(proc)
            Catch
                Return
            End Try

            Environment.Exit(0)
        End Sub

        Public Shared Function KillPreviousInstances() As Integer
            Dim thisProcess As Process = Process.GetCurrentProcess()
            Dim thisExe As String = IO.Path.GetFileName(System.Reflection.Assembly.GetEntryAssembly().CodeBase)
            Dim pList() As System.Diagnostics.Process = System.Diagnostics.Process.GetProcessesByName(thisExe)
            Dim killCount As Integer = 0

            Try
                For Each eachProcess As Process In pList
                    If (Not thisProcess.Equals(eachProcess)) Then
                        killCount += 1
                        eachProcess.Kill()
                    End If
                Next
            Catch ex As Exception
                Errors.Add(ex.Message)
                killCount = -1
            End Try

            Return killCount
        End Function
    End Class
#End Region

#Region "Event log"
    Public Class WindowsEventLog
        Public Shared Function WriteInformation(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Information)
        End Function

        Public Shared Function WriteError(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Error)
        End Function

        Public Shared Function WriteWarning(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Warning)
        End Function

        Public Shared Function WriteEntry(sEvent As String, oType As EventLogEntryType) As String
            Try
                Dim sSource As String = My.Application.Info.ProductName
                Dim sLog As String = "Application"

                If Not EventLog.SourceExists(sSource) Then
                    EventLog.CreateEventSource(New EventSourceCreationData(sSource, sLog))
                End If

                Dim ELog As New EventLog(sLog, ".", sSource)
                ELog.WriteEntry(sEvent, EventLogEntryType.Error)

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
    End Class
#End Region

End Class
