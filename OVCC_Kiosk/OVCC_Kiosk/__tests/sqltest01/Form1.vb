﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim uTime As Double
        uTime = (DateTime.UtcNow - New DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds


        Dim _s As New Stopwatch

        _s.Start()



        SessionHandler.StartSession()
        SessionHandler.TermsDeclined()
        SessionHandler.EndSession()

        SessionHandler.StartSession()
        SessionHandler.TermsAccepted()
        SessionHandler.Activity("office")
        SessionHandler.EndSession()


        _s.Stop()

        MsgBox(_s.ElapsedMilliseconds)
    End Sub
End Class
