﻿Public Class myFolders
    Public Class myFolderItem
        Private mText As String
        Public Property Text() As String
            Get
                Return mText
            End Get
            Set(ByVal value As String)
                mText = value
            End Set
        End Property

        Private mPath As String
        Public Property Path() As String
            Get
                Return mPath
            End Get
            Set(ByVal value As String)
                mPath = value
            End Set
        End Property

        Private mIconIndex As Integer
        Public Property IconIndex() As Integer
            Get
                Return mIconIndex
            End Get
            Set(ByVal value As Integer)
                mIconIndex = value
            End Set
        End Property

        Private mIconSelectedIndex As Integer
        Public Property IconSelectedIndex() As Integer
            Get
                Return mIconSelectedIndex
            End Get
            Set(ByVal value As Integer)
                mIconSelectedIndex = value
            End Set
        End Property

        Public Sub New()

        End Sub

        Public Sub New(Text As String, Path As String, IconIndex As Integer, IconSelected As Integer)
            mText = Text
            mPath = Path
            mIconIndex = IconIndex
            mIconSelectedIndex = IconSelected
        End Sub
    End Class

    Private mList As New List(Of myFolderItem)
    Private mCount As Integer

    Public Sub New()
        mList = New List(Of myFolderItem)
        mCount = 0
    End Sub

    Public Sub Add(Text As String, Path As String, IconIndex As Integer, IconSelected As Integer)
        mList.Add(New myFolderItem(Text, Path, IconIndex, IconSelected))
    End Sub

    Public Function GetNext(ByRef FolderItem As myFolderItem) As Boolean
        If mCount >= mList.Count Then
            mCount = 0
            Return False
        End If

        FolderItem = New myFolderItem
        FolderItem = mList.Item(mCount)

        Return True
    End Function
End Class
