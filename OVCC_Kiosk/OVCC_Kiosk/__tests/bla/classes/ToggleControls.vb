﻿Public Class ToggleControls
    Private Shared mStates As Hashtable
    Private Shared mExceptions As New List(Of String)

    Private Shared mParentControl As Control = Nothing
    Public Shared Property ParentControl() As Control
        Get
            Return mParentControl
        End Get
        Set(ByVal value As Control)
            mParentControl = value
        End Set
    End Property

    Private Shared mGlobalState As Boolean = True
    Public Shared Property GlobalState() As Boolean
        Get
            Return mGlobalState
        End Get
        Set(ByVal value As Boolean)
            mGlobalState = value
        End Set
    End Property

    Private Shared Sub _SaveState(cntrl As Control)
        mStates = New Hashtable

        For Each _c As Control In cntrl.Controls
            mStates.Add(_c.Name, _c.Enabled)
        Next
    End Sub

    Private Shared Sub _RestoreState(cntrl As Control)
        Dim o As Object = Nothing

        For Each _c As Control In cntrl.Controls
            Try
                o = mStates.Item(_c.Name)
            Catch ex As Exception
            End Try

            If Not o Is Nothing Then
                Try
                    _c.Enabled = Convert.ToBoolean(o)
                Catch ex As Exception
                End Try
            End If
        Next
    End Sub


    Public Shared Sub AddException(cntrl As Control)
        AddException(cntrl.Name)
    End Sub

    Public Shared Sub AddException(name As String)
        mExceptions.Add(name)
    End Sub

    Public Shared Sub ClearExceptions()
        mExceptions = New List(Of String)
    End Sub


    Public Shared Sub EnableAll(cntrl As Control)
        For Each _c As Control In cntrl.Controls
            _c.Enabled = True
        Next

        mGlobalState = True
    End Sub

    Public Shared Sub EnableAll()
        If mParentControl Is Nothing Then
            Exit Sub
        End If

        EnableAll(mParentControl)
    End Sub


    Public Shared Sub Disable(cntrl As Control)
        If Not mGlobalState Then
            Exit Sub
        End If

        _SaveState(cntrl)

        For Each _c As Control In cntrl.Controls
            If Not mExceptions.Contains(_c.Name) Then
                _c.Enabled = False
            End If
        Next

        mGlobalState = False
    End Sub

    Public Shared Sub Disable()
        If mParentControl Is Nothing Then
            Exit Sub
        End If

        Disable(mParentControl)
    End Sub


    Public Shared Sub Restore(cntrl As Control)
        If mGlobalState Then
            Exit Sub
        End If

        _RestoreState(cntrl)

        mGlobalState = True
    End Sub

    Public Shared Sub Restore()
        If mParentControl Is Nothing Then
            Exit Sub
        End If

        _RestoreState(mParentControl)
    End Sub
End Class
