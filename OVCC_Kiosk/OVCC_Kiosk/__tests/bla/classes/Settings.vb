﻿Public Class Settings
    Public Enum IconIndices As Integer
        Drive = 0
        Generic = 1
        Documents = 2
        Downloads = 3
        Pictures = 4
        Desktop = 5
    End Enum

    Public Class Generic
        Public Shared Debugging As Boolean = False
        Public Shared ContextMenu As Boolean = False
        Public Shared DirectoriesCollapsable As Boolean = True
        Public Shared ShowNetworkDrives As Boolean = False
        Public Shared StartWithDownloadsFolder As Boolean = False
        Public Shared AppendUnzippedFolderWithFullDate As Boolean = False
        Public Shared ShowHiddenFiles As Boolean = False
        Public Shared ShowSystemFiles As Boolean = False
        Public Shared ShowTemporaryFiles As Boolean = False
        Public Shared DetectFolderChanges As Boolean = False
        Public Shared AllowFileDelete As Boolean = False
        Public Shared AllowPrinting As Boolean = False
    End Class

    Public Class CommandLineParams
        Public Shared Debugging As String = "--debug"
        Public Shared StartWithDownloadsFolder As String = "--start-with-downloads-folder"
        Public Shared ShowHiddenFiles As String = "--show-hidden-files"
        Public Shared ShowSystemFiles As String = "--show-system-files"
        Public Shared ShowTemporaryFiles As String = "--show-temporary-files"
        Public Shared DetectFolderChanges As String = "--detect-folder-changes"
    End Class

    Public Class Strings
        Public Class PathNames
            Public Shared Downloads As String = "Downloads"
        End Class

        Public Shared AppTitle As String = "OVCC File Explorer"
    End Class

    Public Class Images
        Public Shared OpenImagesWithMSPaint As Boolean = False
        Public Shared OpenImagesWithPinta As Boolean = True
        Public Shared Extensions As String = "png;bmp;gif;jpg;jpeg"
        Public Shared ReadOnly MSPaintExe As String = "C:\Windows\System32\mspaint.exe"
        Public Shared ReadOnly PintaExe As String = "C:\Program Files\Pinta\Pinta.exe"

        Public Shared Function IsMSPaintRequiredForThisExtension(_extension2check As String) As Boolean
            If Not OpenImagesWithMSPaint Then
                Return False
            End If


            Dim _extensions As String() = Extensions.Split(";")
            Dim _ret As Boolean = False

            If _extension2check.StartsWith(".") Then
                _extension2check = _extension2check.Substring(1, _extension2check.Length - 1)
            End If

            For Each _extension As String In _extensions
                If _extension.Equals(_extension2check) Then
                    _ret = True

                    Exit For
                End If
            Next

            Return _ret
        End Function

        Public Shared Function IsPintaRequiredForThisExtension(_extension2check As String) As Boolean
            If Not OpenImagesWithPinta Then
                Return False
            End If

            If Not IO.File.Exists(Settings.Images.PintaExe) Then
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteError("Pinta not found", 1)
                    Helpers.Logger.WriteError(Settings.Images.PintaExe, 2)
                End If

                OpenImagesWithMSPaint = True
                Return False
            End If


            Dim _extensions As String() = Extensions.Split(";")
            Dim _ret As Boolean = False

            If _extension2check.StartsWith(".") Then
                _extension2check = _extension2check.Substring(1, _extension2check.Length - 1)
            End If

            For Each _extension As String In _extensions
                If _extension.Equals(_extension2check) Then
                    _ret = True

                    Exit For
                End If
            Next

            Return _ret
        End Function
    End Class

    Public Class Paths
        Public Shared Names As String() = {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        }
        Public Shared Paths As String() = {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        }
        Public Shared Icons As Integer() = {
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic
        }
    End Class


    Public Shared RegistryKey As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\FileExplorer"

    Public Shared Sub Load()
        Generic.Debugging = Helpers.Registry.GetValue_Boolean(RegistryKey, "Debugging", False)
        Generic.ContextMenu = Helpers.Registry.GetValue_Boolean(RegistryKey, "ContextMenu", False)
        Generic.DirectoriesCollapsable = Helpers.Registry.GetValue_Boolean(RegistryKey, "DirectoriesCollapsable", True)
        Generic.ShowNetworkDrives = Helpers.Registry.GetValue_Boolean(RegistryKey, "ShowNetworkDrives", False)
        Generic.StartWithDownloadsFolder = Helpers.Registry.GetValue_Boolean(RegistryKey, "StartWithDownloadsFolder", False)
        Generic.AppendUnzippedFolderWithFullDate = Helpers.Registry.GetValue_Boolean(RegistryKey, "AppendUnzippedFolderWithFullDate", False)
        Generic.ShowHiddenFiles = Helpers.Registry.GetValue_Boolean(RegistryKey, "ShowHiddenFiles", False)
        Generic.ShowSystemFiles = Helpers.Registry.GetValue_Boolean(RegistryKey, "ShowSystemFiles", False)
        Generic.ShowTemporaryFiles = Helpers.Registry.GetValue_Boolean(RegistryKey, "ShowTemporaryFiles", False)
        Generic.DetectFolderChanges = Helpers.Registry.GetValue_Boolean(RegistryKey, "DetectFolderChanges", False)
        Generic.AllowFileDelete = Helpers.Registry.GetValue_Boolean(RegistryKey, "AllowFileDelete", False)
        Generic.AllowPrinting = Helpers.Registry.GetValue_Boolean(RegistryKey, "AllowPrinting", False)

        Images.OpenImagesWithMSPaint = Helpers.Registry.GetValue_Boolean(RegistryKey, "OpenImagesWithPaint", True)
        Images.Extensions = Helpers.Registry.GetValue_String(RegistryKey, "ImageExtensions", "png;bmp;gif;jpg;jpeg")

        For i As Integer = 1 To 10
            Paths.Names(i - 1) = Helpers.Registry.GetValue_String(RegistryKey, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Name", "")
            Paths.Paths(i - 1) = Helpers.Registry.GetValue_String(RegistryKey, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Path", "")
            Paths.Icons(i - 1) = Helpers.Registry.GetValue_Integer(RegistryKey, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Icon", 1)
        Next

        Save()
    End Sub

    Public Shared Sub Save()
        Helpers.Registry.SetValue_Boolean(RegistryKey, "Debugging", Generic.Debugging)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "ContextMenu", Generic.ContextMenu)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "DirectoriesCollapsable", Generic.DirectoriesCollapsable)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "ShowNetworkDrives", Generic.ShowNetworkDrives)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "StartWithDownloadsFolder", Generic.StartWithDownloadsFolder)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "AppendUnzippedFolderWithFullDate", Generic.AppendUnzippedFolderWithFullDate)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "ShowHiddenFiles", Generic.ShowHiddenFiles)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "ShowSystemFiles", Generic.ShowSystemFiles)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "ShowTemporaryFiles", Generic.ShowTemporaryFiles)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "DetectFolderChanges", Generic.DetectFolderChanges)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "AllowFileDelete", Generic.AllowFileDelete)
        Helpers.Registry.SetValue_Boolean(RegistryKey, "AllowPrinting", Generic.AllowPrinting)

        Helpers.Registry.SetValue_Boolean(RegistryKey, "OpenImagesWithPaint", Images.OpenImagesWithMSPaint)
        Helpers.Registry.SetValue_String(RegistryKey, "ImageExtensions", Images.Extensions)

        For i As Integer = 1 To 10
            Helpers.Registry.SetValue_String(RegistryKey, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Name", Paths.Names(i - 1))
            Helpers.Registry.SetValue_String(RegistryKey, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Path", Paths.Paths(i - 1))
            Helpers.Registry.SetValue_Integer(RegistryKey, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Icon", Paths.Icons(i - 1))
        Next
    End Sub
End Class
