﻿Public Class frmOverwriteConfirmation
    '=======================================================================================================================
    '=======================================================================================================================
    '=======================================================================================================================
    Private Declare Function EnableMenuItem Lib "user32.dll" Alias "EnableMenuItem" (ByVal hMenu As IntPtr, ByVal uIDEnableItem As Int32, ByVal uEnable As Int32) As Int32

    Private _Moveable As Boolean = True
    Public Overridable Property Moveable() As Boolean
        Get
            Return _Moveable
        End Get
        Set(ByVal Value As Boolean)
            If _Moveable <> Value Then
                _Moveable = Value
            End If
        End Set
    End Property

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = &H117& Then
            'Handles popup of system menu.
            If m.LParam.ToInt32 \ 65536 <> 0 Then 'divide by 65536 to get hiword.
                Dim AbleFlags As Int32 = &H0&
                If Not _Moveable Then AbleFlags = &H2& Or &H1&
                EnableMenuItem(m.WParam, &HF010&, &H0& Or AbleFlags)
            End If
        End If

        If Not _Moveable Then
            'Cancels any attempt to drag the window by it's caption.
            If m.Msg = &HA1 Then If m.WParam.ToInt32 = &H2 Then Return
            'Redundant but cancels any clicks on the Move system menu item.
            If m.Msg = &H112 Then If (m.WParam.ToInt32 And &HFFF0) = &HF010& Then Return
        End If

        'Return control to base message handler.
        MyBase.WndProc(m)
    End Sub
    '=======================================================================================================================
    '=======================================================================================================================
    '=======================================================================================================================

    Private Sub frmOverwriteConfirmation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sTmp As String = frmMain.mFileDraggedName

        If sTmp.EndsWith("\") Then
            sTmp = sTmp.Substring(0, sTmp.Length - 1)
        End If

        sTmp = IO.Path.GetFileName(sTmp)

        If frmMain.mFileDraggedIsDir Then
            lblWarning.Text = "Are you sure you want to overwrite the directory '" & sTmp & "' ?"
        Else
            lblWarning.Text = "Are you sure you want to overwrite the file '" & sTmp & "' ?"
        End If


        btnDelete.Enabled = False
        btnCancel.Focus()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub chkCheck_CheckedChanged(sender As Object, e As EventArgs) Handles chkCheck.CheckedChanged
        btnDelete.Enabled = chkCheck.CheckState
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Me.DialogResult = DialogResult.OK
    End Sub
End Class