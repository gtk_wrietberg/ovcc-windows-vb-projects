﻿Public Class frmProperties
    '=======================================================================================================================
    '=======================================================================================================================
    '=======================================================================================================================
    Private Declare Function EnableMenuItem Lib "user32.dll" Alias "EnableMenuItem" (ByVal hMenu As IntPtr, ByVal uIDEnableItem As Int32, ByVal uEnable As Int32) As Int32

    Private _Moveable As Boolean = True
    Public Overridable Property Moveable() As Boolean
        Get
            Return _Moveable
        End Get
        Set(ByVal Value As Boolean)
            If _Moveable <> Value Then
                _Moveable = Value
            End If
        End Set
    End Property

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = &H117& Then
            'Handles popup of system menu.
            If m.LParam.ToInt32 \ 65536 <> 0 Then 'divide by 65536 to get hiword.
                Dim AbleFlags As Int32 = &H0&
                If Not _Moveable Then AbleFlags = &H2& Or &H1&
                EnableMenuItem(m.WParam, &HF010&, &H0& Or AbleFlags)
            End If
        End If

        If Not _Moveable Then
            'Cancels any attempt to drag the window by it's caption.
            If m.Msg = &HA1 Then If m.WParam.ToInt32 = &H2 Then Return
            'Redundant but cancels any clicks on the Move system menu item.
            If m.Msg = &H112 Then If (m.WParam.ToInt32 And &HFFF0) = &HF010& Then Return
        End If

        'Return control to base message handler.
        MyBase.WndProc(m)
    End Sub
    '=======================================================================================================================
    '=======================================================================================================================
    '=======================================================================================================================


    Private Sub frmProperties_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sf As String = IO.Path.Combine(frmMain.treeFolders.SelectedNode.Name, frmMain.mFileSelectedNames.Item(0))
        Dim sLocation As String = sf

        For Each mm As myFolderItem In frmMain.mMyFolders
            Dim dirName As String = New IO.DirectoryInfo(mm.Path).Name
            Console.WriteLine("mm=" & mm.Path)
            If sf.StartsWith(mm.Path) Then
                sLocation = sf.Replace(mm.Path, "")
                If sLocation.StartsWith("\") And sLocation.Length > 1 Then
                    sLocation = sLocation.Substring(1)
                End If
                sLocation = IO.Path.Combine(dirName, sLocation)

                Exit For
            End If
        Next



        If IO.Directory.Exists(sf) Then
            Dim di As New IO.DirectoryInfo(sf)

            picIcon.Image = My.Resources.ico_folder

            lblTypeTitle.Text = "Type:"
            lblAccessed.Text = di.LastAccessTime.ToString
            lblCreated.Text = di.CreationTime.ToString
            txtLocation.Text = sLocation
            lblModified.Text = di.LastWriteTime.ToString
            lblName.Text = di.Name
            lblSize.Text = "-"
            lblType.Text = "Folder"
        ElseIf IO.File.Exists(sf) Then
            Dim fi As New IO.FileInfo(sf)
            Dim _icon As Icon
            _icon = Helpers.Windows.RegisteredFileType.Icon.FromFilePath(sf)

            If _icon Is Nothing Then
                picIcon.Image = My.Resources.ico_unknownfile
            Else
                picIcon.Image = Bitmap.FromHicon(_icon.Handle)
            End If

            lblTypeTitle.Text = "Type of file:"
            lblAccessed.Text = fi.LastAccessTime.ToString
            lblCreated.Text = fi.CreationTime.ToString
            txtLocation.Text = sLocation
            lblModified.Text = fi.LastWriteTime.ToString
            lblName.Text = fi.Name
            lblSize.Text = Helpers.FilesAndFolders.FileSize.GetHumanReadable(fi.Length)
            lblType.Text = Helpers.FilesAndFolders.GetFileTypeFromRegistry(fi.Extension)
        Else
            Me.DialogResult = DialogResult.Abort
        End If

        'Dim _listviewitem As New ListViewItem(New String() {fi.Name, fi.LastWriteTime.ToString, Helpers.FilesAndFolders.GetFileTypeFromRegistry(fi.Extension), Helpers.FilesAndFolders.FileSize.GetHumanReadable(fi.Length), fi.Length})

    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        Me.DialogResult = DialogResult.OK
    End Sub
End Class