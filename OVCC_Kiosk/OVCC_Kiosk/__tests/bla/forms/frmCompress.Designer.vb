﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompress
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bgWorkerCompress = New System.ComponentModel.BackgroundWorker()
        Me.progressCompress = New System.Windows.Forms.ProgressBar()
        Me.radCompressStore = New System.Windows.Forms.RadioButton()
        Me.grpRadioButtons = New System.Windows.Forms.GroupBox()
        Me.radCompressBest = New System.Windows.Forms.RadioButton()
        Me.radCompressNormal = New System.Windows.Forms.RadioButton()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.grpRadioButtons.SuspendLayout()
        Me.SuspendLayout()
        '
        'bgWorkerCompress
        '
        '
        'progressCompress
        '
        Me.progressCompress.Location = New System.Drawing.Point(218, 189)
        Me.progressCompress.Name = "progressCompress"
        Me.progressCompress.Size = New System.Drawing.Size(172, 27)
        Me.progressCompress.TabIndex = 0
        '
        'radCompressStore
        '
        Me.radCompressStore.AutoSize = True
        Me.radCompressStore.Location = New System.Drawing.Point(6, 21)
        Me.radCompressStore.Name = "radCompressStore"
        Me.radCompressStore.Size = New System.Drawing.Size(143, 17)
        Me.radCompressStore.TabIndex = 1
        Me.radCompressStore.Text = "Store (no compression)"
        Me.radCompressStore.UseVisualStyleBackColor = True
        '
        'grpRadioButtons
        '
        Me.grpRadioButtons.Controls.Add(Me.radCompressBest)
        Me.grpRadioButtons.Controls.Add(Me.radCompressNormal)
        Me.grpRadioButtons.Controls.Add(Me.radCompressStore)
        Me.grpRadioButtons.Location = New System.Drawing.Point(12, 12)
        Me.grpRadioButtons.Name = "grpRadioButtons"
        Me.grpRadioButtons.Size = New System.Drawing.Size(172, 91)
        Me.grpRadioButtons.TabIndex = 3
        Me.grpRadioButtons.TabStop = False
        Me.grpRadioButtons.Text = "Compression method"
        '
        'radCompressBest
        '
        Me.radCompressBest.AutoSize = True
        Me.radCompressBest.Location = New System.Drawing.Point(6, 67)
        Me.radCompressBest.Name = "radCompressBest"
        Me.radCompressBest.Size = New System.Drawing.Size(149, 17)
        Me.radCompressBest.TabIndex = 3
        Me.radCompressBest.Text = "Slow (best compression)"
        Me.radCompressBest.UseVisualStyleBackColor = True
        '
        'radCompressNormal
        '
        Me.radCompressNormal.AutoSize = True
        Me.radCompressNormal.Checked = True
        Me.radCompressNormal.Location = New System.Drawing.Point(6, 44)
        Me.radCompressNormal.Name = "radCompressNormal"
        Me.radCompressNormal.Size = New System.Drawing.Size(159, 17)
        Me.radCompressNormal.TabIndex = 2
        Me.radCompressNormal.TabStop = True
        Me.radCompressNormal.Text = "Fast (normal compression)"
        Me.radCompressNormal.UseVisualStyleBackColor = True
        '
        'btnStart
        '
        Me.btnStart.Location = New System.Drawing.Point(12, 109)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(75, 23)
        Me.btnStart.TabIndex = 4
        Me.btnStart.Text = "Start"
        Me.btnStart.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(109, 109)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmCompress
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(193, 139)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.grpRadioButtons)
        Me.Controls.Add(Me.progressCompress)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCompress"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "compress folder"
        Me.grpRadioButtons.ResumeLayout(False)
        Me.grpRadioButtons.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents bgWorkerCompress As System.ComponentModel.BackgroundWorker
    Friend WithEvents progressCompress As ProgressBar
    Friend WithEvents radCompressStore As RadioButton
    Friend WithEvents grpRadioButtons As GroupBox
    Friend WithEvents radCompressNormal As RadioButton
    Friend WithEvents radCompressBest As RadioButton
    Friend WithEvents btnStart As Button
    Friend WithEvents btnCancel As Button
End Class
