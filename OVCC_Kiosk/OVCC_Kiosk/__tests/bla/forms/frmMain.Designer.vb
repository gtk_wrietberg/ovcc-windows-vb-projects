﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.bgWorkerFiles = New System.ComponentModel.BackgroundWorker()
        Me.tmrHideOpeningDialog = New System.Windows.Forms.Timer(Me.components)
        Me.tmrHideLongVersionString = New System.Windows.Forms.Timer(Me.components)
        Me.bgWorkerFileCopy = New System.ComponentModel.BackgroundWorker()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.pnlTopBar = New System.Windows.Forms.Panel()
        Me.pnlTopBar_Buttons = New System.Windows.Forms.Panel()
        Me.picClose = New System.Windows.Forms.PictureBox()
        Me.pnlHeader.SuspendLayout()
        Me.pnlTopBar.SuspendLayout()
        Me.pnlTopBar_Buttons.SuspendLayout()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bgWorkerFiles
        '
        Me.bgWorkerFiles.WorkerReportsProgress = True
        Me.bgWorkerFiles.WorkerSupportsCancellation = True
        '
        'tmrHideOpeningDialog
        '
        Me.tmrHideOpeningDialog.Interval = 2000
        '
        'tmrHideLongVersionString
        '
        Me.tmrHideLongVersionString.Interval = 2000
        '
        'bgWorkerFileCopy
        '
        Me.bgWorkerFileCopy.WorkerReportsProgress = True
        Me.bgWorkerFileCopy.WorkerSupportsCancellation = True
        '
        'pnlHeader
        '
        Me.pnlHeader.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlHeader.Controls.Add(Me.pnlTopBar)
        Me.pnlHeader.Location = New System.Drawing.Point(60, 84)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(1071, 112)
        Me.pnlHeader.TabIndex = 11
        '
        'pnlTopBar
        '
        Me.pnlTopBar.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlTopBar.Controls.Add(Me.pnlTopBar_Buttons)
        Me.pnlTopBar.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTopBar.Location = New System.Drawing.Point(0, 0)
        Me.pnlTopBar.Name = "pnlTopBar"
        Me.pnlTopBar.Size = New System.Drawing.Size(1071, 40)
        Me.pnlTopBar.TabIndex = 12
        '
        'pnlTopBar_Buttons
        '
        Me.pnlTopBar_Buttons.Controls.Add(Me.picClose)
        Me.pnlTopBar_Buttons.Dock = System.Windows.Forms.DockStyle.Right
        Me.pnlTopBar_Buttons.Location = New System.Drawing.Point(936, 0)
        Me.pnlTopBar_Buttons.Name = "pnlTopBar_Buttons"
        Me.pnlTopBar_Buttons.Size = New System.Drawing.Size(135, 40)
        Me.pnlTopBar_Buttons.TabIndex = 1
        '
        'picClose
        '
        Me.picClose.BackColor = System.Drawing.Color.Transparent
        Me.picClose.Image = Global.FileExplorer.My.Resources.Resources.topbar_buttons__close_on
        Me.picClose.Location = New System.Drawing.Point(90, 0)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(45, 40)
        Me.picClose.TabIndex = 2
        Me.picClose.TabStop = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.ClientSize = New System.Drawing.Size(1339, 676)
        Me.Controls.Add(Me.pnlHeader)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "OVCC File Explorer"
        Me.pnlHeader.ResumeLayout(False)
        Me.pnlTopBar.ResumeLayout(False)
        Me.pnlTopBar_Buttons.ResumeLayout(False)
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents treeFolders As BufferedTreeView
    Friend WithEvents bgWorkerFiles As System.ComponentModel.BackgroundWorker
    Friend WithEvents tmrHideOpeningDialog As Timer
    Friend WithEvents tmrHideLongVersionString As Timer
    Friend WithEvents bgWorkerFileCopy As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlHeader As Panel
    Friend WithEvents pnlTopBar As Panel
    Friend WithEvents pnlTopBar_Buttons As Panel
    Friend WithEvents picClose As PictureBox
End Class
