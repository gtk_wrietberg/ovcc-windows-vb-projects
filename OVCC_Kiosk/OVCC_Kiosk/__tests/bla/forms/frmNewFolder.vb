﻿Public Class frmNewFolder
    '=======================================================================================================================
    '=======================================================================================================================
    '=======================================================================================================================
    Private Declare Function EnableMenuItem Lib "user32.dll" Alias "EnableMenuItem" (ByVal hMenu As IntPtr, ByVal uIDEnableItem As Int32, ByVal uEnable As Int32) As Int32

    Private _Moveable As Boolean = True
    Public Overridable Property Moveable() As Boolean
        Get
            Return _Moveable
        End Get
        Set(ByVal Value As Boolean)
            If _Moveable <> Value Then
                _Moveable = Value
            End If
        End Set
    End Property

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = &H117& Then
            'Handles popup of system menu.
            If m.LParam.ToInt32 \ 65536 <> 0 Then 'divide by 65536 to get hiword.
                Dim AbleFlags As Int32 = &H0&
                If Not _Moveable Then AbleFlags = &H2& Or &H1&
                EnableMenuItem(m.WParam, &HF010&, &H0& Or AbleFlags)
            End If
        End If

        If Not _Moveable Then
            'Cancels any attempt to drag the window by it's caption.
            If m.Msg = &HA1 Then If m.WParam.ToInt32 = &H2 Then Return
            'Redundant but cancels any clicks on the Move system menu item.
            If m.Msg = &H112 Then If (m.WParam.ToInt32 And &HFFF0) = &HF010& Then Return
        End If

        'Return control to base message handler.
        MyBase.WndProc(m)
    End Sub
    '=======================================================================================================================
    '=======================================================================================================================
    '=======================================================================================================================


    Private Sub ToggleControls(state As Boolean)
        btnSave.Enabled = state
        btnCancel.Enabled = state
        txtFolderName.Enabled = state
    End Sub

    Private Function DirNameIsOk(dirName As String) As Boolean
        Dim dir As String = IO.Path.GetDirectoryName(dirName)

        Return Not dir.Intersect(IO.Path.GetInvalidPathChars()).Any()
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        ToggleControls(False)

        Dim bOk As Boolean = False

        If DirNameIsOk(txtFolderName.Text) Then
            If Not IO.Directory.Exists(IO.Path.Combine(frmMain.treeFolders.SelectedNode.Name, txtFolderName.Text)) Then
                bOk = True
            End If
        End If

        If Not bOk Then
            'folder alreaydy exists or not valid
            ToggleControls(True)

            txtFolderName.Focus()
            txtFolderName.Select()
            txtFolderName.SelectAll()

            ShakeTextbox()
        Else
            Me.DialogResult = DialogResult.OK
        End If
    End Sub

    Private Sub frmNewFolder_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sName As String = "New folder", iCount As Integer = 1

        While IO.Directory.Exists(IO.Path.Combine(frmMain.treeFolders.SelectedNode.Name, sName))
            iCount += 1
            sName = "New folder (" & iCount.ToString & ")"
        End While

        txtFolderName.Text = sName

        txtFolderName.Focus()
        txtFolderName.SelectAll()
        txtFolderName.Select()
    End Sub

    Private txtboxPos As Integer = 2
    Private txtboxShakes As Integer = 3
    Private Sub ShakeTextbox()
        For i As Integer = 0 To txtboxShakes - 1
            txtFolderName.Left += 5
            txtFolderName.Refresh()

            Threading.Thread.Sleep(50)

            txtFolderName.Left -= 5
            txtFolderName.Refresh()

            Threading.Thread.Sleep(50)

            txtFolderName.Left -= 5
            txtFolderName.Refresh()

            Threading.Thread.Sleep(50)

            txtFolderName.Left += 5
            txtFolderName.Refresh()

            Threading.Thread.Sleep(50)
        Next
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ToggleControls(False)

        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub txtFolderName_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub txtFolderName_KeyDown(sender As Object, e As KeyEventArgs) Handles txtFolderName.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnSave.PerformClick()
        End If
    End Sub
End Class