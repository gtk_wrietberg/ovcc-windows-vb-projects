﻿Imports System.IO
Imports System.IO.Pipes
Imports System.ServiceModel
Imports System.Text
Imports System.Threading

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        OpenServers()
    End Sub



    Private sSecretHandshake As String = "Hello!"
    Private numThreads As Integer = 4

    Private tServer__Session As Thread
    Private tServer__Logger As Thread


    Private Sub OpenServers()
        tServer__Session = New Thread(AddressOf ServerThread__Session)
        tServer__Session.Start()

        tServer__Logger = New Thread(AddressOf ServerThread__Logger)
        tServer__Logger.Start()

        'Dim i As Integer
        'Dim servers(numThreads) As Thread

        '_tmplog("Waiting for client connect..." & vbCrLf)
        'For i = 0 To numThreads - 1
        '    servers(i) = New Thread(AddressOf ServerThread)
        '    servers(i).Start()
        'Next i
        'Thread.Sleep(250)
        'While i > 0
        '    For j As Integer = 0 To numThreads - 1
        '        If Not (servers(j) Is Nothing) Then
        '            If servers(j).Join(250) Then
        '                _tmplog("Server thread[" & servers(j).ManagedThreadId & "] finished.")
        '                servers(j) = Nothing
        '                i -= 1    ' decrement the thread watch count
        '            End If
        '        End If
        '    Next j

        '    _tmplog(i)
        'End While
        '_tmplog(vbCrLf + "Server threads exhausted, exiting.")
    End Sub

    Public Sub ServerThread__Session(Data As Object)
        Dim pipeServer As New NamedPipeServerStream("ovcc_pipe_session", PipeDirection.InOut, 1)
        Dim threadId As Integer = Thread.CurrentThread.ManagedThreadId
        Dim threadKillSwitch As Boolean = False

        _tmplog("ServerThread__Session started")

        Do
            pipeServer.WaitForConnection()
            _tmplog("ServerThread__Session connection!")

            Try
                Dim ss As New StreamString(pipeServer)
                ss.WriteString(sSecretHandshake)
                Dim receivedString As String = ss.ReadString()

                _tmplog("ServerThread__Session receivedString: " & receivedString)

                ss.WriteString("received ok")
            Catch e As IOException
                _tmplog("ServerThread__Session ERROR: " & e.Message)
            End Try

        Loop While Not threadKillSwitch
    End Sub

    Public Sub ServerThread__Logger(Data As Object)
        Dim pipeServer As New NamedPipeServerStream("ovcc_pipe_logger", PipeDirection.InOut, 1)
        Dim threadId As Integer = Thread.CurrentThread.ManagedThreadId
        Dim threadKillSwitch As Boolean = False

        _tmplog("ServerThread__Logger started")

        Do
            pipeServer.WaitForConnection()

            Try
                Dim ss As New StreamString(pipeServer)
                ss.WriteString(sSecretHandshake)
                Dim receivedString As String = ss.ReadString()

                _tmplog("ServerThread__Logger receivedString: " & receivedString)


            Catch e As IOException
                _tmplog("ServerThread__Logger ERROR: " & e.Message)
            End Try

        Loop While Not threadKillSwitch
    End Sub


    Private Sub _tmplog(s As String)
        'do stuff
        Me.Invoke(New Action(Of String)(AddressOf __tmplog), s)
        'do more stuff
    End Sub

    Public Sub __tmplog(s As String)
        Dim sDate As String = Now().ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")
        Using sw As New IO.StreamWriter("C:\Temp\pipe_server.txt", True)
            sw.WriteLine(sDate & " - " & s)
        End Using
    End Sub


    'Public Sub ServerThread(data As Object)
    '    Me._tmplog("ServerThread")
    '    Dim pipeServer As New NamedPipeServerStream(".", "testpipe", numThreads)

    '    Dim threadId As Integer = Thread.CurrentThread.ManagedThreadId

    '    Me.SetTextBox1Text_Threadsafe("Started thread #" & threadId)

    '    ' Wait for a client to connect
    '    pipeServer.WaitForConnection()

    '    SetTextBox1Text_Threadsafe("Client connected on thread #" & threadId)
    '    'Console.WriteLine("Client connected on thread[{0}].", threadId)
    '    Try
    '        ' Read the request from the client. Once the client has
    '        ' written to the pipe its security token will be available.

    '        Dim ss As New StreamString(pipeServer)

    '        ' Verify our identity to the connected client using a
    '        ' string that the client anticipates.

    '        ss.WriteString(sSecretHandshake)
    '        Dim receivedString As String = ss.ReadString()

    '        SetTextBox1Text_Threadsafe("receivedString: " & receivedString)

    '        '' Read in the contents of the file while impersonating the client.
    '        'Dim fileReader As New ReadFileToStream(ss, filename)

    '        '' Display the name of the user we are impersonating.
    '        'Console.WriteLine("Reading file: {0} on thread[{1}] as user: {2}.",
    '        '    filename, threadId, pipeServer.GetImpersonationUserName())
    '        'pipeServer.RunAsClient(AddressOf fileReader.Start)
    '        '' Catch the IOException that is raised if the pipe is broken
    '        '' or disconnected.
    '    Catch e As IOException
    '        SetTextBox1Text_Threadsafe("ERROR: " & e.Message)
    '    End Try
    '    pipeServer.Close()
    'End Sub

    Public Class StreamString
        Private ioStream As Stream
        Private streamEncoding As UnicodeEncoding

        Public Sub New(ioStream As Stream)
            Me.ioStream = ioStream
            streamEncoding = New UnicodeEncoding(False, False)
        End Sub

        Public Function ReadString() As String
            Return _StreamToString(ioStream)
            'Dim len As Integer = 0
            'len = CType(ioStream.ReadByte(), Integer) * 256
            'len += CType(ioStream.ReadByte(), Integer)
            'Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
            'ioStream.Read(inBuffer, 0, len)

            'Return streamEncoding.GetString(CType(inBuffer, Byte()))
        End Function

        Public Function WriteString(outString As String) As Integer
            Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
            ioStream.Write(outBuffer, 0, outBuffer.Length)
            'Dim len As Integer = outBuffer.Length
            'If len > UInt16.MaxValue Then
            '    len = CType(UInt16.MaxValue, Integer)
            'End If
            'ioStream.WriteByte(CType(len \ 256, Byte))
            'ioStream.WriteByte(CType(len And 255, Byte))
            'ioStream.Write(outBuffer, 0, outBuffer.Length)
            ioStream.Flush()

            Return outBuffer.Length + 2
        End Function

        Private Function _StreamToString(s As Stream) As String
            Dim strReader As New StreamReader(s, Encoding.Unicode)
            Return strReader.ReadToEnd
        End Function
    End Class

End Class
