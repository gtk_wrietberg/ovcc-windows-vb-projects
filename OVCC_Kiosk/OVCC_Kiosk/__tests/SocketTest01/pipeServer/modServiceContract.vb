﻿Imports System.ServiceModel

Module modServiceContract

    <ServiceContract>
    Public Interface IServiceCommunication
#Region "heartbeat"
        ''' <summary>
        ''' Sends heartbeat to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function InternalHeartbeat() As Boolean
#End Region

#Region "errors"
        ''' <summary>
        ''' Sends heartbeat to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function ClientError(ErrorString As String) As Boolean
#End Region

#Region "logon / logoff / shutdown"
        ''' <summary>
        ''' Signals logon to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function LogonTriggered() As Boolean

        ''' <summary>
        ''' Signals logout to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function LogoutTriggered() As Boolean

        ''' <summary>
        ''' Signals shutdown to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function ShutdownTriggered() As Boolean

        ''' <summary>
        ''' Signals suspend to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function SuspendTriggered() As Boolean
#End Region
    End Interface

End Module
