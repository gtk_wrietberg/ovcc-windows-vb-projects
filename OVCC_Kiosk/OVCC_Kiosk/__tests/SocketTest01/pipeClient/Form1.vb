﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Security.Principal
Imports System.Text
Imports System.Threading

Public Class Form1
    Private sSecretHandshake As String = "Hello!"

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        peop()
    End Sub

    Private Sub peop()
        Try
            Dim pipeClient As New NamedPipeClientStream(".", "ovcc_pipe_session", PipeDirection.InOut, PipeOptions.None) ', TokenImpersonationLevel.Impersonation)'


            _tmplog("Connecting to server...")
            pipeClient.Connect(2500)
            _tmplog("Connected to server...")

            Dim ss As New StreamString(pipeClient)

            _tmplog("handshake: " & ss.ReadString)
            ' Validate the server's signature string.
            If ss.ReadString().Equals(sSecretHandshake) Then
                ' The client security token is sent with the first write.
                ' Send the name of the file whose contents are returned
                ' by the server.
                ss.WriteString("response")

                ' Print the file to the screen.
                _tmplog(ss.ReadString())
            Else
                _tmplog("Server could not be verified.")
            End If

            _tmplog("Disconnecting from server...")
            pipeClient.Close()
        Catch ex As Exception
            _tmplog("NamedPipeClientStream ERROR: " & ex.Message)
        End Try

        ' Give the client process some time to display results before exiting.
        'Thread.Sleep(4000)
    End Sub


    Public Class StreamString
        Private ioStream As Stream
        Private streamEncoding As UnicodeEncoding

        Public Sub New(ioStream As Stream)
            Me.ioStream = ioStream
            streamEncoding = New UnicodeEncoding(False, False)
        End Sub

        Public Function ReadString() As String
            Return _StreamToString(ioStream)
            'Dim len As Integer = 0
            'len = CType(ioStream.ReadByte(), Integer) * 256
            'len += CType(ioStream.ReadByte(), Integer)
            'Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
            'ioStream.Read(inBuffer, 0, len)

            'Return streamEncoding.GetString(CType(inBuffer, Byte()))
        End Function

        Public Function WriteString(outString As String) As Integer
            Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
            Dim len As Integer = outBuffer.Length
            If len > UInt16.MaxValue Then
                len = CType(UInt16.MaxValue, Integer)
            End If
            ioStream.WriteByte(CType(len \ 256, Byte))
            ioStream.WriteByte(CType(len And 255, Byte))
            ioStream.Write(outBuffer, 0, outBuffer.Length)
            ioStream.Flush()

            Return outBuffer.Length + 2
        End Function

        Private Function _StreamToString(s As Stream) As String
            Dim strReader As New StreamReader(s, Encoding.Unicode)
            Return strReader.ReadToEnd
        End Function
    End Class


    Public Sub _tmplog(s As String)
        Dim sDate As String = Now().ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")
        Using sw As New IO.StreamWriter("C:\Temp\pipe_client.txt", True)
            sw.WriteLine(sDate & " - " & s)
        End Using
    End Sub
End Class
