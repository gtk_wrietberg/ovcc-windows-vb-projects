﻿Imports System.ServiceModel

Public Class ServiceCommunication_Server
    Private _host As ServiceHost
    Private mLastError As String = ""

    Public Property LastError() As String
        Get
            Return mLastError
        End Get
        Set(ByVal value As String)
            mLastError = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Function StartServer() As Boolean
        Try
            _host = New ServiceHost(GetType(ServiceCommunication_Implementation), New Uri() {New Uri(ServiceCommunication_Shared.Constants.c__PIPE_URI)})
            _host.AddServiceEndpoint(GetType(ServiceCommunication_Shared.IServiceCommunication), New NetNamedPipeBinding(), ServiceCommunication_Shared.Constants.c__PIPE_Endpoint)

            _host.Open()

            Return True
        Catch ex As Exception
            mLastError = ex.Message

            Return False
        End Try
    End Function

    Public Function StopServer() As Boolean
        Try
            _host.Close()

            Return True
        Catch ex As Exception
            mLastError = ex.Message

            Return False
        End Try
    End Function

    Public ReadOnly Property State() As CommunicationState
        Get
            Dim _s As CommunicationState

            Try
                _s = _host.State
            Catch ex As Exception
                _s = CommunicationState.Faulted
            End Try

            Return _s
        End Get
    End Property
End Class
