﻿Imports System.ServiceModel

Public Class Form1
    'Private hostWhistle As ServiceHost
    Private oServer As ServiceCommunication_Server

#Region "thread safe stuff"
    Delegate Sub SetTextBox1Text_Threadsafe_Callback(ByVal [text] As String)

    Public Sub SetTextBox1Text_Threadsafe(ByVal [text] As String)
        If Me.TextBox1.InvokeRequired Then
            Dim d As New SetTextBox1Text_Threadsafe_Callback(AddressOf SetTextBox1Text_Threadsafe)
            Me.Invoke(d, New Object() {[text]})
        Else
            TextBox1.AppendText([text])
        End If
    End Sub
#End Region

    Private bComm As Boolean = False

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        oServer = New ServiceCommunication_Server

        UpdateBUttons()

        AddText("started")
    End Sub

    Private Sub UpdateBUttons()
        UpdateBUttons(False)
    End Sub

    Private Sub UpdateBUttons(bForceOff As Boolean)
        If bForceOff Then
            Button1.Enabled = False
            Button2.Enabled = False

        Else
            Button1.Enabled = Not bComm
            Button2.Enabled = bComm
        End If
    End Sub

    Public Sub AddText(Text As String)
        SetTextBox1Text_Threadsafe(Text & vbCrLf)
    End Sub


#Region "service communication"
    Private Function OpenServiceCommunication() As Boolean
        Return oServer.StartServer

        'Try
        '    hostWhistle = New ServiceHost(GetType(ServiceCommunication), New Uri() {New Uri(OVCC_Constants.Pipe.URI)})
        '    hostWhistle.AddServiceEndpoint(GetType(IServiceCommunication), New NetNamedPipeBinding(), OVCC_Constants.Pipe.Endpoint)
        '    hostWhistle.Open()

        '    Return True
        'Catch ex As Exception
        '    Return False
        'End Try
    End Function

    Private Function CloseServiceCommunication() As Boolean
        Return oServer.StopServer

        'Try
        '    If Not hostWhistle Is Nothing Then
        '        hostWhistle.Close()
        '    End If

        '    Return True
        'Catch ex As Exception
        '    Return False
        'End Try
    End Function
#End Region

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        UpdateBUttons(True)

        bComm = OpenServiceCommunication()

        If Not bComm Then
            MsgBox(oServer.LastError)
        End If

        UpdateBUttons()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        UpdateBUttons(True)

        CloseServiceCommunication()

        bComm = False
        UpdateBUttons()

    End Sub

    Private Sub tmrState_Tick(sender As Object, e As EventArgs) Handles tmrState.Tick
        Try
            txtState.Text = oServer.State.ToString
        Catch ex As Exception
            txtState.Text = ex.Message
        End Try
    End Sub
End Class
