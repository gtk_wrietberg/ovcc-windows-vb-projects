﻿Imports System.ServiceModel

<ServiceBehavior()>
Public Class ServiceCommunication_Implementation
    Implements ServiceCommunication_Shared.IServiceCommunication
#Region "heartbeat"
    Public Function InternalHeartbeat() As String Implements ServiceCommunication_Shared.IServiceCommunication.InternalHeartbeat
        'Helpers.Logger.WriteMessage("heartbeat")

        Console.WriteLine("InternalHeartbeat!")

        Dim s As String = "heartbeat at: " & Now.ToLongTimeString

        'Form1.AddText(s)

        Return s
    End Function
#End Region

#Region "errors"
    Public Function ClientError(ErrorString As String) As String Implements ServiceCommunication_Shared.IServiceCommunication.ClientError
        Console.WriteLine("ClientError ('" & ErrorString & "')!")

        Return "ClientError ('" & ErrorString & "') at " & Now.ToLongTimeString
    End Function
#End Region

#Region "logon / logoff / shutdown"
    Public Function LogonTriggered() As String Implements ServiceCommunication_Shared.IServiceCommunication.LogonTriggered
        Console.WriteLine("LogonTriggered!")

        Return LogonTriggeredActions()
    End Function

    Public Function LogoutTriggered() As String Implements ServiceCommunication_Shared.IServiceCommunication.LogoutTriggered
        Console.WriteLine("LogoutTriggered!")

        Return LogoutTriggeredActions()
    End Function

    Public Function SuspendTriggered() As String Implements ServiceCommunication_Shared.IServiceCommunication.SuspendTriggered
        'Helpers.Logger.WriteMessage("SuspendTriggeredActions")

        Return "SuspendTriggered at: " & Now.ToLongTimeString
    End Function

    Public Function ShutdownTriggered() As String Implements ServiceCommunication_Shared.IServiceCommunication.ShutdownTriggered
        'Helpers.Logger.WriteMessage("ShutdownTriggeredActions")

        Return "ShutdownTriggered at: " & Now.ToLongTimeString
    End Function

    Public Function TextList() As ServiceCommunication_Shared.dc Implements ServiceCommunication_Shared.IServiceCommunication.TextList
        Dim _dc As New ServiceCommunication_Shared.dc

        _dc.Title = ServiceCommunication_Shared.Helpers.RandomString(10) & " " & Now.ToLongTimeString
        _dc.Message = ServiceCommunication_Shared.Helpers.RandomString(100) & " " & Now.ToLongTimeString


        Return _dc
    End Function
#End Region


    '========================================================================================================================================================================
    Public Function LogoutTriggeredActions() As String
        'Helpers.Logger.WriteMessage("Switching user profile")
        'Helpers.Logger.WriteMessage("active", 1)
        'Helpers.Logger.WriteMessage(clsSession.ActiveProfile.ToString, 2)

        'Helpers.Logger.WriteMessage("swapping", 1)
        'clsSession.SetNextProfile()

        'Helpers.Logger.WriteMessage("active", 1)
        'Helpers.Logger.WriteMessage(clsSession.ActiveProfile.ToString, 2)

        'Helpers.Registry.SetValue_Integer(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles, OVCC_Constants.Registry.VALUE__Profiles_Active, clsSession.ActiveProfile)

        Return "LogoutTriggeredActions at: " & Now.ToLongTimeString
    End Function

    Public Function LogonTriggeredActions() As String
        'Helpers.Logger.WriteMessage("Cleaning inactive profile")
        'If clsSession.CleanInactiveProfile() Then
        '    Helpers.Logger.WriteMessage("ok", 1)
        'Else
        '    Helpers.Logger.WriteError("fail", 1)
        '    Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
        'End If

        Return "LogonTriggeredActions at: " & Now.ToLongTimeString
    End Function
End Class
