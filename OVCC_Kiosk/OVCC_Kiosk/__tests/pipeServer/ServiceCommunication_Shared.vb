﻿Imports System.ServiceModel
Imports System.Runtime.Serialization

Public Class ServiceCommunication_Shared
    Public Class Constants
        Public Shared ReadOnly c__PIPE_URI As String = "net.pipe://localhost"
        Public Shared ReadOnly c__PIPE_Endpoint As String = "OVCCPipe"
    End Class

    Public Class Helpers
        Public Shared Function RandomInteger(min As Integer, max As Integer) As Integer
            Dim _r As New Random()

            Return _r.Next(min, max)
        End Function

        Public Shared Function RandomString(Length As Integer) As String
            Dim CharsToChooseFrom As String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = ""

            For i As Integer = 0 To Length - 1
                sTmp &= Mid(CharsToChooseFrom, RandomInteger(1, CharsToChooseFrom.Length + 1), 1)
            Next

            Return sTmp
        End Function
    End Class

#Region "interface"
    <ServiceContract>
    Public Interface IServiceCommunication
#Region "heartbeat"
        ''' <summary>
        ''' Sends heartbeat to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function InternalHeartbeat() As String
#End Region

#Region "errors"
        ''' <summary>
        ''' Sends client error message to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function ClientError(ErrorString As String) As String
        'Function ClientError(ErrorString As String) As PipeResponse
#End Region

#Region "logon / logoff / shutdown"
        ''' <summary>
        ''' Signals logon to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function LogonTriggered() As String

        ''' <summary>
        ''' Signals logout to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function LogoutTriggered() As String

        ''' <summary>
        ''' Signals shutdown to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function ShutdownTriggered() As String

        ''' <summary>
        ''' Signals suspend to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function SuspendTriggered() As String


        <OperationContract()>
        Function TextList() As dc
#End Region
    End Interface

    <DataContract(Namespace:="http://guesttek/services/contracts", Name:="dc")>
    Public Class dc
        Private mTitle As String
        Private mMessage As String

        <DataMember(Name:="Title", IsRequired:=True)>
        Public Property Title() As String
            Get
                Return mTitle
            End Get
            Set(ByVal value As String)
                mTitle = value
            End Set
        End Property

        <DataMember(Name:="Message", IsRequired:=True)>
        Public Property Message() As String
            Get
                Return mMessage
            End Get
            Set(ByVal value As String)
                mMessage = value
            End Set
        End Property
    End Class

#End Region
End Class
