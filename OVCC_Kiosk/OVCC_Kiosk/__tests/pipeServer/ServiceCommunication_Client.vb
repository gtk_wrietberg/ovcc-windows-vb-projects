﻿Imports System.ServiceModel

Public Class ServiceCommunication_Client
    Private Shared _pipeFactory As ChannelFactory(Of ServiceCommunication_Shared.IServiceCommunication)
    Private Shared _pipeProxy As ServiceCommunication_Shared.IServiceCommunication

    Private Shared mLastError As String
    Public Shared Property LastError() As String
        Get
            Return mLastError
        End Get
        Set(ByVal value As String)
            mLastError = value
        End Set
    End Property

    Public Shared Function OpenPipe(ByRef Response As String) As Boolean
        Dim bRet As Boolean = OpenPipe()

        If bRet Then
            Response = ""
        Else
            Response = mLastError
        End If

        Return bRet
    End Function

    Public Shared Function OpenPipe() As Boolean
        Try
            _pipeFactory = New ChannelFactory(Of ServiceCommunication_Shared.IServiceCommunication)(New NetNamedPipeBinding(), New EndpointAddress(ServiceCommunication_Shared.Constants.c__PIPE_URI & "/" & ServiceCommunication_Shared.Constants.c__PIPE_Endpoint))
            _pipeProxy = _pipeFactory.CreateChannel()

        Catch ex As Exception
            mLastError = ex.Message

            Return False
        End Try

        Return True
    End Function

    Public Enum ServiceCommunicationMessageType As Integer
        InternalHeartbeat = 0
        LogonDetected = 1
        LogoutDetected = 2
        ClientError = 3

    End Enum

    Public Shared Function SendMessage_TextList() As ServiceCommunication_Shared.dc
        Dim _dc As New ServiceCommunication_Shared.dc

        Try
            _dc = _pipeProxy.TextList
        Catch ex As Exception
            _dc.Title = "ERROR"
            _dc.Message = ex.Message
        End Try

        Return _dc
    End Function

    Public Shared Function SendMessage(MessageType As ServiceCommunicationMessageType) As String
        Return SendMessage(MessageType, "")
    End Function

    Public Shared Function SendMessage(MessageType As ServiceCommunicationMessageType, Param As String) As String
        Dim _response As String = ""

        Select Case MessageType
            Case ServiceCommunicationMessageType.InternalHeartbeat
                Try
                    _response = _pipeProxy.InternalHeartbeat()
                Catch ex As Exception
                    _response = "error: " & ex.Message
                End Try
            Case ServiceCommunicationMessageType.LogoutDetected
                Try
                    _response = _pipeProxy.LogoutTriggered()
                Catch ex As Exception
                    _response = "error: " & ex.Message
                End Try
            Case ServiceCommunicationMessageType.LogonDetected
                Try
                    _response = _pipeProxy.LogonTriggered()
                Catch ex As Exception
                    _response = "error: " & ex.Message
                End Try
            Case ServiceCommunicationMessageType.ClientError
                Try
                    _response = _pipeProxy.ClientError(Param)
                Catch ex As Exception
                    _response = "error: " & ex.Message
                End Try
            Case Else
                _response = "error: Incorrect message for service"
        End Select

        Return _response
    End Function
End Class
