﻿Imports System.ServiceModel

Public Class ServiceCommunication
#Region "constants"
    Public Shared ReadOnly c__PIPE_URI As String = "net.pipe://localhost"
    Public Shared ReadOnly c__PIPE_Endpoint As String = "OVCCPipe"
#End Region

#Region "interface"
    <ServiceContract>
    Public Interface IServiceCommunication
#Region "heartbeat"
        ''' <summary>
        ''' Sends heartbeat to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function InternalHeartbeat() As String
#End Region

#Region "errors"
        ''' <summary>
        ''' Sends client error message to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function ClientError(ErrorString As String) As String
        'Function ClientError(ErrorString As String) As PipeResponse
#End Region

#Region "logon / logoff / shutdown"
        ''' <summary>
        ''' Signals logon to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function LogonTriggered() As String

        ''' <summary>
        ''' Signals logout to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function LogoutTriggered() As String

        ''' <summary>
        ''' Signals shutdown to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function ShutdownTriggered() As String

        ''' <summary>
        ''' Signals suspend to service
        ''' </summary>
        ''' <returns>String ; "ok" if ok, anything else on error</returns>
        <OperationContract()>
        Function SuspendTriggered() As String
#End Region
    End Interface
#End Region

#Region "implementation"
    <ServiceBehavior()>
    Public Class Implementation
        Implements IServiceCommunication

#Region "heartbeat"
        Public Function InternalHeartbeat() As String Implements IServiceCommunication.InternalHeartbeat
            'Helpers.Logger.WriteMessage("heartbeat")

            Console.WriteLine("InternalHeartbeat!")

            Return "heartbeat at: " & Now.ToLongTimeString
        End Function
#End Region

#Region "errors"
        Public Function ClientError(ErrorString As String) As String Implements IServiceCommunication.ClientError
            Console.WriteLine("ClientError ('" & ErrorString & "')!")

            Return "ClientError ('" & ErrorString & "') at " & Now.ToLongTimeString
        End Function
#End Region

#Region "logon / logoff / shutdown"
        Public Function LogonTriggered() As String Implements IServiceCommunication.LogonTriggered
            Console.WriteLine("LogonTriggered!")

            Return LogonTriggeredActions()
        End Function

        Public Function LogoutTriggered() As String Implements IServiceCommunication.LogoutTriggered
            Console.WriteLine("LogoutTriggered!")

            Return LogoutTriggeredActions()
        End Function

        Public Function SuspendTriggered() As String Implements IServiceCommunication.SuspendTriggered
            'Helpers.Logger.WriteMessage("SuspendTriggeredActions")

            Return "SuspendTriggered at: " & Now.ToLongTimeString
        End Function

        Public Function ShutdownTriggered() As String Implements IServiceCommunication.ShutdownTriggered
            'Helpers.Logger.WriteMessage("ShutdownTriggeredActions")

            Return "ShutdownTriggered at: " & Now.ToLongTimeString
        End Function
#End Region


        '========================================================================================================================================================================
        Public Function LogoutTriggeredActions() As String
            'Helpers.Logger.WriteMessage("Switching user profile")
            'Helpers.Logger.WriteMessage("active", 1)
            'Helpers.Logger.WriteMessage(clsSession.ActiveProfile.ToString, 2)

            'Helpers.Logger.WriteMessage("swapping", 1)
            'clsSession.SetNextProfile()

            'Helpers.Logger.WriteMessage("active", 1)
            'Helpers.Logger.WriteMessage(clsSession.ActiveProfile.ToString, 2)

            'Helpers.Registry.SetValue_Integer(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles, OVCC_Constants.Registry.VALUE__Profiles_Active, clsSession.ActiveProfile)

            Return "LogoutTriggeredActions at: " & Now.ToLongTimeString
        End Function

        Public Function LogonTriggeredActions() As String
            'Helpers.Logger.WriteMessage("Cleaning inactive profile")
            'If clsSession.CleanInactiveProfile() Then
            '    Helpers.Logger.WriteMessage("ok", 1)
            'Else
            '    Helpers.Logger.WriteError("fail", 1)
            '    Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
            'End If

            Return "LogonTriggeredActions at: " & Now.ToLongTimeString
        End Function
    End Class
#End Region

#Region "server"
    Public Class Server
        Private _host As ServiceHost
        Private mLastError As String = ""

        Public Property LastError() As String
            Get
                Return mLastError
            End Get
            Set(ByVal value As String)
                mLastError = value
            End Set
        End Property

        Public Sub New()

        End Sub

        Public Function StartServer() As Boolean
            Try
                _host = New ServiceHost(GetType(Implementation), New Uri() {New Uri(c__PIPE_URI)})
                _host.AddServiceEndpoint(GetType(IServiceCommunication), New NetNamedPipeBinding(), c__PIPE_Endpoint)
                _host.Open()

                Return True
            Catch ex As Exception
                mLastError = ex.Message

                Return False
            End Try
        End Function

        Public Function StopServer() As Boolean
            Try
                _host.Close()

                Return True
            Catch ex As Exception
                mLastError = ex.Message

                Return False
            End Try
        End Function

        Public ReadOnly Property State() As CommunicationState
            Get
                Dim _s As CommunicationState

                Try
                    _s = _host.State
                Catch ex As Exception
                    _s = CommunicationState.Faulted
                End Try

                Return _s
            End Get
        End Property
    End Class
#End Region

#Region "client"
    Public Class Client
        Private Shared _pipeFactory As ChannelFactory(Of IServiceCommunication)
        Private Shared _pipeProxy As IServiceCommunication

        Private Shared mLastError As String
        Public Shared Property LastError() As String
            Get
                Return mLastError
            End Get
            Set(ByVal value As String)
                mLastError = value
            End Set
        End Property

        Public Shared Function OpenPipe(ByRef Response As String) As Boolean
            Dim bRet As Boolean = OpenPipe()

            If bRet Then
                Response = ""
            Else
                Response = mLastError
            End If

            Return bRet
        End Function

        Public Shared Function OpenPipe() As Boolean
            Try
                _pipeFactory = New ChannelFactory(Of IServiceCommunication)(New NetNamedPipeBinding(), New EndpointAddress(c__PIPE_URI & "/" & c__PIPE_Endpoint))
                _pipeProxy = _pipeFactory.CreateChannel()
            Catch ex As Exception
                mLastError = ex.Message

                Return False
            End Try

            Return True
        End Function

        Public Enum ServiceCommunicationMessageType As Integer
            InternalHeartbeat = 0
            LogonDetected = 1
            LogoutDetected = 2
            ClientError = 3

        End Enum

        Public Shared Function SendMessage(MessageType As ServiceCommunicationMessageType) As String
            Return SendMessage(MessageType, "")
        End Function

        Public Shared Function SendMessage(MessageType As ServiceCommunicationMessageType, Param As String) As String
            Dim _response As String = ""

            Select Case MessageType
                Case ServiceCommunicationMessageType.InternalHeartbeat
                    Try
                        _response = _pipeProxy.InternalHeartbeat()
                    Catch ex As Exception
                        _response = "error: " & ex.Message
                    End Try
                Case ServiceCommunicationMessageType.LogoutDetected
                    Try
                        _response = _pipeProxy.LogoutTriggered()
                    Catch ex As Exception
                        _response = "error: " & ex.Message
                    End Try
                Case ServiceCommunicationMessageType.LogonDetected
                    Try
                        _response = _pipeProxy.LogonTriggered()
                    Catch ex As Exception
                        _response = "error: " & ex.Message
                    End Try
                Case ServiceCommunicationMessageType.ClientError
                    Try
                        _response = _pipeProxy.ClientError(Param)
                    Catch ex As Exception
                        _response = "error: " & ex.Message
                    End Try
                Case Else
                    _response = "error: Incorrect message for service"
            End Select

            Return _response
        End Function
    End Class

#End Region
End Class
