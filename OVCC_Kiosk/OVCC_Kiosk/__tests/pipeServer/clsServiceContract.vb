﻿Public Class ServiceCommunication
    Implements IServiceCommunication

#Region "events"
    Public Event Event_InternalHeartbeat(ByVal sender As Object, ByVal e As ServiceCommunicationEventArgs)
    Public Event Event_ClientError(ByVal sender As Object, ByVal e As ServiceCommunicationEventArgs)
    Public Event Event_LogonTriggered(ByVal sender As Object, ByVal e As ServiceCommunicationEventArgs)
    Public Event Event_LogoutTriggered(ByVal sender As Object, ByVal e As ServiceCommunicationEventArgs)

    Public Sub _Event_InternalHeartbeat()
        RaiseEvent Event_InternalHeartbeat(Me, New ServiceCommunicationEventArgs)
    End Sub

    Public Sub _Event_ClientError(ErrorString As String)
        Dim oServiceCommunicationEventArgs As New ServiceCommunicationEventArgs

        oServiceCommunicationEventArgs.Message = ErrorString

        RaiseEvent Event_ClientError(Me, oServiceCommunicationEventArgs)
    End Sub

    Public Sub _Event_LogonTriggered()
        RaiseEvent Event_LogonTriggered(Me, New ServiceCommunicationEventArgs)
    End Sub

    Public Sub _Event_LogoutTriggered()
        RaiseEvent Event_LogoutTriggered(Me, New ServiceCommunicationEventArgs)
    End Sub

    Private Delegate Sub Raise_Event_InternalHeartbeat(ByVal args As ServiceCommunicationEventArgs)
    Private Delegate Sub Raise_Event_ClientError(ByVal args As ServiceCommunicationEventArgs)
    Private Delegate Sub Raise_Event_LogonTriggered(ByVal args As ServiceCommunicationEventArgs)
    Private Delegate Sub Raise_Event_LogoutTriggered(ByVal args As ServiceCommunicationEventArgs)

    Public Class ServiceCommunicationEventArgs
        Inherits EventArgs

        Public Property Code As Integer = 0
        Public Property Message As String = ""
    End Class
#End Region



#Region "heartbeat"
    Public Function InternalHeartbeat() As Boolean Implements IServiceCommunication.InternalHeartbeat
        Dim _eventArgs As New ServiceCommunicationEventArgs

        RaiseEvent Event_InternalHeartbeat(Me, _eventArgs)
        'Helpers.Logger.WriteMessage("heartbeat")

        Console.WriteLine("InternalHeartbeat!")

        Return True
    End Function
#End Region

#Region "errors"
    Public Function ClientError(ErrorString As String) As Boolean Implements IServiceCommunication.ClientError
        Dim _eventArgs As New ServiceCommunicationEventArgs

        _eventArgs.Message = ErrorString

        RaiseEvent Event_ClientError(Me, _eventArgs)
        'Helpers.Logger.WriteMessage("client error: " & ErrorString)

        Console.WriteLine("ClientError ('" & _eventArgs.Message & "')!")

        Return True
    End Function
#End Region

#Region "logon / logoff / shutdown"
    Public Function LogonTriggered() As Boolean Implements IServiceCommunication.LogonTriggered
        Dim _eventArgs As New ServiceCommunicationEventArgs

        RaiseEvent Event_LogonTriggered(Me, _eventArgs)

        Console.WriteLine("LogonTriggered!")

        Return LogonTriggeredActions()
    End Function

    Public Function LogoutTriggered() As Boolean Implements IServiceCommunication.LogoutTriggered
        Dim _eventArgs As New ServiceCommunicationEventArgs

        RaiseEvent Event_LogoutTriggered(Me, _eventArgs)

        Console.WriteLine("LogoutTriggered!")

        Return LogoutTriggeredActions()
    End Function

    Public Function SuspendTriggered() As Boolean Implements IServiceCommunication.SuspendTriggered
        'Helpers.Logger.WriteMessage("SuspendTriggeredActions")

        Return True
    End Function

    Public Function ShutdownTriggered() As Boolean Implements IServiceCommunication.ShutdownTriggered
        'Helpers.Logger.WriteMessage("ShutdownTriggeredActions")

        Return True
    End Function
#End Region


    '========================================================================================================================================================================
    Public Function LogoutTriggeredActions() As Boolean
        'Helpers.Logger.WriteMessage("Switching user profile")
        'Helpers.Logger.WriteMessage("active", 1)
        'Helpers.Logger.WriteMessage(clsSession.ActiveProfile.ToString, 2)

        'Helpers.Logger.WriteMessage("swapping", 1)
        'clsSession.SetNextProfile()

        'Helpers.Logger.WriteMessage("active", 1)
        'Helpers.Logger.WriteMessage(clsSession.ActiveProfile.ToString, 2)

        'Helpers.Registry.SetValue_Integer(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles, OVCC_Constants.Registry.VALUE__Profiles_Active, clsSession.ActiveProfile)


        Return True
    End Function

    Public Function LogonTriggeredActions() As Boolean
        'Helpers.Logger.WriteMessage("Cleaning inactive profile")
        'If clsSession.CleanInactiveProfile() Then
        '    Helpers.Logger.WriteMessage("ok", 1)
        'Else
        '    Helpers.Logger.WriteError("fail", 1)
        '    Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
        'End If

        Return True
    End Function
End Class
