﻿Imports Microsoft.Web.WebView2.Core
Imports Newtonsoft.Json.Linq

Public Class Form1
    Private WithEvents corewebview2_Main As CoreWebView2


#Region "Thread Safe stuff"
    Delegate Sub DebugToTextBoxCallback(ByVal [text] As String)

    Public Sub DebugToTextBox(ByVal [text] As String)
        If Me.txtDebug.InvokeRequired Then
            Dim d As New DebugToTextBoxCallback(AddressOf DebugToTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtDebug.AppendText([text] & vbCrLf)
        End If
    End Sub
#End Region



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' MsgBox("Form1_Load")
        Dim j As JObject


        'WebView21.EnsureCoreWebView2Async()
        DebugToTextBox("Form1_Load - 1")
        innit()
        DebugToTextBox("Form1_Load - 2")

        'WebView21.CoreWebView2 = New Microsoft.Web.WebView2.Core.CoreWebView2()
    End Sub

    Private Async Sub innit()
        DebugToTextBox("innit - 1")
        Await WebView21.EnsureCoreWebView2Async()
        DebugToTextBox("innit - 2")

        corewebview2_Main = WebView21.CoreWebView2

        DebugToTextBox("innit - 3")
        WebView21.CoreWebView2.AddHostObjectToScript("test", New TestInterface())
        DebugToTextBox("innit - 4")
        'WebView21.Source = New Uri("file:///C:\Program Files\GuestTek\OVCC\Kiosk\layout\themes\__test\index.webview.html")
        ' WebView21.CoreWebView2.Navigate("file:///C:\Program Files\GuestTek\OVCC\Kiosk\layout\themes\__test\index.webview.html")
        WebView21.CoreWebView2.Navigate("https://www.guesttek.com/hotel-staff-support/")
        DebugToTextBox("innit - 5")

        WebView21.CoreWebView2.Settings.AreDevToolsEnabled = False
        WebView21.CoreWebView2.Settings.AreDefaultContextMenusEnabled = False


        DebugToTextBox("innit - 6")
    End Sub

    Private Sub WebView21_CoreWebView2InitializationCompleted(sender As Object, e As CoreWebView2InitializationCompletedEventArgs) Handles WebView21.CoreWebView2InitializationCompleted

    End Sub

    Private Sub WebView21_NavigationCompleted(sender As Object, e As CoreWebView2NavigationCompletedEventArgs) Handles WebView21.NavigationCompleted
        'MsgBox("WebView21_NavigationCompleted: " & e.ToString)
    End Sub

    Private Sub WebView21_Click(sender As Object, e As EventArgs) Handles WebView21.Click
        DebugToTextBox("WebView21_Click")
    End Sub

    Private Sub WebView21_ContextMenuStripChanged(sender As Object, e As EventArgs) Handles WebView21.QueryAccessibilityHelp

    End Sub

    Private Sub corewebview2_Main_DOMContentLoaded(sender As Object, e As CoreWebView2DOMContentLoadedEventArgs) Handles corewebview2_Main.DOMContentLoaded

        DebugToTextBox("corewebview2_Main_DOMContentLoaded: " & WebView21.CoreWebView2.Source)
    End Sub

    Private Sub WebView21_MouseClick(sender As Object, e As MouseEventArgs) Handles WebView21.MouseClick
        DebugToTextBox("Mouseclick: x=" & e.X & " - y=" & e.Y)
    End Sub
End Class
