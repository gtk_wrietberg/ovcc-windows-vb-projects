﻿
Imports System.Management

Public Class Form1
    Delegate Sub ClearProcessTextBoxCallback()
    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Public Sub ClearProcessTextBox1()
        If Me.TextBox1.InvokeRequired Then
            Dim d As New ClearProcessTextBoxCallback(AddressOf ClearProcessTextBox1)
            Me.Invoke(d, New Object() {})
        Else
            Me.TextBox1.Clear()
        End If
    End Sub

    Public Sub ClearProcessTextBox2()
        If Me.TextBox2.InvokeRequired Then
            Dim d As New ClearProcessTextBoxCallback(AddressOf ClearProcessTextBox2)
            Me.Invoke(d, New Object() {})
        Else
            Me.TextBox2.Clear()
        End If
    End Sub

    Public Sub ClearProcessTextBox3()
        If Me.TextBox3.InvokeRequired Then
            Dim d As New ClearProcessTextBoxCallback(AddressOf ClearProcessTextBox3)
            Me.Invoke(d, New Object() {})
        Else
            Me.TextBox3.Clear()
        End If
    End Sub

    Public Sub LogToProcessTextBox1(ByVal [text] As String)
        If Me.TextBox1.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox1)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.TextBox1.AppendText([text])
        End If
    End Sub

    Public Sub LogToProcessTextBox2(ByVal [text] As String)
        If Me.TextBox2.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox2)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.TextBox2.AppendText([text])
        End If
    End Sub

    Public Sub LogToProcessTextBox3(ByVal [text] As String)
        If Me.TextBox3.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox3)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.TextBox3.AppendText([text])
        End If
    End Sub

    Public Class ProcessCompare
        Public Class Pr0c3ss
            Public Sub New()
                mId = -1
                mName = ""
                mParentId = -1
                mParentName = ""
                mUsername = ""
            End Sub

            Public Sub Clear()
                mId = 0
                mName = ""
                mParentId = 0
                mParentName = ""
                mUsername = ""
            End Sub

            Public Sub New(Id As Integer, Name As String, Username As String, ParentId As Integer, ParentName As String)
                mId = Id
                mName = Name
                mParentId = ParentId
                mParentName = ParentName
                mUsername = Username
            End Sub

            Private mId As Integer
            Public Property Id() As Integer
                Get
                    Return mId
                End Get
                Set(ByVal value As Integer)
                    mId = value
                End Set
            End Property

            Private mName As String
            Public Property Name() As String
                Get
                    Return mName
                End Get
                Set(ByVal value As String)
                    mName = value
                End Set
            End Property

            Private mParentId As Integer
            Public Property ParentId() As Integer
                Get
                    Return mParentId
                End Get
                Set(ByVal value As Integer)
                    mParentId = value
                End Set
            End Property

            Private mParentName As String
            Public Property ParentName() As String
                Get
                    Return mParentName
                End Get
                Set(ByVal value As String)
                    mParentName = value
                End Set
            End Property

            Private mUsername As String
            Public Property Username() As String
                Get
                    Return mUsername
                End Get
                Set(ByVal value As String)
                    mUsername = value
                End Set
            End Property

            Public Overrides Function ToString() As String
                Return mId & "," & mName & "," & mUsername & "," & mParentId & "," & mParentName
            End Function
        End Class

        Private Shared mPrevious As New List(Of Pr0c3ss)
        Private Shared mCurrent As New List(Of Pr0c3ss)

        Public Shared Sub LoadProcesses()
            LoadProcesses("")
        End Sub

        Public Shared Sub LoadProcesses(Username As String)
            Username = Username.ToLower

            Dim tmpId As Integer = 0, tmpName As String = "", tmpOwner As String = ""

            mPrevious.Clear()
            mPrevious.AddRange(mCurrent)

            mCurrent.Clear()

            For Each p As Process In Process.GetProcesses
                If _GetParentProcess(p.Id, tmpId, tmpName) Then

                End If

                tmpOwner = GetProcessOwner(p.Id)
                'tmpOwner = GetUserName(p.Id)


                'If (Not Username.Equals("") And p.StartInfo.EnvironmentVariables.Item("username").ToLower.Equals(Username)) Or Username.Equals("") Then
                '    mCurrent.Add(New Pr0c3ss(p.Id, p.ProcessName, p.StartInfo.EnvironmentVariables.Item("username"), tmpId, tmpName))
                'End If

                If Not Username.Equals("") Then
                    If tmpOwner.ToLower.Equals(Username) Then
                        mCurrent.Add(New Pr0c3ss(p.Id, p.ProcessName, tmpOwner, tmpId, tmpName))
                    Else
                        'skipped! not selected user
                    End If
                Else
                    mCurrent.Add(New Pr0c3ss(p.Id, p.ProcessName, tmpOwner, tmpId, tmpName))
                End If
            Next
        End Sub

        Public Shared Function GetProcessOwner(processId As Integer) As String
            Dim sReturn As String = "_NO_OWNER_"

            Try
                Dim query As String = "Select * From Win32_Process Where ProcessID = " & processId.ToString
                Dim searcher As New ManagementObjectSearcher(query)
                Dim processList As ManagementObjectCollection = searcher.[Get]()

                For Each obj As ManagementObject In processList
                    Dim argList As String() = New String() {String.Empty, String.Empty}
                    Dim returnVal As Integer = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList))
                    If returnVal = 0 Then
                        ' argList(0) == User
                        ' argList(1) == DOMAIN
                        sReturn = argList(0)

                        Exit For
                    End If
                Next
            Catch ex As Exception
                sReturn = ex.Message
            End Try

            Return sReturn
        End Function

        Public Shared Function GetUserName(processId As Integer) As String
            'Dim selectQuery As SelectQuery = New SelectQuery("Win32_Process")
            Dim selectQuery As SelectQuery = New SelectQuery("Select * From Win32_Process Where ProcessID = " & processId.ToString)
            Dim searcher As ManagementObjectSearcher = New ManagementObjectSearcher(selectQuery)
            Dim processList As ManagementObjectCollection = searcher.[Get]()

            For Each proc As ManagementObject In processList
                Dim s(1) As String
                proc.InvokeMethod("GetOwner", CType(s, Object()))
                Return ("User (" & processList.Count & "): " & s(1) & "\\" & s(0))
            Next

            Return "OOPS"
        End Function

        Public Shared Function GetCurrentProcesses() As List(Of Pr0c3ss)
            Return mCurrent
        End Function

        Public Shared Function GetPreviousProcesses() As List(Of Pr0c3ss)
            Return mPrevious
        End Function


        Public Shared Function Compare() As List(Of Pr0c3ss)
            Dim lCompare As New List(Of Pr0c3ss)
            Dim bMatch As Boolean = False


            For iCurrent As Integer = 0 To mCurrent.Count - 1
                bMatch = False

                For iPrevious As Integer = 0 To mPrevious.Count - 1
                    If mPrevious.Item(iPrevious).Id = mCurrent.Item(iCurrent).Id Then
                        bMatch = True

                        Exit For
                    End If
                Next

                If Not bMatch Then
                    lCompare.Add(mCurrent.Item(iCurrent))
                End If
            Next

            Return lCompare
        End Function

        Private Shared Function _GetParentProcess(Id As Integer, ByRef ParentId As Integer, ByRef ParentName As String) As Boolean
            Try
                Dim pp As Process

                pp = ParentProcess.ParentProcessUtilities.GetParentProcess(Id)

                ParentId = pp.Id
                ParentName = pp.ProcessName
            Catch ex As Exception
                ParentId = -1
                ParentName = ex.Message
            End Try

            Return True
        End Function
    End Class

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        'TextBox1.Clear()

        'Dim s As String = ""
        'Dim pp As Process

        's = ""
        's &= "id,"
        's &= "name,"
        's &= "parent_id,"
        's &= "parent_name"

        's &= vbCrLf

        'For Each p As Process In Process.GetProcesses()
        '    s = ""
        '    s &= p.Id & ","
        '    s &= p.ProcessName & ","

        '    Try
        '        pp = ParentProcess.ParentProcessUtilities.GetParentProcess(p.Id)

        '        If Not pp Is Nothing Then
        '            s &= pp.Id & ","
        '            s &= pp.ProcessName
        '        Else
        '            s &= "-1,"
        '            s &= "(nothing)"
        '        End If
        '    Catch ex As Exception
        '        s &= "-1,"
        '        s &= ex.Message
        '    End Try

        '    s &= vbCrLf

        '    TextBox1.AppendText(s)
        'Next




        'Dim p As New Process

        'p = ParentProcess.ParentProcessUtilities.GetParentProcess(CInt(TextBox1.Text))

        'If Not p Is Nothing Then
        '    MsgBox("Parent: id=" & p.Id & " ; name=" & p.ProcessName)

        'Else

        '    MsgBox("ophan")
        'End If
    End Sub

    Private Sub Blablablabla()
        ProcessCompare.LoadProcesses("Wouter")

        ClearProcessTextBox1()
        ClearProcessTextBox2()
        ClearProcessTextBox3()

        For Each p As ProcessCompare.Pr0c3ss In ProcessCompare.GetPreviousProcesses
            LogToProcessTextBox1(p.ToString & vbCrLf)
        Next

        For Each p As ProcessCompare.Pr0c3ss In ProcessCompare.GetCurrentProcesses
            LogToProcessTextBox2(p.ToString & vbCrLf)
        Next






        Dim tmpList As New List(Of ProcessCompare.Pr0c3ss)

        tmpList = ProcessCompare.Compare

        For Each p As ProcessCompare.Pr0c3ss In tmpList
            LogToProcessTextBox3(p.ToString & vbCrLf)
        Next

    End Sub

    Private Sub bgworkerProcesses_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworkerProcesses.DoWork
        Blablablabla()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        bgworkerProcesses.RunWorkerAsync()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

    End Sub
End Class
