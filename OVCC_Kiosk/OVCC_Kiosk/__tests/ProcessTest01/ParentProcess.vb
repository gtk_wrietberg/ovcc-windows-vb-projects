﻿Imports System.Runtime.InteropServices

Public Class ParentProcess

    ''' <summary>
    ''' A utility class to determine a process parent.
    ''' </summary>
    <StructLayout(LayoutKind.Sequential)>
    Public Structure ParentProcessUtilities
        ' These members must match PROCESS_BASIC_INFORMATION
        Friend Reserved1 As IntPtr
        Friend PebBaseAddress As IntPtr
        Friend Reserved2_0 As IntPtr
        Friend Reserved2_1 As IntPtr
        Friend UniqueProcessId As IntPtr
        Friend InheritedFromUniqueProcessId As IntPtr

        <DllImport("ntdll.dll")>
        Private Shared Function NtQueryInformationProcess(processHandle As IntPtr, processInformationClass As Integer, ByRef processInformation As ParentProcessUtilities, processInformationLength As Integer, ByRef returnLength As Integer) As Integer
        End Function

        ''' <summary>
        ''' Gets the parent process of the current process.
        ''' </summary>
        ''' <returns>An instance of the Process class.</returns>
        Public Shared Function GetParentProcess() As Process
            Return GetParentProcess(Process.GetCurrentProcess().Handle)
        End Function

        ''' <summary>
        ''' Gets the parent process of specified process.
        ''' </summary>
        ''' <param name="id">The process id.</param>
        ''' <returns>An instance of the Process class.</returns>
        Public Shared Function GetParentProcess(id As Integer) As Process
            Dim process__1 As Process = Process.GetProcessById(id)

            Return GetParentProcess(process__1.Handle)
        End Function

        ''' <summary>
        ''' Gets the parent process of a specified process.
        ''' </summary>
        ''' <param name="handle">The process handle.</param>
        ''' <returns>An instance of the Process class or null if an error occurred.</returns>
        Public Shared Function GetParentProcess(handle As IntPtr) As Process
            Dim pbi As New ParentProcessUtilities()
            Dim returnLength As Integer
            Dim status As Integer = NtQueryInformationProcess(handle, 0, pbi, Marshal.SizeOf(pbi), returnLength)

            If status <> 0 Then
                Return Nothing
            End If

            Try
                Return Process.GetProcessById(pbi.InheritedFromUniqueProcessId.ToInt32())
            Catch generatedExceptionName As ArgumentException
                ' not found


                Return Nothing
            End Try
        End Function
    End Structure
End Class
