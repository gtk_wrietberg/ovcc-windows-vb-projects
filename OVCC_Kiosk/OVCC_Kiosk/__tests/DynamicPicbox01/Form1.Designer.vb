﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        btnAdd = New Button()
        btnRemove = New Button()
        lblClicked = New Label()
        imglistLanguages = New ImageList(components)
        lblHover = New Label()
        pnlFlags = New Panel()
        Panel1 = New Panel()
        Panel1.SuspendLayout()
        SuspendLayout()
        ' 
        ' btnAdd
        ' 
        btnAdd.Location = New Point(108, 34)
        btnAdd.Name = "btnAdd"
        btnAdd.Size = New Size(87, 27)
        btnAdd.TabIndex = 0
        btnAdd.Text = "add"
        btnAdd.UseVisualStyleBackColor = True
        ' 
        ' btnRemove
        ' 
        btnRemove.Location = New Point(108, 67)
        btnRemove.Name = "btnRemove"
        btnRemove.Size = New Size(87, 27)
        btnRemove.TabIndex = 1
        btnRemove.Text = "remove"
        btnRemove.UseVisualStyleBackColor = True
        ' 
        ' lblClicked
        ' 
        lblClicked.BackColor = Color.WhiteSmoke
        lblClicked.BorderStyle = BorderStyle.FixedSingle
        lblClicked.Location = New Point(40, 97)
        lblClicked.Name = "lblClicked"
        lblClicked.Size = New Size(155, 22)
        lblClicked.TabIndex = 2
        lblClicked.Text = "Label1"
        lblClicked.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' imglistLanguages
        ' 
        imglistLanguages.ColorDepth = ColorDepth.Depth8Bit
        imglistLanguages.ImageSize = New Size(32, 20)
        imglistLanguages.TransparentColor = Color.Transparent
        ' 
        ' lblHover
        ' 
        lblHover.BackColor = Color.WhiteSmoke
        lblHover.BorderStyle = BorderStyle.FixedSingle
        lblHover.Location = New Point(40, 133)
        lblHover.Name = "lblHover"
        lblHover.Size = New Size(155, 22)
        lblHover.TabIndex = 3
        lblHover.Text = "Label1"
        lblHover.TextAlign = ContentAlignment.MiddleCenter
        ' 
        ' pnlFlags
        ' 
        pnlFlags.BackColor = Color.WhiteSmoke
        pnlFlags.BorderStyle = BorderStyle.FixedSingle
        pnlFlags.Location = New Point(79, 55)
        pnlFlags.Name = "pnlFlags"
        pnlFlags.Size = New Size(240, 303)
        pnlFlags.TabIndex = 4
        ' 
        ' Panel1
        ' 
        Panel1.BackColor = Color.WhiteSmoke
        Panel1.BorderStyle = BorderStyle.FixedSingle
        Panel1.Controls.Add(btnAdd)
        Panel1.Controls.Add(btnRemove)
        Panel1.Controls.Add(lblHover)
        Panel1.Controls.Add(lblClicked)
        Panel1.Location = New Point(534, 160)
        Panel1.Name = "Panel1"
        Panel1.Size = New Size(227, 198)
        Panel1.TabIndex = 5
        ' 
        ' Form1
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Lime
        ClientSize = New Size(800, 450)
        Controls.Add(Panel1)
        Controls.Add(pnlFlags)
        Name = "Form1"
        Text = "Form1"
        Panel1.ResumeLayout(False)
        ResumeLayout(False)
    End Sub

    Friend WithEvents btnAdd As Button
    Friend WithEvents btnRemove As Button
    Friend WithEvents lblClicked As Label
    Friend WithEvents imglistLanguages As ImageList
    Friend WithEvents lblHover As Label
    Friend WithEvents pnlFlags As Panel
    Friend WithEvents Panel1 As Panel

End Class
