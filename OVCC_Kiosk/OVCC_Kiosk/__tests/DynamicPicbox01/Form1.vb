﻿Public Class Form1
    Private ReadOnly mPicBoxWidth As Integer = 32
    Private ReadOnly mPicBoxHeight As Integer = 20
    Private ReadOnly mPicBoxMargin As Integer = 6

    Private ReadOnly mLabelWidth As Integer = 240
    Private ReadOnly mLabelHeight As Integer = 18

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime



        Dim dImages As String() = IO.Directory.GetFiles("C:\Program Files\GuestTek\OVCC\Kiosk\layout\images\flags\rectangle", "*.png")

        imglistLanguages.Images.Clear()

        For i As Integer = 0 To dImages.Length - 1
            Dim img_ As Image = New Bitmap(dImages(i))

            imglistLanguages.Images.Add("flag_" & i.ToString, img_)
        Next



        RemovePicboxes()
        AddPicboxes()
    End Sub

    Private Sub AddPicboxes()
        pnlFlags.Width = 240
        pnlFlags.Height = mPicBoxMargin + (mPicBoxHeight + mLabelHeight + mPicBoxMargin) * (6 + 1)

        For i As Integer = 0 To 6
            Dim tmpPic As New PictureBox
            tmpPic.BorderStyle = BorderStyle.None
            tmpPic.Width = mPicBoxWidth
            tmpPic.Height = mPicBoxHeight
            tmpPic.Name = "picLanguage_" & i.ToString
            tmpPic.Top = mPicBoxMargin + (mPicBoxHeight + mLabelHeight + mPicBoxMargin) * i
            tmpPic.Left = (pnlFlags.Width - mPicBoxWidth) / 2
            tmpPic.Image = imglistLanguages.Images.Item(i)
            ' AddHandler tmpPic.MouseClick, AddressOf picbox_Click


            Dim tmpLabel As New Label
            tmpLabel.AutoSize = True
            tmpLabel.BackColor = Color.Transparent
            tmpLabel.Text = "Language " & (New String("qwe", i + 1))
            tmpLabel.Name = "lblLanguage_" & i.ToString
            tmpLabel.TextAlign = ContentAlignment.TopCenter
            tmpLabel.BorderStyle = BorderStyle.FixedSingle
            'AddHandler tmpLabel.MouseClick, AddressOf label_Click


            pnlFlags.Controls.Add(tmpPic)
            pnlFlags.Controls.Add(tmpLabel)


            tmpLabel.Top = mPicBoxMargin + (tmpPic.Height) * (i + 1) + (mLabelHeight + mPicBoxMargin) * i
            tmpLabel.Left = tmpPic.Left - (tmpLabel.Width - tmpPic.Width) / 2
        Next

        AddHandlers()

        Me.Refresh()
    End Sub

    Private Sub AddHandlers()
        For Each c As Control In pnlFlags.Controls
            If c.Name.StartsWith("picLanguage_") Then
                AddHandler c.MouseClick, AddressOf picbox_Click
            End If
            If c.Name.StartsWith("lblLanguage_") Then
                AddHandler c.MouseClick, AddressOf label_Click
            End If
        Next
    End Sub

    Private Sub RemovePicboxes()
        Dim lDelete As New List(Of Control)
        For Each c As Control In pnlFlags.Controls
            If c.Name.StartsWith("picLanguage_") Or c.Name.StartsWith("lblLanguage_") Then
                lDelete.Add(c)
            End If
        Next
        For Each c As Control In lDelete
            c.Dispose()
        Next
    End Sub


    Private Sub picbox_Click(sender As Object, e As EventArgs)
        Dim PictureBox_ As PictureBox = CType(sender, PictureBox)
        Dim PictureBoxName = PictureBox_.Name

        lblClicked.Text = PictureBoxName
    End Sub

    Private Sub label_Click(sender As Object, e As EventArgs)
        Dim Label_ As Label = CType(sender, Label)
        Dim LabelName = Label_.Name

        lblClicked.Text = LabelName
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        RemovePicboxes()
        AddPicboxes()
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        RemovePicboxes()
    End Sub

    Private Sub Form1_MouseHover(sender As Object, e As EventArgs) Handles Me.MouseHover
        lblHover.Text = "HOVER"
    End Sub

    Private Sub Form1_MouseLeave(sender As Object, e As EventArgs) Handles Me.MouseLeave
        lblHover.Text = "leave"
    End Sub
End Class
