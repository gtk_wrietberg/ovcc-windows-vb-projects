﻿Imports System.Management
Imports System.Text

Public Class Form2
    Private pidExplorer As Integer = -1

#Region "thread safe stuff"
    Delegate Sub SetTextBox1Text_Threadsafe_Callback(ByVal [text] As String)

    Public Sub SetTextBox1Text_Threadsafe(ByVal [text] As String)
        If Me.TextBox1.InvokeRequired Then
            Dim d As New SetTextBox1Text_Threadsafe_Callback(AddressOf SetTextBox1Text_Threadsafe)
            Me.Invoke(d, New Object() {[text]})
        Else
            TextBox1.AppendText([text])
        End If
    End Sub
#End Region

    'Private WithEvents ProcessStartWatcher As New ManagementEventWatcher(New WqlEventQuery("SELECT * FROM Win32_ProcessStartTrace"))
    'Private WithEvents ProcessStopWatcher As New ManagementEventWatcher(New WqlEventQuery("SELECT * FROM Win32_ProcessStopTrace"))
    Private WithEvents ProcessStartWatcher As ManagementEventWatcher
    Private WithEvents ProcessStopWatcher As ManagementEventWatcher


    'String queryString = "SELECT * FROM __InstanceCreationEvent WITHIN .025 WHERE TargetInstance ISA 'Win32_Process'";
    'processWatcher = New ManagementEventWatcher(@"\\.\root\CIMV2", queryString);
    'processWatcher.EventArrived += ProcessStartHandler;
    'processWatcher.Start();


    Private Sub Form2_Load(sender As Object, e As System.EventArgs) Handles MyBase.Load
        Dim pProcess As Process = Process.GetProcessesByName("explorer").First
        pidExplorer = pProcess.Id

        ProcessStartWatcher = New ManagementEventWatcher("\\.\root\CIMV2", "SELECT * FROM __InstanceCreationEvent WITHIN .025 WHERE TargetInstance ISA 'Win32_Process'")

        ProcessStartWatcher.Start()
        'ProcessStopWatcher.Start()
    End Sub

    Private Sub Form2_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs)
        ProcessStartWatcher.Stop()
        'ProcessStopWatcher.Stop()
    End Sub

    Private Sub ProcessStartWatcher_EventArrived(sender As Object, e As System.Management.EventArrivedEventArgs) Handles ProcessStartWatcher.EventArrived
        Dim targetInstance As ManagementBaseObject = e.NewEvent.Properties("TargetInstance").Value


        'Dim ProcessName As String = e.NewEvent.Properties("ProcessName").Value
        'Dim PID As Integer = e.NewEvent.Properties("ProcessID").Value
        'Dim ParentID As Integer = e.NewEvent.Properties("ParentProcessID").Value
        Dim Caption As String = targetInstance.Properties("Caption").Value
        Dim PID As Integer = targetInstance.Properties("ProcessID").Value
        Dim ParentID As Integer = targetInstance.Properties("ParentProcessID").Value

        Dim sb As New StringBuilder

        If ParentID = pidExplorer Then
            For Each p As PropertyData In targetInstance.Properties
                Dim sName As String = p.Name
                Dim oValue As Object = p.Value
                Dim sValue As String = ""

                If oValue Is Nothing Then
                    sValue = "<null>"
                Else
                    sValue = oValue.ToString
                End If
                sb.Append(p.Name & "=" & sValue & vbCrLf)
            Next


            SetTextBox1Text_Threadsafe(sb.ToString & vbCrLf)
            SetTextBox1Text_Threadsafe("-------------------------" & vbCrLf)
        End If

        'MessageBox.Show(String.Format("Process ""{0}"" with ID {1} started.", ProcessName, PID))
    End Sub

    Private Sub ProcessStopWatcher_EventArrived(sender As Object, e As System.Management.EventArrivedEventArgs) Handles ProcessStopWatcher.EventArrived
        Dim ProcessName As String = e.NewEvent.Properties("ProcessName").Value
        Dim PID As Integer = e.NewEvent.Properties("ProcessID").Value

        'MessageBox.Show(String.Format("Process ""{0}"" with ID {1} stopped.", ProcessName, PID))
    End Sub
End Class