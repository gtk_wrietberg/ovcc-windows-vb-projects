﻿Imports System.ComponentModel

Public Class Form1
    Private ReadOnly C_OWNER As String = "Wouter"

    Private mWorking As Boolean = False

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        mWorking = Not mWorking
        If mWorking Then
            Button1.Text = "turn OFF"
        Else
            Button1.Text = "turn ON"
        End If

        tmr_BGWORKER.Enabled = mWorking
    End Sub

    Private Sub tmr_BGWORKER_Tick(sender As Object, e As EventArgs) Handles tmr_BGWORKER.Tick
        tmr_BGWORKER.Enabled = False

        bgworker_Processes_StartWork()
    End Sub

    Private Sub bgworker_Processes_StartWork()
        TextBox2.Clear()
        TextBox3.Clear()

        Label1.Text = "?"
        Label2.Text = "?"
        Label3.Text = "?"

        bgworker_Processes.RunWorkerAsync()
    End Sub

    Private Sub bgworker_Processes_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworker_Processes.DoWork

        ProcessChecker.LoadProcesses()
    End Sub

    Private Sub bgworker_Processes_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworker_Processes.RunWorkerCompleted
        Label1.Text = "total: " & ProcessChecker.Count_AllProcesses.ToString


        For Each _p As ProcessChecker.ProcessData In ProcessChecker.GetOldProcesses(C_OWNER)
            TextBox2.AppendText(_p.pid & " ; " & _p.Name & " ; " & _p.Owner & " ; " & _p.Path)
            TextBox2.AppendText(vbCrLf)
        Next
        Label2.Text = ProcessChecker.Count_OldProcesses.ToString

        For Each _p As ProcessChecker.ProcessData In ProcessChecker.GetNewProcesses(C_OWNER)
            TextBox3.AppendText(_p.pid & " ; " & _p.Name & " ; " & _p.Owner & " ; " & _p.Path)
            TextBox3.AppendText(vbCrLf)
        Next
        Label3.Text = ProcessChecker.Count_NewProcesses.ToString


        tmr_BGWORKER.Enabled = mWorking
    End Sub
End Class
