﻿Imports System.Management

Public Class ProcessChecker
    Public Class ProcessData
        Private mPid As Integer
        Public Property pid() As Integer
            Get
                Return mPid
            End Get
            Set(ByVal value As Integer)
                mPid = value
            End Set
        End Property
        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mPath As String
        Public Property Path() As String
            Get
                Return mPath
            End Get
            Set(ByVal value As String)
                mPath = value
            End Set
        End Property

        Private mOwner As String
        Public Property Owner() As String
            Get
                Return mOwner
            End Get
            Set(ByVal value As String)
                mOwner = value
            End Set
        End Property
    End Class

    Public Class ProcessDataCompare
        Implements IEqualityComparer(Of ProcessData)

        Public Function _Equals(ByVal x As ProcessData, ByVal y As ProcessData) As Boolean Implements IEqualityComparer(Of ProcessData).Equals
            If Object.ReferenceEquals(x, y) Then Return True
            If Object.ReferenceEquals(x, Nothing) OrElse Object.ReferenceEquals(y, Nothing) Then Return False

            Return x.pid = y.pid
        End Function

        Public Function _GetHashCode(ByVal _ProcessData As ProcessData) As Integer Implements IEqualityComparer(Of ProcessData).GetHashCode
            If Object.ReferenceEquals(_ProcessData, Nothing) Then Return 0

            Dim hashProductCode As Integer = _ProcessData.pid.GetHashCode()

            Return hashProductCode
        End Function
    End Class


    Public Shared LastError As String = ""
    Private Shared mProcessData_Last As New List(Of ProcessData)
    Private Shared mProcessData_Current As New List(Of ProcessData)

    Private Shared mProcessData_New As New List(Of ProcessData)
    Private Shared mProcessData_Old As New List(Of ProcessData)


    Public Shared Sub Reset()
        mProcessData_Last = New List(Of ProcessData)
        mProcessData_Current = New List(Of ProcessData)

        mProcessData_New = New List(Of ProcessData)
        mProcessData_Old = New List(Of ProcessData)
    End Sub

    Public Shared Sub Swap()
        mProcessData_Last = New List(Of ProcessData)
        mProcessData_Last.AddRange(mProcessData_Current)

        mProcessData_Current = New List(Of ProcessData)

        mProcessData_New = New List(Of ProcessData)
        mProcessData_Old = New List(Of ProcessData)
    End Sub

    Public Shared Function LoadProcesses() As Boolean

        Swap()

        Dim selectQuery As SelectQuery = New SelectQuery("Win32_Process")
        Dim searcher As ManagementObjectSearcher = New ManagementObjectSearcher(selectQuery)


        For Each proc As ManagementObject In searcher.Get
            Dim _processData As New ProcessData


            Try
                _processData.pid = proc("ProcessId").ToString
            Catch ex As Exception
                _processData.pid = -1
            End Try


            Try
                _processData.Name = proc("Name").ToString
            Catch ex As Exception
                _processData.Name = ex.Message
            End Try


            Try
                _processData.Path = proc("ExecutablePath").ToString
            Catch ex As Exception
                _processData.Path = ex.Message
            End Try


            Try
                Dim s(1) As String
                proc.InvokeMethod("GetOwner", CType(s, Object()))

                If s(0) Is Nothing Then
                    _processData.Owner = ""
                Else
                    _processData.Owner = s(0)
                End If

            Catch ex As Exception
                _processData.Owner = ex.Message
            End Try



            mProcessData_Current.Add(_processData)
        Next


        mProcessData_New = mProcessData_Current.Except(mProcessData_Last, New ProcessDataCompare).ToList
        mProcessData_Old = mProcessData_Last.Except(mProcessData_Current, New ProcessDataCompare).ToList


        Return True
    End Function

    Public Shared Function Count_AllProcesses() As Integer
            Return mProcessData_Current.Count
        End Function

        Public Shared Function Count_NewProcesses() As Integer
            Return mProcessData_New.Count
        End Function

        Public Shared Function Count_OldProcesses() As Integer
            Return mProcessData_Old.Count
        End Function

        Public Shared Iterator Function GetAllProcesses(Optional Owner As String = "") As IEnumerable(Of ProcessData)
            Dim counter As Integer = -1

            If mProcessData_Current.Count > 0 Then
                While counter < mProcessData_Current.Count
                    counter += 1

                    If counter < mProcessData_Current.Count Then
                        If Owner.Equals("") Or mProcessData_Current.Item(counter).Owner.Equals(Owner) Then
                            Yield mProcessData_Current.Item(counter)
                        End If
                    End If
                End While
            End If
        End Function

        Public Shared Iterator Function GetNewProcesses(Optional Owner As String = "") As IEnumerable(Of ProcessData)
            Dim counter As Integer = -1

            If mProcessData_New.Count > 0 Then
                While counter < mProcessData_New.Count
                    counter += 1

                    If counter < mProcessData_New.Count Then
                        If Owner.Equals("") Or mProcessData_New.Item(counter).Owner.Equals(Owner) Then
                            Yield mProcessData_New.Item(counter)
                        End If
                    End If
                End While
            End If
        End Function

        Public Shared Iterator Function GetOldProcesses(Optional Owner As String = "") As IEnumerable(Of ProcessData)
            Dim counter As Integer = -1

            If mProcessData_Old.Count > 0 Then
                While counter < mProcessData_Old.Count
                    counter += 1

                    If counter < mProcessData_Old.Count Then
                        If Owner.Equals("") Or mProcessData_Old.Item(counter).Owner.Equals(Owner) Then
                            Yield mProcessData_Old.Item(counter)
                        End If
                    End If
                End While
            End If
        End Function
    End Class
