﻿Imports System.IO
Imports System.IO.Pipes

Public Class Form1


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        peop()
    End Sub

    Private Sub peop()
        Dim sRand1 As String
        Dim sRand2 As String
        Dim sRand3 As String
        Dim sRand4 As String


        sRand1 = RandomString(12)
        sRand2 = RandomString(12)
        sRand3 = RandomString(12)
        sRand4 = RandomString(12)


        NamedPipe.Logger.Client.Message(sRand1)
        NamedPipe.Logger.Client.Warning(sRand2)
        NamedPipe.Logger.Client.Error(sRand3)
        NamedPipe.Logger.Client.Debug(sRand4)
    End Sub


    Public Function RandomString(Length As Integer) As String
        Return RandomString(Length, "")
    End Function

    Public Function RandomString(Length As Integer, CharsToChooseFrom As String) As String
        If CharsToChooseFrom = "" Then
            'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
            CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        End If
        If Length < 1 Then
            Length = 1
        End If

        Dim sTmp As String = "", iRnd As Integer

        Randomize()

        For i As Integer = 0 To Length - 1
            iRnd = Int(CharsToChooseFrom.Length * Rnd())
            sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
        Next

        Return sTmp
    End Function



    Private Sub _stmp_log(s As String)
        Dim sDate As String = Now().ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        Using sw As New IO.StreamWriter("C:\Temp\pipe_client.txt", True)
            sw.WriteLine(sDate & " - " & s)
        End Using
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        NamedPipe.Session.Client.SessionStart()
        NamedPipe.Session.Client.SessionTermsAcccepted()
        NamedPipe.Session.Client.SessionActivity("app|office123.exe")
        NamedPipe.Session.Client.SessionActivity("app:poep.kak")
        NamedPipe.Session.Client.SessionEnd()
    End Sub
End Class
