﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Net
Imports System.Net.Sockets
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Threading

Public Class Form1
    Private mThread_Logger As Threading.Thread
    Private mThread_Session As Threading.Thread

    Private mLoop As Boolean
    Private mOwnLogger As NamedPipe.Logger.Server._Logger

    Private mThreadCountMax_Logger As Integer = 25
    Private mThreadCountMax_Session As Integer = 3

    Private ReadOnly OwnLogAppName As String = My.Application.Info.ProductName

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        mOwnLogger = New NamedPipe.Logger.Server._Logger(OwnLogAppName)

        mOwnLogger.WriteMessage(New String("*", 50))
        mOwnLogger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        mOwnLogger.WriteMessage("started")


        StartThreads()
    End Sub


#Region "threads"
    Private Sub StartThreads()
        mLoop = True

        mThread_Logger = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Logger))
        mThread_Logger.Start()

        mThread_Session = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Session))
        mThread_Session.Start()
    End Sub

    Private Sub KillThreads()
        mLoop = False

        Try
            mThread_Logger.Abort()
        Catch ex As Exception

        End Try

        Try
            mThread_Session.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Thread__Logger()
        Dim tThreads(mThreadCountMax_Logger) As Thread

        For i As Integer = 0 To mThreadCountMax_Logger - 1
            mOwnLogger.WriteDebug("Starting logger thread #" & i)

            tThreads(i) = New Thread(AddressOf NamedPipe.Logger.Server.Instance)
            tThreads(i).Start()
        Next

        Do
            For i As Integer = 0 To mThreadCountMax_Logger - 1
                If Not tThreads(i).IsAlive Then
                    mOwnLogger.WriteDebug("Restarting logger thread #" & i)

                    tThreads(i) = New Thread(AddressOf NamedPipe.Logger.Server.Instance)
                    tThreads(i).Start()
                End If
            Next

            Thread.Sleep(1000)
        Loop While mLoop
    End Sub

    Private Sub Thread__Session()
        Dim tThreads(mThreadCountMax_Session) As Thread

        For i As Integer = 0 To mThreadCountMax_Session - 1
            mOwnLogger.WriteDebug("Starting session thread #" & i)

            tThreads(i) = New Thread(AddressOf NamedPipe.Session.Server.Instance)
            tThreads(i).Start()
        Next

        Do
            For i As Integer = 0 To mThreadCountMax_Session - 1
                If Not tThreads(i).IsAlive Then
                    mOwnLogger.WriteDebug("Restarting session thread #" & i)

                    tThreads(i) = New Thread(AddressOf NamedPipe.Session.Server.Instance)
                    tThreads(i).Start()
                End If
            Next

            Thread.Sleep(1000)
        Loop While mLoop
    End Sub
#End Region

End Class
