﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        Button1 = New Button()
        CustomProgressBar1 = New CustomProgressBar()
        TrackBar1 = New TrackBar()
        ColorDialog1 = New ColorDialog()
        Button2 = New Button()
        Panel1 = New Panel()
        Panel2 = New Panel()
        Button3 = New Button()
        Button4 = New Button()
        Button5 = New Button()
        RadioButton1 = New RadioButton()
        RadioButton2 = New RadioButton()
        RadioButton3 = New RadioButton()
        CustomProgressBar2 = New CustomProgressBar()
        Timer1 = New Timer(components)
        CType(TrackBar1, ComponentModel.ISupportInitialize).BeginInit()
        SuspendLayout()
        ' 
        ' Button1
        ' 
        Button1.Location = New Point(12, 12)
        Button1.Name = "Button1"
        Button1.Size = New Size(217, 74)
        Button1.TabIndex = 0
        Button1.Text = "Button1"
        Button1.UseVisualStyleBackColor = True
        ' 
        ' CustomProgressBar1
        ' 
        CustomProgressBar1.AnimationSpeed = 1R
        CustomProgressBar1.AnimationType = CustomProgressBar.ANIMATION_TYPES.VALUE
        CustomProgressBar1.AnimationWidth = 5
        CustomProgressBar1.BackColor = Color.Transparent
        CustomProgressBar1.BackgroundColor = Color.FromArgb(CByte(255), CByte(192), CByte(255))
        CustomProgressBar1.Border = True
        CustomProgressBar1.BorderStyle = BorderStyle.FixedSingle
        CustomProgressBar1.Location = New Point(12, 209)
        CustomProgressBar1.Name = "CustomProgressBar1"
        CustomProgressBar1.ProgressBarBorder = False
        CustomProgressBar1.ProgressBarColor = Color.FromArgb(CByte(192), CByte(255), CByte(192))
        CustomProgressBar1.Size = New Size(554, 10)
        CustomProgressBar1.TabIndex = 1
        CustomProgressBar1.Value = 0R
        ' 
        ' TrackBar1
        ' 
        TrackBar1.Location = New Point(366, 12)
        TrackBar1.Maximum = 100
        TrackBar1.Name = "TrackBar1"
        TrackBar1.Size = New Size(200, 45)
        TrackBar1.TabIndex = 2
        TrackBar1.TickStyle = TickStyle.None
        ' 
        ' Button2
        ' 
        Button2.Location = New Point(235, 12)
        Button2.Name = "Button2"
        Button2.Size = New Size(80, 31)
        Button2.TabIndex = 3
        Button2.Text = "bg"
        Button2.UseVisualStyleBackColor = True
        ' 
        ' Panel1
        ' 
        Panel1.BorderStyle = BorderStyle.FixedSingle
        Panel1.Location = New Point(321, 12)
        Panel1.Name = "Panel1"
        Panel1.Size = New Size(30, 30)
        Panel1.TabIndex = 4
        ' 
        ' Panel2
        ' 
        Panel2.BorderStyle = BorderStyle.FixedSingle
        Panel2.Location = New Point(321, 56)
        Panel2.Name = "Panel2"
        Panel2.Size = New Size(30, 30)
        Panel2.TabIndex = 6
        ' 
        ' Button3
        ' 
        Button3.Location = New Point(235, 55)
        Button3.Name = "Button3"
        Button3.Size = New Size(80, 31)
        Button3.TabIndex = 5
        Button3.Text = "fg"
        Button3.UseVisualStyleBackColor = True
        ' 
        ' Button4
        ' 
        Button4.Location = New Point(12, 112)
        Button4.Name = "Button4"
        Button4.Size = New Size(75, 44)
        Button4.TabIndex = 7
        Button4.Text = "animate start"
        Button4.UseVisualStyleBackColor = True
        ' 
        ' Button5
        ' 
        Button5.Location = New Point(93, 112)
        Button5.Name = "Button5"
        Button5.Size = New Size(75, 44)
        Button5.TabIndex = 9
        Button5.Text = "animate stop"
        Button5.UseVisualStyleBackColor = True
        ' 
        ' RadioButton1
        ' 
        RadioButton1.AutoSize = True
        RadioButton1.Checked = True
        RadioButton1.Location = New Point(174, 112)
        RadioButton1.Name = "RadioButton1"
        RadioButton1.Size = New Size(70, 19)
        RadioButton1.TabIndex = 10
        RadioButton1.TabStop = True
        RadioButton1.Text = "left right"
        RadioButton1.UseVisualStyleBackColor = True
        ' 
        ' RadioButton2
        ' 
        RadioButton2.AutoSize = True
        RadioButton2.Location = New Point(174, 137)
        RadioButton2.Name = "RadioButton2"
        RadioButton2.Size = New Size(70, 19)
        RadioButton2.TabIndex = 11
        RadioButton2.Text = "right left"
        RadioButton2.UseVisualStyleBackColor = True
        ' 
        ' RadioButton3
        ' 
        RadioButton3.AutoSize = True
        RadioButton3.Location = New Point(174, 162)
        RadioButton3.Name = "RadioButton3"
        RadioButton3.Size = New Size(42, 19)
        RadioButton3.TabIndex = 12
        RadioButton3.Text = "kitt"
        RadioButton3.UseVisualStyleBackColor = True
        ' 
        ' CustomProgressBar2
        ' 
        CustomProgressBar2.AnimationSpeed = 1R
        CustomProgressBar2.AnimationType = CustomProgressBar.ANIMATION_TYPES.VALUE
        CustomProgressBar2.AnimationWidth = 5
        CustomProgressBar2.BackColor = Color.Transparent
        CustomProgressBar2.BackgroundColor = Color.FromArgb(CByte(255), CByte(192), CByte(255))
        CustomProgressBar2.Border = True
        CustomProgressBar2.BorderStyle = BorderStyle.FixedSingle
        CustomProgressBar2.Location = New Point(12, 225)
        CustomProgressBar2.Name = "CustomProgressBar2"
        CustomProgressBar2.ProgressBarBorder = False
        CustomProgressBar2.ProgressBarColor = Color.FromArgb(CByte(192), CByte(255), CByte(192))
        CustomProgressBar2.Size = New Size(554, 10)
        CustomProgressBar2.TabIndex = 13
        CustomProgressBar2.Value = 0R
        ' 
        ' Timer1
        ' 
        Timer1.Interval = 10
        ' 
        ' Form1
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Gray
        ClientSize = New Size(1403, 537)
        Controls.Add(CustomProgressBar2)
        Controls.Add(RadioButton3)
        Controls.Add(RadioButton2)
        Controls.Add(RadioButton1)
        Controls.Add(Button5)
        Controls.Add(Button4)
        Controls.Add(Panel2)
        Controls.Add(Panel1)
        Controls.Add(Button3)
        Controls.Add(Button2)
        Controls.Add(TrackBar1)
        Controls.Add(CustomProgressBar1)
        Controls.Add(Button1)
        Name = "Form1"
        Text = "Form1"
        CType(TrackBar1, ComponentModel.ISupportInitialize).EndInit()
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents CustomProgressBar1 As CustomProgressBar
    Friend WithEvents TrackBar1 As TrackBar
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents Button2 As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents RadioButton2 As RadioButton
    Friend WithEvents RadioButton3 As RadioButton
    Friend WithEvents CustomProgressBar2 As CustomProgressBar
    Friend WithEvents Timer1 As Timer

End Class
