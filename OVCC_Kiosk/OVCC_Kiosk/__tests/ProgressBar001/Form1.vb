﻿Public Class Form1
    Private mProgressValue As Integer = 0


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Timer1.Enabled = True
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CustomProgressBar1.BackgroundColor = Color.Transparent
        CustomProgressBar1.Border = True
        CustomProgressBar1.ProgressBarColor = Color.Red
        CustomProgressBar1.ProgressBarBorder = True

        CustomProgressBar1.AnimationSpeed = 10
        CustomProgressBar1.AnimationWidth = 25

        CustomProgressBar1.Initialise()


        CustomProgressBar2.BackgroundColor = Color.Transparent
        CustomProgressBar2.Border = True
        CustomProgressBar2.ProgressBarColor = Color.Red
        CustomProgressBar2.ProgressBarBorder = True

        CustomProgressBar2.AnimationSpeed = 11
        CustomProgressBar2.AnimationWidth = 25

        CustomProgressBar2.Initialise()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If CustomProgressBar1.IsFull Then
            mProgressValue = 0
            Timer1.Enabled = False
            Exit Sub
        End If

        CustomProgressBar1.Value = mProgressValue
        'CustomProgressBar1.Draw()
        mProgressValue += 1
    End Sub

    Private Sub TrackBar1_Scroll(sender As Object, e As EventArgs) Handles TrackBar1.Scroll
        CustomProgressBar1.Value = TrackBar1.Value
        'CustomProgressBar1.Draw()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If ColorDialog1.ShowDialog = DialogResult.OK Then
            Panel1.BackColor = ColorDialog1.Color
            CustomProgressBar1.BackgroundColor = ColorDialog1.Color
            'CustomProgressBar1.Draw()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If ColorDialog1.ShowDialog = DialogResult.OK Then
            Panel2.BackColor = ColorDialog1.Color
            CustomProgressBar1.ProgressBarColor = ColorDialog1.Color
            'CustomProgressBar1.Draw()
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If RadioButton1.Checked Then CustomProgressBar1.AnimationType = CustomProgressBar.ANIMATION_TYPES.LEFT_RIGHT
        If RadioButton2.Checked Then CustomProgressBar1.AnimationType = CustomProgressBar.ANIMATION_TYPES.RIGHT_LEFT
        If RadioButton3.Checked Then CustomProgressBar1.AnimationType = CustomProgressBar.ANIMATION_TYPES.KITT

        CustomProgressBar1.StartAnimation()


        If RadioButton1.Checked Then CustomProgressBar2.AnimationType = CustomProgressBar.ANIMATION_TYPES.LEFT_RIGHT
        If RadioButton2.Checked Then CustomProgressBar2.AnimationType = CustomProgressBar.ANIMATION_TYPES.RIGHT_LEFT
        If RadioButton3.Checked Then CustomProgressBar2.AnimationType = CustomProgressBar.ANIMATION_TYPES.KITT

        CustomProgressBar2.StartAnimation()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        CustomProgressBar1.StopAnimation()

        CustomProgressBar2.StopAnimation()
    End Sub
End Class
