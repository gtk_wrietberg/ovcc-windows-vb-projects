﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CustomProgressBar
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        pnlBar = New Panel()
        pnlContainer = New Panel()
        tmrAnimate = New Timer(components)
        pnlContainer.SuspendLayout()
        SuspendLayout()
        ' 
        ' pnlBar
        ' 
        pnlBar.Location = New Point(3, 3)
        pnlBar.Name = "pnlBar"
        pnlBar.Size = New Size(59, 28)
        pnlBar.TabIndex = 0
        ' 
        ' pnlContainer
        ' 
        pnlContainer.Controls.Add(pnlBar)
        pnlContainer.Dock = DockStyle.Fill
        pnlContainer.Location = New Point(0, 0)
        pnlContainer.Name = "pnlContainer"
        pnlContainer.Size = New Size(1042, 90)
        pnlContainer.TabIndex = 1
        ' 
        ' tmrAnimate
        ' 
        tmrAnimate.Interval = 10
        ' 
        ' CustomProgressBar
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        BorderStyle = BorderStyle.FixedSingle
        Controls.Add(pnlContainer)
        Name = "CustomProgressBar"
        Size = New Size(1042, 90)
        pnlContainer.ResumeLayout(False)
        ResumeLayout(False)
    End Sub
    Friend WithEvents pnlBar As Panel
    Friend WithEvents pnlContainer As Panel
    Friend WithEvents tmrAnimate As Timer

End Class
