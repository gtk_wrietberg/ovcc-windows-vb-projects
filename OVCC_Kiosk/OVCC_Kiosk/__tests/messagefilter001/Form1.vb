﻿Public Class Form1
    Implements IMessageFilter

    Private bMessageFilterActive As Boolean = False


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Application.AddMessageFilter(Me)
    End Sub

    Public Function PreFilterMessage(ByRef m As Message) As Boolean Implements IMessageFilter.PreFilterMessage
        If (m.Msg >= &H100 And m.Msg <= &H109) Or (m.Msg >= &H200 And m.Msg <= &H20E) Then
            If bMessageFilterActive Then
                Label1.Text = "YES - " & m.Msg
            Else
                Label1.Text = "no - " & m.Msg
            End If
        End If
        'If (m.Msg >= &H100 And m.Msg <= &H109) Or (m.Msg >= &H200 And m.Msg <= &H20E) Then
        '    'NamedPipe.Logger.Client.Debug("PreFilterMessage:" & m.Msg)
        '    If bMessageFilterActive Then
        '        NamedPipe.Logger.Client.Debug("PreFilterMessage:CloseScreensaver")
        '        bMessageFilterActive = False
        '        CloseScreensaver()
        '    End If
        'End If

        Return False
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        bMessageFilterActive = True
    End Sub
End Class
