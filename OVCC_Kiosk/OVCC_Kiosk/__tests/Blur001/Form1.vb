﻿Imports System.ComponentModel
Imports System.Drawing.Imaging


Public Class Form1
    Private p As PictureBox

    Private grabbedpic As Image
    Private blurredpic As Image

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        ShowBlurredPicture()
        MsgBox("TEST")
        HideBlurredPicture()
    End Sub

    Sub ShowBlurredPicture()
        '' Dim blurredpic As Bitmap = gausianBlur(False, New Size(5, 5), GetFormPic)
        'Dim blurredpic As Bitmap = ImageEffect.GaussBlur(GetFormPic, 4, 1.0)

        'p = New PictureBox
        'p.Image = blurredpic

        'p.Location = New Point(-System.Windows.Forms.SystemInformation.FrameBorderSize.Width, -(System.Windows.Forms.SystemInformation.CaptionHeight + System.Windows.Forms.SystemInformation.FrameBorderSize.Height))
        'p.Size = New Size(Me.Size)
        'Me.Controls.Add(p)
        'p.Visible = True
        'p.BringToFront()
        ''AddHandler p.Click, AddressOf picclick


        grabbedpic = GetFormPic()

        bgworker_Blur.RunWorkerAsync()
    End Sub

    Sub HideBlurredPicture()
        If p IsNot Nothing Then
            p.Dispose()
        End If
    End Sub

    Sub picclick(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Controls.Remove(sender)
    End Sub

    Function GetFormPic() As Bitmap
        Dim ScreenSize As Size = Me.Size
        Dim screenGrab As New Bitmap(Me.Width, Me.Height)
        Dim g As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(screenGrab)
        g.CopyFromScreen(Me.Location, New Point(0, 0), Me.Size)
        Return screenGrab
    End Function

    Private Sub bgworker_Blur_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworker_Blur.DoWork
        blurredpic = grabbedpic
        ImageTools.Blur(blurredpic, 4)

        'blurredpic = ImageEffect.GaussBlur(GetFormPic, 10, 1.0)
    End Sub

    Private Sub bgworker_Blur_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworker_Blur.RunWorkerCompleted
        p = New PictureBox
        p.Image = blurredpic

        p.Location = New Point(-System.Windows.Forms.SystemInformation.FrameBorderSize.Width, -(System.Windows.Forms.SystemInformation.CaptionHeight + System.Windows.Forms.SystemInformation.FrameBorderSize.Height))
        p.Size = New Size(Me.Size)
        Me.Controls.Add(p)
        p.Visible = True
        p.BringToFront()
    End Sub
End Class