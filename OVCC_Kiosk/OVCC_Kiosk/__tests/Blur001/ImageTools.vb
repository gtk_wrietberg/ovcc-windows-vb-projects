﻿Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices

Public Class ImageTools
    Public Class ExposedBitmap
        Public pinnedArray As PinnedByteArray
        Public exBitmap As Bitmap
        Public ReadOnly pixelFormat As PixelFormat
        Public ReadOnly bytesPerPixel As Integer
        Public ReadOnly stride As Integer
        Public ReadOnly Height As Integer
        Public ReadOnly Width As Integer
        Private horizontalCoords As Integer = -1
        Private verticalCoords As Integer = -1
        Private horizontalLoc As Integer = 0
        Private verticalLoc As Integer = 0
        Private location As Integer = 0

        Public Sub GetPixel(ByVal x As Integer, ByVal y As Integer, <Out> ByRef red As Byte, <Out> ByRef green As Byte, <Out> ByRef blue As Byte)
            If x = horizontalCoords AndAlso y = verticalCoords Then
                blue = pinnedArray.bytes(location)
                green = pinnedArray.bytes(location + 1)
                red = pinnedArray.bytes(location + 2)
                Return
            Else

                If x <> horizontalCoords Then
                    horizontalCoords = x
                    horizontalLoc = horizontalCoords * bytesPerPixel
                End If

                If y <> verticalCoords Then
                    verticalCoords = y
                    verticalLoc = verticalCoords * stride
                End If

                location = verticalLoc + horizontalLoc
            End If

            blue = pinnedArray.bytes(location)
            green = pinnedArray.bytes(location + 1)
            red = pinnedArray.bytes(location + 2)
        End Sub

        Public Sub SetPixel(ByVal x As Integer, ByVal y As Integer, ByVal red As Byte, ByVal green As Byte, ByVal blue As Byte)
            If x = horizontalCoords AndAlso y = verticalCoords Then
                pinnedArray.bytes(location) = blue
                pinnedArray.bytes(location + 1) = green
                pinnedArray.bytes(location + 2) = red
                Return
            Else

                If x <> horizontalCoords Then
                    horizontalCoords = x
                    horizontalLoc = horizontalCoords * bytesPerPixel
                End If

                If y <> verticalCoords Then
                    verticalCoords = y
                    verticalLoc = verticalCoords * stride
                End If

                location = verticalLoc + horizontalLoc
            End If

            pinnedArray.bytes(location) = blue
            pinnedArray.bytes(location + 1) = green
            pinnedArray.bytes(location + 2) = red
        End Sub

        Public Function GetRed(ByVal x As Integer, ByVal y As Integer) As Byte
            If x = horizontalCoords AndAlso y = verticalCoords Then
                Return pinnedArray.bytes(location + 2)
            Else

                If x <> horizontalCoords Then
                    horizontalCoords = x
                    horizontalLoc = horizontalCoords * bytesPerPixel
                End If

                If y <> verticalCoords Then
                    verticalCoords = y
                    verticalLoc = verticalCoords * stride
                End If

                location = verticalLoc + horizontalLoc
            End If

            Return pinnedArray.bytes(location + 2)
        End Function

        Public Function GetGreen(ByVal x As Integer, ByVal y As Integer) As Byte
            If x = horizontalCoords AndAlso y = verticalCoords Then
                Return pinnedArray.bytes(location + 1)
            Else

                If x <> horizontalCoords Then
                    horizontalCoords = x
                    horizontalLoc = horizontalCoords * bytesPerPixel
                End If

                If y <> verticalCoords Then
                    verticalCoords = y
                    verticalLoc = verticalCoords * stride
                End If

                location = verticalLoc + horizontalLoc
            End If

            Return pinnedArray.bytes(location + 1)
        End Function

        Public Function GetBlue(ByVal x As Integer, ByVal y As Integer) As Byte
            If x = horizontalCoords AndAlso y = verticalCoords Then
                Return pinnedArray.bytes(location)
            Else

                If x <> horizontalCoords Then
                    horizontalCoords = x
                    horizontalLoc = horizontalCoords * bytesPerPixel
                End If

                If y <> verticalCoords Then
                    verticalCoords = y
                    verticalLoc = verticalCoords * stride
                End If

                location = verticalLoc + horizontalLoc
            End If

            Return pinnedArray.bytes(location)
        End Function

        Public Sub SetRed(ByVal x As Integer, ByVal y As Integer, ByVal byt As Byte)
            If x = horizontalCoords AndAlso y = verticalCoords Then
                pinnedArray.bytes(location + 2) = byt
                Return
            Else

                If x <> horizontalCoords Then
                    horizontalCoords = x
                    horizontalLoc = horizontalCoords * bytesPerPixel
                End If

                If y <> verticalCoords Then
                    verticalCoords = y
                    verticalLoc = verticalCoords * stride
                End If

                location = verticalLoc + horizontalLoc
            End If

            pinnedArray.bytes(location + 2) = byt
        End Sub

        Public Sub SetGreen(ByVal x As Integer, ByVal y As Integer, ByVal byt As Byte)
            If x = horizontalCoords AndAlso y = verticalCoords Then
                pinnedArray.bytes(location + 1) = byt
                Return
            Else

                If x <> horizontalCoords Then
                    horizontalCoords = x
                    horizontalLoc = horizontalCoords * bytesPerPixel
                End If

                If y <> verticalCoords Then
                    verticalCoords = y
                    verticalLoc = verticalCoords * stride
                End If

                location = verticalLoc + horizontalLoc
            End If

            pinnedArray.bytes(location + 1) = byt
        End Sub

        Public Sub SetBlue(ByVal x As Integer, ByVal y As Integer, ByVal byt As Byte)
            If x = horizontalCoords AndAlso y = verticalCoords Then
                pinnedArray.bytes(location) = byt
                Return
            Else

                If x <> horizontalCoords Then
                    horizontalCoords = x
                    horizontalLoc = horizontalCoords * bytesPerPixel
                End If

                If y <> verticalCoords Then
                    verticalCoords = y
                    verticalLoc = verticalCoords * stride
                End If

                location = verticalLoc + horizontalLoc
            End If

            pinnedArray.bytes(location) = byt
        End Sub

        Public Class PinnedByteArray
            Public bytes As Byte()
            Friend handle As GCHandle
            Friend ptr As IntPtr
            Private referenceCount As Integer
            Private destroyed As Boolean

            Public Sub New(ByVal length As Integer)
                bytes = New Byte(length - 1) {}
                handle = GCHandle.Alloc(bytes, GCHandleType.Pinned)
                ptr = Marshal.UnsafeAddrOfPinnedArrayElement(bytes, 0)
                referenceCount += 1
            End Sub

            Friend Sub AddReference()
                referenceCount += 1
            End Sub

            Friend Sub ReleaseReference()
                referenceCount -= 1
                If referenceCount <= 0 Then Destroy()
            End Sub

            Private Sub Destroy()
                If Not destroyed Then
                    handle.Free()
                    bytes = Nothing
                    destroyed = True
                End If
            End Sub

            Protected Overrides Sub Finalize()
                Destroy()
            End Sub
        End Class

        Private Function GetStride(ByVal width As Integer, ByVal bytesPerPixel As Integer) As Integer
            Dim stride As Integer = width * bytesPerPixel
            stride += If(stride Mod 4 = 0, 0, 4 - (stride Mod 4))
            Return stride
        End Function

        Public Sub New(ByRef sourceBmp As Bitmap)
            Height = sourceBmp.Height
            Width = sourceBmp.Width
            pixelFormat = sourceBmp.PixelFormat
            bytesPerPixel = Image.GetPixelFormatSize(pixelFormat) / 8
            stride = GetStride(Width, bytesPerPixel)
            pinnedArray = New PinnedByteArray(stride * Height)
            exBitmap = New Bitmap(Width, Height, stride, pixelFormat, pinnedArray.ptr)
            Dim g As Graphics = Graphics.FromImage(exBitmap)
            g.DrawImage(sourceBmp, 0, 0, Width, Height)
            g.Dispose()
        End Sub
    End Class

    Public Shared Sub Blur(ByRef image As Bitmap)
        Blur(image, New Rectangle(0, 0, image.Width, image.Height), 2)
    End Sub

    Public Shared Sub Blur(ByRef image As Bitmap, ByVal blurSize As Int32)
        Blur(image, New Rectangle(0, 0, image.Width, image.Height), blurSize)
    End Sub

    Private Shared Sub Blur(ByRef image As Bitmap, ByVal rectangle As Rectangle, ByVal blurSize As Int32)
        Dim blurred As ExposedBitmap = New ExposedBitmap(image)
        Dim height As Integer = blurred.Height
        Dim width As Integer = blurred.Width

        For xx As Integer = rectangle.X To rectangle.X + rectangle.Width - 1

            For yy As Integer = rectangle.Y To rectangle.Y + rectangle.Height - 1
                Dim avgR As Integer = 0, avgG As Integer = 0, avgB As Integer = 0
                Dim blurPixelCount As Integer = 0
                Dim horizontalLocation As Integer
                Dim verticalLocation As Integer
                Dim pixelPointer As Integer
                Dim x As Integer

                x = xx
                While (x < xx + blurSize AndAlso x < width)
                    horizontalLocation = x * blurred.bytesPerPixel
                    Dim y As Integer = yy

                    While (y < yy + blurSize AndAlso y < height)
                        verticalLocation = y * blurred.stride
                        pixelPointer = verticalLocation + horizontalLocation
                        avgB += blurred.pinnedArray.bytes(pixelPointer)
                        avgG += blurred.pinnedArray.bytes(pixelPointer + 1)
                        avgR += blurred.pinnedArray.bytes(pixelPointer + 2)
                        blurPixelCount += 1
                        y += 1
                    End While

                    x += 1
                End While

                Dim bavgr As Byte = CByte((avgR / blurPixelCount))
                Dim bavgg As Byte = CByte((avgG / blurPixelCount))
                Dim bavgb As Byte = CByte((avgB / blurPixelCount))

                x = xx
                While x < xx + blurSize AndAlso x < width AndAlso x < rectangle.Width
                    horizontalLocation = x * blurred.bytesPerPixel
                    Dim y As Integer = yy

                    While y < yy + blurSize AndAlso y < height AndAlso y < rectangle.Height
                        verticalLocation = y * blurred.stride
                        pixelPointer = verticalLocation + horizontalLocation
                        blurred.pinnedArray.bytes(pixelPointer) = bavgb
                        blurred.pinnedArray.bytes(pixelPointer + 1) = bavgg
                        blurred.pinnedArray.bytes(pixelPointer + 2) = bavgr
                        y += 1
                    End While

                    x += 1
                End While
            Next
        Next

        image = blurred.exBitmap
    End Sub
End Class
