﻿Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices

Public Class ImageEffect
    Public Shared Function GaussBlur(IncomingBitmap As Bitmap, Radius As Integer, Opacity As Decimal) As Drawing.Image

        Dim destination As New Bitmap(IncomingBitmap.Width, IncomingBitmap.Height)
        Using graphics As Drawing.Graphics = Graphics.FromImage(destination)
            graphics.DrawImage(IncomingBitmap, 0, 0, IncomingBitmap.Width, IncomingBitmap.Height)
        End Using

        'Gaussian Blur
        Dim source As Bitmap = CType(destination.Clone(), Bitmap)
        Dim destinationData As BitmapData = destination.LockBits(New Rectangle(0, 0, destination.Width, destination.Height),
                                         ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb)
        Dim sourceData As BitmapData = source.LockBits(New Rectangle(0, 0, source.Width, source.Height),
                                       ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb)

        BoxBlur(destinationData, sourceData, destination.Width, destination.Height, Radius - 1, Opacity)
        BoxBlur(sourceData, destinationData, destination.Width, destination.Height, Radius - 1, Opacity)
        BoxBlur(destinationData, sourceData, destination.Width, destination.Height, Radius - 1, Opacity)
        BoxBlur(sourceData, destinationData, destination.Width, destination.Height, Radius - 1, Opacity)
        BoxBlur(destinationData, sourceData, destination.Width, destination.Height, Radius, Opacity)
        BoxBlur(sourceData, destinationData, destination.Width, destination.Height, Radius, Opacity)

        destination.UnlockBits(destinationData)
        source.UnlockBits(sourceData)

        Return destination
    End Function


    Private Shared Sub BoxBlur(ByRef data1 As BitmapData,
                              ByRef data2 As BitmapData,
                              Width As Integer, Height As Integer,
                              Radius As Integer, Opacity As Decimal)
        Dim CHANNELS As Integer = 4 'ARGB Alpha Red Green Blue 0-255 each
        'Unless LittleEndian, then the bytes are backwards

        Try
            'Copy the bitmap data into some arrays for faster processing
            Dim data1Array As Byte() = New Byte(CInt(Height * Width * CHANNELS) - 1) {}
            Marshal.Copy(data1.Scan0, data1Array, 0, data1Array.Length - 1)

            Dim data2Array As Byte() = New Byte(CInt(Height * Width * CHANNELS) - 1) {}
            Marshal.Copy(data2.Scan0, data2Array, 0, data2Array.Length - 1)
            'The blurred color of the current pixel is the average of the current pixel's color,
            ' and its Math.ceil(Radius/2) neighboring pixels so a blur radius of 3 looks like:
            ' X X X
            ' X P X
            ' X X X

            'Go through each row
            For row As Integer = 0 To Height - 1 Step 1
                'Go through each column
                For column As Integer = 0 To Width - 1 Step 1
                    'Now we are at a single pixel
                    Dim aSum As Integer = 0
                    Dim rSum As Integer = 0
                    Dim gSum As Integer = 0
                    Dim bSum As Integer = 0
                    Dim tSum As Integer = 0
                    'Look upwards based on the radius
                    Dim neighboringPixelLine As Integer = CInt(Math.Ceiling(Radius / 2))
                    'Look up to see if we can get argb values for the pixels
                    For rowsUp As Integer = neighboringPixelLine To 0 Step -1
                        'If we can get a value from the row above, or the current row proceed
                        If (row - rowsUp) >= 0 Then
                            'Look left to see if we can get a value
                            For columnsLeft As Integer = neighboringPixelLine To 0 Step -1
                                If (column - columnsLeft) >= 0 Then
                                    '(Channel * Row * Width) + (Channel * Column)
                                    Dim startOfByte As Integer = (CHANNELS * (row - rowsUp) * Width) +
                                                                 (CHANNELS * (column - columnsLeft))
                                    'If little endian, which most systems are, the bytes are stored as BGRA
                                    If System.BitConverter.IsLittleEndian = True Then
                                        bSum += data1Array(startOfByte)
                                        gSum += data1Array(startOfByte + 1)
                                        rSum += data1Array(startOfByte + 2)
                                        aSum += data1Array(startOfByte + 3)
                                        tSum += 1
                                    Else
                                        'If big endian, which a lot of systems are not, the bytes are stored as ARGB
                                        aSum += data1Array(startOfByte)
                                        rSum += data1Array(startOfByte + 1)
                                        gSum += data1Array(startOfByte + 2)
                                        bSum += data1Array(startOfByte + 3)
                                        tSum += 1
                                    End If
                                End If
                            Next
                            'Look right to see if we can get a value
                            For columnsRight As Integer = neighboringPixelLine To 1 Step -1
                                If (column + columnsRight) < Width Then
                                    '(Channel * Row * Width) + (Channel * Column)
                                    Dim startOfByte As Integer = (CHANNELS * (row - rowsUp) * Width) +
                                                                 (CHANNELS * (column + columnsRight))
                                    'If little endian, which most systems are, the bytes are stored as BGRA
                                    If System.BitConverter.IsLittleEndian = True Then
                                        bSum += data1Array(startOfByte)
                                        gSum += data1Array(startOfByte + 1)
                                        rSum += data1Array(startOfByte + 2)
                                        aSum += data1Array(startOfByte + 3)
                                        tSum += 1
                                    Else
                                        'If big endian, which a lot of systems are not, the bytes are stored as ARGB
                                        aSum += data1Array(startOfByte)
                                        rSum += data1Array(startOfByte + 1)
                                        gSum += data1Array(startOfByte + 2)
                                        bSum += data1Array(startOfByte + 3)
                                        tSum += 1
                                    End If
                                End If
                            Next
                        End If
                    Next
                    'Now we can look downwards to get the bottom half of the values
                    For rowsDown As Integer = neighboringPixelLine To 1 Step -1
                        'If we can get a value from the row below proceed
                        If (row + rowsDown) < Height Then
                            'Look left to see if we can get a value
                            For columnsLeft As Integer = neighboringPixelLine To 0 Step -1
                                If (column - columnsLeft) >= 0 Then
                                    '(Channel * Row * Width) + (Channel * Column)
                                    Dim startOfByte As Integer = (CHANNELS * (row + rowsDown) * Width) +
                                                                 (CHANNELS * (column - columnsLeft))
                                    'If little endian, which most systems are, the bytes are stored as BGRA
                                    If System.BitConverter.IsLittleEndian = True Then
                                        bSum += data1Array(startOfByte)
                                        gSum += data1Array(startOfByte + 1)
                                        rSum += data1Array(startOfByte + 2)
                                        aSum += data1Array(startOfByte + 3)
                                        tSum += 1
                                    Else
                                        'If big endian, which a lot of systems are not, the bytes are stored as ARGB
                                        aSum += data1Array(startOfByte)
                                        rSum += data1Array(startOfByte + 1)
                                        gSum += data1Array(startOfByte + 2)
                                        bSum += data1Array(startOfByte + 3)
                                        tSum += 1
                                    End If
                                End If
                            Next
                            'Look right to see if we can get a value
                            For columnsRight As Integer = neighboringPixelLine To 1 Step -1
                                If (column + columnsRight) < Width Then
                                    '(Channel * Row * Width) + (Channel * Column)
                                    Dim startOfByte As Integer = (CHANNELS * (row + rowsDown) * Width) +
                                                                 (CHANNELS * (column + columnsRight))
                                    'If little endian, which most systems are, the bytes are stored as BGRA
                                    If System.BitConverter.IsLittleEndian = True Then
                                        bSum += data1Array(startOfByte)
                                        gSum += data1Array(startOfByte + 1)
                                        rSum += data1Array(startOfByte + 2)
                                        aSum += data1Array(startOfByte + 3)
                                        tSum += 1
                                    Else
                                        'If big endian, which a lot of systems are not, the bytes are stored as ARGB
                                        aSum += data1Array(startOfByte)
                                        rSum += data1Array(startOfByte + 1)
                                        gSum += data1Array(startOfByte + 2)
                                        bSum += data1Array(startOfByte + 3)
                                        tSum += 1
                                    End If
                                End If
                            Next
                        End If
                    Next
                    'Now that we have a sum total of the ARGB values, we can rewrite the current pixel value
                    bSum = CInt(bSum / tSum)
                    gSum = CInt(gSum / tSum)
                    rSum = CInt(rSum / tSum)
                    aSum = CInt(aSum / tSum)
                    If Opacity < 1.0 Then
                        aSum = CInt(Math.Ceiling(Opacity * aSum))
                    End If

                    '(Channel * Row * Width) + (Channel * Column)
                    Dim startOfPixelByte As Integer = (CHANNELS * row * Width) + (CHANNELS * column)
                    'If little endian, which most systems are, the bytes are stored as BGRA
                    If System.BitConverter.IsLittleEndian = True Then
                        data2Array(startOfPixelByte) = CByte(bSum)
                        data2Array(startOfPixelByte + 1) = CByte(gSum)
                        data2Array(startOfPixelByte + 2) = CByte(rSum)
                        data2Array(startOfPixelByte + 3) = CByte(aSum)
                    Else
                        'If big endian, which a lot of systems are not, the bytes are stored as ARGB
                        data2Array(startOfPixelByte) = CByte(aSum)
                        data2Array(startOfPixelByte + 1) = CByte(rSum)
                        data2Array(startOfPixelByte + 2) = CByte(gSum)
                        data2Array(startOfPixelByte + 3) = CByte(bSum)
                    End If

                Next
            Next



            'Copy the data back where it came from now
            Marshal.Copy(data1Array, 0, data1.Scan0, data1Array.Length - 1)
            Marshal.Copy(data2Array, 0, data2.Scan0, data2Array.Length - 1)

        Catch ex As Exception
            Throw New ArgumentException(ex.ToString())
        End Try


    End Sub
End Class
