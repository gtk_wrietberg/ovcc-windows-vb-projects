﻿Public Class Bla
    Public Class SessionStuff
        Public Class SessionAction
            Private Const ACTION__OPEN_URL = "open_url"
            Private Const ACTION__OPEN_APP = "open_app"

            Private Class _action
                Private mType As String
                Public Property Type() As String
                    Get
                        Return mType
                    End Get
                    Set(ByVal value As String)
                        mType = value
                    End Set
                End Property

                Private mParam As String
                Public Property Param() As String
                    Get
                        Return mParam
                    End Get
                    Set(ByVal value As String)
                        mParam = value
                    End Set
                End Property

                Public Sub New()
                    mType = ""
                    mParam = ""
                End Sub

                Public Sub New([Type] As String, Param As String)
                    mType = [Type]
                    mParam = Param
                End Sub
            End Class

            Private Shared _actions As New List(Of _action)

            Private Shared _type As String = ""
            Private Shared _param As String = ""

            Public Shared Sub Add_Url(Param As String)
                _add(ACTION__OPEN_URL, Param)
            End Sub

            Public Shared Sub Add_App(Param As String)
                _add(ACTION__OPEN_APP, Param)
            End Sub

            Private Shared Sub _add(Type As String, Param As String)
                _actions.Add(New _action([Type], Param))
            End Sub

            Public Shared Sub Reset()
                _actions.Clear()
            End Sub

            Public Shared Function ExecuteNext() As Boolean
                If _actions.Count > 0 Then
                    Dim _current_action As _action = _actions.Item(0)
                    _actions = _actions.Skip(1).ToList

                    If _current_action.Type.Equals(ACTION__OPEN_URL) Then
                        _OpenDefaultBrowser(_current_action.Param)
                    End If
                    If _current_action.Type.Equals(ACTION__OPEN_APP) Then
                        _OpenApp(_current_action.Param)
                    End If

                    Return True
                Else
                    Return False
                End If
            End Function


            '--------------------------------------------------
            Private Shared Function _OpenDefaultBrowser(Url As String) As Boolean
                Dim bRet As Boolean = False

                MsgBox("Opening URL: " & Url)


                Return bRet
            End Function

            Private Shared Function _OpenApp(Path As String) As Boolean
                Dim bRet As Boolean = False

                MsgBox("Opening App: " & Path)


                Return bRet
            End Function
        End Class
    End Class
End Class
