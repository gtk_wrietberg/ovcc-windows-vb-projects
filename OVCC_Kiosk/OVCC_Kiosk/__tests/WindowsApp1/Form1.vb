﻿

Imports System.Text.RegularExpressions

Public Class Form1
    Private mLetters As New List(Of String)

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        mLetters.Add("A")
        mLetters.Add("B")
        mLetters.Add("C")
        mLetters.Add("D")
        mLetters.Add("E")
        mLetters.Add("F")
        mLetters.Add("G")
        mLetters.Add("H")
        mLetters.Add("I")
        mLetters.Add("J")


        LinkLabel1.Links.Clear()
        LinkLabel1.Text = String.Join(" - ", mLetters.ToArray)

        For i As Integer = 0 To mLetters.Count - 1
            LinkLabel1.Links.Add(4 * i, 1)
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Bla.SessionStuff.SessionAction.Add_App("01 ABC")
        Bla.SessionStuff.SessionAction.Add_Url("02 DEF")
        Bla.SessionStuff.SessionAction.Add_App("03 GHI")
        Bla.SessionStuff.SessionAction.Add_Url("04 JKL")
        Bla.SessionStuff.SessionAction.Add_App("05 MNO")
        Bla.SessionStuff.SessionAction.Add_Url("06 PQR")
        Bla.SessionStuff.SessionAction.Add_App("07 STU")
        Bla.SessionStuff.SessionAction.Add_Url("08 VWX")
        Bla.SessionStuff.SessionAction.Add_App("09 YZA")
        Bla.SessionStuff.SessionAction.Add_Url("10 BCD")
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Do While Bla.SessionStuff.SessionAction.ExecuteNext
            MsgBox("NEXT")
        Loop

        MsgBox("done")
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        MsgBox(LinkLabel1.Text.Substring(e.Link.Start, 1))
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim lst As New List(Of String)

        lst.Add("piemel")
        lst.Add("pik")

        MsgBox(String.Join(".", lst.ToArray))

        For i As Integer = 0 To lst.Count - 1
            lst.Item(i) &= "saus"
        Next




        MsgBox(String.Join(".", lst.ToArray))
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim re As New Regex("^ExternalApplication_([0-9]{3})__.+$")

        MsgBox(re.Replace("ExternalApplication_123__Name", "$1"))
        'ExternalApplication_%%%__Name
    End Sub
End Class
