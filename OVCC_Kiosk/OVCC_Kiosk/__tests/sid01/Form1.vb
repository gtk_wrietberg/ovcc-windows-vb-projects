﻿Imports System.Security.Principal

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Dim sId As String = ""

            If Helpers.WindowsUser.ConvertUsernameToSid(TextBox1.Text, sId) Then
                TextBox2.Text = sId
            Else
                TextBox2.Text = "FAIL!!! " & Helpers.Errors.GetLast()
            End If
        Catch ex As Exception
            TextBox2.Text = ex.Message
        End Try

        ' HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\S-1-5-21-850432816-418829102-2837382953-1029

        MsgBox(Helpers.Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\" & TextBox2.Text, "ProfileImagePath"))

        'System.Security.Principal.WindowsIdentity id = System.Security.Principal.WindowsIdentity.GetCurrent();
        '    String sid = id.User.AccountDomainSid.ToString();
    End Sub
End Class
