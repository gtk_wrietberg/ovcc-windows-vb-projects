﻿Imports System.ServiceModel

Public Class ServiceCommunicationClient
    Private Shared _pipeFactory As ChannelFactory(Of IServiceCommunication)
    Private Shared _pipeProxy As IServiceCommunication

    Private Shared mLastError As String
    Public Shared Property LastError() As String
        Get
            Return mLastError
        End Get
        Set(ByVal value As String)
            mLastError = value
        End Set
    End Property

    Public Shared Function OpenPipe(ByRef Response As String) As Boolean
        Dim bRet As Boolean = OpenPipe()

        If bRet Then
            Response = ""
        Else
            Response = mLastError
        End If

        Return bRet
    End Function

    Public Shared Function OpenPipe() As Boolean
        Try
            _pipeFactory = New ChannelFactory(Of IServiceCommunication)(New NetNamedPipeBinding(), New EndpointAddress(OVCC_Constants.Pipe.URI & "/" & OVCC_Constants.Pipe.Endpoint))
            _pipeProxy = _pipeFactory.CreateChannel()
        Catch ex As Exception
            mLastError = ex.Message

            Return False
        End Try

        Return True
    End Function

    Public Enum ServiceCommunicationMessageType As Integer
        InternalHeartbeat = 0
        LogonDetected = 1
        LogoutDetected = 2
        ClientError = 3

    End Enum

    Public Shared Function SendMessage(MessageType As ServiceCommunicationMessageType, ByRef Response As String) As Boolean
        Return SendMessage(MessageType, "", Response)
    End Function

    Public Shared Function SendMessage(MessageType As ServiceCommunicationMessageType, Param As String, ByRef Response As String) As Boolean
        Dim bResponse As Boolean = False
        Dim sResponse As String = ""


        Select Case MessageType
            Case ServiceCommunicationMessageType.InternalHeartbeat
                Try
                    bResponse = _pipeProxy.InternalHeartbeat()
                Catch ex As Exception
                    bResponse = False
                    sResponse = ex.Message
                End Try
            Case ServiceCommunicationMessageType.LogoutDetected
                Try
                    bResponse = _pipeProxy.LogoutTriggered()
                Catch ex As Exception
                    bResponse = False
                    sResponse = ex.Message
                End Try
            Case ServiceCommunicationMessageType.LogonDetected
                Try
                    bResponse = _pipeProxy.LogonTriggered()
                Catch ex As Exception
                    bResponse = False
                    sResponse = ex.Message
                End Try
            Case ServiceCommunicationMessageType.ClientError
                Try
                    bResponse = _pipeProxy.ClientError(Param)
                Catch ex As Exception
                    bResponse = False
                    sResponse = ex.Message
                End Try
            Case Else
                bResponse = False
                sResponse = "Incorrect message for service"
        End Select


        Response = sResponse
        Return bResponse
    End Function


End Class
