﻿Public Class Form1
    Private Response As String = ""

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not ServiceCommunication_Client.OpenPipe() Then
            MsgBox("OOPS!!!")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim _r As String = ServiceCommunication_Client.SendMessage(ServiceCommunication_Client.ServiceCommunicationMessageType.InternalHeartbeat)

        MsgBox("OOPS (InternalHeartbeat): " & _r)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim _r As String = ServiceCommunication_Client.SendMessage(ServiceCommunication_Client.ServiceCommunicationMessageType.LogonDetected)

        MsgBox("OOPS (LogonDetected): " & _r)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim _r As String = ServiceCommunication_Client.SendMessage(ServiceCommunication_Client.ServiceCommunicationMessageType.LogoutDetected)

        MsgBox("OOPS (LogoutDetected): " & _r)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim _r As String = ServiceCommunication_Client.SendMessage(ServiceCommunication_Client.ServiceCommunicationMessageType.ClientError)

        MsgBox("OOPS (ClientError): " & _r)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim sRand As String = RandomString(25)

        Dim _r As String = ServiceCommunication_Client.SendMessage(ServiceCommunication_Client.ServiceCommunicationMessageType.ClientError, sRand)

        MsgBox("OOPS (ClientError): " & _r)
    End Sub

    '============================
    Public Function RandomString(Length As Integer) As String
        Return RandomString(Length, "")
    End Function

    Public Function RandomString(Length As Integer, CharsToChooseFrom As String) As String
        If CharsToChooseFrom = "" Then
            'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
            CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        End If
        If Length < 1 Then
            Length = 1
        End If

        Dim sTmp As String = "", iRnd As Integer

        Randomize()

        For i As Integer = 0 To Length - 1
            iRnd = Int(CharsToChooseFrom.Length * Rnd())
            sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
        Next

        Return sTmp
    End Function

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        If Not ServiceCommunication_Client.OpenPipe() Then
            MsgBox("OOPS!!!")
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Dim _dc As New ServiceCommunication_Shared.dc

        _dc = ServiceCommunication_Client.SendMessage_TextList

        MsgBox("_dc.title=" & _dc.Title)
        MsgBox("_dc.message=" & _dc.Message)

    End Sub
End Class
