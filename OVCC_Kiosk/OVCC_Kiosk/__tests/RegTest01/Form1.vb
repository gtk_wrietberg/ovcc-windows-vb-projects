﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Forms.AxHost

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim s As String = ""

        'For i As Integer = 0 To 99
        '    s = OVCC_Data.Registry.KEY__OVCCKiosk_Messages & "\Message" & Helpers.Types.LeadingZero(i, 2)

        '    Helpers.Registry.CreateKey(s)
        '    Helpers.Registry.SetValue_String(s, "Title", "")
        '    Helpers.Registry.SetValue_String(s, "Text", "")
        '    Helpers.Registry.SetValue_Integer(s, "Type", 0)
        '    Helpers.Registry.SetValue_Integer(s, "Timeout", 0)
        'Next
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        TextBox2.Text = Helpers.XOrObfuscation_v2.Obfuscate(TextBox1.Text)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Label2.Text = TextBox3.Text
        Label3.Text = TextBox3.Text & TextBox3.Text



        Panel1.Width = Math.Max(Label2.Width, Label3.Width)

        Label2.Left = Panel1.Width - Label2.Width
        Label3.Left = Panel1.Width - Label3.Width
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ToggleTaskManager(True)



    End Sub

    Private Sub ToggleTaskManager(state As Boolean)
        If state Then
            WindowsPolicies.TaskManager.Enable()
        Else
            WindowsPolicies.TaskManager.Disable()
        End If

        btnToggle.Text = If(WindowsPolicies.TaskManager.IsDisabled, "enable", "disable")
    End Sub

    Private Sub btnToggle_Click(sender As Object, e As EventArgs) Handles btnToggle.Click
        If WindowsPolicies.TaskManager.IsDisabled Then
            ToggleTaskManager(True)
        Else
            ToggleTaskManager(False)
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        TextBox4.Text = DateTime.Now.Ticks.ToString
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        TextBox5.Text = DateTime.MaxValue.Ticks.ToString

        Dim d As New DateTime(CLng(TextBox5.Text))

        MsgBox(d.ToString)
    End Sub
End Class
