﻿Public Class Form1
    Private ReadOnly c__REGISTRY_KEY__ProfileImagePath As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\%%PROFILE_SID_STRING%%"
    Private ReadOnly c__REGISTRY_VALUE__ProfileImagePath As String = "ProfileImagePath"



    Private ReadOnly c__Path01 As String = "C:\Temp\Profile\copy01\OVCC"
    Private ReadOnly c__Path02 As String = "C:\Temp\Profile\copy02\OVCC"




    Public g_RegKey As String = ""

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sid As String = ""

        If Not Helpers.WindowsUser.ConvertUsernameToSid("OVCC", sid) Then
            MsgBox("error getting sid!!!")



            Exit Sub()
        End If

        g_RegKey = c__REGISTRY_KEY__ProfileImagePath.Replace("%%PROFILE_SID_STRING%%", sid)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            MsgBox("profile: " & Helpers.Registry.GetValue_String(g_RegKey, c__REGISTRY_VALUE__ProfileImagePath))

        Catch ex As Exception
            MsgBox("error: " & ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim s As String = ""

        s = Helpers.Registry.GetValue_String(g_RegKey, c__REGISTRY_VALUE__ProfileImagePath)

        If s.Equals(c__Path01) Then
            Helpers.Registry.SetValue_String(g_RegKey, c__REGISTRY_VALUE__ProfileImagePath, c__Path02)

            Button1.PerformClick()
        ElseIf s.Equals(c__Path02) Then
            Helpers.Registry.SetValue_String(g_RegKey, c__REGISTRY_VALUE__ProfileImagePath, c__Path01)

            Button1.PerformClick()
        Else
            MsgBox("huh?")
        End If

    End Sub
End Class
