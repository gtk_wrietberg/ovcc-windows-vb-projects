﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Button1 = New Button()
        TextBox1 = New TextBox()
        Panel1 = New Panel()
        PictureBox1 = New PictureBox()
        pnlCOntainer = New Panel()
        Panel5 = New Panel()
        Panel4 = New Panel()
        Panel3 = New Panel()
        picWord = New PictureBox()
        CType(PictureBox1, ComponentModel.ISupportInitialize).BeginInit()
        pnlCOntainer.SuspendLayout()
        Panel3.SuspendLayout()
        CType(picWord, ComponentModel.ISupportInitialize).BeginInit()
        SuspendLayout()
        ' 
        ' Button1
        ' 
        Button1.Location = New Point(12, 41)
        Button1.Name = "Button1"
        Button1.Size = New Size(299, 104)
        Button1.TabIndex = 0
        Button1.Text = "Button1"
        Button1.UseVisualStyleBackColor = True
        ' 
        ' TextBox1
        ' 
        TextBox1.Location = New Point(12, 12)
        TextBox1.Name = "TextBox1"
        TextBox1.Size = New Size(299, 23)
        TextBox1.TabIndex = 1
        TextBox1.Text = ".docx"
        TextBox1.TextAlign = HorizontalAlignment.Center
        ' 
        ' Panel1
        ' 
        Panel1.BorderStyle = BorderStyle.FixedSingle
        Panel1.Location = New Point(337, 41)
        Panel1.Name = "Panel1"
        Panel1.Size = New Size(37, 30)
        Panel1.TabIndex = 2
        ' 
        ' PictureBox1
        ' 
        PictureBox1.BorderStyle = BorderStyle.FixedSingle
        PictureBox1.Location = New Point(337, 94)
        PictureBox1.Name = "PictureBox1"
        PictureBox1.Size = New Size(100, 111)
        PictureBox1.TabIndex = 3
        PictureBox1.TabStop = False
        ' 
        ' pnlCOntainer
        ' 
        pnlCOntainer.Controls.Add(Panel5)
        pnlCOntainer.Controls.Add(Panel4)
        pnlCOntainer.Controls.Add(Panel3)
        pnlCOntainer.Location = New Point(520, 235)
        pnlCOntainer.Name = "pnlCOntainer"
        pnlCOntainer.Size = New Size(480, 228)
        pnlCOntainer.TabIndex = 4
        ' 
        ' Panel5
        ' 
        Panel5.BackColor = Color.Blue
        Panel5.Dock = DockStyle.Left
        Panel5.Location = New Point(320, 0)
        Panel5.Name = "Panel5"
        Panel5.Size = New Size(160, 228)
        Panel5.TabIndex = 2
        ' 
        ' Panel4
        ' 
        Panel4.BackColor = Color.Green
        Panel4.Dock = DockStyle.Left
        Panel4.Location = New Point(160, 0)
        Panel4.Name = "Panel4"
        Panel4.Size = New Size(160, 228)
        Panel4.TabIndex = 1
        ' 
        ' Panel3
        ' 
        Panel3.BackColor = Color.Red
        Panel3.Controls.Add(picWord)
        Panel3.Dock = DockStyle.Left
        Panel3.Location = New Point(0, 0)
        Panel3.Name = "Panel3"
        Panel3.Size = New Size(160, 228)
        Panel3.TabIndex = 0
        ' 
        ' picWord
        ' 
        picWord.BackColor = Color.Transparent
        picWord.BorderStyle = BorderStyle.FixedSingle
        picWord.Location = New Point(3, 3)
        picWord.Name = "picWord"
        picWord.Size = New Size(151, 119)
        picWord.TabIndex = 5
        picWord.TabStop = False
        ' 
        ' Form1
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(1152, 529)
        Controls.Add(pnlCOntainer)
        Controls.Add(PictureBox1)
        Controls.Add(Panel1)
        Controls.Add(TextBox1)
        Controls.Add(Button1)
        Name = "Form1"
        Text = "Form1"
        CType(PictureBox1, ComponentModel.ISupportInitialize).EndInit()
        pnlCOntainer.ResumeLayout(False)
        Panel3.ResumeLayout(False)
        CType(picWord, ComponentModel.ISupportInitialize).EndInit()
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents pnlCOntainer As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents picWord As PictureBox

End Class
