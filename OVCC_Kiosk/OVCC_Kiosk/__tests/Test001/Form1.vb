﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim _b As Boolean
        Dim _name As String = ""
        Dim _application As String = ""
        Dim _description As String = ""

        If LinkExtensionToApplication.HasApplication(TextBox1.Text) Then
            Panel1.BackColor = Color.DarkGreen
        Else
            Panel1.BackColor = Color.DarkRed
        End If

        If LinkExtensionToApplication.GetInfo(TextBox1.Text, _name, _application) Then
            Dim _icon As Icon = Icon.ExtractIcon(_application, 0, 256)

            PictureBox1.Image = _icon.ToBitmap
            PictureBox1.Height = _icon.Height
            PictureBox1.Width = _icon.Width

            _description = Helpers.Windows.Programs.FileInfo.GetFileDescription(_application)


            MsgBox("name: " & _name & vbCrLf & "app: " & _application & vbCrLf & "description: " & _description)
        Else
            PictureBox1.Image = Nothing

            MsgBox("error: " & LinkExtensionToApplication.LastError)
        End If


    End Sub
End Class
