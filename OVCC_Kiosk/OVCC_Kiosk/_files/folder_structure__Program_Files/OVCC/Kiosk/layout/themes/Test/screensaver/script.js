var SCREENSAVER_DEBUG=false;

function DEBUG(txt) {
	try {
		if(SCREENSAVER_DEBUG) $('#txtDebug').append(txt+"\n"); 
	}
	catch(e) {}
}

$(document).ready(function() {
	StartSlideshow();
});

var imgsScr=new Array();
imgsScr.push('images/1_boarding-1920x1080.jpg');
imgsScr.push('images/2_createdocuments-1920x1080.jpg');
imgsScr.push('images/3_search-1920x1080.jpg');
imgsScr.push('images/4_entertainment-1920x1080.jpg');
imgsScr.push('images/5_info-1920x1080.jpg');




var imgCurrent=0;

function StartSlideshow() {
	if(SCREENSAVER_DEBUG) $('#divDebug').css('display','block');
	
	DEBUG("imgsScr.length="+imgsScr.length);
	
	LoadNextImage();
}

function LoadNextImage() {
	DEBUG("loading image ("+imgCurrent+"): "+imgsScr[imgCurrent]);

	$('#divImg').css('background-image','url("'+imgsScr[imgCurrent]+'")');

	imgCurrent++;
	DEBUG("imgCurrent="+imgCurrent);

	DEBUG("imgCurrent>=imgsScr.length ="+imgCurrent+">="+imgsScr.length);

	if(imgCurrent>=imgsScr.length) imgCurrent=0;

	FadeIn();
}

function FadeIn() {
	$('#divImg').fadeTo(2500,1.0,function() {
		Wait();
	});
}

function Wait() {
	setTimeout('FadeOut()',5000);
}

function FadeOut() {
	$('#divImg').fadeTo(2500,0.0,function() {
		LoadNextImage();
	});
}
