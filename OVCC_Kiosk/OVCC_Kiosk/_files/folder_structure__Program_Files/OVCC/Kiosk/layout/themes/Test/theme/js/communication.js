async function Initialise() {
	try {
		document.getElementById("idInterfaceReturnCode").innerHTML="init";
		
		document.getElementById("idInterfaceReturnCode").innerHTML="waiting";
		const ret=await chrome.webview.hostObjects.OVCC_Kiosk.InitialiseScriptInterface();
		
		document.getElementById("idInterfaceReturnCode").innerHTML="return: "+ret;
	}
	catch(e) {
		LogError("Initialise error: "+e.message);
		document.getElementById("idInterfaceReturnCode").innerHTML="fail: "+e.message;
	}
	
	HideLogoutButton();
	ShowHomepageMenu();
	ShowInformationPanel();
	
	LoadLocalisation();
	SessionCheck();
}


//--------------------------------------------------------------------------------------
//UI
async function ShowLogoutButton() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.ShowLogoutButton();
	}
	catch(e) {
		LogError("ShowLogoutButton error: "+e.message);
	}
}

async function HideLogoutButton() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.HideLogoutButton();
	}
	catch(e) {
		LogError("HideLogoutButton error: "+e.message);
	}
}

async function ShowHomepageMenu() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.ShowHomepageMenu();
	}
	catch(e) {
		LogError("ShowHomepageMenu error: "+e.message);
	}
}

async function HideHomepageMenu() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.HideHomepageMenu();
	}
	catch(e) {
		LogError("HideHomepageMenu error: "+e.message);
	}
}

async function ShowInformationPanel() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.ShowInformationPanel();
	}
	catch(e) {
		LogError("ShowInformationPanel error: "+e.message);
	}
}

async function HideInformationPanel() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.HideInformationPanel();
	}
	catch(e) {
		LogError("HideInformationPanel error: "+e.message);
	}
}
//--------------------------------------------------------------------------------------


function OpenInternet() {
	if(!OpenUrlByName('internet')) {
		alert("Something went wrong!");
	}
}

function OpenHotelInfo() {
	if(!OpenUrlByName('hotelinfo')) {
		alert("Something went wrong!");
	}
}

function OpenMaps() {
	if(!OpenUrlByName('maps')) {
		alert("Something went wrong!");
	}
}

function OpenWeather() {
	if(!OpenUrlByName('weather')) {
		alert("Something went wrong!");
	}
}

async function OpenAirlines() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.OpenAirlinesDialog();
	}
	catch(e) {
		LogError("OpenAirlines error: "+e.message);
	}
}

async function OpenUrlByName(url_name) {
	let bRet=false;

	try {
		bRet=await chrome.webview.hostObjects.OVCC_Kiosk.UI.OpenUrlByName(url_name);
	}
	catch(e) {
		LogError("OpenUrlByName error: "+e.message);
	}
	
	return bRet;
}

async function OpenNotepad() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.OpenApp("Notepad");
	}
	catch(e) {
		LogError("OpenNotepad error: "+e.message);
	}
}

async function OpenMSPaint() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.UI.OpenApp("MSPaint");
	}
	catch(e) {
		LogError("OpenMSPaint error: "+e.message);
	}
}

async function LoadLocalisation() {
	try {
		var aLocalisation=[];
		var inputs=document.getElementsByTagName("span");
		for(var i=0;i<inputs.length;i++) {
			console.log(i+": "+inputs[i].id);
			if(inputs[i].id.indexOf('LanguageString_')==0) {
				const str=await LoadString(inputs[i].id.replace('LanguageString_',''));
				inputs[i].innerHTML=str;
			}
		}
	}
	catch(e) {
		LogError("LoadLocalisation() error: "+e.message);
	}
}

async function LoadString(StringId) {
	var str="[String:?,"+StringId+"]";
	
	try {
		str=await chrome.webview.hostObjects.OVCC_Kiosk.Languages.LoadString(StringId);
	}
	catch(e) {
		LogError("LoadString error: "+e.message);
	}
	
	return str;
}


async function SessionCheck() {
	try {
		const isSessionActive=await chrome.webview.hostObjects.OVCC_Kiosk.Session.IsActive();
		const sessionId=await chrome.webview.hostObjects.OVCC_Kiosk.Session.SessionId();
		
		document.getElementById("idSessionActive").innerHTML=isSessionActive;
		document.getElementById("idSessionId").innerHTML=sessionId;
	}
	catch(e) {
		LogError("Error in SessionCheck: "+e.message);
	}
	
	setTimeout('SessionCheck()',1000);
}


async function TriggerTerms() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.Session.TriggerTerms();
		
		LogMessage("terms triggered");
	}
	catch(e) {
		LogError("Error in TriggerTerms: "+e.message);
	}
}


async function SessionLogout() {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.Session.SessionLogout();
		
		LogMessage("logout triggered");
	}
	catch(e) {
		LogError("Error in SessionLogout: "+e.message);
	}
}



//============================================================================================

async function LogMessage(sMessage) {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.Logger.Message(sMessage);
	}
	catch(e) {
		
	}
}

async function LogWarning(sMessage) {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.Logger.Warning(sMessage);
	}
	catch(e) {
		
	}
}

async function LogError(sMessage) {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.Logger.Error(sMessage);
	}
	catch(e) {
		
	}
}

async function LogDebug(sMessage) {
	try {
		await chrome.webview.hostObjects.OVCC_Kiosk.Logger.Debug(sMessage);
	}
	catch(e) {
		
	}
}


window.onload=function() {
	Initialise();
};
