function hideButton(buttonName) {
	if(document.getElementById("grid_button_"+buttonName)) {
		let _button_count=document.querySelectorAll('[id^=grid_button_]').length;

		document.getElementById("grid_button_"+buttonName).remove();
		document.getElementById("grid_label_"+buttonName).remove();
		

		var _button_gridTemplateAreas=_getGridTemplateAreas('gridButtons');
		var _label_gridTemplateAreas=_getGridTemplateAreas('gridLabels');
		
		_button_gridTemplateAreas=_button_gridTemplateAreas.replace("button_"+buttonName,"").replace("  "," ").trim();
		_label_gridTemplateAreas=_label_gridTemplateAreas.replace("label_"+buttonName,"").replace("  "," ").trim();
		
		document.getElementById("gridButtons").style.gridTemplateColumns="1fr ".repeat(_button_count-1);
		document.getElementById("gridLabels").style.gridTemplateColumns="1fr ".repeat(_button_count-1);
		
		document.getElementById("gridButtons").style.gridTemplateAreas=_button_gridTemplateAreas;
		document.getElementById("gridLabels").style.gridTemplateAreas=_label_gridTemplateAreas;
	}
}

function _getGridTemplateAreas(element_name) {
	let element=document.getElementById(element_name);
	
	if(element) {
		let css_obj=getComputedStyle(element);
		return css_obj.getPropertyValue('grid-template-areas');
	}
	
	return '';
}
