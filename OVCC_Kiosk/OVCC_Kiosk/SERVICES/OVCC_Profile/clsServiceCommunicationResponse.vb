﻿Public Class ServiceCommunicationResponse
    Public Sub New()
        mResult = False
        mResponse = ""
    End Sub

    Public Sub New(newResult As Boolean, newResponse As String)
        mResult = newResult
        mResponse = newResponse
    End Sub

    Private mResult As Boolean
    Public Property Result() As Boolean
        Get
            Return mResult
        End Get
        Set(ByVal value As Boolean)
            mResult = value
        End Set
    End Property

    Private mResponse As String
    Public Property Response() As String
        Get
            Return mResponse
        End Get
        Set(ByVal value As String)
            mResponse = value
        End Set
    End Property
End Class
