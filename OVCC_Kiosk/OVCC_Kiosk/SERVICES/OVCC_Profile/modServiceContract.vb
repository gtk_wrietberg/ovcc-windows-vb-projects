﻿Imports System.ServiceModel

Module modServiceContract

    <ServiceContract>
    Public Interface IServiceCommunication
#Region "heartbeat"
        ''' <summary>
        ''' Sends heartbeat to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function InternalHeartbeat() As ServiceCommunicationResponse
#End Region

#Region "errors"
        ''' <summary>
        ''' Sends heartbeat to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function ClientError(ErrorString As String) As ServiceCommunicationResponse
#End Region

#Region "logon / logoff / shutdown"
        ''' <summary>
        ''' Signals logon to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function LogonTriggered() As ServiceCommunicationResponse

        ''' <summary>
        ''' Signals logout to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function LogoutTriggered() As ServiceCommunicationResponse

        ''' <summary>
        ''' Signals shutdown to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function ShutdownTriggered() As ServiceCommunicationResponse

        ''' <summary>
        ''' Signals suspend to service
        ''' </summary>
        ''' <returns>ServiceCommunicationResponse</returns>
        <OperationContract()>
        Function SuspendTriggered() As ServiceCommunicationResponse
#End Region
    End Interface

End Module
