﻿Public Class ServiceCommunication
    Implements IServiceCommunication

#Region "thread safe event blah"
    Private EventHandlerList As New ArrayList
    Private o As New Object

    Custom Event SynchroEvent As EventHandler
        AddHandler(ByVal value As EventHandler)
            EventHandlerList.Add(value)
        End AddHandler
        RemoveHandler(ByVal value As EventHandler)
            EventHandlerList.Remove(value)
        End RemoveHandler
        RaiseEvent(ByVal sender As Object, ByVal e As EventArgs)
            SyncLock (o)
                For Each handler As EventHandler In EventHandlerList
                    If handler IsNot Nothing Then
                        handler.Invoke(sender, e)
                    End If
                Next
            End SyncLock
        End RaiseEvent
    End Event
#End Region


#Region "heartbeat"
    Public Function InternalHeartbeat() As ServiceCommunicationResponse Implements IServiceCommunication.InternalHeartbeat
        Helpers.Logger.WriteMessage("heartbeat")

        Return New ServiceCommunicationResponse(True, "ok")
    End Function
#End Region

#Region "errors"
    Public Function ClientError(ErrorString As String) As ServiceCommunicationResponse Implements IServiceCommunication.ClientError
        Helpers.Logger.WriteMessage("client error: " & ErrorString)

        Return New ServiceCommunicationResponse(True, "ok")
    End Function
#End Region

#Region "logon / logoff / shutdown"
    Public Function LogonTriggered() As ServiceCommunicationResponse Implements IServiceCommunication.LogonTriggered
        Return LogonTriggeredActions()
    End Function

    Public Function LogoutTriggered() As ServiceCommunicationResponse Implements IServiceCommunication.LogoutTriggered
        Return LogoutTriggeredActions()
    End Function

    Public Function SuspendTriggered() As ServiceCommunicationResponse Implements IServiceCommunication.SuspendTriggered
        Helpers.Logger.WriteMessage("SuspendTriggeredActions")

        Return New ServiceCommunicationResponse(True, "ok")
    End Function

    Public Function ShutdownTriggered() As ServiceCommunicationResponse Implements IServiceCommunication.ShutdownTriggered
        Helpers.Logger.WriteMessage("ShutdownTriggeredActions")

        Return New ServiceCommunicationResponse(True, "ok")
    End Function
#End Region


    '========================================================================================================================================================================
    Public Function LogoutTriggeredActions() As ServiceCommunicationResponse
        Helpers.Logger.WriteMessage("Switching user profile")
        Helpers.Logger.WriteMessage("active", 1)
        Helpers.Logger.WriteMessage(clsSession.ActiveProfile.ToString, 2)

        Helpers.Logger.WriteMessage("swapping", 1)
        clsSession.SetNextProfile()

        Helpers.Logger.WriteMessage("active", 1)
        Helpers.Logger.WriteMessage(clsSession.ActiveProfile.ToString, 2)

        Helpers.Registry.SetValue_Integer(OVCC_Data.Registry.KEY__OVCCKiosk_Profiles, OVCC_Data.Registry.VALUENAME__Profiles_Active, clsSession.ActiveProfile)


        Return New ServiceCommunicationResponse(True, "ok")
    End Function

    Public Function LogonTriggeredActions() As ServiceCommunicationResponse
        Helpers.Logger.WriteMessage("Cleaning inactive profile")
        If clsSession.CleanInactiveProfile() Then
            Helpers.Logger.WriteMessage("ok", 1)
        Else
            Helpers.Logger.WriteError("fail", 1)
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
        End If

        Return New ServiceCommunicationResponse(True, "ok")
    End Function
End Class
