﻿Public Class KioskProfile
    Private Shared ReadOnly c__MAX_PROFILES As Integer = 2

    Private Shared mProfilePaths As List(Of String)
    Private Shared mActiveProfile As Integer


    Public Shared Sub Initialise(sKioskUserName As String)
        Dim sid As String = ""

        mKioskUserName = sKioskUserName

        If Not Helpers.WindowsUser.ConvertUsernameToSid(mKioskUserName, sid) Then
            mProfilesRegistryKey = ""
        Else
            mProfilesRegistryKey = Settings.Constants.Registry.Windows.KEY__ProfileImagePath.Replace("%%PROFILE_SID_STRING%%", sid)
        End If

        mProfilePaths = New List(Of String)
    End Sub

    Private Shared mKioskUserName As String
    Public Shared Property KioskUserName() As String
        Get
            Return mKioskUserName
        End Get
        Set(ByVal value As String)
            mKioskUserName = value
        End Set
    End Property


#Region "profile paths"
    Public Shared Function Path_ActiveProfile() As String
        Return mProfilePaths(mActiveProfile)
    End Function

    Public Shared Function Path_Profile(index As Integer) As String
        If index < 0 Or index >= mProfilePaths.Count Then
            Return ""
        End If

        Return mProfilePaths(index)
    End Function

    Private Shared mPath_CleanProfile As String
    Public Shared Property Path_CleanProfile() As String
        Get
            Return mPath_CleanProfile
        End Get
        Set(ByVal value As String)
            mPath_CleanProfile = value
        End Set
    End Property

    Public Shared Property ActiveProfile() As Integer
        Get
            mActiveProfile = Settings.Get.Integer(Settings.Constants.Registry.OVCC.KEY__OVCCKiosk_Profiles, Settings.Constants.Registry.OVCC.VALUENAME__Profiles_Active, 0)

            Return mActiveProfile
        End Get
        Set(ByVal value As Integer)
            If value < 0 Then value = 0
            If value >= c__MAX_PROFILES Then value = c__MAX_PROFILES - 1

            mActiveProfile = value
            Settings.Set.Integer(Settings.Constants.Registry.OVCC.KEY__OVCCKiosk_Profiles, Settings.Constants.Registry.OVCC.VALUENAME__Profiles_Active, mActiveProfile)
        End Set
    End Property

    Public Shared ReadOnly Property PreviousProfile() As Integer
        Get
            Dim tmp As Integer = mActiveProfile

            tmp -= 1

            If tmp < 0 Then
                tmp = (c__MAX_PROFILES - 1)
            End If

            Return tmp
        End Get
    End Property

    Public Shared Function GetNextProfile() As Integer
        Dim tmp As Integer = mActiveProfile

        tmp += 1

        If tmp >= c__MAX_PROFILES Then
            tmp = 0
        End If

        Return tmp
    End Function

    Public Shared Function SetNextProfile() As Integer
        mActiveProfile += 1

        If mActiveProfile >= c__MAX_PROFILES Then
            mActiveProfile = 0
        End If

        Return mActiveProfile
    End Function

    Public Shared Sub AddProfilePath(Path As String)
        mProfilePaths.Add(Path)
    End Sub
#End Region

    Private Shared mProfilesRegistryKey As String
    Public Shared ReadOnly Property ProfilesRegistryKey() As String
        Get
            Return mProfilesRegistryKey
        End Get
    End Property

    Public Shared Function WriteProfileToRegistry() As Boolean
        Try
            Settings.Set.String(mProfilesRegistryKey, Settings.Constants.Registry.Windows.VALUENAME__ProfileImagePath, Path_ActiveProfile)
        Catch ex As Exception
            Helpers.Errors.Add(ex.Message)

            Return False
        End Try

        Return True
    End Function

    Public Shared Function CleanActiveProfile() As Boolean
        Return _CleanProfile(mActiveProfile)
    End Function

    Public Shared Function CleanInactiveProfile() As Boolean
        Return _CleanProfile((c__MAX_PROFILES - 1) - mActiveProfile)
    End Function

    Private Shared Function _CleanProfile(ProfileIndex As Integer) As Boolean
        Dim sPath As String = Path_Profile(ProfileIndex)
        Dim lErrors As Long = 0

        Try
            If sPath.Equals("") Then
                Throw New Exception("Profile path empty!")
            End If

            If mPath_CleanProfile.Equals("") Then
                Throw New Exception("Active profile path empty!")
            End If

            If Not IO.Directory.Exists(sPath) Then
                Throw New Exception("Profile path ('" & sPath & "') does not exist!")
            End If

            If Not IO.Directory.Exists(mPath_CleanProfile) Then
                Throw New Exception("Active profile path ('" & mPath_CleanProfile & "') does not exist!")
            End If


            Dim oCopy As New Helpers.FilesAndFolders.CopyFiles With {
                .BackupDirectory = "", 'disables backup 
                .SourceDirectory = mPath_CleanProfile,
                .DestinationDirectory = sPath,
                .SuppressCopyLogging = True
            }


            lErrors = Helpers.FilesAndFolders.Folder.CleanFolder(sPath, "I_am_sure")

            oCopy.CopyFiles()

            Return True
        Catch ex As Exception
            Helpers.Errors.Add(ex.Message)
        End Try

        Return False
    End Function
End Class
