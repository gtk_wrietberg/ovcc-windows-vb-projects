﻿Public Class Integrity
    Private Shared mDebug As Boolean = False
    Public Shared Property Debugging() As Boolean
        Get
            Return mDebug
        End Get
        Set(ByVal value As Boolean)
            mDebug = value
        End Set
    End Property

    Public Class Processes
        Public Shared Function IsRunning__UI() As Boolean
            'check if UI is running

            Return Helpers.Processes.IsProcessRunning(Settings.Constants.Paths.Files.Homepage)
        End Function
    End Class

    Public Class Permissions
        Public Shared Function LogfileFolder() As Boolean
            If Not IO.Directory.Exists(Settings.Constants.Paths.Folders.Kiosk_Logfiles) Then
                Return False
            End If


            Try
                ''Replace all permissions with FullControl for KIOSK_ADMIN
                'Helpers.FilesAndFolders.Permissions.FolderSecurity_ADD(
                '    Settings.Constants.Paths.Folders.Kiosk_Logfiles,
                '    Settings.Constants.Users.Usernames.KIOSK_ADMIN,
                '    Security.AccessControl.FileSystemRights.FullControl,
                '    Security.AccessControl.AccessControlType.Allow,
                '    True)

                ''Add KIOSK_USER with READ/WRITE
                'Helpers.FilesAndFolders.Permissions.FolderSecurity_ADD(
                '    Settings.Constants.Paths.Folders.Kiosk_Logfiles,
                '    Settings.Constants.Users.Usernames.KIOSK_USER,
                '    Security.AccessControl.FileSystemRights.Write Or Security.AccessControl.FileSystemRights.Read,
                '    Security.AccessControl.AccessControlType.Allow,
                '    False)


                ''Add KIOSK_SLAVE with READ/WRITE
                'Helpers.FilesAndFolders.Permissions.FolderSecurity_ADD(
                '    Settings.Constants.Paths.Folders.Kiosk_Logfiles,
                '    Settings.Constants.Users.Usernames.KIOSK_SLAVE,
                '    Security.AccessControl.FileSystemRights.Write Or Security.AccessControl.FileSystemRights.Read,
                '    Security.AccessControl.AccessControlType.Allow,
                '    False)

                Return True
            Catch ex As Exception


                Return False
            End Try

        End Function
    End Class

    'Public Class Profile
    '    Public Shared ReadOnly Property ActiveIndex() As Integer
    '        Get
    '            Return Helpers.Registry64.GetValue_Integer(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles, OVCC_Constants.Registry.VALUENAME__Profiles_Active, -1)
    '        End Get
    '    End Property

    '    Public Shared ReadOnly Property InactiveIndex() As Integer
    '        Get
    '            Dim iTmp As Integer = -1

    '            iTmp = Helpers.Registry64.GetValue_Integer(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles, OVCC_Constants.Registry.VALUENAME__Profiles_Active, -1)

    '            If iTmp = 1 Then
    '                Return 2
    '            ElseIf iTmp = 2 Then
    '                Return 1
    '            Else
    '                Return -1
    '            End If
    '        End Get
    '    End Property

    '    Public Shared ReadOnly Property ActivePath As String
    '        Get
    '            Return Path(ActiveIndex)
    '        End Get
    '    End Property

    '    Public Shared ReadOnly Property MasterPath As String
    '        Get
    '            Return Helpers.Registry64.GetValue_String(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles_Paths, OVCC_Constants.Registry.VALUENAME__Profiles_Paths_Master)
    '        End Get
    '    End Property

    '    Public Shared Function Path(index As Integer) As String
    '        If index = 1 Then
    '            Return Helpers.Registry64.GetValue_String(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles_Paths, OVCC_Constants.Registry.VALUENAME__Profiles_Paths_Copy01)
    '        ElseIf index = 2 Then
    '            Return Helpers.Registry64.GetValue_String(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles_Paths, OVCC_Constants.Registry.VALUENAME__Profiles_Paths_Copy02)
    '        Else
    '            Return ""
    '        End If
    '    End Function

    '    Public Shared Function Swap() As Boolean
    '        If ActiveIndex = 1 Then
    '            Helpers.Registry64.SetValue_Integer(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles, OVCC_Constants.Registry.VALUENAME__Profiles_Active, 2)
    '        ElseIf ActiveIndex = 2 Then
    '            Helpers.Registry64.SetValue_Integer(OVCC_Constants.Registry.KEY__OVCCKiosk_Profiles, OVCC_Constants.Registry.VALUENAME__Profiles_Active, 1)
    '        Else
    '            Return False
    '        End If

    '        Return True
    '    End Function

    '    Public Shared Function CleanThis() As Boolean
    '        Return _Clean(ActiveIndex)
    '    End Function

    '    Public Shared Function CleanTheOther() As Boolean
    '        Return _Clean(InactiveIndex)
    '    End Function

    '    Private Shared Function _Clean(index As Integer) As Boolean
    '        Dim pathMaster As String, pathFilthy As String

    '        Try
    '            pathMaster = MasterPath
    '            pathFilthy = Path(index)

    '            If Not IO.Directory.Exists(pathMaster) Then
    '                Throw New Exception("'" & pathMaster & "' does not exist!")
    '            End If

    '            If Not IO.Directory.Exists(pathFilthy) Then
    '                Throw New Exception("'" & pathFilthy & "' does not exist!")
    '            End If

    '            If Integrity.Debugging Then Helpers.Logger.WriteDebugRelative("profile id " & index.ToString, 1)

    '            Dim oCopy As New Helpers.FilesAndFolders.CopyFiles With {
    '                .BackupDirectory = "", 'disables backup 
    '                .SourceDirectory = pathMaster,
    '                .DestinationDirectory = pathFilthy,
    '                .SuppressCopyLogging = True
    '            }

    '            'Delete everything
    '            If Integrity.Debugging Then Helpers.Logger.WriteDebugRelative("clean folder '" & pathFilthy & "'", 2)
    '            Helpers.FilesAndFolders.Folder.CleanFolder(pathFilthy, "I_am_sure")

    '            'copy master files
    '            If Integrity.Debugging Then Helpers.Logger.WriteDebugRelative("copy master files", 2)
    '            oCopy.CopyFiles()

    '            'set permissions to KIOSK_USER, just to be sure
    '            If Integrity.Debugging Then Helpers.Logger.WriteDebugRelative("setting permissions", 2)
    '            Helpers.FilesAndFolders.Permissions.FolderSecurity_REPLACE__Full(pathFilthy, OVCC_Constants.Users.KIOSK_USER)

    '            Return True
    '        Catch ex As Exception
    '            If Integrity.Debugging Then
    '                Helpers.Logger.WriteErrorRelative("error", 1)
    '                Helpers.Logger.WriteErrorRelative(ex.Message, 2)
    '            End If

    '            Return False
    '        End Try
    '    End Function
    'End Class

    Public Class AutoStart
        Public Shared Function Enable() As Boolean
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_AutoAdminLogon,
                Settings.Constants.AutoLogon.ENABLED_AutoAdminLogon)
            Helpers.Registry64.SetValue_String(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_DefaultDomain,
                Settings.Constants.AutoLogon.ENABLED_DefaultDomain)
            Helpers.Registry64.SetValue_String(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_DefaultPassword,
                Settings.Constants.AutoLogon.ENABLED_DefaultPassword)
            Helpers.Registry64.SetValue_String(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_DefaultUsername,
                Settings.Constants.AutoLogon.ENABLED_DefaultUsername)
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_DisableCAD,
                Settings.Constants.AutoLogon.ENABLED_DisableCAD)
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_ForceAutoLogon,
                Settings.Constants.AutoLogon.ENABLED_ForceAutoLogon)
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_ForceUnlockLogon,
                Settings.Constants.AutoLogon.ENABLED_ForceUnlockLogon)
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_AutoLogonCount,
                Settings.Constants.AutoLogon.ENABLED_AutoLogonCount)

            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__UserSwitch,
                Settings.Constants.Registry.Windows.VALUENAME__UserSwitch_Enabled,
                Settings.Constants.AutoLogon.ENABLED_UserSwitch)

            Return True
        End Function

        Public Shared Function Disable() As Boolean
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_AutoAdminLogon,
                Settings.Constants.AutoLogon.DISABLED_AutoAdminLogon)
            Helpers.Registry64.SetValue_String(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_DefaultDomain,
                Settings.Constants.AutoLogon.DISABLED_DefaultDomain)
            Helpers.Registry64.SetValue_String(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_DefaultPassword,
                Settings.Constants.AutoLogon.DISABLED_DefaultPassword)
            Helpers.Registry64.SetValue_String(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_DefaultUsername,
                Settings.Constants.AutoLogon.DISABLED_DefaultUsername)
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_DisableCAD,
                Settings.Constants.AutoLogon.DISABLED_DisableCAD)
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_ForceAutoLogon,
                Settings.Constants.AutoLogon.DISABLED_ForceAutoLogon)
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_ForceUnlockLogon,
                Settings.Constants.AutoLogon.DISABLED_ForceUnlockLogon)
            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__AutoLogon,
                Settings.Constants.Registry.Windows.VALUENAME__AutoLogon_AutoLogonCount,
                Settings.Constants.AutoLogon.DISABLED_AutoLogonCount)

            Helpers.Registry64.SetValue_Integer(
                Settings.Constants.Registry.Windows.KEY__UserSwitch,
                Settings.Constants.Registry.Windows.VALUENAME__UserSwitch_Enabled,
                Settings.Constants.AutoLogon.DISABLED_UserSwitch)

            Return True
        End Function

    End Class
End Class
