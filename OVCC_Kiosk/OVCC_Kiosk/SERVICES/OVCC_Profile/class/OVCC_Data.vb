﻿Public Class OVCC_Data
    Public Class Registry
        Public Shared KEY__ProfileImagePath As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\%%PROFILE_SID_STRING%%"
        Public Shared VALUENAME__ProfileImagePath As String = "ProfileImagePath"


        Public Shared KEY__AutoLogon As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
        Public Shared VALUENAME__AutoLogon_AutoAdminLogon As String = "AutoAdminLogon"
        Public Shared VALUENAME__AutoLogon_DefaultDomain As String = "DefaultDomain"
        Public Shared VALUENAME__AutoLogon_DefaultPassword As String = "DefaultPassword"
        Public Shared VALUENAME__AutoLogon_DefaultUsername As String = "DefaultUsername"
        Public Shared VALUENAME__AutoLogon_DisableCAD As String = "DisableCAD"
        Public Shared VALUENAME__AutoLogon_ForceAutoLogon As String = "ForceAutoLogon"
        Public Shared VALUENAME__AutoLogon_ForceUnlockLogon As String = "ForceUnlockLogon "
        Public Shared VALUENAME__AutoLogon_AutoLogonCount As String = "AutoLogonCount"


        Public Shared KEY__UserSwitch As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\UserSwitch​"
        Public Shared VALUENAME__UserSwitch_Enabled As String = "Enabled"


        Public Shared KEY__GuestTek_Root As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek"

        Public Shared KEY__OVCCKiosk As String = KEY__GuestTek_Root & "\OVCCKiosk"

        Public Shared KEY__OVCCKiosk_Settings As String = KEY__OVCCKiosk & "\Settings"
        Public Shared VALUENAME__Settings_OriginalUserSwitch As String = "OriginalUserSwitch"

        Public Shared KEY__OVCCKiosk_Messages As String = KEY__OVCCKiosk & "\Messages"

        Public Shared KEY__OVCCKiosk_Profiles As String = KEY__OVCCKiosk & "\Profiles"
        Public Shared VALUENAME__Profiles_Active As String = "Active"

        Public Shared KEY__OVCCKiosk_Profiles_Paths As String = KEY__OVCCKiosk_Profiles & "\Paths"
        Public Shared VALUENAME__Profiles_Paths_Master As String = "Master"
        Public Shared VALUENAME__Profiles_Paths_Copy01 As String = "Copy01"
        Public Shared VALUENAME__Profiles_Paths_Copy02 As String = "Copy02"
    End Class

    Public Class AutoLogon
        Public Shared ENABLED_AutoAdminLogon As Integer = 1
        Public Shared ENABLED_DefaultDomain As String = ""
        Public Shared ENABLED_DefaultPassword As String = Users.Usernames.KIOSK_USER
        Public Shared ENABLED_DefaultUsername As String = Users.Passwords.KIOSK_USER
        Public Shared ENABLED_DisableCAD As Integer = 1
        Public Shared ENABLED_ForceAutoLogon As Integer = 1
        Public Shared ENABLED_ForceUnlockLogon As Integer = 1
        Public Shared ENABLED_AutoLogonCount As Integer = 0

        Public Shared ENABLED_UserSwitch As Integer = 0


        Public Shared DISABLED_AutoAdminLogon As Integer = 0
        Public Shared DISABLED_DefaultDomain As String = ""
        Public Shared DISABLED_DefaultPassword As String = ""
        Public Shared DISABLED_DefaultUsername As String = ""
        Public Shared DISABLED_DisableCAD As Integer = 1
        Public Shared DISABLED_ForceAutoLogon As Integer = 0
        Public Shared DISABLED_ForceUnlockLogon As Integer = 0
        Public Shared DISABLED_AutoLogonCount As Integer = 0

        Public Shared DISABLED_UserSwitch As Integer = 0
    End Class

    Public Class UI
        Public Shared INFO__INITIAL_MARGIN As Integer = 40
        Public Shared INFO__POPUP_FADE_LENGTH As Integer = 1000 ' ms
        Public Shared INFO__POPUP_DURATION As Integer = 5000 'ms
        Public Shared INFO__POPUP_ERROR_DURATION As Integer = 15000 'ms
    End Class

    Public Class Users
        Public Class Usernames
            Public Shared KIOSK_USER As String = "ovcc_user"
            Public Shared KIOSK_ADMIN As String = "ovcc_admin"
            Public Shared KIOSK_SLAVE As String = "ovcc_slave"
        End Class

        Public Class Passwords
            'obfuscated with Helpers.XOrObfuscation_v2
            Public Shared KIOSK_USER As String = "575f475f06512c79246a77506f4c1c140e52505b45505f555d04004715530f0f5a05075311555a01737d236377056a1d1e1b0d020a5411045e555a5604111c5d080d5956525a44055502782c2363705167194a150a510c5015565c005f0507124609505f5055525a475351572b7e"
            Public Shared KIOSK_ADMIN As String = "575c150354057d76763673076f191e1450535e5743035a070b555141475b0d5a5857030b4a530754737771367f016a1e4a47585f59004204585058010546415b58095e55560f4a015a01287c246074563b4b481450030c0347040e045d055540145e5a5d5a045052475453522f2b"
            Public Shared KIOSK_SLAVE As String = ""
        End Class
    End Class

    Public Class Paths
        Public Class Folders
            Public Shared ReadOnly _ROOT As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\OVCC\Kiosk"
            Public Shared ReadOnly KioskLogfiles As String = _ROOT & "\Logfiles"
        End Class

        Public Class Files
            Public Shared ReadOnly UI As String = Folders._ROOT & "\OVCC_UI.exe"
            Public Shared ReadOnly Info As String = Folders._ROOT & "\OVCC_Info.exe"
            Public Shared ReadOnly SessionTrigger As String = Folders._ROOT & "\OVCC_SessionTrigger.exe"
            Public Shared ReadOnly UserAppStarter As String = Folders._ROOT & "\OVCC_UserAppStarter.exe"
        End Class
    End Class
End Class
