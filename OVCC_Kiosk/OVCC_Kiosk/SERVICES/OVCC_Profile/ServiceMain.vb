﻿Imports System.Management
Imports System.Text

Public Class ServiceMain
    Private mThread_Integrity As Threading.Thread
    Private mThread_Process As Threading.Thread

    Private mOwnLogger As NamedPipe.Logger.LogWriter

    Private mLoop As Boolean = True


#Region "service start/stop"
    Protected Overrides Sub OnStart(ByVal args() As String)
        mOwnLogger = New NamedPipe.Logger.LogWriter(My.Application.Info.ProductName)

        mOwnLogger.Debugging = Settings.Get.String(Settings.Constants.Registry.OVCC.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.VALUENAME__Settings_Debug)


        mOwnLogger.WriteMessage(New String("*", 50))
        mOwnLogger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        mOwnLogger.WriteMessage("started")



        '=================================================================================================================
        KioskProfile.Initialise(Settings.Constants.Users.Usernames.KIOSK_USER)
        KioskProfile.ActiveProfile = 0
        KioskProfile.Path_CleanProfile = Helpers.Registry.GetValue_String(
                                    Settings.Constants.Registry.OVCC.KEY__OVCCKiosk_Profiles_Paths,
                                    Settings.Constants.Registry.OVCC.VALUENAME__Profiles_Paths_Template)
        KioskProfile.AddProfilePath(Helpers.Registry.GetValue_String(
                                    Settings.Constants.Registry.OVCC.KEY__OVCCKiosk_Profiles_Paths,
                                    Settings.Constants.Registry.OVCC.VALUENAME__Profiles_Paths_Copy01))
        KioskProfile.AddProfilePath(Helpers.Registry.GetValue_String(
                                    Settings.Constants.Registry.OVCC.KEY__OVCCKiosk_Profiles_Paths,
                                    Settings.Constants.Registry.OVCC.VALUENAME__Profiles_Paths_Copy02))


        '=================================================================================================================
        StartThreads()
    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()
    End Sub
#End Region


#Region "threads"
    Private Sub StartThreads()
        mLoop = True


        mThread_Integrity = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Integrity))
        mThread_Integrity.Start()

        mThread_Process = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Process))
        mThread_Process.Start()
    End Sub

    Private Sub KillThreads()
        mLoop = False


        Try
            mThread_Integrity.Abort()
        Catch ex As Exception

        End Try

        Try
            mThread_Process.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Thread_Integrity()

    End Sub

    Private Sub Thread_Process()


    End Sub
#End Region

End Class
