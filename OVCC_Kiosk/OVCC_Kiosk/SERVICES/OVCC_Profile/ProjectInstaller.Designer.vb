﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OVCC_SessionWatchdog_ProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.OVCC_SessionWatchdog_Installer = New System.ServiceProcess.ServiceInstaller()
        '
        'OVCC_SessionWatchdog_ProcessInstaller
        '
        Me.OVCC_SessionWatchdog_ProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService
        Me.OVCC_SessionWatchdog_ProcessInstaller.Password = Nothing
        Me.OVCC_SessionWatchdog_ProcessInstaller.Username = Nothing
        '
        'OVCC_SessionWatchdog_Installer
        '
        Me.OVCC_SessionWatchdog_Installer.ServiceName = "OVCC_SessionWatchdog"
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.OVCC_SessionWatchdog_ProcessInstaller, Me.OVCC_SessionWatchdog_Installer})

    End Sub

    Friend WithEvents OVCC_SessionWatchdog_ProcessInstaller As ServiceProcess.ServiceProcessInstaller
    Friend WithEvents OVCC_SessionWatchdog_Installer As ServiceProcess.ServiceInstaller
End Class
