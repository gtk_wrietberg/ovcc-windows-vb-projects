﻿Imports System.Data.SQLite
Imports System.Drawing

Public Class Sqlite
    Implements IDisposable
    Private mDatabaseFile As String
    Private mDatabaseConnectionString As String
    Private disposedValue As Boolean

    Public Shared Function ToMemory() As Boolean

    End Function

    Public Property DatabaseFile() As String
        Get
            Return mDatabaseFile
        End Get
        Set(ByVal value As String)
            mDatabaseFile = value
        End Set
    End Property

    Public Sub New(DatabaseFile As String)
        mDatabaseFile = DatabaseFile

        mDatabaseConnectionString = "Data Source=" & mDatabaseFile & ";Version=3;"
    End Sub

    Private Function _BuildSqlCommand(sQuery As String, dParams As Dictionary(Of String, Object)) As SQLiteCommand
        Dim _sqlCommand As New SQLiteCommand(sQuery)

        If dParams.Count > 0 Then
            For Each kvp As KeyValuePair(Of String, Object) In dParams
                _sqlCommand.Parameters.AddWithValue(kvp.Key, kvp.Value)
            Next
        End If

        Return _sqlCommand
    End Function

    Public Function [SELECT](sQuery As String, dParams As Dictionary(Of String, Object)) As Dictionary(Of String, Object)
        Dim results As New Dictionary(Of String, Object)

        Using _db_conn As New SQLiteConnection(mDatabaseConnectionString)
            _db_conn.Open()

            Dim _db_command As SQLiteCommand = _BuildSqlCommand(sQuery, dParams)
            _db_command.Connection = _db_conn

            Using _db_reader As SQLiteDataReader = _db_command.ExecuteReader()
                If _db_reader.HasRows Then
                    While _db_reader.Read
                        For i As Integer = 0 To _db_reader.FieldCount - 1
                            results.Add(_db_reader.GetName(i), _db_reader.GetValue(i))
                        Next
                    End While
                End If
            End Using
        End Using

        Return results
    End Function

    Public Function [SELECT_SCALAR](sQuery As String) As Object
        Dim result As Object

        Using _db_conn As New SQLiteConnection(mDatabaseConnectionString)
            _db_conn.Open()

            Dim _db_command As SQLiteCommand = _db_conn.CreateCommand()
            _db_command.CommandText = sQuery

            result = _db_command.ExecuteScalar()
        End Using

        Return result
    End Function

    Public Function [INSERT](sQuery As String, dParams As Dictionary(Of String, Object)) As Integer
        Dim lVoid As Long

        Return INSERT(sQuery, dParams, lVoid)
    End Function

    Public Function [INSERT](sQuery As String, dParams As Dictionary(Of String, Object), ByRef lastInsertedId As Long) As Integer
        Dim insertedRows As Integer = -1

        Using _db_conn As New SQLiteConnection(mDatabaseConnectionString)
            _db_conn.Open()

            Dim _db_command As SQLiteCommand = _BuildSqlCommand(sQuery, dParams)
            _db_command.Connection = _db_conn

            insertedRows = _db_command.ExecuteNonQuery()

            _db_command.CommandText = "SELECT last_insert_rowid()"
            lastInsertedId = CType(_db_command.ExecuteScalar(), Long)
        End Using

        Return insertedRows
    End Function

    Public Function [UPDATE](sQuery As String, dParams As Dictionary(Of String, Object)) As Integer
        Dim affectedRows As Integer = -1

        Using _db_conn As New SQLiteConnection(mDatabaseConnectionString)
            _db_conn.Open()

            Dim _db_command As SQLiteCommand = _BuildSqlCommand(sQuery, dParams)
            _db_command.Connection = _db_conn

            affectedRows = _db_command.ExecuteNonQuery()
        End Using

        Return affectedRows
    End Function

    Public Function [DELETE](sQuery As String, dParams As Dictionary(Of String, Object)) As Integer
        Dim affectedRows As Integer = -1

        Using _db_conn As New SQLiteConnection(mDatabaseConnectionString)
            _db_conn.Open()

            Dim _db_command As SQLiteCommand = _BuildSqlCommand(sQuery, dParams)
            _db_command.Connection = _db_conn

            affectedRows = _db_command.ExecuteNonQuery()
        End Using

        Return affectedRows
    End Function

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not disposedValue Then
            If disposing Then
                ' TODO: dispose managed state (managed objects)
            End If

            ' TODO: free unmanaged resources (unmanaged objects) and override finalizer
            ' TODO: set large fields to null
            disposedValue = True
        End If
    End Sub

    ' ' TODO: override finalizer only if 'Dispose(disposing As Boolean)' has code to free unmanaged resources
    ' Protected Overrides Sub Finalize()
    '     ' Do not change this code. Put cleanup code in 'Dispose(disposing As Boolean)' method
    '     Dispose(disposing:=False)
    '     MyBase.Finalize()
    ' End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code. Put cleanup code in 'Dispose(disposing As Boolean)' method
        Dispose(disposing:=True)
        GC.SuppressFinalize(Me)
    End Sub
End Class
