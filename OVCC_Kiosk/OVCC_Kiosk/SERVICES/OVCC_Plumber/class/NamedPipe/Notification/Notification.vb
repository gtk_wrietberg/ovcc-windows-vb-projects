﻿Namespace NamedPipe.Notification

    Public Class Notification
        Private mIndex As Integer
        Public Property Index() As Integer
            Get
                Return mIndex
            End Get
            Set(ByVal value As Integer)
                mIndex = value
            End Set
        End Property

        Private mType As NotificationType
        Public Property [Type]() As NotificationType
            Get
                Return mType
            End Get
            Set(ByVal value As NotificationType)
                mType = value
            End Set
        End Property

        Private mText As String
        Public Property [Text]() As String
            Get
                Return mText
            End Get
            Set(ByVal value As String)
                mText = value
            End Set
        End Property

        Public Sub New()
        End Sub

        Public Sub New(iIndex As Integer, iType As Integer, sText As String)
            mIndex = iIndex
            mType = iType
            mText = sText
        End Sub
    End Class
End Namespace
