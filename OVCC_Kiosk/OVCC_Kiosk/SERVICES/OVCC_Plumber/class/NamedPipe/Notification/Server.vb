﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports System.Text
Imports Newtonsoft.Json.Linq

Namespace NamedPipe.Notification
    Public Class Server
        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__notification"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"
        Private Shared ReadOnly SECRET_RESPONSE As String = Helpers.XOrObfuscation_v2.Deobfuscate(Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__CommunicationSecret, ""))

        Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__NOTIFICATION"

        Public Shared Sub [Instance]()
            Dim mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

            Dim PipeSecurity As New PipeSecurity
            PipeSecurity.AddAccessRule(New PipeAccessRule("Users", PipeAccessRights.ReadWrite, AccessControlType.Allow))
            PipeSecurity.AddAccessRule(New PipeAccessRule(WindowsIdentity.GetCurrent().Owner, PipeAccessRights.FullControl, AccessControlType.Allow))

            Dim pipeServer As New NamedPipeServerStream(PIPENAME, PipeDirection.InOut, 100, PipeTransmissionMode.Message, PipeOptions.None, 4096, 4096, PipeSecurity)

            Try
                pipeServer.WaitForConnection()

                Dim ss As New _StreamString(pipeServer)
                ss.WriteString(SECRET_HANDSHAKE)

                Dim receivedString As String = ss.ReadString()
                mOwnLogger.WriteDebug("received: " & receivedString)

                If Not receivedString.Equals("<DIE>") Then
                    Try
                        Dim jsonReceived As New JObject
                        jsonReceived = JObject.Parse(receivedString)

                        If jsonReceived.GetValue("secret") Is Nothing Then
                            Throw New Exception("system - empty secret")
                        End If

                        If jsonReceived.GetValue("action") Is Nothing Then
                            Throw New Exception("notification - empty action")
                        End If

                        If jsonReceived.GetValue("index") Is Nothing Then
                            Throw New Exception("notification - empty index")
                        End If

                        If jsonReceived.GetValue("type") Is Nothing Then
                            Throw New Exception("notification - empty type")
                        End If

                        If jsonReceived.GetValue("text") Is Nothing Then
                            Throw New Exception("notification - empty text")
                        End If

                        Dim _notification_secret As String
                        Dim _notification_action As String
                        Dim _notification_type As Integer
                        Dim _notification_index As Integer
                        Dim _notification_type_string As String
                        Dim _notification_index_string As String
                        Dim _notification_text As String

                        _notification_secret = jsonReceived.GetValue("secret").ToString
                        _notification_action = jsonReceived.GetValue("action").ToString
                        _notification_index_string = jsonReceived.GetValue("index").ToString
                        _notification_type_string = jsonReceived.GetValue("type").ToString
                        _notification_text = jsonReceived.GetValue("text").ToString

                        mOwnLogger.WriteDebug("_notification_secret: " & _notification_secret)
                        mOwnLogger.WriteDebug("_notification_action: " & _notification_action)
                        mOwnLogger.WriteDebug("_notification_type: " & _notification_type_string)
                        mOwnLogger.WriteDebug("_notification_index: " & _notification_index_string)
                        mOwnLogger.WriteDebug("_notification_text: " & _notification_text)

                        If Not _notification_secret.Equals(SECRET_RESPONSE) Then
                            Throw New Exception("system - no access")
                        End If


                        Select Case _notification_action
                            Case "add"
                                If Integer.TryParse(_notification_type_string, _notification_type) Then
                                    NotificationHandler.AddNotification(_notification_type, _notification_text)
                                Else
                                    Throw New Exception("notification - invalid type")
                                End If
                            Case "delete"
                                If Integer.TryParse(_notification_index_string, _notification_index) Then
                                    NotificationHandler.RemoveNotification(_notification_index)
                                Else
                                    Throw New Exception("notification - invalid type")
                                End If
                            Case Else
                                Throw New Exception("notification - invalid action")
                        End Select


                        ss.WriteString("<ACK>")
                    Catch ex As Exception
                        ss.WriteString("<NACK>")

                        mOwnLogger.WriteDebug("error: " & ex.Message)
                    End Try
                End If

                pipeServer.Close()
            Catch e As Exception
                mOwnLogger.WriteError("Error: " & e.Message)
            End Try

            If pipeServer IsNot Nothing Then
                pipeServer.Dispose()
            End If
        End Sub

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)
                Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                ioStream.Read(inBuffer, 0, len)

                Return streamEncoding.GetString(CType(inBuffer, Byte()))
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace
