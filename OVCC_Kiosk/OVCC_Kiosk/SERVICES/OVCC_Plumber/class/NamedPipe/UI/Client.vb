﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports System.IO.Pipes
Imports System.Text

Namespace NamedPipe.UI
    Public Class Client
        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__ui"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"
        Private Shared ReadOnly SECRET_RESPONSE As String = Helpers.XOrObfuscation_v2.Deobfuscate(Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__CommunicationSecret, ""))

        Public Enum CLIENT_RESPONSE
            OK = 0
            TIMEOUT = 1
            INVALID_SERVER = 2
            [ERROR] = 10
        End Enum


        Public Shared Function HelpdeskDialog() As CLIENT_RESPONSE
            Return _write("helpdesk_dialog")
        End Function

        Public Shared Function HelpdeskClosed() As CLIENT_RESPONSE
            Return _write("helpdesk_closed")
        End Function

        Public Shared Function HelpdeskTimeout() As CLIENT_RESPONSE
            Return _write("helpdesk_timeout")
        End Function


        Public Shared Function AirlinesDialog() As CLIENT_RESPONSE
            Return _write("airlines_dialog")
        End Function

        Public Shared Function AirlinesTimeout() As CLIENT_RESPONSE
            Return _write("airlines_timeout")
        End Function

        Public Shared Function AirlineSelected(url As String) As CLIENT_RESPONSE
            Return _write("airlines_selected", url)
        End Function

        Public Shared Function AirlinesClosed() As CLIENT_RESPONSE
            Return _write("airlines_closed")
        End Function


        Public Shared Function OfficeDialog() As CLIENT_RESPONSE
            Return _write("office_dialog")
        End Function

        Public Shared Function OfficeTimeout() As CLIENT_RESPONSE
            Return _write("office_timeout")
        End Function

        Public Shared Function OfficeSelected(path As String) As CLIENT_RESPONSE
            Return _write("office_selected", path)
        End Function

        Public Shared Function OfficeClosed() As CLIENT_RESPONSE
            Return _write("office_closed")
        End Function


        Public Shared Function CustomDialog() As CLIENT_RESPONSE
            Return _write("custom_dialog")
        End Function

        Public Shared Function CustomClosed() As CLIENT_RESPONSE
            Return _write("custom_closed")
        End Function

        Public Shared Function CustomTimeout() As CLIENT_RESPONSE
            Return _write("custom_timeout")
        End Function


        Public Shared Function DebuggingOn() As CLIENT_RESPONSE
            Return _write("debug_on")
        End Function

        Public Shared Function DebuggingOff() As CLIENT_RESPONSE
            Return _write("debug_off")
        End Function


        Public Shared Function KillThread() As CLIENT_RESPONSE
            Return _kill()
        End Function


        Private Shared Function _write(sAction As String, Optional sValue As String = "") As CLIENT_RESPONSE
            Dim response As CLIENT_RESPONSE
            Dim pipeClient As NamedPipeClientStream = Nothing
            Try
                pipeClient = New NamedPipeClientStream(".", PIPENAME, PipeDirection.InOut, PipeOptions.None)
                pipeClient.Connect(1000)

                Dim ss As New _StreamString(pipeClient)
                If ss.ReadString().Equals(SECRET_HANDSHAKE) Then
                    Dim jsonMessage As New JObject

                    jsonMessage.Add("secret", SECRET_RESPONSE)
                    jsonMessage.Add("action", sAction)
                    jsonMessage.Add("value", sValue)

                    Dim sJsonMessage As String = JsonConvert.SerializeObject(jsonMessage, Formatting.None)

                    ss.WriteString(sJsonMessage)

                    Dim sAck As String = ss.ReadString()

                    response = CLIENT_RESPONSE.OK
                Else
                    response = CLIENT_RESPONSE.INVALID_SERVER
                End If

                pipeClient.Close()
            Catch ex_timeout As TimeoutException
                response = CLIENT_RESPONSE.TIMEOUT
            Catch ex As Exception
                response = CLIENT_RESPONSE.ERROR
            End Try

            If pipeClient IsNot Nothing Then
                pipeClient.Dispose()
            End If

            Return response
        End Function

        Private Shared Function _kill() As CLIENT_RESPONSE
            Dim response As CLIENT_RESPONSE
            Dim pipeClient As NamedPipeClientStream = Nothing
            Try
                pipeClient = New NamedPipeClientStream(".", PIPENAME, PipeDirection.InOut, PipeOptions.None)
                pipeClient.Connect(1000)

                Dim ss As New _StreamString(pipeClient)
                If ss.ReadString().Equals(SECRET_HANDSHAKE) Then
                    ss.WriteString("<DIE>")
                    response = CLIENT_RESPONSE.OK
                Else
                    response = CLIENT_RESPONSE.INVALID_SERVER
                End If

                pipeClient.Close()
            Catch ex_timeout As TimeoutException
                response = CLIENT_RESPONSE.TIMEOUT
            Catch ex As Exception
                response = CLIENT_RESPONSE.ERROR
            End Try

            If pipeClient IsNot Nothing Then
                pipeClient.Dispose()
            End If

            Return response
        End Function

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)
                Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                ioStream.Read(inBuffer, 0, len)

                Return streamEncoding.GetString(CType(inBuffer, Byte()))
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace