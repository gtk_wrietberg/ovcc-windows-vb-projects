﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Namespace NamedPipe.UI
    Public Class Server
        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__ui"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"
        Private Shared ReadOnly SECRET_RESPONSE As String = Helpers.XOrObfuscation_v2.Deobfuscate(Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__CommunicationSecret, ""))

        Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__UI"

        Public Shared Sub [Instance]()
            Dim mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

            Dim PipeSecurity As New PipeSecurity
            PipeSecurity.AddAccessRule(New PipeAccessRule("Users", PipeAccessRights.ReadWrite, AccessControlType.Allow))
            PipeSecurity.AddAccessRule(New PipeAccessRule(WindowsIdentity.GetCurrent().Owner, PipeAccessRights.FullControl, AccessControlType.Allow))

            Dim pipeServer As New NamedPipeServerStream(PIPENAME, PipeDirection.InOut, 100, PipeTransmissionMode.Message, PipeOptions.None, 4096, 4096, PipeSecurity)

            Try
                pipeServer.WaitForConnection()

                Dim ss As New _StreamString(pipeServer)
                ss.WriteString(SECRET_HANDSHAKE)

                Dim receivedString As String = ss.ReadString()
                mOwnLogger.WriteDebug("received: " & receivedString)

                If Not receivedString.Equals("<DIE>") Then
                    Try
                        Dim jsonReceived As New JObject
                        jsonReceived = JObject.Parse(receivedString)

                        If jsonReceived.GetValue("secret") Is Nothing Then
                            Throw New Exception("ui - empty secret")
                        End If

                        If jsonReceived.GetValue("action") Is Nothing Then
                            Throw New Exception("ui - empty action")
                        End If

                        If jsonReceived.GetValue("value") Is Nothing Then
                            Throw New Exception("ui - empty value")
                        End If

                        Dim _ui_secret As String
                        Dim _ui_action As String
                        Dim _ui_value As String

                        _ui_secret = jsonReceived.GetValue("secret").ToString
                        _ui_action = jsonReceived.GetValue("action").ToString
                        _ui_value = jsonReceived.GetValue("value").ToString

                        mOwnLogger.WriteDebug("_ui_secret: " & _ui_secret)
                        mOwnLogger.WriteDebug("_ui_action: " & _ui_action)
                        mOwnLogger.WriteDebug("_ui_value: " & _ui_value)

                        If Not _ui_secret.Equals(SECRET_RESPONSE) Then
                            Throw New Exception("ui - no access")
                        End If


                        Select Case _ui_action
                            Case "helpdesk_dialog"
                                mOwnLogger.WriteDebug("helpdesk_dialog")
                                UIHandler.HelpdeskDialog()
                            Case "helpdesk_closed"
                                mOwnLogger.WriteDebug("helpdesk_close")
                                UIHandler.HelpdeskClosed()
                            Case "helpdesk_timeout"
                                mOwnLogger.WriteDebug("helpdesk_timeout")
                                UIHandler.HelpdeskTimeout()

                            Case "airlines_dialog"
                                mOwnLogger.WriteDebug("airlines_dialog")
                                UIHandler.AirlinesDialog()
                            Case "airlines_selected"
                                mOwnLogger.WriteDebug("airlines_selected")
                                UIHandler.AirlineSelected(_ui_value)
                            Case "airlines_closed"
                                mOwnLogger.WriteDebug("airlines_closed")
                                UIHandler.AirlinesClosed()
                            Case "airlines_timeout"
                                mOwnLogger.WriteDebug("airlines_timeout")
                                UIHandler.AirlinesTimeout()

                            Case "office_dialog"
                                mOwnLogger.WriteDebug("office_dialog")
                                UIHandler.OfficeDialog()
                            Case "office_selected"
                                mOwnLogger.WriteDebug("office_selected")
                                UIHandler.OfficeSelected(_ui_value)
                            Case "office_closed"
                                mOwnLogger.WriteDebug("office_closed")
                                UIHandler.OfficeClosed()
                            Case "office_timeout"
                                mOwnLogger.WriteDebug("office_timeout")
                                UIHandler.OfficeTimeout()

                            Case "custom_dialog"
                                mOwnLogger.WriteDebug("custom_dialog")
                                UIHandler.CustomDialog()
                            Case "custom_closed"
                                mOwnLogger.WriteDebug("custom_closed")
                                UIHandler.CustomClosed()
                            Case "custom_timeout"
                                mOwnLogger.WriteDebug("custom_timeout")
                                UIHandler.CustomTimeout()

                            Case "debug_on"
                                mOwnLogger.WriteDebug("debug_on")
                                UIHandler.DebuggingOn()
                            Case "debug_off"
                                mOwnLogger.WriteDebug("debug_off")
                                UIHandler.DebuggingOff()

                            Case Else
                                mOwnLogger.WriteWarning("Invalid request: action=" & _ui_action & " ; value=" & _ui_value)
                                SessionHandler.InvalidRequest(_ui_action, _ui_value)
                        End Select


                        ss.WriteString("<ACK>")
                    Catch ex As Exception
                        ss.WriteString("<NACK>")

                        mOwnLogger.WriteDebug("error: " & ex.Message)
                    End Try
                End If

                pipeServer.Close()
            Catch e As Exception
                mOwnLogger.WriteError("Error: " & e.Message)
            End Try

            If pipeServer IsNot Nothing Then
                pipeServer.Dispose()
            End If
        End Sub

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)
                Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                ioStream.Read(inBuffer, 0, len)

                Return streamEncoding.GetString(CType(inBuffer, Byte()))
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace
