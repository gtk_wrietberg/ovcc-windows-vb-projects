﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports System.Text
Imports Newtonsoft.Json.Linq

Namespace NamedPipe.System
    Public Class Server
        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__system"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"
        Private Shared ReadOnly SECRET_RESPONSE As String = Helpers.XOrObfuscation_v2.Deobfuscate(Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__CommunicationSecret, ""))

        Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__SYSTEM"

        Public Shared Sub [Instance]()
            Dim mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

            Dim PipeSecurity As New PipeSecurity
            PipeSecurity.AddAccessRule(New PipeAccessRule("Users", PipeAccessRights.ReadWrite, AccessControlType.Allow))
            PipeSecurity.AddAccessRule(New PipeAccessRule(WindowsIdentity.GetCurrent().Owner, PipeAccessRights.FullControl, AccessControlType.Allow))

            Dim pipeServer As New NamedPipeServerStream(PIPENAME, PipeDirection.InOut, 100, PipeTransmissionMode.Message, PipeOptions.None, 4096, 4096, PipeSecurity)

            Try
                pipeServer.WaitForConnection()

                Dim ss As New _StreamString(pipeServer)
                ss.WriteString(SECRET_HANDSHAKE)

                Dim receivedString As String = ss.ReadString()
                mOwnLogger.WriteDebug("received: " & receivedString)

                If Not receivedString.Equals("<DIE>") Then
                    Try
                        Dim jsonReceived As New JObject
                        jsonReceived = JObject.Parse(receivedString)

                        If jsonReceived.GetValue("secret") Is Nothing Then
                            Throw New Exception("system - empty secret")
                        End If

                        If jsonReceived.GetValue("action") Is Nothing Then
                            Throw New Exception("system - empty action")
                        End If

                        If jsonReceived.GetValue("value") Is Nothing Then
                            Throw New Exception("system - empty value")
                        End If

                        Dim _system_secret As String
                        Dim _system_action As String
                        Dim _system_value As String

                        _system_secret = jsonReceived.GetValue("secret").ToString
                        _system_action = jsonReceived.GetValue("action").ToString
                        _system_value = jsonReceived.GetValue("value").ToString

                        mOwnLogger.WriteDebug("_system_secret: " & _system_secret)
                        mOwnLogger.WriteDebug("_system_action: " & _system_action)
                        mOwnLogger.WriteDebug("_system_value: " & _system_value)

                        If Not _system_secret.Equals(SECRET_RESPONSE) Then
                            Throw New Exception("system - no access")
                        End If


                        Select Case _system_action
                            Case "started_appstarted"
                                mOwnLogger.WriteDebug("started_appstarted")
                                SystemHandler.StartedAppStarter()
                            Case "closed_appstarted"
                                mOwnLogger.WriteDebug("closed_appstarted")
                                SystemHandler.ClosedAppStarter()

                            Case "started_homepage"
                                mOwnLogger.WriteDebug("started_homepage")
                                SystemHandler.StartedHomepage()
                            Case "closed_homepage_blocked"
                                mOwnLogger.WriteDebug("closed_homepage_blocked")
                                SystemHandler.ClosedHomepageBlocked()
                            Case "closed_homepage"
                                mOwnLogger.WriteDebug("closed_homepage")
                                SystemHandler.ClosedHomepage()

                            Case "started_screensaver"
                                mOwnLogger.WriteDebug("started_screensaver")
                                SystemHandler.StartedScreensaver()
                            Case "closed_screensaver"
                                mOwnLogger.WriteDebug("closed_screensaver")
                                SystemHandler.ClosedScreensaver()

                            Case "started_logout"
                                mOwnLogger.WriteDebug("started_logout")
                                SystemHandler.StartedLogout()
                            Case "closed_logout"
                                mOwnLogger.WriteDebug("closed_logout")
                                SystemHandler.ClosedLogout()

                            Case "started_notification"
                                mOwnLogger.WriteDebug("started_notification")
                                SystemHandler.StartedNotification()
                            Case "closed_notification"
                                mOwnLogger.WriteDebug("closed_notification")
                                SystemHandler.ClosedNotification()

                            Case Else
                                SessionHandler.InvalidRequest(_system_action, _system_value)
                        End Select


                        ss.WriteString("<ACK>")
                    Catch ex As Exception
                        ss.WriteString("<NACK>")

                        mOwnLogger.WriteDebug("error: " & ex.Message)
                    End Try
                End If

                pipeServer.Close()
            Catch e As Exception
                mOwnLogger.WriteError("Error: " & e.Message)
            End Try

            If pipeServer IsNot Nothing Then
                pipeServer.Dispose()
            End If
        End Sub

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)
                Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                ioStream.Read(inBuffer, 0, len)

                Return streamEncoding.GetString(CType(inBuffer, Byte()))
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace
