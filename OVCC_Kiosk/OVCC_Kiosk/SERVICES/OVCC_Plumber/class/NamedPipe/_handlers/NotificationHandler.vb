﻿Public Class NotificationHandler
    Public Shared Sub AddNotification(_notification_type As NamedPipe.Notification.NotificationType, _notification_text As String)
        Settings.Set.Long(
                    Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                    Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__LastUpdate,
                    Helpers.TimeDate.ToUnix)


        Dim mList As List(Of String) = Helpers.Registry64.FindValues(
                                            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                                            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationType_SEARCHSTRING)


        Dim iMax As Integer = 0

        For Each _notification_value As String In mList
            Dim sIndex As String = _notification_value.Replace(Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationType_SEARCHSTRING, "")

            Dim iIndex As Integer
            If Integer.TryParse(sIndex, iIndex) Then
                If iIndex > iMax Then
                    iMax = iIndex
                End If
            End If
        Next


        iMax += 1


        Settings.Set.Integer(
                        Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                        Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationType_SEARCHSTRING & iMax.ToString,
                        _notification_type)

        Settings.Set.String(
                        Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                        Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationText_SEARCHSTRING & iMax.ToString,
                        _notification_text)
    End Sub

    Public Shared Sub RemoveNotification(iIndex As Integer)
        Helpers.Registry64.RemoveValue(
                Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationType_SEARCHSTRING & iIndex.ToString)

        Helpers.Registry64.RemoveValue(
                Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Notifications,
                Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Notifications__NotificationText_SEARCHSTRING & iIndex.ToString)
    End Sub
End Class
