﻿Imports System.Data.SQLite

Public Class LanguageHandler
    Private Shared _sql_conn As SQLiteConnection = Nothing

    Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__LanguageHandler"
    Private Shared mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

    Private Shared Function GetSessionId() As String
        Return Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__SessionId, "")
    End Function


    Private Shared Function _connection_string() As String
        Return "Data Source=" & Settings.Constants.Paths.Files.Data.DATABASE_SESSIONS & ";Version=3;"
    End Function

    Private Shared Function _ConnectDatabase() As Boolean
        Dim bPleaseConnect As Boolean = False
        Try
            If _sql_conn Is Nothing Then
                bPleaseConnect = True
            Else
                If Not _sql_conn.State.Equals(ConnectionState.Open) Then
                    bPleaseConnect = True
                End If
            End If

            If bPleaseConnect Then
                _sql_conn = New SQLiteConnection(_connection_string)
                _sql_conn.Open()


                Return True
            End If
        Catch ex As Exception

        End Try


        Return False
    End Function

    Private Shared Function _DisconnectDatabase() As Boolean
        Try
            _sql_conn.Close()

            Return True
        Catch ex As Exception

        End Try

        Return False
    End Function

    Private Shared Function _insert_session_info(sState As String, sActivity As String) As Integer
        Return _insert_session_info("", sState, sActivity)
    End Function

    Private Shared Function _insert_session_info(sSessionId As String, sState As String, sActivity As String) As Integer
        _ConnectDatabase()

        If sSessionId.Equals("") Then sSessionId = GetSessionId()

        Dim insertedRows As Integer = -1
        Dim timestampQuery As Long = DateTime.Now.Ticks
        Dim sQuery As String = "INSERT INTO SessionInfo (session_id,state,activity,timestamp) VALUES (@session_id,@state,@activity,@timestamp)"
        Dim sQueryLogged As String = sQuery

        sQueryLogged = sQueryLogged.Replace("@session_id", "'" & sSessionId & "'")
        sQueryLogged = sQueryLogged.Replace("@state", "'" & sState & "'")
        sQueryLogged = sQueryLogged.Replace("@activity", "'" & sActivity & "'")
        sQueryLogged = sQueryLogged.Replace("@timestamp", timestampQuery.ToString)

        mOwnLogger.WriteDebug("running query: " & sQueryLogged)


        Try
            Dim _sqlCommand As New SQLiteCommand(sQuery, _sql_conn)
            _sqlCommand.Parameters.AddWithValue("session_id", sSessionId)
            _sqlCommand.Parameters.AddWithValue("state", sState)
            _sqlCommand.Parameters.AddWithValue("activity", sActivity)
            _sqlCommand.Parameters.AddWithValue("timestamp", timestampQuery)

            insertedRows = _sqlCommand.ExecuteNonQuery()
        Catch ex As Exception
            mOwnLogger.WriteError("Error while running query: " & sQuery)
        End Try


        Return insertedRows
    End Function


    Public Shared Function SetActiveLanguage(LanguageId As Integer) As Integer
        mOwnLogger.WriteDebug("SetActiveLanguage - " & LanguageId.ToString())

        Settings.Set.Integer(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Languages, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Languages__ActiveLanguageId, LanguageId)

        Return _insert_session_info("set_language", LanguageId.ToString())
    End Function

    Public Shared Function DefaultLanguage() As Integer
        Dim _default As Integer = Settings.Get.Integer(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__DefaultLanguageId, 0)
        mOwnLogger.WriteDebug("SetDefaultLanguage - " & _default.ToString())

        Settings.Set.Integer(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Languages, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Languages__ActiveLanguageId, _default)

        Return _insert_session_info("default_language", _default.ToString())
    End Function
End Class
