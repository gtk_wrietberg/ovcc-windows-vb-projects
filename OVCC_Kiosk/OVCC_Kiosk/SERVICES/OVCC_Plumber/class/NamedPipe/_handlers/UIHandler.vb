﻿Imports System.Data.SQLite

Public Class UIHandler
    Private Shared _sql_conn As SQLiteConnection = Nothing

    Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__UIHandler"
    Private Shared mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

    Private Shared Function GetSessionId() As String
        Return Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__SessionId, "")
    End Function


    Private Shared Function _connection_string() As String
        Return "Data Source=" & Settings.Constants.Paths.Files.Data.DATABASE_SESSIONS & ";Version=3;"
    End Function

    Private Shared Function _ConnectDatabase() As Boolean
        Dim bPleaseConnect As Boolean = False
        Try
            If _sql_conn Is Nothing Then
                bPleaseConnect = True
            Else
                If Not _sql_conn.State.Equals(ConnectionState.Open) Then
                    bPleaseConnect = True
                End If
            End If

            If bPleaseConnect Then
                _sql_conn = New SQLiteConnection(_connection_string)
                _sql_conn.Open()


                Return True
            End If
        Catch ex As Exception

        End Try


        Return False
    End Function

    Private Shared Function _DisconnectDatabase() As Boolean
        Try
            _sql_conn.Close()

            Return True
        Catch ex As Exception

        End Try

        Return False
    End Function

    Private Shared Function _insert_session_info(sState As String, sActivity As String) As Integer
        Return _insert_session_info("", sState, sActivity)
    End Function

    Private Shared Function _insert_session_info(sSessionId As String, sState As String, sActivity As String) As Integer
        _ConnectDatabase()

        If sSessionId.Equals("") Then sSessionId = GetSessionId()

        Dim insertedRows As Integer = -1
        Dim timestampQuery As Long = DateTime.Now.Ticks
        Dim sQuery As String = "INSERT INTO SessionInfo (session_id,state,activity,timestamp) VALUES (@session_id,@state,@activity,@timestamp)"
        Dim sQueryLogged As String = sQuery

        sQueryLogged = sQueryLogged.Replace("@session_id", "'" & sSessionId & "'")
        sQueryLogged = sQueryLogged.Replace("@state", "'" & sState & "'")
        sQueryLogged = sQueryLogged.Replace("@activity", "'" & sActivity & "'")
        sQueryLogged = sQueryLogged.Replace("@timestamp", timestampQuery.ToString)

        mOwnLogger.WriteDebug("running query: " & sQueryLogged)


        Try
            Dim _sqlCommand As New SQLiteCommand(sQuery, _sql_conn)
            _sqlCommand.Parameters.AddWithValue("session_id", sSessionId)
            _sqlCommand.Parameters.AddWithValue("state", sState)
            _sqlCommand.Parameters.AddWithValue("activity", sActivity)
            _sqlCommand.Parameters.AddWithValue("timestamp", timestampQuery)

            insertedRows = _sqlCommand.ExecuteNonQuery()
        Catch ex As Exception
            mOwnLogger.WriteError("Error while running query: " & sQuery)
        End Try


        Return insertedRows
    End Function


    '----------------------------
    Public Shared Function HelpdeskDialog() As Integer
        mOwnLogger.WriteDebug("HelpdeskDialog")

        Return _insert_session_info("ui", "helpdesk_dialog")
    End Function

    Public Shared Function HelpdeskClosed() As Integer
        mOwnLogger.WriteDebug("HelpdeskClosed")

        Return _insert_session_info("ui", "helpdesk_closed")
    End Function

    Public Shared Function HelpdeskTimeout() As Integer
        mOwnLogger.WriteDebug("HelpdeskTimeout")

        Return _insert_session_info("ui", "helpdesk_timeout")
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Function AirlinesDialog() As Integer
        mOwnLogger.WriteDebug("AirlinesDialog")

        Return _insert_session_info("ui", "airline_dialog")
    End Function

    Public Shared Function AirlineSelected(url As String) As Integer
        mOwnLogger.WriteDebug("AirlineSelected")

        Return _insert_session_info("ui", "airline selected: " & url)
    End Function

    Public Shared Function AirlinesClosed() As Integer
        mOwnLogger.WriteDebug("AirlinesClosed")

        Return _insert_session_info("ui", "airline_closed")
    End Function

    Public Shared Function AirlinesTimeout() As Integer
        mOwnLogger.WriteDebug("AirlinesTimeout")

        Return _insert_session_info("ui", "airline_timeout")
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Function OfficeDialog() As Integer
        mOwnLogger.WriteDebug("OfficeDialog")

        Return _insert_session_info("ui", "office_dialog")
    End Function

    Public Shared Function OfficeSelected(path As String) As Integer
        mOwnLogger.WriteDebug("OfficeSelected")

        Return _insert_session_info("ui", "office selected: " & path)
    End Function

    Public Shared Function OfficeClosed() As Integer
        mOwnLogger.WriteDebug("OfficeClosed")

        Return _insert_session_info("ui", "office_closed")
    End Function

    Public Shared Function OfficeTimeout() As Integer
        mOwnLogger.WriteDebug("OfficeTimeout")

        Return _insert_session_info("ui", "office_timeout")
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Function CustomDialog() As Integer
        mOwnLogger.WriteDebug("CustomDialog")

        Return _insert_session_info("ui", "custom_dialog")
    End Function

    Public Shared Function CustomClosed() As Integer
        mOwnLogger.WriteDebug("CustomClosed")

        Return _insert_session_info("ui", "custom_closed")
    End Function

    Public Shared Function CustomTimeout() As Integer
        mOwnLogger.WriteDebug("CustomTimeout")

        Return _insert_session_info("ui", "custom_timeout")
    End Function


    '----------------------------
    Public Shared Function DebuggingOn() As Integer
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__Debug, True)

        Return _insert_session_info("ui", "debug_on")
    End Function

    Public Shared Function DebuggingOff() As Integer
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__Debug, False)

        Return _insert_session_info("ui", "debug_off")
    End Function
End Class
