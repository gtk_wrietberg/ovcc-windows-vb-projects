﻿Imports System.Data.SqlClient
Imports System.Data.SQLite
Imports System.Drawing
Imports System.Web.UI.WebControls

Public Class SessionHandler
    Private Shared _sql_conn As SQLiteConnection = Nothing

    Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__SessionHandler"
    Private Shared mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

    Private Shared Function GetSessionId() As String
        Return Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__SessionId, "")
    End Function

    Private Shared Function GenerateSessionId() As String
        Return Guid.NewGuid().ToString
    End Function

    Private Shared Function _connection_string() As String
        Return "Data Source=" & Settings.Constants.Paths.Files.Data.DATABASE_SESSIONS & ";Version=3;"
    End Function

    Private Shared Function _ConnectDatabase() As Boolean
        Dim bPleaseConnect As Boolean = False
        Try
            If _sql_conn Is Nothing Then
                bPleaseConnect = True
            Else
                If Not _sql_conn.State.Equals(ConnectionState.Open) Then
                    bPleaseConnect = True
                End If
            End If

            If bPleaseConnect Then
                _sql_conn = New SQLiteConnection(_connection_string)

                _sql_conn.Open()


                Return True
            End If
        Catch ex As Exception

        End Try


        Return False
    End Function

    Private Shared Function _DisconnectDatabase() As Boolean
        Try
            _sql_conn.Close()

            Return True
        Catch ex As Exception

        End Try

        Return False
    End Function

    Private Shared Function _insert_session_info(sState As String, sActivity As String) As Integer
        Return _insert_session_info("", sState, sActivity)
    End Function

    Private Shared Function _insert_session_info(sSessionId As String, sState As String, sActivity As String) As Integer
        _ConnectDatabase()

        If sSessionId.Equals("") Then sSessionId = GetSessionId()

        Dim insertedRows As Integer = -1
        Dim timestampQuery As Long = DateTime.Now.Ticks
        Dim sQuery As String = "INSERT INTO SessionInfo (session_id,state,activity,timestamp) VALUES (@session_id,@state,@activity,@timestamp)"
        Dim sQueryLogged As String = sQuery

        sQueryLogged = sQueryLogged.Replace("@session_id", "'" & sSessionId & "'")
        sQueryLogged = sQueryLogged.Replace("@state", "'" & sState & "'")
        sQueryLogged = sQueryLogged.Replace("@activity", "'" & sActivity & "'")
        sQueryLogged = sQueryLogged.Replace("@timestamp", timestampQuery.ToString)

        mOwnLogger.WriteDebug("running query: " & sQueryLogged)


        Try
            Dim _sqlCommand As New SQLiteCommand(sQuery, _sql_conn)
            _sqlCommand.Parameters.AddWithValue("session_id", sSessionId)
            _sqlCommand.Parameters.AddWithValue("state", sState)
            _sqlCommand.Parameters.AddWithValue("activity", sActivity)
            _sqlCommand.Parameters.AddWithValue("timestamp", timestampQuery)

            insertedRows = _sqlCommand.ExecuteNonQuery()
        Catch ex As Exception
            mOwnLogger.WriteError("Error while running query: " & sQuery)
        End Try


        Return insertedRows
    End Function

    Private Shared Function _insert_session_start(sSessionId As String)
        Return _insert_session(sSessionId, "start")
    End Function

    Private Shared Function _insert_session_end(sSessionId As String)
        Return _insert_session(sSessionId, "end")
    End Function

    Private Shared Function _insert_session(sSessionId As String, sState As String) As Integer
        _ConnectDatabase()

        Dim insertedOrUpdatedRows As Integer = -1
        Dim sQuery As String
        Dim sQueryLogged As String
        Dim timestampQuery As Long = DateTime.Now.Ticks


        If sState.Equals("start") Then
            sQuery = "INSERT INTO Sessions (session_id,timestamp_start,timestamp_end) VALUES (@session_id,@timestamp_start,@timestamp_end)"

            sQueryLogged = sQuery.Replace("@session_id", "'" & sSessionId & "'")
            sQueryLogged = sQueryLogged.Replace("@timestamp_start", timestampQuery.ToString)
            sQueryLogged = sQueryLogged.Replace("@timestamp_end", "0")

            mOwnLogger.WriteDebug("running query: " & sQueryLogged)

            Try
                Dim _sqlCommand As New SQLiteCommand(sQuery, _sql_conn)
                _sqlCommand.Parameters.AddWithValue("session_id", sSessionId)
                _sqlCommand.Parameters.AddWithValue("timestamp_start", timestampQuery)
                _sqlCommand.Parameters.AddWithValue("timestamp_end", 0)

                insertedOrUpdatedRows = _sqlCommand.ExecuteNonQuery()
            Catch ex As Exception
                mOwnLogger.WriteError("Error while running query: " & sQuery)
            End Try
        ElseIf sState.Equals("end") Then
            sQuery = "UPDATE Sessions SET timestamp_end=@timestamp_end WHERE session_id=@session_id"

            sQueryLogged = sQuery.Replace("@session_id", "'" & sSessionId & "'")
            sQueryLogged = sQueryLogged.Replace("@timestamp_end", timestampQuery.ToString)

            mOwnLogger.WriteDebug("running query: " & sQueryLogged)

            Try
                Dim _sqlCommand As New SQLiteCommand(sQuery, _sql_conn)
                _sqlCommand.Parameters.AddWithValue("session_id", sSessionId)
                _sqlCommand.Parameters.AddWithValue("timestamp_end", timestampQuery)

                insertedOrUpdatedRows = _sqlCommand.ExecuteNonQuery()
            Catch ex As Exception
                mOwnLogger.WriteError("Error while running query: " & sQuery)
            End Try
        End If


        Return insertedOrUpdatedRows
    End Function


    Public Shared Sub ProfileLogon()
        mOwnLogger.WriteDebug("ProfileLogon")

        Settings.Set.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__ProfileAction, "logon")
    End Sub

    Public Shared Sub ProfileLogoff()
        mOwnLogger.WriteDebug("ProfileLogoff")

        Settings.Set.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__ProfileAction, "logoff")
    End Sub

    Public Shared Sub StoreIdleTime(sIdleTime As String)
        Dim lIdleTime As Long = -1

        If Not Long.TryParse(sIdleTime, lIdleTime) Then
            lIdleTime = -1
        End If


        Settings.Set.Long(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IdleTime, lIdleTime)
    End Sub

    Public Shared Function StartSession() As Integer
        mOwnLogger.WriteDebug("StartSession")

        Dim _new_session_id As String = GenerateSessionId()

        Settings.Set.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__SessionId, _new_session_id)
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsActive, True)
        Settings.Set.Long(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__StartTimestamp, DateTime.Now.Ticks)

        _insert_session_start(_new_session_id)

        Return _insert_session_info("session", "start")
    End Function

    Public Shared Function EndSession() As Integer
        mOwnLogger.WriteDebug("EndSession")

        Dim tmpSessionId As String = GetSessionId()

        _ResetSessionValues()

        _insert_session_end(tmpSessionId)

        Return _insert_session_info(tmpSessionId, "session", "end")
    End Function

    Public Shared Function EndSessionForced() As Integer
        mOwnLogger.WriteDebug("EndSessionForced")

        Dim tmpSessionId As String = GetSessionId()

        _ResetSessionValues()

        Return _insert_session_info(tmpSessionId, "session", "end_forced")
    End Function

    Private Shared Sub _ResetSessionValues()
        Settings.Set.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__SessionId, "")
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsActive, False)
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsAccepted, False)
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsRequested, False)
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsLogoutRequested, False)

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsIdleWarningActive, False)
    End Sub

    Public Shared Function LogoutRequested() As Integer
        mOwnLogger.WriteDebug("LogoutRequested")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsLogoutRequested, True)

        Return _insert_session_info("session", "logout_requested")
    End Function

    Public Shared Function LogoutRequestCancelled() As Integer
        mOwnLogger.WriteDebug("LogoutRequestCancelled")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsLogoutRequested, False)

        Return _insert_session_info("session", "logout_requested_cancelled")
    End Function

    Public Shared Function IdleWarning() As Integer
        mOwnLogger.WriteDebug("IdleWarning")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsIdleWarningActive, True)

        Dim _insert_session_return As Integer = _insert_session_info("session", "idle_warning")

        Return _insert_session_return
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Function TermsRequested() As Integer
        mOwnLogger.WriteDebug("TermsRequested")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsRequested, True)

        Return _insert_session_info("terms", "requested")
    End Function

    Public Shared Function TermsTimeout() As Integer
        mOwnLogger.WriteDebug("TermsTimeout")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsAccepted, False)

        Return _insert_session_info("terms", "timeout")
    End Function

    Public Shared Function TermsCancelled() As Integer
        mOwnLogger.WriteDebug("TermsCancelled")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsAccepted, False)

        Return _insert_session_info("terms", "cancelled")
    End Function

    Public Shared Function TermsAccepted() As Integer
        mOwnLogger.WriteDebug("TermsAccepted")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsRequested, False)
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsAccepted, True)

        Return _insert_session_info("terms", "accepted")
    End Function

    Public Shared Function TermsDeclined() As Integer
        mOwnLogger.WriteDebug("TermsDeclined")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsRequested, False)
        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsTermsAccepted, False)

        Return _insert_session_info("terms", "declined")
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Function GuestPasswordAccepted() As Integer
        mOwnLogger.WriteDebug("GuestPasswordAccepted")

        Return _insert_session_info("guest_password", "accepted")
    End Function

    Public Shared Function GuestPasswordCancelled() As Integer
        mOwnLogger.WriteDebug("GuestPasswordCancelled")

        Return _insert_session_info("guest_password", "cancelled")
    End Function

    Public Shared Function GuestPasswordDenied() As Integer
        mOwnLogger.WriteDebug("GuestPasswordDenied")

        Return _insert_session_info("guest_password", "denied")
    End Function

    Public Shared Function GuestPasswordTimeout() As Integer
        mOwnLogger.WriteDebug("GuestPasswordTimeout")

        Return _insert_session_info("guest_password", "timeout")
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Function ScreensaverTriggered() As Integer
        mOwnLogger.WriteDebug("ScreensaverTriggered")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsScreensaverActive, True)

        Return _insert_session_info("screensaver", "triggered")
    End Function

    Public Shared Function ScreensaverKilled() As Integer
        mOwnLogger.WriteDebug("ScreensaverKilled")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__IsScreensaverActive, False)

        Return _insert_session_info("screensaver", "killed")
    End Function

    Public Shared Function ScreensaverStarted() As Integer
        mOwnLogger.WriteDebug("ScreensaverStarted")

        Return _insert_session_info("screensaver", "started")
    End Function

    Public Shared Function ScreensaverFailed() As Integer
        mOwnLogger.WriteDebug("ScreensaverFailed")

        Return _insert_session_info("screensaver", "failed")
    End Function

    Public Shared Function ScreensaverRunning() As Integer
        mOwnLogger.WriteDebug("ScreensaverRunning")

        Return _insert_session_info("screensaver", "running")
    End Function

    Public Shared Function ScreensaverStopped() As Integer
        mOwnLogger.WriteDebug("ScreensaverStopped")

        Return _insert_session_info("screensaver", "stopped")
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Function StartHomepageKeepAlive() As Integer
        mOwnLogger.WriteDebug("StartHomepageKeepAlive")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__HomepageKeepAlive, True)

        Return _insert_session_info("homepage_keep_alive", "start")
    End Function

    Public Shared Function StopHomepageKeepAlive() As Integer
        mOwnLogger.WriteDebug("StopHomepageKeepAlive")

        Settings.Set.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Session, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Session__HomepageKeepAlive, False)

        Return _insert_session_info("homepage_keep_alive", "stop")
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Function Activity(sActivity As String) As Integer
        Return _insert_session_info("activity", sActivity)
    End Function


    '--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Shared Sub InvalidRequest(sState As String, sActivity As String)
        mOwnLogger.WriteWarning("Invalid request: state=" & sState & " - activity=" & sActivity)
    End Sub
End Class
