﻿Imports System.Data.SQLite

Public Class SystemHandler
    Private Shared _sql_conn As SQLiteConnection = Nothing

    Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__SystemHandler"
    Private Shared mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)


    Private Shared Function _connection_string() As String
        Return "Data Source=" & Settings.Constants.Paths.Files.Data.DATABASE_SESSIONS & ";Version=3;"
    End Function

    Private Shared Function _ConnectDatabase() As Boolean
        Dim bPleaseConnect As Boolean = False
        Try
            If _sql_conn Is Nothing Then
                bPleaseConnect = True
            Else
                If Not _sql_conn.State.Equals(ConnectionState.Open) Then
                    bPleaseConnect = True
                End If
            End If

            If bPleaseConnect Then
                _sql_conn = New SQLiteConnection(_connection_string)
                _sql_conn.Open()


                Return True
            End If
        Catch ex As Exception

        End Try

        Return False
    End Function

    Private Shared Function _DisconnectDatabase() As Boolean
        Try
            _sql_conn.Close()

            Return True
        Catch ex As Exception

        End Try

        Return False
    End Function

    Private Shared Function _insert_system_action(sService As String, sAction As String) As Integer
        _ConnectDatabase()

        Dim insertedRows As Integer = -1
        Dim timestampQuery As Long = DateTime.Now.Ticks
        Dim sQuery As String = "INSERT INTO System (timestamp,service,action) VALUES (@timestamp,@service,@action)"
        Dim sQueryLogged As String = sQuery

        sQueryLogged = sQueryLogged.Replace("@timestamp", timestampQuery.ToString)
        sQueryLogged = sQueryLogged.Replace("@service", "'" & sService & "'")
        sQueryLogged = sQueryLogged.Replace("@action", "'" & sAction & "'")

        mOwnLogger.WriteDebug("running query: " & sQueryLogged)


        Try
            Dim _sqlCommand As New SQLiteCommand(sQuery, _sql_conn)
            _sqlCommand.Parameters.AddWithValue("timestamp", timestampQuery)
            _sqlCommand.Parameters.AddWithValue("service", sService)
            _sqlCommand.Parameters.AddWithValue("action", sAction)

            insertedRows = _sqlCommand.ExecuteNonQuery()
        Catch ex As Exception
            mOwnLogger.WriteError("Error while running query: " & sQuery)
        End Try


        Return insertedRows
    End Function


    '----------------------------
    Public Shared Function PlumberStarted() As Integer
        mOwnLogger.WriteDebug("PlumberStarted")

        Return _insert_system_action("plumber", "started")
    End Function

    Public Shared Function PlumberStopping() As Integer
        mOwnLogger.WriteDebug("PlumberStopping")

        Return _insert_system_action("plumber", "stopping")
    End Function

    Public Shared Function PlumberStopped() As Integer
        mOwnLogger.WriteDebug("PlumberStopped")

        Return _insert_system_action("plumber", "stopped")
    End Function

    Public Shared Function PlumberReport(sReport As String) As Integer
        mOwnLogger.WriteDebug("PlumberReport")

        Return _insert_system_action("plumber", sReport)
    End Function


    '----------------------------
    Public Shared Function StartedAppStarter() As Integer
        mOwnLogger.WriteDebug("StartedAppStarter")

        Return _insert_system_action("appstarter", "started")
    End Function

    Public Shared Function ClosedAppStarter() As Integer
        mOwnLogger.WriteDebug("ClosedAppStarter")

        Return _insert_system_action("appstarter", "closed")
    End Function


    '----------------------------
    Public Shared Function StartedHomepage() As Integer
        mOwnLogger.WriteDebug("StartedHomepage")

        Return _insert_system_action("homepage", "started")
    End Function

    Public Shared Function ClosedHomepageBlocked() As Integer
        mOwnLogger.WriteDebug("ClosedHomepageBlocked")

        Return _insert_system_action("homepage", "close_blocked")
    End Function

    Public Shared Function ClosedHomepage() As Integer
        mOwnLogger.WriteDebug("ClosedHomepage")

        Return _insert_system_action("homepage", "closed")
    End Function


    '----------------------------
    Public Shared Function StartedScreensaver() As Integer
        mOwnLogger.WriteDebug("StartedScreensaver")

        Return _insert_system_action("screensaver", "started")
    End Function

    Public Shared Function ClosedScreensaver() As Integer
        mOwnLogger.WriteDebug("ClosedScreensaver")

        Return _insert_system_action("screensaver", "closed")
    End Function


    '----------------------------
    Public Shared Function StartedLogout() As Integer
        mOwnLogger.WriteDebug("StartedLogout")

        Return _insert_system_action("logout", "started")
    End Function

    Public Shared Function ClosedLogout() As Integer
        mOwnLogger.WriteDebug("ClosedLogout")

        Return _insert_system_action("logout", "closed")
    End Function


    '----------------------------
    Public Shared Function StartedNotification() As Integer
        mOwnLogger.WriteDebug("StartedNotification")

        Return _insert_system_action("notification", "started")
    End Function

    Public Shared Function ClosedNotification() As Integer
        mOwnLogger.WriteDebug("ClosedNotification")

        Return _insert_system_action("notification", "closed")
    End Function
End Class
