﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports System.IO.Pipes
Imports System.Text

Namespace NamedPipe.Session
    Public Class Client
        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__session"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"
        Private Shared ReadOnly SECRET_RESPONSE As String = Helpers.XOrObfuscation_v2.Deobfuscate(Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__CommunicationSecret, ""))

        Public Enum CLIENT_RESPONSE
            OK = 0
            TIMEOUT = 1
            INVALID_SERVER = 2
            [ERROR] = 10
        End Enum


        Public Shared Function CommTest() As Boolean
            Return (_write("comm_test") = CLIENT_RESPONSE.OK)
        End Function


        Public Shared Function ProfileLogon() As CLIENT_RESPONSE
            Return _write("profile_logon")
        End Function

        Public Shared Function ProfileLogoff() As CLIENT_RESPONSE
            Return _write("profile_logoff")
        End Function


        Public Shared Function IdleTime(lIdleTime As Long) As CLIENT_RESPONSE
            Return _write("idle_time", lIdleTime.ToString)
        End Function


        Public Shared Function SessionStart() As CLIENT_RESPONSE
            Return _write("session", "start")
        End Function

        Public Shared Function SessionLogoutRequested() As CLIENT_RESPONSE
            Return _write("session", "logout_requested")
        End Function

        Public Shared Function SessionLogoutRequestCancelled() As CLIENT_RESPONSE
            Return _write("session", "logout_request_cancelled")
        End Function

        Public Shared Function IdleWarning() As CLIENT_RESPONSE
            Return _write("session", "idle_warning")
        End Function

        Public Shared Function SessionEnd() As CLIENT_RESPONSE
            Return _write("session", "end")
        End Function

        Public Shared Function SessionEndForced() As CLIENT_RESPONSE
            Return _write("session", "end_forced")
        End Function


        Public Shared Function SessionTermsRequested() As CLIENT_RESPONSE
            Return _write("terms", "requested")
        End Function

        Public Shared Function SessionTermsCancelled() As CLIENT_RESPONSE
            Return _write("terms", "cancelled")
        End Function

        Public Shared Function SessionTermsTimeout() As CLIENT_RESPONSE
            Return _write("terms", "timeout")
        End Function

        Public Shared Function SessionTermsAccepted() As CLIENT_RESPONSE
            Return _write("terms", "accepted")
        End Function

        Public Shared Function SessionTermsDeclined() As CLIENT_RESPONSE
            Return _write("terms", "declined")
        End Function


        Public Shared Function GuestPasswordAccepted() As CLIENT_RESPONSE
            Return _write("guest_password", "accepted")
        End Function

        Public Shared Function GuestPasswordCancelled() As CLIENT_RESPONSE
            Return _write("guest_password", "cancelled")
        End Function

        Public Shared Function GuestPasswordTimedout() As CLIENT_RESPONSE
            Return _write("guest_password", "timeout")
        End Function

        Public Shared Function GuestPasswordDenied() As CLIENT_RESPONSE
            Return _write("guest_password", "denied")
        End Function


        Public Shared Function ScreensaverTriggered() As CLIENT_RESPONSE
            Return _write("screensaver", "triggered")
        End Function

        Public Shared Function ScreensaverKilled() As CLIENT_RESPONSE
            Return _write("screensaver", "killed")
        End Function

        Public Shared Function ScreensaverStarted() As CLIENT_RESPONSE
            Return _write("screensaver", "started")
        End Function

        Public Shared Function ScreensaverFailed() As CLIENT_RESPONSE
            Return _write("screensaver", "failed")
        End Function

        Public Shared Function ScreensaverRunning() As CLIENT_RESPONSE
            Return _write("screensaver", "running")
        End Function

        Public Shared Function ScreensaverStopped() As CLIENT_RESPONSE
            Return _write("screensaver", "stopped")
        End Function


        Public Shared Function SessionActivity(sActivity As String) As CLIENT_RESPONSE
            Return _write("activity", sActivity)
        End Function


        Public Shared Function StartKeepAlive() As CLIENT_RESPONSE
            Return _write("start_homepagekeepalive")
        End Function

        Public Shared Function StopKeepAlive() As CLIENT_RESPONSE
            Return _write("stop_homepagekeepalive")
        End Function


        Public Shared Function KillThread() As CLIENT_RESPONSE
            Return _kill()
        End Function


        Private Shared Function _write(sessionState As String, Optional sActivity As String = "") As CLIENT_RESPONSE
            Dim response As CLIENT_RESPONSE
            Dim pipeClient As NamedPipeClientStream = Nothing
            Try
                pipeClient = New NamedPipeClientStream(".", PIPENAME, PipeDirection.InOut, PipeOptions.None)
                pipeClient.Connect(1000)

                Dim ss As New _StreamString(pipeClient)
                If ss.ReadString().Equals(SECRET_HANDSHAKE) Then
                    Dim jsonMessage As New JObject

                    jsonMessage.Add("secret", SECRET_RESPONSE)
                    jsonMessage.Add("state", sessionState)
                    jsonMessage.Add("activity", sActivity)

                    Dim sJsonMessage As String = JsonConvert.SerializeObject(jsonMessage, Formatting.None)

                    ss.WriteString(sJsonMessage)

                    Dim sAck As String = ss.ReadString()

                    response = CLIENT_RESPONSE.OK
                Else
                    response = CLIENT_RESPONSE.INVALID_SERVER
                End If

                pipeClient.Close()
            Catch ex_timeout As TimeoutException
                response = CLIENT_RESPONSE.TIMEOUT
            Catch ex As Exception
                response = CLIENT_RESPONSE.ERROR
            End Try

            If pipeClient IsNot Nothing Then
                pipeClient.Dispose()
            End If

            Return response
        End Function

        Private Shared Function _kill() As CLIENT_RESPONSE
            Dim response As CLIENT_RESPONSE
            Dim pipeClient As NamedPipeClientStream = Nothing
            Try
                pipeClient = New NamedPipeClientStream(".", PIPENAME, PipeDirection.InOut, PipeOptions.None)
                pipeClient.Connect(1000)

                Dim ss As New _StreamString(pipeClient)
                If ss.ReadString().Equals(SECRET_HANDSHAKE) Then
                    ss.WriteString("<DIE>")
                    response = CLIENT_RESPONSE.OK
                Else
                    response = CLIENT_RESPONSE.INVALID_SERVER
                End If

                pipeClient.Close()
            Catch ex_timeout As TimeoutException
                response = CLIENT_RESPONSE.TIMEOUT
            Catch ex As Exception
                response = CLIENT_RESPONSE.ERROR
            End Try

            If pipeClient IsNot Nothing Then
                pipeClient.Dispose()
            End If

            Return response
        End Function

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)
                Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                ioStream.Read(inBuffer, 0, len)

                Return streamEncoding.GetString(CType(inBuffer, Byte()))
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace