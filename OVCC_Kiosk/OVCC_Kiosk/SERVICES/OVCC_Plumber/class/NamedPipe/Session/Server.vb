﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Namespace NamedPipe.Session
    Public Class Server
        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__session"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"
        Private Shared ReadOnly SECRET_RESPONSE As String = Helpers.XOrObfuscation_v2.Deobfuscate(Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__CommunicationSecret, ""))

        Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__SESSION"

        Public Shared Sub [Instance]()
            Dim mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

            Dim PipeSecurity As New PipeSecurity
            PipeSecurity.AddAccessRule(New PipeAccessRule("Users", PipeAccessRights.ReadWrite, AccessControlType.Allow))
            pipeSecurity.AddAccessRule(New PipeAccessRule(WindowsIdentity.GetCurrent().Owner, PipeAccessRights.FullControl, AccessControlType.Allow))

            Dim pipeServer As New NamedPipeServerStream(PIPENAME, PipeDirection.InOut, 100, PipeTransmissionMode.Message, PipeOptions.None, 4096, 4096, pipeSecurity)

            Try
                pipeServer.WaitForConnection()

                Dim ss As New _StreamString(pipeServer)
                ss.WriteString(SECRET_HANDSHAKE)

                Dim receivedString As String = ss.ReadString()
                mOwnLogger.WriteDebug("received: " & receivedString)

                If Not receivedString.Equals("<DIE>") Then
                    Try
                        Dim jsonReceived As New JObject
                        jsonReceived = JObject.Parse(receivedString)

                        If jsonReceived.GetValue("secret") Is Nothing Then
                            Throw New Exception("session - empty secret")
                        End If

                        If jsonReceived.GetValue("state") Is Nothing Then
                            Throw New Exception("session - empty state")
                        End If

                        If jsonReceived.GetValue("activity") Is Nothing Then
                            Throw New Exception("session - empty activity")
                        End If

                        Dim _session_secret As String
                        Dim _session_state As String
                        Dim _session_activity As String

                        _session_secret = jsonReceived.GetValue("secret").ToString
                        _session_state = jsonReceived.GetValue("state").ToString
                        _session_activity = jsonReceived.GetValue("activity").ToString

                        mOwnLogger.WriteDebug("_session_secret: " & _session_secret)
                        mOwnLogger.WriteDebug("_session_state: " & _session_state)
                        mOwnLogger.WriteDebug("_session_activity: " & _session_activity)

                        If Not _session_secret.Equals(SECRET_RESPONSE) Then
                            Throw New Exception("session - no access")
                        End If


                        Select Case _session_state
                            Case "idle_time"
                                SessionHandler.StoreIdleTime(_session_activity)

                            Case "profile_logon"
                                SessionHandler.ProfileLogon()
                            Case "profile_logoff"
                                SessionHandler.ProfileLogoff()

                            Case "session"
                                Select Case _session_activity
                                    Case "start"
                                        SessionHandler.StartSession()
                                    Case "logout_requested"
                                        SessionHandler.LogoutRequested()
                                    Case "logout_request_cancelled"
                                        SessionHandler.LogoutRequestCancelled()
                                    Case "end"
                                        SessionHandler.EndSession()
                                    Case "idle_warning"
                                        SessionHandler.IdleWarning()
                                    Case "end_forced"
                                        SessionHandler.EndSessionForced()
                                    Case Else
                                        SessionHandler.InvalidRequest(_session_state, _session_activity)
                                End Select

                            Case "terms"
                                Select Case _session_activity
                                    Case "timeout"
                                        SessionHandler.TermsTimeout()
                                    Case "requested"
                                        SessionHandler.TermsRequested()
                                    Case "accepted"
                                        SessionHandler.TermsAccepted()
                                    Case "declined"
                                        SessionHandler.TermsDeclined()
                                    Case "cancelled"
                                        SessionHandler.TermsCancelled()
                                    Case Else
                                        SessionHandler.InvalidRequest(_session_state, _session_activity)
                                End Select

                            Case "activity"
                                SessionHandler.Activity(_session_activity)

                            Case "guest_password"
                                Select Case _session_activity
                                    Case "accepted"
                                        SessionHandler.GuestPasswordAccepted()
                                    Case "cancelled"
                                        SessionHandler.GuestPasswordCancelled()
                                    Case "denied"
                                        SessionHandler.GuestPasswordDenied()
                                    Case "timeout"
                                        SessionHandler.GuestPasswordTimeout()
                                    Case Else
                                        SessionHandler.InvalidRequest(_session_state, _session_activity)
                                End Select

                            Case "screensaver"
                                Select Case _session_activity
                                    Case "triggered"
                                        SessionHandler.ScreensaverTriggered()
                                    Case "killed"
                                        SessionHandler.ScreensaverKilled()
                                    Case "started"
                                        SessionHandler.ScreensaverStarted()
                                    Case "failed"
                                        SessionHandler.ScreensaverFailed()
                                    Case "running"
                                        SessionHandler.ScreensaverRunning()
                                    Case "stopped"
                                        SessionHandler.ScreensaverStopped()
                                    Case Else
                                        SessionHandler.InvalidRequest(_session_state, _session_activity)
                                End Select

                            Case "start_homepagekeepalive"
                                SessionHandler.StartHomepageKeepAlive()
                            Case "stop_homepagekeepalive"
                                SessionHandler.StopHomepageKeepAlive()

                            Case Else
                                SessionHandler.InvalidRequest(_session_state, _session_activity)
                        End Select


                        ss.WriteString("<ACK>")
                    Catch ex As Exception
                        ss.WriteString("<NACK>")

                        mOwnLogger.WriteDebug("error: " & ex.Message)
                    End Try
                End If

                pipeServer.Close()
            Catch e As Exception
                mOwnLogger.WriteError("Error: " & e.Message)
            End Try

            If pipeServer IsNot Nothing Then
                pipeServer.Dispose()
            End If
        End Sub

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)
                Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                ioStream.Read(inBuffer, 0, len)

                Return streamEncoding.GetString(CType(inBuffer, Byte()))
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace
