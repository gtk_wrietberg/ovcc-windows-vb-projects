﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Namespace NamedPipe.Logger
    Public Class Server
        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__logger"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"

        Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__LOGGER"

        Public Shared Sub [Instance]()
            Dim mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

            Dim pipeSecurity As New PipeSecurity
            pipeSecurity.AddAccessRule(New PipeAccessRule("Users", PipeAccessRights.ReadWrite, AccessControlType.Allow))
            pipeSecurity.AddAccessRule(New PipeAccessRule(WindowsIdentity.GetCurrent().Owner, PipeAccessRights.FullControl, AccessControlType.Allow))

            Dim pipeServer As New NamedPipeServerStream(PIPENAME, PipeDirection.InOut, 100, PipeTransmissionMode.Message, PipeOptions.None, 4096, 4096, pipeSecurity)


            Try
                pipeServer.WaitForConnection()

                Dim ss As New _StreamString(pipeServer)
                ss.WriteString(SECRET_HANDSHAKE)

                Dim receivedString As String = ss.ReadString()
                mOwnLogger.WriteDebug("received: " & receivedString)

                If Not receivedString.Equals("<DIE>") Then
                    Try
                        Dim jsonReceived As New JObject
                        jsonReceived = JObject.Parse(receivedString)


                        If jsonReceived.GetValue("app") Is Nothing Then
                            Throw New Exception("log - empty app")
                        End If
                        If jsonReceived.GetValue("prefix") Is Nothing Then
                            Throw New Exception("log - empty prefix")
                        End If
                        If jsonReceived.GetValue("type") Is Nothing Then
                            Throw New Exception("log - empty type")
                        End If
                        If jsonReceived.GetValue("msg") Is Nothing Then
                            Throw New Exception("log - empty msg")
                        End If
                        If jsonReceived.GetValue("indent") Is Nothing Then
                            Throw New Exception("log - empty indent")
                        End If

                        Dim _log_AppName As String, _log_Prefix As String
                        Dim _log_MsgTypeTmp As String, _log_MsgType As Integer
                        Dim _log_Msg As String
                        Dim _log_IndentTmp As String, _log_Indent As Integer

                        _log_AppName = jsonReceived.GetValue("app").ToString
                        _log_Prefix = jsonReceived.GetValue("prefix").ToString
                        _log_MsgTypeTmp = jsonReceived.GetValue("type").ToString
                        _log_Msg = jsonReceived.GetValue("msg").ToString
                        _log_IndentTmp = jsonReceived.GetValue("indent").ToString

                        If Not Integer.TryParse(_log_MsgTypeTmp, _log_MsgType) Then
                            _log_MsgType = -1
                        End If
                        If Not Integer.TryParse(_log_IndentTmp, _log_Indent) Then
                            _log_Indent = 0
                        End If

                        Using _logger As New LogWriter(_log_AppName)
                            _logger.Write(_log_Prefix, _log_Msg, _log_MsgType, _log_Indent)
                        End Using

                        ss.WriteString("<ACK>")
                    Catch ex As Exception
                        ss.WriteString("<NACK>")

                        mOwnLogger.WriteDebug("error: " & ex.Message)
                    End Try
                End If

                pipeServer.Close()
            Catch e As Exception
                mOwnLogger.WriteError("Error: " & e.Message)
                mOwnLogger.WriteError("Source: " & e.Source)
                mOwnLogger.WriteError("Stacktrace: " & e.StackTrace)
            End Try

            If pipeServer IsNot Nothing Then
                pipeServer.Dispose()
            End If
        End Sub

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)

                If len > 0 Then
                    Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                    ioStream.Read(inBuffer, 0, len)

                    Return streamEncoding.GetString(CType(inBuffer, Byte()))
                Else
                    Return ""
                End If
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace
