﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.IO
Imports System.IO.Pipes
Imports System.Text

Namespace NamedPipe.Logger
    Public Class Client
        Private Enum LOGGER_MESSAGE_TYPE
            [DEFAULT] = 0
            [WARNING] = 1
            [ERROR] = 2
            [DEBUG] = 6
        End Enum

        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__logger"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"


        Public Shared Sub Message(sMessage As String)
            Message("", sMessage, 0)
        End Sub

        Public Shared Sub Message(sPrefix As String, sMessage As String)
            Message(sPrefix, sMessage, 0)
        End Sub

        Public Shared Sub Message(sMessage As String, iIndentCount As Integer)
            Message("", sMessage, iIndentCount)
        End Sub

        Public Shared Sub Message(sPrefix As String, sMessage As String, iIndentCount As Integer)
            _write_async(sPrefix, sMessage, LOGGER_MESSAGE_TYPE.DEFAULT, iIndentCount)
        End Sub


        Public Shared Sub Warning(sMessage As String)
            Warning("", sMessage, 0)
        End Sub

        Public Shared Sub Warning(sPrefix As String, sMessage As String)
            Warning(sPrefix, sMessage, 0)
        End Sub

        Public Shared Sub Warning(sMessage As String, iIndentCount As Integer)
            Warning("", sMessage, iIndentCount)
        End Sub

        Public Shared Sub Warning(sPrefix As String, sMessage As String, iIndentCount As Integer)
            _write_async(sPrefix, sMessage, LOGGER_MESSAGE_TYPE.WARNING, iIndentCount)
        End Sub


        Public Shared Sub [Error](sMessage As String)
            [Error]("", sMessage, 0)
        End Sub

        Public Shared Sub [Error](sPrefix As String, sMessage As String)
            [Error](sPrefix, sMessage, 0)
        End Sub

        Public Shared Sub [Error](sMessage As String, iIndentCount As Integer)
            [Error]("", sMessage, iIndentCount)
        End Sub

        Public Shared Sub [Error](sPrefix As String, sMessage As String, iIndentCount As Integer)
            _write_async(sPrefix, sMessage, LOGGER_MESSAGE_TYPE.ERROR, iIndentCount)
        End Sub


        Public Shared Sub Debug(sMessage As String)
            Debug("", sMessage, 0)
        End Sub

        Public Shared Sub Debug(sPrefix As String, sMessage As String)
            Debug(sPrefix, sMessage, 0)
        End Sub

        Public Shared Sub Debug(sMessage As String, iIndentCount As Integer)
            Debug("", sMessage, iIndentCount)
        End Sub

        Public Shared Sub Debug(sPrefix As String, sMessage As String, iIndentCount As Integer)
            Dim bDebugging As Boolean = Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__Debug, False)

            If bDebugging Then
                _write_async(sPrefix, sMessage, LOGGER_MESSAGE_TYPE.DEBUG, iIndentCount)
            End If
        End Sub


        Public Shared Sub KillThread()
            _kill()
        End Sub

        'Private Shared Sub _write(sPrefix As String, sMessage As String, iType As LOGGER_MESSAGE_TYPE)
        '    Dim pipeClient As NamedPipeClientStream = Nothing
        '    Try
        '        pipeClient = New NamedPipeClientStream(".", PIPENAME, PipeDirection.InOut, PipeOptions.None)
        '        pipeClient.Connect(1000)

        '        Dim ss As New _StreamString(pipeClient)
        '        If ss.ReadString().Equals(SECRET_HANDSHAKE) Then
        '            Dim jsonMessage As New JObject

        '            jsonMessage.Add("app", My.Application.Info.ProductName)
        '            jsonMessage.Add("prefix", sPrefix)
        '            jsonMessage.Add("type", iType)
        '            jsonMessage.Add("msg", sMessage)


        '            Dim sJsonMessage As String = JsonConvert.SerializeObject(jsonMessage, Formatting.None)

        '            ss.WriteString(sJsonMessage)

        '            Dim sAck As String = ss.ReadString()
        '        Else
        '            Console.WriteLine("Server could not be verified.")
        '        End If

        '        pipeClient.Close()
        '    Catch ex As Exception

        '    End Try

        '    If pipeClient IsNot Nothing Then
        '        pipeClient.Dispose()
        '    End If
        'End Sub

        Private Shared Async Sub _write_async(sPrefix As String, sMessage As String, iType As LOGGER_MESSAGE_TYPE, ByVal iIndentCount As Integer)
            Dim pipeClient As NamedPipeClientStream = Nothing
            Try
                pipeClient = New NamedPipeClientStream(".", PIPENAME, PipeDirection.InOut, PipeOptions.None)
                Await pipeClient.ConnectAsync(1000)

                Dim ss As New _StreamString(pipeClient)
                If ss.ReadString().Equals(SECRET_HANDSHAKE) Then
                    Dim jsonMessage As New JObject

                    jsonMessage.Add("app", My.Application.Info.ProductName)
                    jsonMessage.Add("prefix", sPrefix)
                    jsonMessage.Add("type", iType)
                    jsonMessage.Add("msg", sMessage)
                    jsonMessage.Add("indent", iIndentCount.ToString)


                    Dim sJsonMessage As String = JsonConvert.SerializeObject(jsonMessage, Formatting.None)

                    ss.WriteString(sJsonMessage)

                    Dim sAck As String = ss.ReadString()
                Else
                    Console.WriteLine("Server could not be verified.")
                End If

                pipeClient.Close()
            Catch ex As Exception

            End Try

            If pipeClient IsNot Nothing Then
                pipeClient.Dispose()
            End If
        End Sub


        Private Shared Sub _kill()
            Dim pipeClient As NamedPipeClientStream = Nothing
            Try
                pipeClient = New NamedPipeClientStream(".", PIPENAME, PipeDirection.InOut, PipeOptions.None)
                pipeClient.Connect(1000)

                Dim ss As New _StreamString(pipeClient)

                Dim receivedString As String = ss.ReadString()
                ss.WriteString("<DIE>")


                pipeClient.Close()
            Catch ex As Exception

            End Try

            If pipeClient IsNot Nothing Then
                pipeClient.Dispose()
            End If
        End Sub

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)
                Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                ioStream.Read(inBuffer, 0, len)

                Return streamEncoding.GetString(CType(inBuffer, Byte()))
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace