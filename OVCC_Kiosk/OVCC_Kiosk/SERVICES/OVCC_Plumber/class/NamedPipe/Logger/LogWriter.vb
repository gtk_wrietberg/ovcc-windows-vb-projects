﻿Namespace NamedPipe.Logger
    Public Class LogWriter
        Implements IDisposable

        Private mLogBasePath As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "GuestTek\OVCC\Kiosk\logfiles")
        Private mLogFilePath As String

        Private mAppName As String
        Private disposedValue As Boolean
        Private ReadOnly cIndentLength As Integer = 2

        Private mForcedDebugging As Boolean = False

        Public Property AppName() As String
            Get
                Return mAppName
            End Get
            Set(ByVal value As String)
                mAppName = value
            End Set
        End Property

        Public Sub New(AppName As String)
            Me.New(AppName, False)
        End Sub

        Public Sub New(AppName As String, ForcedDebugging As Boolean)
            mAppName = AppName
            mForcedDebugging = ForcedDebugging

            mLogFilePath = IO.Path.Combine(mLogBasePath, Now().ToString("yyyy-MM-dd"))

            If Not IO.Directory.Exists(mLogFilePath) Then
                IO.Directory.CreateDirectory(mLogFilePath)
            End If
        End Sub

        Public Enum MESSAGE_TYPE
            [DEFAULT] = 0
            [WARNING] = 1
            [ERROR] = 2
            [DEBUG] = 6
        End Enum


        '--------------------------------------------------------------------------------------------------------------------------------------------
        Public Function WriteMessage(ByVal sMessage As String) As String
            Return WriteMessage("", sMessage, 0)
        End Function

        Public Function WriteMessage(ByVal sMessage As String, ByVal iIndentCount As Integer) As String
            Return WriteMessage("", sMessage, iIndentCount)
        End Function

        Public Function WriteMessage(ByVal sPrefix As String, ByVal sMessage As String) As String
            Return WriteMessage(sPrefix, sMessage, 0)
        End Function

        Public Function WriteMessage(ByVal sPrefix As String, ByVal sMessage As String, ByVal iIndentCount As Integer) As String
            Return Write(sPrefix, sMessage, MESSAGE_TYPE.DEFAULT, iIndentCount)
        End Function


        '--------------------------------------------------------------------------------------------------------------------------------------------
        Public Function WriteWarning(ByVal sMessage As String) As String
            Return WriteWarning("", sMessage, 0)
        End Function

        Public Function WriteWarning(ByVal sMessage As String, ByVal iIndentCount As Integer) As String
            Return WriteWarning("", sMessage, iIndentCount)
        End Function

        Public Function WriteWarning(ByVal sPrefix As String, ByVal sMessage As String) As String
            Return WriteWarning(sPrefix, sMessage, 0)
        End Function

        Public Function WriteWarning(ByVal sPrefix As String, ByVal sMessage As String, ByVal iIndentCount As Integer) As String
            Return Write(sPrefix, sMessage, MESSAGE_TYPE.WARNING, iIndentCount)
        End Function


        '--------------------------------------------------------------------------------------------------------------------------------------------
        Public Function WriteError(ByVal sMessage As String) As String
            Return WriteError("", sMessage, 0)
        End Function

        Public Function WriteError(ByVal sMessage As String, ByVal iIndentCount As Integer) As String
            Return WriteError("", sMessage, iIndentCount)
        End Function

        Public Function WriteError(ByVal sPrefix As String, ByVal sMessage As String) As String
            Return WriteError(sPrefix, sMessage, 0)
        End Function

        Public Function WriteError(ByVal sPrefix As String, ByVal sMessage As String, ByVal iIndentCount As Integer) As String
            Return Write(sPrefix, sMessage, MESSAGE_TYPE.ERROR, iIndentCount)
        End Function


        '--------------------------------------------------------------------------------------------------------------------------------------------
        Public Function WriteDebug(ByVal sMessage As String) As String
            Return WriteDebug("", sMessage, 0)
        End Function

        Public Function WriteDebug(ByVal sMessage As String, ByVal iIndentCount As Integer) As String
            Return WriteDebug("", sMessage, iIndentCount)
        End Function

        Public Function WriteDebug(ByVal sPrefix As String, ByVal sMessage As String) As String
            Return WriteDebug(sPrefix, sMessage, 0)
        End Function

        Public Function WriteDebug(ByVal sPrefix As String, ByVal sMessage As String, ByVal iIndentCount As Integer) As String
            Dim bDebugging As Boolean = Settings.Get.Boolean(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings__Debug, False)

            If bDebugging Or mForcedDebugging Then
                Return Write(sPrefix, sMessage, MESSAGE_TYPE.DEBUG, iIndentCount)
            Else
                Return ""
            End If
        End Function


        '--------------------------------------------------------------------------------------------------------------------------------------------
        Public Function Write(ByVal sPrefix As String, ByVal sMessage As String, ByVal cMessageType As MESSAGE_TYPE, ByVal iIndentCount As Integer) As String
            Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String

            sMessage = Trim(sMessage)

            Select Case cMessageType
                Case MESSAGE_TYPE.WARNING
                    sMsgTypePrefix = "[*] "
                Case MESSAGE_TYPE.ERROR
                    sMsgTypePrefix = "[!] "
                Case MESSAGE_TYPE.DEBUG
                    sMsgTypePrefix = "[#] "
                Case MESSAGE_TYPE.DEFAULT
                    sMsgTypePrefix = "[.] "
                Case Else
                    sMsgTypePrefix = "[?] "
            End Select


            sMsgDatePrefix = Now.ToString & " - "

            sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

            If Not sPrefix.Equals("") Then
                sMsgPrefix = sMsgPrefix & "[" & sPrefix & "] - "
            End If

            If iIndentCount > 0 Then
                sMsgPrefix &= (New String(" ", cIndentLength * iIndentCount))
            End If


            UpdateLogfile(sMsgPrefix & sMessage)

            Return sMsgPrefix & sMessage
        End Function

        Private Sub UpdateLogfile(ByVal sString As String)
            Dim sFile As String = mLogFilePath & "\" & mAppName & ".txt"

            sFile = sFile.Replace("..", ".")


            For i As Integer = 0 To 10
                Try
                    Using sw As New IO.StreamWriter(sFile, True)
                        sw.WriteLine(sString)
                    End Using

                    Exit For
                Catch ex As Exception
                    'Oops, file locked?
                    Threading.Thread.Sleep(250)
                End Try
            Next
        End Sub

        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not disposedValue Then
                If disposing Then
                    ' TODO: dispose managed state (managed objects)
                End If

                ' TODO: free unmanaged resources (unmanaged objects) and override finalizer
                ' TODO: set large fields to null
                disposedValue = True
            End If
        End Sub

        ' ' TODO: override finalizer only if 'Dispose(disposing As Boolean)' has code to free unmanaged resources
        ' Protected Overrides Sub Finalize()
        '     ' Do not change this code. Put cleanup code in 'Dispose(disposing As Boolean)' method
        '     Dispose(disposing:=False)
        '     MyBase.Finalize()
        ' End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            ' Do not change this code. Put cleanup code in 'Dispose(disposing As Boolean)' method
            Dispose(disposing:=True)
            GC.SuppressFinalize(Me)
        End Sub
    End Class
End Namespace

