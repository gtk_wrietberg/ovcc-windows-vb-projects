﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class NamedPipe


    Public Class StreamString
        Private ioStream As Stream
        Private streamEncoding As UnicodeEncoding

        Public Sub New(ioStream As Stream)
            Me.ioStream = ioStream
            streamEncoding = New UnicodeEncoding(False, False)
        End Sub

        Public Function ReadString() As String
            Dim len As Integer = 0
            len = CType(ioStream.ReadByte(), Integer) * 256
            len += CType(ioStream.ReadByte(), Integer)
            Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
            ioStream.Read(inBuffer, 0, len)

            Return streamEncoding.GetString(CType(inBuffer, Byte()))
        End Function

        Public Function WriteString(outString As String) As Integer
            Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
            Dim len As Integer = outBuffer.Length
            If len > UInt16.MaxValue Then
                len = CType(UInt16.MaxValue, Integer)
            End If
            ioStream.WriteByte(CType(len \ 256, Byte))
            ioStream.WriteByte(CType(len And 255, Byte))
            ioStream.Write(outBuffer, 0, outBuffer.Length)
            ioStream.Flush()

            Return outBuffer.Length + 2
        End Function
    End Class


    Public Class Logging
        Public Shared Sub Server()
            Dim pipeServer_Logger As New NamedPipeServerStream(Constants.PIPENAME_LOGGER, PipeDirection.InOut, 100)

            Try
                pipeServer_Logger.WaitForConnection()

                Dim ss As New StreamString(pipeServer_Logger)

                ss.WriteString(Constants.SECRET_HANDSHAKE)

                Dim receivedString As String = ss.ReadString()

                Try
                    Dim jsonReceived As New JObject
                    jsonReceived = JObject.Parse(receivedString)



                    If jsonReceived.GetValue("app") Is Nothing Then
                        Throw New Exception("log - invalid app")
                    End If
                    If jsonReceived.GetValue("type") Is Nothing Then
                        Throw New Exception("log - invalid type")
                    End If
                    If jsonReceived.GetValue("msg") Is Nothing Then
                        Throw New Exception("log - invalid msg")
                    End If

                    Dim _log_AppName As String = jsonReceived.GetValue("app")
                    Dim _log_MsgType As Integer
                    Dim _log_Msg As String = jsonReceived.GetValue("msg")


                    If Not Integer.TryParse(jsonReceived.GetValue("type").ToString, _log_MsgType) Then
                        _log_MsgType = -1
                    End If

                    Dim _logger As New Logger(_log_AppName)
                    _logger.Write(_log_Msg, _log_MsgType)
                Catch ex As JsonReaderException

                End Try


                ss.WriteString("<ACK>")
            Catch e As Exception

            End Try

            pipeServer_Logger.Close()
            pipeServer_Logger.Dispose()
        End Sub

        Public Class Client
            Public Shared Sub Message(sMessage As String)

            End Sub


            Private Shared Sub _write(sMessage As String, iType As Logger.MESSAGE_TYPE)
                Dim pipeClient As NamedPipeClientStream
                Try
                    pipeClient = New NamedPipeClientStream(".", Constants.PIPENAME_LOGGER, PipeDirection.InOut, PipeOptions.None)

                    pipeClient.Connect()

                    Dim ss As New StreamString(pipeClient)
                    If ss.ReadString().Equals(Constants.SECRET_HANDSHAKE) Then
                        Dim jsonMessage As New JObject

                        jsonMessage.Add("app", My.Application.Info.ProductName)
                        jsonMessage.Add("type", iType.ToString)
                        jsonMessage.Add("msg", sMessage)


                        Dim sJsonMessage As String = JsonConvert.SerializeObject(jsonMessage, Formatting.None)

                        ss.WriteString(sJsonMessage)

                        Dim sAck As String = ss.ReadString()
                    Else
                        Console.WriteLine("Server could not be verified.")
                    End If

                    pipeClient.Close()
                Catch ex As Exception

                End Try

                If pipeClient IsNot Nothing Then
                    pipeClient.Dispose()
                End If
            End Sub
        End Class


    End Class
End Class
