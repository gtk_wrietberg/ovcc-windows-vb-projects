﻿Imports System.IO
Imports System.IO.Pipes
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports System.Text
Imports Newtonsoft.Json.Linq

Namespace NamedPipe.Language
    Public Class Server
        Private Shared ReadOnly PIPENAME As String = "ovcc_pipe__language"
        Private Shared ReadOnly SECRET_HANDSHAKE As String = "SUPER_SECRET_HANDSHAKE"
        Private Shared ReadOnly SECRET_RESPONSE As String = Helpers.XOrObfuscation_v2.Deobfuscate(Settings.Get.String(Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security, Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__CommunicationSecret, ""))

        Private Shared OwnLogAppName As String = "OVCC_Plumber__DEBUGGER__LANGUAGE"

        Public Shared Sub [Instance]()
            Dim mOwnLogger As New NamedPipe.Logger.LogWriter(OwnLogAppName)

            Dim PipeSecurity As New PipeSecurity
            PipeSecurity.AddAccessRule(New PipeAccessRule("Users", PipeAccessRights.ReadWrite, AccessControlType.Allow))
            PipeSecurity.AddAccessRule(New PipeAccessRule(WindowsIdentity.GetCurrent().Owner, PipeAccessRights.FullControl, AccessControlType.Allow))

            Dim pipeServer As New NamedPipeServerStream(PIPENAME, PipeDirection.InOut, 100, PipeTransmissionMode.Message, PipeOptions.None, 4096, 4096, PipeSecurity)

            Try
                pipeServer.WaitForConnection()

                Dim ss As New _StreamString(pipeServer)
                ss.WriteString(SECRET_HANDSHAKE)

                Dim receivedString As String = ss.ReadString()
                mOwnLogger.WriteDebug("received: " & receivedString)

                If Not receivedString.Equals("<DIE>") Then
                    Try
                        Dim jsonReceived As New JObject
                        jsonReceived = JObject.Parse(receivedString)

                        If jsonReceived.GetValue("secret") Is Nothing Then
                            Throw New Exception("language - empty secret")
                        End If

                        If jsonReceived.GetValue("action") Is Nothing Then
                            Throw New Exception("language - empty action")
                        End If

                        If jsonReceived.GetValue("value") Is Nothing Then
                            Throw New Exception("language - empty value")
                        End If

                        Dim _language_secret As String
                        Dim _language_action As String
                        Dim _language_value As String

                        _language_secret = jsonReceived.GetValue("secret").ToString
                        _language_action = jsonReceived.GetValue("action").ToString
                        _language_value = jsonReceived.GetValue("value").ToString

                        mOwnLogger.WriteDebug("_language_secret: " & _language_secret)
                        mOwnLogger.WriteDebug("_language_action: " & _language_action)
                        mOwnLogger.WriteDebug("_language_value: " & _language_value)

                        If Not _language_secret.Equals(SECRET_RESPONSE) Then
                            Throw New Exception("language - no access")
                        End If


                        Select Case _language_action
                            Case "set_language"
                                mOwnLogger.WriteDebug("set_language")

                                Dim _tmp_language As Integer = -1
                                If Not Integer.TryParse(_language_value, _tmp_language) Then
                                    Throw New Exception("language - invalid value")
                                End If

                                If _tmp_language >= 0 Then
                                    LanguageHandler.SetActiveLanguage(_tmp_language)
                                End If
                            Case "default_language"
                                mOwnLogger.WriteDebug("default_language")

                                LanguageHandler.DefaultLanguage()
                        End Select


                        ss.WriteString("<ACK>")
                    Catch ex As Exception
                        ss.WriteString("<NACK>")

                        mOwnLogger.WriteDebug("error: " & ex.Message)
                    End Try
                End If

                pipeServer.Close()
            Catch e As Exception
                mOwnLogger.WriteError("Error: " & e.Message)
            End Try

            If pipeServer IsNot Nothing Then
                pipeServer.Dispose()
            End If
        End Sub

        Private Class _StreamString
            Private ioStream As Stream
            Private streamEncoding As UnicodeEncoding

            Public Sub New(ioStream As Stream)
                Me.ioStream = ioStream
                streamEncoding = New UnicodeEncoding(False, False)
            End Sub

            Public Function ReadString() As String
                Dim len As Integer = 0
                len = CType(ioStream.ReadByte(), Integer) * 256
                len += CType(ioStream.ReadByte(), Integer)
                Dim inBuffer As Array = Array.CreateInstance(GetType(Byte), len)
                ioStream.Read(inBuffer, 0, len)

                Return streamEncoding.GetString(CType(inBuffer, Byte()))
            End Function

            Public Function WriteString(outString As String) As Integer
                Dim outBuffer() As Byte = streamEncoding.GetBytes(outString)
                Dim len As Integer = outBuffer.Length
                If len > UInt16.MaxValue Then
                    len = CType(UInt16.MaxValue, Integer)
                End If
                ioStream.WriteByte(CType(len \ 256, Byte))
                ioStream.WriteByte(CType(len And 255, Byte))
                ioStream.Write(outBuffer, 0, outBuffer.Length)
                ioStream.Flush()

                Return outBuffer.Length + 2
            End Function
        End Class
    End Class
End Namespace
