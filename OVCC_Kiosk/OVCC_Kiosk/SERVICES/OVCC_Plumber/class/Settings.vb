﻿Public Class Settings
    Public Class Constants
        Public Class Registry
            Public Class Windows
                Public Shared ReadOnly KEY__ProfileImagePath As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\ProfileList\%%PROFILE_SID_STRING%%"
                Public Shared ReadOnly VALUENAME__ProfileImagePath As String = "ProfileImagePath"


                Public Shared ReadOnly KEY__AutoLogon As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon"
                Public Shared ReadOnly VALUENAME__AutoLogon_AutoAdminLogon As String = "AutoAdminLogon"
                Public Shared ReadOnly VALUENAME__AutoLogon_DefaultDomain As String = "DefaultDomain"
                Public Shared ReadOnly VALUENAME__AutoLogon_DefaultPassword As String = "DefaultPassword"
                Public Shared ReadOnly VALUENAME__AutoLogon_DefaultUsername As String = "DefaultUsername"
                Public Shared ReadOnly VALUENAME__AutoLogon_DisableCAD As String = "DisableCAD"
                Public Shared ReadOnly VALUENAME__AutoLogon_ForceAutoLogon As String = "ForceAutoLogon"
                Public Shared ReadOnly VALUENAME__AutoLogon_ForceUnlockLogon As String = "ForceUnlockLogon"
                Public Shared ReadOnly VALUENAME__AutoLogon_AutoLogonCount As String = "AutoLogonCount"


                Public Shared ReadOnly KEY__UserSwitch As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\LogonUI\UserSwitch​"
                Public Shared ReadOnly VALUENAME__UserSwitch_Enabled As String = "Enabled"


                Public Shared ReadOnly KEY_Windows_RunAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run"
                Public Shared ReadOnly KEY_Windows_RunOnceAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunOnce"
            End Class

            Public Class OVCC
                Public Class Kiosk
                    Public Shared ReadOnly KEY__GuestTek_Root As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek"


                    Public Shared ReadOnly KEY__OVCCKiosk As String = KEY__GuestTek_Root & "\OVCCKiosk"


                    Public Shared ReadOnly KEY__OVCCKiosk_Settings As String = KEY__OVCCKiosk & "\Settings"
                    Public Shared ReadOnly VALUENAME__Settings__Debug As String = "Debug"
                    Public Shared ReadOnly VALUENAME__Settings__AdminPassword As String = "AdminPassword"
                    Public Shared ReadOnly VALUENAME__Settings__GuestPassword As String = "GuestPassword"
                    Public Shared ReadOnly VALUENAME__Settings__GuestPasswordEnabled As String = "GuestPasswordEnabled"
                    Public Shared ReadOnly VALUENAME__Settings__ScreensaverTimeout As String = "ScreensaverTimeout"
                    Public Shared ReadOnly VALUENAME__Settings__NotificationPosition As String = "NotificationPosition"
                    Public Shared ReadOnly VALUENAME__Settings__NotificationPositionAnimationSteps As String = "NotificationPositionAnimationSteps"
                    Public Shared ReadOnly VALUENAME__Settings__DefaultLanguageId As String = "DefaultLanguageId"
                    Public Shared ReadOnly VALUENAME__Settings__SessionTimeout As String = "SessionTimeout"
                    Public Shared ReadOnly VALUENAME__Settings__SessionTimeoutWarningDuration As String = "SessionTimeoutWarningDuration"
                    Public Shared ReadOnly VALUENAME__Settings__OriginalUserSwitch As String = "OriginalUserSwitch"
                    Public Shared ReadOnly VALUENAME__Settings__SecretBoxSize As String = "SecretBoxSize"
                    Public Shared ReadOnly VALUENAME__Settings__SecretBoxDelay As String = "SecretBoxDelay"
                    Public Shared ReadOnly VALUENAME__Settings__OfficeExtensions_Word As String = "OfficeExtensions_Word"
                    Public Shared ReadOnly DEFAULTVALUE__Settings__OfficeExtensions_Word As String = ".doc" & Globals.ExternalApplications.OfficeExtensionsSeparatorCharacter & ".docx"
                    Public Shared ReadOnly VALUENAME__Settings__OfficeExtensions_Excel As String = "OfficeExtensions_Excel"
                    Public Shared ReadOnly DEFAULTVALUE__Settings__OfficeExtensions_Excel As String = ".xls" & Globals.ExternalApplications.OfficeExtensionsSeparatorCharacter & ".xlsx"
                    Public Shared ReadOnly VALUENAME__Settings__OfficeExtensions_Powerpoint As String = "OfficeExtensions_Powerpoint"
                    Public Shared ReadOnly DEFAULTVALUE__Settings__OfficeExtensions_Powerpoint As String = ".ppt" & Globals.ExternalApplications.OfficeExtensionsSeparatorCharacter & ".pptx"


                    Public Shared ReadOnly KEY__OVCCKiosk_Settings_ExternalApplications As String = KEY__OVCCKiosk_Settings & "\ExternalApplications"
                    Public Shared ReadOnly VALUENAME__Settings_ExternalApplication__FINDVALUES_SEARCHSTRING As String = "ExternalApplication_"
                    Public Shared ReadOnly VALUENAME__Settings_ExternalApplication__Name_FINDVALUES_SEARCHSTRING As String = "__Name"
                    Public Shared ReadOnly VALUENAME__Settings_ExternalApplication__Name_SEARCHSTRING As String = "ExternalApplication_%%__Name"
                    Public Shared ReadOnly VALUENAME__Settings_ExternalApplication__Path_SEARCHSTRING As String = "ExternalApplication_%%__Path"
                    Public Shared ReadOnly VALUENAME__Settings_ExternalApplication__Params_SEARCHSTRING As String = "ExternalApplication_%%__Params"


                    Public Shared ReadOnly KEY__OVCCKiosk_Settings_Security As String = KEY__OVCCKiosk_Settings & "\Security"
                    Public Shared ReadOnly VALUENAME__Settings_Security__CommunicationSecret As String = "CommunicationSecret"
                    Public Shared ReadOnly VALUENAME__Settings_Security__PermittedScriptingHosts As String = "PermittedScriptingHosts"


                    Public Shared ReadOnly KEY__OVCCKiosk_Settings_Urls As String = KEY__OVCCKiosk_Settings & "\Urls"


                    Public Shared ReadOnly KEY__OVCCKiosk_Notifications As String = KEY__OVCCKiosk & "\Notifications"
                    Public Shared ReadOnly VALUENAME__Notifications__LastUpdate As String = "_Last"
                    Public Shared ReadOnly VALUENAME__Notifications__NotificationType_SEARCHSTRING As String = "Notification_Type_"
                    Public Shared ReadOnly VALUENAME__Notifications__NotificationText_SEARCHSTRING As String = "Notification_Text_"


                    Public Shared ReadOnly KEY__OVCCKiosk_Profiles As String = KEY__OVCCKiosk & "\Profiles"
                    Public Shared ReadOnly VALUENAME__Profiles__Action As String = "Action"
                    Public Shared ReadOnly VALUENAME__Profiles__Active As String = "Active"


                    Public Shared ReadOnly KEY__OVCCKiosk_Profiles_Paths As String = KEY__OVCCKiosk_Profiles & "\Paths"
                    Public Shared ReadOnly VALUENAME__Profiles_Paths__Template As String = "Template"
                    Public Shared ReadOnly VALUENAME__Profiles_Paths__Copy01 As String = "Copy01"
                    Public Shared ReadOnly VALUENAME__Profiles_Paths__Copy02 As String = "Copy02"


                    Public Shared ReadOnly KEY__OVCCKiosk_Processes As String = KEY__OVCCKiosk & "\Processes"
                    Public Shared ReadOnly VALUENAME__Processes__ExplorerPid As String = "ExplorerPid"


                    Public Shared ReadOnly KEY__OVCCKiosk_Languages As String = KEY__OVCCKiosk & "\Languages"
                    Public Shared ReadOnly VALUENAME__Languages__ActiveLanguageId As String = "ActiveLanguageId"


                    Public Shared ReadOnly KEY__OVCCKiosk_Session As String = KEY__OVCCKiosk & "\Session"
                    Public Shared ReadOnly VALUENAME__Session__IsActive As String = "IsActive"
                    Public Shared ReadOnly VALUENAME__Session__StartTimestamp As String = "StartTimestamp"
                    Public Shared ReadOnly VALUENAME__Session__SessionId As String = "SessionId"
                    Public Shared ReadOnly VALUENAME__Session__IsTermsAccepted As String = "IsTermsAccepted"
                    Public Shared ReadOnly VALUENAME__Session__IsTermsRequested As String = "IsTermsRequested"
                    Public Shared ReadOnly VALUENAME__Session__IsLogoutRequested As String = "IsLogoutRequested"
                    Public Shared ReadOnly VALUENAME__Session__ProfileAction As String = "ProfileAction"
                    Public Shared ReadOnly VALUENAME__Session__HomepageKeepAlive As String = "HomepageKeepAlive"
                    Public Shared ReadOnly VALUENAME__Session__IdleTime As String = "IdleTime"
                    Public Shared ReadOnly VALUENAME__Session__IsScreensaverActive As String = "IsScreensaverActive"
                    Public Shared ReadOnly VALUENAME__Session__IsIdleWarningActive As String = "IsIdleWarningActive"
                    Public Shared ReadOnly VALUENAME__Session__AutoStartActive As String = "AutoStartActive"


                    Public Shared ReadOnly KEY__OVCCKiosk_Themes As String = KEY__OVCCKiosk & "\Themes"
                    Public Shared ReadOnly VALUENAME__Themes__BaseFolder As String = "BaseFolder"
                    Public Shared ReadOnly VALUENAME__Themes__ActiveTheme As String = "ActiveTheme"
                End Class
            End Class
        End Class


        Public Class RegularExpressions
            Public Shared ReadOnly EXTERNAL_APPLICATION As String = "^ExternalApplication_([0-9]{2})__.+$"
        End Class


        Public Class AutoLogon
            Public Shared ReadOnly ENABLED_AutoAdminLogon As Integer = 1
            Public Shared ReadOnly ENABLED_DefaultDomain As String = ""
            Public Shared ReadOnly ENABLED_DefaultUsername As String = Users.Usernames.KIOSK_USER
            Public Shared ReadOnly ENABLED_DefaultPassword As String = Users.Passwords.KIOSK_USER
            Public Shared ReadOnly ENABLED_DisableCAD As Integer = 1
            Public Shared ReadOnly ENABLED_ForceAutoLogon As Integer = 1
            Public Shared ReadOnly ENABLED_ForceUnlockLogon As Integer = 1
            Public Shared ReadOnly ENABLED_AutoLogonCount As Integer = 0

            Public Shared ReadOnly ENABLED_UserSwitch As Integer = 0


            Public Shared ReadOnly DISABLED_AutoAdminLogon As Integer = 0
            Public Shared ReadOnly DISABLED_DefaultDomain As String = ""
            Public Shared ReadOnly DISABLED_DefaultPassword As String = ""
            Public Shared ReadOnly DISABLED_DefaultUsername As String = ""
            Public Shared ReadOnly DISABLED_DisableCAD As Integer = 1
            Public Shared ReadOnly DISABLED_ForceAutoLogon As Integer = 0
            Public Shared ReadOnly DISABLED_ForceUnlockLogon As Integer = 0
            Public Shared ReadOnly DISABLED_AutoLogonCount As Integer = 0

            Public Shared DISABLED_UserSwitch As Integer = 0
        End Class


        Public Class Applications
            Public Class Homepage
                Public Shared ReadOnly WebBrowserScriptInterface_Name As String = "OVCC_Kiosk"
            End Class
        End Class


        Public Class UI
            Public Shared ReadOnly INFO__INITIAL_MARGIN As Integer = 40
            Public Shared ReadOnly INFO__POPUP_FADE_LENGTH As Integer = 1000 ' ms
            Public Shared ReadOnly INFO__POPUP_DURATION As Integer = 5000 'ms
            Public Shared ReadOnly INFO__POPUP_ERROR_DURATION As Integer = 15000 'ms

            Public Shared ReadOnly LANGUAGE__FLAG_TOP_MARGIN As Integer = 50
            Public Shared ReadOnly LANGUAGE__FLAG_MARGIN As Integer = 5
            Public Shared ReadOnly LANGUAGE__LABEL_WIDTH As Integer = 240
            Public Shared ReadOnly LANGUAGE__LABEL_HEIGHT As Integer = 18

            Public Shared ReadOnly AIRLINE__ICON_WIDTH As Integer = 130
            Public Shared ReadOnly AIRLINE__ICON_HEIGHT As Integer = 40
            Public Shared ReadOnly AIRLINE__ICON_MARGIN As Integer = 10
            Public Shared ReadOnly AIRLINE__LABEL_HEIGHT As Integer = 18

            Public Shared ReadOnly OFFICE__ICON_TOP As Integer = 12
            Public Shared ReadOnly OFFICE__ICON_SIZE As Integer = 128
        End Class


        Public Class Services
            Public Class Plumber
                Public Shared ReadOnly Name As String = "OVCC_Plumber"
            End Class

            Public Class Watchdog
                Public Shared ReadOnly Name As String = "OVCC_Watchdog"
            End Class
        End Class


        Public Class Users
            Public Class Usernames
                Public Shared ReadOnly KIOSK_USER As String = "OVCC_User"
                Public Shared ReadOnly KIOSK_ADMIN As String = "OVCC_Admin"
            End Class

            Public Class Passwords
                'obfuscated with Helpers.XOrObfuscation_v2
                Public Shared ReadOnly KIOSK_USER As String = "575f475f06512c79246a77506f4c1c140e52505b45505f555d04004715530f0f5a05075311555a01737d236377056a1d1e1b0d020a5411045e555a5604111c5d080d5956525a44055502782c2363705167194a150a510c5015565c005f0507124609505f5055525a475351572b7e"
                Public Shared ReadOnly KIOSK_ADMIN As String = "575c150354057d76763673076f191e1450535e5743035a070b555141475b0d5a5857030b4a530754737771367f016a1e4a47585f59004204585058010546415b58095e55560f4a015a01287c246074563b4b481450030c0347040e045d055540145e5a5d5a045052475453522f2b"
                Public Shared ReadOnly KIOSK_SLAVE As String = ""
            End Class
        End Class


        Public Class Paths
            Public Class Folders
                Public Shared ReadOnly _ROOT As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "GuestTek\OVCC\Kiosk")

                Public Shared ReadOnly Kiosk_Applications As String = IO.Path.Combine(_ROOT, "apps")
                Public Shared ReadOnly Kiosk_Config As String = IO.Path.Combine(_ROOT, "config")
                Public Shared ReadOnly Kiosk_Config_Languages As String = IO.Path.Combine(Kiosk_Config, "languages")
                Public Shared ReadOnly Kiosk_Config_Airlines As String = IO.Path.Combine(Kiosk_Config, "airlines")
                Public Shared ReadOnly Kiosk_Databases As String = IO.Path.Combine(_ROOT, "databases")
                Public Shared ReadOnly Kiosk_Layout As String = IO.Path.Combine(_ROOT, "layout")
                Public Shared ReadOnly Kiosk_Terms As String = IO.Path.Combine(Kiosk_Layout, "terms")
                Public Shared ReadOnly Kiosk_Themes As String = IO.Path.Combine(Kiosk_Layout, "themes")
                Public Shared ReadOnly Kiosk_Images As String = IO.Path.Combine(Kiosk_Layout, "images")

                Public Shared ReadOnly Kiosk_LanguageFlags As String = IO.Path.Combine(Kiosk_Images, "flags")
                Public Shared ReadOnly Kiosk_AirlineImages As String = IO.Path.Combine(Kiosk_Images, "airlines")
            End Class

            Public Class Files
                Public Class Apps
                    Public Class Homepage
                        Public Shared ReadOnly Name As String = "OVCC_Homepage"
                        Public Shared ReadOnly Path As String = IO.Path.Combine(Folders.Kiosk_Applications, "homepage", Name & ".exe")
                    End Class

                    Public Class Notification
                        Public Shared ReadOnly Name As String = "OVCC_Notification"
                        Public Shared ReadOnly Path As String = IO.Path.Combine(Folders.Kiosk_Applications, "notification", Name & ".exe")
                    End Class

                    Public Class Logout
                        Public Shared ReadOnly Name As String = "OVCC_Logout"
                        Public Shared ReadOnly Path As String = IO.Path.Combine(Folders.Kiosk_Applications, "logout", Name & ".exe")
                    End Class

                    Public Class Screensaver
                        Public Shared ReadOnly Name As String = "OVCC_Screensaver"
                        Public Shared ReadOnly Path As String = IO.Path.Combine(Folders.Kiosk_Applications, "screensaver", Name & ".exe")
                    End Class
                End Class

                Public Class Data
                    Public Shared ReadOnly USER_DATA_FOLDER = IO.Path.Combine(IO.Path.GetTempPath(), "OVCC_Kiosk", "WebView2")
                    Public Shared ReadOnly LANGUAGES_FILE As String = IO.Path.Combine(Folders.Kiosk_Config_Languages, "languages.xml")
                    Public Shared ReadOnly AIRLINES_FILE As String = IO.Path.Combine(Folders.Kiosk_Config_Airlines, "airlines.xml")
                    Public Shared ReadOnly DATABASE_SESSIONS As String = IO.Path.Combine(Folders.Kiosk_Databases, "OVCCKiosk.sessions.db")
                End Class

                Public Class Misc
                    Public Shared ReadOnly EXPLORER_PROCESS_NAME As String = "explorer"
                End Class




                'Public Shared ReadOnly Info As String = IO.Path.Combine(Folders._ROOT, "OVCC_Info.exe")
                'Public Shared ReadOnly SessionTrigger As String = IO.Path.Combine(Folders._ROOT, "OVCC_SessionTrigger.exe")
                'Public Shared ReadOnly UserAppStarter As String = IO.Path.Combine(Folders._ROOT, "OVCC_UserAppStarter.exe")

            End Class
        End Class
    End Class


    Public Class [Get]
        Public Shared Function [String](Key As String, ValueName As String) As String
            Return [String](Key, ValueName, "")
        End Function

        Public Shared Function [String](Key As String, ValueName As String, [Default] As String) As String
            Return Helpers.Registry.GetValue_String(Key, ValueName, [Default])
        End Function


        Public Shared Function [Boolean](Key As String, ValueName As String) As Boolean
            Return [Boolean](Key, ValueName, False)
        End Function

        Public Shared Function [Boolean](Key As String, ValueName As String, [Default] As Boolean) As Boolean
            Return Helpers.Registry.GetValue_Boolean(Key, ValueName, [Default])
        End Function


        Public Shared Function [Integer](Key As String, ValueName As String) As Integer
            Return [Integer](Key, ValueName, 0)
        End Function

        Public Shared Function [Integer](Key As String, ValueName As String, [Default] As Integer) As Integer
            Return Helpers.Registry.GetValue_Integer(Key, ValueName, [Default])
        End Function


        Public Shared Function [Long](Key As String, ValueName As String) As Long
            Return [Long](Key, ValueName, 0)
        End Function

        Public Shared Function [Long](Key As String, ValueName As String, [Default] As Long) As Long
            Return Helpers.Registry.GetValue_Long(Key, ValueName, [Default])
        End Function
    End Class


    Public Class [Set]
        Public Shared Function [String](Key As String, ValueName As String, Value As String) As Boolean
            Return Helpers.Registry.SetValue_String(Key, ValueName, Value)
        End Function

        Public Shared Function [Boolean](Key As String, ValueName As String, Value As Boolean) As Boolean
            Return Helpers.Registry.SetValue_Boolean(Key, ValueName, Value)
        End Function

        Public Shared Function [Integer](Key As String, ValueName As String, Value As Integer) As Boolean
            Return Helpers.Registry.SetValue_Integer(Key, ValueName, Value)
        End Function

        Public Shared Function [Long](Key As String, ValueName As String, Value As Long) As Boolean
            Return Helpers.Registry.SetValue_Long(Key, ValueName, Value)
        End Function
    End Class
End Class
