﻿Public Class ServiceMain
    Private mThread_Logger As Threading.Thread
    Private mThread_Session As Threading.Thread
    Private mThread_Language As Threading.Thread
    Private mThread_Notification As Threading.Thread
    Private mThread_UI As Threading.Thread
    Private mThread_System As Threading.Thread

    Private mLoop As Boolean = False
    Private mOwnLogger As NamedPipe.Logger.LogWriter

    Private mThreadCountMax_Logger As Integer = 60
    Private mThreadCountMax_Session As Integer = 20
    Private mThreadCountMax_Language As Integer = 10
    Private mThreadCountMax_Notification As Integer = 10
    Private mThreadCountMax_UI As Integer = 10
    Private mThreadCountMax_System As Integer = 10

    Private mThreadCount_Logger As Integer = 0
    Private mThreadCount_Session As Integer = 0
    Private mThreadCount_Language As Integer = 0
    Private mThreadCount_Notification As Integer = 0
    Private mThreadCount_UI As Integer = 0
    Private mThreadCount_System As Integer = 0


    Protected Overrides Sub OnStart(ByVal args() As String)
        mOwnLogger = New NamedPipe.Logger.LogWriter(My.Application.Info.ProductName)


        mOwnLogger.WriteMessage(New String("*", 50))
        mOwnLogger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        mOwnLogger.WriteMessage("started")


        SystemHandler.PlumberStarted()


        'Generate communication secret
        Settings.Set.String(
            Settings.Constants.Registry.OVCC.Kiosk.KEY__OVCCKiosk_Settings_Security,
            Settings.Constants.Registry.OVCC.Kiosk.VALUENAME__Settings_Security__CommunicationSecret,
            Helpers.XOrObfuscation_v2.Obfuscate(Guid.NewGuid().ToString))


        '=================================================================================================================
        StartThreads()
    End Sub

    Protected Overrides Sub OnStop()
        SystemHandler.PlumberStopping()

        SystemHandler.PlumberReport("Logger threads consumed: " & mThreadCount_Logger)
        SystemHandler.PlumberReport("Session threads consumed: " & mThreadCount_Session)
        SystemHandler.PlumberReport("Language threads consumed: " & mThreadCount_Language)
        SystemHandler.PlumberReport("Notification threads consumed: " & mThreadCount_Notification)
        SystemHandler.PlumberReport("UI threads consumed: " & mThreadCount_UI)
        SystemHandler.PlumberReport("System threads consumed: " & mThreadCount_System)

        mOwnLogger.WriteMessage("stopping")
        KillThreads()

        SystemHandler.PlumberStopped()

        mOwnLogger.Dispose()
    End Sub

#Region "threads"
#Region "start/stop"
    Private Sub StartThreads()
        mLoop = True

        mThread_Logger = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Logger))
        mThread_Logger.Start()

        mThread_Session = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Session))
        mThread_Session.Start()

        mThread_Language = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Language))
        mThread_Language.Start()

        mThread_Notification = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__Notification))
        mThread_Notification.Start()

        mThread_UI = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__UI))
        mThread_UI.Start()

        mThread_System = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread__System))
        mThread_System.Start()
    End Sub

    Private Sub KillThreads()
        mLoop = False

        mOwnLogger.WriteMessage("waiting for threads to finish")

        Dim bThreadsRunning As Boolean = False
        Do
            bThreadsRunning = bThreadsRunning Or (mThread_Logger.ThreadState = Threading.ThreadState.Running)
            bThreadsRunning = bThreadsRunning Or (mThread_Session.ThreadState = Threading.ThreadState.Running)
            bThreadsRunning = bThreadsRunning Or (mThread_Language.ThreadState = Threading.ThreadState.Running)
            bThreadsRunning = bThreadsRunning Or (mThread_Notification.ThreadState = Threading.ThreadState.Running)
            bThreadsRunning = bThreadsRunning Or (mThread_UI.ThreadState = Threading.ThreadState.Running)
            bThreadsRunning = bThreadsRunning Or (mThread_System.ThreadState = Threading.ThreadState.Running)

            Threading.Thread.Sleep(1000)
        Loop While bThreadsRunning
    End Sub
#End Region


#Region "profile switcher"

#End Region


#Region "pipe threads"
    Private Sub Thread__Logger()
        mOwnLogger.WriteMessage("LOGGER", "started")


        Dim tThreads(mThreadCountMax_Logger) As Threading.Thread

        For i As Integer = 0 To mThreadCountMax_Logger - 1
            tThreads(i) = New Threading.Thread(AddressOf NamedPipe.Logger.Server.Instance)
            tThreads(i).Start()

            mOwnLogger.WriteDebug("LOGGER", "Starting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)
        Next

        mOwnLogger.WriteMessage("LOGGER", "Started " & mThreadCountMax_Logger.ToString & " threads")

        Do
            For i As Integer = 0 To mThreadCountMax_Logger - 1
                If Not tThreads(i).IsAlive Then
                    tThreads(i) = New Threading.Thread(AddressOf NamedPipe.Logger.Server.Instance)
                    tThreads(i).Start()

                    mOwnLogger.WriteDebug("LOGGER", "Restarting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)

                    mThreadCount_Logger += 1
                End If
            Next

            For j = 0 To 10
                If Not mLoop Then Exit Do
                Threading.Thread.Sleep(100)
            Next
        Loop While mLoop


        mOwnLogger.WriteDebug("LOGGER", "Stopping threads")
        Dim iThreadsRunning As Integer = 0
        Do
            iThreadsRunning = 0
            For i As Integer = 0 To mThreadCountMax_Logger - 1
                If tThreads(i).ThreadState = Threading.ThreadState.Running Then
                    iThreadsRunning += 1
                End If
            Next

            mOwnLogger.WriteDebug("LOGGER", "iThreadsRunning=" & iThreadsRunning)

            NamedPipe.Logger.Client.KillThread()
        Loop While iThreadsRunning > 0


        mOwnLogger.WriteMessage("LOGGER", "done")
    End Sub

    Private Sub Thread__Session()
        mOwnLogger.WriteMessage("SESSION", "started")


        Dim tThreads(mThreadCountMax_Session) As Threading.Thread

        For i As Integer = 0 To mThreadCountMax_Session - 1
            tThreads(i) = New Threading.Thread(AddressOf NamedPipe.Session.Server.Instance)
            tThreads(i).Start()

            mOwnLogger.WriteDebug("SESSION", "Starting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)
        Next

        mOwnLogger.WriteMessage("SESSION", "Started " & mThreadCountMax_Session.ToString & " threads")

        Do
            For i As Integer = 0 To mThreadCountMax_Session - 1
                If Not tThreads(i).IsAlive Then
                    tThreads(i) = New Threading.Thread(AddressOf NamedPipe.Session.Server.Instance)
                    tThreads(i).Start()

                    mOwnLogger.WriteDebug("SESSION", "Restarting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)

                    mThreadCount_Session += 1
                End If
            Next

            For j = 0 To 10
                If Not mLoop Then Exit Do
                Threading.Thread.Sleep(100)
            Next
        Loop While mLoop


        mOwnLogger.WriteDebug("SESSION", "Stopping threads")
        Dim iThreadsRunning As Integer = 0
        Do
            iThreadsRunning = 0
            For i As Integer = 0 To mThreadCountMax_Session - 1
                If tThreads(i).ThreadState = Threading.ThreadState.Running Then
                    iThreadsRunning += 1
                End If
            Next

            mOwnLogger.WriteDebug("SESSION", "iThreadsRunning=" & iThreadsRunning)

            NamedPipe.Session.Client.KillThread()
        Loop While iThreadsRunning > 0


        mOwnLogger.WriteMessage("SESSION", "done")
    End Sub

    Private Sub Thread__Language()
        mOwnLogger.WriteMessage("LANGUAGE", "started")


        Dim tThreads(mThreadCountMax_Language) As Threading.Thread

        For i As Integer = 0 To mThreadCountMax_Language - 1
            tThreads(i) = New Threading.Thread(AddressOf NamedPipe.Language.Server.Instance)
            tThreads(i).Start()

            mOwnLogger.WriteDebug("LANGUAGE", "Starting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)
        Next

        mOwnLogger.WriteMessage("LANGUAGE", "Started " & mThreadCountMax_Language.ToString & " threads")

        Do
            For i As Integer = 0 To mThreadCountMax_Language - 1
                If Not tThreads(i).IsAlive Then
                    tThreads(i) = New Threading.Thread(AddressOf NamedPipe.Language.Server.Instance)
                    tThreads(i).Start()

                    mOwnLogger.WriteDebug("LANGUAGE", "Restarting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)

                    mThreadCount_Language += 1
                End If
            Next

            For j = 0 To 10
                If Not mLoop Then Exit Do
                Threading.Thread.Sleep(100)
            Next
        Loop While mLoop


        mOwnLogger.WriteDebug("LANGUAGE", "Stopping threads")
        Dim iThreadsRunning As Integer = 0
        Do
            iThreadsRunning = 0
            For i As Integer = 0 To mThreadCountMax_Language - 1
                If tThreads(i).ThreadState = Threading.ThreadState.Running Then
                    iThreadsRunning += 1
                End If
            Next

            mOwnLogger.WriteDebug("LANGUAGE", "iThreadsRunning=" & iThreadsRunning)

            NamedPipe.Language.Client.KillThread()
        Loop While iThreadsRunning > 0


        mOwnLogger.WriteMessage("LANGUAGE", "done")
    End Sub

    Private Sub Thread__Notification()
        mOwnLogger.WriteMessage("NOTIFICATION", "started")


        Dim tThreads(mThreadCountMax_Notification) As Threading.Thread

        For i As Integer = 0 To mThreadCountMax_Notification - 1
            tThreads(i) = New Threading.Thread(AddressOf NamedPipe.Notification.Server.Instance)
            tThreads(i).Start()

            mOwnLogger.WriteDebug("NOTIFICATION", "Starting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)
        Next

        mOwnLogger.WriteMessage("NOTIFICATION", "Started " & mThreadCountMax_Notification.ToString & " threads")

        Do
            For i As Integer = 0 To mThreadCountMax_Notification - 1
                If Not tThreads(i).IsAlive Then
                    tThreads(i) = New Threading.Thread(AddressOf NamedPipe.Notification.Server.Instance)
                    tThreads(i).Start()

                    mOwnLogger.WriteDebug("NOTIFICATION", "Restarting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)

                    mThreadCount_Notification += 1
                End If
            Next

            For j = 0 To 10
                If Not mLoop Then Exit Do
                Threading.Thread.Sleep(100)
            Next
        Loop While mLoop


        mOwnLogger.WriteDebug("NOTIFICATION", "Stopping threads")
        Dim iThreadsRunning As Integer = 0
        Do
            iThreadsRunning = 0
            For i As Integer = 0 To mThreadCountMax_Notification - 1
                If tThreads(i).ThreadState = Threading.ThreadState.Running Then
                    iThreadsRunning += 1
                End If
            Next

            mOwnLogger.WriteDebug("NOTIFICATION", "iThreadsRunning=" & iThreadsRunning)

            NamedPipe.Notification.Client.KillThread()
        Loop While iThreadsRunning > 0


        mOwnLogger.WriteMessage("NOTIFICATION", "done")
    End Sub

    Private Sub Thread__UI()
        mOwnLogger.WriteMessage("UI", "started")


        Dim tThreads(mThreadCountMax_UI) As Threading.Thread

        For i As Integer = 0 To mThreadCountMax_UI - 1
            tThreads(i) = New Threading.Thread(AddressOf NamedPipe.UI.Server.Instance)
            tThreads(i).Start()

            mOwnLogger.WriteDebug("UI", "Starting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)
        Next

        mOwnLogger.WriteMessage("UI", "Started " & mThreadCountMax_UI.ToString & " threads")

        Do
            For i As Integer = 0 To mThreadCountMax_UI - 1
                If Not tThreads(i).IsAlive Then
                    tThreads(i) = New Threading.Thread(AddressOf NamedPipe.UI.Server.Instance)
                    tThreads(i).Start()

                    mOwnLogger.WriteDebug("UI", "Restarting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)

                    mThreadCount_UI += 1
                End If
            Next

            For j = 0 To 10
                If Not mLoop Then Exit Do
                Threading.Thread.Sleep(100)
            Next
        Loop While mLoop


        mOwnLogger.WriteDebug("UI", "Stopping threads")
        Dim iThreadsRunning As Integer = 0
        Do
            iThreadsRunning = 0
            For i As Integer = 0 To mThreadCountMax_UI - 1
                If tThreads(i).ThreadState = Threading.ThreadState.Running Then
                    iThreadsRunning += 1
                End If
            Next

            mOwnLogger.WriteDebug("UI", "iThreadsRunning=" & iThreadsRunning)

            NamedPipe.UI.Client.KillThread()
        Loop While iThreadsRunning > 0


        mOwnLogger.WriteMessage("UI", "done")
    End Sub

    Private Sub Thread__System()
        mOwnLogger.WriteMessage("SYSTEM", "started")


        Dim tThreads(mThreadCountMax_System) As Threading.Thread

        For i As Integer = 0 To mThreadCountMax_System - 1
            tThreads(i) = New Threading.Thread(AddressOf NamedPipe.System.Server.Instance)
            tThreads(i).Start()

            mOwnLogger.WriteDebug("SYSTEM", "Starting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)
        Next

        mOwnLogger.WriteMessage("SYSTEM", "Started " & mThreadCountMax_System.ToString & " threads")

        Do
            For i As Integer = 0 To mThreadCountMax_System - 1
                If Not tThreads(i).IsAlive Then
                    tThreads(i) = New Threading.Thread(AddressOf NamedPipe.System.Server.Instance)
                    tThreads(i).Start()

                    mOwnLogger.WriteDebug("SYSTEM", "Restarting thread #" & (i + 1) & " - id: " & tThreads(i).ManagedThreadId)

                    mThreadCount_System += 1
                End If
            Next

            For j = 0 To 10
                If Not mLoop Then Exit Do
                Threading.Thread.Sleep(100)
            Next
        Loop While mLoop


        mOwnLogger.WriteDebug("SYSTEM", "Stopping threads")
        Dim iThreadsRunning As Integer = 0
        Do
            iThreadsRunning = 0
            For i As Integer = 0 To mThreadCountMax_System - 1
                If tThreads(i).ThreadState = Threading.ThreadState.Running Then
                    iThreadsRunning += 1
                End If
            Next

            mOwnLogger.WriteDebug("SYSTEM", "iThreadsRunning=" & iThreadsRunning)

            NamedPipe.System.Client.KillThread()
        Loop While iThreadsRunning > 0


        mOwnLogger.WriteMessage("SYSTEM", "done")
    End Sub
#End Region
#End Region
End Class
