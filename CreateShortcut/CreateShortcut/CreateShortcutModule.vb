Imports IWshRuntimeLibrary

Module CreateShortcutModule
    Public Function CreateShortCut(ByVal shortcutName As String, ByVal creationDir As String, ByVal targetFullpath As String, ByVal shortcutArguments As String, ByVal workingDir As String, ByVal iconFile As String, ByVal iconNumber As Integer) As String
        Try
            If Not IO.Directory.Exists(creationDir) Then
                Return """" & creationDir & """ does not exist" & Environment.NewLine
            End If

            Dim shortCut As IWshRuntimeLibrary.IWshShortcut
            Dim wShell As WshShell = New WshShellClass

            shortCut = CType(wShell.CreateShortcut(creationDir & "\" & shortcutName & ".lnk"), IWshRuntimeLibrary.IWshShortcut)
            shortCut.TargetPath = targetFullpath
            shortCut.Arguments = shortcutArguments
            shortCut.WindowStyle = 1
            shortCut.Description = shortcutName
            shortCut.WorkingDirectory = workingDir
            shortCut.IconLocation = iconFile & ", " & iconNumber
            shortCut.Save()

            Return ""
        Catch ex As System.Exception
            Return "ex.Message=" & ex.Message & Environment.NewLine & _
                   "ex.Source =" & ex.Source & Environment.NewLine
        End Try
    End Function
End Module
