#Region "[### TO DO ###]"

'### TO DO #####################################################################
'-------------------------------------------------------------------------------
' DATE: 2008-03-12
' WHO: Warrick Procter
' DESCRIPTION:
' o Test.
'-------------------------------------------------------------------------------
'### TO DO #####################################################################

#End Region

#Region "[=== OPTIONS ===]"

Option Strict On
Option Explicit On
Option Compare Binary

#End Region

#Region "[=== IMPORTS ===]"

Imports System.Runtime.InteropServices

#End Region

''' <copyright>
'''###########################################################################
'''##                Copyright (c) 2008 Warrick Procter.                    ##
'''##                                                                       ##
'''## This work is covered by the "Code Project Open License", a copy of    ##
'''## which is enclosed with this package as:                               ##
'''##         "Code Project Open License (CPOL).txt",                       ##
'''## and is available from http://www.codeproject.com/.                    ##
'''##                                                                       ##
'''## No other use is permitted without the express prior written           ##
'''## permission of Warrick Procter.                                        ##
'''## For permission, try these contact addresses (current at the time of   ##
'''## writing):                                                             ##
'''##     procter_AT_xtra_DOT_co_DOT_nz                                     ##
'''##     The address for service of company "ZED Limited", New Zealand.    ##
'''###########################################################################
''' </copyright>
''' <disclaimer>
'''###########################################################################
'''## REPRESENTATIONS, WARRANTIES AND DISCLAIMER                            ##
'''## ------------------------------------------                            ##
'''## THIS WORK IS PROVIDED "AS IS", "WHERE IS" AND "AS AVAILABLE", WITHOUT ##
'''## ANY EXPRESS OR IMPLIED WARRANTIES OR CONDITIONS OR GUARANTEES. YOU,   ##
'''## THE USER, ASSUME ALL RISK IN ITS USE, INCLUDING COPYRIGHT             ##
'''## INFRINGEMENT, PATENT INFRINGEMENT, SUITABILITY, ETC. AUTHOR EXPRESSLY ##
'''## DISCLAIMS ALL EXPRESS, IMPLIED OR STATUTORY WARRANTIES OR CONDITIONS, ##
'''## INCLUDING WITHOUT LIMITATION, WARRANTIES OR CONDITIONS OF             ##
'''## MERCHANTABILITY, MERCHANTABLE QUALITY OR FITNESS FOR A PARTICULAR     ##
'''## PURPOSE, OR ANY WARRANTY OF TITLE OR NON-INFRINGEMENT, OR THAT THE    ##
'''## WORK (OR ANY PORTION THEREOF) IS CORRECT, USEFUL, BUG-FREE OR FREE OF ##
'''## VIRUSES. YOU MUST PASS THIS DISCLAIMER ON WHENEVER YOU DISTRIBUTE THE ##
'''## WORK OR DERIVATIVE WORKS.                                             ##
'''###########################################################################
''' </disclaimer>
''' <history>
''' 2008-03-12 [Warrick Procter] Created.
''' </history>
''' <summary>
''' modConstants - Implement common stuff for Special Folders.
''' </summary>
''' <overview>
''' </overview>
''' <remarks>
''' </remarks>
''' <notes>
''' </notes>
Public Module modSpecialFolders

#Region "[=== CHIDL CONSTANTS ===]"

    ''' <summary>
    ''' The virtual folder representing the Windows desktop, the root of the namespace.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_DESKTOP As Int32 = &H0

    ''' <summary>
    ''' A virtual folder representing the Internet.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_INTERNET As Int32 = &H1

    ''' <summary>
    ''' The file system directory that contains the user's program groups
    ''' (which are themselves file system directories).
    ''' A typical path is C:\Documents and Settings\username\Start Menu\Programs.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PROGRAMS As Int32 = &H2

    ''' <summary>
    ''' The virtual folder containing icons for the Control Panel applications.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_CONTROLS As Int32 = &H3

    ''' <summary>
    ''' The virtual folder containing installed printers.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PRINTERS As Int32 = &H4

    ''' <summary>
    ''' Version 6.0. The virtual folder representing the My Documents desktop item.
    ''' This is equivalent to Public Const kCSIDL_MYDOCUMENTS. 
    ''' Previous to Version 6.0. The file system directory used to physically store
    ''' a user's common repository of documents.
    ''' A typical path is C:\Documents and Settings\username\My Documents.
    ''' This should be distinguished from the virtual My Documents folder in the namespace.
    ''' To access that virtual folder, use SHGetFolderLocation, which returns
    ''' the ITEMIDLIST for the virtual location, or refer to the technique described in
    ''' Managing the File System.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PERSONAL As Int32 = &H5

    ''' <summary>
    ''' The file system directory that serves as a common repository for the user's favorite items.
    ''' A typical path is C:\Documents and Settings\username\Favorites.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_FAVORITES As Int32 = &H6

    ''' <summary>
    ''' The file system directory that corresponds to the user's Startup program group.
    ''' The system starts these programs whenever any user logs onto Windows NT or starts Windows 95.
    ''' A typical path is C:\Documents and Settings\username\Start Menu\Programs\Startup.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_STARTUP As Int32 = &H7

    ''' <summary>
    ''' The file system directory that contains shortcuts to the user's most recently used documents.
    ''' A typical path is C:\Documents and Settings\username\My Recent Documents.
    ''' To create a shortcut in this folder, use SHAddToRecentDocs.
    ''' In addition to creating the shortcut, this function updates the
    ''' Shell's list of recent documents and adds the shortcut to the
    ''' My Recent Documents submenu of the Start menu.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_RECENT As Int32 = &H8

    ''' <summary>
    ''' The file system directory that contains Send To menu items.
    ''' A typical path is C:\Documents and Settings\username\SendTo.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_SENDTO As Int32 = &H9

    ''' <summary>
    ''' The virtual folder containing the objects in the user's Recycle Bin.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_BITBUCKET As Int32 = &HA

    ''' <summary>
    ''' The file system directory containing Start menu items.
    ''' A typical path is C:\Documents and Settings\username\Start Menu.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_STARTMENU As Int32 = &HB

    ''' <summary>
    ''' Version 6.0. The virtual folder representing the My Documents desktop item.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_MYDOCUMENTS As Int32 = &HC

    ''' <summary>
    ''' The file system directory that serves as a common repository for music files.
    ''' A typical path is C:\Documents and Settings\User\My Documents\My Music.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_MYMUSIC As Int32 = &HD

    ''' <summary>
    ''' Version 6.0. The file system directory that serves as a common repository for video files.
    ''' A typical path is C:\Documents and Settings\username\My Documents\My Videos.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_MYVIDEO As Int32 = &HE

    ''' <summary> === UNUSED === </summary>
    Public Const kCSIDL_0000000F As Int32 = &HF

    ''' <summary>
    ''' The file system directory used to physically store file objects on the desktop
    ''' (not to be confused with the desktop folder itself).
    ''' A typical path is C:\Documents and Settings\username\Desktop.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_DESKTOPDIRECTORY As Int32 = &H10

    ''' <summary>
    ''' The virtual folder representing My Computer, containing everything on
    ''' the local computer: storage devices, printers, and Control Panel.
    ''' The folder may also contain mapped network drives.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_DRIVES As Int32 = &H11

    ''' <summary>
    ''' A virtual folder representing Network Neighborhood, the root of the
    ''' network namespace hierarchy.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_NETWORK As Int32 = &H12

    ''' <summary>
    ''' A file system directory containing the link objects that may exist in
    ''' the My Network Places virtual folder.
    ''' It is not the same as Public Const kCSIDL_NETWORK, which represents the network namespace root.
    ''' A typical path is C:\Documents and Settings\username\NetHood.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_NETHOOD As Int32 = &H13

    ''' <summary>
    ''' A virtual folder containing fonts. A typical path is C:\Windows\Fonts.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_FONTS As Int32 = &H14

    ''' <summary>
    ''' The file system directory that serves as a common repository for document templates.
    ''' A typical path is C:\Documents and Settings\username\Templates.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_TEMPLATES As Int32 = &H15

    ''' <summary>
    ''' The file system directory that contains the programs and folders that
    ''' appear on the Start menu for all users.
    ''' A typical path is C:\Documents and Settings\All Users\Start Menu.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_STARTMENU As Int32 = &H16

    ''' <summary>
    ''' The file system directory that contains the directories for the
    ''' common program groups that appear on the Start menu for all users.
    ''' A typical path is C:\Documents and Settings\All Users\Start Menu\Programs.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_PROGRAMS As Int32 = &H17

    ''' <summary>
    ''' The file system directory that contains the programs that appear in the
    ''' Startup folder for all users.
    ''' A typical path is C:\Documents and Settings\All Users\Start Menu\Programs\Startup.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_STARTUP As Int32 = &H18

    ''' <summary>
    ''' The file system directory that contains files and folders that appear on the desktop for
    ''' all users. A typical path is C:\Documents and Settings\All Users\Desktop.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_DESKTOPDIRECTORY As Int32 = &H19

    ''' <summary>
    ''' Version 4.71. The file system directory that serves as a common
    ''' repository for application-specific data.
    ''' A typical path is C:\Documents and Settings\username\Application Data.
    ''' This CSIDL is supported by the redistributable Shfolder.dll for systems
    ''' that do not have the Microsoft Internet Explorer 4.0 integrated Shell installed.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_APPDATA As Int32 = &H1A

    ''' <summary>
    ''' The file system directory that contains the link objects that can exist in
    ''' the Printers virtual folder.
    ''' A typical path is C:\Documents and Settings\username\PrintHood.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PRINTHOOD As Int32 = &H1B

    ''' <summary>
    ''' Version 5.0. The file system directory that serves as a data repository for
    ''' local (nonroaming) applications.
    ''' A typical path is C:\Documents and Settings\username\Local Settings\Application Data.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_LOCAL_APPDATA As Int32 = &H1C

    ''' <summary>
    ''' The file system directory that corresponds to the user's
    ''' nonlocalized Startup program group.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_ALTSTARTUP As Int32 = &H1D

    ''' <summary>
    ''' The file system directory that corresponds to the nonlocalized
    ''' Startup program group for all users.
    ''' Valid only for Microsoft Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_ALTSTARTUP As Int32 = &H1E

    ''' <summary>
    ''' The file system directory that serves as a common repository for
    ''' favorite items common to all users.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_FAVORITES As Int32 = &H1F

    ''' <summary>
    ''' Version 4.72. The file system directory that serves as a common repository for
    ''' temporary Internet files.
    ''' A typical path is C:\Documents and Settings\username\Local Settings\Temporary Internet Files.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_INTERNET_CACHE As Int32 = &H20

    ''' <summary>
    ''' The file system directory that serves as a common repository for Internet cookies.
    ''' A typical path is C:\Documents and Settings\username\Cookies.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COOKIES As Int32 = &H21

    ''' <summary>
    ''' The file system directory that serves as a common repository for Internet history items.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_HISTORY As Int32 = &H22

    ''' <summary>
    ''' Version 5.0. The file system directory containing application data for all users.
    ''' A typical path is C:\Documents and Settings\All Users\Application Data.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_APPDATA As Int32 = &H23

    ''' <summary>
    ''' Version 5.0. The Windows directory or SYSROOT.
    ''' This corresponds to the %windir% or %SYSTEMROOT% environment variables.
    ''' A typical path is C:\Windows.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_WINDOWS As Int32 = &H24

    ''' <summary>
    ''' Version 5.0. The Windows System folder.
    ''' A typical path is C:\Windows\System32.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_SYSTEM As Int32 = &H25

    ''' <summary>
    ''' Version 5.0. The Program Files folder. A typical path is C:\Program Files.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PROGRAM_FILES As Int32 = &H26

    ''' <summary>
    ''' Version 5.0. The file system directory that serves as a common repository for image files.
    ''' A typical path is C:\Documents and Settings\username\My Documents\My Pictures.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_MYPICTURES As Int32 = &H27

    ''' <summary>
    ''' Version 5.0. The user's profile folder.
    ''' A typical path is C:\Documents and Settings\username.
    ''' Applications should not create files or folders at this level;
    ''' they should put their data under the locations referred to by
    ''' Public Const kCSIDL_APPDATA or Public Const kCSIDL_LOCAL_APPDATA.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PROFILE As Int32 = &H28

    ''' <summary>
    ''' x86 system directory on RISC.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_SYSTEMX86 As Int32 = &H29

    ''' <summary>
    ''' x86 C:\Program Files on RISC.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PROGRAM_FILESX86 As Int32 = &H2A

    ''' <summary>
    ''' Version 5.0. A folder for components that are shared across applications.
    ''' A typical path is C:\Program Files\Common.
    ''' Valid only for Windows NT, Windows 2000, and Windows XP systems.
    ''' Not valid for Windows Millennium Edition (Windows Me).
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PROGRAM_FILES_COMMON As Int32 = &H2B

    ''' <summary>
    ''' x86 Program Files\Common on RISC.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PROGRAM_FILES_COMMONX86 As Int32 = &H2C

    ''' <summary>
    ''' The file system directory that contains the templates that are available to all users.
    ''' A typical path is C:\Documents and Settings\All Users\Templates.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_TEMPLATES As Int32 = &H2D

    ''' <summary>
    ''' The file system directory that contains documents that are common to all users.
    ''' A typical paths is C:\Documents and Settings\All Users\Documents.
    ''' Valid for Windows NT systems and Microsoft Windows 95 and Windows 98 systems
    ''' with Shfolder.dll installed.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_DOCUMENTS As Int32 = &H2E

    ''' <summary>
    ''' Version 5.0. The file system directory containing administrative tools for
    ''' all users of the computer.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_ADMINTOOLS As Int32 = &H2F

    ''' <summary>
    ''' Version 5.0. The file system directory that is used to store
    ''' administrative tools for an individual user.
    ''' The Microsoft Management Console (MMC) will save customized consoles
    ''' to this directory, and it will roam with the user.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_ADMINTOOLS As Int32 = &H30

    ''' <summary>
    ''' Network and Dial-up Connections.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_CONNECTIONS As Int32 = &H31

    ''' <summary> === UNUSED === </summary>
    Public Const kCSIDL_00000032 As Int32 = &H32

    ''' <summary> === UNUSED === </summary>
    Public Const kCSIDL_00000033 As Int32 = &H33

    ''' <summary> === UNUSED === </summary>
    Public Const kCSIDL_00000034 As Int32 = &H34

    ''' <summary>
    ''' Version 6.0. The file system directory that serves as a repository for
    ''' music files common to all users.
    ''' A typical path is C:\Documents and Settings\All Users\Documents\My Music.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_MUSIC As Int32 = &H35

    ''' <summary>
    ''' Version 6.0. The file system directory that serves as a repository for
    ''' image files common to all users.
    ''' A typical path is C:\Documents and Settings\All Users\Documents\My Pictures.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_PICTURES As Int32 = &H36

    ''' <summary>
    ''' Version 6.0. The file system directory that serves as a repository for
    ''' video files common to all users.
    ''' A typical path is C:\Documents and Settings\All Users\Documents\My Videos.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_VIDEO As Int32 = &H37

    ''' <summary>
    ''' %windir%\Resources\, For theme and other windows resources.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_RESOURCES As Int32 = &H38

    ''' <summary>
    ''' %windir%\Resources\[LangID], for theme and other windows specific resources.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_RESOURCES_LOCALIZED As Int32 = &H39

    ''' <summary>
    ''' Links to All Users OEM specific apps.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMMON_OEM_LINKS As Int32 = &H3A

    ''' <summary>
    ''' Version 6.0. The file system directory acting as a staging area for files
    ''' waiting to be written to CD. A typical path is
    ''' C:\Documents and Settings\username\Local Settings\Application Data\Microsoft\CD Burning.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_CDBURN_AREA As Int32 = &H3B

    ''' <summary> === UNUSED === </summary>
    Public Const kCSIDL_0000003C As Int32 = &H3C

    ''' <summary>
    ''' Computers Near Me (computered from Workgroup membership).
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_COMPUTERSNEARME As Int32 = &H3D

    ''' <summary>
    ''' Version 6.0. The file system directory containing user profile folders.
    ''' A typical path is C:\Documents and Settings.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kCSIDL_PROFILES As Int32 = &H3E

    ''' <summary> === UNUSED === </summary>
    Public Const kCSIDL_0000003F As Int32 = &H3F

    ' *** UNUSED ***
    ' Values &H00000040 through &H000000FF have not been allocated.
    ' *** Keep this up to date ***

    ''' <summary>
    ''' Build a simple pointer to an item identifier list (PIDL).
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_SIMPLE_IDLIST As Int32 = &H100

    ''' <summary>
    ''' Gets the folder's default path independent of the current location of its parent.
    ''' KF_FLAG_DEFAULT_PATH must also be set.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_NOT_PARENT_RELATIVE As Int32 = &H200

    ''' <summary>
    ''' Gets the default path for a known folder that is redirected elsewhere.
    ''' If this flag is not set, the function retrieves the current�and possibly redirected�path of the folder.
    ''' This flag includes a verification of the folder's existence unless KF_FLAG_DONT_VERIFY is also set.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_DEFAULT_PATH As Int32 = &H400

    ''' <summary>
    ''' Initializes the folder using its Desktop.ini settings.
    ''' If the folder cannot be initialized, the function returns a failure code and no path is returned.
    ''' This flag should be combined with KF_FLAG_CREATE, because if the folder has not yet been created,
    ''' the initialization fails because the result of KF_FLAG_INIT is only a desktop.ini file, not its directory.
    ''' KF_FLAG_CREATE | KF_FLAG_INIT will always succeed.
    ''' If the folder is located on a network, the function might take longer to execute.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_INIT As Int32 = &H800

    ''' <summary>
    ''' Gets the true system path for the folder, free of any aliased placeholders such as %USERPROFILE%.
    ''' This flag can only be used with SHSetKnownFolderPath and IKnownFolder::SetPath.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_NO_ALIAS As Int32 = &H1000

    ''' <summary>
    ''' Stores the full path in the registry without environment strings.
    ''' If this flag is not set, portions of the path may be represented by
    ''' environment strings such as %USERPROFILE%.
    ''' This flag can only be used with SHSetKnownFolderPath and IKnownFolder::SetPath.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_DONT_UNEXPAND As Int32 = &H2000

    ''' <summary>
    ''' Specifies not to verify the folder's existence before attempting to retrieve the path or IDList.
    ''' If this flag is not set, an attempt is made to verify that the folder is truly present at the path.
    ''' If that verification fails due to the folder being absent or inaccessible,
    ''' the function returns a failure code and no path is returned.
    ''' If the folder is located on a network, the function might take some time to execute.
    ''' Setting this flag can reduce that lagtime.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_DONT_VERIFY As Int32 = &H4000

    ''' <summary>
    ''' Forces the creation of the specified folder if that folder does not already exist.
    ''' The security provisions predefined for that folder are applied.
    ''' If the folder does not exist and cannot be created, the function returns a failure code and no path is returned.
    ''' This value can be used only with the following functions and methods:
    '''     SHGetKnownFolderPath
    '''     SHGetKnownFolderIDList
    '''     IKnownFolder::GetPath
    '''     IKnownFolder::GetIDList
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_CREATE As Int32 = &H8000

    ''' <summary>
    ''' Mask for all possible flag values.
    ''' </summary>
    ''' <remarks></remarks>
    Public Const kKF_FLAG_MASK As Int32 = &HFF00

#End Region

#Region "[=== WinAPI DECLARATIONS ===]"

    ''' <summary>
    ''' Get special folder paths.
    ''' </summary>
    ''' <param name="hwndOwner">
    ''' [in] Handle to an owner window.
    ''' This parameter is typically set to NULL.
    ''' If it is not NULL, and a dial-up connection needs to be made to access the folder,
    ''' a user interface (UI) prompt will appear in this window.
    ''' </param>
    ''' <param name="nFolder">
    ''' [in] A CSIDL value that identifies the folder whose path is to be retrieved.
    ''' Only real folders are valid.
    ''' If a virtual folder is specified, this function will fail.
    ''' You can force creation of a folder with SHGetFolderPath by combining the
    ''' folder's CSIDL with KF_FLAG_CREATE.
    ''' </param>
    ''' <param name="hToken">
    ''' [in] An access token that can be used to represent a particular user.
    ''' For systems earlier than Microsoft Windows 2000, it should be set to NULL.
    ''' For later systems, hToken is usually set to NULL.
    ''' However, you may need to assign a value to hToken for those folders that
    ''' can have multiple users but are treated as belonging to a single user.
    ''' The most commonly used folder of this type is My Documents.
    ''' The caller is responsible for correct impersonation when hToken is non-NULL.
    ''' It must have appropriate security privileges for the particular user,
    ''' including TOKEN_QUERY and TOKEN_IMPERSONATE, and the user's registry hive
    ''' must be currently mounted.
    ''' See Access Control for further discussion of access control issues.
    ''' Assigning the hToken parameter a value of -1 indicates the Default User.
    ''' This allows clients of SHGetFolderPath to find folder locations
    ''' (such as the Desktop folder) for the Default User.
    ''' The Default User user profile is duplicated when any new user account is created,
    ''' and includes special folders such as My Documents and Desktop.
    ''' Any items added to the Default User folder also appear in any new user account.
    ''' </param>
    ''' <param name="dwFlags">
    ''' [in] Flags to specify which path is to be returned.
    ''' It is used for cases where the folder associated with a CSIDL may be moved or
    ''' renamed by the user.
    ''' SHGFP_TYPE_CURRENT
    '''     Return the folder's current path.
    ''' SHGFP_TYPE_DEFAULT
    '''     Return the folder's default path.
    ''' </param>
    ''' <param name="lpszPath">
    ''' [out] Pointer to a null-terminated string of length MAX_PATH which will receive the path.
    ''' If an error occurs or S_FALSE is returned, this string will be empty.
    ''' </param>
    ''' <returns>
    ''' Returns standard HRESULT codes, including the following:
    ''' S_FALSE SHGetFolderPathA only.
    '''     The CSIDL in nFolder is valid, but the folder does not exist.
    '''     Note that the failure code is different for the ANSI and Unicode versions of this function.
    ''' E_FAIL SHGetFolderPathW only.
    '''     The CSIDL in nFolder is valid, but the folder does not exist.
    '''     Note that the failure code is different for the ANSI and Unicode versions of this function.
    ''' E_INVALIDARG The CSIDL in nFolder is not valid. 
    ''' </returns>
    ''' <remarks>
    ''' </remarks>
    Public Declare Function SHELL32_GetFolderPath _
    Lib "shell32.dll" _
    Alias "SHGetFolderPathA" _
          (ByVal hwndOwner As Int32 _
          , ByVal nFolder As Int32 _
          , ByVal hToken As Int32 _
          , ByVal dwFlags As Int32 _
          , ByVal lpszPath As String _
          ) As Int32

#End Region

#Region "[=== PUBLIC METHODS ===]"

    ''' <summary>
    ''' Get a special folder path, creating it if it does not exist.
    ''' </summary>
    ''' <param name="aSpecialFolder">
    ''' Special folder.
    ''' </param>
    ''' <returns>
    ''' Zero if successful.
    ''' </returns>
    ''' <remarks>
    ''' </remarks>
    Public Function CreateSpecialFolderPath _
          (ByVal aSpecialFolder As enuCSIDLPhysical _
          ) As String
        Dim xFlag As enuCSIDLPhysical = aSpecialFolder _
              Or enuCSIDLPhysical.FlagCreate
        Return GetSpecialFolderPath(xFlag)
    End Function

    ''' <summary>
    ''' Get a special folder path.
    ''' </summary>
    ''' <param name="aSpecialFolder">
    ''' Special folder.
    ''' </param>
    ''' <returns>
    ''' Zero if successful.
    ''' </returns>
    ''' <remarks>
    ''' </remarks>
    Public Function GetSpecialFolderPath _
          (ByVal aSpecialFolder As enuCSIDLPhysical _
          ) As String
        ' String for our WinAPI call.
        Dim xStr As String = Space(260)
        ' Working int32.
        Dim xInt As Int32
        ' Get the special folder path null-terminated-string.
        xInt = SHELL32_GetFolderPath(0, aSpecialFolder, 0, 0, xStr)
        ' Check HResult.
        If xInt <> 0 Then
            ' Error, return blank.
            Return ""
        Else
            ' No error, got a path. Return characters prior to null value.
            xInt = InStr(xStr, vbNullChar)
            If xInt > 0 Then
                Return Left(xStr, xInt - 1)
            Else
                Return xStr
            End If
        End If
    End Function

#End Region

End Module
