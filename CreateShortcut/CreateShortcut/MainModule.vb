Module MainModule
    Public Sub Main()
        Dim sReturnValue As String = ""
        Dim sLine As String
        Dim sArgsSplit() As String

        If Not IO.File.Exists(IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.Location()) & "\shortcuts.txt") Then
            Environment.Exit(-1)
        End If

        Dim sr As New IO.StreamReader(IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly.Location()) & "\shortcuts.txt")
        Do
            sLine = sr.ReadLine()

            sArgsSplit = sLine.Split(",")
            If sArgsSplit.Length = 7 Then
                sReturnValue &= CreateShortCut(sArgsSplit(0), sArgsSplit(1), sArgsSplit(2), sArgsSplit(3), sArgsSplit(4), sArgsSplit(5), sArgsSplit(6))
            End If
        Loop While Not sr.EndOfStream

        If sReturnValue.Equals("") Then
            Environment.Exit(0)
        Else
            Environment.Exit(-2)
        End If

    End Sub
End Module
