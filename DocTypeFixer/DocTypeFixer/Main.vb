Module Main
    Public Sub Main()
        Dim bRestartSiteKiosk As Boolean = False

        InitGlobals()

        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        Dim sTmp As String, iTmp As Integer
        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, c_PARAM__UPDATE_FILES) > 0 Then

                g_Live = True
            End If
            If InStr(arg, c_PARAM__ROLLBACK) > 0 Then
                g_RollBack = True
            End If
            If InStr(arg, c_PARAM__ROLLBACK_STEP) > 0 Then
                sTmp = arg.Replace(c_PARAM__ROLLBACK_STEP, "")
                If Integer.TryParse(sTmp, iTmp) Then
                    If iTmp < 0 Or iTmp > 10000 Then
                        iTmp = 0
                    End If
                Else
                    iTmp = 0
                End If
                g_RollBackStep = iTmp
            End If
        Next

        If g_RollBackStep > 0 Then
            g_RollBack = True
        End If
        If g_RollBack Then
            g_Live = False
        End If
        If g_Live Then
            g_RollBackStep = -1
            g_RollBack = False
        End If

        oLogger.WriteToLog(New String("-", 50))
        If g_Live Then
            oLogger.WriteToLog("Update files", , 0)
            UpdateFiles()

            bRestartSiteKiosk = True
        ElseIf g_RollBack Or g_RollBackStep > 0 Then
            oLogger.WriteToLog("Rolling back", , 0)
            Rollback()

            bRestartSiteKiosk = True
        Else
            oLogger.WriteToLog("Incorrect or missing parameters", , 0)
            oLogger.WriteToLog("Available parameters:", , 0)
            oLogger.WriteEmptyLineToLog()
            oLogger.WriteToLogWithoutDate(c_PARAM__UPDATE_FILES)
            oLogger.WriteToLogWithoutDate("Enable file patching")
            oLogger.WriteEmptyLineToLog()
            oLogger.WriteToLogWithoutDate(c_PARAM__ROLLBACK)
            oLogger.WriteToLogWithoutDate("Restore last backup")
            oLogger.WriteEmptyLineToLog()
            oLogger.WriteToLogWithoutDate(c_PARAM__ROLLBACK_STEP & "n")
            oLogger.WriteToLogWithoutDate("Restore last backup where n is the number of the backup to restore (1 is the last, 2 the 2nd last, etc.)")
            oLogger.WriteEmptyLineToLog()
        End If

        '------------------------------------------------------
        If bRestartSiteKiosk Then
            oLogger.WriteToLog("Killing SiteKiosk", , 0)
            KillSiteKiosk()
        End If
        
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub

    Private Sub Rollback()
        If g_RollBack And g_RollBackStep < 1 Then
            g_RollBackStep = 1
        End If

        If g_RollBackStep = 1 Then
            oLogger.WriteToLog("rolling back " & g_RollBackStep & " step", , 1)
        Else
            oLogger.WriteToLog("rolling back " & g_RollBackStep & " steps", , 1)
        End If


        Dim lBackupDirectories As New List(Of String)
        'Get backup folders sorted by last write date, descending
        lBackupDirectories = FileHelper.GetDirectories(g_BackupBaseDirectory)

        If g_RollBackStep > lBackupDirectories.Count Then
            g_RollBackStep = lBackupDirectories.Count
        End If

        'Select folder to roll back from
        Dim sDir As String = ""
        For i As Integer = (g_RollBackStep - 1) To lBackupDirectories.Count - 1
            sDir = lBackupDirectories.Item(i)
            oLogger.WriteToLog("found backup folder", , 1)
            oLogger.WriteToLog(sDir, , 2)

            If FileHelper.DirectoryEmpty(sDir) Then
                oLogger.WriteToLog("empty! let's try the next one. And by next, I mean previous.", , 3)
                sDir = ""
            Else
                Exit For
            End If
        Next

        If sDir = "" Then
            oLogger.WriteToLog("no backup files found, sorry.", , 1)
        Else
            'Copy backup files back (and skip the backup this time)
            FileHelper.CopyFiles(sDir, "c:\", True)
        End If

        oLogger.WriteToLog("done", , 1)
    End Sub

    Private Sub UpdateFiles()
        ' Getting list of files
        oLogger.WriteToLog("Get file list", , 1)
        Dim lFiles As List(Of String)

        If FileHelper.DirectoryExists(c_BASEFOLDER__64) Then
            oLogger.WriteToLog("Base folder: " & c_BASEFOLDER__64, , 2)
            lFiles = FileHelper.GetFilesRecursive(c_BASEFOLDER__64, c_FILEEXTENSIONS)
        ElseIf FileHelper.DirectoryExists(c_BASEFOLDER__32) Then
            oLogger.WriteToLog("Base folder: " & c_BASEFOLDER__32, , 2)
            lFiles = FileHelper.GetFilesRecursive(c_BASEFOLDER__32, c_FILEEXTENSIONS)
        Else
            ' WTF!!!
            oLogger.WriteToLog("No valid base folder found! WTF!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Exit Sub
        End If


        ' Fix files
        oLogger.WriteToLog("Can we fix it? Yes we can!", , 1)

        Dim patch_results As New Hashtable

        patch_results = FileHelper.FixDocType(lFiles)

        oLogger.WriteToLog("Final results", , 2)
        For Each patch_result As DictionaryEntry In patch_results
            oLogger.WriteToLog(patch_result.Key, , 3)
            oLogger.WriteToLog(patch_result.Value.ToString, , 4)
        Next
    End Sub
End Module
