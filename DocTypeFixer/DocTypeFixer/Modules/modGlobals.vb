Module modGlobals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupBaseDirectory As String
    Public g_BackupDirectory As String

    Public g_Live As Boolean
    Public g_RollBack As Boolean
    Public g_RollBackStep As Integer


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupBaseDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_backups"
        g_BackupBaseDirectory &= "\" & Application.ProductName

        g_BackupDirectory = g_BackupBaseDirectory & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(g_BackupDirectory)

        'Default values
        g_Live = False
        g_RollBack = False
        g_RollBackStep = 0
    End Sub
End Module
