Module modConstants
    Public ReadOnly c_BASEFOLDER__32 = "c:\Program Files\SiteKiosk"
    Public ReadOnly c_BASEFOLDER__64 = "c:\Program Files (x86)\SiteKiosk"

    'Public ReadOnly c_BASEFOLDER__32 = "C:\iBAHN Projects\LobbyPC Software - customisations\Theme Packs\Hilton.newUI.revision3.doctype_fixed\files"
    'Public ReadOnly c_BASEFOLDER__64 = "C:\iBAHN Projects\LobbyPC Software - customisations\Theme Packs\Hilton.newUI.revision3.doctype_fixed\files"

    'Public ReadOnly c_BASEFOLDER__32 As String = "C:\test\DocTypeFixer\test"
    'Public ReadOnly c_BASEFOLDER__64 As String = "C:\test\DocTypeFixer\test"

    ' test folder: C:\test\DocTypeFixer\test

    Public ReadOnly c_FILEEXTENSIONS As String = "*.htm|*.html|*.hsn"

    'fix result hashtable keys
    Public ReadOnly c_HASHTABLE_KEY__skipped_no_doctype As String = "Skipped (no doctype found)"
    Public ReadOnly c_HASHTABLE_KEY__skipped_ok_doctype As String = "Skipped (doctype looks ok)"
    Public ReadOnly c_HASHTABLE_KEY__patched As String = "Patched"
    Public ReadOnly c_HASHTABLE_KEY__failed As String = "Backup failed, patch skipped"

    'Command line parameters
    Public ReadOnly c_PARAM__UPDATE_FILES As String = "--update-files"
    Public ReadOnly c_PARAM__ROLLBACK As String = "--rollback"
    Public ReadOnly c_PARAM__ROLLBACK_STEP As String = "--rollback:"
End Module
