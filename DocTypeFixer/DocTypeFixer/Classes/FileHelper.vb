Imports System.IO

Public Class FileHelper
    Private Class clsCompareDirectoryInfo
        Implements IComparer

        Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements IComparer.Compare
            Dim Dir1 As DirectoryInfo
            Dim Dir2 As DirectoryInfo

            Dir1 = DirectCast(x, DirectoryInfo)
            Dir2 = DirectCast(y, DirectoryInfo)

            Compare = DateTime.Compare(Dir2.LastWriteTime, Dir1.LastWriteTime)
        End Function
    End Class

    Public Shared Function FileExists(ByVal sFile As String) As Boolean
        Return File.Exists(sFile)
    End Function

    Public Shared Function DirectoryExists(ByVal sDirectory As String) As Boolean
        Return Directory.Exists(sDirectory)
    End Function

    Public Shared Function DirectoryEmpty(ByVal sDirectory As String) As Boolean
        Return Directory.GetFileSystemEntries(sDirectory).Length <= 0
    End Function

    Public Shared Function GetDirectories(ByVal initial As String) As List(Of String)
        Dim dirs As New ArrayList
        Dim result As New List(Of String)

        Try
            Dim dir1info As New DirectoryInfo(initial)
            Dim dir2info As DirectoryInfo

            For Each dir1 As DirectoryInfo In dir1info.GetDirectories
                dir2info = New DirectoryInfo(dir1.FullName)
                For Each dir2 As DirectoryInfo In dir2info.GetDirectories
                    dirs.Add(dir2)
                Next
            Next

            dirs.Sort(New clsCompareDirectoryInfo)
        Catch ex As Exception
        End Try

        For Each dir As DirectoryInfo In dirs
            result.Add(dir.FullName)
        Next

        Return result
    End Function

    Public Shared Function GetFilesRecursive(ByVal initial As String, Optional ByVal filter As String = "*.*") As List(Of String)
        Dim result As New List(Of String)
        Dim stack As New Stack(Of String)

        Dim MultipleFilters() As String = filter.Split("|")
        stack.Push(initial)

        Do While (stack.Count > 0)
            Dim dir As String = stack.Pop

            oLogger.WriteToLogRelative("examining folder", , 1)
            oLogger.WriteToLogRelative(dir, , 2)

            Try
                For Each FileFilter As String In MultipleFilters
                    result.AddRange(Directory.GetFiles(dir, FileFilter, SearchOption.TopDirectoryOnly))
                Next

                Dim directoryName As String
                For Each directoryName In Directory.GetDirectories(dir)
                    stack.Push(directoryName)
                Next

            Catch ex As Exception
            End Try
        Loop

        oLogger.WriteToLogRelative("removing duplicates", , 1)
        result.Sort()
        For x As Integer = result.Count - 1 To 1 Step -1
            If result(x) = result(x - 1) Then
                result.RemoveAt(x)
            End If
        Next x

        oLogger.WriteToLogRelative("found " & result.Count & " files", , 1)

        Return result
    End Function

    Public Shared Sub CopyFiles(ByVal SourceDir As String, ByVal DestinationDir As String, Optional ByVal skip_backup As Boolean = False)
        RecursiveDirectoryCopy(SourceDir, DestinationDir, skip_backup)
    End Sub

    Private Shared Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String, ByVal skip_backup As Boolean)
        Dim sDir As String
        Dim dDirInfo As IO.DirectoryInfo
        Dim sDirInfo As IO.DirectoryInfo
        Dim sFile As String
        Dim sFileInfo As IO.FileInfo
        Dim dFileInfo As IO.FileInfo

        ' Add trailing separators to the supplied paths if they don't exist.
        If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            sourceDir &= System.IO.Path.DirectorySeparatorChar
        End If
        If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            destDir &= System.IO.Path.DirectorySeparatorChar
        End If

        'If destination directory does not exist, create it.
        dDirInfo = New System.IO.DirectoryInfo(destDir)
        If dDirInfo.Exists = False Then dDirInfo.Create()
        dDirInfo = Nothing


        ' Get a list of directories from the current parent.
        For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
            sDirInfo = New System.IO.DirectoryInfo(sDir)
            dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
            ' Create the directory if it does not exist.
            If dDirInfo.Exists = False Then dDirInfo.Create()
            ' Since we are in recursive mode, copy the children also
            RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName, skip_backup)
            sDirInfo = Nothing
            dDirInfo = Nothing
        Next

        ' Get the files from the current parent.
        For Each sFile In System.IO.Directory.GetFiles(sourceDir)
            sFileInfo = New System.IO.FileInfo(sFile)
            dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

            oLogger.WriteToLogRelative("copying", , 1)
            oLogger.WriteToLogRelative("source: " & sFileInfo.FullName, , 2)
            oLogger.WriteToLogRelative("dest  : " & dFileInfo.FullName, , 2)

            'If File does exists, backup.
            If dFileInfo.Exists And Not skip_backup Then
                oLogger.WriteToLogRelative("already exists", , 3)

                If dFileInfo.IsReadOnly Then
                    oLogger.WriteToLogRelative("file is read-only, fixing...", , 4)
                    dFileInfo.IsReadOnly = False
                End If
                oLogger.WriteToLogRelative("creating backup", , 3)
                If BackupFile(dFileInfo) Then

                Else

                End If
            End If

            Try
                sFileInfo.CopyTo(dFileInfo.FullName, True)
                If IO.File.Exists(dFileInfo.FullName) Then
                    oLogger.WriteToLogRelative("ok", , 2)
                Else
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End If
            Catch ex As Exception
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            End Try

            sFileInfo = Nothing
            dFileInfo = Nothing
        Next
    End Sub

    Public Shared Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLogRelative("backup: " & sFullDest, , 5)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLogRelative("ok", , 6)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

    Public Shared Function FixDocType(ByVal file_list As List(Of String)) As Hashtable
        Dim result As Hashtable = New Hashtable

        Dim f As String
        Dim line As String
        Dim lines As New List(Of String)
        Dim line_number As Integer
        Dim skip_file As Boolean
        Dim file_patched As Boolean

        result.Add(c_HASHTABLE_KEY__skipped_no_doctype, 0)
        result.Add(c_HASHTABLE_KEY__skipped_ok_doctype, 0)
        result.Add(c_HASHTABLE_KEY__patched, 0)
        result.Add(c_HASHTABLE_KEY__failed, 0)

        For i As Integer = 0 To file_list.Count - 1
            lines.Clear()
            line_number = 0
            skip_file = False
            file_patched = False

            f = file_list.Item(i)

            oLogger.WriteToLogRelative("examining file", , 1)
            oLogger.WriteToLogRelative(f, , 2)

            Using srRead As New IO.StreamReader(f)
                Do While srRead.Peek() >= 0
                    line = srRead.ReadLine()
                    line_number += 1

                    If line_number = 1 Then
                        ' Check for doctype in first line
                        If Not line.Contains("<!DOCTYPE") Then
                            ' Skip this file
                            skip_file = True
                            Exit Do
                        End If
                    End If

                    lines.Add(line)
                Loop
            End Using

            If skip_file Then
                ' do nothing
                oLogger.WriteToLogRelative("no patch needed (no doctype found in first line)", , 3)
                result(c_HASHTABLE_KEY__skipped_no_doctype) += 1
            Else
                ' check if we need to patch
                If lines.Item(0).Contains(".dtd") Then
                    lines.Item(0) = "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"">"
                    file_patched = True
                End If
                If lines.Item(1).Contains(".dtd") Then
                    lines.Item(0) = "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"">"
                    lines.RemoveAt(1)
                    file_patched = True
                End If
                If Not file_patched Then
                    oLogger.WriteToLogRelative("no patch needed (doctype looks ok)", , 3)
                    result(c_HASHTABLE_KEY__skipped_ok_doctype) += 1
                Else
                    oLogger.WriteToLogRelative("patching", , 3)
                    oLogger.WriteToLogRelative("backup", , 4)
                    If BackupFile(New IO.FileInfo(f)) Then
                        oLogger.WriteToLogRelative("saving patched file", , 4)
                        Using sw As New IO.StreamWriter(f)
                            For Each l As String In lines
                                sw.WriteLine(l)
                            Next
                        End Using

                        result(c_HASHTABLE_KEY__patched) += 1
                    Else
                        result(c_HASHTABLE_KEY__failed) += 1
                    End If
                End If
            End If
        Next

        Return result
    End Function
End Class