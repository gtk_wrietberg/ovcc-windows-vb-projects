Imports Microsoft.Win32

Module Main
    Private ReadOnly cRegKey_HostName As String = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters"
    Private ReadOnly cRegKey_ComputerName As String = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\ComputerName"

    Private oLogger As Logger

    Public Sub Main()
        oLogger = New Logger

        oLogger.LogFilePath = "C:\"
        oLogger.LogFileName = "HostNameSync.log"
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog(New String("-", 20))

        Try
            '----------------------------------------------------------------------------
            oLogger.WriteToLog("Getting computer name", , 0)

            Dim sComputerName As String, sActiveComputerName As String
            Dim sHostName As String, sNVHostName As String

            sComputerName = Registry.GetValue(cRegKey_ComputerName & "\ComputerName", "ComputerName", "")
            sActiveComputerName = Registry.GetValue(cRegKey_ComputerName & "\ActiveComputerName", "ComputerName", "")

            oLogger.WriteToLog("ComputerName", , 1)
            oLogger.WriteToLog(sComputerName, , 2)
            oLogger.WriteToLog("ActiveComputerName", , 1)
            oLogger.WriteToLog(sActiveComputerName, , 2)


            oLogger.WriteToLog("Getting host name", , 0)
            sHostName = Registry.GetValue(cRegKey_HostName, "Hostname", "")
            sNVHostName = Registry.GetValue(cRegKey_HostName, "NV Hostname", "")

            oLogger.WriteToLog("Hostname", , 1)
            oLogger.WriteToLog(sHostName, , 2)
            oLogger.WriteToLog("NV HostName", , 1)
            oLogger.WriteToLog(sNVHostName, , 2)

            If sComputerName = "" Then
                oLogger.WriteToLog("ComputerName is empty, huh?", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                ExitApp(1)
            End If
            If sActiveComputerName = "" Then
                oLogger.WriteToLog("ActiveComputerName is empty, huh?", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                ExitApp(2)
            End If


            '----------------------------------------------------------------------------
            If sComputerName <> sActiveComputerName Or sHostName <> sActiveComputerName Or sNVHostName <> sActiveComputerName Then
                oLogger.WriteToLog("Syncing names", , 0)
            Else
                oLogger.WriteToLog("Nothing to do", , 0)
                ExitApp(0)
            End If


            If sComputerName <> sActiveComputerName Then
                oLogger.WriteToLog("ComputerName", , 1)
                oLogger.WriteToLog("was", , 2)
                oLogger.WriteToLog(sComputerName, , 3)

                Registry.SetValue(cRegKey_ComputerName & "\ComputerName", "ComputerName", sActiveComputerName, RegistryValueKind.String)

                oLogger.WriteToLog("is now", , 2)
                oLogger.WriteToLog(sActiveComputerName, , 3)
            End If
            If sHostName <> sActiveComputerName Then
                oLogger.WriteToLog("HostName", , 1)
                oLogger.WriteToLog("was", , 2)
                oLogger.WriteToLog(sHostName, , 3)

                Registry.SetValue(cRegKey_HostName, "Hostname", sActiveComputerName, RegistryValueKind.String)

                oLogger.WriteToLog("is now", , 2)
                oLogger.WriteToLog(sActiveComputerName, , 3)
            End If
            If sNVHostName <> sActiveComputerName Then
                oLogger.WriteToLog("NV HostName", , 1)
                oLogger.WriteToLog("was", , 2)
                oLogger.WriteToLog(sNVHostName, , 3)

                Registry.SetValue(cRegKey_HostName, "NV Hostname", sActiveComputerName, RegistryValueKind.String)

                oLogger.WriteToLog("is now", , 2)
                oLogger.WriteToLog(sActiveComputerName, , 3)
            End If

            ExitApp(0)
        Catch e As Exception
            oLogger.WriteToLog("ERROR ERROR ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("Err.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(e.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            ExitApp(100)
        End Try
    End Sub

    Private Sub ExitApp(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("bye (" & iExitCode.ToString & ")", , 0)
        System.Environment.Exit(iExitCode)
    End Sub
End Module
