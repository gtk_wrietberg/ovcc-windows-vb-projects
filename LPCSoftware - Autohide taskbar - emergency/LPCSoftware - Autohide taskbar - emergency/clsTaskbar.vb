Imports System.Runtime.InteropServices

Public Class clsTaskBar
#Region "WIN API FUNCTIONS"
    <DllImport("user32.dll")> Private Shared Function FindWindow(ByVal className As String, ByVal windowText As String) As Integer
    End Function

    <DllImport("user32.dll")> Private Shared Function FindWindowEx(ByVal HWnd1 As IntPtr, ByVal HWnd2 As IntPtr, ByVal lpsz1 As IntPtr, ByVal lpsz2 As String) As Integer
    End Function

    <DllImport("user32.dll")> Private Shared Function ShowWindow(ByVal HWnd As Integer, ByVal Command As Integer) As Integer
    End Function

    <DllImport("user32.dll")> Private Shared Function IsWindowVisible(ByVal hwnd As Integer) As Long
    End Function
#End Region

    Private ReadOnly SW_HIDE As Integer = 0
    Private ReadOnly SW_SHOW As Integer = 1

    Public Sub New()
    End Sub

    Public Function GetHWnd_Taskbar() As Integer
        Dim iHWnd As Integer = 0

        Try
            iHWnd = FindWindow("Shell_TrayWnd", "")
        Catch ex As Exception

        End Try

        Return iHWnd
    End Function

    Public Function GetHWnd_StartButton() As Integer
        Dim ptr1 = New IntPtr(49175)
        Dim iHWnd As Integer = 0

        Try
            iHWnd = FindWindowEx(IntPtr.Zero, IntPtr.Zero, ptr1, "Start")
        Catch ex As Exception

        End Try

        Return iHWnd
    End Function

    Public Function GetHWnd_Desktop() As Integer
        Dim iHWnd As Integer = 0

        Try
            iHWnd = FindWindow("Progman", "Program Manager")
        Catch ex As Exception

        End Try

        Return iHWnd
    End Function

    Public Sub HideTaskbar()
        _Hide(GetHWnd_Taskbar)
    End Sub

    Public Sub HideStartButton()
        _Hide(GetHWnd_StartButton)
    End Sub

    Public Sub HideDesktop()
        _Hide(GetHWnd_Desktop)
    End Sub

    Public Sub HideAll()
        HideTaskbar()
        HideStartButton()
        HideDesktop()
    End Sub

    Public Sub ShowTaskbar()
        _Show(GetHWnd_Taskbar)
    End Sub

    Public Sub ShowStartButton()
        _Show(GetHWnd_StartButton)
    End Sub

    Public Sub ShowDesktop()
        _Show(GetHWnd_Desktop)
    End Sub

    Public Sub ShowAll()
        ShowTaskbar()
        ShowStartButton()
        ShowDesktop()
    End Sub

    Private Function _IsWindowVisible(ByVal hWnd As Integer) As Boolean
        Try
            Return IsWindowVisible(hWnd) <> 0
        Catch ex As Exception
        End Try

        Return False
    End Function

    Private Function _Hide(ByVal hWnd As Integer) As Boolean
        Try
            ShowWindow(hWnd, SW_HIDE)

            Return True
        Catch ex As Exception
        End Try

        Return False
    End Function

    Private Function _Show(ByVal hWnd As Integer) As Boolean
        Try
            ShowWindow(hWnd, SW_SHOW)

            Return True
        Catch ex As Exception
        End Try

        Return False
    End Function
End Class
