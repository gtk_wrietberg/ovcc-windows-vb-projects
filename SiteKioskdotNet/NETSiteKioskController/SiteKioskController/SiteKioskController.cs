using System;
using System.Collections.Generic;
using System.Text;

using System.Runtime.InteropServices;
using SiteKioskRuntimeLib;
using SiteCashLib;
using PluginBaseLib;
using System.Collections;

namespace SiteKioskController
{
	/// <summary>
	/// SiteKioskController, perform various tasks with SiteKiosk (when it is running)
	/// </summary>
	public class k_SiteKioskController : IDisposable
	{
		[DllImport("ole32.dll", CallingConvention = CallingConvention.StdCall)]
		public static extern int CoGetClassObject (ref Guid rclsid, uint dwClsContext, IntPtr pServerInfo, ref Guid riid, out IntPtr ppv);

		ISiteKiosk mk_pSiteKiosk;

		/// <summary>
		/// Get main SiteKiosk object
		/// </summary>
		public ISiteKiosk SiteKiosk
		{
			get
			{
				return GetSiteKioskInterface();
			}
		}

		/// <summary>
		/// Plugin Collection, use the index to get a specific plugin
		/// </summary>
		public IPluginCollection Plugins
		{
			get
			{
				return SiteKiosk.Plugins as IPluginCollection;
			}
		}

		/// <summary>
		/// Shortcut to SiteCash plugin, it is casted as ISiteCash9
		/// </summary>
		public ISiteCash9 SiteCash
		{
			get
			{
				return this.GetPlugin("SiteCash") as ISiteCash9;
			}
		}

		/// <summary>
		/// Needed for different UI functions, e.g. Navigating
		/// </summary>
		public ISiteKioskUI SiteKioskUI
		{
			get
			{
				return this.SiteKiosk.SiteKioskUI as ISiteKioskUI;
			}
		}

		/// <summary>
		/// Get SiteKiosk main window or any other window
		/// </summary>
		public IWindowList WindowList
		{
			get
			{
				return SiteKiosk.WindowList as IWindowList;
			}
		}

		/// <summary>
		/// Create a new SiteKiosk Controller instance
		/// </summary>
		public k_SiteKioskController()
		{
			mk_pSiteKiosk = null;
		}

		/// <summary>
		/// Get one specific plugin from the plugin collection
		/// </summary>
		/// <param name="as_PluginName">Possible values: SiteCoach, SiteCam, SiteCash</param>
		/// <returns>Returns Requested IPluginBase</returns>
		public IPluginBase GetPlugin(string as_PluginName)
		{
			return this.Plugins[as_PluginName] as IPluginBase;
		}

		private ISiteKiosk GetSiteKioskInterface()
		{
			if (mk_pSiteKiosk != null)
				return mk_pSiteKiosk;

			// initialize GUID's for classes and interfaces
			Guid lr_FactoryGuid = typeof(ISiteKioskFactory).GUID;
			Guid lr_FactoryClass = typeof(SiteKioskFactoryClass).GUID;
			Guid lr_SiteKioskGuid = typeof(ISiteKiosk).GUID;

			// try to get the ISiteKioskFactory interface of the instance
			// of SiteKioskFactoryClass
			IntPtr lk_FactoryPtr = new IntPtr();
			CoGetClassObject(ref lr_FactoryClass, 4, new IntPtr(), ref lr_FactoryGuid, out lk_FactoryPtr);
			if (lk_FactoryPtr == IntPtr.Zero)
				// SiteKiosk is not running
				throw new ArgumentNullException("SiteKiosk is not running!");

			// convert the received IntPtr to the requested ISiteKioskFactory
			// interface
			ISiteKioskFactory lk_Factory = Marshal.GetObjectForIUnknown(lk_FactoryPtr) as ISiteKioskFactory;

			if (lk_Factory == null)
				throw new ArgumentNullException("SiteKiosk is not running!");

			// call CreateSiteKiosk to get the ISiteKiosk interface of the
			// current instance of SiteKiosk
			IntPtr lk_SiteKioskPtr = new IntPtr();
			lk_Factory.CreateSiteKiosk(ref lr_SiteKioskGuid, out lk_SiteKioskPtr);

			if (lk_SiteKioskPtr == IntPtr.Zero)
				throw new ArgumentNullException("SiteKiosk is not running!");

			// convert the received IntPtr to the requested
			// ISiteKioskFactory interface
			mk_pSiteKiosk = Marshal.GetObjectForIUnknown(lk_SiteKioskPtr) as ISiteKiosk;

			if (mk_pSiteKiosk == null)
				throw new ArgumentNullException("SiteKiosk is not running!");

			return mk_pSiteKiosk;
		}

		#region IDisposable Members

		/// <summary>
		/// Clear COM references
		/// </summary>
		public void Dispose()
		{
			mk_pSiteKiosk = null;
		}

		#endregion
	}
}
