using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using PluginBaseLib;
using SiteKioskRuntimeLib;

namespace SiteKioskController
{
	public partial class k_Form : Form
	{
		k_SiteKioskController mk_Controller;

        public k_Form()
		{
			InitializeComponent();

			mk_Controller = new k_SiteKioskController();

            

        }

		private void StatusTmr_Tick(object sender, EventArgs e)
		{
			try
			{
				UpdateStats();
			}
			catch (Exception)
			{}
		}

		private void UpdateStats()
		{
			StringBuilder ls_Status = new StringBuilder();
			if (mk_Controller.SiteCash == null || !(mk_Controller.SiteCash as IPluginBase).Enabled)
			{
				ls_Status.Append("SiteCash needs to be installed and enabled!");
			}
			else
			{
				ls_Status.Append("Balance: ");
				ls_Status.Append(mk_Controller.SiteCash.CurrentBalance);
			}

			StatusLbl.Text = ls_Status.ToString();
		}

		private void CreditEdt_Click(object sender, EventArgs e)
		{
			decimal ld_Amount = 0.0M;
			if (decimal.TryParse(CreditDebitEdt.Text, out ld_Amount))
				mk_Controller.SiteCash.Credit(ld_Amount);

			UpdateStats();
		}

		private void DebitEdt_Click(object sender, EventArgs e)
		{
			decimal ld_Amount = 0.0M;
			if (decimal.TryParse(CreditDebitEdt.Text, out ld_Amount))
				mk_Controller.SiteCash.Debit(ld_Amount);

			UpdateStats();
		}

		private void NavBtn_Click(object sender, EventArgs e)
		{
			IWindowInfo lk_Window = mk_Controller.WindowList.MainWindow as IWindowInfo;
			ISiteKioskWindow lk_SKWindow = lk_Window.SiteKioskWindow as ISiteKioskWindow;
			(lk_SKWindow.SiteKioskWebBrowser as ISiteKioskWebBrowser).Navigate(NavEdt.Text, true);
		}
	}
}