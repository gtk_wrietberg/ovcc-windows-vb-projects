using System.Runtime.InteropServices;
using SiteKioskRuntimeLib;

namespace SiteKioskController
{
	partial class k_Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.StatusTxtLbl = new System.Windows.Forms.Label();
			this.StatusLbl = new System.Windows.Forms.Label();
			this.StatusTmr = new System.Windows.Forms.Timer(this.components);
			this.CreditDebitEdt = new System.Windows.Forms.TextBox();
			this.CreditEdt = new System.Windows.Forms.Button();
			this.DebitEdt = new System.Windows.Forms.Button();
			this.NavEdt = new System.Windows.Forms.TextBox();
			this.NavBtn = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// StatusTxtLbl
			// 
			this.StatusTxtLbl.AutoSize = true;
			this.StatusTxtLbl.Location = new System.Drawing.Point(12, 9);
			this.StatusTxtLbl.Name = "StatusTxtLbl";
			this.StatusTxtLbl.Size = new System.Drawing.Size(87, 13);
			this.StatusTxtLbl.TabIndex = 1;
			this.StatusTxtLbl.Text = "SiteKiosk Status:";
			// 
			// StatusLbl
			// 
			this.StatusLbl.AutoSize = true;
			this.StatusLbl.Location = new System.Drawing.Point(12, 32);
			this.StatusLbl.Name = "StatusLbl";
			this.StatusLbl.Size = new System.Drawing.Size(73, 13);
			this.StatusLbl.TabIndex = 2;
			this.StatusLbl.Text = "Balance: 0.00";
			// 
			// StatusTmr
			// 
			this.StatusTmr.Enabled = true;
			this.StatusTmr.Interval = 1000;
			this.StatusTmr.Tick += new System.EventHandler(this.StatusTmr_Tick);
			// 
			// CreditDebitEdt
			// 
			this.CreditDebitEdt.Location = new System.Drawing.Point(15, 69);
			this.CreditDebitEdt.Name = "CreditDebitEdt";
			this.CreditDebitEdt.Size = new System.Drawing.Size(100, 20);
			this.CreditDebitEdt.TabIndex = 3;
			this.CreditDebitEdt.Text = "0.00";
			// 
			// CreditEdt
			// 
			this.CreditEdt.Location = new System.Drawing.Point(122, 69);
			this.CreditEdt.Name = "CreditEdt";
			this.CreditEdt.Size = new System.Drawing.Size(47, 23);
			this.CreditEdt.TabIndex = 4;
			this.CreditEdt.Text = "Credit";
			this.CreditEdt.UseVisualStyleBackColor = true;
			this.CreditEdt.Click += new System.EventHandler(this.CreditEdt_Click);
			// 
			// DebitEdt
			// 
			this.DebitEdt.Location = new System.Drawing.Point(175, 69);
			this.DebitEdt.Name = "DebitEdt";
			this.DebitEdt.Size = new System.Drawing.Size(75, 23);
			this.DebitEdt.TabIndex = 5;
			this.DebitEdt.Text = "Debit";
			this.DebitEdt.UseVisualStyleBackColor = true;
			this.DebitEdt.Click += new System.EventHandler(this.DebitEdt_Click);
			// 
			// NavEdt
			// 
			this.NavEdt.Location = new System.Drawing.Point(15, 118);
			this.NavEdt.Name = "NavEdt";
			this.NavEdt.Size = new System.Drawing.Size(235, 20);
			this.NavEdt.TabIndex = 6;
			this.NavEdt.Text = "http://";
			// 
			// NavBtn
			// 
			this.NavBtn.Location = new System.Drawing.Point(179, 144);
			this.NavBtn.Name = "NavBtn";
			this.NavBtn.Size = new System.Drawing.Size(71, 22);
			this.NavBtn.TabIndex = 7;
			this.NavBtn.Text = "Navigate";
			this.NavBtn.UseVisualStyleBackColor = true;
			this.NavBtn.Click += new System.EventHandler(this.NavBtn_Click);
			// 
			// k_Form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Controls.Add(this.NavBtn);
			this.Controls.Add(this.NavEdt);
			this.Controls.Add(this.DebitEdt);
			this.Controls.Add(this.CreditEdt);
			this.Controls.Add(this.CreditDebitEdt);
			this.Controls.Add(this.StatusLbl);
			this.Controls.Add(this.StatusTxtLbl);
			this.Name = "k_Form";
			this.Text = "SiteKiosk Controller";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label StatusTxtLbl;
		private System.Windows.Forms.Label StatusLbl;
		private System.Windows.Forms.Timer StatusTmr;
		private System.Windows.Forms.TextBox CreditDebitEdt;
		private System.Windows.Forms.Button CreditEdt;
		private System.Windows.Forms.Button DebitEdt;
		private System.Windows.Forms.TextBox NavEdt;
		private System.Windows.Forms.Button NavBtn;
	}
}

