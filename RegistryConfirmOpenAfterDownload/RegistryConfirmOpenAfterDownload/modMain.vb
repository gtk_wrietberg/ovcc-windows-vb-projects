Imports Microsoft.Win32
Imports System.Xml

Module modMain
    Private ReadOnly RegClasses As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Classes"

    Private oExtensions As clsExtensions

    Public Sub Main()
        Dim xmlDoc As XmlDocument
        Dim nodeRoot As XmlNode
        Dim nodeList As XmlNodeList
        Dim node As XmlNode
        Dim sExtension As String, sSetting As String
        Dim sRegValue As String, iRegValue As Integer, iRegValueNew As Integer, oRegValue As Object

        oExtensions = New clsExtensions

        xmlDoc = New XmlDocument
        xmlDoc.Load("RegistryConfirmOpenAfterDownload.config.xml")

        nodeRoot = xmlDoc.SelectSingleNode("RegistryConfirmOpenAfterDownload")
        nodeList = nodeRoot.SelectSingleNode("extensions").SelectNodes("extension")
        For Each node In nodeList
            oExtensions.Add(node.SelectSingleNode("name").InnerText, node.SelectSingleNode("settings").SelectSingleNode("confirmopenafterdownload").InnerText)
        Next

        sExtension = ""
        sSetting = ""
        While oExtensions.GetNext(sExtension, sSetting)
            sRegValue = Registry.GetValue(RegClasses & "\." & sExtension, "", "")

            If sRegValue <> "" Then
                oRegValue = Registry.GetValue(RegClasses & "\" & sRegValue, "EditFlags", 0)

                If LCase(oRegValue.GetType.FullName).IndexOf("int") > -1 Then
                    iRegValue = CType(oRegValue, Integer)
                Else
                    iRegValue = 0
                End If

                If sSetting = "no" Then
                    iRegValueNew = 65536
                Else
                    iRegValueNew = 0
                End If

                If iRegValueNew <> iRegValue Then
                    Registry.SetValue(RegClasses & "\" & sRegValue, "EditFlags", iRegValueNew)
                End If
            End If
        End While
    End Sub
End Module
