Public Class clsExtensions
    Private mExtensions As Collection
    Private mSettings As Collection
    Private mCount As Integer

    Public Sub New()
        Me.Reset()
    End Sub

    Public Sub Reset()
        mExtensions = New Collection
        mSettings = New Collection
        mCount = 0
    End Sub

    Public Sub Add(ByVal Extension As String, ByVal ConfirmOpenAfterDownload As String)
        mExtensions.Add(Extension)
        mSettings.Add(ConfirmOpenAfterDownload)
    End Sub

    Public Function GetNext(ByRef Extension As String, ByRef ConfirmOpenAfterDownload As String) As Boolean
        mCount = mCount + 1
        If mCount > mExtensions.Count Then
            Return False
        End If

        Extension = CType(mExtensions.Item(mCount), String)
        ConfirmOpenAfterDownload = CType(mSettings.Item(mCount), String)

        Return True
    End Function
End Class
