﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("RegistryConfirmOpenAfterDownload")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("iBAHN")> 
<Assembly: AssemblyProduct("RegistryConfirmOpenAfterDownload")> 
<Assembly: AssemblyCopyright("Copyright ©  2009")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b230f1ca-b28b-4302-b188-f9b6d8b84331")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.1.1113")> 
<Assembly: AssemblyFileVersion("1.2.1.1113")> 
