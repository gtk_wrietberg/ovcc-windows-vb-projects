﻿Imports System.ComponentModel
Imports System.Net.Http
Imports System.Runtime

Public Class frmMain
#Region "Thread Safe stuff"
    Delegate Sub LogToProgressTextBoxCallback(ByVal [text] As String)

    Public Sub LogToProgressTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProgressTextBoxCallback(AddressOf LogToProgressTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text])
        End If
    End Sub
#End Region


    Private ReadOnly _file_devices As String = "devices.txt"
    Private ReadOnly _file_properties As String = "properties.txt"


    Private Class _device
        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mCurrentPropertyId As Integer
        Public Property CurrentPropertyId() As Integer
            Get
                Return mCurrentPropertyId
            End Get
            Set(ByVal value As Integer)
                mCurrentPropertyId = value
            End Set
        End Property

        Private mNewPropertyId As Integer
        Public Property NewPropertyId() As Integer
            Get
                Return mNewPropertyId
            End Get
            Set(ByVal value As Integer)
                mNewPropertyId = value
            End Set
        End Property

        Public Sub New(sName As String, iCurrentPropertyId As Integer)
            mName = sName
            mCurrentPropertyId = iCurrentPropertyId
        End Sub

        Public Overrides Function ToString() As String
            Return mName & "," & mCurrentPropertyId & "," & mNewPropertyId
        End Function
    End Class

    Private Class _property
        Private mId As Integer
        Public Property Id() As Integer
            Get
                Return mId
            End Get
            Set(ByVal value As Integer)
                mId = value
            End Set
        End Property

        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mNavisionCode As String
        Public Property NavisionCode() As String
            Get
                Return mNavisionCode
            End Get
            Set(ByVal value As String)
                mNavisionCode = value
            End Set
        End Property

        Private mOVCCCode As String
        Public Property OVCCCode() As String
            Get
                Return mOVCCCode
            End Get
            Set(ByVal value As String)
                mOVCCCode = value
            End Set
        End Property

        Public Sub New(iId As Integer, sName As String, sNavisionCode As String, sOVCCCode As String)
            mId = iId
            mName = sName
            mNavisionCode = sNavisionCode
            mOVCCCode = sOVCCCode
        End Sub
    End Class


    Private mDevices As New List(Of _device)
    Private mProperties As New List(Of _property)


    Public Sub Load_Devices()
        If Not IO.File.Exists(_file_devices) Then
            MsgBox("file '" & _file_devices & "' not found!")
            Exit Sub
        End If

        Try
            Using sr As New IO.StreamReader(_file_devices)
                Dim line As String
                Dim sDevice As String()

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    sDevice = line.Split(",")

                    mDevices.Add(New _device(sDevice(0), sDevice(1)))
                End While
            End Using
        Catch ex As Exception
            MsgBox("error: " & ex.Message)
        End Try

        lblDevices.Text = "Devices: " & mDevices.Count.ToString
    End Sub

    Public Sub Load_Properties()
        If Not IO.File.Exists(_file_properties) Then
            MsgBox("file '" & _file_properties & "' not found!")
            Exit Sub
        End If

        Try
            Using sr As New IO.StreamReader(_file_properties)
                Dim line As String
                Dim sProperty As String()

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    sProperty = line.Split(",")

                    mProperties.Add(New _property(sProperty(0), sProperty(1), sProperty(2), sProperty(3)))
                End While
            End Using
        Catch ex As Exception
            MsgBox("error: " & ex.Message)
        End Try

        lblProperties.Text = "Properties: " & mProperties.Count.ToString
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        Load_Devices()
        Load_Properties()

        btnProcess.Enabled = True
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        btnProcess.Enabled = False


        Dim tmpString As String

        tmpString = My.Resources.devices
        Try
            IO.File.WriteAllText(_file_devices, tmpString)
        Catch ex As Exception
            MsgBox("error: " & ex.Message)
        End Try

        tmpString = My.Resources.properties
        Try
            IO.File.WriteAllText(_file_properties, tmpString)
        Catch ex As Exception
            MsgBox("error: " & ex.Message)
        End Try
    End Sub

    Private Sub btnProcess_Click(sender As Object, e As EventArgs) Handles btnProcess.Click
        btnLoad.Enabled = False
        btnProcess.Enabled = False

        bgWorker_Process.RunWorkerAsync()
    End Sub

    Private Sub bgWorker_Process_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker_Process.DoWork
        Dim bFound As Boolean = False

        Dim oDevices As New List(Of _device)

        For Each _device In mDevices
            bFound = False
            For Each _property In mProperties
                Dim _current_device As _device = _device

                If _property.NavisionCode.Length >= 5 Then
                    If _device.Name.IndexOf(_property.NavisionCode) = 2 Then
                        bFound = True
                    End If
                    'If _device.Name.Contains(_property.NavisionCode) Then
                    '    bFound = True
                    'End If
                End If

                If Not bFound Then
                    If _device.Name.IndexOf(_property.OVCCCode) = 2 Then
                        If _device.Name.Contains(_property.OVCCCode) Then
                            bFound = True
                        End If
                    End If
                End If

                If bFound Then
                    _current_device.NewPropertyId = _property.Id

                    If _current_device.NewPropertyId <> _current_device.CurrentPropertyId Then
                        LogToProgressTextBox(_device.ToString)
                        LogToProgressTextBox(vbCrLf)
                    End If

                    Exit For
                Else
                    _current_device.NewPropertyId = -1
                End If

                oDevices.Add(_current_device)
            Next
        Next

        mDevices.Clear()
        mDevices.AddRange(oDevices)
    End Sub

    Private Sub bgWorker_Process_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgWorker_Process.RunWorkerCompleted
        btnLoad.Enabled = True

        MsgBox("done")
    End Sub
End Class
