﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        btnLoad = New Button()
        txtProgress = New TextBox()
        lblDevices = New Label()
        lblProperties = New Label()
        btnProcess = New Button()
        bgWorker_Process = New ComponentModel.BackgroundWorker()
        SuspendLayout()
        ' 
        ' btnLoad
        ' 
        btnLoad.Location = New Point(12, 12)
        btnLoad.Name = "btnLoad"
        btnLoad.Size = New Size(181, 75)
        btnLoad.TabIndex = 0
        btnLoad.Text = "Load"
        btnLoad.UseVisualStyleBackColor = True
        ' 
        ' txtProgress
        ' 
        txtProgress.Location = New Point(12, 102)
        txtProgress.Multiline = True
        txtProgress.Name = "txtProgress"
        txtProgress.ScrollBars = ScrollBars.Vertical
        txtProgress.Size = New Size(776, 336)
        txtProgress.TabIndex = 1
        ' 
        ' lblDevices
        ' 
        lblDevices.Location = New Point(199, 12)
        lblDevices.Name = "lblDevices"
        lblDevices.Size = New Size(121, 16)
        lblDevices.TabIndex = 2
        lblDevices.Text = "Devices: 0"
        ' 
        ' lblProperties
        ' 
        lblProperties.Location = New Point(199, 28)
        lblProperties.Name = "lblProperties"
        lblProperties.Size = New Size(121, 16)
        lblProperties.TabIndex = 3
        lblProperties.Text = "Properties: 0"
        ' 
        ' btnProcess
        ' 
        btnProcess.Enabled = False
        btnProcess.Location = New Point(607, 12)
        btnProcess.Name = "btnProcess"
        btnProcess.Size = New Size(181, 75)
        btnProcess.TabIndex = 4
        btnProcess.Text = "Process"
        btnProcess.UseVisualStyleBackColor = True
        ' 
        ' bgWorker_Process
        ' 
        bgWorker_Process.WorkerReportsProgress = True
        bgWorker_Process.WorkerSupportsCancellation = True
        ' 
        ' frmMain
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(800, 450)
        Controls.Add(btnProcess)
        Controls.Add(lblProperties)
        Controls.Add(lblDevices)
        Controls.Add(txtProgress)
        Controls.Add(btnLoad)
        FormBorderStyle = FormBorderStyle.FixedSingle
        Name = "frmMain"
        Text = "Form1"
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents btnLoad As Button
    Friend WithEvents txtProgress As TextBox
    Friend WithEvents lblDevices As Label
    Friend WithEvents lblProperties As Label
    Friend WithEvents btnProcess As Button
    Friend WithEvents bgWorker_Process As System.ComponentModel.BackgroundWorker

End Class
