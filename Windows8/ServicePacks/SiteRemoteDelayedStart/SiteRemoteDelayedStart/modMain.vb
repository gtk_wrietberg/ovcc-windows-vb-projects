﻿Module modMain
    Private ReadOnly cSITEKIOSK_ServiceName As String = "SiteRemote Client"

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()


        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        '_SiteKioskServiceConfiguration()
        Helpers.Logger.Write("Changing start type for SiteKiosk SiteRemote service ('" & cSITEKIOSK_ServiceName & "')", , 0)
        _SiteKioskServiceConfiguration()


        'Done
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("ok, bye", , 1)
    End Sub

    Private Sub _SiteKioskServiceConfiguration()
        Dim _ret As String = "", _startupType As ServiceBootFlag = ServiceBootFlag.UNDEFINED

        Helpers.Logger.Write("current startup type", , 1)

        _ret = ServiceInstaller.GetStartupType(cSITEKIOSK_ServiceName, _startupType)

        If _ret.Equals("") Then
            Helpers.Logger.Write(_startupType.ToString, , 2)
        Else
            Helpers.Logger.Write("error", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.Write(_ret, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If


        Helpers.Logger.Write("setting startup type", , 1)
        Helpers.Logger.Write("DelayedAutoStart", , 2)

        _ret = ServiceInstaller.ChangeStartupType(cSITEKIOSK_ServiceName, ServiceBootFlag.DelayedAutoStart)

        If _ret.Equals("") Then
            Helpers.Logger.Write("ok", , 3)
        Else
            Helpers.Logger.Write("error", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.Write(_ret, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End If
    End Sub
End Module
