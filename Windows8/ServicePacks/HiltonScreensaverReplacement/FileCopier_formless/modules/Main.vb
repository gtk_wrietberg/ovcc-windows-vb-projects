Module Main
    Private ReadOnly mScreensaverUrl As String = "file://C:\Program Files (x86)\SiteKiosk\Skins\Public\Startpages\Hilton\screensaver\index.html"

    Public Sub Main()
        InitGlobals()


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.LogFilePath = g_LogFileDirectory

        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        '------------------------------------------------------
        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles = New Helpers.FilesAndFolders.CopyFiles With {
            .BackupDirectory = g_BackupDirectory,
            .SourceDirectory = "files",
            .DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder(True)
        }
        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        Helpers.Logger.Write("Updating config", , 0)
        SkCfg_Update()


        '------------------------------------------------------
        Dim iSkKillCount As Integer = 0
        Helpers.Logger.Write("Killing SiteKiosk", , 0)
        iSkKillCount = Helpers.SiteKiosk.Restart()
        If iSkKillCount = 1 Then
            Helpers.Logger.Write("killed 1 instance", , 1)
        Else
            Helpers.Logger.Write("killed " & iSkKillCount & " instances", , 1)
        End If


        '------------------------------------------------------
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("bye", , 1)
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_Screensaver As Xml.XmlNode
            Dim mXmlNode_ScreensaverUrl As Xml.XmlNode

            Dim mConfigChanged As Boolean = False

            Dim mSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()


            mSkCfgXml = New Xml.XmlDocument

            Helpers.Logger.WriteMessageRelative(mSkCfgFile, 2)
            Helpers.Logger.WriteMessageRelative("loading", 2)
            mSkCfgXml.Load(mSkCfgFile)
            Helpers.Logger.WriteMessageRelative("ok", 3)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            Helpers.Logger.WriteMessageRelative("root node", 2)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                Helpers.Logger.WriteMessageRelative("ok", 3)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            Helpers.Logger.WriteMessageRelative("screensaver node", 2)
            mXmlNode_Screensaver = mXmlNode_Root.SelectSingleNode("sk:screensaver", ns)
            If Not mXmlNode_Screensaver Is Nothing Then
                Helpers.Logger.WriteMessageRelative("ok", 3)
            Else
                Throw New Exception("mXmlNode_Screensaver Is Nothing")
            End If


            Helpers.Logger.WriteMessageRelative("screensaver url node", 2)
            mXmlNode_ScreensaverUrl = mXmlNode_Screensaver.SelectSingleNode("sk:url", ns)
            If Not mXmlNode_ScreensaverUrl Is Nothing Then
                Dim sUrl As String = mXmlNode_ScreensaverUrl.InnerText

                Helpers.Logger.WriteMessageRelative("value", 3)
                Helpers.Logger.WriteMessageRelative(sUrl, 4)

                If sUrl.Contains("Hilton") Then
                    Helpers.Logger.WriteMessageRelative("Hilton detected", 3)

                    If sUrl.EndsWith("Hilton\screensaver\index.html") Then
                        ' no change needed
                        Helpers.Logger.WriteMessageRelative("no change needed", 4)
                    Else
                        Helpers.Logger.WriteMessageRelative("change needed", 4)
                        Helpers.Logger.WriteMessageRelative(mScreensaverUrl, 5)
                        mXmlNode_ScreensaverUrl.InnerText = mScreensaverUrl
                        mConfigChanged = True
                    End If
                End If
            Else
                Throw New Exception("mXmlNode_ScreensaverUrl Is Nothing")
            End If


            Helpers.Logger.WriteMessageRelative("saving", 2)
            If mConfigChanged Then
                Helpers.Logger.WriteMessageRelative("creating backup", 3)
                Helpers.FilesAndFolders.Backup.File(mSkCfgFile, 3)

                Helpers.Logger.WriteMessageRelative("saving existing file", 3)
                mSkCfgXml.Save(mSkCfgFile)
                Helpers.Logger.WriteMessageRelative("ok", 4)
            Else
                Helpers.Logger.WriteMessageRelative("no change, saving skipped", 3)
            End If

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("FAIL", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            Return False
        End Try
    End Function
End Module

