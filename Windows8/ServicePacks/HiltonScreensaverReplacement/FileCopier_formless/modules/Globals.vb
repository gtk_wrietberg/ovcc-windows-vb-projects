Module Globals
    Public g_LogFileDirectory As String
    Public g_BackupDirectory As String

    Public g_TESTMODE As Boolean


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString

        g_BackupDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(g_BackupDirectory)
    End Sub


End Module
