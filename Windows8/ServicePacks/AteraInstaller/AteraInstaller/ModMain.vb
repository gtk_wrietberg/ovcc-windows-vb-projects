﻿Module ModMain

    Private ReadOnly Atera_CmdLine As String = "msiexec.exe"
    Private ReadOnly Atera_Params As String = "/i ""http://HelpdeskSupport256443563.servicedesk.atera.com/GetAgent/Msi/?customerId=1&integratorLogin=james.young%40guest-tek.com"" /qn"

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("started", , 0)

        Helpers.Logger.WriteMessage("Starting installer", 0)
        Helpers.Logger.WriteMessage(Atera_CmdLine & " " & Atera_Params, 1)


        If Helpers.Processes.StartProcess(Atera_CmdLine, Atera_Params, True) Then
            Helpers.Logger.WriteMessage("ok", 2)
        Else
            Helpers.Logger.WriteError("fail!", 2)
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(), 3)
        End If

        Helpers.Logger.WriteMessage("bye", 0)
    End Sub
End Module
