﻿Imports System.IO
Imports System.Net
Imports System.Threading

Module ModMain
    Private WithEvents oProcess As ProcessRunner

    Public Sub Main()
        Application.DoEvents()

        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("started", 1)


        Helpers.Logger.WriteMessage("Is '" & InstallerGlobals.Ninja.Install.Service_Name & "' service installed?", 0)
        If ServiceInstaller.ServiceIsInstalled(InstallerGlobals.Ninja.Install.Service_Name) And False Then
            Helpers.Logger.WriteWarning("yes", 1)
            ExitCode.SetValue(ExitCode.ExitCodes.SERVICE_ALREADY_INSTALLED)
            Functions.ExitApplication()
            Exit Sub
        Else
            Helpers.Logger.WriteMessage("nope", 1)
        End If


        Helpers.Logger.WriteMessage("Copying Ninja installer", 0)
        Try
            Dim dInfo As New IO.DirectoryInfo(Environment.CurrentDirectory) 'System.AppDomain.CurrentDomain.BaseDirectory()
            Dim aFiles As IO.FileInfo() = dInfo.GetFiles("*-windows-installer.msi")

            Helpers.Logger.WriteMessage("looking in folder", 1)
            Helpers.Logger.WriteMessage(Environment.CurrentDirectory, 2)

            If aFiles.Count > 0 Then
                'Grab the first one
                Helpers.Logger.WriteMessage("using installer", 1)
                Helpers.Logger.WriteMessage(aFiles(0).FullName, 2)

                If Not IO.Directory.Exists(IO.Path.GetDirectoryName(InstallerGlobals.Ninja.Install.SETUP_PATH)) Then
                    Helpers.Logger.WriteMessage("creating destination directory", 1)
                    Helpers.Logger.WriteMessage(IO.Path.GetDirectoryName(InstallerGlobals.Ninja.Install.SETUP_PATH), 2)

                    IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(InstallerGlobals.Ninja.Install.SETUP_PATH))
                End If

                If IO.File.Exists(InstallerGlobals.Ninja.Install.SETUP_PATH) Then
                    Helpers.Logger.WriteMessage("found existing installer", 1)
                    Helpers.Logger.WriteMessage(InstallerGlobals.Ninja.Install.SETUP_PATH, 2)

                    Helpers.Logger.WriteMessage("removing", 3)
                    IO.File.Delete(InstallerGlobals.Ninja.Install.SETUP_PATH)
                End If


                Helpers.Logger.WriteMessage("copying", 1)
                Helpers.Logger.WriteMessage("from", 2)
                Helpers.Logger.WriteMessage(aFiles(0).FullName, 3)

                Helpers.Logger.WriteMessage("to", 2)
                Helpers.Logger.WriteMessage(InstallerGlobals.Ninja.Install.SETUP_PATH, 3)

                IO.File.Copy(aFiles(0).FullName, InstallerGlobals.Ninja.Install.SETUP_PATH)
            Else
                Helpers.Logger.WriteError("no installer found", 1)

                Helpers.Logger.WriteMessage("using internal installer", 1)

                If Not IO.Directory.Exists(IO.Path.GetDirectoryName(InstallerGlobals.Ninja.Install.SETUP_PATH)) Then
                    Helpers.Logger.WriteMessage("creating destination directory", 1)
                    Helpers.Logger.WriteMessage(IO.Path.GetDirectoryName(InstallerGlobals.Ninja.Install.SETUP_PATH), 2)

                    IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(InstallerGlobals.Ninja.Install.SETUP_PATH))
                End If

                Helpers.Logger.WriteMessage("copying", 1)
                Helpers.Logger.WriteMessage("from", 2)
                Helpers.Logger.WriteMessage("My.Resources.NinjaMSI", 3)

                Helpers.Logger.WriteMessage("to", 2)
                Helpers.Logger.WriteMessage(InstallerGlobals.Ninja.Install.SETUP_PATH, 3)

                File.WriteAllBytes(InstallerGlobals.Ninja.Install.SETUP_PATH, My.Resources.NinjaMSI)
            End If

            Main__Continued()
        Catch ex As Exception
            Helpers.Logger.WriteError("failed", 2)
            Helpers.Logger.WriteError(ex.Message, 3)

            ExitCode.SetValue(ExitCode.ExitCodes.INSTALLER_NOT_COPIED)
            Functions.ExitApplication()
            Exit Sub
        End Try
    End Sub

    Private Sub Main__Continued()
        'Firewall stuff
        Main__Firewall_disable()


        'Start Ninja installer
        InstallNinja()
    End Sub

    Public Sub Main__Firewall_disable()
        Helpers.Logger.WriteMessage("Manipulate firewall", 0)


        Helpers.Logger.WriteMessage("Check and save state", 1)

        Helpers.Logger.WriteMessage("Standard/Private", 2)
        InstallerGlobals.Ninja.Firewall.IsStandardOn = Functions.Firewall.GetState("standard")
        Helpers.Logger.WriteMessage(InstallerGlobals.Ninja.Firewall.IsStandardOn, 3)

        Helpers.Logger.WriteMessage("Domain", 2)
        InstallerGlobals.Ninja.Firewall.IsDomainOn = Functions.Firewall.GetState("domain")
        Helpers.Logger.WriteMessage(InstallerGlobals.Ninja.Firewall.IsDomainOn, 3)

        Helpers.Logger.WriteMessage("Public", 2)
        InstallerGlobals.Ninja.Firewall.IsPublicOn = Functions.Firewall.GetState("public")
        Helpers.Logger.WriteMessage(InstallerGlobals.Ninja.Firewall.IsPublicOn, 3)


        Helpers.Logger.WriteMessage("Temporarily disable firewall", 1)

        Helpers.Logger.WriteMessage("Standard/Private", 2)
        If InstallerGlobals.Ninja.Firewall.IsStandardOn Then
            Functions.NetSH(InstallerGlobals.Ninja.Firewall.NetSH_Arguments__DisableFirewall_Standard)
        Else
            Helpers.Logger.WriteMessage("already off", 2)
        End If

        Helpers.Logger.WriteMessage("Domain", 2)
        If InstallerGlobals.Ninja.Firewall.IsDomainOn Then
            Functions.NetSH(InstallerGlobals.Ninja.Firewall.NetSH_Arguments__DisableFirewall_Domain)
        Else
            Helpers.Logger.WriteMessage("already off", 2)
        End If

        Helpers.Logger.WriteMessage("Public", 2)
        If InstallerGlobals.Ninja.Firewall.IsPublicOn Then
            Functions.NetSH(InstallerGlobals.Ninja.Firewall.NetSH_Arguments__DisableFirewall_Public)
        Else
            Helpers.Logger.WriteMessage("already off", 2)
        End If
    End Sub

    Public Sub Main__Firewall_enable()
        Helpers.Logger.WriteMessage("Enable firewall again", 1)

        Helpers.Logger.WriteMessage("Standard/Private", 2)
        If InstallerGlobals.Ninja.Firewall.IsStandardOn Then
            Functions.NetSH(InstallerGlobals.Ninja.Firewall.NetSH_Arguments__EnableFirewall_Standard)
        Else
            Helpers.Logger.WriteMessage("was off before installer", 2)
        End If

        Helpers.Logger.WriteMessage("Domain", 2)
        If InstallerGlobals.Ninja.Firewall.IsDomainOn Then
            Functions.NetSH(InstallerGlobals.Ninja.Firewall.NetSH_Arguments__EnableFirewall_Domain)
        Else
            Helpers.Logger.WriteMessage("was off before installer", 2)
        End If

        Helpers.Logger.WriteMessage("Public", 2)
        If InstallerGlobals.Ninja.Firewall.IsPublicOn Then
            Functions.NetSH(InstallerGlobals.Ninja.Firewall.NetSH_Arguments__EnableFirewall_Public)
        Else
            Helpers.Logger.WriteMessage("was off before installer", 2)
        End If


        'done
        Functions.ExitApplication()
    End Sub

#Region "Ninja installation"
    Private Sub InstallNinja()
        Helpers.Logger.WriteMessage("installing Ninja", 0)

        oProcess = New ProcessRunner
        oProcess.FileName = InstallerGlobals.Ninja.Install.SETUP_NAME
        oProcess.Arguments = InstallerGlobals.Ninja.Install.SETUP_PARAMETERS
        oProcess.MaxTimeout = 1200
        'oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub InstallNinja_Started(_p As Process, _t As Double) Handles oProcess.Started
        Helpers.Logger.WriteMessage("started", 1)
    End Sub

    Private Sub InstallNinja_Done(_p As Process, _t As Double) Handles oProcess.Done
        Helpers.Logger.WriteMessage("done", 1)

        Main__Firewall_enable()
    End Sub

    Private Sub InstallNinja_Failed(_p As Process, _t As Double, _m As String) Handles oProcess.Failed
        Helpers.Logger.WriteError("failed", 1)
        Helpers.Logger.WriteError(_m, 2)

        ExitCode.SetValue(ExitCode.ExitCodes.NINJA_INSTALLER_FAILED)

        Main__Firewall_enable()
    End Sub

    Private Sub InstallNinja_Timeout(_p As Process, _t As Double) Handles oProcess.TimeOut
        Helpers.Logger.WriteError("timed out", 1)

        ExitCode.SetValue(ExitCode.ExitCodes.NINJA_INSTALLER_TIMEOUT)
    End Sub

    Private Sub InstallNinja_KillFailed(_p As Process, _m As String) Handles oProcess.KillFailed
        Helpers.Logger.WriteError("could not be killed", 1)

        ExitCode.SetValue(ExitCode.ExitCodes.NINJA_INSTALLER_ZOMBIE)
    End Sub
#End Region
End Module
