﻿Public Class ExitCode
    Public Enum ExitCodes As Integer
        OK = 0
        SERVICE_ALREADY_INSTALLED = 1
        INSTALLER_NOT_FOUND = 2
        INSTALLER_NOT_COPIED = 4
        INTERNAL_INSTALLER_NOT_FOUND = 8
        NINJA_INSTALLER_FAILED = 16
        NINJA_INSTALLER_TIMEOUT = 32
        NINJA_INSTALLER_ZOMBIE = 64
        NOT_SET = 262144
    End Enum

    Private Shared mExitCode As Integer = ExitCodes.NOT_SET

    Public Shared Function GetValue() As Integer
        Return mExitCode
    End Function

    Public Shared Sub SetValue(value As ExitCodes)
        _SetValue(value, False)
    End Sub

    Public Shared Sub SetValueExclusive(value As ExitCodes)
        _SetValue(value, True)
    End Sub

    Private Shared Sub _SetValue(value As ExitCodes, Optional Exclusive As Boolean = False)
        If Exclusive Then
            mExitCode = value
        Else
            If Contains(ExitCodes.NOT_SET) Then
                mExitCode = ExitCodes.OK
            End If

            mExitCode = mExitCode Or value
        End If
    End Sub

    Public Shared Sub UnsetValue(value As ExitCodes)
        If (mExitCode And value) Then
            mExitCode = mExitCode Xor value
        End If
    End Sub

    Public Shared Function Contains(value As ExitCodes) As Boolean
        Return (mExitCode And value)
    End Function

    Public Shared Function IsValue(value As ExitCodes) As Boolean
        Return (mExitCode = value)
    End Function

    Public Overloads Shared Function ToString() As String
        Dim sRet As String = ""

        sRet = mExitCode.ToString

        If Contains(ExitCodes.NOT_SET) Then
            sRet = sRet & " (" & ExitCodes.NOT_SET & "=" & System.Enum.GetName(GetType(ExitCodes), ExitCodes.NOT_SET) & ")"
        ElseIf IsValue(ExitCodes.OK) Then
            sRet = sRet & " (" & ExitCodes.OK & "=" & System.Enum.GetName(GetType(ExitCodes), ExitCodes.OK) & ")"
        Else
            Dim lTmp As New List(Of String)
            Dim aCodes As Array
            aCodes = System.Enum.GetValues(GetType(ExitCodes))

            For Each iCode As Integer In aCodes
                If mExitCode And iCode Then
                    lTmp.Add(iCode.ToString & "=" & System.Enum.GetName(GetType(ExitCodes), iCode))
                End If
            Next

            sRet = sRet & " (" & String.Join(" + ", lTmp) & ")"
        End If

        Return sRet
    End Function
End Class
