﻿Imports System.Net
Imports System.Text.RegularExpressions

Public Class Functions
    Public Shared Function ProgramFiles() As String
        Dim _s As String = ""

        _s = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
        If _s = "" Then
            _s = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        Return _s
    End Function

    Public Shared Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

    Public Shared Function IsIpAddress(sIp As String) As Boolean
        Dim rE As String = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
        Dim r As New Regex(rE)

        Return Not r.Match(sIp).Value.Equals("")
    End Function

    Public Shared Sub NetSH(Arguments As String)
        Helpers.Logger.WriteMessageRelative("Running command", 1)
        Helpers.Logger.WriteMessageRelative("netsh " & Arguments, 2)

        If Helpers.Processes.StartProcess("cmd", "/c netsh " & Arguments, True) Then
            Helpers.Logger.WriteMessageRelative("ok", 3)
        Else
            Helpers.Logger.WriteErrorRelative("fail!", 3)
            Helpers.Logger.WriteErrorRelative(Helpers.Errors.GetLast(), 4)
        End If
    End Sub

    Public Class Firewall
        Public Shared Function GetState(PolicyName As String) As Boolean
            Helpers.Logger.WriteMessageRelative("Check Firewall state for policy '" & PolicyName & "'", 1)

            PolicyName = PolicyName.ToLower

            Dim retValue As String = ""
            Dim regKey As String = ""

            Select Case PolicyName
                Case "standard"
                    regKey = InstallerGlobals.Ninja.Registry.FirewallPolicy_Standard
                Case "domain"
                    regKey = InstallerGlobals.Ninja.Registry.FirewallPolicy_Domain
                Case "public"
                    regKey = InstallerGlobals.Ninja.Registry.FirewallPolicy_Public
                Case Else
                    Helpers.Logger.WriteErrorRelative("unknown policy name", 2)
                    Return False
            End Select

            Helpers.Logger.WriteMessageRelative("Looking in registry", 2)
            Helpers.Logger.WriteMessageRelative(regKey, 3)

            retValue = Helpers.Registry.GetValue_String(regKey, InstallerGlobals.Ninja.Registry.FirewallPolicy_ValueName, "")

            Helpers.Logger.WriteMessageRelative("Value", 2)
            Helpers.Logger.WriteMessageRelative(InstallerGlobals.Ninja.Registry.FirewallPolicy_ValueName, 3)
            Helpers.Logger.WriteMessageRelative(retValue, 4)

            Return retValue.Equals("1")
        End Function
    End Class

    Public Class Download
        Public Shared Function FileExists(fileUrl As String) As Boolean
            Dim filesize As Long = 0

            Try
                Dim request As WebRequest

                request = WebRequest.Create(fileUrl)
                request.Method = "HEAD"


                Dim response = request.GetResponse()

                filesize = response.ContentLength
                response.Close()
            Catch ex As Exception
                Return False
            End Try

            Return (filesize > 0)
        End Function
    End Class
End Class
