﻿Public Class InstallerGlobals
    Public Class Installer
        Public Shared ReadOnly BACKUP_FOLDER As String = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_backups", My.Application.Info.ProductName, My.Application.Info.Version.ToString, Date.Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))
    End Class

    Public Class Ninja
        Public Class Install
            Public Shared ReadOnly Service_Name As String = "NinjaRMMAgent"

            Public Shared ReadOnly SETUP_PATH As String = IO.Path.Combine(Functions.ProgramFiles, "GuestTek\_NinjaSetup\ninja.msi")
            Public Shared ReadOnly SETUP_NAME As String = "msiexec"
            Public Shared ReadOnly SETUP_PARAMETERS As String = "/i """ & SETUP_PATH & """ /quiet /norestart"
        End Class

        Public Class Firewall
            Public Shared IsStandardOn As Boolean = False
            Public Shared IsDomainOn As Boolean = False
            Public Shared IsPublicOn As Boolean = False


            Public Shared ReadOnly NetSH_Arguments__EnableFirewall_Standard As String = "advfirewall set privateprofile state on"
            Public Shared ReadOnly NetSH_Arguments__DisableFirewall_Standard As String = "advfirewall set privateprofile state off"

            Public Shared ReadOnly NetSH_Arguments__EnableFirewall_Domain As String = "advfirewall set domainprofile state on"
            Public Shared ReadOnly NetSH_Arguments__DisableFirewall_Domain As String = "advfirewall set domainprofile state off"

            Public Shared ReadOnly NetSH_Arguments__EnableFirewall_Public As String = "advfirewall set publicprofile state on"
            Public Shared ReadOnly NetSH_Arguments__DisableFirewall_Public As String = "advfirewall set publicprofile state off"
        End Class

        Public Class Registry
            Public Shared ReadOnly FirewallPolicy_Standard As String = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile"
            Public Shared ReadOnly FirewallPolicy_Domain As String = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\DomainProfile"
            Public Shared ReadOnly FirewallPolicy_Public As String = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\PublicProfile"

            Public Shared ReadOnly FirewallPolicy_ValueName As String = "EnableFirewall"
        End Class
    End Class
End Class
