Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        'InitGlobals()
        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


        Helpers.Logger.Debugging = False
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)


        Try
            Dim sHTC_Files As String() = IO.Directory.GetFiles("C:\Program Files (x86)\SiteKiosk\Skins\Public\Startpages", "*.htc", IO.SearchOption.AllDirectories)

            For Each sHTC_File As String In sHTC_Files
                Helpers.Logger.WriteMessage("file", 0)
                Helpers.Logger.WriteMessage(sHTC_File, 1)

                Try
                    Helpers.Logger.WriteMessage("renaming", 2)
                    Helpers.Logger.WriteMessage(sHTC_File & ".WTF", 3)

                    IO.File.Move(sHTC_File, sHTC_File & ".WTF")
                    Helpers.Logger.WriteMessage("ok", 4)
                Catch ex As Exception
                    ExitCode.SetValue(ExitCode.ExitCodes.RENAMING_FAILED)

                    Helpers.Logger.WriteMessage("failed", 4)
                    Helpers.Logger.WriteMessage(ex.Message, 5)
                End Try
            Next
        Catch ex As Exception
            ExitCode.SetValue(ExitCode.ExitCodes.FAILED_TO_GET_FILES)

            Helpers.Logger.WriteMessage("FAIL", 1)
            Helpers.Logger.WriteMessage(ex.Message, 2)
        End Try


        ExitApplication()
    End Sub

    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module

