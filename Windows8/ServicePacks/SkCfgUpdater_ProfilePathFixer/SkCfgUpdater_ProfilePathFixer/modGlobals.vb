Module modGlobals
    Public g_SiteKioskUsername As String = "SiteKiosk"
    Public g_SiteKioskProfilePath As String = "C:\Users\SiteKiosk"
    Public g_SiteKioskDownloadFolder As String = ""
    Public g_List_FoldersCleaned As New List(Of String)
    Public g_List_FoldersVisible As New List(Of String)

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        'g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & Application.ProductName
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        'IO.Directory.CreateDirectory(g_BackupDirectory)
    End Sub


    Public Sub GetSiteKioskProfilePath()
        Dim sId As String = ""
        Dim sProfileDir As String = ""

        Helpers.Logger.Write("looking for profile folder for '" & g_SiteKioskUsername & "' user", , 0)
        Helpers.Logger.Write("user", , 1)
        Helpers.Logger.Write(g_SiteKioskUsername, , 2)

        Helpers.Logger.Write("sid", , 1)
        If Not Helpers.WindowsUser.ConvertUsernameToSid(g_SiteKioskUsername, sId) Then
            Helpers.Logger.Write("nothing found", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            Helpers.Logger.Write(sId, , 2)
        End If

        Helpers.Logger.Write("profile path", , 1)
        If Not Helpers.WindowsUser.GetProfileDirectory(g_SiteKioskUsername, sProfileDir) Then
            Helpers.Logger.Write("nothing found", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            Helpers.Logger.Write(sProfileDir, , 2)
        End If

        If Not sProfileDir.Equals("") Then
            g_SiteKioskProfilePath = sProfileDir
        Else
            Helpers.Logger.Write("using default", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 2)
            Helpers.Logger.Write(g_SiteKioskProfilePath, Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 3)
        End If

        g_SiteKioskDownloadFolder = sProfileDir & "\Downloads"
    End Sub
End Module
