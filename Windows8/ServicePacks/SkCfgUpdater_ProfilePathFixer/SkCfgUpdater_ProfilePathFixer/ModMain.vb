Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()


        Helpers.Logger.Debugging = False
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)


        GetSiteKioskProfilePath()


        'Folders to add to clean queue
        g_List_FoldersCleaned.Add(g_SiteKioskProfilePath & "\Downloads")
        g_List_FoldersCleaned.Add(g_SiteKioskProfilePath & "\AppData\Roaming\Skype")
        g_List_FoldersCleaned.Add(g_SiteKioskProfilePath & "\Documents")
        g_List_FoldersCleaned.Add(g_SiteKioskProfilePath & "\Desktop")
        g_List_FoldersCleaned.Add(g_SiteKioskProfilePath & "\Pictures")

        g_List_FoldersVisible.Add(g_SiteKioskProfilePath & "\Documents")
        g_List_FoldersVisible.Add(g_SiteKioskProfilePath & "\Desktop")




        Helpers.Logger.Write("searching for SkCfg file", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        Helpers.Logger.Write("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            Helpers.Logger.Write("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            Helpers.Logger.Write("ok", , 2)
        End If

        Helpers.Logger.Write("adding the following cleaned folders", , 0)
        For Each sFolder As String In g_List_FoldersCleaned
            Helpers.Logger.Write(sFolder, , 1)
        Next


        Helpers.Logger.Write("patching", , 0)
        SkCfg_Update()

        Bye()
    End Sub

    Private Sub Bye()
        Helpers.Logger.Write("Bye", , 0)
        Helpers.Logger.Write(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_DownloadManager As Xml.XmlNode
            Dim mXmlNode_DownloadFolder As Xml.XmlNode
            Dim mXmlNode_DeleteDirectories As Xml.XmlNode
            Dim mXmlNode_Dirs As Xml.XmlNodeList
            Dim mXmlNode_Dir As Xml.XmlNode
            Dim mXmlNode_NewDir As Xml.XmlNode

            Dim mXmlNode_FileManager As Xml.XmlNode
            Dim mXmlNode_VisiblePaths As Xml.XmlNode
            Dim mXmlNode_Urls As Xml.XmlNodeList
            Dim mXmlNode_Url As Xml.XmlNode
            Dim mXmlNode_NewUrl As Xml.XmlNode


            mSkCfgXml = New Xml.XmlDocument

            Helpers.Logger.Write("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            Helpers.Logger.Write("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            Helpers.Logger.Write("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                Helpers.Logger.Write("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            Helpers.Logger.Write("download-manager node", , 1)
            mXmlNode_DownloadManager = mXmlNode_Root.SelectSingleNode("sk:download-manager", ns)
            If Not mXmlNode_DownloadManager Is Nothing Then
                Helpers.Logger.Write("ok", , 2)
            Else
                Throw New Exception("mXmlNode_DownloadManager Is Nothing")
            End If


            Helpers.Logger.Write("download-folder node", , 1)
            mXmlNode_DownloadFolder = mXmlNode_DownloadManager.SelectSingleNode("sk:download-folder", ns)
            If Not mXmlNode_DownloadFolder Is Nothing Then
                Helpers.Logger.Write("currently", , 2)
                Helpers.Logger.Write(mXmlNode_DownloadFolder.InnerText, , 3)

                If mXmlNode_DownloadFolder.InnerText.Equals(g_SiteKioskDownloadFolder) Then
                    Helpers.Logger.Write("ok", , 4)
                Else
                    Helpers.Logger.Write("changed to", , 2)
                    mXmlNode_DownloadFolder.InnerText = g_SiteKioskDownloadFolder
                    Helpers.Logger.Write(mXmlNode_DownloadFolder.InnerText, , 3)

                    mConfigChanged = True
                End If
            Else
                Throw New Exception("mXmlNode_DownloadFolder Is Nothing")
            End If


            Helpers.Logger.Write("delete-directories node", , 1)
            mXmlNode_DeleteDirectories = mXmlNode_DownloadManager.SelectSingleNode("sk:delete-directories", ns)
            If Not mXmlNode_DeleteDirectories Is Nothing Then
                Helpers.Logger.Write("ok", , 2)
            Else
                Throw New Exception("mXmlNode_DeleteDirectories Is Nothing")
            End If

            Helpers.Logger.Write("dir nodelist", , 1)
            mXmlNode_Dirs = mXmlNode_DeleteDirectories.SelectNodes("sk:dir", ns)
            If Not mXmlNode_Dirs Is Nothing Then
                Helpers.Logger.Write("ok", , 2)

                Helpers.Logger.Write("checking for duplicates", , 1)
                For Each mXmlNode_Dir In mXmlNode_Dirs
                    If g_List_FoldersCleaned.Contains(mXmlNode_Dir.InnerText) Then
                        Helpers.Logger.Write("already there: " & mXmlNode_Dir.InnerText, , 2)
                        g_List_FoldersCleaned.Remove(mXmlNode_Dir.InnerText)
                    End If
                Next
            Else
                Helpers.Logger.Write("empty", , 2)
                'Throw New Exception("mXmlNode_Dirs Is Nothing")
            End If

            If g_List_FoldersCleaned.Count > 0 Then
                If g_List_FoldersCleaned.Count > 1 Then
                    Helpers.Logger.Write("adding " & g_List_FoldersCleaned.Count & " folders", , 1)
                Else
                    Helpers.Logger.Write("adding 1 folder", , 1)
                End If


                For Each sFolder As String In g_List_FoldersCleaned
                    Helpers.Logger.Write(sFolder, , 2)

                    mXmlNode_NewDir = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "dir", ns.LookupNamespace("sk"))
                    mXmlNode_NewDir.InnerText = sFolder

                    mXmlNode_DeleteDirectories.AppendChild(mXmlNode_NewDir)
                Next

                mConfigChanged = True
            Else
                Helpers.Logger.Write("no folders to add", , 1)
            End If


            Helpers.Logger.Write("filemanager node", , 1)
            mXmlNode_FileManager = mXmlNode_Root.SelectSingleNode("sk:filemanager", ns)
            If Not mXmlNode_FileManager Is Nothing Then
                Helpers.Logger.Write("ok", , 2)
            Else
                Throw New Exception("mXmlNode_FileManager Is Nothing")
            End If

            Helpers.Logger.Write("visible-paths node", , 1)
            mXmlNode_VisiblePaths = mXmlNode_FileManager.SelectSingleNode("sk:visible-paths", ns)
            If Not mXmlNode_VisiblePaths Is Nothing Then
                Helpers.Logger.Write("ok", , 2)
            Else
                Throw New Exception("mXmlNode_VisiblePaths Is Nothing")
            End If

            Helpers.Logger.Write("url nodelist", , 1)
            mXmlNode_Urls = mXmlNode_VisiblePaths.SelectNodes("sk:url", ns)
            If Not mXmlNode_Urls Is Nothing Then
                Helpers.Logger.Write("ok", , 2)

                Helpers.Logger.Write("checking for duplicates", , 1)
                For Each mXmlNode_Url In mXmlNode_Urls
                    If g_List_FoldersVisible.Contains(mXmlNode_Url.InnerText) Then
                        Helpers.Logger.Write("already there: " & mXmlNode_Url.InnerText, , 2)
                        g_List_FoldersVisible.Remove(mXmlNode_Url.InnerText)
                    End If
                Next
            Else
                Helpers.Logger.Write("empty", , 2)
                'Throw New Exception("mXmlNode_Urls Is Nothing")
            End If


            If g_List_FoldersVisible.Count > 0 Then
                If g_List_FoldersVisible.Count > 1 Then
                    Helpers.Logger.Write("adding " & g_List_FoldersVisible.Count & " folders", , 1)
                Else
                    Helpers.Logger.Write("adding 1 folder", , 1)
                End If


                For Each sFolder As String In g_List_FoldersVisible
                    Helpers.Logger.Write(sFolder, , 2)

                    mXmlNode_NewUrl = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
                    mXmlNode_NewUrl.InnerText = sFolder

                    mXmlNode_VisiblePaths.AppendChild(mXmlNode_NewUrl)
                Next

                mConfigChanged = True
            Else
                Helpers.Logger.Write("no folders to add", , 1)
            End If


            Helpers.Logger.Write("saving", , 0)
            If mConfigChanged Then
                Helpers.Logger.Write("creating backup of '" & mSkCfgFile & "'", , 1)
                BackupFile(New IO.FileInfo(mSkCfgFile))

                Helpers.Logger.Write("saving existing file", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                Helpers.Logger.Write("ok", , 2)
            Else
                Helpers.Logger.Write("skipped, nothing was changed", , 1)
            End If

            Return True
        Catch ex As Exception
            Helpers.Logger.Write("FAIL", , 2)
            Helpers.Logger.Write(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            Helpers.Logger.WriteRelative("writing: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                Helpers.Logger.WriteRelative("done", , 2)
                bRet = True
            Else
                Helpers.Logger.WriteRelative("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteRelative("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Helpers.Logger.WriteRelative("ex.Message=" & ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

