﻿Public Class Constants
    Public Class Communication
        Public Shared ReadOnly UserAgentString As String = "OVCCThemeInventory"

        Public Shared ReadOnly ServerUrl As String = "https://guesttek.wouterrietberg.nl/theme/index.php"
    End Class

    Public Class SkCfg
        Public Shared ReadOnly LogoutUrl As String = "/browserbar/logout-navigation/url"
        Public Shared ReadOnly ThemeUrl As String = "/startpageconfig/startpage"
        Public Shared ReadOnly ScreensaverUrl As String = "/screensaver/url"

        Public Shared ReadOnly Applications As String = "/programs/file"
        Public Shared ReadOnly Applications_Attrib_filename As String = "filename"
    End Class
End Class
