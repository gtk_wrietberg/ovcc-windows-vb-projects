﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Net
Imports System.Text

Public Class JSON
    Private mServerUrl As String
    Public Property ServerUrl() As String
        Get
            Return mServerUrl
        End Get
        Set(ByVal value As String)
            mServerUrl = value
        End Set
    End Property

    Public Sub New()
        mServerUrl = ""
    End Sub

    Public Sub New(ServerUrl As String)
        mServerUrl = ServerUrl
    End Sub


    Public Request As New JObject
    Public RawResponse As New JObject

    Public LastError As String = ""

    Public Function Send() As Boolean
        Dim webClient As New WebClient()
        Dim resByte As Byte()
        Dim resString As String = ""
        Dim reqString() As Byte

        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12
        'ServicePointManager.ServerCertificateValidationCallback = AddressOf ValidateRemoteCertificate

        Try
            Helpers.Logger.WriteDebugRelative("sending request to: " & mServerUrl, 1)

            webClient.Headers("content-type") = "application/json"
            webClient.Headers.Add("User-Agent: " & Constants.Communication.UserAgentString)
            reqString = Encoding.Default.GetBytes(JsonConvert.SerializeObject(Me.Request, Formatting.Indented))
            resByte = webClient.UploadData(mServerUrl, "post", reqString)
            resString = Encoding.Default.GetString(resByte)
            Me.RawResponse = JObject.Parse(resString)

            webClient.Dispose()

            Me.LastError = ""

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("Sending request to '" & mServerUrl & "' failed!", 1)
            Helpers.Logger.WriteErrorRelative("raw response", 2)
            Helpers.Logger.WriteErrorRelative(resString, 3)
            Helpers.Logger.WriteErrorRelative("exception", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            Me.LastError = ex.Message
        End Try

        Return False
    End Function
End Class