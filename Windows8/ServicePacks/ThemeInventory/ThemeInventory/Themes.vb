﻿Public Class Themes
    Private Class _Theme
        Public Sub New(Name As String, Index As Integer)
            mName = Name
            mIndex = Index
        End Sub

        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mIndex As Integer
        Public Property Index() As Integer
            Get
                Return mIndex
            End Get
            Set(ByVal value As Integer)
                mIndex = value
            End Set
        End Property
    End Class


    Private Shared mThemes As New List(Of _Theme)

    Public Shared Sub Add(iIndex As Integer, sName As String)
        mThemes.Add(New _Theme(sName, iIndex))
    End Sub

    Public Shared Function Exists(iIndex As Integer) As Boolean
        Return [Get](iIndex) > -1
    End Function

    Public Shared Function Exists(sName As String) As Boolean
        Return Not [Get](sName).Equals("")
    End Function

    Public Shared Function [Get](iIndex As Integer) As String
        For Each _t As _Theme In mThemes
            If _t.Index = iIndex Then
                Return _t.Name
            End If
        Next

        Return ""
    End Function

    Public Shared Function [Get](sName As String) As Integer
        For Each _t As _Theme In mThemes
            If _t.Name.ToLower.Equals(sName.ToLower) Then
                Return _t.Index
            End If
        Next

        Return -1
    End Function
End Class
