﻿Imports System.Runtime.InteropServices
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Module modMain
    Private Class _theme_stuff
        <JsonProperty("full_path")>
        Public Property Path() As String

        <JsonProperty("html")>
        Public Property HtmlFile() As String

        <JsonProperty("name_directory")>
        Public Property NameDirectory() As String

        <JsonProperty("name_html")>
        Public Property NameHtml() As String

        <JsonProperty("internal_name")>
        Public Property InternalName() As String

        <JsonProperty("internal_index")>
        Public Property InternalIndex() As Integer

        <JsonProperty("guesttek_logo_date")>
        Public Property GuesttekLogoDate() As String

        <JsonProperty("problem")>
        Public Property Problem() As Integer
    End Class

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)

        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


#Region "existing themes"
        Themes.Add(0, "Hilton - Standard")
        Themes.Add(1, "Hilton - Garden Inn")
        Themes.Add(2, "Hilton - Hampton")
        Themes.Add(3, "Hilton - Waldord Astoria")
        Themes.Add(4, "Hilton - Conrad")
        Themes.Add(5, "Hilton - DoubleTree")
        Themes.Add(6, "Hilton - Canopy")
        Themes.Add(7, "Hilton - Curio")
        Themes.Add(8, "Hilton - Embassy Suites")
        Themes.Add(9, "Hilton - Homewood Suites")
        Themes.Add(10, "Hilton - Home2")
        Themes.Add(11, "Hilton - Hilton Grand Vacations")
        Themes.Add(12, "Hilton - Tru")
        Themes.Add(13, "Hilton - Tapestry Collection")
        Themes.Add(14, "Hilton - LXR")
        Themes.Add(15, "Hilton - Motto")
        Themes.Add(16, "Hilton - Signia")
        Themes.Add(17, "Hilton - Tempo")
        Themes.Add(666, "iBahn")
        Themes.Add(667, "GuestTek")
        Themes.Add(668, "ResidenceInn")
        Themes.Add(670, "Towneplace")
        Themes.Add(671, "HotelArts")
        Themes.Add(672, "Radisson - Country Inn and Suites")
        Themes.Add(673, "Fairfield Inn")
        Themes.Add(674, "Ritz-Carlton - Abu Dhabi")
        Themes.Add(700, "Marriott - Generic")
        Themes.Add(701, "Marriott - AC-Hotels")
        Themes.Add(702, "Marriott - Autograph")
        Themes.Add(703, "Marriott - Courtyard")
        Themes.Add(704, "Marriott - Fairfield")
        Themes.Add(705, "Marriott - Fourpoints")
        Themes.Add(706, "Marriott - JW-Marriott")
        Themes.Add(707, "Marriott - LuxuryCollection")
        Themes.Add(708, "Marriott - Meridien")
        Themes.Add(709, "Marriott - Renaissance")
        Themes.Add(710, "Marriott - Residence-Inn")
        Themes.Add(711, "Marriott - Ritz-Carlton")
        Themes.Add(712, "Marriott - Sheraton")
        Themes.Add(713, "Marriott - Springhill-Suites")
        Themes.Add(714, "Marriott - StRegis")
        Themes.Add(715, "Marriott - Towneplace-Suites")
        Themes.Add(716, "Marriott - Westin")
#End Region


        Dim oTheme As New _theme_stuff
        Dim JSON_ThemeInventory As New JSON()

        JSON_ThemeInventory.Request.Add("machine_name", Environment.MachineName)


        Helpers.SiteKiosk.SkCfg.XML.Lock()
        Helpers.SiteKiosk.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
        Helpers.SiteKiosk.SkCfg.XML.Load()

        Dim sCurrentThemeFullPath As String = "", sCurrentThemeIndex As String = "", sCurrentTheme As String = ""
        Helpers.SiteKiosk.SkCfg.XML.GetValue(Constants.SkCfg.ThemeUrl, sCurrentThemeFullPath)
        Helpers.SiteKiosk.SkCfg.XML.Unlock()

        sCurrentThemeFullPath = Helpers.SiteKiosk.TranslateSiteKioskPathToNormalPath(sCurrentThemeFullPath)
        sCurrentThemeIndex = IO.Path.GetFileName(sCurrentThemeFullPath)

        oTheme.Path = sCurrentThemeFullPath.Replace("\", "/")
        oTheme.HtmlFile = sCurrentThemeIndex
        oTheme.Problem = 0


        If sCurrentThemeIndex.StartsWith("index_") Then
            oTheme.NameHtml = sCurrentThemeIndex.Replace("index_", "").Replace(".html", "")
        Else
            oTheme.NameHtml = ""
        End If


        sCurrentTheme = Helpers.FilesAndFolders.Folder.GetTopDirectory(sCurrentThemeFullPath)
        oTheme.NameDirectory = sCurrentTheme


        If IO.File.Exists(sCurrentThemeFullPath) Then
            Dim oLocalSettings As New Helpers.SiteKiosk.iBahnScript.LocalSettings
            Helpers.SiteKiosk.iBahnScript.ScriptFile = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "Skins\Public\iBAHN\Scripts\iBAHN_functions.js")
            Helpers.SiteKiosk.iBahnScript.Load(oLocalSettings)

            oTheme.InternalIndex = oLocalSettings.ThemeIndex
            oTheme.InternalName = Themes.Get(oLocalSettings.ThemeIndex)
        Else
            oTheme.InternalIndex = -1
            oTheme.InternalName = ""

            ExitCode.SetValue(ExitCode.ExitCodes.THEME_NOT_FOUND)
        End If

        If (oTheme.NameHtml.Equals("") And oTheme.InternalName.Equals("")) Or oTheme.InternalIndex = -1 Then
            oTheme.Problem = 1
        End If

        If oTheme.Path.ToLower.Contains("guest-tek") Or oTheme.Path.ToLower.Contains("guesttek") Then
            oTheme.GuesttekLogoDate = GetGuesttekThemeLogoTimestamp().ToString("yyyy-MM-dd")
        Else
            oTheme.GuesttekLogoDate = ""
        End If


        JSON_ThemeInventory.Request.Add("theme", JObject.FromObject(oTheme))
        JSON_ThemeInventory.ServerUrl = Constants.Communication.ServerUrl


        If Not JSON_ThemeInventory.Send() Then
            ExitCode.SetValue(ExitCode.ExitCodes.SEND_FAILED)
        Else
            Helpers.Logger.WriteMessage("Server reponse:", 0)
            Helpers.Logger.WriteWithoutDate(JSON_ThemeInventory.RawResponse.ToString)
        End If


        ExitApplication()
    End Sub

    Private Function GetGuesttekThemeLogoTimestamp() As DateTime
        Dim sFile As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "Skins\Public\Startpages\Guest-tek\scripts\siteaddress.js")
        Dim sLogo As String = ""
        Dim dDate As DateTime = DateTime.MinValue


        If Not IO.File.Exists(sFile) Then
            Return dDate
        End If


        'find logo
        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                'default:logo = "img/logo/logo_guesttek.png";break;

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("default:logo") Then
                        sLogo = Text.RegularExpressions.Regex.Replace(line, ".*""(.+)"".*", "$1")
                        sLogo = sLogo.Replace("/", "\")
                        sLogo = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "Skins\Public\Startpages\Guest-tek", sLogo)

                        Exit While
                    End If
                End While
            End Using

            If Not IO.File.Exists(sLogo) Then
                Return dDate
            End If


            dDate = IO.File.GetLastWriteTime(sLogo)
        Catch ex As Exception

        End Try


        Return dDate
    End Function


    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteEmptyLine()
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
