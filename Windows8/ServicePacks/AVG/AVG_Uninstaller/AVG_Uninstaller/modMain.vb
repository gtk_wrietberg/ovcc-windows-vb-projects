﻿Module modMain
    Private ReadOnly c_Timeout As Integer = 1000 * 60 * 15 ' 15 minutes
    Private ReadOnly c_AVG_Root As String = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\AVG"
    Private ReadOnly c_AVG_exe As String = "avgmfapx.exe"
    Private ReadOnly c_AVG_Params As String = "/Appmode=Setup /uninstall /uilevel=Silent /dontrestart"

    Public Sub Main()
        ExitCode.SetValue(ExitCode.ExitCodes.OK)


        InitGlobals()


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.LogFilePath = g_LogFileDirectory

        Helpers.Logger.WriteMessage(New String("*", 50))
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)

        Helpers.Logger.Debugging = False


        '---------------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Start AVG uninstall", 0)

        Helpers.Logger.WriteMessage("Find '" & c_AVG_exe & "' in '" & c_AVG_Root & "'", 1)

        If Not IO.Directory.Exists(c_AVG_Root) Then
            Helpers.Logger.WriteError("Folder '" & c_AVG_Root & "' does not exist!", 2)
            ExitCode.SetValue(ExitCode.ExitCodes.FOLDER_NOT_FOUND)

            ExitApplication()
        End If


        Dim lFiles As New List(Of String)

        lFiles = Helpers.FilesAndFolders.File.FindRecursive(c_AVG_Root, c_AVG_exe)

        If lFiles.Count = 1 Then
            Helpers.Logger.WriteMessage("found 1 file", 2)
        Else
            Helpers.Logger.WriteMessage("found " & lFiles.Count & " files", 2)
        End If

        If lFiles.Count = 0 Then
            'uh oh
            Helpers.Logger.WriteError("File not found!", 2)

            ExitCode.SetValue(ExitCode.ExitCodes.FILE_NOT_FOUND)

            ExitApplication()
        End If


        Dim sFile_AVG As String = ""

        If lFiles.Count = 1 Then
            sFile_AVG = lFiles.Item(0)
        Else
            Helpers.Logger.WriteMessage("finding most recent", 3)

            Dim dDate As Date, dLast As Date = New Date(1970, 1, 1)

            For i As Integer = 0 To lFiles.Count - 1
                dDate = IO.File.GetLastWriteTime(lFiles.Item(i))

                If dDate > dLast Then
                    dLast = dDate
                    sFile_AVG = lFiles.Item(i)
                End If
            Next
        End If

        Helpers.Logger.WriteMessage("using", 2)
        Helpers.Logger.WriteMessage(sFile_AVG, 3)

        Helpers.Logger.WriteMessage("start uninstall", 2)
        Helpers.Logger.WriteMessage("""" & sFile_AVG & """ " & c_AVG_Params, 3)


        Dim sWatch As New Stopwatch, bProcOk As Boolean

        sWatch.Start()
        bProcOk = Helpers.Processes.StartProcess("""" & sFile_AVG & """", c_AVG_Params, True, c_Timeout)
        sWatch.Stop()

        Helpers.Logger.WriteError("time elapsed: " & sWatch.ElapsedMilliseconds & "ms", 4)

        If Not bProcOk Then
            Helpers.Logger.WriteError("fail", 4)
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 5)

            ExitCode.SetValue(ExitCode.ExitCodes.UNINSTALL_FAILED)
        Else
            Helpers.Logger.WriteMessage("ok", 4)

            ExitCode.SetValue(ExitCode.ExitCodes.OK)
        End If


        ''---------------------------------------------------------------------------------------------------------------------
        'Helpers.Logger.WriteMessage("Start AVG Web TuneUp uninstall", 0)

        'Dim sFile_AVGWebTuneUp As String = ""

        'sFile_AVGWebTuneUp = Helpers.Windows.Programs.ProductGUID.GetUninstallIdentifier("AVG Web TuneUp")


        '---------------------------------------------------------------------------------------------------------------------
        ExitApplication()
    End Sub

    Private Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

End Module
