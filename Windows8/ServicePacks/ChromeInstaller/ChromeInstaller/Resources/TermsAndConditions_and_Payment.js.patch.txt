//CHROME phase 3 shortcut
function TermsAndConditions__ExecuteAction_url(sUrl) {
	var iAppId=SiteKiosk.ScriptDispatch.Applications__GetId("chrome");
	if(iAppId>-1) {
		var sAppPath=SiteKiosk.ExternalApps.Item(iAppId).Path;

		sAppPath=sAppPath.replace(/\\/g,"/");
		sAppPath="\"" + sAppPath + "\" " + sUrl;

		TermsAndConditions__LogMessage("Opening chrome: "+sAppPath);

		SiteKiosk.ExternalApps.Run(sAppPath,false);
	} else {
		TermsAndConditions__LogMessage("Opening (chrome not found): "+sUrl);

		_TermsAndConditions_NewWindow=SiteKiosk.SiteKioskUI.CreateBrowserWindow();
		_TermsAndConditions_NewWindow.Move((screen.width-screen.width*0.75)/2,(screen.height-screen.height*0.75)/2,screen.width*0.75,screen.height*0.75);
		_TermsAndConditions_NewWindow.SiteKioskWebBrowser.Navigate(sUrl,false);
		_TermsAndConditions_NewWindow.Show();
		_TermsAndConditions_NewWindow.SetFocus();
	}
}

//OLD
function TermsAndConditions__ExecuteAction_url__DEPRECATED(sUrl) {