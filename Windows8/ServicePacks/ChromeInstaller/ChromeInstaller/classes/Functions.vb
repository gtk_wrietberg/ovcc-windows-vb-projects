﻿Imports System.Management

Public Class Functions
    Public Shared Function FindSiteKioskUserProfileFolder(ByRef SiteKioskUserProfile As String) As Boolean
        Dim c_SiteKioskUsername As String = "sitekiosk"
        Dim sProfileDir As String = ""
        Dim bRet As Boolean = False

        bRet = Helpers.WindowsUser.GetProfileDirectory(c_SiteKioskUsername, sProfileDir)

        SiteKioskUserProfile = sProfileDir

        If SiteKioskUserProfile.Length < 10 Then
            bRet = False
        End If

        Return bRet
    End Function

    Public Shared Function FindChromeExecutable(ByRef ChromeExecutablePath As String) As Boolean
        Dim _tmp_path As String = ""

        ChromeExecutablePath = ""

        Try
            _tmp_path = Helpers.Registry.GetValue_String(InstallerGlobals.Chrome.Registry.CHROME_APP_PATH, "", "")

            If _tmp_path.Equals("") Then
                ExitCode.SetValue(ExitCode.ExitCodes.CHROME_NOT_FOUND_IN_REGISTRY)

                Throw New Exception("'chrome.exe' not found in registry")
            End If

            If Not IO.File.Exists(_tmp_path) Then
                ExitCode.SetValue(ExitCode.ExitCodes.CHROME_NOT_FOUND_IN_PATH)

                Throw New Exception("'chrome.exe' not found in path")
            End If

            ChromeExecutablePath = _tmp_path
        Catch ex As Exception
            ExitCode.SetValue(ExitCode.ExitCodes.ERROR)

            Helpers.Logger.WriteErrorRelative("error while trying to find 'chrome.exe'", 1)
            Helpers.Logger.WriteErrorRelative(ex.Message, 2)

            Return False
        End Try


        Return True
    End Function

    Public Shared Function ProgramFiles() As String
        Dim _s As String = ""

        _s = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
        If _s = "" Then
            _s = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        Return _s
    End Function

    Public Shared Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

    Public Shared Function patchScriptFile(FilePath As String, SearchString As String, ReplaceString As String) As Boolean
        Try
            Dim linesScript As New List(Of String)
            Dim bScriptChanged As Boolean = False


            Helpers.Logger.WriteMessageRelative(FilePath, 1)

            Using sr As New IO.StreamReader(FilePath)
                Dim line As String, bDone As Boolean = False

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("//CHROME phase 3 shortcut") Then
                        Helpers.Logger.WriteMessageRelative("already patched", 2)
                        Exit While
                    End If

                    If line.Contains(SearchString) Then
                        'found function, replacing
                        Helpers.Logger.WriteMessageRelative("found", 2)

                        Helpers.Logger.WriteMessageRelative("changing line", 3)
                        Helpers.Logger.WriteMessageRelative("from", 4)
                        Helpers.Logger.WriteMessageRelative(line, 5)


                        line = ReplaceString

                        Helpers.Logger.WriteMessageRelative("to", 4)
                        Helpers.Logger.WriteMessageRelative(line, 5)


                        bScriptChanged = True
                    End If

                    linesScript.Add(line)
                End While
            End Using

            If bScriptChanged Then
                Helpers.Logger.WriteMessageRelative("saving", 2)
                Helpers.Logger.WriteMessageRelative("backup", 3)
                Helpers.FilesAndFolders.Backup.File(FilePath, 3, My.Application.Info.ProductName)

                Helpers.Logger.WriteMessageRelative("writing", 3)
                Helpers.Logger.WriteMessageRelative(FilePath, 4)
                Using sw As New IO.StreamWriter(FilePath)
                    For Each line As String In linesScript
                        sw.WriteLine(line)
                    Next
                End Using

                Helpers.Logger.WriteMessageRelative("ok", 3)
            Else
                Helpers.Logger.WriteMessageRelative("no update needed", 2)
            End If

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteError("error", 1)
            Helpers.Logger.WriteError(ex.Message, 2)

            Return False
        End Try
    End Function
End Class
