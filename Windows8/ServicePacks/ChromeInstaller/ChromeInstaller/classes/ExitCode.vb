﻿Public Class ExitCode
    Public Enum ExitCodes As Integer
        OK = 0
        TEST_MODE = 1
        ALREADY_RUNNING = 2
        [ERROR] = 4
        INVALID_ACTION_VALUE = 8
        SITEKOSK_NOT_FOUND = 16
        SITEKIOSK_INCORRECT_VERSION = 32
        SITEKIOSK_PROFILE_NOT_FOUND = 64
        CHROME_NOT_FOUND_IN_REGISTRY = 128
        CHROME_NOT_FOUND_IN_PATH = 256
        CHROME_INSTALL_NOT_COPIED = 512
        CHROME_INSTALL_FAILED = 1024
        CHROME_INSTALL_TIMEOUT = 2048
        CHROME_INSTALL_ZOMBIE = 4096
        CHROME_WRAPPER_NOT_COPIED = 8192
        CONFIG_CHANGE_FAILED = 16384
        CONFIG_CHANGE_FAILED_DELETED_DIRS = 32768
        CONFIG_CHANGE_APP_LOGO_ICON = 65536
        CONFIG_CHANGE_THEME_ICONS = 131072
        NOT_SET = 262144
    End Enum

    Private Shared mExitCode As Integer = ExitCodes.NOT_SET

    Public Shared Function GetValue() As Integer
        Return mExitCode
    End Function

    Public Shared Sub SetValue(value As ExitCodes)
        _SetValue(value, False)
    End Sub

    Public Shared Sub SetValueExclusive(value As ExitCodes)
        _SetValue(value, True)
    End Sub

    Private Shared Sub _SetValue(value As ExitCodes, Optional Exclusive As Boolean = False)
        If Exclusive Then
            mExitCode = value
        Else
            If Contains(ExitCodes.NOT_SET) Then
                mExitCode = ExitCodes.OK
            End If

            mExitCode = mExitCode Or value
        End If
    End Sub

    Public Shared Sub UnsetValue(value As ExitCodes)
        If (mExitCode And value) Then
            mExitCode = mExitCode Xor value
        End If
    End Sub

    Public Shared Function Contains(value As ExitCodes) As Boolean
        Return (mExitCode And value)
    End Function

    Public Shared Function IsValue(value As ExitCodes) As Boolean
        Return (mExitCode = value)
    End Function

    Public Overloads Shared Function ToString() As String
        Dim sRet As String = ""

        sRet = mExitCode.ToString

        If Contains(ExitCodes.NOT_SET) Then
            sRet = sRet & " (" & ExitCodes.NOT_SET & "=" & System.Enum.GetName(GetType(ExitCodes), ExitCodes.NOT_SET) & ")"
        ElseIf IsValue(ExitCodes.OK) Then
            sRet = sRet & " (" & ExitCodes.OK & "=" & System.Enum.GetName(GetType(ExitCodes), ExitCodes.OK) & ")"
        Else
            Dim lTmp As New List(Of String)
            Dim aCodes As Array
            aCodes = System.Enum.GetValues(GetType(ExitCodes))

            For Each iCode As Integer In aCodes
                If mExitCode And iCode Then
                    lTmp.Add(iCode.ToString & "=" & System.Enum.GetName(GetType(ExitCodes), iCode))
                End If
            Next

            sRet = sRet & " (" & String.Join(" + ", lTmp) & ")"
        End If

        Return sRet
    End Function
End Class
