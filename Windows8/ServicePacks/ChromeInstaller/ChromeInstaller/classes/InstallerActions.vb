﻿Public Class InstallerActions
    Public Enum ACTIONS As Integer
        [NOTHING] = 0
        INVALID = 1
        INSTALL_CHROME = 2
        ADD_CHROME_TO_APPLICATIONS = 4
        REMOVE_FIREFOX_FROM_APPLICATIONS = 8
        REPLACE_STANDARD_SK_BROWSER_WITH_CHROME = 16
        UPDATE_ALL_THEMES = 32
        RESTART_SITEKIOSK_WHEN_DONE = 64
    End Enum

    Private Shared mAction As ACTIONS = ACTIONS.NOTHING

    Public Shared Sub Reset()
        mAction = ACTIONS.NOTHING
    End Sub

    Public Shared Sub [Set](value As ACTIONS)
        mAction = mAction Or value
    End Sub

    Public Shared Sub [Set](value As Integer)
        mAction = value
    End Sub

    Public Shared Sub Unset(value As ACTIONS)
        If (mAction And value) Then
            mAction = mAction Xor value
        End If
    End Sub

    Public Shared Function [Get]() As Integer
        Return mAction
    End Function

    Public Shared Function Contains(value As ACTIONS) As Boolean
        Return (mAction And value)
    End Function

    Public Shared Function IsValue(value As ACTIONS) As Boolean
        Return (mAction = value)
    End Function

    Public Shared Function IsValid() As Boolean
        Dim aActions As Array, total As Integer = 0

        aActions = System.Enum.GetValues(GetType(ACTIONS))
        For Each iAction As Integer In aActions
            total += iAction
        Next

        If mAction >= 0 And mAction <= total Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Overloads Shared Function ToString() As String
        Dim sRet As String = ""

        sRet = mAction.ToString

        If IsValue(ACTIONS.NOTHING) Then
            sRet = sRet & " (" & ACTIONS.NOTHING & "=" & System.Enum.GetName(GetType(ACTIONS), ACTIONS.NOTHING) & ")"
        Else
            Dim lTmp As New List(Of String)
            Dim aActions As Array

            aActions = System.Enum.GetValues(GetType(ACTIONS))

            For Each iAction As Integer In aActions
                If mAction And iAction Then
                    lTmp.Add(iAction.ToString & "=" & System.Enum.GetName(GetType(ACTIONS), iAction))
                End If
            Next

            sRet = sRet & " (" & String.Join(" + ", lTmp) & ")"
        End If

        Return sRet
    End Function
End Class
