﻿Public Class InstallerGlobals
    Public Class Installer
        Public Shared ReadOnly BACKUP_FOLDER As String = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_backups", My.Application.Info.ProductName, My.Application.Info.Version.ToString, Date.Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))
    End Class

    Public Class Chrome
        Public Class Install
            Public Shared ReadOnly CHROME_NAME As String = "Google Chrome"
            Public Shared ReadOnly CHROME_NAME_SHORT As String = "Chrome"

            Public Shared ReadOnly CHROME_SETUP_PATH As String = IO.Path.Combine(Functions.ProgramFiles, "GuestTek\_ChromeSetup\ChromeSetup.exe")
            Public Shared ReadOnly CHROME_SETUP_NAME As String = "ChromeSetup.exe"
            Public Shared ReadOnly CHROME_SETUP_PARAMETERS As String = "/silent /install"

            Public Shared ReadOnly CHROME_WRAPPER_NAME As String = "ChromeWrapper.exe"
            Public Shared ReadOnly CHROME_WRAPPER_PATH As String = IO.Path.Combine(Functions.ProgramFiles, "GuestTek\ChromeWrapper\ChromeWrapper.exe")
        End Class

        Public Class Run
            'Public Shared ReadOnly CHROME_PROFILE_DIRECTORY As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Google\Chrome\User Data")
            'Public Shared ReadOnly CHROME_PROFILE_DIRECTORY As String = "C:\Users\SiteKiosk\AppData\Local\Google\Chrome\User Data"
            Public Shared ReadOnly CHROME_PROFILE_DIRECTORY_TEMPLATE As String = "%%SITEKIOSK_USER_PROFILE_DIRECTORY%%\AppData\Local\Google\Chrome\User Data"
            Public Shared CHROME_PROFILE_DIRECTORY As String = ""

            Public Shared CHROME_FULL_PATH As String = ""
            Public Shared ReadOnly CHROME_PARAMETERS As String = "--user-data-dir=""%%CHROME_PROFILE_DIRECTORY%%"" --no-default-browser-check"

            Public Shared CHROME_PROCESS_NAME As String = "chrome"

            Public Shared ReadOnly CHROME_START_PAGE As String = "https://www.google.com"
        End Class

        Public Class Registry
            Public Shared ReadOnly CHROME_APP_PATH As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe"
        End Class
    End Class

    Public Class Firefox
        Public Class Install
            Public Shared ReadOnly FIREFOX_NAME As String = "Mozilla Firefox"
            Public Shared ReadOnly FIREFOX_NAME_SHORT As String = "Firefox"
            Public Shared ReadOnly FIREFOX_EXECUTABLE_NAME As String = "firefox.exe"
        End Class
    End Class

    Public Class Sitekiosk
        Public Shared ReadOnly SITEKIOSK_CHROME_WINDOWCLASS_NAME As String = "Chrome_WidgetWin_1"
        Public Shared Version As Integer = 0
        Public Shared NewFiles As Boolean = False
        Public Shared ReadOnly USER_PROFILE_DEFAULT As String = "C:\Users\SiteKiosk"
        Public Shared USER_PROFILE As String = ""
    End Class
End Class
