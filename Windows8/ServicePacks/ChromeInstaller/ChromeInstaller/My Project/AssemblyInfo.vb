Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ChromeInstaller")>
<Assembly: AssemblyDescription("Installs Chrome and updates OVCC configuration")>
<Assembly: AssemblyCompany("GuestTek")>
<Assembly: AssemblyProduct("ChromeInstaller")>
<Assembly: AssemblyCopyright("Copyright ©  2020")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("76230f64-1ead-4924-b90f-c82a8e23c251")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.1.24151.1058")>
<Assembly: AssemblyFileVersion("1.1.24151.1058")>

<assembly: AssemblyInformationalVersion("0.0.24151.1058")>