﻿Imports System.Text.RegularExpressions

Module Main
    Private WithEvents oProcess As ProcessRunner

    Public Sub Main()
        Application.DoEvents()


        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("started", , 0)


        If Helpers.Processes.IsAppAlreadyRunning Then
            Helpers.Logger.WriteError("there's already an instance running, exiting", 0)
            ExitCode.SetValue(ExitCode.ExitCodes.ALREADY_RUNNING)
            Functions.ExitApplication()
            Exit Sub
        End If


        InstallerActions.Reset()
        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals("--restart-sitekiosk") Or arg.Equals("--kill-sitekiosk") Then
                InstallerActions.Set(InstallerActions.ACTIONS.RESTART_SITEKIOSK_WHEN_DONE)
            End If

            If arg.Equals("--install-chrome") Then
                InstallerActions.Set(InstallerActions.ACTIONS.INSTALL_CHROME)
            End If

            If arg.Equals("--add-to-applications") Then
                InstallerActions.Set(InstallerActions.ACTIONS.ADD_CHROME_TO_APPLICATIONS)
            End If

            If arg.Equals("--remove-firefox") Then
                InstallerActions.Set(InstallerActions.ACTIONS.REMOVE_FIREFOX_FROM_APPLICATIONS)
            End If

            If arg.Equals("--replace-sitekiosk-browser") Then
                InstallerActions.Set(InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME)
            End If

            If arg.Equals("--update-themes") Then
                InstallerActions.Set(InstallerActions.ACTIONS.UPDATE_ALL_THEMES)
            End If

            If arg.Equals("--do-everything") Then
                InstallerActions.Set(InstallerActions.ACTIONS.INSTALL_CHROME)
                InstallerActions.Set(InstallerActions.ACTIONS.ADD_CHROME_TO_APPLICATIONS)
                InstallerActions.Set(InstallerActions.ACTIONS.REMOVE_FIREFOX_FROM_APPLICATIONS)
                InstallerActions.Set(InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME)
                InstallerActions.Set(InstallerActions.ACTIONS.UPDATE_ALL_THEMES)
                InstallerActions.Set(InstallerActions.ACTIONS.RESTART_SITEKIOSK_WHEN_DONE)
            End If

            If arg.StartsWith("--installer-action:") Then
                Dim tmp_s As String = arg.Replace("--installer-action:", "")
                Dim tmp_i As Integer = -1

                If Integer.TryParse(tmp_s, tmp_i) Then
                    InstallerActions.Set(tmp_i)
                Else
                    InstallerActions.Set(InstallerActions.ACTIONS.INVALID)
                End If

                Exit For
            End If
        Next

        If Not InstallerActions.IsValid() Then
            InstallerActions.Set(InstallerActions.ACTIONS.INVALID)
        End If

        Helpers.Logger.WriteMessage("installer actions:", 0)
        Helpers.Logger.WriteMessage(InstallerActions.ToString(), 1)

        '--------------------------------------------------------------------------------------------------------------------------------------------
        If InstallerActions.Contains(InstallerActions.ACTIONS.INVALID) Then
            Helpers.Logger.WriteError("INVALID!!!!", 2)

            ExitCode.SetValue(ExitCode.ExitCodes.INVALID_ACTION_VALUE)
            Step7_Done()
        Else
            If InstallerActions.IsValue(InstallerActions.ACTIONS.NOTHING) Then
                Helpers.Logger.WriteError("EMPTY!!!!", 2)

                ExitCode.SetValue(ExitCode.ExitCodes.INVALID_ACTION_VALUE)
                Step7_Done()
            Else
                Step0_Initialise()
            End If
        End If
    End Sub

#Region "Step 0"
    Private Sub Step0_Initialise()
        Helpers.Logger.WriteMessage("checking SiteKiosk version", 0)
        Dim skVersion As Integer

        If Not Integer.TryParse(Helpers.SiteKiosk.GetSiteKioskVersion(True), skVersion) Then
            skVersion = 0
        End If

        Helpers.Logger.WriteMessage("found version " & skVersion, 1)

        If skVersion = 0 Then
            Helpers.Logger.WriteError("sitekiosk not found", 2)

            ExitCode.SetValue(ExitCode.ExitCodes.SITEKOSK_NOT_FOUND)

            Step7_Done()

            Exit Sub
        End If

        If skVersion < 7 Or skVersion > 9 Then
            Helpers.Logger.WriteError("incorrect sitekiosk version", 2)

            ExitCode.SetValue(ExitCode.ExitCodes.SITEKIOSK_INCORRECT_VERSION)

            Step7_Done()

            Exit Sub
        End If

        InstallerGlobals.Sitekiosk.Version = skVersion

        If skVersion <> 9 Then
            Helpers.Logger.WriteError("not sk9", 2)

            If InstallerActions.Contains(InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME) Or
                InstallerActions.Contains(InstallerActions.ACTIONS.UPDATE_ALL_THEMES) Then

                Helpers.Logger.WriteError("the following is skipped", 3)

                If InstallerActions.Contains(InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME) Then
                    Helpers.Logger.WriteError(InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME & "=" & System.Enum.GetName(GetType(InstallerActions.ACTIONS), InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME), 4)
                End If

                If InstallerActions.Contains(InstallerActions.ACTIONS.UPDATE_ALL_THEMES) Then
                    Helpers.Logger.WriteError(InstallerActions.ACTIONS.UPDATE_ALL_THEMES & "=" & System.Enum.GetName(GetType(InstallerActions.ACTIONS), InstallerActions.ACTIONS.UPDATE_ALL_THEMES), 4)
                End If
            End If

            InstallerActions.Unset(InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME)
            InstallerActions.Unset(InstallerActions.ACTIONS.UPDATE_ALL_THEMES)
        End If


        Helpers.Logger.WriteMessage("copying files", 0)

        Try
            Helpers.Logger.WriteMessage(InstallerGlobals.Chrome.Install.CHROME_WRAPPER_NAME, 1)
            Helpers.Logger.WriteMessage(InstallerGlobals.Chrome.Install.CHROME_WRAPPER_PATH, 2)

            IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(InstallerGlobals.Chrome.Install.CHROME_WRAPPER_PATH))
            IO.File.WriteAllBytes(InstallerGlobals.Chrome.Install.CHROME_WRAPPER_PATH, My.Resources.ChromeWrapper)
        Catch ex As Exception
            Helpers.Logger.WriteError("failed", 2)
            Helpers.Logger.WriteError(ex.Message, 3)

            ExitCode.SetValue(ExitCode.ExitCodes.CHROME_WRAPPER_NOT_COPIED)

            Step7_Done()

            Exit Sub
        End Try


        Try
            Helpers.Logger.WriteMessage(InstallerGlobals.Chrome.Install.CHROME_SETUP_NAME, 1)
            Helpers.Logger.WriteMessage(InstallerGlobals.Chrome.Install.CHROME_SETUP_PATH, 2)

            IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(InstallerGlobals.Chrome.Install.CHROME_SETUP_PATH))
            IO.File.WriteAllBytes(InstallerGlobals.Chrome.Install.CHROME_SETUP_PATH, My.Resources.ChromeSetup)
        Catch ex As Exception
            Helpers.Logger.WriteError("failed", 2)
            Helpers.Logger.WriteError(ex.Message, 3)

            ExitCode.SetValue(ExitCode.ExitCodes.CHROME_INSTALL_NOT_COPIED)

            Step7_Done()

            Exit Sub
        End Try


        Helpers.Logger.WriteMessage("finding sitekiosk user profile", 0)
        If Functions.FindSiteKioskUserProfileFolder(InstallerGlobals.Sitekiosk.USER_PROFILE) Then
            Helpers.Logger.WriteMessage("found in registry", 1)
            Helpers.Logger.WriteMessage(InstallerGlobals.Sitekiosk.USER_PROFILE, 2)
        Else
            Helpers.Logger.WriteWarning("not found in registry", 1)
            Helpers.Logger.WriteWarning("using default", 2)
            Helpers.Logger.WriteWarning(InstallerGlobals.Sitekiosk.USER_PROFILE_DEFAULT, 3)

            InstallerGlobals.Sitekiosk.USER_PROFILE = InstallerGlobals.Sitekiosk.USER_PROFILE_DEFAULT
        End If

        If IO.Directory.Exists(InstallerGlobals.Sitekiosk.USER_PROFILE) Then
            Helpers.Logger.WriteMessage("ok, path exists", 1)
        Else
            Helpers.Logger.WriteError("Path does not exist!", 1)
            Helpers.Logger.WriteMessage("Continuing anyway", 2)

            ExitCode.SetValue(ExitCode.ExitCodes.SITEKIOSK_PROFILE_NOT_FOUND)

            'Step7_Done()

            'Exit Sub
        End If


        '-------------------
        Step1_InstallChrome()
    End Sub
#End Region

#Region "Step 1"
    '================================================================================================================================================
    'Step 1
    Private Sub Step1_InstallChrome()
        Helpers.Logger.WriteMessage("installing chrome", 0)

        If InstallerActions.Contains(InstallerActions.ACTIONS.INSTALL_CHROME) Then
            oProcess = New ProcessRunner
            oProcess.FileName = InstallerGlobals.Chrome.Install.CHROME_SETUP_PATH
            oProcess.Arguments = InstallerGlobals.Chrome.Install.CHROME_SETUP_PARAMETERS
            oProcess.MaxTimeout = 1200
            'oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()
        Else
            Helpers.Logger.WriteMessage("skipped, not selected", 1)

            Step2_CheckInstall()
        End If
    End Sub

    Private Sub Step1_InstallChrome_Started(_p As Process, _t As Double) Handles oProcess.Started
        Helpers.Logger.WriteMessage("started", 1)
    End Sub

    Private Sub Step1_InstallChrome_Done(_p As Process, _t As Double) Handles oProcess.Done
        Helpers.Logger.WriteMessage("done", 1)

        Step2_CheckInstall()
    End Sub

    Private Sub Step1_InstallChrome_Failed(_p As Process, _t As Double, _m As String) Handles oProcess.Failed
        Helpers.Logger.WriteError("failed", 1)
        Helpers.Logger.WriteError(_m, 2)

        ExitCode.SetValue(ExitCode.ExitCodes.ERROR)
        ExitCode.SetValue(ExitCode.ExitCodes.CHROME_INSTALL_FAILED)

        Step2_CheckInstall()
    End Sub

    Private Sub Step1_InstallChrome_Timeout(_p As Process, _t As Double) Handles oProcess.TimeOut
        Helpers.Logger.WriteError("timed out", 1)

        ExitCode.SetValue(ExitCode.ExitCodes.ERROR)
        ExitCode.SetValue(ExitCode.ExitCodes.CHROME_INSTALL_TIMEOUT)
    End Sub

    Private Sub Step1_InstallChrome_KillFailed(_p As Process, _m As String) Handles oProcess.KillFailed
        Helpers.Logger.WriteError("could not be killed", 1)

        ExitCode.SetValue(ExitCode.ExitCodes.ERROR)
        ExitCode.SetValue(ExitCode.ExitCodes.CHROME_INSTALL_ZOMBIE)
    End Sub
#End Region

#Region "Step 2"
    '================================================================================================================================================
    'Step 2
    Private Sub Step2_CheckInstall()
        If ExitCode.Contains(ExitCode.ExitCodes.ERROR) Then
            Helpers.Logger.WriteError("fatal error")

            Functions.ExitApplication()
            Exit Sub
        End If

        Step3_CheckChrome()
    End Sub
#End Region

#Region "Step 3"
    '================================================================================================================================================
    'Step 3
    Private Sub Step3_CheckChrome()
        Helpers.Logger.WriteMessage("checking chrome", 0)
        If Functions.FindChromeExecutable(InstallerGlobals.Chrome.Run.CHROME_FULL_PATH) Then
            Helpers.Logger.WriteMessage("Chrome path", 1)
            Helpers.Logger.WriteMessage(InstallerGlobals.Chrome.Run.CHROME_FULL_PATH, 2)
        Else
            Functions.ExitApplication()

            Exit Sub
        End If

        '-------------------
        Step4_UpdateOVCC_Config()
    End Sub
#End Region

#Region "Step 4"
    '================================================================================================================================================
    'Step 4
    Private Sub Step4_UpdateOVCC_Config()
        Helpers.Logger.WriteMessage("updating OVCC config", 0)

        Try
            Helpers.Logger.WriteMessage("loading SK config file", 1)

            Dim sSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()
            Dim bConfigChanged As Boolean = False

            Helpers.Logger.WriteMessage(sSkCfgFile, 2)


            Dim nt As System.Xml.XmlNameTable
            Dim ns As System.Xml.XmlNamespaceManager
            Dim xmlSkCfg As System.Xml.XmlDocument
            Dim xmlNode_Root As System.Xml.XmlNode

            xmlSkCfg = New System.Xml.XmlDocument

            xmlSkCfg.Load(sSkCfgFile)
            Helpers.Logger.WriteMessage("ok", 3)

            nt = xmlSkCfg.NameTable
            ns = New System.Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

#Region "sk programs template"
            Dim mXmlNode_Template_Program__File As Xml.XmlNode
            Dim mXmlNode_Template_Program__Runas As Xml.XmlNode
            Dim mXmlNode_Template_Program__Startup As Xml.XmlNode
            Dim mXmlNode_Template_Program__tmp As Xml.XmlNode

            mXmlNode_Template_Program__File = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "file", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__Runas = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "runas", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__Startup = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "startup", ns.LookupNamespace("sk"))


            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Startup.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__Startup.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("enabled"))
            mXmlNode_Template_Program__Startup.Attributes.GetNamedItem("enabled").Value = "false"

            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Startup)


            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "username", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "domainname", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__Runas.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("enabled"))
            mXmlNode_Template_Program__Runas.Attributes.GetNamedItem("enabled").Value = "false"

            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Runas)


            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "additional-exe", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__tmp)


            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("autostart"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("autostart").Value = "false"

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("filename"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("filename").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("description"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("description").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("title"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("title").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("screenshot-path"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("screenshot-path").Value = ""
#End Region


            Dim mXmlNode_Programs As Xml.XmlNode
            Dim mXmlNode_Files As Xml.XmlNodeList
            Dim mXmlNode_File As Xml.XmlNode
            Dim mXmlNode_FileClone As Xml.XmlNode
            Dim sTmpAppTitle As String, sTmpAppDescription As String, bChromeAlreadyThere As Boolean = False

            mXmlNode_Programs = xmlNode_Root.SelectSingleNode("sk:programs", ns)
            mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)

            If InstallerActions.Contains(InstallerActions.ACTIONS.REMOVE_FIREFOX_FROM_APPLICATIONS) Then
                Helpers.Logger.WriteMessage("checking app list for " & InstallerGlobals.Firefox.Install.FIREFOX_NAME, 1)
                For Each mXmlNode_File In mXmlNode_Files
                    Try
                        sTmpAppTitle = mXmlNode_File.Attributes.GetNamedItem("title").Value.ToLower
                    Catch ex As Exception
                        sTmpAppTitle = ""
                    End Try
                    sTmpAppDescription = mXmlNode_File.Attributes.GetNamedItem("description").Value.ToLower

                    If sTmpAppTitle = InstallerGlobals.Firefox.Install.FIREFOX_NAME.ToLower Or
                        sTmpAppTitle = InstallerGlobals.Firefox.Install.FIREFOX_NAME_SHORT.ToLower Or
                        sTmpAppDescription = InstallerGlobals.Firefox.Install.FIREFOX_NAME.ToLower Or
                        sTmpAppDescription = InstallerGlobals.Firefox.Install.FIREFOX_NAME_SHORT.ToLower Then

                        'already there, let's delete it
                        Helpers.Logger.WriteMessage("'" & InstallerGlobals.Firefox.Install.FIREFOX_NAME & "' in list", 2)

                        Helpers.Logger.WriteMessage("deleting", 3)
                        mXmlNode_Programs.RemoveChild(mXmlNode_File)

                        bConfigChanged = True
                    End If
                Next
            End If

            If InstallerActions.Contains(InstallerActions.ACTIONS.ADD_CHROME_TO_APPLICATIONS) Then
                Helpers.Logger.WriteMessage("checking app list for '" & InstallerGlobals.Chrome.Install.CHROME_NAME & "'", 1)
                For Each mXmlNode_File In mXmlNode_Files
                    Try
                        sTmpAppTitle = mXmlNode_File.Attributes.GetNamedItem("title").Value.ToLower
                    Catch ex As Exception
                        sTmpAppTitle = ""
                    End Try
                    sTmpAppDescription = mXmlNode_File.Attributes.GetNamedItem("description").Value.ToLower

                    If sTmpAppTitle = InstallerGlobals.Chrome.Install.CHROME_NAME.ToLower Or
                        sTmpAppTitle = InstallerGlobals.Chrome.Install.CHROME_NAME_SHORT.ToLower Or
                        sTmpAppDescription = InstallerGlobals.Chrome.Install.CHROME_NAME.ToLower Or
                        sTmpAppDescription = InstallerGlobals.Chrome.Install.CHROME_NAME_SHORT.ToLower Then

                        'already there, let's delete it
                        Helpers.Logger.WriteMessage("'" & InstallerGlobals.Chrome.Install.CHROME_NAME & "' already in list", 2)

                        Helpers.Logger.WriteMessage("deleting", 3)
                        mXmlNode_Programs.RemoveChild(mXmlNode_File)

                        bConfigChanged = True
                    End If
                Next


                Helpers.Logger.WriteMessage("adding '" & InstallerGlobals.Chrome.Install.CHROME_NAME & "' to app list", 1)

                mXmlNode_FileClone = mXmlNode_Template_Program__File.CloneNode(True)
                If InstallerActions.Contains(InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME) Then
                    mXmlNode_FileClone.Attributes.GetNamedItem("filename").Value = """" & InstallerGlobals.Chrome.Install.CHROME_WRAPPER_PATH & """"
                Else
                    'We don't replace regular browsing, so we add Google.com to start item
                    mXmlNode_FileClone.Attributes.GetNamedItem("filename").Value = """" & InstallerGlobals.Chrome.Install.CHROME_WRAPPER_PATH & """ " & InstallerGlobals.Chrome.Run.CHROME_START_PAGE
                End If
                Try
                    mXmlNode_FileClone.Attributes.GetNamedItem("title").Value = InstallerGlobals.Chrome.Install.CHROME_NAME
                Catch ex As Exception
                    Helpers.Logger.WriteMessage("skipping app title (SK7?)", 3)
                End Try
                mXmlNode_FileClone.Attributes.GetNamedItem("description").Value = InstallerGlobals.Chrome.Install.CHROME_NAME
                mXmlNode_FileClone.SelectSingleNode("sk:additional-exe", ns).InnerText = InstallerGlobals.Chrome.Run.CHROME_FULL_PATH

                mXmlNode_Programs.PrependChild(mXmlNode_FileClone)

                Helpers.Logger.WriteMessage("ok", 3)

                bConfigChanged = True



                '---------------------------------------------------
                Dim mXmlNode_WindowManager As Xml.XmlNode
                Dim mXmlNode_Windows As Xml.XmlNodeList
                Dim mXmlNode_Window As Xml.XmlNode
                Dim sTmpClass As String, sTmpAction As String

                mXmlNode_WindowManager = xmlNode_Root.SelectSingleNode("sk:windowmanager", ns)
                mXmlNode_Windows = mXmlNode_WindowManager.SelectNodes("sk:window", ns)

                Helpers.Logger.WriteMessage("checking window list for " & InstallerGlobals.Sitekiosk.SITEKIOSK_CHROME_WINDOWCLASS_NAME, 2)

                For Each mXmlNode_Window In mXmlNode_Windows
                    sTmpClass = mXmlNode_Window.SelectSingleNode("sk:class", ns).InnerText

                    If sTmpClass = InstallerGlobals.Sitekiosk.SITEKIOSK_CHROME_WINDOWCLASS_NAME Then
                        Helpers.Logger.WriteMessage("ok", 3)

                        sTmpAction = mXmlNode_Window.SelectSingleNode("sk:action", ns).InnerText
                        Helpers.Logger.WriteMessage("action is '" & sTmpAction & "'", 3)

                        If sTmpAction = "2" Then
                            Helpers.Logger.WriteMessage("ok", 4)
                        Else
                            Helpers.Logger.WriteMessage("setting action to '2'", 4)
                            mXmlNode_Window.SelectSingleNode("sk:action", ns).InnerText = "2"
                            Helpers.Logger.WriteMessage("ok", 5)

                            bConfigChanged = True
                        End If
                    End If
                Next


                '---------------------------------------------------
                InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY = InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY_TEMPLATE.Replace("%%SITEKIOSK_USER_PROFILE_DIRECTORY%%", InstallerGlobals.Sitekiosk.USER_PROFILE)

                Try
                    Helpers.Logger.WriteMessage("checking cleaned folders for '" & InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY & "'", 2)

                    Dim mXmlNode_DownloadManager As Xml.XmlNode
                    Dim mXmlNode_DeleteDirectories As Xml.XmlNode
                    Dim mXmlNode_Dirs As Xml.XmlNodeList
                    Dim mXmlNode_Dir As Xml.XmlNode
                    Dim mXmlNode_NewDir As Xml.XmlNode

                    mXmlNode_DownloadManager = xmlNode_Root.SelectSingleNode("sk:download-manager", ns)
                    If mXmlNode_DownloadManager Is Nothing Then
                        Throw New Exception("mXmlNode_DownloadManager Is Nothing")
                    End If

                    mXmlNode_DeleteDirectories = mXmlNode_DownloadManager.SelectSingleNode("sk:delete-directories", ns)
                    If mXmlNode_DeleteDirectories Is Nothing Then
                        Throw New Exception("mXmlNode_DeleteDirectories Is Nothing")
                    End If

                    mXmlNode_Dirs = mXmlNode_DeleteDirectories.SelectNodes("sk:dir", ns)
                    Dim bDeleteDirAlreadyThere As Boolean = False
                    If Not mXmlNode_Dirs Is Nothing Then
                        For Each mXmlNode_Dir In mXmlNode_Dirs
                            If mXmlNode_Dir.InnerText.Equals(InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY) Then
                                bDeleteDirAlreadyThere = True
                            End If
                        Next
                    Else
                        Throw New Exception("mXmlNode_Dirs Is Nothing")
                    End If

                    If Not bDeleteDirAlreadyThere Then
                        mXmlNode_NewDir = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "dir", ns.LookupNamespace("sk"))
                        mXmlNode_NewDir.InnerText = InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY

                        mXmlNode_DeleteDirectories.AppendChild(mXmlNode_NewDir)
                    End If
                Catch ex As Exception
                    Helpers.Logger.WriteError("Error while adding folders to cleaned folders!!!", 2)
                    Helpers.Logger.WriteError(ex.Message, 3)

                    Throw New Exception("Error while adding folders to cleaned folders: " & ex.Message)
                End Try
            End If


            '---------------------------------------------------
            Helpers.Logger.WriteMessage("Allowing 'everyone' access to '" & InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY & "'", 2)
            Try
                Helpers.Logger.WriteMessage("creating folder", 3)
                IO.Directory.CreateDirectory(InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY)
                Helpers.Logger.WriteMessage("ok", 4)

                Helpers.Logger.WriteMessage("setting permissions", 3)
                If Not Helpers.FilesAndFolders.Permissions.FolderSecurity_REPLACEandPURGE__Full_Everyone(InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY) Then
                    Helpers.Logger.WriteError("failed", 4)
                    Helpers.Logger.WriteError(Helpers.Errors.GetLast(), 5)
                Else
                    Helpers.Logger.WriteMessage("ok", 4)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteError("failed", 3)
                Helpers.Logger.WriteError(ex.Message, 4)
            End Try


            '---------------------------------------------------
            Helpers.Logger.WriteMessage("saving", 2)
            If bConfigChanged Then
                Helpers.Logger.WriteMessage("backup", 3)
                Helpers.FilesAndFolders.Backup.File(sSkCfgFile, 0, My.Application.Info.ProductName)

                Helpers.Logger.WriteMessage("writing", 3)
                Helpers.Logger.WriteMessage(sSkCfgFile, 4)
                xmlSkCfg.Save(sSkCfgFile)
                Helpers.Logger.WriteMessage("ok", 5)
            Else
                Helpers.Logger.WriteWarning("skipped, nothing was changed", 3)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteError("error", 2)
            Helpers.Logger.WriteError(ex.Message, 3)

            ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_FAILED)

            Step7_Done()

            Exit Sub
        End Try

        '------
        Step5_UpdateOVCC_Files()
    End Sub
#End Region

#Region "Step 5"
    Private Sub Step5_UpdateOVCC_Files()
        Helpers.Logger.WriteMessage("updating OVCC files", 0)


        Helpers.Logger.WriteMessage("updating applications script file", 1)
        If InstallerActions.Contains(InstallerActions.ACTIONS.ADD_CHROME_TO_APPLICATIONS) Then
            Try
                Dim sApplicationsScript As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder, "Skins\Public\iBAHN\Scripts\internal\Applications.js")
                Dim lstApps As New List(Of String)
                Dim linesApplications As New List(Of String)
                Dim bApplicationsScriptChanged As Boolean = False

                Helpers.Logger.WriteMessage(sApplicationsScript, 2)
                Helpers.Logger.WriteMessage("looking for chrome", 3)

                Using sr As New IO.StreamReader(sApplicationsScript)
                    Dim line As String, bDone As Boolean = False

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.StartsWith("var _Applications__array=") And Not bDone Then
                            lstApps = line.Replace("var _Applications__array=", "").Replace("[", "").Replace("]", "").Replace(";", "").Split(",").ToList

                            If lstApps.Contains("""firefox""") Then
                                If InstallerActions.Contains(InstallerActions.ACTIONS.REMOVE_FIREFOX_FROM_APPLICATIONS) Then
                                    Helpers.Logger.WriteMessage("removing firefox", 4)
                                    lstApps.Remove("""firefox""")
                                End If
                            End If

                            If lstApps.Contains("""chrome""") Then
                                'ok
                                Helpers.Logger.WriteMessage("chrome already there", 4)
                            Else
                                Helpers.Logger.WriteMessage("adding chrome", 4)
                                lstApps.Add("""chrome""")

                                line = "var _Applications__array=[" & String.Join(",", lstApps.ToArray) & "];"

                                bApplicationsScriptChanged = True
                            End If

                            bDone = True
                        End If

                        linesApplications.Add(line)
                    End While
                End Using

                If bApplicationsScriptChanged Then
                    Helpers.Logger.WriteMessage("saving script file", 2)
                    Helpers.Logger.WriteMessage("backup", 3)
                    Helpers.FilesAndFolders.Backup.File(sApplicationsScript, 0, My.Application.Info.ProductName)

                    Helpers.Logger.WriteMessage("writing", 3)
                    Helpers.Logger.WriteMessage(sApplicationsScript, 4)
                    Using sw As New IO.StreamWriter(sApplicationsScript)
                        For Each line As String In linesApplications
                            sw.WriteLine(line)
                        Next
                    End Using
                    Helpers.Logger.WriteMessage("ok", , 5)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteError("error", 2)
                Helpers.Logger.WriteError(ex.Message, 3)

                ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_FAILED)

                Step7_Done()

                Exit Sub
            End Try
        Else
            Helpers.Logger.WriteMessage("skipped, not set", 2)
        End If


        Helpers.Logger.WriteMessage("making Chrome the default browser", 1)
        If InstallerActions.Contains(InstallerActions.ACTIONS.REPLACE_STANDARD_SK_BROWSER_WITH_CHROME) Then
            Helpers.Logger.WriteMessage("does Chrome icon exist", 2)
            Dim chromeIcon As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder, "Skins\Public\Media\Chrome\Img\Icons\chrome.ico")

            If Not IO.File.Exists(chromeIcon) Then
                Helpers.Logger.WriteMessage("nope", 3)
                Helpers.Logger.WriteMessage("copying", 3)

                Try
                    Helpers.Logger.WriteMessage("creating folder", 4)
                    IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(chromeIcon))
                    Helpers.Logger.WriteMessage("ok", 5)

                    Helpers.Logger.WriteMessage("copying file", 4)

                    Dim _bytes() As Byte

                    Using ms As IO.MemoryStream = New IO.MemoryStream
                        My.Resources.chromeicon.Save(ms)

                        _bytes = ms.ToArray
                    End Using

                    IO.File.WriteAllBytes(chromeIcon, _bytes)

                    Helpers.Logger.WriteMessage("ok", 5)
                Catch ex As Exception
                    Helpers.Logger.WriteError("FAIL", 5)
                    Helpers.Logger.WriteError(ex.Message, 6)

                    ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_FAILED)
                    ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_APP_LOGO_ICON)
                End Try
            Else
                Helpers.Logger.WriteMessage("yep", 4)
            End If


            Helpers.Logger.WriteMessage("patching script files", 2)

            'TermsAndConditions_and_Payment.js
            Try
                Dim sScript As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder, "Skins\Public\iBAHN\Scripts\internal\TermsAndConditions_and_Payment.js")
                Dim linesScript As New List(Of String)
                Dim bScriptChanged As Boolean = False


                Helpers.Logger.WriteMessage(sScript, 3)

                Using sr As New IO.StreamReader(sScript)
                    Dim line As String, bDone As Boolean = False

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("//CHROME phase 3 shortcut") Then
                            Helpers.Logger.WriteMessage("already patched", 4)
                            Exit While
                        End If

                        If line.Contains("function TermsAndConditions__ExecuteAction_url(sUrl) {") Then
                            'found function, replacing
                            Helpers.Logger.WriteMessage("found", 4)

                            Helpers.Logger.WriteMessage("changing line", 5)
                            Helpers.Logger.WriteMessage("from", 6)
                            Helpers.Logger.WriteWithoutDate(line)


                            line = My.Resources.TermsAndConditions_and_Payment_js_patch

                            Helpers.Logger.WriteMessage("to", 6)
                            Helpers.Logger.WriteWithoutDate(line)


                            bScriptChanged = True
                        End If

                        linesScript.Add(line)
                    End While
                End Using

                If bScriptChanged Then
                    Helpers.Logger.WriteMessage("saving", 4)
                    Helpers.Logger.WriteMessage("backup", 5)
                    Helpers.FilesAndFolders.Backup.File(sScript, 0, My.Application.Info.ProductName)

                    Helpers.Logger.WriteMessage("writing", 5)
                    Helpers.Logger.WriteMessage(sScript, 6)
                    Using sw As New IO.StreamWriter(sScript)
                        For Each line As String In linesScript
                            sw.WriteLine(line)
                        Next
                    End Using

                    Helpers.Logger.WriteMessage("ok", 5)
                Else
                    Helpers.Logger.WriteMessage("no update needed", 4)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteError("error", 3)
                Helpers.Logger.WriteError(ex.Message, 4)

                ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_FAILED)

                Step7_Done()

                Exit Sub
            End Try

            'freeairlineaccess.html
            Try
                Dim sScript As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder, "Skins\default\Systemdialog\freeairlineaccess.html")
                Dim linesScript As New List(Of String)
                Dim bScriptChanged As Boolean = False


                Helpers.Logger.WriteMessage(sScript, 3)

                Using sr As New IO.StreamReader(sScript)
                    Dim line As String, bDone As Boolean = False

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("//CHROME phase 3 shortcut") Then
                            Helpers.Logger.WriteMessage("already patched", 4)
                            Exit While
                        End If

                        If line.Contains("function openAirlineWebpage(num) {") Then
                            'found function, replacing
                            Helpers.Logger.WriteMessage("found", 4)

                            Helpers.Logger.WriteMessage("changing line", 5)
                            Helpers.Logger.WriteMessage("from", 6)
                            Helpers.Logger.WriteWithoutDate(line)


                            line = My.Resources.freeairlineaccess_html_patch

                            Helpers.Logger.WriteMessage("to", 6)
                            Helpers.Logger.WriteWithoutDate(line)


                            bScriptChanged = True
                        End If

                        linesScript.Add(line)
                    End While
                End Using

                If bScriptChanged Then
                    Helpers.Logger.WriteMessage("saving", 4)
                    Helpers.Logger.WriteMessage("backup", 5)
                    Helpers.FilesAndFolders.Backup.File(sScript, 0, My.Application.Info.ProductName)

                    Helpers.Logger.WriteMessage("writing", 5)
                    Helpers.Logger.WriteMessage(sScript, 6)
                    Using sw As New IO.StreamWriter(sScript)
                        For Each line As String In linesScript
                            sw.WriteLine(line)
                        Next
                    End Using

                    Helpers.Logger.WriteMessage("ok", 5)
                Else
                    Helpers.Logger.WriteMessage("no update needed", 4)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteError("error", 3)
                Helpers.Logger.WriteError(ex.Message, 4)

                ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_FAILED)

                Step7_Done()

                Exit Sub
            End Try

            'StartmenuDialog.html
            Try
                Dim sScript As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder, "Skins\Win7IE8\StartmenuDialog.html")
                Dim linesScript As New List(Of String)
                Dim bScriptChanged As Boolean = False


                Helpers.Logger.WriteMessage(sScript, 3)

                Using sr As New IO.StreamReader(sScript)
                    Dim line As String, bAlreadyPatched As Boolean = False

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('firefox')+')") Then
                            ' AddAppToStarmenu('OpenLink("http://www.google.com/ncr");', StringTable.LoadString(500), SiteKiosk.SiteKioskDirectory + "skins\\Public\\Media\\Ie\\Img\\Icons\\ie.ico");
                            ' AddAppToStarmenu('iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('firefox')+')', "Firefox", SiteKiosk.SiteKioskDirectory + "skins\\Public\\Media\\Firefox\\Img\\Icons\\ff.ico");
                            Helpers.Logger.WriteMessage("found Firefox remains", 4)

                            Helpers.Logger.WriteMessage("changing line", 5)
                            Helpers.Logger.WriteMessage("from", 6)
                            Helpers.Logger.WriteWithoutDate(line)

                            line = line.Replace("'iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('firefox')+')'", "'OpenLink(""http://www.google.com/ncr"");'")
                            line = line.Replace("skins\\Public\\Media\\Firefox\\Img\\Icons\\ff.ico", "skins\\Public\\Media\\Ie\\Img\\Icons\\ie.ico")
                            line = line.Replace("""Firefox""", "StringTable.LoadString(500)")

                            Helpers.Logger.WriteMessage("to", 6)
                            Helpers.Logger.WriteWithoutDate(line)


                            bScriptChanged = True
                        End If

                        If line.Contains("//CHROME phase 3 shortcut") Then
                            Helpers.Logger.WriteMessage("main patch already there", 4)
                            bAlreadyPatched = True
                            'Exit While
                        End If

                        If Not bAlreadyPatched And line.Contains("function OpenLink(as_Link)") Then
                            'found function, replacing
                            Helpers.Logger.WriteMessage("found", 4)

                            Helpers.Logger.WriteMessage("changing line", 5)
                            Helpers.Logger.WriteMessage("from", 6)
                            Helpers.Logger.WriteWithoutDate(line)


                            line = My.Resources.StartmenuDialog_html_patch

                            Helpers.Logger.WriteMessage("to", 6)
                            Helpers.Logger.WriteWithoutDate(line)


                            bScriptChanged = True
                        End If

                        linesScript.Add(line)
                    End While
                End Using

                If bScriptChanged Then
                    Helpers.Logger.WriteMessage("saving", 4)
                    Helpers.Logger.WriteMessage("backup", 5)
                    Helpers.FilesAndFolders.Backup.File(sScript, 0, My.Application.Info.ProductName)

                    Helpers.Logger.WriteMessage("writing", 5)
                    Helpers.Logger.WriteMessage(sScript, 6)
                    Using sw As New IO.StreamWriter(sScript)
                        For Each line As String In linesScript
                            sw.WriteLine(line)
                        Next
                    End Using

                    Helpers.Logger.WriteMessage("ok", 5)
                Else
                    Helpers.Logger.WriteMessage("no update needed", 4)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteError("error", 3)
                Helpers.Logger.WriteError(ex.Message, 4)

                ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_FAILED)

                Step7_Done()

                Exit Sub
            End Try
        Else
            Helpers.Logger.WriteMessage("skipped, not set", 2)
        End If


        Helpers.Logger.WriteMessage("updating all theme internet buttons", 1)
        If InstallerActions.Contains(InstallerActions.ACTIONS.UPDATE_ALL_THEMES) Then
            Helpers.Logger.WriteMessage("iterating theme folders", 2)
            Dim sThemeFolders As String() = IO.Directory.GetDirectories(IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder, "Skins\Public\Startpages"))

            For Each sThemeFolder As String In sThemeFolders
                Try
                    Helpers.Logger.WriteMessage("entering '" & sThemeFolder & "'", 3)

                    If Not IO.Directory.Exists(IO.Path.Combine(sThemeFolder, "img\icons")) Then
                        Helpers.Logger.WriteMessage("no icon folder found, skipping", 4)

                        Continue For
                    End If

                    Helpers.Logger.WriteMessage("updating internet icon", 4)
                    Dim sIcon As String = IO.Path.Combine(sThemeFolder, "img\icons\internet_pc.png")

                    If Not IO.File.Exists(sIcon) Then
                        sIcon = IO.Path.Combine(sThemeFolder, "img\icons\internet.png")

                        If Not IO.File.Exists(sIcon) Then
                            Helpers.Logger.WriteMessage("internet icon not found, skipping", 5)

                            Continue For
                        End If
                    End If

                    Helpers.Logger.WriteMessage(sIcon, 5)

                    Helpers.Logger.WriteMessage("backup", 6)
                    Helpers.FilesAndFolders.Backup.File(sIcon, 0, My.Application.Info.ProductName)

                    Helpers.Logger.WriteMessage("copying from resources", 6)
                    Dim _bytes() As Byte

                    Using ms As IO.MemoryStream = New IO.MemoryStream
                        My.Resources.chrome_internet_icon_for_themes.Save(ms, Imaging.ImageFormat.Png)

                        _bytes = ms.ToArray
                    End Using

                    IO.File.WriteAllBytes(sIcon, _bytes)

                    Helpers.Logger.WriteMessage("ok", 7)
                Catch ex As Exception
                    Helpers.Logger.WriteError("FAIL", 5)
                    Helpers.Logger.WriteError(ex.Message, 6)

                    ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_FAILED)
                    ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_CHANGE_THEME_ICONS)
                End Try
            Next
        Else
            Helpers.Logger.WriteMessage("skipped, not set", 2)
        End If


        '------
        Step6_KillSitekiosk()
    End Sub
#End Region


    '================================================================================================================================================
    'Step 6
    Private Sub Step6_KillSitekiosk()
        Helpers.Logger.WriteMessage("killing SiteKiosk", 0)

        If InstallerActions.Contains(InstallerActions.ACTIONS.RESTART_SITEKIOSK_WHEN_DONE) Then
            Dim iKilled As Integer = Helpers.SiteKiosk.Application.Kill()

            If iKilled = 1 Then
                Helpers.Logger.WriteMessage("killed 1 process", 1)
            Else
                Helpers.Logger.WriteMessage("killed " & iKilled & " processes", 1)
            End If
        Else
            Helpers.Logger.WriteMessage("skipped, not set", 1)
        End If


        Step7_Done()
    End Sub

    Private Sub Step7_Done()
        Functions.ExitApplication()
    End Sub
End Module
