﻿Public Class frmMain
    Private mError As Boolean = False

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ExitCode.SetValue(ExitCode.ExitCodes.OK)

        Me.TransparencyKey = Color.Lime
        'Me.Top = Screen.PrimaryScreen.Bounds.Height - Me.Height
        'Me.Left = Screen.PrimaryScreen.Bounds.Width - Me.Width
        Me.Top = My.Computer.Screen.Bounds.Height - Me.Height
        Me.Left = My.Computer.Screen.Bounds.Width - Me.Width
        Helpers.Forms.OnTop.Put(Me.Handle)

        If Not Functions.FindChromeExecutable(InstallerGlobals.Chrome.Run.CHROME_FULL_PATH) Then
            lblMessage.Text = "chrome not found"
            lblMessage.ForeColor = Color.Red

            mError = True

            DelayedExitApplication(10000)
            Exit Sub
        End If


        If Not Functions.FindSiteKioskUserProfileFolder(InstallerGlobals.Sitekiosk.USER_PROFILE) Then
            InstallerGlobals.Sitekiosk.USER_PROFILE = InstallerGlobals.Sitekiosk.USER_PROFILE_DEFAULT
        End If

        If Not IO.Directory.Exists(InstallerGlobals.Sitekiosk.USER_PROFILE) Then
            lblMessage.Text = "SK profile not found"
            lblMessage.ForeColor = Color.Red

            mError = True

            DelayedExitApplication(10000)
            Exit Sub
        End If

        InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY = InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY_TEMPLATE.Replace("%%SITEKIOSK_USER_PROFILE_DIRECTORY%%", InstallerGlobals.Sitekiosk.USER_PROFILE)


        '----------------
        tmrStart.Enabled = True
    End Sub

    Private Sub tmrStart_Tick(sender As Object, e As EventArgs) Handles tmrStart.Tick
        tmrStart.Enabled = False


        Dim args As String = InstallerGlobals.Chrome.Run.CHROME_PARAMETERS

        args = args.Replace("%%CHROME_PROFILE_DIRECTORY%%", InstallerGlobals.Chrome.Run.CHROME_PROFILE_DIRECTORY)

        Dim cmdArgs() As String = Environment.GetCommandLineArgs()

        If cmdArgs.Count > 1 Then
            cmdArgs = cmdArgs.Skip(1).ToArray
            args = args & " " & String.Join(" ", cmdArgs)
        End If


        Dim p As New ProcessStartInfo

        p.FileName = InstallerGlobals.Chrome.Run.CHROME_FULL_PATH
        p.Arguments = args

        Process.Start(p)

        '================================
        Threading.Thread.Sleep(1000)

        tmrChromeStarted.Enabled = True
    End Sub

    Private mChromeWaitCount As Integer = 0
    Private Sub tmrChromeStarted_Tick(sender As Object, e As EventArgs) Handles tmrChromeStarted.Tick
        mChromeWaitCount += 1

        If Helpers.Processes.IsProcessRunning(InstallerGlobals.Chrome.Run.CHROME_PROCESS_NAME) Then
            tmrChromeStarted.Enabled = False

            DelayedExitApplication(2500)
            Exit Sub
        End If

        If mChromeWaitCount > 30 Then
            tmrChromeStarted.Enabled = False

            lblMessage.Text = "chrome not started"
            lblMessage.ForeColor = Color.Red

            mError = True
            Me.Visible = True

            DelayedExitApplication(10000)
        End If
    End Sub

    Private Sub DelayedExitApplication(Delay_ms As Integer)
        tmrDelayedExit.Interval = Delay_ms
        tmrDelayedExit.Enabled = True
    End Sub

    Private Sub tmrDelayedExit_Tick(sender As Object, e As EventArgs) Handles tmrDelayedExit.Tick
        tmrDelayedExit.Enabled = False

        tmrFadeOut.Enabled = True
    End Sub

    Private mFadeValue As Double = 1.0
    Private mFadeStep As Double = 0.1
    Private Sub tmrFadeOut_Tick(sender As Object, e As EventArgs) Handles tmrFadeOut.Tick
        mFadeValue -= mFadeStep

        If mFadeValue < 0 Then
            tmrFadeOut.Enabled = False

            Functions.ExitApplication()
            Exit Sub
        End If

        Me.Opacity = mFadeValue
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If Not mError Then
            Me.Visible = False
        End If
    End Sub
End Class