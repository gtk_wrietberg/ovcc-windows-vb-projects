﻿Imports System.IO

Module modMain
    Public Sub Main()
        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("started", 1)


        Dim sId As String = ""
        Dim sProfileDir As String = ""

        Helpers.Logger.Write("looking for profile folder for '" & c_SiteKioskUsername & "' user", , 0)
        Helpers.Logger.Write("user", , 1)
        Helpers.Logger.Write(c_SiteKioskUsername, , 2)

        Helpers.Logger.Write("sid", , 1)
        If Not Helpers.WindowsUser.ConvertUsernameToSid(c_SiteKioskUsername, sId) Then
            Helpers.Logger.Write("nothing found", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            Helpers.Logger.Write(sId, , 2)
        End If

        Helpers.Logger.Write("profile path", , 1)
        If Not Helpers.WindowsUser.GetProfileDirectory(c_SiteKioskUsername, sProfileDir) Then
            Helpers.Logger.Write("nothing found", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            Helpers.Logger.Write(sProfileDir, , 2)
        End If

        If Not sProfileDir.Equals("") Then
            g_SiteKioskProfilePath = sProfileDir
        End If


        Results.RunAsUser = Environment.UserName

        Results.ProfilePath = g_SiteKioskProfilePath
        Results.ProfileName = g_SiteKioskProfilePath.Replace(c_ProfilePath, "")
        Results.ProfileIsOk = c_SiteKioskUsername.ToLower.Equals(g_SiteKioskProfilePath.Replace(c_ProfilePath, "").ToLower)
        Results.ProfileExists = IO.Directory.Exists(g_SiteKioskProfilePath)

        Dim lSize As Long = 0
        Dim lCount As Long = 0
        Dim dOldest As Date = #2099-01-01#
        Dim dNewest As Date = #1970-01-01#

        Helpers.Logger.Write("looking for files", , 0)
        DirectoryFileSizeAndCountAndNewestAndOldest(g_SiteKioskProfilePath & "\Desktop", lSize, lCount, dNewest, dOldest, True)
        DirectoryFileSizeAndCountAndNewestAndOldest(g_SiteKioskProfilePath & "\Documents", lSize, lCount, dNewest, dOldest, True)
        DirectoryFileSizeAndCountAndNewestAndOldest(g_SiteKioskProfilePath & "\Downloads", lSize, lCount, dNewest, dOldest, True)
        DirectoryFileSizeAndCountAndNewestAndOldest(g_SiteKioskProfilePath & "\Pictures", lSize, lCount, dNewest, dOldest, True)

        Results.ProfileFileCount = lCount
        Results.ProfileFileSize = lSize
        Results.ProfileOldestFileTimestamp = dOldest
        Results.ProfileNewestFileTimestamp = dNewest

        Results.Save()

        ExitApplication()
    End Sub

    Private Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

    Private Function DirectoryFileSizeAndCountAndNewestAndOldest(ByVal sPath As String, ByRef lSize As Long, ByRef lCount As Long, ByRef dNewest As Date, ByRef dOldest As Date, Optional ByVal bRecursive As Boolean = False) As Boolean
        Dim diDir As New IO.DirectoryInfo(sPath)

        Helpers.Logger.Write(sPath, , 1)

        Try
            Dim fil As FileInfo
            Dim result As Integer

            For Each fil In diDir.GetFiles()
                Helpers.Logger.Write(fil.Name, , 2)

                If fil.Attributes.HasFlag(FileAttributes.Hidden) Then
                    Helpers.Logger.Write("skipped", , 3)

                    Continue For
                End If

                If fil.Extension = ".lnk" Then
                    Helpers.Logger.Write("skipped", , 3)

                    Continue For
                End If

                lSize += fil.Length
                lCount += 1

                result = Date.Compare(fil.LastWriteTime, dOldest)
                If result < 0 Then
                    dOldest = fil.LastWriteTime
                End If

                result = Date.Compare(fil.LastWriteTime, dNewest)
                If result > 0 Then
                    dNewest = fil.LastWriteTime
                End If
            Next fil

            If bRecursive = True Then
                Dim diSubDir As DirectoryInfo

                For Each diSubDir In diDir.GetDirectories()
                    DirectoryFileSizeAndCountAndNewestAndOldest(diSubDir.FullName, lSize, lCount, dNewest, dOldest, True)
                Next diSubDir
            End If

            Return True
        Catch ex As System.IO.FileNotFoundException
            Return 0
        Catch exx As Exception
            Return 0
        End Try

        Return True
    End Function
End Module
