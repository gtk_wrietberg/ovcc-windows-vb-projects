﻿Public Class Results
    Private Shared ReadOnly c_RegRoot As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCStatusChecker"

    Public Shared ProfileName As String = ""
    Public Shared ProfilePath As String = ""
    Public Shared ProfileIsOk As Boolean = False
    Public Shared ProfileExists As Boolean = False
    Public Shared ProfileFileCount As Long = -1
    Public Shared ProfileFileSize As Long = -1
    Public Shared ProfileOldestFileTimestamp As Date = New Date(0)
    Public Shared ProfileNewestFileTimestamp As Date = New Date(0)
    Public Shared RunAsUser As String = ""

    Public Shared Function Save() As Boolean
        Helpers.Logger.Write("results", , 0)

        Try
            Helpers.Registry.CreateKey(c_RegRoot)

            Helpers.Logger.Write("RunAsUser", , 1)
            Helpers.Logger.Write(RunAsUser, , 2)
            Helpers.Registry.SetValue_String(c_RegRoot, "RunAsUser", RunAsUser)

            Helpers.Logger.Write("ProfileName", , 1)
            Helpers.Logger.Write(ProfileName, , 2)
            Helpers.Registry.SetValue_String(c_RegRoot, "ProfileName", ProfileName)

            Helpers.Logger.Write("ProfilePath", , 1)
            Helpers.Logger.Write(ProfilePath, , 2)
            Helpers.Registry.SetValue_String(c_RegRoot, "ProfilePath", ProfilePath)

            Helpers.Logger.Write("ProfileIsOk", , 1)
            Helpers.Logger.Write(ProfileIsOk, , 2)
            Helpers.Registry.SetValue_Boolean(c_RegRoot, "ProfileIsOk", ProfileIsOk)

            Helpers.Logger.Write("ProfileExists", , 1)
            Helpers.Logger.Write(ProfileExists, , 2)
            Helpers.Registry.SetValue_Boolean(c_RegRoot, "ProfileExists", ProfileExists)

            Helpers.Logger.Write("ProfileFileCount", , 1)
            Helpers.Logger.Write(ProfileFileCount, , 2)
            Helpers.Registry.SetValue_Integer(c_RegRoot, "ProfileFileCount", ProfileFileCount)

            Helpers.Logger.Write("ProfileFileSize", , 1)
            Helpers.Logger.Write(ProfileFileSize, , 2)
            Helpers.Registry.SetValue_Integer(c_RegRoot, "ProfileFileSize", ProfileFileSize)

            Helpers.Logger.Write("ProfileOldestFileTimestamp", , 1)
            Helpers.Logger.Write(ProfileOldestFileTimestamp, , 2)
            Helpers.Registry.SetValue_String(c_RegRoot, "ProfileOldestFileTimestamp", ProfileOldestFileTimestamp.ToString)

            Helpers.Logger.Write("ProfileNewestFileTimestamp", , 1)
            Helpers.Logger.Write(ProfileNewestFileTimestamp, , 2)
            Helpers.Registry.SetValue_String(c_RegRoot, "ProfileNewestFileTimestamp", ProfileNewestFileTimestamp.ToString)

            Helpers.Logger.Write("LastRun", , 1)
            Helpers.Logger.Write(Now(), , 2)
            Helpers.Registry.SetValue_String(c_RegRoot, "LastRun", Now())

            Return True
        Catch ex As Exception
            ExitCode.SetValue(ExitCode.ExitCodes.NO_PERMISSION)
        End Try

        Return False
    End Function
End Class
