﻿Imports System.IO
Imports System.Text

Public Class CleanersHelpers

    Public Class PrinterQueue
        Public Shared Function IsPrinterQueueCleanerServiceRunning() As Boolean
            Try
                Dim ssCleaner As ServiceProcess.ServiceControllerStatus = ServiceInstaller.GetServiceStatus(Constants.c__SERVICE_Name)

                If ssCleaner = ServiceProcess.ServiceControllerStatus.Running Or ssCleaner = ServiceProcess.ServiceControllerStatus.StartPending Then
                    Return True
                Else
                    Helpers.Errors.Add("PQCS error: service not running (state: " & ssCleaner.ToString & ")")
                End If
            Catch ex As Exception
                Helpers.Errors.Add("PQCS error:" & ex.Message)
            End Try

            Return False
        End Function

        Public Shared Function TriggerPrinterQueueCleanup() As Boolean
            Try
                Dim fs As FileStream = File.Create(Constants.c__Path_WindowsTemp & "\" & Constants.c__File_TriggerFile)

                ' Add text to the file.
                Dim info As Byte() = New UTF8Encoding(True).GetBytes(Helpers.Types.RandomString(32))
                fs.Write(info, 0, info.Length)
                fs.Close()

                If File.Exists(Constants.c__Path_WindowsTemp & "\" & Constants.c__File_TriggerFile) Then
                    Return True
                Else
                    Helpers.Errors.Add("PQCS trigger error: file not created")
                    Return False
                End If
            Catch ex As Exception
                Helpers.Errors.Add("PQCS trigger error: " & ex.Message)

                Return False
            End Try
        End Function
    End Class


    Public Class ChromeUserData
        Public Shared Function IsChromeUserDataCleanerServiceRunning() As Boolean
            Try
                Dim ssCleaner As ServiceProcess.ServiceControllerStatus = ServiceInstaller.GetServiceStatus(Constants.c__SERVICE_Name)

                If ssCleaner = ServiceProcess.ServiceControllerStatus.Running Or ssCleaner = ServiceProcess.ServiceControllerStatus.StartPending Then
                    Return True
                Else
                    Helpers.Errors.Add("PQCS error: service not running (state: " & ssCleaner.ToString & ")")
                End If
            Catch ex As Exception
                Helpers.Errors.Add("PQCS error:" & ex.Message)
            End Try

            Return False
        End Function

        Public Shared Function TriggerPrinterQueueCleanup() As Boolean
            Try
                Dim fs As FileStream = File.Create(Constants.c__Path_WindowsTemp & "\" & Constants.c__File_TriggerFile)

                ' Add text to the file.
                Dim info As Byte() = New UTF8Encoding(True).GetBytes(Helpers.Types.RandomString(32))
                fs.Write(info, 0, info.Length)
                fs.Close()

                If File.Exists(Constants.c__Path_WindowsTemp & "\" & Constants.c__File_TriggerFile) Then
                    Return True
                Else
                    Helpers.Errors.Add("PQCS trigger error: file not created")
                    Return False
                End If
            Catch ex As Exception
                Helpers.Errors.Add("PQCS trigger error: " & ex.Message)

                Return False
            End Try
        End Function
    End Class


End Class
