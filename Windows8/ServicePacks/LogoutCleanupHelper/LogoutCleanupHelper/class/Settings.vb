﻿Public Class Settings
    Public Class Registry
        Public Shared ReadOnly KEY__LCH As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\LogoutCleanupHelper"

        Public Shared ReadOnly KEY__LCH__Cleaners As String = KEY__LCH & "\Cleaners"
        Public Shared ReadOnly VALUE__LHC_FireFox As String = "Firefox"
        Public Shared ReadOnly VALUE__LHC_Office As String = "Office"
        Public Shared ReadOnly VALUE__LHC_PrintQueue As String = "PrintQueue"


        Public Shared ReadOnly KEY__PQCS As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\PrintQueueCleanerService"
        Public Shared ReadOnly VALUE__PQCS_State As String = "State"

        Public Shared ReadOnly DEFAULTVALUE__PQCS_State As String = ""

        Public Shared ReadOnly KEY__SITEKIOSK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"
        Public Shared ReadOnly VALUE__SiteKiosk_LastCfg As String = "LastCfg"
    End Class

    Public Class ReadWrite
        Public Class Cleaners
            Public Shared Property Firefox As Boolean
                Get
                    Return Helpers.Registry.GetValue_Boolean(Registry.KEY__LCH__Cleaners, Registry.VALUE__LHC_FireFox, False)
                End Get
                Set(value As Boolean)
                    Helpers.Registry.SetValue_Boolean(Registry.KEY__LCH__Cleaners, Registry.VALUE__LHC_FireFox, value)
                End Set
            End Property

            Public Shared Property Office As Boolean
                Get
                    Return Helpers.Registry.GetValue_Boolean(Registry.KEY__LCH__Cleaners, Registry.VALUE__LHC_Office, False)
                End Get
                Set(value As Boolean)
                    Helpers.Registry.SetValue_Boolean(Registry.KEY__LCH__Cleaners, Registry.VALUE__LHC_Office, value)
                End Set
            End Property

            Public Shared Property PrintQueue As Boolean
                Get
                    Return Helpers.Registry.GetValue_Boolean(Registry.KEY__LCH__Cleaners, Registry.VALUE__LHC_PrintQueue, False)
                End Get
                Set(value As Boolean)
                    Helpers.Registry.SetValue_Boolean(Registry.KEY__LCH__Cleaners, Registry.VALUE__LHC_PrintQueue, value)
                End Set
            End Property
        End Class
    End Class
End Class
