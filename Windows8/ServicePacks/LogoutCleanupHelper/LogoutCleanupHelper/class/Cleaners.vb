﻿Option Compare Text

Public Class Cleaners
    Public Class PrintQueue
        Public Shared Function Clean() As Boolean
            Helpers.Logger.WriteMessageRelative("cleaning printer queue", 1)

            Helpers.Logger.WriteMessageRelative("triggering service", 2)
            If CleanersHelpers.PrinterQueue.TriggerPrinterQueueCleanup() Then
                Helpers.Logger.WriteMessageRelative("ok", 3)
            Else
                Helpers.Logger.WriteErrorRelative("fail", 3)
                Helpers.Logger.WriteErrorRelative(Helpers.Errors.GetLast(False), 4)

                Return False
            End If

            Return True
        End Function
    End Class

    Public Class Office
        ' [HKEY_LOCAL_MACHINE\SK_USER_HIVE\Software\Microsoft\MSOIdentityCRL\UserExtendedProperties\microsoftonline.com::liam.mcglynn@itb.ie]
        ' [HKEY_LOCAL_MACHINE\SK_USER_HIVE\Software\Microsoft\Office\15.0\Common\Identity\Identities\675b3290714db151_OrgId]
        ' [HKEY_LOCAL_MACHINE\SK_USER_HIVE\Software\Microsoft\Office\15.0\Common\ServicesManagerCache\Identities\675b3290714db151_OrgId]


        Private Shared ReadOnly REG_KEY__ROOT001 As String = "Software\Microsoft\MSOIdentityCRL"
        Private Shared ReadOnly REG_KEY__SUB001 As String = "UserExtendedProperties"

        Private Shared ReadOnly REG_KEY__ROOT002 As String = "Software\Microsoft\Office\%%OFFICE_VERSION%%.0\Common\Identity"
        Private Shared ReadOnly REG_KEY__SUB002 As String = "Identities"

        Private Shared ReadOnly REG_KEY__ROOT003 As String = "Software\Microsoft\Office\%%OFFICE_VERSION%%.0\Common\ServicesManagerCache"
        Private Shared ReadOnly REG_KEY__SUB003 As String = "Identities"

        Public Shared Function Clean() As Boolean
            Helpers.Logger.WriteMessageRelative("cleaning registry", 1)

            Helpers.Logger.WriteMessageRelative(REG_KEY__ROOT001, 2)
            Helpers.Logger.WriteMessageRelative(REG_KEY__SUB001, 3)

            If Helpers.Registry.DeleteSubKey(REG_KEY__ROOT001, REG_KEY__SUB001) Then
                Helpers.Logger.WriteMessageRelative("ok", 4)
            Else
                Helpers.Logger.WriteErrorRelative("return message: " & Helpers.Errors.GetLast(False), 4)
            End If


            '-----------------------------------------------------------------------------------------------
            'Sledgehammer time. 
            Dim sTmp As String = ""
            For iOfficeVersion As Integer = 11 To 20
                sTmp = REG_KEY__ROOT002.Replace("%%OFFICE_VERSION%%", iOfficeVersion.ToString)

                Helpers.Logger.WriteMessageRelative(sTmp, 2)
                If Helpers.Registry.KeyExists_CU(sTmp) Then
                    Helpers.Logger.WriteMessageRelative(REG_KEY__SUB002, 3)
                    If Helpers.Registry.KeyExists_CU(sTmp & "\" & REG_KEY__SUB002) Then
                        If Helpers.Registry.DeleteSubKey(sTmp, REG_KEY__SUB002) Then
                            Helpers.Logger.WriteMessageRelative("ok", 4)
                        Else
                            Helpers.Logger.WriteErrorRelative("return message: " & Helpers.Errors.GetLast(False), 4)
                        End If
                    Else
                        Helpers.Logger.WriteWarningRelative("not found", 4)
                        Helpers.Logger.WriteWarningRelative("last error", 5)
                        Helpers.Logger.WriteWarningRelative(Helpers.Errors.GetLast(False), 6)
                    End If
                Else
                    Helpers.Logger.WriteWarningRelative("not found", 3)
                    Helpers.Logger.WriteWarningRelative("last error", 4)
                    Helpers.Logger.WriteWarningRelative(Helpers.Errors.GetLast(False), 5)
                End If


                '---------------------------------------
                sTmp = REG_KEY__ROOT003.Replace("%%OFFICE_VERSION%%", iOfficeVersion.ToString)

                Helpers.Logger.WriteMessageRelative(sTmp, 2)
                If Helpers.Registry.KeyExists_CU(sTmp) Then
                    Helpers.Logger.WriteMessageRelative(REG_KEY__SUB003, 3)
                    If Helpers.Registry.KeyExists_CU(sTmp & "\" & REG_KEY__SUB003) Then
                        If Helpers.Registry.DeleteSubKey(sTmp, REG_KEY__SUB003) Then
                            Helpers.Logger.WriteMessageRelative("ok", 4)
                        Else
                            Helpers.Logger.WriteErrorRelative("return message: " & Helpers.Errors.GetLast(False), 4)
                        End If
                    Else
                        Helpers.Logger.WriteWarningRelative("not found", 4)
                        Helpers.Logger.WriteWarningRelative("last error", 5)
                        Helpers.Logger.WriteWarningRelative(Helpers.Errors.GetLast(False), 6)
                    End If
                Else
                    Helpers.Logger.WriteWarningRelative("not found", 3)
                    Helpers.Logger.WriteWarningRelative("last error", 4)
                    Helpers.Logger.WriteWarningRelative(Helpers.Errors.GetLast(False), 5)
                End If
            Next

            Return True
        End Function
    End Class

    Public Class FireFox
        Public Class Constants
            Public Shared ReadOnly FirefoxAppDataPathTemplate As String = "C:\Users\%%%USERNAME%%%\AppData\Roaming\Mozilla\Firefox\"
            Public Shared ReadOnly FirefoxProfiles_ini As String = "profiles.ini"
            Public Shared ReadOnly FirefoxInstalls_ini As String = "installs.ini"

            'Public Shared ReadOnly SavedProfilePath As String = "saved_profile\%%TIMESTAMP%%"
            Public Shared ReadOnly SavedProfilePath As String = "saved_profile"

            Public Class RegistryKeys
                Public Shared ReadOnly KEY__Cleaners_Firefox As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\LogoutCleanupHelper\Cleaners\Firefox"
                Public Shared ReadOnly VALUE__Cleaners_Firefox__name As String = "saved_profile__name"
                Public Shared ReadOnly VALUE__Cleaners_Firefox__path As String = "saved_profile__path"
            End Class
        End Class

        Public Class ProfileData
            Private mIndex As Integer
            Public Property Index() As Integer
                Get
                    Return mIndex
                End Get
                Set(ByVal value As Integer)
                    mIndex = value
                End Set
            End Property

            Private mName As String
            Public Property Name() As String
                Get
                    Return mName
                End Get
                Set(ByVal value As String)
                    mName = value
                End Set
            End Property

            Private mPath As String
            Public Property Path() As String
                Get
                    Return mPath
                End Get
                Set(ByVal value As String)
                    mPath = value
                End Set
            End Property

            Private mFullPath As String
            Public Property FullPath() As String
                Get
                    Return mFullPath
                End Get
                Set(ByVal value As String)
                    mFullPath = value
                End Set
            End Property

            Private mDate As Date
            Public Property [Date]() As Date
                Get
                    Return mDate
                End Get
                Set(ByVal value As Date)
                    mDate = value

                    mDateNumeric = Helpers.TimeDate.ToUnix(mDate)
                End Set
            End Property

            Private mDateNumeric As Long
            Public ReadOnly Property DateNumeric_JustForSortingPurposes() As Long
                Get
                    Return mDateNumeric
                End Get
            End Property

            Private mSize As Long
            Public Property Size() As Long
                Get
                    Return mSize
                End Get
                Set(ByVal value As Long)
                    mSize = value
                End Set
            End Property

            Private mDefault As Boolean
            Public Property [Default]() As Boolean
                Get
                    Return mDefault
                End Get
                Set(ByVal value As Boolean)
                    mDefault = value
                End Set
            End Property
        End Class

        Public Class Properties
            Public Shared ReadOnly Property ProfileRootPath() As String
                Get
                    If mUserName.Equals("") Then
                        'uh oh
                        Return Constants.FirefoxAppDataPathTemplate
                    Else
                        Return Constants.FirefoxAppDataPathTemplate.Replace("%%%USERNAME%%%", mUserName)
                    End If
                End Get
            End Property

            Public Shared ReadOnly Property ProfilesIni() As String
                Get
                    Return IO.Path.Combine(ProfileRootPath & Constants.FirefoxProfiles_ini)
                End Get
            End Property

            Public Shared ReadOnly Property InstallsIni() As String
                Get
                    Return IO.Path.Combine(ProfileRootPath & Constants.FirefoxInstalls_ini)
                End Get
            End Property

            Private Shared mUserName As String = ""
            Public Shared Property Username() As String
                Get
                    Return mUserName
                End Get
                Set(ByVal value As String)
                    mUserName = value
                End Set
            End Property

            Public Class AvailableProfiles
                Private Shared mProfiles As New List(Of ProfileData)

                Public Shared Sub [Add](_p As ProfileData)
                    mProfiles.Add(_p)
                End Sub

                Public Shared Iterator Function [Get]() As IEnumerable(Of ProfileData)
                    Dim counter As Integer = -1

                    If mProfiles.Count > 0 Then
                        While counter < mProfiles.Count
                            counter += 1

                            If counter < mProfiles.Count Then
                                Yield mProfiles.Item(counter)
                            End If
                        End While
                    End If
                End Function

                Public Enum SortKeys
                    [Date] = 0
                    [FullPath]
                    [Index]
                    [Name]
                    [Path]
                    [Size]
                End Enum

                Public Shared Sub MatchDefault(DefaultProfilePath As String)
                    For _i As Integer = 0 To mProfiles.Count - 1
                        If mProfiles.Item(_i).Path = DefaultProfilePath Then
                            mProfiles.Item(_i).Default = True

                            Exit For
                        End If
                    Next
                End Sub

                Public Shared Function GetDefault(DefaultProfile As ProfileData) As Boolean
                    For Each _p As ProfileData In mProfiles
                        If _p.Default Then
                            DefaultProfile.Date = _p.Date
                            DefaultProfile.Default = _p.Default
                            DefaultProfile.FullPath = _p.FullPath
                            DefaultProfile.Index = _p.Index
                            DefaultProfile.Name = _p.Name
                            DefaultProfile.Path = _p.Path
                            DefaultProfile.Size = _p.Size

                            Return True
                        End If
                    Next

                    Return False
                End Function

                Public Shared Sub [Sort]()
                    Sort(SortKeys.Date)
                End Sub

                Public Shared Sub [Sort](SortBy As SortKeys)
                    mProfiles = mProfiles.OrderByDescending(Function(x)
                                                                Select Case SortBy
                                                                    Case SortKeys.Date
                                                                        Return x.DateNumeric_JustForSortingPurposes
                                                                    Case SortKeys.FullPath
                                                                        Return x.FullPath
                                                                    Case SortKeys.Index
                                                                        Return x.Index
                                                                    Case SortKeys.Name
                                                                        Return x.Name
                                                                    Case SortKeys.Path
                                                                        Return x.Path
                                                                    Case SortKeys.Size
                                                                        Return x.Size
                                                                End Select
                                                                Return x.Date
                                                            End Function
                                                            ).ToList()
                End Sub

                Public Shared Sub Reset()
                    mProfiles = New List(Of ProfileData)
                End Sub

                Public Shared Function Find(Index As Integer, ByRef Profile As ProfileData) As Boolean
                    Dim _ret As Boolean = False

                    For Each _profile As ProfileData In mProfiles
                        If _profile.Index = Index Then
                            Profile = _profile
                            _ret = True

                            Exit For
                        End If
                    Next

                    Return _ret
                End Function

                Public Shared Function Find(Name As String, ByRef Profile As ProfileData) As Boolean
                    Dim _ret As Boolean = False

                    For Each _profile As ProfileData In mProfiles
                        If _profile.Name = Name Then
                            Profile = _profile
                            _ret = True

                            Exit For
                        End If
                    Next

                    Return _ret
                End Function
            End Class
        End Class

        Public Class Functions
            Public Shared Function GetAllFirefoxProfiles() As Boolean
                Helpers.Logger.WriteMessageRelative("looking for profiles", 1)


                Properties.AvailableProfiles.Reset()


                Dim sLine As String = "", sTmp As String = "", iTmp As Integer = -1
                Dim _profile As New ProfileData


                Helpers.Logger.WriteMessageRelative("root path", 2)
                Helpers.Logger.WriteMessageRelative(Properties.ProfileRootPath, 3)

                Try
                    If Not IO.Directory.Exists(Properties.ProfileRootPath) Then
                        Throw New Exception("Folder '" & Properties.ProfileRootPath & "' does not exist")
                    End If

                    If Not IO.File.Exists(Properties.ProfilesIni) Then
                        Throw New Exception("File '" & Properties.ProfilesIni & "' does not exist")
                    End If


                    Helpers.Logger.WriteMessageRelative("ini file", 2)
                    Helpers.Logger.WriteMessageRelative(Properties.ProfilesIni, 3)

                    Dim _inifile As New Helpers.FilesAndFolders.IniFile(Properties.ProfilesIni)
                    Dim DefaultProfilePath As String = ""

                    Helpers.Logger.WriteMessageRelative("iterating", 2)
                    For Each _s As Helpers.FilesAndFolders.IniFile.Section In _inifile.Sections
                        If _s.Name.StartsWith("Install") Then
                            Helpers.Logger.WriteMessageRelative("found install section", 3)

                            For Each _k As Helpers.FilesAndFolders.IniFile.Key In _s.Keys
                                If _k.Name = "Default" Then
                                    DefaultProfilePath = _k.Value
                                End If
                            Next
                        End If

                        If _s.Name.StartsWith("Profile") Then
                            Helpers.Logger.WriteMessageRelative("found profile", 3)

                            _profile = New ProfileData

                            sTmp = _s.Name.Replace("Profile", "")
                            If Integer.TryParse(sTmp, iTmp) Then
                                _profile.Index = iTmp
                            Else
                                _profile.Index = -1
                            End If

                            Helpers.Logger.WriteMessageRelative("index", 4)
                            Helpers.Logger.WriteMessageRelative(_profile.Index, 5)


                            For Each _k As Helpers.FilesAndFolders.IniFile.Key In _s.Keys
                                Select Case _k.Name
                                    Case "Name"
                                        _profile.Name = _k.Value
                                        Helpers.Logger.WriteMessageRelative("name", 4)
                                        Helpers.Logger.WriteMessageRelative(_k.Value, 5)
                                    Case "Path"
                                        _profile.Path = _k.Value
                                        Helpers.Logger.WriteMessageRelative("path", 4)
                                        Helpers.Logger.WriteMessageRelative(_k.Value, 5)

                                        Helpers.Logger.WriteMessageRelative("full path", 4)
                                        Try
                                            _profile.FullPath = IO.Path.Combine(Properties.ProfileRootPath, _k.Value).Replace("/", "\")
                                            Helpers.Logger.WriteMessageRelative(_profile.FullPath, 5)
                                        Catch ex As Exception
                                            _profile.FullPath = ""
                                            Helpers.Logger.WriteMessageRelative("error", 5)
                                            Helpers.Logger.WriteMessageRelative(ex.Message, 6)
                                        End Try
                                End Select
                            Next


                            Helpers.Logger.WriteMessageRelative("folder size and last modified date", 4)
                            If Not _profile.FullPath.Equals("") Then
                                If IO.Directory.Exists(_profile.FullPath) Then
                                    Helpers.Logger.WriteMessageRelative("folder size", 5)

                                    _profile.Size = Helpers.FilesAndFolders.Folder.DirectorySize(_profile.FullPath, True)

                                    Helpers.Logger.WriteMessageRelative(_profile.Size & " (" & Helpers.FilesAndFolders.FileSize.GetHumanReadable(_profile.Size) & ")", 6)


                                    Helpers.Logger.WriteMessageRelative("date", 5)
                                    Try
                                        Dim sNewestFile As String = Helpers.FilesAndFolders.GetNewestFile(_profile.FullPath)
                                        Dim infoFile As New IO.FileInfo(sNewestFile)

                                        _profile.Date = infoFile.LastWriteTime
                                        Helpers.Logger.WriteMessageRelative(_profile.Date.ToString, 6)
                                    Catch ex As Exception
                                        _profile.Date = New Date(0)
                                        Helpers.Logger.WriteMessageRelative("error", 6)
                                        Helpers.Logger.WriteMessageRelative(ex.Message, 7)
                                    End Try
                                Else
                                    Helpers.Logger.WriteMessageRelative("path not found!", 6)
                                End If
                            Else
                                Helpers.Logger.WriteMessageRelative("path empty!", 6)
                            End If


                            Properties.AvailableProfiles.Add(_profile)
                        End If
                    Next


                    Properties.AvailableProfiles.MatchDefault(DefaultProfilePath)
                    Properties.AvailableProfiles.Sort()


                    Helpers.Logger.WriteMessageRelative("ok", 2)
                Catch ex As Exception
                    Helpers.Logger.WriteErrorRelative("FATAL ERROR IN GetAllFirefoxProfiles()", 3)
                    Helpers.Logger.WriteErrorRelative(ex.Message, 4)

                    Return False
                End Try

                Return True
            End Function


            Public Shared Function Save(_profile As Cleaners.FireFox.ProfileData) As Boolean
                Return Save(_profile, False, My.Application.Info.DirectoryPath, 0)
            End Function

            Public Shared Function Save(_profile As Cleaners.FireFox.ProfileData, skipProfilesIni As Boolean, savedProfilesBasePath As String, logDepth As Integer) As Boolean
                Helpers.Logger.WriteMessageRelative("saving default profile", 1 + logDepth)

                Try
                    If Not IO.Directory.Exists(Properties.ProfileRootPath) Then
                        Helpers.Errors.Add("Folder '" & Properties.ProfileRootPath & "' does not exist")
                        Throw New Exception("Folder '" & Properties.ProfileRootPath & "' does not exist")
                    End If

                    If Not IO.File.Exists(Properties.ProfilesIni) Then
                        Helpers.Errors.Add("File '" & Properties.ProfilesIni & "' does not exist")
                        Throw New Exception("File '" & Properties.ProfilesIni & "' does not exist")
                    End If


                    Helpers.Logger.WriteMessageRelative("ini file", 2 + logDepth)

                    If skipProfilesIni Then
                        Helpers.Logger.WriteMessageRelative("skipped", 3 + logDepth)
                    Else
                        Helpers.Logger.WriteMessageRelative(Properties.ProfilesIni, 3 + logDepth)

                        Dim _inifile As New Helpers.FilesAndFolders.IniFile(Properties.ProfilesIni)

                        Helpers.Logger.WriteMessageRelative("iterating", 2 + logDepth)
                        For Each _s As Helpers.FilesAndFolders.IniFile.Section In _inifile.Sections
                            If _s.Name.StartsWith("Install") Then
                                Helpers.Logger.WriteMessageRelative("found install section", 3 + logDepth)

                                For Each _k As Helpers.FilesAndFolders.IniFile.Key In _s.Keys
                                    If _k.Name = "Default" Then
                                        Helpers.Logger.WriteMessageRelative("setting default profile", 4 + logDepth)

                                        Helpers.Logger.WriteMessageRelative("current value", 5 + logDepth)
                                        Helpers.Logger.WriteMessageRelative(_k.Value, 6 + logDepth)

                                        Helpers.Logger.WriteMessageRelative("removing old default", 5 + logDepth)

                                        _s.Keys.Remove(_k)

                                        Helpers.Logger.WriteMessageRelative("ok", 6 + logDepth)


                                        Dim __k As New Helpers.FilesAndFolders.IniFile.Key()

                                        __k.Name = "Default"
                                        __k.Value = _profile.Path
                                        __k.Comments = ""

                                        Helpers.Logger.WriteMessageRelative("new value", 5 + logDepth)
                                        Helpers.Logger.WriteMessageRelative(__k.Value, 6 + logDepth)

                                        Helpers.Logger.WriteMessageRelative("adding new default", 5 + logDepth)

                                        _s.Keys.Add(__k)

                                        Helpers.Logger.WriteMessageRelative("ok", 6 + logDepth)

                                        Exit For
                                    End If
                                Next
                            End If
                        Next

                        _inifile.SaveFile(False, True)
                    End If

                    '----------------------------------------------
                    Helpers.Logger.WriteMessageRelative("copying profile", 2 + logDepth)


                    Dim Path__Destination As String

                    If Not IO.Directory.Exists(savedProfilesBasePath) Then
                        Helpers.Errors.Add("Base path for destination not found ('" & savedProfilesBasePath & "')")
                        Throw New Exception("Base path for destination not found ('" & savedProfilesBasePath & "')")
                    End If

                    'Path__Destination = "C:\Program Files (x86)\GuestTek\LogoutCleanupHelper\" & Constants.SavedProfilePath
                    'Path__Destination = My.Application.Info.DirectoryPath & "\" & Constants.SavedProfilePath
                    Path__Destination = IO.Path.Combine(savedProfilesBasePath, Constants.SavedProfilePath)
                    Path__Destination = Path__Destination.Replace("/", "\")
                    Path__Destination = Path__Destination.Replace("\\", "\")
                    'sPath = sPath.Replace("%%TIMESTAMP%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))
                    Path__Destination &= "\" & Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


                    Helpers.Logger.WriteMessageRelative(Path__Destination, 2 + logDepth)

                    If Not IO.Directory.Exists(Path__Destination) Then
                        IO.Directory.CreateDirectory(Path__Destination)
                    End If


                    If IO.Directory.Exists(_profile.FullPath) And IO.Directory.Exists(Path__Destination) Then
                        Helpers.Logger.WriteMessageRelative("copying", 2 + logDepth)
                        Helpers.Logger.WriteMessageRelative("source", 3 + logDepth)
                        Helpers.Logger.WriteMessageRelative(_profile.FullPath, 4 + logDepth)
                        Helpers.Logger.WriteMessageRelative("destination", 3 + logDepth)
                        Helpers.Logger.WriteMessageRelative(Path__Destination, 4 + logDepth)

                        Dim _CopyDirectory As New Helpers.FilesAndFolders.CopyFiles

                        _CopyDirectory.SkipBackup = True
                        _CopyDirectory.SuppressCopyLogging = True
                        _CopyDirectory.SourceDirectory = _profile.FullPath
                        _CopyDirectory.DestinationDirectory = Path__Destination
                        _CopyDirectory.LogDepth = logDepth
                        _CopyDirectory.CopyFiles()

                        Helpers.Logger.WriteMessageRelative("ok", 4 + logDepth)
                        Helpers.Logger.WriteMessageRelative("files found: " & _CopyDirectory.FileCount_total.ToString, 5 + logDepth)
                        Helpers.Logger.WriteMessageRelative("files copied: " & _CopyDirectory.FileCount_copied.ToString, 5 + logDepth)
                        Helpers.Logger.WriteMessageRelative("files failed: " & _CopyDirectory.FileCount_failed.ToString, 5 + logDepth)
                    Else
                        Helpers.Errors.Add("source or destination path not found")

                        If Not IO.Directory.Exists(_profile.FullPath) Then
                            Helpers.Logger.WriteErrorRelative("source path not found", 2 + logDepth)
                            Helpers.Logger.WriteErrorRelative(_profile.FullPath, 3 + logDepth)
                        End If
                        If Not IO.Directory.Exists(Path__Destination) Then
                            Helpers.Logger.WriteErrorRelative("destination path not found", 2 + logDepth)
                            Helpers.Logger.WriteErrorRelative(Path__Destination, 3 + logDepth)
                        End If
                    End If


                    '----------------------------------------------
                    Helpers.Logger.WriteMessageRelative("saving profile details to registry", 2 + logDepth)

                    Helpers.Logger.WriteMessageRelative(Constants.RegistryKeys.KEY__Cleaners_Firefox, 3 + logDepth)

                    Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Cleaners_Firefox, Constants.RegistryKeys.VALUE__Cleaners_Firefox__name, _profile.Name)
                    Helpers.Logger.WriteMessageRelative(Constants.RegistryKeys.VALUE__Cleaners_Firefox__name, 4 + logDepth)
                    Helpers.Logger.WriteMessageRelative(_profile.Name, 5 + logDepth)

                    Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Cleaners_Firefox, Constants.RegistryKeys.VALUE__Cleaners_Firefox__path, _profile.Path)
                    Helpers.Logger.WriteMessageRelative(Constants.RegistryKeys.VALUE__Cleaners_Firefox__path, 4 + logDepth)
                    Helpers.Logger.WriteMessageRelative(_profile.Path, 5 + logDepth)
                Catch ex As Exception
                    Helpers.Logger.WriteErrorRelative("error", 1 + logDepth)
                    Helpers.Logger.WriteErrorRelative(ex.GetType().ToString & ": " & ex.Message, 2 + logDepth)


                    If ex.Data.Count > 0 Then
                        Helpers.Logger.WriteErrorRelative("more details", 3 + logDepth)

                        For Each _entry As DictionaryEntry In ex.Data
                            Helpers.Logger.WriteErrorRelative("data entry", 4 + logDepth)
                            Helpers.Logger.WriteErrorRelative("key", 5 + logDepth)
                            Helpers.Logger.WriteErrorRelative(_entry.Key.ToString(), 6 + logDepth)
                            Helpers.Logger.WriteErrorRelative("key", 5 + logDepth)
                            Helpers.Logger.WriteErrorRelative(_entry.Value, 6 + logDepth)
                        Next
                    End If

                    Return False
                End Try


                Return True
            End Function

            Public Shared Function Clean() As Boolean
                If Not GetAllFirefoxProfiles() Then
                    Helpers.Errors.Add("Fatal error in GetAllFirefoxProfiles()")

                    Return False
                End If

                Dim DefaultProfileFromRegistry_name As String, DefaultProfileFromRegistry_path As String, DefaultProfile As New ProfileData

                DefaultProfileFromRegistry_name = Helpers.Registry.GetValue_String(
                    Constants.RegistryKeys.KEY__Cleaners_Firefox,
                    Constants.RegistryKeys.VALUE__Cleaners_Firefox__name,
                    "")
                DefaultProfileFromRegistry_path = Helpers.Registry.GetValue_String(
                    Constants.RegistryKeys.KEY__Cleaners_Firefox,
                    Constants.RegistryKeys.VALUE__Cleaners_Firefox__path,
                    "")


                If Not Properties.AvailableProfiles.GetDefault(DefaultProfile) Then
                    Helpers.Errors.Add("No default profile found!")

                    Return False
                End If


                If Not DefaultProfileFromRegistry_path.Equals(DefaultProfile.Path) Then
                    Helpers.Logger.WriteWarningRelative("Paths don't match!", 1)
                    Helpers.Logger.WriteWarningRelative("Default profile from registry is not the same as in profiles.ini !!!", 2)
                End If

                Try
                    Helpers.Logger.WriteMessageRelative("cleaning", 1)
                    Helpers.Logger.WriteMessageRelative(DefaultProfile.FullPath, 2)

                    Helpers.FilesAndFolders.Folder.CleanFolder(DefaultProfile.FullPath, "I_am_sure")


                    Dim Path_Source As String, Path_Destination As String

                    Path_Source = Helpers.FilesAndFolders.GetNewestFolder(My.Application.Info.DirectoryPath & "\" & Constants.SavedProfilePath)
                    Path_Destination = DefaultProfile.FullPath

                    Helpers.Logger.WriteMessageRelative("copying", 1)
                    Helpers.Logger.WriteMessageRelative("source", 2)
                    Helpers.Logger.WriteMessageRelative(Path_Source, 3)
                    Helpers.Logger.WriteMessageRelative("destination", 2)
                    Helpers.Logger.WriteMessageRelative(Path_Destination, 3)


                    Dim _CopyDirectory As New Helpers.FilesAndFolders.CopyFiles

                    _CopyDirectory.SkipBackup = True
                    _CopyDirectory.SuppressCopyLogging = True
                    _CopyDirectory.SourceDirectory = Path_Source
                    _CopyDirectory.DestinationDirectory = Path_Destination
                    _CopyDirectory.CopyFiles()
                Catch ex As Exception
                    Helpers.Errors.Add(ex.Message)

                    Return False
                End Try

                Return True
            End Function
        End Class
    End Class

    'Public Class Firefox_OLD
    '    Public Class FirefoxProfile
    '        Public Class Profile
    '            Private mIndex As Integer
    '            Public Property Index() As Integer
    '                Get
    '                    Return mIndex
    '                End Get
    '                Set(ByVal value As Integer)
    '                    mIndex = value
    '                End Set
    '            End Property

    '            Private mName As String
    '            Public Property Name() As String
    '                Get
    '                    Return mName
    '                End Get
    '                Set(ByVal value As String)
    '                    mName = value
    '                End Set
    '            End Property

    '            Private mPath As String
    '            Public Property Path() As String
    '                Get
    '                    Return mPath
    '                End Get
    '                Set(ByVal value As String)
    '                    mPath = value
    '                End Set
    '            End Property

    '            Private mFullPath As String
    '            Public Property FullPath() As String
    '                Get
    '                    Return mFullPath
    '                End Get
    '                Set(ByVal value As String)
    '                    mFullPath = value
    '                End Set
    '            End Property

    '            Private mDate As Date
    '            Public Property [Date]() As Date
    '                Get
    '                    Return mDate
    '                End Get
    '                Set(ByVal value As Date)
    '                    mDate = value
    '                End Set
    '            End Property

    '            Private mSize As Long
    '            Public Property Size() As Long
    '                Get
    '                    Return mSize
    '                End Get
    '                Set(ByVal value As Long)
    '                    mSize = value
    '                End Set
    '            End Property
    '        End Class

    '        Private Shared mProfileRootPath As String
    '        Public Shared Property ProfileRootPath() As String
    '            Get
    '                Return mProfileRootPath
    '            End Get
    '            Set(ByVal value As String)
    '                mProfileRootPath = value
    '            End Set
    '        End Property

    '        Private Shared mProfilesIni As String
    '        Public Shared Property ProfilesIni() As String
    '            Get
    '                Return mProfilesIni
    '            End Get
    '            Set(ByVal value As String)
    '                mProfilesIni = value
    '            End Set
    '        End Property

    '        Private Shared mActiveProfilePath As String
    '        Public Shared ReadOnly Property ActiveProfilePath() As String
    '            Get
    '                Return mActiveProfilePath
    '            End Get
    '        End Property

    '        Private Shared mLastError As String
    '        Public Shared Property LastError() As String
    '            Get
    '                Return mLastError
    '            End Get
    '            Set(ByVal value As String)
    '                mLastError = value
    '            End Set
    '        End Property

    '        Private Shared mUserName As String
    '        Public Shared Property Username() As String
    '            Get
    '                Return mUserName
    '            End Get
    '            Set(ByVal value As String)
    '                mUserName = value
    '            End Set
    '        End Property



    '        'Typical profiles.ini looks like this
    '        '    [General]
    '        'StartWithLastProfile=1

    '        '[Profile0]
    '        'Name=Wouter_main_profile
    '        'IsRelative=1
    '        'Path=Profiles/j98xt73i.default
    '        'Default=1

    '        '[Profile1]
    '        'Name=test_profile
    '        'IsRelative=1
    '        'Path=Profiles/ahbw0lkv.test_profile

    '        Private Shared mProfiles As New List(Of Profile)

    '        Public Shared Iterator Function GetProfiles() As IEnumerable(Of Profile)
    '            Dim counter As Integer = -1

    '            If mProfiles.Count > 0 Then
    '                While counter < mProfiles.Count
    '                    counter += 1

    '                    If counter < mProfiles.Count Then
    '                        Yield mProfiles.Item(counter)
    '                    End If
    '                End While
    '            End If
    '        End Function


    '        Public Shared Function GetAllFirefoxProfiles() As Boolean
    '            Helpers.Logger.WriteMessageRelative("looking for profiles", 1)

    '            Helpers.Logger.WriteMessageRelative("getting some constants", 2)
    '            LoadFirefoxStuffConstants()


    '            mProfiles = New List(Of Profile)

    '            Dim sProfileRootPath As String = mProfileRootPath.Replace("%%%USERNAME%%%", mUserName)
    '            Dim sLine As String = "", sTmp As String = "", iTmp As Integer = -1
    '            Dim _profile As New Profile


    '            Helpers.Logger.WriteMessageRelative("root path", 2)
    '            Helpers.Logger.WriteMessageRelative(sProfileRootPath, 3)

    '            Try
    '                If Not IO.Directory.Exists(sProfileRootPath) Then
    '                    Throw New Exception("Folder '" & sProfileRootPath & "' does not exist")
    '                End If

    '                If Not IO.File.Exists(sProfileRootPath & "\" & mProfilesIni) Then
    '                    Throw New Exception("File '" & sProfileRootPath & "\" & mProfilesIni & "' does not exist")
    '                End If


    '                Helpers.Logger.WriteMessageRelative("ini file", 2)
    '                Helpers.Logger.WriteMessageRelative(IO.Path.Combine(sProfileRootPath, mProfilesIni), 3)

    '                Dim _inifile As New Helpers.FilesAndFolders.IniFile(IO.Path.Combine(sProfileRootPath, mProfilesIni))

    '                Helpers.Logger.WriteMessageRelative("iterating", 2)
    '                For Each _s As Helpers.FilesAndFolders.IniFile.Section In _inifile.Sections
    '                    If _s.Name.StartsWith("Profile") Then
    '                        Helpers.Logger.WriteMessageRelative("found profile", 3)

    '                        _profile = New Profile

    '                        sTmp = _s.Name.Replace("Profile", "")
    '                        If Integer.TryParse(sTmp, iTmp) Then
    '                            _profile.Index = iTmp
    '                        Else
    '                            _profile.Index = -1
    '                        End If

    '                        Helpers.Logger.WriteMessageRelative("index", 4)
    '                        Helpers.Logger.WriteMessageRelative(_profile.Index, 5)


    '                        For Each _k As Helpers.FilesAndFolders.IniFile.Key In _s.Keys
    '                            Select Case _k.Name
    '                                Case "Name"
    '                                    _profile.Name = _k.Value
    '                                    Helpers.Logger.WriteMessageRelative("name", 4)
    '                                    Helpers.Logger.WriteMessageRelative(_k.Value, 5)
    '                                Case "Path"
    '                                    _profile.Path = _k.Value
    '                                    Helpers.Logger.WriteMessageRelative("path", 4)
    '                                    Helpers.Logger.WriteMessageRelative(_k.Value, 5)

    '                                    Helpers.Logger.WriteMessageRelative("full path", 4)
    '                                    Try
    '                                        _profile.FullPath = IO.Path.Combine(sProfileRootPath, _k.Value).Replace("/", "\")
    '                                        Helpers.Logger.WriteMessageRelative(_profile.FullPath, 5)
    '                                    Catch ex As Exception
    '                                        _profile.FullPath = ""
    '                                        Helpers.Logger.WriteMessageRelative("error", 5)
    '                                        Helpers.Logger.WriteMessageRelative(ex.Message, 6)
    '                                    End Try
    '                            End Select
    '                        Next


    '                        Helpers.Logger.WriteMessageRelative("folder size and last modified date", 4)
    '                        If Not _profile.FullPath.Equals("") Then
    '                            If IO.Directory.Exists(_profile.FullPath) Then
    '                                Helpers.Logger.WriteMessageRelative("folder size", 5)

    '                                _profile.Size = Helpers.FilesAndFolders.Folder.DirectorySize(_profile.FullPath, True)

    '                                Helpers.Logger.WriteMessageRelative(_profile.Size & " (" & Helpers.FilesAndFolders.FileSize.GetHumanReadable(_profile.Size) & ")", 6)


    '                                Helpers.Logger.WriteMessageRelative("date", 5)
    '                                Try
    '                                    Dim sNewestFile As String = Helpers.FilesAndFolders.GetNewestFile(_profile.FullPath)
    '                                    Dim infoFile As New IO.FileInfo(sNewestFile)

    '                                    _profile.Date = infoFile.LastWriteTime
    '                                    Helpers.Logger.WriteMessageRelative(_profile.Date.ToString, 6)
    '                                Catch ex As Exception
    '                                    _profile.Date = New Date(0)
    '                                    Helpers.Logger.WriteMessageRelative("error", 6)
    '                                    Helpers.Logger.WriteMessageRelative(ex.Message, 7)
    '                                End Try
    '                            Else
    '                                Helpers.Logger.WriteMessageRelative("path not found!", 6)
    '                            End If
    '                        Else
    '                            Helpers.Logger.WriteMessageRelative("path empty!", 6)
    '                        End If


    '                        mProfiles.Add(_profile)
    '                    End If
    '                Next

    '                Helpers.Logger.WriteMessageRelative("ok", 2)
    '            Catch ex As Exception
    '                Helpers.Logger.WriteErrorRelative("FATAL ERROR IN GetAllFirefoxProfiles()", 3)
    '                Helpers.Logger.WriteErrorRelative(ex.Message, 4)

    '                Return False
    '            End Try

    '            Return True
    '        End Function

    '        Public Shared Function FindActiveFirefoxProfile() As Boolean
    '            Dim sPath As String = mProfileRootPath.Replace("%%%USERNAME%%%", mUserName)
    '            Dim sLine As String = "", sTmp As String = ""
    '            Dim bRet As Boolean = False

    '            mActiveProfilePath = ""

    '            Helpers.Logger.WriteMessageRelative("search path", 1)
    '            Helpers.Logger.WriteMessageRelative(sPath, 2)

    '            Try

    '                'Dir does not exist
    '                If Not IO.Directory.Exists(sPath) Then
    '                    Throw New Exception("Folder '" & sPath & "' does not exist")
    '                End If

    '                If Not IO.File.Exists(sPath & "\" & mProfilesIni) Then
    '                    Throw New Exception("File '" & sPath & "\" & mProfilesIni & "' does not exist")
    '                End If

    '                Helpers.Logger.WriteMessageRelative("search through file", 1)
    '                Helpers.Logger.WriteMessageRelative(sPath & "\" & mProfilesIni, 2)

    '                Using sr As New IO.StreamReader(sPath & "\" & mProfilesIni)
    '                    Do While sr.Peek() <> -1
    '                        sLine = sr.ReadLine()
    '                        If Not sTmp.Equals("") Then
    '                            If sLine.Contains("Default=1") Then
    '                                mActiveProfilePath = sPath & "\" & sTmp

    '                                Helpers.Logger.WriteMessageRelative("found profile", 3)
    '                                Helpers.Logger.WriteMessageRelative(mActiveProfilePath, 4)

    '                                bRet = True
    '                                Exit Do
    '                            End If
    '                        End If
    '                        If sLine.Contains("Path=") Then
    '                            Helpers.Logger.WriteMessageRelative("found trigger line", 3)
    '                            Helpers.Logger.WriteMessageRelative(sLine, 4)

    '                            sTmp = sLine.Replace("Path=", "")
    '                        End If
    '                    Loop
    '                End Using

    '                mActiveProfilePath = mActiveProfilePath.Replace("/", "\")
    '                mActiveProfilePath = mActiveProfilePath.Replace("\\", "\")
    '            Catch ex As Exception
    '                bRet = False
    '                mLastError = ex.Message

    '                Helpers.Logger.WriteErrorRelative("error", 1)
    '                Helpers.Logger.WriteErrorRelative(ex.Message, 2)
    '            End Try

    '            Return bRet
    '        End Function
    '    End Class

    '    Private Shared ReadOnly FirefoxAppDataPath As String = "C:\Users\%%%USERNAME%%%\AppData\Roaming\Mozilla\Firefox\"
    '    Private Shared ReadOnly FirefoxProfiles_ini As String = "profiles.ini"

    '    'Public Shared ReadOnly SavedProfilePath As String = "saved_profile\%%TIMESTAMP%%"
    '    Private Shared ReadOnly SavedProfilePath As String = "saved_profile"

    '    Private Shared Sub LoadFirefoxStuffConstants()
    '        FirefoxProfile.Username = "sitekiosk"
    '        FirefoxProfile.ProfileRootPath = FirefoxAppDataPath
    '        FirefoxProfile.ProfilesIni = FirefoxProfiles_ini
    '    End Sub

    '    Private Shared Function LoadFirefoxStuff() As Boolean
    '        FirefoxProfile.Username = "sitekiosk"
    '        FirefoxProfile.ProfileRootPath = FirefoxAppDataPath
    '        FirefoxProfile.ProfilesIni = FirefoxProfiles_ini

    '        If Not FirefoxProfile.FindActiveFirefoxProfile() Then
    '            Return False
    '        Else
    '            Return True
    '        End If
    '    End Function

    '    Public Shared Function Clean() As Boolean
    '        If Not LoadFirefoxStuff() Then
    '            Helpers.Errors.Add("Firefox profile not found")

    '            Return False
    '        End If


    '        Dim sPath As String

    '        sPath = GetLatestSavedProfileFolder()

    '        If Not IO.Directory.Exists(sPath) Then
    '            Helpers.Errors.Add("none found in '" & My.Application.Info.DirectoryPath & "\" & SavedProfilePath & "'")

    '            Return False
    '        End If

    '        If Not IO.Directory.Exists(FirefoxProfile.ActiveProfilePath) Then
    '            Helpers.Errors.Add("active firefox profile '" & FirefoxProfile.ActiveProfilePath & "' does not exist")

    '            Return False
    '        End If


    '        Try
    '            Helpers.Logger.WriteMessageRelative("cleaning", 1)
    '            Helpers.Logger.WriteMessageRelative(FirefoxProfile.ActiveProfilePath, 2)

    '            CleanDirectory(FirefoxProfile.ActiveProfilePath)


    '            Helpers.Logger.WriteMessageRelative("copying", 1)
    '            Helpers.Logger.WriteMessageRelative(sPath & " ==> " & FirefoxProfile.ActiveProfilePath, 2)

    '            My.Computer.FileSystem.CopyDirectory(sPath, FirefoxProfile.ActiveProfilePath, True)
    '        Catch ex As Exception
    '            Helpers.Errors.Add(ex.Message)

    '            Return False
    '        End Try

    '        Return True
    '    End Function

    '    Public Shared Function Save() As Boolean
    '        If Not LoadFirefoxStuff() Then
    '            Helpers.Errors.Add("Firefox profile not found")

    '            Return False
    '        End If


    '        Try
    '            Helpers.Logger.WriteMessageRelative("creating profile folder copy", 1)

    '            Dim sPath As String

    '            sPath = My.Application.Info.DirectoryPath & "\" & SavedProfilePath
    '            sPath = sPath.Replace("/", "\")
    '            sPath = sPath.Replace("\\", "\")
    '            'sPath = sPath.Replace("%%TIMESTAMP%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))
    '            sPath &= "\" & Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

    '            Helpers.Logger.WriteMessageRelative(sPath, 2)

    '            If Not IO.Directory.Exists(sPath) Then
    '                IO.Directory.CreateDirectory(sPath)
    '            End If


    '            If IO.Directory.Exists(FirefoxProfile.ActiveProfilePath) And IO.Directory.Exists(sPath) Then
    '                Helpers.Logger.WriteMessageRelative("copying", 1)
    '                Helpers.Logger.WriteMessageRelative(FirefoxProfile.ActiveProfilePath & " ==> " & sPath, 2)

    '                My.Computer.FileSystem.CopyDirectory(FirefoxProfile.ActiveProfilePath, sPath, True)
    '            End If
    '        Catch ex As Exception
    '            Helpers.Logger.WriteMessageRelative("error", 1)
    '            Helpers.Logger.WriteMessageRelative(ex.Message, 2)

    '            Return False
    '        End Try

    '        Return True
    '    End Function


    '    Private Shared Function GetLatestSavedProfileFolder() As String
    '        Return Helpers.FilesAndFolders.GetNewestFolder(My.Application.Info.DirectoryPath & "\" & SavedProfilePath)
    '    End Function

    '    Private Shared Sub CleanDirectory(dir As String)
    '        For Each d In IO.Directory.GetDirectories(dir)
    '            IO.Directory.Delete(d, True)
    '        Next

    '        For Each f In IO.Directory.GetFiles(dir)
    '            IO.File.Delete(f)
    '        Next
    '    End Sub

    'End Class
End Class
