﻿Imports Microsoft.Win32

Module Main
    Public Sub Main()
        Helpers.Logger.InitialiseLogger(Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\LogoutCleanupHelper\_logs", False)
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)


        'Kill al previous instances, if any
        Helpers.Logger.WriteMessage("Killing previous instances", 0)
        Dim prevInstances As Integer = 0
        prevInstances = Helpers.Processes.KillPreviousInstances()
        If prevInstances < 0 Then
            Helpers.Logger.WriteError("failed", 1)
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
        Else
            If prevInstances > 0 Then
                Helpers.Logger.WriteMessage(prevInstances.ToString & " killed", 1)
            Else
                Helpers.Logger.WriteMessage("none found", 1)
            End If
        End If


        '-----------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Clean Office", 0)
        If Settings.ReadWrite.Cleaners.Office Then
            If Helpers.Generic.IsMSOfficeInstalled Then
                Cleaners.Office.Clean()
            Else
                Helpers.Logger.WriteMessage("office not found, skipped", 1)
            End If
        Else
            Helpers.Logger.WriteMessage("skipped, not enabled", 1)
        End If


        '-----------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Clean Firefox", 0)
        If Settings.ReadWrite.Cleaners.Firefox Then
            If Helpers.Generic.IsFirefoxInstalled Then
                Dim iInfinityProtection As Integer = 0, iInstances As Integer = 0

                Helpers.Logger.WriteMessage("Killing Firefox", 1)
                Do
                    iInstances += Helpers.Processes.KillProcesses("firefox")

                    iInfinityProtection += 1

                    Threading.Thread.Sleep(2000)
                Loop While Helpers.Processes.IsProcessRunning("firefox") And iInfinityProtection < 10

                If iInfinityProtection = 1 Then
                    Helpers.Logger.WriteMessage("killed " & iInstances.ToString & " in " & iInfinityProtection.ToString & " loop", 2)
                Else
                    Helpers.Logger.WriteMessage("killed " & iInstances.ToString & " in " & iInfinityProtection.ToString & " loops", 2)
                End If


                Helpers.Logger.WriteMessage("cleaning", 1)

                Cleaners.FireFox.Properties.Username = "sitekiosk"

                If Not Cleaners.FireFox.Functions.Clean() Then
                    Helpers.Logger.WriteError("error", 2)
                    Helpers.Logger.WriteError(Helpers.Errors.GetLast, 3)
                End If
            Else
                Helpers.Logger.WriteMessage("not found, skipped", 1)
            End If
        Else
            Helpers.Logger.WriteMessage("skipped, not enabled", 1)
        End If


        '-----------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Clean Print Queue", 0)
        If Settings.ReadWrite.Cleaners.PrintQueue Then
            If CleanersHelpers.PrinterQueue.IsPrinterQueueCleanerServiceRunning() Then
                Cleaners.PrintQueue.Clean()
            Else
                Helpers.Logger.WriteError("cleaner service not found, skipped", 1)
                Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
            End If
        Else
            Helpers.Logger.WriteMessage("skipped, not enabled", 1)
        End If


        '-----------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Done", 0)
        Helpers.Logger.WriteMessage("ok bye", 1)
    End Sub
End Module
