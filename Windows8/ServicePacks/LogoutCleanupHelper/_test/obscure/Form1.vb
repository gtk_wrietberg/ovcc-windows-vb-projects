﻿Public Class Form1
    Private Sub btnObscure_Click(sender As Object, e As EventArgs) Handles btnObscure.Click
        txtObscure.Text = Helpers.XOrObfuscation_v2.Obfuscate(txtText.Text)
    End Sub

    Private Sub btnText_Click(sender As Object, e As EventArgs) Handles btnText.Click
        txtText.Text = Helpers.XOrObfuscation_v2.Deobfuscate(txtObscure.Text)
    End Sub
End Class
