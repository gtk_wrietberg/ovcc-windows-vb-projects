﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnObscure = New System.Windows.Forms.Button()
        Me.txtObscure = New System.Windows.Forms.TextBox()
        Me.txtText = New System.Windows.Forms.TextBox()
        Me.btnText = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnObscure
        '
        Me.btnObscure.Location = New System.Drawing.Point(98, 154)
        Me.btnObscure.Name = "btnObscure"
        Me.btnObscure.Size = New System.Drawing.Size(150, 143)
        Me.btnObscure.TabIndex = 0
        Me.btnObscure.Text = "text -> obscure"
        Me.btnObscure.UseVisualStyleBackColor = True
        '
        'txtObscure
        '
        Me.txtObscure.Location = New System.Drawing.Point(98, 303)
        Me.txtObscure.Name = "txtObscure"
        Me.txtObscure.Size = New System.Drawing.Size(366, 20)
        Me.txtObscure.TabIndex = 1
        '
        'txtText
        '
        Me.txtText.Location = New System.Drawing.Point(98, 128)
        Me.txtText.Name = "txtText"
        Me.txtText.Size = New System.Drawing.Size(366, 20)
        Me.txtText.TabIndex = 2
        '
        'btnText
        '
        Me.btnText.Location = New System.Drawing.Point(314, 154)
        Me.btnText.Name = "btnText"
        Me.btnText.Size = New System.Drawing.Size(150, 143)
        Me.btnText.TabIndex = 3
        Me.btnText.Text = "obscure -> text"
        Me.btnText.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btnText)
        Me.Controls.Add(Me.txtText)
        Me.Controls.Add(Me.txtObscure)
        Me.Controls.Add(Me.btnObscure)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnObscure As Button
    Friend WithEvents txtObscure As TextBox
    Friend WithEvents txtText As TextBox
    Friend WithEvents btnText As Button
End Class
