﻿Module Main
    Private oCopyFiles As Helpers.FilesAndFolders.CopyFiles

    Public Sub Main()
        Initialise()

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Copy files", 0)

        Dim sAppPath As String = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\LogoutCleanupHelper\"

        oCopyFiles = New Helpers.FilesAndFolders.CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory
        oCopyFiles.SourceDirectory = "files"
        oCopyFiles.DestinationDirectory = sAppPath

        Helpers.Logger.WriteMessage("LogoutCleanupHelper", 1)
        oCopyFiles.CopyFiles()


        Dim sAppPath02 As String = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\PrintQueueCleanerService\"

        oCopyFiles = New Helpers.FilesAndFolders.CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory
        oCopyFiles.SourceDirectory = "files_PQCS"
        oCopyFiles.DestinationDirectory = sAppPath02

        Helpers.Logger.WriteMessage("PrintQueueCleanerService", 1)
        oCopyFiles.CopyFiles()


        '==================================================================================================================================================================================
        'Install PrintQueueCleanerService
        Helpers.Logger.WriteMessage("installing service", 0)
        Helpers.Logger.WriteMessage(Constants.c__SERVICE_Name, 1)

        Dim sStmp As String = ""

        If ServiceInstaller.ServiceIsInstalled(Constants.c__SERVICE_Name) Then
            Helpers.Logger.WriteMessage("already exists", 2)
            Helpers.Logger.WriteMessage("stopping", 3)
            sStmp = ServiceInstaller.StopService(Constants.c__SERVICE_Name)
            Helpers.Logger.WriteMessage(sStmp, 4)

            Threading.Thread.Sleep(15000)

            Helpers.Logger.WriteMessage("uninstalling", 3)
            sStmp = ServiceInstaller.Uninstall(Constants.c__SERVICE_Name)
            Helpers.Logger.WriteMessage(sStmp, 4)

            Threading.Thread.Sleep(15000)

            Helpers.Logger.WriteMessage("is uninstalled?", 2)
            If Not ServiceInstaller.ServiceIsInstalled(Constants.c__SERVICE_Name) Then
                Helpers.Logger.WriteMessage("ok", 4)
            Else
                Helpers.Logger.WriteError("fail", 4)
            End If

            Threading.Thread.Sleep(5000)
        End If


        Helpers.Logger.WriteMessage("installing", 2)
        ServiceInstaller.InstallService(Constants.c__SERVICE_Name, Constants.c__SERVICE_DisplayName, Constants.c__SERVICE_Path)

        Threading.Thread.Sleep(5000)


        Helpers.Logger.WriteMessage("set description", 2)
        ServiceInstaller.ChangeServiceDescription(Constants.c__SERVICE_Name, Constants.c__SERVICE_Description)

        Helpers.Logger.WriteMessage("installed?", 2)
        If ServiceInstaller.ServiceIsInstalled(Constants.c__SERVICE_Name) Then
            Helpers.Logger.WriteMessage("ok", 3)

            Threading.Thread.Sleep(5000)

            Helpers.Logger.WriteMessage("starting", 2)
            ServiceInstaller.StartService(Constants.c__SERVICE_Name)

            Threading.Thread.Sleep(10000)
        Else
            Helpers.Logger.WriteMessage("fail", 3)
        End If


        Dim tSS As ServiceProcess.ServiceControllerStatus = ServiceInstaller.GetServiceStatus(Constants.c__SERVICE_Name)
        Helpers.Logger.WriteMessage("is running?", 2)
        Helpers.Logger.WriteMessage("service state", 3)
        Helpers.Logger.WriteMessage(tSS.ToString, 4)
        If tSS = ServiceProcess.ServiceControllerStatus.Running Or tSS = ServiceProcess.ServiceControllerStatus.StartPending Then
            Helpers.Logger.WriteMessage("ok", 3)
        Else
            Helpers.Logger.WriteError("FAIL", 3)
        End If


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Registry", 0)
        Helpers.Logger.WriteMessage(Constants.RegistryKeys.KEY__PQCS, , 1)
        Helpers.Logger.Write(Constants.RegistryKeys.VALUE__PQCS_State, , 2)
        Helpers.Logger.Write(Constants.RegistryKeys.DEFAULTVALUE__PQCS_State, , 3)
        Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__PQCS, Constants.RegistryKeys.VALUE__PQCS_State, Constants.RegistryKeys.DEFAULTVALUE__PQCS_State)


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Settings", 0)

        Helpers.Logger.WriteMessage("Firefox cleaning", 1)
        Settings.ReadWrite.Cleaners.Firefox = True
        If Settings.ReadWrite.Cleaners.Firefox Then
            Helpers.Logger.WriteMessage("on", 1)
        Else
            Helpers.Logger.WriteMessage("off", 1)
        End If

        Helpers.Logger.WriteMessage("Office cleaning", 1)
        Settings.ReadWrite.Cleaners.Office = True
        If Settings.ReadWrite.Cleaners.Office Then
            Helpers.Logger.WriteMessage("on", 1)
        Else
            Helpers.Logger.WriteMessage("off", 1)
        End If

        Helpers.Logger.WriteMessage("Print Queue cleaning", 1)
        Settings.ReadWrite.Cleaners.PrintQueue = True
        If Settings.ReadWrite.Cleaners.PrintQueue Then
            Helpers.Logger.WriteMessage("on", 1)
        Else
            Helpers.Logger.WriteMessage("off", 1)
        End If


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Open up _logs subfolder, so the sitekiosk user can write logs to it.", 0)
        If IO.Directory.Exists(sAppPath & "_logs") Then
            If Helpers.FilesAndFolders.Permissions.FolderSecurity_REPLACE__Full_Everyone(sAppPath & "_logs") Then
                Helpers.Logger.WriteMessage("ok", 1)
            Else
                Helpers.Logger.WriteError("Error while changing the ACL", 1)
                Helpers.Logger.WriteError(Helpers.Errors.GetLast, 2)
            End If
        Else
            Helpers.Logger.WriteError("Folder not found!", 1)
        End If


        '==================================================================================================================================================================================
        'We need to update all logout.html files
        Helpers.Logger.WriteMessage("Updating logout.html files", 0)

        Dim logoutFiles As New List(Of String)

        logoutFiles.Add(Helpers.FilesAndFolders.GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\Hilton\logout.html")
        logoutFiles.Add(Helpers.FilesAndFolders.GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\Guest-tek\logout.html")
        logoutFiles.Add(Helpers.FilesAndFolders.GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\GuestTek\logout.html")
        logoutFiles.Add(Helpers.FilesAndFolders.GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\iBAHN\logout.html")


        Helpers.Logger.WriteMessage("loading patch", 1)
        Helpers.Logger.WriteMessage(PATCH_PATH, 2)

        If Not IO.File.Exists(PATCH_PATH) Then
            Helpers.Logger.WriteError("file not found", 3)
        Else
            PATCH_CONTENTS = New List(Of String)

            Try
                Using sr As New IO.StreamReader(PATCH_PATH)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("%%%PROGRAM_FILES%%%") Then
                            Helpers.Logger.WriteMessage("patching patch", 3)
                            line = line.Replace("%%%PROGRAM_FILES%%%", Helpers.FilesAndFolders.GetProgramFilesFolder())
                            line = line.Replace("\", "/")
                        End If

                        PATCH_CONTENTS.Add(line)
                    End While
                End Using

                Helpers.Logger.WriteMessage("ok", 3)
            Catch ex As Exception
                Helpers.Logger.WriteError("error while reading patch", 3)
                Helpers.Logger.WriteError(ex.Message, 3)

                PATCH_CONTENTS = New List(Of String)
            End Try

            If PATCH_CONTENTS.Count > 0 Then
                For Each logoutFile As String In logoutFiles
                    UpdateLogoutHtml(logoutFile)
                Next
            Else
                Helpers.Logger.WriteError("patch appears empty", 3)
            End If
        End If


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Done", 0)
        Helpers.Logger.WriteMessage("ok", 1)
        Helpers.Logger.WriteMessage("bye", 2)
    End Sub


    Private ReadOnly PATCH_PATH As String = "patch.html.txt"
    Private PATCH_CONTENTS As New List(Of String)
    Private Function UpdateLogoutHtml(filePath As String) As Boolean
        Helpers.FilesAndFolders.Backup.File(filePath, g_BackupDirectory)


        Dim lines As New List(Of String)

        Helpers.Logger.WriteMessageRelative("updating", 1)
        Helpers.Logger.WriteMessageRelative(filePath, 2)

        If Not IO.File.Exists(filePath) Then
            Helpers.Logger.WriteErrorRelative("file not found", 3)

            Return False
        End If


        Try
            Helpers.Logger.WriteMessageRelative("patching", 1)

            Dim bPatchStarted As Boolean = False
            Dim bPatchEnded As Boolean = False

            Using sr As New IO.StreamReader(filePath)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine


                    'see if file contains first line of patch
                    If line.Contains(PATCH_CONTENTS.Item(0)) Then
                        bPatchStarted = True
                    End If


                    If Not bPatchStarted Or bPatchEnded Then
                        lines.Add(line)
                    End If


                    'see if file contains last line of patch
                    If line.Contains(PATCH_CONTENTS.Item(PATCH_CONTENTS.Count - 1)) Then
                        'extra check
                        If bPatchStarted And Not bPatchEnded Then
                            bPatchEnded = True

                            'time to write patch
                            lines.AddRange(PATCH_CONTENTS)
                        End If
                    End If
                End While
            End Using

            Helpers.Logger.WriteMessageRelative("writing", 1)
            Using sw As New IO.StreamWriter(filePath)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using

            Helpers.Logger.WriteMessageRelative("ok", 2)
            Return True
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("error", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 2)
            Return False
        End Try

    End Function
End Module
