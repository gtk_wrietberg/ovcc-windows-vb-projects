﻿Imports System.ComponentModel

Public Class frmMain
#Region "thread safe"
    Delegate Sub ThreadSafe_Callback__BlockControls(ByVal [enabled] As Boolean)

    Private Sub ThreadSafe__BlockControls(ByVal [enabled] As Boolean)
        If Me.progressCopy.InvokeRequired Then
            Dim d As New ThreadSafe_Callback__BlockControls(AddressOf ThreadSafe__BlockControls)
            Me.Invoke(d, New Object() {[enabled]})
        Else
            btnSave.Enabled = Not [enabled]
            btnSave.Visible = Not [enabled]

            lstProfiles.Enabled = Not [enabled]

            If [enabled] Then
                progressCopy.Enabled = True
                progressCopy.Visible = True
                progressCopy.MarqueeAnimationSpeed = 10

                progressCopy.Top = btnSave.Top
                progressCopy.Left = btnSave.Left
            Else
                progressCopy.Enabled = False
                progressCopy.Visible = False
                progressCopy.MarqueeAnimationSpeed = 0

                progressCopy.Top = btnSave.Top + 640
                progressCopy.Left = btnSave.Left
            End If
        End If
    End Sub
#End Region


    Private mSelectedFirefoxProfile As New Cleaners.FireFox.ProfileData

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        bgworkerCopy.RunWorkerAsync()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.InitialiseLogger(Environment.CurrentDirectory & "\_logs", False)
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)

        Me.Size = New Size(612, 381)
        Helpers.Forms.TopMost(Me.Handle, True)
        '612; 325

        btnSave.Enabled = False


        'lstProfiles.Columns.Item(4).TextAlign = HorizontalAlignment.Center



        Cleaners.FireFox.Properties.Username = "sitekiosk"


        lstProfiles.Items.Clear()

        Cleaners.FireFox.Functions.GetAllFirefoxProfiles()

        For Each _profile As Cleaners.FireFox.ProfileData In Cleaners.FireFox.Properties.AvailableProfiles.Get
            Dim newSubItem As ListViewItem.ListViewSubItem
            Dim newItem As New ListViewItem(_profile.Name)


            newSubItem = New ListViewItem.ListViewSubItem
            newSubItem.Text = Helpers.FilesAndFolders.FileSize.GetHumanReadable(_profile.Size)
            newSubItem.Tag = ""
            newItem.SubItems.Add(newSubItem)

            newSubItem = New ListViewItem.ListViewSubItem
            newSubItem.Text = _profile.Date.ToString
            newSubItem.Tag = ""
            newItem.SubItems.Add(newSubItem)

            If _profile.Default Then
                newItem.Selected = True
            End If

            lstProfiles.Items.Add(newItem)
        Next

    End Sub

    Private Sub lstProfiles_ColumnWidthChanging(sender As Object, e As ColumnWidthChangingEventArgs) Handles lstProfiles.ColumnWidthChanging
        e.Cancel = True
        e.NewWidth = lstProfiles.Columns(e.ColumnIndex).Width
    End Sub

    Private Sub lstProfiles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstProfiles.SelectedIndexChanged
        If lstProfiles.SelectedItems.Count > 0 Then
            Dim iIndex As Integer = -1

            mSelectedFirefoxProfile = New Cleaners.FireFox.ProfileData
            If Cleaners.FireFox.Properties.AvailableProfiles.Find(lstProfiles.SelectedItems.Item(0).Text, mSelectedFirefoxProfile) Then
                btnSave.Enabled = True
            End If
        End If
    End Sub

    Private Sub bgworkerCopy_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworkerCopy.DoWork
        ThreadSafe__BlockControls(True)

        Helpers.Logger.WriteMessage("Saving Firefox profile", 0)

        If Not Cleaners.FireFox.Functions.Save(mSelectedFirefoxProfile) Then
            Helpers.Logger.WriteError("error", 1)
            Helpers.Logger.WriteError(Helpers.Errors.GetLast, 2)


            e.Result = 1 'error
            'MsgBox("There was an error, please check the log files!", MsgBoxStyle.Exclamation Or MsgBoxStyle.OkOnly)
        Else
            Helpers.Logger.WriteMessage("ok", 1)

            e.Result = 0
        End If
    End Sub

    Private Sub bgworkerCopy_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bgworkerCopy.ProgressChanged

    End Sub

    Private Sub bgworkerCopy_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworkerCopy.RunWorkerCompleted
        If e.Result = 0 Then
            Me.Close()
        Else
            MessageBox.Show(Helpers.Errors.GetLast, "An error occurred", MessageBoxButtons.OK, MessageBoxIcon.Error)

            ThreadSafe__BlockControls(False)
        End If
    End Sub
End Class
