﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lstProfiles = New System.Windows.Forms.ListView()
        Me.colheadName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colheadSize = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colheadDate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.progressCopy = New System.Windows.Forms.ProgressBar()
        Me.bgworkerCopy = New System.ComponentModel.BackgroundWorker()
        Me.picboxLogo = New System.Windows.Forms.PictureBox()
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(12, 280)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(572, 49)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save Firefox profile for SiteKiosk user"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lstProfiles
        '
        Me.lstProfiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colheadName, Me.colheadSize, Me.colheadDate})
        Me.lstProfiles.FullRowSelect = True
        Me.lstProfiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstProfiles.HideSelection = False
        Me.lstProfiles.Location = New System.Drawing.Point(12, 88)
        Me.lstProfiles.MultiSelect = False
        Me.lstProfiles.Name = "lstProfiles"
        Me.lstProfiles.Size = New System.Drawing.Size(572, 186)
        Me.lstProfiles.TabIndex = 5
        Me.lstProfiles.UseCompatibleStateImageBehavior = False
        Me.lstProfiles.View = System.Windows.Forms.View.Details
        '
        'colheadName
        '
        Me.colheadName.Text = "Name"
        Me.colheadName.Width = 270
        '
        'colheadSize
        '
        Me.colheadSize.Text = "Size"
        Me.colheadSize.Width = 87
        '
        'colheadDate
        '
        Me.colheadDate.Text = "Last modified date"
        Me.colheadDate.Width = 210
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(208, 12)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(376, 70)
        Me.lblTitle.TabIndex = 6
        Me.lblTitle.Text = "Select which Firefox profile to use as a template"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'progressCopy
        '
        Me.progressCopy.Location = New System.Drawing.Point(12, 363)
        Me.progressCopy.Name = "progressCopy"
        Me.progressCopy.Size = New System.Drawing.Size(572, 49)
        Me.progressCopy.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.progressCopy.TabIndex = 7
        '
        'bgworkerCopy
        '
        Me.bgworkerCopy.WorkerReportsProgress = True
        '
        'picboxLogo
        '
        Me.picboxLogo.BackColor = System.Drawing.Color.White
        Me.picboxLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picboxLogo.Image = Global.FirefoxProfileSave.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.picboxLogo.Location = New System.Drawing.Point(12, 12)
        Me.picboxLogo.Name = "picboxLogo"
        Me.picboxLogo.Size = New System.Drawing.Size(190, 70)
        Me.picboxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picboxLogo.TabIndex = 8
        Me.picboxLogo.TabStop = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(596, 424)
        Me.Controls.Add(Me.picboxLogo)
        Me.Controls.Add(Me.progressCopy)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.lstProfiles)
        Me.Controls.Add(Me.btnSave)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Firefox Profile Save"
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnSave As Button
    Friend WithEvents lstProfiles As ListView
    Friend WithEvents colheadName As ColumnHeader
    Friend WithEvents colheadDate As ColumnHeader
    Friend WithEvents lblTitle As Label
    Friend WithEvents colheadSize As ColumnHeader
    Friend WithEvents progressCopy As ProgressBar
    Friend WithEvents bgworkerCopy As System.ComponentModel.BackgroundWorker
    Friend WithEvents picboxLogo As PictureBox
End Class
