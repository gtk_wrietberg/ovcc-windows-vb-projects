﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.progressFirefox = New System.Windows.Forms.ProgressBar()
        Me.lblProgress_Speed = New System.Windows.Forms.Label()
        Me.lblProgress_Timeleft = New System.Windows.Forms.Label()
        Me.picboxLogo = New System.Windows.Forms.PictureBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.tmrInstaller = New System.Windows.Forms.Timer(Me.components)
        Me.pnlSmall_IsFirefoxInstalled = New System.Windows.Forms.Panel()
        Me.lblIsFirefoxInstalled_Result = New System.Windows.Forms.Label()
        Me.lblIsFirefoxInstalled = New System.Windows.Forms.Label()
        Me.pnlSmall_DownloadFirefox = New System.Windows.Forms.Panel()
        Me.lblDownloadFirefox_Result = New System.Windows.Forms.Label()
        Me.lblDownloadFirefox = New System.Windows.Forms.Label()
        Me.pnlSmall_InstallProfile = New System.Windows.Forms.Panel()
        Me.lblInstallProfile_Result = New System.Windows.Forms.Label()
        Me.lblInstallProfile = New System.Windows.Forms.Label()
        Me.pnlSmall_InstallFirefox = New System.Windows.Forms.Panel()
        Me.lblInstallFirefox_Result = New System.Windows.Forms.Label()
        Me.lblInstallFirefox = New System.Windows.Forms.Label()
        Me.pnlSmall_SaveProfile = New System.Windows.Forms.Panel()
        Me.lblSaveProfile_Result = New System.Windows.Forms.Label()
        Me.lblSaveProfile = New System.Windows.Forms.Label()
        Me.pnlSmall_VerifyProfile = New System.Windows.Forms.Panel()
        Me.lblVerifyProfile_Result = New System.Windows.Forms.Label()
        Me.lblVerifyProfile = New System.Windows.Forms.Label()
        Me.pnlLarge_IsFirefoxInstalled = New System.Windows.Forms.Panel()
        Me.lblIsFirefoxInstalledProgress = New System.Windows.Forms.Label()
        Me.lblLarge_IsFirefoxInstalled = New System.Windows.Forms.Label()
        Me.pnlLarge_DownloadFirefox = New System.Windows.Forms.Panel()
        Me.lblDownloadFirefox_URL = New System.Windows.Forms.Label()
        Me.lblLarge_DownloadFirefox = New System.Windows.Forms.Label()
        Me.pnlSmall_InitialiseFirefox = New System.Windows.Forms.Panel()
        Me.lblInitialiseFirefox_Result = New System.Windows.Forms.Label()
        Me.lblInitialiseFirefox = New System.Windows.Forms.Label()
        Me.pnlLarge_InitialiseFirefox = New System.Windows.Forms.Panel()
        Me.btnInitialiseFirefox_No = New System.Windows.Forms.Button()
        Me.lblInitialiseFirefox_Text = New System.Windows.Forms.Label()
        Me.btnInitialiseFirefox_Yes = New System.Windows.Forms.Button()
        Me.lblLarge_InitialiseFirefox = New System.Windows.Forms.Label()
        Me.pnlLarge_InstallFirefox = New System.Windows.Forms.Panel()
        Me.lblLarge_InstallFirefox = New System.Windows.Forms.Label()
        Me.pnlLarge_VerifyProfile = New System.Windows.Forms.Panel()
        Me.lblLarge_VerifyProfile = New System.Windows.Forms.Label()
        Me.pnlLarge_InstallProfile = New System.Windows.Forms.Panel()
        Me.lblLarge_InstallProfile = New System.Windows.Forms.Label()
        Me.pnlLarge_SaveProfile = New System.Windows.Forms.Panel()
        Me.lblLarge_SaveProfile = New System.Windows.Forms.Label()
        Me.pnlSmall_Done = New System.Windows.Forms.Panel()
        Me.lblDone_Result = New System.Windows.Forms.Label()
        Me.lblDone = New System.Windows.Forms.Label()
        Me.pnlLarge_done = New System.Windows.Forms.Panel()
        Me.lblLarge_Done = New System.Windows.Forms.Label()
        Me.pnlError = New System.Windows.Forms.Panel()
        Me.lblError_Result = New System.Windows.Forms.Label()
        Me.lblErrorMessage = New System.Windows.Forms.Label()
        Me.lblLarge_Error = New System.Windows.Forms.Label()
        Me.pnlLarge_UninstallFirefox = New System.Windows.Forms.Panel()
        Me.lblLarge_UninstallFirefox = New System.Windows.Forms.Label()
        Me.pnlSmall_UninstallFirefox = New System.Windows.Forms.Panel()
        Me.lblUninstallFirefox_Result = New System.Windows.Forms.Label()
        Me.lblUninstallFirefox = New System.Windows.Forms.Label()
        Me.pnlLarge_Initialise = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblLarge_Initialise = New System.Windows.Forms.Label()
        Me.pnlSmall_Initialise = New System.Windows.Forms.Panel()
        Me.lblInitialise_Result = New System.Windows.Forms.Label()
        Me.lblInitialise = New System.Windows.Forms.Label()
        Me.lblInitialiseFirefox_Timeout = New System.Windows.Forms.Label()
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlSmall_IsFirefoxInstalled.SuspendLayout()
        Me.pnlSmall_DownloadFirefox.SuspendLayout()
        Me.pnlSmall_InstallProfile.SuspendLayout()
        Me.pnlSmall_InstallFirefox.SuspendLayout()
        Me.pnlSmall_SaveProfile.SuspendLayout()
        Me.pnlSmall_VerifyProfile.SuspendLayout()
        Me.pnlLarge_IsFirefoxInstalled.SuspendLayout()
        Me.pnlLarge_DownloadFirefox.SuspendLayout()
        Me.pnlSmall_InitialiseFirefox.SuspendLayout()
        Me.pnlLarge_InitialiseFirefox.SuspendLayout()
        Me.pnlLarge_InstallFirefox.SuspendLayout()
        Me.pnlLarge_VerifyProfile.SuspendLayout()
        Me.pnlLarge_InstallProfile.SuspendLayout()
        Me.pnlLarge_SaveProfile.SuspendLayout()
        Me.pnlSmall_Done.SuspendLayout()
        Me.pnlLarge_done.SuspendLayout()
        Me.pnlError.SuspendLayout()
        Me.pnlLarge_UninstallFirefox.SuspendLayout()
        Me.pnlSmall_UninstallFirefox.SuspendLayout()
        Me.pnlLarge_Initialise.SuspendLayout()
        Me.pnlSmall_Initialise.SuspendLayout()
        Me.SuspendLayout()
        '
        'progressFirefox
        '
        Me.progressFirefox.BackColor = System.Drawing.Color.Gainsboro
        Me.progressFirefox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.progressFirefox.Location = New System.Drawing.Point(6, 113)
        Me.progressFirefox.Name = "progressFirefox"
        Me.progressFirefox.Size = New System.Drawing.Size(564, 32)
        Me.progressFirefox.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressFirefox.TabIndex = 1
        '
        'lblProgress_Speed
        '
        Me.lblProgress_Speed.Location = New System.Drawing.Point(3, 95)
        Me.lblProgress_Speed.Name = "lblProgress_Speed"
        Me.lblProgress_Speed.Size = New System.Drawing.Size(181, 18)
        Me.lblProgress_Speed.TabIndex = 2
        Me.lblProgress_Speed.Text = "download speed"
        Me.lblProgress_Speed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblProgress_Timeleft
        '
        Me.lblProgress_Timeleft.Location = New System.Drawing.Point(391, 95)
        Me.lblProgress_Timeleft.Name = "lblProgress_Timeleft"
        Me.lblProgress_Timeleft.Size = New System.Drawing.Size(181, 18)
        Me.lblProgress_Timeleft.TabIndex = 3
        Me.lblProgress_Timeleft.Text = "download timeleft"
        Me.lblProgress_Timeleft.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'picboxLogo
        '
        Me.picboxLogo.BackColor = System.Drawing.Color.White
        Me.picboxLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picboxLogo.Image = Global.FirefoxInstaller.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.picboxLogo.Location = New System.Drawing.Point(12, 12)
        Me.picboxLogo.Name = "picboxLogo"
        Me.picboxLogo.Size = New System.Drawing.Size(190, 70)
        Me.picboxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picboxLogo.TabIndex = 10
        Me.picboxLogo.TabStop = False
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(208, 12)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(382, 70)
        Me.lblTitle.TabIndex = 9
        Me.lblTitle.Text = "OVCC Firefox installer"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tmrInstaller
        '
        Me.tmrInstaller.Interval = 2000
        '
        'pnlSmall_IsFirefoxInstalled
        '
        Me.pnlSmall_IsFirefoxInstalled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_IsFirefoxInstalled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_IsFirefoxInstalled.Controls.Add(Me.lblIsFirefoxInstalled_Result)
        Me.pnlSmall_IsFirefoxInstalled.Controls.Add(Me.lblIsFirefoxInstalled)
        Me.pnlSmall_IsFirefoxInstalled.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_IsFirefoxInstalled.Location = New System.Drawing.Point(12, 133)
        Me.pnlSmall_IsFirefoxInstalled.Name = "pnlSmall_IsFirefoxInstalled"
        Me.pnlSmall_IsFirefoxInstalled.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_IsFirefoxInstalled.TabIndex = 12
        '
        'lblIsFirefoxInstalled_Result
        '
        Me.lblIsFirefoxInstalled_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsFirefoxInstalled_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblIsFirefoxInstalled_Result.Name = "lblIsFirefoxInstalled_Result"
        Me.lblIsFirefoxInstalled_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblIsFirefoxInstalled_Result.TabIndex = 14
        Me.lblIsFirefoxInstalled_Result.Text = "No"
        Me.lblIsFirefoxInstalled_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblIsFirefoxInstalled
        '
        Me.lblIsFirefoxInstalled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsFirefoxInstalled.Location = New System.Drawing.Point(3, -1)
        Me.lblIsFirefoxInstalled.Name = "lblIsFirefoxInstalled"
        Me.lblIsFirefoxInstalled.Size = New System.Drawing.Size(497, 25)
        Me.lblIsFirefoxInstalled.TabIndex = 13
        Me.lblIsFirefoxInstalled.Text = "Is Firefox installed?"
        Me.lblIsFirefoxInstalled.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_DownloadFirefox
        '
        Me.pnlSmall_DownloadFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_DownloadFirefox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_DownloadFirefox.Controls.Add(Me.lblDownloadFirefox_Result)
        Me.pnlSmall_DownloadFirefox.Controls.Add(Me.lblDownloadFirefox)
        Me.pnlSmall_DownloadFirefox.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_DownloadFirefox.Location = New System.Drawing.Point(12, 197)
        Me.pnlSmall_DownloadFirefox.Name = "pnlSmall_DownloadFirefox"
        Me.pnlSmall_DownloadFirefox.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_DownloadFirefox.TabIndex = 13
        '
        'lblDownloadFirefox_Result
        '
        Me.lblDownloadFirefox_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDownloadFirefox_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblDownloadFirefox_Result.Name = "lblDownloadFirefox_Result"
        Me.lblDownloadFirefox_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblDownloadFirefox_Result.TabIndex = 14
        Me.lblDownloadFirefox_Result.Text = "No"
        Me.lblDownloadFirefox_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDownloadFirefox
        '
        Me.lblDownloadFirefox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDownloadFirefox.Location = New System.Drawing.Point(3, -1)
        Me.lblDownloadFirefox.Name = "lblDownloadFirefox"
        Me.lblDownloadFirefox.Size = New System.Drawing.Size(497, 25)
        Me.lblDownloadFirefox.TabIndex = 13
        Me.lblDownloadFirefox.Text = "Download Firefox"
        Me.lblDownloadFirefox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_InstallProfile
        '
        Me.pnlSmall_InstallProfile.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_InstallProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_InstallProfile.Controls.Add(Me.lblInstallProfile_Result)
        Me.pnlSmall_InstallProfile.Controls.Add(Me.lblInstallProfile)
        Me.pnlSmall_InstallProfile.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_InstallProfile.Location = New System.Drawing.Point(12, 293)
        Me.pnlSmall_InstallProfile.Name = "pnlSmall_InstallProfile"
        Me.pnlSmall_InstallProfile.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_InstallProfile.TabIndex = 15
        '
        'lblInstallProfile_Result
        '
        Me.lblInstallProfile_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallProfile_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblInstallProfile_Result.Name = "lblInstallProfile_Result"
        Me.lblInstallProfile_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblInstallProfile_Result.TabIndex = 14
        Me.lblInstallProfile_Result.Text = "No"
        Me.lblInstallProfile_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblInstallProfile
        '
        Me.lblInstallProfile.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallProfile.Location = New System.Drawing.Point(3, -1)
        Me.lblInstallProfile.Name = "lblInstallProfile"
        Me.lblInstallProfile.Size = New System.Drawing.Size(497, 25)
        Me.lblInstallProfile.TabIndex = 13
        Me.lblInstallProfile.Text = "Install profile"
        Me.lblInstallProfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_InstallFirefox
        '
        Me.pnlSmall_InstallFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_InstallFirefox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_InstallFirefox.Controls.Add(Me.lblInstallFirefox_Result)
        Me.pnlSmall_InstallFirefox.Controls.Add(Me.lblInstallFirefox)
        Me.pnlSmall_InstallFirefox.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_InstallFirefox.Location = New System.Drawing.Point(12, 229)
        Me.pnlSmall_InstallFirefox.Name = "pnlSmall_InstallFirefox"
        Me.pnlSmall_InstallFirefox.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_InstallFirefox.TabIndex = 14
        '
        'lblInstallFirefox_Result
        '
        Me.lblInstallFirefox_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallFirefox_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblInstallFirefox_Result.Name = "lblInstallFirefox_Result"
        Me.lblInstallFirefox_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblInstallFirefox_Result.TabIndex = 14
        Me.lblInstallFirefox_Result.Text = "No"
        Me.lblInstallFirefox_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblInstallFirefox
        '
        Me.lblInstallFirefox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallFirefox.Location = New System.Drawing.Point(3, -1)
        Me.lblInstallFirefox.Name = "lblInstallFirefox"
        Me.lblInstallFirefox.Size = New System.Drawing.Size(497, 25)
        Me.lblInstallFirefox.TabIndex = 13
        Me.lblInstallFirefox.Text = "Install Firefox"
        Me.lblInstallFirefox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_SaveProfile
        '
        Me.pnlSmall_SaveProfile.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_SaveProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_SaveProfile.Controls.Add(Me.lblSaveProfile_Result)
        Me.pnlSmall_SaveProfile.Controls.Add(Me.lblSaveProfile)
        Me.pnlSmall_SaveProfile.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_SaveProfile.Location = New System.Drawing.Point(12, 357)
        Me.pnlSmall_SaveProfile.Name = "pnlSmall_SaveProfile"
        Me.pnlSmall_SaveProfile.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_SaveProfile.TabIndex = 17
        '
        'lblSaveProfile_Result
        '
        Me.lblSaveProfile_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaveProfile_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblSaveProfile_Result.Name = "lblSaveProfile_Result"
        Me.lblSaveProfile_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblSaveProfile_Result.TabIndex = 14
        Me.lblSaveProfile_Result.Text = "No"
        Me.lblSaveProfile_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSaveProfile
        '
        Me.lblSaveProfile.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSaveProfile.Location = New System.Drawing.Point(3, -1)
        Me.lblSaveProfile.Name = "lblSaveProfile"
        Me.lblSaveProfile.Size = New System.Drawing.Size(497, 25)
        Me.lblSaveProfile.TabIndex = 13
        Me.lblSaveProfile.Text = "Save profile"
        Me.lblSaveProfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_VerifyProfile
        '
        Me.pnlSmall_VerifyProfile.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_VerifyProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_VerifyProfile.Controls.Add(Me.lblVerifyProfile_Result)
        Me.pnlSmall_VerifyProfile.Controls.Add(Me.lblVerifyProfile)
        Me.pnlSmall_VerifyProfile.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_VerifyProfile.Location = New System.Drawing.Point(12, 325)
        Me.pnlSmall_VerifyProfile.Name = "pnlSmall_VerifyProfile"
        Me.pnlSmall_VerifyProfile.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_VerifyProfile.TabIndex = 16
        '
        'lblVerifyProfile_Result
        '
        Me.lblVerifyProfile_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerifyProfile_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblVerifyProfile_Result.Name = "lblVerifyProfile_Result"
        Me.lblVerifyProfile_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblVerifyProfile_Result.TabIndex = 14
        Me.lblVerifyProfile_Result.Text = "No"
        Me.lblVerifyProfile_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblVerifyProfile
        '
        Me.lblVerifyProfile.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVerifyProfile.Location = New System.Drawing.Point(3, -1)
        Me.lblVerifyProfile.Name = "lblVerifyProfile"
        Me.lblVerifyProfile.Size = New System.Drawing.Size(497, 25)
        Me.lblVerifyProfile.TabIndex = 13
        Me.lblVerifyProfile.Text = "Verify profile"
        Me.lblVerifyProfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_IsFirefoxInstalled
        '
        Me.pnlLarge_IsFirefoxInstalled.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_IsFirefoxInstalled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_IsFirefoxInstalled.Controls.Add(Me.lblIsFirefoxInstalledProgress)
        Me.pnlLarge_IsFirefoxInstalled.Controls.Add(Me.lblLarge_IsFirefoxInstalled)
        Me.pnlLarge_IsFirefoxInstalled.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_IsFirefoxInstalled.Location = New System.Drawing.Point(716, 22)
        Me.pnlLarge_IsFirefoxInstalled.Name = "pnlLarge_IsFirefoxInstalled"
        Me.pnlLarge_IsFirefoxInstalled.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_IsFirefoxInstalled.TabIndex = 13
        '
        'lblIsFirefoxInstalledProgress
        '
        Me.lblIsFirefoxInstalledProgress.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIsFirefoxInstalledProgress.Location = New System.Drawing.Point(4, 66)
        Me.lblIsFirefoxInstalledProgress.Name = "lblIsFirefoxInstalledProgress"
        Me.lblIsFirefoxInstalledProgress.Size = New System.Drawing.Size(568, 21)
        Me.lblIsFirefoxInstalledProgress.TabIndex = 18
        Me.lblIsFirefoxInstalledProgress.Text = "Url"
        Me.lblIsFirefoxInstalledProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLarge_IsFirefoxInstalled
        '
        Me.lblLarge_IsFirefoxInstalled.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_IsFirefoxInstalled.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_IsFirefoxInstalled.Name = "lblLarge_IsFirefoxInstalled"
        Me.lblLarge_IsFirefoxInstalled.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_IsFirefoxInstalled.TabIndex = 14
        Me.lblLarge_IsFirefoxInstalled.Text = "Is Firefox installed?"
        Me.lblLarge_IsFirefoxInstalled.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_DownloadFirefox
        '
        Me.pnlLarge_DownloadFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_DownloadFirefox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_DownloadFirefox.Controls.Add(Me.lblDownloadFirefox_URL)
        Me.pnlLarge_DownloadFirefox.Controls.Add(Me.lblLarge_DownloadFirefox)
        Me.pnlLarge_DownloadFirefox.Controls.Add(Me.lblProgress_Timeleft)
        Me.pnlLarge_DownloadFirefox.Controls.Add(Me.progressFirefox)
        Me.pnlLarge_DownloadFirefox.Controls.Add(Me.lblProgress_Speed)
        Me.pnlLarge_DownloadFirefox.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_DownloadFirefox.Location = New System.Drawing.Point(1318, 22)
        Me.pnlLarge_DownloadFirefox.Name = "pnlLarge_DownloadFirefox"
        Me.pnlLarge_DownloadFirefox.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_DownloadFirefox.TabIndex = 15
        '
        'lblDownloadFirefox_URL
        '
        Me.lblDownloadFirefox_URL.Font = New System.Drawing.Font("Segoe UI Semibold", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDownloadFirefox_URL.Location = New System.Drawing.Point(4, 50)
        Me.lblDownloadFirefox_URL.Name = "lblDownloadFirefox_URL"
        Me.lblDownloadFirefox_URL.Size = New System.Drawing.Size(568, 21)
        Me.lblDownloadFirefox_URL.TabIndex = 17
        Me.lblDownloadFirefox_URL.Text = "Url"
        Me.lblDownloadFirefox_URL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLarge_DownloadFirefox
        '
        Me.lblLarge_DownloadFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lblLarge_DownloadFirefox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_DownloadFirefox.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_DownloadFirefox.Name = "lblLarge_DownloadFirefox"
        Me.lblLarge_DownloadFirefox.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_DownloadFirefox.TabIndex = 14
        Me.lblLarge_DownloadFirefox.Text = "Download Firefox"
        Me.lblLarge_DownloadFirefox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_InitialiseFirefox
        '
        Me.pnlSmall_InitialiseFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_InitialiseFirefox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_InitialiseFirefox.Controls.Add(Me.lblInitialiseFirefox_Result)
        Me.pnlSmall_InitialiseFirefox.Controls.Add(Me.lblInitialiseFirefox)
        Me.pnlSmall_InitialiseFirefox.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_InitialiseFirefox.Location = New System.Drawing.Point(12, 261)
        Me.pnlSmall_InitialiseFirefox.Name = "pnlSmall_InitialiseFirefox"
        Me.pnlSmall_InitialiseFirefox.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_InitialiseFirefox.TabIndex = 15
        '
        'lblInitialiseFirefox_Result
        '
        Me.lblInitialiseFirefox_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInitialiseFirefox_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblInitialiseFirefox_Result.Name = "lblInitialiseFirefox_Result"
        Me.lblInitialiseFirefox_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblInitialiseFirefox_Result.TabIndex = 14
        Me.lblInitialiseFirefox_Result.Text = "No"
        Me.lblInitialiseFirefox_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblInitialiseFirefox
        '
        Me.lblInitialiseFirefox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInitialiseFirefox.Location = New System.Drawing.Point(3, -1)
        Me.lblInitialiseFirefox.Name = "lblInitialiseFirefox"
        Me.lblInitialiseFirefox.Size = New System.Drawing.Size(497, 25)
        Me.lblInitialiseFirefox.TabIndex = 13
        Me.lblInitialiseFirefox.Text = "Initialise Firefox"
        Me.lblInitialiseFirefox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_InitialiseFirefox
        '
        Me.pnlLarge_InitialiseFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_InitialiseFirefox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_InitialiseFirefox.Controls.Add(Me.lblInitialiseFirefox_Timeout)
        Me.pnlLarge_InitialiseFirefox.Controls.Add(Me.btnInitialiseFirefox_No)
        Me.pnlLarge_InitialiseFirefox.Controls.Add(Me.lblInitialiseFirefox_Text)
        Me.pnlLarge_InitialiseFirefox.Controls.Add(Me.btnInitialiseFirefox_Yes)
        Me.pnlLarge_InitialiseFirefox.Controls.Add(Me.lblLarge_InitialiseFirefox)
        Me.pnlLarge_InitialiseFirefox.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_InitialiseFirefox.Location = New System.Drawing.Point(1318, 184)
        Me.pnlLarge_InitialiseFirefox.Name = "pnlLarge_InitialiseFirefox"
        Me.pnlLarge_InitialiseFirefox.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_InitialiseFirefox.TabIndex = 17
        '
        'btnInitialiseFirefox_No
        '
        Me.btnInitialiseFirefox_No.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInitialiseFirefox_No.Location = New System.Drawing.Point(429, 98)
        Me.btnInitialiseFirefox_No.Name = "btnInitialiseFirefox_No"
        Me.btnInitialiseFirefox_No.Size = New System.Drawing.Size(141, 47)
        Me.btnInitialiseFirefox_No.TabIndex = 17
        Me.btnInitialiseFirefox_No.Text = "No"
        Me.btnInitialiseFirefox_No.UseVisualStyleBackColor = True
        '
        'lblInitialiseFirefox_Text
        '
        Me.lblInitialiseFirefox_Text.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInitialiseFirefox_Text.Location = New System.Drawing.Point(7, 42)
        Me.lblInitialiseFirefox_Text.Name = "lblInitialiseFirefox_Text"
        Me.lblInitialiseFirefox_Text.Size = New System.Drawing.Size(563, 53)
        Me.lblInitialiseFirefox_Text.TabIndex = 16
        Me.lblInitialiseFirefox_Text.Text = "Do you see an instance of firefox running behind this app?"
        Me.lblInitialiseFirefox_Text.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnInitialiseFirefox_Yes
        '
        Me.btnInitialiseFirefox_Yes.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInitialiseFirefox_Yes.Location = New System.Drawing.Point(6, 98)
        Me.btnInitialiseFirefox_Yes.Name = "btnInitialiseFirefox_Yes"
        Me.btnInitialiseFirefox_Yes.Size = New System.Drawing.Size(141, 47)
        Me.btnInitialiseFirefox_Yes.TabIndex = 15
        Me.btnInitialiseFirefox_Yes.Text = "Yes"
        Me.btnInitialiseFirefox_Yes.UseVisualStyleBackColor = True
        '
        'lblLarge_InitialiseFirefox
        '
        Me.lblLarge_InitialiseFirefox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_InitialiseFirefox.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_InitialiseFirefox.Name = "lblLarge_InitialiseFirefox"
        Me.lblLarge_InitialiseFirefox.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_InitialiseFirefox.TabIndex = 14
        Me.lblLarge_InitialiseFirefox.Text = "Initialise Firefox"
        Me.lblLarge_InitialiseFirefox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_InstallFirefox
        '
        Me.pnlLarge_InstallFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_InstallFirefox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_InstallFirefox.Controls.Add(Me.lblLarge_InstallFirefox)
        Me.pnlLarge_InstallFirefox.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_InstallFirefox.Location = New System.Drawing.Point(716, 182)
        Me.pnlLarge_InstallFirefox.Name = "pnlLarge_InstallFirefox"
        Me.pnlLarge_InstallFirefox.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_InstallFirefox.TabIndex = 16
        '
        'lblLarge_InstallFirefox
        '
        Me.lblLarge_InstallFirefox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_InstallFirefox.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_InstallFirefox.Name = "lblLarge_InstallFirefox"
        Me.lblLarge_InstallFirefox.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_InstallFirefox.TabIndex = 14
        Me.lblLarge_InstallFirefox.Text = "Install Firefox"
        Me.lblLarge_InstallFirefox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_VerifyProfile
        '
        Me.pnlLarge_VerifyProfile.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_VerifyProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_VerifyProfile.Controls.Add(Me.lblLarge_VerifyProfile)
        Me.pnlLarge_VerifyProfile.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_VerifyProfile.Location = New System.Drawing.Point(1318, 350)
        Me.pnlLarge_VerifyProfile.Name = "pnlLarge_VerifyProfile"
        Me.pnlLarge_VerifyProfile.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_VerifyProfile.TabIndex = 17
        '
        'lblLarge_VerifyProfile
        '
        Me.lblLarge_VerifyProfile.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_VerifyProfile.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_VerifyProfile.Name = "lblLarge_VerifyProfile"
        Me.lblLarge_VerifyProfile.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_VerifyProfile.TabIndex = 14
        Me.lblLarge_VerifyProfile.Text = "Verify profile"
        Me.lblLarge_VerifyProfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_InstallProfile
        '
        Me.pnlLarge_InstallProfile.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_InstallProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_InstallProfile.Controls.Add(Me.lblLarge_InstallProfile)
        Me.pnlLarge_InstallProfile.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_InstallProfile.Location = New System.Drawing.Point(716, 350)
        Me.pnlLarge_InstallProfile.Name = "pnlLarge_InstallProfile"
        Me.pnlLarge_InstallProfile.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_InstallProfile.TabIndex = 16
        '
        'lblLarge_InstallProfile
        '
        Me.lblLarge_InstallProfile.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_InstallProfile.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_InstallProfile.Name = "lblLarge_InstallProfile"
        Me.lblLarge_InstallProfile.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_InstallProfile.TabIndex = 14
        Me.lblLarge_InstallProfile.Text = "Install profile"
        Me.lblLarge_InstallProfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_SaveProfile
        '
        Me.pnlLarge_SaveProfile.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_SaveProfile.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_SaveProfile.Controls.Add(Me.lblLarge_SaveProfile)
        Me.pnlLarge_SaveProfile.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_SaveProfile.Location = New System.Drawing.Point(716, 517)
        Me.pnlLarge_SaveProfile.Name = "pnlLarge_SaveProfile"
        Me.pnlLarge_SaveProfile.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_SaveProfile.TabIndex = 16
        '
        'lblLarge_SaveProfile
        '
        Me.lblLarge_SaveProfile.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_SaveProfile.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_SaveProfile.Name = "lblLarge_SaveProfile"
        Me.lblLarge_SaveProfile.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_SaveProfile.TabIndex = 14
        Me.lblLarge_SaveProfile.Text = "Save profile"
        Me.lblLarge_SaveProfile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_Done
        '
        Me.pnlSmall_Done.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_Done.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_Done.Controls.Add(Me.lblDone_Result)
        Me.pnlSmall_Done.Controls.Add(Me.lblDone)
        Me.pnlSmall_Done.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_Done.Location = New System.Drawing.Point(12, 389)
        Me.pnlSmall_Done.Name = "pnlSmall_Done"
        Me.pnlSmall_Done.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_Done.TabIndex = 18
        '
        'lblDone_Result
        '
        Me.lblDone_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDone_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblDone_Result.Name = "lblDone_Result"
        Me.lblDone_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblDone_Result.TabIndex = 15
        Me.lblDone_Result.Text = "No"
        Me.lblDone_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDone
        '
        Me.lblDone.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDone.Location = New System.Drawing.Point(3, -1)
        Me.lblDone.Name = "lblDone"
        Me.lblDone.Size = New System.Drawing.Size(497, 25)
        Me.lblDone.TabIndex = 13
        Me.lblDone.Text = "Done"
        Me.lblDone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_done
        '
        Me.pnlLarge_done.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_done.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_done.Controls.Add(Me.lblLarge_Done)
        Me.pnlLarge_done.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_done.Location = New System.Drawing.Point(1318, 517)
        Me.pnlLarge_done.Name = "pnlLarge_done"
        Me.pnlLarge_done.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_done.TabIndex = 17
        '
        'lblLarge_Done
        '
        Me.lblLarge_Done.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_Done.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_Done.Name = "lblLarge_Done"
        Me.lblLarge_Done.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_Done.TabIndex = 14
        Me.lblLarge_Done.Text = "Done"
        Me.lblLarge_Done.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlError
        '
        Me.pnlError.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlError.Controls.Add(Me.lblError_Result)
        Me.pnlError.Controls.Add(Me.lblErrorMessage)
        Me.pnlError.Controls.Add(Me.lblLarge_Error)
        Me.pnlError.Location = New System.Drawing.Point(1318, 687)
        Me.pnlError.Name = "pnlError"
        Me.pnlError.Size = New System.Drawing.Size(578, 154)
        Me.pnlError.TabIndex = 21
        '
        'lblError_Result
        '
        Me.lblError_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblError_Result.ForeColor = System.Drawing.Color.Red
        Me.lblError_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblError_Result.Name = "lblError_Result"
        Me.lblError_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblError_Result.TabIndex = 16
        Me.lblError_Result.Text = "Failed"
        Me.lblError_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblErrorMessage
        '
        Me.lblErrorMessage.ForeColor = System.Drawing.Color.Black
        Me.lblErrorMessage.Location = New System.Drawing.Point(5, 27)
        Me.lblErrorMessage.Name = "lblErrorMessage"
        Me.lblErrorMessage.Size = New System.Drawing.Size(567, 117)
        Me.lblErrorMessage.TabIndex = 15
        Me.lblErrorMessage.Text = "error message"
        Me.lblErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLarge_Error
        '
        Me.lblLarge_Error.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_Error.ForeColor = System.Drawing.Color.Black
        Me.lblLarge_Error.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_Error.Name = "lblLarge_Error"
        Me.lblLarge_Error.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_Error.TabIndex = 14
        Me.lblLarge_Error.Text = "Error"
        Me.lblLarge_Error.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_UninstallFirefox
        '
        Me.pnlLarge_UninstallFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_UninstallFirefox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_UninstallFirefox.Controls.Add(Me.lblLarge_UninstallFirefox)
        Me.pnlLarge_UninstallFirefox.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_UninstallFirefox.Location = New System.Drawing.Point(716, 688)
        Me.pnlLarge_UninstallFirefox.Name = "pnlLarge_UninstallFirefox"
        Me.pnlLarge_UninstallFirefox.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_UninstallFirefox.TabIndex = 22
        '
        'lblLarge_UninstallFirefox
        '
        Me.lblLarge_UninstallFirefox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_UninstallFirefox.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_UninstallFirefox.Name = "lblLarge_UninstallFirefox"
        Me.lblLarge_UninstallFirefox.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_UninstallFirefox.TabIndex = 14
        Me.lblLarge_UninstallFirefox.Text = "Uninstall Firefox"
        Me.lblLarge_UninstallFirefox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_UninstallFirefox
        '
        Me.pnlSmall_UninstallFirefox.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_UninstallFirefox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_UninstallFirefox.Controls.Add(Me.lblUninstallFirefox_Result)
        Me.pnlSmall_UninstallFirefox.Controls.Add(Me.lblUninstallFirefox)
        Me.pnlSmall_UninstallFirefox.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_UninstallFirefox.Location = New System.Drawing.Point(12, 165)
        Me.pnlSmall_UninstallFirefox.Name = "pnlSmall_UninstallFirefox"
        Me.pnlSmall_UninstallFirefox.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_UninstallFirefox.TabIndex = 19
        '
        'lblUninstallFirefox_Result
        '
        Me.lblUninstallFirefox_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUninstallFirefox_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblUninstallFirefox_Result.Name = "lblUninstallFirefox_Result"
        Me.lblUninstallFirefox_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblUninstallFirefox_Result.TabIndex = 15
        Me.lblUninstallFirefox_Result.Text = "No"
        Me.lblUninstallFirefox_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUninstallFirefox
        '
        Me.lblUninstallFirefox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUninstallFirefox.Location = New System.Drawing.Point(3, -1)
        Me.lblUninstallFirefox.Name = "lblUninstallFirefox"
        Me.lblUninstallFirefox.Size = New System.Drawing.Size(497, 25)
        Me.lblUninstallFirefox.TabIndex = 13
        Me.lblUninstallFirefox.Text = "Uninstall Firefox"
        Me.lblUninstallFirefox.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLarge_Initialise
        '
        Me.pnlLarge_Initialise.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLarge_Initialise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLarge_Initialise.Controls.Add(Me.Label1)
        Me.pnlLarge_Initialise.Controls.Add(Me.lblLarge_Initialise)
        Me.pnlLarge_Initialise.ForeColor = System.Drawing.Color.Black
        Me.pnlLarge_Initialise.Location = New System.Drawing.Point(12, 678)
        Me.pnlLarge_Initialise.Name = "pnlLarge_Initialise"
        Me.pnlLarge_Initialise.Size = New System.Drawing.Size(578, 154)
        Me.pnlLarge_Initialise.TabIndex = 17
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(563, 53)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "One moment please"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLarge_Initialise
        '
        Me.lblLarge_Initialise.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLarge_Initialise.Location = New System.Drawing.Point(3, -1)
        Me.lblLarge_Initialise.Name = "lblLarge_Initialise"
        Me.lblLarge_Initialise.Size = New System.Drawing.Size(571, 25)
        Me.lblLarge_Initialise.TabIndex = 14
        Me.lblLarge_Initialise.Text = "Initialise"
        Me.lblLarge_Initialise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlSmall_Initialise
        '
        Me.pnlSmall_Initialise.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlSmall_Initialise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSmall_Initialise.Controls.Add(Me.lblInitialise_Result)
        Me.pnlSmall_Initialise.Controls.Add(Me.lblInitialise)
        Me.pnlSmall_Initialise.ForeColor = System.Drawing.Color.Black
        Me.pnlSmall_Initialise.Location = New System.Drawing.Point(12, 101)
        Me.pnlSmall_Initialise.Name = "pnlSmall_Initialise"
        Me.pnlSmall_Initialise.Size = New System.Drawing.Size(578, 26)
        Me.pnlSmall_Initialise.TabIndex = 19
        '
        'lblInitialise_Result
        '
        Me.lblInitialise_Result.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInitialise_Result.Location = New System.Drawing.Point(442, 0)
        Me.lblInitialise_Result.Name = "lblInitialise_Result"
        Me.lblInitialise_Result.Size = New System.Drawing.Size(129, 25)
        Me.lblInitialise_Result.TabIndex = 15
        Me.lblInitialise_Result.Text = "No"
        Me.lblInitialise_Result.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblInitialise
        '
        Me.lblInitialise.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInitialise.Location = New System.Drawing.Point(3, -1)
        Me.lblInitialise.Name = "lblInitialise"
        Me.lblInitialise.Size = New System.Drawing.Size(497, 25)
        Me.lblInitialise.TabIndex = 13
        Me.lblInitialise.Text = "Initialise"
        Me.lblInitialise.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblInitialiseFirefox_Timeout
        '
        Me.lblInitialiseFirefox_Timeout.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInitialiseFirefox_Timeout.Location = New System.Drawing.Point(153, 98)
        Me.lblInitialiseFirefox_Timeout.Name = "lblInitialiseFirefox_Timeout"
        Me.lblInitialiseFirefox_Timeout.Size = New System.Drawing.Size(270, 47)
        Me.lblInitialiseFirefox_Timeout.TabIndex = 18
        Me.lblInitialiseFirefox_Timeout.Text = "times out in xs"
        Me.lblInitialiseFirefox_Timeout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1916, 877)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlSmall_Initialise)
        Me.Controls.Add(Me.pnlLarge_Initialise)
        Me.Controls.Add(Me.pnlSmall_UninstallFirefox)
        Me.Controls.Add(Me.pnlLarge_UninstallFirefox)
        Me.Controls.Add(Me.pnlError)
        Me.Controls.Add(Me.pnlLarge_done)
        Me.Controls.Add(Me.pnlSmall_Done)
        Me.Controls.Add(Me.pnlLarge_VerifyProfile)
        Me.Controls.Add(Me.pnlLarge_SaveProfile)
        Me.Controls.Add(Me.pnlLarge_InstallProfile)
        Me.Controls.Add(Me.pnlLarge_InitialiseFirefox)
        Me.Controls.Add(Me.pnlSmall_InitialiseFirefox)
        Me.Controls.Add(Me.pnlLarge_InstallFirefox)
        Me.Controls.Add(Me.pnlLarge_DownloadFirefox)
        Me.Controls.Add(Me.pnlLarge_IsFirefoxInstalled)
        Me.Controls.Add(Me.pnlSmall_SaveProfile)
        Me.Controls.Add(Me.pnlSmall_InstallProfile)
        Me.Controls.Add(Me.pnlSmall_VerifyProfile)
        Me.Controls.Add(Me.pnlSmall_InstallFirefox)
        Me.Controls.Add(Me.pnlSmall_DownloadFirefox)
        Me.Controls.Add(Me.pnlSmall_IsFirefoxInstalled)
        Me.Controls.Add(Me.picboxLogo)
        Me.Controls.Add(Me.lblTitle)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Firefox Installer"
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlSmall_IsFirefoxInstalled.ResumeLayout(False)
        Me.pnlSmall_DownloadFirefox.ResumeLayout(False)
        Me.pnlSmall_InstallProfile.ResumeLayout(False)
        Me.pnlSmall_InstallFirefox.ResumeLayout(False)
        Me.pnlSmall_SaveProfile.ResumeLayout(False)
        Me.pnlSmall_VerifyProfile.ResumeLayout(False)
        Me.pnlLarge_IsFirefoxInstalled.ResumeLayout(False)
        Me.pnlLarge_DownloadFirefox.ResumeLayout(False)
        Me.pnlSmall_InitialiseFirefox.ResumeLayout(False)
        Me.pnlLarge_InitialiseFirefox.ResumeLayout(False)
        Me.pnlLarge_InstallFirefox.ResumeLayout(False)
        Me.pnlLarge_VerifyProfile.ResumeLayout(False)
        Me.pnlLarge_InstallProfile.ResumeLayout(False)
        Me.pnlLarge_SaveProfile.ResumeLayout(False)
        Me.pnlSmall_Done.ResumeLayout(False)
        Me.pnlLarge_done.ResumeLayout(False)
        Me.pnlError.ResumeLayout(False)
        Me.pnlLarge_UninstallFirefox.ResumeLayout(False)
        Me.pnlSmall_UninstallFirefox.ResumeLayout(False)
        Me.pnlLarge_Initialise.ResumeLayout(False)
        Me.pnlSmall_Initialise.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents progressFirefox As ProgressBar
    Friend WithEvents lblProgress_Speed As Label
    Friend WithEvents lblProgress_Timeleft As Label
    Friend WithEvents picboxLogo As PictureBox
    Friend WithEvents lblTitle As Label
    Friend WithEvents tmrInstaller As Timer
    Friend WithEvents pnlSmall_IsFirefoxInstalled As Panel
    Friend WithEvents lblIsFirefoxInstalled As Label
    Friend WithEvents lblIsFirefoxInstalled_Result As Label
    Friend WithEvents pnlSmall_DownloadFirefox As Panel
    Friend WithEvents lblDownloadFirefox_Result As Label
    Friend WithEvents lblDownloadFirefox As Label
    Friend WithEvents pnlSmall_InstallProfile As Panel
    Friend WithEvents lblInstallProfile_Result As Label
    Friend WithEvents lblInstallProfile As Label
    Friend WithEvents pnlSmall_InstallFirefox As Panel
    Friend WithEvents lblInstallFirefox_Result As Label
    Friend WithEvents lblInstallFirefox As Label
    Friend WithEvents pnlSmall_SaveProfile As Panel
    Friend WithEvents lblSaveProfile_Result As Label
    Friend WithEvents lblSaveProfile As Label
    Friend WithEvents pnlSmall_VerifyProfile As Panel
    Friend WithEvents lblVerifyProfile_Result As Label
    Friend WithEvents lblVerifyProfile As Label
    Friend WithEvents pnlLarge_IsFirefoxInstalled As Panel
    Friend WithEvents lblLarge_IsFirefoxInstalled As Label
    Friend WithEvents pnlLarge_DownloadFirefox As Panel
    Friend WithEvents lblLarge_DownloadFirefox As Label
    Friend WithEvents pnlSmall_InitialiseFirefox As Panel
    Friend WithEvents lblInitialiseFirefox_Result As Label
    Friend WithEvents lblInitialiseFirefox As Label
    Friend WithEvents pnlLarge_InitialiseFirefox As Panel
    Friend WithEvents lblLarge_InitialiseFirefox As Label
    Friend WithEvents pnlLarge_InstallFirefox As Panel
    Friend WithEvents lblLarge_InstallFirefox As Label
    Friend WithEvents pnlLarge_VerifyProfile As Panel
    Friend WithEvents lblLarge_VerifyProfile As Label
    Friend WithEvents pnlLarge_InstallProfile As Panel
    Friend WithEvents lblLarge_InstallProfile As Label
    Friend WithEvents pnlLarge_SaveProfile As Panel
    Friend WithEvents lblLarge_SaveProfile As Label
    Friend WithEvents lblDownloadFirefox_URL As Label
    Friend WithEvents pnlSmall_Done As Panel
    Friend WithEvents lblDone As Label
    Friend WithEvents pnlLarge_done As Panel
    Friend WithEvents lblLarge_Done As Label
    Friend WithEvents lblDone_Result As Label
    Friend WithEvents pnlError As Panel
    Friend WithEvents lblLarge_Error As Label
    Friend WithEvents lblErrorMessage As Label
    Friend WithEvents lblError_Result As Label
    Friend WithEvents btnInitialiseFirefox_No As Button
    Friend WithEvents lblInitialiseFirefox_Text As Label
    Friend WithEvents btnInitialiseFirefox_Yes As Button
    Friend WithEvents lblIsFirefoxInstalledProgress As Label
    Friend WithEvents pnlLarge_UninstallFirefox As Panel
    Friend WithEvents lblLarge_UninstallFirefox As Label
    Friend WithEvents pnlSmall_UninstallFirefox As Panel
    Friend WithEvents lblUninstallFirefox_Result As Label
    Friend WithEvents lblUninstallFirefox As Label
    Friend WithEvents pnlLarge_Initialise As Panel
    Friend WithEvents lblLarge_Initialise As Label
    Friend WithEvents pnlSmall_Initialise As Panel
    Friend WithEvents lblInitialise_Result As Label
    Friend WithEvents lblInitialise As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblInitialiseFirefox_Timeout As Label
End Class
