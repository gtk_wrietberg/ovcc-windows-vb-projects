﻿Imports System.Net
Imports System.Threading

Public Class frmMain
#Region "UI"
    Public ReadOnly UI_PANEL_MARGIN As Integer = 6
    Public ReadOnly UI_APP_WIDTH As Integer = 620
    Public ReadOnly UI_APP_HEIGHT As Integer = 594
#End Region

#Region "firefox download stuff"
    Public ReadOnly FIREFOX_DOWNLOAD_URL_TEMPLATE As String = "https://download.mozilla.org/?product=firefox-latest-ssl&os=!!OS!!&lang=!!LANG!!"
    Public ReadOnly FIREFOX_DOWNLOAD_WIN32 As String = "win"
    Public ReadOnly FIREFOX_DOWNLOAD_WIN64 As String = "win64"
    Public ReadOnly FIREFOX_DOWNLOAD_LANG_GB As String = "en-GB"
    Public ReadOnly FIREFOX_DOWNLOAD_LANG_US As String = "en-US"

    Public FIREFOX_DOWNLOAD_URL As String = ""
    Public FIREFOX_DOWNLOAD_FOLDER As String = ""
    Public FIREFOX_DOWNLOAD_FULLPATH As String = ""
    Public FIREFOX_INSTALL_INI_FULLPATH As String = ""
    Public FIREFOX_INSTALLED_FULLPATH As String = ""

    Public ReadOnly FIREFOX_DOWNLOAD_FILENAME As String = "OVCC_FIREFOX_INSTALLER.exe"
    Public ReadOnly FIREFOX_INSTALL_PARAMETERS As String = "/S /TaskbarShortcut=false /DesktopShortcut=false /StartMenuShortcut=false /OptionalExtensions=false"  ' https://firefox-source-docs.mozilla.org/browser/installer/windows/installer/FullConfig.html
    Public ReadOnly FIREFOX_INSTALL_INI_FILENAME As String = "firefox_install.ini"
#End Region

#Region "timeouts and delays"
    Private ReadOnly FIREFOX_INITIALISATION_TIMEOUT_s As Integer = 30
#End Region

#Region "user stuff"
    Private SITEKIOSK_USER_OLD_PASSWORD As Boolean = False
#End Region

#Region "Thread Safe stuff"
    Private threadPanelActions As Thread
    Private threadPanelActions_Pause As New Threading.AutoResetEvent(False)

    Delegate Sub ThreadSafeCallBack__UpdateResult(ByVal [index] As Integer, ByVal [text] As String)

    Public Sub ThreadSafe__UpdateResult(ByVal [index] As Integer, ByVal [text] As String)
        If [index] < 0 Or [index] >= mPanels_Result.Count Then
            Exit Sub
        End If

        If mPanels_Result.Item([index]).InvokeRequired Then
            Dim d As New ThreadSafeCallBack__UpdateResult(AddressOf ThreadSafe__UpdateResult)
            Me.Invoke(d, New Object() {[index], [text]})

            Exit Sub
        End If


        Select Case [text]
            Case PanelResults.OK
                mPanels_Result.Item([index]).ForeColor = Color.DarkGreen
            Case PanelResults.SKIPPED
                mPanels_Result.Item([index]).ForeColor = Color.Orange
            Case Else
                mPanels_Result.Item([index]).ForeColor = Color.Black
        End Select

        mPanels_Result.Item([index]).Text = [text]
    End Sub


    Delegate Sub ThreadSafeCallBack__ThreadComplete([_result] As String)
    Public Sub ThreadSafe__ThreadComplete([_result] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__ThreadComplete(AddressOf ThreadSafe__ThreadComplete)
            Me.Invoke(d, New Object() {[_result]})

            Exit Sub
        End If

        If _result = PanelResults.OK Or _result = PanelResults.SKIPPED Then
            ThreadSafe__UpdateResult(mActivePanel, [_result])

            NextPanel()
        Else
            'ERROR

            ErrorPanel(_result)
        End If
    End Sub


    Delegate Sub ThreadSafeCallBack__DownloadProgress([percentage] As Integer, [Progress_Speed] As String, [Progress_Timeleft] As String)
    Public Sub ThreadSafe__DownloadProgress([percentage] As Integer, [Progress_Speed] As String, [Progress_Timeleft] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__DownloadProgress(AddressOf ThreadSafe__DownloadProgress)
            Me.Invoke(d, New Object() {[percentage], [Progress_Speed], [Progress_Timeleft]})

            Exit Sub
        End If

        If percentage < progressFirefox.Minimum Or percentage > progressFirefox.Maximum Then
            percentage = progressFirefox.Minimum
        End If
        progressFirefox.Value = percentage

        lblProgress_Speed.Text = [Progress_Speed]
        lblProgress_Timeleft.Text = [Progress_Timeleft]
    End Sub


    Delegate Sub ThreadSafeCallBack__lblDownloadFirefox_URL([_text] As String)
    Public Sub ThreadSafe__lblDownloadFirefox_URL([_text] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__lblDownloadFirefox_URL(AddressOf ThreadSafe__lblDownloadFirefox_URL)
            Me.Invoke(d, New Object() {[_text]})

            Exit Sub
        End If

        lblDownloadFirefox_URL.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__lblIsFirefoxInstalledProgress([_text] As String)
    Public Sub ThreadSafe__lblIsFirefoxInstalledProgress([_text] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__lblIsFirefoxInstalledProgress(AddressOf ThreadSafe__lblIsFirefoxInstalledProgress)
            Me.Invoke(d, New Object() {[_text]})

            Exit Sub
        End If

        lblIsFirefoxInstalledProgress.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__FormOpacity([_opacity] As Double)
    Public Sub ThreadSafe__FormOpacity([_opacity] As Double)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__FormOpacity(AddressOf ThreadSafe__FormOpacity)
            Me.Invoke(d, New Object() {[_opacity]})

            Exit Sub
        End If

        Me.Opacity = _opacity
    End Sub


    Delegate Sub ThreadSafeCallBack__InitialiseFirefox_Controls_toggle([_text] As String, [_state] As Boolean)
    Public Sub ThreadSafe__InitialiseFirefox_Controls_toggle([_text] As String, [_state] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__InitialiseFirefox_Controls_toggle(AddressOf ThreadSafe__InitialiseFirefox_Controls_toggle)
            Me.Invoke(d, New Object() {[_text], [_state]})

            Exit Sub
        End If


        btnInitialiseFirefox_Yes.Enabled = [_state]
        btnInitialiseFirefox_No.Enabled = [_state]

        btnInitialiseFirefox_Yes.Visible = [_state]
        btnInitialiseFirefox_No.Visible = [_state]

        lblInitialiseFirefox_Text.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__InitialiseFirefox_Timeout([_text] As String)
    Public Sub ThreadSafe__InitialiseFirefox_Timeout([_text] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__InitialiseFirefox_Timeout(AddressOf ThreadSafe__InitialiseFirefox_Timeout)
            Me.Invoke(d, New Object() {[_text]})

            Exit Sub
        End If


        lblInitialiseFirefox_Timeout.Text = [_text]
    End Sub
#End Region

#Region "panel stuff"
    Public Enum ApplicationSteps As Integer
        Initialise = 0
        IsFirefoxInstalled
        UninstallFirefox
        DownloadFirefox
        InstallFirefox
        InitialiseFirefox
        InstallProfile
        VerifyProfile
        SaveProfile
        Done
        [Error]
    End Enum
    Private mPanelActionsMax As Integer = [Enum].GetValues(GetType(ApplicationSteps)).Cast(Of ApplicationSteps)().Max() - 1 ' -1 because we don't count the error panel

    Public Structure PanelResults
        Public Const OK As String = "Ok"
        Public Const SKIPPED As String = "Skipped"
    End Structure

    Private mPanels_Small As New List(Of Panel)
    Private mPanels_Large As New List(Of Panel)
    Private mPanels_Title As New List(Of Label)
    Private mPanels_Result As New List(Of Label)

    Private mActivePanel As ApplicationSteps = ApplicationSteps.Initialise

    Private Sub ResetAllPanels()
        mPanels_Small = New List(Of Panel)
        mPanels_Small.Add(pnlSmall_Initialise)
        mPanels_Small.Add(pnlSmall_IsFirefoxInstalled)
        mPanels_Small.Add(pnlSmall_UninstallFirefox)
        mPanels_Small.Add(pnlSmall_DownloadFirefox)
        mPanels_Small.Add(pnlSmall_InstallFirefox)
        mPanels_Small.Add(pnlSmall_InitialiseFirefox)
        mPanels_Small.Add(pnlSmall_InstallProfile)
        mPanels_Small.Add(pnlSmall_VerifyProfile)
        mPanels_Small.Add(pnlSmall_SaveProfile)
        mPanels_Small.Add(pnlSmall_Done)

        For _i As Integer = 0 To mPanels_Small.Count - 1
            mPanels_Small.Item(_i).Visible = True
            mPanels_Small.Item(_i).Location = New Point(12, 100 + _i * (mPanels_Small.Item(_i).Height + UI_PANEL_MARGIN))
        Next


        mPanels_Large = New List(Of Panel)
        mPanels_Large.Add(pnlLarge_Initialise)
        mPanels_Large.Add(pnlLarge_IsFirefoxInstalled)
        mPanels_Large.Add(pnlLarge_UninstallFirefox)
        mPanels_Large.Add(pnlLarge_DownloadFirefox)
        mPanels_Large.Add(pnlLarge_InstallFirefox)
        mPanels_Large.Add(pnlLarge_InitialiseFirefox)
        mPanels_Large.Add(pnlLarge_InstallProfile)
        mPanels_Large.Add(pnlLarge_VerifyProfile)
        mPanels_Large.Add(pnlLarge_SaveProfile)
        mPanels_Large.Add(pnlLarge_done)

        For Each _p As Panel In mPanels_Large
            _p.Visible = False
            _p.Enabled = False
        Next


        mPanels_Title = New List(Of Label)
        mPanels_Title.Add(lblInitialise)
        mPanels_Title.Add(lblIsFirefoxInstalled)
        mPanels_Title.Add(lblUninstallFirefox)
        mPanels_Title.Add(lblDownloadFirefox)
        mPanels_Title.Add(lblInstallFirefox)
        mPanels_Title.Add(lblInitialiseFirefox)
        mPanels_Title.Add(lblInstallProfile)
        mPanels_Title.Add(lblVerifyProfile)
        mPanels_Title.Add(lblSaveProfile)
        mPanels_Title.Add(lblDone)


        mPanels_Result = New List(Of Label)
        mPanels_Result.Add(lblInitialise_Result)
        mPanels_Result.Add(lblIsFirefoxInstalled_Result)
        mPanels_Result.Add(lblUninstallFirefox_Result)
        mPanels_Result.Add(lblDownloadFirefox_Result)
        mPanels_Result.Add(lblInstallFirefox_Result)
        mPanels_Result.Add(lblInitialiseFirefox_Result)
        mPanels_Result.Add(lblInstallProfile_Result)
        mPanels_Result.Add(lblVerifyProfile_Result)
        mPanels_Result.Add(lblSaveProfile_Result)
        mPanels_Result.Add(lblDone_Result)

        For Each _l As Label In mPanels_Result
            _l.Text = ""
        Next


        mActivePanel = ApplicationSteps.Initialise

        RefreshPanels()
    End Sub

    Private Sub NextPanel()
        mActivePanel += 1

        If mActivePanel >= mPanelActionsMax Then
            mActivePanel = mPanelActionsMax
        End If
        If mActivePanel < 0 Then
            mActivePanel = ApplicationSteps.Initialise
        End If

        RefreshPanels()
    End Sub

    Private Sub RefreshPanels()
        For _i As Integer = 0 To mPanels_Large.Count - 1
            If _i = mActivePanel Then
                mPanels_Small.Item(_i).Visible = False

                mPanels_Large.Item(_i).Visible = True
                mPanels_Large.Item(_i).Enabled = True
            Else
                mPanels_Small.Item(_i).Visible = True

                mPanels_Large.Item(_i).Visible = False
                mPanels_Large.Item(_i).Enabled = False
            End If

            If _i < mActivePanel Then
                mPanels_Small.Item(_i).Location = New Point(12, 100 + _i * (mPanels_Small.Item(_i).Height + UI_PANEL_MARGIN))
            ElseIf _i = mActivePanel Then
                mPanels_Large.Item(_i).Location = New Point(12, 100 + _i * (mPanels_Small.Item(_i).Height + UI_PANEL_MARGIN))
            Else
                mPanels_Small.Item(_i).Location = New Point(12, 100 + _i * (mPanels_Small.Item(_i).Height + UI_PANEL_MARGIN) + (mPanels_Large.Item(_i).Height - mPanels_Small.Item(_i).Height))
            End If
        Next

        pnlError.Visible = False
        pnlError.Enabled = False


        DoPanelAction()
    End Sub

    Private Sub DoPanelAction()
        Helpers.Logger.Write("executing action '" & mActivePanel.ToString & "'", , 0)

        Select Case mActivePanel
            Case ApplicationSteps.Initialise
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__Initialise))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.IsFirefoxInstalled
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__IsFirefoxInstalled))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.UninstallFirefox
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__UninstallFirefox))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.DownloadFirefox
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__DownloadFirefox))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.InstallFirefox
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__InstallFirefox))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.InitialiseFirefox
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__InitialiseFirefox))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.InstallProfile
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__InstallProfile))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.VerifyProfile
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__VerifyProfile))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.SaveProfile
                Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__SaveProfile))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.Done
                'done
        End Select
    End Sub

#Region "error panel"
    Private Sub ErrorPanel(ErrorMessage As String)
        'Try
        '    threadPanelActions.Abort()
        'Catch ex As Exception

        'End Try

        If mActivePanel >= mPanelActionsMax Or mActivePanel < 0 Then
            'uh?
            lblLarge_Error.Text = "Error while retrieving the progress step"
        Else
            lblLarge_Error.Text = mPanels_Title.Item(mActivePanel).Text
            lblErrorMessage.Text = ErrorMessage

            mPanels_Large.Item(mActivePanel).Visible = False
            mPanels_Large.Item(mActivePanel).Enabled = False
            mPanels_Small.Item(mActivePanel).Visible = False
            mPanels_Small.Item(mActivePanel).Enabled = False
            pnlError.Visible = True
            pnlError.Enabled = True
            pnlError.Location = New Point(12, 100 + mActivePanel * (mPanels_Small.Item(mActivePanel).Height + UI_PANEL_MARGIN))
        End If
    End Sub
#End Region

#Region "panel actions"
    Private Sub PanelAction__Initialise()
        Helpers.Logger.WriteRelative("initialising", , 1)


        FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL_TEMPLATE
        If Environment.Is64BitOperatingSystem Then
            FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL.Replace("!!OS!!", FIREFOX_DOWNLOAD_WIN64)
        Else
            FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL.Replace("!!OS!!", FIREFOX_DOWNLOAD_WIN32)
        End If
        If Thread.CurrentThread.CurrentCulture.Name.Equals("en-US") Then
            FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL.Replace("!!LANG!!", FIREFOX_DOWNLOAD_LANG_US)
        Else
            FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL.Replace("!!LANG!!", FIREFOX_DOWNLOAD_LANG_GB)
        End If

        ThreadSafe__lblDownloadFirefox_URL(FIREFOX_DOWNLOAD_URL)


        FIREFOX_DOWNLOAD_FOLDER = IO.Path.Combine(IO.Path.GetTempPath(), "__OVCC_FIREFOX_DOWNLOAD__")
        FIREFOX_DOWNLOAD_FULLPATH = IO.Path.Combine(FIREFOX_DOWNLOAD_FOLDER, FIREFOX_DOWNLOAD_FILENAME)
        FIREFOX_INSTALL_INI_FULLPATH = IO.Path.Combine(FIREFOX_DOWNLOAD_FOLDER, FIREFOX_INSTALL_INI_FILENAME)


        Helpers.Logger.WriteRelative("firefox download url", , 2)
        Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_URL, , 3)
        Helpers.Logger.WriteRelative("firefox download path", , 2)
        Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_FULLPATH, , 3)


        Helpers.Logger.WriteRelative("which sitekiosk user password do we use?", , 2)

        Dim sReturnMessage As String = ""

        Helpers.Logger.WriteRelative("testing new password", , 3)
        If Helpers.WindowsUser.CheckLogon(
            Constants.UsernamePassword.SiteKiosk.Username,
            Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password),
            sReturnMessage) Then

            SITEKIOSK_USER_OLD_PASSWORD = False

            Helpers.Logger.WriteRelative("ok", , 4)
        Else
            Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 4)
            Helpers.Logger.WriteRelative(sReturnMessage, , 5)

            Helpers.Logger.WriteRelative("testing old password", , 3)

            If Helpers.WindowsUser.CheckLogon(
                Constants.UsernamePassword.SiteKiosk.Username,
                Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password_old),
                sReturnMessage) Then

                SITEKIOSK_USER_OLD_PASSWORD = True

                Helpers.Logger.WriteRelative("ok", , 4)
            Else
                Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                Helpers.Logger.WriteRelative(sReturnMessage, , 5)

                ThreadSafe__ThreadComplete("An error occurred while accessing the '" & Constants.UsernamePassword.SiteKiosk.Username & "' user account: " & sReturnMessage)

                Exit Sub
            End If
        End If

        Thread.Sleep(1000)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub


    Private Sub PanelAction__IsFirefoxInstalled()
        Helpers.Logger.WriteRelative("checking if firefox is installed", , 1)

        ThreadSafe__lblIsFirefoxInstalledProgress("checking")

        Thread.Sleep(1000)

        If Helpers.Generic.IsFirefoxInstalled Then
            Helpers.Logger.WriteRelative("yes", , 2)
            ThreadSafe__lblIsFirefoxInstalledProgress("YES")
        Else
            Helpers.Logger.WriteRelative("no", , 2)
            ThreadSafe__lblIsFirefoxInstalledProgress("NO")
        End If

        Thread.Sleep(1000)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub


    Private Sub PanelAction__UninstallFirefox()
        Helpers.Logger.WriteRelative("uninstalling firefox", , 1)


        If Not Helpers.Generic.IsFirefoxInstalled Then
            Helpers.Logger.WriteRelative("skipped, not yet installed", , 2)
            ThreadSafe__ThreadComplete(PanelResults.SKIPPED)

            Exit Sub
        End If


        'Thread.Sleep(1000)


        'If Helpers.Processes.StartProcess(FIREFOX_DOWNLOAD_FULLPATH, FIREFOX_INSTALL_PARAMETERS, True, 30000) Then

        'End If





        'Thread.Sleep(1000)

        ThreadSafe__ThreadComplete(PanelResults.SKIPPED)
    End Sub


    Private downloader_ElapsedTime As Date
    Private downloader_ElapsedTimeTotal As TimeSpan
    Private downloader_BytesReceivedTotal As Double
    Private downloader_BytesTotal As Double
    Private downloader_Busy As Boolean = False
    Private downloader_Timeout As Integer = 0

    Private downloader_MeanSpeedTotal As Double
    Private downloader_MeanSpeedCount As Double

    Private Sub PanelAction__DownloadFirefox()
        Helpers.Logger.WriteRelative("downloading firefox", , 1)


        If Helpers.Generic.IsFirefoxInstalled Then
            Helpers.Logger.WriteRelative("skipped, already installed", , 2)
            ThreadSafe__ThreadComplete(PanelResults.SKIPPED)

            Exit Sub
        End If


        Thread.Sleep(1000)


        Try
            Helpers.Logger.WriteRelative("creating path", , 2)
            Helpers.Logger.WriteRelative(IO.Path.GetDirectoryName(FIREFOX_DOWNLOAD_FULLPATH), , 3)

            IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(FIREFOX_DOWNLOAD_FULLPATH))


            downloader_ElapsedTime = Date.Now
            downloader_MeanSpeedTotal = 0
            downloader_MeanSpeedCount = 0

            Dim client As WebClient = New WebClient

            AddHandler client.DownloadProgressChanged, AddressOf PanelAction__DownloadFirefox_ProgressChanged
            AddHandler client.DownloadFileCompleted, AddressOf PanelAction__DownloadFirefox_DownloadCompleted

            Helpers.Logger.WriteRelative("starting download", , 2)
            Helpers.Logger.WriteRelative("url", , 3)
            Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_URL, , 4)
            Helpers.Logger.WriteRelative("path", , 3)
            Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_FULLPATH, , 4)
            client.DownloadFileAsync(New Uri(FIREFOX_DOWNLOAD_URL), FIREFOX_DOWNLOAD_FULLPATH)
            Helpers.Logger.WriteRelative("started", , 3)

            downloader_Busy = True
            downloader_Timeout = 0
        Catch ex As Exception
            Helpers.Logger.WriteRelative("An error occurred while downloading Firefox: " & ex.Message, , 4)
            ThreadSafe__ThreadComplete("An error occurred while downloading Firefox: " & ex.Message)

            Exit Sub
        End Try


        Do
            Thread.Sleep(1000)

            downloader_Timeout += 1
        Loop While downloader_Timeout < 30 And downloader_Busy

        If downloader_Busy Then
            'uh oh, download timed out
            Helpers.Logger.WriteRelative("download timed out", , 4)
            ThreadSafe__ThreadComplete("Firefox download timed out!")
        Else
            If IO.File.Exists(FIREFOX_DOWNLOAD_FULLPATH) Then
                Helpers.Logger.WriteRelative("ok", , 3)
                Helpers.Logger.WriteRelative("elapsed time", , 4)
                Helpers.Logger.WriteRelative(downloader_ElapsedTimeTotal.Seconds & " seconds", , 5)
                Helpers.Logger.WriteRelative("average speed", , 4)
                Helpers.Logger.WriteRelative(Helpers.FilesAndFolders.FileSize.GetHumanReadable(Math.Round(downloader_MeanSpeedTotal / downloader_MeanSpeedCount)) & "/s", , 5)

                ThreadSafe__ThreadComplete(PanelResults.OK)
            Else
                Helpers.Logger.WriteRelative("failed", , 3)
                ThreadSafe__ThreadComplete("Could not find downloaded Firefox installer '" & FIREFOX_DOWNLOAD_FULLPATH & "'")
            End If
        End If
    End Sub

    Private Sub PanelAction__DownloadFirefox_ProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
        downloader_Timeout = 0


        downloader_BytesReceivedTotal = Double.Parse(e.BytesReceived.ToString())
        downloader_BytesTotal = Double.Parse(e.TotalBytesToReceive.ToString())

        Dim _ts As TimeSpan = Date.Now.Subtract(downloader_ElapsedTime)
        Dim percentage_d As Double = downloader_BytesReceivedTotal / downloader_BytesTotal * 100
        Dim percentage_s As String = Int32.Parse(Math.Truncate(percentage_d).ToString()).ToString & "%" ' funky
        Dim percentage_i As Integer = 0
        Dim speed As Double = (downloader_BytesReceivedTotal / _ts.Seconds) 'bytes/s
        Dim timeleft As Double = (downloader_BytesTotal - downloader_BytesReceivedTotal) / speed ' s left
        Dim sSpeed As String = "", sTimeleft As String = ""


        percentage_i = Int32.Parse(Math.Truncate(percentage_d).ToString())

        Try
            sSpeed = "Download speed: " & Helpers.FilesAndFolders.FileSize.GetHumanReadable(Math.Round(speed)) & "/s"

            downloader_MeanSpeedTotal = downloader_MeanSpeedTotal + speed
            downloader_MeanSpeedCount = downloader_MeanSpeedCount + 1.0
        Catch ex As Exception
            sSpeed = "Download speed: " & Helpers.FilesAndFolders.FileSize.GetHumanReadable(0) & "/s"
        End Try

        sTimeleft = "Time remaining: " & Math.Round(timeleft) & "s"


        ThreadSafe__DownloadProgress(percentage_i, sSpeed, sTimeleft)
    End Sub

    Private Sub PanelAction__DownloadFirefox_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        downloader_ElapsedTimeTotal = Date.Now.Subtract(downloader_ElapsedTime)
        downloader_Busy = False
        downloader_Timeout = 0

        ThreadSafe__DownloadProgress(100, "", "")
    End Sub


    Private Sub PanelAction__InstallFirefox()
        Helpers.Logger.WriteRelative("installing firefox", , 1)


        If Helpers.Generic.IsFirefoxInstalled Then
            Helpers.Logger.WriteRelative("skipped, already installed", , 2)
            ThreadSafe__ThreadComplete(PanelResults.SKIPPED)

            Exit Sub
        End If


        Thread.Sleep(1000)


        'If Helpers.Processes.StartProcess(FIREFOX_DOWNLOAD_FULLPATH, FIREFOX_INSTALL_PARAMETERS, True, 30000) Then

        'End If






        Thread.Sleep(2500)

        ThreadSafe__ThreadComplete("BLA: " & FIREFOX_INSTALLED_FULLPATH)
    End Sub


    Private mInitialiseFirefox__waiting As Boolean = False
    Private mInitialiseFirefox__success As Boolean = False
    Private mInitialiseFirefox__timeout As Integer = 0
    Private Sub PanelAction__InitialiseFirefox()
        Helpers.Logger.WriteRelative("initialising firefox", , 1)

        ThreadSafe__InitialiseFirefox_Controls_toggle(
            "One moment, starting Firefox as user '" & Constants.UsernamePassword.SiteKiosk.Username & "'" & vbCrLf & vbCrLf & "!!! Please do not interact with Firefox !!!",
            False)
        ThreadSafe__InitialiseFirefox_Timeout("")

        Helpers.Logger.WriteRelative("finding firefox", , 2)
        'Find the firefox executable
        FIREFOX_INSTALLED_FULLPATH = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "Mozilla Firefox\firefox.exe")

        If Not IO.File.Exists(FIREFOX_INSTALLED_FULLPATH) Then
            FIREFOX_INSTALLED_FULLPATH = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Mozilla Firefox\firefox.exe")

            If Not IO.File.Exists(FIREFOX_INSTALLED_FULLPATH) Then
                'uh oh
                Helpers.Logger.WriteRelative("could not find firefox.exe", , 3)
                ThreadSafe__ThreadComplete("Could not find firefox.exe")
            End If
        End If

        Helpers.Logger.WriteRelative(FIREFOX_INSTALLED_FULLPATH, , 3)


        ThreadSafe__FormOpacity(0.9)

        mInitialiseFirefox__waiting = False
        mInitialiseFirefox__timeout = 0


        Dim tmpPW As String

        If Not SITEKIOSK_USER_OLD_PASSWORD Then
            tmpPW = Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password)
        Else
            tmpPW = Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password_old)
        End If


        Helpers.Logger.WriteRelative("starting firefox", , 2)
        Helpers.Logger.WriteRelative("command line", , 3)
        Helpers.Logger.WriteRelative(FIREFOX_INSTALLED_FULLPATH, , 4)
        Helpers.Logger.WriteRelative("username", , 3)
        Helpers.Logger.WriteRelative(Constants.UsernamePassword.SiteKiosk.Username, , 4)
        Helpers.Logger.WriteRelative("password", , 3)
        Helpers.Logger.WriteRelative(tmpPW, , 4)
        Helpers.Logger.WriteRelative("arguments", , 3)
        Helpers.Logger.WriteRelative(" ", , 4)
        Helpers.Logger.WriteRelative("working directory", , 3)
        Helpers.Logger.WriteRelative(IO.Path.GetDirectoryName(FIREFOX_INSTALLED_FULLPATH), , 4)

        If Helpers.Processes.StartProcessAsUser(
            FIREFOX_INSTALLED_FULLPATH,
            Constants.UsernamePassword.SiteKiosk.Username,
            tmpPW,
            True,
            "-foreground",
            IO.Path.GetDirectoryName(FIREFOX_INSTALLED_FULLPATH),
            False,
            -1,
            False) Then

            Helpers.Logger.WriteRelative("ok", , 3)

            mInitialiseFirefox__waiting = True
            mInitialiseFirefox__success = False
        Else
            ThreadSafe__ThreadComplete("An error occurred while starting firefox ('" & FIREFOX_INSTALLED_FULLPATH & "'): " & Helpers.Errors.GetLast)

            Exit Sub
        End If

        Thread.Sleep(2500)

        ThreadSafe__InitialiseFirefox_Controls_toggle(
            "Do you see an instance of Firefox running behind this app?" & vbCrLf & vbCrLf & "!!! Please do not interact with Firefox !!!",
            True)


        Do
            ThreadSafe__InitialiseFirefox_Timeout("times out in " & (FIREFOX_INITIALISATION_TIMEOUT_s - mInitialiseFirefox__timeout).ToString & "s")

            mInitialiseFirefox__timeout += 1

            Thread.Sleep(1000)
        Loop While mInitialiseFirefox__waiting And mInitialiseFirefox__timeout < FIREFOX_INITIALISATION_TIMEOUT_s

        Helpers.Generic.KillAllFirefox()
        ThreadSafe__FormOpacity(1.0)

        If mInitialiseFirefox__success Then
            ThreadSafe__ThreadComplete(PanelResults.OK)
        Else
            If mInitialiseFirefox__timeout >= FIREFOX_INITIALISATION_TIMEOUT_s Then
                ThreadSafe__ThreadComplete("It appears Firefox timed out, please restart and try again.")
            Else
                ThreadSafe__ThreadComplete("Firefox failed to start. Please restart and try again.")
            End If
        End If
    End Sub

    Private Sub PanelAction__InitialiseFirefox_Yes_Click(sender As Object, e As EventArgs) Handles btnInitialiseFirefox_Yes.Click
        mInitialiseFirefox__waiting = False
        mInitialiseFirefox__success = True
    End Sub

    Private Sub PanelAction__InitialiseFirefox_No_Click(sender As Object, e As EventArgs) Handles btnInitialiseFirefox_No.Click
        mInitialiseFirefox__waiting = False
        mInitialiseFirefox__success = False
    End Sub


    Private Sub PanelAction__InstallProfile()
        Thread.Sleep(2500)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub

    Private Sub PanelAction__VerifyProfile()
        Thread.Sleep(2500)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub

    Private Sub PanelAction__SaveProfile()
        Thread.Sleep(2500)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub

    Private Sub PanelAction__Done()
        Thread.Sleep(2500)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub
#End Region
#End Region

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("started", , 0)


        If Helpers.Processes.IsAppAlreadyRunning Then
            Helpers.Logger.Write("there's already an instance running, exiting..", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)
            Application.Exit()
        End If


        Helpers.Forms.TopMost(Me.Handle, True)

        Me.Width = UI_APP_WIDTH
        Me.Height = UI_APP_HEIGHT
        Me.Location = New Point((Screen.PrimaryScreen.Bounds.Width - Me.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - UI_APP_HEIGHT) / 2)


        '---------------------------------------------------------------------------------------
        Helpers.Logger.Write("user permission", , 1)
        If MessageBox.Show(Me, "This app will close any currently open Firefox windows, and reinstall Firefox completely. Is this ok?", My.Application.Info.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = DialogResult.No Then
            Helpers.Logger.Write("user said no", , 2)

            Me.Close()
            Exit Sub
        End If
        Helpers.Logger.Write("user said yes", , 2)


        '---------------------------------------------------------------------------------------
        Dim killedFirefox As Integer = 0
        Helpers.Logger.Write("killing all firefox processes", , 1)
        killedFirefox = Helpers.Generic.KillAllFirefox()
        Helpers.Logger.Write("killed " & killedFirefox.ToString, , 2)


        '---------------------------------------------------------------------------------------
        ThreadSafe__InitialiseFirefox_Controls_toggle("", False)


        '---------------------------------------------------------------------------------------
        ResetAllPanels()
    End Sub


End Class
