
// call this function to change the active language
function startTime()
{
	var twentyfourhourformat=true;
	
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var d=today.getDay();
	var dom=today.getDate();
	var mon=today.getMonth();
	var year=today.getFullYear();
	var ampm='';
	
	if(!twentyfourhourformat) {
		ampm=' '+(h>=12?'PM':'AM');
		h=h%12;
		h=h?h:12;
	}

	// add a zero in front of minutes<10
	m=checkTime(m);
	document.getElementById('main_time').innerHTML=h+":"+m+ampm;
	
	d=checkDay(d);
	document.getElementById('main_day').innerHTML=d;

	mon=checkMonth(mon);
	document.getElementById('main_date').innerHTML=mon+" "+dom+" "+year;

	t=setTimeout(function(){startTime()},1000);
}

function checkTime(i)
{
	if (i<10)
	{
		i="0" + i;
	}
	return i;
}

function checkDay(i)
{
	var day_name="unknown";
	switch(i)
	{
		case 0:day_name=LoadString(80);break;// sunday
		case 1:day_name=LoadString(81);break;// monday
		case 2:day_name=LoadString(82);break;// tuesday
		case 3:day_name=LoadString(83);break;// wednesday
		case 4:day_name=LoadString(84);break;// thursday
		case 5:day_name=LoadString(85);break;// friday
		case 6:day_name=LoadString(86);break;// saturday
		default:break;
	}
	return day_name;
}


function checkMonth(i)
{
	var month_name="unknown";
	switch(i)
	{
		case 0:month_name=LoadString(87);break;// jan
		case 1:month_name=LoadString(88);break;// feb
		case 2:month_name=LoadString(89);break;// mar
		case 3:month_name=LoadString(90);break;// apr
		case 4:month_name=LoadString(91);break;// may
		case 5:month_name=LoadString(92);break;// jun
		case 6:month_name=LoadString(93);break;// jul
		case 7:month_name=LoadString(94);break;// aug
		case 8:month_name=LoadString(95);break;// sep
		case 9:month_name=LoadString(96);break;// oct
		case 10:month_name=LoadString(97);break;// nov
		case 11:month_name=LoadString(98);break;// dec

		default:break;
	}
	return month_name;
}

addLoadEvent(startTime);