﻿Module Main
    Enum ClockState
        NotSet = 0
        _24hr
        ampm
    End Enum

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("started", 0)

        Dim stateClock As ClockState = ClockState.NotSet

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals("24") Or arg.Equals("24hr") Or arg.Equals("-24") Or arg.Equals("-24hr") Or arg.Equals("--24") Or arg.Equals("--24hr") Then
                stateClock = ClockState._24hr
            End If

            If arg.Equals("ampm") Or arg.Equals("-ampm") Or arg.Equals("--ampm") Then
                stateClock = ClockState.ampm
            End If
        Next

        If stateClock = ClockState.NotSet Then
            'Nothing
            Helpers.Logger.WriteMessage("nothing chosen", 1)

            Exit Sub
        End If


        Dim sClockScriptTemplate As String = My.Resources.clock_js_template
        Dim sClockScript As String


        Helpers.Logger.WriteMessage("change all themes internet clock", 0)
        If stateClock = ClockState._24hr Then
            Helpers.Logger.WriteMessage("theme clocks will be set to 24hr", 1)

            sClockScript = sClockScriptTemplate.Replace("%%%twentyfourhourformat%%%", "true")
        Else
            Helpers.Logger.WriteMessage("theme clocks will be set to am/pm", 1)

            sClockScript = sClockScriptTemplate.Replace("%%%twentyfourhourformat%%%", "false")
        End If

        Try
            Dim sThemeFolders As String() = IO.Directory.GetDirectories(IO.Path.Combine(GetSiteKioskDirectory, "Skins\Public\Startpages"))

            For Each sThemeFolder As String In sThemeFolders
                Helpers.Logger.WriteMessage("entering '" & sThemeFolder & "'", 2)

                If Not IO.Directory.Exists(IO.Path.Combine(sThemeFolder, "scripts")) Then
                    Helpers.Logger.WriteMessage("no scripts folder found, skipping", 3)

                    Continue For
                End If

                If Not IO.File.Exists(IO.Path.Combine(sThemeFolder, "scripts\clock.js")) Then
                    Helpers.Logger.WriteMessage("clock script not found, skipping", 3)

                    Continue For
                End If

                Dim sFile As String = IO.Path.Combine(sThemeFolder, "scripts\clock.js")

                Helpers.Logger.WriteMessage("updating clock script", 3)
                Helpers.Logger.WriteMessage(sFile, 4)

                Helpers.Logger.WriteMessage("backup", 5)
                Helpers.FilesAndFolders.Backup.File(sFile, 0, My.Application.Info.ProductName)

                Helpers.Logger.WriteMessage("saving", 5)
                Using sw As New IO.StreamWriter(sFile)
                    sw.Write(sClockScript)
                End Using

                Helpers.Logger.WriteMessage("ok", 6)
            Next
        Catch ex As Exception
            Helpers.Logger.WriteError("FAIL", 1)
            Helpers.Logger.WriteError(ex.Message, 2)
        End Try
    End Sub

    Public Function GetSiteKioskDirectory() As String
        Dim skDir As String = ""

        Try
            Dim regKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry32)
            skDir = regKey.OpenSubKey("SOFTWARE\Provisio\SiteKiosk").GetValue("InstallDir", "")
        Catch ex As Exception

        End Try

        Return skDir
    End Function
End Module
