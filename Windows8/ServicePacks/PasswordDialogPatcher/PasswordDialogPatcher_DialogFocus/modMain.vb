﻿Module modMain
    Private ReadOnly SCRIPT_FILE As String = "%%PROGRAM_FILES%%\SiteKiosk\Skins\Public\iBAHN\Scripts\internal\TermsAndConditions_and_Payment.js"
    Private ReadOnly SEARCH_STRING As String = "bResult=bResult||SiteKiosk.SiteKioskUI.IsDialogRunning("
    Private ReadOnly FIX_STRING1 As String = "script-device-OVCC-password-login"
    Private ReadOnly FIX_STRING2 As String = "script-device-hilton-login"
    Private ReadOnly FIX_STRING_END As String = "ZZZZ__this-is-just-here-for-the-updater"

    Public Sub main()
        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("started", 1)


        Dim sFile As String = SCRIPT_FILE.Replace("%%PROGRAM_FILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder())


        Helpers.Logger.WriteMessage("Script file", 0)
        Helpers.Logger.WriteMessage(sFile, 1)
        If Not IO.File.Exists(sFile) Then
            Helpers.Logger.WriteError("not found", 2)
            ExitCode.SetValue(ExitCode.ExitCodes.SCRIPT_FILE_NOT_FOUND)
            ExitApplication()
            Exit Sub
        End If

        Helpers.Logger.WriteMessage("ok", 2)


        Helpers.Logger.WriteMessage("Updating", 0)
        Try

            Dim lstApps As New List(Of String)
            Dim linesApplications As New List(Of String)
            Dim bApplicationsScriptChanged As Boolean = False
            Dim lineLast As String = ""

            Helpers.Logger.WriteMessage("looking for '" & FIX_STRING1 & "' or '" & FIX_STRING2 & "'", 1)

            Using sr As New IO.StreamReader(sFile)
                Dim line As String, bDone As Boolean = False

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains(SEARCH_STRING) Then
                        If line.Contains(FIX_STRING_END) Then
                            'already fixed
                            Helpers.Logger.WriteError("already fixed", 2)
                            ExitCode.SetValue(ExitCode.ExitCodes.SCRIPT_FILE_ALREADY_FIXED)
                            bApplicationsScriptChanged = False
                            Exit While
                        End If

                        lineLast = line
                    Else
                        If Not lineLast.Equals("") Then
                            Helpers.Logger.WriteMessage("added", 2)

                            linesApplications.Add(vbTab & "bResult=bResult||SiteKiosk.SiteKioskUI.IsDialogRunning(""" & FIX_STRING1 & """);")
                            linesApplications.Add(vbTab & "bResult=bResult||SiteKiosk.SiteKioskUI.IsDialogRunning(""" & FIX_STRING2 & """);")
                            linesApplications.Add(vbTab & "bResult=bResult||SiteKiosk.SiteKioskUI.IsDialogRunning(""" & FIX_STRING_END & """);")
                            lineLast = ""

                            bApplicationsScriptChanged = True
                        End If
                    End If

                    linesApplications.Add(line)
                End While
            End Using

            If bApplicationsScriptChanged Then
                Helpers.Logger.WriteMessage("saving", 1)
                Helpers.Logger.WriteMessage("backup", 2)
                Helpers.FilesAndFolders.Backup.File(sFile, 0, My.Application.Info.ProductName)

                Helpers.Logger.WriteMessage("writing", 2)
                Helpers.Logger.WriteMessage(sFile, 3)
                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In linesApplications
                        sw.WriteLine(line)
                    Next
                End Using
                Helpers.Logger.WriteMessage("ok", , 4)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteError("error", 1)
            Helpers.Logger.WriteError(ex.Message, 2)
            ExitCode.SetValue(ExitCode.ExitCodes.SCRIPT_FILE_UPDATE_FAILED)
            ExitApplication()
            Exit Sub
        End Try


        ExitApplication()
    End Sub

    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
