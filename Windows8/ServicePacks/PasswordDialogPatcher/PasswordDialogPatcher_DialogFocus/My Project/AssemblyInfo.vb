Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("PasswordDialogPatcher_DialogFocus")>
<Assembly: AssemblyDescription("PasswordDialogPatcher_DialogFocus")>
<Assembly: AssemblyCompany("Guest-Tek")>
<Assembly: AssemblyProduct("PasswordDialogPatcher_DialogFocus")>
<Assembly: AssemblyCopyright("Copyright ©  2022")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("2954c250-82b6-4d11-8964-2ee8763bfd5e")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.23349.1452")>
<Assembly: AssemblyFileVersion("1.0.23349.1452")>

<assembly: AssemblyInformationalVersion("0.0.23349.1452")>