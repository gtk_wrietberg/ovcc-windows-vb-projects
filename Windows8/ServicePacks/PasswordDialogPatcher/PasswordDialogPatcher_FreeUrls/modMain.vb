﻿Module modMain
    Private ReadOnly MAIN_SCRIPT_FILE As String = "%%PROGRAM_FILES%%\SiteKiosk\Skins\default\Scripts\guest-tek.js"
    Private ReadOnly MAIN_SCRIPT_FILE_OLD As String = "%%PROGRAM_FILES%%\SiteKiosk\Skins\default\Scripts\ibahn.js"

    Private ReadOnly IBAHN_FUNCTIONS_SCRIPT_FILE As String = "%%PROGRAM_FILES%%\SiteKiosk\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"
    Private ReadOnly FIX_SEARCH_STRING__IBAHN_FUNCTIONS As String = "OVCCPasswordDialogFix 202312"

    Private ReadOnly LOGIN_HTML_FILE As String = "%%PROGRAM_FILES%%\SiteKiosk\SiteCash\Html\Password\login.html"
    Private ReadOnly FIX_SEARCH_STRING__LOGIN_HTML As String = "if(SiteKiosk.ScriptDispatch.OVCC_PasswordAccessModule__IsPasswordEmpty()) {"


    Public Sub Main()
        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("started", 1)

        Dim bIsOldStyle As Boolean = False


        Helpers.Logger.WriteMessage("Checking password dialog version", 0)
        Helpers.Logger.WriteMessage("Main script file", 1)

        Dim sFile__MAIN_SCRIPT As String = MAIN_SCRIPT_FILE.Replace("%%PROGRAM_FILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder())

        Helpers.Logger.WriteMessage(sFile__MAIN_SCRIPT, 2)
        If Not IO.File.Exists(sFile__MAIN_SCRIPT) Then
            Helpers.Logger.WriteError("not found", 3)

            sFile__MAIN_SCRIPT = MAIN_SCRIPT_FILE_OLD.Replace("%%PROGRAM_FILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder())
            Helpers.Logger.WriteMessage("Main script file (old style)", 1)
            Helpers.Logger.WriteMessage(sFile__MAIN_SCRIPT, 2)

            If Not IO.File.Exists(sFile__MAIN_SCRIPT) Then
                Helpers.Logger.WriteError("not found", 2)
                ExitCode.SetValue(ExitCode.ExitCodes.MAIN_SCRIPT_NOT_FOUND)
                ExitApplication()
                Exit Sub
            End If
        End If


        Try
            Using sr As New IO.StreamReader(sFile__MAIN_SCRIPT)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("OVCC_PasswordAccessModule__IsEnabled()") Then
                        bIsOldStyle = False
                        Exit While
                    End If

                    If line.Contains("HiltonPasswordAccessModule__IsEnabled()") Then
                        bIsOldStyle = True
                        Exit While
                    End If
                End While
            End Using
        Catch ex As Exception
            Helpers.Logger.WriteError("error", 1)
            Helpers.Logger.WriteError(ex.Message, 2)
            ExitCode.SetValue(ExitCode.ExitCodes.IBAHN_FUNCTIONS_SCRIPT_FILE_UPDATE_FAILED)
        End Try

        If bIsOldStyle Then
            Helpers.Logger.WriteMessage("Old style password dialog", 1)
        Else
            Helpers.Logger.WriteMessage("New style password dialog", 1)
        End If


        '-----------------------------------------------------------------------------------
        Dim sFile__IBAHN_FUNCTIONS As String = IBAHN_FUNCTIONS_SCRIPT_FILE.Replace("%%PROGRAM_FILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder())

        Helpers.Logger.WriteMessage("Script file", 0)
        Helpers.Logger.WriteMessage(sFile__IBAHN_FUNCTIONS, 1)
        If Not IO.File.Exists(sFile__IBAHN_FUNCTIONS) Then
            Helpers.Logger.WriteError("not found", 2)
            ExitCode.SetValue(ExitCode.ExitCodes.IBAHN_FUNCTIONS_SCRIPT_FILE_NOT_FOUND)
        Else
            Helpers.Logger.WriteMessage("ok", 2)


            Helpers.Logger.WriteMessage("Updating", 0)
            Helpers.Logger.WriteMessage(sFile__IBAHN_FUNCTIONS, 1)

            Try
                Dim lines As New List(Of String)
                Dim bScriptChanged As Boolean = False
                Dim bPatchStarted As Boolean = False

                Using sr As New IO.StreamReader(sFile__IBAHN_FUNCTIONS)
                    Dim line As String, bDone As Boolean = False

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains(FIX_SEARCH_STRING__IBAHN_FUNCTIONS) Then
                            Helpers.Logger.WriteError("already fixed", 2)
                            ExitCode.SetValue(ExitCode.ExitCodes.IBAHN_FUNCTIONS_SCRIPT_FILE_ALREADY_FIXED)
                            bScriptChanged = False
                            Exit While
                        End If

                        If line.Contains("function iBAHN__OpenFreeUrl(sUrl) {") Then
                            bPatchStarted = True

                            Dim sPatch As String
                            If bIsOldStyle Then
                                sPatch = My.Resources.iBAHN_functions_js_PATCH__OLD_STYLE
                            Else
                                sPatch = My.Resources.iBAHN_functions_js_PATCH
                            End If
                            Dim lPatchLines As List(Of String) = sPatch.Split(New String() {Environment.NewLine}, StringSplitOptions.None).ToList

                            For Each sPatchLine As String In lPatchLines
                                sPatchLine = Trim(sPatchLine)

                                lines.Add(sPatchLine)
                            Next

                            bScriptChanged = True
                        End If

                        If line.Contains("function iBAHN__OpenInternetUrl() {") Then
                            bPatchStarted = False
                        End If

                        If line.Contains("function iBAHN__OpenFreeAirlineDialog() {") Then
                            line = line.Replace("iBAHN__OpenFreeAirlineDialog", "iBAHN__OpenFreeAirlineDialog__BROKEN")
                        End If

                        If Not bPatchStarted Then
                            lines.Add(line)
                        End If
                    End While
                End Using

                If bScriptChanged Then
                    Helpers.Logger.WriteMessage("saving", 1)
                    Helpers.Logger.WriteMessage("backup", 2)
                    Helpers.FilesAndFolders.Backup.File(sFile__IBAHN_FUNCTIONS, 0, My.Application.Info.ProductName)

                    Helpers.Logger.WriteMessage("writing", 2)
                    Helpers.Logger.WriteMessage(sFile__IBAHN_FUNCTIONS, 3)
                    Using sw As New IO.StreamWriter(sFile__IBAHN_FUNCTIONS)
                        For Each line As String In lines
                            sw.WriteLine(line)
                        Next
                    End Using
                    Helpers.Logger.WriteMessage("ok", , 4)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteError("error", 1)
                Helpers.Logger.WriteError(ex.Message, 2)
                ExitCode.SetValue(ExitCode.ExitCodes.IBAHN_FUNCTIONS_SCRIPT_FILE_UPDATE_FAILED)
            End Try
        End If


        '-----------------------------------------------------------------------------------
        Dim sFile__LOGIN_HTML As String = LOGIN_HTML_FILE.Replace("%%PROGRAM_FILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder())

        Helpers.Logger.WriteMessage("Script file", 0)
        Helpers.Logger.WriteMessage(sFile__LOGIN_HTML, 1)
        If Not IO.File.Exists(sFile__LOGIN_HTML) Then
            Helpers.Logger.WriteError("not found", 2)
            ExitCode.SetValue(ExitCode.ExitCodes.LOGIN_HTML_FILE_NOT_FOUND)
        Else
            Helpers.Logger.WriteMessage("ok", 2)


            Helpers.Logger.WriteMessage("Updating", 0)
            Helpers.Logger.WriteMessage(sFile__LOGIN_HTML, 1)

            Try
                Dim lines As New List(Of String)
                Dim bScriptChanged As Boolean = False

                Using sr As New IO.StreamReader(sFile__LOGIN_HTML)
                    Dim line As String, bDone As Boolean = False

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains(FIX_SEARCH_STRING__LOGIN_HTML) Then
                            Helpers.Logger.WriteError("already fixed", 2)
                            ExitCode.SetValue(ExitCode.ExitCodes.LOGIN_HTML_FILE_ALREADY_FIXED)
                            bScriptChanged = False
                            Exit While
                        End If

                        If line.Contains("if(SiteKiosk.ScriptDispatch.OVCC_PasswordAccessModule__IsPasswordEmpty) {") Then
                            line = line.Replace("IsPasswordEmpty)", "IsPasswordEmpty())")

                            bScriptChanged = True
                        End If

                        lines.Add(line)
                    End While
                End Using

                If bScriptChanged Then
                    Helpers.Logger.WriteMessage("saving", 1)
                    Helpers.Logger.WriteMessage("backup", 2)
                    Helpers.FilesAndFolders.Backup.File(sFile__LOGIN_HTML, 0, My.Application.Info.ProductName)

                    Helpers.Logger.WriteMessage("writing", 2)
                    Helpers.Logger.WriteMessage(sFile__LOGIN_HTML, 3)
                    Using sw As New IO.StreamWriter(sFile__LOGIN_HTML)
                        For Each line As String In lines
                            sw.WriteLine(line)
                        Next
                    End Using
                    Helpers.Logger.WriteMessage("ok", , 4)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteError("error", 1)
                Helpers.Logger.WriteError(ex.Message, 2)
                ExitCode.SetValue(ExitCode.ExitCodes.LOGIN_HTML_FILE_UPDATE_FAILED)
                ExitApplication()
                Exit Sub
            End Try
        End If


        ExitApplication()
    End Sub

    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
