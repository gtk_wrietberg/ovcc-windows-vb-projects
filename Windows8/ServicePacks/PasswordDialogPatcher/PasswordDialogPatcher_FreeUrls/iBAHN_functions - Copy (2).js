//Url of the hotel, if set to empty (""), ibahn.com is loaded
var iBAHN_Global_HotelURL="https://www.wouterrietberg.nl/HotelInfo";

//Url to browse to when user clicks the internet button
var iBAHN_Global_InternetURL="https://www.google.com/?MARRIOTT";

//Url to browse to when user clicks the weather button
var iBAHN_Global_WeatherURL="https://www.weather.com/MARRIOTT";

//Url to browse to when user clicks the map button
// This url can be customised for local address
var iBAHN_Global_MapURL="https://www.wouterrietberg.nl/Map";

//Hotel/Site Address (for UI)
var iBAHN_Global_SiteAddress="ugiuygtui6tb867";

// Brand index (!!!!!!!!!! -1 !!!!!!!!!!!)
// 0 Hilton Hotel & Resorts
// 1 Garden Inn
// 2 Hampton
// 3 Waldord Astoria
// 4 Conrad
// 5 DoubleTree
// 6 Canopy
// 7 Curio
// 8 Embassy Suites
// 9 Homewood Suites
// 10 Home2
// 11 Hilton Grand Vacations
// 12 Tru
// 13 Tapestry Collection
var iBAHN_Global_Brand=672;

var iBAHN_Office_ShowOnlineWhenNotInstalled=false;
var iBAHN_Office_WordURL="https://www.office.com/launch/word";
var iBAHN_Office_ExcelURL="https://www.office.com/launch/excel";
var iBAHN_Office_PowerpointURL="https://www.office.com/launch/powerpoint";



//----------------------------------------------------------------------------------------------------------
function iBAHN__OpenUrl(sUrl) {
	try {
		SiteKiosk.ScriptDispatch.TermsAndConditions_ActionStack__TriggerPayment();
	} 
	catch(e) {}
	iBAHN__AddToActionStack("url:"+sUrl);
}

function iBAHN__OpenFreeUrl(sUrl) {
	iBAHN__AddToActionStack("url:"+sUrl);
}

function iBAHN__OpenFreeUrlOnHomepage(sUrl) {
	iBAHN__AddToActionStack("url_homepage:"+sUrl);
}

function iBAHN__OpenInternetUrl() {
	iBAHN__OpenUrl(iBAHN_Global_InternetURL);
}

function iBAHN__OpenHotelUrl() {
	iBAHN__OpenFreeUrl(iBAHN_Global_HotelURL);
}

function iBAHN__OpenWeatherUrl() {
	iBAHN__OpenFreeUrl(iBAHN_Global_WeatherURL);
}

function iBAHN__OpenMapUrl() {
	iBAHN__OpenFreeUrl(iBAHN_Global_MapURL);
}

function iBAHN__OpenOfficeUrl(sOfficeApp) {
	switch(sOfficeApp) {
		case "word":
			iBAHN__OpenUrl(iBAHN_Office_WordURL);
			break;
		case "excel":
			iBAHN__OpenUrl(iBAHN_Office_ExcelURL);
			break;
		case "powerpoint":
			iBAHN__OpenUrl(iBAHN_Office_PowerpointURL);
			break;
		default:
			return false;
	}
}

function iBAHN__RunJavascript(sJavascript) {
	iBAHN__AddToActionStack("javascript:"+sJavascript);
}

function iBAHN__OpenApplicationById(iId) {
	try {
		if(SiteKiosk.Plugins("SiteCash").PayApplications) SiteKiosk.ScriptDispatch.TermsAndConditions_ActionStack__TriggerPayment();
	}
	catch(e) {}
	
	iBAHN__AddToActionStack("application:id="+iId);
}

function iBAHN__OpenApplicationByName(sName) {
	try {
		if(SiteKiosk.Plugins("SiteCash").PayApplications) SiteKiosk.ScriptDispatch.TermsAndConditions_ActionStack__TriggerPayment();
	}
	catch(e) {}
	
	iBAHN__AddToActionStack("application:name="+sName);
}

function iBAHN__OpenApplicationByPath(sPath) {
	try {
		if(SiteKiosk.Plugins("SiteCash").PayApplications) SiteKiosk.ScriptDispatch.TermsAndConditions_ActionStack__TriggerPayment();
	}
	catch(e) {}
	
	iBAHN__AddToActionStack("application:path="+sPath);
}

function iBAHN__OpenFreeAirlineDialog() {
	iBAHN__AddToActionStack("special:FreeAirlinesDialog");
}

function iBAHN__OpenFileManagerDialog() {
	try {
		SiteKiosk.ScriptDispatch.TermsAndConditions_ActionStack__TriggerPayment();
	}
	catch(e) {}
	iBAHN__AddToActionStack("special:FileManagerDialog");
}

function iBAHN__AddToActionStack(sAction) {
	iBAHN__LogMessage("Adding to stack: "+sAction);
	try {
		if(SiteKiosk.ScriptDispatch.TermsAndConditions_ActionStack__IsLocked()) {
			iBAHN__LogError("Stack is locked, action not added!");
		} else {
			SiteKiosk.ScriptDispatch.TermsAndConditions_ActionStack__Add(sAction);
		}
	}
	catch(e) {}
}

function iBAHN__DoesApplicationExist(sName) {
	var ret;
	
	try {
		ret=(SiteKiosk.ScriptDispatch.Applications__GetId(sName)>-1);
	}
	catch(e) {
		ret=false;
	}
	return ret;
}

function iBAHN__GetApplicationId(sName) {
	var ret;
	
	try {
		ret=SiteKiosk.ScriptDispatch.Applications__GetId(sName);
	} 
	catch(e) {
		ret=-1;
	}
	return ret;
}

function iBAHN__AreApplicationsInitialised() {
	var ret;
	try {
		ret=SiteKiosk.ScriptDispatch.Applications__IsDone();
	} 
	catch(e) {
		ret=true;
	}
	return ret;
}

function iBAHN__SiteKioskLogout() {
	var lk_WarnHTMLDialog=SiteKiosk.SiteKioskUI.CreateHTMLDialog();
	
	lk_WarnHTMLDialog.LoadDefaultSettings("logout-warning");
	lk_WarnHTMLDialog.Parent = SiteKiosk.WindowList.MainWindow.SiteKioskWindow.Window;
	lk_WarnHTMLDialog.ShowModal()
}

function iBAHN__LogMessage(s) {
	iBAHN__LogWrite(s,20);
}

function iBAHN__LogWarning(s) {
	iBAHN__LogWrite(s,30);
}

function iBAHN__LogError(s) {
	iBAHN__LogWrite(s,40);
}

function iBAHN__LogWrite(s,n) {
	try {
		SiteKiosk.Logfile.Write(1014,n,"GuestTek",s);
	}
	catch(e) {
		//alert(s);
	}
}
