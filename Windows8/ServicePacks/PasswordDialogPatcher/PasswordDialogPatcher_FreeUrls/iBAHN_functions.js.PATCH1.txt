
//OVCCPasswordDialogFix 202312 - start
function iBAHN__OpenFreeUrl(sUrl) {
	if(SiteKiosk.ScriptDispatch.OVCC_PasswordAccessModule__IsEnabled()) {
		iBAHN__OpenUrl(sUrl);
	} else {
		iBAHN__AddToActionStack("url:"+sUrl);
	}
}

function iBAHN__OpenFreeUrlOnHomepage(sUrl) {
	if(SiteKiosk.ScriptDispatch.OVCC_PasswordAccessModule__IsEnabled()) {
		try {
			SiteKiosk.ScriptDispatch.TermsAndConditions_ActionStack__TriggerPayment();
		} 
		catch(e) {}
	}
	iBAHN__AddToActionStack("url_homepage:"+sUrl);
}
//OVCCPasswordDialogFix 202312 - end

