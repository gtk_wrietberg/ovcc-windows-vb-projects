﻿Public Class frmMain
    Private ReadOnly c_REGROOT As String = "HKEY_CURRENT_USER\Software\AppDataLow\Software\Microsoft\IME\15.0\IMEJP\MSIME"
    Private ReadOnly c_REGKEY__ToolBarEnabled As String = "ToolBarEnabled"
    Private ReadOnly c_REGKEY__kanaMd As String = "kanaMd"

    Private Sub btnEnable_Click(sender As Object, e As EventArgs) Handles btnEnable.Click
        Helpers.Registry64.SetValue_Integer(c_REGROOT, c_REGKEY__ToolBarEnabled, 1)
        Helpers.Registry64.DeleteValue(c_REGROOT, c_REGKEY__kanaMd)
    End Sub

    Private Sub btnDisable_Click(sender As Object, e As EventArgs) Handles btnDisable.Click
        Helpers.Registry64.SetValue_Integer(c_REGROOT, c_REGKEY__ToolBarEnabled, 0)
    End Sub

    Private Function GetCurrentKeyboardLayout() As String
        Return InputLanguage.CurrentInputLanguage.LayoutName
    End Function

    Private Sub btnCurrentKeyboardLayout_Click(sender As Object, e As EventArgs) Handles btnCurrentKeyboardLayout.Click
        MsgBox(GetCurrentKeyboardLayout())
    End Sub
End Class
