﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnEnable = New System.Windows.Forms.Button()
        Me.btnDisable = New System.Windows.Forms.Button()
        Me.btnCurrentKeyboardLayout = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnEnable
        '
        Me.btnEnable.Location = New System.Drawing.Point(12, 12)
        Me.btnEnable.Name = "btnEnable"
        Me.btnEnable.Size = New System.Drawing.Size(151, 57)
        Me.btnEnable.TabIndex = 0
        Me.btnEnable.Text = "Enable"
        Me.btnEnable.UseVisualStyleBackColor = True
        '
        'btnDisable
        '
        Me.btnDisable.Location = New System.Drawing.Point(184, 12)
        Me.btnDisable.Name = "btnDisable"
        Me.btnDisable.Size = New System.Drawing.Size(151, 57)
        Me.btnDisable.TabIndex = 1
        Me.btnDisable.Text = "Disable"
        Me.btnDisable.UseVisualStyleBackColor = True
        '
        'btnCurrentKeyboardLayout
        '
        Me.btnCurrentKeyboardLayout.Location = New System.Drawing.Point(12, 75)
        Me.btnCurrentKeyboardLayout.Name = "btnCurrentKeyboardLayout"
        Me.btnCurrentKeyboardLayout.Size = New System.Drawing.Size(323, 20)
        Me.btnCurrentKeyboardLayout.TabIndex = 2
        Me.btnCurrentKeyboardLayout.Text = "Current keyboard layout"
        Me.btnCurrentKeyboardLayout.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(347, 107)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCurrentKeyboardLayout)
        Me.Controls.Add(Me.btnDisable)
        Me.Controls.Add(Me.btnEnable)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "IME Toolbar"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnEnable As Button
    Friend WithEvents btnDisable As Button
    Friend WithEvents btnCurrentKeyboardLayout As Button
End Class
