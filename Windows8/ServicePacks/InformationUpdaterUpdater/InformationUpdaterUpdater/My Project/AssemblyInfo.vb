Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("InformationUpdaterUpdater")>
<Assembly: AssemblyDescription("InformationUpdaterUpdater")>
<Assembly: AssemblyCompany("GuestTek")>
<Assembly: AssemblyProduct("InformationUpdaterUpdater")>
<Assembly: AssemblyCopyright("Copyright ©  2024")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("a8c35148-0cff-4e30-b820-08de10ad5d87")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.2.24128.1409")>

<Assembly: AssemblyFileVersion("1.2.24128.1409")>
<assembly: AssemblyInformationalVersion("0.0.24128.1409")>