Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("RemoveLegalNotice")>
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("RemoveLegalNotice")>
<Assembly: AssemblyCopyright("Copyright ©  2024")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("1950c60e-0e7c-4c4d-b5e4-ce34eab1572c")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.24068.1036")> 
<Assembly: AssemblyFileVersion("1.0.24068.1036")> 

<assembly: AssemblyInformationalVersion("0.0.24068.1036")>