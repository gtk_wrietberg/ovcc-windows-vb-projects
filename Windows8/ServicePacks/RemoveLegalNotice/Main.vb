﻿Module Main
    Public ReadOnly cREGKEY__LEGALNOTICE As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
    Public ReadOnly cREGVALUE__LEGALNOTICECAPTION As String = "legalnoticecaption"
    Public ReadOnly cREGVALUE__LEGALNOTICETEXT As String = "legalnoticetext"

    Public Sub Main()
        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("started", 1)


        Helpers.Logger.WriteMessage("Clearing registry keys", 0)
        Dim bRet As Boolean = True

        Helpers.Logger.WriteMessage(cREGKEY__LEGALNOTICE, 1)
        Helpers.Logger.WriteMessage(cREGVALUE__LEGALNOTICECAPTION, 2)
        bRet = bRet And Helpers.Registry64.SetValue_String(cREGKEY__LEGALNOTICE, cREGVALUE__LEGALNOTICECAPTION, "")
        If Not bRet Then
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(False, False))
        End If

        Helpers.Logger.WriteMessage(cREGVALUE__LEGALNOTICETEXT, 2)
        bRet = bRet And Helpers.Registry64.SetValue_String(cREGKEY__LEGALNOTICE, cREGVALUE__LEGALNOTICETEXT, "")
        If Not bRet Then
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(False, False))
        End If


        If Not bRet Then
            ExitCode.SetValue(ExitCode.ExitCodes.REGISTRY_ERROR)
        End If


        ExitApplication()
    End Sub

    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
