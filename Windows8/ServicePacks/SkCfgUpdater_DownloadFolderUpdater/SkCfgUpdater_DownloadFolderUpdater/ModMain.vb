Imports System.Text.RegularExpressions

Module ModMain
    Private mSkCfgFile As String
    Private mConfigChanged As Boolean = False

    Public Sub Main()
        InitGlobals()

        'Folders to add to clean queue
        g_List_FoldersCleaned.Add("C:\Users\Public")
        g_List_FoldersCleaned.Add("C:\Users\Public\Documents\SiteKiosk")
        g_List_FoldersCleaned.Add("C:\Users\SiteKiosk\Downloads")
        g_List_FoldersCleaned.Add("C:\Users\SiteKiosk\AppData\Roaming\Skype")
        g_List_FoldersCleaned.Add("C:\Users\SiteKiosk\Documents")
        g_List_FoldersCleaned.Add("C:\Users\SiteKiosk\Desktop")
        g_List_FoldersCleaned.Add("C:\BLAATAAP")

        g_List_FoldersVisible.Add("C:\Users\SiteKiosk\Documents")
        g_List_FoldersVisible.Add("C:\Users\SiteKiosk\Desktop")
        g_List_FoldersVisible.Add("C:\BLAATAAP")


        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("searching for SkCfg file", , 0)
        mSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        oLogger.WriteToLog("found: " & mSkCfgFile, , 1)

        If Not IO.File.Exists(mSkCfgFile) Then
            oLogger.WriteToLog("Nothing found in HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 2)
        End If

        oLogger.WriteToLog("adding the following cleaned folders", , 0)
        For Each sFolder As String In g_List_FoldersCleaned
            oLogger.WriteToLog(sFolder, , 1)
        Next


        oLogger.WriteToLog("patching", , 0)
        SkCfg_Update()

        Bye()
    End Sub

    Private Sub Bye()
        oLogger.WriteToLog("Bye", , 0)
        oLogger.WriteToLog(New String("=", 50))
    End Sub

    Private Function SkCfg_Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode

            Dim mXmlNode_DownloadManager As Xml.XmlNode
            Dim mXmlNode_DownloadFolder As Xml.XmlNode
            Dim mXmlNode_DeleteDirectories As Xml.XmlNode
            Dim mXmlNode_Dirs As Xml.XmlNodeList
            Dim mXmlNode_Dir As Xml.XmlNode
            Dim mXmlNode_NewDir As Xml.XmlNode

            Dim mXmlNode_FileManager As Xml.XmlNode
            Dim mXmlNode_VisiblePaths As Xml.XmlNode
            Dim mXmlNode_Urls As Xml.XmlNodeList
            Dim mXmlNode_Url As Xml.XmlNode
            Dim mXmlNode_NewUrl As Xml.XmlNode


            mSkCfgXml = New Xml.XmlDocument

            oLogger.WriteToLog("loading", , 1)
            mSkCfgXml.Load(mSkCfgFile)
            oLogger.WriteToLog("ok", , 2)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLog("root node", , 1)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            oLogger.WriteToLog("download-manager node", , 1)
            mXmlNode_DownloadManager = mXmlNode_Root.SelectSingleNode("sk:download-manager", ns)
            If Not mXmlNode_DownloadManager Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_DownloadManager Is Nothing")
            End If


            oLogger.WriteToLog("download-folder node", , 1)
            mXmlNode_DownloadFolder = mXmlNode_DownloadManager.SelectSingleNode("sk:download-folder", ns)
            If Not mXmlNode_DownloadFolder Is Nothing Then
                oLogger.WriteToLog("currently", , 2)
                oLogger.WriteToLog(mXmlNode_DownloadFolder.InnerText, , 3)

                If mXmlNode_DownloadFolder.InnerText.Equals(c_PATH_DownloadFolder) Then
                    oLogger.WriteToLog("ok", , 4)
                Else
                    oLogger.WriteToLog("changed to", , 2)
                    mXmlNode_DownloadFolder.InnerText = c_PATH_DownloadFolder
                    oLogger.WriteToLog(mXmlNode_DownloadFolder.InnerText, , 3)

                    mConfigChanged = True
                End If
            Else
                Throw New Exception("mXmlNode_DownloadFolder Is Nothing")
            End If


            oLogger.WriteToLog("delete-directories node", , 1)
            mXmlNode_DeleteDirectories = mXmlNode_DownloadManager.SelectSingleNode("sk:delete-directories", ns)
            If Not mXmlNode_DeleteDirectories Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_DeleteDirectories Is Nothing")
            End If

            oLogger.WriteToLog("dir nodelist", , 1)
            mXmlNode_Dirs = mXmlNode_DeleteDirectories.SelectNodes("sk:dir", ns)
            If Not mXmlNode_Dirs Is Nothing Then
                oLogger.WriteToLog("ok", , 2)

                oLogger.WriteToLog("checking for duplicates", , 1)
                For Each mXmlNode_Dir In mXmlNode_Dirs
                    If g_List_FoldersCleaned.Contains(mXmlNode_Dir.InnerText) Then
                        oLogger.WriteToLog("already there: " & mXmlNode_Dir.InnerText, , 2)
                        g_List_FoldersCleaned.Remove(mXmlNode_Dir.InnerText)
                    End If
                Next
            Else
                oLogger.WriteToLog("empty", , 2)
                'Throw New Exception("mXmlNode_Dirs Is Nothing")
            End If

            If g_List_FoldersCleaned.Count > 0 Then
                If g_List_FoldersCleaned.Count > 1 Then
                    oLogger.WriteToLog("adding " & g_List_FoldersCleaned.Count & " folders", , 1)
                Else
                    oLogger.WriteToLog("adding 1 folder", , 1)
                End If


                For Each sFolder As String In g_List_FoldersCleaned
                    oLogger.WriteToLog(sFolder, , 2)

                    mXmlNode_NewDir = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "dir", ns.LookupNamespace("sk"))
                    mXmlNode_NewDir.InnerText = sFolder

                    mXmlNode_DeleteDirectories.AppendChild(mXmlNode_NewDir)
                Next

                mConfigChanged = True
            Else
                oLogger.WriteToLog("no folders to add", , 1)
            End If


            oLogger.WriteToLog("filemanager node", , 1)
            mXmlNode_FileManager = mXmlNode_Root.SelectSingleNode("sk:filemanager", ns)
            If Not mXmlNode_FileManager Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_FileManager Is Nothing")
            End If

            oLogger.WriteToLog("visible-paths node", , 1)
            mXmlNode_VisiblePaths = mXmlNode_FileManager.SelectSingleNode("sk:visible-paths", ns)
            If Not mXmlNode_VisiblePaths Is Nothing Then
                oLogger.WriteToLog("ok", , 2)
            Else
                Throw New Exception("mXmlNode_VisiblePaths Is Nothing")
            End If

            oLogger.WriteToLog("url nodelist", , 1)
            mXmlNode_Urls = mXmlNode_VisiblePaths.SelectNodes("sk:url", ns)
            If Not mXmlNode_Urls Is Nothing Then
                oLogger.WriteToLog("ok", , 2)

                oLogger.WriteToLog("checking for duplicates", , 1)
                For Each mXmlNode_Url In mXmlNode_Urls
                    If g_List_FoldersVisible.Contains(mXmlNode_Url.InnerText) Then
                        oLogger.WriteToLog("already there: " & mXmlNode_Url.InnerText, , 2)
                        g_List_FoldersVisible.Remove(mXmlNode_Url.InnerText)
                    End If
                Next
            Else
                oLogger.WriteToLog("empty", , 2)
                'Throw New Exception("mXmlNode_Urls Is Nothing")
            End If


            If g_List_FoldersVisible.Count > 0 Then
                If g_List_FoldersVisible.Count > 1 Then
                    oLogger.WriteToLog("adding " & g_List_FoldersVisible.Count & " folders", , 1)
                Else
                    oLogger.WriteToLog("adding 1 folder", , 1)
                End If


                For Each sFolder As String In g_List_FoldersVisible
                    oLogger.WriteToLog(sFolder, , 2)

                    mXmlNode_NewUrl = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
                    mXmlNode_NewUrl.InnerText = sFolder

                    mXmlNode_VisiblePaths.AppendChild(mXmlNode_NewUrl)
                Next

                mConfigChanged = True
            Else
                oLogger.WriteToLog("no folders to add", , 1)
            End If





            oLogger.WriteToLog("saving", , 0)
            If mConfigChanged Then
                oLogger.WriteToLog("creating backup of '" & mSkCfgFile & "'", , 1)
                BackupFile(New IO.FileInfo(mSkCfgFile))

                oLogger.WriteToLog("saving existing file", , 1)
                mSkCfgXml.Save(mSkCfgFile)
                oLogger.WriteToLog("ok", , 2)
            Else
                oLogger.WriteToLog("skipped, nothing was changed", , 1)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", , 2)
            oLogger.WriteToLog(ex.Message, , 3)

            Return False
        End Try
    End Function

    Private Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        IO.Directory.CreateDirectory(g_BackupDirectory)

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = g_BackupDirectory & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLogRelative("writing: " & sFullDest, , 1)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLogRelative("done", , 2)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function

End Module

