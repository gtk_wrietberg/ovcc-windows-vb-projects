Public Class SkCfg
    Private Shared mOldSkCfgFile As String
    Private Shared mNewSkCfgFile As String
    Private Shared mBackupFolder As String
    Private Shared mSiteKioskFolder As String

    Public Shared Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Shared ReadOnly Property SiteKioskVersion As Integer
        Get
            Dim sTmp As String = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "Build", "0")
            Dim iTmp As Integer = 0

            sTmp = sTmp.Substring(0, sTmp.IndexOf(".") - 1)

            If Integer.TryParse(sTmp, iTmp) Then
                Return iTmp
            Else
                Return -1
            End If
        End Get
    End Property

    Public Shared Function Update(Optional ByVal bOverWriteOldFile As Boolean = False) As Boolean
        Try
            oLogger.WriteToLogRelative("finding SiteKiosk install directory", , 1)
            mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            oLogger.WriteToLogRelative("found: " & mSiteKioskFolder, , 2)

            oLogger.WriteToLogRelative("finding active skcfg file", , 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            oLogger.WriteToLogRelative("found: " & mOldSkCfgFile, , 2)


            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            If bOverWriteOldFile Then
                oLogger.WriteToLogRelative("backing up current config", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)

                Dim sBackupPath As String = oFile.DirectoryName
                Dim sBackupName As String = oFile.Name
                Dim sBackupFile As String
                Dim oDirInfo As System.IO.DirectoryInfo
                sBackupPath = sBackupPath.Replace("C:", "")
                sBackupPath = sBackupPath.Replace("c:", "")
                sBackupPath = sBackupPath.Replace("\\", "\")
                sBackupPath = mBackupFolder & "\" & sBackupPath

                sBackupFile = sBackupPath & "\" & sBackupName

                oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
                If Not oDirInfo.Exists Then oDirInfo.Create()

                oFile = New IO.FileInfo(mOldSkCfgFile)
                oFile.CopyTo(sBackupFile, True)

                oLogger.WriteToLogRelative("backup: " & mOldSkCfgFile & " => " & sBackupFile, , 2)

                mNewSkCfgFile = mOldSkCfgFile
            Else
                oLogger.WriteToLogRelative("making copy", , 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)
                mNewSkCfgFile = oFile.DirectoryName & "\GuestTek__InfoUpdater__" & sDateString & ".skcfg"
                oFile.CopyTo(mNewSkCfgFile, True)

                oLogger.WriteToLogRelative("copy: " & mNewSkCfgFile, , 2)
            End If


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If


            '*************************************************************************************************************
            'Download folder config 
            oLogger.WriteToLogRelative("download-manager update", , 2)
            Dim xmlNode_DownloadManager As Xml.XmlNode
            Dim xmlNode_DownloadFolder As Xml.XmlNode

            oLogger.WriteToLogRelative("loading download-manager node", , 3)
            xmlNode_DownloadManager = xmlNode_Root.SelectSingleNode("sk:download-manager", ns)

            oLogger.WriteToLogRelative("loading download-folder node", , 4)
            xmlNode_DownloadFolder = xmlNode_DownloadManager.SelectSingleNode("sk:download-folder", ns)

            oLogger.WriteToLogRelative("new value", , 5)
            oLogger.WriteToLogRelative(c_SITEKIOSK_DOWNLOAD_FOLDER_FULL, , 6)
            xmlNode_DownloadFolder.InnerText = c_SITEKIOSK_DOWNLOAD_FOLDER_FULL


            '*************************************************************************************************************
            'Saving
            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Save(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)

            If Not bOverWriteOldFile Then
                oLogger.WriteToLogRelative("activating", , 2)
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

                Dim sTempRegVal As String
                sTempRegVal = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

                If sTempRegVal = mNewSkCfgFile Then
                    oLogger.WriteToLogRelative("ok", , 3)
                Else
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_WARNING, 3)

                    oLogger.WriteToLogRelative("trying to fix this by overwriting the existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 4)

                    oLogger.WriteToLogRelative("backing up existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mOldSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile & "." & sDateString & "." & Application.ProductName & ".backup", True)

                    'Application .ProductName 

                    oLogger.WriteToLogRelative("overwriting existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mNewSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile, True)
                End If
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Return False
        End Try
    End Function
End Class
