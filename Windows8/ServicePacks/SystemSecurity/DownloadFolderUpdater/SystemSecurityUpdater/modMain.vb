﻿Imports System.IO

Module modMain
    Public ReadOnly c_SITEKIOSK_DOWNLOAD_FOLDER As String = "$(UserProfileDirectory)\Downloads"
    Public ReadOnly c_SITEKIOSK_DOWNLOAD_FOLDER_FULL As String = "C:\Users\SiteKiosk\Downloads"

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        oLogger.WriteToLog("Check SiteKiosk version", , 0)
        oLogger.WriteToLog("version", , 1)
        oLogger.WriteToLog(SkCfg.SiteKioskVersion.ToString, , 2)


        '------------------------------------------------------
        oLogger.WriteToLog("Update SkCfg", , 0)
        SkCfg.BackupFolder = g_BackupDirectory
        SkCfg.Update(True)


        '------------------------------------------------------
        oLogger.WriteToLog("Update security settings", , 0)
        SetDownloadFolderSecurity()


        '------------------------------------------------------
        oLogger.WriteToLog("Apply security settings", , 0)
        ApplyDefaultSecurity()


        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub

    Public Sub ApplyDefaultSecurity()
        oLogger.WriteToLogRelative("start security manager", , 1)
        oLogger.WriteToLogRelative(g_SystemSecurityManagerFile & " /applydefault", , 2)

        Dim watch As Stopwatch = Stopwatch.StartNew()

        Dim p As Process = Process.Start(g_SystemSecurityManagerFile, "/applydefault")

        oLogger.WriteToLogRelative("pid: " & p.Id.ToString, , 2)

        If Not p.WaitForExit(30000) Then
            oLogger.WriteToLogRelative("TIMEOUT!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        watch.Stop()
        oLogger.WriteToLogRelative("elapsed time: " & watch.ElapsedMilliseconds.ToString & " ms", , 2)
    End Sub

    Public Sub SetDownloadFolderSecurity()
        Try
            Dim sSwConfig As String = ""

            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(g_SwConfigFile, , 3)
            xmlSkCfg.Load(g_SwConfigFile)
            oLogger.WriteToLogRelative("ok", , 4)


            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("config")

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("config")
            End If


            '*************************************************************************************************************
            oLogger.WriteToLogRelative("Update directories", , 2)
            Dim xmlNode_Directories As Xml.XmlNode
            Dim xmlNodeList_Directories As Xml.XmlNodeList
            Dim xmlNode_Directory As Xml.XmlNode
            Dim xmlNode_DirectoryClone As Xml.XmlNode
            Dim xmlNode_DirectoryCloneClone As Xml.XmlNode

            oLogger.WriteToLogRelative("loading directories node", , 3)
            xmlNode_Directories = xmlNode_Root.SelectSingleNode("directories")

            xmlNode_DirectoryClone = xmlNode_Directories.SelectSingleNode("directory").CloneNode(True)
            xmlNodeList_Directories = xmlNode_Directories.SelectNodes("directory")

            oLogger.WriteToLogRelative("removing current download folder directory, if any", , 4)
            For Each xmlNode_Directory In xmlNodeList_Directories
                If xmlNode_Directory.Attributes.GetNamedItem("path").Value = c_SITEKIOSK_DOWNLOAD_FOLDER Then
                    oLogger.WriteToLogRelative("found one", , 5)
                    oLogger.WriteToLogRelative("deleted", , 6)
                End If
            Next


            oLogger.WriteToLogRelative("adding download folder", , 4)
            xmlNode_DirectoryCloneClone = xmlNode_DirectoryClone.CloneNode(True)
            oLogger.WriteToLogRelative("path", , 5)
            oLogger.WriteToLogRelative(c_SITEKIOSK_DOWNLOAD_FOLDER, , 6)
            oLogger.WriteToLogRelative("read", , 5)
            oLogger.WriteToLogRelative("true", , 6)
            oLogger.WriteToLogRelative("write", , 5)
            oLogger.WriteToLogRelative("true", , 6)
            oLogger.WriteToLogRelative("execute", , 5)
            oLogger.WriteToLogRelative("true", , 6)

            xmlNode_DirectoryCloneClone.Attributes.GetNamedItem("path").Value = c_SITEKIOSK_DOWNLOAD_FOLDER
            xmlNode_DirectoryCloneClone.Attributes.GetNamedItem("read").Value = "true"
            xmlNode_DirectoryCloneClone.Attributes.GetNamedItem("write").Value = "true"
            xmlNode_DirectoryCloneClone.Attributes.GetNamedItem("execute").Value = "true"

            xmlNode_Directories.AppendChild(xmlNode_DirectoryCloneClone)


            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(g_SwConfigFile, , 3)
            xmlSkCfg.Save(g_SwConfigFile)
            oLogger.WriteToLogRelative("ok", , 4)
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

        End Try
    End Sub
End Module
