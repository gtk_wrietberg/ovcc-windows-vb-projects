Module Globals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupBaseDirectory As String
    Public g_BackupDirectory As String
    Public g_BackupDate As String
    Public g_SwConfigFile As String
    Public g_SystemSecurityManagerFile As String

    Public g_TESTMODE As Boolean


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        Dim sProgFiles As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sProgFiles.Equals(String.Empty) Then
            sProgFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        g_SwConfigFile = sProgFiles & "\SiteKiosk\SystemSecurity\swconfig.xml"
        g_SystemSecurityManagerFile = sProgFiles & "\SiteKiosk\SystemSecurity.exe"


        g_BackupDate = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        g_LogFileDirectory = sProgFiles & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupBaseDirectory = sProgFiles & "\GuestTek\_backups"
        g_BackupDirectory = g_BackupBaseDirectory & "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & g_BackupDate

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try

    End Sub


End Module
