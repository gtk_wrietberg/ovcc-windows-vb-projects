Module Main
    Private mAppsToKill As List(Of String)

    Public Sub Main()
        InitGlobals()


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.LogFilePath = g_LogFileDirectory

        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        '------------------------------------------------------
        mAppsToKill = New List(Of String)

        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, "--kill-app:") > 0 Then
                mAppsToKill.Add(arg.Replace("--kill-app:", ""))
            End If
        Next


        '------------------------------------------------------
        'kill apps if specified
        Helpers.Logger.Write("Kill apps", , 0)

        If mAppsToKill.Count <= 0 Then
            Helpers.Logger.Write("No apps needed killing", , 1)
        Else
            Dim iKilled As Integer = 0

            For i As Integer = 0 To mAppsToKill.Count - 1
                Helpers.Logger.Write("killing", , 1)
                Helpers.Logger.Write(mAppsToKill.Item(i), , 2)

                iKilled = Helpers.Processes.KillProcesses(mAppsToKill.Item(i))
                If iKilled < 0 Then
                    'oops
                    Helpers.Logger.Write("FAIL!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Else
                    Helpers.Logger.Write("ok (x" & iKilled & ")", , 3)
                End If
            Next
        End If


        '------------------------------------------------------
        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles = New Helpers.FilesAndFolders.CopyFiles With {
            .BackupDirectory = g_BackupDirectory,
            .SourceDirectory = "files",
            .DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder(True)
        }
        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        Helpers.Logger.Write("Creating last activity file", , 0)
        Dim sLastActivityFile As String = Helpers.FilesAndFolders.GetWindowsFolder() & "\Temp\_LastActivity.tmp"

        Helpers.Logger.Write(sLastActivityFile, , 1)

        Try
            Helpers.Logger.Write("writing", , 2)

            Dim sWriter As New System.IO.StreamWriter(sLastActivityFile, False)
            sWriter.Write("<lastactivity>2517393587</lastactivity>")
            sWriter.Close()

            Helpers.Logger.Write("ok", , 3)

            Helpers.Logger.Write("updating permissions", , 2)
            If Helpers.FilesAndFolders.Permissions.FileSecurity_REPLACE__Full_Everyone(sLastActivityFile) Then
                Helpers.Logger.Write("ok", , 3)
            Else
                Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Helpers.Logger.Write(Helpers.Errors.GetLast(False), Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If
        Catch ex As Exception
            Helpers.Logger.Write("fail, exception", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.Write(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try


        '------------------------------------------------------
        Helpers.Logger.Write("Updating guest-tek.js file to remove TimeWarning debugging", , 0)
        If UpdateGuestTekJs() Then
            Helpers.Logger.Write("ok", , 1)
        Else
            Helpers.Logger.Write("fail", , 1)
        End If


        '------------------------------------------------------
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("bye", , 1)
    End Sub

    Public Function UpdateGuestTekJs() As Boolean
        Dim lines As New List(Of String)
        Dim bChanged As Boolean = False

        Helpers.Logger.WriteMessageRelative("reading script file", 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\default\Scripts\guest-tek.js"

        If Not IO.File.Exists(sFile) Then
            Helpers.Logger.WriteErrorRelative("FAIL", 2)

            Return False
        Else
            Helpers.Logger.WriteMessageRelative("ok", 2)
        End If

        Try
            Helpers.Logger.WriteMessageRelative("setting values", 1)

            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine 'var debugTimeWarning=1;

                    If line.Contains("var debugTimeWarning=1;") Then
                        bChanged = True

                        line = "var debugTimeWarning=0;"

                        Helpers.Logger.WriteMessageRelative("updating", 2)
                        Helpers.Logger.WriteMessageRelative("var debugTimeWarning=1;  ==>  var debugTimeWarning=0;", 3)
                    End If

                    lines.Add(line)
                End While
            End Using

            Helpers.Logger.WriteMessageRelative("writing script file", 1)
            If bChanged Then
                Helpers.FilesAndFolders.Backup.File(sFile, g_BackupDirectory)

                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using

                Helpers.Logger.WriteMessageRelative("ok", 2)
            Else
                Helpers.Logger.WriteMessageRelative("no change, no save needed", 2)
            End If

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("FAIL", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            Return False
        End Try
    End Function
End Module

