﻿Imports System.ComponentModel

Public Class frmMain
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Forms.OnTop.Put(Me.Handle)


        '------------------------------------------------------
        InitGlobals()


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.LogFilePath = g_LogFileDirectory

        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        tmrStart.Enabled = True
    End Sub

    Private Sub bgWork_DoWork(sender As Object, e As ComponentModel.DoWorkEventArgs) Handles bgWork.DoWork
        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles = New Helpers.FilesAndFolders.CopyFiles With {
            .BackupDirectory = g_BackupDirectory,
            .SourceDirectory = "files",
            .DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder(True)
        }
        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()
    End Sub

    Private Sub tmrStart_Tick(sender As Object, e As EventArgs) Handles tmrStart.Tick
        tmrStart.Enabled = False

        bgWork.RunWorkerAsync()
    End Sub

    Private Sub bgWork_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgWork.RunWorkerCompleted
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("bye", , 1)

        pBar.Visible = 0
        lblText.Text = "Done!"
        Me.ControlBox = True
        tmrDone.Enabled = True
    End Sub

    Private Sub tmrDone_Tick(sender As Object, e As EventArgs) Handles tmrDone.Tick
        Helpers.Logger.Write("(auto-close)", , 2)

        Me.Close()
    End Sub
End Class