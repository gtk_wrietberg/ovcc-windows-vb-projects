﻿Module modMain
    Public Sub Main()
        ExitCode.SetValue(ExitCode.ExitCodes.OK)


        InitGlobals()


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.LogFilePath = g_LogFileDirectory

        Helpers.Logger.WriteMessage(New String("*", 50))
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)

        Helpers.Logger.Debugging = False


        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("init", 0)
        Dim sTmp As String = ""

        Dim args() As String = Environment.GetCommandLineArgs().Skip(1).ToArray 'Skip first element (exe name) ; LINQ
        For Each arg As String In args
            Helpers.Logger.WriteMessage("param", 1)
            Helpers.Logger.WriteMessage(arg, 2)

            If InStr(arg, "--block-edge-first-run") > 0 Or
                InStr(arg, "--block-edge") > 0 Or
                InStr(arg, "--block") > 0 Then
                g_FIRSTRUN = 0
            End If

            If InStr(arg, "--allow-edge-first-run") > 0 Or
                InStr(arg, "--allow-edge") > 0 Or
                InStr(arg, "--allow") > 0 Then
                g_FIRSTRUN = 1
            End If
        Next


        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        If g_FIRSTRUN < 0 Or g_FIRSTRUN > 1 Then
            Helpers.Logger.WriteError("param error", 0)
            Helpers.Logger.WriteMessage("'g_FIRSTRUN' = " & g_FIRSTRUN.ToString, 1)

            ExitCode.SetValue(ExitCode.ExitCodes.INCORRECT_PARAM)
            ExitApplication()
        End If

        Helpers.Logger.WriteMessage("does key '" & c_REGKEY & "' exist?", 0)
        If Not Helpers.Registry64.KeyExists(c_REGKEY, sTmp) Then
            Helpers.Logger.WriteMessage("nope", 1)
            Helpers.Logger.WriteMessage("'Helpers.Registry64.KeyExists' says: " & sTmp, 2)

            Helpers.Logger.WriteMessage("creating", 1)
            If Helpers.Registry64.CreateKey(c_REGKEY) Then
                Helpers.Logger.WriteMessage("ok", 2)
            Else
                Helpers.Logger.WriteError("fail", 2)
                Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 3)

                ExitCode.SetValue(ExitCode.ExitCodes.ERROR_OCCURRED)
                ExitApplication()
            End If
        Else
            Helpers.Logger.WriteMessage("yep", 1)
        End If

        If g_FIRSTRUN = 0 Then
            'block edge
            Helpers.Logger.WriteMessage("blocking edge from auto starting", 0)

            If Not Helpers.Registry64.SetValue_Integer(c_REGKEY, c_REGVAL, 1) Then
                Helpers.Logger.WriteError("error", 1)
                Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
            Else
                Helpers.Logger.WriteMessage("ok", 1)
            End If
        End If

        If g_FIRSTRUN = 1 Then
            'allow edge
            Helpers.Logger.WriteMessage("allowing edge from auto starting", 0)

            If Not Helpers.Registry64.DeleteValue(c_REGKEY, c_REGVAL) Then
                Helpers.Logger.WriteError("error", 1)
                Helpers.Logger.WriteError(Helpers.Errors.GetLast(False), 2)
            Else
                Helpers.Logger.WriteMessage("ok", 1)
            End If
        End If


        ExitApplication()
    End Sub

    Private Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

End Module
