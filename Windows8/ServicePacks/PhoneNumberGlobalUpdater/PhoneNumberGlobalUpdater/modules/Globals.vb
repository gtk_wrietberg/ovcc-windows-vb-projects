Module Globals
    Public oLogger As Logger
    ' Public oSkCfg As SkCfg

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String
    Public g_OverwriteExistingConfig As Boolean

    Public g_TESTMODE As Boolean = False


    Public ReadOnly c_PARAM_TESTMODE As String = "--testmode"
    Public ReadOnly c_PARAM_PHONEOLD As String = "--oldphone:"
    Public ReadOnly c_PARAM_PHONENEW As String = "--newphone:"


    Public ReadOnly c_PATH_language_files As String = "Language\Browserskins\Default-Ieskin"


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        Try
            System.IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try
        Try
            System.IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try
    End Sub
End Module
