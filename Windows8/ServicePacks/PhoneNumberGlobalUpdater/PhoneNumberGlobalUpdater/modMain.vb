﻿Module modMain
    Private ReadOnly cPhoneUrlString As String = "&lt;a href=""http://www.guesttek.com/hotel-staff-support/"" target=""_blank""&gt;www.guesttek.com/hotel-staff-support/&lt;/a&gt;"

    Public Sub Main()
        If Helpers.Generic.IsDevMachine Then
            g_TESTMODE = True
        End If

        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))
        oLogger.WriteToLog(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        oLogger.WriteToLog("init", , 0)
        Dim sTmp As String = ""

        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, c_PARAM_TESTMODE) > 0 Then
                g_TESTMODE = True
            End If
        Next


        '------------------------------------------------------
        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles = New Helpers.FilesAndFolders.CopyFiles With {
            .BackupDirectory = g_BackupDirectory,
            .SourceDirectory = "files",
            .DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder(True)
        }
        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        oLogger.WriteToLog("updating language files", , 0)
        Dim sSKPath As String = Helpers.SiteKiosk.GetSiteKioskInstallFolder & "\" & c_PATH_language_files

        oLogger.WriteToLog("language folder", , 1)
        oLogger.WriteToLog(sSKPath, , 2)

        If Not IO.Directory.Exists(sSKPath) Then
            oLogger.WriteToLog("not found!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            ExitCode.SetValue(ExitCode.ExitCodes.LANGUAGE_FOLDER_NOT_FOUND)

            ExitApplication()
        End If

        'find all xml files in folder
        oLogger.WriteToLog("getting files", , 1)

        Dim listFiles As New List(Of String)

        For Each file As String In My.Computer.FileSystem.GetFiles(sSKPath, FileIO.SearchOption.SearchTopLevelOnly, "*.xml")
            oLogger.WriteToLog("found: " & file, , 2)
            listFiles.Add(file)
        Next

        oLogger.WriteToLog("total: " & listFiles.Count, , 2)

        If listFiles.Count = 0 Then
            oLogger.WriteToLog("no language files found!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            ExitCode.SetValue(ExitCode.ExitCodes.NO_LANGUAGE_FILES_FOUND)

            ExitApplication()
        End If


        'edit the files
        For Each file As String In listFiles
            UpdateLanguageXmlFile(file)
        Next

        '-------------------------------------------------------------------------------------------------------------------------------------------------------------
        ExitApplication()
    End Sub

    Private Function UpdateLanguageXmlFile(sFile As String) As Boolean
        Try
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            Dim bFoundIt As Boolean = False


            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("loading", , 1)
            oLogger.WriteToLogRelative(sFile, , 2)
            xmlSkCfg.Load(sFile)
            oLogger.WriteToLogRelative("ok", , 3)

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("language")

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("root node (language) not found")
            End If


            oLogger.WriteToLogRelative("loading string nodelist", , 2)
            Dim xmlNodeList_Strings As Xml.XmlNodeList
            Dim xmlNode_String As Xml.XmlNode
            Dim sTmp As String = ""

            Dim sString As String = ""
            Dim pattern As String = "(.+)(\(|\（)(.+)(\)|\）)(:|：)(\s*)(.+)" 'Notice the funky colon (U+FF1A FULLWIDTH COLON) and brackets(U+FF08 and U+FF09), thanks China
            Dim replacement As String = "$1$5$6" & cPhoneUrlString
            Dim rgx As New System.Text.RegularExpressions.Regex(pattern)


            xmlNodeList_Strings = xmlNode_Root.SelectNodes("string")
            For Each xmlNode_String In xmlNodeList_Strings
                If xmlNode_String.Attributes.GetNamedItem("id").InnerText = "1100000" Then
                    'found the specific language string id, mark it for a backup
                    oLogger.WriteToLogRelative("found phone number string", , 3)
                    bFoundIt = True

                    'replace old phone number with new one
                    oLogger.WriteToLogRelative("replacing", , 3)

                    oLogger.WriteToLogRelative("was", , 4)
                    oLogger.WriteToLogRelative(xmlNode_String.InnerText, , 5)


                    sString = xmlNode_String.InnerText ' <string id="1100000">OneView Connection Centre helpdesk (gratis): 00800 3400 1111</string>
                    sTmp = rgx.Replace(sString, replacement)


                    oLogger.WriteToLogRelative("becomes", , 4)
                    oLogger.WriteToLogRelative(sTmp, , 5)

                    If sTmp.Equals(xmlNode_String.InnerText) Then
                        'nothing changed, wtf
                        oLogger.WriteToLogRelative("no change?!!", Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                        bFoundIt = False
                    Else
                        'xmlNode_String.InnerText = sTmp
                        xmlNode_String.InnerXml = sTmp

                        oLogger.WriteToLogRelative("ok", , 4)
                    End If
                End If
            Next


            If bFoundIt Then
                MakeBackup(sFile)

                oLogger.WriteToLogRelative("saving", , 1)
                xmlSkCfg.Save(sFile)
                oLogger.WriteToLogRelative("ok", , 2)
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            Return False
        End Try

    End Function

    Private Function MakeBackup(sFile As String) As Boolean
        oLogger.WriteToLogRelative("creating backup", , 1)

        Try
            Dim oFile As System.IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            oFile = New System.IO.FileInfo(sFile)

            Dim sBackupPath As String = oFile.DirectoryName
            Dim sBackupName As String = oFile.Name
            Dim sBackupFile As String
            Dim oDirInfo As System.IO.DirectoryInfo
            sBackupPath = sBackupPath.Replace("C:", "")
            sBackupPath = sBackupPath.Replace("c:", "")
            sBackupPath = sBackupPath.Replace("\\", "\")
            sBackupPath = g_BackupDirectory & "\" & sBackupPath

            sBackupFile = sBackupPath & "\" & sBackupName

            oLogger.WriteToLogRelative("backup: " & sFile & " => " & sBackupFile, , 2)

            oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oFile = New System.IO.FileInfo(sFile)
            oFile.CopyTo(sBackupFile, True)

            oLogger.WriteToLogRelative("ok", , 3)

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            Return False
        End Try
    End Function

    Private Sub ExitApplication()
        '-----------------------------------------------
        'done
        oLogger.WriteToLog("exiting", , 0)
        oLogger.WriteToLog("exit code", , 1)
        oLogger.WriteToLog(ExitCode.ToString(), , 2)
        oLogger.WriteToLog("bye", , 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub

End Module
