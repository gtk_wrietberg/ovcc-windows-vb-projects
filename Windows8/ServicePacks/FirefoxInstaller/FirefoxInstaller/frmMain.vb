﻿Imports System.IO
Imports System.IO.Compression
Imports System.Net
Imports System.Text.RegularExpressions
Imports System.Threading

Public Class frmMain
    Private WithEvents oProcess_UninstallFirefox As New ProcessRunner
    Private WithEvents oProcess_InstallFirefox As New ProcessRunner
    Private WithEvents oProcess_InitialiseFirefox As New ProcessRunner
    Private WithEvents oProcess_VerifyProfile As New ProcessRunner
    Private WithEvents oProcess_InstallLogoutHelper As New ProcessRunner

#Region "Constants & globals"
#Region "UI"
    Private UI_FIRST_PANEL_TOP As Integer = 0
#End Region

#Region "firefox download stuff"
    Private FIREFOX_DOWNLOAD_URL As String = ""
    Private OVCC_TEMP_FOLDER As String = ""
    Private FIREFOX_DOWNLOAD_FULLPATH As String = ""
    Private FIREFOX_INSTALLED_FULLPATH As String = ""
    Private FIREFOX_UNINSTALLER_FULLPATH As String = ""
    Private FIREFOX_UNINSTALL_EXISTING_VERSION As Boolean = False
    Private FIREFOX_PROFILE_TEMPLATE_FULLPATH As String = ""
    Private LOGOUTHELPER_INSTALLER_FULLPATH As String = ""
    Private LOGOUTHELPER_INSTALLED_BASEPATH As String = ""
#End Region

#Region "timeouts and delays and counters"
    Private APP_RETRIES_COUNTER As Integer = 0
#End Region

#Region "user stuff"
    Private SITEKIOSK_USER_OLD_PASSWORD As Boolean = False
#End Region
#End Region

#Region "Thread Safe stuff"
    Private threadPanelActions As Thread
    'Private threadPanelActions_Pause As New Threading.AutoResetEvent(False)

    Delegate Sub ThreadSafeCallBack__UpdateResult(ByVal [index] As Integer, ByVal [text] As String)

    Public Sub ThreadSafe__UpdateResult(ByVal [index] As Integer, ByVal [text] As String)
        If [index] < 0 Or [index] >= mPanels_Result.Count Then
            Exit Sub
        End If

        If mPanels_Result.Item([index]).InvokeRequired Then
            Dim d As New ThreadSafeCallBack__UpdateResult(AddressOf ThreadSafe__UpdateResult)
            Me.Invoke(d, New Object() {[index], [text]})

            Exit Sub
        End If


        Select Case [text]
            Case PanelResults.OK
                mPanels_Result.Item([index]).ForeColor = Color.DarkGreen
            Case PanelResults.SKIPPED
                mPanels_Result.Item([index]).ForeColor = Color.Orange
            Case Else
                mPanels_Result.Item([index]).ForeColor = Color.Black
        End Select

        mPanels_Result.Item([index]).Text = [text]
    End Sub


    Delegate Sub ThreadSafeCallBack__ThreadComplete([_result] As String)
    Public Sub ThreadSafe__ThreadComplete([_result] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__ThreadComplete(AddressOf ThreadSafe__ThreadComplete)
            Me.Invoke(d, New Object() {[_result]})

            Exit Sub
        End If


        If _result = PanelResults.OK Or _result = PanelResults.SKIPPED Then
            ThreadSafe__UpdateResult(mActivePanel, [_result])

            NextPanel()
        Else
            'ERROR

            ErrorPanel([_result])
        End If
    End Sub


    Delegate Sub ThreadSafeCallBack__ThreadCompleteAndJump([_result] As String, [_index] As Integer)
    Public Sub ThreadSafe__ThreadCompleteAndJump([_result] As String, [_index] As Integer)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__ThreadCompleteAndJump(AddressOf ThreadSafe__ThreadCompleteAndJump)
            Me.Invoke(d, New Object() {[_result], [_index]})

            Exit Sub
        End If

        If _result = PanelResults.OK Or _result = PanelResults.SKIPPED Then
            ThreadSafe__UpdateResult(mActivePanel, [_result])

            JumpToPanel([_index])
        Else
            'ERROR

            ErrorPanel([_result])
        End If
    End Sub


    Delegate Sub ThreadSafeCallBack__DownloadProgress([percentage] As Integer, [Progress_Speed] As String, [Progress_Timeleft] As String)
    Public Sub ThreadSafe__DownloadProgress([percentage] As Integer, [Progress_Speed] As String, [Progress_Timeleft] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__DownloadProgress(AddressOf ThreadSafe__DownloadProgress)
            Me.Invoke(d, New Object() {[percentage], [Progress_Speed], [Progress_Timeleft]})

            Exit Sub
        End If

        If percentage < progressDownloadFirefox.Minimum Or percentage > progressDownloadFirefox.Maximum Then
            percentage = progressDownloadFirefox.Minimum
        End If
        progressDownloadFirefox.Value = percentage

        lblProgress_Speed.Text = [Progress_Speed]
        lblProgress_Timeleft.Text = [Progress_Timeleft]
    End Sub


    Delegate Sub ThreadSafeCallBack__lblDownloadFirefox_URL([_text] As String)
    Public Sub ThreadSafe__lblDownloadFirefox_URL([_text] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__lblDownloadFirefox_URL(AddressOf ThreadSafe__lblDownloadFirefox_URL)
            Me.Invoke(d, New Object() {[_text]})

            Exit Sub
        End If

        lblDownloadFirefox_URL.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__FormOpacity([_opacity] As Double)
    Public Sub ThreadSafe__FormOpacity([_opacity] As Double)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__FormOpacity(AddressOf ThreadSafe__FormOpacity)
            Me.Invoke(d, New Object() {[_opacity]})

            Exit Sub
        End If

        Me.Opacity = _opacity
    End Sub


    Delegate Sub ThreadSafeCallBack__FormClose()
    Public Sub ThreadSafe__FormClose()
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__FormClose(AddressOf ThreadSafe__FormClose)
            Me.Invoke(d, New Object() {})

            Exit Sub
        End If

        Helpers.Logger.Write("exiting", , 0)
        Helpers.Logger.Write("ok", , 1)
        Helpers.Logger.Write("thanks", , 2)
        Helpers.Logger.Write("bye", , 3)
        Me.Close()
    End Sub


    Delegate Sub ThreadSafeCallBack__InitialiseFirefox_Controls_toggle([_text] As String, [_state] As Boolean)
    Public Sub ThreadSafe__InitialiseFirefox_Controls_toggle([_text] As String, [_state] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__InitialiseFirefox_Controls_toggle(AddressOf ThreadSafe__InitialiseFirefox_Controls_toggle)
            Me.Invoke(d, New Object() {[_text], [_state]})

            Exit Sub
        End If


        btnInitialiseFirefox_Yes.Enabled = [_state]
        btnInitialiseFirefox_No.Enabled = [_state]

        btnInitialiseFirefox_Yes.Visible = [_state]
        btnInitialiseFirefox_No.Visible = [_state]

        lblInitialiseFirefox_Text.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__InitialiseFirefox_Timeout([_text] As String)
    Public Sub ThreadSafe__InitialiseFirefox_Timeout([_text] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__InitialiseFirefox_Timeout(AddressOf ThreadSafe__InitialiseFirefox_Timeout)
            Me.Invoke(d, New Object() {[_text]})

            Exit Sub
        End If

        lblInitialiseFirefox_Timeout.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__btnInitialise_Option([_index] As Integer, [_text] As String, [_enabled] As Boolean, [_visible] As Boolean)
    Public Sub ThreadSafe__btnInitialise_Option([_index] As Integer, [_text] As String, [_enabled] As Boolean, [_visible] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__btnInitialise_Option(AddressOf ThreadSafe__btnInitialise_Option)
            Me.Invoke(d, New Object() {[_index], [_text], [_enabled], [_visible]})

            Exit Sub
        End If

        Select Case [_index]
            Case 0
                btnInitialise_Option1.Visible = [_visible]
                btnInitialise_Option2.Visible = [_visible]
                btnInitialise_Option3.Visible = [_visible]

                btnInitialise_Option1.Enabled = [_enabled]
                btnInitialise_Option2.Enabled = [_enabled]
                btnInitialise_Option3.Enabled = [_enabled]
            Case 1
                If Not [_text].Equals("") Then
                    btnInitialise_Option1.Text = [_text]
                End If

                btnInitialise_Option1.Visible = [_visible]
                btnInitialise_Option1.Enabled = [_enabled]
            Case 2
                If Not [_text].Equals("") Then
                    btnInitialise_Option2.Text = [_text]
                End If

                btnInitialise_Option2.Visible = [_visible]
                btnInitialise_Option2.Enabled = [_enabled]
            Case 3
                If Not [_text].Equals("") Then
                    btnInitialise_Option3.Text = [_text]
                End If

                btnInitialise_Option3.Visible = [_visible]
                btnInitialise_Option3.Enabled = [_enabled]
        End Select
    End Sub


    Delegate Sub ThreadSafeCallBack__Initialise_Text([_text] As String)
    Public Sub ThreadSafe__Initialise_Text([_text] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Initialise_Text(AddressOf ThreadSafe__Initialise_Text)
            Me.Invoke(d, New Object() {[_text]})

            Exit Sub
        End If

        lblInitialise_Text.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__Initialise_Text_Position([_x] As Integer, [_y] As Integer)
    Public Sub ThreadSafe__Initialise_Text_Position([_x] As Integer, [_y] As Integer)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Initialise_Text_Position(AddressOf ThreadSafe__Initialise_Text_Position)
            Me.Invoke(d, New Object() {[_x], [_y]})

            Exit Sub
        End If

        lblInitialise_Text.Top = [_y]
        lblInitialise_Text.Left = [_x]
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_tmrTimeout__InitialiseFirefox([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_tmrTimeout__InitialiseFirefox([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_tmrTimeout__InitialiseFirefox(AddressOf ThreadSafe__Toggle_tmrTimeout__InitialiseFirefox)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        tmrTimeout__InitialiseFirefox.Enabled = [_enabled]
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_tmrWindowVisible__InitialiseFirefox([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_tmrWindowVisible__InitialiseFirefox([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_tmrWindowVisible__InitialiseFirefox(AddressOf ThreadSafe__Toggle_tmrWindowVisible__InitialiseFirefox)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        tmrWindowVisible__InitialiseFirefox.Enabled = [_enabled]
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_progressUninstallFirefox([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_progressUninstallFirefox([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_progressUninstallFirefox(AddressOf ThreadSafe__Toggle_progressUninstallFirefox)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        With progressUninstallFirefox
            If [_enabled] Then
                .Style = ProgressBarStyle.Marquee
                .MarqueeAnimationSpeed = 20
            Else
                .Style = ProgressBarStyle.Continuous
                .Value = 0
            End If
        End With
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_progressInstallFirefox([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_progressInstallFirefox([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_progressInstallFirefox(AddressOf ThreadSafe__Toggle_progressInstallFirefox)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        With progressInstallFirefox
            If [_enabled] Then
                .Style = ProgressBarStyle.Marquee
                .MarqueeAnimationSpeed = 20
            Else
                .Style = ProgressBarStyle.Continuous
                .Value = 0
            End If
        End With
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_progressInstallProfile([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_progressInstallProfile([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_progressInstallProfile(AddressOf ThreadSafe__Toggle_progressInstallProfile)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        With progressInstallProfile
            If [_enabled] Then
                .Style = ProgressBarStyle.Marquee
                .MarqueeAnimationSpeed = 20
            Else
                .Style = ProgressBarStyle.Continuous
                .Value = 0
            End If
        End With
    End Sub


    Delegate Sub ThreadSafeCallBack__InstallProfile_Text([_text] As String)
    Public Sub ThreadSafe__InstallProfile_Text([_text] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Initialise_Text(AddressOf ThreadSafe__InstallProfile_Text)
            Me.Invoke(d, New Object() {[_text]})

            Exit Sub
        End If

        lblInstallProfile_Text.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__VerifyProfile_Controls_toggle([_text] As String, [_state] As Boolean)
    Public Sub ThreadSafe__VerifyProfile_Controls_toggle([_text] As String, [_state] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__InitialiseFirefox_Controls_toggle(AddressOf ThreadSafe__VerifyProfile_Controls_toggle)
            Me.Invoke(d, New Object() {[_text], [_state]})

            Exit Sub
        End If

        btnVerifyProfile_StartFirefox.Enabled = [_state]
        btnVerifyProfile_StartFirefox.Visible = [_state]

        lblVerifyProfile_Text.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__ToggleForm([_state] As Boolean)
    Public Sub ThreadSafe__ToggleForm([_state] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__ToggleForm(AddressOf ThreadSafe__ToggleForm)
            Me.Invoke(d, New Object() {[_state]})

            Exit Sub
        End If

        If [_state] Then
            Helpers.Forms.TopMost(Me.Handle, True)
            Me.WindowState = FormWindowState.Normal
        Else
            Helpers.Forms.TopMost(Me.Handle, False)
            Me.WindowState = FormWindowState.Minimized
        End If
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_progressInstallLogoutHelper([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_progressInstallLogoutHelper([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_progressInstallLogoutHelper(AddressOf ThreadSafe__Toggle_progressInstallLogoutHelper)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        With progressInstallLogoutHelper
            If [_enabled] Then
                .Style = ProgressBarStyle.Marquee
                .MarqueeAnimationSpeed = 20
            Else
                .Style = ProgressBarStyle.Continuous
                .Value = 0
            End If
        End With
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_progressCleanup([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_progressCleanup([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_progressCleanup(AddressOf ThreadSafe__Toggle_progressCleanup)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        With progressCleanup
            If [_enabled] Then
                .Style = ProgressBarStyle.Marquee
                .MarqueeAnimationSpeed = 20
            Else
                .Style = ProgressBarStyle.Continuous
                .Value = 0
            End If
        End With
    End Sub


    Delegate Sub ThreadSafeCallBack__SaveProfile_Text([_text] As String)
    Public Sub ThreadSafe__SaveProfile_Text([_text] As String)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Initialise_Text(AddressOf ThreadSafe__SaveProfile_Text)
            Me.Invoke(d, New Object() {[_text]})

            Exit Sub
        End If

        lblSaveProfile_Text.Text = [_text]
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_progressSaveProfile([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_progressSaveProfile([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_progressSaveProfile(AddressOf ThreadSafe__Toggle_progressSaveProfile)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        With progressSaveProfile
            If [_enabled] Then
                .Style = ProgressBarStyle.Marquee
                .MarqueeAnimationSpeed = 20
            Else
                .Style = ProgressBarStyle.Continuous
                .Value = 0
            End If
        End With
    End Sub


    Delegate Sub ThreadSafeCallBack__Toggle_progressUpdateConfiguration([_enabled] As Boolean)
    Public Sub ThreadSafe__Toggle_progressUpdateConfiguration([_enabled] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Toggle_progressUpdateConfiguration(AddressOf ThreadSafe__Toggle_progressUpdateConfiguration)
            Me.Invoke(d, New Object() {[_enabled]})

            Exit Sub
        End If

        With progressUpdateConfiguration
            If [_enabled] Then
                .Style = ProgressBarStyle.Marquee
                .MarqueeAnimationSpeed = 20
            Else
                .Style = ProgressBarStyle.Continuous
                .Value = 0
            End If
        End With
    End Sub


    Delegate Sub ThreadSafeCallBack__Done_Controls_toggle([_state] As Boolean)
    Public Sub ThreadSafe__Done_Controls_toggle([_state] As Boolean)
        If Me.InvokeRequired Then
            Dim d As New ThreadSafeCallBack__Done_Controls_toggle(AddressOf ThreadSafe__Done_Controls_toggle)
            Me.Invoke(d, New Object() {[_state]})

            Exit Sub
        End If

        btnDone_Close.Enabled = [_state]
        btnDone_Close.Visible = [_state]
    End Sub
#End Region

#Region "panel stuff"
    Public Enum ApplicationSteps As Integer
        Initialise = 0
        UninstallFirefox
        DownloadFirefox
        InstallFirefox
        InitialiseFirefox
        InstallProfile
        InstallLogoutHelper
        UpdateConfiguration
        VerifyProfile
        SaveProfile
        Cleanup
        Done
        [Error]
    End Enum
    Private mPanelActionsMax As Integer = [Enum].GetValues(GetType(ApplicationSteps)).Cast(Of ApplicationSteps)().Max() - 1 ' -1 because we don't count the error panel

    Public Structure PanelResults
        Public Const OK As String = "Ok"
        Public Const SKIPPED As String = "Skipped"
    End Structure

    Private mPanels_Small As New List(Of Panel)
    Private mPanels_Large As New List(Of Panel)
    Private mPanels_Title As New List(Of Label)
    Private mPanels_Result As New List(Of Label)

    Private mActivePanel As ApplicationSteps = ApplicationSteps.Initialise

    Private Sub ResetAllPanels()
        mPanels_Small = New List(Of Panel)
        mPanels_Small.Add(pnlSmall_Initialise)
        mPanels_Small.Add(pnlSmall_UninstallFirefox)
        mPanels_Small.Add(pnlSmall_DownloadFirefox)
        mPanels_Small.Add(pnlSmall_InstallFirefox)
        mPanels_Small.Add(pnlSmall_InitialiseFirefox)
        mPanels_Small.Add(pnlSmall_InstallProfile)
        mPanels_Small.Add(pnlSmall_InstallLogoutHelper)
        mPanels_Small.Add(pnlSmall_UpdateConfiguration)
        mPanels_Small.Add(pnlSmall_VerifyProfile)
        mPanels_Small.Add(pnlSmall_SaveProfile)
        mPanels_Small.Add(pnlSmall_Cleanup)
        mPanels_Small.Add(pnlSmall_Done)

        For _i As Integer = 0 To mPanels_Small.Count - 1
            mPanels_Small.Item(_i).Visible = True
            mPanels_Small.Item(_i).Width = InstallerConstants.UI.UI_SMALL_PANEL_WIDTH
            mPanels_Small.Item(_i).Height = InstallerConstants.UI.UI_SMALL_PANEL_HEIGHT
            mPanels_Small.Item(_i).Location = New Point(InstallerConstants.UI.UI_PANEL_MARGIN, UI_FIRST_PANEL_TOP + _i * (InstallerConstants.UI.UI_SMALL_PANEL_HEIGHT + InstallerConstants.UI.UI_PANEL_MARGIN))
        Next


        mPanels_Large = New List(Of Panel)
        mPanels_Large.Add(pnlLarge_Initialise)
        mPanels_Large.Add(pnlLarge_UninstallFirefox)
        mPanels_Large.Add(pnlLarge_DownloadFirefox)
        mPanels_Large.Add(pnlLarge_InstallFirefox)
        mPanels_Large.Add(pnlLarge_InitialiseFirefox)
        mPanels_Large.Add(pnlLarge_InstallProfile)
        mPanels_Large.Add(pnlLarge_InstallLogoutHelper)
        mPanels_Large.Add(pnlLarge_UpdateConfiguration)
        mPanels_Large.Add(pnlLarge_VerifyProfile)
        mPanels_Large.Add(pnlLarge_SaveProfile)
        mPanels_Large.Add(pnlLarge_Cleanup)
        mPanels_Large.Add(pnlLarge_Done)

        For Each _p As Panel In mPanels_Large
            _p.Visible = False
            _p.Enabled = False
            _p.Width = InstallerConstants.UI.UI_LARGE_PANEL_WIDTH
            _p.Height = InstallerConstants.UI.UI_LARGE_PANEL_HEIGHT
        Next


        mPanels_Title = New List(Of Label)
        mPanels_Title.Add(lblInitialise)
        mPanels_Title.Add(lblUninstallFirefox)
        mPanels_Title.Add(lblDownloadFirefox)
        mPanels_Title.Add(lblInstallFirefox)
        mPanels_Title.Add(lblInitialiseFirefox)
        mPanels_Title.Add(lblInstallProfile)
        mPanels_Title.Add(lblInstallLogoutHelper)
        mPanels_Title.Add(lblUpdateConfiguration)
        mPanels_Title.Add(lblVerifyProfile)
        mPanels_Title.Add(lblSaveProfile)
        mPanels_Title.Add(lblCleanup)
        mPanels_Title.Add(lblDone)


        mPanels_Result = New List(Of Label)
        mPanels_Result.Add(lblInitialise_Result)
        mPanels_Result.Add(lblUninstallFirefox_Result)
        mPanels_Result.Add(lblDownloadFirefox_Result)
        mPanels_Result.Add(lblInstallFirefox_Result)
        mPanels_Result.Add(lblInitialiseFirefox_Result)
        mPanels_Result.Add(lblInstallProfile_Result)
        mPanels_Result.Add(lblInstallLogoutHelper_Result)
        mPanels_Result.Add(lblUpdateConfiguration_Result)
        mPanels_Result.Add(lblVerifyProfile_Result)
        mPanels_Result.Add(lblSaveProfile_Result)
        mPanels_Result.Add(lblCleanup_Result)
        mPanels_Result.Add(lblDone_Result)

        For Each _l As Label In mPanels_Result
            _l.Text = ""
        Next


        mActivePanel = ApplicationSteps.Initialise

        RefreshPanels()
    End Sub

    Private Sub NextPanel()
        mActivePanel += 1

        If mActivePanel >= mPanelActionsMax Then
            mActivePanel = mPanelActionsMax
        End If
        If mActivePanel < 0 Then
            mActivePanel = ApplicationSteps.Initialise
        End If

        RefreshPanels()
    End Sub

    Private Sub JumpToPanel(_index As Integer)
        mActivePanel = _index

        If mActivePanel >= mPanelActionsMax Then
            mActivePanel = mPanelActionsMax
        End If
        If mActivePanel < 0 Then
            mActivePanel = ApplicationSteps.Initialise
        End If

        RefreshPanels()
    End Sub

    Private Sub RefreshPanels()
        For _i As Integer = 0 To mPanels_Large.Count - 1
            If _i = mActivePanel Then
                mPanels_Small.Item(_i).Visible = False

                mPanels_Large.Item(_i).Visible = True
                mPanels_Large.Item(_i).Enabled = True
            Else
                mPanels_Small.Item(_i).Visible = True

                mPanels_Large.Item(_i).Visible = False
                mPanels_Large.Item(_i).Enabled = False
            End If

            If _i < mActivePanel Then
                mPanels_Small.Item(_i).Location = New Point(InstallerConstants.UI.UI_PANEL_MARGIN, UI_FIRST_PANEL_TOP + _i * (mPanels_Small.Item(_i).Height + InstallerConstants.UI.UI_PANEL_MARGIN))
            ElseIf _i = mActivePanel Then
                mPanels_Large.Item(_i).Location = New Point(InstallerConstants.UI.UI_PANEL_MARGIN, UI_FIRST_PANEL_TOP + _i * (mPanels_Small.Item(_i).Height + InstallerConstants.UI.UI_PANEL_MARGIN))
            Else
                mPanels_Small.Item(_i).Location = New Point(InstallerConstants.UI.UI_PANEL_MARGIN, UI_FIRST_PANEL_TOP + _i * (mPanels_Small.Item(_i).Height + InstallerConstants.UI.UI_PANEL_MARGIN) + (mPanels_Large.Item(_i).Height - mPanels_Small.Item(_i).Height))
            End If
        Next

        pnlError.Visible = False
        pnlError.Enabled = False


        DoPanelAction()
    End Sub

    Private Sub DoPanelAction()
        Helpers.Logger.Write("executing action '" & mActivePanel.ToString & "'", , 0)

        Select Case mActivePanel
            Case ApplicationSteps.Initialise
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__Initialise))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.UninstallFirefox
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__UninstallFirefox))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.DownloadFirefox
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__DownloadFirefox))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.InstallFirefox
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__InstallFirefox))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.InitialiseFirefox
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__InitialiseFirefox))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.InstallProfile
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__InstallProfile))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.InstallLogoutHelper
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__InstallLogoutHelper))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.UpdateConfiguration
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__UpdateConfiguration))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.VerifyProfile
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__VerifyProfile))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.SaveProfile
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__SaveProfile))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.Cleanup
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__Cleanup))
                Me.threadPanelActions.Start()
            Case ApplicationSteps.Done
                'Me.threadPanelActions_Pause = New Threading.AutoResetEvent(False)
                Me.threadPanelActions = New Thread(New ThreadStart(AddressOf Me.PanelAction__Done))
                Me.threadPanelActions.Start()
        End Select
    End Sub
#End Region

#Region "error panel"
    Private Sub ErrorPanel(ErrorMessage As String)
        Helpers.Logger.WriteError("FATAL ERROR", 0)
        Helpers.Logger.WriteError("showing error message to user", 1)
        Helpers.Logger.WriteError(ErrorMessage, 2)

        ThreadSafe__FormOpacity(1.0)

        If mActivePanel >= mPanelActionsMax Or mActivePanel < 0 Then
            'uh?
            lblLarge_Error.Text = "Error while retrieving the progress step"
        Else
            lblLarge_Error.Text = mPanels_Title.Item(mActivePanel).Text
            lblErrorMessage.Text = ErrorMessage

            mPanels_Large.Item(mActivePanel).Visible = False
            mPanels_Large.Item(mActivePanel).Enabled = False
            mPanels_Small.Item(mActivePanel).Visible = False
            mPanels_Small.Item(mActivePanel).Enabled = False
            pnlError.Visible = True
            pnlError.Enabled = True
            pnlError.Location = New Point(InstallerConstants.UI.UI_PANEL_MARGIN, UI_FIRST_PANEL_TOP + mActivePanel * (mPanels_Small.Item(mActivePanel).Height + InstallerConstants.UI.UI_PANEL_MARGIN))

            If APP_RETRIES_COUNTER < InstallerConstants.TimeoutsDelaysCounters.APP_RETRIES_COUNTER_max Then
                Helpers.Logger.WriteError("retry allowed", 1)
                Helpers.Logger.WriteError((APP_RETRIES_COUNTER + 1).ToString & "/" & InstallerConstants.TimeoutsDelaysCounters.APP_RETRIES_COUNTER_max.ToString, 2)
                btnError_Retry.Enabled = True
            Else
                Helpers.Logger.WriteError("retry no longer allowed", 1)
                btnError_Retry.Enabled = False
            End If

            btnError_Cancel.Enabled = True
        End If
    End Sub

    Private Sub btnError_Retry_Click(sender As Object, e As EventArgs) Handles btnError_Retry.Click
        btnError_Retry.Enabled = False

        APP_RETRIES_COUNTER += 1

        Helpers.Logger.WriteError("user pressed '" & btnError_Retry.Text & "'", 1)

        ResetAllPanels()
    End Sub

    Private Sub btnError_Cancel_Click(sender As Object, e As EventArgs) Handles btnError_Cancel.Click
        Helpers.Logger.WriteError("user pressed '" & btnError_Cancel.Text & "'", 1)
        ThreadSafe__FormClose()
    End Sub

#End Region

#Region "panel actions"
#Region "Initialise"
    Private mInitialise__waiting As Boolean = False
    Private mInitialise__response As Integer = 0
    Private Sub PanelAction__Initialise()
        Helpers.Logger.WriteRelative("initialising", , 1)


        ThreadSafe__btnInitialise_Option(1, " ", False, False)
        ThreadSafe__btnInitialise_Option(2, " ", False, False)
        ThreadSafe__btnInitialise_Option(3, " ", False, False)

        ThreadSafe__Initialise_Text_Position(7, 43)

        ThreadSafe__Initialise_Text("One moment please")


        FIREFOX_DOWNLOAD_URL = InstallerConstants.Firefox.Download.FIREFOX_DOWNLOAD_URL_TEMPLATE
        If Environment.Is64BitOperatingSystem Then
            FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL.Replace("!!OS!!", InstallerConstants.Firefox.Download.FIREFOX_DOWNLOAD_WIN64)
        Else
            FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL.Replace("!!OS!!", InstallerConstants.Firefox.Download.FIREFOX_DOWNLOAD_WIN32)
        End If
        If Thread.CurrentThread.CurrentCulture.Name.Equals("en-US") Then
            FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL.Replace("!!LANG!!", InstallerConstants.Firefox.Download.FIREFOX_DOWNLOAD_LANG_US)
        Else
            FIREFOX_DOWNLOAD_URL = FIREFOX_DOWNLOAD_URL.Replace("!!LANG!!", InstallerConstants.Firefox.Download.FIREFOX_DOWNLOAD_LANG_GB)
        End If

        ThreadSafe__lblDownloadFirefox_URL(FIREFOX_DOWNLOAD_URL)


        OVCC_TEMP_FOLDER = IO.Path.Combine(IO.Path.GetTempPath(), "__OVCC_FIREFOX_DOWNLOAD__")
        FIREFOX_DOWNLOAD_FULLPATH = IO.Path.Combine(OVCC_TEMP_FOLDER, InstallerConstants.Firefox.Install.FIREFOX_DOWNLOAD_FILENAME)

        Helpers.Logger.WriteRelative("creating temp path", , 2)
        Helpers.Logger.WriteRelative(OVCC_TEMP_FOLDER, , 3)
        IO.Directory.CreateDirectory(OVCC_TEMP_FOLDER)


        LOGOUTHELPER_INSTALLED_BASEPATH = InstallerConstants.Firefox.Install.LOGOUTHELPER_INSTALLED_BASEPATH_TEMPLATE.Replace("%%PROGRAMFILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder)


        Helpers.Logger.WriteRelative("firefox download url", , 2)
        Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_URL, , 3)
        Helpers.Logger.WriteRelative("firefox download path", , 2)
        Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_FULLPATH, , 3)


        Helpers.Logger.WriteRelative("which sitekiosk user password do we use?", , 2)

        Dim sReturnMessage As String = ""

        Helpers.Logger.WriteRelative("testing new password", , 3)
        If Helpers.WindowsUser.CheckLogon(
            Constants.UsernamePassword.SiteKiosk.Username,
            Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password),
            sReturnMessage) Then

            SITEKIOSK_USER_OLD_PASSWORD = False

            Helpers.Logger.WriteRelative("ok", , 4)
        Else
            Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 4)
            Helpers.Logger.WriteRelative(sReturnMessage, , 5)

            Helpers.Logger.WriteRelative("testing old password", , 3)

            If Helpers.WindowsUser.CheckLogon(
                Constants.UsernamePassword.SiteKiosk.Username,
                Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password_old),
                sReturnMessage) Then

                SITEKIOSK_USER_OLD_PASSWORD = True

                Helpers.Logger.WriteRelative("ok", , 4)
            Else
                Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                Helpers.Logger.WriteRelative(sReturnMessage, , 5)

                ThreadSafe__ThreadComplete("An error occurred while accessing the '" & Constants.UsernamePassword.SiteKiosk.Username & "' user account: " & sReturnMessage)

                Exit Sub
            End If
        End If


        '--------------------------------------------
        Helpers.Logger.WriteRelative("copying internal profile template", , 2)
        Helpers.Logger.WriteRelative("file", , 3)
        FIREFOX_PROFILE_TEMPLATE_FULLPATH = IO.Path.Combine(OVCC_TEMP_FOLDER, InstallerConstants.Firefox.Install.FIREFOX_PROFILE_TEMPLATE_FILENAME)
        Helpers.Logger.WriteRelative(FIREFOX_PROFILE_TEMPLATE_FULLPATH, , 4)

        Helpers.Logger.WriteRelative("copying from resources", , 3)
        Dim _bytes1() As Byte = My.Resources.OVCC_FIREFOX_PROFILE_TEMPLATE

        IO.File.WriteAllBytes(FIREFOX_PROFILE_TEMPLATE_FULLPATH, _bytes1)

        If Not IO.File.Exists(FIREFOX_PROFILE_TEMPLATE_FULLPATH) Then
            Helpers.Logger.WriteRelative("fail", , 4)

            ThreadSafe__ThreadComplete("fatal error: unable to copy profile template")

            Exit Sub
        End If

        Helpers.Logger.WriteRelative("ok", , 4)


        '--------------------------------------------
        Helpers.Logger.WriteRelative("copying internal LogoutHelper package", , 2)
        Helpers.Logger.WriteRelative("file", , 3)
        LOGOUTHELPER_INSTALLER_FULLPATH = IO.Path.Combine(OVCC_TEMP_FOLDER, InstallerConstants.Firefox.Install.LOGOUTHELPER_FILENAME)
        Helpers.Logger.WriteRelative(LOGOUTHELPER_INSTALLER_FULLPATH, , 4)

        Helpers.Logger.WriteRelative("copying from resources", , 3)
        Dim _bytes2() As Byte = My.Resources.LogoutCleanupHelper_deploy

        IO.File.WriteAllBytes(LOGOUTHELPER_INSTALLER_FULLPATH, _bytes2)

        If Not IO.File.Exists(LOGOUTHELPER_INSTALLER_FULLPATH) Then
            Helpers.Logger.WriteRelative("fail", , 4)

            ThreadSafe__ThreadComplete("fatal error: unable to copy profile template")

            Exit Sub
        End If

        Helpers.Logger.WriteRelative("ok", , 4)


        '--------------------------------------------
        Helpers.Logger.WriteRelative("is firefox already installed?", , 2)
        If Helpers.Generic.IsFirefoxInstalled Then
            Helpers.Logger.WriteRelative("yes", , 3)

            ThreadSafe__btnInitialise_Option(1, "Yes, reinstall", True, True)
            ThreadSafe__btnInitialise_Option(2, "No, use existing", True, True)
            ThreadSafe__btnInitialise_Option(3, "Cancel and exit", True, True)

            ThreadSafe__Initialise_Text("This app will close any currently open Firefox windows, uninstall the current Firefox, and reinstall the latest Firefox. Is this ok?")
        Else
            Helpers.Logger.WriteRelative("no", , 3)

            ThreadSafe__btnInitialise_Option(1, "Yes, install", True, True)
            ThreadSafe__btnInitialise_Option(2, " ", False, False)
            ThreadSafe__btnInitialise_Option(3, "Cancel and exit", True, True)

            ThreadSafe__Initialise_Text("This app will install the latest version of Firefox. Is this ok?")
        End If
        ThreadSafe__Initialise_Text_Position(7, 23)


        Helpers.Logger.WriteRelative("waiting for user input", , 2)

        mInitialise__waiting = True
        mInitialise__response = 0

        Do
            Thread.Sleep(500)
        Loop While mInitialise__waiting


        Select Case mInitialise__response
            Case 1
                Helpers.Logger.WriteRelative("yes, (re)install", , 2)

                Helpers.Generic.KillAllFirefox()

                ThreadSafe__ThreadComplete(PanelResults.OK)
            Case 2
                Helpers.Logger.WriteRelative("no, use existing", , 2)

                Helpers.Generic.KillAllFirefox()

                ThreadSafe__ThreadComplete(PanelResults.OK)
            Case 3
                Helpers.Logger.WriteRelative("cancelled by user, exiting", , 2)
                ThreadSafe__ThreadComplete("cancelled by user, nothing happened")
            Case Else
                Helpers.Logger.WriteRelative("unknown response (" & mInitialise__response & ")", , 2)
                ThreadSafe__ThreadComplete("internal error")
        End Select
    End Sub

    Private Sub btnInitialise_Option1_Click(sender As Object, e As EventArgs) Handles btnInitialise_Option1.Click
        ThreadSafe__btnInitialise_Option(0, "", False, False)

        FIREFOX_UNINSTALL_EXISTING_VERSION = True
        mInitialise__waiting = False
        mInitialise__response = 1
    End Sub

    Private Sub btnInitialise_Option2_Click(sender As Object, e As EventArgs) Handles btnInitialise_Option2.Click
        ThreadSafe__btnInitialise_Option(0, "", False, False)

        FIREFOX_UNINSTALL_EXISTING_VERSION = False
        mInitialise__waiting = False
        mInitialise__response = 2
    End Sub

    Private Sub btnInitialise_Option3_Click(sender As Object, e As EventArgs) Handles btnInitialise_Option3.Click
        ThreadSafe__btnInitialise_Option(0, "", False, False)

        mInitialise__waiting = False
        mInitialise__response = 3
    End Sub
#End Region


#Region "Uninstall firefox"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__UninstallFirefox()
        Helpers.Logger.WriteRelative("uninstalling firefox", , 1)


        If Not Helpers.Generic.IsFirefoxInstalled Or Not FIREFOX_UNINSTALL_EXISTING_VERSION Then
            Helpers.Logger.WriteRelative("skipped", , 2)

            If Not Helpers.Generic.IsFirefoxInstalled Then
                Helpers.Logger.WriteRelative("not installed", , 3)
            ElseIf Not FIREFOX_UNINSTALL_EXISTING_VERSION Then
                Helpers.Logger.WriteRelative("user chose to use existing version", , 3)
            Else
                Helpers.Logger.WriteRelative("huh, not sure how I got here?", , 3) '  ;-)
            End If


            ThreadSafe__ThreadComplete(PanelResults.SKIPPED)

            Exit Sub
        End If


        If Not FindFirefoxUninstaller(FIREFOX_UNINSTALLER_FULLPATH) Then
            ThreadSafe__ThreadComplete(Helpers.Errors.GetLast(False, False))

            Exit Sub
        End If



        Helpers.Logger.WriteRelative("starting uninstaller", , 2)
        Helpers.Logger.WriteRelative("command line", , 3)
        Helpers.Logger.WriteRelative(FIREFOX_UNINSTALLER_FULLPATH, , 4)
        Helpers.Logger.WriteRelative("arguments", , 3)
        Helpers.Logger.WriteRelative(InstallerConstants.Firefox.Install.FIREFOX_UNINSTALL_PARAMETERS, , 4)


        oProcess_UninstallFirefox = New ProcessRunner
        oProcess_UninstallFirefox.Arguments = InstallerConstants.Firefox.Install.FIREFOX_UNINSTALL_PARAMETERS
        oProcess_UninstallFirefox.ExternalAppToWaitFor = "un_a"
        oProcess_UninstallFirefox.FileName = FIREFOX_UNINSTALLER_FULLPATH
        oProcess_UninstallFirefox.MaxTimeout = InstallerConstants.TimeoutsDelaysCounters.FIREFOX_UNINSTALL_TIMEOUT_s
        oProcess_UninstallFirefox.ProcessStyle = ProcessWindowStyle.Normal
        oProcess_UninstallFirefox.TestMode = False

        oProcess_UninstallFirefox.StartProcess()
    End Sub

#Region "Process Events"
    Private Sub oProcess_UninstallFirefox_DEBUG_MESSAGE(DebugString As String) Handles oProcess_UninstallFirefox.DEBUG_MESSAGE
        Helpers.Logger.WriteRelative("oProcess_UninstallFirefox.DEBUG_MESSAGE", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
        Helpers.Logger.WriteRelative(DebugString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
    End Sub

    Private Sub oProcess_UninstallFirefox_Busy(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_UninstallFirefox.Busy

    End Sub

    Private Sub oProcess_UninstallFirefox_Test(ByVal EventProcess As System.Diagnostics.Process) Handles oProcess_UninstallFirefox.Test

    End Sub

    Private Sub oProcess_UninstallFirefox_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_UninstallFirefox.Done
        oProcess_UninstallFirefox.ProcessDone()

        ThreadSafe__Toggle_progressUninstallFirefox(False)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub

    Private Sub oProcess_UninstallFirefox_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess_UninstallFirefox.Failed
        Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Helpers.Logger.WriteRelative(ErrorMessage, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

        ThreadSafe__ThreadComplete("An error occurred while starting firefox uninstaller ('" & FIREFOX_UNINSTALLER_FULLPATH & "'): " & ErrorMessage)

        ThreadSafe__Toggle_progressUninstallFirefox(False)

        oProcess_UninstallFirefox.ProcessDone()
    End Sub

    Private Sub oProcess_UninstallFirefox_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_UninstallFirefox.Started
        Helpers.Logger.WriteRelative("ok", , 3)

        ThreadSafe__Toggle_progressUninstallFirefox(True)
    End Sub

    Private Sub oProcess_UninstallFirefox_TimeOut(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_UninstallFirefox.TimeOut
        Helpers.Logger.WriteRelative("timed out", , 3)

        oProcess_UninstallFirefox.ProcessDone()
        Helpers.Generic.KillAllFirefox()

        ThreadSafe__Toggle_progressUninstallFirefox(False)

        ThreadSafe__ThreadComplete("Firefox uninstaller timed out")
    End Sub

    Private Sub oProcess_UninstallFirefox_TimeOutFinal(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_UninstallFirefox.TimeOutFinal
        'not used, can only time out once
    End Sub
#End Region
#End Region


#Region "Download firefox"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private downloader_ElapsedTime As Date
    Private downloader_ElapsedTimeTotal As TimeSpan
    Private downloader_BytesReceivedTotal As Double
    Private downloader_BytesTotal As Double
    Private downloader_Busy As Boolean = False
    Private downloader_Timeout As Integer = 0

    Private downloader_MeanSpeedTotal As Double
    Private downloader_MeanSpeedCount As Double

    Private downloader_TimeLeft As String = ""
    Private downloader_DownloadSpeed As Double = 0.0

    Private Sub PanelAction__DownloadFirefox()
        Helpers.Logger.WriteRelative("downloading firefox", , 1)


        If Helpers.Generic.IsFirefoxInstalled And Not FIREFOX_UNINSTALL_EXISTING_VERSION Then
            Helpers.Logger.WriteRelative("skipped", , 2)
            Helpers.Logger.WriteRelative("user chose to use existing version", , 3)


            ThreadSafe__ThreadComplete(PanelResults.SKIPPED)

            Exit Sub
        End If


        ThreadSleep(2500)


        Try
            downloader_ElapsedTime = Date.Now
            downloader_MeanSpeedTotal = 0
            downloader_MeanSpeedCount = 0

            Dim client As WebClient = New WebClient

            AddHandler client.DownloadProgressChanged, AddressOf PanelAction__DownloadFirefox_ProgressChanged
            AddHandler client.DownloadFileCompleted, AddressOf PanelAction__DownloadFirefox_DownloadCompleted

            Helpers.Logger.WriteRelative("starting download", , 2)
            Helpers.Logger.WriteRelative("url", , 3)
            Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_URL, , 4)
            Helpers.Logger.WriteRelative("path", , 3)
            Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_FULLPATH, , 4)
            client.DownloadFileAsync(New Uri(FIREFOX_DOWNLOAD_URL), FIREFOX_DOWNLOAD_FULLPATH)
            Helpers.Logger.WriteRelative("started", , 3)

            downloader_Busy = True
            downloader_Timeout = 0
        Catch ex As Exception
            Helpers.Logger.WriteRelative("An error occurred while downloading Firefox: " & ex.Message, , 4)
            ThreadSafe__ThreadComplete("An error occurred while downloading Firefox: " & ex.Message)

            Exit Sub
        End Try


        Do
            ThreadSleep(2500)

            downloader_Timeout += 1
        Loop While downloader_Timeout < 30 And downloader_Busy

        If downloader_Busy Then
            'uh oh, download timed out
            Helpers.Logger.WriteRelative("download timed out", , 4)
            ThreadSafe__ThreadComplete("Firefox download timed out!")
        Else
            If IO.File.Exists(FIREFOX_DOWNLOAD_FULLPATH) Then
                Helpers.Logger.WriteRelative("ok", , 3)
                Helpers.Logger.WriteRelative("elapsed time", , 4)
                Helpers.Logger.WriteRelative(downloader_ElapsedTimeTotal.Seconds & " seconds", , 5)
                Helpers.Logger.WriteRelative("average speed", , 4)
                Helpers.Logger.WriteRelative(Helpers.FilesAndFolders.FileSize.GetHumanReadable(Math.Round(downloader_MeanSpeedTotal / downloader_MeanSpeedCount)) & "/s", , 5)

                ThreadSafe__ThreadComplete(PanelResults.OK)
            Else
                Helpers.Logger.WriteRelative("failed", , 3)
                ThreadSafe__ThreadComplete("Could not find downloaded Firefox installer '" & FIREFOX_DOWNLOAD_FULLPATH & "'")
            End If
        End If
    End Sub

    Private Sub PanelAction__DownloadFirefox_ProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
        downloader_Timeout = 0

        downloader_BytesReceivedTotal = Double.Parse(e.BytesReceived.ToString())
        downloader_BytesTotal = Double.Parse(e.TotalBytesToReceive.ToString())

        Dim _ts As TimeSpan = Date.Now.Subtract(downloader_ElapsedTime)
        Dim speed As Double = (downloader_BytesReceivedTotal / _ts.Seconds) 'bytes/s
        Dim timeleft As Double = (downloader_BytesTotal - downloader_BytesReceivedTotal) / speed ' s left
        Dim sSpeed As String = ""



        Try
            If Double.IsInfinity(speed) Then
                sSpeed = "Download speed: ?B/s"
            Else
                downloader_DownloadSpeed = speed
                sSpeed = "Download speed: " & Helpers.FilesAndFolders.FileSize.GetHumanReadable(Math.Round(speed)) & "/s"

                timeleft = (downloader_BytesTotal - downloader_BytesReceivedTotal) / speed
                downloader_TimeLeft = "Time remaining: " & Math.Round(timeleft) & "s"

                downloader_MeanSpeedTotal = downloader_MeanSpeedTotal + speed
                downloader_MeanSpeedCount = downloader_MeanSpeedCount + 1.0
            End If


        Catch ex As Exception
            sSpeed = "Download speed: " & Helpers.FilesAndFolders.FileSize.GetHumanReadable(0) & "/s"
        End Try


        ThreadSafe__DownloadProgress(e.ProgressPercentage, sSpeed, downloader_TimeLeft)
    End Sub

    Private Sub PanelAction__DownloadFirefox_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        downloader_ElapsedTimeTotal = Date.Now.Subtract(downloader_ElapsedTime)
        downloader_Busy = False
        downloader_Timeout = 0

        ThreadSafe__DownloadProgress(100, "", "")
    End Sub
#End Region


#Region "Install firefox"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__InstallFirefox()
        Helpers.Logger.WriteRelative("installing firefox", , 1)


        If Helpers.Generic.IsFirefoxInstalled Then
            Helpers.Logger.WriteRelative("skipped, already installed", , 2)
            ThreadSafe__ThreadComplete(PanelResults.SKIPPED)

            Exit Sub
        End If


        If Not IO.File.Exists(FIREFOX_DOWNLOAD_FULLPATH) Then
            ThreadSafe__ThreadComplete("Installer '" & FIREFOX_DOWNLOAD_FULLPATH & "' not found!")

            Exit Sub
        End If



        Helpers.Logger.WriteRelative("starting installer", , 2)
        Helpers.Logger.WriteRelative("command line", , 3)
        Helpers.Logger.WriteRelative(FIREFOX_DOWNLOAD_FULLPATH, , 4)
        Helpers.Logger.WriteRelative("arguments", , 3)
        Helpers.Logger.WriteRelative(InstallerConstants.Firefox.Install.FIREFOX_INSTALL_PARAMETERS, , 4)


        oProcess_InstallFirefox = New ProcessRunner
        oProcess_InstallFirefox.Arguments = InstallerConstants.Firefox.Install.FIREFOX_INSTALL_PARAMETERS
        oProcess_InstallFirefox.FileName = FIREFOX_DOWNLOAD_FULLPATH
        oProcess_InstallFirefox.MaxTimeout = InstallerConstants.TimeoutsDelaysCounters.FIREFOX_INSTALL_TIMEOUT_s
        oProcess_InstallFirefox.ProcessStyle = ProcessWindowStyle.Normal
        oProcess_InstallFirefox.TestMode = False

        oProcess_InstallFirefox.StartProcess()
    End Sub

#Region "Process Events"
    Private Sub oProcess_InstallFirefox_DEBUG_MESSAGE(DebugString As String) Handles oProcess_InstallFirefox.DEBUG_MESSAGE
        Helpers.Logger.WriteRelative("oProcess_UninstallFirefox.DEBUG_MESSAGE", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
        Helpers.Logger.WriteRelative(DebugString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
    End Sub

    Private Sub oProcess_InstallFirefox_Busy(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InstallFirefox.Busy

    End Sub

    Private Sub oProcess_InstallFirefox_Test(ByVal EventProcess As System.Diagnostics.Process) Handles oProcess_InstallFirefox.Test

    End Sub

    Private Sub oProcess_InstallFirefox_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InstallFirefox.Done
        Helpers.Logger.WriteRelative("done", , 2)

        oProcess_InstallFirefox.ProcessDone()

        ThreadSafe__Toggle_progressInstallFirefox(False)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub

    Private Sub oProcess_InstallFirefox_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess_InstallFirefox.Failed
        Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Helpers.Logger.WriteRelative(ErrorMessage, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

        ThreadSafe__ThreadComplete("An error occurred while starting firefox installer ('" & FIREFOX_DOWNLOAD_FULLPATH & "'): " & ErrorMessage)

        ThreadSafe__Toggle_progressInstallFirefox(False)

        oProcess_InstallFirefox.ProcessDone()
    End Sub

    Private Sub oProcess_InstallFirefox_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InstallFirefox.Started
        Helpers.Logger.WriteRelative("ok", , 3)

        ThreadSafe__Toggle_progressInstallFirefox(True)
    End Sub

    Private Sub oProcess_InstallFirefox_TimeOut(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_InstallFirefox.TimeOut
        Helpers.Logger.WriteRelative("timed out", , 3)

        oProcess_InstallFirefox.ProcessDone()
        Helpers.Generic.KillAllFirefox()

        ThreadSafe__Toggle_progressInstallFirefox(False)

        ThreadSafe__ThreadComplete("Firefox installer timed out")
    End Sub

    Private Sub oProcess_InstallFirefox_TimeOutFinal(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_InstallFirefox.TimeOutFinal
        'not used, can only time out once
    End Sub
#End Region
#End Region


#Region "Initialise firefox"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private mInitialiseFirefox__timeout As Integer = 0
    Private mInitialiseFirefox__WindowWaitTimeout As Integer = 0
    Private Sub PanelAction__InitialiseFirefox()
        Helpers.Logger.WriteRelative("initialising firefox", , 1)

        ThreadSafe__InitialiseFirefox_Controls_toggle(
            "One moment, starting Firefox as user '" & Constants.UsernamePassword.SiteKiosk.Username & "'" & vbCrLf & vbCrLf & "!!! Please do not interact with Firefox at this time!!!",
            False)
        ThreadSafe__InitialiseFirefox_Timeout("")

        Helpers.Logger.WriteRelative("finding firefox", , 2)
        'Find the firefox executable
        If Not FindFirefoxExecutable(FIREFOX_INSTALLED_FULLPATH) Then
            Helpers.Logger.WriteRelative("could not find firefox.exe", , 3)
            Helpers.Logger.WriteRelative(Helpers.Errors.GetLast(False, False), , 4)

            ThreadSafe__ThreadComplete("Could not find firefox.exe")
            Exit Sub
        End If


        Helpers.Logger.WriteRelative(FIREFOX_INSTALLED_FULLPATH, , 3)


        ThreadSafe__FormOpacity(0.9)

        mInitialiseFirefox__timeout = 0
        mInitialiseFirefox__WindowWaitTimeout = 0


        Dim tmpPW As String

        If Not SITEKIOSK_USER_OLD_PASSWORD Then
            tmpPW = Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password)
        Else
            tmpPW = Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password_old)
        End If


        Helpers.Logger.WriteRelative("starting firefox", , 2)
        Helpers.Logger.WriteRelative("command line", , 3)
        Helpers.Logger.WriteRelative(FIREFOX_INSTALLED_FULLPATH, , 4)
        Helpers.Logger.WriteRelative("username", , 3)
        Helpers.Logger.WriteRelative(Constants.UsernamePassword.SiteKiosk.Username, , 4)
        Helpers.Logger.WriteRelative("password", , 3)
        Helpers.Logger.WriteRelative(New String("*", 10), , 4)
        Helpers.Logger.WriteRelative("arguments", , 3)
        Helpers.Logger.WriteRelative(InstallerConstants.Firefox.Install.FIREFOX_RUN_PARAMETERS, , 4)
        Helpers.Logger.WriteRelative("working directory", , 3)
        Helpers.Logger.WriteRelative(IO.Path.GetDirectoryName(FIREFOX_INSTALLED_FULLPATH), , 4)


        oProcess_InitialiseFirefox = New ProcessRunner
        oProcess_InitialiseFirefox.Arguments = InstallerConstants.Firefox.Install.FIREFOX_RUN_PARAMETERS
        oProcess_InitialiseFirefox.ExternalAppToWaitFor = ""
        oProcess_InitialiseFirefox.FileName = FIREFOX_INSTALLED_FULLPATH
        oProcess_InitialiseFirefox.LoadUserProfile = True
        oProcess_InitialiseFirefox.MaxTimeout = InstallerConstants.TimeoutsDelaysCounters.FIREFOX_INITIALISATION_TIMEOUT_s
        oProcess_InitialiseFirefox.PassWord = tmpPW
        oProcess_InitialiseFirefox.ProcessStyle = ProcessWindowStyle.Normal
        oProcess_InitialiseFirefox.TestMode = False
        oProcess_InitialiseFirefox.UserName = Constants.UsernamePassword.SiteKiosk.Username
        oProcess_InitialiseFirefox.UseShellExecute = False
        oProcess_InitialiseFirefox.WorkingDirectory = IO.Path.GetDirectoryName(FIREFOX_INSTALLED_FULLPATH)

        oProcess_InitialiseFirefox.StartProcess()
    End Sub

#Region "timer events"
    Private Sub tmrTimeout__InitialiseFirefox_Tick(sender As Object, e As EventArgs) Handles tmrTimeout__InitialiseFirefox.Tick
        ThreadSafe__InitialiseFirefox_Timeout("times out in " & (InstallerConstants.TimeoutsDelaysCounters.FIREFOX_INITIALISATION_TIMEOUT_s - mInitialiseFirefox__timeout).ToString & "s")

        mInitialiseFirefox__timeout += 1
        If mInitialiseFirefox__timeout > InstallerConstants.TimeoutsDelaysCounters.FIREFOX_INITIALISATION_TIMEOUT_s Then
            ThreadSafe__Toggle_tmrTimeout__InitialiseFirefox(False)
        End If
    End Sub

    Private Sub tmrWindowVisible__InitialiseFirefox_Tick(sender As Object, e As EventArgs) Handles tmrWindowVisible__InitialiseFirefox.Tick
        mInitialiseFirefox__WindowWaitTimeout += 1
        If mInitialiseFirefox__WindowWaitTimeout > InstallerConstants.TimeoutsDelaysCounters.FIREFOX_INITIALISATION_WINDOW_WAIT_TIMEOUT_x Or WindowVisible.FindWindowByClass(InstallerConstants.Sitekiosk.SITEKIOSK_FIREFOX_WINDOWCLASS_NAME) Then
            ThreadSafe__Toggle_tmrWindowVisible__InitialiseFirefox(False)

            Helpers.Logger.WriteRelative("waiting for user input", , 2)

            ThreadSafe__InitialiseFirefox_Controls_toggle(
                "Do you see an instance of Firefox running behind this app? If it's not yet visible, please wait a few moments more, it may take a little time to start." & vbCrLf & vbCrLf & "!!! Please do not interact with Firefox at this time!!!",
                True)

            ThreadSafe__Toggle_tmrTimeout__InitialiseFirefox(True)
        End If
    End Sub
#End Region

#Region "Process Events"
    Private Sub oProcess_InitialiseFirefox_DEBUG_MESSAGE(DebugString As String) Handles oProcess_InitialiseFirefox.DEBUG_MESSAGE
        Helpers.Logger.WriteRelative("oProcess_InitialiseFirefox.DEBUG_MESSAGE", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
        Helpers.Logger.WriteRelative(DebugString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
    End Sub

    Private Sub oProcess_InitialiseFirefox_Busy(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InitialiseFirefox.Busy

    End Sub

    Private Sub oProcess_InitialiseFirefox_Test(ByVal EventProcess As System.Diagnostics.Process) Handles oProcess_InitialiseFirefox.Test

    End Sub

    Private Sub oProcess_InitialiseFirefox_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InitialiseFirefox.Done
        oProcess_InitialiseFirefox.ProcessDone()
    End Sub

    Private Sub oProcess_InitialiseFirefox_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess_InitialiseFirefox.Failed
        Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Helpers.Logger.WriteRelative(ErrorMessage, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

        ThreadSafe__ThreadComplete("An error occurred while starting firefox ('" & FIREFOX_INSTALLED_FULLPATH & "'): " & ErrorMessage)

        oProcess_InitialiseFirefox.ProcessDone()
    End Sub

    Private Sub oProcess_InitialiseFirefox_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InitialiseFirefox.Started
        Helpers.Logger.WriteRelative("ok", , 3)

        Helpers.Logger.WriteRelative("starting delay", , 2)

        ThreadSafe__Toggle_tmrWindowVisible__InitialiseFirefox(True)

        ThreadSafe__InitialiseFirefox_Controls_toggle(
            "Starting Firefox..." & vbCrLf & vbCrLf & "!!! Please do not interact with Firefox at this time!!!",
            False)
    End Sub

    Private Sub oProcess_InitialiseFirefox_TimeOut(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_InitialiseFirefox.TimeOut
        Helpers.Logger.WriteRelative("timed out", , 3)

        oProcess_InitialiseFirefox.ProcessDone()
        Helpers.Generic.KillAllFirefox()

        ThreadSafe__Toggle_tmrTimeout__InitialiseFirefox(False)

        ThreadSafe__InitialiseFirefox_Controls_toggle(
            "You've waited too long!",
            False)
        ThreadSafe__InitialiseFirefox_Timeout("")

        ThreadSafe__FormOpacity(1.0)


        ThreadSafe__ThreadComplete("User waited too long ")
    End Sub

    Private Sub oProcess_InitialiseFirefox_TimeOutFinal(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_InitialiseFirefox.TimeOutFinal
        'not used, can only time out once
    End Sub
#End Region

    Private Sub PanelAction__InitialiseFirefox_Yes_Click(sender As Object, e As EventArgs) Handles btnInitialiseFirefox_Yes.Click
        Helpers.Logger.WriteRelative("user saw firefox", , 3)

        ThreadSafe__Toggle_tmrTimeout__InitialiseFirefox(False)

        ThreadSafe__InitialiseFirefox_Controls_toggle(
            "Thank you!",
            False)
        ThreadSafe__InitialiseFirefox_Timeout("")

        ThreadSafe__FormOpacity(1.0)


 
        oProcess_InitialiseFirefox.ProcessDone()
        Helpers.Generic.KillAllFirefox()

        Thread.Sleep(2000)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub

    Private Sub PanelAction__InitialiseFirefox_No_Click(sender As Object, e As EventArgs) Handles btnInitialiseFirefox_No.Click
        Helpers.Logger.WriteRelative("user did not see firefox", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)

        ThreadSafe__Toggle_tmrTimeout__InitialiseFirefox(False)

        ThreadSafe__InitialiseFirefox_Controls_toggle(
            "I'm sorry!",
            False)
        ThreadSafe__InitialiseFirefox_Timeout("")

        ThreadSafe__FormOpacity(1.0)

        oProcess_InitialiseFirefox.ProcessDone()
        Helpers.Generic.KillAllFirefox()

        Thread.Sleep(1000)

        ThreadSafe__ThreadComplete("Firefox failed to start. Please restart and try again.")
    End Sub
#End Region


#Region "Install firefox profile"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__InstallProfile()
        ThreadSafe__Toggle_progressInstallProfile(True)
        ThreadSafe__InstallProfile_Text("installing firefox profile template")

        Helpers.Logger.WriteRelative("installing firefox profile template", , 1)

        Dim sProfile_Path As String = ""

        Try
            Cleaners.FireFox.Properties.Username = Constants.UsernamePassword.SiteKiosk.Username

            sProfile_Path = IO.Path.Combine(Cleaners.FireFox.Properties.ProfileRootPath, InstallerConstants.Firefox.Install.FIREFOX_PROFILE_PATH).Replace("/", "\")

            Helpers.Logger.WriteRelative("extracting", , 2)
            Helpers.Logger.WriteRelative("destination folder", , 3)
            Helpers.Logger.WriteRelative(sProfile_Path, , 4)
            If IO.Directory.Exists(sProfile_Path) Then
                Helpers.Logger.WriteRelative("already exists, cleaning", , 5)
                Helpers.FilesAndFolders.Folder.CleanFolder(sProfile_Path, "I_am_sure")
            End If
            ZipFile.ExtractToDirectory(FIREFOX_PROFILE_TEMPLATE_FULLPATH, sProfile_Path)


            Helpers.Logger.WriteRelative("backup existing profiles.ini", , 3)
            Helpers.FilesAndFolders.Backup.File(Cleaners.FireFox.Properties.ProfilesIni, 3)

            Helpers.Logger.WriteRelative("updating profiles.ini", , 3)
            Dim _inifile As New Helpers.FilesAndFolders.IniFile(Cleaners.FireFox.Properties.ProfilesIni)

            Helpers.Logger.WriteRelative("deleting all current profiles", , 4)
            Dim _tmp2delete As New List(Of String)
            For Each _s As Helpers.FilesAndFolders.IniFile.Section In _inifile.Sections
                If _s.Name.StartsWith("Profile") Then
                    Helpers.Logger.WriteRelative(_s.Name, , 5)
                    _tmp2delete.Add(_s.Name)
                End If
            Next
            For Each _s As String In _tmp2delete
                _inifile.DeleteSection(_s)
            Next


            Helpers.Logger.WriteRelative("adding profile", , 4)

            _inifile.AddSection("Profile0", "", 1)

            Dim ___k As New Helpers.FilesAndFolders.IniFile.Key

            ___k = New Helpers.FilesAndFolders.IniFile.Key
            ___k.Name = "Name"
            ___k.Value = InstallerConstants.Firefox.Install.FIREFOX_PROFILE_NAME
            ___k.Comments = ""
            _inifile.AddKey("Profile0", ___k)

            ___k = New Helpers.FilesAndFolders.IniFile.Key
            ___k.Name = "IsRelative"
            ___k.Value = "1"
            ___k.Comments = ""
            _inifile.AddKey("Profile0", ___k)

            ___k = New Helpers.FilesAndFolders.IniFile.Key
            ___k.Name = "Path"
            ___k.Value = InstallerConstants.Firefox.Install.FIREFOX_PROFILE_PATH
            ___k.Comments = ""
            _inifile.AddKey("Profile0", ___k)

            ___k = New Helpers.FilesAndFolders.IniFile.Key
            ___k.Name = "Default"
            ___k.Value = "1"
            ___k.Comments = ""
            _inifile.AddKey("Profile0", ___k)

            Helpers.Logger.WriteMessageRelative("ok", 5)


            Helpers.Logger.WriteRelative("set default profile", , 4)
            For Each _s As Helpers.FilesAndFolders.IniFile.Section In _inifile.Sections
                If _s.Name.StartsWith("Install") Then
                    For Each _k As Helpers.FilesAndFolders.IniFile.Key In _s.Keys
                        If _k.Name = "Default" Then
                            Helpers.Logger.WriteMessageRelative("current value", 5)
                            Helpers.Logger.WriteMessageRelative(_k.Value, 6)

                            Helpers.Logger.WriteMessageRelative("removing old default", 5)

                            _s.Keys.Remove(_k)

                            Helpers.Logger.WriteMessageRelative("ok", 6)


                            Dim __k As New Helpers.FilesAndFolders.IniFile.Key

                            __k.Name = "Default"
                            __k.Value = InstallerConstants.Firefox.Install.FIREFOX_PROFILE_PATH
                            __k.Comments = ""

                            Helpers.Logger.WriteMessageRelative("new value", 5)
                            Helpers.Logger.WriteMessageRelative(__k.Value, 6)

                            Helpers.Logger.WriteMessageRelative("adding new default", 5)

                            _s.Keys.Add(__k)

                            Helpers.Logger.WriteMessageRelative("ok", 6)

                            Exit For
                        End If
                    Next
                End If
            Next

            Helpers.Logger.WriteRelative("saving", , 4)
            _inifile.SaveFile(False, True)
            Helpers.Logger.WriteRelative("ok", , 5)

        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("error", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            ThreadSafe__ThreadComplete(ex.Message)
            Exit Sub
        End Try


        ThreadSleep(2500)
        ThreadSafe__Toggle_progressInstallProfile(False)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub
#End Region


#Region "Install LogoutHelper"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__InstallLogoutHelper()
        Helpers.Logger.WriteRelative("installing LogoutHelper", , 1)

        Helpers.Logger.WriteRelative("starting installer", , 2)
        Helpers.Logger.WriteRelative("command line", , 3)
        Helpers.Logger.WriteRelative(LOGOUTHELPER_INSTALLER_FULLPATH, , 4)


        oProcess_InstallLogoutHelper = New ProcessRunner
        oProcess_InstallLogoutHelper.Arguments = ""
        oProcess_InstallLogoutHelper.FileName = LOGOUTHELPER_INSTALLER_FULLPATH
        oProcess_InstallLogoutHelper.MaxTimeout = InstallerConstants.TimeoutsDelaysCounters.LOGOUTHELPER_INSTALL_TIMEOUT_s
        oProcess_InstallLogoutHelper.ProcessStyle = ProcessWindowStyle.Normal
        oProcess_InstallLogoutHelper.TestMode = False
        oProcess_InstallLogoutHelper.ExternalAppToWaitFor = "LogoutCleanupHelperInstaller"

        oProcess_InstallLogoutHelper.StartProcess()
    End Sub

#Region "Process Events"
    Private Sub oProcess_InstallLogoutHelper_DEBUG_MESSAGE(DebugString As String) Handles oProcess_InstallLogoutHelper.DEBUG_MESSAGE
        Helpers.Logger.WriteRelative("oProcess_InstallLogoutHelper.DEBUG_MESSAGE", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
        Helpers.Logger.WriteRelative(DebugString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
    End Sub

    Private Sub oProcess_InstallLogoutHelper_Busy(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InstallLogoutHelper.Busy

    End Sub

    Private Sub oProcess_InstallLogoutHelper_Test(ByVal EventProcess As System.Diagnostics.Process) Handles oProcess_InstallLogoutHelper.Test

    End Sub

    Private Sub oProcess_InstallLogoutHelper_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InstallLogoutHelper.Done
        Helpers.Logger.WriteRelative("done", , 2)

        oProcess_InstallLogoutHelper.ProcessDone()

        ThreadSafe__Toggle_progressInstallLogoutHelper(False)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub

    Private Sub oProcess_InstallLogoutHelper_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess_InstallLogoutHelper.Failed
        Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Helpers.Logger.WriteRelative(ErrorMessage, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

        ThreadSafe__ThreadComplete("An error occurred while starting firefox installer ('" & FIREFOX_DOWNLOAD_FULLPATH & "'): " & ErrorMessage)

        ThreadSafe__Toggle_progressInstallLogoutHelper(False)

        oProcess_InstallFirefox.ProcessDone()
    End Sub

    Private Sub oProcess_InstallLogoutHelper_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_InstallLogoutHelper.Started
        Helpers.Logger.WriteRelative("ok", , 3)
        Helpers.Logger.WriteRelative("for more info see LogoutHelperInstaller logs", , 4)

        ThreadSafe__Toggle_progressInstallLogoutHelper(True)
    End Sub

    Private Sub oProcess_InstallLogoutHelper_TimeOut(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_InstallLogoutHelper.TimeOut
        Helpers.Logger.WriteRelative("timed out", , 3)

        oProcess_InstallLogoutHelper.ProcessDone()

        ThreadSafe__Toggle_progressInstallLogoutHelper(False)

        ThreadSafe__ThreadComplete("LogoutHelper installer timed out")
    End Sub

    Private Sub oProcess_InstallLogoutHelper_TimeOutFinal(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_InstallLogoutHelper.TimeOutFinal
        'not used, can only time out once
    End Sub
#End Region
#End Region


#Region "Update configuration"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__UpdateConfiguration()
        Helpers.Logger.WriteRelative("updating configuration", , 1)

        ThreadSafe__Toggle_progressUpdateConfiguration(True)

        ThreadSleep(1000)

        Try
            Helpers.Logger.WriteRelative("loading SK config file", , 2)

            Dim sSkCfgFile As String = GetSiteKioskConfiguration()
            Dim bConfigChanged As Boolean = False

            Helpers.Logger.WriteRelative(sSkCfgFile, , 3)


            Dim nt As System.Xml.XmlNameTable
            Dim ns As System.Xml.XmlNamespaceManager
            Dim xmlSkCfg As System.Xml.XmlDocument
            Dim xmlNode_Root As System.Xml.XmlNode

            xmlSkCfg = New System.Xml.XmlDocument

            xmlSkCfg.Load(sSkCfgFile)
            Helpers.Logger.WriteRelative("ok", , 4)

            nt = xmlSkCfg.NameTable
            ns = New System.Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

#Region "sk programs template"
            Dim mXmlNode_Template_Program__File As Xml.XmlNode
            Dim mXmlNode_Template_Program__Runas As Xml.XmlNode
            Dim mXmlNode_Template_Program__Startup As Xml.XmlNode
            Dim mXmlNode_Template_Program__tmp As Xml.XmlNode

            mXmlNode_Template_Program__File = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "file", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__Runas = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "runas", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__Startup = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "startup", ns.LookupNamespace("sk"))


            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Startup.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__Startup.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("enabled"))
            mXmlNode_Template_Program__Startup.Attributes.GetNamedItem("enabled").Value = "false"

            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Startup)


            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "username", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "domainname", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__Runas.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("enabled"))
            mXmlNode_Template_Program__Runas.Attributes.GetNamedItem("enabled").Value = "false"

            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Runas)


            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("autostart"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("autostart").Value = "false"

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("filename"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("filename").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("description"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("description").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("title"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("title").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("screenshot-path"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("screenshot-path").Value = ""
#End Region

            Dim mXmlNode_Programs As Xml.XmlNode
            Dim mXmlNode_Files As Xml.XmlNodeList
            Dim mXmlNode_File As Xml.XmlNode
            Dim mXmlNode_FileClone As Xml.XmlNode
            Dim sTmpAppName As String, bFirefoxAlreadyThere As Boolean = False
            Dim sTmpAppTitle As String, sTmpAppDescription As String

            mXmlNode_Programs = xmlNode_Root.SelectSingleNode("sk:programs", ns)
            mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)

            Helpers.Logger.WriteMessageRelative("checking app list for " & InstallerConstants.Firefox.Install.FIREFOX_NAME, 2)
            For Each mXmlNode_File In mXmlNode_Files
                sTmpAppName = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower

                If sTmpAppName = FIREFOX_INSTALLED_FULLPATH.ToLower Then
                    'already there, let's make sure that the title and description are ok
                    Helpers.Logger.WriteMessageRelative(InstallerConstants.Firefox.Install.FIREFOX_NAME & " already in list", 3)

                    Try
                        sTmpAppTitle = mXmlNode_File.Attributes.GetNamedItem("title").Value

                        If Not sTmpAppTitle.Equals(InstallerConstants.Firefox.Install.FIREFOX_NAME) Then
                            Helpers.Logger.WriteMessageRelative("app title is not correct", 4)
                            Helpers.Logger.WriteMessageRelative("old title", 5)
                            Helpers.Logger.WriteMessageRelative(sTmpAppTitle, 6)
                            Helpers.Logger.WriteMessageRelative("new title", 5)
                            Helpers.Logger.WriteMessageRelative(InstallerConstants.Firefox.Install.FIREFOX_NAME, 6)

                            mXmlNode_File.Attributes.GetNamedItem("title").Value = InstallerConstants.Firefox.Install.FIREFOX_NAME

                            bConfigChanged = True
                        End If
                    Catch ex As Exception
                        Helpers.Logger.WriteErrorRelative("app title is missing, SK7?", 4)
                    End Try


                    sTmpAppDescription = mXmlNode_File.Attributes.GetNamedItem("description").Value

                    If Not sTmpAppDescription.ToLower.Contains(InstallerConstants.Firefox.Install.FIREFOX_NAME_SHORT.ToLower) Then
                        Helpers.Logger.WriteMessageRelative("app description is not correct", 4)
                        Helpers.Logger.WriteMessageRelative("old description", 5)
                        Helpers.Logger.WriteMessageRelative(sTmpAppDescription, 6)
                        Helpers.Logger.WriteMessageRelative("new description", 5)
                        Helpers.Logger.WriteMessageRelative(InstallerConstants.Firefox.Install.FIREFOX_NAME, 6)

                        mXmlNode_File.Attributes.GetNamedItem("description").Value = InstallerConstants.Firefox.Install.FIREFOX_NAME

                        bConfigChanged = True
                    End If


                    bFirefoxAlreadyThere = True

                    Exit For
                End If
            Next


            If Not bFirefoxAlreadyThere Then
                Helpers.Logger.WriteMessageRelative("not there", 3)

                Helpers.Logger.WriteMessageRelative("adding " & InstallerConstants.Firefox.Install.FIREFOX_NAME & " to app list", 2)

                mXmlNode_FileClone = mXmlNode_Template_Program__File.CloneNode(True)
                mXmlNode_FileClone.Attributes.GetNamedItem("filename").Value = FIREFOX_INSTALLED_FULLPATH
                Try
                    mXmlNode_FileClone.Attributes.GetNamedItem("title").Value = InstallerConstants.Firefox.Install.FIREFOX_NAME
                Catch ex As Exception
                    Helpers.Logger.WriteMessageRelative("skipping app title (SK7?)", 3)
                End Try
                mXmlNode_FileClone.Attributes.GetNamedItem("description").Value = InstallerConstants.Firefox.Install.FIREFOX_NAME

                mXmlNode_Programs.AppendChild(mXmlNode_FileClone)
                'mXmlNode_Programs.PrependChild(mXmlNode_FileClone)

                Helpers.Logger.WriteMessageRelative("ok", 3)

                bConfigChanged = True
            End If


            '---------------------------------------------------
            Dim mXmlNode_WindowManager As Xml.XmlNode
            Dim mXmlNode_Windows As Xml.XmlNodeList
            Dim mXmlNode_Window As Xml.XmlNode
            Dim sTmpClass As String, sTmpAction As String

            mXmlNode_WindowManager = xmlNode_Root.SelectSingleNode("sk:windowmanager", ns)
            mXmlNode_Windows = mXmlNode_WindowManager.SelectNodes("sk:window", ns)

            Helpers.Logger.WriteMessageRelative("checking window list for " & InstallerConstants.Sitekiosk.SITEKIOSK_FIREFOX_WINDOWCLASS_NAME, 2)

            For Each mXmlNode_Window In mXmlNode_Windows
                sTmpClass = mXmlNode_Window.SelectSingleNode("sk:class", ns).InnerText

                If sTmpClass = InstallerConstants.Sitekiosk.SITEKIOSK_FIREFOX_WINDOWCLASS_NAME Then
                    Helpers.Logger.WriteMessageRelative("ok", 3)

                    sTmpAction = mXmlNode_Window.SelectSingleNode("sk:action", ns).InnerText
                    Helpers.Logger.WriteMessageRelative("action is '" & sTmpAction & "'", 3)

                    If sTmpAction = "2" Then
                        Helpers.Logger.WriteMessageRelative("ok", 4)
                    Else
                        Helpers.Logger.WriteMessageRelative("setting action to '2'", 4)
                        mXmlNode_Window.SelectSingleNode("sk:action", ns).InnerText = "2"
                        Helpers.Logger.WriteMessageRelative("ok", 5)

                        bConfigChanged = True
                    End If
                End If
            Next


            '---------------------------------------------------
            Helpers.Logger.WriteMessageRelative("saving", 2)
            If bConfigChanged Then
                Helpers.Logger.WriteRelative("backup", , 3)
                Helpers.FilesAndFolders.Backup.File(sSkCfgFile, 3, My.Application.Info.ProductName)

                Helpers.Logger.WriteMessageRelative("writing", 3)
                Helpers.Logger.WriteMessageRelative(sSkCfgFile, 4)
                xmlSkCfg.Save(sSkCfgFile)
                Helpers.Logger.WriteMessageRelative("ok", 5)
            Else
                Helpers.Logger.WriteWarningRelative("skipped, nothing was changed", 3)
            End If


            '---------------------------------------------------
            Helpers.Logger.WriteRelative("loading script file", , 2)
            Dim sApplicationsScript As String = IO.Path.Combine(GetSiteKioskDirectory, "Skins\Public\iBAHN\Scripts\internal\Applications.js")
            Dim lstApps As New List(Of String)
            Dim linesApplications As New List(Of String)
            Dim bApplicationsScriptChanged As Boolean = False

            Helpers.Logger.WriteRelative(sApplicationsScript, , 3)
            Helpers.Logger.WriteRelative("looking for firefox", , 3)

            Using sr As New IO.StreamReader(sApplicationsScript)
                Dim line As String, bDone As Boolean = False

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.StartsWith("var _Applications__array=") And Not bDone Then
                        lstApps = line.Replace("var _Applications__array=", "").Replace("[", "").Replace("]", "").Replace(";", "").Split(",").ToList
                        If lstApps.Contains("""firefox""") Then
                            'ok
                            Helpers.Logger.WriteRelative("already there", , 4)
                        Else
                            Helpers.Logger.WriteRelative("adding", , 4)
                            lstApps.Add("""firefox""")

                            line = "var _Applications__array=[" & String.Join(",", lstApps.ToArray) & "];"

                            bApplicationsScriptChanged = True
                        End If

                        bDone = True
                    End If

                    linesApplications.Add(line)
                End While
            End Using

            If bApplicationsScriptChanged Then
                Helpers.Logger.WriteRelative("saving script file", , 2)
                Helpers.Logger.WriteRelative("backup", , 3)
                Helpers.FilesAndFolders.Backup.File(sApplicationsScript, 3, My.Application.Info.ProductName)

                Helpers.Logger.WriteRelative("writing", , 3)
                Helpers.Logger.WriteRelative(sApplicationsScript, , 4)
                Using sw As New IO.StreamWriter(sApplicationsScript)
                    For Each line As String In linesApplications
                        sw.WriteLine(line)
                    Next
                End Using
                Helpers.Logger.WriteRelative("ok", , 5)
            End If


            '---------------------------------------------------
            Helpers.Logger.WriteRelative("check all buttons scripts", , 2)
            Try
                Dim sThemeFolders As String() = IO.Directory.GetDirectories(IO.Path.Combine(GetSiteKioskDirectory, "Skins\Public\Startpages"))

                For Each sThemeFolder As String In sThemeFolders
                    Dim sScriptPath As String = "", sScriptContents As String = ""

                    Helpers.Logger.WriteRelative("entering '" & sThemeFolder & "'", , 3)

                    If sThemeFolder.EndsWith("\SiteCafe") Then
                        Helpers.Logger.WriteRelative("skipping", , 4)

                        Continue For
                    ElseIf IO.File.Exists(IO.Path.Combine(sThemeFolder, "scripts\buttons.js")) Then
                        sScriptContents = My.Resources.GUESTTEK__buttons_js
                        sScriptPath = IO.Path.Combine(sThemeFolder, "scripts\buttons.js")

                        Helpers.Logger.WriteRelative("normal theme", , 4)
                    ElseIf IO.File.Exists(IO.Path.Combine(sThemeFolder, "js\buttons.js")) And sThemeFolder.EndsWith("\Hilton") Then
                        sScriptContents = My.Resources.HILTON__buttons_js
                        sScriptPath = IO.Path.Combine(sThemeFolder, "js\buttons.js")

                        Helpers.Logger.WriteRelative("Hilton theme", , 4)
                    Else
                        Helpers.Logger.WriteRelative("no script/js folder found, skipping", , 4)

                        Continue For
                    End If

                    Helpers.Logger.WriteRelative("updating buttons.js", , 5)
                    Dim bNeedFix As Boolean = True
                    Using sr As New IO.StreamReader(sScriptPath)
                        Dim line As String

                        While Not sr.EndOfStream
                            line = sr.ReadLine

                            If line.Contains("var bDoYouReallyWantTheFirefoxButton=true;") Then
                                bNeedFix = False

                                Exit While
                            End If
                        End While
                    End Using

                    If bNeedFix Then
                        Helpers.Logger.WriteRelative("saving script file", , 6)
                        Helpers.Logger.WriteRelative("backup", , 7)
                        Helpers.FilesAndFolders.Backup.File(sScriptPath, 7, My.Application.Info.ProductName)

                        Helpers.Logger.WriteRelative("writing", , 7)
                        Helpers.Logger.WriteRelative(sScriptPath, , 8)
                        Using sw As New IO.StreamWriter(sScriptPath)
                            sw.Write(sScriptContents)
                        End Using
                        Helpers.Logger.WriteRelative("ok", , 9)
                    Else
                        Helpers.Logger.WriteRelative("no update needed", , 7)
                    End If

                    Helpers.Logger.WriteRelative("ok", , 3)
                Next
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("FAIL", 3)
                Helpers.Logger.WriteErrorRelative(ex.Message, 4)
            End Try


            '---------------------------------------------------
            Helpers.Logger.WriteRelative("loading StartmenuDialog file", , 2)
            Dim sStartmenuDialog As String = IO.Path.Combine(GetSiteKioskDirectory, "Skins\Win7IE8\StartmenuDialog.html")
            Dim linesStartmenu As New List(Of String)
            Dim bStartmenuScriptChanged As Boolean = False

            Helpers.Logger.WriteRelative(sStartmenuDialog, , 3)
            Helpers.Logger.WriteRelative("looking for IE icon", , 4)

            Using sr As New IO.StreamReader(sStartmenuDialog)
                Dim line As String, bDone As Boolean = False

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("skins\\Public\\Media\\Ie\\Img\\Icons\\ie.ico") Then
                        'found ie icon
                        Helpers.Logger.WriteRelative("found", , 4)

                        Helpers.Logger.WriteRelative("changing line", , 4)
                        Helpers.Logger.WriteRelative("from", , 5)
                        Helpers.Logger.WriteRelative(line, , 6)

                        line = line.Replace("StringTable.LoadString(500)", "StringTable.LoadString(50)")
                        line = line.Replace("skins\\Public\\Media\\Ie\\Img\\Icons\\ie.ico", "skins\\Public\\Media\\Firefox\\Img\\Icons\\ff.ico")
                        line = Regex.Replace(line, "OpenLink\(""[^""]+""\);", "iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('firefox')+')")

                        Helpers.Logger.WriteRelative("to", , 5)
                        Helpers.Logger.WriteRelative(line, , 6)


                        bStartmenuScriptChanged = True
                    End If

                    linesStartmenu.Add(line)
                End While
            End Using

            If bStartmenuScriptChanged Then
                Helpers.Logger.WriteRelative("saving script file", , 3)
                Helpers.Logger.WriteRelative("backup", , 4)
                Helpers.FilesAndFolders.Backup.File(sStartmenuDialog, 3, My.Application.Info.ProductName)

                Helpers.Logger.WriteRelative("writing", , 4)
                Helpers.Logger.WriteRelative(sStartmenuDialog, , 5)
                Using sw As New IO.StreamWriter(sStartmenuDialog)
                    For Each line As String In linesStartmenu
                        sw.WriteLine(line)
                    Next
                End Using

                Helpers.Logger.WriteRelative("ok", , 4)
            Else
                Helpers.Logger.WriteRelative("no update needed", , 3)
            End If


            '---------------------------------------------------
            Helpers.Logger.WriteRelative("does Firefox icon exist", , 2)
            Dim ffIcon As String = IO.Path.Combine(GetSiteKioskDirectory, "Skins\Public\Media\Firefox\Img\Icons\ff.ico")

            If Not IO.File.Exists(ffIcon) Then
                Helpers.Logger.WriteRelative("nope", , 3)
                Helpers.Logger.WriteRelative("copying", , 3)

                Try
                    Helpers.Logger.WriteRelative("creating folder", , 4)
                    IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(ffIcon))
                    Helpers.Logger.WriteRelative("ok", , 5)

                    Helpers.Logger.WriteRelative("copying file", , 4)

                    Dim _bytes() As Byte

                    Using ms As MemoryStream = New MemoryStream
                        My.Resources.ff.Save(ms)

                        _bytes = ms.ToArray
                    End Using

                    IO.File.WriteAllBytes(ffIcon, _bytes)

                    Helpers.Logger.WriteRelative("ok", , 5)
                Catch ex As Exception
                    Helpers.Logger.WriteErrorRelative("FAIL", 4)
                    Helpers.Logger.WriteErrorRelative(ex.Message, 5)
                End Try
            Else
                Helpers.Logger.WriteRelative("yep", , 3)
            End If


            '---------------------------------------------------
            Helpers.Logger.WriteRelative("change all theme internet buttons", , 2)
            Try
                Dim sThemeFolders As String() = IO.Directory.GetDirectories(IO.Path.Combine(GetSiteKioskDirectory, "Skins\Public\Startpages"))

                For Each sThemeFolder As String In sThemeFolders
                    Helpers.Logger.WriteRelative("entering '" & sThemeFolder & "'", , 3)

                    If Not IO.Directory.Exists(IO.Path.Combine(sThemeFolder, "img\icons")) Then
                        Helpers.Logger.WriteRelative("no icon folder found, skipping", , 4)

                        Continue For
                    End If

                    Helpers.Logger.WriteRelative("updating internet icon", , 4)
                    Dim sIcon As String = IO.Path.Combine(sThemeFolder, "img\icons\internet_pc.png")

                    If Not IO.File.Exists(sIcon) Then
                        sIcon = IO.Path.Combine(sThemeFolder, "img\icons\internet.png")

                        If Not IO.File.Exists(sIcon) Then
                            Helpers.Logger.WriteRelative("internet icon not found, skipping", , 5)

                            Continue For
                        End If
                    End If

                    Helpers.Logger.WriteRelative(sIcon, , 5)

                    Helpers.Logger.WriteRelative("backup", , 6)
                    Helpers.FilesAndFolders.Backup.File(sIcon, 6, My.Application.Info.ProductName)

                    Helpers.Logger.WriteRelative("copying from resources", , 6)
                    Dim _bytes() As Byte

                    Using ms As MemoryStream = New MemoryStream
                        'My.Resources.ff.Save(ms)
                        My.Resources.firefox_internet_icon_for_themes.Save(ms, Imaging.ImageFormat.Png)

                        _bytes = ms.ToArray
                    End Using

                    IO.File.WriteAllBytes(sIcon, _bytes)

                    Helpers.Logger.WriteRelative("ok", , 7)
                Next
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("FAIL", 3)
                Helpers.Logger.WriteErrorRelative(ex.Message, 4)
            End Try
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("error", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            ThreadSafe__ThreadComplete("updating configuration failed: " & ex.Message)
            Exit Sub
        End Try


        ThreadSafe__Toggle_progressUpdateConfiguration(False)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub
#End Region


#Region "Verify firefox profile"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__VerifyProfile()
        Helpers.Logger.WriteRelative("verifying firefox profile", , 1)

        ThreadSafe__VerifyProfile_Controls_toggle(
            "When you click the 'Start Firefox' button, Firefox will be started with the new profile. Please check if everything is alright, and make changes when needed." &
            vbCrLf & "This app will move to the background and continue automatically when you close Firefox.",
            True)

        Helpers.Logger.WriteRelative("finding firefox", , 2)
        'Find the firefox executable
        If Not FindFirefoxExecutable(FIREFOX_INSTALLED_FULLPATH) Then
            Helpers.Logger.WriteRelative("could Not find firefox.exe", , 3)
            Helpers.Logger.WriteRelative(Helpers.Errors.GetLast(False, False), , 4)

            ThreadSafe__ThreadComplete("Could Not find firefox.exe")
            Exit Sub
        End If
    End Sub

    Private Sub btnVerifyProfile_StartFirefox_Click(sender As Object, e As EventArgs) Handles btnVerifyProfile_StartFirefox.Click
        ThreadSafe__VerifyProfile_Controls_toggle(
            "one moment please, starting Firefox" & vbCrLf & "This app will move to the background automatically in a few moments, and will reappear when Firefox is closed.",
            False)


        Dim tmpPW As String

        If Not SITEKIOSK_USER_OLD_PASSWORD Then
            tmpPW = Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password)
        Else
            tmpPW = Helpers.XOrObfuscation_v2.Deobfuscate(Constants.UsernamePassword.SiteKiosk.Password_old)
        End If

        Helpers.Logger.WriteRelative("starting firefox", , 2)
        Helpers.Logger.WriteRelative("command line", , 3)
        Helpers.Logger.WriteRelative(FIREFOX_INSTALLED_FULLPATH, , 4)
        Helpers.Logger.WriteRelative("username", , 3)
        Helpers.Logger.WriteRelative(Constants.UsernamePassword.SiteKiosk.Username, , 4)
        Helpers.Logger.WriteRelative("password", , 3)
        Helpers.Logger.WriteRelative(New String("*", 10), , 4)
        Helpers.Logger.WriteRelative("arguments", , 3)
        Helpers.Logger.WriteRelative(InstallerConstants.Firefox.Install.FIREFOX_RUN_PARAMETERS, , 4)
        Helpers.Logger.WriteRelative("working directory", , 3)
        Helpers.Logger.WriteRelative(IO.Path.GetDirectoryName(FIREFOX_INSTALLED_FULLPATH), , 4)


        oProcess_VerifyProfile = New ProcessRunner
        oProcess_VerifyProfile.Arguments = InstallerConstants.Firefox.Install.FIREFOX_RUN_PARAMETERS
        oProcess_VerifyProfile.ExternalAppToWaitFor = ""
        oProcess_VerifyProfile.FileName = FIREFOX_INSTALLED_FULLPATH
        oProcess_VerifyProfile.LoadUserProfile = True
        oProcess_VerifyProfile.MaxTimeout = InstallerConstants.TimeoutsDelaysCounters.FIREFOX_VERIFY_PROFILE_TIMEOUT_s
        oProcess_VerifyProfile.PassWord = tmpPW
        oProcess_VerifyProfile.ProcessStyle = ProcessWindowStyle.Normal
        oProcess_VerifyProfile.TestMode = False
        oProcess_VerifyProfile.UserName = Constants.UsernamePassword.SiteKiosk.Username
        oProcess_VerifyProfile.UseShellExecute = False
        oProcess_VerifyProfile.WorkingDirectory = IO.Path.GetDirectoryName(FIREFOX_INSTALLED_FULLPATH)

        oProcess_VerifyProfile.StartProcess()
    End Sub

#Region "Process Events"
    Private Sub oProcess_VerifyProfile_DEBUG_MESSAGE(DebugString As String) Handles oProcess_VerifyProfile.DEBUG_MESSAGE
        Helpers.Logger.WriteRelative("oProcess_VerifyProfile.DEBUG_MESSAGE", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
        Helpers.Logger.WriteRelative(DebugString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
    End Sub

    Private Sub oProcess_VerifyProfile_Busy(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_VerifyProfile.Busy

    End Sub

    Private Sub oProcess_VerifyProfile_Test(ByVal EventProcess As System.Diagnostics.Process) Handles oProcess_VerifyProfile.Test

    End Sub

    Private Sub oProcess_VerifyProfile_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_VerifyProfile.Done
        ThreadSafe__ToggleForm(True)

        oProcess_VerifyProfile.ProcessDone()

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub

    Private Sub oProcess_VerifyProfile_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess_VerifyProfile.Failed
        Helpers.Logger.WriteRelative("failed", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Helpers.Logger.WriteRelative(ErrorMessage, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

        ThreadSafe__ToggleForm(True)

        ThreadSafe__ThreadComplete("An error occurred while starting firefox ('" & FIREFOX_INSTALLED_FULLPATH & "'): " & ErrorMessage)

        oProcess_VerifyProfile.ProcessDone()
    End Sub

    Private Sub oProcess_VerifyProfile_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess_VerifyProfile.Started
        Helpers.Logger.WriteRelative("ok", , 3)

        Helpers.Logger.WriteRelative("waiting for user to verify firefox", , 2)

        ThreadSafe__ToggleForm(False)
    End Sub

    Private Sub oProcess_VerifyProfile_TimeOut(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_VerifyProfile.TimeOut
        Helpers.Logger.WriteRelative("timed out", , 3)

        oProcess_VerifyProfile.ProcessDone()
        Helpers.Generic.KillAllFirefox()

        ThreadSafe__ToggleForm(True)


        ThreadSafe__VerifyProfile_Controls_toggle(
            "You've waited too long!",
            False)


        ThreadSafe__ThreadComplete("User waited too long")
    End Sub

    Private Sub oProcess_VerifyProfile_TimeOutFinal(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess_VerifyProfile.TimeOutFinal
        'not used, can only time out once
    End Sub
#End Region
#End Region


#Region "Save firefox profile"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__SaveProfile()
        Helpers.Logger.WriteRelative("saving firefox profile", , 1)

        ThreadSafe__Toggle_progressSaveProfile(True)
        ThreadSafe__SaveProfile_Text("copying profile to OVCC backend")

        ThreadSleep()

        Dim firefoxProfile As New Cleaners.FireFox.ProfileData

        firefoxProfile.Default = 1
        firefoxProfile.Index = 0
        firefoxProfile.Name = InstallerConstants.Firefox.Install.FIREFOX_PROFILE_NAME
        firefoxProfile.Path = InstallerConstants.Firefox.Install.FIREFOX_PROFILE_PATH
        firefoxProfile.FullPath = IO.Path.Combine(Cleaners.FireFox.Properties.ProfileRootPath, InstallerConstants.Firefox.Install.FIREFOX_PROFILE_PATH).Replace("/", "\")

        If Not IO.Directory.Exists(LOGOUTHELPER_INSTALLED_BASEPATH) Then
            Helpers.Logger.WriteRelative("base path for LogoutHelper not found!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.WriteRelative(LOGOUTHELPER_INSTALLED_BASEPATH, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            ThreadSafe__ThreadComplete("OVCC files not found in '" & LOGOUTHELPER_INSTALLED_BASEPATH & "'")
            Exit Sub
        End If

        Helpers.Logger.WriteRelative("copying firefox profile", , 2)
        If Not Cleaners.FireFox.Functions.Save(firefoxProfile, True, LOGOUTHELPER_INSTALLED_BASEPATH, 2) Then
            Helpers.Logger.WriteErrorRelative("error", 3)
            Helpers.Logger.WriteErrorRelative(Helpers.Errors.GetLast, 4)
        Else
            Helpers.Logger.WriteMessageRelative("ok", 3)
        End If


        ThreadSafe__Toggle_progressSaveProfile(False)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub
#End Region


#Region "Cleanup"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__Cleanup()
        Helpers.Logger.WriteRelative("cleanup", , 1)

        ThreadSafe__Toggle_progressCleanup(True)


        Dim sPath As String = IO.Path.GetDirectoryName(FIREFOX_DOWNLOAD_FULLPATH)
        Helpers.Logger.WriteRelative("deleting temp folder '" & sPath & "'", , 2)

        If IO.Directory.Exists(sPath) Then
            Dim deleteErrors As Long = 0

            Helpers.Logger.WriteRelative("cleaning files", , 3)
            deleteErrors = Helpers.FilesAndFolders.Folder.CleanFolder(sPath, "I_am_sure")
            If deleteErrors = 0 Then
                Helpers.Logger.WriteRelative("ok", , 4)
            Else
                If deleteErrors > 1 Then
                    Helpers.Logger.WriteWarningRelative("failed to delete " & deleteErrors.ToString & " files", 4)
                Else
                    Helpers.Logger.WriteWarningRelative("failed to delete " & deleteErrors.ToString & " file", 4)
                End If
            End If

            Helpers.Logger.WriteRelative("removing directory", , 3)
            Try
                IO.Directory.Delete(sPath)
                Helpers.Logger.WriteRelative("ok", , 4)
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("failed", 4)
                Helpers.Logger.WriteErrorRelative(ex.Message, 5)
            End Try
        Else
            Helpers.Logger.WriteRelative("doesn't exist, nothing to clean", , 3)
        End If



        ThreadSleep()


        ThreadSafe__Toggle_progressCleanup(False)

        ThreadSafe__ThreadComplete(PanelResults.OK)
    End Sub
#End Region


#Region "Done"
    '-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
    Private Sub PanelAction__Done()
        'done
        ThreadSafe__Done_Controls_toggle(True)
    End Sub

    Private Sub btnDone_Close_Click(sender As Object, e As EventArgs) Handles btnDone_Close.Click
        ThreadSafe__FormClose()
    End Sub
#End Region
#End Region

#Region "some more functions subs whatever"
    Public Function GetSiteKioskConfiguration() As String
        Dim skConfig As String = ""

        Try
            Dim regKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry32)
            skConfig = regKey.OpenSubKey("SOFTWARE\Provisio\SiteKiosk").GetValue("LastCfg", "")
        Catch ex As Exception

        End Try

        Return skConfig
    End Function

    Public Function GetSiteKioskDirectory() As String
        Dim skDir As String = ""

        Try
            Dim regKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry32)
            skDir = regKey.OpenSubKey("SOFTWARE\Provisio\SiteKiosk").GetValue("InstallDir", "")
        Catch ex As Exception

        End Try

        Return skDir
    End Function

    Public Sub ThreadSleep()
        ThreadSleep(1000, 5000)
    End Sub

    Public Sub ThreadSleep(_u As Integer)
        ThreadSleep(1000, _u)
    End Sub

    Public Sub ThreadSleep(_l As Integer, _u As Integer)
        Thread.Sleep(Helpers.Generic.RandomInteger(_l, _u, False))
    End Sub

    Public Function FindFirefoxExecutable(ByRef FirefoxExecutablePath As String) As Boolean
        Dim returnValue As Boolean = True

        FirefoxExecutablePath = ""

        Try
            Dim returnString As String = "", errorMessage As String = ""

            'First 64-bit
            If Helpers.Registry64.FindKeyBySubValueAndReturnValue(
                        "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                        "firefox",
                        "DisplayName",
                        "InstallLocation",
                        returnString,
                        errorMessage) Then

                FirefoxExecutablePath = returnString
            Else
                'Not found, let's try in 32-bit
                If Helpers.Registry.FindKeyBySubValueAndReturnValue(
                        "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                        "firefox",
                        "DisplayName",
                        "InstallLocation",
                        returnString,
                        errorMessage) Then

                    FirefoxExecutablePath = returnString
                Else
                    Throw New Exception("Could not find 'firefox' in 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall' registry key")
                End If
            End If

            FirefoxExecutablePath = FirefoxExecutablePath.Replace("""", "")
            FirefoxExecutablePath = IO.Path.Combine(FirefoxExecutablePath, InstallerConstants.Firefox.Install.FIREFOX_EXECUTABLE_NAME)

            If Not IO.File.Exists(FirefoxExecutablePath) Then
                Throw New Exception("Found 'firefox' in registry, but the file '" & FirefoxExecutablePath & "' does not exist")
            End If
        Catch ex As Exception
            returnValue = False

            Helpers.Errors.Add("Error while finding firefox uninstaller: " & ex.Message)
        End Try


        Return returnValue
    End Function

    Public Function FindFirefoxUninstaller(ByRef FirefoxUninstallerPath As String) As Boolean
        Dim returnValue As Boolean = True

        FirefoxUninstallerPath = ""

        Try
            Dim returnString As String = "", errorMessage As String = ""

            'First 64-bit
            If Helpers.Registry64.FindKeyBySubValueAndReturnValue(
                        "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                        "firefox",
                        "DisplayName",
                        "UninstallString",
                        returnString,
                        errorMessage) Then

                FirefoxUninstallerPath = returnString
            Else
                'Not found, let's try in 32-bit
                If Helpers.Registry.FindKeyBySubValueAndReturnValue(
                        "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall",
                        "firefox",
                        "DisplayName",
                        "UninstallString",
                        returnString,
                        errorMessage) Then

                    FirefoxUninstallerPath = returnString
                Else
                    Throw New Exception("Could not find 'firefox' in 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall' registry key")
                End If
            End If

            FirefoxUninstallerPath = FirefoxUninstallerPath.Replace("""", "")

            If Not IO.File.Exists(FirefoxUninstallerPath) Then
                Throw New Exception("Found 'firefox' in registry, but the uninstaller file '" & FirefoxUninstallerPath & "' does not exist")
            End If
        Catch ex As Exception
            returnValue = False

            Helpers.Errors.Add("Error while finding firefox uninstaller: " & ex.Message)
        End Try


        Return returnValue
    End Function

#End Region

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("started", , 0)


        If Helpers.Processes.IsAppAlreadyRunning Then
            Helpers.Logger.Write("there's already an instance running, exiting..", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)
            Application.Exit()
        End If


        '---------------------------------------------------------------------------------------
        Helpers.Forms.TopMost(Me.Handle, True)


        '---------------------------------------------------------------------------------------
        lblVersion.Text = My.Application.Info.Version.ToString


        '---------------------------------------------------------------------------------------
        UI_FIRST_PANEL_TOP = picboxLogo.Top + picboxLogo.Height + InstallerConstants.UI.UI_PANEL_MARGIN


        '---------------------------------------------------------------------------------------
        ResetAllPanels()


        '---------------------------------------------------------------------------------------
        Dim clientHeight As Integer = UI_FIRST_PANEL_TOP + (InstallerConstants.UI.UI_LARGE_PANEL_HEIGHT + InstallerConstants.UI.UI_PANEL_MARGIN) + ((InstallerConstants.UI.UI_SMALL_PANEL_HEIGHT + InstallerConstants.UI.UI_PANEL_MARGIN) * (mPanels_Small.Count - 1))
        Dim clientWidth As Integer = InstallerConstants.UI.UI_PANEL_MARGIN + InstallerConstants.UI.UI_SMALL_PANEL_WIDTH + InstallerConstants.UI.UI_PANEL_MARGIN

        Me.ClientSize = New Size(clientWidth, clientHeight)

        Me.Location = New Point((Screen.PrimaryScreen.Bounds.Width - Me.Width) / 2, (Screen.PrimaryScreen.Bounds.Height - Me.Height) / 2)


        '---------------------------------------------------------------------------------------
        ThreadSafe__InitialiseFirefox_Controls_toggle("", False)
        ThreadSafe__Toggle_progressUninstallFirefox(False)
        ThreadSafe__Toggle_progressInstallFirefox(False)
    End Sub
End Class
