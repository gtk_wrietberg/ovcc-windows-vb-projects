﻿Imports System.Diagnostics
Imports System.Threading

Public Class ProcessRunner
    Private WithEvents myProcess As New System.Diagnostics.Process
    Private elapsedTime As Double
    Private elapsedTimeTotal As Double
    Private eventHandled As Boolean


    Private Const cSleepAmount As Double = 0.2#

    Private Const OneSec As Double = 1.0# / (1440.0# * 60.0#)

    Private mMaxTimeout As Double = 30
    Private mTimeoutCount As Integer = 0
    Private mTimeoutCountMax As Integer = 5
    Private mTimeoutHappened As Boolean = False


    Private mProcessStyle As ProcessWindowStyle

    Private mFileName As String
    Private mArguments As String
    Private mWorkingDirectory As String
    Private mUserName As String
    Private mPassWord As String
    Private mExternalAppToWaitFor As String
    Private mLoadUserProfile As Boolean
    Private mUseShellExecute As Boolean

    Private mIsProcessDone As Boolean

    Private mErrorsOccurred As Boolean

    Private mBusyCounter As Integer
    Private mBusyTrigger As Integer
    Private mBusyTriggered As Boolean

    Private mTestMode As Boolean
    Private mDebugging As Boolean

    Public Event DEBUG_MESSAGE(DebugString As String)
    Public Event Started(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Busy(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Failed(ByVal EventProcess As Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String)
    Public Event KillFailed(ByVal EventProcess As Process, ByVal ErrorMessage As String)
    Public Event TimeOut(ByVal EventProcess As Process, ByVal TimeElapsed As Double, ByVal TimeoutCount As Integer, ByVal TimeoutCountMax As Integer)
    Public Event TimeOutFinal(ByVal EventProcess As Process, ByVal TimeElapsed As Double, ByVal TimeoutCount As Integer, ByVal TimeoutCountMax As Integer)
    Public Event Test(ByVal EventProcess As Process)
    Public Event Done(ByVal EventProcess As Process, ByVal TimeElapsed As Double)

    Public Sub New()
        mMaxTimeout = 30000
        mTimeoutCount = 0
        mTimeoutCountMax = 5
        mTimeoutHappened = False

        mProcessStyle = ProcessWindowStyle.Normal

        mFileName = ""
        mArguments = ""
        mUserName = ""
        mPassWord = ""
        mExternalAppToWaitFor = ""

        mBusyCounter = 0
        mBusyTrigger = 10
        mBusyTriggered = False

        mIsProcessDone = False
        mErrorsOccurred = False

        mTestMode = False
        mDebugging = False
    End Sub

#Region "Properties"
    Public Property TestMode() As Boolean
        Get
            Return mTestMode
        End Get
        Set(ByVal value As Boolean)
            mTestMode = value
        End Set
    End Property

    Public Property Debugging() As Boolean
        Get
            Return mDebugging
        End Get
        Set(ByVal value As Boolean)
            mDebugging = value
        End Set
    End Property

    Public Property BusyTrigger() As Integer
        Get
            Return mBusyTrigger
        End Get
        Set(ByVal value As Integer)
            mBusyTrigger = value
        End Set
    End Property

    Public Property MaxTimeout() As Double
        Get
            Return mMaxTimeout
        End Get
        Set(ByVal value As Double)
            mMaxTimeout = value
        End Set
    End Property

    Public ReadOnly Property TimeoutCount() As Integer
        Get
            Return mTimeoutCount
        End Get
    End Property

    Public ReadOnly Property TimeoutOccurred As Boolean
        Get
            Return mTimeoutHappened
        End Get
    End Property

    Public Property ProcessStyle() As ProcessWindowStyle
        Get
            Return mProcessStyle
        End Get
        Set(ByVal value As ProcessWindowStyle)
            mProcessStyle = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return mFileName
        End Get
        Set(ByVal value As String)
            mFileName = value
        End Set
    End Property

    Public Property Arguments() As String
        Get
            Return mArguments
        End Get
        Set(ByVal value As String)
            mArguments = value
        End Set
    End Property

    Public Property WorkingDirectory() As String
        Get
            Return mWorkingDirectory
        End Get
        Set(ByVal value As String)
            mWorkingDirectory = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set
    End Property

    Public Property PassWord() As String
        Get
            Return mPassWord
        End Get
        Set(ByVal value As String)
            mPassWord = value
        End Set
    End Property

    Public Property LoadUserProfile() As Boolean
        Get
            Return mLoadUserProfile
        End Get
        Set(ByVal value As Boolean)
            mLoadUserProfile = value
        End Set
    End Property

    Public Property UseShellExecute() As Boolean
        Get
            Return mUseShellExecute
        End Get
        Set(ByVal value As Boolean)
            mUseShellExecute = value
        End Set
    End Property

    Public Property ExternalAppToWaitFor() As String
        Get
            Return mExternalAppToWaitFor
        End Get
        Set(ByVal value As String)
            mExternalAppToWaitFor = value
        End Set
    End Property

    Public ReadOnly Property IsProcessDone() As Boolean
        Get
            Return mIsProcessDone
        End Get
    End Property

    Public ReadOnly Property ErrorsOccurred() As Boolean
        Get
            Return mErrorsOccurred
        End Get
    End Property

    Public ReadOnly Property BusyTriggered() As Boolean
        Get
            Return mBusyTriggered
        End Get
    End Property
#End Region

    Public Sub StartProcess()
        StartProcess(mFileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String)
        StartProcess(FileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String, ByVal Arguments As String)
        elapsedTime = 0.0#
        elapsedTimeTotal = 0.0#

        eventHandled = False

        mBusyCounter = 0

        If mTestMode Then
            RaiseEvent Test(myProcess)
            RaiseEvent Done(myProcess, elapsedTimeTotal)

            Exit Sub
        End If

        Try
            'Dim pInfo As New ProcessStartInfo()

            'pInfo.FileName = cmdLine
            'pInfo.Arguments = cmdArguments
            'pInfo.WorkingDirectory = sWorkingDirectory
            'If sUserName <> "" Then
            '    pInfo.UserName = sUserName
            '    pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
            '    pInfo.Domain = Nothing
            '    pInfo.UseShellExecute = False
            '    pInfo.LoadUserProfile = bLoadUserProfile
            '    pInfo.Verb = "runas"
            '    If hidden Then
            '        pInfo.WindowStyle = ProcessWindowStyle.Hidden
            '    End If
            'End If




            myProcess.StartInfo.FileName = mFileName
            myProcess.StartInfo.Arguments = mArguments
            myProcess.StartInfo.WorkingDirectory = mWorkingDirectory
            If mUserName <> "" Then
                Dim securePass As New Security.SecureString()

                For Each c As Char In mPassWord
                    securePass.AppendChar(c)
                Next c

                myProcess.StartInfo.UserName = mUserName
                myProcess.StartInfo.Password = securePass
                myProcess.StartInfo.LoadUserProfile = mLoadUserProfile
                myProcess.StartInfo.UseShellExecute = mUseShellExecute
                myProcess.StartInfo.Verb = "runas"
            End If
            myProcess.StartInfo.WindowStyle = mProcessStyle
            myProcess.EnableRaisingEvents = True
            myProcess.Start()

            RaiseEvent Started(myProcess, elapsedTimeTotal)
        Catch ex As Exception
            RaiseEvent Failed(myProcess, elapsedTimeTotal, ex.Message)
            mErrorsOccurred = True

            Exit Sub
        End Try

        Try
            If myProcess.Id <= 0 Then
                RaiseEvent Failed(myProcess, elapsedTimeTotal, "pid = 0")
                mErrorsOccurred = True

                Exit Sub
            End If
        Catch ex As Exception
            RaiseEvent Failed(myProcess, elapsedTimeTotal, ex.Message)
            mErrorsOccurred = True

            Exit Sub
        End Try

        _TimeoutLoop()
    End Sub

    Public Sub ResumeWaiting()
        _TimeoutLoop()
    End Sub

    Private Function _TimeoutLoop() As Boolean
        mTimeoutHappened = False

        elapsedTime = 0.0#

        Do While _TimeoutCheck()
            elapsedTime += cSleepAmount
            elapsedTimeTotal += cSleepAmount
            mBusyCounter += 1

            elapsedTime = Math.Round(elapsedTime, 2)

            If mDebugging Then RaiseEvent DEBUG_MESSAGE("elapsedTime=" & elapsedTime.ToString & " - mMaxTimeout=" & mMaxTimeout.ToString)

            If elapsedTime > mMaxTimeout Then
                mTimeoutCount += 1
                mTimeoutHappened = True

                If mTimeoutCount >= mTimeoutCountMax Then
                    RaiseEvent TimeOutFinal(myProcess, elapsedTimeTotal, mTimeoutCount, mTimeoutCountMax)

                    StopProcess()
                Else
                    RaiseEvent TimeOut(myProcess, elapsedTimeTotal, mTimeoutCount, mTimeoutCountMax)
                End If

                Exit Do
            Else
                If mBusyCounter >= mBusyTrigger Then
                    mBusyCounter = 0
                    mBusyTriggered = True
                    RaiseEvent Busy(myProcess, elapsedTimeTotal)
                End If
            End If

            Thread.Sleep(cSleepAmount * 1000)
        Loop

        If Not mTimeoutHappened Or mTimeoutCount >= mTimeoutCountMax Then
            RaiseEvent Done(myProcess, elapsedTimeTotal)
        End If

        Return mTimeoutHappened
    End Function

    Private Function _TimeoutCheck() As Boolean
        Dim bLoop As Boolean = False

        Try
            If myProcess Is Nothing Then
                Return False
            End If

            bLoop = Not eventHandled And Not myProcess.HasExited

            If mExternalAppToWaitFor <> "" Then
                If Process.GetProcessesByName(mExternalAppToWaitFor).Length > 0 Then
                    bLoop = True
                End If
            End If
        Catch ex As Exception

        End Try

        Return bLoop
    End Function

    Public Sub StopProcess()
        Try
            myProcess.Kill()
        Catch ex As Exception
            RaiseEvent KillFailed(myProcess, ex.Message)
        End Try
    End Sub

    Public Sub ProcessDone()
        eventHandled = True

        myProcess.Close()

        mIsProcessDone = True
    End Sub
End Class
