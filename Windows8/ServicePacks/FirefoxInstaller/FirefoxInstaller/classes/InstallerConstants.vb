﻿Public Class InstallerConstants
    Public Class UI
        Public Shared ReadOnly UI_PANEL_MARGIN As Integer = 6
        Public Shared ReadOnly UI_SMALL_PANEL_WIDTH As Integer = 578
        Public Shared ReadOnly UI_SMALL_PANEL_HEIGHT As Integer = 26
        Public Shared ReadOnly UI_LARGE_PANEL_WIDTH As Integer = 578
        Public Shared ReadOnly UI_LARGE_PANEL_HEIGHT As Integer = 154
    End Class

    Public Class Firefox
        Public Class Download
            Public Shared ReadOnly FIREFOX_DOWNLOAD_URL_TEMPLATE As String = "https://download.mozilla.org/?product=firefox-latest-ssl&os=!!OS!!&lang=!!LANG!!"
            Public Shared ReadOnly FIREFOX_DOWNLOAD_WIN32 As String = "win"
            Public Shared ReadOnly FIREFOX_DOWNLOAD_WIN64 As String = "win64"
            Public Shared ReadOnly FIREFOX_DOWNLOAD_LANG_GB As String = "en-GB"
            Public Shared ReadOnly FIREFOX_DOWNLOAD_LANG_US As String = "en-US"
        End Class

        Public Class Install
            Public Shared ReadOnly FIREFOX_DOWNLOAD_FILENAME As String = "OVCC_FIREFOX_INSTALLER.exe"
            Public Shared ReadOnly FIREFOX_UNINSTALL_PARAMETERS As String = "/S"
            Public Shared ReadOnly FIREFOX_INSTALL_PARAMETERS As String = "/S /OptionalExtensions=false"  ' https://firefox-source-docs.mozilla.org/browser/installer/windows/installer/FullConfig.html
            Public Shared ReadOnly FIREFOX_RUN_PARAMETERS As String = "-foreground -new-instance"
            Public Shared ReadOnly FIREFOX_NAME As String = "Mozilla Firefox"
            Public Shared ReadOnly FIREFOX_NAME_SHORT As String = "Firefox"
            Public Shared ReadOnly FIREFOX_EXECUTABLE_NAME As String = "firefox.exe"
            Public Shared ReadOnly FIREFOX_PROFILE_TEMPLATE_FILENAME As String = "OVCC_FIREFOX_TEMPLATE.zip"
            Public Shared ReadOnly FIREFOX_PROFILE_NAME As String = "0vccpr0f1le.OVCC"
            Public Shared ReadOnly FIREFOX_PROFILE_PATH As String = "Profiles/0vccpr0f1le.OVCC"
            Public Shared ReadOnly LOGOUTHELPER_FILENAME As String = "LogoutCleanupHelper.deploy.exe"
            Public Shared ReadOnly LOGOUTHELPER_INSTALLED_BASEPATH_TEMPLATE As String = "%%PROGRAMFILES%%\GuestTek\LogoutCleanupHelper"
        End Class
    End Class

    Public Class Sitekiosk
        Public Shared ReadOnly SITEKIOSK_FIREFOX_WINDOWCLASS_NAME As String = "MozillaWindowClass"
    End Class

    Public Class TimeoutsDelaysCounters
        Public Shared ReadOnly FIREFOX_INITIALISATION_TIMEOUT_s As Integer = 120
        Public Shared ReadOnly FIREFOX_INITIALISATION_WINDOW_WAIT_TIMEOUT_x As Integer = 60
        Public Shared ReadOnly FIREFOX_UNINSTALL_TIMEOUT_s As Integer = 600
        Public Shared ReadOnly FIREFOX_INSTALL_TIMEOUT_s As Integer = 600
        Public Shared ReadOnly FIREFOX_VERIFY_PROFILE_TIMEOUT_s As Integer = 1800
        Public Shared ReadOnly LOGOUTHELPER_INSTALL_TIMEOUT_s As Integer = 600

        Public Shared ReadOnly APP_RETRIES_COUNTER_max As Integer = 3
    End Class
End Class
