﻿Imports VBGraphics

Module Module1
    Public ReadOnly CONST__size_x As Integer = 900
    Public ReadOnly CONST__size_y As Integer = 600
    Public ReadOnly CONST__lines As Integer = 100

    Sub Main()
        ' Create a graphics windows with resolution 600 x 400.
        Dim gw As New GraphicsWindow(CONST__size_x, CONST__size_y)

        ' Draw something in the graphics window...
        For i As Integer = 0 To CONST__lines - 1
            gw.DrawLine(i * CInt(CONST__size_x / CONST__lines), 0, CONST__size_x - 1, CONST__size_y - 1, Color.Red)
            gw.DrawLine(0, i * CInt(CONST__size_y / CONST__lines), CONST__size_x - 1, CONST__size_y - 1, Color.Red)
        Next
        'gw.DrawLine(0, 0, 599, 399, Color.Red)
        gw.DrawText("Press any key to close...", 0, 370, Color.White, "Cambria", 16)

        ' Make the graphics window closable, and end the program when the window is closed.
        gw.EndProgramOnClose = True
        gw.CanClose = True

        ' Reads a key in the keyboard. Note: if the user closes the window, this statement will not
        ' finish execution.
        gw.ReadKey()

        ' It is a good habit to dispose the graphics window after use.
        gw.Dispose()
    End Sub
End Module
