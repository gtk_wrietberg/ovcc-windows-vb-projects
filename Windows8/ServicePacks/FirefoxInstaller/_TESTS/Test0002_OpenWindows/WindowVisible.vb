﻿Imports System.Runtime.InteropServices
Imports System.Text

Public Class WindowVisible
    Private Delegate Function CallBack(ByVal hwnd As Integer, ByVal lParam As Integer) As Boolean
    Private Declare Function EnumWindows Lib "user32" (ByVal Adress As CallBack, ByVal y As Integer) As Integer
    Private Declare Function IsWindowVisible Lib "user32.dll" (ByVal hwnd As IntPtr) As Boolean
    Private Shared hWnd As Integer

    <DllImport("user32.dll", SetLastError:=True)>
    Private Shared Function GetWindowThreadProcessId(ByVal hWnd As IntPtr, ByRef lpdwProcessId As Integer) As Integer
    End Function


    <DllImport("user32.dll", CharSet:=CharSet.Auto)>
    Private Shared Sub GetClassName(ByVal hWnd As System.IntPtr, ByVal lpClassName As System.Text.StringBuilder, ByVal nMaxCount As Integer)
    End Sub

    Private Shared _ClassName As String = ""
    Public Shared Pids As New List(Of Integer)


    Public Shared Function FindWindowByClass(ClassName As String) As Boolean
        _ClassName = ClassName
        Pids = New List(Of Integer)

        EnumWindows(AddressOf Enumerator, 0)

        Return Pids.Count > 0
    End Function

    Private Shared Function Enumerator(ByVal hwnd As IntPtr, ByVal lParam As Integer) As Boolean
        Dim classname As String = ""

        Dim pId As Integer = -1

        If IsWindowVisible(hwnd) Then
            GetWindowThreadProcessId(hwnd, pId)
            classname = GetWindowClass(hwnd)

            If classname.Equals("MozillaWindowClass") Then
                Pids.Add(pId)
            End If
        End If

        Return True
    End Function

    Public Shared Function GetWindowClass(ByVal hwnd As Long) As String
        Dim sClassName As New StringBuilder("", 256)

        Call GetClassName(hwnd, sClassName, 256)

        Return sClassName.ToString
    End Function
End Class
