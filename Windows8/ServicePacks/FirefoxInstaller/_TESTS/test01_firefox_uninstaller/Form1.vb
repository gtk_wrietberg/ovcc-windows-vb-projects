﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim path As String = ""

        If FindFirefoxUninstaller(path) Then
            MsgBox(path)
        Else
            MsgBox("error:" & Helpers.Errors.GetLast(False, False))
        End If
    End Sub




    Public ReadOnly FIREFOX_UNINSTALL_SUBPATH As String = "Mozilla Firefox\uninstall\helper.exe"

    Public Function FindFirefoxUninstaller(ByRef FirefoxUninstallerPath As String) As Boolean
        Dim tmp_path1 As String = "", tmp_path2 = ""
        Dim returnValue As Boolean = True

        FirefoxUninstallerPath = ""

        Try
            If Helpers.Generic.IsFirefoxInstalled Then
                If Environment.Is64BitOperatingSystem Then
                    tmp_path1 = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), FIREFOX_UNINSTALL_SUBPATH)
                    tmp_path2 = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), FIREFOX_UNINSTALL_SUBPATH)

                    If IO.File.Exists(tmp_path1) And IO.File.Exists(tmp_path2) Then
                        'uh oh, we have to find out which one is the newest
                        Dim _date1 As Date, _date2 As Date, _diff As Integer = 0

                        _date1 = Helpers.FilesAndFolders.GetFileLastModifiedDateAsDate(tmp_path1)
                        _date2 = Helpers.FilesAndFolders.GetFileLastModifiedDateAsDate(tmp_path2)
                        _diff = Date.Compare(_date1, _date2)

                        Select Case _diff
                            Case < 0
                                FirefoxUninstallerPath = tmp_path2
                            Case 0
                                FirefoxUninstallerPath = tmp_path2
                            Case > 0
                                FirefoxUninstallerPath = tmp_path1
                        End Select
                    Else
                        If IO.File.Exists(tmp_path1) Then
                            FirefoxUninstallerPath = tmp_path1
                        ElseIf IO.File.Exists(tmp_path2) Then
                            FirefoxUninstallerPath = tmp_path2
                        Else
                            Throw New Exception("Could not find '" & FIREFOX_UNINSTALL_SUBPATH & "' in 'Program Files' or 'Program Files (x86)' folder")
                        End If
                    End If
                Else
                    tmp_path1 = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "Mozilla Firefox\uninstall\helper.exe")

                    If IO.File.Exists(tmp_path1) Then
                        FirefoxUninstallerPath = tmp_path1
                    Else
                        Throw New Exception("Could not find '" & FIREFOX_UNINSTALL_SUBPATH & "' in 'Program Files' folder")
                    End If
                End If
            Else
                Throw New Exception("Apparently Firefox is not installed")
            End If
        Catch ex As Exception
            returnValue = False

            Helpers.Errors.Add("Error while finding firefox uninstaller: " & ex.Message)
        End Try


        Return returnValue
    End Function

End Class
