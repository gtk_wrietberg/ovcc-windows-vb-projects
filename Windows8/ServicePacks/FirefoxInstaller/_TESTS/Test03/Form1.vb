﻿Imports System.IO
Imports System.Text.RegularExpressions

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sStartmenuDialog As String = IO.Path.Combine(GetSiteKioskDirectory, "Skins\Win7IE8\StartmenuDialog.html")
        Dim linesStartmenu As New List(Of String)
        Dim bStartmenuScriptChanged As Boolean = False





        Using sr As New IO.StreamReader(sStartmenuDialog)
            Dim line As String, bDone As Boolean = False

            While Not sr.EndOfStream
                line = sr.ReadLine

                If line.Contains("skins\\Public\\Media\\Ie\\Img\\Icons\\ie.ico") Then
                    Debug.WriteLine(line)
                    line = line.Replace("StringTable.LoadString(500)", """Firefox""")
                    line = line.Replace("skins\\Public\\Media\\Ie\\Img\\Icons\\ie.ico", "skins\\Public\\Media\\Firefox\\Img\\Icons\\ff.ico")
                    line = Regex.Replace(line, "OpenLink\(""[^""]+""\);", "iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('firefox')+')")
                    Debug.WriteLine(line)


                    ' iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('firefox')+')
                    bStartmenuScriptChanged = True
                End If

                linesStartmenu.Add(line)
            End While
        End Using


        If bStartmenuScriptChanged Then
            Helpers.Logger.WriteRelative("saving script file", , 2)
            Helpers.Logger.WriteRelative("backup", , 3)
            Helpers.FilesAndFolders.Backup.File(sStartmenuDialog, 3, My.Application.Info.ProductName)

            Helpers.Logger.WriteRelative("writing", , 3)
            Helpers.Logger.WriteRelative(sStartmenuDialog, , 4)
            Using sw As New IO.StreamWriter(sStartmenuDialog)
                For Each line As String In linesStartmenu
                    sw.WriteLine(line)
                Next
            End Using
            Helpers.Logger.WriteRelative("ok", , 5)
        End If
    End Sub

    Public Function GetSiteKioskDirectory() As String
        Dim skDir As String = ""

        Try
            Dim regKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry32)
            skDir = regKey.OpenSubKey("SOFTWARE\Provisio\SiteKiosk").GetValue("InstallDir", "")
        Catch ex As Exception

        End Try

        Return skDir
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Helpers.Logger.WriteRelative("does Firefox icon exist", , 3)
        Dim ffIcon As String = IO.Path.Combine(GetSiteKioskDirectory, "Skins\Public\Media\Firefox\Img\Icons\ff.ico")

        If Not IO.File.Exists(ffIcon) Then
            Helpers.Logger.WriteRelative("nope", , 4)
            Helpers.Logger.WriteRelative("copying", , 4)

            Try
                Helpers.Logger.WriteRelative("creating folder", , 5)
                IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(ffIcon))
                Helpers.Logger.WriteRelative("ok", , 6)

                Helpers.Logger.WriteRelative("copying file", , 5)

                Dim _bytes() As Byte

                Using ms As MemoryStream = New MemoryStream
                    My.Resources.ff.Save(ms)

                    _bytes = ms.ToArray
                End Using

                IO.File.WriteAllBytes(ffIcon, _bytes)

                Helpers.Logger.WriteRelative("ok", , 6)
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("FAIL", 5)
                Helpers.Logger.WriteErrorRelative(ex.Message, 6)
            End Try
        Else
            Helpers.Logger.WriteRelative("yep", , 4)
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("started", , 0)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Helpers.Logger.WriteRelative("change all theme internet buttons", , 3)
        Try
            Dim sThemeFolders As String() = IO.Directory.GetDirectories(IO.Path.Combine(GetSiteKioskDirectory, "Skins\Public\Startpages"))

            For Each sThemeFolder As String In sThemeFolders
                Helpers.Logger.WriteRelative("entering '" & sThemeFolder & "'", , 4)

                If Not IO.Directory.Exists(IO.Path.Combine(sThemeFolder, "img\icons")) Then
                    Helpers.Logger.WriteRelative("no icon folder found, skipping", , 5)

                    Continue For
                End If

                Helpers.Logger.WriteRelative("updating internet icon", , 5)
                Dim sIcon As String = IO.Path.Combine(sThemeFolder, "img\icons\internet_pc.png")

                If Not IO.File.Exists(sIcon) Then
                    sIcon = IO.Path.Combine(sThemeFolder, "img\icons\internet.png")

                    If Not IO.File.Exists(sIcon) Then
                        Helpers.Logger.WriteRelative("internet icon not found, skipping", , 6)

                        Continue For
                    End If
                End If

                Helpers.Logger.WriteRelative(sIcon, , 6)

                Helpers.Logger.WriteRelative("backup", , 7)
                Helpers.FilesAndFolders.Backup.File(sIcon, 7, My.Application.Info.ProductName)

                Helpers.Logger.WriteRelative("copying from resources", , 7)
                Dim _bytes() As Byte

                Using ms As MemoryStream = New MemoryStream
                    My.Resources.ff.Save(ms)

                    _bytes = ms.ToArray
                End Using

                IO.File.WriteAllBytes(sIcon, _bytes)

                Helpers.Logger.WriteRelative("ok", , 8)
            Next
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("FAIL", 4)
            Helpers.Logger.WriteErrorRelative(ex.Message, 5)
        End Try
    End Sub
End Class
