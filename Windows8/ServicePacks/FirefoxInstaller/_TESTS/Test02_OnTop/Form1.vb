﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Forms.TopMost(Me.Handle, True)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Helpers.Forms.TopMost(Me.Handle, False)
        Me.WindowState = FormWindowState.Minimized

        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False

        Me.WindowState = FormWindowState.Normal
        Helpers.Forms.TopMost(Me.Handle, True)
    End Sub
End Class
