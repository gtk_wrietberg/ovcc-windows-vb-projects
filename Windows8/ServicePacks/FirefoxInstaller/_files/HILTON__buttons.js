var _Buttons=new Array();
var _Labels=new Array();

var _idBuildButtonsInit=0;

function BuildButtonsInit() {
	if(iBAHN__AreApplicationsInitialised()) {
		LoadButtonContent();
		BuildButtonTable();
	} else {	
		_idBuildButtonsInit=setTimeout('BuildButtonsInit()',250);
	}
}

function LoadButtonContent() {
	// reset arrays in case of re-load
	_Buttons=new Array();
	_Labels=new Array();
	
	var bDoYouReallyWantTheFirefoxButton=true;

	if(iBAHN__DoesApplicationExist('firefox')&&bDoYouReallyWantTheFirefoxButton) {
		_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('firefox')+')"><img src="img/icons/internet.png" alt="" border="0"></a>');
		_Labels.push ('<a class="aButtons" onclick="javascript:iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('firefox')+')">'+LoadString(50)+'</a>');
	} else {
	_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenInternetUrl()"><img src="img/icons/internet.png" alt="" border="0"></a>');
	_Labels.push ('<a class="aButtons" onclick="javascript:iBAHN__OpenInternetUrl()">'+LoadString(50)+'</a>');
	}

	_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenWeatherUrl()"><img src="img/icons/weather.png" alt="" border="0"></a>');
	_Labels.push ('<a class="aButtons" onclick="javascript:iBAHN__OpenWeatherUrl()">'+LoadString(63)+'</a>');
	_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenMapUrl()"><img src="img/icons/maps.png" alt="" border="0"></a>');	
	_Labels.push ('<a class="aButtons" onclick="javascript:iBAHN__OpenMapUrl()">'+LoadString(64)+'</a>');	

	// _Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenHotelUrl()"><img src="img/icons/hotel.png" alt="" border="0"><br>'+LoadString(52)+'</a>');

	_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenFreeAirlineDialog()"><img src="img/icons/boarding.png" alt="" border="0"></a>');
	_Labels.push('<a class="aButtons" onclick="javascript:iBAHN__OpenFreeAirlineDialog()">'+LoadString(53)+'</a>');

	if(iBAHN__DoesApplicationExist('word')) {
		_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('word')+')"><img src="img/icons/word.png" alt="" border="0"></a>');
		_Labels.push ('<a class="aButtons" onclick="javascript:iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('word')+')">Word</a>');
	}
	if(iBAHN__DoesApplicationExist('excel')) {
		_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('excel')+')"><img src="img/icons/excel.png" alt="" border="0"></a>');
		_Labels.push ('<a class="aButtons" onclick="javascript:iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('excel')+')">Excel</a>');
	}
	if(iBAHN__DoesApplicationExist('powerpoint')) {
		_Buttons.push('<a class="aButtons" onclick="javascript:iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('powerpoint')+')"><img src="img/icons/powerpoint.png" alt="" border="0"></a>');
		_Labels.push ('<a class="aButtons" onclick="javascript:iBAHN__OpenApplicationById('+iBAHN__GetApplicationId('powerpoint')+')">PowerPoint</a>');
	}
}

function BuildButtonTable() {
	var oDivButtons=document.getElementById('buttons');
	
	oDivButtons.innerHTML='';
	
	var table=document.createElement('table');
	
	table.className="tableButtons";
	table.align="center";
	table.cellSpacing = "0";
	table.cellPadding = "0";
	
	var tbody=document.createElement('tbody');
	var row,cell;

	// buttons row
	row=document.createElement('tr');
	row.className="trButtons";
	
	for(var i=0;i<_Buttons.length;i++) {
		cell=document.createElement('td');
		cell.className="tdButtons";
		cell.onmouseover=function() {
			this.className="tdButtonsOver";
		}
		cell.onmouseout=function() {
			this.className="tdButtons";
		}
		cell.innerHTML=_Buttons[i];
		row.appendChild(cell);
		if (i < _Buttons.length-1) {
			// add spacer
			cell=document.createElement('td');
			cell.className="tdSpacers";
			cell.innerHTML='&nbsp;';
			row.appendChild(cell);
		}
	}
	
	tbody.appendChild(row);

	// labels row
	row=document.createElement('tr');
	row.className="trLabels";
	
	for(var i=0;i<_Labels.length;i++) {
		cell=document.createElement('td');
		cell.className="tdLabels";
		cell.innerHTML=_Labels[i];
		row.appendChild(cell);
		if (i < _Labels.length-1) {
			// add spacer
			cell=document.createElement('td');
			cell.className="tdSpacers";
			cell.innerHTML='&nbsp;';
			row.appendChild(cell);
		}
	}
	tbody.appendChild(row);

	table.appendChild(tbody);
	oDivButtons.appendChild(table);
}

addLoadEvent(BuildButtonsInit);
