﻿Module ModMain
    Private ReadOnly Anydesk_CmdLine As String = "anydesk.exe"
    Private ReadOnly Anydesk_Params As String = "--silent --remove"

    Public Sub Main()
        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("started", 0)

        Dim sPath As String = ""


        Helpers.Logger.WriteMessage("searching for Anydesk", 0)

        sPath = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "Anydesk", Anydesk_CmdLine)
        Helpers.Logger.WriteMessage(sPath, 1)

        If Not IO.File.Exists(sPath) Then
            Helpers.Logger.WriteError("not found", 2)

            ExitCode.SetValue(ExitCode.ExitCodes.ANYDESK_NOT_FOUND)
            ExitApplication()
            Exit Sub
        Else
            Helpers.Logger.WriteMessage("found", 2)
        End If


        Helpers.Logger.WriteMessage("Starting uninstaller", 0)
        Helpers.Logger.WriteMessage(sPath & " " & Anydesk_Params, 1)


        If Helpers.Processes.StartProcess(sPath, Anydesk_Params, True) Then
            Helpers.Logger.WriteMessage("ok", 2)
        Else
            Helpers.Logger.WriteError("fail!", 2)
            Helpers.Logger.WriteError(Helpers.Errors.GetLast(), 3)
            ExitCode.SetValue(ExitCode.ExitCodes.ANYDESK_UNINSTALL_FAILED)
        End If

        Helpers.Logger.WriteMessage("bye", 0)
    End Sub

    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
