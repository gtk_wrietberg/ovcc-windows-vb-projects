﻿Imports System.Text.RegularExpressions

Public Class Settings
    Public Class Generic
        Public Enum ScrubbingMethods As Integer
            Scrub = 0
            Obfuscate = 1
        End Enum

        Public Shared Debugging As Boolean = False
        Public Shared ScrubbingMethod As ScrubbingMethods = ScrubbingMethods.Scrub
    End Class

    Public Class Logfiles
        Public Shared Folder As String = ""
        Public Shared LastScrubbedDate As String = ""
        Public Shared Suffix As String = ""
    End Class


    Public Shared SETTINGS_REGISTRYKEY As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\SiteKioskLogfileScrubber"
    Public Shared DEFAULT_DATE As String = "2001-01-01"

    Private Shared ReadOnly rePattern_datecheck As String = "^([0-9]{4})-([0-9]{2})-([0-9]{2})$"

    Public Shared Sub Load()
        Generic.Debugging = Helpers.Registry.GetValue_Boolean(SETTINGS_REGISTRYKEY & "\Settings", "Debugging", False)
        Generic.ScrubbingMethod = Helpers.Registry.GetValue_Integer(SETTINGS_REGISTRYKEY & "\Settings", "ScrubbingMethod", -1)

        'sanity check
        If Not [Enum].IsDefined(GetType(Generic.ScrubbingMethods), Generic.ScrubbingMethod) Then
            Generic.ScrubbingMethod = Generic.ScrubbingMethods.Scrub
        End If


        Logfiles.Folder = Helpers.Registry.GetValue_String(SETTINGS_REGISTRYKEY & "\Logfiles", "Folder", "")
        Logfiles.LastScrubbedDate = Helpers.Registry.GetValue_String(SETTINGS_REGISTRYKEY & "\Logfiles", "LastScrubbedDate", DEFAULT_DATE)
        Logfiles.Suffix = Helpers.Registry.GetValue_String(SETTINGS_REGISTRYKEY & "\Logfiles", "Suffix", "")

        If Not Regex.Match(Logfiles.LastScrubbedDate, rePattern_datecheck).Success Then
            Logfiles.LastScrubbedDate = DEFAULT_DATE
        End If

        'sanity save
        Save()
    End Sub

    Public Shared Sub Save()
        Helpers.Registry.SetValue_Boolean(SETTINGS_REGISTRYKEY & "\Settings", "Debugging", Generic.Debugging)
        Helpers.Registry.SetValue_Integer(SETTINGS_REGISTRYKEY & "\Settings", "ScrubbingMethod", Generic.ScrubbingMethod)

        Helpers.Registry.SetValue_String(SETTINGS_REGISTRYKEY & "\Logfiles", "Folder", Logfiles.Folder)
        Helpers.Registry.SetValue_String(SETTINGS_REGISTRYKEY & "\Logfiles", "LastScrubbedDate", Logfiles.LastScrubbedDate)
        Helpers.Registry.SetValue_String(SETTINGS_REGISTRYKEY & "\Logfiles", "Suffix", Logfiles.Suffix)
    End Sub
End Class
