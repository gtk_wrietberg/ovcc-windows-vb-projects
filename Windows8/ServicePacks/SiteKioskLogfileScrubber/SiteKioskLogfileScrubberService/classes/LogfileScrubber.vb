﻿Imports System.IO
Imports System.Text.RegularExpressions

Public Class LogfileScrubber
    Private Shared ReadOnly rePattern_file As String = "^([0-9]{4})-([0-9]{2})-([0-9]{2})\.txt$"
    Private Shared ReadOnly rePattern_file_without_extension As String = "^([0-9]{4})-([0-9]{2})-([0-9]{2})$"

    '' Leave this: 20 03ec 2017-12-15 09:33:52 -0000 [SiteKiosk] Navigation: file:///C:/Program%20Files%20(x86)/SiteKiosk/Skins/Public/Startpages/Hilton/index.html
    '' Scrub this: 20 03ec 2017-12-15 09:34:01 -0000 [SiteKiosk] Navigation: https://hilton.hiltonwifi.com/LONMETW/?utm_source=gtk&utm_medium=direct&utm_campaign=bc
    '' Scrub this: 20 03ed 2017-12-15 09:34:02 -0000 [SiteKiosk] Frame-Navigation: https://s0.2mdn.net/dfp/472982/382581422/1512058469863/EN_6_970x250/index.html

    Private Shared ReadOnly rePattern_Navigation01 As String = "(.+)(\[SiteKiosk\] )(Frame-Navigation|Navigation)(: )(http[s]*://)([^/]+)(.*)"

    Private Shared mList_Logs As List(Of String)
    Private Shared mTodaysLog As String

    Private Shared mLinesScrubbedCount As Long = 0


    Public Shared Function ScrubLogs() As Boolean
        mLinesScrubbedCount = 0
        mTodaysLog = ""

        Helpers.Logger.WriteMessage("start collecting", 1)

        Try
            _CollectLogs(Settings.Logfiles.Folder, Settings.Logfiles.LastScrubbedDate)

            For Each LogFile As String In mList_Logs

            Next

        Catch ex As Exception
            Helpers.Logger.WriteError("error", 2)
            Helpers.Logger.WriteError(ex.Message, 3)

            Return False
        End Try


        Return True
    End Function

    Public Shared Function IncrementLastScrubbedDate() As Boolean
        Try
            Dim d As New Date

            d = Date.Parse(Settings.Logfiles.LastScrubbedDate)
            d = d.AddDays(1)

            Settings.Logfiles.LastScrubbedDate = d.ToString("yyyy-MM-dd")

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function ScrubLine(line As String) As String
        Select Case Settings.Generic.ScrubbingMethod
            Case Settings.Generic.ScrubbingMethods.Scrub
                Return Regex.Replace(line, rePattern_Navigation01, "$1$2$3" & Settings.Logfiles.Suffix & "$4$5$6/***")
            Case Settings.Generic.ScrubbingMethods.Obfuscate
                Return Regex.Replace(line, rePattern_Navigation01, "$1$2$3" & Settings.Logfiles.Suffix & "$4") & Helpers.XOrObfuscation_v2.Obfuscate(Regex.Replace(line, rePattern_Navigation01, "$5$6$7"))
            Case Else
                Return line
        End Select
    End Function

    Private Shared Sub _CollectLogs(dir As String, year As Integer, month As Integer, day As Integer)
        Dim d As New Date(year, month, day)

        _CollectLogs(dir, d)
    End Sub

    Private Shared Sub _CollectLogs(dir As String, date_as_string As String)
        Dim d As New Date

        Try
            d = Date.Parse(date_as_string)
        Catch ex As Exception
            d = Date.Parse(Settings.DEFAULT_DATE)
        End Try

        _CollectLogs(dir, d)
    End Sub


    Private Shared Sub _CollectLogs(dir As String, d As Date)
        Dim files() As String
        Dim tmpfile As String, tmpfile_arr() As String
        Dim nd As Date = Date.Now


        mList_Logs = New List(Of String)

        files = Directory.GetFiles(dir).Where(Function(path) Regex.Match(IO.Path.GetFileName(path), rePattern_file).Success).ToArray()

        If files.Count = 1 Then
            Helpers.Logger.WriteRelative("found 1 possible file", 1)
        Else
            Helpers.Logger.WriteRelative("found " & files.Count.ToString & " possible files", 1)
        End If


        For i As Integer = 0 To files.Count - 1
            Try
                tmpfile = IO.Path.GetFileName(files(i))
                tmpfile = tmpfile.Replace(".txt", "")
                tmpfile_arr = tmpfile.Split("-")

                If CInt(tmpfile_arr(0)) >= d.Year And CInt(tmpfile_arr(1)) >= d.Month And CInt(tmpfile_arr(2)) >= d.Day Then
                    If CInt(tmpfile_arr(0)) = nd.Year And CInt(tmpfile_arr(1)) = nd.Month And CInt(tmpfile_arr(2)) = nd.Day Then
                        mTodaysLog = IO.Path.GetFileName(files(i))
                    Else
                        mList_Logs.Add(IO.Path.GetFileName(files(i)))
                    End If
                End If
            Catch ex As Exception
                'oopsie
            End Try
        Next

        If mList_Logs.Count = 1 Then
            Helpers.Logger.WriteRelative("found 1 possible file", 1)
        Else
            Helpers.Logger.WriteRelative("found " & files.Count.ToString & " possible files", 1)
        End If
    End Sub

    Private Shared Sub __ScrubLog(sFile As String)
        Try
            Dim lines As New List(Of String)
            Dim linecount As Integer = 0, linefound As Boolean = False, alreadypatched As Boolean = False

            If Not IO.File.Exists(sFile) Then
                Throw New Exception("'" & sFile & "' not found!")
            End If

            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine


                    lines.Add(line)

                End While
            End Using

            Helpers.Logger.WriteMessageRelative("writing", 2)
            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using
        Catch ex As Exception
            Helpers.Logger.WriteError("error", 3)
            Helpers.Logger.WriteError(ex.Message, 4)
        End Try

    End Sub

End Class
