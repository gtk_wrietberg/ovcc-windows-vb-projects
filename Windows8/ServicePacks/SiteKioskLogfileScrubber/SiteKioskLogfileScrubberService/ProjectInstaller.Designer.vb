﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SiteKioskLogfileScrubberServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.SiteKioskLogfileScrubberServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'SiteKioskLogfileScrubberServiceProcessInstaller
        '
        Me.SiteKioskLogfileScrubberServiceProcessInstaller.Password = Nothing
        Me.SiteKioskLogfileScrubberServiceProcessInstaller.Username = Nothing
        '
        'SiteKioskLogfileScrubberServiceInstaller
        '
        Me.SiteKioskLogfileScrubberServiceInstaller.ServiceName = "SiteKioskLogfileScrubberService"
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.SiteKioskLogfileScrubberServiceProcessInstaller, Me.SiteKioskLogfileScrubberServiceInstaller})

    End Sub

    Friend WithEvents SiteKioskLogfileScrubberServiceProcessInstaller As ServiceProcess.ServiceProcessInstaller
    Friend WithEvents SiteKioskLogfileScrubberServiceInstaller As ServiceProcess.ServiceInstaller
End Class
