﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        LogfileScrubber.ScrubLogs()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MsgBox(LogfileScrubber.ScrubLine(TextBox1.Text))
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Settings.Load()

        'MsgBox(LogfileScrubber.Setting_Suffix)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Settings.Generic.Debugging = True
        Settings.Generic.ScrubbingMethod = Settings.Generic.ScrubbingMethods.Scrub
        Settings.Logfiles.Folder = "C:\Program Files (x86)\SiteKiosk\logfiles\__TEST"
        Settings.Logfiles.LastScrubbedDate = "2001-01-01"
        Settings.Logfiles.Suffix = "_xxx_"

        Settings.Save()
    End Sub
End Class
