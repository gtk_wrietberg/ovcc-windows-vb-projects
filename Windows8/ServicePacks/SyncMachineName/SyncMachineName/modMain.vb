﻿Module modMain
    Sub Main()
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)

        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


        Dim sMachineName As String = Environment.MachineName
        Dim sMachineNameFromScript As String = ""
        Dim sScriptFile As String = IO.Path.Combine(Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", ""), "Skins\default\Scripts\guest-tek.js")

        Helpers.Logger.WriteMessage("looking for script file", 0)
        Helpers.Logger.WriteMessage(sScriptFile, 1)

        If Not IO.File.Exists(sScriptFile) Then
            Helpers.Logger.WriteError("NOT FOUND!", 2)
            ExitCode.SetValue(ExitCode.ExitCodes.SCRIPT_NOT_FOUND)
            ExitApplication()
            Exit Sub
        End If

        Helpers.Logger.WriteMessage("ok", 2)


        Using sr As New IO.StreamReader(sScriptFile)
            Dim line As String

            While Not sr.EndOfStream
                line = sr.ReadLine

                If line.Contains("var LocationID=") Then
                    line = line.Replace("var LocationID=", "")
                    line = line.Replace("""", "")
                    line = line.Replace(";", "")

                    sMachineNameFromScript = line

                    Exit While
                End If
            End While
        End Using


        Helpers.Logger.WriteMessage("looking for machine name", 0)
        Helpers.Logger.WriteMessage("from system", 1)
        Helpers.Logger.WriteMessage(sMachineName, 2)
        Helpers.Logger.WriteMessage("from script", 1)
        Helpers.Logger.WriteMessage(sMachineNameFromScript, 2)


        If sMachineName.Equals(sMachineNameFromScript) Then
            Helpers.Logger.WriteMessage("no update needed!", 0)

            ExitApplication()
            Exit Sub
        End If


        Helpers.Logger.WriteMessage("updating script file", 0)

        Dim lines As New List(Of String)

        Try
            Dim sBackupFolder As String = ""

            sBackupFolder = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_backups"
            sBackupFolder &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
            sBackupFolder &= "\" & Now().ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            If Not IO.Directory.Exists(sBackupFolder) Then
                IO.Directory.CreateDirectory(sBackupFolder)
            End If

            Helpers.Logger.WriteMessage("making a backup", 1)
            Helpers.FilesAndFolders.Backup.File(sScriptFile, sBackupFolder, 1)


            Helpers.Logger.WriteMessage("making changes", 1)
            Using sr As New IO.StreamReader(sScriptFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var LocationID=") Then
                        line = "var LocationID=""" & sMachineName & """;"
                    End If

                    lines.Add(line)
                End While
            End Using
            Helpers.Logger.WriteMessage("ok", 2)

            Helpers.Logger.WriteMessage("writing script file", 1)
            Using sw As New IO.StreamWriter(sScriptFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using
            Helpers.Logger.WriteMessage("ok", 2)
        Catch ex As Exception
            Helpers.Logger.WriteError("FAIL", 1)
            Helpers.Logger.WriteError(ex.Message, 2)

            ExitCode.SetValue(ExitCode.ExitCodes.UPDATE_FAILED)
            ExitApplication()
            Exit Sub
        End Try


        Helpers.Logger.WriteMessage("checking", 0)

        sMachineNameFromScript = ""
        Using sr As New IO.StreamReader(sScriptFile)
            Dim line As String

            While Not sr.EndOfStream
                line = sr.ReadLine

                If line.Contains("var LocationID=") Then
                    line = line.Replace("var LocationID=", "")
                    line = line.Replace("""", "")
                    line = line.Replace(";", "")

                    sMachineNameFromScript = line

                    Exit While
                End If
            End While
        End Using

        Helpers.Logger.WriteMessage("from system", 1)
        Helpers.Logger.WriteMessage(sMachineName, 2)
        Helpers.Logger.WriteMessage("from script", 1)
        Helpers.Logger.WriteMessage(sMachineNameFromScript, 2)

        If Not sMachineName.Equals(sMachineNameFromScript) Then
            Helpers.Logger.WriteError("values not matching?!?!", 1)

            ExitCode.SetValue(ExitCode.ExitCodes.VALUES_NOT_MATCHING_AFTER_UPDATE)
            ExitApplication()
            Exit Sub
        End If

        Helpers.Logger.WriteMessage("values are now matching", 1)


        ExitApplication()
    End Sub

    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteEmptyLine()
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
