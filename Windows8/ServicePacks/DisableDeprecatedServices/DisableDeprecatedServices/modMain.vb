﻿Module modMain
    Private mServiceList As New List(Of String)

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.WriteMessage(New String("*", 50))
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)


        mServiceList.Add("Form1")
        mServiceList.Add("AeXNSClient")
        mServiceList.Add("OVCCAgent_Heartbeat")
        mServiceList.Add("OVCCAgent_Logfile")
        mServiceList.Add("LobbyPCHeartbeat")
        mServiceList.Add("LobbyPCLogfileUploader")


        '---------------------------
        Dim _ret As String = "", _starttype As ServiceBootFlag

        Helpers.Logger.WriteMessage("disabling services", 0)
        For Each service_name As String In mServiceList
            Helpers.Logger.WriteMessage(service_name, 1)

            If Not ServiceInstaller.ServiceIsInstalled(service_name) Then
                Helpers.Logger.WriteWarning("service not installed", 2)

                Continue For
            End If


            Helpers.Logger.WriteMessage("current start type", 3)
            _ret = ServiceInstaller.GetStartupType(service_name, _starttype)
            If _ret.Equals("") Then
                'ok
                Helpers.Logger.WriteMessage(_starttype.ToString, 3)

                If _starttype.Equals(ServiceBootFlag.Disabled) Then
                    'ok
                    Helpers.Logger.WriteMessage("ok", 2)
                Else
                    Helpers.Logger.WriteMessage("set start type", 2)
                    Helpers.Logger.WriteMessage(ServiceBootFlag.Disabled.ToString, 3)

                    _ret = ServiceInstaller.SetStartupType(service_name, ServiceBootFlag.Disabled)
                    If _ret.Equals("") Then
                        Helpers.Logger.WriteMessage("ok", 4)
                    Else
                        Helpers.Logger.WriteError("failed", 4)
                        Helpers.Logger.WriteError(_ret, 5)
                    End If
                End If


                Helpers.Logger.WriteMessage("checking start type", 2)
                _ret = ServiceInstaller.GetStartupType(service_name, _starttype)
                If _ret.Equals("") Then
                    'ok
                    Helpers.Logger.WriteMessage(_starttype.ToString, 3)
                Else
                    Helpers.Logger.WriteError("failed", 3)
                    Helpers.Logger.WriteError(_ret, 4)
                End If
            Else
                Helpers.Logger.WriteError("failed", 3)
                Helpers.Logger.WriteError(_ret, 4)
            End If

            Threading.Thread.Sleep(500)
        Next


        Helpers.Logger.WriteError("done", 0)
        Helpers.Logger.WriteError("ok", 1)
        Helpers.Logger.WriteError("bye", 2)
    End Sub
End Module
