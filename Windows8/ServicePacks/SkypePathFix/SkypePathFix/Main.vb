﻿Module Main
    Private mReportMissingSkype As Boolean = False

    Public Sub Main()
        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)

        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("started", , 0)


        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals("--report-missing-skype") Then
                mReportMissingSkype = True
            End If
        Next

        If mReportMissingSkype Then
            Helpers.Logger.WriteMessage("returning exit code when Skype is not installed and not configured", 1)
        Else
            Helpers.Logger.WriteWarning("not returning exit code when Skype is not installed and not configured", 1)
        End If


        Helpers.Logger.WriteMessage("updating OVCC config", 0)

        Dim sSkypePath_new As String = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "Microsoft\Skype for Desktop\Skype.exe")


        Try
            Helpers.Logger.WriteMessage("loading SK config file", 1)

            Dim sSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()
            Dim bConfigChanged As Boolean = False

            Helpers.Logger.WriteMessage(sSkCfgFile, 2)


            Dim nt As System.Xml.XmlNameTable
            Dim ns As System.Xml.XmlNamespaceManager
            Dim xmlSkCfg As System.Xml.XmlDocument
            Dim xmlNode_Root As System.Xml.XmlNode

            xmlSkCfg = New System.Xml.XmlDocument

            xmlSkCfg.Load(sSkCfgFile)
            Helpers.Logger.WriteMessage("ok", 3)

            nt = xmlSkCfg.NameTable
            ns = New System.Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)


            Dim mXmlNode_Programs As Xml.XmlNode
            Dim mXmlNode_Files As Xml.XmlNodeList
            Dim mXmlNode_File As Xml.XmlNode
            Dim sTmpAppName As String
            Dim bSkypeFoundInConfig As Boolean = False

            mXmlNode_Programs = xmlNode_Root.SelectSingleNode("sk:programs", ns)
            mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)

            Helpers.Logger.WriteMessage("checking app list for 'Skype'", 1)
            For Each mXmlNode_File In mXmlNode_Files
                sTmpAppName = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower

                If sTmpAppName.Contains("skype.exe") Then
                    bSkypeFoundInConfig = True

                    Helpers.Logger.WriteMessage("found", 2)
                    Helpers.Logger.WriteMessage(sTmpAppName, 3)

                    If IO.File.Exists(sTmpAppName) Then
                        Helpers.Logger.WriteMessage("path exists, leaving it alone", 2)
                    Else
                        Helpers.Logger.WriteWarning("path does not exist", 2)
                        Helpers.Logger.WriteWarning("looking for updated path", 2)
                        Helpers.Logger.WriteMessage(sSkypePath_new, 3)

                        If IO.File.Exists(sSkypePath_new) Then
                            Helpers.Logger.WriteMessage("path exists", 4)

                            Helpers.Logger.WriteMessage("updating config", 2)

                            mXmlNode_File.Attributes.GetNamedItem("filename").Value = sSkypePath_new

                            Helpers.Logger.WriteMessage("ok", 3)
                            bConfigChanged = True
                        Else
                            Helpers.Logger.WriteWarning("path does not exist", 4)

                            ExitCode.SetValue(ExitCode.ExitCodes.SKYPE_IN_CONFIG_BUT_NOT_INSTALLED)
                        End If
                    End If
                End If
            Next

            If Not bSkypeFoundInConfig Then
                Helpers.Logger.WriteWarning("Skype not found in config", 2)

                If IO.File.Exists(sSkypePath_new) Then
                    Helpers.Logger.WriteWarning("But Skype is installed!", 3)
                    Helpers.Logger.WriteWarning(sSkypePath_new, 4)

                    If mReportMissingSkype Then
                        ExitCode.SetValue(ExitCode.ExitCodes.SKYPE_INSTALLED_BUT_NOT_IN_CONFIG)
                    End If
                Else
                    If mReportMissingSkype Then
                        ExitCode.SetValue(ExitCode.ExitCodes.SKYPE_NOT_INSTALLED)
                    End If
                End If


            End If


            '---------------------------------------------------
            Helpers.Logger.WriteMessage("saving", 1)
            If bConfigChanged Then
                Helpers.Logger.WriteMessage("backup", , 2)
                Helpers.FilesAndFolders.Backup.File(sSkCfgFile, 3, My.Application.Info.ProductName)

                Helpers.Logger.WriteMessage("writing", 2)
                Helpers.Logger.WriteMessage(sSkCfgFile, 3)
                xmlSkCfg.Save(sSkCfgFile)
                Helpers.Logger.WriteMessage("ok", 4)
            Else
                Helpers.Logger.WriteWarning("skipped, nothing was changed", 2)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("error", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            ExitCode.SetValue(ExitCode.ExitCodes.FATAL_ERROR)
        End Try




        '------
        ExitApplication()
    End Sub

    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
