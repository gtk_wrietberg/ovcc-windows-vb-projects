Imports Microsoft.Win32

Public Class Cleanup
    Private Const cDoesNotExistString As String = "(does not exist)"
    Private Const cRegistryStartupLocation1 As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"

    Private mServices As New List(Of Cleanup_Service)
    Private mCountServices As Integer

    Private mStartups As New List(Of Cleanup_Startup)
    Private mStartupsLocations As New List(Of String)
    Private mStartupsKeys As New List(Of String)
    Private mCountStartups As Integer

    Private mLocations As New List(Of Cleanup_Location)
    Private mCountLocations As Integer

    Private mRegistry As New List(Of Cleanup_Registry)
    Private mCountRegistry As Integer

    Private mLocationTotalFileSize As Long
    Private mLocationTotalDeletedFileSize As Long
    Private mLocationTotalDelayedFileSize As Long

    Public Sub New()
        mServices = New List(Of Cleanup_Service)
        mStartups = New List(Of Cleanup_Startup)
        mStartupsKeys = New List(Of String)
        mStartupsLocations = New List(Of String)
        mLocations = New List(Of Cleanup_Location)
        mRegistry = New List(Of Cleanup_Registry)

        mCountServices = 0
        mCountStartups = 0
        mCountLocations = 0

        mLocationTotalFileSize = 0
        mLocationTotalDeletedFileSize = 0
        mLocationTotalDelayedFileSize = 0
    End Sub

#Region "properties"
    Public ReadOnly Property NumberOfServices() As Integer
        Get
            Return mServices.Count
        End Get
    End Property

    Public ReadOnly Property NumberOfStartups() As Integer
        Get
            Return mStartups.Count
        End Get
    End Property

    Public ReadOnly Property NumberOfLocations() As Integer
        Get
            Return mLocations.Count
        End Get
    End Property

    Public ReadOnly Property TotalFileSize() As Long
        Get
            Return mLocationTotalFileSize
        End Get
    End Property

    Public ReadOnly Property TotalDeletedFileSize() As Long
        Get
            Return mLocationTotalDeletedFileSize
        End Get
    End Property

    Public ReadOnly Property TotalDelayedFileSize() As Long
        Get
            Return mLocationTotalDelayedFileSize
        End Get
    End Property
#End Region

#Region "reset counters"
    Public Sub ResetCounters()
        mCountServices = 0
        mCountStartups = 0
        mCountLocations = 0
    End Sub

    Public Sub ResetCounterServices()
        mCountServices = 0
    End Sub

    Public Sub ResetCounterStartups()
        mCountStartups = 0
    End Sub

    Public Sub ResetCounterLocations()
        mCountLocations = 0
    End Sub
#End Region

#Region "setters"
    Public Sub AddService(ByVal sServiceName As String)
        Dim oConfig_Service As New Cleanup_Service

        oConfig_Service.Name = sServiceName

        mServices.Add(oConfig_Service)
    End Sub

    Public Sub AddStartup(ByVal sStartupName As String)
        Dim oConfig_Startup As New Cleanup_Startup

        oConfig_Startup.Name = sStartupName

        mStartups.Add(oConfig_Startup)
    End Sub

    Public Sub AddRegistry(ByVal sKeyName As String, ByVal sValueName As String, ByVal oValue As Object, ByVal oType As RegistryValueKind)
        Dim oConfig_Registry As New Cleanup_Registry

        oConfig_Registry.KeyName = sKeyName
        oConfig_Registry.ValueName = sValueName
        oConfig_Registry.Value = oValue
        oConfig_Registry.Type = oType

        mRegistry.Add(oConfig_Registry)
    End Sub

    Public Sub AddLocation(ByVal sPath As String, ByVal bRecursive As Boolean, ByVal sFileNamePattern As String, ByVal iMaxFileAge As Integer)
        Dim oConfig_Location As New Cleanup_Location

        oConfig_Location.Path = sPath
        oConfig_Location.Recursive = bRecursive
        oConfig_Location.FileNamePattern = sFileNamePattern
        oConfig_Location.MaxFileAge = iMaxFileAge

        mLocations.Add(oConfig_Location)
    End Sub
#End Region

#Region "getters"
    Public Function GetNextService(ByRef sName As String, ByRef bFound As Boolean, ByRef bDisabled As Boolean) As Boolean
        If mCountServices > mServices.Count - 1 Then
            Return False
        End If

        mCountServices += 1

        sName = mServices.Item(mCountServices - 1).Name
        bFound = mServices.Item(mCountServices - 1).Found
        bDisabled = mServices.Item(mCountServices - 1).Disabled

        Return True
    End Function

    Public Function GetNextStartup(ByRef sName As String, ByRef bFound As Boolean) As Boolean
        If mCountStartups > mStartups.Count - 1 Then
            Return False
        End If

        mCountStartups += 1

        sName = mStartups.Item(mCountStartups - 1).Name
        bFound = mStartups.Item(mCountStartups - 1).Found

        Return True
    End Function

    Public Function GetNextLocation(ByRef sPath As String, ByRef iFound As Integer, ByRef iDeleted As Integer, ByRef iDelayed As Integer) As Boolean
        If mCountLocations > mLocations.Count - 1 Then
            Return False
        End If

        mCountLocations += 1

        sPath = mLocations.Item(mCountLocations - 1).Path
        iFound = mLocations.Item(mCountLocations - 1).Found
        iDeleted = mLocations.Item(mCountLocations - 1).Deleted
        iDelayed = mLocations.Item(mCountLocations - 1).Delayed

        Return True
    End Function
#End Region

#Region "cleaners"
#Region "public"
    Public Function CleanUp_Services() As Boolean
        Dim oService As Cleanup_Service
        Dim bRet As Boolean

        bRet = True

        For Each oService In mServices
            bRet = bRet And _DisableWindowsService(oService)
        Next

        Return bRet
    End Function

    Public Function CleanUp_Startups() As Boolean
        mStartupsLocations = New List(Of String)
        mStartupsLocations.Add(cRegistryStartupLocation1)

        If Not _FindWindowsStartupKeys() Then
            Return False
        End If

        If mStartupsKeys.Count > 0 Then
            Return _DisableWindowsStartup()
        Else
            oLogger.WriteToLogRelative("no startup keys found", , 2)

            Return True
        End If
    End Function

    Public Function CleanUp_Locations() As Boolean
        Dim oFileCleanUp As FileCleanup
        Dim oLocation As Cleanup_Location
        Dim bRet As Boolean

        bRet = True

        For Each oLocation In mLocations
            oFileCleanUp = New FileCleanup

            oFileCleanUp.Folder = oLocation.Path
            oFileCleanUp.LoadFiles(oLocation.FileNamePattern, oLocation.MaxFileAge, oLocation.Recursive)

            oLocation.Found = oFileCleanUp.FilesCount
            oLocation.TotalFileSize = oFileCleanUp.FilesTotalSize
            mLocationTotalFileSize += oFileCleanUp.FilesTotalSize

            oFileCleanUp.DeleteFilesAndFolders()

            oLocation.Deleted = oFileCleanUp.FilesDeleted
            oLocation.Delayed = oFileCleanUp.FilesDelayed
            oLocation.TotalDeletedFileSize = oFileCleanUp.FilesDeletedTotalSize
            mLocationTotalDeletedFileSize += oFileCleanUp.FilesDeletedTotalSize
            oLocation.TotalDelayedFileSize = oFileCleanUp.FilesDelayedTotalSize
            mLocationTotalDelayedFileSize += oFileCleanUp.FilesDelayedTotalSize
        Next

        Return bRet
    End Function

    Public Function CleanUp_RegistryKeys() As Boolean
        Dim oRegistry As Cleanup_Registry
        Dim bRet As Boolean

        bRet = True

        For Each oRegistry In mRegistry
            bRet = bRet And _DisableRegistryKeys(oRegistry)
        Next

        Return bRet
    End Function
#End Region

#Region "private"
    Private Function _DisableWindowsService(ByRef oService As Cleanup_Service) As Boolean
        Dim rk_SERVICE As RegistryKey
        oService.Found = False
        oService.Disabled = False

        oLogger.WriteToLogRelative("disabling " & oService.Name, , 1)
        oLogger.WriteToLogRelative("opening registry key", , 2)
        Try
            oLogger.WriteToLogRelative("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\" & oService.Name, , 3)
            rk_SERVICE = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & oService.Name, True)
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 4)
            oLogger.WriteToLogRelative(ex.ToString, , 5)

            Return False
        End Try

        If rk_SERVICE Is Nothing Then
            oLogger.WriteToLogRelative("does not exist", , 4)

            Return True
        End If

        oLogger.WriteToLogRelative("ok", , 4)

        oService.Found = True

        oLogger.WriteToLogRelative("disabling service", , 2)

        Try
            oLogger.WriteToLogRelative("setting 'Start' to " & cServiceDisabledValue.ToString, , 3)
            rk_SERVICE.SetValue("Start", cServiceDisabledValue, RegistryValueKind.DWord)
            oLogger.WriteToLogRelative("ok", , 4)
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 4)
            oLogger.WriteToLogRelative(ex.ToString, , 5)

            Return False
        End Try

        oLogger.WriteToLogRelative("double checking", , 2)

        Dim lCheckValue As Long

        Try
            oLogger.WriteToLogRelative("getting 'Start' value", , 3)
            lCheckValue = Long.Parse(rk_SERVICE.GetValue("Start").ToString)

            If lCheckValue = cServiceDisabledValue Then
                oLogger.WriteToLogRelative("ok, because " & lCheckValue.ToString & " = " & cServiceDisabledValue.ToString, , 4)
            Else
                oLogger.WriteToLogRelative("fail, because " & lCheckValue.ToString & " != " & cServiceDisabledValue.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                Return False
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 4)
            oLogger.WriteToLogRelative(ex.ToString, , 5)

            Return False
        End Try

        oService.Disabled = True

        Return True
    End Function

    Private Function _FindWindowsStartupKeys() As Boolean
        Dim rk_STARTUP As RegistryKey
        Dim oStartup As Cleanup_Startup
        Dim sLocation As String, sValueName As String, sCheckValue As String
        Dim bOk As Boolean

        mStartupsKeys = New List(Of String)

        oLogger.WriteToLogRelative("finding startup keys ", , 1)

        oLogger.WriteToLogRelative("startup location", , 2)

        For Each sLocation In mStartupsLocations
            bOk = True

            Try
                oLogger.WriteToLogRelative("HKEY_LOCAL_MACHINE\" & sLocation, , 3)
                rk_STARTUP = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sLocation, True)

                If rk_STARTUP Is Nothing Then
                    oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                    bOk = False
                End If
            Catch ex As Exception
                oLogger.WriteToLogRelative("fail", , 4)
                oLogger.WriteToLogRelative(ex.ToString, , 5)

                bOk = False
            End Try

            If bOk Then
                oLogger.WriteToLogRelative("searching for startup locations", , 2)
                oLogger.WriteToLogRelative("iterating startups", , 3)
                For Each oStartup In mStartups
                    Try
                        oLogger.WriteToLogRelative("iterating subkeys", , 4)
                        For Each sValueName In rk_STARTUP.GetValueNames
                            Try
                                sCheckValue = rk_STARTUP.GetValue(sValueName, cDoesNotExistString).ToString

                                If InStr(sValueName.ToLower, oStartup.Name.ToLower, CompareMethod.Text) > 0 Or _
                                   InStr(sCheckValue.ToLower, oStartup.Name.ToLower, CompareMethod.Text) > 0 Then
                                    oStartup.Found = True
                                    mStartupsKeys.Add(sLocation & "!" & sValueName)
                                    oLogger.WriteToLogRelative("found: " & sLocation & "!" & sValueName, , 5)
                                Else
                                    oLogger.WriteToLogRelative("this is not the subkey you're looking for.", , 5)
                                End If
                            Catch ex As Exception
                                oLogger.WriteToLogRelative("fail", , 5)
                                oLogger.WriteToLogRelative(ex.ToString, , 6)
                            End Try
                        Next
                    Catch ex As Exception
                        oLogger.WriteToLogRelative("fail", , 4)
                        oLogger.WriteToLogRelative(ex.ToString, , 5)
                    End Try
                Next
            End If
        Next

        Return True
    End Function

    Private Function _DisableWindowsStartup() As Boolean
        Dim rk_STARTUP As RegistryKey
        Dim sFullKey As String, sTmp() As String, sKey As String, sValue As String

        oLogger.WriteToLogRelative("removing found startup keys", , 1)

        For Each sFullKey In mStartupsKeys
            oLogger.WriteToLogRelative("key: " & sFullKey, , 2)

            sTmp = Split(sFullKey, "!", 2)
            If sTmp.Length < 2 Then
                oLogger.WriteToLogRelative("invalid key", Logger.MESSAGE_TYPE.LOG_WARNING, 3)
            Else
                sKey = sTmp(0)
                sValue = sTmp(1)

                oLogger.WriteToLogRelative("deleting", , 3)
                Try
                    rk_STARTUP = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sKey, True)

                    If rk_STARTUP Is Nothing Then
                        oLogger.WriteToLogRelative("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                    Else
                        Try
                            rk_STARTUP.DeleteValue(sValue, True)
                            oLogger.WriteToLogRelative("ok", , 4)
                        Catch ex As Exception
                            oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                            oLogger.WriteToLogRelative(ex.ToString, , 5)
                        End Try
                    End If
                Catch ex As Exception
                    oLogger.WriteToLogRelative("fail", , 4)
                    oLogger.WriteToLogRelative(ex.ToString, , 5)

                    Return False
                End Try
            End If
        Next

        Return True
    End Function

    Private Function _DisableRegistryKeys(ByVal oRegistry As Cleanup_Registry) As Boolean
        oLogger.WriteToLogRelative("changing " & oRegistry.KeyName & "!" & oRegistry.ValueName, , 1)

        Try
            If oRegistry.Type = RegistryValueKind.MultiString Then
                Registry.SetValue(oRegistry.KeyName, oRegistry.ValueName, oRegistry.Value.Split(";"), oRegistry.Type)
            Else
                Registry.SetValue(oRegistry.KeyName, oRegistry.ValueName, oRegistry.Value, oRegistry.Type)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try
    End Function
#End Region
#End Region

#Region "private classes"
    Private Class Cleanup_Service
        Private mName As String
        Private mFound As Boolean
        Private mDisabled As Boolean

        Public Sub New()
            mName = ""
            mFound = False
            mDisabled = False
        End Sub

        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Public Property Found() As Boolean
            Get
                Return mFound
            End Get
            Set(ByVal value As Boolean)
                mFound = value
            End Set
        End Property

        Public Property Disabled() As Boolean
            Get
                Return mDisabled
            End Get
            Set(ByVal value As Boolean)
                mDisabled = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Startup
        Private mName As String
        Private mFound As Boolean

        Public Sub New()
            mName = ""
        End Sub

        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Public Property Found() As Boolean
            Get
                Return mFound
            End Get
            Set(ByVal value As Boolean)
                mFound = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Location
        Private mPath As String
        Private mRecursive As Boolean
        Private mFileNamePattern As String
        Private mMaxFileAge As Integer

        Private mFound As Integer
        Private mDeleted As Integer
        Private mDelayed As Integer

        Private mTotalFileSize As Long
        Private mTotalDeletedFileSize As Long
        Private mTotalDelayedFileSize As Long

        Public Property Path() As String
            Get
                Return mPath
            End Get
            Set(ByVal value As String)
                mPath = value
            End Set
        End Property

        Public Property Recursive() As Boolean
            Get
                Return mRecursive
            End Get
            Set(ByVal value As Boolean)
                mRecursive = value
            End Set
        End Property

        Public Property FileNamePattern() As String
            Get
                Return mFileNamePattern
            End Get
            Set(ByVal value As String)
                mFileNamePattern = value
            End Set
        End Property

        Public Property MaxFileAge() As Integer
            Get
                Return mMaxFileAge
            End Get
            Set(ByVal value As Integer)
                mMaxFileAge = value
            End Set
        End Property

        Public Property Found() As Integer
            Get
                Return mFound
            End Get
            Set(ByVal value As Integer)
                mFound = value
            End Set
        End Property

        Public Property Deleted() As Integer
            Get
                Return mDeleted
            End Get
            Set(ByVal value As Integer)
                mDeleted = value
            End Set
        End Property

        Public Property Delayed() As Integer
            Get
                Return mDelayed
            End Get
            Set(ByVal value As Integer)
                mDelayed = value
            End Set
        End Property

        Public Property TotalFileSize() As Long
            Get
                Return mTotalFileSize
            End Get
            Set(ByVal value As Long)
                mTotalFileSize = value
            End Set
        End Property

        Public Property TotalDeletedFileSize() As Long
            Get
                Return mTotalDeletedFileSize
            End Get
            Set(ByVal value As Long)
                mTotalDeletedFileSize = value
            End Set
        End Property

        Public Property TotalDelayedFileSize() As Long
            Get
                Return mTotalDelayedFileSize
            End Get
            Set(ByVal value As Long)
                mTotalDelayedFileSize = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Registry
        Private mKeyName As String
        Private mValueName As String
        Private mValue As String

        Private mType As RegistryValueKind

        Public Sub New()

        End Sub

        Public Property KeyName() As String
            Get
                Return mKeyName
            End Get
            Set(ByVal value As String)
                mKeyName = value
            End Set
        End Property

        Public Property ValueName() As String
            Get
                Return mValueName
            End Get
            Set(ByVal value As String)
                mValueName = value
            End Set
        End Property

        Public Property Value() As String
            Get
                Return mValue
            End Get
            Set(ByVal _value As String)
                mValue = _value
            End Set
        End Property

        Public Property Type() As RegistryValueKind
            Get
                Return mType
            End Get
            Set(ByVal value As RegistryValueKind)
                mType = value
            End Set
        End Property
    End Class
#End Region
End Class
