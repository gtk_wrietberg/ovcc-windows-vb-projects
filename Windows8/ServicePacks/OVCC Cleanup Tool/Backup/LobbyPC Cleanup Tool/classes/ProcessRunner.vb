Imports System.Diagnostics
Imports System.Threading

Public Class ProcessRunner
    Private WithEvents myProcess As New System.Diagnostics.Process
    Private elapsedTime As Double
    Private eventHandled As Boolean


    Private Const cSleepAmount As Double = 0.5#

    Private Const OneSec As Double = 1.0# / (1440.0# * 60.0#)

    Private mMaxTimeout As Double = 30
    Private mProcessStyle As ProcessWindowStyle

    Private mFileName As String
    Private mArguments As String
    Private mUserName As String
    Private mPassWord As String
    Private mExternalAppToWaitFor As String

    Private mIsProcessDone As Boolean

    Public Event Started(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Failed(ByVal EventProcess As Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String)
    Public Event KillFailed(ByVal EventProcess As Process, ByVal ErrorMessage As String)
    Public Event TimeOut(ByVal EventProcess As Process, ByVal TimeElapsed As Double)
    Public Event Done(ByVal EventProcess As Process, ByVal TimeElapsed As Double)

    Public Sub New()
        mMaxTimeout = 30000
        mProcessStyle = ProcessWindowStyle.Normal

        mFileName = ""
        mArguments = ""
        mUserName = ""
        mPassWord = ""
        mExternalAppToWaitFor = ""

        mIsProcessDone = False
    End Sub

#Region "Properties"
    Public Property MaxTimeout() As Double
        Get
            Return mMaxTimeout
        End Get
        Set(ByVal value As Double)
            mMaxTimeout = value
        End Set
    End Property

    Public Property ProcessStyle() As ProcessWindowStyle
        Get
            Return mProcessStyle
        End Get
        Set(ByVal value As ProcessWindowStyle)
            mProcessStyle = value
        End Set
    End Property

    Public Property FileName() As String
        Get
            Return mFileName
        End Get
        Set(ByVal value As String)
            mFileName = value
        End Set
    End Property

    Public Property Arguments() As String
        Get
            Return mArguments
        End Get
        Set(ByVal value As String)
            mArguments = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set
    End Property

    Public Property PassWord() As String
        Get
            Return mPassWord
        End Get
        Set(ByVal value As String)
            mPassWord = value
        End Set
    End Property

    Public Property ExternalAppToWaitFor() As String
        Get
            Return mExternalAppToWaitFor
        End Get
        Set(ByVal value As String)
            mExternalAppToWaitFor = value
        End Set
    End Property

    Public ReadOnly Property IsProcessDone() As Boolean
        Get
            Return mIsProcessDone
        End Get
    End Property
#End Region

    Public Sub StartProcess()
        StartProcess(mFileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String)
        StartProcess(FileName, mArguments)
    End Sub

    Public Sub StartProcess(ByVal FileName As String, ByVal Arguments As String)
        elapsedTime = 0.0#
        eventHandled = False

        Try
            myProcess.StartInfo.FileName = FileName
            myProcess.StartInfo.Arguments = Arguments
            If mUserName <> "" Then
                Dim securePass As New Security.SecureString()

                For Each c As Char In mPassWord
                    securePass.AppendChar(c)
                Next c

                myProcess.StartInfo.UseShellExecute = False
                myProcess.StartInfo.UserName = mUserName
                myProcess.StartInfo.Password = securePass
            End If
            myProcess.StartInfo.WindowStyle = mProcessStyle
            myProcess.EnableRaisingEvents = True
            myProcess.Start()

            RaiseEvent Started(myProcess, elapsedTime)
        Catch ex As Exception
            RaiseEvent Failed(myProcess, elapsedTime, ex.Message)

            Exit Sub
        End Try

        Do While _TimeoutLoop()
            elapsedTime += cSleepAmount

            If elapsedTime > mMaxTimeout Then
                RaiseEvent TimeOut(myProcess, elapsedTime)
                StopProcess()
                Exit Do
            End If

            Thread.Sleep(500)
        Loop

        RaiseEvent Done(myProcess, elapsedTime)
    End Sub

    Private Function _TimeoutLoop() As Boolean
        Dim bLoop As Boolean

        bLoop = Not eventHandled And Not myProcess.HasExited

        If mExternalAppToWaitFor <> "" Then
            If Process.GetProcessesByName(mExternalAppToWaitFor).Length > 0 Then
                bLoop = True
            End If
        End If

        Return bLoop
    End Function

    Private Function _IsPidRunning(ByVal iProcessId As Integer) As Boolean
        Dim p As Process

        For Each p In Process.GetProcesses
            If p.Id = iProcessId Then
                Return True
            End If
        Next

        Return False
    End Function

    Public Sub StopProcess()
        Try
            myProcess.Kill()
        Catch ex As Exception
            RaiseEvent KillFailed(myProcess, ex.Message)
        End Try
    End Sub

    Public Sub ProcessDone()
        eventHandled = True

        Try
            myProcess.Close()
        Catch ex As Exception

        End Try

        mIsProcessDone = True
    End Sub
End Class
