Imports Microsoft.Win32

Public Class Cleanup
    Private Const cDoesNotExistString As String = "(does not exist)"
    Private Const cRegistryStartupLocation1 As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"

    Private mServices As New List(Of Cleanup_Service)
    Private mCountServices As Integer

    Private mStartups As New List(Of Cleanup_Startup)
    Private mStartupsLocations As New List(Of String)
    Private mStartupsKeys As New List(Of String)
    Private mCountStartups As Integer

    Private mLocations As New List(Of Cleanup_Location)
    Private mCountLocations As Integer

    Private mRegistry As New List(Of Cleanup_Registry)
    Private mCountRegistry As Integer

    Private mLocationTotalFileSize As Long
    Private mLocationTotalDeletedFileSize As Long
    Private mLocationTotalDelayedFileSize As Long

    Public Sub New()
        mServices = New List(Of Cleanup_Service)
        mStartups = New List(Of Cleanup_Startup)
        mStartupsKeys = New List(Of String)
        mStartupsLocations = New List(Of String)
        mLocations = New List(Of Cleanup_Location)
        mRegistry = New List(Of Cleanup_Registry)

        mCountServices = 0
        mCountStartups = 0
        mCountLocations = 0

        mLocationTotalFileSize = 0
        mLocationTotalDeletedFileSize = 0
        mLocationTotalDelayedFileSize = 0
    End Sub

#Region "properties"
    Public ReadOnly Property NumberOfServices() As Integer
        Get
            Return mServices.Count
        End Get
    End Property

    Public ReadOnly Property NumberOfStartups() As Integer
        Get
            Return mStartups.Count
        End Get
    End Property

    Public ReadOnly Property NumberOfLocations() As Integer
        Get
            Return mLocations.Count
        End Get
    End Property

    Public ReadOnly Property TotalFileSize() As Long
        Get
            Return mLocationTotalFileSize
        End Get
    End Property

    Public ReadOnly Property TotalDeletedFileSize() As Long
        Get
            Return mLocationTotalDeletedFileSize
        End Get
    End Property

    Public ReadOnly Property TotalDelayedFileSize() As Long
        Get
            Return mLocationTotalDelayedFileSize
        End Get
    End Property
#End Region

#Region "reset counters"
    Public Sub ResetCounters()
        mCountServices = 0
        mCountStartups = 0
        mCountLocations = 0
    End Sub

    Public Sub ResetCounterServices()
        mCountServices = 0
    End Sub

    Public Sub ResetCounterStartups()
        mCountStartups = 0
    End Sub

    Public Sub ResetCounterLocations()
        mCountLocations = 0
    End Sub
#End Region

#Region "setters"
    Public Sub AddService(ByVal sServiceName As String)
        Dim oConfig_Service As New Cleanup_Service

        oConfig_Service.Name = sServiceName

        mServices.Add(oConfig_Service)
    End Sub

    Public Sub AddStartup(ByVal sStartupName As String)
        Dim oConfig_Startup As New Cleanup_Startup

        oConfig_Startup.Name = sStartupName

        mStartups.Add(oConfig_Startup)
    End Sub

    Public Sub AddRegistry(ByVal sKeyName As String, ByVal sValueName As String, ByVal oValue As Object, ByVal oType As RegistryValueKind)
        Dim oConfig_Registry As New Cleanup_Registry

        oConfig_Registry.KeyName = sKeyName
        oConfig_Registry.ValueName = sValueName
        oConfig_Registry.Value = oValue
        oConfig_Registry.Type = oType

        mRegistry.Add(oConfig_Registry)
    End Sub

    Public Sub AddLocation(ByVal sPath As String, ByVal bRecursive As Boolean, ByVal sFileNamePattern As String, ByVal iMaxFileAge As Integer)
        Dim oConfig_Location As New Cleanup_Location

        oConfig_Location.Path = sPath
        oConfig_Location.Recursive = bRecursive
        oConfig_Location.FileNamePattern = sFileNamePattern
        oConfig_Location.MaxFileAge = iMaxFileAge

        mLocations.Add(oConfig_Location)
    End Sub
#End Region

#Region "getters"
    Public Function GetNextService(ByRef sName As String, ByRef bFound As Boolean, ByRef bDisabled As Boolean) As Boolean
        If mCountServices > mServices.Count - 1 Then
            Return False
        End If

        mCountServices += 1

        sName = mServices.Item(mCountServices - 1).Name
        bFound = mServices.Item(mCountServices - 1).Found
        bDisabled = mServices.Item(mCountServices - 1).Disabled

        Return True
    End Function

    Public Function GetNextStartup(ByRef sName As String, ByRef bFound As Boolean) As Boolean
        If mCountStartups > mStartups.Count - 1 Then
            Return False
        End If

        mCountStartups += 1

        sName = mStartups.Item(mCountStartups - 1).Name
        bFound = mStartups.Item(mCountStartups - 1).Found

        Return True
    End Function

    Public Function GetNextLocation(ByRef sPath As String, ByRef iFound As Integer, ByRef iDeleted As Integer, ByRef iDelayed As Integer) As Boolean
        If mCountLocations > mLocations.Count - 1 Then
            Return False
        End If

        mCountLocations += 1

        sPath = mLocations.Item(mCountLocations - 1).Path
        iFound = mLocations.Item(mCountLocations - 1).Found
        iDeleted = mLocations.Item(mCountLocations - 1).Deleted
        iDelayed = mLocations.Item(mCountLocations - 1).Delayed

        Return True
    End Function
#End Region

#Region "cleaners"
#Region "public"
    Public Function CleanUp_Services() As Integer
        Dim oService As Cleanup_Service
        Dim iRet As Integer = 0

        For Each oService In mServices
            If _DisableWindowsService(oService) Then
                iRet += 1
            End If
        Next

        Return iRet
    End Function

    Public Function CleanUp_Startups() As Boolean
        mStartupsLocations = New List(Of String)
        mStartupsLocations.Add(cRegistryStartupLocation1)

        If Not _FindWindowsStartupKeys() Then
            Return False
        End If

        If mStartupsKeys.Count > 0 Then
            Return _DisableWindowsStartup()
        Else


            Return True
        End If
    End Function

    Public Function CleanUp_Locations() As Boolean
        Dim oFileCleanUp As FileCleanup
        Dim oLocation As Cleanup_Location
        Dim sPath As String = "", sUsername As String = "", sProfileDir As String = "", sRegex As String = "%%(.+)%%.*"
        Dim bRet As Boolean

        bRet = True

        For Each oLocation In mLocations
            oFileCleanUp = New FileCleanup

            sPath = oLocation.Path
            If sPath.StartsWith("%%") Then 'profile path?
                sUsername = Text.RegularExpressions.Regex.Replace(sPath, sRegex, "$1")
                If Helpers.WindowsUser.GetProfileDirectory(sUsername, sProfileDir) Then
                    sPath = sPath.Replace("%%" & sUsername & "%%", sProfileDir)
                Else
                    sPath = sPath.Replace("%%" & sUsername & "%%", "C:\Users\" & sUsername)
                End If
                sPath = sPath.Replace("//", "/")
            End If

            oFileCleanUp.Folder = sPath
            oFileCleanUp.LoadFiles(oLocation.FileNamePattern, oLocation.MaxFileAge, oLocation.Recursive)

            oLocation.Found = oFileCleanUp.FilesCount
            oLocation.TotalFileSize = oFileCleanUp.FilesTotalSize
            mLocationTotalFileSize += oFileCleanUp.FilesTotalSize

            oFileCleanUp.DeleteFilesAndFolders()

            oLocation.Deleted = oFileCleanUp.FilesDeleted
            oLocation.Delayed = oFileCleanUp.FilesDelayed
            oLocation.TotalDeletedFileSize = oFileCleanUp.FilesDeletedTotalSize
            mLocationTotalDeletedFileSize += oFileCleanUp.FilesDeletedTotalSize
            oLocation.TotalDelayedFileSize = oFileCleanUp.FilesDelayedTotalSize
            mLocationTotalDelayedFileSize += oFileCleanUp.FilesDelayedTotalSize
        Next

        Return bRet
    End Function

    Public Function CleanUp_RegistryKeys() As Boolean
        Dim oRegistry As Cleanup_Registry
        Dim bRet As Boolean

        bRet = True

        For Each oRegistry In mRegistry
            Helpers.Logger.WriteMessageRelative("", 1)
            bRet = bRet And _DisableRegistryKeys(oRegistry)
        Next

        Return bRet
    End Function
#End Region

#Region "private"
    Private Function _DisableWindowsService(ByRef oService As Cleanup_Service) As Boolean
        Dim rk_SERVICE As RegistryKey
        oService.Found = False
        oService.Disabled = False


        Helpers.Logger.WriteMessageRelative("searching service by service name: " & oService.Name, 1)
        Helpers.Logger.WriteMessageRelative("opening registry key", 2)
        Try
            Helpers.Logger.WriteMessageRelative("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\" & oService.Name, 3)
            rk_SERVICE = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & oService.Name, True)
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("not found", 4)
            Helpers.Logger.WriteErrorRelative(ex.ToString, 5)
        End Try

        If rk_SERVICE Is Nothing Then
            Helpers.Logger.WriteWarningRelative("does not exist", 4)
        Else
            oService.Found = True
        End If

        If Not oService.Found Then
            Helpers.Logger.WriteMessageRelative("searching service by service display name: " & oService.Name, 1)
            Helpers.Logger.WriteMessageRelative("opening registry key", 2)

            Dim rk_SERVICES As RegistryKey
            Dim rk_SERVICE_tmp As RegistryKey
            Try
                Helpers.Logger.WriteMessageRelative("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services", 3)
                rk_SERVICES = Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services", True)

                Dim aServices() As String
                aServices = rk_SERVICES.GetSubKeyNames()

                Dim sDisplayName As String = ""

                For Each sService As String In aServices
                    Try
                        rk_SERVICE_tmp = Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & sService, True)
                        sDisplayName = rk_SERVICE.GetValue("DisplayName")

                        If Not sDisplayName Is Nothing Then
                            If sDisplayName.Equals(oService.Name) Then
                                rk_SERVICE = rk_SERVICE_tmp

                                Exit For
                            End If
                        End If
                    Catch ex As Exception

                    End Try
                Next
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("not found", 4)
                Helpers.Logger.WriteErrorRelative(ex.ToString, 5)
            End Try
        End If

        If rk_SERVICE Is Nothing Then
            Helpers.Logger.WriteWarningRelative("does not exist", 4)
        Else
            oService.Found = True
        End If


        If Not oService.Found Then
            Return False
        End If


        Helpers.Logger.WriteMessageRelative("ok", 4)
        Helpers.Logger.WriteMessageRelative("disabling", 2)

        Try
            Helpers.Logger.WriteMessageRelative("setting 'Startup type' to " & cServiceDisabledValue.ToString, 3)
            rk_SERVICE.SetValue("Start", cServiceDisabledValue, RegistryValueKind.DWord)
            Helpers.Logger.WriteMessageRelative("ok", 4)
        Catch ex As Exception
            Helpers.Logger.WriteWarningRelative("fail", 4)
            Helpers.Logger.WriteWarningRelative(ex.ToString, 5)

            Return False
        End Try

        Helpers.Logger.WriteMessageRelative("checking", 2)

        Dim lCheckValue As Long

        Try
            Helpers.Logger.WriteMessageRelative("getting 'Startup type' value", 3)
            lCheckValue = Long.Parse(rk_SERVICE.GetValue("Start").ToString)

            If lCheckValue = cServiceDisabledValue Then
                Helpers.Logger.WriteMessageRelative("ok, because " & lCheckValue.ToString & " = " & cServiceDisabledValue.ToString, 4)
            Else
                Helpers.Logger.WriteWarningRelative("fail, because " & lCheckValue.ToString & " != " & cServiceDisabledValue.ToString, 4)

                Return False
            End If
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("fail", , 4)
            Helpers.Logger.WriteErrorRelative(ex.ToString, 5)

            Return False
        End Try

        oService.Disabled = True

        Return True
    End Function

    Private Function _FindWindowsStartupKeys() As Boolean
        Dim rk_STARTUP As RegistryKey
        Dim oStartup As Cleanup_Startup
        Dim sLocation As String, sValueName As String, sCheckValue As String
        Dim bOk As Boolean

        mStartupsKeys = New List(Of String)

        Helpers.Logger.WriteMessageRelative("finding startup keys ", 1)

        Helpers.Logger.WriteMessageRelative("registry", 2)

        For Each sLocation In mStartupsLocations
            bOk = True

            Try
                Helpers.Logger.WriteMessageRelative("HKEY_LOCAL_MACHINE\" & sLocation, 3)
                rk_STARTUP = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sLocation, True)

                If rk_STARTUP Is Nothing Then
                    Helpers.Logger.WriteMessageRelative("does not exist", 4)

                    bOk = False
                End If
            Catch ex As Exception
                Helpers.Logger.WriteErrorRelative("fail", 4)
                Helpers.Logger.WriteErrorRelative(ex.ToString, 5)

                bOk = False
            End Try

            If bOk Then
                Helpers.Logger.WriteMessageRelative("searching for startup locations", 2)
                Helpers.Logger.WriteMessageRelative("iterating startups", 3)
                For Each oStartup In mStartups
                    Try
                        Helpers.Logger.WriteMessageRelative("iterating subkeys", 4)
                        For Each sValueName In rk_STARTUP.GetValueNames
                            Try
                                sCheckValue = rk_STARTUP.GetValue(sValueName, cDoesNotExistString).ToString

                                If InStr(sValueName.ToLower, oStartup.Name.ToLower, CompareMethod.Text) > 0 Then
                                    oStartup.Found = True
                                    mStartupsKeys.Add(sLocation & "!" & sValueName)
                                    Helpers.Logger.WriteMessageRelative("found (name): " & sLocation & "!" & sValueName, , 5)
                                ElseIf InStr(sCheckValue.ToLower, oStartup.Name.ToLower, CompareMethod.Text) > 0 Then
                                    oStartup.Found = True
                                    mStartupsKeys.Add(sLocation & "!" & sValueName)
                                    Helpers.Logger.WriteMessageRelative("found (path): " & sLocation & "!" & sValueName, , 5)
                                Else
                                    Helpers.Logger.WriteMessageRelative("this is not the subkey you're looking for.", , 5)
                                End If
                            Catch ex As Exception
                                Helpers.Logger.WriteErrorRelative("fail", , 5)
                                Helpers.Logger.WriteErrorRelative(ex.ToString, , 6)
                            End Try
                        Next
                    Catch ex As Exception
                        Helpers.Logger.WriteErrorRelative("fail", , 4)
                        Helpers.Logger.WriteErrorRelative(ex.ToString, , 5)
                    End Try
                Next
            End If
        Next

        Return True
    End Function

    Private Function _DisableWindowsStartup() As Boolean
        Dim rk_STARTUP As RegistryKey
        Dim sFullKey As String, sTmp() As String, sKey As String, sValue As String

        Helpers.Logger.WriteMessageRelative("removing found startup keys", 1)

        For Each sFullKey In mStartupsKeys
            Helpers.Logger.WriteMessageRelative("key: " & sFullKey, , 2)

            sTmp = Split(sFullKey, "!", 2)
            If sTmp.Length < 2 Then
                Helpers.Logger.WriteWarningRelative("invalid key", 3)
            Else
                sKey = sTmp(0)
                sValue = sTmp(1)

                Helpers.Logger.WriteMessageRelative("deleting", 3)
                Try
                    rk_STARTUP = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sKey, True)

                    If rk_STARTUP Is Nothing Then
                        Helpers.Logger.WriteWarningRelative("not found", 4)
                    Else
                        Try
                            rk_STARTUP.DeleteValue(sValue, True)
                            Helpers.Logger.WriteMessageRelative("ok", 4)
                        Catch ex As Exception
                            Helpers.Logger.WriteWarningRelative("failed", 4)
                            Helpers.Logger.WriteWarningRelative(ex.ToString, 5)
                        End Try
                    End If
                Catch ex As Exception
                    Helpers.Logger.WriteWarningRelative("fail", 4)
                    Helpers.Logger.WriteWarningRelative(ex.ToString, 5)

                    Return False
                End Try
            End If
        Next

        Return True
    End Function

    Private Function _DisableRegistryKeys(ByVal oRegistry As Cleanup_Registry) As Boolean
        Helpers.Logger.WriteMessageRelative("changing " & oRegistry.KeyName & "!" & oRegistry.ValueName, 1)

        Try
            If oRegistry.Type = RegistryValueKind.MultiString Then
                Registry.SetValue(oRegistry.KeyName, oRegistry.ValueName, oRegistry.Value.Split(";"), oRegistry.Type)
            Else
                Registry.SetValue(oRegistry.KeyName, oRegistry.ValueName, oRegistry.Value, oRegistry.Type)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("ERROR", 2)
            Helpers.Logger.WriteErrorRelative("ex.Message=" & ex.Message, 3)
            Helpers.Logger.WriteErrorRelative("ex.Source =" & ex.Source, 3)
        End Try
    End Function
#End Region
#End Region

#Region "private classes"
    Private Class Cleanup_Service
        Private mName As String
        Private mFound As Boolean
        Private mDisabled As Boolean

        Public Sub New()
            mName = ""
            mFound = False
            mDisabled = False
        End Sub

        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Public Property Found() As Boolean
            Get
                Return mFound
            End Get
            Set(ByVal value As Boolean)
                mFound = value
            End Set
        End Property

        Public Property Disabled() As Boolean
            Get
                Return mDisabled
            End Get
            Set(ByVal value As Boolean)
                mDisabled = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Startup
        Private mName As String
        Private mFound As Boolean

        Public Sub New()
            mName = ""
        End Sub

        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Public Property Found() As Boolean
            Get
                Return mFound
            End Get
            Set(ByVal value As Boolean)
                mFound = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Location
        Private mPath As String
        Private mRecursive As Boolean
        Private mFileNamePattern As String
        Private mMaxFileAge As Integer

        Private mFound As Integer
        Private mDeleted As Integer
        Private mDelayed As Integer

        Private mTotalFileSize As Long
        Private mTotalDeletedFileSize As Long
        Private mTotalDelayedFileSize As Long

        Public Property Path() As String
            Get
                Return mPath
            End Get
            Set(ByVal value As String)
                mPath = value
            End Set
        End Property

        Public Property Recursive() As Boolean
            Get
                Return mRecursive
            End Get
            Set(ByVal value As Boolean)
                mRecursive = value
            End Set
        End Property

        Public Property FileNamePattern() As String
            Get
                Return mFileNamePattern
            End Get
            Set(ByVal value As String)
                mFileNamePattern = value
            End Set
        End Property

        Public Property MaxFileAge() As Integer
            Get
                Return mMaxFileAge
            End Get
            Set(ByVal value As Integer)
                mMaxFileAge = value
            End Set
        End Property

        Public Property Found() As Integer
            Get
                Return mFound
            End Get
            Set(ByVal value As Integer)
                mFound = value
            End Set
        End Property

        Public Property Deleted() As Integer
            Get
                Return mDeleted
            End Get
            Set(ByVal value As Integer)
                mDeleted = value
            End Set
        End Property

        Public Property Delayed() As Integer
            Get
                Return mDelayed
            End Get
            Set(ByVal value As Integer)
                mDelayed = value
            End Set
        End Property

        Public Property TotalFileSize() As Long
            Get
                Return mTotalFileSize
            End Get
            Set(ByVal value As Long)
                mTotalFileSize = value
            End Set
        End Property

        Public Property TotalDeletedFileSize() As Long
            Get
                Return mTotalDeletedFileSize
            End Get
            Set(ByVal value As Long)
                mTotalDeletedFileSize = value
            End Set
        End Property

        Public Property TotalDelayedFileSize() As Long
            Get
                Return mTotalDelayedFileSize
            End Get
            Set(ByVal value As Long)
                mTotalDelayedFileSize = value
            End Set
        End Property
    End Class

    Private Class Cleanup_Registry
        Private mKeyName As String
        Private mValueName As String
        Private mValue As String

        Private mType As RegistryValueKind

        Public Sub New()

        End Sub

        Public Property KeyName() As String
            Get
                Return mKeyName
            End Get
            Set(ByVal value As String)
                mKeyName = value
            End Set
        End Property

        Public Property ValueName() As String
            Get
                Return mValueName
            End Get
            Set(ByVal value As String)
                mValueName = value
            End Set
        End Property

        Public Property Value() As String
            Get
                Return mValue
            End Get
            Set(ByVal _value As String)
                mValue = _value
            End Set
        End Property

        Public Property Type() As RegistryValueKind
            Get
                Return mType
            End Get
            Set(ByVal value As RegistryValueKind)
                mType = value
            End Set
        End Property
    End Class
#End Region
End Class
