Public Class Cleanup_Service
    Private mName As String
    Private mFound As Boolean
    Private mDisabled As Boolean

    Public Sub New()
        mName = ""
        mFound = False
        mDisabled = False
    End Sub

    Public Property Name() As String
        Get
            Return mName
        End Get
        Set(ByVal value As String)
            mName = value
        End Set
    End Property

    Public Property Found() As Boolean
        Get
            Return mFound
        End Get
        Set(ByVal value As Boolean)
            mFound = value
        End Set
    End Property

    Public Property Disabled() As Boolean
        Get
            Return mDisabled
        End Get
        Set(ByVal value As Boolean)
            mDisabled = value
        End Set
    End Property
End Class
