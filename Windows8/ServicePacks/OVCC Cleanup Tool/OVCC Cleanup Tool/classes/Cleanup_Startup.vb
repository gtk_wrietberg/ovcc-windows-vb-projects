Public Class Cleanup_Startup
    Private mName As String
    Private mFound As Boolean
    Private mDeleted As Boolean

    Public Sub New()
        mName = ""
    End Sub

    Public Property Name() As String
        Get
            Return mName
        End Get
        Set(ByVal value As String)
            mName = value
        End Set
    End Property

    Public Property Found() As Boolean
        Get
            Return mFound
        End Get
        Set(ByVal value As Boolean)
            mFound = value
        End Set
    End Property

    Public Property Deleted() As Boolean
        Get
            Return mDeleted
        End Get
        Set(ByVal value As Boolean)
            mDeleted = value
        End Set
    End Property
End Class
