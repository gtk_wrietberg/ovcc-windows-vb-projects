Module Delay
    Private Const OneSec As Double = 1.0# / (1440.0# * 60.0#)

    Public Sub Delay(ByVal dblSecs As Double)
        Dim dblWaitTil As Date

        Now.AddSeconds(OneSec)
        dblWaitTil = Now.AddSeconds(OneSec).AddSeconds(dblSecs)

        Do Until Now > dblWaitTil
            Application.DoEvents()
        Loop
    End Sub
End Module
