Module PreventMultipleInstances
    Private Declare Function OpenIcon Lib "user32" (ByVal hwnd As Long) As Long
    Private Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As Long

    Public Sub ActivatePrevInstance(ByVal argStrAppToFind As String)
        If UBound(Diagnostics.Process.GetProcessesByName(Diagnostics.Process.GetCurrentProcess.ProcessName)) = 0 Then
            Exit Sub
        End If

        Dim PrevHndl As Long
        Dim result As Long

        Dim objProcess As New Process
        Dim objProcesses() As Process
        objProcesses = Process.GetProcesses()

        For Each objProcess In objProcesses
            If UCase(objProcess.MainWindowTitle) = UCase(argStrAppToFind) Then
                'MsgBox("Another instance of " & argStrAppToFind & " is already running on this machine. You cannot run TWO instances at a time. Please use the other instance.")
                PrevHndl = objProcess.MainWindowHandle.ToInt32()
                Exit For
            End If
        Next
        If PrevHndl = 0 Then Exit Sub

        result = OpenIcon(PrevHndl)
        result = SetForegroundWindow(PrevHndl)

        End
    End Sub
End Module
