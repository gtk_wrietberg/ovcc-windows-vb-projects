Imports Microsoft.Win32

Module modDisableStartup
    Public Function DisableWindowsStartup(ByRef oStartup As Cleanup_Startup) As Boolean
        Dim rk_STARTUP As RegistryKey

        oStartup.Found = False
        oStartup.Deleted = False

        oLogger.WriteToLogRelative("opening registry key", , 1)
        Try
            oLogger.WriteToLogRelative("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", , 2)
            rk_STARTUP = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
            oLogger.WriteToLogRelative("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 3)
            oLogger.WriteToLogRelative(ex.ToString, , 4)

            Return False
        End Try

        If rk_STARTUP Is Nothing Then
            oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End If


        oLogger.WriteToLogRelative("looking for startup '" & oStartup.Name & "'", , 1)

        Dim sCheckValue As String
        Try
            sCheckValue = rk_STARTUP.GetValue(oStartup.Name, cDoesNotExistString).ToString

            If sCheckValue = cDoesNotExistString Then
                oLogger.WriteToLogRelative("does not exist", Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)

                Return False
            End If

            oStartup.Found = True
            oLogger.WriteToLogRelative("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 2)
            oLogger.WriteToLogRelative(ex.ToString, , 3)

            Return False
        End Try

        oLogger.WriteToLogRelative("removing startup", , 1)
        Try
            rk_STARTUP.DeleteValue(oStartup.Name)

            oLogger.WriteToLogRelative("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 2)
            oLogger.WriteToLogRelative(ex.ToString, , 3)

            Return False
        End Try

        oLogger.WriteToLogRelative("double checking", , 1)
        Try
            sCheckValue = rk_STARTUP.GetValue(oStartup.Name, cDoesNotExistString).ToString

            If sCheckValue = cDoesNotExistString Then
                oLogger.WriteToLogRelative("removed", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

                oStartup.Deleted = True
            Else
                oLogger.WriteToLogRelative("still there", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                Return False
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 2)
            oLogger.WriteToLogRelative(ex.ToString, , 3)

            Return False
        End Try

        Return True
    End Function
End Module
