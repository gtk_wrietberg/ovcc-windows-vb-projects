Imports System.IO
Imports System.Xml

Module modLoadConfig
    Public Function LoadConfiguration() As Boolean
        Try
            Dim xmlDoc As XmlDocument
            Dim xmlNodelist As XmlNodeList
            Dim xmlNodelist2 As XmlNodeList
            Dim xmlNode As XmlNode
            Dim xmlNode2 As XmlNode
            Dim sTmp As String
            Dim sTmpName As String, bTmpRecursive As Boolean, sTmpPattern As String, iTmpMaxAge As Integer
            Dim sKeyName As String, sValueName As String, sValue As String, sType As String, oType As Microsoft.Win32.RegistryValueKind

            xmlDoc = New XmlDocument()
            oCleanup = New Cleanup

            '------------
            Helpers.Logger.WriteMessageRelative("loading config XML file", 1)
            xmlDoc.Load(cConfigFileName)
            Helpers.Logger.WriteMessageRelative("ok", 2)

            '------------
            Helpers.Logger.WriteMessageRelative("loading services", 1)
            xmlNodelist = xmlDoc.SelectNodes("/cleanuptool/config/services/service")
            For Each xmlNode In xmlNodelist
                sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                Helpers.Logger.WriteMessageRelative(sTmpName, 2)
                oCleanup.AddService(sTmpName)
            Next

            '------------
            Helpers.Logger.WriteMessageRelative("loading startup apps", 1)
            xmlNodelist = xmlDoc.SelectNodes("/cleanuptool/config/startups/application")
            For Each xmlNode In xmlNodelist
                sTmpName = xmlNode.Attributes.GetNamedItem("name").Value
                Helpers.Logger.WriteMessageRelative(sTmpName, 2)
                oCleanup.AddStartup(sTmpName)
            Next

            '------------
            Helpers.Logger.WriteMessageRelative("loading files and folders", 1)
            xmlNodelist = xmlDoc.SelectNodes("/cleanuptool/config/files-and-folders/location")
            For Each xmlNode In xmlNodelist
                sTmpName = xmlNode.Attributes.GetNamedItem("path").Value
                Helpers.Logger.WriteMessageRelative(sTmpName, 2)

                Helpers.Logger.WriteMessageRelative("params", 3)

                sTmp = xmlNode.ChildNodes.Item(0).InnerText
                If sTmp = "yes" Then
                    bTmpRecursive = True
                Else
                    bTmpRecursive = False
                End If
                Helpers.Logger.WriteMessageRelative("recursive: " & bTmpRecursive.ToString, 4)

                sTmpPattern = xmlNode.ChildNodes.Item(1).InnerText
                Helpers.Logger.WriteMessageRelative("pattern: " & sTmpPattern, 4)

                sTmp = xmlNode.ChildNodes.Item(2).InnerText
                If IsNumeric(sTmp) Then
                    Try
                        iTmpMaxAge = Integer.Parse(sTmp)
                    Catch ex As Exception
                        iTmpMaxAge = 0
                    End Try
                Else
                    iTmpMaxAge = 0
                End If
                Helpers.Logger.WriteMessageRelative("max age: " & iTmpMaxAge.ToString, 4)

                oCleanup.AddLocation(sTmpName, bTmpRecursive, sTmpPattern, iTmpMaxAge)
            Next

            '------------
            Helpers.Logger.WriteMessageRelative("loading registry keys", 1)
            xmlNodelist = xmlDoc.SelectNodes("/cleanuptool/config/registry/key")
            For Each xmlNode In xmlNodelist
                sKeyName = xmlNode.Attributes.GetNamedItem("name").Value

                xmlNodelist2 = xmlNode.SelectNodes("value")
                For Each xmlNode2 In xmlNodelist2
                    sValueName = xmlNode2.Attributes.GetNamedItem("name").Value
                    sType = xmlNode2.Attributes.GetNamedItem("type").Value
                    sValue = xmlNode2.InnerText

                    Select Case sType.ToLower
                        Case "DWord".ToLower
                            oType = Microsoft.Win32.RegistryValueKind.DWord
                        Case "MultiString".ToLower
                            oType = Microsoft.Win32.RegistryValueKind.MultiString
                        Case Else
                            sType = "String"
                            oType = Microsoft.Win32.RegistryValueKind.String
                    End Select

                    Helpers.Logger.WriteMessageRelative(sKeyName & "!" & sValueName & " = " & sValue & " (" & sType & ")", 2)
                    oCleanup.AddRegistry(sKeyName, sValueName, sValue, oType)
                Next
            Next

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("ERROR", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            Return False
        End Try
    End Function
End Module
