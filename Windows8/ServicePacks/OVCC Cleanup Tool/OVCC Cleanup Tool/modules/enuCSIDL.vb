#Region "[### TO DO ###]"

'### TO DO #####################################################################
'-------------------------------------------------------------------------------
' DATE: 2008-03-12
' WHO: Warrick Procter
' DESCRIPTION:
' o Test.
'-------------------------------------------------------------------------------
'### TO DO #####################################################################

#End Region

#Region "[=== OPTIONS ===]"

Option Strict On
Option Explicit On
Option Compare Binary

#End Region

#Region "[=== IMPORTS ===]"

Imports System.Runtime.InteropServices

#End Region


''' <copyright>
'''###########################################################################
'''## Copyright (c) 2008 Warrick Procter.                                   ##
'''##                                                                       ##
'''## This work is covered by the "Code Project Open License", a copy of    ##
'''## which is enclosed with this package as:                               ##
'''##         "Code Project Open License (CPOL).txt".                       ##
'''##                                                                       ##
'''## No other use is permitted without the express prior written           ##
'''## permission of Warrick Procter.                                        ##
'''## For permission, try these contact addresses (current at the time of   ##
'''## writing):                                                             ##
'''##     procter@xtra.co.nz                                                ##
'''##     The address for service of company "ZED Limited", New Zealand.    ##
'''###########################################################################
''' </copyright>
''' <disclaimer>
'''###########################################################################
'''## REPRESENTATIONS, WARRANTIES AND DISCLAIMER                            ##
'''## ------------------------------------------                            ##
'''## THIS WORK IS PROVIDED "AS IS", "WHERE IS" AND "AS AVAILABLE", WITHOUT ##
'''## ANY EXPRESS OR IMPLIED WARRANTIES OR CONDITIONS OR GUARANTEES. YOU,   ##
'''## THE USER, ASSUME ALL RISK IN ITS USE, INCLUDING COPYRIGHT             ##
'''## INFRINGEMENT, PATENT INFRINGEMENT, SUITABILITY, ETC. AUTHOR EXPRESSLY ##
'''## DISCLAIMS ALL EXPRESS, IMPLIED OR STATUTORY WARRANTIES OR CONDITIONS, ##
'''## INCLUDING WITHOUT LIMITATION, WARRANTIES OR CONDITIONS OF             ##
'''## MERCHANTABILITY, MERCHANTABLE QUALITY OR FITNESS FOR A PARTICULAR     ##
'''## PURPOSE, OR ANY WARRANTY OF TITLE OR NON-INFRINGEMENT, OR THAT THE    ##
'''## WORK (OR ANY PORTION THEREOF) IS CORRECT, USEFUL, BUG-FREE OR FREE OF ##
'''## VIRUSES. YOU MUST PASS THIS DISCLAIMER ON WHENEVER YOU DISTRIBUTE THE ##
'''## WORK OR DERIVATIVE WORKS.                                             ##
'''###########################################################################
''' </disclaimer>
''' <history>
''' 2008-03-12 [Warrick Procter] Created.
''' </history>
''' <summary>
''' enuCSIDL - CSIDL enumeration.
''' CSIDL values provide a unique system-independent way to identify
''' special folders used frequently by applications, but which may
''' not have the same name or location on any given system.
''' </summary>
''' <overview>
''' </overview>
''' <remarks>
''' For example, the system folder may be "C:\Windows" on one system and "C:\Winnt" on another.
''' These constants are defined in Shlobj.h and Shfolder.h.
''' </remarks>
''' <notes>
''' </notes>
Public Enum enuCSIDL As Int32

    ''' <summary>
    ''' The virtual folder representing the Windows desktop, the root of the namespace.
    ''' </summary>
    ''' <remarks></remarks>
    Desktop = kCSIDL_DESKTOP

    ''' <summary>
    ''' A virtual folder representing the Internet.
    ''' </summary>
    ''' <remarks></remarks>
    Internet = kCSIDL_INTERNET

    ''' <summary>
    ''' The file system directory that contains the user's program groups
    ''' (which are themselves file system directories).
    ''' A typical path is C:\Documents and Settings\username\Start Menu\Programs.
    ''' </summary>
    ''' <remarks></remarks>
    Programs = kCSIDL_PROGRAMS

    ''' <summary>
    ''' The virtual folder containing icons for the Control Panel applications.
    ''' </summary>
    ''' <remarks></remarks>
    Controls = kCSIDL_CONTROLS

    ''' <summary>
    ''' The virtual folder containing installed printers.
    ''' </summary>
    ''' <remarks></remarks>
    Printers = kCSIDL_PRINTERS

    ''' <summary>
    ''' Version 6.0. The virtual folder representing the My Documents desktop item.
    ''' This is equivalent to MYDOCUMENTS. 
    ''' Previous to Version 6.0. The file system directory used to physically store
    ''' a user's common repository of documents.
    ''' A typical path is C:\Documents and Settings\username\My Documents.
    ''' This should be distinguished from the virtual My Documents folder in the namespace.
    ''' To access that virtual folder, use SHGetFolderLocation, which returns
    ''' the ITEMIDLIST for the virtual location, or refer to the technique described in
    ''' Managing the File System.
    ''' </summary>
    ''' <remarks></remarks>
    Personal = kCSIDL_PERSONAL

    ''' <summary>
    ''' The file system directory that serves as a common repository for the user's favorite items.
    ''' A typical path is C:\Documents and Settings\username\Favorites.
    ''' </summary>
    ''' <remarks></remarks>
    Favorites = kCSIDL_FAVORITES

    ''' <summary>
    ''' The file system directory that corresponds to the user's Startup program group.
    ''' The system starts these programs whenever any user logs onto Windows NT or starts Windows 95.
    ''' A typical path is C:\Documents and Settings\username\Start Menu\Programs\Startup.
    ''' </summary>
    ''' <remarks></remarks>
    Startup = kCSIDL_STARTUP

    ''' <summary>
    ''' The file system directory that contains shortcuts to the user's most recently used documents.
    ''' A typical path is C:\Documents and Settings\username\My Recent Documents.
    ''' To create a shortcut in this folder, use SHAddToRecentDocs.
    ''' In addition to creating the shortcut, this function updates the
    ''' Shell's list of recent documents and adds the shortcut to the
    ''' My Recent Documents submenu of the Start menu.
    ''' </summary>
    ''' <remarks></remarks>
    Recent = kCSIDL_RECENT

    ''' <summary>
    ''' The file system directory that contains Send To menu items.
    ''' A typical path is C:\Documents and Settings\username\SendTo.
    ''' </summary>
    ''' <remarks></remarks>
    SendTo = kCSIDL_SENDTO

    ''' <summary>
    ''' The virtual folder containing the objects in the user's Recycle Bin.
    ''' </summary>
    ''' <remarks></remarks>
    BitBucket = kCSIDL_BITBUCKET

    ''' <summary>
    ''' The file system directory containing Start menu items.
    ''' A typical path is C:\Documents and Settings\username\Start Menu.
    ''' </summary>
    ''' <remarks></remarks>
    StartMenu = kCSIDL_STARTMENU

    ''' <summary>
    ''' Version 6.0. The virtual folder representing the My Documents desktop item.
    ''' </summary>
    ''' <remarks></remarks>
    MyDocuments = kCSIDL_MYDOCUMENTS

    ''' <summary>
    ''' The file system directory that serves as a common repository for music files.
    ''' A typical path is C:\Documents and Settings\User\My Documents\My Music.
    ''' </summary>
    ''' <remarks></remarks>
    MyMusic = kCSIDL_MYMUSIC

    ''' <summary>
    ''' Version 6.0. The file system directory that serves as a common repository for video files.
    ''' A typical path is C:\Documents and Settings\username\My Documents\My Videos.
    ''' </summary>
    ''' <remarks></remarks>
    MyVideo = kCSIDL_MYVIDEO

    ''' <summary>
    ''' The file system directory used to physically store file objects on the desktop
    ''' (not to be confused with the desktop folder itself).
    ''' A typical path is C:\Documents and Settings\username\Desktop.
    ''' </summary>
    ''' <remarks></remarks>
    DesktopDirectory = kCSIDL_DESKTOPDIRECTORY

    ''' <summary>
    ''' The virtual folder representing My Computer, containing everything on
    ''' the local computer: storage devices, printers, and Control Panel.
    ''' The folder may also contain mapped network drives.
    ''' </summary>
    ''' <remarks></remarks>
    Drives = kCSIDL_DRIVES

    ''' <summary>
    ''' A virtual folder representing Network Neighborhood, the root of the
    ''' network namespace hierarchy.
    ''' </summary>
    ''' <remarks></remarks>
    Network = kCSIDL_NETWORK

    ''' <summary>
    ''' A file system directory containing the link objects that may exist in
    ''' the My Network Places virtual folder.
    ''' It is not the same as NETWORK, which represents the network namespace root.
    ''' A typical path is C:\Documents and Settings\username\NetHood.
    ''' </summary>
    ''' <remarks></remarks>
    Nethood = kCSIDL_NETHOOD

    ''' <summary>
    ''' A virtual folder containing fonts. A typical path is C:\Windows\Fonts.
    ''' </summary>
    ''' <remarks></remarks>
    Fonts = kCSIDL_FONTS

    ''' <summary>
    ''' The file system directory that serves as a common repository for document templates.
    ''' A typical path is C:\Documents and Settings\username\Templates.
    ''' </summary>
    ''' <remarks></remarks>
    Templates = kCSIDL_TEMPLATES

    ''' <summary>
    ''' The file system directory that contains the programs and folders that
    ''' appear on the Start menu for all users.
    ''' A typical path is C:\Documents and Settings\All Users\Start Menu.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    CommonStartMenu = kCSIDL_COMMON_STARTMENU

    ''' <summary>
    ''' The file system directory that contains the directories for the
    ''' common program groups that appear on the Start menu for all users.
    ''' A typical path is C:\Documents and Settings\All Users\Start Menu\Programs.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    CommonPrograms = kCSIDL_COMMON_PROGRAMS

    ''' <summary>
    ''' The file system directory that contains the programs that appear in the
    ''' Startup folder for all users.
    ''' A typical path is C:\Documents and Settings\All Users\Start Menu\Programs\Startup.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    CommonStartup = kCSIDL_COMMON_STARTUP

    ''' <summary>
    ''' The file system directory that contains files and folders that appear on the desktop for
    ''' all users. A typical path is C:\Documents and Settings\All Users\Desktop.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    CommonDesktopDirectory = kCSIDL_COMMON_DESKTOPDIRECTORY

    ''' <summary>
    ''' Version 4.71. The file system directory that serves as a common
    ''' repository for application-specific data.
    ''' A typical path is C:\Documents and Settings\username\Application Data.
    ''' This CSIDL is supported by the redistributable Shfolder.dll for systems
    ''' that do not have the Microsoft Internet Explorer 4.0 integrated Shell installed.
    ''' </summary>
    ''' <remarks></remarks>
    AppData = kCSIDL_APPDATA

    ''' <summary>
    ''' The file system directory that contains the link objects that can exist in
    ''' the Printers virtual folder.
    ''' A typical path is C:\Documents and Settings\username\PrintHood.
    ''' </summary>
    ''' <remarks></remarks>
    Printhood = kCSIDL_PRINTHOOD

    ''' <summary>
    ''' Version 5.0. The file system directory that serves as a data repository for
    ''' local (nonroaming) applications.
    ''' A typical path is C:\Documents and Settings\username\Local Settings\Application Data.
    ''' </summary>
    ''' <remarks></remarks>
    LocalAppdata = kCSIDL_LOCAL_APPDATA

    ''' <summary>
    ''' The file system directory that corresponds to the user's
    ''' nonlocalized Startup program group.
    ''' </summary>
    ''' <remarks></remarks>
    AltStartup = kCSIDL_ALTSTARTUP

    ''' <summary>
    ''' The file system directory that corresponds to the nonlocalized
    ''' Startup program group for all users.
    ''' Valid only for Microsoft Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    CommonAltStartup = kCSIDL_COMMON_ALTSTARTUP

    ''' <summary>
    ''' The file system directory that serves as a common repository for
    ''' favorite items common to all users.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    CommonFavorites = kCSIDL_COMMON_FAVORITES

    ''' <summary>
    ''' Version 4.72. The file system directory that serves as a common repository for
    ''' temporary Internet files.
    ''' A typical path is C:\Documents and Settings\username\Local Settings\Temporary Internet Files.
    ''' </summary>
    ''' <remarks></remarks>
    InternetCache = kCSIDL_INTERNET_CACHE

    ''' <summary>
    ''' The file system directory that serves as a common repository for Internet cookies.
    ''' A typical path is C:\Documents and Settings\username\Cookies.
    ''' </summary>
    ''' <remarks></remarks>
    Cookies = kCSIDL_COOKIES

    ''' <summary>
    ''' The file system directory that serves as a common repository for Internet history items.
    ''' </summary>
    ''' <remarks></remarks>
    History = kCSIDL_HISTORY

    ''' <summary>
    ''' Version 5.0. The file system directory containing application data for all users.
    ''' A typical path is C:\Documents and Settings\All Users\Application Data.
    ''' </summary>
    ''' <remarks></remarks>
    CommonAppData = kCSIDL_COMMON_APPDATA

    ''' <summary>
    ''' Version 5.0. The Windows directory or SYSROOT.
    ''' This corresponds to the %windir% or %SYSTEMROOT% environment variables.
    ''' A typical path is C:\Windows.
    ''' </summary>
    ''' <remarks></remarks>
    Windows = kCSIDL_WINDOWS

    ''' <summary>
    ''' Version 5.0. The Windows System folder.
    ''' A typical path is C:\Windows\System32.
    ''' </summary>
    ''' <remarks></remarks>
    System = kCSIDL_SYSTEM

    ''' <summary>
    ''' Version 5.0. The Program Files folder. A typical path is C:\Program Files.
    ''' </summary>
    ''' <remarks></remarks>
    ProgramFiles = kCSIDL_PROGRAM_FILES

    ''' <summary>
    ''' Version 5.0. The file system directory that serves as a common repository for image files.
    ''' A typical path is C:\Documents and Settings\username\My Documents\My Pictures.
    ''' </summary>
    ''' <remarks></remarks>
    MyPictures = kCSIDL_MYPICTURES

    ''' <summary>
    ''' Version 5.0. The user's profile folder.
    ''' A typical path is C:\Documents and Settings\username.
    ''' Applications should not create files or folders at this level;
    ''' they should put their data under the locations referred to by
    ''' APPDATA or LOCAL_APPDATA.
    ''' </summary>
    ''' <remarks></remarks>
    Profile = kCSIDL_PROFILE

    ''' <summary>
    ''' x86 system directory on RISC.
    ''' </summary>
    ''' <remarks></remarks>
    SystemX86 = kCSIDL_SYSTEMX86

    ''' <summary>
    ''' x86 C:\Program Files on RISC.
    ''' </summary>
    ''' <remarks></remarks>
    ProgramFilesX86 = kCSIDL_PROGRAM_FILESX86

    ''' <summary>
    ''' Version 5.0. A folder for components that are shared across applications.
    ''' A typical path is C:\Program Files\Common.
    ''' Valid only for Windows NT, Windows 2000, and Windows XP systems.
    ''' Not valid for Windows Millennium Edition (Windows Me).
    ''' </summary>
    ''' <remarks></remarks>
    CommonProgramFiles = kCSIDL_PROGRAM_FILES_COMMON

    ''' <summary>
    ''' x86 Program Files\Common on RISC.
    ''' </summary>
    ''' <remarks></remarks>
    CommonProgramFilesX86 = kCSIDL_PROGRAM_FILES_COMMONX86

    ''' <summary>
    ''' The file system directory that contains the templates that are available to all users.
    ''' A typical path is C:\Documents and Settings\All Users\Templates.
    ''' Valid only for Windows NT systems.
    ''' </summary>
    ''' <remarks></remarks>
    CommonTemplates = kCSIDL_COMMON_TEMPLATES

    ''' <summary>
    ''' The file system directory that contains documents that are common to all users.
    ''' A typical paths is C:\Documents and Settings\All Users\Documents.
    ''' Valid for Windows NT systems and Microsoft Windows 95 and Windows 98 systems
    ''' with Shfolder.dll installed.
    ''' </summary>
    ''' <remarks></remarks>
    CommonDocuments = kCSIDL_COMMON_DOCUMENTS

    ''' <summary>
    ''' Version 5.0. The file system directory containing administrative tools for
    ''' all users of the computer.
    ''' </summary>
    ''' <remarks></remarks>
    CommonAdminTools = kCSIDL_COMMON_ADMINTOOLS

    ''' <summary>
    ''' Version 5.0. The file system directory that is used to store
    ''' administrative tools for an individual user.
    ''' The Microsoft Management Console (MMC) will save customized consoles
    ''' to this directory, and it will roam with the user.
    ''' </summary>
    ''' <remarks></remarks>
    AdminTools = kCSIDL_ADMINTOOLS

    ''' <summary>
    ''' Network and Dial-up Connections.
    ''' </summary>
    ''' <remarks></remarks>
    Connections = kCSIDL_CONNECTIONS

    ''' <summary>
    ''' Version 6.0. The file system directory that serves as a repository for
    ''' music files common to all users.
    ''' A typical path is C:\Documents and Settings\All Users\Documents\My Music.
    ''' </summary>
    ''' <remarks></remarks>
    CommonMusic = kCSIDL_COMMON_MUSIC

    ''' <summary>
    ''' Version 6.0. The file system directory that serves as a repository for
    ''' image files common to all users.
    ''' A typical path is C:\Documents and Settings\All Users\Documents\My Pictures.
    ''' </summary>
    ''' <remarks></remarks>
    CommonPictures = kCSIDL_COMMON_PICTURES

    ''' <summary>
    ''' Version 6.0. The file system directory that serves as a repository for
    ''' video files common to all users.
    ''' A typical path is C:\Documents and Settings\All Users\Documents\My Videos.
    ''' </summary>
    ''' <remarks></remarks>
    CommonVideo = kCSIDL_COMMON_VIDEO

    ''' <summary>
    ''' %windir%\Resources\, For theme and other windows resources.
    ''' </summary>
    ''' <remarks></remarks>
    Resources = kCSIDL_RESOURCES

    ''' <summary>
    ''' %windir%\Resources\[LangID], for theme and other windows specific resources.
    ''' </summary>
    ''' <remarks></remarks>
    ResourcesLocalized = kCSIDL_RESOURCES_LOCALIZED

    ''' <summary>
    ''' Links to All Users OEM specific apps.
    ''' </summary>
    ''' <remarks></remarks>
    CommonOEMLinks = kCSIDL_COMMON_OEM_LINKS

    ''' <summary>
    ''' Version 6.0. The file system directory acting as a staging area for files
    ''' waiting to be written to CD. A typical path is
    ''' C:\Documents and Settings\username\Local Settings\Application Data\Microsoft\CD Burning.
    ''' </summary>
    ''' <remarks></remarks>
    CDburnArea = kCSIDL_CDBURN_AREA

    ''' <summary>
    ''' Computers Near Me (computered from Workgroup membership).
    ''' </summary>
    ''' <remarks></remarks>
    ComputersNearMe = kCSIDL_COMPUTERSNEARME

    ''' <summary>
    ''' Version 6.0. The file system directory containing user profile folders.
    ''' A typical path is C:\Documents and Settings.
    ''' </summary>
    ''' <remarks></remarks>
    Profiles = kCSIDL_PROFILES

    ''' <summary>
    ''' Build a simple pointer to an item identifier list (PIDL).
    ''' </summary>
    ''' <remarks></remarks>
    FlagSimpleIDList = kKF_FLAG_SIMPLE_IDLIST

    ''' <summary>
    ''' Gets the folder's default path independent of the current location of its parent.
    ''' KF_FLAG_DEFAULT_PATH must also be set.
    ''' </summary>
    ''' <remarks></remarks>
    FlagNotParentRelative = kKF_FLAG_NOT_PARENT_RELATIVE

    ''' <summary>
    ''' Gets the default path for a known folder that is redirected elsewhere.
    ''' If this flag is not set, the function retrieves the current�and possibly redirected�path of the folder.
    ''' This flag includes a verification of the folder's existence unless KF_FLAG_DONT_VERIFY is also set.
    ''' </summary>
    ''' <remarks></remarks>
    FlagDefaultPath = kKF_FLAG_DEFAULT_PATH

    ''' <summary>
    ''' Initializes the folder using its Desktop.ini settings.
    ''' If the folder cannot be initialized, the function returns a failure code and no path is returned.
    ''' This flag should be combined with KF_FLAG_CREATE, because if the folder has not yet been created,
    ''' the initialization fails because the result of KF_FLAG_INIT is only a desktop.ini file, not its directory.
    ''' KF_FLAG_CREATE | KF_FLAG_INIT will always succeed.
    ''' If the folder is located on a network, the function might take longer to execute.
    ''' </summary>
    ''' <remarks></remarks>
    FlagInit = kKF_FLAG_INIT

    ''' <summary>
    ''' Gets the true system path for the folder, free of any aliased placeholders such as %USERPROFILE%.
    ''' This flag can only be used with SHSetKnownFolderPath and IKnownFolder::SetPath.
    ''' </summary>
    ''' <remarks></remarks>
    FlagNoAlias = kKF_FLAG_NO_ALIAS

    ''' <summary>
    ''' Stores the full path in the registry without environment strings.
    ''' If this flag is not set, portions of the path may be represented by
    ''' environment strings such as %USERPROFILE%.
    ''' This flag can only be used with SHSetKnownFolderPath and IKnownFolder::SetPath.
    ''' </summary>
    ''' <remarks></remarks>
    FlagDontExpand = kKF_FLAG_DONT_UNEXPAND

    ''' <summary>
    ''' Specifies not to verify the folder's existence before attempting to retrieve the path or IDList.
    ''' If this flag is not set, an attempt is made to verify that the folder is truly present at the path.
    ''' If that verification fails due to the folder being absent or inaccessible,
    ''' the function returns a failure code and no path is returned.
    ''' If the folder is located on a network, the function might take some time to execute.
    ''' Setting this flag can reduce that lagtime.
    ''' </summary>
    ''' <remarks></remarks>
    FlagDontVerify = kKF_FLAG_DONT_VERIFY

    ''' <summary>
    ''' Forces the creation of the specified folder if that folder does not already exist.
    ''' The security provisions predefined for that folder are applied.
    ''' If the folder does not exist and cannot be created, the function returns a failure code and no path is returned.
    ''' This value can be used only with the following functions and methods:
    '''     SHGetKnownFolderPath
    '''     SHGetKnownFolderIDList
    '''     IKnownFolder::GetPath
    '''     IKnownFolder::GetIDList
    ''' </summary>
    ''' <remarks></remarks>
    FlagCreate = kKF_FLAG_CREATE

    ''' <summary>
    ''' Mask for all possible flag values.
    ''' </summary>
    ''' <remarks></remarks>
    FlagMask = kKF_FLAG_MASK

End Enum
