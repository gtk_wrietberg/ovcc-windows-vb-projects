Imports Microsoft.Win32

Module modDisableService
    Public Function DisableWindowsService(ByRef oService As Cleanup_Service) As Boolean
        Dim rk_SERVICE As RegistryKey

        oService.Found = False
        oService.Disabled = False

        oLogger.WriteToLogRelative("opening registry key", , 1)
        Try
            oLogger.WriteToLogRelative("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\" & oService.Name, , 2)
            rk_SERVICE = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & oService.Name, True)
            oLogger.WriteToLogRelative("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 3)
            oLogger.WriteToLogRelative(ex.ToString, , 4)

            Return False
        End Try

        If rk_SERVICE Is Nothing Then
            oLogger.WriteToLogRelative("does not exist", , 3)

            Return True
        End If

        oService.Found = True

        oLogger.WriteToLogRelative("disabling service", , 1)

        Try
            oLogger.WriteToLogRelative("setting 'Start' to " & cServiceDisabledValue.ToString, , 2)
            rk_SERVICE.SetValue("Start", cServiceDisabledValue)
            oLogger.WriteToLogRelative("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 3)
            oLogger.WriteToLogRelative(ex.ToString, , 4)

            Return False
        End Try

        oLogger.WriteToLogRelative("double checking", , 1)

        Dim lCheckValue As Long

        Try
            oLogger.WriteToLogRelative("getting 'Start' value", , 2)
            lCheckValue = Long.Parse(rk_SERVICE.GetValue("Start").ToString)

            If lCheckValue = cServiceDisabledValue Then
                oLogger.WriteToLogRelative("ok, because " & lCheckValue.ToString & " = " & cServiceDisabledValue.ToString, , 3)
            Else
                oLogger.WriteToLogRelative("fail, because " & lCheckValue.ToString & " != " & cServiceDisabledValue.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                Return False
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", , 3)
            oLogger.WriteToLogRelative(ex.ToString, , 4)

            Return False
        End Try

        oService.Disabled = True

        Return True
    End Function
End Module
