#Region "[### TO DO ###]"

'### TO DO #####################################################################
'-------------------------------------------------------------------------------
' DATE: 2008-03-12
' WHO: Warrick Procter
' DESCRIPTION:
' o Test.
'-------------------------------------------------------------------------------
'### TO DO #####################################################################

#End Region

#Region "[=== OPTIONS ===]"

Option Strict On
Option Explicit On
Option Compare Binary

#End Region

#Region "[=== IMPORTS ===]"

Imports System.Runtime.InteropServices

#End Region

''' <copyright>
'''###########################################################################
'''##                Copyright (c) 2008 Warrick Procter.                    ##
'''##                                                                       ##
'''## This work is covered by the "Code Project Open License", a copy of    ##
'''## which is enclosed with this package as:                               ##
'''##         "Code Project Open License (CPOL).txt",                       ##
'''## and is available from http://www.codeproject.com/.                    ##
'''##                                                                       ##
'''## No other use is permitted without the express prior written           ##
'''## permission of Warrick Procter.                                        ##
'''## For permission, try these contact addresses (current at the time of   ##
'''## writing):                                                             ##
'''##     procter_AT_xtra_DOT_co_DOT_nz                                     ##
'''##     The address for service of company "ZED Limited", New Zealand.    ##
'''###########################################################################
''' </copyright>
''' <disclaimer>
'''###########################################################################
'''## REPRESENTATIONS, WARRANTIES AND DISCLAIMER                            ##
'''## ------------------------------------------                            ##
'''## THIS WORK IS PROVIDED "AS IS", "WHERE IS" AND "AS AVAILABLE", WITHOUT ##
'''## ANY EXPRESS OR IMPLIED WARRANTIES OR CONDITIONS OR GUARANTEES. YOU,   ##
'''## THE USER, ASSUME ALL RISK IN ITS USE, INCLUDING COPYRIGHT             ##
'''## INFRINGEMENT, PATENT INFRINGEMENT, SUITABILITY, ETC. AUTHOR EXPRESSLY ##
'''## DISCLAIMS ALL EXPRESS, IMPLIED OR STATUTORY WARRANTIES OR CONDITIONS, ##
'''## INCLUDING WITHOUT LIMITATION, WARRANTIES OR CONDITIONS OF             ##
'''## MERCHANTABILITY, MERCHANTABLE QUALITY OR FITNESS FOR A PARTICULAR     ##
'''## PURPOSE, OR ANY WARRANTY OF TITLE OR NON-INFRINGEMENT, OR THAT THE    ##
'''## WORK (OR ANY PORTION THEREOF) IS CORRECT, USEFUL, BUG-FREE OR FREE OF ##
'''## VIRUSES. YOU MUST PASS THIS DISCLAIMER ON WHENEVER YOU DISTRIBUTE THE ##
'''## WORK OR DERIVATIVE WORKS.                                             ##
'''###########################################################################
''' </disclaimer>
''' <history>
''' 2008-03-12 [Warrick Procter] Created.
''' </history>
''' <summary>
''' enuCSIDLFlags - CSIDL directory flags enumeration.
''' See enuCSIDL for detailed documentation.
''' </summary>
''' <overview>
''' </overview>
''' <remarks>
''' See enuCSIDL for detailed documentation.
''' </remarks>
''' <notes>
''' </notes>
Public Enum enuCSIDLFlags As Int32

    ''' <summary>
    ''' Gets the default path for a known folder that is redirected elsewhere.
    ''' If this flag is not set, the function retrieves the current�and possibly redirected�path of the folder.
    ''' This flag includes a verification of the folder's existence unless KF_FLAG_DONT_VERIFY is also set.
    ''' </summary>
    ''' <remarks></remarks>
    DefaultPath = kKF_FLAG_DEFAULT_PATH

    ''' <summary>
    ''' Build a simple pointer to an item identifier list (PIDL).
    ''' </summary>
    ''' <remarks></remarks>
    SimpleIDList = kKF_FLAG_SIMPLE_IDLIST

    ''' <summary>
    ''' Gets the folder's default path independent of the current location of its parent.
    ''' KF_FLAG_DEFAULT_PATH must also be set.
    ''' </summary>
    ''' <remarks></remarks>
    NotParentRelative = kKF_FLAG_NOT_PARENT_RELATIVE

    ''' <summary>
    ''' Initializes the folder using its Desktop.ini settings.
    ''' If the folder cannot be initialized, the function returns a failure code and no path is returned.
    ''' This flag should be combined with KF_FLAG_CREATE, because if the folder has not yet been created,
    ''' the initialization fails because the result of KF_FLAG_INIT is only a desktop.ini file, not its directory.
    ''' KF_FLAG_CREATE | KF_FLAG_INIT will always succeed.
    ''' If the folder is located on a network, the function might take longer to execute.
    ''' </summary>
    ''' <remarks></remarks>
    Init = kKF_FLAG_INIT

    ''' <summary>
    ''' Gets the true system path for the folder, free of any aliased placeholders such as %USERPROFILE%.
    ''' This flag can only be used with SHSetKnownFolderPath and IKnownFolder::SetPath.
    ''' </summary>
    ''' <remarks></remarks>
    NoAlias = kKF_FLAG_NO_ALIAS

    ''' <summary>
    ''' Stores the full path in the registry without environment strings.
    ''' If this flag is not set, portions of the path may be represented by
    ''' environment strings such as %USERPROFILE%.
    ''' This flag can only be used with SHSetKnownFolderPath and IKnownFolder::SetPath.
    ''' </summary>
    ''' <remarks></remarks>
    DontExpand = kKF_FLAG_DONT_UNEXPAND

    ''' <summary>
    ''' Specifies not to verify the folder's existence before attempting to retrieve the path or IDList.
    ''' If this flag is not set, an attempt is made to verify that the folder is truly present at the path.
    ''' If that verification fails due to the folder being absent or inaccessible,
    ''' the function returns a failure code and no path is returned.
    ''' If the folder is located on a network, the function might take some time to execute.
    ''' Setting this flag can reduce that lagtime.
    ''' </summary>
    ''' <remarks></remarks>
    DontVerify = kKF_FLAG_DONT_VERIFY

    ''' <summary>
    ''' Forces the creation of the specified folder if that folder does not already exist.
    ''' The security provisions predefined for that folder are applied.
    ''' If the folder does not exist and cannot be created, the function returns a failure code and no path is returned.
    ''' This value can be used only with the following functions and methods:
    '''     SHGetKnownFolderPath
    '''     SHGetKnownFolderIDList
    '''     IKnownFolder::GetPath
    '''     IKnownFolder::GetIDList
    ''' </summary>
    ''' <remarks></remarks>
    Create = kKF_FLAG_CREATE

    ''' <summary>
    ''' Mask for all possible flag values.
    ''' </summary>
    ''' <remarks></remarks>
    Mask = kKF_FLAG_MASK

End Enum

