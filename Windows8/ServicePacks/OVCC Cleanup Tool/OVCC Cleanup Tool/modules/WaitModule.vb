Module WaitModule
    Private Structure FILETIME
        Dim dwLowDateTime As Long
        Dim dwHighDateTime As Long
    End Structure

    Private Const WAIT_ABANDONED& = &H80&
    Private Const WAIT_ABANDONED_0& = &H80&
    Private Const WAIT_FAILED& = -1&
    Private Const WAIT_IO_COMPLETION& = &HC0&
    Private Const WAIT_OBJECT_0& = 0
    Private Const WAIT_OBJECT_1& = 1
    Private Const WAIT_TIMEOUT& = &H102&

    Private Const INFINITE = &HFFFF
    Private Const ERROR_ALREADY_EXISTS = 183&

    Private Const QS_HOTKEY& = &H80
    Private Const QS_KEY& = &H1
    Private Const QS_MOUSEBUTTON& = &H4
    Private Const QS_MOUSEMOVE& = &H2
    Private Const QS_PAINT& = &H20
    Private Const QS_POSTMESSAGE& = &H8
    Private Const QS_SENDMESSAGE& = &H40
    Private Const QS_TIMER& = &H10
    Private Const QS_MOUSE& = (QS_MOUSEMOVE Or QS_MOUSEBUTTON)
    Private Const QS_INPUT& = (QS_MOUSE Or QS_KEY)
    Private Const QS_ALLEVENTS& = (QS_INPUT Or QS_POSTMESSAGE Or QS_TIMER Or QS_PAINT Or QS_HOTKEY)
    Private Const QS_ALLINPUT& = (QS_SENDMESSAGE Or QS_PAINT Or QS_TIMER Or QS_POSTMESSAGE Or QS_MOUSEBUTTON Or QS_MOUSEMOVE Or QS_HOTKEY Or QS_KEY)

    Private Declare Function CreateWaitableTimer Lib "kernel32" Alias "CreateWaitableTimerA" (ByVal lpSemaphoreAttributes As Long, ByVal bManualReset As Long, ByVal lpName As String) As Long
    Private Declare Function OpenWaitableTimer Lib "kernel32" Alias "OpenWaitableTimerA" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal lpName As String) As Long
    Private Declare Function SetWaitableTimer Lib "kernel32" (ByVal hTimer As Long, ByVal lpDueTime As FILETIME, ByVal lPeriod As Long, ByVal pfnCompletionRoutine As Long, ByVal lpArgToCompletionRoutine As Long, ByVal fResume As Long) As Long
    Private Declare Function CancelWaitableTimer Lib "kernel32" (ByVal hTimer As Long)
    Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
    Private Declare Function WaitForSingleObject Lib "kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
    Private Declare Function MsgWaitForMultipleObjects Lib "user32" (ByVal nCount As Long, ByVal pHandles As Long, ByVal fWaitAll As Long, ByVal dwMilliseconds As Long, ByVal dwWakeMask As Long) As Long

    Public Sub Wait(ByVal lNumberOfSeconds As Long)
        WaitMilliseconds(lNumberOfSeconds * 1000&)
    End Sub

    Public Sub WaitMilliseconds(ByVal lNumberOfMilliSeconds As Long)
        Dim ft As FILETIME
        Dim lBusy As Long
        Dim lRet As Long
        Dim dblDelay As Double
        Dim dblDelayLow As Double
        Dim dblUnits As Double
        Dim hTimer As Long

        hTimer = CreateWaitableTimer(0, True, IO.Path.GetFileName(Application.ExecutablePath) & "Timer")

        If Err.LastDllError = ERROR_ALREADY_EXISTS Then
            ' If the timer already exists, it does not hurt to open it
            ' as long as the person who is trying to open it has the
            ' proper access rights.
        Else
            ft.dwLowDateTime = -1
            ft.dwHighDateTime = -1
            lRet = SetWaitableTimer(hTimer, ft, 0, 0, 0, 0)
        End If

        ' Convert the Units to nanoseconds.
        dblUnits = CDbl(&H10000) * CDbl(&H10000)
        dblDelay = CDbl(lNumberOfMilliSeconds) * 1000

        ' By setting the high/low time to a negative number, it tells
        ' the Wait (in SetWaitableTimer) to use an offset time as
        ' opposed to a hardcoded time. If it were positive, it would
        ' try to convert the value to GMT.
        ft.dwHighDateTime = -CLng(dblDelay / dblUnits) - 1
        dblDelayLow = -dblUnits * (dblDelay / dblUnits - Fix(dblDelay / dblUnits))

        If dblDelayLow < CDbl(&H80000000) Then
            ' &H80000000 is MAX_LONG, so you are just making sure
            ' that you don't overflow when you try to stick it into
            ' the FILETIME structure.
            dblDelayLow = dblUnits + dblDelayLow
        End If

        ft.dwLowDateTime = CLng(dblDelayLow)
        lRet = SetWaitableTimer(hTimer, ft, 0, 0, 0, False)

        Do
            ' QS_ALLINPUT means that MsgWaitForMultipleObjects will
            ' return every time the thread in which it is running gets
            ' a message. If you wanted to handle messages in here you could,
            ' but by calling Doevents you are letting DefWindowProc
            ' do its normal windows message handling---Like DDE, etc.
            lBusy = MsgWaitForMultipleObjects(1, hTimer, False, INFINITE, QS_ALLINPUT&)

            Application.DoEvents()
        Loop Until lBusy = WAIT_OBJECT_0

        ' Close the handles when you are done with them.
        CloseHandle(hTimer)
    End Sub
End Module
