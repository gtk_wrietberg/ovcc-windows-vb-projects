Module CreateFolderRecursive
    Public Function CreateFolder(ByVal destDir As String) As Boolean
        If Right(destDir, 1) = "\" Then
            destDir = Left(destDir, destDir.Length - 1)
        End If

        Return _CreateFolderRecursively(destDir)
    End Function

    Private Function _CreateFolderRecursively(ByVal destDir As String) As Boolean
        Dim i As Long
        Dim prevDir As String = ""

        For i = destDir.Length To 1 Step -1
            If Mid(destDir, i, 1) = "\" Then
                prevDir = Left(destDir, i - 1)
                Exit For
            End If
        Next i

        If prevDir = "" Then _CreateFolderRecursively = False : Exit Function
        If Not Dir(prevDir & "\", vbDirectory).Length > 0 Then
            If Not _CreateFolderRecursively(prevDir) Then _CreateFolderRecursively = False : Exit Function
        End If

        Try
            MkDir(destDir)
            _CreateFolderRecursively = True
        Catch ex As Exception
            _CreateFolderRecursively = False
        End Try
    End Function
End Module
