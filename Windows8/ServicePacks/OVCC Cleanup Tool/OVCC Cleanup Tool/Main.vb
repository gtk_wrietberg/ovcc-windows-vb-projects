Module Main
    Public Sub Main()
        Dim bSafetyOff As Boolean = False

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)

        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)


        For Each arg As String In Environment.GetCommandLineArgs()
            If arg = "--safety-off" Then
                bSafetyOff = True
            End If
        Next

        If Not bSafetyOff Then
            Helpers.Logger.WriteWarning("Safety not disabled", 0)

            ExitCode.SetValue(ExitCode.ExitCodes.SAFETY_STILL_ENABLED)
            ExitApplication()

            Exit Sub
        End If


        Helpers.Logger.WriteMessage("Loading configuration", 0)
        If Not LoadConfiguration() Then
            Helpers.Logger.WriteError("failed", 1)

            ExitCode.SetValue(ExitCode.ExitCodes.CONFIG_ERROR)
            ExitApplication()
            Exit Sub
        End If


        Helpers.Logger.WriteMessage("Starting cleanup", 0)

        CleaningDepartement()
    End Sub

    Private Sub CleaningDepartement()
        Helpers.Logger.WriteMessage("Disabling services", 1)
        Helpers.Logger.WriteMessage("amount: " & oCleanup.NumberOfServices.ToString, 2)

        If oCleanup.CleanUp_Services() Then
            Helpers.Logger.WriteMessage("done", 3)
        Else
            Helpers.Logger.WriteWarning("done, but with errors", 2)
        End If


        '------------
        Helpers.Logger.WriteMessage("Removing startups", 0)

        If oCleanup.CleanUp_Startups() Then
            Helpers.Logger.WriteMessage("done", , 1)
        Else
            Helpers.Logger.WriteMessage("done, but with errors", 1)
        End If


        '------------
        Helpers.Logger.WriteMessage("Cleaning folders", 0)
        oCleanup.CleanUp_Locations()

        Helpers.Logger.WriteMessage("Bytes found", 1)
        Helpers.Logger.WriteMessage(oCleanup.TotalFileSize, 2)
        Helpers.Logger.WriteMessage("Bytes deleted", 1)
        Helpers.Logger.WriteMessage(oCleanup.TotalDeletedFileSize, 2)
        Helpers.Logger.WriteMessage("Bytes delayed", 1)
        Helpers.Logger.WriteMessage(oCleanup.TotalDelayedFileSize, 2)


        '------------
        Helpers.Logger.WriteMessage("Cleaning registry")
        If oCleanup.CleanUp_RegistryKeys() Then
            Helpers.Logger.WriteMessage("done", 1)
        Else
            Helpers.Logger.WriteWarning("done", 1)
        End If


        '------------
        ExitApplication()
    End Sub

    Public Sub ExitApplication()
        '-----------------------------------------------
        'done
        Helpers.Logger.WriteEmptyLine()
        Helpers.Logger.WriteMessage("exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString(), 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
