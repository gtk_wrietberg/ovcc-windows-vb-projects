﻿Module Main
    Public Sub Main()
        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)

        Dim scriptFile As String = IO.Path.Combine(Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", ""), "Skins\default\Scripts\guest-tek.js")
        Dim lines As New List(Of String)
        Dim fileChanged As Boolean = False


        Helpers.Logger.WriteMessage("checking script file", 0)
        Helpers.Logger.WriteMessage(scriptFile, 1)

        If Not IO.File.Exists(scriptFile) Then
            Helpers.Logger.WriteError("does not exist", 2)
            Exit Sub
        Else
            Helpers.Logger.WriteMessage("ok", 2)
        End If

        Try
            Dim currentSetting As String = ""

            Using sr As New IO.StreamReader(scriptFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var enableTimeWarning") Then
                        currentSetting = line
                        currentSetting = currentSetting.Replace("var enableTimeWarning=", "")
                        currentSetting = currentSetting.Replace(";", "")

                        Helpers.Logger.WriteMessage("found 'enableTimeWarning'", 1)
                        Helpers.Logger.WriteMessage("current value", 2)
                        Helpers.Logger.WriteMessage(currentSetting, 3)

                        If currentSetting.Equals("0") Then
                            Helpers.Logger.WriteMessage("no change needed", 1)
                            Exit Sub
                        End If

                        Helpers.Logger.WriteMessage("new value", 2)
                        Helpers.Logger.WriteMessage("0", 3)
                        line = "var enableTimeWarning=0;"

                        fileChanged = True
                    End If

                    lines.Add(line)
                End While
            End Using

            If fileChanged Then
                Helpers.Logger.WriteMessage("backing up script file", 0)
                Helpers.FilesAndFolders.Backup.File(scriptFile, 0, "TimeWarningDisabler")

                Helpers.Logger.WriteMessage("writing script file", 0)
                Using sw As New IO.StreamWriter(scriptFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using

                Helpers.Logger.WriteMessage("ok", 1)
            End If



        Catch ex As Exception
            Helpers.Logger.WriteError("ERROR", 0)
            Helpers.Logger.WriteError(ex.Message, 1)
        End Try
    End Sub


End Module
