Module modMain
    Public ReadOnly xpath_Applications As String = "/programs/file"
    Public ReadOnly attrib_filename As String = "filename"

    Public Sub Main()
        Dim _app As String = ""
        Dim _apps As New List(Of String)


        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Helpers.Logger.WriteMessage("started", 0)


        Helpers.SiteKiosk.SkCfg.XML.Lock()
        Helpers.SiteKiosk.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig

        Helpers.Logger.WriteMessage("Loading config", 0)
        Helpers.Logger.WriteMessage(Helpers.SiteKiosk.SkCfg.XML.FilePath, 1)

        If Helpers.SiteKiosk.SkCfg.XML.Load() Then
            Helpers.Logger.WriteMessage("ok", 2)
        Else
            Helpers.Logger.WriteError("oops", 2)
            If Not Helpers.SiteKiosk.SkCfg.XML.HasError Then
                Helpers.Logger.WriteError("Unknown cause", 3)
            Else
                Helpers.Logger.WriteError(Helpers.SiteKiosk.SkCfg.XML.LastError, 3)
            End If

            Bye()
            Exit Sub
        End If

        Helpers.Logger.WriteMessage("Loading apps", 0)
        If Helpers.SiteKiosk.SkCfg.XML.GetFirstValue(xpath_Applications, attrib_filename, _app) Then
            _apps.Add(_app)

            Do While Helpers.SiteKiosk.SkCfg.XML.GetNextValue(xpath_Applications, attrib_filename, _app)
                _apps.Add(_app)
            Loop
        End If


        For Each __app As String In _apps
            Dim __app_without_quotes As String = _RemoveStupidQuoteFromFilePath(__app)

            Helpers.Logger.WriteMessage("Found app", 1)
            Helpers.Logger.WriteMessage(__app, 2)
            If Not __app_without_quotes.Equals(__app) Then
                Helpers.Logger.WriteMessage("removing quotes", 3)
                Helpers.Logger.WriteMessage(__app_without_quotes, 4)
            End If

            Helpers.Logger.WriteMessage("Checking if file exists", 3)
            If IO.File.Exists(__app_without_quotes) Then
                Helpers.Logger.WriteMessage("ok", 4)
            Else
                Helpers.Logger.WriteWarning("not found", 4)

                Helpers.Logger.WriteMessage("Deleting from config", 3)
                Dim iRet As Integer = Helpers.SiteKiosk.SkCfg.XML.DeleteValue("/programs/file", "filename", __app)
                If iRet < 0 Then
                    Helpers.Logger.WriteError("Something has gone wrong", 4)
                    If Not Helpers.SiteKiosk.SkCfg.XML.HasError Then
                        Helpers.Logger.WriteError("Unknown cause", 5)
                    Else
                        Helpers.Logger.WriteError(Helpers.SiteKiosk.SkCfg.XML.LastError, 5)
                    End If
                ElseIf iRet = 0 Then
                    Helpers.Logger.WriteError("Not found", 4)
                Else
                    Helpers.Logger.WriteMessage("ok", 4)
                End If
            End If
        Next

        If Helpers.SiteKiosk.SkCfg.XML.NeedsSaving Then
            Helpers.Logger.WriteMessage("Saving config", 0)

            If Helpers.SiteKiosk.SkCfg.XML.Save() Then
                Helpers.Logger.WriteMessage("ok", 1)
            Else
                Helpers.Logger.WriteError("oops", 1)
                If Not Helpers.SiteKiosk.SkCfg.XML.HasError Then
                    Helpers.Logger.WriteError("Unknown cause", 2)
                Else
                    Helpers.Logger.WriteError(Helpers.SiteKiosk.SkCfg.XML.LastError, 2)
                End If
            End If
        Else
            Helpers.Logger.WriteMessage("Nothing changed, no save needed", 0)
        End If


        Helpers.SiteKiosk.SkCfg.XML.Unlock()


        Bye()
    End Sub

    Private Sub Bye()
        Helpers.Logger.WriteMessage("Done", 0)
        Helpers.Logger.WriteMessage("ok", 1)
        Helpers.Logger.WriteMessage("bye", 2)
    End Sub


    '--------------------------------------
    Private Function _RemoveStupidQuoteFromFilePath(sFilePath As String) As String
        sFilePath = sFilePath.Replace("&quot;", "")
        sFilePath = sFilePath.Replace("""", "")

        Return sFilePath
    End Function
End Module

