﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnSourceDirectory = New System.Windows.Forms.Button()
        Me.filedialogInternalExecutable = New System.Windows.Forms.OpenFileDialog()
        Me.folderdialogSourceDirectory = New System.Windows.Forms.FolderBrowserDialog()
        Me.txtSourceDirectory = New System.Windows.Forms.TextBox()
        Me.group_1_SourceDirectory = New System.Windows.Forms.GroupBox()
        Me.group_2_InternalExecutable = New System.Windows.Forms.GroupBox()
        Me.btnInternalExecutable = New System.Windows.Forms.Button()
        Me.txtInternalExecutable = New System.Windows.Forms.TextBox()
        Me.group_3_DestinationDirectory = New System.Windows.Forms.GroupBox()
        Me.btnDestinationDirectory = New System.Windows.Forms.Button()
        Me.txtDestinationDirectory = New System.Windows.Forms.TextBox()
        Me.group_4_PackageName = New System.Windows.Forms.GroupBox()
        Me.txtPackageName = New System.Windows.Forms.TextBox()
        Me.btnBuild = New System.Windows.Forms.Button()
        Me.folderdialogDestinationDirectory = New System.Windows.Forms.FolderBrowserDialog()
        Me.group_5_Build = New System.Windows.Forms.GroupBox()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.bgworkerBuildPackage = New System.ComponentModel.BackgroundWorker()
        Me.pnlProgressMain = New System.Windows.Forms.Panel()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.pnlProgressShadow = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbThemes = New System.Windows.Forms.ComboBox()
        Me.group_1_SourceDirectory.SuspendLayout()
        Me.group_2_InternalExecutable.SuspendLayout()
        Me.group_3_DestinationDirectory.SuspendLayout()
        Me.group_4_PackageName.SuspendLayout()
        Me.group_5_Build.SuspendLayout()
        Me.pnlProgressMain.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSourceDirectory
        '
        Me.btnSourceDirectory.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSourceDirectory.Location = New System.Drawing.Point(6, 21)
        Me.btnSourceDirectory.Name = "btnSourceDirectory"
        Me.btnSourceDirectory.Size = New System.Drawing.Size(344, 45)
        Me.btnSourceDirectory.TabIndex = 1
        Me.btnSourceDirectory.Text = "Choose source directory"
        Me.btnSourceDirectory.UseVisualStyleBackColor = True
        '
        'filedialogInternalExecutable
        '
        Me.filedialogInternalExecutable.FileName = "OpenFileDialog1"
        Me.filedialogInternalExecutable.Filter = "Executables|*.exe"
        '
        'folderdialogSourceDirectory
        '
        Me.folderdialogSourceDirectory.ShowNewFolderButton = False
        '
        'txtSourceDirectory
        '
        Me.txtSourceDirectory.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSourceDirectory.Location = New System.Drawing.Point(6, 72)
        Me.txtSourceDirectory.Name = "txtSourceDirectory"
        Me.txtSourceDirectory.ReadOnly = True
        Me.txtSourceDirectory.Size = New System.Drawing.Size(344, 22)
        Me.txtSourceDirectory.TabIndex = 2
        '
        'group_1_SourceDirectory
        '
        Me.group_1_SourceDirectory.BackColor = System.Drawing.Color.Transparent
        Me.group_1_SourceDirectory.Controls.Add(Me.btnSourceDirectory)
        Me.group_1_SourceDirectory.Controls.Add(Me.txtSourceDirectory)
        Me.group_1_SourceDirectory.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_1_SourceDirectory.Location = New System.Drawing.Point(12, 12)
        Me.group_1_SourceDirectory.Name = "group_1_SourceDirectory"
        Me.group_1_SourceDirectory.Size = New System.Drawing.Size(356, 102)
        Me.group_1_SourceDirectory.TabIndex = 3
        Me.group_1_SourceDirectory.TabStop = False
        Me.group_1_SourceDirectory.Text = "1. Source directory"
        '
        'group_2_InternalExecutable
        '
        Me.group_2_InternalExecutable.BackColor = System.Drawing.Color.Transparent
        Me.group_2_InternalExecutable.Controls.Add(Me.btnInternalExecutable)
        Me.group_2_InternalExecutable.Controls.Add(Me.txtInternalExecutable)
        Me.group_2_InternalExecutable.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_2_InternalExecutable.Location = New System.Drawing.Point(12, 125)
        Me.group_2_InternalExecutable.Name = "group_2_InternalExecutable"
        Me.group_2_InternalExecutable.Size = New System.Drawing.Size(356, 104)
        Me.group_2_InternalExecutable.TabIndex = 4
        Me.group_2_InternalExecutable.TabStop = False
        Me.group_2_InternalExecutable.Text = "2. Internal executable"
        '
        'btnInternalExecutable
        '
        Me.btnInternalExecutable.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInternalExecutable.Location = New System.Drawing.Point(6, 21)
        Me.btnInternalExecutable.Name = "btnInternalExecutable"
        Me.btnInternalExecutable.Size = New System.Drawing.Size(344, 45)
        Me.btnInternalExecutable.TabIndex = 1
        Me.btnInternalExecutable.Text = "Choose internal executable"
        Me.btnInternalExecutable.UseVisualStyleBackColor = True
        '
        'txtInternalExecutable
        '
        Me.txtInternalExecutable.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInternalExecutable.Location = New System.Drawing.Point(6, 72)
        Me.txtInternalExecutable.Name = "txtInternalExecutable"
        Me.txtInternalExecutable.ReadOnly = True
        Me.txtInternalExecutable.Size = New System.Drawing.Size(344, 23)
        Me.txtInternalExecutable.TabIndex = 2
        '
        'group_3_DestinationDirectory
        '
        Me.group_3_DestinationDirectory.BackColor = System.Drawing.Color.Transparent
        Me.group_3_DestinationDirectory.Controls.Add(Me.btnDestinationDirectory)
        Me.group_3_DestinationDirectory.Controls.Add(Me.txtDestinationDirectory)
        Me.group_3_DestinationDirectory.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_3_DestinationDirectory.Location = New System.Drawing.Point(12, 240)
        Me.group_3_DestinationDirectory.Name = "group_3_DestinationDirectory"
        Me.group_3_DestinationDirectory.Size = New System.Drawing.Size(356, 104)
        Me.group_3_DestinationDirectory.TabIndex = 4
        Me.group_3_DestinationDirectory.TabStop = False
        Me.group_3_DestinationDirectory.Text = "3. Destination directory"
        '
        'btnDestinationDirectory
        '
        Me.btnDestinationDirectory.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.btnDestinationDirectory.Location = New System.Drawing.Point(6, 21)
        Me.btnDestinationDirectory.Name = "btnDestinationDirectory"
        Me.btnDestinationDirectory.Size = New System.Drawing.Size(344, 45)
        Me.btnDestinationDirectory.TabIndex = 1
        Me.btnDestinationDirectory.Text = "Choose destination directory"
        Me.btnDestinationDirectory.UseVisualStyleBackColor = True
        '
        'txtDestinationDirectory
        '
        Me.txtDestinationDirectory.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDestinationDirectory.Location = New System.Drawing.Point(6, 72)
        Me.txtDestinationDirectory.Name = "txtDestinationDirectory"
        Me.txtDestinationDirectory.ReadOnly = True
        Me.txtDestinationDirectory.Size = New System.Drawing.Size(344, 23)
        Me.txtDestinationDirectory.TabIndex = 2
        '
        'group_4_PackageName
        '
        Me.group_4_PackageName.BackColor = System.Drawing.Color.Transparent
        Me.group_4_PackageName.Controls.Add(Me.txtPackageName)
        Me.group_4_PackageName.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_4_PackageName.Location = New System.Drawing.Point(12, 355)
        Me.group_4_PackageName.Name = "group_4_PackageName"
        Me.group_4_PackageName.Size = New System.Drawing.Size(356, 55)
        Me.group_4_PackageName.TabIndex = 5
        Me.group_4_PackageName.TabStop = False
        Me.group_4_PackageName.Text = "4. Package Name"
        '
        'txtPackageName
        '
        Me.txtPackageName.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPackageName.Location = New System.Drawing.Point(6, 24)
        Me.txtPackageName.Name = "txtPackageName"
        Me.txtPackageName.Size = New System.Drawing.Size(344, 23)
        Me.txtPackageName.TabIndex = 3
        '
        'btnBuild
        '
        Me.btnBuild.Font = New System.Drawing.Font("Segoe UI Semibold", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.btnBuild.Location = New System.Drawing.Point(6, 23)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(344, 45)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "Build"
        Me.btnBuild.UseVisualStyleBackColor = True
        '
        'group_5_Build
        '
        Me.group_5_Build.BackColor = System.Drawing.Color.Transparent
        Me.group_5_Build.Controls.Add(Me.btnBuild)
        Me.group_5_Build.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.group_5_Build.Location = New System.Drawing.Point(12, 494)
        Me.group_5_Build.Name = "group_5_Build"
        Me.group_5_Build.Size = New System.Drawing.Size(356, 74)
        Me.group_5_Build.TabIndex = 6
        Me.group_5_Build.TabStop = False
        Me.group_5_Build.Text = "6. Build package"
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblVersion.ForeColor = System.Drawing.Color.White
        Me.lblVersion.Location = New System.Drawing.Point(675, 575)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(222, 20)
        Me.lblVersion.TabIndex = 7
        Me.lblVersion.Text = "v?.?.?.?"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'bgworkerBuildPackage
        '
        Me.bgworkerBuildPackage.WorkerReportsProgress = True
        '
        'pnlProgressMain
        '
        Me.pnlProgressMain.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.pnlProgressMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlProgressMain.Controls.Add(Me.lblProgress)
        Me.pnlProgressMain.Location = New System.Drawing.Point(579, 23)
        Me.pnlProgressMain.Name = "pnlProgressMain"
        Me.pnlProgressMain.Size = New System.Drawing.Size(300, 30)
        Me.pnlProgressMain.TabIndex = 8
        Me.pnlProgressMain.Visible = False
        '
        'lblProgress
        '
        Me.lblProgress.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress.Location = New System.Drawing.Point(3, 1)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(291, 25)
        Me.lblProgress.TabIndex = 4
        Me.lblProgress.Text = "%% TITLE %%"
        Me.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlProgressShadow
        '
        Me.pnlProgressShadow.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlProgressShadow.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.pnlProgressShadow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlProgressShadow.Location = New System.Drawing.Point(582, 26)
        Me.pnlProgressShadow.Name = "pnlProgressShadow"
        Me.pnlProgressShadow.Size = New System.Drawing.Size(300, 30)
        Me.pnlProgressShadow.TabIndex = 9
        Me.pnlProgressShadow.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.cmbThemes)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 418)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(356, 68)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "5. Set theme"
        '
        'cmbThemes
        '
        Me.cmbThemes.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbThemes.FormattingEnabled = True
        Me.cmbThemes.Location = New System.Drawing.Point(6, 28)
        Me.cmbThemes.Name = "cmbThemes"
        Me.cmbThemes.Size = New System.Drawing.Size(344, 25)
        Me.cmbThemes.TabIndex = 0
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(86, Byte), Integer), CType(CType(164, Byte), Integer))
        Me.BackgroundImage = Global.OVCCPackageCreator.My.Resources.Resources.CorporateHorse_bg_bordered_900x600
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(900, 600)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.pnlProgressMain)
        Me.Controls.Add(Me.pnlProgressShadow)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.group_5_Build)
        Me.Controls.Add(Me.group_4_PackageName)
        Me.Controls.Add(Me.group_3_DestinationDirectory)
        Me.Controls.Add(Me.group_2_InternalExecutable)
        Me.Controls.Add(Me.group_1_SourceDirectory)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "OVCC Package Creator"
        Me.group_1_SourceDirectory.ResumeLayout(False)
        Me.group_1_SourceDirectory.PerformLayout()
        Me.group_2_InternalExecutable.ResumeLayout(False)
        Me.group_2_InternalExecutable.PerformLayout()
        Me.group_3_DestinationDirectory.ResumeLayout(False)
        Me.group_3_DestinationDirectory.PerformLayout()
        Me.group_4_PackageName.ResumeLayout(False)
        Me.group_4_PackageName.PerformLayout()
        Me.group_5_Build.ResumeLayout(False)
        Me.pnlProgressMain.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSourceDirectory As Button
    Friend WithEvents filedialogInternalExecutable As OpenFileDialog
    Friend WithEvents folderdialogSourceDirectory As FolderBrowserDialog
    Friend WithEvents txtSourceDirectory As TextBox
    Friend WithEvents group_1_SourceDirectory As GroupBox
    Friend WithEvents group_2_InternalExecutable As GroupBox
    Friend WithEvents btnInternalExecutable As Button
    Friend WithEvents txtInternalExecutable As TextBox
    Friend WithEvents group_3_DestinationDirectory As GroupBox
    Friend WithEvents btnDestinationDirectory As Button
    Friend WithEvents txtDestinationDirectory As TextBox
    Friend WithEvents group_4_PackageName As GroupBox
    Friend WithEvents btnBuild As Button
    Friend WithEvents folderdialogDestinationDirectory As FolderBrowserDialog
    Friend WithEvents group_5_Build As GroupBox
    Friend WithEvents txtPackageName As TextBox
    Friend WithEvents lblVersion As Label
    Friend WithEvents bgworkerBuildPackage As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlProgressMain As Panel
    Friend WithEvents lblProgress As Label
    Friend WithEvents pnlProgressShadow As Panel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cmbThemes As ComboBox
End Class
