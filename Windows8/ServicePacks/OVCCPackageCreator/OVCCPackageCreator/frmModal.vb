﻿Public Class frmModal
    Private Sub frmModal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadSettings()
    End Sub

    Private Sub LoadSettings()
        If Globals.Settings.CorporateFriendly Then
            pnlMain.BackgroundImage = My.Resources.Corporate_bg_bordered_transparent_300x200
        Else
            pnlMain.BackgroundImage = My.Resources.CorporateHorse_bg_bordered_transparent_300x200
        End If
    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub
End Class