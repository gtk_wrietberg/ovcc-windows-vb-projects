﻿Imports System.ComponentModel
Imports System.Net.Http
Imports System.Reflection
Imports System.Text.RegularExpressions
Imports OVCCPackageCreator.Functions

Public Class frmMain
#Region "Form move"
    Public MoveForm As Boolean
    Public MoveForm_MousePosition As Point


    Private Sub lblAppTitle_MouseDown(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            MoveForm = True
            Me.Cursor = Cursors.NoMove2D
            MoveForm_MousePosition = e.Location
        End If
    End Sub

    Private Sub lblAppTitle_MouseMove(sender As Object, e As MouseEventArgs)
        If MoveForm Then
            Me.Location = Me.Location + (e.Location - MoveForm_MousePosition)
        End If
    End Sub

    Private Sub lblAppTitle_MouseUp(sender As Object, e As MouseEventArgs)
        If e.Button = MouseButtons.Left Then
            MoveForm = False
            Me.Cursor = Cursors.Default
        End If
    End Sub
#End Region

    Private Sub UpdateUI(UIStep As Integer)
        group_1_SourceDirectory.Enabled = (UIStep And 1)
        group_2_InternalExecutable.Enabled = (UIStep And 2)
        group_3_DestinationDirectory.Enabled = (UIStep And 4)
        group_4_PackageName.Enabled = (UIStep And 8)
        group_5_Build.Enabled = (UIStep And 16)
    End Sub


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Logger.Message(New String("*", 50), 0)
        Logger.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Logger.Message("started", 0)


        Globals.Settings.SourceDirectory = ""
        Globals.Settings.InternalExecutable = ""
        Globals.Settings.DestinationDirectory = ""


        Dim myFileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = myFileVersionInfo.FileVersion


        LoadSettings()
        Copy7zFiles()


        FillThemesCombobox()


        UpdateUI(31)
    End Sub

    Private Sub FillThemesCombobox()
        Dim bOk As Boolean = False
        Dim _index As Integer = 0


        cmbThemes.Items.Clear()

        cmbThemes.Items.Add("No theme change")
        cmbThemes.SelectedIndex = 0


        Try
            'See if there is a theme in the source directory
            Dim _themesmainfolder As String() = IO.Directory.GetDirectories(Globals.Settings.SourceDirectory, "files\SiteKiosk\Skins\Public\Startpages", IO.SearchOption.AllDirectories)

            If _themesmainfolder.Length > 0 Then
                'Found a themes folder, let's collect the themes in there
                For i As Integer = 0 To _themesmainfolder.Length - 1
                    Dim _themes As String() = IO.Directory.GetDirectories(_themesmainfolder(i), "*", IO.SearchOption.TopDirectoryOnly)

                    For j As Integer = 0 To _themes.Length - 1
                        cmbThemes.Items.Add(_themes(j).Replace(_themesmainfolder(i), "").Replace("\", ""))
                    Next
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Copy7zFiles()
        If (Environment.Is64BitProcess) Then
            Console.WriteLine("64-bit process")
        Else
            Console.WriteLine("32-bit process")
        End If

        If Not IO.File.Exists(IO.Path.Combine(Globals.Settings.CurrentDirectory, Globals._7zFiles._7zr_exe)) Then
            If Environment.Is64BitProcess Then
                'IO.File.WriteAllBytes(IO.Path.Combine(Globals.Settings.CurrentDirectory, Globals._7zFiles._7zr_exe), My.Resources._7zr_64_exe)
                IO.File.WriteAllBytes(IO.Path.Combine(Globals.Settings.CurrentDirectory, Globals._7zFiles._7zr_exe), My.Resources._7zr_exe)
            Else
                IO.File.WriteAllBytes(IO.Path.Combine(Globals.Settings.CurrentDirectory, Globals._7zFiles._7zr_exe), My.Resources._7zr_exe)
            End If
        End If

        If Not IO.File.Exists(IO.Path.Combine(Globals.Settings.CurrentDirectory, Globals._7zFiles._7zSD_sfx)) Then
            IO.File.WriteAllBytes(IO.Path.Combine(Globals.Settings.CurrentDirectory, Globals._7zFiles._7zSD_sfx), My.Resources._7zSD_sfx)
        End If
    End Sub

    Private Sub LoadSettings()
        Logger.Message("loading settings", 0)

        Globals.Settings.CorporateFriendly = My.Settings.CorporateFriendly
        Globals.Settings.CurrentDirectory = My.Application.Info.DirectoryPath
        Globals.Settings.SourceDirectory = My.Settings.SourceDirectory
        Globals.Settings.DestinationDirectory = My.Settings.DestinationDirectory

        Logger.Message("SourceDirectory", 1)
        Logger.Message(Globals.Settings.SourceDirectory, 2)

        Logger.Message("DestinationDirectory", 1)
        Logger.Message(Globals.Settings.DestinationDirectory, 2)


        SwitchTheme()


        txtSourceDirectory.Text = Globals.Settings.SourceDirectory
        folderdialogSourceDirectory.SelectedPath = Globals.Settings.SourceDirectory

        filedialogInternalExecutable.InitialDirectory = Globals.Settings.SourceDirectory

        txtDestinationDirectory.Text = Globals.Settings.DestinationDirectory
        folderdialogDestinationDirectory.SelectedPath = Globals.Settings.DestinationDirectory
    End Sub

    Private Sub SaveSettings()
        My.Settings.CorporateFriendly = Globals.Settings.CorporateFriendly
        My.Settings.SourceDirectory = Globals.Settings.SourceDirectory
        My.Settings.DestinationDirectory = Globals.Settings.DestinationDirectory
        My.Settings.Save()
    End Sub

    Private Sub SwitchTheme()
        If Globals.Settings.CorporateFriendly Then
            Me.BackgroundImage = My.Resources.Corporate_bg_bordered_900x600
            Me.Icon = My.Resources.package
            Me.BackColor = Color.FromArgb(60, 190, 179) 'bluegreen
            lblVersion.ForeColor = Color.Black
        Else
            Me.BackgroundImage = My.Resources.CorporateHorse_bg_bordered_900x600
            Me.Icon = My.Resources.horse_face
            Me.BackColor = Color.FromArgb(253, 86, 164) 'pink
            lblVersion.ForeColor = Color.White
        End If
    End Sub

    Private Function CheckSettings(ByRef ErrorString As String) As Boolean
        ErrorString = ""

        If Globals.Settings.SourceDirectory.Equals("") Then
            ErrorString += "Source directory is empty!" & vbCrLf
        Else
            If Not IO.Directory.Exists(Globals.Settings.SourceDirectory) Then
                ErrorString += "Source directory does not exist!" & vbCrLf
            End If
        End If

        If Globals.Settings.InternalExecutable.Equals("") Then
            ErrorString += "Internal executable is empty!" & vbCrLf
        Else
            If Not IO.File.Exists(Globals.Settings.InternalExecutable) Then
                ErrorString += "Internal executabley does not exist!" & vbCrLf
            End If
        End If

        If Globals.Settings.DestinationDirectory.Equals("") Then
            ErrorString += "Destination directory is empty!" & vbCrLf
        Else
            If Not IO.Directory.Exists(Globals.Settings.DestinationDirectory) Then
                ErrorString += "Destination directory does not exist!" & vbCrLf
            End If
        End If

        If Globals.Settings.PackageName.Equals("") Then
            ErrorString += "Package name is empty!" & vbCrLf
        Else
            Dim r As Regex = New Regex("[^A-Za-z0-9.\-_]")

            If r.IsMatch(Globals.Settings.PackageName) Then
                ErrorString += "Package name contains invalid characters!" & vbCrLf
            End If
        End If


        Return ErrorString.Equals("")
    End Function

    Private Sub ShowModalDialog(sTitle As String, sMessage As String, bOk As Boolean, bCancel As Boolean)
        For Each f As Form In Application.OpenForms().OfType(Of frmModal)
            f.Close()
        Next


        Dim frm As New frmModal

        frm.lblTitle.Text = sTitle
        frm.lblMessage.Text = sMessage
        frm.StartPosition = FormStartPosition.CenterParent

        If Not bOk And Not bCancel Then
            bOk = True
        End If

        frm.btnOk.Visible = bOk
        frm.btnCancel.Visible = bCancel

        Dim dlgResult As DialogResult = frm.ShowDialog(Me)
    End Sub

    Private Sub ShowProgressDialog(sText As String)
        lblProgress.Text = sText

        pnlProgressMain.Top = (Me.Height - pnlProgressMain.Height) / 2
        pnlProgressMain.Left = (Me.Width - pnlProgressMain.Width) / 2

        pnlProgressShadow.Top = pnlProgressMain.Top + 6
        pnlProgressShadow.Left = pnlProgressMain.Left + 6

        pnlProgressMain.Visible = True
        pnlProgressShadow.Visible = True
    End Sub

    Private Sub CloseProgressDialog()
        pnlProgressMain.Visible = False
        pnlProgressShadow.Visible = False
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub btnSourceDirectory_Click(sender As Object, e As EventArgs) Handles btnSourceDirectory.Click
        Logger.Message("selecting Source Directory", 0)

        Dim dResult As DialogResult = folderdialogSourceDirectory.ShowDialog()

        If dResult = DialogResult.OK Then
            Globals.Settings.SourceDirectory = folderdialogSourceDirectory.SelectedPath
            txtSourceDirectory.Text = Globals.Settings.SourceDirectory

            filedialogInternalExecutable.InitialDirectory = Globals.Settings.SourceDirectory

            SaveSettings()

            Logger.Message("SourceDirectory", 1)
            Logger.Message(Globals.Settings.SourceDirectory, 2)


            FillThemesCombobox()
        End If
    End Sub

    Private Sub btnInternalExecutable_Click(sender As Object, e As EventArgs) Handles btnInternalExecutable.Click
        Logger.Message("selecting Internal Executable", 0)

        Dim dResult As DialogResult = filedialogInternalExecutable.ShowDialog()

        If dResult = DialogResult.OK Then
            Globals.Settings.InternalExecutable = filedialogInternalExecutable.FileName
            txtInternalExecutable.Text = Globals.Settings.InternalExecutableRelative

            Logger.Message("InternalExecutable", 1)
            Logger.Message(Globals.Settings.InternalExecutable, 2)
            Logger.Message("InternalExecutableRelative", 1)
            Logger.Message(Globals.Settings.InternalExecutableRelative, 2)
        End If
    End Sub

    Private Sub btnChooseDestinationDirectory_Click(sender As Object, e As EventArgs) Handles btnDestinationDirectory.Click
        Logger.Message("selecting Destination Directory", 0)

        Dim dResult As DialogResult = folderdialogDestinationDirectory.ShowDialog()

        If dResult = DialogResult.OK Then
            Globals.Settings.DestinationDirectory = folderdialogDestinationDirectory.SelectedPath
            txtDestinationDirectory.Text = Globals.Settings.DestinationDirectory

            SaveSettings()

            Logger.Message("DestinationDirectory", 1)
            Logger.Message(Globals.Settings.DestinationDirectory, 2)
        End If
    End Sub

    Private Sub txtPackageName_TextChanged(sender As Object, e As EventArgs) Handles txtPackageName.TextChanged
        Globals.Settings.PackageName = txtPackageName.Text
    End Sub

    Private Sub btnBuild_Click(sender As Object, e As EventArgs) Handles btnBuild.Click
        Logger.Message("building package", 0)

        UpdateUI(0)

        Dim sErrorString As String = ""
        If Not CheckSettings(sErrorString) Then
            Logger.Error("ERROR", 1)
            Logger.Text(vbCrLf)
            Logger.Text(sErrorString)
            Logger.Text(vbCrLf)

            ShowModalDialog("Invalid setting", sErrorString, True, False)
            UpdateUI(31)
            Exit Sub
        End If

        ShowProgressDialog("Building, please wait")

        bgworkerBuildPackage.RunWorkerAsync()
    End Sub

    Private Sub filedialogInternalExecutable_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles filedialogInternalExecutable.FileOk
        Console.WriteLine("filedialogInternalExecutable.InitialDirectory: " + filedialogInternalExecutable.InitialDirectory)
        Console.WriteLine("filedialogInternalExecutable.FileName        : " + filedialogInternalExecutable.FileName)

        If Not filedialogInternalExecutable.FileName.ToLower().EndsWith(".exe") Then
            Console.WriteLine("NOT EXE")

            e.Cancel = True
        End If

        If Not filedialogInternalExecutable.FileName.ToLower().StartsWith(Globals.Settings.SourceDirectory.ToLower()) Then
            Console.WriteLine("NOT INITIAL PATH")


            filedialogInternalExecutable.Reset()
            filedialogInternalExecutable.InitialDirectory = Globals.Settings.SourceDirectory

            e.Cancel = True
        End If
    End Sub

    Private Sub frmMain_MouseClick(sender As Object, e As MouseEventArgs) Handles Me.MouseClick
        If e.X >= 0 And e.X < 10 And e.Y >= 0 And e.Y < 10 Then
            Globals.Settings.CorporateFriendly = Not Globals.Settings.CorporateFriendly
            SaveSettings()
            'Application.Restart()
            SwitchTheme()
            Exit Sub
        End If

        '508-341
        '598-430

        If Not Globals.Settings.CorporateFriendly Then
            If e.X >= 508 And e.X <= 598 And e.Y >= 341 And e.Y <= 430 Then
                My.Computer.Audio.Stop()
                My.Computer.Audio.Play(My.Resources.mixkit_scared_stallion_horse_30, AudioPlayMode.Background)
                Console.WriteLine("*neigh*")
            End If
        End If
    End Sub

    Private Sub bgworkerBuildPackage_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworkerBuildPackage.DoWork
        Dim configFileContents As String = My.Resources.default_config
        Dim configFile As String = IO.Path.Combine(Globals.Settings.CurrentDirectory, "package_config.txt")

        configFileContents = configFileContents.Replace("%%PACKAGE_NAME%%", Globals.Settings.PackageName)
        configFileContents = configFileContents.Replace("%%INTERNAL_EXECUTABLE%%", Globals.Settings.InternalExecutableRelative)
        If Globals.Settings.ThemeSelected.Length > 0 Then
            configFileContents = configFileContents.Replace("%%INTERNAL_PARAMETERS%%", " --selected-theme:" & Globals.Settings.ThemeSelected)
        Else
            configFileContents = configFileContents.Replace("%%INTERNAL_PARAMETERS%%", "")
        End If

        configFileContents = configFileContents.Replace("\\", "\")

        If IO.File.Exists(configFile) Then
            IO.File.Delete(configFile)
        End If
        IO.File.WriteAllText(configFile, configFileContents)


        Dim path__7zr_exe As String = IO.Path.Combine(Globals.Settings.CurrentDirectory, Globals._7zFiles._7zr_exe)
        Dim path__7zSD_sfx As String = IO.Path.Combine(Globals.Settings.CurrentDirectory, Globals._7zFiles._7zSD_sfx)
        Dim path__7z_archive As String = IO.Path.Combine(Globals.Settings.DestinationDirectory, Globals.Settings.PackageName & ".7z")
        Dim path__7z_package As String = IO.Path.Combine(Globals.Settings.DestinationDirectory, Globals.Settings.PackageName & ".exe")


        Functions.Cmd.Run(
            """" & path__7zr_exe & """",
            "a """ & path__7z_archive & """ """ & Globals.Settings.SourceDirectory & "\*"" -mx -mf=BCJ2",
            False, True)


        Dim filesIn As String() = {path__7zSD_sfx, configFile, path__7z_archive}
        Functions.Files.Combine(filesIn, path__7z_package)


        'cleanup
        Logger.Message("cleanup", 1)

        Logger.Message(configFile, 2)
        IO.File.Delete(configFile)

        Logger.Message(path__7z_archive, 2)
        IO.File.Delete(path__7z_archive)
    End Sub

    Private Sub bgworkerBuildPackage_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworkerBuildPackage.RunWorkerCompleted
        Shell("explorer """ & Globals.Settings.DestinationDirectory & """", AppWinStyle.NormalFocus)

        Logger.Message("ok", 1)

        CloseProgressDialog()
        UpdateUI(31)
    End Sub

    Private Sub cmbThemes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbThemes.SelectedIndexChanged
        If cmbThemes.SelectedIndex > 0 Then
            Globals.Settings.ThemeSelected = cmbThemes.SelectedItem.ToString
        End If
    End Sub
End Class
