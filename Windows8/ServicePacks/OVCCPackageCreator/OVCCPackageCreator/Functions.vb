﻿Public Class Functions
    Public Class Cmd
        Public Shared Function Run(command As String, arguments As String, permanent As Boolean, quotesAroundEntireArgument As Boolean) As Boolean
            Dim p As Process = New Process()
            Dim pi As ProcessStartInfo = New ProcessStartInfo()

            If quotesAroundEntireArgument Then
                pi.Arguments = " " + If(permanent = True, "/K", "/C") + " """ + command + " " + arguments + """"
            Else
                pi.Arguments = " " + If(permanent = True, "/K", "/C") + " " + command + " " + arguments
            End If

            pi.FileName = "cmd.exe"
            pi.WindowStyle = ProcessWindowStyle.Hidden

            p.StartInfo = pi


            Logger.Message("Running command:", 0)
            Logger.Text(vbCrLf)
            Logger.Text(pi.FileName & " " & pi.Arguments)
            Logger.Text(vbCrLf)

            p.Start()
            p.WaitForExit()

            Return True
        End Function
    End Class

    Public Class Files
        Public Shared Function Combine(filesIn As String(), fileOut As String) As Boolean
            Logger.Message("combining files", 0)

            Using outputStream = IO.File.Create(fileOut)
                For Each fileIn In filesIn
                    Logger.Message("file in", 1)
                    Logger.Message(fileIn, 2)

                    Using inputStream = IO.File.OpenRead(fileIn)
                        inputStream.CopyTo(outputStream)
                    End Using
                Next

                Logger.Message("file out", 1)
                Logger.Message(fileOut, 2)
            End Using

            Return True
        End Function
    End Class
End Class
