﻿Public Class Globals
    Public Class _7zFiles
        Public Shared ReadOnly _7zr_exe As String = "7zr.exe"
        Public Shared ReadOnly _7zSD_sfx As String = "7zSD.sfx"
    End Class

    Public Class Settings
        Public Shared CurrentDirectory As String = ""

        Public Shared SourceDirectory As String = ""
        Public Shared InternalExecutable As String = ""
        Public Shared DestinationDirectory As String = ""

        Public Shared PackageName As String = ""

        Public Shared CorporateFriendly As Boolean = False

        Public Shared ThemeSelected As String = ""

        Public Shared Function InternalExecutableRelative() As String
            Return InternalExecutable.Replace(SourceDirectory, ".\").Replace("\\", "\")
        End Function

        Public Shared InternalParameters As String = ""
    End Class
End Class
