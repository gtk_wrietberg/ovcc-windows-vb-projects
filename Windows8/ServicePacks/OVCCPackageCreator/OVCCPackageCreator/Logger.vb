﻿Public Class Logger
    Private Shared pPrevDepth As Integer = 0

    Public Enum MESSAGE_TYPE
        LOG_DEFAULT = 0
        LOG_WARNING = 1
        LOG_ERROR = 2
        LOG_DEBUG = 6
    End Enum

    Public Shared Sub Debug(text As String, Optional iDepth As Integer = 0)
        _write(text, MESSAGE_TYPE.LOG_DEBUG, iDepth, False)
    End Sub

    Public Shared Sub Message(text As String, Optional iDepth As Integer = 0)
        _write(text, MESSAGE_TYPE.LOG_DEFAULT, iDepth, False)
    End Sub

    Public Shared Sub Warning(text As String, Optional iDepth As Integer = 0)
        _write(text, MESSAGE_TYPE.LOG_WARNING, iDepth, False)
    End Sub

    Public Shared Sub [Error](text As String, Optional iDepth As Integer = 0)
        _write(text, MESSAGE_TYPE.LOG_ERROR, iDepth, False)
    End Sub

    Public Shared Sub Text(text As String)
        _write(text, MESSAGE_TYPE.LOG_DEFAULT, 0, True)
    End Sub

    Private Shared Sub _write(text As String, type As MESSAGE_TYPE, iDepth As Integer, bNoPrefix As Boolean)
        Dim sPrefixType As String = ""
        Dim sPrefixDate As String = ""
        Dim sPrefix As String = ""
        Dim sLogText As String = ""

        If Not bNoPrefix Then
            If iDepth < 0 Then
                iDepth = 0
            End If


            Select Case type
                Case MESSAGE_TYPE.LOG_WARNING
                    sPrefixType = "[*] "
                Case MESSAGE_TYPE.LOG_ERROR
                    sPrefixType = "[!] "
                Case MESSAGE_TYPE.LOG_DEBUG
                    sPrefixType = "[#] "
                Case MESSAGE_TYPE.LOG_DEFAULT
                    sPrefixType = "[.] "
                Case Else
                    sPrefixType = "[?] "
            End Select


            sPrefixDate = Now.ToString & " - "


            sPrefix = sPrefixType & sPrefixDate


            If iDepth < pPrevDepth Then
                For iDepthStep = 1 To iDepth
                    sPrefix = sPrefix & "| "
                Next

                sPrefix = sPrefix & vbCrLf
                sPrefix = sPrefix & sPrefixType & sPrefixDate
            End If

            If iDepth > 0 Then
                For iDepthStep = 1 To iDepth - 1
                    sPrefix = sPrefix & "| "
                Next

                sPrefix = sPrefix & "|-"
            End If

            pPrevDepth = iDepth
        End If

        sLogText = sPrefix & text


        Try
            Dim sFile As String = Application.ProductName & ".log"
            Dim sw As New IO.StreamWriter(sFile, True)
            sw.WriteLine(sLogText)
            sw.Close()
        Catch ex As Exception

        End Try
    End Sub

End Class
