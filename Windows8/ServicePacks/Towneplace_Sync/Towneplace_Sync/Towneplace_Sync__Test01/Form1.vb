﻿Imports System.IO
Imports System.Net
Imports System.Text.RegularExpressions

Public Class Form1
    Private ReadOnly c_URL As String = "http://www.towneplacemap.com/map/show/CKBTS"
    Private ReadOnly c_URL_root As String = "http://www.towneplacemap.com"

    Private ReadOnly c_File_RAW As String = "C:\__temp\__Towneplaza\towneplace_raw.html"
    Private ReadOnly c_File_Parsed As String = "C:\__temp\__Towneplaza\towneplace_parsed.html"

    Private ReadOnly c_UserAgent As String = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)"

    Private mDownloaded As List(Of String)
    Private mMoreParsing_CSS As List(Of String)
    Private mMoreParsing_JS As List(Of String)
    Private mDepth As Integer = 0

    Private Function _CountDirectoriesInPath(sPath As String) As Integer
        Dim sTmp() As String, iCorrection As Integer = 2

        If sPath.Contains("/") Then
            sTmp = sPath.Split(New Char() {"/"c})

            'If sPath.EndsWith("/") Then
            '    iCorrection = 1
            'Else
            '    iCorrection = 2
            'End If
        Else
            sTmp = sPath.Split(New Char() {"\"c})

            'If sPath.EndsWith("\") Then
            '    iCorrection = 1
            'Else
            '    iCorrection = 2
            'End If

        End If

        Dim iTmp As Integer = sTmp.Count - iCorrection

        If iTmp < 0 Then iTmp = 0

        Return iTmp
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        mDownloaded = New List(Of String)
        mMoreParsing_CSS = New List(Of String)
        mMoreParsing_JS = New List(Of String)

        Try
            Dim request As HttpWebRequest = WebRequest.Create(c_URL)

            request.UserAgent = c_UserAgent

            Using response As WebResponse = request.GetResponse()
                Using reader As New StreamReader(response.GetResponseStream())
                    Dim html As String = reader.ReadToEnd()

                    File.WriteAllText(c_File_RAW, html)
                End Using
            End Using

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try



        Dim streamRead As StreamReader
        streamRead = My.Computer.FileSystem.OpenTextFileReader(c_File_RAW)
        'MsgBox(streamRead.ReadToEnd)

        Dim sLine As String = "", sLink As String = ""

        Dim re_href1 As Regex = New Regex("(.+)(href|src)(="")(/)([^""]+)("")(.*)")
        '                                  1   2         3    4  5       6   7 

        Dim re_url1 As Regex = New Regex("(.+)(url\(""|url\()(/)([^\)""]+)(""\)|\))(.+)")
        '                                 1   2              3  4         5        6


        Dim streamWrite As StreamWriter = New StreamWriter(c_File_Parsed, False)
        'streamWrite.WriteLine("Here is the first string.")
        'streamWrite.Close()

        Do
            sLine = streamRead.ReadLine
            If sLine Is Nothing Then
                Exit Do
            End If

            If sLine.Contains("<meta http-equiv=""X-UA-Compatible"" content=""IE=5"">") Then
                sLine = "<meta http-equiv=""X-UA-Compatible"" content=""IE=11"">"
            End If


            If re_href1.Match(sLine).Success Then
                sLink = c_URL_root & re_href1.Replace(sLine, "$4$5")
                sLine = re_href1.Replace(sLine, "$1$2$3$5$6$7")
            End If

            If re_url1.Match(sLine).Success Then
                sLink = c_URL_root & re_url1.Replace(sLine, "$3$4")
                sLine = re_url1.Replace(sLine, "$1$2$4$5$6")
            End If


            'If re_href2.Match(sLine).Success Then
            '    sLink = c_URL & "/" & re_href2.Replace(sLine, "$1")
            'End If

            'If re_src1.Match(sLine).Success Then
            '    sLink = c_URL & "/" & re_src1.Replace(sLine, "$1")
            'End If

            'If re_src2.Match(sLine).Success Then
            '    sLink = c_URL & "/" & re_src2.Replace(sLine, "$1")
            'End If



            If Not sLink.Equals("") Then
                Console.WriteLine("downloading '" & sLink & "'")
                If _DownloadThing(sLink) Then
                    Console.WriteLine("ok")
                Else
                    Console.WriteLine("fail")
                End If
            End If



            streamWrite.WriteLine(sLine)
        Loop Until sLine Is Nothing

        streamWrite.Close()
        streamRead.Close()


        'another iteration for the js and css files


        For Each sFile As String In mMoreParsing_CSS
            Console.WriteLine("parse: " & sFile)

            mDepth = _CountDirectoriesInPath(sFile) - _CountDirectoriesInPath(c_File_Parsed)

            Console.WriteLine("depth: " & mDepth.ToString)

            _ParseFile_CSS(sFile)
        Next


    End Sub

    Private Function _ParseFile_CSS(sFile As String) As Boolean
        Dim lLines As New List(Of String)
        Dim streamRead As StreamReader
        Dim sLine As String = "", sLink As String = ""
        'Dim re_url As Regex = New Regex("(url\("")(/)([^""]+)(""\))")
        Dim re_url As Regex = New Regex("(url\(""|url\()(/)([^\)""]+)(""\)|\))") ' matches url("...") and url(...)

        streamRead = My.Computer.FileSystem.OpenTextFileReader(sFile)


        Do
            sLine = streamRead.ReadLine
            If sLine Is Nothing Then
                Exit Do
            End If


            If re_url.Match(sLine).Success Then
                For Each m As Match In re_url.Matches(sLine)
                    sLink = c_URL_root & m.Groups.Item(2).ToString() & m.Groups.Item(3).ToString()
                    Console.WriteLine("downloading '" & sLink & "'")
                    If _DownloadThing(sLink) Then
                        Console.WriteLine("ok")
                    Else
                        Console.WriteLine("fail")
                    End If
                Next

                sLine = re_url.Replace(sLine, New MatchEvaluator(AddressOf _Match_css_url))
            End If


            lLines.Add(sLine)
        Loop Until sLine Is Nothing

        streamRead.Close()


        Dim streamWrite As StreamWriter = New StreamWriter(sFile, False)

        For Each sNewLine As String In lLines
            streamWrite.WriteLine(sNewLine)
        Next

        streamWrite.Close()

        Return True
    End Function

    Private Function _Match_css_url(ByVal m As Match) As String
        Dim sDots As String = ""

        For i As Integer = 0 To mDepth - 1
            sDots &= "../"
        Next

        Return m.Groups.Item(1).ToString() & sDots & m.Groups.Item(3).ToString() & m.Groups.Item(4).ToString()
    End Function


    Private Function _DownloadThing(sUrl As String) As Boolean
        Try
            If mDownloaded.Contains(sUrl) Then
                'skip
                Console.WriteLine("skipped")
                Return True
                Exit Function
            End If

            Dim uri As New Uri(sUrl)
            Dim sFile As String = uri.AbsolutePath

            If sFile.StartsWith("/") Then
                sFile = sFile.Substring(1)
            End If



            Dim sPath As String = IO.Path.GetDirectoryName(c_File_Parsed) & "\" & IO.Path.GetDirectoryName(sFile.Replace("/", "\"))

            IO.Directory.CreateDirectory(sPath)

            Console.WriteLine("creating folder '" & sPath & "'")

            sFile = sPath & "\" & IO.Path.GetFileName(sFile)


            Dim clnt As WebClient = New WebClient

            clnt.DownloadFile(sUrl, sFile)

            mDownloaded.Add(sUrl)

            Dim sExtension As String = IO.Path.GetExtension(sFile)

            'If sExtension.Equals(".css") Or sExtension.Equals(".js") Then
            If sExtension.Equals(".css") Then
                mMoreParsing_CSS.Add(sFile)
            End If

            If sExtension.Equals(".js") Then
                mMoreParsing_JS.Add(sFile)
            End If

        Catch ex As Exception

            Return False
        End Try

        Return True
    End Function


    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim myClient As WebClient = New WebClient()

        myClient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)")

        Dim myResponse As Stream = myClient.OpenRead(c_URL)
        Dim sr As StreamReader = New StreamReader(myResponse)

        MsgBox(sr.ReadToEnd)

        sr.Close()
        myResponse.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim rx As New Regex(TextBox2.Text)

        If rx.Match(TextBox1.Text).Success Then
            MsgBox("YES!!!")
        Else
            MsgBox("POEP!!!")

        End If

        For Each m As Match In rx.Matches(TextBox1.Text)
            MsgBox("download: " & c_URL_root & m.Groups.Item(2).ToString() & m.Groups.Item(3).ToString())
        Next

        TextBox3.Text = rx.Replace(TextBox1.Text, New MatchEvaluator(AddressOf _Match_poep))
    End Sub

    Private Function _Match_poep(ByVal m As Match) As String
        Return m.Groups.Item(1).ToString() & ".." & m.Groups.Item(2).ToString() & m.Groups.Item(3).ToString() & m.Groups.Item(4).ToString()
        'Return m.ToString.Replace("""/", """../")
    End Function

    Private Sub TextBox3_TextChanged(sender As Object, e As EventArgs) Handles TextBox3.TextChanged

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        MsgBox(_CountDirectoriesInPath(TextBox4.Text))
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim uri As New Uri(TextBox5.Text)

        MsgBox(uri.Scheme & Uri.SchemeDelimiter & uri.Host)
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim sLine As String = TextBox6.Text
        Dim re_Compat As Regex = New Regex("(\<a href=""javascript:showmaplocation\('[0-9]+', ')([^']+)(.+)")

        If re_Compat.Match(sLine).Success Then
            sLine = re_Compat.Replace(sLine, "$2")

            MsgBox(sLine)
        Else
            MsgBox("FAIL")
        End If


    End Sub
End Class
