﻿Imports System.IO
Imports System.Net
Imports System.Text.RegularExpressions

Public Class TP_Parser
    Private Shared mMoreDownload As New List(Of String)
    Private Shared mMoreParsing_CSS As New List(Of String)
    Private Shared mMoreParsing_JS As New List(Of String)

    Private Shared mMapLocations As New List(Of Integer)

    Private Shared mDepth As Integer = 0



    Private Shared ReadOnly c_UserAgent As String = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)"


    Private Shared re_Compat As Regex = New Regex("(\<meta http-equiv=""X-UA-Compatible"" content=""IE=)([0-9]+)("">)")
    '                                              1                                                    2       3

    Private Shared re_href1 As Regex = New Regex("(.+)(href|src)(\s*=\s*"")(/)([^""]+)("")(.*)")
    '                                             1   2         3    4  5       6   7 

    Private Shared re_url1 As Regex = New Regex("(.+)(url\(""|url\()(/)([^\)""]+)(""\)|\))(.+)")
    '                                            1   2              3  4         5        6

    Private Shared re_map As Regex = New Regex("(\<a href=""javascript:showmaplocation\('[0-9]+', ')([^']+)(.+)")
    '                                           1                                                   2      3

    Private Shared pattern_mapReturnToIndex As String = "(\<a href="")(javascript:showmaplocation\('[0-9]+', ')([^""]+)(.+)"

    ' /map/show/CKBTS?map=on



    Private Shared mUrl As String
    Public Shared Property URL() As String
        Get
            Return mUrl
        End Get
        Set(ByVal value As String)
            mUrl = value
        End Set
    End Property

    Private Shared mRootFile As String
    Public Shared Property RootFile() As String
        Get
            Return mRootFile
        End Get
        Set(ByVal value As String)
            mRootFile = value
        End Set
    End Property

    Public Enum PARSER_ACTION
        _01_ParseRoot = 0
        _02_Download_Root
        _03_ParseCss_Root
        _04_Download_Css_Root
        _05_ParseMaps
        _06_ParseCss_Maps
        _07_Download_Css_Maps
    End Enum

    Public Shared Function Action(pAction As PARSER_ACTION) As Boolean
        Select Case pAction
            Case PARSER_ACTION._01_ParseRoot
                'Load root html, edit it, and collect stuff
                _ParseRoot()
            Case PARSER_ACTION._02_Download_Root
                'Download stuff found in root html
                _DownloadAllItems()
            Case PARSER_ACTION._03_ParseCss_Root
                'Parse css files for more stuff
                _ParseCssFiles()
            Case PARSER_ACTION._04_Download_Css_Root
                'Download stuff found in css files
                _DownloadAllItems()
            Case PARSER_ACTION._05_ParseMaps
                'Parse map locations for more stuff
                _ParseMapLocations()
            Case PARSER_ACTION._06_ParseCss_Maps
                'Parse css files for more stuff
                _ParseCssFiles()
            Case PARSER_ACTION._07_Download_Css_Maps
                'Download stuff found in css files
                _DownloadAllItems()
            Case Else
                Return False
        End Select

        Return True
    End Function





    Private Shared Function _ParseRoot() As Boolean
        Try
            Dim request As HttpWebRequest = WebRequest.Create(mUrl)

            request.UserAgent = c_UserAgent

            Using response As WebResponse = request.GetResponse()
                Using reader As New StreamReader(response.GetResponseStream())
                    Dim html As String = reader.ReadToEnd()

                    File.WriteAllText(mRootFile, html)
                End Using
            End Using

        Catch ex As Exception
            Return False
        End Try


        'make copy of raw file
        IO.File.Copy(mRootFile, mRootFile & ".bak", True)


        Dim streamRead As StreamReader
        streamRead = My.Computer.FileSystem.OpenTextFileReader(mRootFile)


        Dim sLine As String = "", sLink As String = ""
        Dim sLines As New List(Of String)


        Do
            sLink = ""
            sLine = streamRead.ReadLine
            If sLine Is Nothing Then
                Exit Do
            End If

            'If sLine.Contains("<meta http-equiv=""X-UA-Compatible"" content=""IE=5"">") Then
            '    sLine = "<meta http-equiv=""X-UA-Compatible"" content=""IE=11"">"
            'End If

            If re_Compat.Match(sLine).Success Then
                sLine = re_Compat.Replace(sLine, "$1_COULD_NOT_BE_BOTHERED_TO_FIND_THE_PROPER_SOLUTION_11$3")
                sLine = sLine.Replace("_COULD_NOT_BE_BOTHERED_TO_FIND_THE_PROPER_SOLUTION_", "")
            End If

            If re_href1.Match(sLine).Success Then
                sLink = _TrimUrl(mUrl) & re_href1.Replace(sLine, "$4$5")
                sLine = re_href1.Replace(sLine, "$1$2$3$5$6$7")
            End If

            If re_url1.Match(sLine).Success Then
                sLink = _TrimUrl(mUrl) & re_url1.Replace(sLine, "$3$4")
                sLine = re_url1.Replace(sLine, "$1$2$4$5$6")
            End If

            If re_map.Match(sLine).Success Then
                mMapLocations.Add(re_map.Replace(sLine, "$2"))
            End If


            If Not sLink.Equals("") Then
                If Not mMoreDownload.Contains(sLink) Then
                    mMoreDownload.Add(sLink)
                End If
            End If

            sLines.Add(sLine)

        Loop Until sLine Is Nothing


        streamRead.Close()


        'overwrite that mofo
        Dim streamWrite As StreamWriter = New StreamWriter(mRootFile, False)

        For Each tmpLine As String In sLines
            streamWrite.WriteLine(tmpLine)
        Next

        streamWrite.Close()


        Return True
    End Function

    Private Shared Function _ParseMapLocations() As Boolean
        For Each iMapItem As Integer In mMapLocations
            _ParseMapItem(iMapItem)
        Next

        Return True
    End Function

    Private Shared Function _ParseMapItem(mapItem As Integer) As Boolean
        Dim mapUrl As String = ""
        Dim mapFile As String = ""
        Dim sDots As String = ""

        Try
            mapUrl = _TrimUrl(mUrl) & "/maplocation/show/" & mapItem.ToString
            mapFile = IO.Path.GetDirectoryName(mRootFile) & "\maplocation\show\" & mapItem.ToString & ".html"

            mDepth = _CountDirectoriesInPath(mapFile) - _CountDirectoriesInPath(mRootFile)
            For i As Integer = 0 To mDepth - 1
                sDots &= "../"
            Next

            IO.Directory.CreateDirectory(IO.Path.GetDirectoryName(mRootFile) & "\maplocation\show")



            Dim request As HttpWebRequest = WebRequest.Create(mapUrl)

            request.UserAgent = c_UserAgent

            Using response As WebResponse = request.GetResponse()
                Using reader As New StreamReader(response.GetResponseStream())
                    Dim html As String = reader.ReadToEnd()

                    File.WriteAllText(mapFile, html)
                End Using
            End Using

        Catch ex As Exception
            Return False
        End Try



        Dim streamRead As StreamReader
        streamRead = My.Computer.FileSystem.OpenTextFileReader(mapFile)

        Dim sLine As String = "", sLink As String = ""
        Dim sLines As New List(Of String)




        Do
            sLink = ""
            sLine = streamRead.ReadLine
            If sLine Is Nothing Then
                Exit Do
            End If

            'If sLine.Contains("<meta http-equiv=""X-UA-Compatible"" content=""IE=5"">") Then
            '    sLine = "<meta http-equiv=""X-UA-Compatible"" content=""IE=11"" />"
            'End If

            If re_Compat.Match(sLine).Success Then
                sLine = re_Compat.Replace(sLine, "$1_COULD_NOT_BE_BOTHERED_TO_FIND_THE_PROPER_SOLUTION_11$3")
                sLine = sLine.Replace("_COULD_NOT_BE_BOTHERED_TO_FIND_THE_PROPER_SOLUTION_", "")
            End If

            If re_href1.Match(sLine).Success Then
                sLink = _TrimUrl(mUrl) & re_href1.Replace(sLine, "$4$5")
                sLine = re_href1.Replace(sLine, "$1$2$3" & sDots & "$5$6$7")
            End If

            If re_url1.Match(sLine).Success Then
                sLink = _TrimUrl(mUrl) & re_url1.Replace(sLine, "$3$4")
                sLine = sDots & re_url1.Replace(sLine, "$1$2" & sDots & "$4$5$6")
            End If

            'If re_map.Match(sLine).Success Then
            '    mMapLocations.Add(re_map.Replace(sLine, "$2"))
            'End If


            If Not sLink.Equals("") Then
                If Not mMoreDownload.Contains(sLink) Then
                    mMoreDownload.Add(sLink)
                End If
            End If


            sLines.Add(sLine)
        Loop Until sLine Is Nothing


        streamRead.Close()


        'overwrite the mofo
        Dim streamWrite As StreamWriter = New StreamWriter(mapFile, False)

        For Each tmpLine As String In sLines
            streamWrite.WriteLine(tmpLine)
        Next

        streamWrite.Close()


        Return True
    End Function


    Private Shared Function _ParseCssFiles() As Boolean
        Dim lTmp As New List(Of String)

        'Make a copy so we can safely remove items from mMoreDownload
        For Each sTmp As String In mMoreParsing_CSS
            lTmp.Add(sTmp)
        Next


        For Each sFile As String In lTmp
            Console.WriteLine("parse: " & sFile)

            mDepth = _CountDirectoriesInPath(sFile) - _CountDirectoriesInPath(mRootFile)

            Console.WriteLine("depth: " & mDepth.ToString)

            _ParseFile_CSS(sFile)

            mMoreParsing_CSS.Remove(sFile)
        Next

        Return True
    End Function

    Private Shared Function _DownloadAllItems() As Boolean
        Dim lTmp As New List(Of String)

        'Make a copy so we can safely remove items from mMoreDownload
        For Each sTmp As String In mMoreDownload
            lTmp.Add(sTmp)
        Next


        For Each sLink As String In lTmp
            Console.WriteLine("downloading '" & sLink & "'")
            If _DownloadItem(sLink) Then
                Console.WriteLine("ok")
            Else
                Console.WriteLine("fail")
            End If
        Next

        Return True
    End Function

    Private Shared Function _ParseFile_CSS(sFile As String) As Boolean
        Dim lLines As New List(Of String)
        Dim streamRead As StreamReader
        Dim sLine As String = "", sLink As String = ""
        'Dim re_url As Regex = New Regex("(url\("")(/)([^""]+)(""\))")
        Dim re_url As Regex = New Regex("(url\(""|url\()(/)([^\)""]+)(""\)|\))") ' matches url("...") and url(...)

        streamRead = My.Computer.FileSystem.OpenTextFileReader(sFile)


        Do
            sLine = streamRead.ReadLine
            If sLine Is Nothing Then
                Exit Do
            End If


            If re_url.Match(sLine).Success Then
                For Each m As Match In re_url.Matches(sLine)
                    sLink = _TrimUrl(mUrl) & m.Groups.Item(2).ToString() & m.Groups.Item(3).ToString()

                    _AddDownloadItem(sLink)
                Next

                sLine = re_url.Replace(sLine, New MatchEvaluator(AddressOf _Match_css_url))
            End If


            lLines.Add(sLine)
        Loop Until sLine Is Nothing

        streamRead.Close()


        Dim streamWrite As StreamWriter = New StreamWriter(sFile, False)

        For Each sNewLine As String In lLines
            streamWrite.WriteLine(sNewLine)
        Next

        streamWrite.Close()

        Return True
    End Function

    Private Shared Function _Match_css_url(ByVal m As Match) As String
        Dim sDots As String = ""

        For i As Integer = 0 To mDepth - 1
            sDots &= "../"
        Next

        Return m.Groups.Item(1).ToString() & sDots & m.Groups.Item(3).ToString() & m.Groups.Item(4).ToString()
    End Function



    Private Shared Function _CountDirectoriesInPath(sPath As String) As Integer
        Dim sTmp() As String, iCorrection As Integer = 2

        If sPath.Contains("/") Then
            sTmp = sPath.Split(New Char() {"/"c})
        Else
            sTmp = sPath.Split(New Char() {"\"c})
        End If

        Dim iTmp As Integer = sTmp.Count - iCorrection

        If iTmp < 0 Then iTmp = 0

        Return iTmp
    End Function

    Private Shared Function _TrimUrl(sUrl As String) As String
        Try
            Dim uri As New Uri(sUrl)

            Return uri.Scheme & Uri.SchemeDelimiter & uri.Host
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Shared Function _AddDownloadItem(sLink As String) As Boolean
        If Not mMoreDownload.Contains(sLink) Then
            mMoreDownload.Add(sLink)
        End If

        Return True
    End Function

    Private Shared Function _RemoveDownloadItem(sLink As String) As Boolean
        Return mMoreDownload.Remove(sLink)
    End Function

    Private Shared Function _DownloadItem(sLink As String) As Boolean
        Dim bRet As Boolean = False

        Try
            Dim uri As New Uri(sLink)
            Dim sFile As String = uri.AbsolutePath

            If sFile.StartsWith("/") Then
                sFile = sFile.Substring(1)
            End If



            Dim sPath As String = IO.Path.GetDirectoryName(mRootFile) & "\" & IO.Path.GetDirectoryName(sFile.Replace("/", "\"))

            IO.Directory.CreateDirectory(sPath)

            Console.WriteLine("creating folder '" & sPath & "'")

            sFile = sPath & "\" & IO.Path.GetFileName(sFile)


            Dim clnt As WebClient = New WebClient

            clnt.DownloadFile(sLink, sFile)


            Dim sExtension As String = IO.Path.GetExtension(sFile)

            If sExtension.Equals(".css") Then
                mMoreParsing_CSS.Add(sFile)
            End If

            If sExtension.Equals(".js") Then
                mMoreParsing_JS.Add(sFile)
            End If

            bRet = True
        Catch ex As Exception

        End Try


        _RemoveDownloadItem(sLink)

        Return bRet
    End Function

End Class
