﻿Imports System.ComponentModel

Public Class Form1
#Region "Thread Safe stuff"
    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Public Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text])
        End If
    End Sub
#End Region

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TP_Parser.URL = "http://www.towneplacemap.com/map/show/CKBTS"
        TP_Parser.RootFile = "C:\__TownPlace_Sync_TEST01\index.html"

        Button1.Enabled = False

        bgWorker_Parser.RunWorkerAsync()


    End Sub

    Private Sub bgWorker_Parser_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker_Parser.DoWork
        Dim aActions As Array = System.Enum.GetValues(GetType(TP_Parser.PARSER_ACTION))

        For Each iAction As Integer In aActions
            bgWorker_Parser.ReportProgress(0, "Install step: " & System.Enum.GetName(GetType(TP_Parser.PARSER_ACTION), iAction) & vbCrLf)
            TP_Parser.Action(iAction)
            bgWorker_Parser.ReportProgress(0, "ok" & vbCrLf)

            Threading.Thread.Sleep(1000)
        Next
    End Sub

    Private Sub bgWorker_Parser_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles bgWorker_Parser.ProgressChanged
        LogToProcessTextBox(e.UserState.ToString)
    End Sub

    Private Sub bgWorker_Parser_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgWorker_Parser.RunWorkerCompleted
        Button1.Enabled = True
    End Sub
End Class
