﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using mshtml;
using System.Net;
using System.IO;

namespace browser_test01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            poepzooi();
        }

        private void poepzooi()
        {
            // Create a request for the URL.            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://www.towneplacemap.com/map/show/CKBTS");
            // If required by the server, set the credentials.   
            request.Credentials = CredentialCache.DefaultCredentials;
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";

            // Get the response.   
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Display the status.   
            Console.WriteLine(response.StatusDescription);
            // Get the stream containing content returned by the server.   
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.   
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.   
            string responseFromServer = reader.ReadToEnd();
            // Display the content.   
            Console.WriteLine(responseFromServer);
            // Cleanup the streams and the response.   
            reader.Close();
            dataStream.Close();
            response.Close();

            //reads the html into an html document to enable parsing   
            IHTMLDocument2 doc = new HTMLDocumentClass();
            doc.write(new object[] { responseFromServer });
            doc.close();

            //loops through each element in the document to check if it qualifies for the attributes to be set   
            foreach (IHTMLElement el in (IHTMLElementCollection)doc.all)
            {
                // check to see if all the desired attributes were found with the correct values   
                if (el.tagName == "META")
                {
                    HTMLMetaElement meta = (HTMLMetaElement)el;
                    Console.WriteLine("Content " + meta.content + "<br/>");
                }

            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
