﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GuestPasswordBugfix")>
<Assembly: AssemblyDescription("Fixes bug in Guest password system")>
<Assembly: AssemblyCompany("GuestTek")>
<Assembly: AssemblyProduct("GuestPasswordBugfix01")>
<Assembly: AssemblyCopyright("Copyright ©  2021")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ddc6645f-e8e0-4b34-9b5d-a08e978477de")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.2.21180")>
<Assembly: AssemblyFileVersion("1.0.2.21180")>
