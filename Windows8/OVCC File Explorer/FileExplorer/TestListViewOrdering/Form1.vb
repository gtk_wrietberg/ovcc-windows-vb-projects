﻿Public Class Form1
    Private lvwColumnSorter As ListViewColumnSorter

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lvwColumnSorter = New ListViewColumnSorter()
        Me.ListView1.ListViewItemSorter = lvwColumnSorter


        Dim columnheader As ColumnHeader    ' Used for creating column headers.
        Dim listviewitem As ListViewItem    ' Used for creating ListView items.

        ' Make sure that the view is set to show details.
        ListView1.View = View.Details

        ' Create some ListView items consisting of first and last names.
        listviewitem = New ListViewItem("Mike")
        listviewitem.SubItems.Add("Nash")
        listviewitem.SubItems.Add("1 kb")
        listviewitem.SubItems.Add(1000)
        Me.ListView1.Items.Add(listviewitem)

        listviewitem = New ListViewItem("Kim")
        listviewitem.SubItems.Add("Abercrombie")
        listviewitem.SubItems.Add("17 b")
        listviewitem.SubItems.Add(17)
        Me.ListView1.Items.Add(listviewitem)

        listviewitem = New ListViewItem("Sunil")
        listviewitem.SubItems.Add("Koduri")
        listviewitem.SubItems.Add("300 kb")
        listviewitem.SubItems.Add(300000)
        Me.ListView1.Items.Add(listviewitem)

        listviewitem = New ListViewItem("Birgit")
        listviewitem.SubItems.Add("Seidl")
        listviewitem.SubItems.Add("2 MB")
        listviewitem.SubItems.Add(2000000)
        Me.ListView1.Items.Add(listviewitem)

        ' Create some column headers for the data.
        columnheader = New ColumnHeader()
        columnheader.Text = "First Name"
        columnheader.Width = 200
        Me.ListView1.Columns.Add(columnheader)

        columnheader = New ColumnHeader()
        columnheader.Text = "Last Name"
        columnheader.Width = 200
        Me.ListView1.Columns.Add(columnheader)

        columnheader = New ColumnHeader()
        columnheader.Text = "Size"
        Me.ListView1.Columns.Add(columnheader)

        columnheader = New ColumnHeader()
        columnheader.Text = "(hidden)"
        columnheader.Width = 0
        Me.ListView1.Columns.Add(columnheader)


        ' Loop through and size each column header to fit the column header text.
        For Each columnheader In Me.ListView1.Columns
            'columnheader.Width = -2
        Next
    End Sub

    Private Sub ListView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListView1.SelectedIndexChanged

    End Sub

    Private Sub ListView1_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles ListView1.ColumnClick
        ' Determine if the clicked column is already the column that is 
        ' being sorted.
        Dim _column As Integer

        If e.Column = 2 Then
            _column = 3
        End If


        'If (e.Column = lvwColumnSorter.SortColumn) Then
        '    ' Reverse the current sort direction for this column.
        '    If (lvwColumnSorter.Order = SortOrder.Ascending) Then
        '        lvwColumnSorter.Order = SortOrder.Descending
        '    Else
        '        lvwColumnSorter.Order = SortOrder.Ascending
        '    End If
        'Else
        '    ' Set the column number that is to be sorted; default to ascending.
        '    lvwColumnSorter.SortColumn = e.Column
        '    lvwColumnSorter.Order = SortOrder.Ascending
        'End If

        If (_column = lvwColumnSorter.SortColumn) Then
            ' Reverse the current sort direction for this column.
            If (lvwColumnSorter.Order = SortOrder.Ascending) Then
                lvwColumnSorter.Order = SortOrder.Descending
            Else
                lvwColumnSorter.Order = SortOrder.Ascending
            End If
        Else
            ' Set the column number that is to be sorted; default to ascending.
            lvwColumnSorter.SortColumn = _column
            lvwColumnSorter.Order = SortOrder.Ascending
        End If


        ' Perform the sort with these new sort options.
        Me.ListView1.Sort()
    End Sub

    Private Sub ListView1_ColumnWidthChanging(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnWidthChangingEventArgs) Handles ListView1.ColumnWidthChanging
        e.Cancel = True
        e.NewWidth = Me.ListView1.Columns(e.ColumnIndex).Width

        'If Me.ListView1.Columns(e.ColumnIndex).Width = 0 Then
        '    e.Cancel = True
        '    e.NewWidth = Me.ListView1.Columns(e.ColumnIndex).Width
        'End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Label1.Text = "one moment please" & vbCrLf & "starting application"
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MsgBox(Helpers.FilesAndFolders.Folder.IsRootFolder(TextBox1.Text))
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        ListView1.LabelEdit = True



        Dim listviewitem As ListViewItem
        listviewitem = New ListViewItem("New Item")
        listviewitem.SubItems.Add("")
        listviewitem.SubItems.Add("")
        listviewitem.SubItems.Add(0)
        listviewitem.Selected = True
        listviewitem.Focused = True
        Me.ListView1.Items.Add(ListViewItem)
    End Sub
End Class
