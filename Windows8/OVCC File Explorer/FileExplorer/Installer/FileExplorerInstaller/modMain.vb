﻿Module modMain
    Private oCopyFiles As Helpers.FilesAndFolders.CopyFiles

    Private ReadOnly cFileExplorerName As String = "FileExplorer"
    Private ReadOnly cFileExplorerDescription As String = "OVCC FileExplorer"
    Private pFileExplorerPath As String = ""

    Private ReadOnly cImageApp As String = "%%PROGRAM_FILES%%\Classic Paint\mspaint1.exe"

    Public Sub Main()
        Initialise()

        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Copy files", 0)

        Dim sAppPath As String = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\FileExplorer\"

        oCopyFiles = New Helpers.FilesAndFolders.CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory
        oCopyFiles.SourceDirectory = IO.Path.Combine(My.Application.Info.DirectoryPath, "files")
        oCopyFiles.DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\"

        Helpers.Logger.WriteMessage("FileExplorer", 1)
        oCopyFiles.CopyFiles()

        pFileExplorerPath = sAppPath & "FileExplorer.exe"


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Registry", 0)
        Helpers.Logger.WriteMessage("Settings", 1)


        Settings.Generic.ContextMenu = True
        Settings.Generic.Debugging = False
        Settings.Generic.DirectoriesCollapsable = True
        Settings.Generic.ShowNetworkDrives = False
        Settings.Generic.StartWithDownloadsFolder = True
        Settings.Generic.AppendUnzippedFolderWithFullDate = False
        Settings.Generic.DetectFolderChanges = False
        Settings.Generic.ShowHiddenFiles = False
        Settings.Generic.ShowSystemFiles = False
        Settings.Generic.ShowTemporaryFiles = False
        Settings.Generic.AllowFileDelete = True
        Settings.Generic.AllowPrinting = True


        Settings.Extensions.Load()
        Settings.Extensions.Add("png", cImageApp)
        Settings.Extensions.Add("bmp", cImageApp)
        Settings.Extensions.Add("gif", cImageApp)
        Settings.Extensions.Add("jpg", cImageApp)
        Settings.Extensions.Add("jpeg", cImageApp)
        Settings.Extensions.Save()


        Settings.Paths.Names(0) = "Desktop"
        Settings.Paths.Paths(0) = "%USERPROFILE%\Desktop"
        Settings.Paths.Icons(0) = Settings.IconIndices.Desktop

        Settings.Paths.Names(1) = "Documents"
        Settings.Paths.Paths(1) = "%USERPROFILE%\Documents"
        Settings.Paths.Icons(1) = Settings.IconIndices.Documents

        Settings.Paths.Names(2) = "Pictures"
        Settings.Paths.Paths(2) = "%USERPROFILE%\Pictures"
        Settings.Paths.Icons(2) = Settings.IconIndices.Pictures


        Settings.Save()

        Settings.InstallerCleanup()


        '==================================================================================================================================================================================
#Region "Permissions"
        Helpers.Logger.WriteMessage("Permissions", 0)
        Helpers.Logger.WriteMessage(sAppPath & "\_logs", 1)

        If IO.Directory.Exists(sAppPath & "\_logs") Then
            If Helpers.FilesAndFolders.Permissions.FolderSecurity_ADDandINHERIT__Everyone(sAppPath & "\_logs", Security.AccessControl.FileSystemRights.FullControl, Security.AccessControl.AccessControlType.Allow, False) Then
                Helpers.Logger.WriteMessage("ok", 2)
            Else
                Helpers.Logger.WriteError("Error while changing the ACL", 2)
                Helpers.Logger.WriteError(Helpers.Errors.GetLast, 3)
            End If
        Else
            Helpers.Logger.WriteError("Folder not found!", 2)
        End If
#End Region


        '==================================================================================================================================================================================
#Region "StartmenuDialog"
        Helpers.Logger.WriteMessage("StartmenuDialog", 0)

        Try
            Dim lines As New List(Of String)
            Dim linecount As Integer = 0, linefound As Boolean = False, alreadypatched As Boolean = False
            Dim StartmenuDialogPath As String = "C:\Program Files (x86)\SiteKiosk\Skins\Win7IE8\StartmenuDialog.html"

            If Not IO.File.Exists(StartmenuDialogPath) Then
                StartmenuDialogPath = "C:\Program Files\SiteKiosk\Skins\Win7IE8\StartmenuDialog.html"
            End If
            If Not IO.File.Exists(StartmenuDialogPath) Then
                Throw New Exception("StartmenuDialog.html not found!")
            End If

            Helpers.Logger.WriteMessage(StartmenuDialogPath, 1)

            Helpers.FilesAndFolders.Backup.File(StartmenuDialogPath, g_BackupDirectory)


            Helpers.Logger.WriteMessage("patching", 2)
            Using sr As New IO.StreamReader(StartmenuDialogPath)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("// = commented out for OVCC FileExplorer =") Then
                        alreadypatched = True

                        Helpers.Logger.WriteWarning("already patched!!!", 3)

                        Exit While
                    End If

                    If Not linefound And line.Contains("//Show / Hide 'File Manager' button") Then
                        linefound = True
                        Helpers.Logger.WriteMessage("found old sk explorer", 3)
                    End If

                    If linefound Then
                        If linecount >= 0 And linecount <= 2 Then
                            If linecount = 0 Then
                                'add an extra couple of lines
                                lines.Add("  ")
                                lines.Add("// =======================================")
                                lines.Add("// = commented out for OVCC FileExplorer =")
                                lines.Add("// =======================================")
                                lines.Add("  ")
                            End If

                            line = "//          " & line
                            linecount += 1

                            Helpers.Logger.WriteMessage("line commented out", 3)
                        End If
                    End If

                    lines.Add(line)

                    If linefound Then
                        If linecount > 2 Then
                            'yeah
                            linefound = False
                            linecount = 0

                            'add an extra couple of lines
                            lines.Add("  ")
                            lines.Add("// =======================================")
                            lines.Add("  ")
                        End If
                    End If
                End While
            End Using

            Helpers.Logger.WriteMessageRelative("writing", 2)
            If Not alreadypatched Then
                Using sw As New IO.StreamWriter(StartmenuDialogPath)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using

                Helpers.Logger.WriteMessage("ok", 3)
            Else
                Helpers.Logger.WriteMessageRelative("skipped", 3)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteError("error", 3)
            Helpers.Logger.WriteError(ex.Message, 4)
        End Try
#End Region


        '==================================================================================================================================================================================
#Region "SkCfg update"
        Helpers.Logger.WriteMessage("SkCfg update", 0)

        Try
            Dim sSkCfgFile As String
            Dim bConfigChanged As Boolean = False

            sSkCfgFile = Helpers.SiteKiosk.GetSiteKioskActiveConfig()
            Helpers.Logger.WriteMessage("config file", 1)
            Helpers.Logger.WriteMessage(sSkCfgFile, 2)

            Helpers.FilesAndFolders.Backup.File(sSkCfgFile, g_BackupDirectory)


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            Helpers.Logger.WriteMessage("updating", 1)

            Helpers.Logger.WriteMessage("loading", 2)
            xmlSkCfg.Load(sSkCfgFile)
            Helpers.Logger.WriteMessage("ok", 3)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            Helpers.Logger.WriteMessage("loading root node", 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If xmlNode_Root Is Nothing Then
                Throw New Exception("sitekiosk-configuration node not found")
            End If


            Dim mXmlNode_Template_Program__File As Xml.XmlNode
            Dim mXmlNode_Template_Program__Runas As Xml.XmlNode
            Dim mXmlNode_Template_Program__Startup As Xml.XmlNode
            Dim mXmlNode_Template_Program__tmp As Xml.XmlNode

            mXmlNode_Template_Program__File = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "file", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__Runas = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "runas", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__Startup = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "startup", ns.LookupNamespace("sk"))


            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Startup.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__Startup.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("enabled"))
            mXmlNode_Template_Program__Startup.Attributes.GetNamedItem("enabled").Value = "false"

            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Startup)


            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "username", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__tmp = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "domainname", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__Runas.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("enabled"))
            mXmlNode_Template_Program__Runas.Attributes.GetNamedItem("enabled").Value = "false"

            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Runas)


            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("autostart"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("autostart").Value = "false"

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("filename"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("filename").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("description"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("description").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("title"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("title").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(xmlSkCfg.CreateAttribute("screenshot-path"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("screenshot-path").Value = ""


            Dim mXmlNode_Programs As Xml.XmlNode
            Dim mXmlNode_Files As Xml.XmlNodeList
            Dim mXmlNode_File As Xml.XmlNode
            Dim mXmlNode_FileClone As Xml.XmlNode
            Dim sTmpAppName As String, bExplorerAlreadyThere As Boolean = False

            mXmlNode_Programs = xmlNode_Root.SelectSingleNode("sk:programs", ns)
            mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)

            Helpers.Logger.WriteMessage("checking app list", 2)
            For Each mXmlNode_File In mXmlNode_Files
                sTmpAppName = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower

                If sTmpAppName = pFileExplorerPath.ToLower Then
                    'already there, let's do nothing
                    bExplorerAlreadyThere = True
                End If
            Next

            If bExplorerAlreadyThere Then
                Helpers.Logger.WriteMessage("FileExplorer already in list", 3)
            Else
                Helpers.Logger.WriteMessage("ok", 3)

                Helpers.Logger.WriteMessage("adding " & cFileExplorerName, 2)

                mXmlNode_FileClone = mXmlNode_Template_Program__File.CloneNode(True)
                mXmlNode_FileClone.Attributes.GetNamedItem("filename").Value = pFileExplorerPath
                mXmlNode_FileClone.Attributes.GetNamedItem("title").Value = cFileExplorerName
                mXmlNode_FileClone.Attributes.GetNamedItem("description").Value = cFileExplorerDescription

                'mXmlNode_Programs.AppendChild(mXmlNode_FileClone)
                mXmlNode_Programs.PrependChild(mXmlNode_FileClone)

                Helpers.Logger.WriteMessage("ok", 3)

                bConfigChanged = True
            End If


            Helpers.Logger.WriteMessage("saving", 2)
            If bConfigChanged Then
                Helpers.Logger.WriteMessage(sSkCfgFile, 3)
                xmlSkCfg.Save(sSkCfgFile)
                Helpers.Logger.WriteMessage("ok", 4)
            Else
                Helpers.Logger.WriteWarning("skipped, nothing was changed", 3)
            End If


        Catch ex As Exception
            Helpers.Logger.WriteError("error", 3)
            Helpers.Logger.WriteError(ex.Message, 4)

        End Try
#End Region


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Done", 0)
        Helpers.Logger.WriteMessage("ok", 1)
        Helpers.Logger.WriteMessage("bye", 2)

    End Sub
End Module
