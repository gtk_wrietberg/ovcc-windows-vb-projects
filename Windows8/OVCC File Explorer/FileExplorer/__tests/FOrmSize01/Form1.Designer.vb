﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.lblNoFilesFound = New System.Windows.Forms.Label()
        Me.listviewFiles = New System.Windows.Forms.ListView()
        Me.colHeader_Name = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Date = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeaderHidden_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.pnlStatusbar = New System.Windows.Forms.Panel()
        Me.lblStatusbar = New System.Windows.Forms.Label()
        Me.treeFolders = New System.Windows.Forms.TreeView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pnlMain.SuspendLayout()
        Me.pnlStatusbar.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.AutoScroll = True
        Me.pnlMain.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlMain.Controls.Add(Me.lblNoFilesFound)
        Me.pnlMain.Controls.Add(Me.listviewFiles)
        Me.pnlMain.Controls.Add(Me.pnlStatusbar)
        Me.pnlMain.Controls.Add(Me.treeFolders)
        Me.pnlMain.Location = New System.Drawing.Point(12, 12)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1015, 408)
        Me.pnlMain.TabIndex = 5
        '
        'lblNoFilesFound
        '
        Me.lblNoFilesFound.BackColor = System.Drawing.Color.White
        Me.lblNoFilesFound.ForeColor = System.Drawing.Color.DarkGray
        Me.lblNoFilesFound.Location = New System.Drawing.Point(251, 62)
        Me.lblNoFilesFound.Name = "lblNoFilesFound"
        Me.lblNoFilesFound.Size = New System.Drawing.Size(757, 22)
        Me.lblNoFilesFound.TabIndex = 5
        Me.lblNoFilesFound.Text = "This folder is empty."
        Me.lblNoFilesFound.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'listviewFiles
        '
        Me.listviewFiles.BackColor = System.Drawing.Color.White
        Me.listviewFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.listviewFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colHeader_Name, Me.colHeader_Date, Me.colHeader_Type, Me.colHeader_Size, Me.colHeaderHidden_Size})
        Me.listviewFiles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listviewFiles.Location = New System.Drawing.Point(249, 4)
        Me.listviewFiles.MultiSelect = False
        Me.listviewFiles.Name = "listviewFiles"
        Me.listviewFiles.Size = New System.Drawing.Size(763, 371)
        Me.listviewFiles.TabIndex = 2
        Me.listviewFiles.UseCompatibleStateImageBehavior = False
        Me.listviewFiles.View = System.Windows.Forms.View.Details
        '
        'colHeader_Name
        '
        Me.colHeader_Name.Text = "Name"
        Me.colHeader_Name.Width = 400
        '
        'colHeader_Date
        '
        Me.colHeader_Date.Text = "Date modified"
        Me.colHeader_Date.Width = 120
        '
        'colHeader_Type
        '
        Me.colHeader_Type.Text = "Type"
        Me.colHeader_Type.Width = 160
        '
        'colHeader_Size
        '
        Me.colHeader_Size.Text = "Size"
        '
        'colHeaderHidden_Size
        '
        Me.colHeaderHidden_Size.Text = ""
        Me.colHeaderHidden_Size.Width = 0
        '
        'pnlStatusbar
        '
        Me.pnlStatusbar.BackColor = System.Drawing.Color.White
        Me.pnlStatusbar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlStatusbar.Controls.Add(Me.lblStatusbar)
        Me.pnlStatusbar.Location = New System.Drawing.Point(3, 379)
        Me.pnlStatusbar.Name = "pnlStatusbar"
        Me.pnlStatusbar.Size = New System.Drawing.Size(1008, 25)
        Me.pnlStatusbar.TabIndex = 3
        '
        'lblStatusbar
        '
        Me.lblStatusbar.BackColor = System.Drawing.Color.Transparent
        Me.lblStatusbar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatusbar.ForeColor = System.Drawing.Color.Black
        Me.lblStatusbar.Location = New System.Drawing.Point(3, -1)
        Me.lblStatusbar.Name = "lblStatusbar"
        Me.lblStatusbar.Size = New System.Drawing.Size(601, 24)
        Me.lblStatusbar.TabIndex = 7
        Me.lblStatusbar.Text = "xx items             xx item selected           xxkB"
        Me.lblStatusbar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'treeFolders
        '
        Me.treeFolders.BackColor = System.Drawing.Color.White
        Me.treeFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.treeFolders.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText
        Me.treeFolders.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeFolders.HideSelection = False
        Me.treeFolders.Indent = 20
        Me.treeFolders.ItemHeight = 20
        Me.treeFolders.Location = New System.Drawing.Point(3, 4)
        Me.treeFolders.Name = "treeFolders"
        Me.treeFolders.ShowLines = False
        Me.treeFolders.ShowRootLines = False
        Me.treeFolders.Size = New System.Drawing.Size(242, 371)
        Me.treeFolders.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(387, 472)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(168, 122)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(1288, 648)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlStatusbar.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlMain As Panel
    Friend WithEvents lblNoFilesFound As Label
    Friend WithEvents listviewFiles As ListView
    Friend WithEvents colHeader_Name As ColumnHeader
    Friend WithEvents colHeader_Date As ColumnHeader
    Friend WithEvents colHeader_Type As ColumnHeader
    Friend WithEvents colHeader_Size As ColumnHeader
    Friend WithEvents colHeaderHidden_Size As ColumnHeader
    Friend WithEvents pnlStatusbar As Panel
    Friend WithEvents lblStatusbar As Label
    Friend WithEvents treeFolders As TreeView
    Friend WithEvents Button1 As Button
End Class
