﻿
Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        '    FileVersionInfo myFileVersionInfo =
        '    FileVersionInfo.GetVersionInfo(Environment.SystemDirectory + "\\Notepad.exe");

        '// Print the file description.
        'textBox1.Text = "File description: " + myFileVersionInfo.FileDescription;

        Dim fi As FileVersionInfo = FileVersionInfo.GetVersionInfo("C:\Program Files (x86)\Adobe\Acrobat Reader DC\Reader\AcroRd32.exe")

        MsgBox(fi.FileDescription)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim items As Array, names As Array

        items = System.Enum.GetValues(GetType(Helpers.Windows.RegisteredFileType.Application.AssocStr))
        names = System.Enum.GetNames(GetType(Helpers.Windows.RegisteredFileType.Application.AssocStr))

        Dim _s1 As String = "", _s2 As String = "", _ss As String = ""
        Dim _ext1 As String = ".abc", _ext2 As String = ".pdf"

        For i As Integer = 0 To items.Length - 1
            Helpers.Windows.RegisteredFileType.Application.GetAnything(_ext1, items(i), _s1)
            Helpers.Windows.RegisteredFileType.Application.GetAnything(_ext2, items(i), _s2)

            _ss &= names(i) & " : " & _s1 & " - " & _s2 & vbCrLf
        Next

        MsgBox(_ss)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim _ext As String = ".pdf"

        MsgBox(Helpers.Windows.RegisteredFileType.Application.HasAssociatedApp(_ext))
    End Sub



    Private aContextMenuItems As String() = {"Open", "-", "Compress to ZIP file", "-", "Cut", "Copy", "Paste", "-", "Delete", "-", "Print", "-", "Create new folder", "-", "Properties"}
    Private WithEvents contextMenu__Files As New ContextMenu()

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim odm As New OwnerDrawMenu(New Font("Tahoma", 8), System.Drawing.Color.Red)

        For Each item As String In aContextMenuItems
            Dim mnuItem As System.Windows.Forms.MenuItem = New System.Windows.Forms.MenuItem
            mnuItem.Text = item
            mnuItem.Name = item
            AddHandler(mnuItem.Click), AddressOf contextMenu__Files__Item_Click

            odm.Add(mnuItem, New Icon("new.ico"), 0, False)
        Next

        contextMenu__Files.MergeMenu(odm)
    End Sub

    Private Sub contextMenu__Files__Item_Click(sender As Object, ByVal e As EventArgs)
        MsgBox(sender.name)
    End Sub


    Private Sub listviewFiles_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown

        If e.Button = MouseButtons.Right Then

            contextMenu__Files.Show(listviewFiles, New Point(e.X, e.Y))

            'Console.WriteLine("listviewFiles_MouseDown (" & e.X.ToString & "," & e.Y.ToString & ")")

            If listviewFiles.GetItemAt(e.X, e.Y) IsNot Nothing Then
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage("Right click on file", 0)
                    Helpers.Logger.WriteMessage(listviewFiles.GetItemAt(e.X, e.Y).Text, 1)
                End If

                'For Each lvItem As ListViewItem In listviewFiles.Items
                '    lvItem.Selected = False
                'Next
                'listviewFiles.GetItemAt(e.X, e.Y).Selected = True

                mFileSelectedNames = New List(Of String)
                For Each lvi As ListViewItem In listviewFiles.SelectedItems
                    mFileSelectedNames.Add(lvi.Text)
                Next
                'If listviewFiles.SelectedItems.Count > 0 Then
                '    mFileSelectedName = listviewFiles.SelectedItems.Item(0).Text
                'Else
                '    mFileSelectedName = ""
                'End If

                ' {"Open", "-", "Compress to ZIP file", "-", "Cut", "Copy", "Paste", "-", "Delete", "-", "Print", "-", "Create new folder", "-", "Properties"}
                '     0     1           2                3     4       5       6      7       8      9     10      11    12                  13     14
                'sendFileToPrinter


                Try
                    contextMenu__Files.MenuItems.Item(0).Visible = True
                    contextMenu__Files.MenuItems.Item(0).Enabled = mFileSelectedNames.Count < 2

                    contextMenu__Files.MenuItems.Item(1).Visible = True
                    contextMenu__Files.MenuItems.Item(2).Visible = True
                    contextMenu__Files.MenuItems.Item(2).Enabled = mFileSelectedNames.Count > 0

                    contextMenu__Files.MenuItems.Item(3).Visible = True
                    contextMenu__Files.MenuItems.Item(4).Visible = True
                    contextMenu__Files.MenuItems.Item(5).Visible = True
                    contextMenu__Files.MenuItems.Item(6).Visible = True
                    contextMenu__Files.MenuItems.Item(6).Enabled = itemsCopied.Count > 0

                    contextMenu__Files.MenuItems.Item(7).Visible = Settings.Generic.AllowFileDelete
                    contextMenu__Files.MenuItems.Item(8).Visible = Settings.Generic.AllowFileDelete

                    contextMenu__Files.MenuItems.Item(9).Visible = True
                    contextMenu__Files.MenuItems.Item(10).Visible = True
                    contextMenu__Files.MenuItems.Item(10).Enabled = (mFileSelectedNames.Count = 1)

                    contextMenu__Files.MenuItems.Item(11).Visible = False
                    contextMenu__Files.MenuItems.Item(12).Visible = False

                    contextMenu__Files.MenuItems.Item(13).Visible = True
                    contextMenu__Files.MenuItems.Item(14).Visible = True
                    contextMenu__Files.MenuItems.Item(14).Enabled = mFileSelectedNames.Count < 2


                    contextMenu__Files.Show(listviewFiles, New Point(e.X, e.Y))
                Catch ex As Exception

                End Try
            Else
                Try
                    If IO.Directory.Exists(treeFolders.SelectedNode.Name) Then
                        contextMenu__Files.MenuItems.Item(0).Visible = False

                        contextMenu__Files.MenuItems.Item(1).Visible = False
                        contextMenu__Files.MenuItems.Item(2).Visible = False

                        contextMenu__Files.MenuItems.Item(3).Visible = False
                        contextMenu__Files.MenuItems.Item(4).Visible = False
                        contextMenu__Files.MenuItems.Item(5).Visible = False
                        contextMenu__Files.MenuItems.Item(6).Visible = True
                        contextMenu__Files.MenuItems.Item(6).Enabled = itemsCopied.Count > 0

                        contextMenu__Files.MenuItems.Item(7).Visible = False
                        contextMenu__Files.MenuItems.Item(8).Visible = False

                        contextMenu__Files.MenuItems.Item(9).Visible = False
                        contextMenu__Files.MenuItems.Item(10).Visible = False

                        contextMenu__Files.MenuItems.Item(11).Visible = True
                        contextMenu__Files.MenuItems.Item(12).Visible = True

                        contextMenu__Files.MenuItems.Item(13).Visible = False
                        contextMenu__Files.MenuItems.Item(14).Visible = False


                        contextMenu__Files.Show(listviewFiles, New Point(e.X, e.Y))
                    End If
                Catch ex As Exception

                End Try
            End If
        End If
    End Sub
End Class
