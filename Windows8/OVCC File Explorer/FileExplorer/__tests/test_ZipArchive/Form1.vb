﻿Imports System.IO.Compression

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sFile As String = "C:\Users\SiteKiosk\Documents\Test.txt"
        Dim sZipFile As String = sFile & ".zip"

        Using zip As ZipArchive = ZipFile.Open(sZipFile, ZipArchiveMode.Create)
            zip.CreateEntryFromFile(sFile, IO.Path.GetFileName(sFile), CompressionLevel.NoCompression)

        End Using
    End Sub
End Class
