﻿Public Class Form1
    Public lstFolders As List(Of String)
    Public lstWatchfolder As List(Of IO.FileSystemWatcher)


#Region "thread safe"
    Delegate Sub _ThreadSafe_Callback__txt_folderactivity_Append(ByVal [text] As String)

    Public Sub txt_folderactivity_Append(ByVal [text] As String)
        If Me.txt_folderactivity.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__txt_folderactivity_Append(AddressOf txt_folderactivity_Append)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txt_folderactivity.AppendText([text])
        End If
    End Sub
#End Region


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lstFolders = New List(Of String)
        lstWatchfolder = New List(Of IO.FileSystemWatcher)

        lstFolders.Add("C:\__temp\___filewatchertest\Folder1")
        lstFolders.Add("C:\__temp\___filewatchertest\Folder2")
        lstFolders.Add("C:\__temp\___filewatchertest\Folder3")


        For Each folder As String In lstFolders
            AddWatcher(folder)
        Next


        _StartWatching()
    End Sub

    Public Sub AddWatcher(wPath As String)
        Dim fsw As New IO.FileSystemWatcher()

        fsw.Path = wPath

        fsw.IncludeSubdirectories = True

        AddHandler fsw.Created, AddressOf logchange
        AddHandler fsw.Changed, AddressOf logchange
        AddHandler fsw.Deleted, AddressOf logchange
        AddHandler fsw.Renamed, AddressOf logrename

        lstWatchfolder.Add(fsw)
    End Sub


    Private Sub _StartWatching()
        For Each fsw As IO.FileSystemWatcher In lstWatchfolder
            fsw.NotifyFilter = IO.NotifyFilters.DirectoryName
            fsw.NotifyFilter = fsw.NotifyFilter Or IO.NotifyFilters.FileName
            fsw.NotifyFilter = fsw.NotifyFilter Or IO.NotifyFilters.Attributes
            fsw.NotifyFilter = fsw.NotifyFilter Or IO.NotifyFilters.LastWrite


            fsw.EnableRaisingEvents = True
        Next



        btnStart.Enabled = False
        btnStop.Enabled = True
    End Sub

    Private Sub _StopWatching()
        For Each fsw As IO.FileSystemWatcher In lstWatchfolder
            fsw.EnableRaisingEvents = False
        Next

        btnStart.Enabled = True
        btnStop.Enabled = False
    End Sub

    Private Sub logchange(ByVal source As Object, ByVal e As System.IO.FileSystemEventArgs)
        txt_folderactivity_Append("'" & e.FullPath & "' ==> '" & e.ChangeType.ToString & "'" & vbCrLf)

        'If e.ChangeType = IO.WatcherChangeTypes.Changed Then
        '    txt_folderactivity_Append("File " & e.FullPath & " has been modified" & vbCrLf)
        'End If
        'If e.ChangeType = IO.WatcherChangeTypes.Created Then
        '    txt_folderactivity_Append("File " & e.FullPath & " has been created" & vbCrLf)
        'End If
        'If e.ChangeType = IO.WatcherChangeTypes.Deleted Then
        '    txt_folderactivity_Append("File " & e.FullPath & " has been deleted" & vbCrLf)
        'End If
    End Sub


    Public Sub logrename(ByVal source As Object, ByVal e As System.IO.RenamedEventArgs)
        txt_folderactivity_Append("'" & e.FullPath & "' ==> 'renamed'" & vbCrLf)
        'txt_folderactivity_Append("File" & e.OldName & " has been renamed to " & e.Name & vbCrLf)
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        _StartWatching()
    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        _StopWatching()
    End Sub


End Class
