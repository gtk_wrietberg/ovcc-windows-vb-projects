﻿Imports System.ComponentModel
Imports System.IO.Compression

Public Class frmMain
    Public mMyFolders As New List(Of myFolderItem)
    Private mFileSystemWatcher As New IO.FileSystemWatcher
    Private mSelectedPath As String = ""
    Private mFileCount As Integer = 0
    Private mFileSelectedCount As Integer = 0
    Public mFileSelectedName As String = ""
    Public mFileSelectedNames As List(Of String)
    Public mFileDraggedName As String = ""
    Public mFileDraggedIsDir As Boolean = False
    Public mFolderSelected As String = ""
    Public mDialogResponse As DialogResult
    Public mSelectedTreeNodeName As String

    Private mLongVersionString As String = ""
    Private mShortVersionString As String = ""

    Private aContextMenuItems As String() = {"Open", "-", "Compress to ZIP file", "-", "Cut", "Copy", "Paste", "-", "Delete", "-", "Print", "-", "Create new folder", "-", "Properties"}
    Private WithEvents contextMenu__Files As New ContextMenu()

    Private mMyForbiddenExtensions As New List(Of String)
    Private mForbiddenExtensionColor As Color = Color.DarkGray

    Private mMyIgnoredFiles As New List(Of String)

    Private lvwColumnSorter As ListViewColumnSorter

    Private itemsCopied As New List(Of _listviewItemCopied)
    Private itemsAreCut As Boolean = False
    Private sFolderCopiedInto As String = ""

    Private Class _listviewItemCopied
        Public FileName As String
        Public FileFullPath As String
    End Class


#Region "Thread Safe stuff"
    Delegate Sub _ThreadSafe_Callback__frmTitle(ByVal [text] As String)
    Delegate Sub _ThreadSafe_Callback__lblStatusbar(ByVal [text] As String)
    Delegate Sub _ThreadSafe_Callback__listviewFiles_Add(ByVal [item] As ListViewItem)
    Delegate Sub _ThreadSafe_Callback__listviewFiles_Clear()
    Delegate Sub _ThreadSafe_Callback__listviewFiles_ImageList(ByVal [imagelist] As ImageList)
    Delegate Sub _ThreadSafe_Callback__lblNoFilesFound_Toggle()
    Delegate Sub _ThreadSafe_Callback__treeFolders_SelectedNode_Name_Store()
    Delegate Sub _ThreadSafe_Callback__progressBar_Toggle()
    Delegate Sub _ThreadSafe_Callback__frmOverwriteConfirmation()

    Public Sub _ThreadSafe__frmOverwriteConfirmation()
        If Me.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__frmOverwriteConfirmation(AddressOf _ThreadSafe__frmOverwriteConfirmation)
            Me.Invoke(d, New Object() {})
        Else
            Me.Opacity = 0.9
            Dim frm As New frmOverwriteConfirmation
            frm.Moveable = False
            mDialogResponse = frm.ShowDialog(Me)
            Me.Opacity = 1.0
        End If
    End Sub

    Public Sub _ThreadSafe__treeFolders_SelectedNode_Name_Store()
        If Me.treeFolders.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__treeFolders_SelectedNode_Name_Store(AddressOf _ThreadSafe__treeFolders_SelectedNode_Name_Store)
            Me.Invoke(d, New Object() {})
        Else
            mSelectedPath = treeFolders.SelectedNode.Name
        End If
    End Sub

    Public Sub _ThreadSafe__frmTitle_Set(ByVal [text] As String)
        If Me.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__frmTitle(AddressOf _ThreadSafe__frmTitle_Set)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.Text = [text]
        End If
    End Sub

    Public Sub _ThreadSafe__lblStatusbar_Set(ByVal [text] As String)
        If Me.lblStatusbar.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__lblStatusbar(AddressOf _ThreadSafe__lblStatusbar_Set)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblStatusbar.Text = [text]
        End If
    End Sub

    Public Sub _ThreadSafe__lblStatusbar_Append(ByVal [text] As String)
        If Me.lblStatusbar.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__lblStatusbar(AddressOf _ThreadSafe__lblStatusbar_Append)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblStatusbar.Text &= [text]
        End If
    End Sub

    Public Sub _ThreadSafe__listviewFiles_Add(ByVal [item] As ListViewItem)
        If Me.listviewFiles.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__listviewFiles_Add(AddressOf _ThreadSafe__listviewFiles_Add)
            Me.Invoke(d, New Object() {[item]})
        Else
            Me.listviewFiles.Items.Add([item])
        End If
    End Sub

    Public Sub _ThreadSafe__listviewFiles_ImageList(ByVal [imagelist] As ImageList)
        If Me.listviewFiles.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__listviewFiles_ImageList(AddressOf _ThreadSafe__listviewFiles_ImageList)
            Me.Invoke(d, New Object() {[imagelist]})
        Else
            Me.listviewFiles.SmallImageList = [imagelist]
        End If
    End Sub

    Public Sub _ThreadSafe__listviewFiles_Clear()
        If Me.listviewFiles.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__listviewFiles_Clear(AddressOf _ThreadSafe__listviewFiles_Clear)
            Me.Invoke(d, New Object() {})
        Else
            Me.listviewFiles.Items.Clear()
        End If
    End Sub

    Public Sub _ThreadSafe__lblNoFilesFound_Show()
        If Me.lblNoFilesFound.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__lblNoFilesFound_Toggle(AddressOf _ThreadSafe__lblNoFilesFound_Show)
            Me.Invoke(d, New Object() {})
        Else
            Me.lblNoFilesFound.Visible = True
            Me.lblNoFilesFound.BringToFront()
        End If
    End Sub

    Public Sub _ThreadSafe__lblNoFilesFound_Hide()
        If Me.lblNoFilesFound.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__lblNoFilesFound_Toggle(AddressOf _ThreadSafe__lblNoFilesFound_Hide)
            Me.Invoke(d, New Object() {})
        Else
            Me.lblNoFilesFound.Visible = False
            Me.lblNoFilesFound.SendToBack()
        End If
    End Sub

    Public Sub _ThreadSafe__progressBar_Show()
        If Me.progressBar.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__progressBar_Toggle(AddressOf _ThreadSafe__progressBar_Show)
            Me.Invoke(d, New Object() {})
        Else
            Me.progressBar.Visible = True

            Me.progressBar.Left = listviewFiles.Left + (listviewFiles.Width - progressBar.Width) / 2
            Me.progressBar.Top = listviewFiles.Top + (listviewFiles.Height - progressBar.Height) / 2

            Me.progressBar.BringToFront()
        End If
    End Sub

    Public Sub _ThreadSafe__progressBar_Hide()
        If Me.progressBar.InvokeRequired Then
            Dim d As New _ThreadSafe_Callback__progressBar_Toggle(AddressOf _ThreadSafe__progressBar_Hide)
            Me.Invoke(d, New Object() {})
        Else
            Me.progressBar.Visible = False
            Me.progressBar.SendToBack()
        End If
    End Sub

    Private Sub _ThreadSafe__UpdateStatusBar(Optional bClear As Boolean = False)
        Try
            If bClear Then
                _ThreadSafe__lblStatusbar_Set("")

                Exit Sub
            End If

            If mFileCount = 1 Then
                _ThreadSafe__lblStatusbar_Set("1 item")
            Else
                _ThreadSafe__lblStatusbar_Set(mFileCount.ToString & " items")
            End If

            If mFileSelectedCount <= 0 Then
                'no items selected
            Else
                Dim lTotalSize As Long = 0

                For Each sFile As String In mFileSelectedNames
                    Dim sPath As String = IO.Path.Combine(mFolderSelected, sFile)
                    Dim fi As New IO.FileInfo(sPath)

                    lTotalSize += fi.Length
                Next

                Dim sSelectedString As String
                If mFileSelectedNames.Count = 1 Then
                    sSelectedString = "1 item selected"
                Else
                    sSelectedString = mFileSelectedNames.Count & " items selected"
                End If


                _ThreadSafe__lblStatusbar_Append(New String(" ", 8) & sSelectedString & New String(" ", 4) & Helpers.FilesAndFolders.FileSize.GetHumanReadable(lTotalSize))
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "drag form with topbar Panel"
    'Private mMoveForm_isMouseDown As Boolean = False
    'Private mMoveForm_mouseOffset As Point

    'Private Sub _Topbar_MouseDown(sender As Object, e As MouseEventArgs)
    '    If e.Button = MouseButtons.Left Then
    '        ' Get the new position
    '        mMoveForm_mouseOffset = New Point(-e.X, -e.Y)
    '        ' Set that left button is pressed
    '        mMoveForm_isMouseDown = True
    '    End If
    'End Sub

    '' MouseMove used to check if mouse cursor is moving
    'Private Sub _Topbar_MouseMove(sender As Object, e As MouseEventArgs)
    '    If mMoveForm_isMouseDown Then
    '        Dim mousePos As Point = Control.MousePosition
    '        ' Get the new form position
    '        mousePos.Offset(mMoveForm_mouseOffset.X, mMoveForm_mouseOffset.Y)
    '        Me.Location = mousePos
    '    End If
    'End Sub

    '' Left mouse button released, form should stop moving
    'Private Sub _Topbar_MouseUp(sender As Object, e As MouseEventArgs)
    '    If e.Button = MouseButtons.Left Then
    '        mMoveForm_isMouseDown = False
    '    End If
    'End Sub

    'Private Sub _Topbar_MouseHover(sender As Object, e As EventArgs)
    '    Me.Cursor = Cursors.SizeAll
    'End Sub

    'Private Sub _Topbar_MouseLeave(sender As Object, e As EventArgs)
    '    Me.Cursor = Cursors.Default
    'End Sub
#End Region

#Region "filesystemwatcher"
    Private Sub _FileSystemWatcher_Event__Changed(ByVal source As Object, ByVal e As System.IO.FileSystemEventArgs)
        If e.ChangeType = IO.WatcherChangeTypes.Changed Or e.ChangeType = IO.WatcherChangeTypes.Created Or e.ChangeType = IO.WatcherChangeTypes.Deleted Then
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage("file/folder change detected: " & e.FullPath, 0)
            End If

            _FileSystemWatcher__Stop()
            _LoadFiles()
        Else
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage(e.ChangeType.ToString & ": " & e.FullPath, 0)
            End If
        End If
    End Sub

    Public Sub _FileSystemWatcher_Event__Renamed(ByVal source As Object, ByVal e As System.IO.RenamedEventArgs)
        If e.ChangeType = IO.WatcherChangeTypes.Changed Or e.ChangeType = IO.WatcherChangeTypes.Renamed Then
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage("file/folder change detected: " & e.FullPath, 0)
            End If

            _FileSystemWatcher__Stop()
            _LoadFiles()
        Else
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage(e.ChangeType.ToString & ": " & e.FullPath, 0)
            End If
        End If
    End Sub

    Private Sub _FileSystemWatcher__Initialise(_path As String)
        If Not Settings.Generic.DetectFolderChanges Then
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage("FileSystemWatcher is disabled")
            End If

            Exit Sub
        End If

        mFileSystemWatcher = New IO.FileSystemWatcher()

        mFileSystemWatcher.Path = _path
        mFileSystemWatcher.IncludeSubdirectories = False

        AddHandler mFileSystemWatcher.Created, AddressOf _FileSystemWatcher_Event__Changed
        AddHandler mFileSystemWatcher.Changed, AddressOf _FileSystemWatcher_Event__Changed
        AddHandler mFileSystemWatcher.Deleted, AddressOf _FileSystemWatcher_Event__Changed
        AddHandler mFileSystemWatcher.Renamed, AddressOf _FileSystemWatcher_Event__Renamed

        mFileSystemWatcher.NotifyFilter = IO.NotifyFilters.DirectoryName
        mFileSystemWatcher.NotifyFilter = mFileSystemWatcher.NotifyFilter Or IO.NotifyFilters.FileName
        mFileSystemWatcher.NotifyFilter = mFileSystemWatcher.NotifyFilter Or IO.NotifyFilters.Attributes
        mFileSystemWatcher.NotifyFilter = mFileSystemWatcher.NotifyFilter Or IO.NotifyFilters.LastWrite

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("FileSystemWatcher path: " & _path)
        End If
    End Sub

    Private Sub _FileSystemWatcher__Start()
        If Not Settings.Generic.DetectFolderChanges Then
            Exit Sub
        End If

        mFileSystemWatcher.EnableRaisingEvents = True

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("FileSystemWatcher started")
        End If
    End Sub

    Private Sub _FileSystemWatcher__Stop()
        mFileSystemWatcher.EnableRaisingEvents = False

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("FileSystemWatcher stopped")
        End If
    End Sub
#End Region

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime

        '-----------settings------------------
        Settings.Load()


        '---command line
        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals(Settings.CommandLineParams.Debugging) Then
                Settings.Generic.Debugging = True
            End If

            If arg.Equals(Settings.CommandLineParams.StartWithDownloadsFolder) Then
                Settings.Generic.StartWithDownloadsFolder = True
            End If

            If arg.Equals(Settings.CommandLineParams.ShowHiddenFiles) Then
                Settings.Generic.ShowHiddenFiles = True
            End If

            If arg.Equals(Settings.CommandLineParams.ShowSystemFiles) Then
                Settings.Generic.ShowSystemFiles = True
            End If

            If arg.Equals(Settings.CommandLineParams.ShowTemporaryFiles) Then
                Settings.Generic.ShowTemporaryFiles = True
            End If

            If arg.Equals(Settings.CommandLineParams.DetectFolderChanges) Then
                Settings.Generic.DetectFolderChanges = True
            End If
        Next

        '-------------------
        If Settings.Generic.Debugging Then
            Helpers.Logger.InitialiseLogger(My.Application.Info.DirectoryPath & "\_logs", False)
            Helpers.Logger.WriteMessage(New String("*", 50), 0)
            Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        End If


        mLongVersionString = "v" & My.Application.Info.Version.ToString &
            " - " & Helpers.FilesAndFolders.GetFileCompileDate(Application.ExecutablePath, "yyMMdd.HHmmss")
        mShortVersionString = "v" & My.Application.Info.Version.Major.ToString & "." & My.Application.Info.Version.Minor.ToString


        lblAppVersion.Text = mShortVersionString


        Me.Width = pnlBorder.Width + pnlBorder.Left * 2
        Me.Height = pnlBorder.Height + pnlBorder.Top * 2

        Me.Left = (My.Computer.Screen.Bounds.Width - Me.Width) / 2
        Me.Top = (My.Computer.Screen.Bounds.Height - Me.Height) / 2


        _ThreadSafe__frmTitle_Set(Settings.Strings.AppTitle)


        lblNoFilesFound.Width = listviewFiles.Width - 6
        lblNoFilesFound.Left = listviewFiles.Left + 3
        lblNoFilesFound.BackColor = listviewFiles.BackColor


        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("UI size & location", 0)
            Helpers.Logger.WriteMessage("width: " & Me.Width.ToString, 1)
            Helpers.Logger.WriteMessage("height: " & Me.Height.ToString, 1)
            Helpers.Logger.WriteMessage("top: " & Me.Top.ToString, 1)
            Helpers.Logger.WriteMessage("left: " & Me.Left.ToString, 1)
        End If


        '-------------------------------------------------
        _ReadForbiddenExtensions()


        '-------------------------------------------------
        Dim MyImages As New ImageList()

        MyImages.ImageSize = New Size(16, 16)

        'make sure the indices of this image list match the ones in Settings
        MyImages.Images.Add("ico_disk", My.Resources.ico_disk)
        MyImages.Images.Add("ico_folder", My.Resources.ico_folder)
        MyImages.Images.Add("ico_documents", My.Resources.ico_documents)
        MyImages.Images.Add("ico_downloads", My.Resources.ico_downloads)
        MyImages.Images.Add("ico_pictures", My.Resources.ico_pictures)
        MyImages.Images.Add("ico_desktop", My.Resources.ico_desktop)

        treeFolders.ImageList = MyImages


        '-------------------------------------------------
        lvwColumnSorter = New ListViewColumnSorter()
        listviewFiles.ListViewItemSorter = lvwColumnSorter


        '-------------------------------------------------
        Dim tmpStringName As String = ""
        Dim tmpStringPath As String = ""
        Dim tmpStringIcon As Integer = 0
        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("Adding folders", 0)
        End If


        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("From registry", 1)
        End If
        For i As Integer = 0 To 9
            tmpStringName = Settings.Paths.Names(i)
            tmpStringPath = Settings.Paths.Paths(i)
            tmpStringIcon = Settings.Paths.Icons(i)

            If tmpStringName = "" Then
                tmpStringName = "(no name #" & Helpers.Types.LeadingZero(i, 2) & ")"
            End If

            If tmpStringIcon < 0 Or tmpStringIcon > (MyImages.Images.Count - 1) Then
                tmpStringIcon = 1
            End If

            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage(tmpStringName, 2)
                Helpers.Logger.WriteMessage(tmpStringPath, 2)
                Helpers.Logger.WriteMessage("icon #" & tmpStringIcon.ToString, 2)
            End If

            If tmpStringPath.StartsWith("%USERPROFILE%") Then
                tmpStringPath = tmpStringPath.Replace("%USERPROFILE%", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile))
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage(tmpStringPath, 2)
                End If
            End If

            If Not IO.Directory.Exists(tmpStringPath) Then
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage("path does not exist, skipping", 3)
                End If
            Else
                mMyFolders.Add(New myFolderItem(tmpStringName, tmpStringPath, tmpStringIcon, tmpStringIcon))
            End If
        Next



        Dim sDownloadFolderFromSkCfg As String = Helpers.SiteKiosk.SkCfg.GetDownloadFolder

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("Downloads folder", 1)
        End If

        If sDownloadFolderFromSkCfg.Equals("") Then
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage("Nothing found in sitekiosk config", 2)
                Helpers.Logger.WriteMessage(Helpers.Errors.GetLast(False), 3)
                Helpers.Logger.WriteMessage("Using default", 2)
            End If

            sDownloadFolderFromSkCfg = "C:\Users\SiteKiosk\Downloads"
        Else
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage("Found in sitekiosk config", 2)
            End If
        End If

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage(sDownloadFolderFromSkCfg, 2)
        End If
        If sDownloadFolderFromSkCfg.StartsWith("%USERPROFILE%") Then
            sDownloadFolderFromSkCfg = sDownloadFolderFromSkCfg.Replace("%USERPROFILE%", Environment.GetFolderPath(Environment.SpecialFolder.UserProfile))
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage(sDownloadFolderFromSkCfg, 2)
            End If
        End If
        mMyFolders.Add(New myFolderItem(Settings.Strings.PathNames.Downloads, sDownloadFolderFromSkCfg, 3, 3))


        'If Settings.Generic.Debugging Then
        '    Helpers.Logger.WriteMessage("Documents folder", 1)
        '    Helpers.Logger.WriteMessage("C:\Users\SiteKiosk\Documents", 2)
        'End If
        'mMyFolders.Add(New myFolderItem("My Documents", "C:\Users\SiteKiosk\Documents", 2, 2))


        'If Settings.Generic.Debugging Then
        '    Helpers.Logger.WriteMessage("Pictures folder", 1)
        '    Helpers.Logger.WriteMessage("C:\Users\SiteKiosk\Pictures", 2)
        'End If
        'mMyFolders.Add(New myFolderItem("My Pictures", "C:\Users\SiteKiosk\Pictures", 4, 4))


        '-----------------------------
        mMyIgnoredFiles.Add("desktop.ini".ToLower)
        mMyIgnoredFiles.Add("Thumbs.db".ToLower)



        '-----------------------------
        If Settings.Generic.StartWithDownloadsFolder Then
            _LoadFoldersIntoTreeview(sDownloadFolderFromSkCfg)
        Else
            _LoadFoldersIntoTreeview()
        End If


        lblOpeningApp.Text = "one moment please" & vbCrLf & "starting application"


        '-------
        If Settings.Generic.ContextMenu Then
            Dim mnuItem As System.Windows.Forms.MenuItem

            For Each item As String In aContextMenuItems
                mnuItem = New System.Windows.Forms.MenuItem
                mnuItem.Text = item
                mnuItem.Name = item
                AddHandler(mnuItem.Click), AddressOf contextMenu__Files__Item_Click
                contextMenu__Files.MenuItems.Add(mnuItem)
            Next
        End If
    End Sub

    Private Sub _ReadForbiddenExtensions()
        mMyForbiddenExtensions = New List(Of String)

        Dim fileContent As String = My.Resources.forbidden_extensions
        Dim stringStream As New IO.StringReader(fileContent)

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("Reading forbidden extensions", 0)
        End If


        Using MyReader As New FileIO.TextFieldParser(stringStream)
            MyReader.TextFieldType = FileIO.FieldType.Delimited
            MyReader.SetDelimiters(",")

            Dim currentRow As String()
            Dim lineNumber As Integer = 1
            Dim extension As String = ""
            While Not MyReader.EndOfData
                Try
                    currentRow = MyReader.ReadFields()

                    If currentRow.Count <> 3 Then
                        Throw New FileIO.MalformedLineException("needs three columns (extension,description,app/os)", lineNumber)
                    End If

                    extension = currentRow(0).ToLower
                    If Not extension.StartsWith(".") Then
                        extension = "." & extension
                    End If

                    mMyForbiddenExtensions.Add(extension)

                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("added '" & extension & "'", 0)
                    End If
                Catch ex As FileIO.MalformedLineException
                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("error", 1)
                        Helpers.Logger.WriteMessage("Line " & ex.LineNumber & "is not valid and will be skipped (" & ex.Message & ").", 2)
                    End If
                End Try

                lineNumber += 1
            End While
        End Using

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("found " & mMyForbiddenExtensions.Count.ToString & " forbidden extensions", 1)
        End If
    End Sub

    Private Sub _LoadFoldersIntoTreeview(Optional PreselectFolder As String = "")
        treeFolders.Nodes.Clear()

        Dim nodeRoot As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("This PC", 0, 0)
        Dim iIndex As Integer = 0, iSelectedIndex = -1

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("Folders", 0)
        End If


        For Each mFI As myFolderItem In mMyFolders
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage(mFI.Text & ": " & mFI.Path, 1)
            End If

            If IO.Directory.Exists(mFI.Path) Then
                'nodeRoot.Nodes.Add(mFI.Path, mFI.Text, mFI.IconIndex, mFI.IconSelectedIndex)
                'PopulateTreeView(mFI.Path, nodeRoot.Nodes(iIndex))
                Dim newNode As System.Windows.Forms.TreeNode = nodeRoot.Nodes.Add(mFI.Path, mFI.Text, mFI.IconIndex, mFI.IconSelectedIndex)
                _PopulateTreeView(mFI.Path, newNode)

                iIndex += 1
            Else
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteError("path not found", 2)
                End If
            End If
        Next


        Drives.CollectDrives()

        For Each _drive As Drives.Drive In Drives.Drives_CDRom
            nodeRoot.Nodes.Add(_drive.Name, "CD/DVD (" & _drive.Name & ")", 0, 0)

            If _drive.IsReady Then
                _PopulateTreeView(_drive.Name, nodeRoot.Nodes(iIndex))
            End If

            iIndex += 1
        Next


        If Settings.Generic.ShowNetworkDrives Then
            For Each _drive As Drives.Drive In Drives.Drives_Network
                nodeRoot.Nodes.Add(_drive.Name, "Network (" & _drive.Name & ")", 0, 0)

                If _drive.IsReady Then
                    _PopulateTreeView(_drive.Name, nodeRoot.Nodes(iIndex))
                End If

                iIndex += 1
            Next
        End If


        For Each _drive As Drives.Drive In Drives.Drives_Removable
            nodeRoot.Nodes.Add(_drive.Name, "Removable (" & _drive.Name & ")", 0, 0)

            If _drive.IsReady Then
                _PopulateTreeView(_drive.Name, nodeRoot.Nodes(iIndex))
            End If

            iIndex += 1
        Next



        treeFolders.Nodes.Add(nodeRoot)
        If Settings.Generic.DirectoriesCollapsable Then
            treeFolders.Nodes(0).Expand()
        Else
            treeFolders.ExpandAll()
        End If


        If Not PreselectFolder.Equals("") Then
            _treeFolders_PreselectFolder_recursive(treeFolders.Nodes, PreselectFolder)
        End If
    End Sub

    Private Sub _treeFolders_PreselectFolder_recursive(ByVal _col As System.Windows.Forms.TreeNodeCollection, ByVal Name As String)
        For Each t As System.Windows.Forms.TreeNode In _col
            If t.Name.Equals(Name) Then
                treeFolders.SelectedNode = t

                Exit For
            End If

            If t.Nodes.Count > 0 Then
                _treeFolders_PreselectFolder_recursive(t.Nodes, Name)
            End If
        Next
    End Sub

    Private Sub _PopulateTreeView(ByVal dir As String, ByVal parentNode As System.Windows.Forms.TreeNode)
        Dim folder As String = String.Empty
        Dim di As IO.DirectoryInfo

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessageRelative(dir, 1)
        End If

        Try
            Dim folders() As String = IO.Directory.GetDirectories(dir)

            Array.Sort(folders)

            If folders.Length > 0 Then
                Dim childNode As System.Windows.Forms.TreeNode = Nothing
                For Each folder In folders
                    di = New IO.DirectoryInfo(folder)

                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessageRelative(folder, 2)
                    End If


                    If di.Attributes And IO.FileAttributes.ReparsePoint Then
                        'symlink, skip
                        If Settings.Generic.Debugging Then
                            Helpers.Logger.WriteMessageRelative("symlink, skip", 3)
                        End If

                        Continue For
                    End If

                    childNode = parentNode.Nodes.Add(folder, di.Name, 1, 1)

                    _PopulateTreeView(folder, childNode)
                Next
            End If
        Catch ex As UnauthorizedAccessException
            parentNode.Nodes.Add(folder & ": Access Denied")
        Catch ex As IO.IOException
            parentNode.Nodes.Add(folder & ": " & ex.Message)
            'Catch ex As Exception
            '    parentNode.Nodes.Add(folder & ": Access Denied")
        End Try
    End Sub


#Region "events"
#Region "bgWorkerFiles"
    Private Sub bgWorkerFiles_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorkerFiles.DoWork
        _ThreadSafe__UpdateStatusBar()

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("Files", 0)
        End If

        _ThreadSafe__listviewFiles_Clear()
        mFileCount = 0
        mFileSelectedCount = 0

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage(mSelectedPath, 1)
        End If

        If Not IO.Directory.Exists(mSelectedPath) Then
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteError("Could not find path", 2)
            End If

            Exit Sub
        End If


        '_ThreadSafe__frmTitle_Set(Settings.Strings.AppTitle & " - " & mSelectedPath)


        Try
            'Dim folders() As String = IO.Directory.GetDirectories(mSelectedPath)
            Dim files() As String = IO.Directory.GetFiles(mSelectedPath)

            Dim _listExtensions As New List(Of String)
            Dim _listForbiddenExtensions As New List(Of String)
            Dim _imagelist As New ImageList()




            _imagelist.ImageSize = New Size(16, 16)

            _ThreadSafe__listviewFiles_ImageList(_imagelist)

            _imagelist.Images.Add(My.Resources.ico_folder)
            _imagelist.Images.Add(My.Resources.ico_unknownfile)

            _listExtensions.Add(".(invalid extension, just placeholder).")
            _listExtensions.Add(".(invalid extension, just another placeholder).")


            Dim dI As New IO.DirectoryInfo(mSelectedPath)
            Dim dSubDirs As List(Of String) = dI.EnumerateDirectories("*.*", IO.SearchOption.TopDirectoryOnly).Where(Function(q) IsReparsePoint(q) = False).Select(Function(q) q.Name).ToList

            If files.Count = 0 And dSubDirs.Count = 0 Then
                'no files
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage("no files", 2)
                End If

                _ThreadSafe__lblNoFilesFound_Show()
            Else
                _ThreadSafe__lblNoFilesFound_Hide()
            End If


            For Each folder As String In dSubDirs
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage(folder, 2)
                End If


                Dim _listviewitem As New ListViewItem(New String() {folder, "", "", "", "-1"})

                _listviewitem.ForeColor = Color.Black
                _listviewitem.ImageIndex = 0

                _ThreadSafe__listviewFiles_Add(_listviewitem)
            Next

            For Each file As String In files
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage(file, 2)
                End If

                If mMyIgnoredFiles.Contains(IO.Path.GetFileName(file).ToLower) Then
                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("ignored", 3)
                    End If

                    Continue For
                End If


                Dim fi As New IO.FileInfo(file)
                Dim imgIndex As Integer
                Dim _icon As Icon
                Dim _extension As String
                Dim _assoc_app_name As String = ""

                'Ignore hidden/system files
                If Not Settings.Generic.ShowHiddenFiles And fi.Attributes.HasFlag(IO.FileAttributes.Hidden) Then
                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("hidden file ignored", 3)
                    End If

                    Continue For
                End If
                If Not Settings.Generic.ShowSystemFiles And fi.Attributes.HasFlag(IO.FileAttributes.System) Then
                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("system file ignored", 3)
                    End If

                    Continue For
                End If
                If Not Settings.Generic.ShowTemporaryFiles And fi.Attributes.HasFlag(IO.FileAttributes.Temporary) Then
                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("temporary file ignored", 3)
                    End If

                    Continue For
                End If


                '--------------------------------------------------------------------
                Dim _listviewitem As New ListViewItem(New String() {fi.Name, fi.LastWriteTime.ToString, Helpers.FilesAndFolders.GetFileTypeFromRegistry(fi.Extension), Helpers.FilesAndFolders.FileSize.GetHumanReadable(fi.Length), fi.Length})

                _listviewitem.ForeColor = Color.Black


                _extension = fi.Extension.ToLower



                If _listForbiddenExtensions.Contains(_extension) Then
                    imgIndex = 1

                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("already forbidden!", 3)
                    End If

                    _listviewitem.ForeColor = mForbiddenExtensionColor
                    _listviewitem.ToolTipText = "This file can not be opened!"
                Else
                    If mMyForbiddenExtensions.Contains(_extension) Then
                        'forbidden!
                        If Settings.Generic.Debugging Then
                            Helpers.Logger.WriteMessage("forbidden!", 3)
                        End If

                        imgIndex = 1
                        _listviewitem.ForeColor = mForbiddenExtensionColor
                        _listviewitem.ToolTipText = "This file can not be opened!"

                        _listForbiddenExtensions.Add(_extension)
                    Else
                        'If Helpers.Windows.RegisteredFileType.Application.HasAssociatedApp(_extension) Then
                        '    Helpers.Windows.RegisteredFileType.Application.GetAppName(_extension, _assoc_app_name)
                        'Else
                        '    _assoc_app_name = "(unknown)"
                        'End If

                        If _listExtensions.Contains(_extension) Then
                            imgIndex = _listExtensions.IndexOf(_extension)

                            If Settings.Generic.Debugging Then
                                Helpers.Logger.WriteMessage("icon already in list (index " & imgIndex.ToString & ")", 3)
                            End If
                        Else
                            _icon = Helpers.Windows.RegisteredFileType.Icon.FromFilePath(file)

                            If _icon Is Nothing Then
                                imgIndex = 1 'unknown

                                If Settings.Generic.Debugging Then
                                    Helpers.Logger.WriteMessage("unknown icon", 3)
                                End If
                            Else
                                _imagelist.Images.Add(_icon)
                                _listExtensions.Add(_extension)

                                imgIndex = _imagelist.Images.Count - 1

                                If Settings.Generic.Debugging Then
                                    Helpers.Logger.WriteMessage("found icon", 3)
                                End If
                            End If
                        End If

                        If _extension.Equals(".zip") Then
                            _listviewitem.ToolTipText = "Double click to unpack this file in the current folder"
                        Else
                            _listviewitem.ToolTipText = "Double click to open this file"
                        End If
                    End If
                End If

                _listviewitem.ImageIndex = imgIndex


                _ThreadSafe__listviewFiles_Add(_listviewitem)
                mFileCount += 1
            Next
        Catch ex As Exception
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteError("exception", 1)
                Helpers.Logger.WriteError(ex.Message, 2)
            End If
        End Try

        _ThreadSafe__UpdateStatusBar()
    End Sub

    Private Function IsReparsePoint(d As IO.DirectoryInfo) As Boolean
        Return ((d.Attributes And IO.FileAttributes.ReparsePoint) = IO.FileAttributes.ReparsePoint)
    End Function

    Private Sub bgWorkerFiles_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgWorkerFiles.RunWorkerCompleted
        HidePleaseWait()

        If Settings.Generic.DetectFolderChanges Then
            _FileSystemWatcher__Initialise(mSelectedPath)
            _FileSystemWatcher__Start()
        End If
    End Sub

#End Region

#Region "listviewFiles"
    Private Sub listviewFiles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listviewFiles.SelectedIndexChanged
        mFileSelectedCount = listviewFiles.SelectedItems.Count

        mFileSelectedNames = New List(Of String)
        For Each lvi As ListViewItem In listviewFiles.SelectedItems
            mFileSelectedNames.Add(lvi.Text)
        Next
        'If listviewFiles.SelectedItems.Count > 0 Then
        '    mFileSelectedName = listviewFiles.SelectedItems.Item(0).Text
        'Else
        '    mFileSelectedName = ""
        'End If

        mFolderSelected = treeFolders.SelectedNode.Name

        _ThreadSafe__UpdateStatusBar()
    End Sub

    Private Sub listviewFiles_DoubleClick(sender As Object, e As EventArgs) Handles listviewFiles.DoubleClick
        Dim sFullPath As String = ""

        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("File double click", 0)
        End If


        Try
            If listviewFiles.SelectedItems.Item(0).ForeColor = mForbiddenExtensionColor Then
                'Oops, forbidden extension, ignore!

                ErrorMessage("This file type can not be opened!", MessageBoxButtons.OK)

                Exit Sub
            End If


            sFullPath = IO.Path.Combine(treeFolders.SelectedNode.Name, listviewFiles.SelectedItems.Item(0).Text)

            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage(sFullPath, 1)
            End If


            If IO.Directory.Exists(sFullPath) And listviewFiles.SelectedItems.Item(0).SubItems.Item(4).Text = "-1" Then
                _LoadFoldersIntoTreeview(sFullPath)

                Exit Sub
            End If


            If Not IO.File.Exists(sFullPath) Then
                Throw New Exception("'" & sFullPath & "' does not exist!")
            End If

            'if zip, then extract and move to that folder
            If IO.Path.GetExtension(sFullPath).Equals(".zip") Then
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage("zip file, extract", 2)
                End If

                _ExtractZip(sFullPath)

                Exit Sub
            End If


            Dim sCommand_Full As String = ""
            Dim sCommand_Exe As String = ""
            Dim sCommand_Param As String = ""

            If Settings.Extensions.GetAppForExtension(IO.Path.GetExtension(sFullPath), sCommand_Exe) Then
                sCommand_Param = """" & sFullPath & """"
            End If

            If sCommand_Exe.Equals("") Then
                If Not Helpers.Windows.RegisteredFileType.Application.GetAnything(IO.Path.GetExtension(sFullPath), Helpers.Windows.RegisteredFileType.Application.AssocStr.Command, sCommand_Full) Then
                    'FAIL
                    ErrorMessage("No associated application for this file type available!", MessageBoxButtons.OK)

                    Throw New Exception("No associated application for this file type available! (AssocStr.Command)")
                End If
                If Not Helpers.Windows.RegisteredFileType.Application.GetAnything(IO.Path.GetExtension(sFullPath), Helpers.Windows.RegisteredFileType.Application.AssocStr.Executable, sCommand_Exe) Then
                    'FAIL
                    ErrorMessage("No associated application for this file type available!", MessageBoxButtons.OK)

                    Throw New Exception("No associated application for this file type available! (AssocStr.Executable)")
                End If
                'If Not Helpers.Windows.RegisteredFileType.Application.HasAssociatedApp(IO.Path.GetExtension(sFullPath)) Then
                '    'FAIL
                '    ErrorMessage("No associated application for this file type available!", MessageBoxButtons.OK)

                '    Throw New Exception("No associated application for this file type available! (AssocStr.SupportedUriProtocols)")
                'End If
                If sCommand_Full.Contains("OpenWith.exe") Then
                    ErrorMessage("No associated application for this file type available!", MessageBoxButtons.OK)

                    Throw New Exception("No dedicated application for this file type available (only OpenWith.exe) !")
                End If


                sCommand_Param = sCommand_Full.Replace(sCommand_Exe, "")
                sCommand_Param = sCommand_Param.Replace("""""", "")

                If sCommand_Param.Contains("%1") Then
                    sCommand_Param = sCommand_Param.Replace("""%1""", "%1")
                    sCommand_Param = sCommand_Param.Replace("%1", """" & sFullPath & """")
                Else
                    sCommand_Param = sCommand_Param & " """ & sFullPath & """"
                End If


                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage("associated app", 1)
                    Helpers.Logger.WriteMessage("sCommand_Full", 2)
                    Helpers.Logger.WriteMessage(sCommand_Full, 3)
                    Helpers.Logger.WriteMessage("sCommand_Exe", 2)
                    Helpers.Logger.WriteMessage(sCommand_Exe, 3)
                    Helpers.Logger.WriteMessage("sCommand_Param", 2)
                    Helpers.Logger.WriteMessage(sCommand_Param, 3)
                End If
            End If


            ShowPleaseWaitOpening()
            tmrHideOpeningDialog.Enabled = True


            If Not sCommand_Exe.StartsWith("""") Then
                sCommand_Exe = """" & sCommand_Exe
            End If
            If Not sCommand_Exe.EndsWith("""") Then
                sCommand_Exe = sCommand_Exe & """"
            End If

            If Helpers.Processes.StartProcess(sCommand_Exe, sCommand_Param, False) Then
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage("command", 1)
                    Helpers.Logger.WriteMessage(sCommand_Exe, 2)
                    Helpers.Logger.WriteMessage("param", 1)
                    Helpers.Logger.WriteMessage(sCommand_Param, 2)
                End If

                Me.WindowState = FormWindowState.Minimized
            Else
                ErrorMessage("An error occurred while opening the application: " & Helpers.Errors.GetLast(False), MessageBoxButtons.OK)

                Throw New Exception("Helpers.Processes.StartProcess error: " & Helpers.Errors.GetLast(False))
            End If
        Catch ex As Exception
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteError("exception", 1)
                Helpers.Logger.WriteError(ex.Message, 2)
            End If
        End Try
    End Sub

    Private Sub listviewFiles_MouseEnter(sender As Object, e As EventArgs) Handles listviewFiles.MouseEnter
        listviewFiles.Focus()
    End Sub

    Private Sub listviewFiles_ColumnWidthChanging(sender As Object, e As ColumnWidthChangingEventArgs) Handles listviewFiles.ColumnWidthChanging
        e.Cancel = True
        e.NewWidth = Me.listviewFiles.Columns(e.ColumnIndex).Width
    End Sub

    Private Sub listviewFiles_ColumnClick(sender As Object, e As ColumnClickEventArgs) Handles listviewFiles.ColumnClick
        Dim _column As Integer

        If e.Column = 3 Then
            'column 3 is human readable file size, column 4 is hidden numeric file size, let's use that.
            _column = 4
        Else
            _column = e.Column
        End If

        If (_column = lvwColumnSorter.SortColumn) Then
            ' Reverse the current sort direction for this column.
            If (lvwColumnSorter.Order = SortOrder.Ascending) Then
                lvwColumnSorter.Order = SortOrder.Descending
            Else
                lvwColumnSorter.Order = SortOrder.Ascending
            End If
        Else
            ' Set the column number that is to be sorted; default to ascending.
            lvwColumnSorter.SortColumn = _column
            lvwColumnSorter.Order = SortOrder.Ascending
        End If


        ' Perform the sort with these new sort options.
        listviewFiles.Sort()
    End Sub

    Private Sub listviewFiles_MouseMove(sender As Object, e As MouseEventArgs) Handles listviewFiles.MouseMove
        ''        Private void listview1_MouseMove(Object sender, MouseEventargs e)
        ''{
        ''    ListViewItem item = listview1.GetItemAt(e.X, e.Y);
        ''    ListViewHitTestInfo info = listview1.HitTest(e.X, e.Y);
        ''    If ((item!= null) && (info.SubItem!= null)) Then
        ''                {
        ''        toolTip1.SetToolTip(listview1, info.SubItem.Text);
        ''    }
        ''    Else
        ''    {
        ''        toolTip1.SetToolTip(listview1, "");
        ''    }
        ''}
        'Dim _item As ListViewItem = listviewFiles.GetItemAt(e.X, e.Y)
        'Dim _info As ListViewHitTestInfo = listviewFiles.HitTest(e.X, e.Y)

        'Dim _tooltip As New ToolTip
        'Dim _string As String = ""

        'If Not _item Is Nothing Then
        '    _string &= "_item: " & _item.Text
        '    _string &= " ; "

        '    If Not _info Is Nothing Then
        '        _string &= "_info: " & _info.SubItem.Text
        '        _string &= " ; "
        '    End If
        'End If

        '_tooltip.SetToolTip(listviewFiles, "")
    End Sub

    Private Sub listviewFiles_MouseLeave(sender As Object, e As EventArgs) Handles listviewFiles.MouseLeave
        'Dim _tooltip As New ToolTip

        '_tooltip.SetToolTip(listviewFiles, "")
    End Sub

    Private Sub listviewFiles_MouseDown(sender As Object, e As MouseEventArgs) Handles listviewFiles.MouseDown
        If Settings.Generic.ContextMenu Then
            If e.Button = MouseButtons.Right Then
                'Console.WriteLine("listviewFiles_MouseDown (" & e.X.ToString & "," & e.Y.ToString & ")")

                If listviewFiles.GetItemAt(e.X, e.Y) IsNot Nothing Then
                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("Right click on file", 0)
                        Helpers.Logger.WriteMessage(listviewFiles.GetItemAt(e.X, e.Y).Text, 1)
                    End If

                    'For Each lvItem As ListViewItem In listviewFiles.Items
                    '    lvItem.Selected = False
                    'Next
                    'listviewFiles.GetItemAt(e.X, e.Y).Selected = True

                    mFileSelectedNames = New List(Of String)
                    For Each lvi As ListViewItem In listviewFiles.SelectedItems
                        mFileSelectedNames.Add(lvi.Text)
                    Next
                    'If listviewFiles.SelectedItems.Count > 0 Then
                    '    mFileSelectedName = listviewFiles.SelectedItems.Item(0).Text
                    'Else
                    '    mFileSelectedName = ""
                    'End If

                    ' {"Open", "-", "Compress to ZIP file", "-", "Cut", "Copy", "Paste", "-", "Delete", "-", "Print", "-", "Create new folder", "-", "Properties"}
                    '     0     1           2                3     4       5       6      7       8      9     10      11    12                  13     14
                    'sendFileToPrinter


                    Try
                        contextMenu__Files.MenuItems.Item(0).Visible = True
                        contextMenu__Files.MenuItems.Item(0).Enabled = mFileSelectedNames.Count < 2

                        contextMenu__Files.MenuItems.Item(1).Visible = True
                        contextMenu__Files.MenuItems.Item(2).Visible = True
                        contextMenu__Files.MenuItems.Item(2).Enabled = mFileSelectedNames.Count > 0

                        contextMenu__Files.MenuItems.Item(3).Visible = True
                        contextMenu__Files.MenuItems.Item(4).Visible = True
                        contextMenu__Files.MenuItems.Item(5).Visible = True
                        contextMenu__Files.MenuItems.Item(6).Visible = True
                        contextMenu__Files.MenuItems.Item(6).Enabled = itemsCopied.Count > 0

                        contextMenu__Files.MenuItems.Item(7).Visible = Settings.Generic.AllowFileDelete
                        contextMenu__Files.MenuItems.Item(8).Visible = Settings.Generic.AllowFileDelete

                        contextMenu__Files.MenuItems.Item(9).Visible = Settings.Generic.AllowPrinting
                        contextMenu__Files.MenuItems.Item(10).Visible = Settings.Generic.AllowPrinting
                        contextMenu__Files.MenuItems.Item(10).Enabled = (mFileSelectedNames.Count = 1)

                        contextMenu__Files.MenuItems.Item(11).Visible = False
                        contextMenu__Files.MenuItems.Item(12).Visible = False

                        contextMenu__Files.MenuItems.Item(13).Visible = True
                        contextMenu__Files.MenuItems.Item(14).Visible = True
                        contextMenu__Files.MenuItems.Item(14).Enabled = mFileSelectedNames.Count < 2


                        contextMenu__Files.Show(listviewFiles, New Point(e.X, e.Y))
                    Catch ex As Exception

                    End Try
                Else
                    Try
                        If IO.Directory.Exists(treeFolders.SelectedNode.Name) Then
                            contextMenu__Files.MenuItems.Item(0).Visible = False

                            contextMenu__Files.MenuItems.Item(1).Visible = False
                            contextMenu__Files.MenuItems.Item(2).Visible = False

                            contextMenu__Files.MenuItems.Item(3).Visible = False
                            contextMenu__Files.MenuItems.Item(4).Visible = False
                            contextMenu__Files.MenuItems.Item(5).Visible = False
                            contextMenu__Files.MenuItems.Item(6).Visible = True
                            contextMenu__Files.MenuItems.Item(6).Enabled = itemsCopied.Count > 0

                            contextMenu__Files.MenuItems.Item(7).Visible = False
                            contextMenu__Files.MenuItems.Item(8).Visible = False

                            contextMenu__Files.MenuItems.Item(9).Visible = False
                            contextMenu__Files.MenuItems.Item(10).Visible = False

                            contextMenu__Files.MenuItems.Item(11).Visible = True
                            contextMenu__Files.MenuItems.Item(12).Visible = True

                            contextMenu__Files.MenuItems.Item(13).Visible = False
                            contextMenu__Files.MenuItems.Item(14).Visible = False


                            contextMenu__Files.Show(listviewFiles, New Point(e.X, e.Y))
                        End If
                    Catch ex As Exception

                    End Try
                End If
            End If
        End If
    End Sub
#End Region

#Region "treeFolders"
    Private Sub treeFolders_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles treeFolders.AfterSelect
        mSelectedTreeNodeName = treeFolders.SelectedNode.Name


        If treeFolders.SelectedNode.Parent Is Nothing Then
            _ThreadSafe__listviewFiles_Clear()
            mFileCount = 0
            mFileSelectedCount = 0
            _ThreadSafe__lblNoFilesFound_Show()
            _ThreadSafe__UpdateStatusBar()

            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage("Invalid node, probably fake C: drive node", 0)
            End If

            Exit Sub
        End If

        lblFakeTabFolderName.Text = treeFolders.SelectedNode.Text
        _buildFoldersPathThingie()

        ShowPleaseWait()

        _LoadFiles()
    End Sub

    Private Sub _buildFoldersPathThingie()
        Dim lTmp As New List(Of String)


        lTmp.Add(treeFolders.SelectedNode.Text)


        Dim _parentNode As TreeNode = treeFolders.SelectedNode.Parent

        Do While _parentNode IsNot Nothing
            lTmp.Add(_parentNode.Text)

            _parentNode = _parentNode.Parent
        Loop

        lTmp.Add("")

        lblFolderPath.Text = String.Join("  >  ", lTmp.ToArray.Reverse)
    End Sub

    Private Sub treeFolders_BeforeCollapse(sender As Object, e As TreeViewCancelEventArgs) Handles treeFolders.BeforeCollapse
        If Settings.Generic.DirectoriesCollapsable Then

        Else
            'e.Cancel = True
        End If
    End Sub

    Private Sub treeFolders_AfterCollapse(sender As Object, e As TreeViewEventArgs) Handles treeFolders.AfterCollapse
        treeFolders.Nodes(0).Expand()
    End Sub

    Private Sub treeFolders_DrawNode(ByVal sender As Object, ByVal e As DrawTreeNodeEventArgs) Handles treeFolders.DrawNode
        If (e.State And TreeNodeStates.Selected) <> 0 Then
            e.Graphics.FillRectangle(Brushes.LightGray, e.Bounds)
            TextRenderer.DrawText(e.Graphics, e.Node.Text, e.Node.NodeFont, e.Bounds, Color.Black, Color.Empty, TextFormatFlags.VerticalCenter)
        Else
            e.Graphics.FillRectangle(SystemBrushes.Window, e.Bounds)
            TextRenderer.DrawText(e.Graphics, e.Node.Text, e.Node.NodeFont, e.Bounds, SystemColors.WindowText, Color.Empty, TextFormatFlags.VerticalCenter)
        End If
    End Sub

    Private Sub treeFolders_MouseEnter(sender As Object, e As EventArgs) Handles treeFolders.MouseEnter
        treeFolders.Focus()
    End Sub

    Private Sub treeFolders_MouseDown(sender As Object, e As MouseEventArgs) Handles treeFolders.MouseDown
        If Settings.Generic.ContextMenu Then
            If e.Button = MouseButtons.Right Then
                Dim nodeRightClicked As TreeNode = treeFolders.GetNodeAt(e.X, e.Y)

                If nodeRightClicked IsNot Nothing Then
                    If Settings.Generic.Debugging Then
                        Helpers.Logger.WriteMessage("Right click on folder", 0)
                        Helpers.Logger.WriteMessage(nodeRightClicked.Text, 1)
                    End If

                    treeFolders.SelectedNode = nodeRightClicked


                    ' {"Open", "-", "Compress to ZIP file", "-", "Cut", "Copy", "Paste", "-", "Delete", "-", "Print", "-", "Create new folder", "-", "Properties"}
                    '     0     1           2                3     4       5       6      7       8      9     10      11    12                  13     14
                    'sendFileToPrinter


                    Try
                        contextMenu__Files.MenuItems.Item(0).Visible = False

                        contextMenu__Files.MenuItems.Item(1).Visible = False
                        contextMenu__Files.MenuItems.Item(2).Visible = False

                        contextMenu__Files.MenuItems.Item(3).Visible = False
                        contextMenu__Files.MenuItems.Item(4).Visible = False
                        contextMenu__Files.MenuItems.Item(5).Visible = False
                        contextMenu__Files.MenuItems.Item(6).Visible = True
                        contextMenu__Files.MenuItems.Item(6).Enabled = itemsCopied.Count > 0

                        contextMenu__Files.MenuItems.Item(7).Visible = False
                        contextMenu__Files.MenuItems.Item(8).Visible = False

                        contextMenu__Files.MenuItems.Item(9).Visible = False
                        contextMenu__Files.MenuItems.Item(10).Visible = False

                        contextMenu__Files.MenuItems.Item(11).Visible = False
                        contextMenu__Files.MenuItems.Item(12).Visible = False

                        contextMenu__Files.MenuItems.Item(13).Visible = False
                        contextMenu__Files.MenuItems.Item(14).Visible = False


                        contextMenu__Files.Show(treeFolders, New Point(e.X, e.Y))
                    Catch ex As Exception

                    End Try
                End If
            End If
        End If
    End Sub
#End Region

#Region "tmrHideOpeningDialog"
    Private Sub tmrHideOpeningDialog_Tick(sender As Object, e As EventArgs) Handles tmrHideOpeningDialog.Tick
        tmrHideOpeningDialog.Enabled = False

        HidePleaseWaitOpening()
    End Sub
#End Region

#Region "lblAppVersion"
    Private Sub lblAppVersion_MouseEnter(sender As Object, e As EventArgs) Handles lblAppVersion.MouseEnter
        lblAppVersion.Text = mLongVersionString
    End Sub

    Private Sub lblAppVersion_MouseLeave(sender As Object, e As EventArgs) Handles lblAppVersion.MouseLeave
        tmrHideLongVersionString.Enabled = True
    End Sub
#End Region

#Region "tmrHideLongVersionString"
    Private Sub tmrHideLongVersionString_Tick(sender As Object, e As EventArgs) Handles tmrHideLongVersionString.Tick
        tmrHideLongVersionString.Enabled = False

        lblAppVersion.Text = mShortVersionString
    End Sub
#End Region
#End Region

    Private Function ErrorMessage(ErrorString As String, _MessageBoxButtons As MessageBoxButtons) As DialogResult
        Return MessageBox.Show(ErrorString, Application.ProductName & " - ERROR", _MessageBoxButtons, MessageBoxIcon.Error)
    End Function

    Private Sub _ExtractZip(FullPath As String)
        Dim extractPath As String = ""

        'get root folder of zip file
        extractPath &= IO.Path.GetDirectoryName(FullPath)

        'add backslash
        extractPath &= "\"

        'add filename without extension
        extractPath &= IO.Path.GetFileName(FullPath).Replace(IO.Path.GetExtension(FullPath), "")

        'add underscore
        extractPath &= "_"

        'add datetime
        'extractPath &= Now().ToString("").Replace("-", "").Replace(":", "").Replace("T", "_").Replace(" ", "_")
        'extractPath &= Now().ToString("HHmmss") 
        If Settings.Generic.AppendUnzippedFolderWithFullDate Then
            extractPath &= Now().ToString("yyyyMMdd_HHmmss")
        Else
            extractPath &= Now().ToString("HHmmss")
        End If


        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("Extracting file", 0)
            Helpers.Logger.WriteMessage("file", 1)
            Helpers.Logger.WriteMessage(FullPath, 2)
            Helpers.Logger.WriteMessage("path", 1)
            Helpers.Logger.WriteMessage(extractPath, 2)
        End If


        Try
            ZipFile.ExtractToDirectory(FullPath, extractPath)

            Helpers.Logger.WriteMessage("ok", 1)


            Helpers.Logger.WriteMessage("reloading folders", 1)
            _LoadFoldersIntoTreeview(extractPath)
        Catch ex As Exception
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteError("exception", 1)
                Helpers.Logger.WriteError(ex.Message, 2)
            End If

            ErrorMessage("An error occurred while extracting '" & IO.Path.GetFileName(FullPath) & ": " & ex.Message, MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub _LoadFiles()
        'mSelectedPath = treeFolders.SelectedNode.Name
        _ThreadSafe__treeFolders_SelectedNode_Name_Store()

        If Not bgWorkerFiles.IsBusy Then
            bgWorkerFiles.RunWorkerAsync()
        End If
    End Sub

#Region "wait panels"
    Private Sub ShowPleaseWait()
        ToggleControls.ClearExceptions()
        ToggleControls.Disable(Me)

        _ThreadSafe__progressBar_Show()
    End Sub

    Private Sub ShowPleaseWaitOpening()
        ToggleControls.ClearExceptions()
        ToggleControls.Disable(Me)

        pnlWaitOpening.Visible = True

        pnlWaitOpening.Left = listviewFiles.Left + (listviewFiles.Width - pnlWaitOpening.Width) / 2
        pnlWaitOpening.Top = listviewFiles.Top + (listviewFiles.Height - pnlWaitOpening.Height) / 2

        pnlWaitOpeningShadow.Width = pnlWaitOpening.Width
        pnlWaitOpeningShadow.Height = pnlWaitOpening.Height
        pnlWaitOpeningShadow.Top = pnlWaitOpening.Top + 3
        pnlWaitOpeningShadow.Left = pnlWaitOpening.Left + 3

        pnlWaitOpeningShadow.BringToFront()
        pnlWaitOpening.BringToFront()
    End Sub


    Private Sub HidePleaseWait()
        ToggleControls.Restore(Me)

        _ThreadSafe__progressBar_Hide()

        'pnlWait.Visible = False
        'pnlWait.Left = 1200
        'pnlWait.Top = 600
        'pnlWaitShadow.Top = 1200
        'pnlWaitShadow.Left = 600
    End Sub

    Private Sub HidePleaseWaitOpening()
        ToggleControls.Restore(Me)

        pnlWaitOpening.Visible = False
        pnlWaitOpening.Left = 1200
        pnlWaitOpening.Top = 600
        pnlWaitOpeningShadow.Top = 1200
        pnlWaitOpeningShadow.Left = 600
    End Sub

    Private Sub pnlWaitOpening_Paint(sender As Object, e As PaintEventArgs) Handles pnlWaitOpening.Paint
        pnlWaitOpening.BorderStyle = BorderStyle.None
        e.Graphics.DrawRectangle(Pens.Red, e.ClipRectangle.Left, e.ClipRectangle.Top, e.ClipRectangle.Width - 1, e.ClipRectangle.Height - 1)
    End Sub

#End Region


    Private Sub contextMenu__Files__Item_Click(sender As Object, ByVal e As EventArgs)
        If Settings.Generic.Debugging Then
            Helpers.Logger.WriteMessage("contextMenu activity", 0)
            'Helpers.Logger.WriteMessage("'" & sender.name & "' " & mFileSelectedName, 1)
            Helpers.Logger.WriteMessage("'" & sender.name & "'", 1)
        End If


        'Console.WriteLine("sender.name=" & sender.name)

        ' {"Open", "-", "Compress to ZIP file", "-", "Cut", "Copy", "Paste", "-", "Delete", "-", "Print", "-", "Create new folder", "-", "Properties"}
        Select Case sender.name
            Case "Open"
                listviewFiles_DoubleClick(sender, e)
            Case "Compress to ZIP file"
                CompressToZip()
            Case "Create new folder"
                AddNewFolderToListview()
            Case "Delete"
                DeleteFileFolder()
            Case "Properties"
                ShowProperties()
            Case "Cut"
                StartCutPaste()
            Case "Copy"
                StartCopyPaste()
            Case "Paste"
                FinaliseCopyPaste()
            Case "Print"
                'Me.WindowState = FormWindowState.Minimized
                sendFileToPrinter(IO.Path.Combine(treeFolders.SelectedNode.Name, mFileSelectedNames.Item(0)))
            Case Else

        End Select
    End Sub

    Private Sub CompressToZip()
        Helpers.Logger.WriteMessage("compress to zip", 0)


        Me.Opacity = 0.9

        Dim frm As New frmCompress
        frm.Moveable = False
        frm.CurrentDir = treeFolders.SelectedNode.Name

        Dim dlg As DialogResult = frm.ShowDialog(Me)

        Select Case dlg
            Case DialogResult.Cancel
                'Cancel
                Helpers.Logger.WriteMessage("canceled", 1)
            Case DialogResult.Abort
                'Aborted
                Helpers.Logger.WriteMessage("aborted", 1)
            Case DialogResult.OK
                'Let's go
                Helpers.Logger.WriteMessage("ok", 1)

                _LoadFiles()
            Case Else
                'unknown
        End Select

        Me.Opacity = 1.0

        frm.Dispose()
    End Sub

    Private Sub ShowProperties()
        Me.Opacity = 0.9

        Dim frm As New frmProperties
        frm.Moveable = False

        Dim dlg As DialogResult = frm.ShowDialog(Me)

        Me.Opacity = 1.0

        frm.Dispose()
    End Sub

    Private Sub DeleteFileFolder()
        For Each tmpFileSelectedName As String In mFileSelectedNames
            mFileSelectedName = tmpFileSelectedName

            Dim o As String = IO.Path.Combine(treeFolders.SelectedNode.Name, mFileSelectedName)


            Helpers.Logger.WriteMessage("deleting file/folder", 0)
            Helpers.Logger.WriteMessage("path", 1)
            Helpers.Logger.WriteMessage(o, 2)


            Me.Opacity = 0.9
            Dim frm As New frmDeleteConfirmation
            frm.Moveable = False
            Dim dlg As DialogResult = frm.ShowDialog(Me)
            Me.Opacity = 1.0

            Select Case dlg
                Case DialogResult.Cancel
                    'Cancel
                    Helpers.Logger.WriteMessage("canceled", 1)
                Case DialogResult.Abort
                    'Aborted
                    Helpers.Logger.WriteMessage("aborted", 1)
                Case DialogResult.OK
                    'Let's go
                    Helpers.Logger.WriteMessage("deleting", 1)

                    Try
                        If IO.Directory.Exists(o) Then
                            IO.Directory.Delete(o, True)

                            If IO.Directory.Exists(o) Then
                                Helpers.Logger.WriteMessage("fail", 2)
                            Else
                                Helpers.Logger.WriteMessage("ok", 2)
                            End If
                        ElseIf IO.File.Exists(o) Then
                            IO.File.Delete(o)

                            If IO.File.Exists(o) Then
                                Helpers.Logger.WriteMessage("fail", 2)
                            Else
                                Helpers.Logger.WriteMessage("ok", 2)
                            End If
                        End If
                    Catch ex As Exception

                    End Try


                    _LoadFoldersIntoTreeview(treeFolders.SelectedNode.Name)
                Case Else
                    'unknown
            End Select

            frm.Dispose()

        Next
    End Sub

    Private Sub AddNewFolderToListview()
        Me.Opacity = 0.9

        Dim frm As New frmNewFolder

        frm.Moveable = False

        Dim dlg As DialogResult = frm.ShowDialog(Me)

        Me.Opacity = 1.0

        Select Case dlg
            Case DialogResult.Cancel
                'cancel
            Case DialogResult.OK
                If IO.Directory.Exists(treeFolders.SelectedNode.Name) Then
                    Try
                        IO.Directory.CreateDirectory(IO.Path.Combine(treeFolders.SelectedNode.Name, frm.txtFolderName.Text))

                        If IO.Directory.Exists(IO.Path.Combine(treeFolders.SelectedNode.Name, frm.txtFolderName.Text)) Then
                            _LoadFoldersIntoTreeview(IO.Path.Combine(treeFolders.SelectedNode.Name, frm.txtFolderName.Text))
                        End If
                    Catch ex As Exception

                    End Try

                End If

            Case Else
                'don't care
        End Select

        frm.Dispose()
    End Sub

    Private Sub listviewFiles_ItemDrag(sender As Object, e As ItemDragEventArgs) Handles listviewFiles.ItemDrag
        itemsCopied = New List(Of _listviewItemCopied)

        For Each lvi As ListViewItem In listviewFiles.SelectedItems
            Dim lvid As New _listviewItemCopied
            lvid.FileName = lvi.Text
            lvid.FileFullPath = IO.Path.Combine(treeFolders.SelectedNode.Name, lvi.Text)

            itemsCopied.Add(lvid)
        Next

        Dim iTotal As Integer = itemsCopied.Count

        sender.DoDragDrop(New DataObject("Integer", iTotal), DragDropEffects.Copy)
    End Sub

    Private Sub treeFolders_DragEnter(sender As Object, e As DragEventArgs) Handles treeFolders.DragEnter
        If e.Data.GetDataPresent("Integer") Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub treeFolders_DragOver(sender As Object, e As DragEventArgs) Handles treeFolders.DragOver
        If Not e.Data.GetDataPresent("Integer") Then
            Exit Sub
        End If

        ClearTreenodeHightlight(treeFolders.Nodes)
        Dim dropLocation = treeFolders.PointToClient(New Point(e.X, e.Y))
        Dim tnFound As TreeNode = treeFolders.GetNodeAt(dropLocation)
        If tnFound IsNot Nothing Then
            If treeFolders.SelectedNode.Name.Equals(tnFound.Name) Or tnFound.Name.Equals("") Then
                e.Effect = DragDropEffects.None
            Else
                e.Effect = DragDropEffects.Copy
                tnFound.BackColor = Color.LightGray
            End If

        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub ClearTreenodeHightlight(nodes As TreeNodeCollection)
        For Each node As TreeNode In nodes
            node.BackColor = Color.Empty
            ClearTreenodeHightlight(node.Nodes)
        Next
    End Sub

    Private Sub treeFolders_DragDrop(sender As Object, e As DragEventArgs) Handles treeFolders.DragDrop
        If Not e.Data.GetDataPresent("Integer") Then
            Exit Sub
        End If

        ClearTreenodeHightlight(treeFolders.Nodes)

        Dim dropLocation = treeFolders.PointToClient(New Point(e.X, e.Y))
        Dim tnFound As TreeNode = treeFolders.GetNodeAt(dropLocation)

        treeFolders.SelectedNode = tnFound

        If tnFound IsNot Nothing Then
            sFolderCopiedInto = tnFound.Name

            If Not bgWorkerFileCopy.IsBusy Then
                bgWorkerFileCopy.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub bgWorkerFileCopy_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgWorkerFileCopy.DoWork
        Dim sSource As String = ""
        Dim sDestination As String = ""

        For Each lvid As _listviewItemCopied In itemsCopied
            Try
                sSource = lvid.FileFullPath
                sDestination = IO.Path.Combine(sFolderCopiedInto, lvid.FileName)

                mFileDraggedName = sDestination
                mFileDraggedIsDir = My.Computer.FileSystem.DirectoryExists(sDestination)

                If My.Computer.FileSystem.FileExists(sDestination) Or My.Computer.FileSystem.DirectoryExists(sDestination) Then
                    'File already exists, show dialog
                    _ThreadSafe__frmOverwriteConfirmation()


                    Select Case mDialogResponse
                        Case DialogResult.Cancel
                            Exit Sub
                        Case DialogResult.OK
                            'Go through with the copying
                        Case Else
                            Exit Sub
                    End Select
                End If

                If My.Computer.FileSystem.FileExists(sSource) Then
                    My.Computer.FileSystem.CopyFile(sSource, sDestination, True)

                    If itemsAreCut Then
                        If My.Computer.FileSystem.FileExists(sDestination) Then
                            My.Computer.FileSystem.DeleteFile(sSource)
                        End If
                    End If
                ElseIf My.Computer.FileSystem.DirectoryExists(sSource) Then
                    My.Computer.FileSystem.CopyDirectory(sSource, sFolderCopiedInto, True)

                    If itemsAreCut Then
                        If My.Computer.FileSystem.DirectoryExists(sDestination) Then
                            My.Computer.FileSystem.DeleteFile(sSource)
                        End If
                    End If
                End If
            Catch ex As Exception
                If ex.GetType().Name.Equals("UnauthorizedAccessException") Then

                End If

                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteError("Error while copying file", 0)
                    Helpers.Logger.WriteError("source", 1)
                    Helpers.Logger.WriteError(sSource, 2)
                    Helpers.Logger.WriteError("destination", 1)
                    Helpers.Logger.WriteError(sDestination, 2)
                    Helpers.Logger.WriteError("error", 1)
                    Helpers.Logger.WriteError(ex.Message, 2)
                End If
            End Try

            Threading.Thread.Sleep(500)
        Next
    End Sub

    Private Sub bgWorkerFileCopy_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgWorkerFileCopy.RunWorkerCompleted
        itemsAreCut = False

        HidePleaseWait()

        itemsCopied = New List(Of _listviewItemCopied)

        _LoadFoldersIntoTreeview(treeFolders.SelectedNode.Name)
    End Sub

    Private Sub StartCutPaste()
        itemsAreCut = True

        StartCopyPaste()
    End Sub

    Private Sub StartCopyPaste()
        itemsCopied = New List(Of _listviewItemCopied)

        For Each lvi As ListViewItem In listviewFiles.SelectedItems
            Dim lvid As New _listviewItemCopied
            lvid.FileName = lvi.Text
            lvid.FileFullPath = IO.Path.Combine(treeFolders.SelectedNode.Name, lvi.Text)

            Console.WriteLine(lvid.FileFullPath)

            itemsCopied.Add(lvid)
        Next

        contextMenu__Files.MenuItems.Item(4).Visible = True
    End Sub

    Private Sub FinaliseCopyPaste()
        contextMenu__Files.MenuItems.Item(4).Visible = False

        Dim sDestinationPath As String = treeFolders.SelectedNode.Name

        sFolderCopiedInto = treeFolders.SelectedNode.Name

        If IO.Directory.Exists(sFolderCopiedInto) Then
            If Not bgWorkerFileCopy.IsBusy Then
                bgWorkerFileCopy.RunWorkerAsync()
            End If
        End If
    End Sub


    '----------------------------------------------------------------------------------------------------------------
    Private isMouseDown As Boolean = False
    Private mouseOffset As Point

    Private Sub picTopBarLeft_MouseDown(sender As Object, e As MouseEventArgs) Handles picTopBarLeft.MouseDown
        If e.Button = MouseButtons.Left Then
            ' Get the new position
            mouseOffset = New Point(-e.X - pnlBorder.Left, -e.Y - pnlBorder.Top)
            ' Set that left button is pressed
            isMouseDown = True
        End If
    End Sub

    Private Sub picTopBarLeft1_MouseMove(sender As Object, e As MouseEventArgs) Handles picTopBarLeft.MouseMove
        If isMouseDown Then
            Dim mousePos As Point = Control.MousePosition
            ' Get the new form position
            mousePos.Offset(mouseOffset.X, mouseOffset.Y)
            Me.Location = mousePos
        End If
    End Sub

    Private Sub picTopBarLeft_MouseUp(sender As Object, e As MouseEventArgs) Handles picTopBarLeft.MouseUp
        If e.Button = MouseButtons.Left Then
            isMouseDown = False
        End If
    End Sub


    Private Sub picMinimize_Click(sender As Object, e As EventArgs) Handles picMinimize.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub

    Private Sub picMinimize_MouseEnter(sender As Object, e As EventArgs) Handles picMinimize.MouseEnter
        picMinimize.Image = My.Resources.topbar_buttons__minimize_hover
    End Sub

    Private Sub picMinimize_MouseLeave(sender As Object, e As EventArgs) Handles picMinimize.MouseLeave
        picMinimize.Image = My.Resources.topbar_buttons__minimize_on
    End Sub

    Private Sub picClose_Click(sender As Object, e As EventArgs) Handles picClose.Click
        Me.Close()
    End Sub

    Private Sub picClose_MouseEnter(sender As Object, e As EventArgs) Handles picClose.MouseEnter
        picClose.Image = My.Resources.topbar_buttons__close_hover
    End Sub

    Private Sub picClose_MouseLeave(sender As Object, e As EventArgs) Handles picClose.MouseLeave
        picClose.Image = My.Resources.topbar_buttons__close_on
    End Sub

    Private Sub picMiddleBarButton_Up_Click(sender As Object, e As EventArgs) Handles picMiddleBarButton_Up.Click
        Dim _parentNode As TreeNode = treeFolders.SelectedNode.Parent

        If _parentNode IsNot Nothing Then
            If Not _parentNode.Name.Equals("") Then
                treeFolders.SelectedNode = _parentNode
            End If
        End If
    End Sub

    Private Sub picMiddleBarButton_Up_MouseEnter(sender As Object, e As EventArgs) Handles picMiddleBarButton_Up.MouseEnter
        picMiddleBarButton_Up.Image = My.Resources.middlebar_buttons__up_hover
    End Sub

    Private Sub picMiddleBarButton_Up_MouseLeave(sender As Object, e As EventArgs) Handles picMiddleBarButton_Up.MouseLeave
        picMiddleBarButton_Up.Image = My.Resources.middlebar_buttons__up_on
    End Sub

    Private Sub picMiddleBarButton_Reload_Click(sender As Object, e As EventArgs) Handles picMiddleBarButton_Reload.Click
        treeFolders_AfterSelect(treeFolders, New TreeViewEventArgs(treeFolders.SelectedNode))
    End Sub

    Private Sub picMiddleBarButton_Reload_MouseEnter(sender As Object, e As EventArgs) Handles picMiddleBarButton_Reload.MouseEnter
        picMiddleBarButton_Reload.Image = My.Resources.middlebar_buttons__reload_hover
    End Sub

    Private Sub picMiddleBarButton_Reload_MouseLeave(sender As Object, e As EventArgs) Handles picMiddleBarButton_Reload.MouseLeave
        picMiddleBarButton_Reload.Image = My.Resources.middlebar_buttons__reload_on
    End Sub


    '-------------------------------------------------------
    Private Sub sendFileToPrinter(filePath As String)
        Dim info As New ProcessStartInfo()
        info.Verb = "print"
        info.FileName = filePath
        info.CreateNoWindow = True
        info.WindowStyle = ProcessWindowStyle.Hidden

        Dim p As New Process()
        p.StartInfo = info
        p.Start()
    End Sub

    Private Sub picTopBarLeft_Click(sender As Object, e As EventArgs) Handles picTopBarLeft.Click

    End Sub

    Private Sub listviewFiles_RightToLeftLayoutChanged(sender As Object, e As EventArgs) Handles listviewFiles.RightToLeftLayoutChanged

    End Sub
End Class
