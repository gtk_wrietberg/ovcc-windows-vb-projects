﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.listviewFiles = New System.Windows.Forms.ListView()
        Me.colHeader_Name = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Date = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeaderHidden_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.pnlStatusbar = New System.Windows.Forms.Panel()
        Me.lblAppVersion = New System.Windows.Forms.Label()
        Me.lblStatusbar = New System.Windows.Forms.Label()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.progressBar = New System.Windows.Forms.ProgressBar()
        Me.lblNoFilesFound = New System.Windows.Forms.Label()
        Me.treeFolders = New FileExplorer.BufferedTreeView()
        Me.bgWorkerFiles = New System.ComponentModel.BackgroundWorker()
        Me.pnlWaitOpening = New System.Windows.Forms.Panel()
        Me.lblOpeningApp = New System.Windows.Forms.Label()
        Me.pnlWaitOpeningShadow = New System.Windows.Forms.Panel()
        Me.tmrHideOpeningDialog = New System.Windows.Forms.Timer(Me.components)
        Me.tmrHideLongVersionString = New System.Windows.Forms.Timer(Me.components)
        Me.bgWorkerFileCopy = New System.ComponentModel.BackgroundWorker()
        Me.picMiddleBarRight = New System.Windows.Forms.PictureBox()
        Me.pnlBorder = New System.Windows.Forms.Panel()
        Me.pnlHeader = New System.Windows.Forms.Panel()
        Me.pnlTopBar = New System.Windows.Forms.Panel()
        Me.lblFakeTabFolderName = New System.Windows.Forms.Label()
        Me.pnlTopBar_Buttons = New System.Windows.Forms.Panel()
        Me.picClose = New System.Windows.Forms.PictureBox()
        Me.picMaximize = New System.Windows.Forms.PictureBox()
        Me.picMinimize = New System.Windows.Forms.PictureBox()
        Me.picTopBarLeft = New System.Windows.Forms.PictureBox()
        Me.pnlMiddleBar = New System.Windows.Forms.Panel()
        Me.lblFolderPath = New System.Windows.Forms.Label()
        Me.pnlMiddleBarLeft = New System.Windows.Forms.Panel()
        Me.picMiddleBarButton_Reload = New System.Windows.Forms.PictureBox()
        Me.picMiddleBarButton_Back = New System.Windows.Forms.PictureBox()
        Me.picMiddleBarButton_Up = New System.Windows.Forms.PictureBox()
        Me.picMiddleBarButton_Next = New System.Windows.Forms.PictureBox()
        Me.pnlStatusbar.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.pnlWaitOpening.SuspendLayout()
        CType(Me.picMiddleBarRight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBorder.SuspendLayout()
        Me.pnlHeader.SuspendLayout()
        Me.pnlTopBar.SuspendLayout()
        Me.pnlTopBar_Buttons.SuspendLayout()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMaximize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMinimize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTopBarLeft, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMiddleBar.SuspendLayout()
        Me.pnlMiddleBarLeft.SuspendLayout()
        CType(Me.picMiddleBarButton_Reload, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMiddleBarButton_Back, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMiddleBarButton_Up, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picMiddleBarButton_Next, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'listviewFiles
        '
        Me.listviewFiles.BackColor = System.Drawing.Color.White
        Me.listviewFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.listviewFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colHeader_Name, Me.colHeader_Date, Me.colHeader_Type, Me.colHeader_Size, Me.colHeaderHidden_Size})
        Me.listviewFiles.Dock = System.Windows.Forms.DockStyle.Fill
        Me.listviewFiles.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listviewFiles.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.listviewFiles.HideSelection = False
        Me.listviewFiles.Location = New System.Drawing.Point(242, 0)
        Me.listviewFiles.Name = "listviewFiles"
        Me.listviewFiles.ShowItemToolTips = True
        Me.listviewFiles.Size = New System.Drawing.Size(829, 503)
        Me.listviewFiles.TabIndex = 2
        Me.listviewFiles.UseCompatibleStateImageBehavior = False
        Me.listviewFiles.View = System.Windows.Forms.View.Details
        '
        'colHeader_Name
        '
        Me.colHeader_Name.Text = "Name"
        Me.colHeader_Name.Width = 400
        '
        'colHeader_Date
        '
        Me.colHeader_Date.Text = "Date modified"
        Me.colHeader_Date.Width = 120
        '
        'colHeader_Type
        '
        Me.colHeader_Type.Text = "Type"
        Me.colHeader_Type.Width = 160
        '
        'colHeader_Size
        '
        Me.colHeader_Size.Text = "Size"
        '
        'colHeaderHidden_Size
        '
        Me.colHeaderHidden_Size.Text = ""
        Me.colHeaderHidden_Size.Width = 0
        '
        'pnlStatusbar
        '
        Me.pnlStatusbar.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.pnlStatusbar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlStatusbar.Controls.Add(Me.lblAppVersion)
        Me.pnlStatusbar.Controls.Add(Me.lblStatusbar)
        Me.pnlStatusbar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlStatusbar.Location = New System.Drawing.Point(242, 478)
        Me.pnlStatusbar.Name = "pnlStatusbar"
        Me.pnlStatusbar.Size = New System.Drawing.Size(829, 25)
        Me.pnlStatusbar.TabIndex = 3
        '
        'lblAppVersion
        '
        Me.lblAppVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblAppVersion.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppVersion.ForeColor = System.Drawing.Color.Black
        Me.lblAppVersion.Location = New System.Drawing.Point(481, -1)
        Me.lblAppVersion.Name = "lblAppVersion"
        Me.lblAppVersion.Size = New System.Drawing.Size(343, 24)
        Me.lblAppVersion.TabIndex = 8
        Me.lblAppVersion.Text = "App name and version go here"
        Me.lblAppVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblStatusbar
        '
        Me.lblStatusbar.BackColor = System.Drawing.Color.Transparent
        Me.lblStatusbar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatusbar.ForeColor = System.Drawing.Color.Black
        Me.lblStatusbar.Location = New System.Drawing.Point(3, -1)
        Me.lblStatusbar.Name = "lblStatusbar"
        Me.lblStatusbar.Size = New System.Drawing.Size(440, 24)
        Me.lblStatusbar.TabIndex = 7
        Me.lblStatusbar.Text = "xx items             xx item selected           xxkB"
        Me.lblStatusbar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMain
        '
        Me.pnlMain.AutoScroll = True
        Me.pnlMain.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlMain.Controls.Add(Me.progressBar)
        Me.pnlMain.Controls.Add(Me.pnlStatusbar)
        Me.pnlMain.Controls.Add(Me.lblNoFilesFound)
        Me.pnlMain.Controls.Add(Me.listviewFiles)
        Me.pnlMain.Controls.Add(Me.treeFolders)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlMain.Location = New System.Drawing.Point(0, 85)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1071, 503)
        Me.pnlMain.TabIndex = 4
        '
        'progressBar
        '
        Me.progressBar.Location = New System.Drawing.Point(125, 341)
        Me.progressBar.MarqueeAnimationSpeed = 50
        Me.progressBar.Name = "progressBar"
        Me.progressBar.Size = New System.Drawing.Size(268, 22)
        Me.progressBar.Step = 1
        Me.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.progressBar.TabIndex = 0
        Me.progressBar.Visible = False
        '
        'lblNoFilesFound
        '
        Me.lblNoFilesFound.BackColor = System.Drawing.Color.Transparent
        Me.lblNoFilesFound.ForeColor = System.Drawing.Color.DarkGray
        Me.lblNoFilesFound.Location = New System.Drawing.Point(262, 30)
        Me.lblNoFilesFound.Name = "lblNoFilesFound"
        Me.lblNoFilesFound.Size = New System.Drawing.Size(757, 22)
        Me.lblNoFilesFound.TabIndex = 5
        Me.lblNoFilesFound.Text = "This folder is empty."
        Me.lblNoFilesFound.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'treeFolders
        '
        Me.treeFolders.AllowDrop = True
        Me.treeFolders.BackColor = System.Drawing.Color.White
        Me.treeFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.treeFolders.Dock = System.Windows.Forms.DockStyle.Left
        Me.treeFolders.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeFolders.HideSelection = False
        Me.treeFolders.Indent = 20
        Me.treeFolders.ItemHeight = 20
        Me.treeFolders.Location = New System.Drawing.Point(0, 0)
        Me.treeFolders.Name = "treeFolders"
        Me.treeFolders.ShowLines = False
        Me.treeFolders.ShowRootLines = False
        Me.treeFolders.Size = New System.Drawing.Size(242, 503)
        Me.treeFolders.TabIndex = 0
        '
        'bgWorkerFiles
        '
        Me.bgWorkerFiles.WorkerReportsProgress = True
        Me.bgWorkerFiles.WorkerSupportsCancellation = True
        '
        'pnlWaitOpening
        '
        Me.pnlWaitOpening.BackColor = System.Drawing.Color.White
        Me.pnlWaitOpening.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWaitOpening.Controls.Add(Me.lblOpeningApp)
        Me.pnlWaitOpening.Location = New System.Drawing.Point(1190, 279)
        Me.pnlWaitOpening.Name = "pnlWaitOpening"
        Me.pnlWaitOpening.Size = New System.Drawing.Size(182, 46)
        Me.pnlWaitOpening.TabIndex = 7
        '
        'lblOpeningApp
        '
        Me.lblOpeningApp.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpeningApp.Location = New System.Drawing.Point(3, 5)
        Me.lblOpeningApp.Name = "lblOpeningApp"
        Me.lblOpeningApp.Size = New System.Drawing.Size(174, 34)
        Me.lblOpeningApp.TabIndex = 0
        Me.lblOpeningApp.Text = "one moment please" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "starting application"
        Me.lblOpeningApp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlWaitOpeningShadow
        '
        Me.pnlWaitOpeningShadow.BackColor = System.Drawing.Color.DarkRed
        Me.pnlWaitOpeningShadow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWaitOpeningShadow.Location = New System.Drawing.Point(1190, 341)
        Me.pnlWaitOpeningShadow.Name = "pnlWaitOpeningShadow"
        Me.pnlWaitOpeningShadow.Size = New System.Drawing.Size(182, 46)
        Me.pnlWaitOpeningShadow.TabIndex = 8
        '
        'tmrHideOpeningDialog
        '
        Me.tmrHideOpeningDialog.Interval = 2000
        '
        'tmrHideLongVersionString
        '
        Me.tmrHideLongVersionString.Interval = 2000
        '
        'bgWorkerFileCopy
        '
        Me.bgWorkerFileCopy.WorkerReportsProgress = True
        Me.bgWorkerFileCopy.WorkerSupportsCancellation = True
        '
        'picMiddleBarRight
        '
        Me.picMiddleBarRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.picMiddleBarRight.Image = Global.FileExplorer.My.Resources.Resources.middlebar_right
        Me.picMiddleBarRight.Location = New System.Drawing.Point(198, 0)
        Me.picMiddleBarRight.Name = "picMiddleBarRight"
        Me.picMiddleBarRight.Size = New System.Drawing.Size(873, 47)
        Me.picMiddleBarRight.TabIndex = 10
        Me.picMiddleBarRight.TabStop = False
        '
        'pnlBorder
        '
        Me.pnlBorder.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlBorder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBorder.Controls.Add(Me.pnlHeader)
        Me.pnlBorder.Controls.Add(Me.pnlMain)
        Me.pnlBorder.Location = New System.Drawing.Point(12, 12)
        Me.pnlBorder.Name = "pnlBorder"
        Me.pnlBorder.Size = New System.Drawing.Size(1073, 590)
        Me.pnlBorder.TabIndex = 11
        '
        'pnlHeader
        '
        Me.pnlHeader.Controls.Add(Me.pnlTopBar)
        Me.pnlHeader.Controls.Add(Me.pnlMiddleBar)
        Me.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlHeader.Location = New System.Drawing.Point(0, 0)
        Me.pnlHeader.Name = "pnlHeader"
        Me.pnlHeader.Size = New System.Drawing.Size(1071, 87)
        Me.pnlHeader.TabIndex = 11
        '
        'pnlTopBar
        '
        Me.pnlTopBar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlTopBar.Controls.Add(Me.lblFakeTabFolderName)
        Me.pnlTopBar.Controls.Add(Me.pnlTopBar_Buttons)
        Me.pnlTopBar.Controls.Add(Me.picTopBarLeft)
        Me.pnlTopBar.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTopBar.Location = New System.Drawing.Point(0, 0)
        Me.pnlTopBar.Name = "pnlTopBar"
        Me.pnlTopBar.Size = New System.Drawing.Size(1071, 40)
        Me.pnlTopBar.TabIndex = 12
        '
        'lblFakeTabFolderName
        '
        Me.lblFakeTabFolderName.BackColor = System.Drawing.Color.Transparent
        Me.lblFakeTabFolderName.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFakeTabFolderName.Location = New System.Drawing.Point(41, 11)
        Me.lblFakeTabFolderName.Name = "lblFakeTabFolderName"
        Me.lblFakeTabFolderName.Size = New System.Drawing.Size(201, 26)
        Me.lblFakeTabFolderName.TabIndex = 12
        Me.lblFakeTabFolderName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlTopBar_Buttons
        '
        Me.pnlTopBar_Buttons.Controls.Add(Me.picClose)
        Me.pnlTopBar_Buttons.Controls.Add(Me.picMaximize)
        Me.pnlTopBar_Buttons.Controls.Add(Me.picMinimize)
        Me.pnlTopBar_Buttons.Dock = System.Windows.Forms.DockStyle.Right
        Me.pnlTopBar_Buttons.Location = New System.Drawing.Point(936, 0)
        Me.pnlTopBar_Buttons.Name = "pnlTopBar_Buttons"
        Me.pnlTopBar_Buttons.Size = New System.Drawing.Size(135, 40)
        Me.pnlTopBar_Buttons.TabIndex = 1
        '
        'picClose
        '
        Me.picClose.BackColor = System.Drawing.Color.Transparent
        Me.picClose.Image = Global.FileExplorer.My.Resources.Resources.topbar_buttons__close_on
        Me.picClose.Location = New System.Drawing.Point(90, 0)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(45, 40)
        Me.picClose.TabIndex = 2
        Me.picClose.TabStop = False
        '
        'picMaximize
        '
        Me.picMaximize.BackColor = System.Drawing.Color.Transparent
        Me.picMaximize.Image = Global.FileExplorer.My.Resources.Resources.topbar_buttons__maximize_off
        Me.picMaximize.Location = New System.Drawing.Point(45, 0)
        Me.picMaximize.Name = "picMaximize"
        Me.picMaximize.Size = New System.Drawing.Size(45, 40)
        Me.picMaximize.TabIndex = 1
        Me.picMaximize.TabStop = False
        '
        'picMinimize
        '
        Me.picMinimize.BackColor = System.Drawing.Color.Transparent
        Me.picMinimize.Image = Global.FileExplorer.My.Resources.Resources.topbar_buttons__minimize_on
        Me.picMinimize.Location = New System.Drawing.Point(0, 0)
        Me.picMinimize.Name = "picMinimize"
        Me.picMinimize.Size = New System.Drawing.Size(45, 40)
        Me.picMinimize.TabIndex = 0
        Me.picMinimize.TabStop = False
        '
        'picTopBarLeft
        '
        Me.picTopBarLeft.BackColor = System.Drawing.Color.Transparent
        Me.picTopBarLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.picTopBarLeft.Image = Global.FileExplorer.My.Resources.Resources.topbar_left
        Me.picTopBarLeft.Location = New System.Drawing.Point(0, 0)
        Me.picTopBarLeft.Name = "picTopBarLeft"
        Me.picTopBarLeft.Size = New System.Drawing.Size(938, 40)
        Me.picTopBarLeft.TabIndex = 0
        Me.picTopBarLeft.TabStop = False
        '
        'pnlMiddleBar
        '
        Me.pnlMiddleBar.Controls.Add(Me.lblFolderPath)
        Me.pnlMiddleBar.Controls.Add(Me.pnlMiddleBarLeft)
        Me.pnlMiddleBar.Controls.Add(Me.picMiddleBarRight)
        Me.pnlMiddleBar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlMiddleBar.Location = New System.Drawing.Point(0, 40)
        Me.pnlMiddleBar.Name = "pnlMiddleBar"
        Me.pnlMiddleBar.Size = New System.Drawing.Size(1071, 47)
        Me.pnlMiddleBar.TabIndex = 11
        '
        'lblFolderPath
        '
        Me.lblFolderPath.BackColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.lblFolderPath.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFolderPath.Location = New System.Drawing.Point(231, 12)
        Me.lblFolderPath.Name = "lblFolderPath"
        Me.lblFolderPath.Size = New System.Drawing.Size(837, 21)
        Me.lblFolderPath.TabIndex = 12
        Me.lblFolderPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMiddleBarLeft
        '
        Me.pnlMiddleBarLeft.Controls.Add(Me.picMiddleBarButton_Reload)
        Me.pnlMiddleBarLeft.Controls.Add(Me.picMiddleBarButton_Back)
        Me.pnlMiddleBarLeft.Controls.Add(Me.picMiddleBarButton_Up)
        Me.pnlMiddleBarLeft.Controls.Add(Me.picMiddleBarButton_Next)
        Me.pnlMiddleBarLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlMiddleBarLeft.Location = New System.Drawing.Point(0, 0)
        Me.pnlMiddleBarLeft.Name = "pnlMiddleBarLeft"
        Me.pnlMiddleBarLeft.Size = New System.Drawing.Size(200, 47)
        Me.pnlMiddleBarLeft.TabIndex = 12
        '
        'picMiddleBarButton_Reload
        '
        Me.picMiddleBarButton_Reload.Image = Global.FileExplorer.My.Resources.Resources.middlebar_buttons__reload_on
        Me.picMiddleBarButton_Reload.Location = New System.Drawing.Point(150, 0)
        Me.picMiddleBarButton_Reload.Name = "picMiddleBarButton_Reload"
        Me.picMiddleBarButton_Reload.Size = New System.Drawing.Size(50, 47)
        Me.picMiddleBarButton_Reload.TabIndex = 15
        Me.picMiddleBarButton_Reload.TabStop = False
        '
        'picMiddleBarButton_Back
        '
        Me.picMiddleBarButton_Back.Image = Global.FileExplorer.My.Resources.Resources.middlebar_buttons__back_off
        Me.picMiddleBarButton_Back.Location = New System.Drawing.Point(0, 0)
        Me.picMiddleBarButton_Back.Name = "picMiddleBarButton_Back"
        Me.picMiddleBarButton_Back.Size = New System.Drawing.Size(50, 47)
        Me.picMiddleBarButton_Back.TabIndex = 12
        Me.picMiddleBarButton_Back.TabStop = False
        '
        'picMiddleBarButton_Up
        '
        Me.picMiddleBarButton_Up.Image = Global.FileExplorer.My.Resources.Resources.middlebar_buttons__up_on
        Me.picMiddleBarButton_Up.Location = New System.Drawing.Point(100, 0)
        Me.picMiddleBarButton_Up.Name = "picMiddleBarButton_Up"
        Me.picMiddleBarButton_Up.Size = New System.Drawing.Size(50, 47)
        Me.picMiddleBarButton_Up.TabIndex = 14
        Me.picMiddleBarButton_Up.TabStop = False
        '
        'picMiddleBarButton_Next
        '
        Me.picMiddleBarButton_Next.Image = Global.FileExplorer.My.Resources.Resources.middlebar_buttons__next_off
        Me.picMiddleBarButton_Next.Location = New System.Drawing.Point(50, 0)
        Me.picMiddleBarButton_Next.Name = "picMiddleBarButton_Next"
        Me.picMiddleBarButton_Next.Size = New System.Drawing.Size(50, 47)
        Me.picMiddleBarButton_Next.TabIndex = 13
        Me.picMiddleBarButton_Next.TabStop = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.ClientSize = New System.Drawing.Size(1339, 676)
        Me.Controls.Add(Me.pnlBorder)
        Me.Controls.Add(Me.pnlWaitOpening)
        Me.Controls.Add(Me.pnlWaitOpeningShadow)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "OVCC File Explorer"
        Me.pnlStatusbar.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlWaitOpening.ResumeLayout(False)
        CType(Me.picMiddleBarRight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBorder.ResumeLayout(False)
        Me.pnlHeader.ResumeLayout(False)
        Me.pnlTopBar.ResumeLayout(False)
        Me.pnlTopBar_Buttons.ResumeLayout(False)
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMaximize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMinimize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTopBarLeft, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMiddleBar.ResumeLayout(False)
        Me.pnlMiddleBarLeft.ResumeLayout(False)
        CType(Me.picMiddleBarButton_Reload, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMiddleBarButton_Back, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMiddleBarButton_Up, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picMiddleBarButton_Next, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents treeFolders As BufferedTreeView
    Friend WithEvents listviewFiles As ListView
    Friend WithEvents colHeader_Name As ColumnHeader
    Friend WithEvents colHeader_Date As ColumnHeader
    Friend WithEvents colHeader_Type As ColumnHeader
    Friend WithEvents colHeader_Size As ColumnHeader
    Friend WithEvents pnlStatusbar As Panel
    Friend WithEvents pnlMain As Panel
    Friend WithEvents lblStatusbar As Label
    Friend WithEvents lblNoFilesFound As Label
    Friend WithEvents bgWorkerFiles As System.ComponentModel.BackgroundWorker
    Friend WithEvents colHeaderHidden_Size As ColumnHeader
    Friend WithEvents pnlWaitOpening As Panel
    Friend WithEvents lblOpeningApp As Label
    Friend WithEvents pnlWaitOpeningShadow As Panel
    Friend WithEvents tmrHideOpeningDialog As Timer
    Friend WithEvents lblAppVersion As Label
    Friend WithEvents tmrHideLongVersionString As Timer
    Friend WithEvents progressBar As ProgressBar
    Friend WithEvents bgWorkerFileCopy As System.ComponentModel.BackgroundWorker
    Friend WithEvents picMiddleBarRight As PictureBox
    Friend WithEvents pnlBorder As Panel
    Friend WithEvents pnlHeader As Panel
    Friend WithEvents pnlMiddleBar As Panel
    Friend WithEvents pnlMiddleBarLeft As Panel
    Friend WithEvents picMiddleBarButton_Back As PictureBox
    Friend WithEvents picMiddleBarButton_Reload As PictureBox
    Friend WithEvents picMiddleBarButton_Up As PictureBox
    Friend WithEvents picMiddleBarButton_Next As PictureBox
    Friend WithEvents pnlTopBar As Panel
    Friend WithEvents picTopBarLeft As PictureBox
    Friend WithEvents pnlTopBar_Buttons As Panel
    Friend WithEvents picMaximize As PictureBox
    Friend WithEvents picMinimize As PictureBox
    Friend WithEvents picClose As PictureBox
    Friend WithEvents lblFakeTabFolderName As Label
    Friend WithEvents lblFolderPath As Label
End Class
