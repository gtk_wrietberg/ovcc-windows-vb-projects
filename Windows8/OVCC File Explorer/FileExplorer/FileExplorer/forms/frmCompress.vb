﻿Imports System.ComponentModel
Imports System.IO.Compression

Public Class frmCompress
    '=======================================================================================================================
    '=======================================================================================================================
    '=======================================================================================================================
    Private Declare Function EnableMenuItem Lib "user32.dll" Alias "EnableMenuItem" (ByVal hMenu As IntPtr, ByVal uIDEnableItem As Int32, ByVal uEnable As Int32) As Int32

    Private _Moveable As Boolean = True
    Public Overridable Property Moveable() As Boolean
        Get
            Return _Moveable
        End Get
        Set(ByVal Value As Boolean)
            If _Moveable <> Value Then
                _Moveable = Value
            End If
        End Set
    End Property

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = &H117& Then
            'Handles popup of system menu.
            If m.LParam.ToInt32 \ 65536 <> 0 Then 'divide by 65536 to get hiword.
                Dim AbleFlags As Int32 = &H0&
                If Not _Moveable Then AbleFlags = &H2& Or &H1&
                EnableMenuItem(m.WParam, &HF010&, &H0& Or AbleFlags)
            End If
        End If

        If Not _Moveable Then
            'Cancels any attempt to drag the window by it's caption.
            If m.Msg = &HA1 Then If m.WParam.ToInt32 = &H2 Then Return
            'Redundant but cancels any clicks on the Move system menu item.
            If m.Msg = &H112 Then If (m.WParam.ToInt32 And &HFFF0) = &HF010& Then Return
        End If

        'Return control to base message handler.
        MyBase.WndProc(m)
    End Sub
    '=======================================================================================================================
    '=======================================================================================================================
    '=======================================================================================================================

    Public CurrentDir As String
    Private mToBeZipped As List(Of String)
    Private mZip As String = ""
    Private mCompressLevel As CompressionLevel

    Private Sub frmCompress_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mToBeZipped = New List(Of String)
        For Each mFile As String In frmMain.mFileSelectedNames
            mToBeZipped.Add(IO.Path.Combine(CurrentDir, mFile))
        Next

        Me.Size = New Size(213, 182)

        progressCompress.Style = ProgressBarStyle.Marquee
        progressCompress.MarqueeAnimationSpeed = 20
        progressCompress.Visible = False
        progressCompress.Left = 12
        progressCompress.Top = 56

        If mToBeZipped.Count <= 0 Then
            Me.DialogResult = DialogResult.Abort
        End If

        If mToBeZipped.Count = 1 Then
            Me.Text = "compressing 1 item"
        Else
            Me.Text = "compressing " & mToBeZipped.Count & " items"
        End If
    End Sub

    Private oZip As ZipArchive
    Private Function _buildZip(_path As String) As Boolean
        Dim pathEntryname As String = _path.Replace(CurrentDir, "")
        If pathEntryname.StartsWith("\") Then
            pathEntryname = pathEntryname.Substring(1)
        End If

        If IO.File.Exists(_path) Then
            Try
                oZip.CreateEntryFromFile(_path, pathEntryname, mCompressLevel)
            Catch ex As Exception

            End Try
        ElseIf IO.Directory.Exists(_path) Then
            Dim aFiles As String() = IO.Directory.GetFiles(_path)

            For i As Integer = 0 To aFiles.Length - 1
                _buildZip(aFiles(i))
            Next
        End If

        Return True
    End Function

    Private Sub bgWorkerCompress_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorkerCompress.DoWork
        Threading.Thread.Sleep(250)

        Try
            oZip = ZipFile.Open(mZip, ZipArchiveMode.Create)
            For Each zipItem As String In mToBeZipped
                _buildZip(zipItem)
            Next
        Catch ex As Exception
            Console.WriteLine("ERROR: " & ex.Message)
        End Try

        oZip.Dispose()

        Threading.Thread.Sleep(250)
    End Sub

    Private Sub bgWorkerCompress_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgWorkerCompress.RunWorkerCompleted
        Me.DialogResult = DialogResult.OK
    End Sub


    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub


    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        btnCancel.Enabled = False
        btnCancel.Visible = False
        btnStart.Enabled = False
        btnStart.Visible = False
        grpRadioButtons.Visible = False
        progressCompress.Visible = True


        Dim iCount As Integer = 0

        mZip = IO.Path.Combine(CurrentDir, IO.Path.GetFileNameWithoutExtension(mToBeZipped.Item(0)) & ".zip")


        While IO.File.Exists(mZip)
            iCount += 1

            mZip = IO.Path.Combine(CurrentDir, IO.Path.GetFileNameWithoutExtension(mToBeZipped.Item(0)) & " (" & iCount.ToString & ").zip")
        End While

        If radCompressStore.Checked Then
            mCompressLevel = CompressionLevel.NoCompression
        End If
        If radCompressNormal.Checked Then
            mCompressLevel = CompressionLevel.Fastest
        End If
        If radCompressBest.Checked Then
            mCompressLevel = CompressionLevel.Optimal
        End If


        Helpers.Logger.WriteMessage("files", 1)
        Helpers.Logger.WriteMessage(String.Join(",", mToBeZipped.ToArray), 2)
        Helpers.Logger.WriteMessage("zip", 1)
        Helpers.Logger.WriteMessage(mZip, 2)

        bgWorkerCompress.RunWorkerAsync()
    End Sub
End Class