﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProperties
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblName = New System.Windows.Forms.Label()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.picIcon = New System.Windows.Forms.PictureBox()
        Me.lblTypeTitle = New System.Windows.Forms.Label()
        Me.lblType = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblCreated = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblSize = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblModified = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblAccessed = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.txtLocation = New System.Windows.Forms.TextBox()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(88, 12)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(239, 32)
        Me.lblName.TabIndex = 0
        Me.lblName.Text = "name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.Location = New System.Drawing.Point(12, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(329, 2)
        Me.Label2.TabIndex = 1
        '
        'picIcon
        '
        Me.picIcon.Location = New System.Drawing.Point(12, 12)
        Me.picIcon.Name = "picIcon"
        Me.picIcon.Size = New System.Drawing.Size(32, 32)
        Me.picIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picIcon.TabIndex = 2
        Me.picIcon.TabStop = False
        '
        'lblTypeTitle
        '
        Me.lblTypeTitle.Location = New System.Drawing.Point(12, 60)
        Me.lblTypeTitle.Name = "lblTypeTitle"
        Me.lblTypeTitle.Size = New System.Drawing.Size(70, 24)
        Me.lblTypeTitle.TabIndex = 3
        Me.lblTypeTitle.Text = "Type of file:"
        Me.lblTypeTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblType
        '
        Me.lblType.Location = New System.Drawing.Point(88, 60)
        Me.lblType.Name = "lblType"
        Me.lblType.Size = New System.Drawing.Size(253, 24)
        Me.lblType.TabIndex = 4
        Me.lblType.Text = "type"
        Me.lblType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label3.Location = New System.Drawing.Point(13, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(329, 2)
        Me.Label3.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(12, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 24)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Location:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblCreated
        '
        Me.lblCreated.Location = New System.Drawing.Point(88, 164)
        Me.lblCreated.Name = "lblCreated"
        Me.lblCreated.Size = New System.Drawing.Size(253, 24)
        Me.lblCreated.TabIndex = 10
        Me.lblCreated.Text = "created"
        Me.lblCreated.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(12, 164)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(70, 24)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Created:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label7.Location = New System.Drawing.Point(13, 156)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(329, 2)
        Me.Label7.TabIndex = 8
        '
        'lblSize
        '
        Me.lblSize.Location = New System.Drawing.Point(88, 124)
        Me.lblSize.Name = "lblSize"
        Me.lblSize.Size = New System.Drawing.Size(253, 24)
        Me.lblSize.TabIndex = 12
        Me.lblSize.Text = "size"
        Me.lblSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(12, 124)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(70, 24)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Size:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblModified
        '
        Me.lblModified.Location = New System.Drawing.Point(88, 188)
        Me.lblModified.Name = "lblModified"
        Me.lblModified.Size = New System.Drawing.Size(253, 24)
        Me.lblModified.TabIndex = 14
        Me.lblModified.Text = "modified"
        Me.lblModified.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(12, 188)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 24)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Modified:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAccessed
        '
        Me.lblAccessed.Location = New System.Drawing.Point(88, 212)
        Me.lblAccessed.Name = "lblAccessed"
        Me.lblAccessed.Size = New System.Drawing.Size(253, 24)
        Me.lblAccessed.TabIndex = 16
        Me.lblAccessed.Text = "accessed"
        Me.lblAccessed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(12, 212)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(70, 24)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Accessed:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(267, 241)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 17
        Me.btnOk.Text = "Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'txtLocation
        '
        Me.txtLocation.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtLocation.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtLocation.Location = New System.Drawing.Point(88, 106)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.ReadOnly = True
        Me.txtLocation.Size = New System.Drawing.Size(250, 15)
        Me.txtLocation.TabIndex = 18
        Me.txtLocation.Text = "location"
        '
        'frmProperties
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(354, 276)
        Me.Controls.Add(Me.txtLocation)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.lblAccessed)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lblModified)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblSize)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblCreated)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblType)
        Me.Controls.Add(Me.lblTypeTitle)
        Me.Controls.Add(Me.picIcon)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblName)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProperties"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Properties"
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblName As Label
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents Label2 As Label
    Friend WithEvents picIcon As PictureBox
    Friend WithEvents lblTypeTitle As Label
    Friend WithEvents lblType As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblCreated As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents lblSize As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents lblModified As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents lblAccessed As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents btnOk As Button
    Friend WithEvents txtLocation As TextBox
End Class
