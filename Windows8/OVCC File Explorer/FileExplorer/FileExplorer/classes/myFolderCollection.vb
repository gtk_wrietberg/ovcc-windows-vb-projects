﻿Public Class myFolderCollection
    Private mList_Keys As New List(Of String)
    Private mList_Values As New List(Of String)

    Private mKeyCount As Integer = 0

    Public Sub New()
        mList_Keys = New List(Of String)
        mList_Values = New List(Of String)
    End Sub

    Public Function Add(Key As String, Value As String) As Boolean
        If Not mList_Keys.Contains(Key) Then
            mKeyCount += 1

            mList_Keys.Add(Key)
            mList_Values.Add(Value)

            Return True
        Else
            Return False
        End If
    End Function

    Public Function Add(Value As String) As Boolean
        Dim tKey As String = "_internal_key_" & _LeadingZero(mKeyCount.ToString, 6)

        Return Add(tKey, Value)
    End Function

    Public ReadOnly Property KeyCount() As Integer
        Get
            Return mKeyCount
        End Get
    End Property

    Public Function GetValueByKey(Key As String) As String
        Dim tIndex As Integer = -1

        If mList_Keys.Contains(Key) Then
            tIndex = mList_Keys.IndexOf(Key)

            Return mList_Values.Item(tIndex)
        Else
            Return ""
        End If
    End Function

    Public Function GetValueByIndex(Index As Integer) As String
        If Index < 0 Or Index >= mList_Values.Count Then
            Return ""
        Else
            Return mList_Values.Item(Index)
        End If
    End Function

    Private Function _LeadingZero(sNum As String, Optional len As Integer = 3) As String
        If sNum.Length < len Then
            sNum = (New String("0", (len - sNum.Length))) & sNum
        End If

        Return sNum
    End Function
End Class
