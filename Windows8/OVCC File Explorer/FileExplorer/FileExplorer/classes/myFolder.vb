﻿Public Class myFolder
    Public Class myFolderItem
        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mText As String
        Public Property Text() As String
            Get
                Return mText
            End Get
            Set(ByVal value As String)
                mText = value
            End Set
        End Property

        Private mPath As String
        Public Property Path() As String
            Get
                Return mPath
            End Get
            Set(ByVal value As String)
                mPath = value
            End Set
        End Property

        Private mIsRootFolder As Boolean
        Public Property IsRootFolder() As Boolean
            Get
                Return mIsRootFolder
            End Get
            Set(ByVal value As Boolean)
                mIsRootFolder = value
            End Set
        End Property

        Private mIconIndex As Integer
        Public Property IconIndex() As Integer
            Get
                Return mIconIndex
            End Get
            Set(ByVal value As Integer)
                mIconIndex = value
            End Set
        End Property

        Private mIconSelectedIndex As Integer
        Public Property IconSelectedIndex() As Integer
            Get
                Return mIconSelectedIndex
            End Get
            Set(ByVal value As Integer)
                mIconSelectedIndex = value
            End Set
        End Property

        Public Sub New()

        End Sub

        Public Sub New(Name As String, Text As String, Path As String, IsRootFolder As Boolean, IconIndex As Integer, IconSelected As Integer)
            mName = Name
            mText = Text
            mPath = Path
            mIsRootFolder = IsRootFolder
            mIconIndex = IconIndex
            mIconSelectedIndex = IconSelected
        End Sub
    End Class

    Private mList As New List(Of myFolderItem)
    Private mCount As Integer

    Public Sub New()
        mList = New List(Of myFolderItem)
        mCount = 0
    End Sub

    Public Sub Add(Text As String, Path As String, IconIndex As Integer, IconSelected As Integer)
        _Add(Text, Text, Path, False, IconIndex, IconSelected)
    End Sub

    Public Sub Add(Text As String, Path As String, IsRootFolder As Boolean, IconIndex As Integer, IconSelected As Integer)
        _Add(Text, Text, Path, IsRootFolder, IconIndex, IconSelected)
    End Sub

    Public Sub Add(Name As String, Text As String, Path As String, IconIndex As Integer, IconSelected As Integer)
        _Add(Name, Text, Path, False, IconIndex, IconSelected)
    End Sub

    Public Sub Add(Name As String, Text As String, Path As String, IsRootFolder As Boolean, IconIndex As Integer, IconSelected As Integer)
        _Add(Name, Text, Path, IsRootFolder, IconIndex, IconSelected)
    End Sub

    Private Sub _Add(Name As String, Text As String, Path As String, IsRootFolder As Boolean, IconIndex As Integer, IconSelected As Integer)
        mList.Add(New myFolderItem(Name, Text, Path, IsRootFolder, IconIndex, IconSelected))
    End Sub

    Public Function GetNext(ByRef FolderItem As myFolderItem) As Boolean
        If mCount >= mList.Count Then
            mCount = 0
            Return False
        End If

        FolderItem = New myFolderItem
        FolderItem = mList.Item(mCount)

        Return True
    End Function
End Class
