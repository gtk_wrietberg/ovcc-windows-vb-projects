﻿Imports System.IO

Public Class Drives
    Public Class Drive
        Public Sub New()

        End Sub

        Public Sub New(_name As String, _type As DriveType, _ready As Boolean)
            mName = _name
            mType = _type
            mReady = _ready
        End Sub

        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mReady As Boolean
        Public Property IsReady() As Boolean
            Get
                Return mReady
            End Get
            Set(ByVal value As Boolean)
                mReady = value
            End Set
        End Property

        Private mType As String
        Public Property Type() As String
            Get
                Return mType
            End Get
            Set(ByVal value As String)
                mType = value
            End Set
        End Property
    End Class

    Public Shared Drives_Network As New List(Of Drive)
    Public Shared Drives_CDRom As New List(Of Drive)
    Public Shared Drives_Removable As New List(Of Drive)

    Public Shared Function CollectDrives() As Boolean
        Drives_Network = New List(Of Drive)
        Drives_CDRom = New List(Of Drive)
        Drives_Removable = New List(Of Drive)

        Try
            Dim allDrives() As DriveInfo = DriveInfo.GetDrives()

            For Each d As DriveInfo In allDrives
                Dim _d As New Drive(d.Name, d.DriveType, d.IsReady)

                If d.DriveType = DriveType.CDRom Then
                    Drives_CDRom.Add(_d)
                End If
                If d.DriveType = DriveType.Network Then
                    Drives_Network.Add(_d)
                End If
                If d.DriveType = DriveType.Removable Then
                    Drives_Removable.Add(_d)
                End If
            Next

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Public Shared Sub ExamineDrives()
    '    Dim allDrives() As DriveInfo = DriveInfo.GetDrives()
    '    Dim systemDrive As String = Environment.GetFolderPath(Environment.SpecialFolder.System).Substring(0, 3)

    '    For Each d As DriveInfo In allDrives
    '        Console.WriteLine("Drive {0}", d.Name)
    '        Console.WriteLine("Is system drive {0}", (d.Name = systemDrive))
    '        Console.WriteLine("  Drive type: {0}", d.DriveType)
    '        If d.IsReady = True Then
    '            Console.WriteLine("  Volume label: {0}", d.VolumeLabel)
    '            Console.WriteLine("  File system: {0}", d.DriveFormat)
    '            Console.WriteLine(
    '                "  Available space to current user:{0, 15} bytes",
    '                d.AvailableFreeSpace)

    '            Console.WriteLine(
    '                "  Total available space:          {0, 15} bytes",
    '                d.TotalFreeSpace)

    '            Console.WriteLine(
    '                "  Total size of drive:            {0, 15} bytes ",
    '                d.TotalSize)
    '        End If
    '    Next
    'End Sub


    'Public Declare Function WNetGetConnection Lib "mpr.dll" Alias "WNetGetConnectionA" (ByVal lpszLocalName As String, ByVal lpszRemoteName As String, ByRef cbRemoteName As Integer) As Integer

    'Public Function GetNetDriveName(ByVal DriveLetter As String) As String
    '    '
    '    ' Return mapped drive UNC name
    '    Dim RetVal As Integer
    '    Dim OutName As String = New String(CChar(" "), 260)
    '    Dim NameLength As Integer = 260

    '    Try
    '        RetVal = WNetGetConnection(DriveLetter, OutName, NameLength)
    '        OutName = OutName.Replace(Chr(0), " ").TrimEnd(CChar(" "))
    '        Return OutName
    '    Catch ex As Exception
    '        Return ""
    '    End Try

    'End Function
End Class
