﻿Imports System.Windows.Forms.ImageList

Public Class Settings
    Public Enum IconIndices As Integer
        Drive = 0
        Generic = 1
        Documents = 2
        Downloads = 3
        Pictures = 4
        Desktop = 5
    End Enum

    Public Class Generic
        Public Shared Debugging As Boolean = False
        Public Shared ContextMenu As Boolean = False
        Public Shared DirectoriesCollapsable As Boolean = True
        Public Shared ShowNetworkDrives As Boolean = False
        Public Shared StartWithDownloadsFolder As Boolean = False
        Public Shared AppendUnzippedFolderWithFullDate As Boolean = False
        Public Shared ShowHiddenFiles As Boolean = False
        Public Shared ShowSystemFiles As Boolean = False
        Public Shared ShowTemporaryFiles As Boolean = False
        Public Shared DetectFolderChanges As Boolean = False
        Public Shared AllowFileDelete As Boolean = False
        Public Shared AllowPrinting As Boolean = False
    End Class

    Public Class CommandLineParams
        Public Shared Debugging As String = "--debug"
        Public Shared StartWithDownloadsFolder As String = "--start-with-downloads-folder"
        Public Shared ShowHiddenFiles As String = "--show-hidden-files"
        Public Shared ShowSystemFiles As String = "--show-system-files"
        Public Shared ShowTemporaryFiles As String = "--show-temporary-files"
        Public Shared DetectFolderChanges As String = "--detect-folder-changes"
    End Class

    Public Class Strings
        Public Class PathNames
            Public Shared Downloads As String = "Downloads"
        End Class

        Public Shared AppTitle As String = "OVCC File Explorer"
    End Class


    Public Class Paths
        Public Shared Names As String() = {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        }
        Public Shared Paths As String() = {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        }
        Public Shared Icons As Integer() = {
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic,
            IconIndices.Generic
        }


        '----------------------
        Private Shared mProgramFilesx86 As String = "%%PROGRAM_FILES_x86%%"
        Private Shared mProgramFiles As String = "%%PROGRAM_FILES%%"

        Public Shared Function TranslatePath(inStr As String) As String
            Return inStr.Replace(mProgramFilesx86, Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)).Replace(mProgramFiles, Environment.GetEnvironmentVariable("ProgramW6432"))
        End Function
    End Class


    Public Shared RegistryKey__ROOT As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\FileExplorer"
    Public Shared RegistryKey__EXTENSIONS As String = RegistryKey__ROOT & "\Extensions"

    Public Shared Sub Load()
        Generic.Debugging = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "Debugging", False)
        Generic.ContextMenu = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "ContextMenu", False)
        Generic.DirectoriesCollapsable = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "DirectoriesCollapsable", True)
        Generic.ShowNetworkDrives = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "ShowNetworkDrives", False)
        Generic.StartWithDownloadsFolder = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "StartWithDownloadsFolder", False)
        Generic.AppendUnzippedFolderWithFullDate = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "AppendUnzippedFolderWithFullDate", False)
        Generic.ShowHiddenFiles = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "ShowHiddenFiles", False)
        Generic.ShowSystemFiles = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "ShowSystemFiles", False)
        Generic.ShowTemporaryFiles = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "ShowTemporaryFiles", False)
        Generic.DetectFolderChanges = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "DetectFolderChanges", False)
        Generic.AllowFileDelete = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "AllowFileDelete", False)
        Generic.AllowPrinting = Helpers.Registry.GetValue_Boolean(RegistryKey__ROOT, "AllowPrinting", False)


        For i As Integer = 1 To 10
            Paths.Names(i - 1) = Helpers.Registry.GetValue_String(RegistryKey__ROOT, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Name", "")
            Paths.Paths(i - 1) = Helpers.Registry.GetValue_String(RegistryKey__ROOT, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Path", "")
            Paths.Icons(i - 1) = Helpers.Registry.GetValue_Integer(RegistryKey__ROOT, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Icon", 1)
        Next

        Extensions.Load()

        Save()
    End Sub

    Public Shared Sub Save()
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "Debugging", Generic.Debugging)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "ContextMenu", Generic.ContextMenu)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "DirectoriesCollapsable", Generic.DirectoriesCollapsable)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "ShowNetworkDrives", Generic.ShowNetworkDrives)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "StartWithDownloadsFolder", Generic.StartWithDownloadsFolder)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "AppendUnzippedFolderWithFullDate", Generic.AppendUnzippedFolderWithFullDate)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "ShowHiddenFiles", Generic.ShowHiddenFiles)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "ShowSystemFiles", Generic.ShowSystemFiles)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "ShowTemporaryFiles", Generic.ShowTemporaryFiles)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "DetectFolderChanges", Generic.DetectFolderChanges)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "AllowFileDelete", Generic.AllowFileDelete)
        Helpers.Registry.SetValue_Boolean(RegistryKey__ROOT, "AllowPrinting", Generic.AllowPrinting)

        For i As Integer = 1 To 10
            Helpers.Registry.SetValue_String(RegistryKey__ROOT, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Name", Paths.Names(i - 1))
            Helpers.Registry.SetValue_String(RegistryKey__ROOT, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Path", Paths.Paths(i - 1))
            Helpers.Registry.SetValue_Integer(RegistryKey__ROOT, "Folder_" & Helpers.Types.LeadingZero(i, 2) & "_Icon", Paths.Icons(i - 1))
        Next
    End Sub


    Public Shared Sub InstallerCleanup()
        Helpers.Registry.RemoveValue(RegistryKey__ROOT, "OpenImagesWithPaint")
        Helpers.Registry.RemoveValue(RegistryKey__ROOT, "ImageExtensions")
    End Sub


    '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    Public Class Extensions
        Private Shared mExtensions As Dictionary(Of String, String)

        Public Shared Function Load() As Boolean
            If Helpers.Registry.Iterate.Init(RegistryKey__EXTENSIONS, True) Then
                mExtensions = New Dictionary(Of String, String)

                Dim sValueName As String = ""

                While Helpers.Registry.Iterate.GetNext(sValueName)
                    mExtensions.Add(sValueName, Helpers.Registry.GetValue_String(RegistryKey__EXTENSIONS, sValueName, ""))
                End While
            End If

            Return True
        End Function

        Public Shared Function Add(sExtension As String, sAppPath As String) As Boolean
            mExtensions.Add(sExtension, sAppPath)

            Return True
        End Function

        Public Shared Function Save() As Boolean
            For Each kvp As KeyValuePair(Of String, String) In mExtensions
                Helpers.Registry.SetValue_String(RegistryKey__EXTENSIONS, kvp.Key, kvp.Value)
            Next

            Return True
        End Function

        Public Shared Function GetAppForExtension(sExtension As String, ByRef sAppPath As String) As Boolean
            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage("looking for extension", 1)
                Helpers.Logger.WriteMessage("extension", 2)
                Helpers.Logger.WriteMessage(sExtension, 3)
            End If

            If sExtension.StartsWith(".") Then
                sExtension = sExtension.Substring(1)
            End If

            If Settings.Generic.Debugging Then
                Helpers.Logger.WriteMessage(sExtension, 3)
            End If

            If mExtensions.Keys.Contains(sExtension) Then
                Helpers.Logger.WriteMessage("app path", 2)
                Helpers.Logger.WriteMessage(sAppPath, 3)

                sAppPath = Paths.TranslatePath(mExtensions.Item(sExtension))

                Helpers.Logger.WriteMessage(sAppPath, 3)

                Return True
            Else
                If Settings.Generic.Debugging Then
                    Helpers.Logger.WriteMessage("not found", 2)
                End If
            End If

            Return False
        End Function
    End Class
End Class
