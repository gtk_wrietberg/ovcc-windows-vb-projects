﻿Public Class myFileInfo
    Private mPath As String
    Public Property Path() As String
        Get
            Return mPath
        End Get
        Set(ByVal value As String)
            mPath = value
        End Set
    End Property

    Private mName As String
    Public Property Name() As String
        Get
            Return mName
        End Get
        Set(ByVal value As String)
            mName = value
        End Set
    End Property

    Private mLastModified As String
    Public Property LastModified() As String
        Get
            Return mLastModified
        End Get
        Set(ByVal value As String)
            mLastModified = value
        End Set
    End Property

    Private mFileType As String
    Public Property FileType() As String
        Get
            Return mFileType
        End Get
        Set(ByVal value As String)
            mFileType = value
        End Set
    End Property

    Private mFileSize As Long
    Public Property FileSize() As Long
        Get
            Return mFileSize
        End Get
        Set(ByVal value As Long)
            mFileSize = value
        End Set
    End Property

    Public Sub New(Path As String, Name As String, FileType As String, LastModified As String, FileSize As Long)
        mPath = Path
        mName = Name
        mFileType = FileType
        mLastModified = LastModified
        mFileSize = FileSize
    End Sub
End Class
