﻿Imports System.ComponentModel
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Web
Imports System.Windows.Forms.VisualStyles.VisualStyleElement

Public Class frmMain
    Private bStarted As Boolean = False

    Public Enum InstallationOrder As Integer
        ERROR_ERROR_ERROR = -1
        APP_START = 0
        USER_QUESTION
        CheckForUpdate
        DownloadUpdate
        RunUpdate
        CheckExistingSitekioskProfile
        ExistingSitekioskProfileHandler
        CheckInstaller
        DownloadInstaller
        VerifyInstaller
        UseExistingInstaller
        RunInstaller
        DONE
    End Enum
    Private INSTALLATION_STEP As InstallationOrder = InstallationOrder.APP_START

#Region "Thread Safe stuff"
    Delegate Sub DELEGATE__ProgressBarValue(ByVal [value] As Integer)
    Public Sub THREADSAFE__ProgressBarValue(ByVal [value] As Integer)
        If Me.progressDownload.InvokeRequired Then
            Dim d As New DELEGATE__ProgressBarValue(AddressOf THREADSAFE__ProgressBarValue)
            Me.Invoke(d, New Object() {[value]})
        Else
            Me.progressDownload.Value = [value]
        End If
    End Sub

    Delegate Sub DELEGATE__ToggleProgressBar(ByVal [value] As Boolean)
    Public Sub THREADSAFE__ToggleProgressBar(ByVal [value] As Boolean)
        If Me.progressDownload.InvokeRequired Then
            Dim d As New DELEGATE__ToggleProgressBar(AddressOf THREADSAFE__ToggleProgressBar)
            Me.Invoke(d, New Object() {[value]})
        Else
            If [value] Then
                Me.lblInfo.Width = 590
                Me.lblInfo.Height = 60
            Else
                Me.lblInfo.Width = 590
                Me.lblInfo.Height = 102
            End If


            Me.progressDownload.Enabled = [value]
            Me.progressDownload.Visible = [value]

            Me.lblProgress_Filesize.Enabled = [value]
            Me.lblProgress_Filesize.Visible = [value]

            Me.lblProgress_Speed.Enabled = [value]
            Me.lblProgress_Speed.Visible = [value]

            Me.lblProgress_Percentage.Enabled = [value]
            Me.lblProgress_Percentage.Visible = [value]
        End If
    End Sub

    Delegate Sub DELEGATE__ToggleButtons(ByVal [value] As Integer, ByVal leftButtonText As String, ByVal rightButtonText As String)
    Public Sub THREADSAFE__ToggleButtons(ByVal [value] As Integer, ByVal leftButtonText As String, ByVal rightButtonText As String)
        If Me.btnOK.InvokeRequired Then
            Dim d As New DELEGATE__ToggleButtons(AddressOf THREADSAFE__ToggleButtons)
            Me.Invoke(d, New Object() {[value]})
        Else
            If [value] Then
                Me.lblInfo.Width = 590
                Me.lblInfo.Height = 60
            Else
                Me.lblInfo.Width = 590
                Me.lblInfo.Height = 102
            End If


            Me.btnOK.Left = 5
            Me.btnOK.Top = 65

            Me.btnCancel.Left = 351
            Me.btnCancel.Top = 65


            Me.btnOK.Text = leftButtonText
            Me.btnCancel.Text = rightButtonText


            Me.btnOK.Enabled = [value] And 1
            Me.btnOK.Visible = [value] And 1

            Me.btnCancel.Enabled = [value] And 2
            Me.btnCancel.Visible = [value] And 2
        End If
    End Sub

    Delegate Sub DELEGATE__ProgressFilesize(ByVal [text] As String)
    Public Sub THREADSAFE__ProgressFilesize(ByVal [text] As String)
        If Me.lblProgress_Filesize.InvokeRequired Then
            Dim d As New DELEGATE__ProgressFilesize(AddressOf THREADSAFE__ProgressFilesize)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblProgress_Filesize.Text = [text]
        End If
    End Sub

    Delegate Sub DELEGATE__ProgressSpeed(ByVal [text] As String)
    Public Sub THREADSAFE__ProgressSpeed(ByVal [text] As String)
        If Me.lblProgress_Speed.InvokeRequired Then
            Dim d As New DELEGATE__ProgressSpeed(AddressOf THREADSAFE__ProgressSpeed)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblProgress_Speed.Text = [text]
        End If
    End Sub

    Delegate Sub DELEGATE__ProgressPercentage(ByVal [text] As String)
    Public Sub THREADSAFE__ProgressPercentage(ByVal [text] As String)
        If Me.lblProgress_Percentage.InvokeRequired Then
            Dim d As New DELEGATE__ProgressPercentage(AddressOf THREADSAFE__ProgressPercentage)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblProgress_Percentage.Text = [text]
        End If
    End Sub

    Delegate Sub DELEGATE__Info(ByVal [text] As String)
    Public Sub THREADSAFE__InfoMessage(ByVal [text] As String)
        If Me.lblInfo.InvokeRequired Then
            Dim d As New DELEGATE__Info(AddressOf THREADSAFE__InfoMessage)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblInfo.ForeColor = Color.Black
            Me.lblInfo.Text = [text]
        End If
    End Sub
    Public Sub THREADSAFE__InfoError(ByVal [text] As String)
        If Me.lblInfo.InvokeRequired Then
            Dim d As New DELEGATE__Info(AddressOf THREADSAFE__InfoError)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblInfo.ForeColor = Color.Red
            Me.lblInfo.Text = [text]
        End If
    End Sub
#End Region

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sLogPath As String
        sLogPath = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\_logs"
        sLogPath &= "\" & Application.ProductName & "\" & Application.ProductVersion

        Try
            IO.Directory.CreateDirectory(sLogPath)
        Catch ex As Exception

        End Try


        Try
            IO.Directory.CreateDirectory(Constants.Files.DOWNLOAD_PATH)
        Catch ex As Exception

        End Try

        Helpers.Logger.Debugging = False
        Helpers.Logger.InitialiseLogger(sLogPath, False)
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)


        Dim sbTitle As New StringBuilder
        sbTitle.Append(Application.ProductName)
        sbTitle.Append(" - v")
        sbTitle.Append(My.Application.Info.Version.Major.ToString)
        sbTitle.Append(".")
        sbTitle.Append(My.Application.Info.Version.Minor.ToString)
        sbTitle.Append(" (build ")
        sbTitle.Append(My.Application.Info.Version.Build.ToString)
        sbTitle.Append("-")
        sbTitle.Append(My.Application.Info.Version.Revision.ToString)
        sbTitle.Append(")")

        Me.Text = sbTitle.ToString


        '---------------
        THREADSAFE__ToggleProgressBar(False)
        THREADSAFE__ProgressBarValue(0)
        THREADSAFE__ProgressFilesize("")
        THREADSAFE__ProgressSpeed("")
        THREADSAFE__ProgressPercentage("")
        THREADSAFE__InfoMessage("")
        THREADSAFE__ToggleButtons(0, "", "")


        '---------------
        Constants.Files.INSTALLER = ""


        '---------------
        ExitCode.SetValueExclusive(ExitCode.ExitCodes.OK)
    End Sub

    Private Sub SkipToInstallationStep(SkipToStep As InstallationOrder)
        INSTALLATION_STEP = SkipToStep - 1
        If INSTALLATION_STEP < InstallationOrder.ERROR_ERROR_ERROR Then INSTALLATION_STEP = InstallationOrder.ERROR_ERROR_ERROR
    End Sub

    Private Sub InstallationStep()
        INSTALLATION_STEP += 1

        Helpers.Logger.WriteMessage("Current installation step: " & INSTALLATION_STEP.ToString, 0)

        If INSTALLATION_STEP >= ([Enum].GetValues(GetType(InstallationOrder)).Cast(Of InstallationOrder)().Max() + 1) Then
            ApplicationExit()
            Exit Sub
        End If


        Select Case INSTALLATION_STEP
            Case InstallationOrder.USER_QUESTION
                THREADSAFE__InfoMessage("This app will download and run the latest OVCCSoftware installer for Windows")

                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(3, "Proceed", "Cancel")
            Case InstallationOrder.CheckExistingSitekioskProfile
                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(0, "", "")

                bgworker.RunWorkerAsync("check_existing_sitekiosk")
            Case InstallationOrder.ExistingSitekioskProfileHandler
                If Constants.Misc.SITEKIOSK_PROFILE_EXISTS > 0 Then
                    If Constants.Misc.SITEKIOSK_PROFILE_EXISTS = 1 Then
                        THREADSAFE__InfoMessage("There is already a SiteKiosk user profile on this machine." & vbCrLf & "Are you sure you want to proceed?")
                        THREADSAFE__ToggleButtons(3, "Proceed", "Cancel")
                    Else
                        THREADSAFE__InfoError("There is already a SiteKiosk user profile on this machine, and it appears to be corrupt." & vbCrLf & "Please uninstall OVCC, and make sure all SiteKiosk profiles are removed, and retry.")
                        THREADSAFE__ToggleButtons(2, "", "Cancel")
                    End If
                Else
                    bgworker.RunWorkerAsync("do nothing")
                End If
            Case InstallationOrder.CheckForUpdate
                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(0, "", "")

                bgworker.RunWorkerAsync("get-latest-update")
            Case InstallationOrder.DownloadUpdate
                THREADSAFE__ToggleProgressBar(True)
                THREADSAFE__ToggleButtons(0, "", "")
                THREADSAFE__ProgressBarValue(0)
                THREADSAFE__ProgressFilesize("")
                THREADSAFE__ProgressSpeed("")
                THREADSAFE__ProgressPercentage("")

                bgworker.RunWorkerAsync("download-update")
            Case InstallationOrder.RunUpdate

                bgworker.RunWorkerAsync("run-update")
            Case InstallationOrder.CheckInstaller
                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(0, "", "")

                bgworker.RunWorkerAsync("get-latest-download")
            Case InstallationOrder.DownloadInstaller
                THREADSAFE__ProgressBarValue(0)
                THREADSAFE__ProgressFilesize("")
                THREADSAFE__ProgressSpeed("")
                THREADSAFE__ProgressPercentage("")
                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(0, "", "")

                bgworker.RunWorkerAsync("download-download")
            Case InstallationOrder.VerifyInstaller
                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(0, "", "")

                bgworker.RunWorkerAsync("verify-download")
            Case InstallationOrder.UseExistingInstaller
                If Constants.Misc.INSTALLER_EXISTS Then
                    THREADSAFE__ToggleProgressBar(False)
                    THREADSAFE__ToggleButtons(3, "Use existing", "Re-download")

                    THREADSAFE__InfoMessage("There's already a downloaded OVCC installer. Do you want to run the existing installer, or re-download?")
                Else
                    InstallationStep()
                End If
            Case InstallationOrder.RunInstaller
                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(0, "", "")

                bgworker.RunWorkerAsync("run-installer")
            Case InstallationOrder.DONE
                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(0, "", "")

                bgworker.RunWorkerAsync("done")
            Case InstallationOrder.ERROR_ERROR_ERROR
                THREADSAFE__ToggleProgressBar(False)
                THREADSAFE__ToggleButtons(2, "", "Cancel")

                bgworker.RunWorkerAsync("display-error")
        End Select
    End Sub


    '--------------------------------------------------------------
    Private Sub ApplicationExit()
        Helpers.Logger.WriteMessage("Exiting", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.GetValue.ToString & " (" & ExitCode.ToString & ")", 2)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())

        Me.Close()
    End Sub


    Private Sub frmMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If Not bStarted Then
            bStarted = True

            THREADSAFE__ToggleProgressBar(False)
            THREADSAFE__ProgressBarValue(0)
            THREADSAFE__ProgressFilesize("")
            THREADSAFE__ProgressSpeed("")
            THREADSAFE__ProgressPercentage("")
            THREADSAFE__ToggleButtons(0, "", "")

            InstallationStep()
        End If
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Helpers.Logger.WriteMessage("Button clicked: " & btnOK.Text, 0)

        If INSTALLATION_STEP = InstallationOrder.USER_QUESTION Then
            InstallationStep()
        End If

        If INSTALLATION_STEP = InstallationOrder.UseExistingInstaller And Constants.Misc.INSTALLER_EXISTS Then
            InstallationStep()
        End If

        If INSTALLATION_STEP = InstallationOrder.ExistingSitekioskProfileHandler Then
            InstallationStep()
        End If

        If INSTALLATION_STEP = InstallationOrder.DONE Then
            ApplicationExit()
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Helpers.Logger.WriteMessage("Button clicked: " & btnCancel.Text, 0)

        If INSTALLATION_STEP = InstallationOrder.UseExistingInstaller And Constants.Misc.INSTALLER_EXISTS Then
            Constants.Misc.RE_DOWNLOAD = True

            THREADSAFE__InfoMessage("")

            SkipToInstallationStep(InstallationOrder.DownloadInstaller)
            InstallationStep()

            Exit Sub
        End If

        ExitCode.SetValue(ExitCode.ExitCodes.CANCELLED_BY_USER)
        ApplicationExit()
    End Sub

    Private Sub bgworker_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgworker.DoWork
        Dim sParam As String = CStr(e.Argument)
        Dim sError As String = ""

        Helpers.Logger.WriteMessage("BackgroundWorker action: " & sParam, 0)

        Select Case sParam
            Case "check_existing_sitekiosk"
                Dim sProfile As String = ""

                If Helpers.WindowsUser.GetProfileDirectory(Constants.Misc.SITEKIOSK_USERNAME, sProfile) Then
                    'Profile exists, let's check if it's valid
                    Helpers.Logger.WriteMessageRelative("SiteKiosk profile exists", 1)
                    Helpers.Logger.WriteMessageRelative(sProfile, 2)
                    If sProfile.ToLower().EndsWith(Constants.Misc.SITEKIOSK_USERNAME.ToLower()) Then
                        Constants.Misc.SITEKIOSK_PROFILE_EXISTS = 1
                        Helpers.Logger.WriteMessageRelative("ok", 3)
                    Else
                        Constants.Misc.SITEKIOSK_PROFILE_EXISTS = 2
                        Helpers.Logger.WriteMessageRelative("corrupt", 3)
                    End If
                Else

                End If
            Case "get-latest-update"
                THREADSAFE__InfoMessage("Checking for update")

                If Not GetLatestUpdate(sError) Then
                    Constants.Misc.ERROR_MESSAGE = "Error while getting update info: " & sError
                    SkipToInstallationStep(InstallationOrder.ERROR_ERROR_ERROR)
                Else
                    Dim vVersion As Version = New Version(Constants.Version.UPDATE)
                    If vVersion.CompareTo(My.Application.Info.Version) > 0 Then
                        THREADSAFE__InfoMessage("Update needed")

                        Constants.Misc.UPDATE_NEEDED = True

                        Threading.Thread.Sleep(Constants.Misc.FAKE_DELAY)
                    End If
                End If
            Case "download-update"
                If Constants.Misc.UPDATE_NEEDED Then
                    If Not DownloadUpdate(sError) Then
                        Constants.Misc.ERROR_MESSAGE = "Error while downloading update: " & sError
                        SkipToInstallationStep(InstallationOrder.ERROR_ERROR_ERROR)
                    Else
                        BuildUpdateScript()

                        Threading.Thread.Sleep(Constants.Misc.FAKE_DELAY)
                    End If
                End If
            Case "run-update"
                If Constants.Misc.UPDATE_NEEDED Then
                    THREADSAFE__InfoMessage("Installing update")

                    Threading.Thread.Sleep(Constants.Misc.FAKE_DELAY)

                    Helpers.Processes.StartProcess(Constants.Files.UPDATE_SCRIPT, "", False)


                    Threading.Thread.Sleep(Constants.Misc.FAKE_DELAY)

                    Exit Sub
                End If
            Case "get-latest-download"
                THREADSAFE__InfoMessage("Checking for installer")

                If Not GetLatestDownload(sError) Then
                    Constants.Misc.ERROR_MESSAGE = "Error while getting installer info: " & sError
                    SkipToInstallationStep(InstallationOrder.ERROR_ERROR_ERROR)
                Else
                    Constants.Misc.INSTALLER_EXISTS = False

                    If IO.File.Exists(Constants.Files.INSTALLER_FULLPATH) And Not Constants.Misc.RE_DOWNLOAD Then
                        Helpers.Logger.WriteMessageRelative("Installer already exists", 1)

                        Dim hasher As New HashFile(Constants.Files.INSTALLER_FULLPATH)
                        Dim sExistingHash As String = hasher.Calculate(Constants.Files.INSTALLER_FULLPATH)
                        hasher.Dispose()

                        Helpers.Logger.WriteMessageRelative("Checksum: " & sExistingHash, 2)

                        If sExistingHash.Equals(Constants.Checksum.DOWNLOAD) Then
                            Constants.Misc.INSTALLER_EXISTS = True

                            Helpers.Logger.WriteMessageRelative("Checksum matches, user question", 3)

                            SkipToInstallationStep(InstallationOrder.UseExistingInstaller)
                        Else
                            Helpers.Logger.WriteMessageRelative("Checksum doesn't match, downloading anyway", 3)
                        End If
                    End If

                    Threading.Thread.Sleep(Constants.Misc.FAKE_DELAY)
                End If
            Case "download-download"
                Constants.Misc.INSTALLER_EXISTS = False

                If Not DownloadInstaller(sError) Then
                    Constants.Misc.ERROR_MESSAGE = "Error while downloading installer: " & sError
                    SkipToInstallationStep(InstallationOrder.ERROR_ERROR_ERROR)
                Else
                    Threading.Thread.Sleep(Constants.Misc.FAKE_DELAY)
                End If
            Case "verify-download"
                THREADSAFE__InfoMessage("Verifying installer")

                If IO.File.Exists(Constants.Files.INSTALLER_FULLPATH) Then
                    Helpers.Logger.WriteMessageRelative("Download ok", 1)

                    Dim hasher As New HashFile(Constants.Files.INSTALLER_FULLPATH)
                    Dim sExistingHash As String = hasher.Calculate(Constants.Files.INSTALLER_FULLPATH)
                    hasher.Dispose()

                    Helpers.Logger.WriteMessageRelative("Checksum: " & sExistingHash, 1)

                    If Not sExistingHash.Equals(Constants.Checksum.DOWNLOAD) Then
                        Helpers.Logger.WriteMessageRelative("Checksum invalid", 2)

                        Constants.Misc.ERROR_MESSAGE = "Error while downloading installer: Invalid checksum"
                        SkipToInstallationStep(InstallationOrder.ERROR_ERROR_ERROR)
                    Else
                        Helpers.Logger.WriteMessageRelative("Checksum ok", 2)
                    End If
                Else
                    Constants.Misc.ERROR_MESSAGE = "Error while downloading installer: File doesn't exist"
                    SkipToInstallationStep(InstallationOrder.ERROR_ERROR_ERROR)
                End If
            Case "run-installer"
                THREADSAFE__InfoMessage("Running installer")

                Microsoft.Win32.Registry.SetValue(Constants.Misc.REGKEY_LPCSOFTWARE, Constants.Misc.REGVALUE__RUNFROMINSTALLER, "yes")
                Helpers.Processes.StartProcess(Constants.Files.INSTALLER_FULLPATH, "", False)

                Threading.Thread.Sleep(Constants.Misc.FAKE_DELAY_LONGER)
            Case "done"
                THREADSAFE__InfoMessage("Bye")

                Threading.Thread.Sleep(Constants.Misc.FAKE_DELAY)
            Case "display-error"
                THREADSAFE__InfoError(Constants.Misc.ERROR_MESSAGE)
            Case Else
                'Do nothing
        End Select
    End Sub

    Private Sub bgworker_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgworker.RunWorkerCompleted
        If INSTALLATION_STEP <> InstallationOrder.ERROR_ERROR_ERROR Then
            InstallationStep()
        End If
    End Sub


#Region "internet stuff"
    Private Function GetLatestUpdate(ByRef sError As String) As Boolean
        Dim theResponse As HttpWebResponse
        Dim theRequest As HttpWebRequest

        Helpers.Logger.WriteMessage("Getting info for latest update", 0)

        Try
            theRequest = WebRequest.Create(Constants.URLs.LATEST_UPDATE)
            theResponse = theRequest.GetResponse()

            Dim streamResponse As New StreamReader(theResponse.GetResponseStream, Encoding.ASCII)
            Dim sContents As String = streamResponse.ReadLine()

            theResponse.Close()
            streamResponse.Close()


            Helpers.Logger.WriteMessage("received data", 1)
            Helpers.Logger.WriteMessage(sContents, 2)


            If Not sContents.Contains(Constants.Misc.SEPARATOR) Then
                Throw New Exception("Invalid file format")
            End If


            Dim aContents As String() = sContents.Split(Constants.Misc.SEPARATOR)
            Dim sFile As String = aContents(0)
            Dim sVersion As String = aContents(1)
            Dim sChecksum As String = ""
            If aContents.Length > 2 Then
                sChecksum = aContents(2)
            End If

            Helpers.Logger.WriteMessage("update file", 1)
            Helpers.Logger.WriteMessage(sFile, 2)
            Helpers.Logger.WriteMessage("update version", 1)
            Helpers.Logger.WriteMessage(sVersion, 2)
            If Not sChecksum.Equals("") Then
                Helpers.Logger.WriteMessage("update checksum", 1)
                Helpers.Logger.WriteMessage(sChecksum, 2)
            End If


            Constants.URLs.UPDATE = sFile
            Constants.Version.UPDATE = sVersion
            Constants.Checksum.UPDATE = sChecksum

            Constants.Files.UPDATE = sFile

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteError("ERROR", 1)
            Helpers.Logger.WriteError(ex.Message, 2)
            sError = ex.Message
            Return False
        End Try
    End Function

    Private Function GetLatestDownload(ByRef sError As String) As Boolean
        Dim theResponse As HttpWebResponse
        Dim theRequest As HttpWebRequest

        Helpers.Logger.WriteMessage("Getting info for latest update", 0)

        Try
            theRequest = WebRequest.Create(Constants.URLs.LATEST_DOWNLOAD)
            theResponse = theRequest.GetResponse()

            Dim streamResponse As New StreamReader(theResponse.GetResponseStream, Encoding.ASCII)
            Dim sContents As String = streamResponse.ReadLine()

            theResponse.Close()
            streamResponse.Close()


            Helpers.Logger.WriteMessage("received data", 1)
            Helpers.Logger.WriteMessage(sContents, 2)


            If Not sContents.Contains(Constants.Misc.SEPARATOR) Then
                Throw New Exception("Invalid file format")
            End If


            Dim aContents As String() = sContents.Split(Constants.Misc.SEPARATOR)
            Dim sFile As String = aContents(0)
            Dim sVersion As String = aContents(1)
            Dim sChecksum As String = ""
            If aContents.Length > 2 Then
                sChecksum = aContents(2)
            End If

            Helpers.Logger.WriteMessage("download file", 1)
            Helpers.Logger.WriteMessage(sFile, 2)
            Helpers.Logger.WriteMessage("download version", 1)
            Helpers.Logger.WriteMessage(sVersion, 2)
            If Not sChecksum.Equals("") Then
                Helpers.Logger.WriteMessage("download checksum", 1)
                Helpers.Logger.WriteMessage(sChecksum, 2)
            End If

            Constants.URLs.DOWNLOAD = sFile
            Constants.Version.DOWNLOAD = sVersion
            Constants.Checksum.DOWNLOAD = sChecksum

            Constants.Files.INSTALLER = sFile

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteError("ERROR", 1)
            Helpers.Logger.WriteError(ex.Message, 2)
            sError = ex.Message
            Return False
        End Try
    End Function


    Private Function DownloadUpdate(ByRef sError As String) As Boolean
        Dim theResponse As HttpWebResponse
        Dim theRequest As HttpWebRequest

        sError = ""

        Helpers.Logger.WriteMessage("Downloading update", 0)

        Try
            theRequest = WebRequest.Create(Constants.URLs.UPDATE)
            theResponse = theRequest.GetResponse


            Dim bytesTotal As Long = theResponse.ContentLength


            THREADSAFE__InfoMessage("Downloading update")
            THREADSAFE__ToggleProgressBar(True)
            THREADSAFE__ProgressBarValue(0)
            THREADSAFE__ProgressFilesize("")
            THREADSAFE__ProgressSpeed("")
            THREADSAFE__ProgressPercentage("")


            Dim writeStream As New IO.FileStream(Constants.Files.UPDATE_FULLPATH, IO.FileMode.Create)

            Dim bytesReadTotal As Long

            'To calculate the download speed
            Dim speedtimer As New Stopwatch
            Dim timeElapsed As Long = 0
            Dim timeTotal As Double = 0
            Dim timeLeft As Double = 0
            Dim timeLeftSeconds As Integer = 0
            Dim readings As Integer = 0

            Helpers.Logger.WriteMessage("Starting", 1)
            speedtimer.Start()

            Do
                If Me.bgworker.CancellationPending Then
                    Exit Do
                End If

                Dim readBytes(Constants.Misc.DATA_CHUNK_SIZE - 1) As Byte
                Dim bytesRead As Integer = theResponse.GetResponseStream.Read(readBytes, 0, Constants.Misc.DATA_CHUNK_SIZE)

                bytesReadTotal += bytesRead
                Dim quotient As Double = bytesReadTotal / bytesTotal
                Dim percent As Short = quotient * 100

                Dim speed As Double = bytesReadTotal / (speedtimer.ElapsedMilliseconds / 1000)

                THREADSAFE__ProgressBarValue(percent)
                THREADSAFE__ProgressPercentage(percent.ToString & "%")
                THREADSAFE__ProgressSpeed(Helpers.FilesAndFolders.FileSize.GetHumanReadable(speed) & "/s")
                THREADSAFE__ProgressFilesize(Helpers.FilesAndFolders.FileSize.GetHumanReadable(CDbl(bytesReadTotal)) & "/" & Helpers.FilesAndFolders.FileSize.GetHumanReadable(CDbl(bytesTotal)))


                If bytesRead = 0 Then Exit Do

                writeStream.Write(readBytes, 0, bytesRead)

                readings += 1
                If readings >= 5 Then
                    timeElapsed = speedtimer.ElapsedMilliseconds

                    timeTotal = (1.0 / quotient) * timeElapsed
                    timeLeft = timeTotal - timeElapsed
                    timeLeftSeconds = CInt(timeLeft / 1000)

                    readings = 0
                End If
            Loop

            speedtimer.Stop()
            Helpers.Logger.WriteMessage("Finished", 1)

            'Close the streams
            theResponse.GetResponseStream.Close()
            writeStream.Close()

            If Me.bgworker.CancellationPending Then
                IO.File.Delete(Constants.Files.UPDATE_FULLPATH)

                Throw New Exception("Download cancelled")
            End If


            Return True
        Catch ex As Exception
            Helpers.Logger.WriteError("ERROR", 1)
            Helpers.Logger.WriteError(ex.Message, 2)

            sError = ex.Message

            Return False
        End Try
    End Function

    Private Function DownloadInstaller(ByRef sError As String) As Boolean
        Dim theResponse As HttpWebResponse
        Dim theRequest As HttpWebRequest

        sError = ""

        Helpers.Logger.WriteMessage("Downloading installer", 0)

        Try
            theRequest = WebRequest.Create(Constants.URLs.DOWNLOAD)
            theResponse = theRequest.GetResponse


            Dim bytesTotal As Long = theResponse.ContentLength


            THREADSAFE__InfoMessage("Downloading installer: " & Constants.Files.INSTALLER)
            THREADSAFE__ToggleProgressBar(True)
            THREADSAFE__ProgressBarValue(0)
            THREADSAFE__ProgressFilesize("")
            THREADSAFE__ProgressSpeed("")
            THREADSAFE__ProgressPercentage("")


            Dim writeStream As New IO.FileStream(Constants.Files.INSTALLER_FULLPATH, IO.FileMode.Create)

            Dim bytesReadTotal As Long

            'To calculate the download speed
            Dim speedtimer As New Stopwatch
            Dim timeElapsed As Long = 0
            Dim timeTotal As Double = 0
            Dim timeLeft As Double = 0
            Dim timeLeftSeconds As Integer = 0
            Dim readings As Integer = 0

            Helpers.Logger.WriteMessage("Starting", 1)
            speedtimer.Start()

            Do
                If Me.bgworker.CancellationPending Then
                    Exit Do
                End If

                Dim readBytes(Constants.Misc.DATA_CHUNK_SIZE - 1) As Byte
                Dim bytesRead As Integer = theResponse.GetResponseStream.Read(readBytes, 0, Constants.Misc.DATA_CHUNK_SIZE)

                bytesReadTotal += bytesRead
                Dim quotient As Double = bytesReadTotal / bytesTotal
                Dim percent As Short = quotient * 100

                Dim speed As Double = bytesReadTotal / (speedtimer.ElapsedMilliseconds / 1000)

                THREADSAFE__ProgressBarValue(percent)
                THREADSAFE__ProgressPercentage(percent.ToString & "%")
                THREADSAFE__ProgressSpeed(Helpers.FilesAndFolders.FileSize.GetHumanReadable(speed) & "/s")
                THREADSAFE__ProgressFilesize(Helpers.FilesAndFolders.FileSize.GetHumanReadable(CDbl(bytesReadTotal)) & "/" & Helpers.FilesAndFolders.FileSize.GetHumanReadable(CDbl(bytesTotal)))


                If bytesRead = 0 Then Exit Do

                writeStream.Write(readBytes, 0, bytesRead)

                readings += 1
                If readings >= 5 Then
                    timeElapsed = speedtimer.ElapsedMilliseconds

                    timeTotal = (1.0 / quotient) * timeElapsed
                    timeLeft = timeTotal - timeElapsed
                    timeLeftSeconds = CInt(timeLeft / 1000)

                    readings = 0
                End If
            Loop

            speedtimer.Stop()
            Helpers.Logger.WriteMessage("Finished", 1)

            'Close the streams
            theResponse.GetResponseStream.Close()
            writeStream.Close()

            If Me.bgworker.CancellationPending Then
                IO.File.Delete(Constants.Files.INSTALLER_FULLPATH)

                Throw New Exception("Download cancelled")
            End If


            Return True
        Catch ex As Exception
            Helpers.Logger.WriteError("ERROR", 1)
            Helpers.Logger.WriteError(ex.Message, 2)

            sError = ex.Message

            Return False
        End Try
    End Function
#End Region


#Region "update"
    Private Sub BuildUpdateScript()
        Helpers.Logger.WriteMessage("Building update script", 0)

        Dim sUpdateScriptContents As String = My.Resources.update
        Dim sUpdateScriptPath As String = IO.Path.Combine(Constants.Files.UPDATE_PATH, "update.bat")

        Helpers.Logger.WriteMessage(sUpdateScriptPath, 1)


        If IO.File.Exists(sUpdateScriptPath) Then
            IO.File.Delete(sUpdateScriptPath)
        End If


        Dim sAppFullPath As String = Application.ExecutablePath
        Dim sAppName As String = IO.Path.GetFileName(sAppFullPath)


        sUpdateScriptContents = sUpdateScriptContents.Replace("__PROCESSNAME__", Process.GetCurrentProcess().ProcessName & "*")
        sUpdateScriptContents = sUpdateScriptContents.Replace("__DOWNLOADED_EXECUTABLE__", Constants.Files.UPDATE_FULLPATH)
        sUpdateScriptContents = sUpdateScriptContents.Replace("__DESTINATION_EXECUTABLE__", sAppFullPath)


        Dim objWriter As New IO.StreamWriter(sUpdateScriptPath, False)
        objWriter.WriteLine(sUpdateScriptContents)
        objWriter.Close()


        Constants.Files.UPDATE_SCRIPT = sUpdateScriptPath
    End Sub
#End Region
End Class
