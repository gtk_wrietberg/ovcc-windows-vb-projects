﻿Public Class Constants
    Public Class Misc
        Public Shared ReadOnly SEPARATOR As String = "|"
        Public Shared ReadOnly FAKE_DELAY As Integer = 1000
        Public Shared ReadOnly FAKE_DELAY_LONGER As Integer = 5000
        Public Shared ReadOnly DATA_CHUNK_SIZE As Integer = 4096

        Public Shared UPDATE_NEEDED As Boolean = False
        Public Shared INSTALLER_EXISTS As Boolean = False
        Public Shared SITEKIOSK_PROFILE_EXISTS As Integer = 0
        Public Shared RE_DOWNLOAD As Boolean = False

        Public Shared ERROR_MESSAGE As String = ""

        Public Shared ReadOnly REGKEY_LPCSOFTWARE As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCSoftware"
        Public Shared ReadOnly REGVALUE__RUNFROMINSTALLER As String = "run_from_downloader"

        Public Shared ReadOnly SITEKIOSK_USERNAME As String = "sitekiosk"
    End Class

    Public Class URLs
        Public Shared ReadOnly BASE As String = "https://ccms-public.s3.amazonaws.com/OVCCWindows/"
        Public Shared ReadOnly LATEST_DOWNLOAD As String = BASE & "latest_download.txt"
        Public Shared ReadOnly LATEST_UPDATE As String = BASE & "latest_update.txt"

        Private Shared mDOWNLOAD As String = ""
        Public Shared Property DOWNLOAD() As String
            Get
                Return BASE & mDOWNLOAD
            End Get
            Set(ByVal value As String)
                mDOWNLOAD = value
            End Set
        End Property

        Private Shared mUPDATE As String = ""
        Public Shared Property UPDATE() As String
            Get
                Return BASE & mUPDATE
            End Get
            Set(ByVal value As String)
                mUPDATE = value
            End Set
        End Property
    End Class

    Public Class Version
        Private Shared mDownload As String
        Public Shared Property DOWNLOAD() As String
            Get
                Return mDownload
            End Get
            Set(ByVal value As String)
                mDownload = value
            End Set
        End Property

        Private Shared mUpdate As String
        Public Shared Property UPDATE() As String
            Get
                Return mUpdate
            End Get
            Set(ByVal value As String)
                mUpdate = value
            End Set
        End Property
    End Class

    Public Class Files
        Public Shared DOWNLOAD_PATH As String = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\Downloads"
        Public Shared UPDATE_PATH As String = IO.Path.GetTempPath()

        Public Shared UPDATE As String = ""
        Public Shared INSTALLER As String = ""

        Public Shared UPDATE_SCRIPT As String = ""

        Public Shared ReadOnly Property UPDATE_FULLPATH() As String
            Get
                Return IO.Path.Combine(UPDATE_PATH, UPDATE)
            End Get
        End Property

        Public Shared ReadOnly Property INSTALLER_FULLPATH() As String
            Get
                Return IO.Path.Combine(DOWNLOAD_PATH, INSTALLER)
            End Get
        End Property
    End Class

    Public Class Checksum
        Private Shared mDownload As String
        Public Shared Property DOWNLOAD() As String
            Get
                Return mDownload
            End Get
            Set(ByVal value As String)
                mDownload = value
            End Set
        End Property

        Private Shared mUpdate As String
        Public Shared Property UPDATE() As String
            Get
                Return mUpdate
            End Get
            Set(ByVal value As String)
                mUpdate = value
            End Set
        End Property
    End Class
End Class
