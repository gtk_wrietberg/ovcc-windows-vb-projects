﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.progressDownload = New System.Windows.Forms.ProgressBar()
        Me.pnlDownloadProgress = New System.Windows.Forms.Panel()
        Me.lblProgress_Filesize = New System.Windows.Forms.Label()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.lblProgress_Speed = New System.Windows.Forms.Label()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.lblSubTitle = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.bgworker = New System.ComponentModel.BackgroundWorker()
        Me.lblProgress_Percentage = New System.Windows.Forms.Label()
        Me.pnlDownloadProgress.SuspendLayout()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'progressDownload
        '
        Me.progressDownload.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.progressDownload.Location = New System.Drawing.Point(0, 79)
        Me.progressDownload.Name = "progressDownload"
        Me.progressDownload.Size = New System.Drawing.Size(596, 23)
        Me.progressDownload.TabIndex = 0
        '
        'pnlDownloadProgress
        '
        Me.pnlDownloadProgress.BackColor = System.Drawing.Color.White
        Me.pnlDownloadProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlDownloadProgress.Controls.Add(Me.lblProgress_Percentage)
        Me.pnlDownloadProgress.Controls.Add(Me.lblProgress_Filesize)
        Me.pnlDownloadProgress.Controls.Add(Me.btnOK)
        Me.pnlDownloadProgress.Controls.Add(Me.lblProgress_Speed)
        Me.pnlDownloadProgress.Controls.Add(Me.btnCancel)
        Me.pnlDownloadProgress.Controls.Add(Me.progressDownload)
        Me.pnlDownloadProgress.Controls.Add(Me.lblInfo)
        Me.pnlDownloadProgress.Location = New System.Drawing.Point(12, 80)
        Me.pnlDownloadProgress.Name = "pnlDownloadProgress"
        Me.pnlDownloadProgress.Size = New System.Drawing.Size(598, 104)
        Me.pnlDownloadProgress.TabIndex = 1
        '
        'lblProgress_Filesize
        '
        Me.lblProgress_Filesize.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress_Filesize.Location = New System.Drawing.Point(353, 57)
        Me.lblProgress_Filesize.Name = "lblProgress_Filesize"
        Me.lblProgress_Filesize.Size = New System.Drawing.Size(240, 19)
        Me.lblProgress_Filesize.TabIndex = 1
        Me.lblProgress_Filesize.Text = "nnn/mmm MB"
        Me.lblProgress_Filesize.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'btnOK
        '
        Me.btnOK.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOK.Location = New System.Drawing.Point(6, 3)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(240, 31)
        Me.btnOK.TabIndex = 12
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'lblProgress_Speed
        '
        Me.lblProgress_Speed.BackColor = System.Drawing.Color.Transparent
        Me.lblProgress_Speed.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress_Speed.ForeColor = System.Drawing.Color.Black
        Me.lblProgress_Speed.Location = New System.Drawing.Point(3, 57)
        Me.lblProgress_Speed.Name = "lblProgress_Speed"
        Me.lblProgress_Speed.Size = New System.Drawing.Size(243, 19)
        Me.lblProgress_Speed.TabIndex = 2
        Me.lblProgress_Speed.Text = "nnn kB/s"
        Me.lblProgress_Speed.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(353, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(240, 31)
        Me.btnCancel.TabIndex = 13
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblInfo
        '
        Me.lblInfo.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInfo.ForeColor = System.Drawing.Color.Black
        Me.lblInfo.Location = New System.Drawing.Point(3, 0)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(590, 57)
        Me.lblInfo.TabIndex = 3
        Me.lblInfo.Text = "info"
        Me.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSubTitle
        '
        Me.lblSubTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblSubTitle.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.lblSubTitle.Location = New System.Drawing.Point(266, 37)
        Me.lblSubTitle.Name = "lblSubTitle"
        Me.lblSubTitle.Size = New System.Drawing.Size(345, 20)
        Me.lblSubTitle.TabIndex = 11
        Me.lblSubTitle.Text = "software downloader"
        Me.lblSubTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(265, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(345, 27)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "OneView Connection Centre"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picLogo
        '
        Me.picLogo.BackColor = System.Drawing.Color.Transparent
        Me.picLogo.Image = Global.OVCCSoftwareDownloader.My.Resources.Resources.GuestTek_Header_Logo
        Me.picLogo.Location = New System.Drawing.Point(12, 5)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(247, 62)
        Me.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.picLogo.TabIndex = 2
        Me.picLogo.TabStop = False
        '
        'bgworker
        '
        Me.bgworker.WorkerReportsProgress = True
        Me.bgworker.WorkerSupportsCancellation = True
        '
        'lblProgress_Percentage
        '
        Me.lblProgress_Percentage.Font = New System.Drawing.Font("Segoe UI", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress_Percentage.Location = New System.Drawing.Point(252, 57)
        Me.lblProgress_Percentage.Name = "lblProgress_Percentage"
        Me.lblProgress_Percentage.Size = New System.Drawing.Size(95, 19)
        Me.lblProgress_Percentage.TabIndex = 14
        Me.lblProgress_Percentage.Text = "0%"
        Me.lblProgress_Percentage.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(620, 194)
        Me.Controls.Add(Me.pnlDownloadProgress)
        Me.Controls.Add(Me.lblSubTitle)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.picLogo)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.pnlDownloadProgress.ResumeLayout(False)
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents progressDownload As ProgressBar
    Friend WithEvents pnlDownloadProgress As Panel
    Friend WithEvents lblProgress_Speed As Label
    Friend WithEvents lblProgress_Filesize As Label
    Friend WithEvents picLogo As PictureBox
    Friend WithEvents lblSubTitle As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents bgworker As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblInfo As Label
    Friend WithEvents btnCancel As Button
    Friend WithEvents btnOK As Button
    Friend WithEvents lblProgress_Percentage As Label
End Class
