﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        txtUsername = New TextBox()
        btnGetSidProfile = New Button()
        Label1 = New Label()
        Panel1 = New Panel()
        Panel2 = New Panel()
        txtProfile = New TextBox()
        Label3 = New Label()
        txtSid = New TextBox()
        Label2 = New Label()
        Panel1.SuspendLayout()
        Panel2.SuspendLayout()
        SuspendLayout()
        ' 
        ' txtUsername
        ' 
        txtUsername.BackColor = Color.White
        txtUsername.Location = New Point(81, 8)
        txtUsername.Name = "txtUsername"
        txtUsername.Size = New Size(383, 23)
        txtUsername.TabIndex = 0
        ' 
        ' btnGetSidProfile
        ' 
        btnGetSidProfile.Location = New Point(470, 8)
        btnGetSidProfile.Name = "btnGetSidProfile"
        btnGetSidProfile.Size = New Size(67, 23)
        btnGetSidProfile.TabIndex = 1
        btnGetSidProfile.Text = "Get info"
        btnGetSidProfile.UseVisualStyleBackColor = True
        ' 
        ' Label1
        ' 
        Label1.Location = New Point(3, 7)
        Label1.Name = "Label1"
        Label1.Size = New Size(83, 23)
        Label1.TabIndex = 2
        Label1.Text = "Username:"
        Label1.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' Panel1
        ' 
        Panel1.BackColor = Color.WhiteSmoke
        Panel1.BorderStyle = BorderStyle.FixedSingle
        Panel1.Controls.Add(txtUsername)
        Panel1.Controls.Add(btnGetSidProfile)
        Panel1.Controls.Add(Label1)
        Panel1.Location = New Point(12, 12)
        Panel1.Name = "Panel1"
        Panel1.Size = New Size(542, 43)
        Panel1.TabIndex = 5
        ' 
        ' Panel2
        ' 
        Panel2.BackColor = Color.WhiteSmoke
        Panel2.BorderStyle = BorderStyle.FixedSingle
        Panel2.Controls.Add(txtProfile)
        Panel2.Controls.Add(Label3)
        Panel2.Controls.Add(txtSid)
        Panel2.Controls.Add(Label2)
        Panel2.Location = New Point(12, 61)
        Panel2.Name = "Panel2"
        Panel2.Size = New Size(542, 69)
        Panel2.TabIndex = 6
        ' 
        ' txtProfile
        ' 
        txtProfile.BackColor = Color.White
        txtProfile.BorderStyle = BorderStyle.FixedSingle
        txtProfile.Location = New Point(81, 37)
        txtProfile.Name = "txtProfile"
        txtProfile.ReadOnly = True
        txtProfile.Size = New Size(456, 23)
        txtProfile.TabIndex = 3
        ' 
        ' Label3
        ' 
        Label3.Location = New Point(3, 36)
        Label3.Name = "Label3"
        Label3.Size = New Size(83, 23)
        Label3.TabIndex = 4
        Label3.Text = "Profile:"
        Label3.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' txtSid
        ' 
        txtSid.BackColor = Color.White
        txtSid.BorderStyle = BorderStyle.FixedSingle
        txtSid.Location = New Point(81, 8)
        txtSid.Name = "txtSid"
        txtSid.ReadOnly = True
        txtSid.Size = New Size(456, 23)
        txtSid.TabIndex = 0
        ' 
        ' Label2
        ' 
        Label2.Location = New Point(3, 7)
        Label2.Name = "Label2"
        Label2.Size = New Size(83, 23)
        Label2.TabIndex = 2
        Label2.Text = "Sid:"
        Label2.TextAlign = ContentAlignment.MiddleLeft
        ' 
        ' Form1
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.Gainsboro
        ClientSize = New Size(567, 142)
        Controls.Add(Panel2)
        Controls.Add(Panel1)
        FormBorderStyle = FormBorderStyle.FixedSingle
        MaximizeBox = False
        MinimizeBox = False
        Name = "Form1"
        ShowIcon = False
        Text = "Get user sid and profile path"
        Panel1.ResumeLayout(False)
        Panel1.PerformLayout()
        Panel2.ResumeLayout(False)
        Panel2.PerformLayout()
        ResumeLayout(False)
    End Sub

    Friend WithEvents txtUsername As TextBox
    Friend WithEvents btnGetSidProfile As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents txtSid As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtProfile As TextBox
    Friend WithEvents Label3 As Label

End Class
