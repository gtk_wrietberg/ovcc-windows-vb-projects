﻿Public Class Form1
    Private Sub btnGetSidProfile_Click(sender As Object, e As EventArgs) Handles btnGetSidProfile.Click
        Dim sSid As String = ""
        Dim sProfile As String = ""

        If Helpers.WindowsUser.ConvertUsernameToSid(txtUsername.Text, sSid) Then
            txtSid.Text = sSid

            If Helpers.WindowsUser.GetProfileDirectory(Constants.Misc.SITEKIOSK_USERNAME, sProfile) Then
                txtProfile.Text = sProfile
            Else
                txtProfile.Text = "No profile!"
            End If
        Else
            txtSid.Text = "user not found!"
        End If


    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtUsername.Text = Constants.Misc.SITEKIOSK_USERNAME
    End Sub
End Class
