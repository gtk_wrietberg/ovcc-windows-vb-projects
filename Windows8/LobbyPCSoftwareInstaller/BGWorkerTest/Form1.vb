﻿Public Class Form1
    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        StupidLoop()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ProgressBar1.MarqueeAnimationSpeed = 20
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        StupidLoop()
    End Sub

    Private Sub StupidLoop()
        For i = 0 To 10
            LogEventToProgressTextBox("msg" & i.ToString, True)
            If i = 4 Then
                Exit Sub
            End If
            Threading.Thread.Sleep(1000)
        Next
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Button2.Enabled = False

        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        Button2.Enabled = True
    End Sub

    Private Sub LogEventToProgressTextBox(ByVal sMessage As String, Optional threadsafe As Boolean = False)
        If threadsafe Then
            LogToProcessTextBox(sMessage)
        Else
            Me.txtProgress.AppendText(sMessage)
        End If
    End Sub

    Private Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text])
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.txtProgress.Clear()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        MsgBox(Environment.UserName)
    End Sub

    Private Sub CreateAdmin()
        LogEvent("Create admin account", , 1)

        Dim sCurrentUser As String = Environment.UserName.ToLower

        LogEvent("user logged in: " & sCurrentUser, , 2)
        If sCurrentUser = cADMIN_Username Then
            LogEvent("and that's a problem.", , 3)
            LogEvent("I can't change stuff for '" & cADMIN_Username & "' when (s)he's logged in!", , 3)

            Exit Sub
        Else
            LogEvent("ok", , 3)
        End If

        Dim sGroupName_Administrators As String, sGroupName_Users As String
        LogEvent("Get group names", , 2)

        sGroupName_Administrators = cWindowsUser.GetAdministratorsGroupName
        LogEvent("Administrators = '" & sGroupName_Administrators & "'", , 3)

        sGroupName_Users = cWindowsUser.GetUsersGroupName
        LogEvent("Users = '" & sGroupName_Users & "'", , 3)



        'Check if user exists
        LogEvent("Does user '" & cADMIN_Username & "' exist?", , 2)
        If Not cWindowsUser.FindUser(cADMIN_Username, sGroupName_Users) And Not cWindowsUser.FindUser(cADMIN_Username, sGroupName_Administrators) Then
            LogEvent("nope", , 3)
            LogEvent("creating", , 3)

            Dim sRet As String = cWindowsUser.AddUser(cADMIN_Username, cADMIN_Username, cADMIN_Password, sGroupName_Administrators)
            If sRet = "ok" Then
                LogEvent("ok", , 4)
            Else
                LogEvent("error:", , 4)
                LogEvent(sRet, , 5)
            End If
        Else
            LogEvent("yep", , 3)

            LogEvent("But is he an admin?", , 2)
            If cWindowsUser.FindUser(cADMIN_Username, sGroupName_Administrators) Then
                LogEvent("yes!", , 3)
            Else
                LogEvent("nope", , 3)
                LogEvent("adding to admin group", , 2)

                Dim sRet As String = cWindowsUser.AddUserToGroup(cADMIN_Username, sGroupName_Administrators)
                If sRet = "ok" Then
                    LogEvent("ok", , 3)
                Else
                    LogEvent("error:", , 3)
                    LogEvent(sRet, , 4)
                End If
            End If

            LogEvent("updating the password", , 2)
            LogEvent("setting password to default", , 3)

            If cWindowsUser.ChangeUserPassword(cADMIN_Username, cADMIN_Password) Then
                LogEvent("ok", , 4)
            Else
                LogEvent("oops", , 4)
            End If
        End If

        LogEvent("remove password expiry", , 3)
        If cWindowsUser.RemovePasswordExpiry(cADMIN_Username) Then
            LogEvent("ok", , 4)
        Else
            LogEvent("oops", , 4)
        End If
    End Sub

    Private Sub LogEvent(ByVal sMessage As String, Optional ByVal cMessageType As Integer = 0, Optional ByVal iDepth As Integer = 0, Optional ByVal bProgressWindowOnly As Boolean = False)

        LogToProcessTextBox(sMessage & vbCrLf)
    End Sub

    Private Sub BackgroundWorker2_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
        CreateAdmin()
    End Sub


    Private Sub BackgroundWorker2_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker2.RunWorkerCompleted
        Button5.Enabled = True
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Button5.Enabled = False

        BackgroundWorker2.RunWorkerAsync()
    End Sub
End Class
