Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sSourceFolder As String
        Dim sDestinationFolder As String

        sSourceFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder
        sDestinationFolder = cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder

        oLogger.WriteToLog("Altiris package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("Altiris.package.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.exe"

        oLogger.WriteToLog("AdobeReader package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("AdobeReader_9.4.package.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_PdfReader = sDestinationFolder & "\AdobeReader_9.4.package.exe"

        oLogger.WriteToLog("CreateShortcut package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("CreateShortcut.package.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_CreateShortcut = sDestinationFolder & "\CreateShortcut.package.exe"

        oLogger.WriteToLog("pcAnywhere package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("pcAnywhere.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_pcAnywhere = sDestinationFolder & "\pcAnywhere.exe"

        oLogger.WriteToLog("FlashPlayer package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("FlashPlayer.package.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_FlashPlayer = sDestinationFolder & "\FlashPlayer.package.exe"

        oLogger.WriteToLog("iBAHN Updater package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("iBAHNUpdate.package.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_iBAHNUpdate = sDestinationFolder & "\iBAHNUpdate.package.exe"

        oLogger.WriteToLog("LobbyPCAgent package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("LobbyPCAgentPatchInstaller.package.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_LobbyPCAgent = sDestinationFolder & "\LobbyPCAgentPatchInstaller.package.exe"

        oLogger.WriteToLog("LobbyPCWatchdog package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("LobbyPCWatchdog.package.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\LobbyPCWatchdog.package.exe"

        oLogger.WriteToLog("PdfProxy package", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("PdfProxy.package.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_PdfProxy = sDestinationFolder & "\PdfProxy.package.exe"

        oLogger.WriteToLog("SiteKiosk7", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("sitekiosk7.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk7.exe"


        sDestinationFolder = cPATHS_iBAHNProgramFilesFolder

        oLogger.WriteToLog("LobbyPCStandalonePostSiteKiosk", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("LobbyPCStandalonePostSiteKiosk.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_PostSiteKioskUpdating = sDestinationFolder & "\LobbyPCStandalonePostSiteKiosk.exe"

        oLogger.WriteToLog("PcHasRebooted", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("PcHasRebooted.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_PcHasRebooted = sDestinationFolder & "\PcHasRebooted.exe"

        oLogger.WriteToLog("SiteKioskAutoStart", , 1)
        oLogger.WriteToLog("copying", , 2)
        If CopyFile("SiteKioskAutoStart.exe", sSourceFolder, sDestinationFolder) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
        oSettings.Path_AutoStart = sDestinationFolder & "\SiteKioskAutoStart.exe"

    End Sub
End Module
