Module Constants___Globals
    'GUI
    Public ReadOnly cGUI_GroupBoxBorder_visible As Boolean = True
    'Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.FromArgb(158, 19, 14))
    Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.Black)

    'Prerequisites
    Public ReadOnly cPREREQUISITES_NeededWindowsVersion As Prerequisites.WINDOWS_VERSION = Prerequisites.WINDOWS_VERSION.WINDOWS_7

    'License 
    '62.50.212.70
    '172.18.192.10
    'Private ReadOnly cLicenseValidationURL As String = "http://172.18.192.10/standalone_license/requesthandler.asp"
    'Private ReadOnly cLicenseValidationURL As String = "http://62.50.212.70/standalone_license/requesthandler_withstupiddelay.asp"
    'Public ReadOnly cLicenseValidationURL As String = "http://62.50.212.70/standalone_license/requesthandler_uninstall.asp"
    'Public ReadOnly cLicenseValidationURL As String = "http://eureport.ibahn.com/standalone_license_2.0/requesthandler.asp"
    Public ReadOnly cLicenseValidationURL As String = "http://lobbypclicensing.ibahn.com/requesthandler_uninstall.asp"


    Public ReadOnly cLICENSE_KeyLength As Integer = 32
    Public ReadOnly cLICENSE_PermitOfflineLicense As Boolean = False

    'Paths
    Public ReadOnly cPATHS_GuestTekProgramFilesFolder As String = GetProgramFilesFolder() & "\GuestTek\OVCCSoftware"
    Public ReadOnly cPATHS_GuestTekProgramFilesResourcesFolder As String = "\installation_files\Resources"
    Public ReadOnly cPATHS_GuestTekProgramFilesToolsFolder As String = "\tools"
    Public ReadOnly cPATHS_GuestTekProgramFilesInternalFolder As String = "\internal"
    Public ReadOnly cPATHS_GuestTekProgramFilesLogsFolder As String = "\logs"
    Public ReadOnly cPATHS_GuestTekProgramFilesInstallationFolder As String = "\installation_files"



    'Debug
    Public ReadOnly cDEBUG_ForcePrerequisiteWindowsVersionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteInternetConnectionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteUserIsAdminError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteAlreadyInstalledError As Boolean = False

    'Registry
    Public ReadOnly cREGKEY_LPCSOFTWARE As String = "SOFTWARE\GuestTek\OVCCSoftware"

    'SiteKiosk
    Public ReadOnly cSITEKIOSK_UserName As String = "sitekiosk"
    Public ReadOnly cSITEKIOSK_PassWord As String = "provisio"

    'Uninstall
    Public ReadOnly cUNINSTALL_COMMAND As String = "msiexec.exe"
    Public ReadOnly cUNINSTALL_PARAMS As String = "/X{%%APP_IDENTIFIER%%} /quiet /qn /norestart"
    Public ReadOnly cUNINSTALL_ALTIRIS_COMMAND_1 As String = "C:\Program Files (x86)\Altiris\Altiris Agent\AeXAgentUtil.exe"
    Public ReadOnly cUNINSTALL_ALTIRIS_COMMAND_2 As String = "C:\Program Files\Altiris\Altiris Agent\AeXAgentUtil.exe"
    Public ReadOnly cUNINSTALL_ALTIRIS_PARAMS As String = "/uninstallagents /clean"

    Public ReadOnly cCLEANFOLDER_SITEKIOSK_1 As String = "c:\Program Files\SiteKiosk"
    Public ReadOnly cCLEANFOLDER_SITEKIOSK_2 As String = "c:\Program Files (x86)\SiteKiosk"
    Public ReadOnly cCLEANFOLDER_ALTIRIS_1 As String = "c:\Program Files\Altiris"
    Public ReadOnly cCLEANFOLDER_ALTIRIS_2 As String = "c:\Program Files (x86)\Altiris"
    Public ReadOnly cCLEANFOLDER_iBAHN_1 As String = "c:\ibahn"
    Public ReadOnly cCLEANFOLDER_iBAHN_2 As String = "c:\Program Files\iBAHN"
    Public ReadOnly cCLEANFOLDER_iBAHN_3 As String = "c:\Program Files (x86)\iBAHN"
    Public ReadOnly cCLEANFOLDER_GuestTek_1 As String = "c:\Program Files\GuestTek"
    Public ReadOnly cCLEANFOLDER_GuestTek_2 As String = "c:\Program Files (x86)\GuestTek"

    Public ReadOnly cCLEANFOLDER_SERIELL As String = "c:\seriell"

    Public ReadOnly cCLEANREGISTRY_SITEKIOSK As String = "PROVISIO"
    Public ReadOnly cCLEANREGISTRY_ALTIRIS As String = "Altiris"
    Public ReadOnly cCLEANREGISTRY_IBAHN As String = "iBAHN"
    Public ReadOnly cCLEANREGISTRY_GUESTTEK As String = "GuestTek"
    Public ReadOnly cCLEANREGISTRY_OVCCSOFTWAREUNINSTALL As String = "OVCCSoftwareOnly"

    'Globals. Me like globals, me are lazy
    Public oLogger As Logger

    Public gBusyInstalling As Boolean
    Public gTestMode As Boolean
    Public gDebug As Boolean
    Public gAllowBruteForce As Boolean

    Public gUnattendedUninstall As Boolean
    Public gUnattendedForcedReboot As Boolean
    Public gUnattendedRemoveLocalUsers As Boolean = False

    Public gLicenseKeyFromRegistry As String

    Public Function GetProgramFilesFolder() As String
        Dim sPath As String = ""

        sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sPath.Equals(String.Empty) Then
            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        Return sPath
    End Function


End Module
