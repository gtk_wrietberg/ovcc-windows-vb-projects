Module Constants___Globals
    'Some extra installers
    'Public ReadOnly cINSTALLERS__HILTON_UI__EMEA As String = "NewUI.Hilton.revision3.installer.EMEA.exe"
    'Public ReadOnly cINSTALLERS__HILTON_UI__US As String = "NewUI.Hilton.revision3.installer.US.exe"
    'Public ReadOnly cINSTALLERS__IBAHN_UI__EMEA As String = "NewUI.iBAHN.installer.EMEA.exe"
    'Public ReadOnly cINSTALLERS__IBAHN_UI__US As String = "NewUI.iBAHN.installer.US.exe"
    'Public ReadOnly cINSTALLERS__GUESTTEK_UI__EMEA As String = "NewUI.Guest-tek.installer.EMEA.exe"
    'Public ReadOnly cINSTALLERS__GUESTTEK_UI__US As String = "NewUI.Guest-tek.installer.US.exe"
    'Public ReadOnly cINSTALLERS__GUESTTEK_UI__US As String = "SiteKiosk8.installer.exe"


    'GUI
    Public ReadOnly cGUI_GroupBoxBorder_visible As Boolean = True
    'Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.FromArgb(158, 19, 14))
    Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.Black)
    Public ReadOnly cGUI_GuestTek_images As Boolean = True

    'Prerequisites
    Public ReadOnly cPREREQUISITES_NeededWindowsVersion As Prerequisites.WINDOWS_VERSION = Prerequisites.WINDOWS_VERSION.WINDOWS_7

    'License 
    '62.50.212.70
    '172.18.192.10
    'Private ReadOnly cLicenseValidationURL As String = "http://172.18.192.10/standalone_license/requesthandler.asp"
    'Private ReadOnly cLicenseValidationURL As String = "http://62.50.212.70/standalone_license/requesthandler_withstupiddelay.asp"
    'Public ReadOnly cLicenseValidationURL As String = "http://62.50.212.70/standalone_license/requesthandler_uninstall.asp"
    'Public ReadOnly cLicenseValidationURL As String = "http://eureport.ibahn.com/standalone_license_2.0/requesthandler.asp"
    Public ReadOnly cLicenseValidationURL As String = "http://lobbypclicensing.ibahn.com/requesthandler.asp"
    Public ReadOnly cLicenseIncrementURL As String = "http://lobbypclicensing.ibahn.com/requesthandler_increment.asp"

    Public ReadOnly cLICENSE_KeyLength As Integer = 32
    Public ReadOnly cLICENSE_PermitOfflineLicense As Boolean = False

    'Paths
    'Public ReadOnly cPATHS_GuestTekProgramFilesFolder As String = "C:\Program Files\GuestTek\LobbyPCSoftware"
    Public ReadOnly cPATHS_GuestTekProgramFilesFolder As String = "C:\Program Files\GuestTek\OVCCSoftware"
    'Public ReadOnly cPATHS_GuestTekProgramFilesFolder_x86 As String = "C:\Program Files (x86)\GuestTek\LobbyPCSoftware"
    Public ReadOnly cPATHS_GuestTekProgramFilesFolder_x86 As String = "C:\Program Files (x86)\GuestTek\OVCCSoftware"
    Public ReadOnly cPATHS_GuestTekProgramFilesResourcesFolder As String = "\installation_files\Resources"
    Public ReadOnly cPATHS_GuestTekProgramFilesToolsFolder As String = "\tools"
    Public ReadOnly cPATHS_GuestTekProgramFilesInternalFolder As String = "\internal"
    Public ReadOnly cPATHS_GuestTekProgramFilesConfigFolder As String = "\config"

    Public ReadOnly cPATHS_ConfigFile As String = "OVCCSoftware.config.xml"

    'Debug
    Public ReadOnly cDEBUG_ForcePrerequisiteWindowsVersionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteInternetConnectionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteUserIsAdminError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteAlreadyInstalledError As Boolean = False

    'SiteKiosk
    Public ReadOnly cSITEKIOSK_UserName As String = "sitekiosk"
    Public ReadOnly cSITEKIOSK_PassWord As String = "provisio"

    'admin user
    Public Const cADMIN_Username As String = "guesttek"
    Public Const cADMIN_Password As String = "rancidkipper"

    'Registry
    Public ReadOnly cREGKEY_LPCSOFTWARE As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCSoftware"

    Public ReadOnly cREGKEY_POLICIES_EXPLORER As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
    Public ReadOnly cREGKEY_POLICIES_EXPLORER2 As String = "HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Explorer"

    'Globals. Me like globals, me are lazy
    Public oLogger As Logger
    Public oSettings As Settings
    Public oLicenseKeySettings As LicenseKeySettings
    Public oRegistryChanger As RegistryChanger

    Public gBusyInstalling As Boolean
    Public gTestMode As Boolean

    Public gAllFilesCopied As Boolean

    Public gHideProgress As Boolean
    Public gRegion As String
    Public gLicenseRegion As String
    Public gEnglishFlag As String
    Public gSkipSkype As Boolean
    Public gSkipLogMeIn As Boolean
    Public gSiteKioskUserPassword As String

    Public gLogMeInDeployID As String

    Public gGuestTekProgramFilesFolder As String
End Module
