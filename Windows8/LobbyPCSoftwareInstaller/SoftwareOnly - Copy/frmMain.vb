﻿Imports System.Threading

Public Class frmMain
    Private iApplicationInstallationCount As Integer = 0
    Private iApplicationInstallationCountMax As Integer = 13

    Private iProgress As Integer = 0
    Private iProgressMax As Integer = 15

    Private WithEvents oProcess As ProcessRunner
    Private oComputerName As ComputerName
    Private installThread As Thread

    Private iActiveGroupBox As Integer

    Private Enum InstallationOrder As Integer
        VisualCplusplusRedistributable = 1
        CreateAdminAccount
        LogMeIn
        Altiris
        SiteKiosk
        SiteKioskUpdate
        SiteKioskUpdate32bit
        SystemSecurityUpdater
        OVCCAgent
        OVCCWatchdog
        Shortcuts
    End Enum

    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

#Region " ClientAreaMove Handling "
    'Private Const WM_NCHITTEST As Integer = &H84
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    'Private Const HTCLIENT As Integer = &H1
    Private Const HTCAPTION As Integer = &H2
    '    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
    '        Select Case m.Msg
    '            Case WM_NCHITTEST
    '                MyBase.WndProc(m)
    '    'If m.Result = HTCLIENT Then m.Result = HTCAPTION
    '                If m.Result.ToInt32 = HTCLIENT Then m.Result = IntPtr.op_Explicit(HTCAPTION) 'Try this in VS.NET 2002/2003 if the latter line of code doesn't do it... thx to Suhas for the tip.
    '            Case Else
    '    'Make sure you pass unhandled messages back to the default message handler.
    '                MyBase.WndProc(m)
    '        End Select
    '    End Sub
#End Region

#Region "Form Events"
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Don't close the form when we're still busy
        e.Cancel = gBusyInstalling
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActivatePrevInstance(Me.Text)

        oComputerName = New ComputerName
        oSettings = New Settings
        oLogger = New Logger


        If InStr(oComputerName.ComputerName.ToLower, "rietberg".ToLower) > 0 _
            Or InStr(oComputerName.ComputerName.ToLower, "wrdev".ToLower) > 0 _
            Or InStr(oComputerName.ComputerName.ToLower, "superlekkerding".ToLower) > 0 Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If
        End If

        'GUI
        Me.StartPosition = FormStartPosition.Manual

        Me.Left = (My.Computer.Screen.Bounds.Width - Me.Width) / 2
        Me.Top = (My.Computer.Screen.Bounds.Height - Me.Height) / 2

        Me.Opacity = 1.0
        gHideProgress = False

        If IO.Directory.Exists(cPATHS_GuestTekProgramFilesFolder_x86) Then
            gGuestTekProgramFilesFolder = cPATHS_GuestTekProgramFilesFolder_x86
        Else
            gGuestTekProgramFilesFolder = cPATHS_GuestTekProgramFilesFolder
        End If

        If Not IO.Directory.Exists(gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder) Then
            IO.Directory.CreateDirectory(gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder)
        End If

        oLogger.LogFilePath = gGuestTekProgramFilesFolder & "\"

        txtProgress.Clear()
        Me.ControlBox = False
        txtProgress.Font = System.Drawing.SystemFonts.MessageBoxFont


        LogEvent(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = "v" & myBuildInfo.FileVersion

        LogEvent(Application.ProductName & " v" & myBuildInfo.FileVersion)
        LogEvent("Computer name: " & oComputerName.ComputerName)


        If gTestMode Then
            LogEvent("Running in test mode!!!", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        End If


        gBusyInstalling = False


        'Install count max
        'iApplicationInstallationCountMax = InstallationOrder.SiteKioskSecurityUpdater + 1
        iApplicationInstallationCountMax = [Enum].GetValues(GetType(InstallationOrder)).Cast(Of InstallationOrder)().Max() + 1


        tmrStartDeployment.Enabled = True
    End Sub
#End Region

#Region "Private Functions"
#Region "Logging"
    Private Sub LogEvent(ByVal sMessage As String, Optional ByVal cMessageType As Logger.MESSAGE_TYPE = Logger.MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bProgressWindowOnly As Boolean = False)
        Dim sLogText As String

        sLogText = oLogger.WriteToLog(sMessage, cMessageType, iDepth, bProgressWindowOnly)

        If bProgressWindowOnly Then
            LogEventToProgressTextBox(sLogText)
        Else
            LogEventToProgressTextBox(sLogText & vbCrLf)
        End If
    End Sub

    Private Sub LogEventToProgressTextBox(ByVal sMessage As String)
        LogToProcessTextBox(sMessage)
    End Sub
#End Region
#Region "Thread Safe stuff"
    Private Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text])
        End If
    End Sub
#End Region

#Region "Deployment and Installers"
#Region "Deployment"
    Private Sub StartDeployment()
        If MsgBox("Are you sure you want to start the installation?", vbYesNo, "Confirm installation") = MsgBoxResult.Yes Then
            _StartDeployment()
        Else
            LogEvent("Installation cancelled by user", Logger.MESSAGE_TYPE.LOG_WARNING)

            Done()
        End If
    End Sub

    Private Sub _StartDeployment()
        LogEvent("Loading deployment config")

        LogEvent("default values", , 1)
        gRegion = "eu"
        LogEvent("region: eu", , 2)
        gLicenseRegion = ""
        LogEvent("license: equal to region", , 2)

        gSkipSkype = False
        gSkipLogMeIn = False

        gLogMeInDeployID = ""
        gSiteKioskUserPassword = cSITEKIOSK_PassWord

        gEnglishFlag = "gb"


        LogEvent("loading command line arguments", , 1)
        For Each arg As String In Environment.GetCommandLineArgs()
            LogEvent("found: " & arg, , 2)

            If InStr(arg, "-license=") > 0 Then
                gLicenseRegion = arg.Replace("-license=", "")
                LogEvent("license: " & gLicenseRegion, , 3)
            End If
            If InStr(arg, "-english-flag=") > 0 Then
                gEnglishFlag = arg.Replace("-english-flag=", "")
                LogEvent("english flag: " & gEnglishFlag, , 3)
                If Not gEnglishFlag.Equals("gb") And Not gEnglishFlag.Equals("us") Then
                    LogEvent("defaulting to 'gb'", , 4)
                    gEnglishFlag = "gb"
                End If
            End If
            If arg = "-eu" Or arg = "-emea" Then
                gRegion = "eu"
                LogEvent("region: " & gRegion, , 3)
            End If
            If arg = "-us" Or arg = "-yankees" Then
                gRegion = "us"
                LogEvent("region: " & gRegion, , 3)
            End If
            If arg = "-hideprogress" Then
                gHideProgress = True

                Me.Opacity = 0.0

                LogEvent("hide progress: " & gHideProgress.ToString, , 3)
            End If
            If arg = "-skipskype" Then
                gSkipSkype = True

                LogEvent("skip skype: " & gSkipSkype.ToString, , 3)
            End If
            If arg = "-skiplogmein" Then
                gSkipLogMeIn = True

                LogEvent("skip LogMeIn: " & gSkipLogMeIn.ToString, , 3)
            End If
            If InStr(arg, "-logmein-deployid=") > 0 Then
                gLogMeInDeployID = arg.Replace("-logmein-deployid=", "")
                LogEvent("LogMeIn DeployID: " & gLogMeInDeployID, , 3)
            End If
            If InStr(arg, "-sitekiosk-user-password=") > 0 Then
                gSiteKioskUserPassword = arg.Replace("-sitekiosk-user-password=", "")
                LogEvent("Non default SiteKiosk user password: " & New String("*", gSiteKioskUserPassword.Length), , 3)
            End If
        Next

        If gLicenseRegion = "" Then
            gLicenseRegion = gRegion
        End If


        LogEvent("Storing deployment config in registry")
        LogEvent("blocked drives", , 1)
        Try
            Dim sDrivesToBeBlocked As String
            sDrivesToBeBlocked = GetDrivesToBeBlocked()
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\BlockedDrives", , 3)
            LogEvent("value: " & sDrivesToBeBlocked, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "BlockedDrives", sDrivesToBeBlocked)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        LogEvent("license", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\License", , 3)
            LogEvent("value: " & gLicenseRegion, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "License", gLicenseRegion)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        LogEvent("english flag", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\EnglishFlag", , 3)
            LogEvent("value: " & gEnglishFlag, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "EnglishFlag", gEnglishFlag)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        If gLogMeInDeployID <> "" Then
            LogEvent("LogMeIn DeployID", , 1)
            Try
                LogEvent("Registry", , 2)
                LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\LogMeInDeployID", , 3)
                LogEvent("value: " & gLogMeInDeployID, , 3)
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "LogMeInDeployID", gLogMeInDeployID)
            Catch ex As Exception
                LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If

        LogEvent("region", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\Region", , 3)
            LogEvent("value: " & gRegion, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "Region", gRegion)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        If gSkipSkype Then
            LogEvent("skip skype", , 1)
            Try
                LogEvent("Registry", , 2)
                LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\SkipSkype", , 3)
                LogEvent("value: yes", , 3)
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "SkipSkype", "yes")
            Catch ex As Exception
                LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If


        LogEvent("Backing up some stuff")

        LogEvent("Computer name", , 1)
        LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Backup\Computer Name", , 2)
        LogEvent("value: " & oComputerName.ComputerName, , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Backup", "Computer Name", oComputerName.ComputerName)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogEvent("Workgroup name", , 1)
        LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Backup\Workgroup Name", , 2)
        LogEvent("value: " & oComputerName.Workgroup, , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Backup", "Workgroup Name", oComputerName.Workgroup)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogEvent("Updating default user profile")
        LogEvent("Registry", , 1)
        LogEvent("key  : HKEY_USERS\.DEFAULT\Control Panel\PowerCfg\CurrentPowerPolicy", , 2)
        LogEvent("value: 2", , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_USERS\.DEFAULT\Control Panel\PowerCfg", "CurrentPowerPolicy", "2")
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        'LogEvent("Updating User Agent")
        'LogEvent("Registry", , 1)
        'LogEvent("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", , 2)
        'LogEvent("value: Mozilla/5.0", , 2)
        'Try
        '    Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "", "Mozilla/5.0")
        'Catch ex As Exception
        '    LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        '    LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        '    LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        'End Try

        'LogEvent("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent\Version", , 2)
        'LogEvent("value: MSIE 9.0", , 2)
        'Try
        '    Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "Version", "MSIE 9.0")
        'Catch ex As Exception
        '    LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        '    LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        '    LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        'End Try


        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V HonorAutoRunSetting  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveTypeAutoRun  /T REG_DWORD  /D 0x000000ff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveAutoRun  /T REG_DWORD  /D 0x0003ffff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoAutoRun  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\SYSTEM\CurrentControlSet\Services\Cdrom" /V AutoRun  /T REG_DWORD  /D 0x00000000  /F
        'reg.exe ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping\Autorun.inf" /VE      /T REG_SZ    /D "@SYS:DoesNotExist" /F

        LogEvent("Disabling AutoRun")
        LogEvent("Registry", , 1)
        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!HonorAutoRunSetting", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "HonorAutoRunSetting", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoDriveTypeAutoRun", , 2)
        LogEvent("value: 255", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoDriveTypeAutoRun", 255)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoDriveAutoRun", , 2)
        LogEvent("value: 262143", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoDriveAutoRun", 262143)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoAutoRun", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoAutoRun", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER2 & "!NoAutoplayfornonVolume", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER2, "NoAutoplayfornonVolume", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try




        LogEvent("Storing main installer version number")
        LogEvent("Registry", , 1)
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        LogEvent(cREGKEY_LPCSOFTWARE & "\Version", , 2)
        LogEvent("Installer = '" & myBuildInfo.FileVersion & "'", , 3)
        Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Version", "Installer", myBuildInfo.FileVersion)


        UnpackResources()


        LogEvent("license file", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\LicenseFile", , 3)
            LogEvent("value: " & oSettings.Path_SiteKioskLicensesFile, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "LicenseFile", oSettings.Path_SiteKioskLicensesFile)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        LogEvent("IsThisTheSoftwarePackage", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\IsThisTheSoftwarePackage", , 3)
            LogEvent("value: yes", , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "IsThisTheSoftwarePackage", "yes")
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        NextInstallationStep()
    End Sub

    Private Sub PostDeployment()
        LogEvent("Post deployment steps")
        LogEvent("Registry", , 1)

        LogEvent(cREGKEY_LPCSOFTWARE, , 2)
        LogEvent("Restarted = 'no'", , 3)
        Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE, "Restarted", "no")


        Done()
    End Sub

    Private Sub Done()
        LogEvent("DONE")

        Me.ControlBox = True

        If Not gTestMode Then
            tmrExit.Enabled = True
        End If
    End Sub

    Private Sub NextInstallationStep()
        tmrInstallerWait.Enabled = False

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub InstallationController()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner

        LogEvent("Installation step: " & CStr(iApplicationInstallationCount) & "/" & iApplicationInstallationCountMax, Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

        If iApplicationInstallationCount < iApplicationInstallationCountMax Then
            tmrInstallerWait.Enabled = True
        End If

        If gTestMode Then
            'iApplicationInstallationCount = iApplicationInstallationCountMax
        End If


        'If iApplicationInstallationCount = InstallationOrder.LogMeIn And gSkipLogMeIn Then
        '    'Skip LogMeIn 
        '    iApplicationInstallationCount += 1
        'End If



        Select Case iApplicationInstallationCount
            Case InstallationOrder.VisualCplusplusRedistributable
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallVisualCplusplusRedistributable))
                Me.installThread.Start()
            Case InstallationOrder.CreateAdminAccount
                _CreateAdminAccount()
            Case InstallationOrder.LogMeIn
                _InstallLogMeIn()
            Case InstallationOrder.Altiris
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallAltiris))
                Me.installThread.Start()
            Case InstallationOrder.OVCCAgent
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallOVCCAgent))
                Me.installThread.Start()
            Case InstallationOrder.OVCCWatchdog
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallOVCCWatchdog))
                Me.installThread.Start()
            Case InstallationOrder.SiteKiosk
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKiosk))
                Me.installThread.Start()
            Case InstallationOrder.SiteKioskUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKioskUpdate))
                Me.installThread.Start()
            Case InstallationOrder.SiteKioskUpdate32bit
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKioskUpdate32bit))
                Me.installThread.Start()
            Case InstallationOrder.SystemSecurityUpdater
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKioskSystemSecurity))
                Me.installThread.Start()
            Case InstallationOrder.Shortcuts
                _CreateShortcuts()
            Case Else
                LogEvent("Done", , 0)

                gBusyInstalling = False

                PostDeployment()
        End Select
    End Sub
#End Region

#Region "Installers"
#Region "admin user creation"
    Private Sub _CreateAdminAccount()
        bgworkerAdminUserCreation.RunWorkerAsync()
    End Sub

    Private Sub bgworkerAdminUserCreation_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworkerAdminUserCreation.DoWork
        LogEvent("Create admin account", , 1)

        Dim sCurrentUser As String = Environment.UserName.ToLower

        LogEvent("user logged in: " & sCurrentUser, , 2)
        If sCurrentUser = cADMIN_Username Then
            LogEvent("and that's a problem.", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("I can't change stuff for '" & cADMIN_Username & "' when (s)he's logged in!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Exit Sub
        Else
            LogEvent("ok", , 3)
        End If

        Dim sGroupName_Administrators As String, sGroupName_Users As String
        LogEvent("Get group names", , 2)

        sGroupName_Administrators = cWindowsUser.GetAdministratorsGroupName
        LogEvent("Administrators = '" & sGroupName_Administrators & "'", , 3)

        sGroupName_Users = cWindowsUser.GetUsersGroupName
        LogEvent("Users = '" & sGroupName_Users & "'", , 3)


        'Check if user exists
        LogEvent("Does user '" & cADMIN_Username & "' exist?", , 2)
        If Not cWindowsUser.FindUser(cADMIN_Username, sGroupName_Users) And Not cWindowsUser.FindUser(cADMIN_Username, sGroupName_Administrators) Then
            LogEvent("nope", , 3)
            LogEvent("creating", , 3)

            Dim sRet As String = cWindowsUser.AddUser(cADMIN_Username, cADMIN_Username, cADMIN_Password, sGroupName_Administrators)
            If sRet = "ok" Then
                LogEvent("ok", , 4)
            Else
                LogEvent("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                LogEvent(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End If
        Else
            LogEvent("yep", , 3)

            LogEvent("But is he an admin?", , 2)
            If cWindowsUser.FindUser(cADMIN_Username, sGroupName_Administrators) Then
                LogEvent("yes!", , 3)
            Else
                LogEvent("nope", , 3)
                LogEvent("adding to admin group", , 2)

                Dim sRet As String = cWindowsUser.AddUserToGroup(cADMIN_Username, sGroupName_Administrators)
                If sRet = "ok" Then
                    LogEvent("ok", , 3)
                Else
                    LogEvent("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    LogEvent(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            End If

            LogEvent("updating the password", , 2)
            LogEvent("setting password to default", , 3)

            If cWindowsUser.ChangeUserPassword(cADMIN_Username, cADMIN_Password) Then
                LogEvent("ok", , 4)
            Else
                LogEvent("oops", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If
        End If

        LogEvent("remove password expiry", , 3)
        If cWindowsUser.RemovePasswordExpiry(cADMIN_Username) Then
            LogEvent("ok", , 4)
        Else
            LogEvent("oops", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End If
    End Sub

    Private Sub bgworkerAdminUserCreation_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgworkerAdminUserCreation.RunWorkerCompleted
        NextInstallationStep()
    End Sub
#End Region

    Private Sub _CreateShortcuts()
        LogEvent("Creating shortcut for OVCCStarter", , 1)
        Shortcut.CreateOnDesktop(oSettings.Path_AutoStart, "Start OVCCStarter software")

        LogEvent("Creating shortcut for OVCCInformationUpdater", , 1)
        Shortcut.CreateOnDesktop(oSettings.Path_HotelPageUpdater, "OVCCInformationUpdater")

        NextInstallationStep()
    End Sub

    Private Sub _InstallVisualCplusplusRedistributable()
        Dim sParams As String = "/q /norestart"

        LogEvent("Installing Visual C++ redistributable")
        LogEvent("Path   : " & oSettings.Path_VisualCplusplusRedistributable, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_VisualCplusplusRedistributable
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLogMeIn()
        If gSkipLogMeIn Then
            LogEvent("Installing remote access application SKIPPED!!")

            NextInstallationStep()
        Else
            Me.installThread = New Thread(New ThreadStart(AddressOf Me.__InstallLogMeIn))
            Me.installThread.Start()
        End If
    End Sub

    Private Sub __InstallLogMeIn()
        Dim sParams As String = ""

        If gLogMeInDeployID <> "" Then
            sParams &= "-spdeployid=" & gLogMeInDeployID
        Else
            sParams &= "-spdeployid=01_ji9beh9f0fscc0pjejl0wrdkd4f1q5r8ednq7"
        End If

        LogEvent("Installing remote access application")
        LogEvent("Path   : " & oSettings.Path_LogMeIn, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        LogEvent("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LogMeIn
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "LogMeIn_Installer"
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallOVCCAgent()
        LogEvent("Installing OVCCAgent service")
        LogEvent("Path   : " & oSettings.Path_LobbyPCAgent, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCAgent
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "OVCCAgent_installer"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallOVCCWatchdog()
        LogEvent("Installing OVCC Watchdog service")
        LogEvent("Path   : " & oSettings.Path_LobbyPCWatchdog, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCWatchdog
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "OVCCWatchdog_installer"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKioskUpdate()
        LogEvent("Installing SiteKiosk update files")
        LogEvent("Path   : " & oSettings.Path_SiteKioskUpdate, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKioskUpdate
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "SiteKioskUpdater"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKioskUpdate32bit()
        LogEvent("Installing SiteKiosk update files (32bit)")
        LogEvent("Path   : " & oSettings.Path_SiteKioskUpdate32bit, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKioskUpdate32bit
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKioskSystemSecurity()
        LogEvent("Installing SiteKiosk System Security Update")
        LogEvent("Path   : " & oSettings.Path_SystemSecurityUpdater, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SystemSecurityUpdater
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallAltiris()
        LogEvent("Installing remote administration application")
        LogEvent("Path   : " & oSettings.Path_Altiris, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_Altiris
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "AeXNSCHTTP"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKiosk()
        'Dim sParams As String = "/S /V""/qn"""
        Dim sParams As String = "/S /V""/qn"" /v""DEFAULTPASSWD=Provisi0"""

        LogEvent("Installing kiosk software")
        LogEvent("Path   : " & oSettings.Path_SiteKiosk, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKiosk
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

#End Region
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationIsCanceled()
        Application.Exit()
        'gBusyInstalling = False

        'MsgBox("Installation cancelled by user", MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation, Application.ProductName)
    End Sub

    Private Sub ApplicationIsExited()
        LogEvent("Exiting application")

        Application.Exit()
    End Sub
#End Region
#End Region

#Region "Events"
#Region "DragWindow events"
    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub picboxOnTopbar_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Process Events"
    Private Sub oProcess_Busy(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Busy
        LogEventToProgressTextBox(".")
    End Sub

    Private Sub oProcess_Test(ByVal EventProcess As System.Diagnostics.Process) Handles oProcess.Test
        LogEvent("test", , 1)
    End Sub

    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("done", , 1)
        LogEvent("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("failed", , 1)
        LogEvent(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        LogEvent("started", , 1)
        LogEvent("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
#End Region

#Region "Timer Events"
    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            NextInstallationStep()
        End If
    End Sub

    Private Sub tmrStartDeployment_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrStartDeployment.Tick
        tmrStartDeployment.Enabled = False

        StartDeployment()
    End Sub

    Private Sub tmrExit_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrExit.Tick
        ApplicationIsExited()
    End Sub
#End Region

    Private Sub txtProgress_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtProgress.MouseDown
        tmrExit.Enabled = False
    End Sub
#End Region
End Class
