﻿''=============================================================================================================
''
'' Author & Modified by Florin Panescu (Florin2020).
''
'' Date: 29-Sep-2014
''
'' Filename: LinkShortcut.vb
''
'' Classes:
'' - InternetShortcut
'' - AppShortcut
''
'' Summary: Create shortcut .lnk & .url
'' This class includes preset destination path for creating shortcut.
'' Ex: Desktop, Start Menu, Startup, SendTo, Quick Launch.
'' To use this preset path, you must write: Target(PathTo...) and choose prefered path.
'' You can use custom destination path.
'' Ex: Dim szCustomPath As String = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
'' and replace Target(PathTo...) with szCustomPath.
''
''=============================================================================================================

#Region " Options "
Option Strict On
Option Explicit On
Option Compare Binary
#End Region

#Region " Imports "
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
#End Region

#Region " Application Shortcut Class"

''' <summary>Application Shortcut Calss.</summary>
Public Class AppShortcut
        Implements IDisposable

    Public Shared Function CreateOnDesktop(ByVal FilePath As String, ByVal Title As String, Optional CurrentUserOnly As Boolean = False) As String
        Dim fileShortcut As String

        If CurrentUserOnly Then
            fileShortcut = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), Title & ".lnk")
        Else
            fileShortcut = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory), Title & ".lnk")
        End If


        Return _Create(fileShortcut, FilePath, Title)
    End Function

    Public Shared Function CreateInStartMenu(ByVal FilePath As String, ByVal Title As String, Optional CurrentUserOnly As Boolean = False) As String
        Dim fileShortcut As String

        If CurrentUserOnly Then
            fileShortcut = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu), Title & ".lnk")
        Else
            fileShortcut = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu), Title & ".lnk")
        End If

        Return _Create(fileShortcut, FilePath, Title)
    End Function

    Private Shared Function _Create(ByVal FileShortcut As String, ByVal FilePath As String, ByVal Title As String) As String
        oLogger.WriteToLogRelative("filepath", , 1)
        oLogger.WriteToLogRelative(FilePath, , 2)

        oLogger.WriteToLogRelative("shortcut", , 1)
        oLogger.WriteToLogRelative(FileShortcut, , 2)

        oLogger.WriteToLogRelative("creating", , 1)


        Dim _ret As String = AppShortcut.AddLnkShortcut(FileShortcut, FilePath, IO.Path.GetDirectoryName(FilePath), String.Empty, Title, FilePath, 0, ProcessWindowStyle.Normal)

        If Not _ret.Equals("") Then
            oLogger.WriteToLogRelative("error", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(_ret, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Else
            oLogger.WriteToLogRelative("ok", , 2)
        End If

        Return _ret
    End Function


    Public Shared Function AddLnkShortcut(ByVal ShortcutPath As String, ByVal AppTarget As String, ByVal WorkingDirectory As String,
                                  ByVal Arguments As String, ByVal Description As String, ByVal nIcon As String,
                                  ByVal nIconindex As Integer, ByVal nWindowStyle As ProcessWindowStyle) As String

        Try
            Dim SavelnkShortcut As AppShortcut = New AppShortcut(ShortcutPath)

            With SavelnkShortcut
                .Path = AppTarget
                .WorkingDirectory = WorkingDirectory
                .Arguments = Arguments
                .Description = Description
                .IconPath = nIcon
                .IconIndex = CInt(nIconindex)
                .WindowStyle = nWindowStyle
                .Save()
            End With

            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function


#Region " Interface "
    <Flags()>
        Private Enum SLR_FLAGS
            SLR_NO_UI = &H1
            SLR_ANY_MATCH = &H2
            SLR_UPDATE = &H4
            SLR_NOUPDATE = &H8
            SLR_NOSEARCH = &H10
            SLR_NOTRACK = &H20
            SLR_NOLINKINFO = &H40
            SLR_INVOKE_MSI = &H80
        End Enum

        ' IShellLink.GetPath fFlags
        <Flags()>
        Private Enum SLGP_FLAGS
            SLGP_SHORTPATH = &H1
            SLGP_UNCPRIORITY = &H2
            SLGP_RAWPATH = &H4
        End Enum

        <ObsoleteAttribute>
        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Ansi)> Private Structure WIN32_FIND_DATAA
            Private dwFileAttributes As Integer
            Private ftCreationTime As FILETIME
            Private ftLastAccessTime As FILETIME
            Private ftLastWriteTime As FILETIME
            Private nFileSizeHigh As Integer
            Private nFileSizeLow As Integer
            Private dwReserved0 As Integer
            Private dwReserved1 As Integer
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PATH)> Private cFileName As String
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=14)> Private cAlternateFileName As String
            Private Const MAX_PATH As Integer = 260
        End Structure

        <ObsoleteAttribute>
        <StructLayoutAttribute(LayoutKind.Sequential, CharSet:=CharSet.Unicode)> Private Structure WIN32_FIND_DATAW
            Private dwFileAttributes As Integer
            Private ftCreationTime As FILETIME
            Private ftLastAccessTime As FILETIME
            Private ftLastWriteTime As FILETIME
            Private nFileSizeHigh As Integer
            Private nFileSizeLow As Integer
            Private dwReserved0 As Integer
            Private dwReserved1 As Integer
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=MAX_PATH)> Private cFileName As String
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=14)> Private cAlternateFileName As String
            Private Const MAX_PATH As Integer = 260
        End Structure

        <
          ComImport(),
          InterfaceType(ComInterfaceType.InterfaceIsIUnknown), Guid("0000010B-0000-0000-C000-000000000046")
        >
        Private Interface IPersistFile

#Region "Methods inherited from IPersist"

            Sub GetClassID(<Out()> ByRef pClassID As Guid)
#End Region

            <PreserveSig()>
            Function IsDirty() As Integer

            Sub Load(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszFileName As String, ByVal dwMode As Integer)

            Sub Save(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszFileName As String,
              <MarshalAs(UnmanagedType.Bool)> ByVal fRemember As Boolean)

            Sub SaveCompleted(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszFileName As String)

            Sub GetCurFile(ByRef ppszFileName As IntPtr)

        End Interface

        <
          ComImport(),
          InterfaceType(ComInterfaceType.InterfaceIsIUnknown),
          Guid("000214EE-0000-0000-C000-000000000046")
        >
        Private Interface IShellLinkA

            <ObsoleteAttribute>
            Sub GetPath(
          <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszFile As StringBuilder, ByVal cchMaxPath As Integer,
          <Out()> ByRef pfd As WIN32_FIND_DATAA, ByVal fFlags As SLGP_FLAGS)

            Sub GetIDList(ByRef ppidl As IntPtr)

            Sub SetIDList(ByVal pidl As IntPtr)

            Sub GetDescription(
              <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszName As StringBuilder, ByVal cchMaxName As Integer)

            Sub SetDescription(
              <MarshalAs(UnmanagedType.LPStr)> ByVal pszName As String)

            Sub GetWorkingDirectory(
              <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszDir As StringBuilder, ByVal cchMaxPath As Integer)

            Sub SetWorkingDirectory(
              <MarshalAs(UnmanagedType.LPStr)> ByVal pszDir As String)

            Sub GetArguments(
              <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszArgs As StringBuilder, ByVal cchMaxPath As Integer)

            Sub SetArguments(
              <MarshalAs(UnmanagedType.LPStr)> ByVal pszArgs As String)

            Sub GetHotkey(ByRef pwHotkey As Short)

            Sub SetHotkey(ByVal wHotkey As Short)

            Sub GetShowCmd(ByRef piShowCmd As Integer)

            Sub SetShowCmd(ByVal iShowCmd As Integer)

            Sub GetIconLocation(
              <Out(), MarshalAs(UnmanagedType.LPStr)> ByVal pszIconPath As StringBuilder,
              ByVal cchIconPath As Integer, ByRef piIcon As Integer)

            Sub SetIconLocation(
              <MarshalAs(UnmanagedType.LPStr)> ByVal pszIconPath As String, ByVal iIcon As Integer)

            Sub SetRelativePath(
              <MarshalAs(UnmanagedType.LPStr)> ByVal pszPathRel As String, ByVal dwReserved As Integer)

            Sub Resolve(ByVal hwnd As IntPtr, ByVal fFlags As SLR_FLAGS)

            Sub SetPath(
              <MarshalAs(UnmanagedType.LPStr)> ByVal pszFile As String)

        End Interface

        <
          ComImport(),
          InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown),
          Guid("000214F9-0000-0000-C000-000000000046")
        >
        Private Interface IShellLinkW

            <ObsoleteAttribute>
            Sub GetPath(
          <Out(), MarshalAs(UnmanagedType.LPWStr)> ByVal pszFile As StringBuilder, ByVal cchMaxPath As Integer,
          <Out()> ByRef pfd As WIN32_FIND_DATAW, ByVal fFlags As SLGP_FLAGS)

            Sub GetIDList(ByRef ppidl As IntPtr)

            Sub SetIDList(ByVal pidl As IntPtr)

            Sub GetDescription(
              <Out(), MarshalAs(UnmanagedType.LPWStr)> ByVal pszName As StringBuilder, ByVal cchMaxName As Integer)

            Sub SetDescription(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszName As String)

            Sub GetWorkingDirectory(
              <Out(), MarshalAs(UnmanagedType.LPWStr)> ByVal pszDir As StringBuilder, ByVal cchMaxPath As Integer)

            Sub SetWorkingDirectory(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszDir As String)

            Sub GetArguments(
              <Out(), MarshalAs(UnmanagedType.LPWStr)> ByVal pszArgs As StringBuilder, ByVal cchMaxPath As Integer)

            Sub SetArguments(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszArgs As String)

            Sub GetHotkey(ByRef pwHotkey As Short)

            Sub SetHotkey(ByVal wHotkey As Short)

            Sub GetShowCmd(ByRef piShowCmd As Integer)

            Sub SetShowCmd(ByVal iShowCmd As Integer)

            Sub GetIconLocation(
              <Out(), MarshalAs(UnmanagedType.LPWStr)> ByVal pszIconPath As StringBuilder,
              ByVal cchIconPath As Integer, ByRef piIcon As Integer)

            Sub SetIconLocation(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszIconPath As String, ByVal iIcon As Integer)

            Sub SetRelativePath(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszPathRel As String, ByVal dwReserved As Integer)

            Sub Resolve(ByVal hwnd As IntPtr, ByVal fFlags As SLR_FLAGS)

            Sub SetPath(
              <MarshalAs(UnmanagedType.LPWStr)> ByVal pszFile As String)

        End Interface
#End Region

#Region " IShellLinkA "
        Private Const INFOTIPSIZE As Integer = 1024
        Private Const MAX_PATH As Integer = 260
        Private Const SW_SHOWNORMAL As Integer = 1
        Private Const SW_SHOWMINIMIZED As Integer = 2
        Private Const SW_SHOWMAXIMIZED As Integer = 3
        Private Const SW_SHOWMINNOACTIVE As Integer = 7

#If [UNICODE] Then
    Private m_Link As IShellLinkW
#Else
        Private m_Link As IShellLinkA
#End If
        Private m_sPath As String

        ' linkPath: Path to new or existing shortcut file (.lnk).
        Private Sub New(ByVal linkPath As String)

            Dim pf As IPersistFile
            Dim CLSID_ShellLink As Guid = New Guid("00021401-0000-0000-C000-000000000046")
            Dim tShellLink As Type

            ' Workaround for VB.NET compiler bug with ComImport classes
            '#If [UNICODE] Then
            '      m_Link = CType(New ShellLink(), IShellLinkW)
            '#Else
            '      m_Link = CType(New ShellLink(), IShellLinkA)
            '#End If
            tShellLink = Type.GetTypeFromCLSID(CLSID_ShellLink)
#If [UNICODE] Then
      m_Link = CType(Activator.CreateInstance(tShellLink), IShellLinkW)
#Else
            m_Link = CType(Activator.CreateInstance(tShellLink), IShellLinkA)
#End If

            m_sPath = linkPath

            If File.Exists(linkPath) Then
                pf = CType(m_Link, IPersistFile)
                pf.Load(linkPath, 0)
            End If

        End Sub

        Private Sub Dispose() Implements IDisposable.Dispose
            If m_Link Is Nothing Then Exit Sub
            Marshal.ReleaseComObject(m_Link)
            m_Link = Nothing
        End Sub

        ' Gets or sets the argument list of the shortcut.
        Private Property Arguments() As String
            Get
                Dim sb As StringBuilder = New StringBuilder(INFOTIPSIZE)
                m_Link.GetArguments(sb, sb.Capacity)
                Return sb.ToString()
            End Get
            Set(ByVal Value As String)
                m_Link.SetArguments(Value)
            End Set
        End Property

        ' Gets or sets a description of the shortcut.
        Private Property Description() As String
            Get
                Dim sb As StringBuilder = New StringBuilder(INFOTIPSIZE)
                m_Link.GetDescription(sb, sb.Capacity)
                Return sb.ToString()
            End Get
            Set(ByVal Value As String)
                m_Link.SetDescription(Value)
            End Set
        End Property

        ' Gets or sets the working directory (aka start in directory) of the shortcut.
        Private Property WorkingDirectory() As String
            Get
                Dim sb As StringBuilder = New StringBuilder(MAX_PATH)
                m_Link.GetWorkingDirectory(sb, sb.Capacity)
                Return sb.ToString
            End Get
            Set(ByVal Value As String)
                m_Link.SetWorkingDirectory(Value)
            End Set
        End Property

        ' If Path returns an empty string, the shortcut is associated with
        ' a PIDL instead, which can be retrieved with IShellLink.GetIDList().
        ' This is beyond the scope of this wrapper class.
        ' Gets or sets the target path of the shortcut.
        Private Property Path() As String
            <ObsoleteAttribute>
            Get
#If [UNICODE] Then
                    Dim wfd As WIN32_FIND_DATAW
#Else
            Dim wfd As WIN32_FIND_DATAA
#End If

            Dim sb As StringBuilder = New StringBuilder(MAX_PATH)
#Disable Warning BC42108 ' Variable is passed by reference before it has been assigned a value
            m_Link.GetPath(sb, sb.Capacity, wfd, SLGP_FLAGS.SLGP_UNCPRIORITY)
#Enable Warning BC42108 ' Variable is passed by reference before it has been assigned a value
            Return sb.ToString
        End Get
            Set(ByVal Value As String)
                m_Link.SetPath(Value)
            End Set
        End Property

        ' Gets or sets the path of the Icon assigned to the shortcut.
        Private Property IconPath() As String
            Get
                Dim sb As StringBuilder = New StringBuilder(MAX_PATH)
                Dim nIconIdx As Integer
                m_Link.GetIconLocation(sb, sb.Capacity, nIconIdx)
                Return sb.ToString()
            End Get
            Set(ByVal Value As String)
                m_Link.SetIconLocation(Value, IconIndex)
            End Set
        End Property

        ' Gets or sets the index of the Icon assigned to the shortcut.
        ' Set to zero when the IconPath property specifies a .ICO file.
        Private Property IconIndex() As Integer
            Get
                Dim sb As StringBuilder = New StringBuilder(MAX_PATH)
                Dim nIconIdx As Integer
                m_Link.GetIconLocation(sb, sb.Capacity, nIconIdx)
                Return nIconIdx
            End Get
            Set(ByVal Value As Integer)
                m_Link.SetIconLocation(IconPath, Value)
            End Set
        End Property

        ' Retrieves the Icon of the shortcut as it will appear in Explorer.
        ' Use the IconPath and IconIndex properties to change it.
        Private ReadOnly Property Icon() As Icon
            Get
                Dim sb As StringBuilder = New StringBuilder(MAX_PATH)
                Dim nIconIdx As Integer
                Dim hIcon, hInst As IntPtr
                Dim ico, clone As Icon

                m_Link.GetIconLocation(sb, sb.Capacity, nIconIdx)

                hInst = Marshal.GetHINSTANCE(Me.GetType().Module)
                hIcon = Native.ExtractIcon(hInst, sb.ToString(), nIconIdx)
                If hIcon.ToInt32() = 0 Then Return Nothing

                ' Return a cloned Icon, because we have to free the original ourself.
                ico = Icon.FromHandle(hIcon)
                clone = CType(ico.Clone(), Icon)
                ico.Dispose()
                Native.DestroyIcon(hIcon)
                Return clone
            End Get
        End Property

        ' Gets or sets the System.Diagnostics.ProcessWindowStyle value
        ' that decides the initial show state of the shortcut target. Note that
        ' ProcessWindowStyle.Hidden is not a valid property value.
        Private Property WindowStyle() As ProcessWindowStyle
            Get
                Dim nWS As Integer
                m_Link.GetShowCmd(nWS)

                Select Case nWS
                    Case SW_SHOWMINIMIZED, SW_SHOWMINNOACTIVE
                        Return ProcessWindowStyle.Minimized
                    Case SW_SHOWMAXIMIZED
                        Return ProcessWindowStyle.Maximized
                    Case Else
                        Return ProcessWindowStyle.Normal
                End Select
            End Get
            Set(ByVal Value As ProcessWindowStyle)
                Dim nWS As Integer

                Select Case Value
                    Case ProcessWindowStyle.Normal
                        nWS = SW_SHOWNORMAL
                    Case ProcessWindowStyle.Minimized
                        nWS = SW_SHOWMINNOACTIVE
                    Case ProcessWindowStyle.Maximized
                        nWS = SW_SHOWMAXIMIZED
                    Case Else ' ProcessWindowStyle.Hidden
                        Throw New ArgumentException("Unsupported ProcessWindowStyle value.")
                End Select
                m_Link.SetShowCmd(nWS)
            End Set
        End Property

        ' Gets or sets the hotkey for the shortcut.
        Private Property Hotkey() As Keys
            Get
                Dim wHotkey As Short
                Dim dwHotkey As Integer

                m_Link.GetHotkey(wHotkey)

                ' Convert from IShellLink 16-bit format to Keys enumeration 32-bit value
                ' IShellLink: &HMMVK
                ' Keys:  &H00MM00VK        
                '   MM = Modifier (Alt, Control, Shift)
                '   VK = Virtual key code
                dwHotkey = (wHotkey And &HFF00I) * &H100I Or (wHotkey And &HFFI)
                Return CType(dwHotkey, Keys)

            End Get
            Set(ByVal Value As Keys)
                Dim wHotkey As Short

                If (Value And Keys.Modifiers) = 0 Then
                    Throw New ArgumentException("Hotkey must include a modifier key.")
                End If

                ' Convert from Keys enumeration 32-bit value to IShellLink 16-bit format
                ' IShellLink: &HMMVK
                ' Keys:  &H00MM00VK        
                '   MM = Modifier (Alt, Control, Shift)
                '   VK = Virtual key code
                wHotkey = CShort(CInt(Value And Keys.Modifiers) \ &H100) Or CShort(Value And Keys.KeyCode)
                m_Link.SetHotkey(wHotkey)

            End Set
        End Property

        ' Saves the shortcut to disk.
        Private Sub Save()
            Dim pf As IPersistFile = CType(m_Link, IPersistFile)
            pf.Save(m_sPath, True)
        End Sub

        ' Returns a reference to the internal ShellLink object,
        ' which can be used to perform more advanced operations
        ' not supported by this wrapper class, by using the
        ' IShellLink interface directly.
        Private ReadOnly Property ShellLink() As Object
            Get
                Return m_Link
            End Get
        End Property
#End Region

#Region "Native Win32 API functions"
        Private Class Native

            <DllImport("shell32.dll", CharSet:=CharSet.Auto)>
            Public Shared Function ExtractIcon(ByVal hInst As IntPtr, ByVal lpszExeFileName As String, ByVal nIconIndex As Integer) As IntPtr
            End Function

            <DllImport("user32.dll")>
            Public Shared Function DestroyIcon(ByVal hIcon As IntPtr) As Boolean
            End Function

        End Class
#End Region

    End Class

#End Region
