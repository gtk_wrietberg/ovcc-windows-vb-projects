Module Constants
    Public ReadOnly cPATHS_GuestTekProgramFilesFolder As String = GetProgramFilesFolder() & "\GuestTek\OVCCSoftware"

    Public Function GetProgramFilesFolder() As String
        Dim sPath As String = ""

        sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sPath.Equals(String.Empty) Then
            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        Return sPath
    End Function

End Module
