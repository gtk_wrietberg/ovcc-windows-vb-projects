<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.picboxLogo = New System.Windows.Forms.PictureBox()
        Me.tmrStart = New System.Windows.Forms.Timer(Me.components)
        Me.tmrExit = New System.Windows.Forms.Timer(Me.components)
        Me.lblLabel = New System.Windows.Forms.Label()
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picboxLogo
        '
        Me.picboxLogo.BackColor = System.Drawing.Color.Transparent
        Me.picboxLogo.BackgroundImage = Global.OVCCSoftwareOnlyUninstaller.My.Resources.Resources.GuestTek_Header_Logo
        Me.picboxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.picboxLogo.Cursor = System.Windows.Forms.Cursors.Default
        Me.picboxLogo.Location = New System.Drawing.Point(12, 12)
        Me.picboxLogo.Name = "picboxLogo"
        Me.picboxLogo.Size = New System.Drawing.Size(182, 68)
        Me.picboxLogo.TabIndex = 29
        Me.picboxLogo.TabStop = False
        '
        'tmrStart
        '
        Me.tmrStart.Interval = 1000
        '
        'tmrExit
        '
        Me.tmrExit.Interval = 500
        '
        'lblLabel
        '
        Me.lblLabel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.lblLabel.Location = New System.Drawing.Point(12, 83)
        Me.lblLabel.Name = "lblLabel"
        Me.lblLabel.Size = New System.Drawing.Size(182, 21)
        Me.lblLabel.TabIndex = 30
        Me.lblLabel.Text = "initialising uninstaller"
        Me.lblLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(206, 107)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblLabel)
        Me.Controls.Add(Me.picboxLogo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmMain"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picboxLogo As System.Windows.Forms.PictureBox
    Friend WithEvents tmrStart As System.Windows.Forms.Timer
    Friend WithEvents tmrExit As System.Windows.Forms.Timer
    Friend WithEvents lblLabel As Label
End Class
