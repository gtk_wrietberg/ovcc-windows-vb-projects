﻿Module modMain
    Public Sub Main()
        Dim sUsername As String = ""

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.StartsWith("-username=") Then
                sUsername = arg.Replace("-username=", "")
            End If
        Next


        Dim sSid As String = ""
        Dim sProfile As String = ""
        Dim sReturn As String = ""

        If Helpers.WindowsUser.ConvertUsernameToSid(sUsername, sSid) Then
            sReturn = "sid: " & sSid
            If Helpers.WindowsUser.GetProfileDirectory(sUsername, sProfile) Then
                sReturn &= " | profile: " & sProfile
            Else
                sReturn &= " | profile: NO PROFILE!"
            End If
        Else
            sReturn = "sid: USER NOT FOUND!"
        End If


        Console.WriteLine(sReturn)
    End Sub
End Module
