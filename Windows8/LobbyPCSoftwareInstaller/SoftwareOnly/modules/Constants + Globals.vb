Module Constants___Globals
    Public ReadOnly cLOGMEIN_DEPLOY_ID__DEFAULT As String = "01_rt1b7rkbgp37385pqrdp4xtdbbo5z907p625u"
    Public ReadOnly cLOGMEIN_DEPLOY_ID__HILTON As String = "01_ohq4d8ll318nlvencnsq9g6ryb63xumqsv26s"


    'GUI
    Public ReadOnly cGUI_GroupBoxBorder_visible As Boolean = True
    'Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.FromArgb(158, 19, 14))
    Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.Black)
    Public ReadOnly cGUI_GuestTek_images As Boolean = True

    'Prerequisites
    Public ReadOnly cPREREQUISITES_NeededWindowsVersion As Prerequisites.WINDOWS_VERSION = Prerequisites.WINDOWS_VERSION.WINDOWS_7

    'License 
    '62.50.212.70
    '172.18.192.10
    'Private ReadOnly cLicenseValidationURL As String = "http://172.18.192.10/standalone_license/requesthandler.asp"
    'Private ReadOnly cLicenseValidationURL As String = "http://62.50.212.70/standalone_license/requesthandler_withstupiddelay.asp"
    'Public ReadOnly cLicenseValidationURL As String = "http://62.50.212.70/standalone_license/requesthandler_uninstall.asp"
    'Public ReadOnly cLicenseValidationURL As String = "http://eureport.ibahn.com/standalone_license_2.0/requesthandler.asp"
    Public ReadOnly cLicenseValidationURL As String = "http://lobbypclicensing.ibahn.com/requesthandler.asp"
    Public ReadOnly cLicenseIncrementURL As String = "http://lobbypclicensing.ibahn.com/requesthandler_increment.asp"
    Public ReadOnly cLicenseUninstallURL As String = "http://lobbypclicensing.ibahn.com/requesthandler_uninstall.asp"

    Public ReadOnly cLICENSE_KeyLength As Integer = 32
    Public ReadOnly cLICENSE_PermitOfflineLicense As Boolean = False

    'Paths
    Public ReadOnly cPATHS_SiteKioskProgramFilesFolder As String = "C:\Program Files\SiteKiosk"
    Public ReadOnly cPATHS_SiteKioskProgramFilesFolder_x86 As String = "C:\Program Files (x86)\SiteKiosk"

    Public ReadOnly cPATHS_GuestTekProgramFilesFolder As String = GetProgramFilesFolder() & "\GuestTek\OVCCSoftware"
    Public ReadOnly cPATHS_GuestTekProgramFilesResourcesFolder As String = "\installation_files\Resources"
    Public ReadOnly cPATHS_GuestTekProgramFilesToolsFolder As String = "\tools"
    Public ReadOnly cPATHS_GuestTekProgramFilesInternalFolder As String = "\internal"
    Public ReadOnly cPATHS_GuestTekProgramFilesLogsFolder As String = "\logs"
    Public ReadOnly cPATHS_GuestTekProgramFilesInstallationFolder As String = "\installation_files"

    'Debug
    Public ReadOnly cDEBUG_ForcePrerequisiteWindowsVersionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteInternetConnectionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteUserIsAdminError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteUserIsGuestTekUserError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteAlreadyInstalledError As Boolean = False

    'SiteKiosk
    Public ReadOnly cSITEKIOSK_InstallLogPath As String = "C:\"
    Public ReadOnly cSITEKIOSK_InstallLogName As String = "SiteKioskInstall.log"

    Public ReadOnly cSITEKIOSK_UserName As String = "sitekiosk"
    Public ReadOnly cSITEKIOSK_PassWord As String = "Provisi0"

    'admin user
    Public Const cADMIN_Username As String = "guesttek"
    Public Const cADMIN_Password As String = "rancidkipper"

    'Registry
    'Public ReadOnly cREGKEY_LPCSOFTWARE As String = "SOFTWARE\Wow6432Node\iBAHN\LobbyPCSoftwareOnly"
    Public ReadOnly cREGKEY_LPCSOFTWARE As String = "SOFTWARE\GuestTek\OVCCSoftware"


    Public ReadOnly cREGKEY_POLICIES_EXPLORER As String = "Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
    Public ReadOnly cREGKEY_POLICIES_EXPLORER2 As String = "Software\Policies\Microsoft\Windows\Explorer"

    Public ReadOnly cREGKEY_LPCSOFTWARE_UNINSTALL As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\" & Application.ProductName

    Public ReadOnly cREGKEY_RUN As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"


    Public ReadOnly cREGKEYNAME_OVCCSOFTWAREUNINSTALL As String = "OVCCSoftwareOnly"


    'Globals. Me like globals, me are lazy
    Public oLogger As Logger
    Public oSettings As Settings
    Public oLicenseKeySettings As LicenseKeySettings
    Public oRegistryChanger As RegistryChanger

    Public gBusyInstalling As Boolean
    Public gTestMode As Boolean

    Public gAllFilesCopied As Boolean

    Public gHideProgress As Boolean
    Public gSkipSkype As Boolean

    Public gUnattendedLicenseCode As String
    Public gUnattendedInstall As Boolean

    Public gSiteKioskShowProgress As Boolean
    Public gSiteKioskInteractiveInstall As Boolean

    Public gForceHiltonUI As Boolean = False


    Public Function GetProgramFilesFolder() As String
        Dim sPath As String = ""

        sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sPath.Equals(String.Empty) Then
            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        Return sPath
    End Function

End Module
