Module modPreInstallRegistryChanges
    Public Sub preinstallregistrychanges()
        '---------------------------------------------------------
        oLogger.WriteToLog("Storing deployment config in registry")
        oLogger.WriteToLog("blocked drives", , 1)
        Try
            Dim sDrivesToBeBlocked As String
            sDrivesToBeBlocked = GetDrivesToBeBlocked()
            oLogger.WriteToLog("Registry", , 2)
            oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Config\BlockedDrives", , 3)
            oLogger.WriteToLog("value: " & sDrivesToBeBlocked, , 3)

            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "BlockedDrives", sDrivesToBeBlocked)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        oLogger.WriteToLog("license", , 1)
        Try
            oLogger.WriteToLog("Registry", , 2)
            oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Config\License", , 3)
            oLogger.WriteToLog("value: " & gLicenseRegion, , 3)

            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "License", gLicenseRegion)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        oLogger.WriteToLog("region", , 1)
        Try
            oLogger.WriteToLog("Registry", , 2)
            oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Config\Region", , 3)
            oLogger.WriteToLog("value: " & gRegion, , 3)

            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "Region", gRegion)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        '---------------------------------------------------------
        oLogger.WriteToLog("settings", , 1)

        oLogger.WriteToLog("hotel information url", , 2)
        Try
            oLogger.WriteToLog("Registry", , 3)
            oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Config\HotelInformationUrl", , 4)
            oLogger.WriteToLog("value: " & oLicense.HotelInformationUrl, , 4)

            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "HotelInformationUrl", oLicense.HotelInformationUrl)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

        oLogger.WriteToLog("payment enabled", , 2)
        Try
            oLogger.WriteToLog("Registry", , 3)
            oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Config\PaymentEnabled", , 4)
            oLogger.WriteToLog("value: " & oLicense.SiteCash.ToString, , 4)

            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "PaymentEnabled", oLicense.SiteCash.ToString)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

        oLogger.WriteToLog("payment enabled", , 2)
        Try
            oLogger.WriteToLog("Registry", , 3)
            oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Config\PaymentEnabled", , 4)
            oLogger.WriteToLog("value: " & oLicense.SiteCash.ToString, , 4)

            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "PaymentEnabled", oLicense.SiteCash.ToString)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try


        '---------------------------------------------------------
        If gSkipSkype Then
            oLogger.WriteToLog("skip skype", , 1)
            Try
                oLogger.WriteToLog("Registry", , 2)
                oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Config\SkipSkype", , 3)
                oLogger.WriteToLog("value: yes", , 3)

                If Not gTestMode Then
                    Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "SkipSkype", "yes")
                Else
                    oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                End If
            Catch ex As Exception
                oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If

        '---------------------------------------------------------
        oLogger.WriteToLog("Backing up some stuff")

        oLogger.WriteToLog("Computer name", , 1)
        oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Backup\Computer Name", , 2)
        oLogger.WriteToLog("value: " & oComputerName.ComputerName, , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Backup", "Computer Name", oComputerName.ComputerName)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        '---------------------------------------------------------
        oLogger.WriteToLog("Workgroup name", , 1)
        oLogger.WriteToLog("key  : " & cREGKEY_LPCSOFTWARE & "\Backup\Workgroup Name", , 2)
        oLogger.WriteToLog("value: " & oComputerName.Workgroup, , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Backup", "Workgroup Name", oComputerName.Workgroup)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        '---------------------------------------------------------
        oLogger.WriteToLog("Updating default user profile")
        oLogger.WriteToLog("Registry", , 1)
        oLogger.WriteToLog("key  : HKEY_USERS\.DEFAULT\Control Panel\PowerCfg\CurrentPowerPolicy", , 2)
        oLogger.WriteToLog("value: 2", , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue("HKEY_USERS\.DEFAULT\Control Panel\PowerCfg", "CurrentPowerPolicy", "2")
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        '---------------------------------------------------------
        oLogger.WriteToLog("Updating User Agent")
        oLogger.WriteToLog("Registry", , 1)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", , 2)
        oLogger.WriteToLog("value: Mozilla/5.0", , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "", "Mozilla/5.0")
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent\Version", , 2)
        oLogger.WriteToLog("value: MSIE 9.0", , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "Version", "MSIE 9.0")
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        '---------------------------------------------------------
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V HonorAutoRunSetting  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveTypeAutoRun  /T REG_DWORD  /D 0x000000ff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveAutoRun  /T REG_DWORD  /D 0x0003ffff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoAutoRun  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\SYSTEM\CurrentControlSet\Services\Cdrom" /V AutoRun  /T REG_DWORD  /D 0x00000000  /F
        'reg.exe ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping\Autorun.inf" /VE      /T REG_SZ    /D "@SYS:DoesNotExist" /F

        oLogger.WriteToLog("Disabling AutoRun")
        oLogger.WriteToLog("Registry", , 1)
        oLogger.WriteToLog("key  : " & cREGKEY_POLICIES_EXPLORER & "!HonorAutoRunSetting", , 2)
        oLogger.WriteToLog("value: 1", , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "HonorAutoRunSetting", 1)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oLogger.WriteToLog("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoDriveTypeAutoRun", , 2)
        oLogger.WriteToLog("value: 255", , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoDriveTypeAutoRun", 255)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oLogger.WriteToLog("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoDriveAutoRun", , 2)
        oLogger.WriteToLog("value: 262143", , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoDriveAutoRun", 262143)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oLogger.WriteToLog("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoAutoRun", , 2)
        oLogger.WriteToLog("value: 1", , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoAutoRun", 1)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        oLogger.WriteToLog("key  : " & cREGKEY_POLICIES_EXPLORER2 & "!NoAutoplayfornonVolume", , 2)
        oLogger.WriteToLog("value: 1", , 2)
        Try
            If Not gTestMode Then
                Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER2, "NoAutoplayfornonVolume", 1)
            Else
                oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        '---------------------------------------------------------
        oLogger.WriteToLog("Storing main installer version number")
        oLogger.WriteToLog("Registry", , 1)
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(cREGKEY_LPCSOFTWARE & "\Version", , 2)
        oLogger.WriteToLog("Installer = '" & myBuildInfo.FileVersion & "'", , 3)
        If Not gTestMode Then
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Version", "Installer", myBuildInfo.FileVersion)
        Else
            oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
        End If
    End Sub
End Module
