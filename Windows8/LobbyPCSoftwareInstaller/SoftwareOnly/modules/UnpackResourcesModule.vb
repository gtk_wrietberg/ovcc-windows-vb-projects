Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sSourceFolder As String
        Dim sDestinationFolder As String

        oLogger.WriteToLog("Copying resources")

        sSourceFolder = cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder
        sDestinationFolder = sSourceFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If


        oLogger.WriteToLog("Altiris package", , 1)
        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.exe"

        oLogger.WriteToLog("LogMeIn package", , 1)
        oSettings.Path_LogMeIn = sDestinationFolder & "\LogMeIn.package.exe"


        oLogger.WriteToLog("SK Updater package", , 1)
        oSettings.Path_SiteKioskUpdate = sDestinationFolder & "\SiteKioskUpdater.package.exe"

        oLogger.WriteToLog("OVCCAgent package", , 1)
        oSettings.Path_LobbyPCAgent = sDestinationFolder & "\OVCCAgent.package.exe"

        oLogger.WriteToLog("LobbyPCWatchdog package", , 1)
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\OVCCWatchdog.package.exe"

        oLogger.WriteToLog("SiteKiosk 9", , 1)
        oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk9.exe"

        oLogger.WriteToLog("SiteKiosk Security Updater", , 1)
        oSettings.Path_SystemSecurityUpdater = sDestinationFolder & "\SystemSecurityUpdater.exe"

        oLogger.WriteToLog("LogoutCleanupHelper", , 1)
        oSettings.Path_LogoutCleanupHelper = sDestinationFolder & "\LogoutCleanupHelper.package.exe"




        sDestinationFolder = cPATHS_GuestTekProgramFilesFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("OVCCStart", , 1)
        _CopyFile("OVCCStart.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_AutoStart = sDestinationFolder & "\OVCCStart.exe"


        oLogger.WriteToLog("OVCCSoftwareOnlyUninstaller", , 1)
        _CopyFile("OVCCSoftwareOnlyUninstaller.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("OVCCSoftwareOnlyUninstaller_.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_Uninstaller = sDestinationFolder & "\OVCCSoftwareOnlyUninstaller.exe"

        oLogger.WriteToLog("Icon", , 1)
        _CopyFile("GuestTek.ico", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_Icon = sDestinationFolder & "\GuestTek.ico"



        sDestinationFolder = cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesToolsFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If


        oLogger.WriteToLog("OVCCInformationUpdater", , 1)
        _CopyFile("OVCCInformationUpdater.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_HotelPageUpdater = sDestinationFolder & "\OVCCInformationUpdater.exe"




        sDestinationFolder = cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInternalFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        'oLogger.WriteToLog("PcHasRebooted", , 1)
        '_CopyFile("PcHasRebooted.exe", sSourceFolder, sDestinationFolder, 2)
        'oSettings.Path_PcHasRebooted = sDestinationFolder & "\PcHasRebooted.exe"

        oLogger.WriteToLog("SkCfgUpdaterSiteCash", , 1)
        _CopyFile("SkCfgUpdater_SiteCash.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("SkCfgUpdater_SiteCash.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_SkCfgUpdaterSiteCash = sDestinationFolder & "\SkCfgUpdater_SiteCash.exe"
    End Sub

    Private Sub _CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, ByVal iLogLevel As Integer)
        oLogger.WriteToLog("copying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)

        oLogger.WriteToLog("retrying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
    End Sub
End Module
