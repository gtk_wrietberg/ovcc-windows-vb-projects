Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCCSoftwareOnly")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("GuestTek")> 
<Assembly: AssemblyProduct("OVCCSoftwareOnly")> 
<Assembly: AssemblyCopyright("Copyright ©  2016")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("dcb6cbf6-2843-4f51-82d4-ffe76d5a987c")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

'<Assembly: AssemblyVersion("2.0.501.1719")> 
'<Assembly: AssemblyFileVersion("2.0.501.1719")> 

<Assembly: AssemblyVersion("3.0.23352")> 

<assembly: AssemblyFileVersion("0.0.23352.1308")>
<assembly: AssemblyInformationalVersion("0.0.23352.1308")>