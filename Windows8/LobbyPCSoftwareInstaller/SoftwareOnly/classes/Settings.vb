Public Class Settings
    Private mInstaller_LogMeIn_Path As String
    Private mInstaller_Altiris_Path As String
    Private mInstaller_SiteKioskUpdate_Path As String
    Private mInstaller_SiteKiosk_Path As String
    Private mInstaller_SystemSecurityUpdater_Path As String
    Private mInstaller_AutoStart_Path As String
    Private mInstaller_HotelPageUpdater_Path As String
    Private mInstaller_LobbyPCAgent_Path As String
    Private mInstaller_LobbyPCWatchdog_Path As String
    Private mInstaller_SkCfgUpdaterSiteCash_Path As String
    Private mInstaller_LogoutCleanupHelper_Path As String
    Private mInstaller_Uninstaller_Path As String
    Private mInstaller_Icon_Path As String

    Public Sub New()

    End Sub

#Region "Properties"
    Public Property Path_LogMeIn() As String
        Get
            Return mInstaller_LogMeIn_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LogMeIn_Path = value
        End Set
    End Property

    Public Property Path_Altiris() As String
        Get
            Return mInstaller_Altiris_Path
        End Get
        Set(ByVal value As String)
            mInstaller_Altiris_Path = value
        End Set
    End Property

    Public Property Path_SiteKioskUpdate() As String
        Get
            Return mInstaller_SiteKioskUpdate_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SiteKioskUpdate_Path = value
        End Set
    End Property

    Public Property Path_SiteKiosk() As String
        Get
            Return mInstaller_SiteKiosk_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SiteKiosk_Path = value
        End Set
    End Property

    Public Property Path_SystemSecurityUpdater() As String
        Get
            Return mInstaller_SystemSecurityUpdater_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SystemSecurityUpdater_Path = value
        End Set
    End Property

    Public Property Path_AutoStart() As String
        Get
            Return mInstaller_AutoStart_Path
        End Get
        Set(ByVal value As String)
            mInstaller_AutoStart_Path = value
        End Set
    End Property

    Public Property Path_HotelPageUpdater() As String
        Get
            Return mInstaller_HotelPageUpdater_Path
        End Get
        Set(ByVal value As String)
            mInstaller_HotelPageUpdater_Path = value
        End Set
    End Property

    Public Property Path_LobbyPCAgent() As String
        Get
            Return mInstaller_LobbyPCAgent_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LobbyPCAgent_Path = value
        End Set
    End Property

    Public Property Path_LobbyPCWatchdog() As String
        Get
            Return mInstaller_LobbyPCWatchdog_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LobbyPCWatchdog_Path = value
        End Set
    End Property

    Public Property Path_LogoutCleanupHelper As String
        Get
            Return mInstaller_LogoutCleanupHelper_Path
        End Get
        Set(value As String)
            mInstaller_LogoutCleanupHelper_Path = value
        End Set
    End Property

    Public Property Path_SkCfgUpdaterSiteCash() As String
        Get
            Return mInstaller_SkCfgUpdaterSiteCash_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SkCfgUpdaterSiteCash_Path = value
        End Set
    End Property

    Public Property Path_Uninstaller() As String
        Get
            Return mInstaller_Uninstaller_Path
        End Get
        Set(ByVal value As String)
            mInstaller_Uninstaller_Path = value
        End Set
    End Property

    Public Property Path_Icon() As String
        Get
            Return mInstaller_Icon_Path
        End Get
        Set(ByVal value As String)
            mInstaller_Icon_Path = value
        End Set
    End Property

#End Region
End Class
