'Imports Microsoft.WindowsAPICodePack
'Imports Microsoft.WindowsAPICodePack.Taskbar
Imports System.Threading

Public Class frmMain
    Private iApplicationInstallationCount As Integer = 0
    Private iApplicationInstallationCountMax As Integer = 13

    Private oPrerequisites As Prerequisites

    Private WithEvents oProcess As New ProcessRunner
    Private WithEvents oLicense As LicenseKey

    Private oComputerName As ComputerName
    Private oLocalization As Localization

    Private installThread As Thread

    Private Panels() As Panel
    Private iActiveGroupBox As Integer

    Private bAdminCreatedOk As Boolean = False

    Private sLastError As String = ""

    Private Enum GroupBoxes As Integer
        Prerequisites = 0
        StartScreen = 1
        TermsAndConditions = 2
        LicenseKey = 3
        LicenseKeyValidation = 4
        PreInstallWarning = 5
        Installation = 6
        DoneAndRestart = 7
        ErrorDuringInstallation = 8
    End Enum

    Private Enum InstallationOrder As Integer
        IncrementLicense = 1
        CreateAdminAccount
        LogMeIn
        Altiris
        SiteKiosk
        CopySiteKioskInstallLog
        CheckSiteKioskInstallation
        SiteKioskUpdate
        LogoutCleanupHelper
        SystemSecurityUpdater
        SkCfgUpdaterSiteCash
        HotelPageUpdate
        OVCCAgent
        OVCCWatchdog
        Shortcuts
        PostDeployment
        DONE
    End Enum

    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)
    Delegate Sub LogToProcessStepTextBoxCallback(ByVal [text] As String)

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Panels = New Panel() {Me.pnlPrerequisites, Me.pnlStartScreen, Me.pnlTermsAndConditions, Me.pnlLicenseKey, Me.pnlLicenseKeyValidation, Me.pnlPreInstallWarning, Me.pnlInstallation, Me.pnlDoneAndRestart, Me.pnlErrorOccurred}
    End Sub

#Region " ClientAreaMove Handling "
    'Private Const WM_NCHITTEST As Integer = &H84
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    'Private Const HTCLIENT As Integer = &H1
    Private Const HTCAPTION As Integer = &H2
    '    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
    '        Select Case m.Msg
    '            Case WM_NCHITTEST
    '                MyBase.WndProc(m)
    '    'If m.Result = HTCLIENT Then m.Result = HTCAPTION
    '                If m.Result.ToInt32 = HTCLIENT Then m.Result = IntPtr.op_Explicit(HTCAPTION) 'Try this in VS.NET 2002/2003 if the latter line of code doesn't do it... thx to Suhas for the tip.
    '            Case Else
    '    'Make sure you pass unhandled messages back to the default message handler.
    '                MyBase.WndProc(m)
    '        End Select
    '    End Sub
#End Region

#Region "Form Events"
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Don't close the form when we're still busy
        e.Cancel = gBusyInstalling
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActivatePrevInstance(Me.Text)

        oPrerequisites = New Prerequisites
        oComputerName = New ComputerName
        oSettings = New Settings
        oLicense = New LicenseKey
        oLogger = New Logger
        oRegistryChanger = New RegistryChanger

        If InStr(oComputerName.ComputerName.ToLower, "rietberg".ToLower) > 0 _
        Or InStr(oComputerName.ComputerName.ToLower, "superlekkerding".ToLower) > 0 _
        Or InStr(oComputerName.ComputerName.ToLower, "wrdev".ToLower) > 0 Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If
        End If

        oPrerequisites.CheckWindowsVersion(cPREREQUISITES_NeededWindowsVersion, True)
        oPrerequisites.CheckInternetConnection()
        oPrerequisites.IsUserAdmin()
        oPrerequisites.IsUserGuestTekUser(cADMIN_Username)
        oPrerequisites.IsAlreadyInstalled()
        If cDEBUG_ForcePrerequisiteWindowsVersionError Or cDEBUG_ForcePrerequisiteInternetConnectionError Or cDEBUG_ForcePrerequisiteUserIsAdminError Or cDEBUG_ForcePrerequisiteAlreadyInstalledError Then
            oPrerequisites.ForcePrerequisiteErrors(cDEBUG_ForcePrerequisiteWindowsVersionError, cDEBUG_ForcePrerequisiteInternetConnectionError, cDEBUG_ForcePrerequisiteUserIsAdminError, cDEBUG_ForcePrerequisiteUserIsGuestTekUserError, cDEBUG_ForcePrerequisiteAlreadyInstalledError)
        End If


        lblWindowsVersion.Text = oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion)

        oLocalization = New Localization
        oLocalization.LoadLanguageFile()

        PopulateLanguageComboBox()
        ReloadLocalization()

        LogToProcessStepTextBox("")

        oLicense.LicenseCodeLength = cLICENSE_KeyLength
        oLicense.PermitTestLicense = cLICENSE_PermitOfflineLicense

        txtLicenseCode.Text = ""

        'GUI
        Me.Width = 634
        Me.Height = 503

        Me.StartPosition = FormStartPosition.CenterScreen
        Me.WindowState = FormWindowState.Normal

        Me.Left = (My.Computer.Screen.Bounds.Width - Me.Width) / 2
        Me.Top = (My.Computer.Screen.Bounds.Height - Me.Height) / 2

        'Me.BackColor = Color.White
        'Me.BackgroundImageLayout = ImageLayout.Tile


        Me.pnlBackgroundBorder.Visible = cGUI_GroupBoxBorder_visible

        btnExit.Top = btnContinue.Top
        btnExit.Left = btnContinue.Left

        btnBack.Top = btnCancel.Top
        btnBack.Left = btnCancel.Left


        If Not IO.Directory.Exists(cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder) Then
            Try
                IO.Directory.CreateDirectory(cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder)
            Catch ex As Exception

            End Try
        End If

        oLogger.LogFilePath = cPATHS_GuestTekProgramFilesFolder & "\"

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = "v" & myBuildInfo.FileMajorPart & "." & myBuildInfo.FileMinorPart

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        If gTestMode Then
            oLogger.WriteToLog("Running in test mode!!!", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        End If


        gBusyInstalling = False

        'Terms and Conditions
        Dim tmpResources As New MyResources
        txtTC.Text = tmpResources.ToString("TermsAndConditions")

        txtTC.ReadOnly = True
        ParseTermsAndConditions(txtTC)

        'Install count max
        iApplicationInstallationCountMax = InstallationOrder.DONE


        'Panels
        Dim i As Integer
        For i = 0 To Panels.Length - 1
            'Panels(i).Top = 84
            'Panels(i).Left = 16
            Panels(i).Top = 76
            Panels(i).Left = 5

            Panels(i).Visible = False
        Next

        '---------------------------------------------------------
        gSkipSkype = False
        gUnattendedLicenseCode = String.Empty
        gUnattendedInstall = False

        gSiteKioskShowProgress = False
        gSiteKioskInteractiveInstall = False


        oLogger.WriteToLog("loading command line arguments", , 1)
        For Each arg As String In Environment.GetCommandLineArgs()
            oLogger.WriteToLog("found: " & arg, , 2)

            If InStr(arg, "-unattended=") > 0 Then
                gUnattendedLicenseCode = arg.Replace("-unattended=", "")
                oLogger.WriteToLog("unattended: " & gUnattendedLicenseCode, , 3)
                gUnattendedInstall = True
            End If
            If arg = "-hideprogress" Then
                gHideProgress = True

                Me.Opacity = 0.0

                oLogger.WriteToLog("hide progress: " & gHideProgress.ToString, , 3)
            End If
            If arg = "-skipskype" Then
                gSkipSkype = True

                oLogger.WriteToLog("skip skype: " & gSkipSkype.ToString, , 3)
            End If
            If arg = "-sitekioskshow" Then
                gSiteKioskShowProgress = True

                oLogger.WriteToLog("show sitekiosk install progress", , 3)
            End If
            If arg = "-sitekioskinteractive" Then
                gSiteKioskInteractiveInstall = True

                oLogger.WriteToLog("sitekiosk install is interactive!", , 3)
            End If

        Next

        If gUnattendedInstall Then
            txtLicenseCode.Text = gUnattendedLicenseCode

            iActiveGroupBox = GroupBoxes.LicenseKeyValidation - 1
        Else
            iActiveGroupBox = -1
        End If


        NextGroupBox()
    End Sub
#End Region

#Region "Private Functions"
#Region "Localization"
    Private Sub PopulateLanguageComboBox()
        '-----------------------------------
        cmbBoxLanguages.Items.Clear()
        Try

            Dim sLanguages() As String
            Dim i As Integer

            sLanguages = oLocalization.GetLanguageNames

            For i = 0 To sLanguages.Length - 1
                If Not sLanguages(i) Is Nothing Then
                    cmbBoxLanguages.Items.Add(sLanguages(i))
                End If
            Next

            cmbBoxLanguages.SelectedIndex = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ReloadLocalization()
        lblTitle.Text = oLocalization.CurrentLanguage.Title

        btnBack.Text = oLocalization.CurrentLanguage.ButtonBack
        btnCancel.Text = oLocalization.CurrentLanguage.ButtonCancel
        btnContinue.Text = oLocalization.CurrentLanguage.ButtonNext
        btnExit.Text = oLocalization.CurrentLanguage.ButtonExit

        lblSelectLanguage.Text = oLocalization.CurrentLanguage.WelcomeSelectLanguage

        lblPrerequisitesRestart.Text = oLocalization.CurrentLanguage.PrerequisitesErrorRestartText

        pnlTermsAndConditions.Text = oLocalization.CurrentLanguage.TermsAndConditionsTitle
        lblTCText.Text = oLocalization.CurrentLanguage.TermsAndConditionsText
        chkTCAgree.Text = oLocalization.CurrentLanguage.TermsAndConditionsCheckBox

        pnlLicenseKey.Text = oLocalization.CurrentLanguage.LicenseValidationTitle
        lblLicenseValidation.Text = oLocalization.CurrentLanguage.LicenseValidationText.Replace("%%LICENSE_CODE_LENGTH%%", cLICENSE_KeyLength.ToString)

        grpbxLicense.Text = oLocalization.CurrentLanguage.LicenseValidationTitle2

        pnlLicenseKeyValidation.Text = oLocalization.CurrentLanguage.LicenseValidationTitle
        lblLicenseValidationCopy.Text = oLocalization.CurrentLanguage.LicenseValidationText.Replace("%%LICENSE_CODE_LENGTH%%", cLICENSE_KeyLength.ToString)
        grpbxLicenseCopy.Text = oLocalization.CurrentLanguage.LicenseValidationTitle2
        lblLicenseProgress.Text = oLocalization.CurrentLanguage.LicenseValidationProgressText

        lblInstallationContinueText.Text = oLocalization.CurrentLanguage.InstallationContinueText

        pnlInstallation.Text = oLocalization.CurrentLanguage.InstallationProgressTitle
        lblInstallationProgressText.Text = oLocalization.CurrentLanguage.InstallationProgressText

        pnlDoneAndRestart.Text = oLocalization.CurrentLanguage.DoneTitle
        lblReboot.Text = oLocalization.CurrentLanguage.DoneRestartText
        lblReboot2.Text = oLocalization.CurrentLanguage.DoneRestartText2
        radRestartNow.Text = oLocalization.CurrentLanguage.DoneRestartNowText
        radRestartLater.Text = oLocalization.CurrentLanguage.DoneRestartLaterText
    End Sub
#End Region

#Region "License code validation"
    Private Sub CheckLicenseCode()
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog("Validating license code")

        oLicense.LicenseCode = txtLicenseCode.Text
        oLicense.ApplicationVersion = myBuildInfo.FileMajorPart.ToString & "." & myBuildInfo.FileMinorPart.ToString

        oLogger.WriteToLog("license code: " & oLicense.LicenseCode, , 1)

        oLicense.CheckLicense()
    End Sub

    Private Sub IncrementLicenseCode()
        oLogger.WriteToLog("Incrementing license code")

        oLicense.IncrementLicense()
    End Sub
#End Region

#Region "CheckPrerequisites"
    Private Sub CheckPrerequisites()
        Dim sError As String

        If oPrerequisites.CheckComplete Then
            If oPrerequisites.AllIsOk Then
                NextGroupBox()
            Else
                oLogger.WriteToLog("Prerequisites error", Logger.MESSAGE_TYPE.LOG_ERROR)

                sError = ""
                If Not oPrerequisites.InternetConnectivity Then
                    oLogger.WriteToLog("No internet connection", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorInternetConnection
                End If
                If Not oPrerequisites.CorrectWindowsVersion Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If

                    If oPrerequisites.HigherWindowsVersionsAreAccepted Then
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " < " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= oLocalization.CurrentLanguage.PrerequisitesErrorMinimumWindowsVersion
                    Else
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " != " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= oLocalization.CurrentLanguage.PrerequisitesErrorWindowsVersion
                    End If

                    sError = sError.Replace("%%NEEDED_WINDOWS_VERSION%%", oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion))
                    sError = sError.Replace("%%RUNNING_WINDOWS_VERSION%%", oPrerequisites.WindowsVersion)
                End If
                If Not oPrerequisites.UserIsAdmin Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("No admin privileges", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorAdminRights
                End If
                If oPrerequisites.IsUserGuestTekUser(cADMIN_Username) Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("Running as '" & cADMIN_Username & "' user", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= "You can't run the installer as the '" & cADMIN_Username & "' user. Please choose a different administrator account."
                End If
                If oPrerequisites.IsAlreadyInstalled Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("Already installed", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorAlreadyInstalled
                End If

                lblPrerequisitesError.Text = sError

                If gTestMode Then
                    UpdateButtons(0, 0, 3, 0)
                Else
                    UpdateButtons(0, 0, 0, 3)
                End If
            End If
        Else
            lblPrerequisitesError.Text = "Unknown error 31. Please contact the helpdesk."

            UpdateButtons(0, 0, 0, 3)
        End If
    End Sub
#End Region

#Region "Groupbox handling"
    Private Sub GotoErrorGroupbox(Optional sErrorMessage As String = "")
        sLastError = sErrorMessage

        GotoGroupBox(GroupBoxes.ErrorDuringInstallation)
    End Sub

    Private Sub GotoGroupBox(ByVal iGroupBoxIndex As Integer)
        iActiveGroupBox = iGroupBoxIndex
        UpdateGroupBox()
    End Sub

    Private Sub NextGroupBox()
        iActiveGroupBox += 1
        UpdateGroupBox()
    End Sub

    Private Sub PreviousGroupBox()
        iActiveGroupBox -= 1
        'Skip these group box if we are going BACK
        'Windows version and internet connection error dialog
        If iActiveGroupBox = GroupBoxes.StartScreen Then
            iActiveGroupBox = -1
        End If
        'License validation dialog
        If iActiveGroupBox = GroupBoxes.LicenseKeyValidation Then
            iActiveGroupBox = GroupBoxes.LicenseKey
        End If
        UpdateGroupBox()
    End Sub

    Private Sub UpdateGroupBox()
        If iActiveGroupBox >= Panels.Length Then
            iActiveGroupBox = Panels.Length - 1
        End If
        'We handle the case of <0 in the following select case statement
        'If iActiveGroupBox < 0 Then
        '   iActiveGroupBox = 0
        'End If

        oLogger.WriteToLog("Groupbox / action: " & iActiveGroupBox.ToString, Logger.MESSAGE_TYPE.LOG_DEBUG)

        Select Case iActiveGroupBox
            Case GroupBoxes.Prerequisites
                UpdateButtons(0, 0, 0, 0)

                CheckPrerequisites()
            Case GroupBoxes.StartScreen
                UpdateButtons(0, 3, 3, 0)

                Me.ControlBox = True
            Case GroupBoxes.TermsAndConditions
                UpdateButtons(0, 0, 2, 0)
                chkTCAgree.Checked = False
            Case GroupBoxes.LicenseKey
                UpdateButtons(3, 0, 2, 0)

                Me.ControlBox = True

                txtLicenseCode.SelectAll()
            Case GroupBoxes.LicenseKeyValidation
                UpdateButtons(2, 0, 2, 0)

                Me.ControlBox = False

                txtLicenseCodeCopy.Text = txtLicenseCode.Text
                tmrLicenseValidationDelay.Enabled = True
            Case GroupBoxes.PreInstallWarning
                UpdateButtons(0, 3, 3, 0)

                Me.ControlBox = True

                PreInstallSteps()
            Case GroupBoxes.Installation
                UpdateButtons(0, 0, 0, 0)

                Me.ControlBox = False

                gBusyInstalling = True

                ProgressBarMarqueeStart()

                StartDeployment()
            Case GroupBoxes.DoneAndRestart
                UpdateButtons(0, 0, 0, 3)
                radRestartNow.Checked = True
                radRestartLater.Checked = False

                If gUnattendedInstall Then
                    ApplicationIsExited(0)
                End If
            Case GroupBoxes.ErrorDuringInstallation
                UpdateButtons(0, 0, 0, 3)

                lblErrorCode.Text = "Error code:  # " & iApplicationInstallationCount.ToString
                If sLastError <> "" Then
                    lblErrorDescription.Text = sLastError
                Else
                    lblErrorDescription.Visible = False
                End If

                If gUnattendedInstall Then
                    ApplicationIsExited(-2)
                End If
            Case Else
                Application.Exit()
        End Select

        If iActiveGroupBox >= 0 And iActiveGroupBox < Panels.Length Then
            Panels(iActiveGroupBox).Visible = True
            pnlBackgroundBorder.Top = Panels(iActiveGroupBox).Top - 1
            pnlBackgroundBorder.Left = Panels(iActiveGroupBox).Left - 1
            pnlBackgroundBorder.Width = Panels(iActiveGroupBox).Width + 2
            pnlBackgroundBorder.Height = Panels(iActiveGroupBox).Height + 2
            pnlBackgroundBorder.Refresh()
        End If

        Dim i As Integer
        For i = 0 To Panels.Length - 1
            If i <> iActiveGroupBox Then
                Panels(i).Visible = False
            End If
        Next
    End Sub
#End Region

#Region "Thread Safe stuff"
    Private Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text] & vbCrLf)
        End If
    End Sub

    Private Sub LogToProcessStepTextBox(ByVal [text] As String)
        If Me.lblInstallationProgressStepText.InvokeRequired Then
            Dim d As New LogToProcessStepTextBoxCallback(AddressOf LogToProcessStepTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblInstallationProgressStepText.Text = [text]
        End If
    End Sub

    'Private Sub UupdateLicenseError(ByVal [text] As String)
    '    lblLicenseError.Text = [text]
    'End Sub

    'Private Sub SshowHideLicenseError(ByVal [bool] As Boolean)
    '    grpboxLicenseError.Visible = [bool]
    'End Sub
#End Region

#Region "Button Handling"
    Private Sub UpdateButtons(ByVal iBack As Integer, ByVal iCancel As Integer, ByVal iContinue As Integer, ByVal iExit As Integer)
        btnBack.Enabled = iBack And 1
        btnBack.Visible = iBack And 2

        btnCancel.Enabled = iCancel And 1
        btnCancel.Visible = iCancel And 2

        btnContinue.Enabled = iContinue And 1
        btnContinue.Visible = iContinue And 2

        btnExit.Enabled = iExit And 1
        btnExit.Visible = iExit And 2
    End Sub
#End Region

#Region "Deployment and Installers"
#Region "Deployment"
    Private Sub PreInstallSteps()
        If gUnattendedInstall Then
            NextGroupBox()
        End If
    End Sub

    Private Sub StartDeployment()
        oLicenseKeySettings = New LicenseKeySettings

        oRegistryChanger.Start()

        '---------------------------------------------------------
        oLogger.WriteToLog("Saving license key to registry")
        Try
            oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE, "LicenseKey", oLicense.LicenseCode)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\" & cREGKEY_LPCSOFTWARE, "LicenseKey", oLicense.LicenseCode)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        '---------------------------------------------------------
        oLogger.WriteToLog("Storing deployment config in registry")

        '###########
        Dim sDrivesToBeBlocked As String
        sDrivesToBeBlocked = GetDrivesToBeBlocked()

        oLogger.WriteToLog("blocked drives", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "BlockedDrives", sDrivesToBeBlocked)

        '---------------------------------------------------------
        oLogger.WriteToLog("settings", , 1)

        '###########
        oLogger.WriteToLog("region", , 1)
        oLicenseKeySettings.ServerRegion = oLicense.ServerRegion
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "Region", oLicense.ServerRegion)

        oLogger.WriteToLog("english flag", , 1)
        If oLicense.CountryName.ToLower = "us" Then
            oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "EnglishFlag", "us")
        Else
            oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "EnglishFlag", "gb")
        End If


        '###########
        oLogger.WriteToLog("hotel information url", , 2)
        oLicenseKeySettings.HotelInformationUrl = oLicense.HotelInformationUrl
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "HotelInformationUrl", oLicense.HotelInformationUrl)

        '###########
        oLogger.WriteToLog("sitekiosk license", , 2)
        oLicenseKeySettings.SiteKioskLicenseName = oLicense.SiteKioskLicenseName
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseName", oLicense.SiteKioskLicenseName)

        oLicenseKeySettings.SiteKioskLicenseLicensee = oLicense.SiteKioskLicenseLicensee
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseLicensee", oLicense.SiteKioskLicenseLicensee)

        oLicenseKeySettings.SiteKioskLicenseSignature = oLicense.SiteKioskLicenseSignature
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseSignature", oLicense.SiteKioskLicenseSignature)


        '###########
        oLogger.WriteToLog("prepaid enabled", , 2)
        oLicenseKeySettings.PrepaidEnabled = oLicense.PrepaidEnabled
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "PrepaidEnabled", oLicense.PrepaidEnabled.ToString)

        '###########
        oLogger.WriteToLog("currency name", , 2)
        oLicenseKeySettings.CurrencyName = oLicense.CurrencyName
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CurrencyName", oLicense.CurrencyName)

        '###########
        oLogger.WriteToLog("currency code", , 2)
        oLicenseKeySettings.CurrencyCode = oLicense.CurrencyCode
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CurrencyCode", oLicense.CurrencyCode)

        '###########
        oLogger.WriteToLog("hourly rate", , 2)
        oLicenseKeySettings.HourlyRate = oLicense.HourlyRate
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "HourlyRate", oLicense.HourlyRate.ToString)

        '###########
        oLogger.WriteToLog("credit card enabled", , 2)
        oLicenseKeySettings.CreditCardEnabled = oLicense.CreditCardEnabled
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardEnabled", oLicense.CreditCardEnabled.ToString)

        '###########
        oLogger.WriteToLog("credit card vendor", , 2)
        oLicenseKeySettings.CreditCardVendorName = oLicense.CreditCardVendorName
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardVendorName", oLicense.CreditCardVendorName)

        oLicenseKeySettings.CreditCardVendorUser = oLicense.CreditCardVendorUser
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardVendorUser", oLicense.CreditCardVendorUser)

        oLicenseKeySettings.CreditCardVendorPass = oLicense.CreditCardVendorPass
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardVendorPass", oLicense.CreditCardVendorPass)

        oLicenseKeySettings.CreditCardVendorSize = oLicense.CreditCardVendorSize
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardVendorSize", oLicense.CreditCardVendorSize)

        '###########
        oLogger.WriteToLog("credit card amount - minimum", , 2)
        oLicenseKeySettings.CreditCardMin = oLicense.CreditCardMin
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardAmountMinimum", oLicense.CreditCardMin.ToString)

        '###########
        oLogger.WriteToLog("credit card amount - maximum", , 2)
        oLicenseKeySettings.CreditCardMax = oLicense.CreditCardMax
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardAmountMaximum", oLicense.CreditCardMax.ToString)

        '###########
        oLogger.WriteToLog("credit card amount - increment", , 2)
        oLicenseKeySettings.CreditCardStep = oLicense.CreditCardStep
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardAmountIncrement", oLicense.CreditCardStep.ToString)

        '###########
        oLogger.WriteToLog("computer name - new", , 2)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "NewComputerName", oLicense.MachineName.ToString)

        '###########
        oLogger.WriteToLog("workgroup - new", , 2)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "NewWorkgroup", oLicense.Workgroup.ToString)

        '###########
        oLogger.WriteToLog("hilton detection", , 2)
        If oLicense.Workgroup.ToLower().Contains("hilton") Then
            oLogger.WriteToLog("found 'hilton' in the workgroup name", , 3)
            gForceHiltonUI = True
        Else
            oLogger.WriteToLog("did not find 'hilton' in the workgroup name", , 3)
            gForceHiltonUI = False
        End If


        '---------------------------------------------------------
        If gSkipSkype Then
            oLogger.WriteToLog("skip skype", , 1)
            oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "SkipSkype", "yes")
        End If

        '---------------------------------------------------------
        oLogger.WriteToLog("Backing up some stuff")

        oLogger.WriteToLog("Computer name", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Backup", "Computer Name", oComputerName.ComputerName)

        '###########
        oLogger.WriteToLog("Workgroup name", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Backup", "Workgroup Name", oComputerName.Workgroup)

        '---------------------------------------------------------
        oLogger.WriteToLog("Updating default user profile")
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.Users, ".DEFAULT\Control Panel\PowerCfg", "CurrentPowerPolicy", 2)

        ''---------------------------------------------------------
        'oLogger.WriteToLog("Updating User Agent")
        'oLogger.WriteToLog("User Agent", , 1)
        'oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "", "Mozilla/5.0")

        'oLogger.WriteToLog("Version", , 1)
        'oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "Version", "MSIE 9.0")

        '---------------------------------------------------------
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V HonorAutoRunSetting  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveTypeAutoRun  /T REG_DWORD  /D 0x000000ff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveAutoRun  /T REG_DWORD  /D 0x0003ffff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoAutoRun  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\SYSTEM\CurrentControlSet\Services\Cdrom" /V AutoRun  /T REG_DWORD  /D 0x00000000  /F
        'reg.exe ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping\Autorun.inf" /VE      /T REG_SZ    /D "@SYS:DoesNotExist" /F

        oLogger.WriteToLog("Disabling AutoRun")
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER, "HonorAutoRunSetting", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER, "NoDriveTypeAutoRun", 255)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER, "NoDriveAutoRun", 262143)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER, "NoAutoRun", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER2, "NoAutoplayfornonVolume", 1)

        '---------------------------------------------------------
        oLogger.WriteToLog("Storing main installer version number")
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Version", "Installer", myBuildInfo.FileVersion)

        '---------------------------------------------------------
        LogToProcessTextBox("Updating workgroup and computer name")
        oLogger.WriteToLog("Updating workgroup and computer name")
        oLogger.WriteToLog("workgroup   : " & oLicense.Workgroup, , 1)
        oLogger.WriteToLog("computername: " & oLicense.MachineName, , 1)
        If Not gTestMode Then
            oComputerName.Workgroup = oLicense.Workgroup
            oComputerName.ComputerName = oLicense.MachineName

            oLogger.WriteToLog("ok", , 1)
        Else
            oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        End If

        '---------------------------------------------------------
        UnpackResourcesInBackground()
    End Sub

    Private Sub PostDeployment()
        oLogger.WriteToLog("Post deployment steps")

        oLogger.WriteToLog("Set installed to 'yes'", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE, "Installed", "yes")

        oLogger.WriteToLog("Set restarted to 'no'", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE, "Restarted", "no")

        'oLogger.WriteToLog("Set PcHadRebooted to RunOnce", 1)
        'oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce", "PcHasRebooted", oSettings.Path_PcHasRebooted, True)

        'oLogger.WriteToLog("Set TaskbarAutohide to Run", 1)
        'oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "TaskbarAutohide", oSettings.Path_TaskbarAutohide, True)


        oLogger.WriteToLog("Add to Programs list", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "DisplayIcon", oSettings.Path_Uninstaller)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "DisplayName", Application.ProductName)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "DisplayVersion", Application.ProductVersion)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "Publisher", Application.CompanyName)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "EstimatedSize", 200000)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "UninstallString", oSettings.Path_Uninstaller)


        'Copy C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp
        'oLogger.WriteToLog("copy c:\Program Files\SiteKiosk\Bitmaps\iBAHN.bmp to C:\ProgramData\Microsoft\User Account Pictures\iBAHN.bmp", , 1)
        'Try
        '    IO.File.Copy("c:\Program Files\SiteKiosk\Bitmaps\iBAHN.bmp", "C:\ProgramData\Microsoft\User Account Pictures\iBAHN.bmp")
        'Catch ex As Exception
        '    oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        '    oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        'End Try

        'If Not IO.File.Exists("C:\ProgramData\Microsoft\User Account Pictures\iBAHN.bmp") Then
        '    oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        '    oLogger.WriteToLog("Destination file does not exist!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        'Else
        '    oLogger.WriteToLog("ok", , 2)
        'End If


        'Copy some log files
        'oLogger.WriteToLog("copy log files", , 1)
        'oLogger.WriteToLog("copy " & cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder & "\unpacked\iBAHNUpdate\SiteKiosk7Installer.log to " & cPATHS_GuestTekProgramFilesFolder & "\SiteKiosk7Installer.log", , 2)
        'Try
        '    IO.File.Copy( _
        '        cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder & "\unpacked\iBAHNUpdate\SiteKiosk7Installer.log", _
        '        cPATHS_GuestTekProgramFilesFolder & "\SiteKiosk7Installer.log")
        'Catch ex As Exception
        '    oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        '    oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        'End Try

        'oLogger.WriteToLog("copy " & cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInstallationFolder & "\hotelpage.updater.log to " & cPATHS_GuestTekProgramFilesFolder & "\hotelpage.updater.log", , 2)
        'Try
        '    IO.File.Copy( _
        '        cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInstallationFolder & "\hotelpage.updater.log", _
        '        cPATHS_GuestTekProgramFilesFolder & "\hotelpage.updater.log")
        'Catch ex As Exception
        '    oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        '    oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        'End Try

        'oLogger.WriteToLog("copy " & cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInstallationFolder & "\SkCfgUpdater.log to " & cPATHS_GuestTekProgramFilesFolder & "\SkCfgUpdater.log", , 2)
        'Try
        '    IO.File.Copy( _
        '        cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInstallationFolder & "\SkCfgUpdater.log", _
        '        cPATHS_GuestTekProgramFilesFolder & "\SkCfgUpdater.log")
        'Catch ex As Exception
        '    oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        '    oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        'End Try


        'Clean up temp install files
        oLogger.WriteToLog("clean up temp install files", , 1)
        Try
            IO.Directory.Delete(cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInstallationFolder, True)
            oLogger.WriteToLog("ok ", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        oRegistryChanger.Done()
    End Sub

    Private Sub NextInstallationStep()
        'tmrInstallerWait.Enabled = False

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub UnpackResourcesInBackground()
        LogToProcessTextBox("Unpacking resources")
        oLogger.WriteToLog("Unpacking resources")

        UpdateInstallationProgressCount()

        tmrUnpackWait.Enabled = True

        Me.installThread = New Thread(New ThreadStart(AddressOf UnpackResourcesModule.UnpackResources))
        Me.installThread.Start()
    End Sub

    Private Sub InstallationController()
        'tmrInstallerWait.Enabled = False
        'oProcess = New ProcessRunner

        'If iApplicationInstallationCount < iApplicationInstallationCountMax Then
        '    tmrInstallerWait.Enabled = True
        'End If

        'If gTestMode Then
        '    iApplicationInstallationCount = iApplicationInstallationCountMax
        'End If

        'oLogger.WriteToLog("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

        oLogger.WriteToLog("Installation step: " & CStr(iApplicationInstallationCount) & "/" & iApplicationInstallationCountMax, Logger.MESSAGE_TYPE.LOG_DEBUG, 0)


        LogToProcessStepTextBox(InstallationCountToString)



        If iApplicationInstallationCount < iApplicationInstallationCountMax Then
            tmrInstallerWait.Enabled = True
        End If


        Select Case iApplicationInstallationCount
            Case InstallationOrder.IncrementLicense
                IncrementLicenseCode()
            Case InstallationOrder.CreateAdminAccount
                _CreateAdminAccount()
            Case InstallationOrder.LogMeIn
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLogMeIn))
                Me.installThread.Start()
            Case InstallationOrder.Altiris
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallAltiris))
                Me.installThread.Start()
            Case InstallationOrder.OVCCAgent
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLobbyPCAgent))
                Me.installThread.Start()
            Case InstallationOrder.OVCCWatchdog
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLobbyPCWatchdog))
                Me.installThread.Start()
            Case InstallationOrder.SiteKiosk
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKiosk))
                Me.installThread.Start()
            Case InstallationOrder.CopySiteKioskInstallLog
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._CopySiteKioskInstallLog))
                Me.installThread.Start()
            Case InstallationOrder.CheckSiteKioskInstallation
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._CheckSiteKioskInstallation))
                Me.installThread.Start()
            Case InstallationOrder.SiteKioskUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKioskUpdate))
                Me.installThread.Start()
            Case InstallationOrder.LogoutCleanupHelper
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLogoutCleanupHelper))
                Me.installThread.Start()
            Case InstallationOrder.SkCfgUpdaterSiteCash
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSkCfgUpdaterSiteCash))
                Me.installThread.Start()
            Case InstallationOrder.HotelPageUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallHotelPageUpdate))
                Me.installThread.Start()
            Case InstallationOrder.Shortcuts
                _CreateShortcuts()
            Case InstallationOrder.SystemSecurityUpdater
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._SystemSecurityUpdater))
                Me.installThread.Start()
            Case InstallationOrder.PostDeployment
                PostDeployment()

                oProcess.ProcessDone()
            Case Else
                oLogger.WriteToLog("DONE", , 0)

                gBusyInstalling = False

                NextGroupBox()
        End Select
    End Sub

    Private Function InstallationCountToString() As String
        Select Case iApplicationInstallationCount
            Case InstallationOrder.Altiris
                Return "Installing remote administration application"
            Case InstallationOrder.CreateAdminAccount
                Return "Creating GuestTek administrator account"
            Case InstallationOrder.HotelPageUpdate
                Return "Updating information for user interface"
            Case InstallationOrder.IncrementLicense
                Return "Validation of license"
            Case InstallationOrder.LogMeIn
                Return "Installing remote management application"
            Case InstallationOrder.OVCCAgent
                Return "Installing heartbeat service"
            Case InstallationOrder.OVCCWatchdog
                Return "Installing security watchdog service"
            Case InstallationOrder.PostDeployment
                Return "Installation cleanup"
            Case InstallationOrder.Shortcuts
                Return "Creating shortcuts"
            Case InstallationOrder.SiteKiosk
                Return "Installing SiteKiosk"
            Case InstallationOrder.CopySiteKioskInstallLog
                Return "Copying SiteKiosk install log"
            Case InstallationOrder.CheckSiteKioskInstallation
                Return "Checking SiteKiosk installation"
            Case InstallationOrder.SiteKioskUpdate
                Return "Installing GuestTek files"
            Case InstallationOrder.LogoutCleanupHelper
                Return "Installing additional logout actions"
            Case InstallationOrder.SkCfgUpdaterSiteCash
                Return "Updating payment settings"
            Case InstallationOrder.SystemSecurityUpdater
                Return "Updating security settings"
            Case Else
                Return ""
        End Select
    End Function
#End Region

#Region "Installers"
#Region "admin user creation"
    Private Sub _CreateAdminAccount()
        bgworkerAdminUserCreation.RunWorkerAsync()
    End Sub

    Private Sub bgworkerAdminUserCreation_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworkerAdminUserCreation.DoWork
        bAdminCreatedOk = False

        oLogger.WriteToLog("Create admin account", , 1)

        Dim sCurrentUser As String = Environment.UserName.ToLower

        oLogger.WriteToLog("user logged in: " & sCurrentUser, , 2)
        If sCurrentUser = cADMIN_Username Then
            oLogger.WriteToLog("and that's a problem.", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("I can't change stuff for '" & cADMIN_Username & "' when (s)he's logged in!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Exit Sub
        Else
            oLogger.WriteToLog("ok", , 3)
        End If

        Dim sGroupName_Administrators As String, sGroupName_Users As String
        oLogger.WriteToLog("Get group names", , 2)

        sGroupName_Administrators = cWindowsUser.GetAdministratorsGroupName
        oLogger.WriteToLog("Administrators = '" & sGroupName_Administrators & "'", , 3)

        sGroupName_Users = cWindowsUser.GetUsersGroupName
        oLogger.WriteToLog("Users = '" & sGroupName_Users & "'", , 3)


        'Check if user exists
        Dim sRet As String = ""
        oLogger.WriteToLog("Does user '" & cADMIN_Username & "' exist?", , 2)
        If Not cWindowsUser.DoesUserExist(cADMIN_Username, sRet) Then
            oLogger.WriteToLog("nope ('" & sRet & "')", , 3)
            oLogger.WriteToLog("creating", , 3)

            sRet = cWindowsUser.AddAdminUser(cADMIN_Username, "GuestTek admin user", cADMIN_Password)
            If sRet = "ok" Then
                oLogger.WriteToLog("ok", , 4)

                bAdminCreatedOk = True
            Else
                oLogger.WriteToLog("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLog(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                bAdminCreatedOk = False
            End If
        Else
            oLogger.WriteToLog("yep ('" & sRet & "')", , 3)

            oLogger.WriteToLog("But is he an admin?", , 2)
            If cWindowsUser.IsUserAdmin(cADMIN_Username, sRet) Then
                oLogger.WriteToLog("yes! ('" & sRet & "')", , 3)

                bAdminCreatedOk = True
            Else
                oLogger.WriteToLog("nope ('" & sRet & "')", , 3)
                oLogger.WriteToLog("adding to admin group", , 2)

                sRet = cWindowsUser.PromoteUserToAdmin(cADMIN_Username)
                If sRet = "ok" Then
                    oLogger.WriteToLog("ok", , 3)

                    bAdminCreatedOk = True
                Else
                    oLogger.WriteToLog("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    oLogger.WriteToLog(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                    bAdminCreatedOk = False
                End If
            End If

            oLogger.WriteToLog("updating the password", , 2)
            oLogger.WriteToLog("setting password to default", , 3)
            sRet = cWindowsUser.ChangePassword(cADMIN_Username, cADMIN_Password)
            If sRet = "ok" Then
                oLogger.WriteToLog("ok", , 3)
            Else
                oLogger.WriteToLog("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                bAdminCreatedOk = False
            End If
        End If

        oLogger.WriteToLog("remove password expiry", , 3)
        sRet = cWindowsUser.RemovePasswordExpiry(cADMIN_Username)
        If sRet = "ok" Then
            oLogger.WriteToLog("ok", , 4)
        Else
            oLogger.WriteToLog("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
        End If
    End Sub

    Private Sub bgworkerAdminUserCreation_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgworkerAdminUserCreation.RunWorkerCompleted
        If bAdminCreatedOk Then
            NextInstallationStep()
        Else
            'Don't continue with a missing or crippled admin account!
            GotoErrorGroupbox("An error occurred while creating the GuestTek administrator account")
        End If

    End Sub
#End Region

    Private Sub _InstallLogMeIn()
        Dim sParams As String = ""

        sParams &= "-spdeployid="

        If oLicense.Workgroup.StartsWith("HILTON") Then
            sParams &= cLOGMEIN_DEPLOY_ID__HILTON
        Else
            sParams &= cLOGMEIN_DEPLOY_ID__DEFAULT
        End If

        ' LogMeIn.deploy.exe -spdeployid=01_rt1b7rkbgp37385pqrdp4xtdbbo5z907p625u

        oLogger.WriteToLog("Installing remote access application")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LogMeIn, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LogMeIn
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "LogMeIn_Installer"
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLobbyPCAgent()
        oLogger.WriteToLog("Installing iBAHN service")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LobbyPCAgent, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCAgent
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "OVCCAgent_installer"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLobbyPCWatchdog()
        oLogger.WriteToLog("Installing iBAHN security service")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LobbyPCWatchdog, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCWatchdog
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "OVCCWatchdog_installer"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKioskUpdate()
        Dim sParams As String = ""

        If gForceHiltonUI Then
            sParams &= "-spforcehilton"
        End If

        oLogger.WriteToLog("Installing iBAHN files")
        oLogger.WriteToLog("Path   : " & oSettings.Path_SiteKioskUpdate, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKioskUpdate
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "SiteKioskUpdater"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLogoutCleanupHelper()
        Dim sParams As String = ""

        oLogger.WriteToLog("Installing LogoutCleanupHelper")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LogoutCleanupHelper, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LogoutCleanupHelper
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "LogoutCleanupHelperInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub


    Private Sub _SystemSecurityUpdater()
        oLogger.WriteToLog("Installing SiteKiosk System Security Update")
        oLogger.WriteToLog("Path   : " & oSettings.Path_SystemSecurityUpdater, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SystemSecurityUpdater
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub


    Private Sub _InstallSkCfgUpdaterSiteCash()
        Dim lParams As New List(Of String), sParams As String = ""

        If oLicenseKeySettings.PrepaidEnabled Then
            lParams.Add("--enable-pp")
        Else
            lParams.Add("--disable-pp")
        End If

        lParams.Add("--currency:" & oLicenseKeySettings.CurrencyCode)
        lParams.Add("--hourly-rate:" & oLicenseKeySettings.HourlyRate.ToString)

        If oLicenseKeySettings.CreditCardEnabled Then
            lParams.Add("--enable-cc")
        Else
            lParams.Add("--disable-cc")
        End If

        lParams.Add("--min-amount-cc:" & oLicenseKeySettings.CreditCardMin.ToString)
        lParams.Add("--max-amount-cc:" & oLicenseKeySettings.CreditCardMax.ToString)
        lParams.Add("--increment-amount-cc:" & oLicenseKeySettings.CreditCardStep.ToString)
        lParams.Add("--region-cc:" & oLicense.CreditCardVendorName)


        sParams = String.Join(" ", lParams)


        oLogger.WriteToLog("Installing SkCfgUpdaterSiteCash")
        oLogger.WriteToLog("Path   : " & oSettings.Path_SkCfgUpdaterSiteCash, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 30", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SkCfgUpdaterSiteCash
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 30
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallHotelPageUpdate()
        Dim sParams As String = ""

        sParams &= "--quiet--hotel-info-url:" & oLicenseKeySettings.HotelInformationUrl

        oLogger.WriteToLog("Installing HotelPageUpdate")
        oLogger.WriteToLog("Path   : " & oSettings.Path_HotelPageUpdater, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 30", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_HotelPageUpdater
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 30
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _CreateShortcuts()
        oLogger.WriteToLog("Creating shortcut for OVCCStarter", , 1)
        AppShortcut.CreateOnDesktop(oSettings.Path_AutoStart, "Start OVCC")
        AppShortcut.CreateInStartMenu(oSettings.Path_AutoStart, "Start OVCC")

        oLogger.WriteToLog("Creating shortcut for OVCCInformationUpdater", , 1)
        AppShortcut.CreateOnDesktop(oSettings.Path_HotelPageUpdater, "OVCC - InformationUpdater")

        oLogger.WriteToLog("Creating Add/Remove entry", , 1)
        oLogger.WriteToLog(_CreateUninstallEntryInAddRemove(), , 2)

        NextInstallationStep()
    End Sub

    Private Function _CreateUninstallEntryInAddRemove() As String
        Try
            Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

            Dim ApplicationName As String = Application.ProductName
            Dim ApplicationVersion As String = myBuildInfo.FileVersion
            Dim ApplicationIconPath As String = oSettings.Path_Icon
            Dim ApplicationPublisher As String = Application.CompanyName
            Dim ApplicationUnInstallPath As String = oSettings.Path_Uninstaller
            Dim ApplicationInstallDirectory As String = cPATHS_GuestTekProgramFilesFolder

            With My.Computer.Registry.LocalMachine.OpenSubKey("Software\Microsoft\Windows\CurrentVersion\Uninstall", True)
                Dim AppKey As Microsoft.Win32.RegistryKey = .CreateSubKey(cREGKEYNAME_OVCCSOFTWAREUNINSTALL)

                AppKey.SetValue("DisplayName", ApplicationName, Microsoft.Win32.RegistryValueKind.String)
                AppKey.SetValue("DisplayVersion", ApplicationVersion, Microsoft.Win32.RegistryValueKind.String)
                AppKey.SetValue("DisplayIcon", ApplicationIconPath, Microsoft.Win32.RegistryValueKind.String)
                AppKey.SetValue("Publisher", ApplicationPublisher, Microsoft.Win32.RegistryValueKind.String)
                AppKey.SetValue("UninstallString", ApplicationUnInstallPath, Microsoft.Win32.RegistryValueKind.String)
                AppKey.SetValue("UninstallPath", ApplicationUnInstallPath, Microsoft.Win32.RegistryValueKind.String)
                AppKey.SetValue("InstallLocation", ApplicationInstallDirectory, Microsoft.Win32.RegistryValueKind.String)
            End With

            Return "ok"
        Catch ex As Exception
            Return "Error: " & ex.Message
        End Try
    End Function


    Private Sub _InstallAltiris()
        oLogger.WriteToLog("Installing remote administration application")
        oLogger.WriteToLog("Path   : " & oSettings.Path_Altiris, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_Altiris
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "AeXNSCHTTP"
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKiosk()
        Dim sParams As String = ""

        'standard params for SK
        sParams = "/S /V""/qn DEFAULTPASSWD=" & cSITEKIOSK_PassWord & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"

        If gSiteKioskShowProgress Then
            sParams = "/V""/qb DEFAULTPASSWD=" & cSITEKIOSK_PassWord & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"
        End If
        If gSiteKioskInteractiveInstall Then
            sParams = "/V""DEFAULTPASSWD=" & cSITEKIOSK_PassWord & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"
        End If

        'sParams = "/S /V""/qn DEFAULTPASSWD=" & cSITEKIOSK_PassWord & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"

        oLogger.WriteToLog("Installing kiosk software")
        oLogger.WriteToLog("Path   : " & oSettings.Path_SiteKiosk, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 1800", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKiosk
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1800
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _CopySiteKioskInstallLog()
        oLogger.WriteToLog("Copying Sitekiosk install log")

        If CopyFile(cSITEKIOSK_InstallLogName, cSITEKIOSK_InstallLogPath, cPATHS_GuestTekProgramFilesFolder, 1) Then
            oLogger.WriteToLog("ok", , 1)
        Else
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("but we should still be able to find it here: ", , 2)
            oLogger.WriteToLog(cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName, , 3)
        End If

        NextInstallationStep()
    End Sub

    Private Sub _CheckSiteKioskInstallation()
        oLogger.WriteToLog("Checking Sitekiosk install")
        oLogger.WriteToLog("counting some folders", , 1)

        Dim iCount As Integer = 0, sPath As String = ""

        If IO.Directory.Exists(cPATHS_SiteKioskProgramFilesFolder_x86) Then
            sPath = cPATHS_SiteKioskProgramFilesFolder_x86
        Else
            sPath = cPATHS_SiteKioskProgramFilesFolder
        End If


        iCount = FileCounter.GetFolderCount(sPath)

        oLogger.WriteToLog("there are " & iCount & " folders in '" & sPath & "'", , 2)

        If iCount < 666 Then
            'uh oh, seems sitekiosk is incomplete
            oLogger.WriteToLog("SiteKiosk did not install properly!", , 3)

            GotoErrorGroupbox("SiteKiosk did not install properly, installation cancelled!")

            Exit Sub
        Else
            oLogger.WriteToLog("looks ok", , 3)
        End If


        'check registry
        oLogger.WriteToLog("checking version", , 1)

        Dim sVersion As String = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk", "Build", "0.0")
        Dim sFirstChar As String = sVersion.Substring(0, 1)
        Dim iVersion As Integer = -1

        If (Integer.TryParse(sFirstChar, iVersion)) Then
            oLogger.WriteToLog("version " & iVersion.ToString, , 2)

            If iVersion >= 9 Then
                oLogger.WriteToLog("looks ok", , 4)
            Else
                oLogger.WriteToLog("uh oh, wrong version!!!", , 4)

                GotoErrorGroupbox("Wrong version of SiteKiosk found, installation cancelled!")

                Exit Sub
            End If
        End If


        oLogger.WriteToLog("ok", , 1)

        NextInstallationStep()
    End Sub


#End Region
#End Region

#Region "Progress bars"
    Private Sub UpdateInstallationProgressBar()
        If iApplicationInstallationCount <= iApplicationInstallationCountMax Then
            UpdateInstallationProgressCount()
            progressInstallation.Value = (100 / iApplicationInstallationCountMax) * iApplicationInstallationCount

            'Try
            '    TaskbarManager.Instance.SetProgressValue(progressInstallation.Value, progressInstallation.Maximum)
            'Catch ex As Exception

            'End Try
        End If
    End Sub

    Private Sub UpdateInstallationProgressCount()
        lblInstallationProgressStep.Text = (iApplicationInstallationCount + 1).ToString & " / " & (iApplicationInstallationCountMax + 1).ToString
        lblInstallationProgressStep.Visible = True
    End Sub


    Private Sub ProgressBarMarqueeStart()
        progressMarquee.Style = ProgressBarStyle.Marquee
        progressMarquee.MarqueeAnimationSpeed = 100
        progressMarquee.Value = 0
    End Sub

    Private Sub ProgressBarMarqueeStop()
        progressMarquee.Style = ProgressBarStyle.Blocks
        progressMarquee.MarqueeAnimationSpeed = 0
        progressMarquee.Value = 0
    End Sub
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationIsCanceled()
        Application.Exit()
        'gBusyInstalling = False

        'MsgBox("Installation cancelled by user", MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation, Application.ProductName)
    End Sub

    Private Sub ApplicationIsExited(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("Exiting application")

        If Not gUnattendedInstall Then
            If radRestartNow.Checked And iActiveGroupBox = GroupBoxes.DoneAndRestart Then
                oLogger.WriteToLog("restarting", , 1)

                If gTestMode Then
                    oLogger.WriteToLog("test mode, so not restarting after all", , 2)
                    MsgBox("I would have restarted here!", MsgBoxStyle.OkOnly Or MsgBoxStyle.Information, Application.ProductName)
                Else
                    oLogger.WriteToLog("bye bye", , 2)
                    WindowsController.ExitWindows(RestartOptions.Reboot, True)
                End If
            End If
        End If

        oLogger.WriteToLog("exit code: " & iExitCode.ToString, , 1)
        Environment.ExitCode = iExitCode
        Application.Exit()
    End Sub
#End Region
#End Region

#Region "Events"
#Region "DragWindow events"
    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picboxLogo.MouseDown
        Me.picboxLogo.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub lblTitle_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseDown
        Me.lblTitle.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        LogToProcessTextBox(vbTab & "Done - Time elapsed: " & TimeElapsed.ToString)
        oLogger.WriteToLog("done", , 1)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        LogToProcessTextBox(vbTab & "Process failed")
        oLogger.WriteToLog("failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        LogToProcessTextBox(vbTab & "Process started")
        oLogger.WriteToLog("started", , 1)
        oLogger.WriteToLog("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess.TimeOut
        LogToProcessTextBox(vbTab & "Process timed out")
        oLogger.WriteToLog("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
#End Region

#Region "Button Events"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ApplicationIsCanceled()
    End Sub

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        NextGroupBox()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        PreviousGroupBox()
    End Sub

    Private Sub btnClearLicense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearLicense.Click
        txtLicenseCode.Text = ""
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        ApplicationIsExited()
    End Sub
#End Region

#Region "Timer Events"
    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            oProcess = New ProcessRunner

            If oProcess.ErrorsOccurred And Not gTestMode Then
                gBusyInstalling = False

                If sLastError = "" Then
                    GotoErrorGroupbox()
                Else
                    'InstallationCountToString
                    GotoErrorGroupbox("Error! '" & sLastError & "'")
                End If
            Else
                iApplicationInstallationCount += 1
                InstallationController()

                UpdateInstallationProgressBar()
            End If
        End If
    End Sub

    Private Sub tmrUnpackWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrUnpackWait.Tick
        If Not Me.installThread.ThreadState = ThreadState.Running And Not Me.installThread.ThreadState = ThreadState.Unstarted Then
            tmrUnpackWait.Enabled = False

            iApplicationInstallationCount = 1
            UpdateInstallationProgressBar()

            InstallationController()
        End If
    End Sub

    Private Sub tmrLicenseValidationDelay_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrLicenseValidationDelay.Tick
        tmrLicenseValidationDelay.Enabled = False

        CheckLicenseCode()
    End Sub
#End Region

#Region "CheckBox Events"
    Private Sub chkTCAgree_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTCAgree.CheckedChanged
        btnContinue.Enabled = chkTCAgree.Checked
    End Sub
#End Region

#Region "RadioButton Events"
    Private Sub radRestartNow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radRestartNow.CheckedChanged
        'UpdateButtons(0, 0, 0, 3)
    End Sub

    Private Sub radRestartLater_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radRestartLater.CheckedChanged
        'UpdateButtons(0, 0, 0, 2)
    End Sub
#End Region

#Region "ComboBox Events"
    Private Sub cmbBoxLanguages_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBoxLanguages.SelectedIndexChanged
        oLocalization.SetCurrentLanguage = cmbBoxLanguages.SelectedIndex + 1

        ReloadLocalization()
    End Sub
#End Region

#Region "Textbox Events"
    Private Sub txtLicenseCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLicenseCode.TextChanged
        grpboxLicenseError.Visible = False

        If txtLicenseCode.Text.Length = cLICENSE_KeyLength Then
            UpdateButtons(3, 0, 3, 0)
        Else
            UpdateButtons(3, 0, 2, 0)
        End If
    End Sub
#End Region

#Region "Paint Events"
    Private Sub pnlBackgroundBorder_Paint(ByVal sender As System.Object, ByVal pe As System.Windows.Forms.PaintEventArgs) Handles pnlBackgroundBorder.Paint
        If cGUI_GroupBoxBorder_visible Then
            pe.Graphics.DrawRectangle(cGUI_GroupBoxBorder_color, pe.ClipRectangle.Left, pe.ClipRectangle.Top, pe.ClipRectangle.Width - 1, pe.ClipRectangle.Height - 1)
        End If
    End Sub
#End Region

#Region "PicBox Events"
    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ApplicationIsCanceled()
    End Sub
#End Region

#Region "License Events"
    Private Sub oLicense_LicenseIncremented() Handles oLicense.LicenseIncremented
        oLogger.WriteToLog("done", , 2)

        iApplicationInstallationCount += 1
        UpdateInstallationProgressBar()

        InstallationController()
    End Sub

    Private Sub oLicense_LicenseNotIncremented(ByVal ReturnCode As Integer, ByVal ReturnMessage As String) Handles oLicense.LicenseNotIncremented
        oLogger.WriteToLog("invalid", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        oLogger.WriteToLog(ReturnCode, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        oLogger.WriteToLog(ReturnMessage, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

        iApplicationInstallationCount += 1
        UpdateInstallationProgressBar()

        InstallationController()
    End Sub

    Private Sub onLicenseIsInvalid(ByVal ReturnCode As Integer, ByVal ReturnMessage As String) Handles oLicense.LicenseIsInvalid
        Dim sError As String = ""

        oLogger.WriteToLog("invalid", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        oLogger.WriteToLog(ReturnCode, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        oLogger.WriteToLog(ReturnMessage, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        If gTestMode Then
            oLogger.WriteToLog(oLicense.RawServerResponse, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        sError = oLocalization.CurrentLanguage.LicenseValidationProgressError
        Select Case ReturnCode
            Case 1
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage1
            Case 2
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage2
            Case 3
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage3
            Case -1
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage4
            Case -2
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage5
            Case -12
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage6
            Case Else
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessageUnknown
        End Select

        lblLicenseError.Text = sError
        grpboxLicenseError.Visible = True
        txtLicenseCode.SelectAll()

        If gUnattendedInstall Then
            ApplicationIsExited(-1)
        Else
            PreviousGroupBox()
        End If
    End Sub

    Private Sub onLicenseValid() Handles oLicense.LicenseIsValid
        oLogger.WriteToLog("valid", , 2)

        If gTestMode Then
            oLogger.WriteToLog(oLicense.RawServerResponse, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        'Go to next step
        NextGroupBox()
    End Sub


#End Region
#End Region
End Class
