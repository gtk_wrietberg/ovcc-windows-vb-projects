<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.grpboxStep1 = New System.Windows.Forms.GroupBox
        Me.lblTitle = New System.Windows.Forms.Label
        Me.lblVersion = New System.Windows.Forms.Label
        Me.btnNext = New System.Windows.Forms.Button
        Me.btnPrev = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.grpboxStep2 = New System.Windows.Forms.GroupBox
        Me.chkTCAgree = New System.Windows.Forms.CheckBox
        Me.txtTC = New System.Windows.Forms.TextBox
        Me.grpboxStep3 = New System.Windows.Forms.GroupBox
        Me.txtDomainOld = New System.Windows.Forms.TextBox
        Me.txtComputerNameOld = New System.Windows.Forms.TextBox
        Me.txtDomainNew = New System.Windows.Forms.TextBox
        Me.txtComputerNameNew = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.grpboxStep4 = New System.Windows.Forms.GroupBox
        Me.txtProgress = New System.Windows.Forms.TextBox
        Me.progressInstallation = New System.Windows.Forms.ProgressBar
        Me.btnFocusCatcher = New System.Windows.Forms.Button
        Me.grpboxStep5 = New System.Windows.Forms.GroupBox
        Me.tmrInstallerWait = New System.Windows.Forms.Timer(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpboxStep1.SuspendLayout()
        Me.grpboxStep2.SuspendLayout()
        Me.grpboxStep3.SuspendLayout()
        Me.grpboxStep4.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Image = Global.LobbyPCStandaloneInstallation.My.Resources.Resources.TopBarLogo
        Me.PictureBox1.Location = New System.Drawing.Point(-1, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(670, 84)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'grpboxStep1
        '
        Me.grpboxStep1.Controls.Add(Me.TextBox1)
        Me.grpboxStep1.Location = New System.Drawing.Point(12, 90)
        Me.grpboxStep1.Name = "grpboxStep1"
        Me.grpboxStep1.Size = New System.Drawing.Size(610, 350)
        Me.grpboxStep1.TabIndex = 2
        Me.grpboxStep1.TabStop = False
        Me.grpboxStep1.Text = "Step 1 - Welcome"
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.White
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(140, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(482, 65)
        Me.lblTitle.TabIndex = 3
        Me.lblTitle.Text = "LobbyPC Standalone"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.White
        Me.lblVersion.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.Location = New System.Drawing.Point(462, 62)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(174, 21)
        Me.lblVersion.TabIndex = 4
        Me.lblVersion.Text = "v1.2.3.4.5"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'btnNext
        '
        Me.btnNext.BackColor = System.Drawing.Color.Gainsboro
        Me.btnNext.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNext.Location = New System.Drawing.Point(535, 445)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(74, 26)
        Me.btnNext.TabIndex = 0
        Me.btnNext.Text = "Next >"
        Me.btnNext.UseVisualStyleBackColor = False
        '
        'btnPrev
        '
        Me.btnPrev.BackColor = System.Drawing.Color.Gainsboro
        Me.btnPrev.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrev.Location = New System.Drawing.Point(455, 445)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(74, 26)
        Me.btnPrev.TabIndex = 5
        Me.btnPrev.Text = "< Prev"
        Me.btnPrev.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Gainsboro
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(34, 446)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(97, 26)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'grpboxStep2
        '
        Me.grpboxStep2.Controls.Add(Me.chkTCAgree)
        Me.grpboxStep2.Controls.Add(Me.txtTC)
        Me.grpboxStep2.Location = New System.Drawing.Point(12, 513)
        Me.grpboxStep2.Name = "grpboxStep2"
        Me.grpboxStep2.Size = New System.Drawing.Size(610, 350)
        Me.grpboxStep2.TabIndex = 3
        Me.grpboxStep2.TabStop = False
        Me.grpboxStep2.Text = "Step 2 - Terms and Conditions"
        '
        'chkTCAgree
        '
        Me.chkTCAgree.Location = New System.Drawing.Point(6, 282)
        Me.chkTCAgree.Name = "chkTCAgree"
        Me.chkTCAgree.Size = New System.Drawing.Size(580, 21)
        Me.chkTCAgree.TabIndex = 2
        Me.chkTCAgree.Text = "I have read and agree with the Terms and Conditions"
        Me.chkTCAgree.UseVisualStyleBackColor = True
        '
        'txtTC
        '
        Me.txtTC.BackColor = System.Drawing.Color.White
        Me.txtTC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTC.Location = New System.Drawing.Point(6, 19)
        Me.txtTC.MaxLength = 150000
        Me.txtTC.Multiline = True
        Me.txtTC.Name = "txtTC"
        Me.txtTC.ReadOnly = True
        Me.txtTC.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTC.Size = New System.Drawing.Size(591, 257)
        Me.txtTC.TabIndex = 0
        Me.txtTC.Text = resources.GetString("txtTC.Text")
        '
        'grpboxStep3
        '
        Me.grpboxStep3.Controls.Add(Me.txtDomainOld)
        Me.grpboxStep3.Controls.Add(Me.txtComputerNameOld)
        Me.grpboxStep3.Controls.Add(Me.txtDomainNew)
        Me.grpboxStep3.Controls.Add(Me.txtComputerNameNew)
        Me.grpboxStep3.Controls.Add(Me.Button1)
        Me.grpboxStep3.Location = New System.Drawing.Point(638, 90)
        Me.grpboxStep3.Name = "grpboxStep3"
        Me.grpboxStep3.Size = New System.Drawing.Size(610, 350)
        Me.grpboxStep3.TabIndex = 7
        Me.grpboxStep3.TabStop = False
        Me.grpboxStep3.Text = "Step 3 - Qwerty"
        '
        'txtDomainOld
        '
        Me.txtDomainOld.Location = New System.Drawing.Point(15, 61)
        Me.txtDomainOld.Name = "txtDomainOld"
        Me.txtDomainOld.ReadOnly = True
        Me.txtDomainOld.Size = New System.Drawing.Size(191, 20)
        Me.txtDomainOld.TabIndex = 4
        Me.txtDomainOld.Text = "ENG_RIETBERG"
        '
        'txtComputerNameOld
        '
        Me.txtComputerNameOld.Location = New System.Drawing.Point(15, 35)
        Me.txtComputerNameOld.Name = "txtComputerNameOld"
        Me.txtComputerNameOld.ReadOnly = True
        Me.txtComputerNameOld.Size = New System.Drawing.Size(191, 20)
        Me.txtComputerNameOld.TabIndex = 3
        Me.txtComputerNameOld.Text = "ENG_VM_STANDALONE"
        '
        'txtDomainNew
        '
        Me.txtDomainNew.Location = New System.Drawing.Point(212, 61)
        Me.txtDomainNew.Name = "txtDomainNew"
        Me.txtDomainNew.Size = New System.Drawing.Size(191, 20)
        Me.txtDomainNew.TabIndex = 2
        Me.txtDomainNew.Text = "ENG_RIETBERG"
        '
        'txtComputerNameNew
        '
        Me.txtComputerNameNew.Location = New System.Drawing.Point(212, 35)
        Me.txtComputerNameNew.Name = "txtComputerNameNew"
        Me.txtComputerNameNew.Size = New System.Drawing.Size(191, 20)
        Me.txtComputerNameNew.TabIndex = 1
        Me.txtComputerNameNew.Text = "ENG_VM_STANDALONE"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(445, 71)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 118)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'grpboxStep4
        '
        Me.grpboxStep4.Controls.Add(Me.txtProgress)
        Me.grpboxStep4.Controls.Add(Me.progressInstallation)
        Me.grpboxStep4.Location = New System.Drawing.Point(638, 513)
        Me.grpboxStep4.Name = "grpboxStep4"
        Me.grpboxStep4.Size = New System.Drawing.Size(610, 350)
        Me.grpboxStep4.TabIndex = 8
        Me.grpboxStep4.TabStop = False
        Me.grpboxStep4.Text = "Step 4 - Installation progress"
        '
        'txtProgress
        '
        Me.txtProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtProgress.Location = New System.Drawing.Point(6, 19)
        Me.txtProgress.Multiline = True
        Me.txtProgress.Name = "txtProgress"
        Me.txtProgress.Size = New System.Drawing.Size(598, 229)
        Me.txtProgress.TabIndex = 1
        '
        'progressInstallation
        '
        Me.progressInstallation.BackColor = System.Drawing.Color.White
        Me.progressInstallation.ForeColor = System.Drawing.Color.Firebrick
        Me.progressInstallation.Location = New System.Drawing.Point(6, 254)
        Me.progressInstallation.Name = "progressInstallation"
        Me.progressInstallation.Size = New System.Drawing.Size(598, 22)
        Me.progressInstallation.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressInstallation.TabIndex = 0
        '
        'btnFocusCatcher
        '
        Me.btnFocusCatcher.Location = New System.Drawing.Point(708, 455)
        Me.btnFocusCatcher.Name = "btnFocusCatcher"
        Me.btnFocusCatcher.Size = New System.Drawing.Size(115, 25)
        Me.btnFocusCatcher.TabIndex = 9
        Me.btnFocusCatcher.Text = "Button1"
        Me.btnFocusCatcher.UseVisualStyleBackColor = True
        '
        'grpboxStep5
        '
        Me.grpboxStep5.Location = New System.Drawing.Point(905, 461)
        Me.grpboxStep5.Name = "grpboxStep5"
        Me.grpboxStep5.Size = New System.Drawing.Size(238, 46)
        Me.grpboxStep5.TabIndex = 10
        Me.grpboxStep5.TabStop = False
        '
        'tmrInstallerWait
        '
        Me.tmrInstallerWait.Interval = 1000
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(27, 26)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(569, 20)
        Me.TextBox1.TabIndex = 0
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1284, 925)
        Me.Controls.Add(Me.grpboxStep5)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnPrev)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.grpboxStep2)
        Me.Controls.Add(Me.btnFocusCatcher)
        Me.Controls.Add(Me.grpboxStep3)
        Me.Controls.Add(Me.grpboxStep4)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.grpboxStep1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "iBAHN - LobbyPC Standalone"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpboxStep1.ResumeLayout(False)
        Me.grpboxStep1.PerformLayout()
        Me.grpboxStep2.ResumeLayout(False)
        Me.grpboxStep2.PerformLayout()
        Me.grpboxStep3.ResumeLayout(False)
        Me.grpboxStep3.PerformLayout()
        Me.grpboxStep4.ResumeLayout(False)
        Me.grpboxStep4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents grpboxStep1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents grpboxStep2 As System.Windows.Forms.GroupBox
    Friend WithEvents grpboxStep3 As System.Windows.Forms.GroupBox
    Friend WithEvents grpboxStep4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnFocusCatcher As System.Windows.Forms.Button
    Friend WithEvents txtTC As System.Windows.Forms.TextBox
    Friend WithEvents chkTCAgree As System.Windows.Forms.CheckBox
    Friend WithEvents progressInstallation As System.Windows.Forms.ProgressBar
    Friend WithEvents txtProgress As System.Windows.Forms.TextBox
    Friend WithEvents txtDomainNew As System.Windows.Forms.TextBox
    Friend WithEvents txtComputerNameNew As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtDomainOld As System.Windows.Forms.TextBox
    Friend WithEvents txtComputerNameOld As System.Windows.Forms.TextBox
    Friend WithEvents grpboxStep5 As System.Windows.Forms.GroupBox
    Friend WithEvents tmrInstallerWait As System.Windows.Forms.Timer
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox

End Class
