﻿Imports System.Reflection

Public Class frmConfiguration
    Public Config_Region As String = "emea"
    Public Config_Theme As String = "GuestTek"
    Public Config_Theme_BrandIndex As Integer = -1

    Public Config_OldVersionWarning As Boolean = False

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        GetGroupBoxCheckedButton()

        Config_Theme = lstThemes.SelectedItem
        Config_Theme_BrandIndex = HotelBrands.Find(Config_Theme).BrandIndex

        Me.DialogResult = DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Me.DialogResult = DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub ShowOldVersionWarning()
        gbInstallConfigTheme.Enabled = False
        gbInstallConfigRegion.Enabled = False

        btnOk.Enabled = False
        btnCancel.Enabled = False

        'pnlWarning.Left = (Me.Width - pnlWarning.Width) / 2
        'pnlWarning.Top = (Me.Height - pnlWarning.Height) / 2
        pnlWarning.Visible = True
        pnlWarningShadow.Visible = True
    End Sub

    Private Sub HideOldVersionWarning()
        gbInstallConfigTheme.Enabled = True
        gbInstallConfigRegion.Enabled = True

        btnOk.Enabled = True
        btnCancel.Enabled = True

        pnlWarning.Visible = False
        pnlWarningShadow.Visible = False
    End Sub

    Private Sub frmConfiguration_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Config_OldVersionWarning Then
            ShowOldVersionWarning()
        Else
            HideOldVersionWarning()
        End If

        Dim xmlSkCfg As Xml.XmlDocument
        Dim xmlNode_Root As Xml.XmlNode, xmlNode_Brands As Xml.XmlNode, xmlNode_BrandList As Xml.XmlNodeList, xmlNode_Brand As Xml.XmlNode
        Dim isHiltonTheme As Boolean = False
        Dim _type As Type = (New HotelBrands.HotelBrand).GetType
        Dim _index As Integer = 0

        xmlSkCfg = New Xml.XmlDocument

        xmlSkCfg.LoadXml(My.Resources.ovcc_themes)

        xmlNode_Root = xmlSkCfg.SelectSingleNode("OVCCThemes")
        xmlNode_Brands = xmlNode_Root.SelectSingleNode("brands")
        xmlNode_BrandList = xmlNode_Brands.SelectNodes("brand")

        For Each xmlNode_Brand In xmlNode_BrandList
            Dim _brand As New HotelBrands.HotelBrand

            _brand.Index = _index
            _brand.BrandIndex = CInt(xmlNode_Brand.SelectSingleNode("index").InnerText)
            _brand.Name = xmlNode_Brand.SelectSingleNode("name").InnerText
            _brand.Path_Theme = xmlNode_Brand.SelectSingleNode("path_theme").InnerText
            _brand.Path_Screensaver = xmlNode_Brand.SelectSingleNode("path_screensaver").InnerText
            _brand.Path_Logout = xmlNode_Brand.SelectSingleNode("path_logout").InnerText
            _brand.Path_PaymentDialog = xmlNode_Brand.SelectSingleNode("path_paymentdialog").InnerText
            _brand.IsLastHilton = False


            If xmlNode_Brand.SelectSingleNode("payment").InnerText.Equals("True") Then
                _brand.PaymentEnabled = True
            Else
                _brand.PaymentEnabled = False
            End If

            If xmlNode_Brand.SelectSingleNode("password").InnerText.Equals("True") Then
                _brand.PasswordEnabled = True
            Else
                _brand.PasswordEnabled = False
            End If


            HotelBrands.Add(_brand)

            _index += 1
        Next

        HotelBrands.SetLastHilton()


        '--------------------------------
        lstThemes.Items.Clear()

        For Each _brand As HotelBrands.HotelBrand In HotelBrands.GetBrands
            lstThemes.Items.Add(_brand.Name)
        Next

        lstThemes.SelectedItem = Config_Theme

        If Config_Theme_BrandIndex > 0 Then
            Dim _brand_tmp As HotelBrands.HotelBrand = HotelBrands.Find(Config_Theme_BrandIndex)


            If _brand_tmp IsNot Nothing Then
                lstThemes.SelectedItem = _brand_tmp.Name
            End If
        End If


        '------------------------------
        SetGroupBoxCheckedButton()
    End Sub

    Private Sub lstThemes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstThemes.SelectedIndexChanged
        'Config_Theme = lstThemes.SelectedItem
        'Config_Theme_BrandIndex = HotelBrands.Find(Config_Theme).BrandIndex
    End Sub

    Private Sub GetGroupBoxCheckedButton()
        For Each ctrl As RadioButton In gbInstallConfigRegion.Controls
            If ctrl.Checked Then Config_Region = ctrl.Text
        Next
    End Sub

    Private Sub SetGroupBoxCheckedButton()
        For Each ctrl As RadioButton In gbInstallConfigRegion.Controls
            If ctrl.Text.ToLower.Equals(Config_Region.ToLower) Then
                ctrl.Checked = True
            End If
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        HideOldVersionWarning()
    End Sub
End Class