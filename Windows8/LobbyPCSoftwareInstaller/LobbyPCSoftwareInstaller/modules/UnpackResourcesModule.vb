Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sSourceFolder As String
        Dim sDestinationFolder As String


        oLogger.WriteToLog("Copying resources")

        sSourceFolder = gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder
        sDestinationFolder = sSourceFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If


        oLogger.WriteToLog("Visual C++ redistributable package", , 1)
        oSettings.Path_VisualCplusplusRedistributable = sDestinationFolder & "\vcredist_x86.exe"


        oLogger.WriteToLog("SiteKiosk Updater package (Win8)", , 1)
        oSettings.Path_SiteKioskUpdate = sDestinationFolder & "\SiteKioskUpdater.package.exe"
        'If gForceBrandUI >= 0 And gForceBrandUI < 100 Then
        '    'Use the Hilton package
        '    oSettings.Path_SiteKioskUpdate = sDestinationFolder & "\SiteKioskUpdater.hilton.exe"
        'Else
        '    oSettings.Path_SiteKioskUpdate = sDestinationFolder & "\SiteKioskUpdater.generic.exe"
        'End If


        oLogger.WriteToLog("LogoutCleanupHelper", , 1)
        oSettings.Path_LogoutCleanupHelper = sDestinationFolder & "\LogoutCleanupHelper.package.exe"


        oLogger.WriteToLog("OVCCFileExplorer", , 1)
        oSettings.Path_OVCCFileExplorer = sDestinationFolder & "\OVCCFileExplorer.deploy.exe"


        oLogger.WriteToLog("StopEdgeFirstRun", , 1)
        oSettings.Path_StopEdgeFirstRun = sDestinationFolder & "\StopEdgeFirstRun.exe"


        oLogger.WriteToLog("AirlineUpdater", , 1)
        oSettings.Path_AirlineUpdater = sDestinationFolder & "\AirlineUpdater.deploy.combined.exe"


        oLogger.WriteToLog("SiteKiosk Security Updater", , 1)
        oSettings.Path_SystemSecurityUpdater = sDestinationFolder & "\SystemSecurityUpdater.exe"


        oLogger.WriteToLog("OVCCWatchdog package", , 1)
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\OVCCWatchdog.package.exe"


        oLogger.WriteToLog("Chrome installer package", , 1)
        oSettings.Path_ChromeInstaller = sDestinationFolder & "\ChromeInstaller.deploy.stage03.exe"


        oLogger.WriteToLog("Pinta installer package", , 1)
        oSettings.Path_PintaInstaller = sDestinationFolder & "\Pinta.exe"


        oLogger.WriteToLog("SiteKiosk", , 1)
        gSkipCplusplusThing = True
        If IO.File.Exists(sDestinationFolder & "\sitekiosk10.exe") Then
            oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk10.exe"
            oLogger.WriteToLog("10", , 2)
        ElseIf IO.File.Exists(sDestinationFolder & "\sitekiosk9.exe") Then
            oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk9.exe"
            oLogger.WriteToLog("9", , 2)
        Else
            gSkipCplusplusThing = False
            oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk8.exe"
            oLogger.WriteToLog("8", , 2)
        End If


        oLogger.WriteToLog("OVCCMonitor package", , 1)
        oSettings.Path_OVCCMonitor = sDestinationFolder & "\OVCCMonitor.package.exe"


        sDestinationFolder = gGuestTekProgramFilesFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If


        'tools
        sDestinationFolder = gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesToolsFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If


        oLogger.WriteToLog("OVCCStart", , 1)
        _CopyFile("OVCCStart.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_AutoStart = sDestinationFolder & "\OVCCStart.exe"


        oLogger.WriteToLog("OVCCInformationUpdater", , 1)
        _CopyFile("OVCCInformationUpdater.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_HotelPageUpdater = sDestinationFolder & "\OVCCInformationUpdater.exe"


        'config
        sDestinationFolder = gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesConfigFolder
        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("SK licenses", , 1)
        _CopyFile("sk-licenses.xml", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_SiteKioskLicensesFile = sDestinationFolder & "\sk-licenses.xml"


        'internal apps
        sDestinationFolder = gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInternalFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If
    End Sub

    Private Sub _CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, ByVal iLogLevel As Integer)
        oLogger.WriteToLog("copying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)

        oLogger.WriteToLog("retrying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
    End Sub
End Module
