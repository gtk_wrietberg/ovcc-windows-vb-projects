Module Constants___Globals
    'Some extra installers
    'Public ReadOnly cINSTALLERS__HILTON_UI__EMEA As String = "NewUI.Hilton.revision3.installer.EMEA.exe"
    'Public ReadOnly cINSTALLERS__HILTON_UI__US As String = "NewUI.Hilton.revision3.installer.US.exe"
    'Public ReadOnly cINSTALLERS__IBAHN_UI__EMEA As String = "NewUI.iBAHN.installer.EMEA.exe"
    'Public ReadOnly cINSTALLERS__IBAHN_UI__US As String = "NewUI.iBAHN.installer.US.exe"
    'Public ReadOnly cINSTALLERS__GUESTTEK_UI__EMEA As String = "NewUI.Guest-tek.installer.EMEA.exe"
    'Public ReadOnly cINSTALLERS__GUESTTEK_UI__US As String = "NewUI.Guest-tek.installer.US.exe"
    'Public ReadOnly cINSTALLERS__GUESTTEK_UI__US As String = "SiteKiosk8.installer.exe"


    'GUI
    Public ReadOnly cGUI_GroupBoxBorder_visible As Boolean = True
    'Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.FromArgb(158, 19, 14))
    Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.Black)

    'Paths
    Public ReadOnly cPATHS_SiteKioskProgramFilesFolder As String = "C:\Program Files\SiteKiosk"
    Public ReadOnly cPATHS_SiteKioskProgramFilesFolder_x86 As String = "C:\Program Files (x86)\SiteKiosk"
    'Public ReadOnly cPATHS_GuestTekProgramFilesFolder As String = "C:\Program Files\GuestTek\LobbyPCSoftware"
    Public ReadOnly cPATHS_GuestTekProgramFilesFolder As String = "C:\Program Files\GuestTek\OVCCSoftware"
    'Public ReadOnly cPATHS_GuestTekProgramFilesFolder_x86 As String = "C:\Program Files (x86)\GuestTek\LobbyPCSoftware"
    Public ReadOnly cPATHS_GuestTekProgramFilesFolder_x86 As String = "C:\Program Files (x86)\GuestTek\OVCCSoftware"
    Public ReadOnly cPATHS_GuestTekProgramFilesResourcesFolder As String = "\installation_files\Resources"
    Public ReadOnly cPATHS_GuestTekProgramFilesToolsFolder As String = "\tools"
    Public ReadOnly cPATHS_GuestTekProgramFilesInternalFolder As String = "\internal"
    Public ReadOnly cPATHS_GuestTekProgramFilesConfigFolder As String = "\config"

    Public ReadOnly cPATHS_ConfigFile As String = "OVCCSoftware.config.xml"

    'Debug
    Public ReadOnly cDEBUG_ForcePrerequisiteWindowsVersionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteInternetConnectionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteUserIsAdminError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteAlreadyInstalledError As Boolean = False

    'SiteKiosk
    Public ReadOnly cSITEKIOSK_InstallLogPath As String = "C:\"
    Public ReadOnly cSITEKIOSK_InstallLogName As String = "SiteKioskInstall.log"
    Public ReadOnly cSITEKIOSK_UserName As String = "sitekiosk"
    Public ReadOnly cSITEKIOSK_PassWord As String = "Provisi0"

    Public ReadOnly cSITEKIOSK_ServiceName As String = "SiteRemote Client"


    'admin user
    Public Const cADMIN_Username As String = "guesttek"
    'Public Const cADMIN_Password As String = "Rancidkipp3r"
    Public Const cADMIN_Password As String = "rancidkipper"
    'Public Const cADMIN_Password__Hilton As String = "H1lt0nUK"
    Public Const cADMIN_Password__Hilton As String = "Genesis"

    'Registry
    Public ReadOnly cREGKEY_LPCSOFTWARE As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCSoftware"
    Public ReadOnly cREGVALUE__RUNFROMINSTALLER As String = "run_from_downloader"

    Public ReadOnly cREGKEY_POLICIES_EXPLORER As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer"
    Public ReadOnly cREGKEY_POLICIES_EXPLORER2 As String = "HKEY_LOCAL_MACHINE\Software\Policies\Microsoft\Windows\Explorer"

    Public ReadOnly cREGKEY___Watchdog As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog"
    Public ReadOnly cREGVALUE__DisabledUntilOVCCStart As String = "DisabledUntilOVCCStart"

    Public ReadOnly cREGKEY__LEGALNOTICE As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System"
    Public ReadOnly cREGVALUE__LEGALNOTICECAPTION As String = "legalnoticecaption"
    Public ReadOnly cREGVALUE__LEGALNOTICETEXT As String = "legalnoticetext"


    'Globals. Yeah, I know.
    Public oLogger As Logger
    Public oSettings As Settings

    Public gBusyInstalling As Boolean
    Public gTestMode As Boolean
    Public gSkipCplusplusThing As Boolean

    Public gForceBrandUI As Integer

    Public gAllFilesCopied As Boolean

    Public gHideProgress As Boolean
    Public gSkipInstallationQuestion As Boolean
    Public gRegion As String
    Public gLicenseRegion As String
    Public gEnglishFlag As String
    Public gSkipSkype As Boolean
    Public gSiteKioskUserPassword As String


    Public gGuestTekProgramFilesFolder As String

    Public gSiteKioskShowProgress As Boolean
    Public gSiteKioskInteractiveInstall As Boolean

    Public gApplicationFatalErrorDescription As String = ""


    Public gWeAreUpdating As Boolean = False
    Public gOldVersionWarning As Boolean = False

    Public gSiteKioskInstallDir As String = ""
    Public gMonitorIdentifier As String = ""
    Public g_iBahnFunctions_js As String = ""


    Public gAllowRunning As String = ""
    Public gSkipManualRunStepCheckThingy As Boolean = False
End Module
