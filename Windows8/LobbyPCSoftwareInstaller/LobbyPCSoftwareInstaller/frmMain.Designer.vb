﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.tmrInstallerWait = New System.Windows.Forms.Timer(Me.components)
        Me.tmrExit = New System.Windows.Forms.Timer(Me.components)
        Me.tmrStartDeployment = New System.Windows.Forms.Timer(Me.components)
        Me.bgworkerAdminUserCreation = New System.ComponentModel.BackgroundWorker()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.lblSubTitle = New System.Windows.Forms.Label()
        Me.txtProgress = New System.Windows.Forms.TextBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.pnlMain.SuspendLayout()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tmrInstallerWait
        '
        Me.tmrInstallerWait.Interval = 1000
        '
        'tmrExit
        '
        Me.tmrExit.Interval = 30000
        '
        'tmrStartDeployment
        '
        Me.tmrStartDeployment.Interval = 1000
        '
        'bgworkerAdminUserCreation
        '
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.lblVersion)
        Me.pnlMain.Controls.Add(Me.lblSubTitle)
        Me.pnlMain.Controls.Add(Me.txtProgress)
        Me.pnlMain.Controls.Add(Me.lblTitle)
        Me.pnlMain.Controls.Add(Me.picLogo)
        Me.pnlMain.Location = New System.Drawing.Point(3, 3)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(1011, 653)
        Me.pnlMain.TabIndex = 5
        '
        'lblVersion
        '
        Me.lblVersion.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.lblVersion.Location = New System.Drawing.Point(788, 97)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(223, 23)
        Me.lblVersion.TabIndex = 7
        Me.lblVersion.Text = "version number goes here"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblSubTitle
        '
        Me.lblSubTitle.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.lblSubTitle.Location = New System.Drawing.Point(299, 60)
        Me.lblSubTitle.Name = "lblSubTitle"
        Me.lblSubTitle.Size = New System.Drawing.Size(712, 57)
        Me.lblSubTitle.TabIndex = 9
        Me.lblSubTitle.Text = "software installer / updater"
        Me.lblSubTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtProgress
        '
        Me.txtProgress.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.txtProgress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtProgress.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProgress.Location = New System.Drawing.Point(3, 123)
        Me.txtProgress.Multiline = True
        Me.txtProgress.Name = "txtProgress"
        Me.txtProgress.ReadOnly = True
        Me.txtProgress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtProgress.Size = New System.Drawing.Size(1004, 527)
        Me.txtProgress.TabIndex = 8
        Me.txtProgress.Text = "test" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "test" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "tes" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "tsts" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "etsd" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "tdrtgdrgdshgggggfghfh" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "fhfg" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "hf" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "hfgh"
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Segoe UI", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(299, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(712, 82)
        Me.lblTitle.TabIndex = 6
        Me.lblTitle.Text = "OneView Connection Centre"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picLogo
        '
        Me.picLogo.BackgroundImage = Global.OVCCSoftwareInstaller.My.Resources.Resources.GuestTek_Header_Logo
        Me.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.picLogo.Location = New System.Drawing.Point(3, 3)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(290, 114)
        Me.picLogo.TabIndex = 5
        Me.picLogo.TabStop = False
        '
        'frmMain
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1016, 658)
        Me.Controls.Add(Me.pnlMain)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "OneView Connection Centre - software installer / updater"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tmrInstallerWait As System.Windows.Forms.Timer
    Friend WithEvents tmrExit As System.Windows.Forms.Timer
    Friend WithEvents tmrStartDeployment As System.Windows.Forms.Timer
    Friend WithEvents bgworkerAdminUserCreation As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlMain As Panel
    Friend WithEvents lblVersion As Label
    Friend WithEvents lblSubTitle As Label
    Friend WithEvents txtProgress As TextBox
    Friend WithEvents lblTitle As Label
    Friend WithEvents picLogo As PictureBox
End Class
