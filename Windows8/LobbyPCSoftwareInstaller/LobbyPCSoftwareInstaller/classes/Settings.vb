Public Class Settings
    Private mInstaller_VisualCplusplusRedistributable_Path As String
    Private mInstaller_SiteKioskUpdate_Path As String
    Private mInstaller_LogoutCleanupHelper_Path As String
    Private mInstaller_OVCCFileExplorer_Path As String
    Private mInstaller_StopEdgeFirstRun_Path As String
    Private mInstaller_AirlineUpdater_Path As String
    Private mInstaller_SiteKiosk_Path As String
    Private mInstaller_AutoStart_Path As String
    Private mInstaller_LobbyPCWatchdog_Path As String
    Private mInstaller_HotelPageUpdater_Path As String
    Private mInstaller_SystemSecurityUpdater_Path As String
    Private mInstaller_SiteKioskLicensesFile_Path As String
    Private mInstaller_ChromeInstaller_Path As String
    Private mInstaller_PintaInstaller_Path As String
    Private mInstaller_OVCCMonitor_Path As String

    Public Sub New()

    End Sub

#Region "Properties"
    Public Property Path_SiteKioskLicensesFile() As String
        Get
            Return mInstaller_SiteKioskLicensesFile_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SiteKioskLicensesFile_Path = value
        End Set
    End Property

    Public Property Path_VisualCplusplusRedistributable() As String
        Get
            Return mInstaller_VisualCplusplusRedistributable_Path
        End Get
        Set(ByVal value As String)
            mInstaller_VisualCplusplusRedistributable_Path = value
        End Set
    End Property

    Public Property Path_SiteKioskUpdate() As String
        Get
            Return mInstaller_SiteKioskUpdate_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SiteKioskUpdate_Path = value
        End Set
    End Property

    Public Property Path_SiteKiosk() As String
        Get
            Return mInstaller_SiteKiosk_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SiteKiosk_Path = value
        End Set
    End Property

    Public Property Path_AutoStart() As String
        Get
            Return mInstaller_AutoStart_Path
        End Get
        Set(ByVal value As String)
            mInstaller_AutoStart_Path = value
        End Set
    End Property

    Public Property Path_LobbyPCWatchdog() As String
        Get
            Return mInstaller_LobbyPCWatchdog_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LobbyPCWatchdog_Path = value
        End Set
    End Property

    Public Property Path_HotelPageUpdater() As String
        Get
            Return mInstaller_HotelPageUpdater_Path
        End Get
        Set(ByVal value As String)
            mInstaller_HotelPageUpdater_Path = value
        End Set
    End Property

    Public Property Path_SystemSecurityUpdater() As String
        Get
            Return mInstaller_SystemSecurityUpdater_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SystemSecurityUpdater_Path = value
        End Set
    End Property

    Public Property Path_LogoutCleanupHelper As String
        Get
            Return mInstaller_LogoutCleanupHelper_Path
        End Get
        Set(value As String)
            mInstaller_LogoutCleanupHelper_Path = value
        End Set
    End Property

    Public Property Path_OVCCFileExplorer As String
        Get
            Return mInstaller_OVCCFileExplorer_Path
        End Get
        Set(value As String)
            mInstaller_OVCCFileExplorer_Path = value
        End Set
    End Property

    Public Property Path_StopEdgeFirstRun As String
        Get
            Return mInstaller_StopEdgeFirstRun_Path
        End Get
        Set(value As String)
            mInstaller_StopEdgeFirstRun_Path = value
        End Set
    End Property

    Public Property Path_AirlineUpdater As String
        Get
            Return mInstaller_AirlineUpdater_Path
        End Get
        Set(value As String)
            mInstaller_AirlineUpdater_Path = value
        End Set
    End Property

    Public Property Path_ChromeInstaller As String
        Get
            Return mInstaller_ChromeInstaller_Path
        End Get
        Set(value As String)
            mInstaller_ChromeInstaller_Path = value
        End Set
    End Property

    Public Property Path_PintaInstaller As String
        Get
            Return mInstaller_PintaInstaller_Path
        End Get
        Set(value As String)
            mInstaller_PintaInstaller_Path = value
        End Set
    End Property

    Public Property Path_OVCCMonitor As String
        Get
            Return mInstaller_OVCCMonitor_Path
        End Get
        Set(value As String)
            mInstaller_OVCCMonitor_Path = value
        End Set
    End Property
#End Region
End Class
