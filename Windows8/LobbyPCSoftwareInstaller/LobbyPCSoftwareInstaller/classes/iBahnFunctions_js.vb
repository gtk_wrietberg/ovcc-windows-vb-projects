﻿Public Class iBahnFunctions_js
    Public Shared HotelInformationUrl As String = "https://guesttek.com/"
    Public Shared InternetUrl As String = "http://www.google.com"
    Public Shared WeatherUrl As String = "https://weather.com/"
    Public Shared MapUrl As String = "https://www.google.com/maps"
    Public Shared HotelAddress As String = "GuestTek, Calgary, Alberta Canada"
    Public Shared ThemeIndex As Integer = 667

    Public Shared LastError As String = ""

    Public Shared ReadOnly Property HasError() As Boolean
        Get
            Return Not LastError.Equals("")
        End Get
    End Property

    Public Shared Function Load() As Boolean
        If Not IO.File.Exists(g_iBahnFunctions_js) Then
            LastError = "File '" & g_iBahnFunctions_js & "' does not exist"
            Return False
        End If

        Try
            Dim bUpdated As Boolean = False

            Using sr As New IO.StreamReader(g_iBahnFunctions_js)
                Dim line As String
                Dim currentValue As String
                Dim iValue As Integer

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        currentValue = line.Replace("var iBAHN_Global_HotelURL=""", "").Replace(""";", "")
                        HotelInformationUrl = currentValue
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        currentValue = line.Replace("var iBAHN_Global_InternetURL=""", "").Replace(""";", "")
                        InternetUrl = currentValue
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        currentValue = line.Replace("var iBAHN_Global_WeatherURL=""", "").Replace(""";", "")
                        WeatherUrl = currentValue
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        currentValue = line.Replace("var iBAHN_Global_MapURL=""", "").Replace(""";", "")
                        MapUrl = currentValue
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        currentValue = line.Replace("var iBAHN_Global_SiteAddress=""", "").Replace(""";", "")
                        HotelAddress = currentValue
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        currentValue = line.Replace("var iBAHN_Global_Brand=", "").Replace(";", "")
                        If Not Integer.TryParse(currentValue, iValue) Then
                            iValue = -1
                        End If
                        ThemeIndex = iValue
                    End If
                End While
            End Using
        Catch ex As Exception
            LastError = ex.Message
            Return False
        End Try


        Return True
    End Function

    Public Shared Function Save() As Boolean
        Dim lines As New List(Of String)

        If Not IO.File.Exists(g_iBahnFunctions_js) Then
            LastError = "File '" & g_iBahnFunctions_js & "' does not exist"
            Return False
        End If

        Try
            Dim bUpdated As Boolean = False

            Using sr As New IO.StreamReader(g_iBahnFunctions_js)
                Dim line As String
                Dim currentValue As String
                Dim newValue As String
                Dim iValue As Integer

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        newValue = HotelInformationUrl
                        currentValue = line.Replace("var iBAHN_Global_HotelURL=""", "").Replace(""";", "")
                        If Not currentValue.Equals(newValue) Then
                            line = "var iBAHN_Global_HotelURL=""" & newValue & """;"
                            bUpdated = True
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        newValue = InternetUrl
                        currentValue = line.Replace("var iBAHN_Global_InternetURL=""", "").Replace(""";", "")
                        If Not currentValue.Equals(newValue) Then
                            line = "var iBAHN_Global_InternetURL=""" & newValue & """;"
                            bUpdated = True
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        newValue = WeatherUrl
                        currentValue = line.Replace("var iBAHN_Global_WeatherURL=""", "").Replace(""";", "")
                        If Not currentValue.Equals(newValue) Then
                            line = "var iBAHN_Global_WeatherURL=""" & newValue & """;"
                            bUpdated = True
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        newValue = MapUrl
                        currentValue = line.Replace("var iBAHN_Global_MapURL=""", "").Replace(""";", "")
                        If Not currentValue.Equals(newValue) Then
                            line = "var iBAHN_Global_MapURL=""" & newValue & """;"
                            bUpdated = True
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        newValue = HotelAddress
                        currentValue = line.Replace("var iBAHN_Global_SiteAddress=""", "").Replace(""";", "")
                        If Not currentValue.Equals(newValue) Then
                            line = "var iBAHN_Global_SiteAddress=""" & newValue & """;"
                            bUpdated = True
                        End If
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        currentValue = line.Replace("var iBAHN_Global_Brand=", "").Replace(";", "")
                        If Not Integer.TryParse(currentValue, iValue) Then
                            iValue = -1
                        End If
                        If iValue <> ThemeIndex Then
                            line = "var iBAHN_Global_Brand=" & ThemeIndex & ";"
                            bUpdated = True
                        End If
                    End If

                    lines.Add(line)
                End While
            End Using

            If bUpdated Then
                Using sw As New IO.StreamWriter(g_iBahnFunctions_js)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using

                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LastError = ex.Message
            Return False
        End Try
    End Function
End Class
