Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement
Imports System.Runtime.InteropServices

Public Class cWindowsUser
    Private Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000

    Private Const SID_USERS As String = "S-1-5-32-545"
    Private Const SID_ADMINISTRATORS As String = "S-1-5-32-544"

    Public Shared Function RemoveUser(sUsername As String, ByRef sError As String) As Boolean
        Try
            sError = ""

            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                AD.Children.Remove(usr)

                Return True
            End If
        Catch ex As Exception
            sError = ex.Message
        End Try

        Return False
    End Function

    Public Shared Function DoesUserExist(sUsername As String, ByRef sError As String) As Boolean
        Try
            sError = ""

            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                Return True
            End If
        Catch ex As Exception
            sError = ex.Message
        End Try

        Return False
    End Function

    Public Shared Function IsUserAdmin(sUsername As String, ByRef sError As String) As Boolean
        Try
            sError = ""

            Dim DC = New PrincipalContext(ContextType.Machine)

            Dim user = UserPrincipal.FindByIdentity(DC, sUsername)
            Dim groups = user.GetGroups()
            Dim adminGroup As String = GetAdministratorsGroupName()

            For Each group As Principal In groups
                If group.Name = adminGroup Then
                    Return True
                End If
            Next
        Catch ex As Exception
            sError = ex.Message
        End Try

        Return False
    End Function

    Public Shared Function IsUserLoggedIn(sUsername As String, ByRef sError As String) As Boolean
        sError = ""

        Try
            If My.User.Name.Substring(My.User.Name.LastIndexOf("\") + 1).ToLower = sUsername.ToLower Then
                Return True
            End If

        Catch ex As Exception
            sError = ex.Message
        End Try

        Return False
    End Function

    Public Shared Function GetLoggedInUser(ByRef sError As String) As String
        sError = ""

        Try
            Return My.User.Name.Substring(My.User.Name.LastIndexOf("\") + 1).ToLower
        Catch ex As Exception
            sError = ex.Message
        End Try

        Return ""
    End Function

    Public Shared Function AddAdminUser(ByVal sUsername As String, sDescription As String, ByVal sPassWord As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim NewUser As DirectoryEntry = AD.Children.Add(sUsername, "user")
            NewUser.Invoke("SetPassword", New Object() {sPassWord})
            NewUser.Invoke("Put", New Object() {"Description", sDescription})
            NewUser.CommitChanges()

            Dim grp As DirectoryEntry, sGrp As String = GetAdministratorsGroupName()

            If sGrp = "" Then
                sGrp = "Administrators"
            End If

            grp = AD.Children.Find(sGrp, "group")
            If grp.Name <> "" Then
                grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
            Else
                Return "group failed"
            End If

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function AddUser(ByVal sUsername As String, sDescription As String, ByVal sPassWord As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim NewUser As DirectoryEntry = AD.Children.Add(sUsername, "user")
            NewUser.Invoke("SetPassword", New Object() {sPassWord})
            NewUser.Invoke("Put", New Object() {"Description", sDescription})
            NewUser.CommitChanges()

            Dim grp As DirectoryEntry, sGrp As String = GetUsersGroupName()

            If sGrp = "" Then
                sGrp = "Users"
            End If

            grp = AD.Children.Find(sGrp, "group")
            If grp.Name <> "" Then
                grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
            End If

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function ChangePassword(ByVal sUsername As String, ByVal sPassWord As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                usr.Invoke("SetPassword", New Object() {sPassWord})
                usr.CommitChanges()

                Return "ok"
            Else
                Return "not found"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function RemovePasswordExpiry(ByVal sUsername As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                usr.Properties("UserFlags").Value = ADS_UF_DONT_EXPIRE_PASSWD
                usr.CommitChanges()

                Return "ok"
            Else
                Return "not found"
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function PromoteUserToAdmin(sUsername As String) As String
        Try
            Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
            Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

            If usr.Name <> "" Then
                Dim grp As DirectoryEntry, sGrp As String = GetAdministratorsGroupName()

                If sGrp = "" Then
                    sGrp = "Administrators"
                End If

                grp = AD.Children.Find(sGrp, "group")
                If grp.Name <> "" Then
                    grp.Invoke("Add", New Object() {usr.Path.ToString()})
                End If
            End If

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function GetAdministratorsGroupName() As String
        Return GetGroupNameFromSid(SID_ADMINISTRATORS)
    End Function

    Public Shared Function GetUsersGroupName() As String
        Return GetGroupNameFromSid(SID_USERS)
    End Function

    Private Shared Function GetGroupNameFromSid(ByVal _sid As String) As String
        Try
            Dim context As PrincipalContext, group As GroupPrincipal

            context = New PrincipalContext(ContextType.Machine)
            group = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, _sid)

            Return group.SamAccountName
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Shared Function HideUserInLogonScreen(ByVal sUsername As String) As Boolean
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", sUsername, 0)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

#Region "funky pasword check stuff"
    <DllImport("advapi32.dll", SetLastError:=True)> _
    Private Shared Function LogonUser(pszUsername As String, pszDomain As String, pszPassword As String, dwLogonType As Integer, dwLogonProvider As Integer, ByRef phToken As IntPtr) As Boolean
    End Function

    ' closes open handes returned by LogonUser
    <DllImport("kernel32.dll", CharSet:=CharSet.Auto)> _
    Private Shared Function CloseHandle(handle As IntPtr) As Boolean
    End Function

    <DllImport("Kernel32.dll", EntryPoint:="FormatMessageW", SetLastError:=True, CharSet:=CharSet.Unicode, CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function FormatMessage(ByVal dwFlags As Integer, ByRef lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByRef lpBuffer As [String], ByVal nSize As Integer, ByRef Arguments As IntPtr) As Integer
    End Function

    Private Enum LogonType As Integer
        LOGON32_LOGON_INTERACTIVE = 2
        LOGON32_LOGON_NETWORK = 3
        LOGON32_LOGON_BATCH = 4
        LOGON32_LOGON_SERVICE = 5
        LOGON32_LOGON_UNLOCK = 7
        LOGON32_LOGON_NETWORK_CLEARTEXT = 8
        LOGON32_LOGON_NEW_CREDENTIALS = 9
    End Enum

    Private Shared ReadOnly FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000

    Public Shared Function VerifyUserPassword(ByVal Username As String, ByVal Password As String) As String
        Return VerifyUserPassword(Username, Password, "")
    End Function

    Public Shared Function VerifyUserPassword(ByVal Username As String, ByVal Password As String, ByVal Domain As String) As String
        Dim sRet As String = ""

        Try
            Dim lStatus As Long
            Dim sMessage As String

            sMessage = Space(512)

            Dim Token As New IntPtr

            LogonUser(Username, Domain, Password, LogonType.LOGON32_LOGON_INTERACTIVE, 0, Token)
            CloseHandle(Token)

            If Token.ToInt32 <> 0 Then
                sRet = "ok"
            Else
                lStatus = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, Err.LastDllError, 0, sMessage, sMessage.Length, 0)
                sRet = sMessage
            End If
        Catch ex As Exception
            sRet = ex.Message
        End Try

        Return sRet
    End Function
#End Region

End Class
