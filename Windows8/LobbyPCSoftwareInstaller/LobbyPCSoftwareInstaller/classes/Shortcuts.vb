﻿Public Class Shortcut
    Public Shared Function CreateOnDesktop(ByVal FilePath As String, ByVal Title As String, Optional CurrentUserOnly As Boolean = False) As String
        Dim fileShortcut As String

        If CurrentUserOnly Then
            fileShortcut = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), Title & ".lnk")
        Else
            fileShortcut = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory), Title & ".lnk")
        End If


        Return _Create(fileShortcut, FilePath, Title)
    End Function

    Public Shared Function CreateInStartMenu(ByVal FilePath As String, ByVal Title As String, Optional CurrentUserOnly As Boolean = False) As String
        Dim fileShortcut As String

        If CurrentUserOnly Then
            fileShortcut = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu), Title & ".lnk")
        Else
            fileShortcut = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu), Title & ".lnk")
        End If

        Return _Create(fileShortcut, FilePath, Title)
    End Function


    Private Shared Function _Create(ByVal FileShortcut As String, ByVal FilePath As String, ByVal Title As String) As String
        AppShortcut.AddLnkShortcut(FileShortcut, FilePath, IO.Path.GetDirectoryName(FilePath), String.Empty, Title & " - het werkt!", FilePath, 0, ProcessWindowStyle.Normal)
        'Try
        '    Dim WshShell As New IWshRuntimeLibrary.WshShell


        '    oLogger.WriteToLogRelative("filepath", , 1)
        '    oLogger.WriteToLogRelative(FilePath, , 2)

        '    oLogger.WriteToLogRelative("shortcut", , 1)
        '    oLogger.WriteToLogRelative(FileShortcut, , 2)


        '    ' short cut files have a .lnk extension
        '    Dim shortCut As IWshRuntimeLibrary.IWshShortcut = DirectCast(WshShell.CreateShortcut(FileShortcut), IWshRuntimeLibrary.IWshShortcut)

        '    oLogger.WriteToLogRelative("creating", , 1)

        '    ' set the shortcut properties
        '    With shortCut
        '        .TargetPath = FilePath
        '        .WindowStyle = 1I
        '        .Description = Title
        '        .WorkingDirectory = IO.Path.GetDirectoryName(FilePath)
        '        ' the next line gets the first Icon from the executing program
        '        .IconLocation = FilePath & ", 0"
        '        .Arguments = String.Empty
        '        .Save() ' save the shortcut file
        '    End With

        '    oLogger.WriteToLogRelative("ok", , 2)

        '    Return ""
        'Catch ex As System.Exception
        '    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        '    oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        '    Return ex.Message
        'End Try
    End Function
End Class
