﻿Public Class DirectoriesFiles
    Public Class Directories
        Public Shared Function GetProgramFilesFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sPath.Equals(String.Empty) Then
                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetProgramFilesFolder64bit(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetGuestTekFolder() As String
            Return IO.Path.Combine(GetProgramFilesFolder, "GuestTek")
        End Function
    End Class
End Class
