﻿Imports System.IO

Public Class FileCounter
    Public Shared Function GetFileCount(path As String) As Integer
        Return _GetFiles(path).Count
    End Function

    Public Shared Function GetFolderCount(path As String) As Integer
        Return _GetFolders(path).Count
    End Function



    Private Shared Function _GetFiles(ByVal initial As String) As List(Of String)
        ' This list stores the results.
        Dim result As New List(Of String)

        ' This stack stores the directories to process.
        Dim stack As New Stack(Of String)

        ' Add the initial directory
        stack.Push(initial)

        ' Continue processing for each stacked directory
        Do While (stack.Count > 0)
            ' Get top directory string
            Dim dir As String = stack.Pop
            ' Add all immediate file paths
            Try
                result.AddRange(Directory.GetFiles(dir, "*.*"))
            Catch ex As Exception

            End Try

            ' Loop through all subdirectories and add them to the stack.
            Try
                Dim directoryName As String
                For Each directoryName In Directory.GetDirectories(dir)
                    stack.Push(directoryName)
                Next
            Catch ex As Exception

            End Try
        Loop

        ' Return the list
        Return result
    End Function

    Private Shared Function _GetFolders(ByVal initial As String) As List(Of String)
        ' This list stores the results.
        Dim result As New List(Of String)

        ' This stack stores the directories to process.
        Dim stack As New Stack(Of String)

        ' Add the initial directory
        stack.Push(initial)

        ' Continue processing for each stacked directory
        Do While (stack.Count > 0)
            ' Get top directory string
            Dim dir As String = stack.Pop

            result.Add(dir)

            ' Loop through all subdirectories and add them to the stack.
            Try
                Dim directoryName As String
                For Each directoryName In Directory.GetDirectories(dir)
                    stack.Push(directoryName)
                Next
            Catch ex As Exception

            End Try
        Loop

        ' Return the list
        Return result
    End Function

End Class
