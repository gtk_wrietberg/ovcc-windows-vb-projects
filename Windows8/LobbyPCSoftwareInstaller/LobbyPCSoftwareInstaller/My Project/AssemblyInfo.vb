Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCCSoftwareInstaller")> 
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Guest-Tek")>
<Assembly: AssemblyProduct("OVCCSoftwareInstaller")>
<Assembly: AssemblyCopyright("Copyright ©  2016-2024")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("0283416e-96fe-42e0-9b36-a066527fa260")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("34.5.24282.1407")>
<Assembly: AssemblyFileVersion("34.5.24282.1407")>