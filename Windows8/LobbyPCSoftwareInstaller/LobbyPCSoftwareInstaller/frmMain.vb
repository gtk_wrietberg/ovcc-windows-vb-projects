﻿Imports System.Net.NetworkInformation
Imports System.Text
Imports System.Threading

Public Class frmMain
    Private WithEvents oProcess As New ProcessRunner
    Private oComputerName As ComputerName
    Private installThread As Threading.Thread

    Public iApplicationInstallationCount As Integer = 0
    Public iApplicationInstallationCountMax As Integer = 12

    Public iProgress As Integer = 0
    Public iProgressMax As Integer = 15

    Public iActiveGroupBox As Integer


    Public Enum InstallationOrder As Integer
        ResetMonitorIdentifier = 1
        VisualCplusplusRedistributable
        CreateAdminAccount
        OVCCMonitor
        SiteKiosk
        CopySiteKioskInstallLog
        CheckSiteKioskInstallation
        SiteKioskUpdate
        SiteKioskConfigUpdate
        LogoutCleanupHelper
        OVCCFileExplorer
        AirlineUpdater
        SystemSecurityUpdater
        StopEdgeFirstRun
        OVCCWatchdog
        SiteKioskServiceConfiguration
        Chrome
        Pinta
        Shortcuts
        RemoveLegalNoticeCaption
        ERROR_ERROR_ERROR
    End Enum


#Region "Thread Safe stuff"
    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Public Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text])
        End If
    End Sub
#End Region


    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

#Region " ClientAreaMove Handling "
    'Private Const WM_NCHITTEST As Integer = &H84
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    'Private Const HTCLIENT As Integer = &H1
    Private Const HTCAPTION As Integer = &H2
    '    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
    '        Select Case m.Msg
    '            Case WM_NCHITTEST
    '                MyBase.WndProc(m)
    '    'If m.Result = HTCLIENT Then m.Result = HTCAPTION
    '                If m.Result.ToInt32 = HTCLIENT Then m.Result = IntPtr.op_Explicit(HTCAPTION) 'Try this in VS.NET 2002/2003 if the latter line of code doesn't do it... thx to Suhas for the tip.
    '            Case Else
    '    'Make sure you pass unhandled messages back to the default message handler.
    '                MyBase.WndProc(m)
    '        End Select
    '    End Sub
#End Region

#Region "Form Events"
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Don't close the form when we're still busy
        e.Cancel = gBusyInstalling
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActivatePrevInstance(Me.Text)

        gAllowRunning = Microsoft.Win32.Registry.GetValue(cREGKEY_LPCSOFTWARE, cREGVALUE__RUNFROMINSTALLER, "no")
        Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE, cREGVALUE__RUNFROMINSTALLER, "")


        oComputerName = New ComputerName
        oSettings = New Settings
        oLogger = New Logger


        If InStr(oComputerName.ComputerName.ToLower, "rietberg".ToLower) > 0 _
            Or InStr(oComputerName.ComputerName.ToLower, "wrdev".ToLower) > 0 _
            Or InStr(oComputerName.ComputerName.ToLower, "bafstaaf".ToLower) > 0 _
            Or InStr(oComputerName.ComputerName.ToLower, "superlekkerding".ToLower) > 0 Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If
        End If

        'GUI
        Me.StartPosition = FormStartPosition.CenterScreen

        'Me.Left = (My.Computer.Screen.Bounds.Width - Me.Width) / 2
        'Me.Top = (My.Computer.Screen.Bounds.Height - Me.Height) / 2

        Me.Opacity = 1.0
        gHideProgress = False

        If IO.Directory.Exists(cPATHS_GuestTekProgramFilesFolder_x86) Then
            gGuestTekProgramFilesFolder = cPATHS_GuestTekProgramFilesFolder_x86
        Else
            gGuestTekProgramFilesFolder = cPATHS_GuestTekProgramFilesFolder
        End If

        If Not IO.Directory.Exists(gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder) Then
            IO.Directory.CreateDirectory(gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder)
        End If

        oLogger.LogFilePath = gGuestTekProgramFilesFolder & "\"

        txtProgress.Clear()
        Me.ControlBox = False
        txtProgress.Font = System.Drawing.SystemFonts.MessageBoxFont


        LogEvent(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = "v" & myBuildInfo.FileVersion

        LogEvent(Application.ProductName & " v" & myBuildInfo.FileVersion)
        LogEvent("Computer name: " & oComputerName.ComputerName)


        If gTestMode Then
            LogEvent("Running in test mode!!!", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        End If


        gBusyInstalling = False


        'Install count max
        'iApplicationInstallationCountMax = InstallationOrder.SiteKioskSecurityUpdater + 1
        'iApplicationInstallationCountMax = [Enum].GetValues(GetType(InstallationOrder)).Cast(Of InstallationOrder)().Max() + 1
        iApplicationInstallationCountMax = ([Enum].GetValues(GetType(InstallationOrder)).Cast(Of InstallationOrder)().Max() + 1) - 1 '-1 because we don't count the error part


        gSiteKioskInstallDir = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk", "InstallDir", "")
        If gSiteKioskInstallDir Is Nothing Then
            gSiteKioskInstallDir = "c:\Program Files (x86)\SiteKiosk"
        End If
        gMonitorIdentifier = IO.Path.Combine(DirectoriesFiles.Directories.GetGuestTekFolder(), "OVCCSoftware\_monitor_identifier.id")
        g_iBahnFunctions_js = IO.Path.Combine(gSiteKioskInstallDir, "Skins\Public\iBAHN\Scripts\iBAHN_functions.js")


        tmrStartDeployment.Enabled = True
    End Sub
#End Region

#Region "Private Functions"
#Region "Logging"
    Private Sub LogEvent(ByVal sMessage As String, Optional ByVal cMessageType As Logger.MESSAGE_TYPE = Logger.MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bProgressWindowOnly As Boolean = False)
        Dim sLogText As String

        sLogText = oLogger.WriteToLog(sMessage, cMessageType, iDepth, bProgressWindowOnly)

        If bProgressWindowOnly Then
            LogEventToProgressTextBox(sLogText)
        Else
            LogEventToProgressTextBox(sLogText & vbCrLf)
        End If
    End Sub

    Private Sub LogEventToProgressTextBox(ByVal sMessage As String)
        LogToProcessTextBox(sMessage)
    End Sub
#End Region

#Region "Deployment and Installers"
#Region "Deployment"
    Private Sub StartDeployment(Optional bReloaded As Boolean = False)
        CheckCommandLineParameters()


        '-------------------
        If gAllowRunning <> "yes" And Not gSkipManualRunStepCheckThingy Then
            LogEvent("This installer should not be run manually", Logger.MESSAGE_TYPE.LOG_ERROR, 0)
            LogEvent("Please use the OVCCSoftwareDownloader", Logger.MESSAGE_TYPE.LOG_ERROR, 0)

            MessageBox.Show(Me, "This installer should not be run manually" & vbCrLf & vbCrLf & "please use the OVCCSoftwareDownloader", "Not for manual use", MessageBoxButtons.OK, MessageBoxIcon.Error)

            LogEvent("Installation cancelled!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            CloseApp()
            Exit Sub
        End If


        '--------------------------------------
        Dim sRet As String = ""

        If Not bReloaded Then
            LogEvent("Checking logged in user", Logger.MESSAGE_TYPE.LOG_DEFAULT)
            LogEvent(cWindowsUser.GetLoggedInUser(sRet), Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            If sRet <> "" Then
                LogEvent("something failed getting the logged in user name (" & sRet & ")", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
            End If

            If cWindowsUser.IsUserLoggedIn(cADMIN_Username, sRet) Then
                Dim bContinue As Boolean = True

                If Not gSkipInstallationQuestion Then
                    If MsgBox("You are logged in as '" & cADMIN_Username & "'. This installer will not be able to update this account. This might cause problems with remote access or updating." & vbCrLf & vbCrLf & "Are you sure you want to continue?", vbYesNo, "Confirm installation") = MsgBoxResult.Yes Then
                        LogEvent("'" & cADMIN_Username & "' user logged in, user forced continue", Logger.MESSAGE_TYPE.LOG_WARNING, 3)
                    Else
                        bContinue = False
                    End If
                Else
                    bContinue = False
                End If


                If Not bContinue Then
                    LogEvent("Installation cancelled!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    LogEvent("'" & cADMIN_Username & "' user logged in, can not continue", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                    Done()

                    Exit Sub
                End If
            Else
                If sRet = "" Then
                    LogEvent("ok", Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                Else
                    LogEvent("something failed during the user check (" & sRet & ")", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                    LogEvent("but we'll continue anyway, it´s not serious enough to cancel", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                End If
            End If


            _CheckForExistingInstall()

            If gWeAreUpdating Then
                LogEvent("This is an update, storing settings", Logger.MESSAGE_TYPE.LOG_DEFAULT)
                iBahnFunctions_js.Load()

                LogEvent("HotelAddress", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
                LogEvent(iBahnFunctions_js.HotelAddress, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                LogEvent("HotelInformationUrl", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
                LogEvent(iBahnFunctions_js.HotelInformationUrl, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                LogEvent("InternetUrl", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
                LogEvent(iBahnFunctions_js.InternetUrl, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                LogEvent("MapUrl", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
                LogEvent(iBahnFunctions_js.MapUrl, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                LogEvent("ThemeIndex", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
                LogEvent(iBahnFunctions_js.ThemeIndex, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
                LogEvent("WeatherUrl", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
                LogEvent(iBahnFunctions_js.WeatherUrl, Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)


                frmConfiguration.Config_Theme_BrandIndex = iBahnFunctions_js.ThemeIndex

                frmConfiguration.Config_OldVersionWarning = gOldVersionWarning


                '----------------------------------------
                'Detect region and flag
                Dim sTmp_License As String = "", sTmp_EnglishFlag As String = ""

                sTmp_License = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCSoftware\Config", "License", "eu")
                sTmp_EnglishFlag = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCSoftware\Config", "EnglishFlag", "gb")

                If sTmp_License Is Nothing Then
                    sTmp_License = "eu"
                End If
                If sTmp_EnglishFlag Is Nothing Then
                    sTmp_EnglishFlag = "gb"
                End If

                If sTmp_License.ToLower.Equals("us") Then
                    If sTmp_EnglishFlag.ToLower.Equals("us") Then
                        frmConfiguration.Config_Region = "us"
                    Else
                        frmConfiguration.Config_Region = "canada"
                    End If
                Else
                    frmConfiguration.Config_Region = "emea"
                End If
            End If
        End If


        If gSkipInstallationQuestion Then
            _StartDeployment()
        Else
            'Show Configuration form
            Me.Opacity = 0.9

            frmConfiguration.StartPosition = FormStartPosition.CenterParent
            Dim configResponse As DialogResult = frmConfiguration.ShowDialog(Me)

            Me.Opacity = 1.0

            If configResponse = DialogResult.OK Then
                If frmConfiguration.Config_Region.ToLower.Equals("canada") Then
                    gLicenseRegion = "us"
                    gEnglishFlag = "gb"
                ElseIf frmConfiguration.Config_Region.ToLower.Equals("us") Then
                    gLicenseRegion = "us"
                    gEnglishFlag = "us"
                Else
                    gLicenseRegion = "eu"
                    gEnglishFlag = "gb"
                End If

                gForceBrandUI = frmConfiguration.Config_Theme_BrandIndex


                If gWeAreUpdating Then
                    If gForceBrandUI <> iBahnFunctions_js.ThemeIndex Then
                        Dim _brand_new As HotelBrands.HotelBrand = HotelBrands.Find(gForceBrandUI)
                        Dim _brand_existing As HotelBrands.HotelBrand = HotelBrands.Find(iBahnFunctions_js.ThemeIndex)

                        Dim _sbtmp As New StringBuilder
                        _sbtmp.Append("You selected a different theme from the one that was already installed:")
                        _sbtmp.Append(vbCrLf & vbCrLf)
                        _sbtmp.Append("Installed: " & _brand_existing.Name)
                        _sbtmp.Append(vbCrLf)
                        _sbtmp.Append("Selected: " & _brand_new.Name)
                        _sbtmp.Append(vbCrLf & vbCrLf)
                        _sbtmp.Append("Are you sure?")

                        If MsgBox(_sbtmp.ToString, vbYesNo, "Confirm theme selection") <> MsgBoxResult.Yes Then
                            'StartDeployment(True)
                            StartDeployment()
                            Exit Sub
                        Else
                            iBahnFunctions_js.ThemeIndex = gForceBrandUI
                        End If
                    End If
                End If


                _StartDeployment()
            Else
                LogEvent("Installation cancelled by user", Logger.MESSAGE_TYPE.LOG_WARNING)

                Done()

                Exit Sub
            End If
        End If
    End Sub

    Private Sub CheckCommandLineParameters()
        LogEvent("Loading deployment config")

        'LogEvent("default values", , 1)
        gRegion = "eu"
        'LogEvent("region: eu", , 2)
        gLicenseRegion = ""
        'LogEvent("license: equal to region", , 2)

        gSkipSkype = True
        gSkipInstallationQuestion = False
        gForceBrandUI = -1

        gSiteKioskUserPassword = cSITEKIOSK_PassWord

        gEnglishFlag = "gb"

        gSiteKioskShowProgress = False
        gSiteKioskInteractiveInstall = False


        If Environment.GetCommandLineArgs().Length > 1 Then
            LogEvent("loading command line arguments", , 1)
            For Each arg As String In Environment.GetCommandLineArgs()
                LogEvent("found: " & arg, , 2)

                If arg = "--skip-manual-run-step-check-thingy" Then
                    gSkipManualRunStepCheckThingy = True
                End If

                If InStr(arg, "-license=") > 0 Then
                    gLicenseRegion = arg.Replace("-license=", "")
                    LogEvent("license: " & gLicenseRegion, , 3)
                End If

                If InStr(arg, "-english-flag=") > 0 Then
                    gEnglishFlag = arg.Replace("-english-flag=", "")
                    LogEvent("english flag: " & gEnglishFlag, , 3)
                    If Not gEnglishFlag.Equals("gb") And Not gEnglishFlag.Equals("us") Then
                        LogEvent("defaulting to 'gb'", , 4)
                        gEnglishFlag = "gb"
                    End If
                End If

                If arg = "-eu" Or arg = "-emea" Then
                    gRegion = "eu"
                    LogEvent("region: " & gRegion, , 3)
                End If

                If arg = "-us" Or arg = "-yankees" Then
                    gRegion = "us"
                    LogEvent("region: " & gRegion, , 3)
                End If

                'If arg = "-hideprogress" Then
                '    gHideProgress = True

                '    Me.Opacity = 0.0

                '    LogEvent("hide progress: " & gHideProgress.ToString, , 3)
                'End If

                If arg = "-skipskype" Then
                    gSkipSkype = True

                    LogEvent("skip skype: " & gSkipSkype.ToString, , 3)
                End If

                'If arg = "-skipquestion" Then
                '    gSkipInstallationQuestion = True

                '    LogEvent("skip question", , 3)
                'End If

                If InStr(arg, "-ui_index=") Then
                    Dim _tmp As String = arg.Replace("-ui_index=", "")

                    LogEvent("force brand ui index: " & _tmp, , 3)

                    If Integer.TryParse(_tmp, gForceBrandUI) Then
                        If gForceBrandUI < 0 Or gForceBrandUI > 1000 Then
                            LogEvent("invalid index, defaulting to -1 (=ignored)", , 3)

                            gForceBrandUI = -1
                        End If
                    Else
                        LogEvent("invalid index, defaulting to -1 (=ignored)", , 3)

                        gForceBrandUI = -1
                    End If
                End If

                If InStr(arg, "-sitekiosk-user-password=") > 0 Then
                    gSiteKioskUserPassword = arg.Replace("-sitekiosk-user-password=", "")
                    LogEvent("Non default SiteKiosk user password: " & New String("*", gSiteKioskUserPassword.Length), , 3)
                End If

                If arg = "-sitekioskshow" Then
                    gSiteKioskShowProgress = True

                    oLogger.WriteToLog("show sitekiosk install progress", , 3)
                End If

                If arg = "-sitekioskinteractive" Then
                    gSiteKioskInteractiveInstall = True

                    oLogger.WriteToLog("sitekiosk install is interactive!", , 3)
                End If
            Next
        End If

        If gLicenseRegion = "" Then
            gLicenseRegion = gRegion
        End If
    End Sub

    Private Function LeashTheHound() As Boolean
        LogEvent("Muzzling Watchdog", , 1)

        Microsoft.Win32.Registry.SetValue(cREGKEY___Watchdog, cREGVALUE__DisabledUntilOVCCStart, "yes")


        Return True
    End Function

    Private Function UnleashTheHound() As Boolean
        LogEvent("Muzzling Watchdog", , 1)

        Microsoft.Win32.Registry.SetValue(cREGKEY___Watchdog, cREGVALUE__DisabledUntilOVCCStart, "no")

        Return True
    End Function

    Private Sub _StartDeployment()
        LogEvent("Storing deployment config in registry")
        LogEvent("blocked drives", , 1)
        Try
            Dim sDrivesToBeBlocked As String
            sDrivesToBeBlocked = GetDrivesToBeBlocked()
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\BlockedDrives", , 3)
            LogEvent("value: " & sDrivesToBeBlocked, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "BlockedDrives", sDrivesToBeBlocked)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        LogEvent("license", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\License", , 3)
            LogEvent("value: " & gLicenseRegion, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "License", gLicenseRegion)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        LogEvent("english flag", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\EnglishFlag", , 3)
            LogEvent("value: " & gEnglishFlag, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "EnglishFlag", gEnglishFlag)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        LogEvent("region", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\Region", , 3)
            LogEvent("value: " & gRegion, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "Region", gRegion)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        If gSkipSkype Then
            LogEvent("skip skype", , 1)
            Try
                LogEvent("Registry", , 2)
                LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\SkipSkype", , 3)
                LogEvent("value: yes", , 3)
                Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "SkipSkype", "yes")
            Catch ex As Exception
                LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If


        LogEvent("Backing up some stuff")

        LogEvent("Computer name", , 1)
        LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Backup\Computer Name", , 2)
        LogEvent("value: " & oComputerName.ComputerName, , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Backup", "Computer Name", oComputerName.ComputerName)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogEvent("Workgroup name", , 1)
        LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Backup\Workgroup Name", , 2)
        LogEvent("value: " & oComputerName.Workgroup, , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Backup", "Workgroup Name", oComputerName.Workgroup)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        LogEvent("Updating default user profile")
        LogEvent("Registry", , 1)
        LogEvent("key  : HKEY_USERS\.DEFAULT\Control Panel\PowerCfg\CurrentPowerPolicy", , 2)
        LogEvent("value: 2", , 2)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_USERS\.DEFAULT\Control Panel\PowerCfg", "CurrentPowerPolicy", "2")
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        'LogEvent("Updating User Agent")
        'LogEvent("Registry", , 1)
        'LogEvent("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", , 2)
        'LogEvent("value: Mozilla/5.0", , 2)
        'Try
        '    Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "", "Mozilla/5.0")
        'Catch ex As Exception
        '    LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        '    LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        '    LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        'End Try

        'LogEvent("key  : HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent\Version", , 2)
        'LogEvent("value: MSIE 9.0", , 2)
        'Try
        '    Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "Version", "MSIE 9.0")
        'Catch ex As Exception
        '    LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        '    LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        '    LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        'End Try


        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V HonorAutoRunSetting  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveTypeAutoRun  /T REG_DWORD  /D 0x000000ff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveAutoRun  /T REG_DWORD  /D 0x0003ffff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoAutoRun  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\SYSTEM\CurrentControlSet\Services\Cdrom" /V AutoRun  /T REG_DWORD  /D 0x00000000  /F
        'reg.exe ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping\Autorun.inf" /VE      /T REG_SZ    /D "@SYS:DoesNotExist" /F

        LogEvent("Disabling AutoRun")
        LogEvent("Registry", , 1)
        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!HonorAutoRunSetting", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "HonorAutoRunSetting", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoDriveTypeAutoRun", , 2)
        LogEvent("value: 255", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoDriveTypeAutoRun", 255)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoDriveAutoRun", , 2)
        LogEvent("value: 262143", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoDriveAutoRun", 262143)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER & "!NoAutoRun", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER, "NoAutoRun", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        LogEvent("key  : " & cREGKEY_POLICIES_EXPLORER2 & "!NoAutoplayfornonVolume", , 2)
        LogEvent("value: 1", , 2)
        Try
            Microsoft.Win32.Registry.SetValue(cREGKEY_POLICIES_EXPLORER2, "NoAutoplayfornonVolume", 1)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try




        LogEvent("Storing main installer version number")
        LogEvent("Registry", , 1)
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        LogEvent(cREGKEY_LPCSOFTWARE & "\Version", , 2)
        LogEvent("Installer = '" & myBuildInfo.FileVersion & "'", , 3)
        Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Version", "Installer", myBuildInfo.FileVersion)


        UnpackResources()


        LogEvent("license file", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\LicenseFile", , 3)
            LogEvent("value: " & oSettings.Path_SiteKioskLicensesFile, , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "LicenseFile", oSettings.Path_SiteKioskLicensesFile)
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        LogEvent("IsThisTheSoftwarePackage", , 1)
        Try
            LogEvent("Registry", , 2)
            LogEvent("key  : " & cREGKEY_LPCSOFTWARE & "\Config\IsThisTheSoftwarePackage", , 3)
            LogEvent("value: yes", , 3)
            Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE & "\Config", "IsThisTheSoftwarePackage", "yes")
        Catch ex As Exception
            LogEvent("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            LogEvent("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        'Make sure that existing Sitekiosk account uses correct password
        If gWeAreUpdating Then
            LogEvent("updating the SiteKioks user password", , 1)
            LogEvent("setting password to default", , 2)
            Dim sRet As String = cWindowsUser.ChangePassword(cSITEKIOSK_UserName, cSITEKIOSK_PassWord)
            If sRet = "ok" Then
                LogEvent("ok", , 3)
            Else
                LogEvent("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                LogEvent(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If
        End If


        LeashTheHound()


        NextInstallationStep()
    End Sub

    Private Sub PostDeployment()
        LogEvent("Post deployment steps")
        LogEvent("Registry", , 1)

        LogEvent(cREGKEY_LPCSOFTWARE, , 2)
        LogEvent("Restarted = 'no'", , 3)
        Microsoft.Win32.Registry.SetValue(cREGKEY_LPCSOFTWARE, "Restarted", "no")


        Done()
    End Sub


    Private Sub Done()
        LogEvent("DONE")

        CloseApp()
    End Sub

    Private Sub CloseApp()
        Me.ControlBox = True

        If gApplicationFatalErrorDescription.Equals("") Then
            LogEvent("This app will close automatically in " & Math.Round(tmrExit.Interval / 1000, 0).ToString & " seconds")
            tmrExit.Enabled = True
        End If
    End Sub

    Private Sub NextInstallationStep()
        'tmrInstallerWait.Enabled = False

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub InstallationErrorOccurred(sError As String)
        gApplicationFatalErrorDescription = sError

        iApplicationInstallationCount = InstallationOrder.ERROR_ERROR_ERROR
        InstallationController()
    End Sub

    Private Sub InstallationController()
        'tmrInstallerWait.Enabled = False
        'oProcess = New ProcessRunner

        LogEvent("Installation step: " & CStr(iApplicationInstallationCount) & "/" & iApplicationInstallationCountMax, Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

        If iApplicationInstallationCount < iApplicationInstallationCountMax Then
            tmrInstallerWait.Enabled = True
        End If

        If gTestMode Then
            'iApplicationInstallationCount = iApplicationInstallationCountMax
        End If


        'If iApplicationInstallationCount = InstallationOrder.LogMeIn And gSkipLogMeIn Then
        '    'Skip LogMeIn 
        '    iApplicationInstallationCount += 1
        'End If

        If iApplicationInstallationCount = InstallationOrder.ERROR_ERROR_ERROR Then
            If gApplicationFatalErrorDescription.Equals("") Then
                iApplicationInstallationCount += 1
            End If
        End If


        Select Case iApplicationInstallationCount
            Case InstallationOrder.VisualCplusplusRedistributable
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallVisualCplusplusRedistributable))
                Me.installThread.Start()
            Case InstallationOrder.CreateAdminAccount
                _CreateAdminAccount()
            Case InstallationOrder.OVCCWatchdog
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallOVCCWatchdog))
                Me.installThread.Start()
            Case InstallationOrder.SiteKiosk
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKiosk))
                Me.installThread.Start()
            Case InstallationOrder.CopySiteKioskInstallLog
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._CopySiteKioskInstallLog))
                Me.installThread.Start()
            Case InstallationOrder.CheckSiteKioskInstallation
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._CheckSiteKioskInstallation))
                Me.installThread.Start()
            Case InstallationOrder.SiteKioskUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKioskUpdate))
                Me.installThread.Start()
            Case InstallationOrder.SiteKioskConfigUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._SiteKioskConfigUpdate))
                Me.installThread.Start()
            Case InstallationOrder.LogoutCleanupHelper
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLogoutCleanupHelper))
                Me.installThread.Start()
            Case InstallationOrder.OVCCFileExplorer
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallOVCCFileExplorer))
                Me.installThread.Start()
            Case InstallationOrder.SystemSecurityUpdater
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._SystemSecurityUpdater))
                Me.installThread.Start()
            Case InstallationOrder.StopEdgeFirstRun
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._StopEdgeFirstRun))
                Me.installThread.Start()
            Case InstallationOrder.AirlineUpdater
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._AirlineUpdater))
                Me.installThread.Start()
            Case InstallationOrder.SiteKioskServiceConfiguration
                _SiteKioskServiceConfiguration()
            Case InstallationOrder.Chrome
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallChrome))
                Me.installThread.Start()
            Case InstallationOrder.Pinta
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallPinta))
                Me.installThread.Start()
            Case InstallationOrder.ResetMonitorIdentifier
                _ResetMonitorIdentifier()
            Case InstallationOrder.OVCCMonitor
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallOVCCMonitor))
                Me.installThread.Start()
            Case InstallationOrder.Shortcuts
                _CreateShortcuts()
            Case InstallationOrder.RemoveLegalNoticeCaption
                _RemoveLegalNoticeCaption()
            Case InstallationOrder.ERROR_ERROR_ERROR
                _ShowError()
            Case Else
                LogEvent("Done", , 0)

                gBusyInstalling = False

                PostDeployment()
        End Select
    End Sub
#End Region

#Region "Installers"
    Private Sub _RemoveLegalNoticeCaption()
        LogEvent("Creating shortcut for OVCCStarter", , 1)
        AppShortcut.CreateOnDesktop(oSettings.Path_AutoStart, "Start OVCC")
        AppShortcut.CreateInStartMenu(oSettings.Path_AutoStart, "Start OVCC")

        LogEvent("Creating shortcut for OVCCInformationUpdater", , 1)
        AppShortcut.CreateOnDesktop(oSettings.Path_HotelPageUpdater, "OVCC - InformationUpdater")

        NextInstallationStep()
    End Sub

    Private Sub _CheckForExistingInstall()
        Dim _ret As String = "", _startupType As ServiceBootFlag = ServiceBootFlag.UNDEFINED

        LogEvent("Checking for existing install", , 1)


        'check registry
        LogEvent("checking SiteKiosk version", , 2)

        Dim sVersion As String = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk", "Build", "0.0")
        If sVersion Is Nothing Then
            sVersion = "-1"
        End If


        Dim aVersion As String() = sVersion.Split(".")
        Dim sFirstChar As String = aVersion(0)
        Dim iVersion As Integer = -1

        If (Integer.TryParse(sFirstChar, iVersion)) Then

        End If

        If iVersion > 0 Then
            LogEvent("found: " & sVersion, , 3)

            If iVersion < 9 Then
                gOldVersionWarning = True
            End If

            LogEvent("checking config file", , 2)

            If IO.File.Exists(g_iBahnFunctions_js) Then
                LogEvent("ok", , 3)

                gWeAreUpdating = True
            Else
                LogEvent("not found", , 3)
            End If
        Else
            LogEvent("not found", , 3)
        End If
    End Sub

#Region "admin user creation"
    Private Sub _CreateAdminAccount()
        bgworkerAdminUserCreation.RunWorkerAsync()
    End Sub

    Private Sub bgworkerAdminUserCreation_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgworkerAdminUserCreation.DoWork
        Dim sRet As String = ""

        LogEvent("Create admin account", , 1)

        Dim sCurrentUser As String = Environment.UserName.ToLower

        LogEvent("user logged in: " & sCurrentUser, , 2)
        If cWindowsUser.IsUserLoggedIn(cADMIN_Username, sRet) Then
            LogEvent("and that might be a problem.", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent("I can't change stuff for '" & cADMIN_Username & "' when (s)he's logged in!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Exit Sub
        Else
            If sRet <> "" Then
                LogEvent("fail:", , 3)
                LogEvent(sRet, , 4)

                Exit Sub
            Else
                LogEvent("ok", , 3)
            End If
        End If

        Dim sGroupName_Administrators As String, sGroupName_Users As String, sPasswordUsed As String
        LogEvent("Get group names", , 2)

        sGroupName_Administrators = cWindowsUser.GetAdministratorsGroupName
        LogEvent("Administrators = '" & sGroupName_Administrators & "'", , 3)

        sGroupName_Users = cWindowsUser.GetUsersGroupName
        LogEvent("Users = '" & sGroupName_Users & "'", , 3)


        LogEvent("Password used", , 2)
        If gForceBrandUI >= 0 And gForceBrandUI < 100 Then
            sPasswordUsed = cADMIN_Password__Hilton
            LogEvent("hilton", , 3)
        Else
            sPasswordUsed = cADMIN_Password
            LogEvent("standard", , 3)
        End If


        'Check if user exists
        LogEvent("Does user '" & cADMIN_Username & "' exist?", , 2)
        If Not cWindowsUser.DoesUserExist(cADMIN_Username, sRet) Then
            LogEvent("nope ('" & sRet & "')", , 3)
            LogEvent("creating", , 3)

            sRet = cWindowsUser.AddAdminUser(cADMIN_Username, "GuestTek admin user", sPasswordUsed)
            If sRet = "ok" Then
                LogEvent("ok", , 4)
            Else
                LogEvent("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                LogEvent(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End If
        Else
            LogEvent("yep ('" & sRet & "')", , 3)

            LogEvent("But is he an admin?", , 2)
            If cWindowsUser.IsUserAdmin(cADMIN_Username, sRet) Then
                LogEvent("yes! ('" & sRet & "')", , 3)
            Else
                LogEvent("nope ('" & sRet & "')", , 3)
                LogEvent("adding to admin group", , 2)

                sRet = cWindowsUser.PromoteUserToAdmin(cADMIN_Username)
                If sRet = "ok" Then
                    LogEvent("ok", , 3)
                Else
                    LogEvent("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    LogEvent(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            End If

            LogEvent("updating the password", , 2)
            LogEvent("setting password to default", , 3)
            sRet = cWindowsUser.ChangePassword(cADMIN_Username, sPasswordUsed)
            If sRet = "ok" Then
                LogEvent("ok", , 3)
            Else
                LogEvent("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                LogEvent(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If
        End If

        LogEvent("remove password expiry", , 3)
        sRet = cWindowsUser.RemovePasswordExpiry(cADMIN_Username)
        If sRet = "ok" Then
            LogEvent("ok", , 4)
        Else
            LogEvent("error:", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            LogEvent(sRet, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
        End If
    End Sub

    Private Sub bgworkerAdminUserCreation_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgworkerAdminUserCreation.RunWorkerCompleted
        NextInstallationStep()
    End Sub
#End Region

    Private Sub _SiteKioskServiceConfiguration()
        Dim _ret As String = "", _startupType As ServiceBootFlag = ServiceBootFlag.UNDEFINED

        LogEvent("Changing start type for SiteKiosk SiteRemote service ('" & cSITEKIOSK_ServiceName & "')", , 1)
        LogEvent("current startup type", , 2)

        _ret = ServiceInstaller.GetStartupType(cSITEKIOSK_ServiceName, _startupType)

        If _ret.Equals("") Then
            LogEvent(_startupType.ToString, , 3)
        Else
            LogEvent("error", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            LogEvent(_ret, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End If


        LogEvent("setting startup type", , 2)
        LogEvent("DelayedAutoStart", , 3)

        _ret = ServiceInstaller.ChangeStartupType(cSITEKIOSK_ServiceName, ServiceBootFlag.DelayedAutoStart)

        If _ret.Equals("") Then
            LogEvent("ok", , 4)
        Else
            LogEvent("error", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            LogEvent(_ret, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
        End If


        NextInstallationStep()
    End Sub

    Private Sub _ResetMonitorIdentifier()
        LogEvent("Removing Monitor identifier file", , 1)

        LogEvent(gMonitorIdentifier, , 2)
        If IO.File.Exists(gMonitorIdentifier) Then
            LogEvent("deleting", , 3)
            Try
                IO.File.Delete(gMonitorIdentifier)

                LogEvent("ok", , 4)
            Catch ex As Exception
                LogEvent("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                LogEvent(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End Try
        Else
            LogEvent("not found", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If


        NextInstallationStep()
    End Sub

    Private Sub _ShowError()
        If Not gApplicationFatalErrorDescription.Equals("") Then
            LogEvent("FATAL ERROR !!! FATAL ERROR !!! FATAL ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 0)
            LogEvent(gApplicationFatalErrorDescription, Logger.MESSAGE_TYPE.LOG_ERROR, 1)

            MessageBox.Show("FATAL ERROR", gApplicationFatalErrorDescription, MessageBoxButtons.OK)
        End If

        Done()
    End Sub

    Private Sub _InstallVisualCplusplusRedistributable()
        If gSkipCplusplusThing Then
            LogEvent("Installing VisualCplusplusRedistributable SKIPPED!!")

            NextInstallationStep()
        Else
            Dim sParams As String = "/q /norestart"

            LogEvent("Installing Visual C++ redistributable")
            LogEvent("Path   : " & oSettings.Path_VisualCplusplusRedistributable, , 1)
            LogEvent("Params : " & sParams, , 1)
            LogEvent("Timeout: 1200", , 1)

            oProcess = New ProcessRunner
            oProcess.FileName = oSettings.Path_VisualCplusplusRedistributable
            oProcess.Arguments = sParams
            oProcess.MaxTimeout = 1200
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()
        End If
    End Sub

    Private Sub _InstallOVCCWatchdog()
        LogEvent("Installing OVCC Watchdog service")
        LogEvent("Path   : " & oSettings.Path_LobbyPCWatchdog, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCWatchdog
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "OVCCWatchdog_installer"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallChrome()
        LogEvent("Installing Chrome")
        LogEvent("Path   : " & oSettings.Path_ChromeInstaller, , 1)
        LogEvent("Params : --installer-action:62", , 1)
        LogEvent("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_ChromeInstaller
        oProcess.Arguments = "--installer-action:62" 'do everything but restarting sitekiosk
        oProcess.ExternalAppToWaitFor = ""
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallPinta()
        LogEvent("Installing Pinta")
        LogEvent("Path   : " & oSettings.Path_PintaInstaller, , 1)
        LogEvent("Params : /install /silent", , 1)
        LogEvent("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_PintaInstaller
        oProcess.Arguments = "/install /silent"
        oProcess.ExternalAppToWaitFor = ""
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _CreateShortcuts()
        LogEvent("Creating shortcut for OVCCStarter", , 1)
        AppShortcut.CreateOnDesktop(oSettings.Path_AutoStart, "Start OVCC")
        AppShortcut.CreateInStartMenu(oSettings.Path_AutoStart, "Start OVCC")

        LogEvent("Creating shortcut for OVCCInformationUpdater", , 1)
        AppShortcut.CreateOnDesktop(oSettings.Path_HotelPageUpdater, "OVCC - InformationUpdater")

        NextInstallationStep()
    End Sub

    Private Sub _InstallOVCCMonitor()
        LogEvent("Installing OVCCMonitor")
        LogEvent("Path   : " & oSettings.Path_OVCCMonitor, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_OVCCMonitor
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = ""
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKioskUpdate()
        Dim sParams As String = ""

        If gForceBrandUI >= 0 And gForceBrandUI < 100 Then
            sParams &= "-spforcehilton" 'theme stuff is handled by _SiteKioskConfigUpdate(), but we need this for password difference
        End If

        LogEvent("Installing SiteKiosk update files")
        LogEvent("Path   : " & oSettings.Path_SiteKioskUpdate, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKioskUpdate
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "SiteKioskUpdater"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _SiteKioskConfigUpdate()
        If gWeAreUpdating Then
            iBahnFunctions_js.Save()
        End If


        Dim sParams As String = ""

        If gForceBrandUI < 0 Then
            sParams &= "--quiet--brand:667"  'Default to Guest-Tek theme
        Else
            sParams &= "--quiet--brand:" & gForceBrandUI.ToString
        End If


        LogEvent("Updating SiteKiosk configuration")
        LogEvent("Path   : " & oSettings.Path_HotelPageUpdater, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_HotelPageUpdater
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLogoutCleanupHelper()
        Dim sParams As String = ""

        LogEvent("Installing LogoutCleanupHelper")
        LogEvent("Path   : " & oSettings.Path_LogoutCleanupHelper, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LogoutCleanupHelper
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "LogoutCleanupHelperInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallOVCCFileExplorer()
        Dim sParams As String = ""

        LogEvent("Installing FileExplorer")
        LogEvent("Path   : " & oSettings.Path_OVCCFileExplorer, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_OVCCFileExplorer
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "FileExplorerInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _StopEdgeFirstRun()
        Dim sParams As String = "--block-edge-first-run"

        LogEvent("Installing StopEdgeFirstRun")
        LogEvent("Path   : " & oSettings.Path_StopEdgeFirstRun, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_StopEdgeFirstRun
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _AirlineUpdater()
        Dim sParams As String = ""

        LogEvent("Installing AirlineUpdater")
        LogEvent("Path   : " & oSettings.Path_AirlineUpdater, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_AirlineUpdater
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "AirlineUpdater"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _SystemSecurityUpdater()
        LogEvent("Installing SiteKiosk System Security Update")
        LogEvent("Path   : " & oSettings.Path_SystemSecurityUpdater, , 1)
        LogEvent("Params : ", , 1)
        LogEvent("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SystemSecurityUpdater
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKiosk()
        Dim sParams As String = ""
        Dim sUpdateParam As String = ""

        If gWeAreUpdating Then
            sUpdateParam = " ADDLOCAL=ALL"
        End If

        'standard params for SK
        sParams = "/S /V""/norestart /qn" & sUpdateParam & " DEFAULTPASSWD=" & cSITEKIOSK_PassWord & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"

        If gSiteKioskShowProgress Then
            sParams = "/V""/norestart /qb" & sUpdateParam & " DEFAULTPASSWD=" & cSITEKIOSK_PassWord & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"
        End If
        If gSiteKioskInteractiveInstall Then
            sParams = "/V""/norestart " & sUpdateParam & " DEFAULTPASSWD=" & cSITEKIOSK_PassWord & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"
        End If

        'sParams = "/S /V""/qn DEFAULTPASSWD=" & cSITEKIOSK_PassWord & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"

        'Dim sParams As String = "/S /V""/qn""" 
        '/S /V"/qn DEFAULTPASSWD=12MyPasswd /l*v C:\log.txt"
        'Dim sParams As String = "/S /V""/qn DEFAULTPASSWD=" & gSiteKioskUserPassword & " /l*v " & cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName & """"

        LogEvent("Installing kiosk software")
        LogEvent("Path   : " & oSettings.Path_SiteKiosk, , 1)
        LogEvent("Params : " & sParams, , 1)
        LogEvent("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKiosk
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _CopySiteKioskInstallLog()
        LogEvent("Copying Sitekiosk install log")

        If CopyFile(cSITEKIOSK_InstallLogName, cSITEKIOSK_InstallLogPath, gGuestTekProgramFilesFolder, 1) Then
            LogEvent("ok", , 1)
        Else
            LogEvent("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            LogEvent("but we should still be able to find it here: ", , 2)
            LogEvent(cSITEKIOSK_InstallLogPath & cSITEKIOSK_InstallLogName, , 3)
        End If

        NextInstallationStep()
    End Sub

    Private Sub _CheckSiteKioskInstallation()
        LogEvent("Checking Sitekiosk install")
        LogEvent("counting some folders", , 1)

        Dim iCount As Integer = 0, sPath As String = ""

        If IO.Directory.Exists(cPATHS_SiteKioskProgramFilesFolder_x86) Then
            sPath = cPATHS_SiteKioskProgramFilesFolder_x86
        Else
            sPath = cPATHS_SiteKioskProgramFilesFolder
        End If


        iCount = FileCounter.GetFolderCount(sPath)

        LogEvent("there are " & iCount & " folders in '" & sPath & "'", , 2)

        If iCount < 666 Then
            'uh oh, seems sitekiosk is incomplete
            LogEvent("SiteKiosk did not install properly!", , 3)

            InstallationErrorOccurred("SiteKiosk did not install properly, installation cancelled!")

            Exit Sub
        Else
            LogEvent("looks ok", , 3)
        End If


        'check registry
        LogEvent("checking version", , 1)

        Dim sVersion As String = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Provisio\SiteKiosk", "Build", "0.0")
        Dim sFirstChar As String = sVersion.Substring(0, 1)
        Dim iVersion As Integer = -1

        If (Integer.TryParse(sFirstChar, iVersion)) Then
            LogEvent("version " & iVersion.ToString, , 2)

            If iVersion >= 9 Then
                LogEvent("looks ok", , 4)
            Else
                LogEvent("uh oh, wrong version!!!", , 4)

                InstallationErrorOccurred("Wrong version of SiteKiosk found, installation cancelled!")

                Exit Sub
            End If
        End If


        LogEvent("ok", , 1)

        NextInstallationStep()
    End Sub

#End Region
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationIsCanceled()
        Application.Exit()
        'gBusyInstalling = False

        'MsgBox("Installation cancelled by user", MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation, Application.ProductName)
    End Sub

    Private Sub ApplicationIsExited()
        LogEvent("Exiting application")

        Application.Exit()
    End Sub
#End Region
#End Region


#Region "Events"
#Region "DragWindow events"
    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub picboxOnTopbar_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Process Events"
    Private Sub oProcess_Busy(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Busy
        LogEventToProgressTextBox(".")
    End Sub

    Private Sub oProcess_Test(ByVal EventProcess As System.Diagnostics.Process) Handles oProcess.Test
        LogEvent("test", , 1)
    End Sub

    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("done", , 1)
        LogEvent("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("failed", , 1)
        LogEvent(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        LogEvent("started", , 1)
        LogEvent("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess.TimeOut
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("timed out (" & TimeoutCount.ToString & "/" & TimeoutCountMax.ToString & ")", , 1)

        'If MessageBox.Show("This process seems to take longer than expected! Do you want to continue ('retry') or cancel?", My.Application.Info.ProductName, MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation) = DialogResult.Retry Then
        '    'set extra timeout to 5 mins
        '    oProcess.MaxTimeout = 300
        '    oProcess.ResumeWaiting()
        'Else
        '    oProcess.ProcessDone()
        'End If

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_TimeOutFinal(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess.TimeOutFinal
        If oProcess.BusyTriggered Then
            LogEventToProgressTextBox(vbCrLf)
        End If
        LogEvent("timed out", , 1)

        oProcess.ProcessDone()
    End Sub
#End Region

#Region "Timer Events"
    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            oProcess = New ProcessRunner
            NextInstallationStep()
        End If
    End Sub

    Private Sub tmrStartDeployment_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrStartDeployment.Tick
        tmrStartDeployment.Enabled = False

        StartDeployment()
    End Sub

    Private Sub tmrExit_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrExit.Tick
        ApplicationIsExited()
    End Sub
#End Region

    Private Sub txtProgress_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        tmrExit.Enabled = False
    End Sub
#End Region
End Class
