﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmConfiguration
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.radInstallConfig__EMEA = New System.Windows.Forms.RadioButton()
        Me.gbInstallConfigRegion = New System.Windows.Forms.GroupBox()
        Me.radInstallConfig__CA = New System.Windows.Forms.RadioButton()
        Me.radInstallConfig__US = New System.Windows.Forms.RadioButton()
        Me.gbInstallConfigTheme = New System.Windows.Forms.GroupBox()
        Me.lstThemes = New System.Windows.Forms.ListBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.pnlWarning = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblWarning = New System.Windows.Forms.Label()
        Me.pnlWarningShadow = New System.Windows.Forms.Panel()
        Me.gbInstallConfigRegion.SuspendLayout()
        Me.gbInstallConfigTheme.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlWarning.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnOk
        '
        Me.btnOk.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Location = New System.Drawing.Point(12, 8)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(254, 39)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "Start installation"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(299, 8)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(254, 39)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'radInstallConfig__EMEA
        '
        Me.radInstallConfig__EMEA.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInstallConfig__EMEA.Location = New System.Drawing.Point(6, 24)
        Me.radInstallConfig__EMEA.Name = "radInstallConfig__EMEA"
        Me.radInstallConfig__EMEA.Size = New System.Drawing.Size(97, 27)
        Me.radInstallConfig__EMEA.TabIndex = 2
        Me.radInstallConfig__EMEA.TabStop = True
        Me.radInstallConfig__EMEA.Text = "EMEA"
        Me.radInstallConfig__EMEA.UseVisualStyleBackColor = True
        '
        'gbInstallConfigRegion
        '
        Me.gbInstallConfigRegion.Controls.Add(Me.radInstallConfig__CA)
        Me.gbInstallConfigRegion.Controls.Add(Me.radInstallConfig__US)
        Me.gbInstallConfigRegion.Controls.Add(Me.radInstallConfig__EMEA)
        Me.gbInstallConfigRegion.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInstallConfigRegion.Location = New System.Drawing.Point(432, 12)
        Me.gbInstallConfigRegion.Name = "gbInstallConfigRegion"
        Me.gbInstallConfigRegion.Size = New System.Drawing.Size(121, 133)
        Me.gbInstallConfigRegion.TabIndex = 8
        Me.gbInstallConfigRegion.TabStop = False
        Me.gbInstallConfigRegion.Text = "Region"
        '
        'radInstallConfig__CA
        '
        Me.radInstallConfig__CA.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInstallConfig__CA.Location = New System.Drawing.Point(6, 100)
        Me.radInstallConfig__CA.Name = "radInstallConfig__CA"
        Me.radInstallConfig__CA.Size = New System.Drawing.Size(97, 27)
        Me.radInstallConfig__CA.TabIndex = 4
        Me.radInstallConfig__CA.TabStop = True
        Me.radInstallConfig__CA.Text = "Canada"
        Me.radInstallConfig__CA.UseVisualStyleBackColor = True
        '
        'radInstallConfig__US
        '
        Me.radInstallConfig__US.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radInstallConfig__US.Location = New System.Drawing.Point(6, 62)
        Me.radInstallConfig__US.Name = "radInstallConfig__US"
        Me.radInstallConfig__US.Size = New System.Drawing.Size(97, 27)
        Me.radInstallConfig__US.TabIndex = 3
        Me.radInstallConfig__US.TabStop = True
        Me.radInstallConfig__US.Text = "US"
        Me.radInstallConfig__US.UseVisualStyleBackColor = True
        '
        'gbInstallConfigTheme
        '
        Me.gbInstallConfigTheme.Controls.Add(Me.lstThemes)
        Me.gbInstallConfigTheme.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInstallConfigTheme.Location = New System.Drawing.Point(12, 12)
        Me.gbInstallConfigTheme.Name = "gbInstallConfigTheme"
        Me.gbInstallConfigTheme.Size = New System.Drawing.Size(414, 133)
        Me.gbInstallConfigTheme.TabIndex = 9
        Me.gbInstallConfigTheme.TabStop = False
        Me.gbInstallConfigTheme.Text = "Theme"
        '
        'lstThemes
        '
        Me.lstThemes.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstThemes.FormattingEnabled = True
        Me.lstThemes.ItemHeight = 17
        Me.lstThemes.Location = New System.Drawing.Point(6, 21)
        Me.lstThemes.Name = "lstThemes"
        Me.lstThemes.Size = New System.Drawing.Size(402, 106)
        Me.lstThemes.TabIndex = 10
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnOk)
        Me.Panel2.Controls.Add(Me.btnCancel)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 152)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(564, 55)
        Me.Panel2.TabIndex = 11
        '
        'pnlWarning
        '
        Me.pnlWarning.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.pnlWarning.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWarning.Controls.Add(Me.Button1)
        Me.pnlWarning.Controls.Add(Me.lblWarning)
        Me.pnlWarning.Location = New System.Drawing.Point(52, 50)
        Me.pnlWarning.Name = "pnlWarning"
        Me.pnlWarning.Size = New System.Drawing.Size(462, 79)
        Me.pnlWarning.TabIndex = 12
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(205, 52)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(50, 20)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "OK"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblWarning
        '
        Me.lblWarning.ForeColor = System.Drawing.Color.White
        Me.lblWarning.Location = New System.Drawing.Point(5, 0)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(451, 49)
        Me.lblWarning.TabIndex = 0
        Me.lblWarning.Text = "Note: You are upgrading from a very old version. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Make sure that the correct the" &
    "me is selected."
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlWarningShadow
        '
        Me.pnlWarningShadow.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlWarningShadow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWarningShadow.Location = New System.Drawing.Point(56, 54)
        Me.pnlWarningShadow.Name = "pnlWarningShadow"
        Me.pnlWarningShadow.Size = New System.Drawing.Size(462, 79)
        Me.pnlWarningShadow.TabIndex = 13
        '
        'frmConfiguration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(564, 207)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlWarning)
        Me.Controls.Add(Me.pnlWarningShadow)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.gbInstallConfigTheme)
        Me.Controls.Add(Me.gbInstallConfigRegion)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmConfiguration"
        Me.Text = "OneView Connection Centre - software configuration"
        Me.gbInstallConfigRegion.ResumeLayout(False)
        Me.gbInstallConfigTheme.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.pnlWarning.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnOk As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents radInstallConfig__EMEA As RadioButton
    Friend WithEvents gbInstallConfigRegion As GroupBox
    Friend WithEvents radInstallConfig__CA As RadioButton
    Friend WithEvents radInstallConfig__US As RadioButton
    Friend WithEvents gbInstallConfigTheme As GroupBox
    Friend WithEvents lstThemes As ListBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents pnlWarning As Panel
    Friend WithEvents Button1 As Button
    Friend WithEvents lblWarning As Label
    Friend WithEvents pnlWarningShadow As Panel
End Class
