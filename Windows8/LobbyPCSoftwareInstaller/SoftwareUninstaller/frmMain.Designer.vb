<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.picboxLogo = New System.Windows.Forms.PictureBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.pnlPrerequisites = New System.Windows.Forms.Panel()
        Me.lblPrerequisites = New System.Windows.Forms.Label()
        Me.tmrInstallerWait = New System.Windows.Forms.Timer(Me.components)
        Me.btnRetry = New System.Windows.Forms.Button()
        Me.pnlOptionSelect = New System.Windows.Forms.Panel()
        Me.lblPrerequisitesRestart = New System.Windows.Forms.Label()
        Me.pnlRadioButtons = New System.Windows.Forms.Panel()
        Me.chkRemoveLocalUsers = New System.Windows.Forms.CheckBox()
        Me.chkRebootWhenDone = New System.Windows.Forms.CheckBox()
        Me.radUninstallOption_OnlyLogMeIn = New System.Windows.Forms.RadioButton()
        Me.radUninstallOption_AllButLogMeIn = New System.Windows.Forms.RadioButton()
        Me.radUninstallOption_All = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnlUninstallation = New System.Windows.Forms.Panel()
        Me.lblUninstalling = New System.Windows.Forms.Label()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.progressMarquee = New System.Windows.Forms.ProgressBar()
        Me.pnlError = New System.Windows.Forms.Panel()
        Me.lblGenericError = New System.Windows.Forms.Label()
        Me.pnlDone = New System.Windows.Forms.Panel()
        Me.lblDone = New System.Windows.Forms.Label()
        Me.pnlAreYouSure = New System.Windows.Forms.Panel()
        Me.chkRemoveLocalUsers_CONFIRM = New System.Windows.Forms.CheckBox()
        Me.lblAreYouSure = New System.Windows.Forms.Label()
        Me.tmrForcedReboot = New System.Windows.Forms.Timer(Me.components)
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.lblSubTitle = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlPrerequisites.SuspendLayout()
        Me.pnlOptionSelect.SuspendLayout()
        Me.pnlRadioButtons.SuspendLayout()
        Me.pnlUninstallation.SuspendLayout()
        Me.pnlError.SuspendLayout()
        Me.pnlDone.SuspendLayout()
        Me.pnlAreYouSure.SuspendLayout()
        Me.SuspendLayout()
        '
        'picboxLogo
        '
        Me.picboxLogo.BackColor = System.Drawing.Color.Transparent
        Me.picboxLogo.BackgroundImage = Global.OVCCSoftwareUninstaller.My.Resources.Resources.GuestTek_Header_Logo
        Me.picboxLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.picboxLogo.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.picboxLogo.Location = New System.Drawing.Point(8, 10)
        Me.picboxLogo.Name = "picboxLogo"
        Me.picboxLogo.Size = New System.Drawing.Size(197, 68)
        Me.picboxLogo.TabIndex = 28
        Me.picboxLogo.TabStop = False
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.btnCancel.Location = New System.Drawing.Point(8, 256)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(197, 36)
        Me.btnCancel.TabIndex = 30
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnNext
        '
        Me.btnNext.BackColor = System.Drawing.Color.Transparent
        Me.btnNext.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNext.ForeColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.btnNext.Location = New System.Drawing.Point(419, 256)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(197, 36)
        Me.btnNext.TabIndex = 31
        Me.btnNext.Text = "NEXT"
        Me.btnNext.UseVisualStyleBackColor = False
        '
        'pnlPrerequisites
        '
        Me.pnlPrerequisites.BackColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.pnlPrerequisites.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPrerequisites.Controls.Add(Me.lblPrerequisites)
        Me.pnlPrerequisites.Location = New System.Drawing.Point(8, 83)
        Me.pnlPrerequisites.Name = "pnlPrerequisites"
        Me.pnlPrerequisites.Size = New System.Drawing.Size(608, 167)
        Me.pnlPrerequisites.TabIndex = 33
        '
        'lblPrerequisites
        '
        Me.lblPrerequisites.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrerequisites.ForeColor = System.Drawing.Color.Maroon
        Me.lblPrerequisites.Location = New System.Drawing.Point(4, 4)
        Me.lblPrerequisites.Name = "lblPrerequisites"
        Me.lblPrerequisites.Size = New System.Drawing.Size(598, 161)
        Me.lblPrerequisites.TabIndex = 0
        Me.lblPrerequisites.Text = "Checking prerequisites"
        Me.lblPrerequisites.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tmrInstallerWait
        '
        Me.tmrInstallerWait.Interval = 1000
        '
        'btnRetry
        '
        Me.btnRetry.BackColor = System.Drawing.Color.Transparent
        Me.btnRetry.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetry.ForeColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(126, Byte), Integer), CType(CType(62, Byte), Integer))
        Me.btnRetry.Location = New System.Drawing.Point(214, 256)
        Me.btnRetry.Name = "btnRetry"
        Me.btnRetry.Size = New System.Drawing.Size(197, 36)
        Me.btnRetry.TabIndex = 37
        Me.btnRetry.Text = "RETRY"
        Me.btnRetry.UseVisualStyleBackColor = False
        '
        'pnlOptionSelect
        '
        Me.pnlOptionSelect.BackColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.pnlOptionSelect.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlOptionSelect.Controls.Add(Me.lblPrerequisitesRestart)
        Me.pnlOptionSelect.Controls.Add(Me.pnlRadioButtons)
        Me.pnlOptionSelect.Controls.Add(Me.Label1)
        Me.pnlOptionSelect.Location = New System.Drawing.Point(52, 381)
        Me.pnlOptionSelect.Name = "pnlOptionSelect"
        Me.pnlOptionSelect.Size = New System.Drawing.Size(608, 167)
        Me.pnlOptionSelect.TabIndex = 34
        '
        'lblPrerequisitesRestart
        '
        Me.lblPrerequisitesRestart.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrerequisitesRestart.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.lblPrerequisitesRestart.Location = New System.Drawing.Point(4, 148)
        Me.lblPrerequisitesRestart.Name = "lblPrerequisitesRestart"
        Me.lblPrerequisitesRestart.Size = New System.Drawing.Size(598, 16)
        Me.lblPrerequisitesRestart.TabIndex = 3
        Me.lblPrerequisitesRestart.Text = "Select CANCEL to exit the uninstallation."
        Me.lblPrerequisitesRestart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlRadioButtons
        '
        Me.pnlRadioButtons.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlRadioButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlRadioButtons.Controls.Add(Me.chkRemoveLocalUsers)
        Me.pnlRadioButtons.Controls.Add(Me.chkRebootWhenDone)
        Me.pnlRadioButtons.Controls.Add(Me.radUninstallOption_OnlyLogMeIn)
        Me.pnlRadioButtons.Controls.Add(Me.radUninstallOption_AllButLogMeIn)
        Me.pnlRadioButtons.Controls.Add(Me.radUninstallOption_All)
        Me.pnlRadioButtons.Location = New System.Drawing.Point(23, 19)
        Me.pnlRadioButtons.Name = "pnlRadioButtons"
        Me.pnlRadioButtons.Size = New System.Drawing.Size(563, 129)
        Me.pnlRadioButtons.TabIndex = 2
        '
        'chkRemoveLocalUsers
        '
        Me.chkRemoveLocalUsers.AutoSize = True
        Me.chkRemoveLocalUsers.Enabled = False
        Me.chkRemoveLocalUsers.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRemoveLocalUsers.ForeColor = System.Drawing.Color.Red
        Me.chkRemoveLocalUsers.Location = New System.Drawing.Point(20, 92)
        Me.chkRemoveLocalUsers.Name = "chkRemoveLocalUsers"
        Me.chkRemoveLocalUsers.Size = New System.Drawing.Size(205, 19)
        Me.chkRemoveLocalUsers.TabIndex = 42
        Me.chkRemoveLocalUsers.Text = "Delete local OVCC admin account"
        Me.chkRemoveLocalUsers.UseVisualStyleBackColor = True
        '
        'chkRebootWhenDone
        '
        Me.chkRebootWhenDone.AutoSize = True
        Me.chkRebootWhenDone.Checked = True
        Me.chkRebootWhenDone.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRebootWhenDone.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRebootWhenDone.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.chkRebootWhenDone.Location = New System.Drawing.Point(427, 92)
        Me.chkRebootWhenDone.Name = "chkRebootWhenDone"
        Me.chkRebootWhenDone.Size = New System.Drawing.Size(126, 19)
        Me.chkRebootWhenDone.TabIndex = 41
        Me.chkRebootWhenDone.Text = "Reboot when done"
        Me.chkRebootWhenDone.UseVisualStyleBackColor = True
        '
        'radUninstallOption_OnlyLogMeIn
        '
        Me.radUninstallOption_OnlyLogMeIn.AutoSize = True
        Me.radUninstallOption_OnlyLogMeIn.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radUninstallOption_OnlyLogMeIn.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.radUninstallOption_OnlyLogMeIn.Location = New System.Drawing.Point(20, 67)
        Me.radUninstallOption_OnlyLogMeIn.Name = "radUninstallOption_OnlyLogMeIn"
        Me.radUninstallOption_OnlyLogMeIn.Size = New System.Drawing.Size(100, 19)
        Me.radUninstallOption_OnlyLogMeIn.TabIndex = 40
        Me.radUninstallOption_OnlyLogMeIn.TabStop = True
        Me.radUninstallOption_OnlyLogMeIn.Text = "Only LogMeIn"
        Me.radUninstallOption_OnlyLogMeIn.UseVisualStyleBackColor = True
        '
        'radUninstallOption_AllButLogMeIn
        '
        Me.radUninstallOption_AllButLogMeIn.AutoSize = True
        Me.radUninstallOption_AllButLogMeIn.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radUninstallOption_AllButLogMeIn.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.radUninstallOption_AllButLogMeIn.Location = New System.Drawing.Point(20, 44)
        Me.radUninstallOption_AllButLogMeIn.Name = "radUninstallOption_AllButLogMeIn"
        Me.radUninstallOption_AllButLogMeIn.Size = New System.Drawing.Size(224, 19)
        Me.radUninstallOption_AllButLogMeIn.TabIndex = 39
        Me.radUninstallOption_AllButLogMeIn.TabStop = True
        Me.radUninstallOption_AllButLogMeIn.Text = "All OVCC software, but NOT LogMeIn"
        Me.radUninstallOption_AllButLogMeIn.UseVisualStyleBackColor = True
        '
        'radUninstallOption_All
        '
        Me.radUninstallOption_All.AutoSize = True
        Me.radUninstallOption_All.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radUninstallOption_All.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.radUninstallOption_All.Location = New System.Drawing.Point(20, 19)
        Me.radUninstallOption_All.Name = "radUninstallOption_All"
        Me.radUninstallOption_All.Size = New System.Drawing.Size(122, 19)
        Me.radUninstallOption_All.TabIndex = 38
        Me.radUninstallOption_All.TabStop = True
        Me.radUninstallOption_All.Text = "All OVCC software"
        Me.radUninstallOption_All.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(598, 23)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Select your uninstall option"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlUninstallation
        '
        Me.pnlUninstallation.BackColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.pnlUninstallation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlUninstallation.Controls.Add(Me.lblUninstalling)
        Me.pnlUninstallation.Controls.Add(Me.lblProgress)
        Me.pnlUninstallation.Controls.Add(Me.progressMarquee)
        Me.pnlUninstallation.Location = New System.Drawing.Point(709, 83)
        Me.pnlUninstallation.Name = "pnlUninstallation"
        Me.pnlUninstallation.Size = New System.Drawing.Size(608, 167)
        Me.pnlUninstallation.TabIndex = 38
        '
        'lblUninstalling
        '
        Me.lblUninstalling.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUninstalling.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.lblUninstalling.Location = New System.Drawing.Point(3, 0)
        Me.lblUninstalling.Name = "lblUninstalling"
        Me.lblUninstalling.Size = New System.Drawing.Size(600, 110)
        Me.lblUninstalling.TabIndex = 37
        Me.lblUninstalling.Text = "Removing stuff." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Please do not reboot or shutdown the pc."
        Me.lblUninstalling.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProgress
        '
        Me.lblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress.Location = New System.Drawing.Point(3, 146)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(600, 19)
        Me.lblProgress.TabIndex = 36
        Me.lblProgress.Text = "73/100"
        Me.lblProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'progressMarquee
        '
        Me.progressMarquee.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.progressMarquee.ForeColor = System.Drawing.Color.Firebrick
        Me.progressMarquee.Location = New System.Drawing.Point(6, 113)
        Me.progressMarquee.Name = "progressMarquee"
        Me.progressMarquee.Size = New System.Drawing.Size(597, 30)
        Me.progressMarquee.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressMarquee.TabIndex = 35
        Me.progressMarquee.UseWaitCursor = True
        '
        'pnlError
        '
        Me.pnlError.BackColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.pnlError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlError.Controls.Add(Me.lblGenericError)
        Me.pnlError.Location = New System.Drawing.Point(709, 335)
        Me.pnlError.Name = "pnlError"
        Me.pnlError.Size = New System.Drawing.Size(608, 167)
        Me.pnlError.TabIndex = 34
        '
        'lblGenericError
        '
        Me.lblGenericError.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGenericError.ForeColor = System.Drawing.Color.Maroon
        Me.lblGenericError.Location = New System.Drawing.Point(4, 4)
        Me.lblGenericError.Name = "lblGenericError"
        Me.lblGenericError.Size = New System.Drawing.Size(598, 161)
        Me.lblGenericError.TabIndex = 0
        Me.lblGenericError.Text = "Here come the error messages"
        Me.lblGenericError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlDone
        '
        Me.pnlDone.BackColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.pnlDone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlDone.Controls.Add(Me.lblDone)
        Me.pnlDone.Location = New System.Drawing.Point(709, 573)
        Me.pnlDone.Name = "pnlDone"
        Me.pnlDone.Size = New System.Drawing.Size(608, 167)
        Me.pnlDone.TabIndex = 35
        '
        'lblDone
        '
        Me.lblDone.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDone.ForeColor = System.Drawing.Color.Maroon
        Me.lblDone.Location = New System.Drawing.Point(4, 4)
        Me.lblDone.Name = "lblDone"
        Me.lblDone.Size = New System.Drawing.Size(598, 161)
        Me.lblDone.TabIndex = 0
        Me.lblDone.Text = "Done."
        Me.lblDone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlAreYouSure
        '
        Me.pnlAreYouSure.BackColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.pnlAreYouSure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlAreYouSure.Controls.Add(Me.chkRemoveLocalUsers_CONFIRM)
        Me.pnlAreYouSure.Controls.Add(Me.lblAreYouSure)
        Me.pnlAreYouSure.Location = New System.Drawing.Point(31, 599)
        Me.pnlAreYouSure.Name = "pnlAreYouSure"
        Me.pnlAreYouSure.Size = New System.Drawing.Size(608, 167)
        Me.pnlAreYouSure.TabIndex = 36
        '
        'chkRemoveLocalUsers_CONFIRM
        '
        Me.chkRemoveLocalUsers_CONFIRM.Enabled = False
        Me.chkRemoveLocalUsers_CONFIRM.Font = New System.Drawing.Font("Segoe UI Black", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRemoveLocalUsers_CONFIRM.ForeColor = System.Drawing.Color.Red
        Me.chkRemoveLocalUsers_CONFIRM.Location = New System.Drawing.Point(130, 142)
        Me.chkRemoveLocalUsers_CONFIRM.Name = "chkRemoveLocalUsers_CONFIRM"
        Me.chkRemoveLocalUsers_CONFIRM.Size = New System.Drawing.Size(347, 20)
        Me.chkRemoveLocalUsers_CONFIRM.TabIndex = 43
        Me.chkRemoveLocalUsers_CONFIRM.Text = "Yes, I want to delete the local OVCC admin account"
        Me.chkRemoveLocalUsers_CONFIRM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkRemoveLocalUsers_CONFIRM.UseVisualStyleBackColor = True
        '
        'lblAreYouSure
        '
        Me.lblAreYouSure.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAreYouSure.ForeColor = System.Drawing.Color.Maroon
        Me.lblAreYouSure.Location = New System.Drawing.Point(4, 4)
        Me.lblAreYouSure.Name = "lblAreYouSure"
        Me.lblAreYouSure.Size = New System.Drawing.Size(598, 135)
        Me.lblAreYouSure.TabIndex = 0
        Me.lblAreYouSure.Text = "Are you sure?"
        Me.lblAreYouSure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tmrForcedReboot
        '
        Me.tmrForcedReboot.Interval = 1000
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblVersion.Font = New System.Drawing.Font("Segoe UI", 6.5!)
        Me.lblVersion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.lblVersion.Location = New System.Drawing.Point(462, 63)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(154, 19)
        Me.lblVersion.TabIndex = 39
        Me.lblVersion.Text = "version number goes here"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblSubTitle
        '
        Me.lblSubTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblSubTitle.Font = New System.Drawing.Font("Segoe UI", 11.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSubTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(108, Byte), Integer), CType(CType(118, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.lblSubTitle.Location = New System.Drawing.Point(211, 45)
        Me.lblSubTitle.Name = "lblSubTitle"
        Me.lblSubTitle.Size = New System.Drawing.Size(405, 22)
        Me.lblSubTitle.TabIndex = 41
        Me.lblSubTitle.Text = "software uninstaller"
        Me.lblSubTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblTitle.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold)
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(211, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(405, 46)
        Me.lblTitle.TabIndex = 40
        Me.lblTitle.Text = "OneView Connection Centre"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1556, 877)
        Me.Controls.Add(Me.lblSubTitle)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.pnlAreYouSure)
        Me.Controls.Add(Me.pnlDone)
        Me.Controls.Add(Me.pnlError)
        Me.Controls.Add(Me.pnlUninstallation)
        Me.Controls.Add(Me.pnlOptionSelect)
        Me.Controls.Add(Me.btnRetry)
        Me.Controls.Add(Me.pnlPrerequisites)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.picboxLogo)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.Text = "OVCC uninstaller"
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlPrerequisites.ResumeLayout(False)
        Me.pnlOptionSelect.ResumeLayout(False)
        Me.pnlRadioButtons.ResumeLayout(False)
        Me.pnlRadioButtons.PerformLayout()
        Me.pnlUninstallation.ResumeLayout(False)
        Me.pnlError.ResumeLayout(False)
        Me.pnlDone.ResumeLayout(False)
        Me.pnlAreYouSure.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picboxLogo As System.Windows.Forms.PictureBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents pnlPrerequisites As System.Windows.Forms.Panel
    Friend WithEvents lblPrerequisites As System.Windows.Forms.Label
    Friend WithEvents tmrInstallerWait As System.Windows.Forms.Timer
    Friend WithEvents btnRetry As System.Windows.Forms.Button
    Friend WithEvents pnlOptionSelect As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pnlRadioButtons As System.Windows.Forms.Panel
    Friend WithEvents radUninstallOption_All As System.Windows.Forms.RadioButton
    Friend WithEvents radUninstallOption_AllButLogMeIn As System.Windows.Forms.RadioButton
    Friend WithEvents radUninstallOption_OnlyLogMeIn As System.Windows.Forms.RadioButton
    Friend WithEvents chkRebootWhenDone As System.Windows.Forms.CheckBox
    Friend WithEvents pnlUninstallation As System.Windows.Forms.Panel
    Friend WithEvents lblUninstalling As System.Windows.Forms.Label
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents progressMarquee As System.Windows.Forms.ProgressBar
    Friend WithEvents pnlError As System.Windows.Forms.Panel
    Friend WithEvents lblGenericError As System.Windows.Forms.Label
    Friend WithEvents pnlDone As System.Windows.Forms.Panel
    Friend WithEvents lblDone As System.Windows.Forms.Label
    Friend WithEvents lblPrerequisitesRestart As System.Windows.Forms.Label
    Friend WithEvents pnlAreYouSure As System.Windows.Forms.Panel
    Friend WithEvents lblAreYouSure As System.Windows.Forms.Label
    Friend WithEvents chkRemoveLocalUsers As System.Windows.Forms.CheckBox
    Friend WithEvents tmrForcedReboot As System.Windows.Forms.Timer
    Friend WithEvents chkRemoveLocalUsers_CONFIRM As CheckBox
    Friend WithEvents lblVersion As Label
    Friend WithEvents lblSubTitle As Label
    Friend WithEvents lblTitle As Label
End Class
