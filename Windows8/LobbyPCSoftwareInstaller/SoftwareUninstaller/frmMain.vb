Imports System.Threading

Public Class frmMain
    Private oPrerequisites As Prerequisites
    Private WithEvents oProcess As ProcessRunner
    Private oComputerName As ComputerName
    Private Panels() As Panel
    Private iActiveGroupBox As Integer

    Private installThread As Thread

    Private iApplicationInstallationCount As InstallationProgress = 0
    Private iApplicationInstallationCountMax As InstallationProgress = InstallationProgress.Done

    Private bSiteKioskUninstallTimeout As Boolean = False

    Private iSeriousErrorCount As Integer = 0

    Private iUninstallOption As UninstallOptions = UninstallOptions.None
    Private p_RebootWhenDone As Boolean = False
    Private p_RemoveOVCCAdminUser As Boolean = False

    Private Enum UninstallOptions As Integer
        None = 0
        All
        AllButLogMeIn
        OnlyLogMeIn
    End Enum

    Private Enum GroupBoxes As Integer
        Prerequisites = 0
        OptionSelect
        AreYouSure
        Uninstallation
        ErrorScreen
        Done
    End Enum

    Private Enum InstallationProgress As Integer
        StopServices = 0
        UninstallSiteKiosk
        UninstallSiteKioskBruteForce
        UninstallAltiris
        CleanUpUser_SiteKiosk
        CleanUpUser_Puppy
        CleanUpUser_Admin
        CleanUpRegistry
        CleanUpFiles
        UninstallLogMeIn
        UninstallLogMeInClient
        Done
    End Enum

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Panels = New Panel() {Me.pnlPrerequisites, Me.pnlOptionSelect, Me.pnlAreYouSure, Me.pnlUninstallation, Me.pnlError, Me.pnlDone}
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gDebug = True
        gAllowBruteForce = True

        Me.Width = 640
        Me.Height = 336

        oPrerequisites = New Prerequisites
        oComputerName = New ComputerName
        oLogger = New Logger


        'oLogger.LogFilePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) & "\"
        oLogger.LogFilePath = "C:\"
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        lblVersion.Text = "v" & myBuildInfo.FileVersion


        If InStr(oComputerName.ComputerName.ToLower, "rietberg".ToLower) > 0 _
          Or InStr(oComputerName.ComputerName.ToLower, "wrdev".ToLower) > 0 _
          Or InStr(oComputerName.ComputerName.ToLower, "superlekkerding".ToLower) > 0 Then
            gTestMode = True

            If MessageBox.Show("Test mode! Continue?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If
        End If

        iApplicationInstallationCountMax = InstallationProgress.Done


        Dim sTmpLicenseKey As String

        sTmpLicenseKey = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & cREGKEY_LPCSOFTWARE, "LicenseKey", "")

        If sTmpLicenseKey Is Nothing Then
            sTmpLicenseKey = ""
        End If

        gLicenseKeyFromRegistry = sTmpLicenseKey


        Dim sArg As String, sTmp As String = ""

        oLogger.WriteToLog("Command line parameters", , 0)
        For Each sArg In My.Application.CommandLineArgs
            sArg = sArg.ToLower

            oLogger.WriteToLog("found", , 1)
            oLogger.WriteToLog(sArg, , 2)

            If sArg = "--debug" Then
                gDebug = True
            End If
            If sArg = "--allow-brute-force" Then
                gAllowBruteForce = True
            End If
            If sArg = "--help" Or sArg = "/?" Or sArg = "-help" Or sArg = "-?" Or sArg = "-h" Then
                ShowHelpMessageBox()
            End If
            If sArg.StartsWith("--unattended:") Then
                sTmp = sArg.Replace("--unattended:", "")

                gUnattendedUninstall = True

                Select Case sTmp
                    Case "all"
                        iUninstallOption = UninstallOptions.All
                    Case "skiplogmein"
                        iUninstallOption = UninstallOptions.AllButLogMeIn
                    Case "logmein"
                        iUninstallOption = UninstallOptions.OnlyLogMeIn
                    Case Else
                        gUnattendedUninstall = False
                End Select
            End If
            If sArg = "--force-reboot" Then
                gUnattendedForcedReboot = True
            End If
            If sArg = "--delete-local-users" Then
                gUnattendedRemoveLocalUsers = True
            End If
        Next

        If gUnattendedUninstall Then
            If gUnattendedForcedReboot Then
                p_RebootWhenDone = True
            End If
            If gUnattendedRemoveLocalUsers Then
                If Not iUninstallOption = UninstallOptions.All Then
                    p_RemoveOVCCAdminUser = False
                    oLogger.WriteToLog("ignoring --delete-local-users", , 0)
                Else
                    p_RemoveOVCCAdminUser = True
                End If
            End If
        End If




        'Panels
        Dim i As Integer
        For i = 0 To Panels.Length - 1
            Panels(i).Top = 83
            Panels(i).Left = 8

            Panels(i).Visible = False
        Next

        iApplicationInstallationCount = 0
        UpdateInstallationProgressCount()

        If gUnattendedUninstall Then
            iActiveGroupBox = GroupBoxes.Uninstallation - 1
        Else
            iActiveGroupBox = -1
        End If


        NextGroupBox()
    End Sub

    Private Sub ShowHelpMessageBox()
        Dim sMessage As String = ""

        sMessage &= "Command line parameters:" & vbCrLf & vbCrLf
        sMessage &= "  --help" & vbCrLf
        sMessage &= "  -help" & vbCrLf
        sMessage &= "  -?" & vbCrLf
        sMessage &= "  /?" & vbCrLf
        sMessage &= "      This information, but you obviously already knew that" & vbCrLf & vbCrLf
        sMessage &= "  --unattended:XXX" & vbCrLf
        sMessage &= "      Run without UI. XXX can be one of the following:" & vbCrLf
        sMessage &= "          all" & vbCrLf
        sMessage &= "              uninstall everything OVCC" & vbCrLf
        sMessage &= "          skiplogmein" & vbCrLf
        sMessage &= "              uninstall everything OVCC, but leave LogMeIn" & vbCrLf
        sMessage &= "          logmein" & vbCrLf
        sMessage &= "              uninstall LogMeIn only" & vbCrLf & vbCrLf
        sMessage &= "  --delete-local-users" & vbCrLf
        sMessage &= "      Remove the local OVCC users. WARNING: You will have no access to the machine anymore!!!" & vbCrLf
        sMessage &= "      Only does something when --unattended:all is used." & vbCrLf
        sMessage &= "  --force-reboot" & vbCrLf
        sMessage &= "      Force a reboot after an unattended uninstall." & vbCrLf
        sMessage &= "      Only does something when --unattended is used." & vbCrLf


        MessageBox.Show(sMessage, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Application.Exit()
    End Sub

#Region "Uninstallers"
    Private Sub StartUninstallation()
        InstallationController()
    End Sub

    Private Sub InstallationController()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner

        oLogger.WriteToLog("Installation step: " & CStr(iApplicationInstallationCount) & " - " & iApplicationInstallationCount.ToString, Logger.MESSAGE_TYPE.LOG_DEBUG, 0)
        UpdateInstallationProgressCount()

        If Not p_RemoveOVCCAdminUser Then
            If iApplicationInstallationCount = InstallationProgress.CleanUpUser_Admin Then
                oLogger.WriteToLog("SKIPPED!!!", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)

                iApplicationInstallationCount += 1
                InstallationController()
                Exit Sub
            End If
        End If

        If iUninstallOption = UninstallOptions.AllButLogMeIn Then
            If iApplicationInstallationCount = InstallationProgress.UninstallLogMeIn _
              Or iApplicationInstallationCount = InstallationProgress.UninstallLogMeInClient Then
                oLogger.WriteToLog("SKIPPED!!!", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)

                iApplicationInstallationCount += 1
                InstallationController()
                Exit Sub
            End If
        End If

        If iUninstallOption = UninstallOptions.OnlyLogMeIn Then
            If iApplicationInstallationCount = InstallationProgress.CleanUpFiles _
                Or iApplicationInstallationCount = InstallationProgress.CleanUpUser_Admin _
                Or iApplicationInstallationCount = InstallationProgress.CleanUpUser_Puppy _
                Or iApplicationInstallationCount = InstallationProgress.CleanUpUser_SiteKiosk _
                Or iApplicationInstallationCount = InstallationProgress.CleanUpRegistry _
                Or iApplicationInstallationCount = InstallationProgress.StopServices _
                Or iApplicationInstallationCount = InstallationProgress.UninstallAltiris _
                Or iApplicationInstallationCount = InstallationProgress.UninstallSiteKiosk _
                Or iApplicationInstallationCount = InstallationProgress.UninstallSiteKioskBruteForce Then

                oLogger.WriteToLog("SKIPPED!!!", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)

                iApplicationInstallationCount += 1
                InstallationController()
                Exit Sub
            End If
        End If

        If iUninstallOption = UninstallOptions.None Then
            iApplicationInstallationCount = InstallationProgress.Done
        End If



        If gTestMode Then
            oLogger.WriteToLog("_FakeUninstallStep()", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)


            If iApplicationInstallationCount >= InstallationProgress.Done Then
                ProgressBarMarqueeStop()

                gBusyInstalling = False

                GoToGroupBox(GroupBoxes.Done)
            Else
                _FakeUninstallStep()
            End If
        Else
            Select Case iApplicationInstallationCount
                Case InstallationProgress.StopServices
                    _StopServices()
                Case InstallationProgress.UninstallSiteKiosk
                    tmrInstallerWait.Enabled = True

                    Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallSiteKiosk))
                    Me.installThread.Start()
                Case InstallationProgress.UninstallSiteKioskBruteForce
                    _UninstallSiteKioskBruteForce()
                Case InstallationProgress.UninstallAltiris
                    tmrInstallerWait.Enabled = True

                    Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallAltiris))
                    Me.installThread.Start()
                Case InstallationProgress.CleanUpUser_Admin
                    _CleanUpUser_Admin()
                Case InstallationProgress.CleanUpUser_Puppy
                    _CleanUpUser_Puppy()
                Case InstallationProgress.CleanUpUser_SiteKiosk
                    _CleanUpUser_SiteKiosk()
                Case InstallationProgress.CleanUpRegistry
                    _CleanUpRegistry()
                Case InstallationProgress.CleanUpFiles
                    _CleanUpFiles()
                Case InstallationProgress.UninstallLogMeIn
                    tmrInstallerWait.Enabled = True

                    Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallLogMeIn))
                    Me.installThread.Start()
                Case InstallationProgress.UninstallLogMeInClient
                    tmrInstallerWait.Enabled = True

                    Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallLogMeInClient))
                    Me.installThread.Start()
                Case Else
                    ProgressBarMarqueeStop()

                    gBusyInstalling = False

                    GoToGroupBox(GroupBoxes.Done)
            End Select
        End If
    End Sub

    Private Sub _FakeUninstallStep()
        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub _UninstallSiteKiosk()
        oLogger.WriteToLog("Uninstalling SiteKiosk")

        bSiteKioskUninstallTimeout = False

        Dim sParams As String
        Dim sId As String

        oProcess = New ProcessRunner

        sId = ProductGUID.GetUninstallIdentifier("sitekiosk")

        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            bSiteKioskUninstallTimeout = True

            Exit Sub
        End If

        sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 1800", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = cUNINSTALL_COMMAND
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1800
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _UninstallSiteKioskBruteForce()
        oLogger.WriteToLog("Uninstalling SiteKiosk - BRUTE FORCE")
        If gAllowBruteForce Then
            oLogger.WriteToLog("forced", , 1)
            bSiteKioskUninstallTimeout = True
        End If
        If Not bSiteKioskUninstallTimeout Then
            oLogger.WriteToLog("not needed", , 1)

            iApplicationInstallationCount += 1
            InstallationController()

            Exit Sub
        End If

        oLogger.WriteToLog("killing SiteKiosk processes", , 1)
        _KillProcess("Configure")
        _KillProcess("SiteKiosk")
        _KillProcess("skpcdsvc")
        _KillProcess("SmartCard")
        _KillProcess("SystemSecurity")
        _KillProcess("Watchdog")
        _KillProcess("SessionMonitor")
        _KillProcess("SiteRemoteClientService")
        _KillProcess("SKScreenshot")
        _KillProcess("msiexec")

        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK_1)
        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK_2)
        __CleanUpFolder(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) & "\Programs\SiteKiosk")

        __CleanUpLocalUser("sitekiosk")
        __CleanUpFolder("C:\Documents and Settings\sitekiosk", True)

        Dim rk_SERVICES As Microsoft.Win32.RegistryKey

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services", , 1)
            rk_SERVICES = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services", True)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)

            ErrorDetected()
            Exit Sub
        End Try

        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\skpcdsvc
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteRemote Client

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\skpcdsvc", , 1)
            rk_SERVICES.DeleteSubKeyTree("skpcdsvc")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        'Oops, the keyboard no longer responds if we delete this key. So let's not do that then
        'Try
        '   oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey", , 1)
        '   rk_SERVICES.DeleteSubKeyTree("SiteKey")
        '   oLogger.WriteToLog("ok", , 2)
        'Catch ex As Exception
        '   oLogger.WriteToLog("fail", , 2)
        'End Try
        'We need to set the BlockKeyDuringStartup to 0 though
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters\BlockDuringStartup
        Try
            oLogger.WriteToLog("setting HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters\BlockDuringStartup to 0", , 1)
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters", "BlockDuringStartup", 0)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteRemote Client", , 1)
            rk_SERVICES.DeleteSubKeyTree("SiteRemote Client")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{ADE43478-C0B2-422D-B111-4DF94171B6C3}
        oLogger.WriteToLog("removing SiteKiosk from 'Add/Remove software' list", , 1)
        Dim sId As String
        Dim rk_UNINSTALL As Microsoft.Win32.RegistryKey

        sId = ProductGUID.GetUninstallIdentifier("sitekiosk")

        oLogger.WriteToLog("step 1 of 2", , 2)
        If sId <> "" Then
            Try
                oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", , 3)
                rk_UNINSTALL = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", True)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)

                ErrorDetected()
                Exit Sub
            End Try

            Try
                oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{" & sId & "}", , 3)
                rk_UNINSTALL.DeleteSubKeyTree("{" & sId & "}")
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)
            End Try
        Else
            oLogger.WriteToLog("not found, so skipped", , 3)
        End If


        sId = ProductGUID.GetProductIdentifier("sitekiosk")
        oLogger.WriteToLog("step 2 of 2", , 2)
        If sId <> "" Then
            Try
                oLogger.WriteToLog("opening CLASSES_ROOT\Installer\Products", , 3)
                rk_UNINSTALL = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Installer\Products", True)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)

                ErrorDetected()
                Exit Sub
            End Try

            Try
                oLogger.WriteToLog("removing CLASSES_ROOT\Installer\Products\" & sId, , 3)
                rk_UNINSTALL.DeleteSubKeyTree(sId)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)
            End Try
        Else
            oLogger.WriteToLog("not found, so skipped", , 3)
        End If


        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub _UninstallTaskbarAutohide()

    End Sub

    Private Sub _UninstallAltiris()
        Dim sAltiris As String = ""

        oLogger.WriteToLog("Uninstalling remote administration application")

        If Not System.IO.File.Exists(cUNINSTALL_ALTIRIS_COMMAND_1) Then
            If Not IO.File.Exists(cUNINSTALL_ALTIRIS_COMMAND_2) Then
                oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

                oProcess.ProcessDone()

                Exit Sub
            Else
                sAltiris = cUNINSTALL_ALTIRIS_COMMAND_2
            End If
        Else
            sAltiris = cUNINSTALL_ALTIRIS_COMMAND_1
        End If

        oLogger.WriteToLog("Path   : " & sAltiris, , 1)
        oLogger.WriteToLog("Params : " & cUNINSTALL_ALTIRIS_PARAMS, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = sAltiris
        oProcess.Arguments = cUNINSTALL_ALTIRIS_PARAMS
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _UninstallLogMeIn()
        oLogger.WriteToLog("Uninstalling remote access application")

        Dim sParams As String
        Dim sId As String

        oProcess = New ProcessRunner

        sId = ProductGUID.GetUninstallIdentifier("logmein")

        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            Exit Sub
        End If

        sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = cUNINSTALL_COMMAND
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _UninstallLogMeInClient()
        oLogger.WriteToLog("Uninstalling remote access application (client)")

        Dim sParams As String
        Dim sId As String

        oProcess = New ProcessRunner

        sId = ProductGUID.GetUninstallIdentifier("logmein client")

        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            Exit Sub
        End If

        sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = cUNINSTALL_COMMAND
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _StopServices()
        oLogger.WriteToLog("Removing services")

        oLogger.WriteToLog("Killing OVCCWatchdog processes", , 1)
        __KillOVCCWatchDogProcesses()

        Dim oService As WindowsService
        Dim sStatus As String

        oLogger.WriteToLog("OVCCAgent_Heartbeat", , 1)
        oService = New WindowsService
        oService.ServiceName = "OVCCAgent_Heartbeat"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("OVCCAgent_Logfile", , 1)
        oService = New WindowsService
        oService.ServiceName = "OVCCAgent_Logfile"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("OVCCWatchdog", , 1)
        oService = New WindowsService
        oService.ServiceName = "OVCCWatchdog"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("OVCCWatchdog_EmergencyDeploy", , 1)
        oService = New WindowsService
        oService.ServiceName = "OVCCWatchdog_EmergencyDeploy"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("PrintQueueCleanerService", , 1)
        oService = New WindowsService
        oService.ServiceName = "PrintQueueCleanerService"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("OVCCMonitor", , 1)
        oService = New WindowsService
        oService.ServiceName = "OVCCMonitor"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("Killing any left-over OVCCWatchdog processes", , 1)
        __KillOVCCWatchDogProcesses()

        oLogger.WriteToLog("TaskbarAutohide", , 1)
        _KillProcess("TaskbarAutohide")

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __KillOVCCWatchDogProcesses()
        _KillProcess("ovccwatchdog_service")
        _KillProcess("ovccwatchdog_warning")
        _KillProcess("ovccwatchdog_trigger")
        _KillProcess("ovccwatchdog_lastactivity")
        _KillProcess("ovccwatchdog_enable")
        _KillProcess("ovccwatchdog_disable")
        _KillProcess("ovccwatchdog_error")
        _KillProcess("ovccwatchdog_muzzle")
        _KillProcess("ovccwatchdog_emergencydeploy_service")
        _KillProcess("ovccwatchdog_emergencydeploy")
    End Sub

    Private Sub _KillProcess(ByVal sProcessName As String)
        Dim p As System.Diagnostics.Process

        oLogger.WriteToLog(sProcessName, , 2)
        For Each p In System.Diagnostics.Process.GetProcessesByName(sProcessName)
            Try
                p.Kill()
                oLogger.WriteToLog("dead", , 3)
            Catch ex As Exception
                oLogger.WriteToLog("zombie", , 3)
            End Try
        Next
    End Sub

    Private Sub _CleanUpFiles()
        oLogger.WriteToLog("Removing folders and files")

        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK_1)
        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK_2)
        __CleanUpFolder(cCLEANFOLDER_ALTIRIS_1)
        __CleanUpFolder(cCLEANFOLDER_ALTIRIS_2)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_1)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_2)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_3)
        __CleanUpFolder(cCLEANFOLDER_GuestTek_1)
        __CleanUpFolder(cCLEANFOLDER_GuestTek_2)
        __CleanUpFolder(cCLEANFOLDER_SERIELL)
        '__CleanUpFolder(Environment.GetFolderPath(Environment.SpecialFolder.CommonPrograms) & GetSpecialFolderPath(enuCSIDLPhysical.CommonPrograms) & "\Altiris")
        '__CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonProgramFiles) & "\Provisio")
        '__CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonProgramFiles) & "\Altiris")
        'Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        __CleanUpFile("C:\Windows\explorer_.exe")
        __CleanUpFile("C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp")
        __CleanUpFile(Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory) & "\Start OVCC.lnk")
        __CleanUpFile(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) & "\Start OVCC.lnk")
        __CleanUpFile(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) & "\Uninstall OVCC.lnk")
        __CleanUpFile(Environment.GetFolderPath(Environment.SpecialFolder.CommonDesktopDirectory) & "\OVCC - InformationUpdater.lnk")
        __CleanUpFile("c:\lobbypc.ver.txt.txt")

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __CleanUpFolder(ByVal sFolder As String, Optional ByVal bIgnoreError As Boolean = False)
        oLogger.WriteToLog("removing: " & sFolder, , 1)
        If System.IO.Directory.Exists(sFolder) Then
            Try
                __ClearAttributes(sFolder)
                System.IO.Directory.Delete(sFolder, True)
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                'If Not bIgnoreError Then ErrorDetected()
            End Try
            If System.IO.Directory.Exists(sFolder) Then
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                oLogger.WriteToLog("marking for delete on reboot", , 2)
                If DeleteFileOnReboot(sFolder) Then
                    oLogger.WriteToLog("ok", , 3)
                Else
                    oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                    'If Not bIgnoreError Then ErrorDetected()
                End If
            Else
                oLogger.WriteToLog("ok", , 2)
            End If
        Else
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub __CleanUpFile(ByVal sFile As String)
        oLogger.WriteToLog("removing: " & sFile, , 1)
        If System.IO.File.Exists(sFile) Then
            Try
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
                System.IO.File.Delete(sFile)
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                'ErrorDetected()
            End Try
            If System.IO.File.Exists(sFile) Then
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                oLogger.WriteToLog("marking for delete on reboot", , 2)
                If DeleteFileOnReboot(sFile) Then
                    oLogger.WriteToLog("ok", , 3)
                Else
                    oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                    ErrorDetected()
                End If
            Else
                oLogger.WriteToLog("ok", , 2)
            End If
        Else
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub __ClearAttributes(ByVal sCurrentFolder As String)
        If System.IO.Directory.Exists(sCurrentFolder) Then
            Dim sSubFolders() As String = System.IO.Directory.GetDirectories(sCurrentFolder)
            Dim sSubFolder As String

            For Each sSubFolder In sSubFolders
                System.IO.File.SetAttributes(sSubFolder, IO.FileAttributes.Normal)
                __ClearAttributes(sSubFolder)
            Next

            Dim sFiles() As String = System.IO.Directory.GetFiles(sCurrentFolder)
            Dim sFile As String

            For Each sFile In sFiles
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
            Next
        End If
    End Sub

    Private Sub _CleanUpUser_Puppy()
        oLogger.WriteToLog("Removing __OVCC_puppy__ user account")

        __CleanUpLocalUser("__OVCC_puppy__")
        __CleanUpFolder("C:\Documents and Settings\__OVCC_puppy__", True)
        __CleanUpFolder("C:\Users\__OVCC_puppy__", True)

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub _CleanUpUser_SiteKiosk()
        oLogger.WriteToLog("Removing SiteKiosk user account")

        __CleanUpLocalUser("SiteKiosk")
        __CleanUpFolder("C:\Documents and Settings\SiteKiosk", True)
        __CleanUpFolder("C:\Users\SiteKiosk", True)

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub _CleanUpUser_Admin()
        oLogger.WriteToLog("Removing local admin accounts")

        __CleanUpLocalUser("iBAHN")
        __CleanUpFolder("C:\Documents and Settings\iBAHN", True)
        __CleanUpFolder("C:\Users\iBAHN", True)

        __CleanUpLocalUser("GuestTek")
        __CleanUpFolder("C:\Documents and Settings\GuestTek", True)
        __CleanUpFolder("C:\Users\GuestTek", True)

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __CleanUpLocalUser(ByVal sUserName As String)
        Dim sError As String = ""

        oLogger.WriteToLog("removing user: " & sUserName, , 1)

        If cWindowsUser.RemoveUser(sUserName, sError) Then
            oLogger.WriteToLog("ok", , 2)
        Else
            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
            oLogger.WriteToLog("Error: " & sError, Logger.MESSAGE_TYPE.LOG_WARNING, 3)
        End If
    End Sub

    Private Sub _CleanUpRegistry()
        oLogger.WriteToLog("Cleaning registry")

        oLogger.WriteToLog("registry rollback", , 1)
        oLogger.WriteToLog("searching for rollback file", , 2)
        If IO.File.Exists(cPATHS_GuestTekProgramFilesFolder & "\OVCCSoftwareOnlyInstallation.registry.backup.xml") Then
            oLogger.WriteToLog("ok", , 3)

            Try
                Dim mXmlDoc As New Xml.XmlDocument
                Dim mXmlNodeRoot As Xml.XmlNode
                Dim mXmlNodelistKeys As Xml.XmlNodeList

                Dim mXmlNodeName As Xml.XmlNode, mXmlNodeValueName As Xml.XmlNode
                Dim mXmlNodeValue As Xml.XmlNode, mXmlNodeType As Xml.XmlNode

                oLogger.WriteToLog("opening", , 2)

                mXmlDoc.Load(cPATHS_GuestTekProgramFilesFolder & "\OVCCSoftwareOnlyInstallation.registry.backup.xml")
                oLogger.WriteToLog("ok", , 3)

                mXmlNodeRoot = mXmlDoc.SelectSingleNode("OVCCSoftwareOnly")
                mXmlNodelistKeys = mXmlNodeRoot.SelectSingleNode("registry-backup").SelectNodes("key")

                '<key>
                '  <name>HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPCSoftwareOnly\Config</name>
                '  <valuename>BlockedDrives</valuename>
                '  <old>
                '    <value>(does not exist)</value>
                '    <type>0</type>
                '  </old>
                '  <new>
                '    <value>C,</value>
                '    <type>0</type>
                '  </new>
                '</key>

                Dim rk As Microsoft.Win32.RegistryKey, sRegKey As String

                oLogger.WriteToLog("iterating", , 2)
                For Each mXmlNodeKey As Xml.XmlNode In mXmlNodelistKeys
                    mXmlNodeName = mXmlNodeKey.SelectSingleNode("name")
                    mXmlNodeValueName = mXmlNodeKey.SelectSingleNode("valuename")
                    mXmlNodeValue = mXmlNodeKey.SelectSingleNode("old").SelectSingleNode("value")
                    mXmlNodeType = mXmlNodeKey.SelectSingleNode("old").SelectSingleNode("type")

                    oLogger.WriteToLog("found", , 3)
                    oLogger.WriteToLog(mXmlNodeKey.SelectSingleNode("name").InnerText & "\" & mXmlNodeValueName.InnerText, , 4)

                    sRegKey = mXmlNodeName.InnerText

                    If mXmlNodeValue.InnerText = "(does not exist)" And mXmlNodeType.InnerText = "0" Then
                        oLogger.WriteToLog("deleting", , 3)
                        Try
                            If mXmlNodeName.InnerText.StartsWith("HKEY_LOCAL_MACHINE\") Then
                                sRegKey = mXmlNodeName.InnerText.Replace("HKEY_LOCAL_MACHINE\", "")

                                rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sRegKey, True)
                            ElseIf mXmlNodeName.InnerText.StartsWith("HKEY_USERS\") Then
                                sRegKey = mXmlNodeName.InnerText.Replace("HKEY_USERS\", "")

                                rk = Microsoft.Win32.Registry.Users.OpenSubKey(sRegKey, True)
                            End If

#Disable Warning BC42104 ' Variable is used before it has been assigned a value
                            rk.DeleteValue(mXmlNodeValueName.InnerText)
#Enable Warning BC42104 ' Variable is used before it has been assigned a value
                            oLogger.WriteToLog("ok", , 3)
                        Catch ex As Exception
                            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                            oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                        End Try
                    Else
                        oLogger.WriteToLog("restoring", , 3)

                        Try
                            Microsoft.Win32.Registry.SetValue(mXmlNodeName.InnerText, mXmlNodeValueName.InnerText, mXmlNodeValue.InnerText, Integer.Parse(mXmlNodeType.InnerText))
                            oLogger.WriteToLog("ok", , 4)
                        Catch ex As Exception
                            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                            oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
                        End Try
                    End If
                Next
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End Try
        Else
            oLogger.WriteToLog("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If


        oLogger.WriteToLog("some more cleaning", , 1)
        Dim rk_SOFTWARE As Microsoft.Win32.RegistryKey
        Dim rk_UNINSTALL As Microsoft.Win32.RegistryKey
        Dim rk_RUN As Microsoft.Win32.RegistryKey

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE", , 2)
            rk_SOFTWARE = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", True)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)

            ErrorDetected()
            Exit Sub
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_SITEKIOSK, , 2)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_SITEKIOSK)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_ALTIRIS, , 2)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_ALTIRIS)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_IBAHN, , 2)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_IBAHN)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_GUESTTEK, , 2)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_GUESTTEK)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try


        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", , 2)
            rk_UNINSTALL = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", True)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)

            ErrorDetected()
            Exit Sub
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\" & cCLEANREGISTRY_OVCCSOFTWAREUNINSTALL, , 2)
            rk_UNINSTALL.DeleteSubKeyTree(cCLEANREGISTRY_OVCCSOFTWAREUNINSTALL)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try


        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", , 2)
            rk_RUN = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)

            ErrorDetected()
            Exit Sub
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\PostSiteKioskUpdating", , 2)
            rk_RUN.DeleteValue("PostSiteKioskUpdating")
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\LobbyPCWatchdogTrigger", , 2)
            rk_RUN.DeleteValue("LobbyPCWatchdogTrigger")
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\TaskbarAutohide", , 2)
            rk_RUN.DeleteValue("TaskbarAutohide")
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\SiteKiosk", , 2)
            rk_RUN.DeleteValue("SiteKiosk")
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub
#End Region

#Region "DragWindow events"
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    Private Const HTCAPTION As Integer = &H2


    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picboxLogo.MouseDown
        Me.picboxLogo.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub lblTitle_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Me.lblTitle.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Prerequisites"
    Private Sub CheckPrerequisites()
        Dim sError As String = ""

        oPrerequisites.IsAlreadyInstalled()
        oPrerequisites.CheckWindowsVersion(cPREREQUISITES_NeededWindowsVersion, True)
        oPrerequisites.CheckInternetConnection()
        oPrerequisites.IsUserAdmin()
        oPrerequisites.IsUserGuestTekUser()

        If oPrerequisites.CheckComplete Then
            If oPrerequisites.AllIsOk_ForUninstaller_Custom Then
                NextGroupBox()
            Else
                sError = ""
                oLogger.WriteToLog("Prerequisites error", Logger.MESSAGE_TYPE.LOG_ERROR)

                If Not oPrerequisites.CorrectWindowsVersion Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If

                    If oPrerequisites.HigherWindowsVersionsAreAccepted Then
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " < " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= "Incorrect Windows version! %%NEEDED_WINDOWS_VERSION%% (or higher) is needed."
                    Else
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " != " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= "Incorrect Windows version! %%NEEDED_WINDOWS_VERSION%% is needed."
                    End If

                    sError = sError.Replace("%%NEEDED_WINDOWS_VERSION%%", oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion))
                    sError = sError.Replace("%%RUNNING_WINDOWS_VERSION%%", oPrerequisites.WindowsVersion)
                End If
                If Not oPrerequisites.UserIsAdmin Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("No admin privileges", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= "This uninstallation needs to run with Administrator privileges."
                End If
                If oPrerequisites.IsUserGuestTekUser Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("Running as GuestTek user", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= "You can't run the uninstaller as the GuestTek user. Please choose another administrator account."
                End If


                If gTestMode Then
                    MessageBox.Show("I would have redirected to the errorscreen here, with this message: " & vbCrLf & vbCrLf & sError,
                                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                    UpdateButtons(ButtonState.Enabled, ButtonState.Enabled, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")
                Else
                    UpdateButtons(ButtonState.Enabled, ButtonState.Hidden, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")

                    ErrorGroupbox(sError)
                End If


                'lblPrerequisites.Text = sError

                'If gTestMode Then
                '    UpdateButtons(ButtonState.Enabled, ButtonState.Enabled, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")
                'Else
                '    UpdateButtons(ButtonState.Enabled, ButtonState.Hidden, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")

                'End If
            End If
        Else
            lblPrerequisites.Text = "Unknown error 31. Please contact the helpdesk."

            UpdateButtons(ButtonState.Enabled, ButtonState.Hidden, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")
        End If
    End Sub
#End Region

#Region "Groupbox handling"
    Private Sub ErrorGroupbox(sError As String)
        iActiveGroupBox = GroupBoxes.ErrorScreen

        lblGenericError.Text = sError

        UpdateGroupBox()
    End Sub

    Private Sub NextGroupBox()
        If iActiveGroupBox = GroupBoxes.OptionSelect Then
            'Collect the settings
        End If

        iActiveGroupBox += 1
        UpdateGroupBox()
    End Sub

    Private Sub PreviousGroupBox()
        iActiveGroupBox -= 1
        'Skip these group box if we are going BACK
        'Windows version and internet connection error dialog
        If iActiveGroupBox = GroupBoxes.Prerequisites Then
            iActiveGroupBox = -1
        End If

        UpdateGroupBox()
    End Sub

    Private Sub GoToGroupBox(ByVal iGroupBox As GroupBoxes)
        iActiveGroupBox = iGroupBox
        UpdateGroupBox()
    End Sub

    Private Sub UpdateGroupBox()
        oLogger.WriteToLog("Groupbox / action: " & iActiveGroupBox.ToString, Logger.MESSAGE_TYPE.LOG_DEBUG)

        Select Case iActiveGroupBox
            Case GroupBoxes.Prerequisites
                UpdateButtons(ButtonState.Hidden, ButtonState.Hidden, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")

                CheckPrerequisites()
            Case GroupBoxes.OptionSelect
                UpdateButtons(ButtonState.Enabled, ButtonState.Disabled, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")

                Me.ControlBox = True
            Case GroupBoxes.AreYouSure
                chkRemoveLocalUsers_CONFIRM.Checked = False
                chkRemoveLocalUsers_CONFIRM.Enabled = chkRemoveLocalUsers.Checked
                chkRemoveLocalUsers_CONFIRM.Visible = chkRemoveLocalUsers.Checked


                Dim sRUS As String = ""

                UpdateButtons(ButtonState.Enabled, ButtonState.Enabled, ButtonState.Hidden, "NO", "YES", "RETRY")

                'Get uninstall and reboot option
                iUninstallOption = GetUninstallOption()
                p_RebootWhenDone = chkRebootWhenDone.Checked
                p_RemoveOVCCAdminUser = chkRemoveLocalUsers.Checked

                Select Case iUninstallOption
                    Case UninstallOptions.All
                        sRUS = "You are about to uninstall all OVCC software, INCLUDING LogMeIn!"
                    Case UninstallOptions.AllButLogMeIn
                        sRUS = "You are about to uninstall all OVCC software and keep LogMeIn!"
                    Case UninstallOptions.OnlyLogMeIn
                        sRUS = "You are about to uninstall LogMeIn!"
                    Case Else
                        sRUS = "You are about to uninstall nothing!"
                End Select

                If p_RemoveOVCCAdminUser Then
                    sRUS &= vbCrLf & vbCrLf & "IMPORTANT: The uninstaller will delete the local OVCC admin. Machine might become unreachable!!!"
                End If

                If p_RebootWhenDone Then
                    sRUS &= vbCrLf & vbCrLf & "After the uninstall, a forced reboot will happen."
                Else
                    sRUS &= vbCrLf & vbCrLf & "No forced reboot selected. You'll have to do that yourself."
                End If

                sRUS &= vbCrLf & vbCrLf & "Are you sure?"

                lblAreYouSure.Text = sRUS
            Case GroupBoxes.Uninstallation
                p_RemoveOVCCAdminUser = chkRemoveLocalUsers.Checked And chkRemoveLocalUsers_CONFIRM.Checked

                UpdateButtons(ButtonState.Hidden, ButtonState.Hidden, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")

                gBusyInstalling = True

                Me.ControlBox = False

                ProgressBarMarqueeStart()
                StartUninstallation()
            Case GroupBoxes.ErrorScreen
                UpdateButtons(ButtonState.Hidden, ButtonState.Enabled, ButtonState.Hidden, "", "EXIT", "")

                Me.ControlBox = True
            Case GroupBoxes.Done
                lblDone.Text = "Finished" & vbCrLf & vbCrLf
                If iSeriousErrorCount > 0 Then
                    lblDone.Text &= "Not all items could be deleted." & vbCrLf
                    lblDone.Text &= "Please reboot the pc, and run the uninstaller again."
                Else
                    lblDone.Text &= "Please reboot the pc to complete the uninstallation."
                End If

                oLogger.WriteToLog("Serious errors detected: " & iSeriousErrorCount.ToString, , 0)


                Me.ControlBox = True

                If gUnattendedUninstall Then
                    If gUnattendedForcedReboot Then
                        oLogger.WriteToLog("done", , 0)
                        oLogger.WriteToLog("rebooting", , 1)

                        If Not gTestMode Then
                            WindowsController.ExitWindows(RestartOptions.Reboot, True)
                        Else
                            oLogger.WriteToLog("WindowsController.ExitWindows(RestartOptions.Reboot, True)", Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                        End If
                    Else
                        oLogger.WriteToLog("done", , 0)
                        oLogger.WriteToLog("bye", , 1)

                        ApplicationExit(0)
                    End If
                Else
                    If p_RebootWhenDone Then
                        UpdateButtons(ButtonState.Hidden, ButtonState.Enabled, ButtonState.Hidden, "", "Cancel reboot (" & mRebootCountDown.ToString & ")", "")

                        StartRebootCountdown()
                    Else
                        UpdateButtons(ButtonState.Hidden, ButtonState.Enabled, ButtonState.Hidden, "EXIT", "EXIT", "EXIT")
                    End If
                End If

            Case Else
                StopRebootContdown()

                Application.Exit()
        End Select

        If iActiveGroupBox >= 0 And iActiveGroupBox < Panels.Length Then
            Panels(iActiveGroupBox).Visible = True
        End If

        Dim i As Integer
        For i = 0 To Panels.Length - 1
            If i <> iActiveGroupBox Then
                Panels(i).Visible = False
            End If
        Next
    End Sub
#End Region

#Region "Button Handling"
    Public Enum ButtonState As Integer
        Hidden = 0
        Disabled
        Enabled
    End Enum

    Private Sub UpdateButtons(ByVal iCancel As ButtonState, ByVal iNext As ButtonState, ByVal iRetry As ButtonState, Optional ByVal sCancel As String = "", Optional ByVal sNext As String = "", Optional ByVal sRetry As String = "")
        Select Case iCancel
            Case ButtonState.Disabled
                btnCancel.Enabled = False
                btnCancel.Visible = True
            Case ButtonState.Enabled
                btnCancel.Enabled = True
                btnCancel.Visible = True
            Case Else
                btnCancel.Enabled = False
                btnCancel.Visible = False
        End Select
        If Not sCancel.Equals("") Then
            btnCancel.Text = sCancel
        End If

        Select Case iNext
            Case ButtonState.Disabled
                btnNext.Enabled = False
                btnNext.Visible = True
            Case ButtonState.Enabled
                btnNext.Enabled = True
                btnNext.Visible = True
            Case Else
                btnNext.Enabled = False
                btnNext.Visible = False
        End Select
        If Not sNext.Equals("") Then
            btnNext.Text = sNext
        End If

        Select Case iRetry
            Case ButtonState.Disabled
                btnRetry.Enabled = False
                btnRetry.Visible = True
            Case ButtonState.Enabled
                btnRetry.Enabled = True
                btnRetry.Visible = True
            Case Else
                btnRetry.Enabled = False
                btnRetry.Visible = False
        End Select
        If Not sRetry.Equals("") Then
            btnRetry.Text = sRetry
        End If
    End Sub
#End Region

#Region "Progress bars"
    Private Sub ProgressBarMarqueeStart()
        progressMarquee.Style = ProgressBarStyle.Marquee
        progressMarquee.MarqueeAnimationSpeed = 100
        progressMarquee.Value = 0
    End Sub

    Private Sub ProgressBarMarqueeStop()
        progressMarquee.Style = ProgressBarStyle.Blocks
        progressMarquee.MarqueeAnimationSpeed = 0
        progressMarquee.Value = 0
    End Sub
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationExit(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("exiting...")
        oLogger.WriteToLog("exit code: " & iExitCode.ToString, , 1)
        oLogger.WriteToLog(New String("=", 50))

        Environment.ExitCode = iExitCode
        Application.Exit()
    End Sub
#End Region

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        NextGroupBox()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If iActiveGroupBox = GroupBoxes.AreYouSure Then
            PreviousGroupBox()
        Else
            ApplicationExit()
        End If
    End Sub

    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            iApplicationInstallationCount += 1
            InstallationController()
        End If
    End Sub

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLog("done", , 1)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLog("failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)

        If iApplicationInstallationCount = InstallationProgress.UninstallSiteKiosk Then
            bSiteKioskUninstallTimeout = True
        Else
            ErrorDetected()
        End If

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_KillFailed(ByVal EventProcess As System.Diagnostics.Process, ByVal ErrorMessage As String) Handles oProcess.KillFailed
        oLogger.WriteToLog("killing process failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLog("started", , 1)
        oLogger.WriteToLog("id  : " & EventProcess.Id.ToString, , 2)
        oLogger.WriteToLog("name: " & EventProcess.ProcessName, , 2)
    End Sub

    Private Sub oProcess_TimeOut(EventProcess As Process, TimeElapsed As Double, TimeoutCount As Integer, TimeoutCountMax As Integer) Handles oProcess.TimeOut
        oLogger.WriteToLog("timed out", , 1)

        If iApplicationInstallationCount = InstallationProgress.UninstallSiteKiosk Then
            bSiteKioskUninstallTimeout = True
        Else
            ErrorDetected()
        End If

        oProcess.ProcessDone()
    End Sub
#End Region

    Private Sub UpdateInstallationProgressCount()
        lblProgress.Text = CStr(iApplicationInstallationCount + 1) & " / " & CStr(iApplicationInstallationCountMax)
        lblProgress.Visible = True
    End Sub

    Private Sub ErrorDetected()
        oLogger.WriteToLogRelative("Error detected", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        iSeriousErrorCount += 1
    End Sub

    Private Sub btnRetry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetry.Click
        iApplicationInstallationCount = 0
        InstallationController()
    End Sub

    Private Function GetUninstallOption() As UninstallOptions
        If radUninstallOption_All.Checked Then
            Return UninstallOptions.All
        End If
        If radUninstallOption_AllButLogMeIn.Checked Then
            Return UninstallOptions.AllButLogMeIn
        End If

        If radUninstallOption_OnlyLogMeIn.Checked Then
            Return UninstallOptions.OnlyLogMeIn
        End If

        Return UninstallOptions.None
    End Function

    Private Sub radUninstallOption_OnlyLogMeIn_CheckedChanged(sender As Object, e As EventArgs) Handles radUninstallOption_OnlyLogMeIn.CheckedChanged
        UpdateButtons(ButtonState.Enabled, ButtonState.Enabled, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")

        If radUninstallOption_OnlyLogMeIn.Checked Then
            chkRemoveLocalUsers.Checked = False
            chkRemoveLocalUsers.Enabled = False
        Else
            chkRemoveLocalUsers.Enabled = True
        End If
    End Sub

    Private Sub radUninstallOption_All_CheckedChanged(sender As Object, e As EventArgs) Handles radUninstallOption_All.CheckedChanged
        UpdateButtons(ButtonState.Enabled, ButtonState.Enabled, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")

        If radUninstallOption_All.Checked Then
            chkRemoveLocalUsers.Enabled = True
        End If
    End Sub

    Private Sub radUninstallOption_AllButLogMeIn_CheckedChanged(sender As Object, e As EventArgs) Handles radUninstallOption_AllButLogMeIn.CheckedChanged
        UpdateButtons(ButtonState.Enabled, ButtonState.Enabled, ButtonState.Hidden, "CANCEL", "NEXT", "RETRY")

        If radUninstallOption_AllButLogMeIn.Checked Then
            chkRemoveLocalUsers.Checked = False
            chkRemoveLocalUsers.Enabled = False
        Else
            chkRemoveLocalUsers.Enabled = True
        End If
    End Sub

    Private Sub StartRebootCountdown()
        mRebootCountDown = 10

        tmrForcedReboot.Enabled = True
    End Sub

    Private Sub StopRebootContdown()
        mRebootCountDown = 10

        tmrForcedReboot.Enabled = False
    End Sub

    Private mRebootCountDown As Integer = 10
    Private Sub tmrForcedReboot_Tick(sender As Object, e As EventArgs) Handles tmrForcedReboot.Tick
        mRebootCountDown -= 1

        UpdateButtons(ButtonState.Hidden, ButtonState.Enabled, ButtonState.Hidden, "", "Cancel reboot (" & mRebootCountDown.ToString & ")", "")

        If mRebootCountDown < 0 Then
            tmrForcedReboot.Enabled = False

            If Not gTestMode Then
                WindowsController.ExitWindows(RestartOptions.Reboot, True)
            Else
                MessageBox.Show("fake reboot")
            End If
        End If
    End Sub

    Private Sub chkRemoveLocalUsers_CheckedChanged(sender As Object, e As EventArgs) Handles chkRemoveLocalUsers.CheckedChanged
        If chkRemoveLocalUsers.Checked Then
            Dim sMsg As String = "When you remove the OVCC admin account, there is a chance that the pc will have no admin account left and becomes unusable!" & vbCrLf & vbCrLf &
                                 "Are you really sure you want to delete the OVCC admin account?"


            If Not MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes Then
                chkRemoveLocalUsers.Checked = False
            End If
        End If
    End Sub
End Class
