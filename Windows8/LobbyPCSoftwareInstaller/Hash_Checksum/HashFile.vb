﻿Imports System.IO

Public Class HashFile
    Implements IDisposable

    Private mFilePath As String
    Public Property FilePath() As String
        Get
            Return mFilePath
        End Get
        Set(ByVal value As String)
            mFilePath = value
        End Set
    End Property

    Private mLastError As String
    Public ReadOnly Property LastError() As String
        Get
            Return mLastError
        End Get
    End Property

    Private mFileStream As IO.FileStream

    Public Sub New(FilePath As String)
        mFilePath = FilePath
    End Sub

    Public Sub Dispose() Implements System.IDisposable.Dispose
        Try
            mFileStream.Close()
            mFileStream.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Public Function Calculate(FilePath As String) As String
        Try
            mFileStream = New FileStream(FilePath, FileMode.Open)
            Dim SHA512 As New Security.Cryptography.SHA512CryptoServiceProvider()
            Dim byteHash() As Byte = SHA512.ComputeHash(mFileStream)

            mFileStream.Close()

            Return Convert.ToBase64String(byteHash)
        Catch ex As Exception
            mLastError = ex.Message
            Return ex.Message
        End Try
    End Function
End Class
