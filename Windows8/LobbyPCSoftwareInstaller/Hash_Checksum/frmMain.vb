﻿Public Class frmMain
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnSelectFile_Click(sender As Object, e As EventArgs) Handles btnSelectFile.Click
        btnCalculateHash.Enabled = False
        txtSelectedFile.Text = ""

        Dim diagResult As DialogResult = OpenFileDialog_OVCC.ShowDialog(Me)
        If diagResult = DialogResult.OK Then
            btnCalculateHash.Enabled = True
            txtSelectedFile.Text = OpenFileDialog_OVCC.FileName
        End If
    End Sub

    Private Sub btnCalculateHash_Click(sender As Object, e As EventArgs) Handles btnCalculateHash.Click
        Dim hasher As New HashFile(txtSelectedFile.Text)
        txtHash.Text = hasher.Calculate(txtSelectedFile.Text)
        hasher.Dispose()
    End Sub

    Private Sub btnToClipboard_Click(sender As Object, e As EventArgs) Handles btnToClipboard.Click
        Clipboard.Clear()
        Clipboard.SetText(txtHash.Text)
    End Sub

    Private Sub btnWriteTextfile_Click(sender As Object, e As EventArgs) Handles btnWriteTextfile.Click
        Dim sPath As String = IO.Path.Combine(IO.Path.GetDirectoryName(Application.ExecutablePath), "latest_download.txt")
        Using writer = My.Computer.FileSystem.OpenTextFileWriter(sPath, False)
            Dim sInstallerName As String = IO.Path.GetFileName(txtSelectedFile.Text)
            Dim sVersion As String = txtVersion.Text

            writer.WriteLine(sInstallerName & "|" & sVersion & "|" & txtHash.Text)
        End Using
    End Sub
End Class
