﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.OpenFileDialog_OVCC = New System.Windows.Forms.OpenFileDialog()
        Me.btnSelectFile = New System.Windows.Forms.Button()
        Me.btnCalculateHash = New System.Windows.Forms.Button()
        Me.txtHash = New System.Windows.Forms.TextBox()
        Me.txtSelectedFile = New System.Windows.Forms.TextBox()
        Me.btnToClipboard = New System.Windows.Forms.Button()
        Me.btnWriteTextfile = New System.Windows.Forms.Button()
        Me.txtVersion = New System.Windows.Forms.TextBox()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'OpenFileDialog_OVCC
        '
        Me.OpenFileDialog_OVCC.InitialDirectory = "D:\_WORK\OVCC\Windows\builds\OVCCDownloader"
        '
        'btnSelectFile
        '
        Me.btnSelectFile.Location = New System.Drawing.Point(9, 12)
        Me.btnSelectFile.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSelectFile.Name = "btnSelectFile"
        Me.btnSelectFile.Size = New System.Drawing.Size(136, 23)
        Me.btnSelectFile.TabIndex = 0
        Me.btnSelectFile.Text = "Select file ..."
        Me.btnSelectFile.UseVisualStyleBackColor = True
        '
        'btnCalculateHash
        '
        Me.btnCalculateHash.Location = New System.Drawing.Point(9, 40)
        Me.btnCalculateHash.Name = "btnCalculateHash"
        Me.btnCalculateHash.Size = New System.Drawing.Size(136, 23)
        Me.btnCalculateHash.TabIndex = 2
        Me.btnCalculateHash.Text = "Calculate hash"
        Me.btnCalculateHash.UseVisualStyleBackColor = True
        '
        'txtHash
        '
        Me.txtHash.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtHash.Location = New System.Drawing.Point(150, 40)
        Me.txtHash.Name = "txtHash"
        Me.txtHash.ReadOnly = True
        Me.txtHash.Size = New System.Drawing.Size(1164, 23)
        Me.txtHash.TabIndex = 3
        '
        'txtSelectedFile
        '
        Me.txtSelectedFile.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtSelectedFile.Location = New System.Drawing.Point(150, 12)
        Me.txtSelectedFile.Name = "txtSelectedFile"
        Me.txtSelectedFile.ReadOnly = True
        Me.txtSelectedFile.Size = New System.Drawing.Size(1224, 23)
        Me.txtSelectedFile.TabIndex = 4
        '
        'btnToClipboard
        '
        Me.btnToClipboard.Location = New System.Drawing.Point(9, 69)
        Me.btnToClipboard.Name = "btnToClipboard"
        Me.btnToClipboard.Size = New System.Drawing.Size(136, 24)
        Me.btnToClipboard.TabIndex = 5
        Me.btnToClipboard.Text = "Copy to clipboard"
        Me.btnToClipboard.UseVisualStyleBackColor = True
        '
        'btnWriteTextfile
        '
        Me.btnWriteTextfile.Location = New System.Drawing.Point(9, 129)
        Me.btnWriteTextfile.Name = "btnWriteTextfile"
        Me.btnWriteTextfile.Size = New System.Drawing.Size(136, 24)
        Me.btnWriteTextfile.TabIndex = 6
        Me.btnWriteTextfile.Text = "Write text file"
        Me.btnWriteTextfile.UseVisualStyleBackColor = True
        '
        'txtVersion
        '
        Me.txtVersion.Location = New System.Drawing.Point(150, 99)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.Size = New System.Drawing.Size(114, 23)
        Me.txtVersion.TabIndex = 7
        Me.txtVersion.Text = "34.1"
        '
        'lblVersion
        '
        Me.lblVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblVersion.Location = New System.Drawing.Point(12, 98)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(132, 24)
        Me.lblVersion.TabIndex = 8
        Me.lblVersion.Text = "Version number"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(1384, 166)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.txtVersion)
        Me.Controls.Add(Me.btnWriteTextfile)
        Me.Controls.Add(Me.btnToClipboard)
        Me.Controls.Add(Me.txtSelectedFile)
        Me.Controls.Add(Me.txtHash)
        Me.Controls.Add(Me.btnCalculateHash)
        Me.Controls.Add(Me.btnSelectFile)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "Hash_Checksum"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents OpenFileDialog_OVCC As OpenFileDialog
    Friend WithEvents btnSelectFile As Button
    Friend WithEvents btnCalculateHash As Button
    Friend WithEvents txtHash As TextBox
    Friend WithEvents txtSelectedFile As TextBox
    Friend WithEvents btnToClipboard As Button
    Friend WithEvents btnWriteTextfile As Button
    Friend WithEvents txtVersion As TextBox
    Friend WithEvents lblVersion As Label
End Class
