Module modGetDrivesToBeBlocked
    Public Function GetDrivesToBeBlocked() As String
        Return GetHardDiskDrives()
    End Function

    Private Function GetHardDiskDrives() As String
        Dim oDriveInfo As System.IO.DriveInfo
        Dim cDriveLetter As Char
        Dim sDriveValue As String = ""

        For Each oDriveInfo In System.IO.DriveInfo.GetDrives()
            If oDriveInfo.DriveType = IO.DriveType.Fixed Then
                cDriveLetter = oDriveInfo.Name.ToUpper.Chars(0)

                sDriveValue &= cDriveLetter & ","
            End If
        Next

        Return sDriveValue
    End Function

    Private Function CalculateNoDrivesValue() As Integer
        Dim oDriveInfo As System.IO.DriveInfo
        Dim cDriveLetter As Char
        Dim iNoDriveValue As Integer = 0

        For Each oDriveInfo In System.IO.DriveInfo.GetDrives()
            If oDriveInfo.DriveType = IO.DriveType.Fixed Then
                cDriveLetter = oDriveInfo.Name.ToUpper.Chars(0)

                iNoDriveValue += 2 ^ (Asc(cDriveLetter) - Asc("A"))
            End If
        Next

        Return iNoDriveValue
    End Function
End Module