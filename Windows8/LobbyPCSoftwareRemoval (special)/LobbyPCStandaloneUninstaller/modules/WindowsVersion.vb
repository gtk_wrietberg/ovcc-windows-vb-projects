Module WindowsVersion

    Public Function GetWindowsVersion() As String
        Return TranslateWindowsVersion(Environment.OSVersion.Platform, Environment.OSVersion.Version.Major.ToString, Environment.OSVersion.Version.Minor.ToString)
    End Function

    Private Function TranslateWindowsVersion(ByVal PlatformID As String, ByVal MajorVersion As String, ByVal MinorVersion As String) As String
        Dim sVersionString As String

        sVersionString = PlatformID & "." & MajorVersion & "." & MinorVersion
        Select Case sVersionString
            Case "1.4.0"
                Return "Win 95"
            Case "1.4.10"
                Return "Win 98"
            Case "1.4.98"
                Return "Win ME"
            Case "2.3.51"
                Return "Win NT 3"
            Case "2.4.0"
                Return "Win NT 4"
            Case "2.5.0"
                Return "Win 2000"
            Case "2.5.1"
                Return "Win XP"
            Case "2.6.0"
                Return "Win Vista"
            Case "2.6.1"
                Return "Win Seven"
            Case Else
                Return "Unknown"
        End Select
    End Function

End Module
