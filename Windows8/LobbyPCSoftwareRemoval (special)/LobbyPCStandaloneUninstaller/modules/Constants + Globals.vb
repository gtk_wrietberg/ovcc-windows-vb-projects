Module Constants___Globals
    'GUI
    Public ReadOnly cGUI_GroupBoxBorder_visible As Boolean = True
    'Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.FromArgb(158, 19, 14))
    Public ReadOnly cGUI_GroupBoxBorder_color As New Drawing.Pen(Color.Black)

    'Prerequisites
    Public ReadOnly cPREREQUISITES_NeededWindowsVersion As Prerequisites.WINDOWS_VERSION = Prerequisites.WINDOWS_VERSION.WINDOWS_XP

    'Debug
    Public ReadOnly cDEBUG_ForcePrerequisiteWindowsVersionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteInternetConnectionError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteUserIsAdminError As Boolean = False
    Public ReadOnly cDEBUG_ForcePrerequisiteAlreadyInstalledError As Boolean = False

    'SiteKiosk
    Public ReadOnly cSITEKIOSK_UserName As String = "sitekiosk"
    Public ReadOnly cSITEKIOSK_PassWord As String = "provisio"

    'Uninstall
    Public ReadOnly cUNINSTALL_COMMAND As String = "msiexec.exe"
    Public ReadOnly cUNINSTALL_PARAMS As String = "/X{%%APP_IDENTIFIER%%} /quiet /qn /norestart"
    Public ReadOnly cUNINSTALL_ALTIRIS_COMMAND As String = "C:\Program Files\Altiris\Altiris Agent\AeXAgentUtil.exe"
    Public ReadOnly cUNINSTALL_ALTIRIS_COMMAND__x86 As String = "C:\Program Files (x86)\Altiris\Altiris Agent\AeXAgentUtil.exe"
    Public ReadOnly cUNINSTALL_ALTIRIS_PARAMS As String = "/uninstallagents /clean"

    Public ReadOnly cCLEANFOLDER_SITEKIOSK As String = "c:\Program Files\SiteKiosk"
    Public ReadOnly cCLEANFOLDER_SITEKIOSK__x86 As String = "c:\Program Files (x86)\SiteKiosk"
    Public ReadOnly cCLEANFOLDER_ALTIRIS As String = "c:\Program Files\Altiris"
    Public ReadOnly cCLEANFOLDER_ALTIRIS__x86 As String = "c:\Program Files (x86)\Altiris"
    Public ReadOnly cCLEANFOLDER_iBAHN_1 As String = "c:\ibahn"
    Public ReadOnly cCLEANFOLDER_iBAHN_2 As String = "c:\Program Files\iBAHN"
    Public ReadOnly cCLEANFOLDER_iBAHN_2__x86 As String = "c:\Program Files (x86)\iBAHN"
    Public ReadOnly cCLEANFOLDER_GuestTek_1 As String = "c:\GuestTek"
    Public ReadOnly cCLEANFOLDER_GuestTek_2 As String = "c:\Program Files\GuestTek"
    Public ReadOnly cCLEANFOLDER_GuestTek_2__x86 As String = "c:\Program Files (x86)\GuestTek"
    Public ReadOnly cCLEANFOLDER_SERIELL As String = "c:\seriell"

    Public ReadOnly cCLEANREGISTRY_SITEKIOSK As String = "PROVISIO"
    Public ReadOnly cCLEANREGISTRY_ALTIRIS As String = "Altiris"
    Public ReadOnly cCLEANREGISTRY_IBAHN As String = "iBAHN"
    Public ReadOnly cCLEANREGISTRY_GUESTTEK As String = "GuestTek"

    'Globals. Me like globals, me are lazy
    Public oLogger As Logger

    Public gBusyInstalling As Boolean
    Public gTestMode As Boolean
    Public gDebug As Boolean
    Public gAllowBruteForce As Boolean

    Public gSkipLicenseCheck As Boolean
End Module
