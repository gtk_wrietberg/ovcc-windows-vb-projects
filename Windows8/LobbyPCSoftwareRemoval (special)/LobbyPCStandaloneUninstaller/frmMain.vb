Imports System.Threading

Public Class frmMain
    Private oPrerequisites As Prerequisites
    Private WithEvents oProcess As ProcessRunner
    Private oComputerName As ComputerName
    Private Panels() As Panel
    Private iActiveGroupBox As Integer

    Private installThread As Thread

    Private iApplicationInstallationCount As Integer = 0
    Private ReadOnly iApplicationInstallationCountMax As Integer = 9

    Private bSiteKioskUninstallTimeout As Boolean = False

    Private iSeriousErrorCount As Integer = 0

    Private Enum GroupBoxes As Integer
        Prerequisites = 0
        StartScreen
        Uninstallation
        Done
    End Enum

    Private Enum InstallationProgress As Integer
        StopServices = 0
        UninstallSiteKiosk
        UninstallSiteKioskBruteForce
        UninstallAltiris
        CleanUpLocalUsers
        CleanUpRegistry
        CleanUpFiles
    End Enum

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Panels = New Panel() {Me.pnlPrerequisites, Me.pnlStartScreen, Me.pnlUninstallation, Me.pnlDone}
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gDebug = False
        gSkipLicenseCheck = False
        gAllowBruteForce = False

        Me.Width = 640
        Me.Height = 260

        oPrerequisites = New Prerequisites
        oComputerName = New ComputerName
        oLogger = New Logger

        Dim sArg As String

        For Each sArg In My.Application.CommandLineArgs
            sArg = sArg.ToLower

            If sArg = "--debug" Then
                gDebug = True
            End If
            If sArg = "--allow-brute-force" Then
                gAllowBruteForce = True
            End If
            If sArg = "--help" Or sArg = "/?" Or sArg = "-help" Or sArg = "-?" Then
                ShowHelpMessageBox()
            End If
        Next

        oLogger.LogDebugMessages = gDebug
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        'Panels
        Dim i As Integer
        For i = 0 To Panels.Length - 1
            Panels(i).Top = 84
            Panels(i).Left = 16

            Panels(i).Visible = False
        Next

        iApplicationInstallationCount = 0
        UpdateInstallationProgressCount()


        iActiveGroupBox = -1
        NextGroupBox()
    End Sub

    Private Sub ShowHelpMessageBox()
        Dim s As String = ""

        s &= "Command line parameters:" & vbCrLf & vbCrLf
        s &= "  --help" & vbCrLf
        s &= "  -help" & vbCrLf
        s &= "  -?" & vbCrLf
        s &= "  /?" & vbCrLf
        s &= "      This information, but you obviously already knew that" & vbCrLf & vbCrLf
        s &= "  --debug" & vbCrLf
        s &= "      Show debug information in log file" & vbCrLf & vbCrLf
        s &= "  --allow-brute-force" & vbCrLf
        s &= "      Allow the brute force removal of installed files, if normal uninstallation fails." & vbCrLf & vbCrLf

        MsgBox(s, MsgBoxStyle.OkOnly Or MsgBoxStyle.Information, Application.ProductName)

        Application.Exit()
    End Sub

#Region "Uninstallers"
    Private Sub StartUninstallation()
        InstallationController()
    End Sub

    Private Sub InstallationController()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner

        oLogger.WriteToLog("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)
        UpdateInstallationProgressCount()

        Select Case iApplicationInstallationCount
            Case InstallationProgress.StopServices
                _StopServices()
            Case InstallationProgress.UninstallSiteKiosk
                tmrInstallerWait.Enabled = True

                Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallSiteKiosk))
                Me.installThread.Start()
            Case InstallationProgress.UninstallSiteKioskBruteForce
                _UninstallSiteKioskBruteForce()
            Case InstallationProgress.UninstallAltiris
                tmrInstallerWait.Enabled = True

                Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallAltiris))
                Me.installThread.Start()
            Case InstallationProgress.CleanUpLocalUsers
                _CleanUpLocalUsers()
            Case InstallationProgress.CleanUpRegistry
                _CleanUpRegistry()
            Case InstallationProgress.CleanUpFiles
                _CleanUpFiles()
            Case Else
                ProgressBarMarqueeStop()

                gBusyInstalling = False

                NextGroupBox()
        End Select
    End Sub

    Private Sub _UninstallSiteKiosk()
        oLogger.WriteToLog("Uninstalling SiteKiosk")

        bSiteKioskUninstallTimeout = False

        Dim sParams As String
        Dim sId As String

        oProcess = New ProcessRunner

        sId = ProductGUID.GetUninstallIdentifier("sitekiosk")

        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            bSiteKioskUninstallTimeout = True

            Exit Sub
        End If

        sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 1800", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = cUNINSTALL_COMMAND
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1800
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _UninstallSiteKioskBruteForce()
        oLogger.WriteToLog("Uninstalling SiteKiosk - BRUTE FORCE")
        If gAllowBruteForce Then
            oLogger.WriteToLog("forced", , 1)
            bSiteKioskUninstallTimeout = True
        End If
        If Not bSiteKioskUninstallTimeout Then
            oLogger.WriteToLog("not needed", , 1)

            iApplicationInstallationCount += 1
            InstallationController()

            Exit Sub
        End If

        oLogger.WriteToLog("killing SiteKiosk processes", , 1)
        _KillProcess("Configure")
        _KillProcess("SiteKiosk")
        _KillProcess("skpcdsvc")
        _KillProcess("SmartCard")
        _KillProcess("SystemSecurity")
        _KillProcess("Watchdog")
        _KillProcess("SessionMonitor")
        _KillProcess("SiteRemoteClientService")
        _KillProcess("SKScreenshot")
        _KillProcess("msiexec")

        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK)
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu) & "\Programs\SiteKiosk")

        __CleanUpLocalUser("sitekiosk")
        __CleanUpFolder("C:\Documents and Settings\sitekiosk", True)

        Dim rk_SERVICES As Microsoft.Win32.RegistryKey

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services", , 1)
            rk_SERVICES = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services", True)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)

            ErrorDetected()
            Exit Sub
        End Try

        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\skpcdsvc
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteRemote Client

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\skpcdsvc", , 1)
            rk_SERVICES.DeleteSubKeyTree("skpcdsvc")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        'Oops, the keyboard no longer responds if we delete this key. So let's not do that then
        'Try
        '   oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey", , 1)
        '   rk_SERVICES.DeleteSubKeyTree("SiteKey")
        '   oLogger.WriteToLog("ok", , 2)
        'Catch ex As Exception
        '   oLogger.WriteToLog("fail", , 2)
        'End Try
        'We need to set the BlockKeyDuringStartup to 0 though
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters\BlockDuringStartup
        Try
            oLogger.WriteToLog("setting HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters\BlockDuringStartup to 0", , 1)
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters", "BlockDuringStartup", 0)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteRemote Client", , 1)
            rk_SERVICES.DeleteSubKeyTree("SiteRemote Client")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{ADE43478-C0B2-422D-B111-4DF94171B6C3}
        oLogger.WriteToLog("removing SiteKiosk from 'Add/Remove software' list", , 1)
        Dim sId As String
        Dim rk_UNINSTALL As Microsoft.Win32.RegistryKey

        sId = ProductGUID.GetUninstallIdentifier("sitekiosk")

        oLogger.WriteToLog("step 1 of 2", , 2)
        If sId <> "" Then
            Try
                oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", , 3)
                rk_UNINSTALL = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", True)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)

                ErrorDetected()
                Exit Sub
            End Try

            Try
                oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{" & sId & "}", , 3)
                rk_UNINSTALL.DeleteSubKeyTree("{" & sId & "}")
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)
            End Try
        Else
            oLogger.WriteToLog("not found, so skipped", , 3)
        End If


        sId = ProductGUID.GetProductIdentifier("sitekiosk")
        oLogger.WriteToLog("step 2 of 2", , 2)
        If sId <> "" Then
            Try
                oLogger.WriteToLog("opening CLASSES_ROOT\Installer\Products", , 3)
                rk_UNINSTALL = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Installer\Products", True)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)

                ErrorDetected()
                Exit Sub
            End Try

            Try
                oLogger.WriteToLog("removing CLASSES_ROOT\Installer\Products\" & sId, , 3)
                rk_UNINSTALL.DeleteSubKeyTree(sId)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)
            End Try
        Else
            oLogger.WriteToLog("not found, so skipped", , 3)
        End If


        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub _UninstallAltiris()
        oLogger.WriteToLog("Uninstalling remote administration application")

        Dim sUninstallAltirisCommand As String

        If IO.File.Exists(cUNINSTALL_ALTIRIS_COMMAND__x86) Then
            sUninstallAltirisCommand = cUNINSTALL_ALTIRIS_COMMAND__x86
        Else
            sUninstallAltirisCommand = cUNINSTALL_ALTIRIS_COMMAND
        End If

        If Not IO.File.Exists(sUninstallAltirisCommand) Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            Exit Sub
        End If

        oLogger.WriteToLog("Path   : " & sUninstallAltirisCommand, , 1)
        oLogger.WriteToLog("Params : " & cUNINSTALL_ALTIRIS_PARAMS, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = sUninstallAltirisCommand
        oProcess.Arguments = cUNINSTALL_ALTIRIS_PARAMS
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _UninstallPcAnywhere()
        oLogger.WriteToLog("Uninstalling remote access application")

        Dim sParams As String
        Dim sId As String

        oProcess = New ProcessRunner

        sId = ProductGUID.GetUninstallIdentifier("pcanywhere")

        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            Exit Sub
        End If

        sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = cUNINSTALL_COMMAND
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _StopServices()
        oLogger.WriteToLog("Removing services")

        oLogger.WriteToLog("Killing GuestTek processes", , 1)
        __KillProcesses_GuestTek()

        Dim oService As WindowsService
        Dim sStatus As String

        oLogger.WriteToLog("LobbyPCAgent_Heartbeat", , 1)
        oService = New WindowsService
        oService.ServiceName = "LobbyPCAgent_Heartbeat"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("LobbyPCAgent_Logfile", , 1)
        oService = New WindowsService
        oService.ServiceName = "LobbyPCAgent_Logfile"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("LobbyPCWatchdogService", , 1)
        oService = New WindowsService
        oService.ServiceName = "LobbyPCWatchdogService"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)



        oLogger.WriteToLog("OVCCAgent_Heartbeat", , 1)
        oService = New WindowsService
        oService.ServiceName = "OVCCAgent_Heartbeat"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("OVCCAgent_Logfile", , 1)
        oService = New WindowsService
        oService.ServiceName = "OVCCAgent_Logfile"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("OVCCWatchdog", , 1)
        oService = New WindowsService
        oService.ServiceName = "OVCCWatchdog"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)


        oLogger.WriteToLog("Killing any left-over GuestTek processes", , 1)
        __KillProcesses_GuestTek()

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __KillProcesses_GuestTek()
        'old watchdog
        _KillProcess("lpcwatchdog_service")
        _KillProcess("lpcwatchdog_warning")
        _KillProcess("lpcwatchdog_trigger")
        _KillProcess("lpcwatchdog_lastactivity")
        _KillProcess("lpcwatchdog_enable")
        _KillProcess("lpcwatchdog_disable")

        'new watchdog
        _KillProcess("ovccwatchdog_service.exe")
        _KillProcess("ovccwatchdog_warning.exe")
        _KillProcess("ovccwatchdog_trigger.exe")
        _KillProcess("ovccwatchdog_lastactivity.exe")
        _KillProcess("ovccwatchdog_enable.exe")
        _KillProcess("ovccwatchdog_disable.exe")
        _KillProcess("ovccwatchdog_error.exe")

        'misc
        _KillProcess("ovccstart")
        _KillProcess("ovccinformationupdater")
    End Sub

    Private Sub _KillProcess(ByVal sProcessName As String)
        Dim p As System.Diagnostics.Process

        oLogger.WriteToLog(sProcessName, , 2)
        For Each p In System.Diagnostics.Process.GetProcessesByName(sProcessName)
            Try
                p.Kill()
                oLogger.WriteToLog("dead", , 3)
            Catch ex As Exception
                oLogger.WriteToLog("zombie", , 3)
            End Try
        Next
    End Sub

    Private Sub _CleanUpFiles()
        oLogger.WriteToLog("Removing folders and files")

        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK)
        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK__x86)
        __CleanUpFolder(cCLEANFOLDER_ALTIRIS)
        __CleanUpFolder(cCLEANFOLDER_ALTIRIS__x86)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_1)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_2)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_2__x86)
        __CleanUpFolder(cCLEANFOLDER_GuestTek_1)
        __CleanUpFolder(cCLEANFOLDER_GuestTek_2)
        __CleanUpFolder(cCLEANFOLDER_GuestTek_2__x86)
        __CleanUpFolder(cCLEANFOLDER_SERIELL)
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonPrograms) & "\Altiris")
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonProgramFiles) & "\Provisio")
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonProgramFiles) & "\Altiris")

        __CleanUpFile("C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp")
        __CleanUpFile("C:\Users\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory) & "\Start LobbyPC.lnk")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu) & "\Start LobbyPC.lnk")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory) & "\OVCCInformationUpdater.lnk")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu) & "\OVCCInformationUpdater.lnk")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory) & "\OVCCStart.lnk")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu) & "\OVCCStart.lnk")
        __CleanUpFile("c:\lobbypc.ver.txt.txt")


        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __CleanUpFolder(ByVal sFolder As String, Optional ByVal bIgnoreError As Boolean = False)
        oLogger.WriteToLog("removing: " & sFolder, , 1)
        If System.IO.Directory.Exists(sFolder) Then
            Try
                __ClearAttributes(sFolder)
                System.IO.Directory.Delete(sFolder, True)
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                If Not bIgnoreError Then ErrorDetected()
            End Try
            If System.IO.Directory.Exists(sFolder) Then
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                oLogger.WriteToLog("marking for delete on reboot", , 2)
                If DeleteFileOnReboot(sFolder) Then
                    oLogger.WriteToLog("ok", , 3)
                Else
                    oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                End If

                If Not bIgnoreError Then ErrorDetected()
            Else
                oLogger.WriteToLog("ok", , 2)
            End If
        Else
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub __CleanUpFile(ByVal sFile As String)
        oLogger.WriteToLog("removing: " & sFile, , 1)
        If System.IO.File.Exists(sFile) Then
            Try
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
                System.IO.File.Delete(sFile)
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                ErrorDetected()
            End Try
            If System.IO.File.Exists(sFile) Then
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                ErrorDetected()
            Else
                oLogger.WriteToLog("ok", , 2)
            End If
        Else
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub __ClearAttributes(ByVal sCurrentFolder As String)
        If System.IO.Directory.Exists(sCurrentFolder) Then
            Dim sSubFolders() As String = System.IO.Directory.GetDirectories(sCurrentFolder)
            Dim sSubFolder As String

            For Each sSubFolder In sSubFolders
                System.IO.File.SetAttributes(sSubFolder, IO.FileAttributes.Normal)
                __ClearAttributes(sSubFolder)
            Next

            Dim sFiles() As String = System.IO.Directory.GetFiles(sCurrentFolder)
            Dim sFile As String

            For Each sFile In sFiles
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
            Next
        End If
    End Sub

    Private Sub _CleanUpLocalUsers()
        oLogger.WriteToLog("Removing local user accounts")

        __CleanUpLocalUser("__LobbyPC_puppy__")
        __CleanUpFolder("C:\Documents and Settings\__LobbyPC_puppy__", True)
        __CleanUpFolder("C:\Users\__LobbyPC_puppy__", True)

        __CleanUpLocalUser("__LobbyPcPuppy__")
        __CleanUpFolder("C:\Documents and Settings\__LobbyPcPuppy__", True)
        __CleanUpFolder("C:\Users\__LobbyPcPuppy__", True)


        '__CleanUpLocalUser("iBAHN")
        '__CleanUpFolder("C:\Documents and Settings\iBAHN", True)

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __CleanUpLocalUser(ByVal sUserName As String)
        oLogger.WriteToLog("removing user: " & sUserName, , 1)

        If WindowsUser.RemoveUser(sUserName) Then
            oLogger.WriteToLog("ok", , 2)
        Else
            oLogger.WriteToLog("fail (does not exist?)", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub _CleanUpRegistry()
        oLogger.WriteToLog("Cleaning registry")

        Dim rk_SOFTWARE As Microsoft.Win32.RegistryKey
        Dim rk_RUN As Microsoft.Win32.RegistryKey

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE", , 1)
            rk_SOFTWARE = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", True)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)

            ErrorDetected()
            Exit Sub
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_SITEKIOSK, , 1)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_SITEKIOSK)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_ALTIRIS, , 1)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_ALTIRIS)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_IBAHN, , 1)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_IBAHN)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_GUESTTEK, , 1)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_GUESTTEK)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", , 1)
            rk_RUN = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)

            ErrorDetected()
            Exit Sub
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\PostSiteKioskUpdating", , 1)
            rk_RUN.DeleteValue("PostSiteKioskUpdating")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\LobbyPCWatchdogTrigger", , 1)
            rk_RUN.DeleteValue("LobbyPCWatchdogTrigger")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\SiteKiosk", , 1)
            rk_RUN.DeleteValue("SiteKiosk")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub
#End Region



#Region "DragWindow events"
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    Private Const HTCAPTION As Integer = &H2


    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picboxLogo.MouseDown
        Me.picboxLogo.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub lblTitle_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseDown
        Me.lblTitle.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Prerequisites"
    Private Sub CheckPrerequisites()
        Dim sError As String = ""

        oPrerequisites.CheckWindowsVersion(cPREREQUISITES_NeededWindowsVersion, True)
        oPrerequisites.Skip_CheckInternetConnection()
        oPrerequisites.IsUserAdmin()
        oPrerequisites.Skip_IsInstalled()
        oPrerequisites.Skip_IsUseriBAHNUser()

        If oPrerequisites.CheckComplete Then
            If oPrerequisites.AllIsOk Then
                NextGroupBox()
            Else
                sError = ""
                If oPrerequisites.AllIsOkWithoutSoftwareInstalledCheck Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("No installation found", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= "No LobbyPC Standalone installation found." & vbCrLf
                    sError &= "If you are sure you want to run the uninstaller, click 'NEXT'"

                    lblPrerequisitesError.Text = sError

                    gSkipLicenseCheck = True

                    UpdateButtons(3, 3, "CANCEL", "NEXT")
                Else
                    oLogger.WriteToLog("Prerequisites error", Logger.MESSAGE_TYPE.LOG_ERROR)


                    If Not oPrerequisites.InternetConnectivity Then
                        oLogger.WriteToLog("No internet connection", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        sError &= "No internet connection."
                    End If
                    If Not oPrerequisites.CorrectWindowsVersion Then
                        If Not sError.Equals("") Then
                            sError &= vbCrLf & vbCrLf
                        End If

                        If oPrerequisites.HigherWindowsVersionsAreAccepted Then
                            oLogger.WriteToLog(oPrerequisites.WindowsVersion & " < " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                            sError &= "Incorrect Windows version! %%NEEDED_WINDOWS_VERSION%% (or higher) is needed."
                        Else
                            oLogger.WriteToLog(oPrerequisites.WindowsVersion & " != " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                            sError &= "Incorrect Windows version! %%NEEDED_WINDOWS_VERSION%% is needed."
                        End If

                        sError = sError.Replace("%%NEEDED_WINDOWS_VERSION%%", oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion))
                        sError = sError.Replace("%%RUNNING_WINDOWS_VERSION%%", oPrerequisites.WindowsVersion)
                    End If
                    If Not oPrerequisites.UserIsAdmin Then
                        If Not sError.Equals("") Then
                            sError &= vbCrLf & vbCrLf
                        End If
                        oLogger.WriteToLog("No admin privileges", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        sError &= "This uninstallation needs to run with Administrator privileges."
                    End If
                    If Not oPrerequisites.Installed Then
                        If Not sError.Equals("") Then
                            sError &= vbCrLf & vbCrLf
                        End If
                        oLogger.WriteToLog("No installation found", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        sError &= "No LobbyPC Standalone installation found."
                    End If
                    If Not oPrerequisites.UserIsNotiBAHNUser Then
                        If Not sError.Equals("") Then
                            sError &= vbCrLf & vbCrLf
                        End If
                        oLogger.WriteToLog("Running as iBAHN user", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        sError &= "You can't run the uninstaller as the iBAHN user. Please choose another administrator account."
                    End If

                    lblPrerequisitesError.Text = sError

                    UpdateButtons(3, 0, "CANCEL", "NEXT")

                End If
            End If
        Else
            lblPrerequisitesError.Text = "Unknown error 31. Please contact the helpdesk."

            UpdateButtons(3, 0, "CANCEL", "NEXT")
        End If
    End Sub
#End Region

#Region "Groupbox handling"
    Private Sub NextGroupBox()
        iActiveGroupBox += 1
        UpdateGroupBox()
    End Sub

    Private Sub PreviousGroupBox()
        iActiveGroupBox -= 1
        'Skip these group box if we are going BACK
        'Windows version and internet connection error dialog
        If iActiveGroupBox = GroupBoxes.StartScreen Then
            iActiveGroupBox = -1
        End If
        UpdateGroupBox()
    End Sub

    Private Sub UpdateGroupBox()
        'If iActiveGroupBox >= Panels.Length Then
        '    iActiveGroupBox = Panels.Length - 1
        'End If

        'We handle the case of <0 in the following select case statement
        'If iActiveGroupBox < 0 Then
        '   iActiveGroupBox = 0
        'End If

        oLogger.WriteToLog("Groupbox / action: " & iActiveGroupBox.ToString, Logger.MESSAGE_TYPE.LOG_DEBUG)

        Select Case iActiveGroupBox
            Case GroupBoxes.Prerequisites
                UpdateButtons(0, 0, "CANCEL", "NEXT")
                CheckPrerequisites()
            Case GroupBoxes.StartScreen
                UpdateButtons(3, 3, "CANCEL", "NEXT")
            Case GroupBoxes.Uninstallation
                UpdateButtons(0, 0, "CANCEL", "NEXT")

                gBusyInstalling = True

                ProgressBarMarqueeStart()
                StartUninstallation()
            Case GroupBoxes.Done
                lblDone.Text = "Finished" & vbCrLf & vbCrLf
                If iSeriousErrorCount > 0 Then
                    lblDone.Text &= "Not all items could be deleted." & vbCrLf
                    lblDone.Text &= "Please reboot the pc, and run the uninstaller again."
                Else
                    lblDone.Text &= "Please reboot the pc to complete the uninstallation."
                End If

                oLogger.WriteToLog("Serious errors detected: " & iSeriousErrorCount.ToString, , 0)


                UpdateButtons(0, 3, "EXIT", "EXIT")
            Case Else
                Application.Exit()
        End Select

        If iActiveGroupBox >= 0 And iActiveGroupBox < Panels.Length Then
            Panels(iActiveGroupBox).Visible = True
        End If

        Dim i As Integer
        For i = 0 To Panels.Length - 1
            If i <> iActiveGroupBox Then
                Panels(i).Visible = False
            End If
        Next
    End Sub
#End Region

#Region "Button Handling"
    Private Sub UpdateButtons(ByVal iBack As Integer, ByVal iNext As Integer, Optional ByVal sBack As String = "", Optional ByVal sNext As String = "")
        btnCancel.Enabled = iBack And 1
        btnCancel.Visible = iBack And 2
        If Not sBack.Equals("") Then
            btnCancel.Text = sBack
        End If

        btnNext.Enabled = iNext And 1
        btnNext.Visible = iNext And 2
        If Not sNext.Equals("") Then
            btnNext.Text = sNext
        End If
    End Sub
#End Region

#Region "Progress bars"
    Private Sub ProgressBarMarqueeStart()
        progressMarquee.Style = ProgressBarStyle.Marquee
        progressMarquee.MarqueeAnimationSpeed = 100
        progressMarquee.Value = 0
    End Sub

    Private Sub ProgressBarMarqueeStop()
        progressMarquee.Style = ProgressBarStyle.Blocks
        progressMarquee.MarqueeAnimationSpeed = 0
        progressMarquee.Value = 0
    End Sub
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationExit()
        oLogger.WriteToLog("exiting...")
        oLogger.WriteToLog(New String("=", 50))

        Application.Exit()
    End Sub
#End Region

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        NextGroupBox()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ApplicationExit()
    End Sub

    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            iApplicationInstallationCount += 1
            InstallationController()
        End If
    End Sub

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLog("done", , 1)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLog("failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)

        If iApplicationInstallationCount = InstallationProgress.UninstallSiteKiosk Then
            bSiteKioskUninstallTimeout = True
        Else
            ErrorDetected()
        End If

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_KillFailed(ByVal EventProcess As System.Diagnostics.Process, ByVal ErrorMessage As String) Handles oProcess.KillFailed
        oLogger.WriteToLog("killing process failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLog("started", , 1)
        oLogger.WriteToLog("id  : " & EventProcess.Id.ToString, , 2)
        oLogger.WriteToLog("name: " & EventProcess.ProcessName, , 2)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLog("timed out", , 1)

        If iApplicationInstallationCount = InstallationProgress.UninstallSiteKiosk Then
            bSiteKioskUninstallTimeout = True
        Else
            ErrorDetected()
        End If

        oProcess.ProcessDone()
    End Sub
#End Region

    Private Sub UpdateInstallationProgressCount()
        lblProgress.Text = (iApplicationInstallationCount + 1).ToString & " / " & (iApplicationInstallationCountMax).ToString
        lblProgress.Visible = True
    End Sub

    Private Sub ErrorDetected()
        oLogger.WriteToLogRelative("Error detected", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        iSeriousErrorCount += 1
    End Sub

    Private Sub lblDone_Click(sender As Object, e As EventArgs) Handles lblDone.Click

    End Sub
End Class
