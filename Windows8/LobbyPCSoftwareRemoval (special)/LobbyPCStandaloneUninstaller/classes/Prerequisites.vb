Imports System.Net
Imports System.Environment

Public Class Prerequisites
    Public Enum WINDOWS_VERSION
        UNKNOWN
        WINDOWS_95
        WINDOWS_98
        WINDOWS_MILLENIUM
        WINDOWS_NT_3
        WINDOWS_NT_4
        WINDOWS_2000
        WINDOWS_XP
        WINDOWS_VISTA
        WINDOWS_7
        WINDOWS_8
        WINDOWS_8_1
        WINDOWS_10_OR_HIGHER
    End Enum

    Private mWindowsVersion As WINDOWS_VERSION

    Private mCheckedWindowsVersion As Boolean
    Private mCorrectWindowsVersion As Boolean
    Private mHigherWindowsVersionsAreAccepted As Boolean

    Private mCheckedInternetConnection As Boolean
    Private mCorrectInternetConnection As Boolean

    Private mCheckedInstalled As Boolean
    Private mInstalled As Boolean

    Private mCheckedUserIsAdmin As Boolean
    Private mUserIsAdmin As Boolean

    Private mCheckedUserIsNotiBAHNUser As Boolean
    Private mUserIsNotiBAHNUser As Boolean

    Private mCheckedUserIsNotGuestTekUser As Boolean
    Private mUserIsNotGuestTekUser As Boolean

    Public Sub New()
        mCheckedWindowsVersion = False
        mCorrectWindowsVersion = False

        mCheckedInternetConnection = False
        mCorrectInternetConnection = False

        mCheckedUserIsAdmin = False
        mUserIsAdmin = False

        mCheckedInstalled = False
        mInstalled = False

        mCheckedUserIsNotiBAHNUser = False
        mUserIsNotiBAHNUser = False

        mCheckedUserIsNotGuestTekUser = False
        mUserIsNotGuestTekUser = False
    End Sub

    Public Sub ForcePrerequisiteErrors(ByVal bWindowsVersion As Boolean, ByVal bInternetConnection As Boolean, ByVal bUserIsAdmin As Boolean, ByVal bAlreadyInstalled As Boolean)
        If bWindowsVersion Then
            mCheckedWindowsVersion = True
            mCorrectWindowsVersion = False
        End If

        If bInternetConnection Then
            mCheckedInternetConnection = True
            mCorrectInternetConnection = False
        End If

        If bUserIsAdmin Then
            mCheckedUserIsAdmin = True
            mUserIsAdmin = False
        End If

        If bAlreadyInstalled Then
            mCheckedInstalled = True
            mInstalled = True
        End If
    End Sub

    Public ReadOnly Property CheckComplete() As Boolean
        Get
            Return (mCheckedWindowsVersion And mCheckedInternetConnection And mCheckedUserIsAdmin And mCheckedInstalled And mCheckedUserIsNotiBAHNUser And mCheckedUserIsNotGuestTekUser)
        End Get
    End Property

    Public ReadOnly Property AllIsOk() As Boolean
        Get
            Return (mCorrectWindowsVersion And mCorrectInternetConnection And mUserIsAdmin And mInstalled And mUserIsNotiBAHNUser And mUserIsNotGuestTekUser)
        End Get
    End Property

    Public ReadOnly Property AllIsOkWithoutSoftwareInstalledCheck() As Boolean
        Get
            Return (mCorrectWindowsVersion And mCorrectInternetConnection And mUserIsAdmin And mUserIsNotiBAHNUser And mUserIsNotGuestTekUser)
        End Get
    End Property

    Public ReadOnly Property CorrectWindowsVersion() As Boolean
        Get
            Return mCorrectWindowsVersion
        End Get
    End Property

    Public ReadOnly Property HigherWindowsVersionsAreAccepted() As Boolean
        Get
            Return mHigherWindowsVersionsAreAccepted
        End Get
    End Property

    Public ReadOnly Property InternetConnectivity() As Boolean
        Get
            Return mCorrectInternetConnection
        End Get
    End Property

    Public ReadOnly Property WindowsVersion() As String
        Get
            Return HumanReadableWindowsVersion(mWindowsVersion)
        End Get
    End Property

    Public ReadOnly Property UserIsAdmin() As Boolean
        Get
            Return mUserIsAdmin
        End Get
    End Property

    Public ReadOnly Property UserIsNotiBAHNUser() As Boolean
        Get
            Return mUserIsNotiBAHNUser
        End Get
    End Property

    Public ReadOnly Property UserIsNotGuestTekUser() As Boolean
        Get
            Return mUserIsNotGuestTekUser
        End Get
    End Property

    Public ReadOnly Property Installed() As Boolean
        Get
            Return mInstalled
        End Get
    End Property

    Public Sub Skip_CheckWindowsVersion()
        mCorrectWindowsVersion = True
        mCheckedWindowsVersion = True
    End Sub

    Public Function CheckWindowsVersion(ByVal MinimumVersion As WINDOWS_VERSION, Optional ByVal OrHigher As Boolean = False) As Boolean
        mWindowsVersion = TranslateWindowsVersion(OSVersion.Platform, OSVersion.Version.Major.ToString, OSVersion.Version.Minor.ToString)
        mHigherWindowsVersionsAreAccepted = OrHigher

        If OrHigher Then
            mCorrectWindowsVersion = (mWindowsVersion >= MinimumVersion)
        Else
            mCorrectWindowsVersion = (mWindowsVersion = MinimumVersion)
        End If

        mCheckedWindowsVersion = True

        Return mCorrectWindowsVersion
    End Function

    Public Sub Skip_CheckInternetConnection()
        mCorrectInternetConnection = True
        mCheckedInternetConnection = True
    End Sub

    Public Function CheckInternetConnection() As Boolean
        Dim objUrl As New System.Uri("http://www.google.com/")
        Dim objWebReq As WebRequest
        objWebReq = WebRequest.Create(objUrl)
        Dim objResp As WebResponse

        Try
            objResp = objWebReq.GetResponse
            objResp.Close()
            objWebReq = Nothing
            mCorrectInternetConnection = True
        Catch ex As Exception
            'objResp.Close()
            objWebReq = Nothing
            mCorrectInternetConnection = False
        End Try

        mCheckedInternetConnection = True

        Return mCorrectInternetConnection
    End Function

    Public Function IsUserAdmin() As Boolean
        mUserIsAdmin = UserFunctions.AdminCheck.IsRunningAsAdmin
        mCheckedUserIsAdmin = True

        Return mUserIsAdmin
    End Function

    Public Sub Skip_IsUseriBAHNUser()
        mUserIsNotiBAHNUser = True
        mCheckedUserIsNotiBAHNUser = True
    End Sub

    Public Sub Skip_IsUserGuestTekUser()
        mUserIsNotGuestTekUser = True
        mCheckedUserIsNotGuestTekUser = True
    End Sub

    Public Function IsUseriBAHNUser() As Boolean
        If My.User.Name.Substring(My.User.Name.LastIndexOf("\") + 1).ToLower = "ibahn" Then
            mUserIsNotiBAHNUser = False
        Else
            mUserIsNotiBAHNUser = True
        End If
        mCheckedUserIsNotiBAHNUser = True

        Return mUserIsNotiBAHNUser
    End Function

    Public Function IsUserGuestTekUser() As Boolean
        If My.User.Name.Substring(My.User.Name.LastIndexOf("\") + 1).ToLower = "guesttek" Then
            mUserIsNotguesttekUser = False
        Else
            mUserIsNotGuestTekUser = True
        End If
        mCheckedUserIsNotGuestTekUser = True

        Return mUserIsNotGuestTekUser
    End Function

    Public Sub Skip_IsInstalled()
        mInstalled = True
        mCheckedInstalled = True
    End Sub

    Public Function IsInstalled() As Boolean
        Return _IsInstalled_GuestTek() Or _IsInstalled_iBAHN() Or _IsInstalled_OVCC()
    End Function

    Private Function _IsInstalled_iBAHN() As Boolean
        Dim sValue As String

        sValue = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPC StandAlone\Backup", "LicenseKey", "")

        If sValue Is Nothing Then
            sValue = ""
        End If

        mInstalled = False
        If sValue <> "" Then
            mInstalled = True
        End If

        mCheckedInstalled = True

        Return mInstalled
    End Function

    Private Function _IsInstalled_GuestTek() As Boolean
        Dim sValue As String

        sValue = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\LobbyPC StandAlone\Backup", "LicenseKey", "")

        If sValue Is Nothing Then
            sValue = ""
        End If

        mInstalled = False
        If sValue <> "" Then
            mInstalled = True
        End If

        mCheckedInstalled = True

        Return mInstalled
    End Function

    Private Function _IsInstalled_OVCC() As Boolean
        Dim sValue As String

        sValue = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCStandAlone\Backup", "LicenseKey", "")

        If sValue Is Nothing Then
            sValue = ""
        End If

        mInstalled = False
        If sValue <> "" Then
            mInstalled = True
        End If

        mCheckedInstalled = True

        Return mInstalled
    End Function


    Private Function TranslateWindowsVersion(ByVal PlatformID As String, ByVal MajorVersion As String, ByVal MinorVersion As String) As WINDOWS_VERSION
        Dim sVersionString As String

        sVersionString = PlatformID & "." & MajorVersion & "." & MinorVersion
        Select Case sVersionString
            Case "1.4.0"
                Return WINDOWS_VERSION.WINDOWS_95
            Case "1.4.10"
                Return WINDOWS_VERSION.WINDOWS_98
            Case "1.4.98"
                Return WINDOWS_VERSION.WINDOWS_MILLENIUM
            Case "2.3.51"
                Return WINDOWS_VERSION.WINDOWS_NT_3
            Case "2.4.0"
                Return WINDOWS_VERSION.WINDOWS_NT_4
            Case "2.5.0"
                Return WINDOWS_VERSION.WINDOWS_2000
            Case "2.5.1"
                Return WINDOWS_VERSION.WINDOWS_XP
            Case "2.6.0"
                Return WINDOWS_VERSION.WINDOWS_VISTA
            Case "2.6.1"
                Return WINDOWS_VERSION.WINDOWS_7
            Case "2.6.2"
                Return WINDOWS_VERSION.WINDOWS_8
            Case "2.6.3"
                Return WINDOWS_VERSION.WINDOWS_8_1
            Case Else
                Return WINDOWS_VERSION.UNKNOWN
        End Select
    End Function

    Public Function HumanReadableWindowsVersion(ByVal WindowsVersion As WINDOWS_VERSION) As String
        Select Case WindowsVersion
            Case WINDOWS_VERSION.WINDOWS_95
                Return "Windows 95"
            Case WINDOWS_VERSION.WINDOWS_98
                Return "Windows 98"
            Case WINDOWS_VERSION.WINDOWS_MILLENIUM
                Return "Windows ME"
            Case WINDOWS_VERSION.WINDOWS_NT_3
                Return "Windows NT3"
            Case WINDOWS_VERSION.WINDOWS_NT_4
                Return "Windows NT4"
            Case WINDOWS_VERSION.WINDOWS_2000
                Return "Windows 2000"
            Case WINDOWS_VERSION.WINDOWS_XP
                Return "Windows XP"
            Case WINDOWS_VERSION.WINDOWS_VISTA
                Return "Windows Vista"
            Case WINDOWS_VERSION.WINDOWS_7
                Return "Windows 7"
            Case Else
                Return "Unknown Windows version"
        End Select
    End Function
End Class
