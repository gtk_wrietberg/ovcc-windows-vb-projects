﻿Public Class OVCCIntegrity
    Public Class Settings
        Private Shared ReadOnly c_RegRoot As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCIntegrityChecker"

        Public Class SiteKioskLicense
            Private Shared mLicensee As String
            Public Shared Property Licensee() As String
                Get
                    Return mLicensee
                End Get
                Set(ByVal value As String)
                    mLicensee = value
                End Set
            End Property

            Private Shared mSignature As String
            Public Shared Property Signature() As String
                Get
                    Return mSignature
                End Get
                Set(ByVal value As String)
                    mSignature = value
                End Set
            End Property
        End Class

        Public Class Files
            Private Shared mPathMaster As String
            Public Shared Property Path_Master() As String
                Get
                    Return mPathMaster
                End Get
                Set(ByVal value As String)
                    mPathMaster = value
                End Set
            End Property

            Private Shared mPathSearch As String
            Public Shared Property Path_Search() As String
                Get
                    Return mPathSearch
                End Get
                Set(ByVal value As String)
                    mPathSearch = value
                End Set
            End Property
        End Class

        Public Shared Function Load() As Boolean
            SiteKioskLicense.Licensee = Helpers.Registry.GetValue_String(c_RegRoot & "\Settings", "SiteKioskLicense__Licensee")
            SiteKioskLicense.Signature = Helpers.Registry.GetValue_String(c_RegRoot & "\Settings", "SiteKioskLicense__Signature")

            Files.Path_Master = Helpers.Registry.GetValue_String(c_RegRoot & "\Settings", "Files__Path_Master")
            Files.Path_Search = Helpers.Registry.GetValue_String(c_RegRoot & "\Settings", "Files__Path_Search")

            Return True
        End Function

        Private m As String
        Public Property NewProperty() As String
            Get
                Return m
            End Get
            Set(ByVal value As String)
                m = value
            End Set
        End Property
    End Class

    Public Class Check
        Public Shared Function SiteKioskLicense() As Boolean
            Helpers.Logger.WriteMessageRelative("Checking SiteKiosk License", 1)

            Dim sCurrent As String = "", sCheck As String = ""

            sCurrent = Helpers.SiteKiosk.License.GetLicensee
            sCheck = Settings.SiteKioskLicense.Licensee

            Helpers.Logger.WriteMessageRelative("Current", 2)
            Helpers.Logger.WriteMessageRelative(sCurrent, 3)

            Helpers.Logger.WriteMessageRelative("Check", 2)
            Helpers.Logger.WriteMessageRelative(sCheck, 3)

            If sCheck = "" Then

                Return False
            End If

            If sCheck.Equals(sCurrent) Then

            Else

            End If
        End Function
    End Class

    Public Class Files
        Public Class FileItem
            Private mFilePath As String
            Public Property Path() As String
                Get
                    Return mFilePath
                End Get
                Set(ByVal value As String)
                    mFilePath = value
                End Set
            End Property

            Private mLastModifified As Date
            Public Property LastModified() As Date
                Get
                    Return mLastModifified
                End Get
                Set(ByVal value As Date)
                    mLastModifified = value
                End Set
            End Property
        End Class


    End Class
End Class
