﻿Public Class Constants
    Public Class RegistryKeys
        Public Shared ReadOnly KEY_Windows_RunAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run"
        Public Shared ReadOnly KEY_Windows_RunOnceAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunOnce"


        Public Shared ReadOnly KEY__Watchdog As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\IntegrityChecker"
        Public Shared ReadOnly VALUE__LastState As String = "LastState"
        Public Shared ReadOnly VALUE__DisabledUntilOVCCStart As String = "DisabledUntilOVCCStart"

        Public Shared ReadOnly DEFAULTVALUE__LastState As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__DisabledUntilOVCCStart As Boolean = True


        Public Shared ReadOnly KEY__Watchdog_Settings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\Settings"
        Public Shared ReadOnly VALUE__WatchdogSettingsDebug As String = "Debug"
        Public Shared ReadOnly VALUE__WatchdogSettingsTestMode As String = "TestMode"
        Public Shared ReadOnly VALUE__WatchdogSettingsSkipFirstRunDelay As String = "SkipFirstRunDelay"
        Public Shared ReadOnly VALUE__WatchdogSettingsCheckForLogMeInConnection As String = "CheckForLogMeInConnection"
        Public Shared ReadOnly VALUE__WatchdogSettingsPlaysound As String = "PlayStupidDogSound"
        Public Shared ReadOnly VALUE__WatchdogSettingsLogMeInThreshold As String = "LogMeInThreshold"
        Public Shared ReadOnly VALUE__WatchdogSettingsZipOldLogs As String = "ZipOldLogs"

        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsDebug As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsTestMode As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsSkipFirstRunDelay As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsCheckForLogMeInConnection As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsPlaysound As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsLogMeInThreshold As Integer = 4
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsZipOldLogs As Boolean = False


        Public Shared ReadOnly KEY__Watchdog_Logging As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\Logging"
        Public Shared ReadOnly VALUE__CurrentLogfilePath As String = "CurrentLogfilePath"
        Public Shared ReadOnly VALUE__CurrentLogfileName As String = "CurrentLogfileName"

        Public Shared ReadOnly DEFAULTVALUE__CurrentLogfilePath As String = ""
        Public Shared ReadOnly DEFAULTVALUE__CurrentLogfileName As String = ""


        Public Shared ReadOnly KEY__Watchdog_OpenToAll As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_"
        Public Shared ReadOnly VALUE__OpenToAll_WriteCheck As String = "WriteCheck"
        Public Shared ReadOnly VALUE__OpenToAll_WriteCheckTimestamp As String = "WriteCheckTimestamp"

        Public Shared ReadOnly DEFAULTVALUE__OpenToAll_WriteCheck As String = ""


        Public Shared ReadOnly KEY__Watchdog_Disabled As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\Disabled"
        Public Shared ReadOnly VALUE__WatchdogDisabled As String = "Disabled"

        Public Shared ReadOnly DEFAULTVALUE__WatchdogDisabled As String = ""


        Public Shared ReadOnly KEY__Watchdog_Error As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\Error"
        Public Shared ReadOnly VALUE__WatchdogError_Timestamp As String = "Timestamp"
        Public Shared ReadOnly VALUE__WatchdogError_Title As String = "Title"
        Public Shared ReadOnly VALUE__WatchdogError_Description As String = "Description"
        Public Shared ReadOnly VALUE__WatchdogError_Severity As String = "Severity"
        Public Shared ReadOnly VALUE__WatchdogError_RebootOption As String = "RebootOption"

        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_Timestamp As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_Title As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_Description As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_Severity As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_RebootOption As Boolean = False


        Public Shared ReadOnly KEY__Watchdog_ErrorHistory As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\Error\History"
        Public Shared ReadOnly VALUE__WatchdogError_History As String() = New String() {"History00", "History01", "History02", "History03", "History04", "History05", "History06", "History07", "History08", "History09", "History10", "History11", "History12", "History13", "History14", "History15", "History16", "History17", "History18", "History19"}

        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_History As String = ""


        Public Shared ReadOnly KEY__Watchdog_Warning As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\Warning"
        Public Shared ReadOnly VALUE__WarningPopupTriggerStarted As String = "TriggerStarted"
        Public Shared ReadOnly VALUE__WarningPopupTriggerCount As String = "TriggerCount"
        Public Shared ReadOnly VALUE__WarningPopupTestmode As String = "Testmode"
        Public Shared ReadOnly VALUE__WarningPopupLoose As String = "Loose"
        Public Shared ReadOnly VALUE__WarningPopupFailure As String = "Failure"
        Public Shared ReadOnly VALUE__WarningPopupFailureLastError As String = "FailureLastError"
        Public Shared ReadOnly VALUE__WarningPopupCountdown As String = "Countdown"
        Public Shared ReadOnly VALUE__WarningPopupTimestamp As String = "Timestamp"

        Public Shared ReadOnly DEFAULTVALUE__WarningPopupTriggerStarted As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupTriggerCount As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupTestmode As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupLoose As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupFailure As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupFailureLastError As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupCountdown As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupTimestamp As Integer = 0


        Public Shared ReadOnly KEY__Watchdog_LastActivity As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\LastActivity"
        Public Shared ReadOnly VALUE__LastActivity As String = "LastActivity"

        Public Shared ReadOnly DEFAULTVALUE__LastActivity As Integer = 0


        Public Shared ReadOnly KEY__Watchdog_AdminMode As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\AdminMode"
        Public Shared ReadOnly VALUE__AdminModeActive As String = "Active"
        Public Shared ReadOnly VALUE__AdminModeActiveTime As String = "Time"

        Public Shared ReadOnly DEFAULTVALUE__AdminModeActive As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__AdminModeActiveTime As Integer = 0


        Public Shared ReadOnly KEY__Puppypower As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\PuppyPower"
        Public Shared ReadOnly VALUE__PuppyPowerUserName As String = "username"
        Public Shared ReadOnly VALUE__PuppyPowerPassWord As String = "password"

        Public Shared ReadOnly DEFAULTVALUE__PuppyPowerUserName As String = ""
        Public Shared ReadOnly DEFAULTVALUE__PuppyPowerPassWord As String = ""


        Public Shared ReadOnly KEY__SiteKiosk As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"
        Public Shared ReadOnly VALUE__Build As String = "Build"
        Public Shared ReadOnly VALUE__InstallDir As String = "InstallDir"
        Public Shared ReadOnly VALUE__LastCfg As String = "LastCfg"
    End Class

    Public Class Service
        Public Shared ReadOnly Name As String = "OVCCWatchdog"
        Public Shared ReadOnly DisplayName As String = "OVCC Watchdog service"
        Public Shared ReadOnly Description As String = "Keeps OVCC in secure mode"
    End Class

End Class
