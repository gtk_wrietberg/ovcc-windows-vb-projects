﻿Public Class IntegrityCheckerService
    Private mThreading_Main As Threading.Thread

    Protected Overrides Sub OnStart(ByVal args() As String)
        'init logger
        Helpers.Logger.InitialiseLogger()

        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("service started", , 0)

    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()

        Helpers.Logger.Write("service stopped")

    End Sub

    Private Sub Thread_Main()

    End Sub

    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try
    End Sub

End Class
