﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.IntegrityCheckerServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.IntegrityCheckerServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'IntegrityCheckerServiceProcessInstaller
        '
        Me.IntegrityCheckerServiceProcessInstaller.Password = Nothing
        Me.IntegrityCheckerServiceProcessInstaller.Username = Nothing
        '
        'IntegrityCheckerServiceInstaller
        '
        Me.IntegrityCheckerServiceInstaller.Description = "Check integrity of OVCC machine"
        Me.IntegrityCheckerServiceInstaller.DisplayName = "IntegrityCheckerService"
        Me.IntegrityCheckerServiceInstaller.ServiceName = "IntegrityCheckerService"
        Me.IntegrityCheckerServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.IntegrityCheckerServiceProcessInstaller, Me.IntegrityCheckerServiceInstaller})

    End Sub

    Friend WithEvents IntegrityCheckerServiceProcessInstaller As ServiceProcess.ServiceProcessInstaller
    Friend WithEvents IntegrityCheckerServiceInstaller As ServiceProcess.ServiceInstaller
End Class
