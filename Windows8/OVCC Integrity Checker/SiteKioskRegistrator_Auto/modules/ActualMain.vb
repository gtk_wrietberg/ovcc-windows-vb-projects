﻿Module modActualMain
    Public Sub ActualMain()



        '---------------------------------------------------------------------------------------------------------------------------------------------
        Dim sLicensee As String = "", sSignature As String = ""

        Helpers.Logger.WriteMessage("Check SiteKiosk License", 0)

        Helpers.Logger.WriteMessage(c_REGVALUE_LICENSEE, 1)
        Helpers.Logger.WriteMessage("current value", 2)

        sLicensee = Helpers.Registry.GetValue_String(c_REGROOT_SITEKIOSK, c_REGVALUE_LICENSEE, "(not set)")
        Helpers.Logger.WriteMessage(sLicensee, 3)

        If Not sLicensee.Equals(mLicensee) Then
            Helpers.Logger.WriteWarning("not ok", 4)

            Helpers.Logger.WriteMessage("updating", 2)
            Helpers.Logger.WriteMessage(mLicensee, 3)

            Helpers.Registry.SetValue_String(c_REGROOT_SITEKIOSK, c_REGVALUE_LICENSEE, mLicensee)

            Helpers.Logger.WriteMessage("double checking", 2)
            sLicensee = Helpers.Registry.GetValue_String(c_REGROOT_SITEKIOSK, c_REGVALUE_LICENSEE, "(not set)")

            If sLicensee.Equals(mLicensee) Then
                Helpers.Logger.WriteMessage("ok", 3)
            Else
                Helpers.Logger.WriteError("FAIL!", 3)
                ExitCode.SetValue(ExitCode.ExitCodes.LICENSEE_NOTCORRECTED)
            End If
        Else
            Helpers.Logger.WriteMessage("ok", 4)
        End If


        Helpers.Logger.WriteMessage(c_REGVALUE_SIGNATURE, 1)
        Helpers.Logger.WriteMessage("current value", 2)

        sSignature = Helpers.Registry.GetValue_String(c_REGROOT_SITEKIOSK, c_REGVALUE_SIGNATURE, "(not set)")
        Helpers.Logger.WriteMessage(sSignature, 3)

        If Not sSignature.Equals(mSignature) Then
            Helpers.Logger.WriteWarning("not ok", 4)

            Helpers.Logger.WriteMessage("updating", 2)
            Helpers.Logger.WriteMessage(mSignature, 3)

            Helpers.Registry.SetValue_String(c_REGROOT_SITEKIOSK, c_REGVALUE_SIGNATURE, mSignature)

            Helpers.Logger.WriteMessage("double checking", 2)
            sSignature = Helpers.Registry.GetValue_String(c_REGROOT_SITEKIOSK, c_REGVALUE_SIGNATURE, "(not set)")

            If sSignature.Equals(mSignature) Then
                Helpers.Logger.WriteMessage("ok", 3)
            Else
                Helpers.Logger.WriteError("FAIL!", 3)
                ExitCode.SetValue(ExitCode.ExitCodes.SIGNATURE_NOTCORRECTED)
            End If
        Else
            Helpers.Logger.WriteMessage("ok", 4)
        End If


        '---------------------------------------------------------------------------------------------------------------------------------------------
        ApplicationExit()
    End Sub

    Public Sub ApplicationExit()
        Helpers.Logger.WriteMessage("done", 0)
        Helpers.Logger.WriteMessage("exit code", 1)
        Helpers.Logger.WriteMessage(ExitCode.ToString, 2)
        Helpers.Logger.WriteMessage("bye", 1)

        Environment.ExitCode = ExitCode.GetValue()
        Environment.Exit(ExitCode.GetValue())
    End Sub
End Module
