﻿Module Constants
    Public ReadOnly c_REGROOT_GUESTTEK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCSoftware\Config"
    Public ReadOnly c_REGVALUE_LICENSE As String = "License"
    Public ReadOnly c_REGVALUE_LICENSEFILE As String = "LicenseFile"

    Public ReadOnly c_REGROOT_SITEKIOSK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"
    Public ReadOnly c_REGVALUE_LICENSEE As String = "Licensee"
    Public ReadOnly c_REGVALUE_SIGNATURE As String = "Signature"

    Public ReadOnly c_LICENSE_SITEKIOSK9_LICENSEE__EU_old As String = """Guest-Tek Interactive Entertainment Ltd. SK9 975754 - 10 License(s)"""
    Public ReadOnly c_LICENSE_SITEKIOSK9_SIGNATURE__EU_old As String = """VGHXJ-C782B-J2P76-PFPQ4-2Q238"""

    Public ReadOnly c_LICENSE_SITEKIOSK9_LICENSEE__EU As String = """Guest-Tek Interactive Entertainment Ltd. SK9 979682 - 170 License(s)"""
    Public ReadOnly c_LICENSE_SITEKIOSK9_SIGNATURE__EU As String = """9W83K-XD6F6-B8JJR-Q763M-XPJ7F"""

    Public ReadOnly c_LICENSE_SITEKIOSK9_LICENSEE__US_old As String = """Guest-Tek Interactive Entertainment Ltd. SK9 100622 - 10 License(s)"""
    Public ReadOnly c_LICENSE_SITEKIOSK9_SIGNATURE__US_old As String = """4HB7K-VQG9M-HHCRM-RRM3Y-Y3P62"""

    Public ReadOnly c_LICENSE_SITEKIOSK9_LICENSEE__US As String = """Guest-Tek Interactive Entertainment Ltd. SKP 103281 - 80 License(s)"""
    Public ReadOnly c_LICENSE_SITEKIOSK9_SIGNATURE__US As String = """YFVDJ-74Y2C-Q4PMV-27JPQ-6BV9B"""


    Public mLicensee As String = ""
    Public mSignature As String = ""
End Module
