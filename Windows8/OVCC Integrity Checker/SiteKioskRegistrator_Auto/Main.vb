﻿Module modMain
    Public Sub Main()
        ExitCode.SetValue(ExitCode.ExitCodes.OK)
        '---------------------------------------------------------------------------------------------------------------------------------------------


        Helpers.Logger.InitialiseLogger()
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)

        '---------------------------------------------------------------------------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Finding license region", 0)
        Dim _license_region As String = ""
        Dim _machine_name As String = ""

        _license_region = Helpers.Registry.GetValue_String(c_REGROOT_GUESTTEK, c_REGVALUE_LICENSE, "").ToLower
        If _license_region <> "eu" And _license_region <> "us" Then
            If _license_region = "" Then
                Helpers.Logger.WriteError("not found", 1)
            Else
                Helpers.Logger.WriteError("invalid value", 1)
                Helpers.Logger.WriteError(_license_region, 2)
            End If

            ExitCode.SetValue(ExitCode.ExitCodes.LICENSE_REGION_NOT_DETECTED)

            ApplicationExit()
        Else
            Helpers.Logger.WriteMessage("found", 1)
            Helpers.Logger.WriteMessage(_license_region, 2)

            If _license_region = "eu" Then
                mLicensee = c_LICENSE_SITEKIOSK9_LICENSEE__EU
                mSignature = c_LICENSE_SITEKIOSK9_SIGNATURE__EU
            End If

            If _license_region = "us" Then
                mLicensee = c_LICENSE_SITEKIOSK9_LICENSEE__US
                mSignature = c_LICENSE_SITEKIOSK9_SIGNATURE__US
            End If


            ActualMain()
        End If
    End Sub
End Module
