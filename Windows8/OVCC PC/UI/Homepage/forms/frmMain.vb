﻿Public Class frmMain
#Region "Hotkey handler"
    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
        If m.Msg = Hotkey.WM_HOTKEY Then
            HotKeyHandler(m.WParam)
        End If

        MyBase.WndProc(m)
    End Sub

    Private Sub HotKeyHandler(Id As IntPtr)
        If Globals.Application.IsLoading Then Exit Sub


        Select Case Id
            Case 1

            Case 2

            Case 3

            Case Else

        End Select

        MsgBox("HOTKEY: " & Id.ToString)
    End Sub
#End Region

    Private oSplash As New frmSplash

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Globals.Application.IsLoading = True


        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("started", , 0)


        '-----------------------------
        If Helpers.Processes.IsAppAlreadyRunning Then
            ApplicationExit("previous instance found")
            Exit Sub
        End If


        '-----------------------------
        'Set fullscreen
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Me.Location = New Point(0, 0)
        Me.Size = SystemInformation.PrimaryMonitorSize
        'Helpers.Forms.TopMost(Me.Handle, True)


        '-----------------------------
        oSplash.Show(Me)
        'Splash_pnlBackground.Top = 0
        'Splash_pnlBackground.Left = 0
        'Splash_pnlBackground.Width = Me.Width
        'Splash_pnlBackground.Height = Me.Height
        'Splash_pnlBackground.BackColor = Color.FromArgb(240, 240, 240)

        'Splash_pnlMain.Top = (Splash_pnlBackground.Height - Splash_pnlMain.Height) / 2
        'Splash_pnlMain.Left = (Splash_pnlBackground.Width - Splash_pnlMain.Width) / 2
        'Splash_pnlMainShadow.Top = Splash_pnlMain.Top + 6
        'Splash_pnlMainShadow.Left = Splash_pnlMain.Left + 6

        'Splash_lblProductname.Text = Application.ProductName
        'Splash_lblVersion.Text = Application.ProductVersion

        'Splash_pnlBackground.BringToFront()


        '-----------------------------
        webMain.ObjectForScripting = New WebBrowserScriptInterface()
        webMain.Url = New Uri("http://localhost:8181/index.html")
        webMain.SendToBack()
        webMain.Dock = DockStyle.Fill


        '-----------------------------
        btnEmergencyExit.Top = 12
        btnEmergencyExit.Left = Me.Width - btnEmergencyExit.Width - 12
        btnEmergencyExit.BringToFront()


        '-----------------------------
        'Hotkeys
        ' ey code list: https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
        Hotkey.FormHandle = Me.Handle
        Hotkey.Register(OVCC_Constants.HotKeys._1_PassWord)
        Hotkey.Register(OVCC_Constants.HotKeys._2_PassWord)


        '-----------------------------



    End Sub


    Private Sub HideSplashForm()
        tmrSplashFadeOut.Enabled = True
    End Sub

    Private Sub DoneLoading()
        oSplash.Hide()
        oSplash.Dispose()
        Globals.Application.IsLoading = False
    End Sub

    Private Sub btnEmergencyExit_Click(sender As Object, e As EventArgs) Handles btnEmergencyExit.Click
        Console.WriteLine("emergency exit!")

        ApplicationExit("emergency button")
    End Sub

    Private dSplashOpacity As Double = 1.0
    Private Sub tmrSplashFadeOut_Tick(sender As Object, e As EventArgs) Handles tmrSplashFadeOut.Tick
        dSplashOpacity -= 0.05
        If dSplashOpacity < 0 Then
            dSplashOpacity = 0
            oSplash.Opacity = 0
            tmrSplashFadeOut.Enabled = False

            DoneLoading()
        Else
            oSplash.Opacity = dSplashOpacity
        End If
    End Sub

    Private Sub webMain_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles webMain.DocumentCompleted
        If Not webMain.Url.ToString().Equals("about:blank") Then
            HideSplashForm()
        End If
    End Sub
End Class
