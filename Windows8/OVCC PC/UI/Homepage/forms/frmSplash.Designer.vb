﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSplash
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Splash_pnlMain = New System.Windows.Forms.Panel()
        Me.Splash_lblVersion = New System.Windows.Forms.Label()
        Me.Splash_picLogo = New System.Windows.Forms.PictureBox()
        Me.Splash_pnlMainShadow = New System.Windows.Forms.Panel()
        Me.progressLoading = New System.Windows.Forms.ProgressBar()
        Me.Splash_pnlMain.SuspendLayout()
        CType(Me.Splash_picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Splash_pnlMain
        '
        Me.Splash_pnlMain.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Splash_pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Splash_pnlMain.Controls.Add(Me.progressLoading)
        Me.Splash_pnlMain.Controls.Add(Me.Splash_lblVersion)
        Me.Splash_pnlMain.Controls.Add(Me.Splash_picLogo)
        Me.Splash_pnlMain.Location = New System.Drawing.Point(12, 12)
        Me.Splash_pnlMain.Name = "Splash_pnlMain"
        Me.Splash_pnlMain.Size = New System.Drawing.Size(436, 150)
        Me.Splash_pnlMain.TabIndex = 2
        '
        'Splash_lblVersion
        '
        Me.Splash_lblVersion.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Splash_lblVersion.Location = New System.Drawing.Point(217, 4)
        Me.Splash_lblVersion.Name = "Splash_lblVersion"
        Me.Splash_lblVersion.Size = New System.Drawing.Size(214, 18)
        Me.Splash_lblVersion.TabIndex = 2
        Me.Splash_lblVersion.Text = "v0.0.0.0"
        Me.Splash_lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Splash_picLogo
        '
        Me.Splash_picLogo.BackgroundImage = Global.OVCCPC_Homepage.My.Resources.Resources.GuestTek_Header_Logo
        Me.Splash_picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Splash_picLogo.Location = New System.Drawing.Point(3, 3)
        Me.Splash_picLogo.Name = "Splash_picLogo"
        Me.Splash_picLogo.Size = New System.Drawing.Size(428, 106)
        Me.Splash_picLogo.TabIndex = 0
        Me.Splash_picLogo.TabStop = False
        '
        'Splash_pnlMainShadow
        '
        Me.Splash_pnlMainShadow.BackColor = System.Drawing.Color.Gray
        Me.Splash_pnlMainShadow.Location = New System.Drawing.Point(17, 17)
        Me.Splash_pnlMainShadow.Name = "Splash_pnlMainShadow"
        Me.Splash_pnlMainShadow.Size = New System.Drawing.Size(436, 152)
        Me.Splash_pnlMainShadow.TabIndex = 3
        '
        'progressLoading
        '
        Me.progressLoading.BackColor = System.Drawing.Color.IndianRed
        Me.progressLoading.Location = New System.Drawing.Point(3, 115)
        Me.progressLoading.MarqueeAnimationSpeed = 10
        Me.progressLoading.Name = "progressLoading"
        Me.progressLoading.Size = New System.Drawing.Size(428, 30)
        Me.progressLoading.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.progressLoading.TabIndex = 0
        '
        'frmSplash
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.ClientSize = New System.Drawing.Size(465, 182)
        Me.Controls.Add(Me.Splash_pnlMain)
        Me.Controls.Add(Me.Splash_pnlMainShadow)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.Name = "frmSplash"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmSplash"
        Me.TopMost = True
        Me.Splash_pnlMain.ResumeLayout(False)
        CType(Me.Splash_picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Splash_pnlMain As Panel
    Friend WithEvents Splash_lblVersion As Label
    Friend WithEvents Splash_picLogo As PictureBox
    Friend WithEvents Splash_pnlMainShadow As Panel
    Friend WithEvents progressLoading As ProgressBar
End Class
