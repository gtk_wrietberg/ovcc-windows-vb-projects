﻿Public Class frmSplash
    Private Sub frmSplash_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime
        Helpers.Forms.TopMost(Me.Handle, True)

        Splash_lblVersion.Text = My.Application.Info.Version.ToString()
    End Sub
End Class