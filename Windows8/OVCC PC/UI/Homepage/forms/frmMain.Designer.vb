﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.webMain = New System.Windows.Forms.WebBrowser()
        Me.btnEmergencyExit = New System.Windows.Forms.Button()
        Me.tmrSplashFadeOut = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'webMain
        '
        Me.webMain.IsWebBrowserContextMenuEnabled = False
        Me.webMain.Location = New System.Drawing.Point(0, 0)
        Me.webMain.MinimumSize = New System.Drawing.Size(23, 23)
        Me.webMain.Name = "webMain"
        Me.webMain.ScrollBarsEnabled = False
        Me.webMain.Size = New System.Drawing.Size(820, 429)
        Me.webMain.TabIndex = 0
        Me.webMain.Url = New System.Uri("", System.UriKind.Relative)
        '
        'btnEmergencyExit
        '
        Me.btnEmergencyExit.BackColor = System.Drawing.Color.Red
        Me.btnEmergencyExit.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEmergencyExit.ForeColor = System.Drawing.Color.White
        Me.btnEmergencyExit.Location = New System.Drawing.Point(940, 12)
        Me.btnEmergencyExit.Name = "btnEmergencyExit"
        Me.btnEmergencyExit.Size = New System.Drawing.Size(173, 44)
        Me.btnEmergencyExit.TabIndex = 1
        Me.btnEmergencyExit.Text = "emergency exit"
        Me.btnEmergencyExit.UseVisualStyleBackColor = False
        '
        'tmrSplashFadeOut
        '
        Me.tmrSplashFadeOut.Interval = 10
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(1462, 1053)
        Me.Controls.Add(Me.btnEmergencyExit)
        Me.Controls.Add(Me.webMain)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.Text = "OVCC - Homepage"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents webMain As WebBrowser
    Friend WithEvents btnEmergencyExit As Button
    Friend WithEvents tmrSplashFadeOut As Timer
End Class
