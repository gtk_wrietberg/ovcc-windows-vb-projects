﻿Module modApplicationExit
    Public Sub ApplicationExit(Optional reason As String = "")

        Helpers.Logger.Write("exiting", , 0)
        If reason <> "" Then
            Helpers.Logger.Write("reason", , 1)
            Helpers.Logger.Write(reason, , 2)
        End If

        Helpers.Logger.Write("bye", , 1)

        Dim frms As New List(Of Form)
        For Each frm As Form In My.Application.OpenForms
            frms.Add(frm)
        Next

        For Each frm As Form In frms
            If Not frm Is Nothing Then
                frm.Close()
            End If
        Next
    End Sub
End Module
