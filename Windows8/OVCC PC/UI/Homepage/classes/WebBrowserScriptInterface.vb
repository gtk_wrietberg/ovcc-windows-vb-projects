﻿<System.Runtime.InteropServices.ComVisible(True)>
Public Class WebBrowserScriptInterface
    <System.Runtime.InteropServices.ComVisible(True)>
    Public Class _OVCC_PC
        <System.Runtime.InteropServices.ComVisible(True)>
        Public Class _UI
            Public Sub Test02()
                Console.WriteLine("OVCC_PC.UI.Test02 from browser")
            End Sub
        End Class

        <System.Runtime.InteropServices.ComVisible(True)>
        Public Class _Message
            Public Sub Message(msg As String)
                _msg(msg, "message")
            End Sub

            Public Sub Warning(msg As String)
                _msg(msg, "warning")
            End Sub

            Public Sub [Error](msg As String)
                _msg(msg, "error")
            End Sub

            Private Sub _msg(msg As String, type As String)
                Select Case type
                    Case "warning"
                        Console.WriteLine("[?] " & msg)
                    Case "error"
                        Console.WriteLine("[!] " & msg)
                    Case Else
                        Console.WriteLine("[.] " & msg)
                End Select
            End Sub
        End Class

        <System.Runtime.InteropServices.ComVisible(True)>
        Public Class _TermsConditions
            Public Sub Accepted()

            End Sub

            Public Sub Declined()

            End Sub
        End Class

        Public UI As New _UI
        Public Message As New _Message
        Public TermsConditions As New _TermsConditions
    End Class


    Public OVCC_PC As New _OVCC_PC
End Class
