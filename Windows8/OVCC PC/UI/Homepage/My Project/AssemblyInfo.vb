Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCCPC_Homepage")>
<Assembly: AssemblyDescription("OVCC PC - Homepage")>
<Assembly: AssemblyCompany("GuestTek")>
<Assembly: AssemblyProduct("OVCC PC - Homepage")>
<Assembly: AssemblyCopyright("Copyright ©  2024")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("a586ed87-a663-414d-9cf5-a7148b2e9586")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.24165.1955")>
<Assembly: AssemblyFileVersion("1.0.24165.1955")>

<assembly: AssemblyInformationalVersion("0.0.24165.1955")>