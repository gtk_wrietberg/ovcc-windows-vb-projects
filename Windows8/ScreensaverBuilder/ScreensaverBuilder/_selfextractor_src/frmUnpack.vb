﻿Imports Microsoft.VisualBasic
Imports System
Imports System.ComponentModel
Imports System.Environment
Imports System.IO
Imports System.IO.Compression
Imports System.Collections.Generic
Imports System.Windows.Forms
Imports System.Reflection
Imports System.Diagnostics
Imports System.Text
Imports System.Threading
Imports System.Timers
Imports System.Drawing


Public Class frmUnpack
    Private mCloseButtonEnabled As Boolean = False

    Private mPasswordTries As Integer = 0
    Private mPasswordTriesMax As Integer = 3


#Region "thread safe"
    Delegate Sub _ThreadSafe_Delegate__txtProgress_Append(ByVal [text] As String)
    Delegate Sub _ThreadSafe_Delegate__progressFiles_Value(ByVal [value] As Integer)

    Public Sub _ThreadSafe__txtProgress_Append(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__txtProgress_Append(AddressOf _ThreadSafe__txtProgress_Append)

            Me.Invoke(d, New Object() {[text]})
        Else
            txtProgress.AppendText([text])
        End If
    End Sub

    Public Sub _ThreadSafe__progressFiles_Value(ByVal [value] As Integer)
        If Me.progressFiles.InvokeRequired Then
            Dim d As New _ThreadSafe_Delegate__progressFiles_Value(AddressOf _ThreadSafe__progressFiles_Value)

            Me.Invoke(d, New Object() {[value]})
        Else
            progressFiles.Value = [value]
        End If
    End Sub
#End Region

#Region "extract and exe events"
    Private Sub ReportProgress_Files(percentage As Integer, filename As String, status As String)
        _ThreadSafe__progressFiles_Value(percentage)
        _ThreadSafe__txtProgress_Append(filename & ": " & status & vbCrLf)
    End Sub

    Private Sub ReportProgress_CRC(percentage As Integer, filename As String, status As String)
        _ThreadSafe__progressFiles_Value(percentage)
        _ThreadSafe__txtProgress_Append(filename & ": " & status & vbCrLf)
    End Sub

    Private Sub ReportProgress_Done()
        _ThreadSafe__progressFiles_Value(100)
    End Sub

    Private Sub ReportProgress_ExeStarting(filename As String)
        Logger.WriteMessage("Post-extraction exe starting: " & filename)
        _ThreadSafe__txtProgress_Append("Post-extraction exe starting: " & filename & vbCrLf)
    End Sub

    Private tmrExeTimeout As New System.Timers.Timer
    Private Sub ReportProgress_ExeStarted(filename As String, pId As Integer, timeOut_ms As Integer)
        mExeTimeout_Count = timeOut_ms
        mExeTimeout_CountMax = timeOut_ms

        tmrExeTimeout = New System.Timers.Timer
        tmrExeTimeout.Interval = 1000

        AddHandler tmrExeTimeout.Elapsed, AddressOf tmrExeTimeout_Elapsed
        tmrExeTimeout.Start()

        Logger.WriteMessage("Post-extraction exe started: " & filename & " - pid " & pId.ToString & " - timeout: " & timeOut_ms.ToString & "ms")
        _ThreadSafe__txtProgress_Append("Post-extraction exe started: " & filename & " - pid " & pId.ToString & " - timeout: " & timeOut_ms.ToString & "ms" & vbCrLf)
    End Sub

    Private Sub ReportProgress_ExeFailed(filename As String, status As String)
        tmrExeTimeout.Stop()

        Logger.WriteMessage("Post-extraction exe failed: " & filename & " - " & status)
        _ThreadSafe__txtProgress_Append("Post-extraction exe failed: " & filename & " - " & status & vbCrLf)
    End Sub

    Private Sub ReportProgress_ExeFinished(filename As String, status As String)
        tmrExeTimeout.Stop()

        If filename.Equals("") Then
            Logger.WriteMessage("Post-extraction exe finished: " & status)
            _ThreadSafe__txtProgress_Append("Post-extraction exe finished: " & status & vbCrLf)
        Else
            Logger.WriteMessage("Post-extraction exe finished: " & filename & " - " & status)
            _ThreadSafe__txtProgress_Append("Post-extraction exe finished: " & filename & " - " & status & vbCrLf)
        End If
    End Sub
#End Region

#Region "pnl border color"
    Private _panel_border_width As Integer = 1
    Private Sub pnlPassword_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles pnlPassword.Paint
        MyBase.OnPaint(e)

        Dim borderWidth As Integer = _panel_border_width
        Dim theColor As Color = Color.Red

        ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, theColor, borderWidth, ButtonBorderStyle.Solid, theColor, borderWidth, ButtonBorderStyle.Solid, theColor, borderWidth, ButtonBorderStyle.Solid, theColor, borderWidth, ButtonBorderStyle.Solid)
    End Sub

    Private Sub tmrPWDialogOscillator_Tick(sender As Object, e As EventArgs) Handles tmrPWDialogOscillator.Tick
        'toggle between 1 and 2
        If _panel_border_width = 1 Then
            _panel_border_width = 2
        Else
            _panel_border_width = 1
        End If

        pnlPassword.Refresh()
        '_panel_border_width = 3 - _panel_border_width
    End Sub
#End Region

    Private Sub frmUnpack_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mCloseButtonEnabled = False

        Me.Size = New Size(800, 400)
        Me.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath)
        Me.Text = Settings.ArchiveName
        Me.KeyPreview = True

        txtProgress.Clear()


        AddHandler Extractor.Extract.event_ExtractingProgress, AddressOf ReportProgress_Files
        AddHandler Extractor.Extract.event_CRCProgress, AddressOf ReportProgress_CRC
        AddHandler Extractor.Extract.event_Done, AddressOf ReportProgress_Done

        AddHandler Extractor.Run.event_Starting, AddressOf ReportProgress_ExeStarting
        AddHandler Extractor.Run.event_Started, AddressOf ReportProgress_ExeStarted
        AddHandler Extractor.Run.event_Failed, AddressOf ReportProgress_ExeFailed
        AddHandler Extractor.Run.event_Finished, AddressOf ReportProgress_ExeFinished


        If Settings.ArchivePassword_Encrypted.Equals("") Then
            _StartTheThing()
        Else
            pnlPassword.Visible = True
            pnlPassword.Top = (txtProgress.Height - pnlPassword.Height) / 2
            pnlPassword.Left = (txtProgress.Width - pnlPassword.Width) / 2

            lblTriesLeft.Text = mPasswordTriesMax.ToString & " tries left"

            tmrPWDialogOscillator.Enabled = True
        End If
    End Sub

    Private Sub frmUnpack_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If e.CloseReason = CloseReason.WindowsShutDown Then
            'let's not block Windows shutdown
            Return
        End If

        If mCloseButtonEnabled Then
            e.Cancel = False
        Else
            e.Cancel = True

            _ThreadSafe__txtProgress_Append("Can not close at this time, busy." & vbCrLf)
        End If
    End Sub


    Private Sub tmrStart_Tick(sender As Object, e As EventArgs) Handles tmrStart.Tick
        tmrStart.Enabled = False

        bgWorker_Unpack.RunWorkerAsync()
    End Sub

    Private mClose_CountMax As Integer = 30
    Private mClose_Count As Integer = mClose_CountMax
    Private Sub tmrDone_Tick(sender As Object, e As EventArgs) Handles tmrDone.Tick
        If mClose_CountMax <= 0 Then
            tmrDone.Enabled = False

            Exit Sub
        End If


        mClose_Count -= 1
        If mClose_Count < 0 Then
            tmrDone.Enabled = False

            _ForcedClose()
        End If

        If mClose_Count = 0 Then
            Me.Text = Settings.ArchiveName & " - closing in 1 second"
        Else
            Me.Text = Settings.ArchiveName & " - closing in " & (mClose_Count + 1).ToString & " seconds"
        End If

        _ThreadSafe__progressFiles_Value((mClose_Count / mClose_CountMax) * 100)
    End Sub

    Private mExeTimeout_CountMax As Integer = 30000
    Private mExeTimeout_Count As Integer = mExeTimeout_CountMax
    Private Sub tmrExeTimeout_Elapsed(sender As Object, e As EventArgs) 'Handles tmrExeTimeout.Tick
        If mExeTimeout_CountMax <= 0 Then
            tmrExeTimeout.Stop()

            Exit Sub
        End If

        mExeTimeout_Count -= tmrExeTimeout.Interval

        If mExeTimeout_Count <= 0 Then
            mExeTimeout_Count = 0

            tmrExeTimeout.Stop()
        End If


        _ThreadSafe__progressFiles_Value((mExeTimeout_Count / mExeTimeout_CountMax) * 100)
    End Sub

    Private Sub bgWorker_Unpack_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker_Unpack.DoWork
        Extractor.Extract.DoIt()
    End Sub

    Private Sub bgWorker_Unpack_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgWorker_Unpack.RunWorkerCompleted
        bgWorker_RunExe.RunWorkerAsync()
    End Sub

    Private Sub bgWorker_RunExe_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgWorker_RunExe.DoWork
        Extractor.Run.PostExtractingExe()
    End Sub

    Private Sub bgWorker_RunExe_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgWorker_RunExe.RunWorkerCompleted
        mCloseButtonEnabled = True
        tmrDone.Enabled = True
    End Sub

    Private Sub frmUnpack_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        If e.KeyCode = Keys.F4 AndAlso e.Alt AndAlso Not mCloseButtonEnabled Then
            'Disable the Alt+F4 key combination.
            e.Handled = True

            _ThreadSafe__txtProgress_Append("Can not close at this time, busy.")
        End If
    End Sub

    Private Sub btnVerifyPassword_Click(sender As Object, e As EventArgs) Handles btnVerifyPassword.Click
        _CheckPassword()
    End Sub

    Private Sub txtArchivePassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtArchivePassword.KeyDown
        If e.KeyData = Keys.Enter Then
            _CheckPassword()
        End If
    End Sub

    Private Sub _CheckPassword()
        Dim pw As String = Crypto.SHA._512.fromString(txtArchivePassword.Text)

        If pw.Equals(XOrObfuscation.v2.Deobfuscate(Settings.ArchivePassword_Encrypted)) Then
            _StartTheThing()
        Else
            mPasswordTries += 1

            If (mPasswordTriesMax - mPasswordTries) = 1 Then
                lblTriesLeft.Text = "1 try left"
            Else
                lblTriesLeft.Text = (mPasswordTriesMax - mPasswordTries).ToString & " tries left"
            End If


            If mPasswordTries >= mPasswordTriesMax Then
                'too bad
                _ForcedClose(ExitCode.ExitCodes.PASSWORD_INCORRECT)
            End If

            txtArchivePassword.Select()
            txtArchivePassword.SelectAll()
        End If
    End Sub

    Private Sub _StartTheThing()
        pnlPassword.Visible = False
        pnlPassword.Top = 1000
        pnlPassword.Left = 1000
        tmrPWDialogOscillator.Enabled = False

        tmrStart.Enabled = True
    End Sub



    Private Sub _ForcedClose(_exitcode As ExitCode.ExitCodes)
        ExitCode.SetValue(_exitcode)

        _ForcedClose()
    End Sub

    Private Sub _ForcedClose()
        mCloseButtonEnabled = True

        Me.Close()
    End Sub
End Class