﻿Imports System.IO

Public Class IconFileInfo
    Private mFi As FileInfo
    Private mIcon As Drawing.Icon

    Public Sub New()

    End Sub

    Public Sub New(filename As String)
        Try
            mFi = New FileInfo(filename)
            mCustomName = mFi.Name

            Try
                mIcon = Drawing.Icon.ExtractAssociatedIcon(filename)
            Catch ex As Exception

            End Try
        Catch ex As Exception

        End Try
    End Sub

    Public ReadOnly Property Icon() As Drawing.Icon
        Get
            Return mIcon
        End Get
    End Property

    Public ReadOnly Property Name() As String
        Get
            If mFi.Name.Equals(mCustomName) Then
                Return mFi.Name
            Else
                Return mCustomName
            End If
        End Get
    End Property

    Private mCustomName As String
    Public Property CustomName() As String
        Get
            Return mCustomName
        End Get
        Set(ByVal value As String)
            mCustomName = value
        End Set
    End Property

    Public ReadOnly Property Size() As Long
        Get
            Return mFi.Length
        End Get
    End Property

    Public ReadOnly Property FullName() As String
        Get
            Return mFi.FullName
        End Get
    End Property

    Public ReadOnly Property Extension() As String
        Get
            Return IO.Path.GetExtension(mFi.FullName)
        End Get
    End Property

    Public ReadOnly Property LastWriteTime() As DateTime
        Get
            Return mFi.LastWriteTime
        End Get
    End Property
End Class
