﻿Imports System
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text

Public Class Crypto
    Public Class SHA
        Public Class _256
            Public Shared Function fromFile(_file As String) As String
                Dim sRet As String = ""

                Try
                    Using _stream As BufferedStream = New BufferedStream(IO.File.OpenRead(_file), 1024 * 512)
                        Dim _sha As SHA256Managed = New SHA256Managed
                        Dim _byte As Byte() = _sha.ComputeHash(_stream)

                        sRet = BitConverter.ToString(_byte).Replace("-", String.Empty)
                    End Using
                Catch ex As Exception

                End Try

                Return sRet
            End Function

            Public Shared Function fromString(_string As String) As String
                Dim sRet As String = ""

                Try
                    Dim _sha As SHA256Managed = New SHA256Managed
                    Dim _byte_in As Byte() = Encoding.UTF8.GetBytes(_string)
                    Dim _byte_out As Byte() = _sha.ComputeHash(_byte_in)

                    sRet = BitConverter.ToString(_byte_out).Replace("-", String.Empty)
                Catch ex As Exception

                End Try

                Return sRet
            End Function
        End Class

        Public Class _512
            Public Shared Function fromFile(_file As String) As String
                Dim sRet As String = ""

                Try
                    Using _stream As BufferedStream = New BufferedStream(IO.File.OpenRead(_file), 1024 * 512)
                        Dim _sha As SHA512Managed = New SHA512Managed
                        Dim _byte As Byte() = _sha.ComputeHash(_stream)

                        sRet = BitConverter.ToString(_byte).Replace("-", String.Empty)
                    End Using
                Catch ex As Exception

                End Try

                Return sRet
            End Function

            Public Shared Function fromString(_string As String) As String
                Dim sRet As String = ""

                Try
                    Dim _sha As SHA512Managed = New SHA512Managed
                    Dim _byte_in As Byte() = Encoding.UTF8.GetBytes(_string)
                    Dim _byte_out As Byte() = _sha.ComputeHash(_byte_in)

                    sRet = BitConverter.ToString(_byte_out).Replace("-", String.Empty)
                Catch ex As Exception

                End Try

                Return sRet
            End Function
        End Class
    End Class
End Class

'Public Class CRC32
'    Public Shared Function fromFile(_file As String) As String
'        Dim sRet As String = ""

'        Try
'            Using _stream As BufferedStream = New BufferedStream(IO.File.OpenRead(_file), 1024 * 512)
'                Dim _sha As SHA256Managed = New SHA256Managed
'                Dim _byte As Byte() = _sha.ComputeHash(_stream)

'                sRet = BitConverter.ToString(_byte).Replace("-", String.Empty)
'            End Using
'        Catch ex As Exception

'        End Try

'        Return sRet
'    End Function
'End Class
