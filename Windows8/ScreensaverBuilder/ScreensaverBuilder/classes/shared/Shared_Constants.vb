﻿Public Class Shared_Constants
    Public Shared ReadOnly IdentifierFileName As String = "__SelfExtractorIdentifier__"
    Public Shared ReadOnly SettingsFileName As String = "__SelfExtractorSettings__"
    Public Shared ReadOnly FileListFileName As String = "__SelfExtractorFileList__"

    Public Shared ReadOnly NORMAL_PATH_SEPARATOR As String = "\"
    Public Shared ReadOnly INTERNAL_PATH_SEPARATOR As String = "/"

    Public Shared ReadOnly STANDARD_OK_REPLY_STRING_THINGY As String = "ok"
End Class
