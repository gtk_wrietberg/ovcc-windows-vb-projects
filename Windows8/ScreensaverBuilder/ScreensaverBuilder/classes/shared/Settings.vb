﻿Imports System


Public Class Settings
    Private Shared mSaved As Boolean
    Public Shared Property Saved() As Boolean
        Get
            Return mSaved
        End Get
        Set(ByVal value As Boolean)
            mSaved = value
        End Set
    End Property

    Private Shared mDestinationPath As String = ""
    Public Shared Property DestinationPath() As String
        Get
            Return mDestinationPath
        End Get
        Set(ByVal value As String)
            mDestinationPath = value
        End Set
    End Property

    Private Shared mArchivePath As String
    Public Shared Property ArchivePath() As String
        Get
            Return mArchivePath
        End Get
        Set(ByVal value As String)
            mArchivePath = value
        End Set
    End Property

    Private Shared mArchiveName As String
    Public Shared Property ArchiveName() As String
        Get
            Return mArchiveName
        End Get
        Set(ByVal value As String)
            mArchiveName = value
        End Set
    End Property

    Private Shared mArchiveAutoExecPath As String
    Public Shared Property ArchiveAutoExecPath() As String
        Get
            Return mArchiveAutoExecPath
        End Get
        Set(ByVal value As String)
            mArchiveAutoExecPath = value
        End Set
    End Property

    Public Shared ReadOnly Property IsArchiveAutoExecSet() As Boolean
        Get
            Return Not mArchiveAutoExecPath.Equals("")
        End Get
    End Property

    Private Shared mArchiveAutoExecParams As String
    Public Shared Property ArchiveAutoExecParams() As String
        Get
            Return mArchiveAutoExecParams
        End Get
        Set(ByVal value As String)
            mArchiveAutoExecParams = value
        End Set
    End Property

    Private Shared mArchiveAutoExecTimeout As Integer
    Public Shared Property ArchiveAutoExecTimeout() As Integer
        Get
            Return mArchiveAutoExecTimeout
        End Get
        Set(ByVal value As Integer)
            mArchiveAutoExecTimeout = value
        End Set
    End Property

    Private Shared mArchiveAutoExecHidden As Boolean
    Public Shared Property ArchiveAutoExecHidden() As Boolean
        Get
            Return mArchiveAutoExecHidden
        End Get
        Set(ByVal value As Boolean)
            mArchiveAutoExecHidden = value
        End Set
    End Property

    Private Shared mArchiveAutoExecUserName As String
    Public Shared Property ArchiveAutoExecUserName() As String
        Get
            Return mArchiveAutoExecUserName
        End Get
        Set(ByVal value As String)
            mArchiveAutoExecUserName = value
        End Set
    End Property

    Private Shared mArchiveAutoExecDomain As String
    Public Shared Property ArchiveAutoExecDomain() As String
        Get
            Return mArchiveAutoExecDomain
        End Get
        Set(ByVal value As String)
            mArchiveAutoExecDomain = value
        End Set
    End Property

    Private Shared mArchiveAutoExecPassWord As String
    Public Shared Property ArchiveAutoExecPassWord() As String
        Get
            Return mArchiveAutoExecPassWord
        End Get
        Set(ByVal value As String)
            mArchiveAutoExecPassWord = value
        End Set
    End Property

    Private Shared mTotalSizeInArchive As Long
    Public Shared Property TotalSizeInArchive() As Long
        Get
            Return mTotalSizeInArchive
        End Get
        Set(ByVal value As Long)
            mTotalSizeInArchive = value
        End Set
    End Property

    Private Shared mCheckCRC32 As Boolean
    Public Shared Property CheckCRC32() As Boolean
        Get
            Return mCheckCRC32
        End Get
        Set(ByVal value As Boolean)
            mCheckCRC32 = value
        End Set
    End Property

    Private Shared mShowUI As Boolean
    Public Shared Property ShowUI() As Boolean
        Get
            Return mShowUI
        End Get
        Set(ByVal value As Boolean)
            mShowUI = value
        End Set
    End Property

    Private Shared mDebugging As Boolean
    Public Shared Property Debugging() As Boolean
        Get
            Return mDebugging
        End Get
        Set(ByVal value As Boolean)
            mDebugging = value
        End Set
    End Property

    Private Shared mInternalZipFileName As String
    Public Shared Property InternalZipFileName() As String
        Get
            Return mInternalZipFileName
        End Get
        Set(ByVal value As String)
            mInternalZipFileName = value
        End Set
    End Property

    Private Shared mIconPath As String
    Public Shared Property IconPath() As String
        Get
            Return mIconPath
        End Get
        Set(ByVal value As String)
            mIconPath = value
        End Set
    End Property

    Private Shared mArchivePassword_PLAINTEXT As String
    Public Shared Property ArchivePassword_PLAINTEXT() As String
        Get
            Return mArchivePassword_PLAINTEXT
        End Get
        Set(ByVal value As String)
            mArchivePassword_PLAINTEXT = value
        End Set
    End Property

    Private Shared mArchivePassword_Encrypted As String
    Public Shared Property ArchivePassword_Encrypted() As String
        Get
            Return mArchivePassword_Encrypted
        End Get
        Set(ByVal value As String)
            mArchivePassword_Encrypted = value
        End Set
    End Property

    Private Shared mInternalZipCompressed As Boolean
    Public Shared Property InternalZipCompressed() As Boolean
        Get
            Return mInternalZipCompressed
        End Get
        Set(ByVal value As Boolean)
            mInternalZipCompressed = value
        End Set
    End Property


    Public Shared Sub Reset()
        mDestinationPath = ""
        mArchiveName = ""
        mArchivePath = ""
        mArchiveAutoExecPath = ""
        mArchiveAutoExecParams = ""
        mArchiveAutoExecTimeout = -1
        mArchiveAutoExecHidden = False
        mArchiveAutoExecUserName = ""
        mArchiveAutoExecDomain = ""
        mArchiveAutoExecPassWord = ""
        mTotalSizeInArchive = -1
        mCheckCRC32 = False
        mInternalZipFileName = ""
        mShowUI = False
        mIconPath = ""
        mDebugging = True
        mArchivePassword_PLAINTEXT = ""
        mArchivePassword_Encrypted = ""
        mInternalZipCompressed = False

        mSaved = False
    End Sub

    Public Shared Function Parse(str As String) As Boolean
        Reset()

        Dim TextLines() As String = str.Split(Environment.NewLine.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)

        For Each textline As String In TextLines
            Dim keyvaluepair() As String = textline.Split("=".ToCharArray(), 2)

            If keyvaluepair.Length = 2 Then
                'ok
                Select Case keyvaluepair(0)
                    Case "Debugging"
                        If Not Boolean.TryParse(keyvaluepair(1), mDebugging) Then
                            mDebugging = False
                        End If
                    Case "DestinationPath"
                        mDestinationPath = keyvaluepair(1)
                    Case "ArchiveName"
                        mArchiveName = keyvaluepair(1)
                    Case "ArchivePath"
                        mArchivePath = keyvaluepair(1)
                    Case "ArchiveAutoExecPath"
                        mArchiveAutoExecPath = keyvaluepair(1)
                    Case "ArchiveAutoExecParams"
                        mArchiveAutoExecParams = keyvaluepair(1)
                    Case "ArchiveAutoExecTimeout"
                        If Not Long.TryParse(keyvaluepair(1), mArchiveAutoExecTimeout) Then
                            mArchiveAutoExecTimeout = -1
                        End If
                    Case "ArchiveAutoExecHidden"
                        If Not Boolean.TryParse(keyvaluepair(1), mArchiveAutoExecHidden) Then
                            mArchiveAutoExecHidden = False
                        End If
                    Case "ArchiveAutoExecUserName"
                        mArchiveAutoExecUserName = keyvaluepair(1)
                    Case "ArchiveAutoExecDomain"
                        mArchiveAutoExecDomain = keyvaluepair(1)
                    Case "ArchiveAutoExecPassWord"
                        mArchiveAutoExecPassWord = keyvaluepair(1)
                    Case "TotalSizeInArchive"
                        If Not Long.TryParse(keyvaluepair(1), mTotalSizeInArchive) Then
                            mTotalSizeInArchive = -1
                        End If
                    Case "CheckCRC32"
                        If Not Boolean.TryParse(keyvaluepair(1), mCheckCRC32) Then
                            mCheckCRC32 = False
                        End If
                    Case "ShowUI"
                        If Not Boolean.TryParse(keyvaluepair(1), mShowUI) Then
                            mShowUI = False
                        End If
                    Case "InternalZipFileName"
                        mInternalZipFileName = keyvaluepair(1)
                    Case "IconPath"
                        mIconPath = keyvaluepair(1)
                    Case "ArchivePassword"
                        mArchivePassword_Encrypted = keyvaluepair(1)
                    Case "InternalZipCompressed"
                        If Not Boolean.TryParse(keyvaluepair(1), mInternalZipCompressed) Then
                            mInternalZipCompressed = False
                        End If
                End Select
            End If
        Next

        Return True
    End Function

    Public Shared Function Dump() As String
        Dim str As String = ""

        str &= "Debugging=" & mDebugging.ToString
        str &= Environment.NewLine

        str &= "DestinationPath=" & mDestinationPath
        str &= Environment.NewLine

        str &= "ArchiveName=" & mArchiveName
        str &= Environment.NewLine

        str &= "ArchivePath=" & mArchivePath
        str &= Environment.NewLine

        str &= "ArchiveAutoExecPath=" & mArchiveAutoExecPath
        str &= Environment.NewLine

        str &= "ArchiveAutoExecParams=" & mArchiveAutoExecParams
        str &= Environment.NewLine

        str &= "ArchiveAutoExecTimeout=" & mArchiveAutoExecTimeout.ToString
        str &= Environment.NewLine

        str &= "ArchiveAutoExecHidden=" & mArchiveAutoExecHidden.ToString
        str &= Environment.NewLine

        str &= "ArchiveAutoExecUserName=" & mArchiveAutoExecUserName.ToString
        str &= Environment.NewLine

        str &= "ArchiveAutoExecDomain=" & mArchiveAutoExecDomain.ToString
        str &= Environment.NewLine

        str &= "ArchiveAutoExecPassWord=" & mArchiveAutoExecPassWord.ToString
        str &= Environment.NewLine

        str &= "TotalSizeInArchive=" & mTotalSizeInArchive.ToString
        str &= Environment.NewLine

        str &= "CheckCRC32=" & mCheckCRC32.ToString
        str &= Environment.NewLine

        str &= "ShowUI=" & mShowUI.ToString
        str &= Environment.NewLine

        str &= "InternalZipFileName=" & mInternalZipFileName.ToString
        str &= Environment.NewLine

        str &= "IconPath=" & mIconPath.ToString
        str &= Environment.NewLine

        str &= "ArchivePassword=" & mArchivePassword_Encrypted.ToString
        str &= Environment.NewLine

        str &= "InternalZipCompressed=" & mInternalZipCompressed.ToString
        str &= Environment.NewLine


        Return str
    End Function
End Class