﻿Public Class PackedFiles
    Private mPaths As New List(Of String)
    Private mFiles As New List(Of List(Of IconFileInfo))

    Private mCurrentIndex As Integer = -1

    Private mFileCounter As Integer = 0


    Private mIterator_Path As Integer = -1
    Private mIterator_File As Integer = -1


    Public Event ContentsChanged(PathCount As Integer, FileCount As Integer)



    Public Sub New()
        Reset()
    End Sub

    Public Sub Reset()
        mPaths = New List(Of String)
        mFiles = New List(Of List(Of IconFileInfo))

        mIterator_Path = -1
        mIterator_File = -1
        mCurrentIndex = -1

        mFileCounter = 0

        mTotalSize = 0

        mIsSorted = False

        mIsUpdated = False

        RaiseEvent ContentsChanged(Count_Paths, mFileCounter)
    End Sub

    Private mTotalSize As Long
    Public ReadOnly Property TotalSize() As Long
        Get
            Return mTotalSize
        End Get
    End Property

    Private mIsSorted As Boolean = False
    Public ReadOnly Property IsSorted() As Boolean
        Get
            Return mIsSorted
        End Get
    End Property

    Public ReadOnly Property Count_Paths As Integer
        Get
            Return mPaths.Count
        End Get
    End Property

    Public ReadOnly Property Count_Files_CurrentPath As Integer
        Get
            If mCurrentIndex > -1 And mCurrentIndex < mPaths.Count Then
                Return mFiles.Item(mCurrentIndex).Count
            Else
                Return 0
            End If
        End Get
    End Property

    Public ReadOnly Property Count_Files_All As Integer
        Get
            If mCurrentIndex > -1 And mCurrentIndex < mPaths.Count Then
                Dim tot As Integer = 0

                For i As Integer = 0 To mPaths.Count - 1
                    tot += mFiles.Item(i).Count
                Next

                Return tot
            Else
                Return 0
            End If
        End Get
    End Property

    Private mIsUpdated As Boolean
    Public ReadOnly Property IsUpdated() As Boolean
        Get
            Return mIsUpdated
        End Get
    End Property


    Public Function AddPath(path As String)
        Return AddPath(path, True, False)
    End Function

    Public Function AddPath(path As String, add_to_current_dir As Boolean)
        Return AddPath(path, True, add_to_current_dir)
    End Function

    Public Function AddPath(path As String, overwrite As Boolean, add_to_current_dir As Boolean) As Boolean
        Dim _index As Integer = -1

        If add_to_current_dir And mCurrentIndex >= 0 And mCurrentIndex < mPaths.Count Then
            path = mPaths.Item(mCurrentIndex) & Shared_Constants.INTERNAL_PATH_SEPARATOR & path
        End If

        'remove rogue double slashes!
        path = path.Replace(Shared_Constants.INTERNAL_PATH_SEPARATOR & Shared_Constants.INTERNAL_PATH_SEPARATOR, Shared_Constants.INTERNAL_PATH_SEPARATOR)


        If path.Equals(Shared_Constants.INTERNAL_PATH_SEPARATOR) And mPaths.Count = 0 Then
            'first path set, doesn't count yet
        Else
            mIsUpdated = True
        End If


        _index = GetPathIndex(path)
        If _index > -1 Then
            'already exists!
            If Not overwrite Then
                Return False
            Else
                'mCurrentIndex = _index
            End If
        Else
            mPaths.Add(path)
            mFiles.Add(New List(Of IconFileInfo))

            RaiseEvent ContentsChanged(Count_Paths, mFileCounter)

            'mCurrentIndex = mPaths.Count - 1
            mIsSorted = False
        End If

        Return True
    End Function


    Public Function UndoNewPaths(_index As Integer) As Boolean
        If _index < 0 Then
            Return False
        End If

        If _index >= mPaths.Count Then
            Return False
        End If

        If (_index + 1) <= (mPaths.Count - 1) Then
            For _i As Integer = (mPaths.Count - 1) To (_index + 1) Step -1
                mPaths.RemoveAt(_i)
                mFiles.RemoveAt(_i)
            Next
        End If

        Return true
    End Function

    Public Function DeletePath(path As String) As Boolean
        Dim _index As Integer = -1
        Dim path_with_ending_slash As String = ""

        If path.EndsWith(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
            path = path.Substring(0, path.Length - 1)
        End If

        path_with_ending_slash = path & Shared_Constants.INTERNAL_PATH_SEPARATOR


        _index = GetPathIndex(path)

        If _index > -1 Then
            For _sub_index As Integer = mPaths.Count - 1 To 0 Step -1
                If mPaths.Item(_sub_index).StartsWith(path_with_ending_slash) Then
                    mPaths.RemoveAt(_sub_index)
                    mFiles.RemoveAt(_sub_index)
                End If
            Next

            mPaths.RemoveAt(_index)
            mFiles.RemoveAt(_index)
        Else
            Return False
        End If


        'do a recount
        mFileCounter = Count_Files_All

        RaiseEvent ContentsChanged(Count_Paths, mFileCounter)

        Return True
    End Function

    Public Function RenamePath(path As String, newname As String) As String
        Dim _index As Integer = -1
        Dim path_with_ending_slash As String = ""
        Dim new_path As String = ""
        Dim new_path_with_ending_slash As String = ""


        If newname.Length <= 0 Then
            Return ""
        End If

        If path.EndsWith(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
            path = path.Substring(0, path.Length - 1)
        End If

        path_with_ending_slash = path & Shared_Constants.INTERNAL_PATH_SEPARATOR


        Dim parts As String() = path.Split(Shared_Constants.INTERNAL_PATH_SEPARATOR)

        If parts.Length <= 0 Then
            Return ""
        End If

        parts(parts.Length - 1) = ""

        new_path = String.Join(Shared_Constants.INTERNAL_PATH_SEPARATOR, parts)
        If Not new_path.EndsWith(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
            new_path = new_path & Shared_Constants.INTERNAL_PATH_SEPARATOR
        End If
        new_path = new_path & newname

        If new_path.EndsWith(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
            new_path = new_path.Substring(0, path.Length - 1)
        End If

        new_path_with_ending_slash = new_path & Shared_Constants.INTERNAL_PATH_SEPARATOR


        _index = GetPathIndex(new_path)

        If _index > -1 Then
            'already exists, no change!!!

            Return ""
        End If


        'Make the change
        _index = GetPathIndex(path)


        If _index > -1 Then
            'Console.WriteLine(_index.ToString & ": '" & mPaths.Item(_index) & "'==>'" & new_path & "'")
            mPaths.Item(_index) = new_path

            Dim _rename_start As Integer = 0

            If mIsSorted Then
                'little shortcut if sorted
                _rename_start = _index + 1
            End If

            For _sub_index As Integer = _rename_start To mPaths.Count - 1
                If mPaths.Item(_sub_index).StartsWith(path_with_ending_slash) Then
                    Dim new_sub_path As String = mPaths.Item(_sub_index).Replace(path_with_ending_slash, new_path_with_ending_slash)

                    'Console.WriteLine(_sub_index.ToString & ": '" & mPaths.Item(_sub_index) & "'==>'" & new_sub_path & "'")
                    mPaths.Item(_sub_index) = new_sub_path
                End If
            Next

            mIsSorted = False

            Return new_path
        Else
            Return ""
        End If
    End Function


#Region "AddFile"
    Public Function AddFile(filename As String) As Boolean
        Dim ifi As New IconFileInfo(filename)

        Return AddFile(mCurrentIndex, ifi)
    End Function

    Public Function AddFile(path As String, filename As String) As Boolean
        Dim ifi As New IconFileInfo(filename)

        Return AddFile(path, ifi)
    End Function

    Public Function AddFile(path As String, ifi As IconFileInfo) As Boolean
        Dim path_index As Integer = GetPathIndex(path)

        Return AddFile(path_index, ifi)
    End Function

    Public Function AddFile(path_index As Integer, ifi As IconFileInfo) As Boolean
        If Not DoesFileExist(path_index, ifi.FullName) And path_index > -1 And path_index < mFiles.Count Then
            mFiles.Item(path_index).Add(ifi)

            mTotalSize += ifi.Size
            mFileCounter += 1


            RaiseEvent ContentsChanged(Count_Paths, mFileCounter)

            Try
                _SortFiles(path_index)
            Catch ex As Exception

            End Try


            mIsUpdated = True

            Return True
        Else
            Return False
        End If
    End Function

    Public Function DeleteFile(path As String, file_index As Integer) As Boolean
        Dim path_index As Integer = GetPathIndex(path)

        Return DeleteFile(path_index, file_index)
    End Function

    Public Function DeleteFile(path_index As Integer, file_index As Integer) As Boolean
        If path_index > -1 And path_index < mFiles.Count Then
            If file_index > -1 And file_index < mFiles.Item(path_index).Count Then
                mFiles.Item(path_index).RemoveAt(file_index)

                mFileCounter -= 1

                RaiseEvent ContentsChanged(Count_Paths, mFileCounter)

                Return True
            End If
        End If

        Return False
    End Function


    Public Function RenameFile(path As String, file_index As Integer, new_name As String) As Boolean
        If path.Equals("") Then path = Shared_Constants.INTERNAL_PATH_SEPARATOR

        Dim path_index As Integer = GetPathIndex(path)

        Return RenameFile(path_index, file_index, new_name)
    End Function

    Public Function RenameFile(path_index As Integer, file_index As Integer, new_name As String) As Boolean
        If path_index > -1 And path_index < mFiles.Count Then
            If file_index > -1 And file_index < mFiles.Item(path_index).Count Then
                Dim new_name_full As String = IO.Path.Combine(mPaths.Item(path_index), new_name)

                If DoesFileExist(path_index, new_name_full) Then
                    'new name already exists
                    Return False
                End If

                'mFiles.Item(path_index).Item(file_index) = New IconFileInfo(new_name_full)
                mFiles.Item(path_index).Item(file_index).CustomName = new_name

                Return True
            End If
        End If

        Return False
    End Function


    Private Function _SortFiles(path_index As Integer) As Boolean
        If path_index > -1 And path_index < mFiles.Count Then
            If mFiles.Item(path_index).Count <= 0 Then
                Return False
            End If

            mFiles.Item(path_index) = mFiles.Item(path_index).Distinct().ToList()

            Dim swaps As Boolean = True

            While swaps = True
                swaps = False

                For i As Integer = 0 To mFiles.Item(path_index).Count - 2
                    Dim ifi0 As New IconFileInfo, ifi1 As New IconFileInfo

                    ifi0 = mFiles.Item(path_index).Item(i)
                    ifi1 = mFiles.Item(path_index).Item(i + 1)

                    If ifi0.Name > ifi1.Name Then
                        Dim temp_ifi As New IconFileInfo

                        temp_ifi = mFiles.Item(path_index).Item(i)
                        mFiles.Item(path_index).Item(i) = mFiles.Item(path_index).Item(i + 1)
                        mFiles.Item(path_index).Item(i + 1) = temp_ifi

                        swaps = True
                    End If
                Next
            End While

            Return True
        Else
            Return False
        End If
    End Function
#End Region

    Public Function DoesFileExist(filename As String) As Boolean
        Return DoesFileExist(mCurrentIndex, filename)
    End Function

    Public Function DoesFileExist(path_index As Integer, filename As String) As Boolean
        If path_index < 0 Or path_index >= mFiles.Count Then
            Return False
        End If

        Dim bRet As Boolean = False

        For Each fi As IconFileInfo In mFiles.Item(path_index)
            If fi.FullName.ToLower.Equals(filename.ToLower) Then
                bRet = True

                Exit For
            End If
        Next

        Return bRet
    End Function

    Public Function DoesFileExistInAnyPath(filename As String) As Boolean
        Dim bRet As Boolean = False

        Do 'dummy loop, so we can break out of nested for
            For pathindex As Integer = 0 To mPaths.Count - 1
                For Each fi As IconFileInfo In mFiles.Item(pathindex)
                    If fi.FullName.ToLower.Equals(filename.ToLower) Then
                        bRet = True

                        Exit Do
                    End If
                Next
            Next
        Loop While False

        Return bRet
    End Function


    Public Sub Sort()
        If mIsSorted Then
            Exit Sub
        End If

        Dim swaps As Boolean = True

        While swaps = True
            swaps = False

            For i As Integer = 0 To mPaths.Count - 2
                If mPaths.Item(i) > mPaths.Item(i + 1) Then
                    Dim temp_mPaths As String = mPaths.Item(i + 1)
                    mPaths.Item(i + 1) = mPaths.Item(i)
                    mPaths.Item(i) = temp_mPaths

                    Dim temp_mFiles1 As New List(Of IconFileInfo), temp_mFiles2 As New List(Of IconFileInfo)
                    For Each item As IconFileInfo In mFiles.Item(i + 1)
                        temp_mFiles1.Add(item)
                    Next
                    For Each item As IconFileInfo In mFiles.Item(i)
                        temp_mFiles2.Add(item)
                    Next
                    mFiles.Item(i + 1).Clear()
                    mFiles.Item(i + 1).AddRange(temp_mFiles2)
                    mFiles.Item(i).Clear()
                    mFiles.Item(i).AddRange(temp_mFiles1)

                    swaps = True
                End If
            Next
        End While

        mIsSorted = True
    End Sub

    Public Sub SetPathIndex(index As Integer)
        mCurrentIndex = index
    End Sub

    Public Function GetCurrentPath() As String
        If mCurrentIndex > 0 And mCurrentIndex < mPaths.Count Then
            Return mPaths.Item(mCurrentIndex)
        Else
            Return ""
        End If
    End Function

    Public Function GetPathIndex(path As String) As Integer
        Return GetPathIndex(path, False)
    End Function

    Public Function GetPathIndex(path As String, set_as_current As Boolean) As Integer
        Dim _index As Integer = -1

        'replace double c_ARCHIVE_INTERNAL_PATH_SEPARATOR
        'path = path.ToLower
        path = path.Replace(Shared_Constants.INTERNAL_PATH_SEPARATOR & Shared_Constants.INTERNAL_PATH_SEPARATOR, Shared_Constants.INTERNAL_PATH_SEPARATOR)
        If path.EndsWith(Shared_Constants.INTERNAL_PATH_SEPARATOR) And Not path.Equals(Shared_Constants.INTERNAL_PATH_SEPARATOR) Then
            path = path.Substring(0, path.Length - 1)
        End If


        _index = mPaths.IndexOf(path)
        If _index > -1 Then
            If set_as_current Then
                SetPathIndex(_index)
            End If
        End If

        Return _index
    End Function

    Public Sub ResetIterator_Path()
        mIterator_Path = -1
    End Sub

    Public Function GetNext_Path(ByRef path As String) As Boolean
        mIterator_Path += 1

        If mIterator_Path < mPaths.Count Then
            mIterator_File = -1
            path = mPaths.Item(mIterator_Path)

            Return True
        Else
            Return False
        End If
    End Function


    Public Sub ResetIterator_File()
        mIterator_File = -1
    End Sub

    Public Function GetNext_File(ByRef file As IconFileInfo) As Boolean
        Return GetNext_File(mCurrentIndex, file)
    End Function

    Public Function GetNext_File(ByVal pathindex As Integer, ByRef file As IconFileInfo) As Boolean
        mIterator_File += 1

        If pathindex < 0 Or pathindex >= mFiles.Count Then
            Return False
        End If

        If mIterator_File < mFiles.Item(pathindex).Count Then
            file = mFiles.Item(pathindex).Item(mIterator_File)

            Return True
        Else
            Return False
        End If
    End Function


    Public Function GetAllExecutables() As String()
        Return GetAllFilesWithExtension(".exe")
    End Function

    Public Function GetAllFilesWithExtension(extension As String) As String()
        Dim _lst As New List(Of String)

        If Not extension.StartsWith(".") Then
            extension = "." & extension
        End If

        For pathindex As Integer = 0 To mPaths.Count - 1
            For Each fi As IconFileInfo In mFiles.Item(pathindex)
                If IO.Path.GetExtension(fi.FullName).Equals(extension) Then
                    _lst.Add(IO.Path.Combine(mPaths.Item(pathindex), fi.Name).Replace(Shared_Constants.INTERNAL_PATH_SEPARATOR, Shared_Constants.NORMAL_PATH_SEPARATOR))
                End If
            Next
        Next

        Return _lst.ToArray
    End Function
End Class
