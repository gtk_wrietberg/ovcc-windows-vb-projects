﻿Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Text

Public Class FunctionsAndStuff
    Public Class Constants
        Public Shared ReadOnly FOLDER_ICONS As String = "_ICONS"
    End Class

    Public Shared Function FilenameIsOK(ByVal fileName As String) As Boolean
        Try
            Dim file As String = IO.Path.GetFileName(fileName)
            Dim directory As String = IO.Path.GetDirectoryName(fileName)

            Return Not (file.Intersect(IO.Path.GetInvalidFileNameChars()).Any() OrElse directory.Intersect(IO.Path.GetInvalidPathChars()).Any())
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Class FilesAndFolders
        Public Shared Function GetFileTypeFromRegistry(FileExtension As String) As String
            Try
                If FileExtension.Contains(" ") Then
                    FileExtension = ""
                    Throw New Exception("space in extension")
                End If

                'Dim reg1 As String = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & FileExtension, "", FileExtension).ToString
                Dim reg1 As Object = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & FileExtension, "", FileExtension)

                If reg1 Is Nothing Then
                    Throw New Exception("Unknown file type")
                End If

                If reg1.ToString.Equals(FileExtension) Then
                    Throw New Exception("Unknown file type")
                End If


                'Dim reg2 As String = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & reg1, "", FileExtension).ToString
                Dim reg2 As Object = My.Computer.Registry.GetValue("HKEY_CLASSES_ROOT\" & reg1, "", FileExtension)

                If reg2 Is Nothing Then
                    Throw New Exception("Unknown file type")
                End If

                If reg2.ToString.Equals(FileExtension) Then
                    Throw New Exception("Unknown file type")
                End If

                Return reg2
            Catch ex As Exception
                If FileExtension.StartsWith(".") Then
                    FileExtension = FileExtension.Substring(1, FileExtension.Length - 1)
                End If

                If FileExtension.Equals("") Then
                    Return "File"
                Else
                    Return FileExtension & " File"
                End If
            End Try
        End Function


        Public Enum _FileSizeHumanReadableTypes
            Use1024 = 0
            Use1024i = 1
            Use1000 = 2
        End Enum

        Public Shared Function GetFileSizeHumanReadable(FileSize As Long) As String
            Return GetFileSizeHumanReadable(FileSize, _FileSizeHumanReadableTypes.Use1024)
        End Function

        Public Shared Function GetFileSizeHumanReadable(FileSize As Long, Base1024 As _FileSizeHumanReadableTypes) As String
            If FileSize < 0 Then
                Return ""
            End If

            Select Case Base1024
                Case _FileSizeHumanReadableTypes.Use1000
                    Select Case FileSize
                        Case > (1000 * 1000 * 1000)
                            Return CInt(FileSize / (1000 * 1000 * 1000)).ToString & "GB"
                        Case > (1000 * 1000)
                            Return CInt(FileSize / (1000 * 1000)).ToString & "MB"
                        Case > 1000
                            Return CInt(FileSize / 1000).ToString & "kB"
                        Case Else
                            Return FileSize.ToString & "B"
                    End Select
                Case _FileSizeHumanReadableTypes.Use1024
                    Select Case FileSize
                        Case > (1024 * 1024 * 1024)
                            Return CInt(FileSize / (1024 * 1024 * 1024)).ToString & "iB"
                        Case > (1024 * 1024)
                            Return CInt(FileSize / (1024 * 1024)).ToString & "MB"
                        Case > 1024
                            Return CInt(FileSize / 1024).ToString & "KB"
                        Case Else
                            Return FileSize.ToString & "B"
                    End Select
                Case _FileSizeHumanReadableTypes.Use1024i
                    Select Case FileSize
                        Case > (1024 * 1024 * 1024)
                            Return CInt(FileSize / (1024 * 1024 * 1024)).ToString & "GiB"
                        Case > (1024 * 1024)
                            Return CInt(FileSize / (1024 * 1024)).ToString & "MiB"
                        Case > 1024
                            Return CInt(FileSize / 1024).ToString & "KiB"
                        Case Else
                            Return FileSize.ToString & "B"
                    End Select
                Case Else
                    Return "?B"
            End Select
        End Function


        Public Shared Function FileCountRecursive(_dir As String) As Integer
            Dim rv As Integer = 0
            Dim directories_stack As New Stack(Of String)


            rv += IO.Directory.GetFiles(_dir, "*.*").Length


            For Each _subdir As String In IO.Directory.GetDirectories(_dir)
                directories_stack.Push(_subdir)
            Next

            If directories_stack.Count > 0 Then
                ' Continue processing for each stacked directory
                Do While (directories_stack.Count > 0)
                    ' Get top directory string
                    Dim _subdir As String = directories_stack.Pop



                    Try
                        rv += IO.Directory.GetFiles(_subdir, "*.*").Length

                        ' Loop through all subdirectories and add them to the stack.
                        For Each _sub_subdir As String In IO.Directory.GetDirectories(_subdir)
                            directories_stack.Push(_sub_subdir)
                        Next
                    Catch ex As Exception

                    End Try
                Loop
            End If

            Return rv
        End Function
    End Class

    Public Class Assembly
        Public Shared Function Version() As String
            Dim _ass As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly
            Dim _ver As Version = _ass.GetName().Version

            Dim builder As New StringBuilder

            builder.Append("v")
            builder.Append(_ver.Major)
            builder.Append(".")
            builder.Append(_ver.Minor)
            builder.Append(".")
            builder.Append(_ver.Build)
            builder.Append(".")
            builder.Append(_ver.Revision)

            Return builder.ToString
        End Function

        Public Shared Function Guid() As String
            Dim _ass As Reflection.Assembly = Reflection.Assembly.GetExecutingAssembly
            Dim _attrs = _ass.GetCustomAttributes(False).OfType(Of GuidAttribute)()

            If _attrs.Any() Then
                Return _attrs.First().Value
            Else
                Return ""
            End If
        End Function
    End Class
End Class
