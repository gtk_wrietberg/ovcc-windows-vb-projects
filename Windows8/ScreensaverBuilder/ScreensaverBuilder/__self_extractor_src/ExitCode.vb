﻿Imports System
Imports System.Collections.Generic

Public Class ExitCode
    Public Enum ExitCodes As Integer
        OK = 0
        PASSWORD_INCORRECT = 1
        SETTINGS_NOT_LOADED = 2
        ZIPID_NOT_MATCHING = 4
        CRC_ERROR = 8
        EXTRACTING_ONE_OR_MORE_FILES_FAILED = 16
        EXTRACTING_FAILED = 32
        POST_EXTRACT_EXE_FAILED = 64
        GENERIC_ERROR = 128
        NOT_SET = 256
    End Enum

    Private Shared mExitCode As Integer = ExitCodes.NOT_SET

    Public Shared Function GetValue() As Integer
        Return mExitCode
    End Function

    Public Shared Sub SetValue(value As ExitCodes)
        _SetValue(value, False)
    End Sub

    Public Shared Sub SetValueExclusive(value As ExitCodes)
        _SetValue(value, True)
    End Sub

    Private Shared Sub _SetValue(value As ExitCodes, Optional Exclusive As Boolean = False)
        If Exclusive Then
            mExitCode = value
        Else
            If Contains(ExitCodes.NOT_SET) Then
                mExitCode = ExitCodes.OK
            End If

            mExitCode = mExitCode Or value
        End If

        SetEnvironmentExitCode()
    End Sub

    Public Shared Sub UnsetValue(value As ExitCodes)
        If (mExitCode And value) Then
            mExitCode = mExitCode Xor value
        End If
    End Sub

    Public Shared Function Contains(value As ExitCodes) As Boolean
        Return (mExitCode And value)
    End Function

    Public Shared Function IsValue(value As ExitCodes) As Boolean
        Return (mExitCode = value)
    End Function

    Public Shared Sub SetEnvironmentExitCode()
        Environment.ExitCode = mExitCode
    End Sub

    Public Overloads Shared Function ToString() As String
        Dim sRet As String = ""

        sRet = mExitCode.ToString

        If Contains(ExitCodes.NOT_SET) Then
            sRet = sRet & " (" & ExitCodes.NOT_SET & "=" & System.Enum.GetName(GetType(ExitCodes), ExitCodes.NOT_SET) & ")"
        Else
            Dim lTmp As New List(Of String)
            Dim aCodes As Array
            aCodes = System.Enum.GetValues(GetType(ExitCodes))

            For Each iCode As Integer In aCodes
                If mExitCode And iCode Then
                    lTmp.Add(iCode.ToString & "=" & System.Enum.GetName(GetType(ExitCodes), iCode))
                End If
            Next

            sRet = sRet & " (" & String.Join(" + ", lTmp) & ")"
        End If

        Return sRet
    End Function
End Class
