﻿Imports System
Imports System.IO
Imports System.IO.Compression
Imports System.Reflection
Imports System.Linq
Imports System.Windows.Forms
Imports System.Diagnostics
Imports System.Security

Public Class Extractor
    Public Class Extract
        Public Shared Event event_Progress(status As String)
        Public Shared Event event_ExtractingProgress(percentage As Integer, filename As String, status As String)
        Public Shared Event event_CRCProgress(percentage As Integer, filename As String, status As String)
        Public Shared Event event_Done()


        Public Shared Function DoIt() As Boolean
            Dim ass As Assembly = Assembly.GetExecutingAssembly()
            Dim res As String() = ass.GetManifestResourceNames()
            Dim sRunFileAfterExtraction As String = ""

            Logger.WriteMessage("extracting files")


            Try
                Dim location As String = System.Environment.GetCommandLineArgs()(0)
                Dim appName As String = System.IO.Path.GetFileName(location)
                Dim size_sofar As Long = 0
                Dim unzipped As Boolean = False


                If Not res.Contains(Settings.InternalZipFileName) Then
                    ExitCode.SetValue(ExitCode.ExitCodes.ZIPID_NOT_MATCHING)

                    Throw New Exception("Could not find internal zip file resource ('" & Settings.InternalZipFileName & "') in assembly!")
                End If

                For Each name As String In res
                    If Not name.Equals(Settings.InternalZipFileName) Then
                        Continue For
                    End If

                    If Settings.Debugging Then Logger.WriteMessage("internal zip found")

                    Dim rs As Stream = ass.GetManifestResourceStream(name)
                    Dim file_status As String = "unknown error"

                    Try
                        Logger.WriteMessage("unpacking")

                        Using _zip As ZipArchive = New ZipArchive(rs)
                            For Each entry As ZipArchiveEntry In _zip.Entries
                                Try
                                    Dim entryFullName As String = entry.FullName

                                    If entryFullName.StartsWith(Shared_Constants.NORMAL_PATH_SEPARATOR) Then
                                        If entryFullName.Length > 1 Then
                                            entryFullName = entryFullName.Substring(1, entryFullName.Length - 1)
                                        End If
                                    End If

                                    Dim _file As String = IO.Path.Combine(Settings.DestinationPath, entryFullName)
                                    Dim _path As String = IO.Path.GetDirectoryName(_file)


                                    If Not IO.Directory.Exists(_path) Then
                                        IO.Directory.CreateDirectory(_path)
                                        If Settings.Debugging Then Logger.WriteMessage("path created")
                                    End If

                                    If Settings.Debugging Then Logger.WriteMessage("extracting")
                                    entry.ExtractToFile(_file, True)

                                    size_sofar += entry.Length
                                    If Settings.Debugging Then Logger.WriteMessage(entry.Length.ToString & " bytes")

                                    file_status = "ok"
                                Catch ex As Exception
                                    Logger.WriteError("failed: " & ex.Message)

                                    ExitCode.SetValue(ExitCode.ExitCodes.EXTRACTING_ONE_OR_MORE_FILES_FAILED)
                                    file_status = ex.Message
                                End Try

                                '#If REPORT_EXTRACTING_PROGRESS Then
                                If Settings.TotalSizeInArchive > 0 Then
                                    RaiseEvent event_ExtractingProgress((size_sofar / Settings.TotalSizeInArchive) * 100, entry.FullName, file_status)
                                    'frmUnpack.bgWorker_Unpack.ReportProgress((size_sofar / Settings.TotalSizeInArchive) * 100, entry.FullName & " - " & file_status)
                                Else
                                    RaiseEvent event_ExtractingProgress(0, entry.FullName, file_status)
                                    'frmUnpack.bgWorker_Unpack.ReportProgress(0, entry.FullName & " - " & file_status)
                                End If
                                '#End If
                            Next
                        End Using

                        Logger.WriteMessage("done unpacking")

                        unzipped = True
                    Catch ex As Exception
                        Logger.WriteFatalError("extracting files failed!!!")
                        Logger.WriteFatalError(ex.Message)

                        ExitCode.SetValue(ExitCode.ExitCodes.EXTRACTING_FAILED)

                        Return False
                    End Try
                Next


                Logger.WriteMessage("CRC check")
                If unzipped Then
                    If Settings.CheckCRC32 Then
                        FileList.ResetFileCounter()

                        Dim currentFileCount As Integer = 0

                        Dim _file As String = "", _path As String = "", _destination_path As String = "", _crc As String = ""

                        While FileList.GetNext(_file, _path, _destination_path, _crc)
                            '(size_sofar / Settings.TotalSizeInArchive) * 100
                            currentFileCount += 1

                            If _destination_path.StartsWith(Shared_Constants.NORMAL_PATH_SEPARATOR) Then
                                If _destination_path.Length > 1 Then
                                    _destination_path = _destination_path.Substring(1, _destination_path.Length - 1)
                                End If
                            End If

                            Dim thisFile As String = IO.Path.Combine(Settings.DestinationPath, _destination_path)
                            'Dim thisCRC As String = CRC32.fromFile(thisFile)
                            Dim thisCRC As String = Crypto.SHA._256.fromFile(thisFile)
                            Dim sResult As String = "ok"

                            If Settings.Debugging Then Logger.WriteMessage(thisFile)
                            If thisCRC.Equals(_crc) Then
                                'ok
                                If Settings.Debugging Then Logger.WriteMessage("CRC match")
                            Else
                                If Settings.Debugging Then Logger.WriteMessage(thisCRC & " should be " & _crc)

                                ExitCode.SetValue(ExitCode.ExitCodes.CRC_ERROR)

                                sResult = "CRC mismatch"
                            End If

                            RaiseEvent event_CRCProgress((currentFileCount / FileList.Count) * 100, thisFile, sResult)
                        End While
                    Else
                        Logger.WriteMessage("skipped")
                    End If
                Else
                    Logger.WriteMessage("skipped, unpacking went wrong!")
                End If



                Return True
            Catch ex As Exception
                Logger.WriteFatalError("extracting files failed!!!")
                Logger.WriteFatalError(ex.Message)

                ExitCode.SetValue(ExitCode.ExitCodes.GENERIC_ERROR)

                Return False
            End Try

            RaiseEvent event_Done()
        End Function
    End Class

    Public Class Run
        Public Shared Event event_Starting(filename As String)
        Public Shared Event event_Started(filename As String, pId As Integer, timeOut_ms As Integer)
        Public Shared Event event_Failed(filename As String, status As String)
        Public Shared Event event_Finished(filename As String, status As String)

        Public Shared Function PostExtractingExe() As Boolean
            If Settings.IsArchiveAutoExecSet Then
                Dim sExe As String = Settings.ArchiveAutoExecPath

                If sExe.StartsWith(Shared_Constants.NORMAL_PATH_SEPARATOR) Then
                    If sExe.Length > 1 Then
                        sExe = sExe.Substring(1, sExe.Length - 1)
                    End If
                End If

                sExe = IO.Path.Combine(Settings.DestinationPath, sExe)

                RaiseEvent event_Starting(sExe)

                If _IsProcessRunning(sExe) Then
                    RaiseEvent event_Failed(sExe, "process was already running!")

                    Return False
                End If

                'MessageBox.Show("sRunFileAfterExtraction: """ & sExe & """ " & Settings.ArchiveAutoExecParams)
                _StartProcessWithExitcode(sExe,
                                          Settings.ArchiveAutoExecParams,
                                          Settings.ArchiveAutoExecUserName,
                                          Settings.ArchiveAutoExecPassWord,
                                          Settings.ArchiveAutoExecTimeout,
                                          Settings.ArchiveAutoExecHidden)

                'MessageBox.Show(sExe & "," &
                '          Settings.ArchiveAutoExecParams & "," &
                '          Settings.ArchiveAutoExecUserName & "," &
                '          Settings.ArchiveAutoExecPassWord & "," &
                '          Settings.ArchiveAutoExecTimeout & "," &
                '          Settings.ArchiveAutoExecHidden)

            Else
                RaiseEvent event_Finished("", "no exe set")
            End If


            Return True
        End Function

        Private Shared Function _IsProcessRunning(filename As String) As Boolean
            For Each p As System.Diagnostics.Process In System.Diagnostics.Process.GetProcesses()
                Try
                    If p.MainModule.FileName.ToLower.Equals(filename.ToLower) Then
                        Return True
                    End If
                Catch ex As Exception

                End Try
            Next

            Return False
        End Function

        Private Shared Function _ConvertPasswordStringToSecureString(ByVal str As String) As System.Security.SecureString
            Dim password As New System.Security.SecureString

            For Each c As Char In str.ToCharArray
                password.AppendChar(c)
            Next

            Return password
        End Function

        Private Shared Function _StartProcessWithExitcode(cmdLine As String, cmdArguments As String, sUserName As String, sPassword As String, timeOut_ms As Integer, hidden As Boolean) As Integer
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            pInfo.UseShellExecute = False
            'pInfo.RedirectStandardOutput = True
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = _ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing

                pInfo.LoadUserProfile = True
                pInfo.Verb = "runas"
                If hidden Then
                    pInfo.UseShellExecute = False
                    pInfo.WindowStyle = ProcessWindowStyle.Hidden
                End If
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                    RaiseEvent event_Started(pInfo.FileName, p.Id, timeOut_ms)
                Else
                    'wtf, not started
                    RaiseEvent event_Failed(pInfo.FileName, "process not started, pid<0 !")

                    Return -1
                End If

                Try
                    'This is here for processes without graphical UI
                    p.WaitForInputIdle()
                Catch ex As Exception

                End Try

                'Dim voidOutput As String = p.StandardOutput.ReadToEnd()

                If timeOut_ms > 0 Then
                    p.WaitForExit(timeOut_ms)
                Else
                    p.WaitForExit()
                End If


                If p.HasExited = False Then
                    If p.Responding Then
                        p.CloseMainWindow()
                    Else
                        p.Kill()
                    End If

                    RaiseEvent event_Failed(pInfo.FileName, "process timed out!")

                    Return -1
                Else
                    Dim ret As Integer = -1

                    Try
                        ret = p.ExitCode
                    Catch ex As Exception
                        ret = -1
                    End Try

                    RaiseEvent event_Finished(pInfo.FileName, "Exited with exit code " & ret.ToString)

                    Return ret
                End If
            Catch ex As Exception
                RaiseEvent event_Failed(pInfo.FileName, "_StartProcess exception: " & ex.Message)

                Return -1
            End Try

            Return -1
        End Function
    End Class
End Class
