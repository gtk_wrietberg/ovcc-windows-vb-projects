﻿<System.ComponentModel.RunInstaller(True)> Partial Class OO_ER__ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.OO_ER__ServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.OO_ER__ServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'OO_ER__ServiceProcessInstaller
        '
        Me.OO_ER__ServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.OO_ER__ServiceProcessInstaller.Password = Nothing
        Me.OO_ER__ServiceProcessInstaller.Username = Nothing
        '
        'OO_ER__ServiceInstaller
        '
        Me.OO_ER__ServiceInstaller.ServiceName = "OO_ER__Service"
        '
        'OO_ER__ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.OO_ER__ServiceProcessInstaller, Me.OO_ER__ServiceInstaller})

    End Sub

    Friend WithEvents OO_ER__ServiceProcessInstaller As ServiceProcess.ServiceProcessInstaller
    Friend WithEvents OO_ER__ServiceInstaller As ServiceProcess.ServiceInstaller
End Class
