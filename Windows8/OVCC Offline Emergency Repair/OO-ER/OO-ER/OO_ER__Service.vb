﻿Public Class OO_ER__Service
    Private mThreading_Main As Threading.Thread
    Private mLoopAbort As Boolean = False


    Protected Overrides Sub OnStart(ByVal args() As String)
        'init logger
        Helpers.Logger.InitialiseLogger()


        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("service started", , 0)


    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()

        Helpers.Logger.Write("service stopped")
    End Sub

    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Thread_Main()
        Dim delayCount As Long = 0
        Dim previousState As EmergencyDetector.EMERGENCY_STATE = EmergencyDetector.EMERGENCY_STATE.NOTHING_HAPPENED
        Dim currentState As EmergencyDetector.EMERGENCY_STATE = EmergencyDetector.EMERGENCY_STATE.NOTHING_HAPPENED
        Dim returnString As String = ""


        Do While Not mLoopAbort
            If delayCount >= Constants.TimeoutsAndDelaysAndCounters.DELAY__ServiceLoopRepeats Then
                delayCount = 0

                'Do the check
                If Helpers.Processes.IsProcessRunning(Constants.FilesAndFolders.FILE__SiteKiosk) Then
                    'sitekiosk is running
                    currentState = EmergencyDetector.GetSiteKioskInEmergencyState(returnString)

                    If currentState = previousState And currentState = EmergencyDetector.EMERGENCY_STATE.YES Then

                    End If
                Else
                    'sitekiosk not running, don't bother
                End If
            End If

            Threading.Thread.Sleep(Constants.TimeoutsAndDelaysAndCounters.DELAY__ServiceLoop * 1000)
        Loop
    End Sub
End Class
