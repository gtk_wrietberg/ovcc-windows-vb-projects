﻿Public Class Constants
    Public Class TimeoutsAndDelaysAndCounters
        Public Shared ReadOnly DELAY__ServiceLoop As Long = 5
        Public Shared ReadOnly DELAY__ServiceLoopRepeats As Long = 6
    End Class

    Public Class FilesAndFolders
        Public Shared ReadOnly FILE__SiteKiosk As String = "sitekiosk.exe"

        Public Shared ReadOnly FILE__AutoStartupXmlFile As String = "%%SITEKIOSK_FOLDER%%\Config\startup.xml"
        Public Shared ReadOnly FILE__AutoStartupCleanXmlFile As String = "%%SITEKIOSK_FOLDER%%\Config\startup_clean.xml"
        Public Shared ReadOnly FILE__AutoStartupExe As String = "%%SITEKIOSK_FOLDER%%\SkStartup.exe"
    End Class

    Public Class UsersAndPasswords
        Public Shared ReadOnly USERNAME__SiteKiosk As String = "sitekiosk"
        Public Shared ReadOnly USERNAME__LobbyPCPuppy As String = "__OVCC_Puppy__"
        Public Shared ReadOnly USERNAME__GuestTekAdmin As String = "guesttek"

        Public Shared ReadOnly PASSWORD__SiteKiosk As String = "Provisi0"
    End Class

    Public Class RegistryKeys
        Public Shared ReadOnly KEY_Windows_RunAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run"
        Public Shared ReadOnly KEY_Windows_RunOnceAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunOnce"


        Public Shared ReadOnly KEY__OOER As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OO-ER"
        Public Shared ReadOnly VALUE__LastState As String = "LastState"
        Public Shared ReadOnly VALUE__LastEmergency As String = "LastEmergency"
        Public Shared ReadOnly VALUE__DisabledUntilOVCCStart As String = "EmergencyCount"

        Public Shared ReadOnly DEFAULTVALUE__LastState As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__LastEmergency As String = "1970-01-01 00:00:00"


        Public Shared ReadOnly KEY__OOER_EmergencyHistory As String = KEY__OOER & "\Emergency_History"
        Public Shared ReadOnly VALUE__OOER_EmergencyHistory As String() = New String() {"History00", "History01", "History02", "History03", "History04", "History05", "History06", "History07", "History08", "History09", "History10", "History11", "History12", "History13", "History14", "History15", "History16", "History17", "History18", "History19"}

        Public Shared ReadOnly DEFAULTVALUE__OOER_EmergencyHistory As String = ""


        Public Shared ReadOnly KEY__SiteKiosk As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"
        Public Shared ReadOnly VALUE__Build As String = "Build"
        Public Shared ReadOnly VALUE__InstallDir As String = "InstallDir"
        Public Shared ReadOnly VALUE__LastCfg As String = "LastCfg"
    End Class

    Public Class Service
        Public Shared ReadOnly Name As String = "OO-ER"
        Public Shared ReadOnly DisplayName As String = "OVCC Offline Emergency Repair"
        Public Shared ReadOnly Description As String = "Repairs emergency mode"
    End Class
End Class
