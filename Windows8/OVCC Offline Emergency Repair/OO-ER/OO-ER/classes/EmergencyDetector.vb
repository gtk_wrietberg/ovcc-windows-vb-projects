﻿Imports System.Text.RegularExpressions

Public Class EmergencyDetector
    Public Enum EMERGENCY_STATE
        NOTHING_HAPPENED = 0

        YES
        YES_BUT_NOT_RECENT
        YES_BUT_NOT_ANYMORE
        NO

        ERROR_OCCURRED
    End Enum

    Private Enum EMERGENCY_STATE_INTERNAL
        NOTHING_HAPPENED = 0

        YES
        YES_BUT_NOT_ANYMORE
        NO

        ERROR_OCCURRED
    End Enum

    Public Shared ReadOnly LineThreshold As Integer = 3

    Public Shared Function GetSiteKioskInEmergencyState(ByRef MessageText As String) As EMERGENCY_STATE
        Dim logfilePath As String = Helpers.SiteKiosk.GetSiteKioskLogfileFolder
        Dim logfileLatest As String = ""

        MessageText = ""

        If Not IO.Directory.Exists(logfilePath) Then
            MessageText = "SK logfile path (" & logfilePath & ") not found!"

            Return EMERGENCY_STATE.ERROR_OCCURRED
        End If


        logfileLatest = Helpers.FilesAndFolders.GetNewestFile(logfilePath, "[0-9]{4}-[0-9]{2}-[0-9]{2}\.txt", True)

        If logfileLatest.Equals("") Then
            'No logfile found, uh oh

            MessageText = "No logfile found in '" & logfilePath & "'!"

            Return EMERGENCY_STATE.ERROR_OCCURRED
        End If


        Dim LineCount As Integer = -1, ThresholdCount As Integer = -100, EmergencyDate As String = "", ReturnCode As EMERGENCY_STATE_INTERNAL

        ReturnCode = InspectLogfile(logfileLatest, LineCount, ThresholdCount, EmergencyDate, MessageText)

        Select Case ReturnCode
            Case EMERGENCY_STATE_INTERNAL.YES
                'Machine is in emergency state
                If ThresholdCount < LineThreshold Then

                    MessageText = "found 'emergency screen has been started' at '" & EmergencyDate & "'"

                    Return EMERGENCY_STATE.YES
                Else
                    MessageText = "found 'emergency screen has been started', but a while ago (" & ThresholdCount.ToString & ")"

                    Return EMERGENCY_STATE.YES_BUT_NOT_RECENT
                End If
            Case EMERGENCY_STATE_INTERNAL.YES_BUT_NOT_ANYMORE
                'Machine corrected itself
                MessageText = "found 'emergency screen has been started' at '" & EmergencyDate & "' but machine is running fine now"

                Return EMERGENCY_STATE.YES_BUT_NOT_ANYMORE
            Case EMERGENCY_STATE_INTERNAL.NO
                MessageText = "no emergency found"

                Return EMERGENCY_STATE.NO
            Case EMERGENCY_STATE_INTERNAL.ERROR_OCCURRED
                'There was an erroro, oopsie
                MessageText = "error: " & MessageText

                Return EMERGENCY_STATE.ERROR_OCCURRED
            Case Else
                Return EMERGENCY_STATE.NOTHING_HAPPENED
        End Select
    End Function

    Private Shared Function InspectLogfile(LogFilePath As String, ByRef LineCount As Integer, ByRef ThresholdCount As Integer, ByRef EmergencyDate As String, ByRef MessageText As String) As EMERGENCY_STATE_INTERNAL
        Dim _ret As EMERGENCY_STATE_INTERNAL = EMERGENCY_STATE_INTERNAL.NOTHING_HAPPENED

        Try
            Dim _lines As String() = IO.File.ReadAllLines(LogFilePath)
            Dim _linecount As Integer = 0, _thresholdcount As Integer = 0
            Dim regex_skip01 As Regex = New Regex("^.+\[SiteKiosk\] Notification: \[(Memory|Process) Status\].+$")
            Dim regex_skip02 As Regex = New Regex("^([0-9]{2} 03f.)(.+)(Monitor )(Off|On)$")

            Dim regex_emergency As Regex = New Regex("^([0-9]{2} 0400 )(.+)( \[SiteKiosk\])(.)(emergency screen)(.+)$")
            Dim regex_emergency_over As Regex = New Regex("^([0-9]{2} 0401)(.+)(exited the emergency)(.+)$")


            _ret = EMERGENCY_STATE_INTERNAL.NO

            '40 0400 2019-01-15 09:44:38 +0100 [SiteKiosk] SiteKiosk was not able to start properly for 10 times in 300 seconds or the SiteRemote Client service did not start in time, therefore the emergency screen has been started.
            '20 0000 2019-01-15 23:55:53 +0100 [SiteKiosk] Notification: [Memory Status] Physical Memory: 1.80 GB (6.08 GB free, 22.9% used); Paging File: 2.10 GB (7.03 GB free, 23.0% used)
            '20 0000 2019-01-15 23:55:53 +0100 [SiteKiosk] Notification: [Process Status] GDI Objects: 62; USER Objects: 118; Handles: 1401; Process Memory Usage: 93.06 MB
            '20 0401 2019-01-15 14:32:21 +0100 [SiteKiosk] SiteKiosk has exited the emergency mode.
            '20 03fa 2019-03-13 09:09:08 +0100 [SiteKiosk] Monitor Off
            '20 03fb 2019-03-13 09:09:10 +0100 [SiteKiosk] Monitor On

            For Each _line As String In _lines.Reverse()
                If regex_emergency.Match(_line).Success Then
                    _ret = EMERGENCY_STATE_INTERNAL.YES

                    EmergencyDate = regex_emergency.Replace(_line, "$1")

                    Exit For
                End If

                'If Not regex_skip01.Match(_line).Success Then
                If Not regex_skip01.Match(_line).Success And Not regex_skip02.Match(_line).Success Then
                    _thresholdcount += 1
                End If

                If regex_emergency_over.Match(_line).Success Then
                    '_thresholdcount = -1
                    _ret = EMERGENCY_STATE_INTERNAL.YES_BUT_NOT_ANYMORE

                    Exit For
                End If

                _linecount += 1
            Next


            LineCount = _linecount
            ThresholdCount = _thresholdcount

            MessageText = ""
        Catch ex As Exception
            LineCount = 0
            ThresholdCount = 0

            MessageText = ex.Message

            _ret = EMERGENCY_STATE_INTERNAL.ERROR_OCCURRED
        End Try

        Return _ret
    End Function
End Class
