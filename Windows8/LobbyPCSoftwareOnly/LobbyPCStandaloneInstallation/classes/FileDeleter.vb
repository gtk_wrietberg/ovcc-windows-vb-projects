Imports System.Runtime.InteropServices

Public Class FileDeleter
    Private ReadOnly MOVEFILE_DELAY_UNTIL_REBOOT As Integer = 4

    Private mFilesPaths As List(Of String)
    Private mFilesDeleted As List(Of Boolean)
    Private mFilesDelayed As List(Of Boolean)

    Public Sub New()
        mFilesPaths = New List(Of String)
        mFilesDeleted = New List(Of Boolean)
        mFilesDelayed = New List(Of Boolean)
    End Sub

    Private Function DeleteFiles() As Boolean
        Dim i As Integer

        For i = 0 To mFilesPaths.Count - 1

        Next
        
    End Function

    Private Function DeleteFile() As Boolean
        Try
            IO.file.Delete((cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder, True)
            oLogger.WriteToLog("ok ", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Function

    <Runtime.InteropServices.DllImport("kernel32", SetLastError:=True)> _
    Private Shared Function MoveFileEx(ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal dwFlags As Integer) As Boolean
    End Function

    Private Function DeleteFileOnReboot(ByVal sFileName As String) As Boolean
        Dim bReturn As Boolean

        Try
            bReturn = MoveFileEx(sFileName, Nothing, MOVEFILE_DELAY_UNTIL_REBOOT)
        Catch ex As Exception
            bReturn = False
        End Try

        Return bReturn
    End Function

    Private Function RecursiveSearch(ByVal sPath As String) As Boolean
        Dim dirInfo As New IO.DirectoryInfo(sPath)
        Dim fileObject As IO.FileSystemInfo

        For Each fileObject In dirInfo.GetFileSystemInfos()
            If fileObject.Attributes = IO.FileAttributes.Directory Then
                RecursiveSearch(fileObject.FullName)
            Else
                mFilesPaths.Add(fileObject.FullName)
                mFilesDeleted.Add(False)
                mFilesDelayed.Add(False)
            End If
        Next

        Return True
    End Function

End Class
