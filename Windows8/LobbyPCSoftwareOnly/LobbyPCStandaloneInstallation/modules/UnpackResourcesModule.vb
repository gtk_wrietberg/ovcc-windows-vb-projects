Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sSourceFolder As String
        Dim sDestinationFolder As String

        oLogger.WriteToLog("Copying resources")

        sSourceFolder = cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder
        sDestinationFolder = sSourceFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        '        If oLicenseKeySettings.ServerRegion = "us" Then
        '        oLogger.WriteToLog("Ye olde Altiris package (us)", , 1)
        '       oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.us.exe"
        '        Else
        '        oLogger.WriteToLog("Altiris package (eu)", , 1)
        '        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.eu.exe"
        '        End If
        oLogger.WriteToLog("Altiris package", , 1)
        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.eu.exe"


        oLogger.WriteToLog("CreateShortcut package", , 1)
        oSettings.Path_CreateShortcut = sDestinationFolder & "\CreateShortcut.package.exe"

        oLogger.WriteToLog("LogMeIn package", , 1)
        oSettings.Path_LogMeIn = sDestinationFolder & "\LogMeIn.deploy.exe"

        oLogger.WriteToLog("iBAHN Updater package (Win7)", , 1)
        oSettings.Path_iBAHNUpdate = sDestinationFolder & "\iBAHNUpdate.package.Win7.exe"


        oSettings.Path_iBAHNUI = ""
        oSettings.Path_GuesttekUI = ""
        oSettings.Path_HiltonUI = ""

        'Decide which newUI package to install
        If IO.File.Exists(sDestinationFolder & "\" & cINSTALLERS__HILTON_UI__EMEA) And IO.File.Exists(sDestinationFolder & "\" & cINSTALLERS__HILTON_UI__US) Then
            'Hilton package, only if included in archive
            oLogger.WriteToLog("Hilton UI package (Win7)", , 1)
            If oLicenseKeySettings.ServerRegion.ToLower = "us" Then
                oSettings.Path_HiltonUI = sDestinationFolder & "\" & cINSTALLERS__HILTON_UI__US
            Else
                oSettings.Path_HiltonUI = sDestinationFolder & "\" & cINSTALLERS__HILTON_UI__EMEA
            End If
            oLogger.WriteToLog("Path", , 2)
            oLogger.WriteToLog(oSettings.Path_HiltonUI, , 3)

            oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "HiltonPackageDetected", "yes")
        ElseIf IO.File.Exists(sDestinationFolder & "\" & cINSTALLERS__GUESTTEK_UI__EMEA) And IO.File.Exists(sDestinationFolder & "\" & cINSTALLERS__GUESTTEK_UI__US) Then
            oLogger.WriteToLog("Guest-tek UI package (Win7)", , 1)
            If oLicenseKeySettings.ServerRegion.ToLower = "us" Then
                oSettings.Path_GuesttekUI = sDestinationFolder & "\" & cINSTALLERS__GUESTTEK_UI__US
            Else
                oSettings.Path_GuesttekUI = sDestinationFolder & "\" & cINSTALLERS__GUESTTEK_UI__EMEA
            End If
        Else
            oLogger.WriteToLog("iBAHN UI package (Win7)", , 1)
            oSettings.Path_HiltonUI = ""
            If oLicenseKeySettings.ServerRegion.ToLower = "us" Then
                oSettings.Path_iBAHNUI = sDestinationFolder & "\" & cINSTALLERS__IBAHN_UI__US
            Else
                oSettings.Path_iBAHNUI = sDestinationFolder & "\" & cINSTALLERS__IBAHN_UI__EMEA
            End If
        End If

        oLogger.WriteToLog("VeriSign", , 1)
        oSettings.Path_VeriSign = sDestinationFolder & "\VeriSign.updater.exe"

        oLogger.WriteToLog("LobbyPCAgent package", , 1)
        oSettings.Path_LobbyPCAgent = sDestinationFolder & "\LobbyPCAgentPatchInstaller.package.exe"

        oLogger.WriteToLog("LobbyPCWatchdog package", , 1)
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\LobbyPCWatchdog.package.exe"

        oLogger.WriteToLog("SiteKiosk7", , 1)
        oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk7.exe"


        sDestinationFolder = cPATHS_GuestTekProgramFilesFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("LobbyPCAutoStart", , 1)
        _CopyFile("LobbyPCAutoStart.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("LobbyPCAutoStart.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_AutoStart = sDestinationFolder & "\LobbyPCAutoStart.exe"

        oLogger.WriteToLog("LobbyPCSoftwareOnlyUninstaller", , 1)
        _CopyFile("LobbyPCSoftwareOnlyUninstaller.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("LobbyPCSoftwareOnlyUninstaller.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("LobbyPCSoftwareOnlyUninstaller_.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("LobbyPCSoftwareOnlyUninstaller_.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_Uninstaller = sDestinationFolder & "\LobbyPCSoftwareOnlyUninstaller.exe"


        sDestinationFolder = cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesToolsFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("HotelPageUpdater", , 1)
        _CopyFile("HotelPageUpdater.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("HotelPageUpdater.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_HotelPageUpdater = sDestinationFolder & "\HotelPageUpdater.exe"


        sDestinationFolder = cPATHS_GuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInternalFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If

        oLogger.WriteToLog("PcHasRebooted", , 1)
        _CopyFile("PcHasRebooted.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_PcHasRebooted = sDestinationFolder & "\PcHasRebooted.exe"

        oLogger.WriteToLog("SkCfgUpdaterSiteCash", , 1)
        _CopyFile("SkCfgUpdater_SiteCash.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("SkCfgUpdater_SiteCash.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_SkCfgUpdaterSiteCash = sDestinationFolder & "\SkCfgUpdater_SiteCash.exe"

        oLogger.WriteToLog("SiteKioskLogoutExtraTasks", , 1)
        _CopyFile("SiteKioskLogoutExtraTasks.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_SiteKioskLogoutExtraTasks = sDestinationFolder & "\SiteKioskLogoutExtraTasks.exe"

        oLogger.WriteToLog("TaskbarAutohide", , 1)
        _CopyFile("TaskbarAutohide.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("TaskbarAutohide.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("TaskbarAutohideEmergency.exe", sSourceFolder, sDestinationFolder, 2)
        _CopyFile("TaskbarAutohideEmergency.exe.manifest", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_TaskbarAutohide = sDestinationFolder & "\TaskbarAutohide.exe"
    End Sub

    Private Sub _CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, ByVal iLogLevel As Integer)
        oLogger.WriteToLog("copying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)

        oLogger.WriteToLog("retrying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
    End Sub
End Module
