Module UnpackResourcesModule
    Public Sub UnpackResources()
        Dim sSourceFolder As String
        Dim sDestinationFolder As String

        oLogger.WriteToLog("Copying resources")

        sSourceFolder = gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesResourcesFolder
        sDestinationFolder = sSourceFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If


        oLogger.WriteToLog("Visual C++ redistributable package", , 1)
        oSettings.Path_VisualCplusplusRedistributable = sDestinationFolder & "\vcredist_x86.exe"


        oLogger.WriteToLog("Altiris package", , 1)
        oSettings.Path_Altiris = sDestinationFolder & "\Altiris.package.exe"


        oLogger.WriteToLog("LogMeIn package", , 1)
        oSettings.Path_LogMeIn = sDestinationFolder & "\LogMeIn.package.exe"


        oLogger.WriteToLog("SiteKiosk Updater package (Win8)", , 1)
        oSettings.Path_SiteKioskUpdate = sDestinationFolder & "\SiteKioskUpdater.package.exe"


        oLogger.WriteToLog("SiteKiosk Security Updater", , 1)
        oSettings.Path_SystemSecurityUpdater = sDestinationFolder & "\SystemSecurityUpdater.exe"


        oLogger.WriteToLog("LobbyPCAgent package", , 1)
        oSettings.Path_LobbyPCAgent = sDestinationFolder & "\OVCCAgent.package.exe"


        oLogger.WriteToLog("LobbyPCWatchdog package", , 1)
        oSettings.Path_LobbyPCWatchdog = sDestinationFolder & "\OVCCWatchdog.package.exe"


        oLogger.WriteToLog("SiteKiosk", , 1)
        If IO.File.Exists(sDestinationFolder & "\sitekiosk10.exe") Then
            oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk10.exe"
            oLogger.WriteToLog("10", , 2)
        ElseIf IO.File.Exists(sDestinationFolder & "\sitekiosk9.exe") Then
            oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk9.exe"
            oLogger.WriteToLog("9", , 2)
        Else
            oSettings.Path_SiteKiosk = sDestinationFolder & "\sitekiosk8.exe"
            oLogger.WriteToLog("8", , 2)
        End If


        sDestinationFolder = gGuestTekProgramFilesFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If


        'tools
        sDestinationFolder = gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesToolsFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If


        oLogger.WriteToLog("OVCCStart", , 1)
        _CopyFile("OVCCStart.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_AutoStart = sDestinationFolder & "\OVCCStart.exe"


        oLogger.WriteToLog("OVCCInformationUpdater", , 1)
        _CopyFile("OVCCInformationUpdater.exe", sSourceFolder, sDestinationFolder, 2)
        oSettings.Path_HotelPageUpdater = sDestinationFolder & "\OVCCInformationUpdater.exe"



        'internal apps
        sDestinationFolder = gGuestTekProgramFilesFolder & cPATHS_GuestTekProgramFilesInternalFolder

        If Not IO.Directory.Exists(sDestinationFolder) Then
            IO.Directory.CreateDirectory(sDestinationFolder)
        End If
    End Sub

    Private Sub _CopyFile(ByVal sFileName As String, ByVal sSourcePath As String, ByVal sDestinationPath As String, ByVal iLogLevel As Integer)
        oLogger.WriteToLog("copying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)

        oLogger.WriteToLog("retrying", , iLogLevel)
        If CopyFile(sFileName, sSourcePath, sDestinationPath, iLogLevel + 1) Then
            oLogger.WriteToLog("ok", , iLogLevel + 1)

            Exit Sub
        End If

        oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, iLogLevel + 1)
    End Sub
End Module
