Public Class Settings
    Private mInstaller_LogMeIn_Path As String
    Private mInstaller_Altiris_Path As String
    Private mInstaller_iBAHNUpdate_Path As String
    Private mInstaller_iBAHNUI_Path As String
    Private mInstaller_HiltonUI_Path As String
    Private mInstaller_AirlineButton_Path As String
    Private mInstaller_SiteKiosk_Path As String
    Private mInstaller_AutoStart_Path As String
    Private mInstaller_HotelPageUpdater_Path As String
    Private mInstaller_CreateShortcut_Path As String
    Private mInstaller_PcHasRebooted_Path As String
    Private mInstaller_LobbyPCAgent_Path As String
    Private mInstaller_LobbyPCWatchdog_Path As String
    Private mInstaller_SiteKioskLogoutExtraTasks_Path As String
    Private mInstaller_TaskbarAutohide_Path As String
    Private mInstaller_SkCfgUpdaterSiteCash_Path As String
    Private mInstaller_VeriSign_Path As String
    Private mInstaller_Uninstaller_Path As String

    Public Sub New()

    End Sub

#Region "Properties"
    Public Property Path_LogMeIn() As String
        Get
            Return mInstaller_LogMeIn_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LogMeIn_Path = value
        End Set
    End Property

    Public Property Path_Altiris() As String
        Get
            Return mInstaller_Altiris_Path
        End Get
        Set(ByVal value As String)
            mInstaller_Altiris_Path = value
        End Set
    End Property

    Public Property Path_iBAHNUpdate() As String
        Get
            Return mInstaller_iBAHNUpdate_Path
        End Get
        Set(ByVal value As String)
            mInstaller_iBAHNUpdate_Path = value
        End Set
    End Property

    Public Property Path_iBAHNUI() As String
        Get
            Return mInstaller_iBAHNUI_Path
        End Get
        Set(ByVal value As String)
            mInstaller_iBAHNUI_Path = value
        End Set
    End Property

    Public Property Path_HiltonUI() As String
        Get
            Return mInstaller_HiltonUI_Path
        End Get
        Set(ByVal value As String)
            mInstaller_HiltonUI_Path = value
        End Set
    End Property

    Public Property Path_SiteKiosk() As String
        Get
            Return mInstaller_SiteKiosk_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SiteKiosk_Path = value
        End Set
    End Property

    Public Property Path_AutoStart() As String
        Get
            Return mInstaller_AutoStart_Path
        End Get
        Set(ByVal value As String)
            mInstaller_AutoStart_Path = value
        End Set
    End Property

    Public Property Path_HotelPageUpdater() As String
        Get
            Return mInstaller_HotelPageUpdater_Path
        End Get
        Set(ByVal value As String)
            mInstaller_HotelPageUpdater_Path = value
        End Set
    End Property

    Public Property Path_CreateShortcut() As String
        Get
            Return mInstaller_CreateShortcut_Path
        End Get
        Set(ByVal value As String)
            mInstaller_CreateShortcut_Path = value
        End Set
    End Property

    Public Property Path_PcHasRebooted() As String
        Get
            Return mInstaller_PcHasRebooted_Path
        End Get
        Set(ByVal value As String)
            mInstaller_PcHasRebooted_Path = value
        End Set
    End Property

    Public Property Path_LobbyPCAgent() As String
        Get
            Return mInstaller_LobbyPCAgent_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LobbyPCAgent_Path = value
        End Set
    End Property

    Public Property Path_LobbyPCWatchdog() As String
        Get
            Return mInstaller_LobbyPCWatchdog_Path
        End Get
        Set(ByVal value As String)
            mInstaller_LobbyPCWatchdog_Path = value
        End Set
    End Property

    Public Property Path_SiteKioskLogoutExtraTasks() As String
        Get
            Return mInstaller_SiteKioskLogoutExtraTasks_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SiteKioskLogoutExtraTasks_Path = value
        End Set
    End Property

    Public Property Path_TaskbarAutohide() As String
        Get
            Return mInstaller_TaskbarAutohide_Path
        End Get
        Set(ByVal value As String)
            mInstaller_TaskbarAutohide_Path = value
        End Set
    End Property

    Public Property Path_SkCfgUpdaterSiteCash() As String
        Get
            Return mInstaller_SkCfgUpdaterSiteCash_Path
        End Get
        Set(ByVal value As String)
            mInstaller_SkCfgUpdaterSiteCash_Path = value
        End Set
    End Property

    Public Property Path_VeriSign() As String
        Get
            Return mInstaller_VeriSign_Path
        End Get
        Set(ByVal value As String)
            mInstaller_VeriSign_Path = value
        End Set
    End Property

    Public Property Path_Uninstaller() As String
        Get
            Return mInstaller_Uninstaller_Path
        End Get
        Set(ByVal value As String)
            mInstaller_Uninstaller_Path = value
        End Set
    End Property
#End Region
End Class
