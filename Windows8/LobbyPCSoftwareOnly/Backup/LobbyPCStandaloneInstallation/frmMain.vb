Imports Microsoft.WindowsAPICodePack
Imports Microsoft.WindowsAPICodePack.Taskbar
Imports System.Threading

Public Class frmMain
    Private iApplicationInstallationCount As Integer = 0
    Private iApplicationInstallationCountMax As Integer = 13

    Private oPrerequisites As Prerequisites

    Private WithEvents oProcess As ProcessRunner
    Private WithEvents oLicense As LicenseKey

    Private oComputerName As ComputerName
    Private oLocalization As Localization

    Private installThread As Thread

    Private Panels() As Panel
    Private iActiveGroupBox As Integer

    Private Enum GroupBoxes As Integer
        Prerequisites = 0
        StartScreen = 1
        TermsAndConditions = 2
        LicenseKey = 3
        LicenseKeyValidation = 4
        PreInstallWarning = 5
        Installation = 6
        DoneAndRestart = 7
        ErrorDuringInstallation = 8
    End Enum

    Private Enum InstallationOrder As Integer
        IncrementLicense = 1
        LogMeIn
        Altiris
        CreateShortcut
        SiteKiosk
        iBAHNUpdate
        NewUI
        SkCfgUpdaterSiteCash
        VeriSignUpdate
        HotelPageUpdate
        LobbyPCAgent
        LobbyPCWatchdog
        Shortcuts
        SiteKioskSecurityUpdater
        PostDeployment
        DONE
    End Enum

    Delegate Sub LogToProcessTextBoxCallback(ByVal [text] As String)

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Panels = New Panel() {Me.pnlPrerequisites, Me.pnlStartScreen, Me.pnlTermsAndConditions, Me.pnlLicenseKey, Me.pnlLicenseKeyValidation, Me.pnlPreInstallWarning, Me.pnlInstallation, Me.pnlDoneAndRestart, Me.pnlErrorOccurred}
    End Sub

#Region " ClientAreaMove Handling "
    'Private Const WM_NCHITTEST As Integer = &H84
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    'Private Const HTCLIENT As Integer = &H1
    Private Const HTCAPTION As Integer = &H2
    '    Protected Overrides Sub WndProc(ByRef m As System.Windows.Forms.Message)
    '        Select Case m.Msg
    '            Case WM_NCHITTEST
    '                MyBase.WndProc(m)
    '    'If m.Result = HTCLIENT Then m.Result = HTCAPTION
    '                If m.Result.ToInt32 = HTCLIENT Then m.Result = IntPtr.op_Explicit(HTCAPTION) 'Try this in VS.NET 2002/2003 if the latter line of code doesn't do it... thx to Suhas for the tip.
    '            Case Else
    '    'Make sure you pass unhandled messages back to the default message handler.
    '                MyBase.WndProc(m)
    '        End Select
    '    End Sub
#End Region

#Region "Form Events"
    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'Don't close the form when we're still busy
        e.Cancel = gBusyInstalling
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ActivatePrevInstance(Me.Text)

        oPrerequisites = New Prerequisites
        oComputerName = New ComputerName
        oSettings = New Settings
        oLicense = New LicenseKey
        oLogger = New Logger
        oRegistryChanger = New RegistryChanger

        If InStr(oComputerName.ComputerName.ToLower, "rietberg".ToLower) > 0 _
        Or InStr(oComputerName.ComputerName.ToLower, "superlekkerding".ToLower) > 0 Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If
        End If

        oPrerequisites.CheckWindowsVersion(cPREREQUISITES_NeededWindowsVersion, False)
        oPrerequisites.CheckInternetConnection()
        oPrerequisites.IsUserAdmin()
        oPrerequisites.IsAlreadyInstalled()
        If cDEBUG_ForcePrerequisiteWindowsVersionError Or cDEBUG_ForcePrerequisiteInternetConnectionError Or cDEBUG_ForcePrerequisiteUserIsAdminError Or cDEBUG_ForcePrerequisiteAlreadyInstalledError Then
            oPrerequisites.ForcePrerequisiteErrors(cDEBUG_ForcePrerequisiteWindowsVersionError, cDEBUG_ForcePrerequisiteInternetConnectionError, cDEBUG_ForcePrerequisiteUserIsAdminError, cDEBUG_ForcePrerequisiteAlreadyInstalledError)
        End If


        lblWindowsVersion.Text = oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion)

        oLocalization = New Localization
        oLocalization.LoadLanguageFile()

        PopulateLanguageComboBox()
        ReloadLocalization()

        oLicense.LicenseCodeLength = cLICENSE_KeyLength
        oLicense.PermitTestLicense = cLICENSE_PermitOfflineLicense

        txtLicenseCode.Text = ""

        'GUI
        Me.Width = 624
        Me.Height = 490

        Me.StartPosition = FormStartPosition.CenterScreen
        Me.WindowState = FormWindowState.Normal

        Me.Left = (My.Computer.Screen.Bounds.Width - Me.Width) / 2
        Me.Top = (My.Computer.Screen.Bounds.Height - Me.Height) / 2

        'Me.BackColor = Color.White
        'Me.BackgroundImageLayout = ImageLayout.Tile

        Me.pnlBackgroundBorder.Visible = cGUI_GroupBoxBorder_visible

        btnExit.Top = btnContinue.Top
        btnExit.Left = btnContinue.Left

        btnBack.Top = btnCancel.Top
        btnBack.Left = btnCancel.Left

        If Not IO.Directory.Exists(cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder) Then
            IO.Directory.CreateDirectory(cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder)
        End If

        oLogger.LogFilePath = cPATHS_iBAHNProgramFilesFolder & "\"

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        lblVersion.Text = "v" & myBuildInfo.FileMajorPart & "." & myBuildInfo.FileMinorPart

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        If gTestMode Then
            oLogger.WriteToLog("Running in test mode!!!", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        End If


        gBusyInstalling = False

        'Terms and Conditions
        Dim tmpResources As New MyResources
        txtTC.Text = tmpResources.ToString("TermsAndConditions")

        txtTC.ReadOnly = True
        ParseTermsAndConditions(txtTC)

        'Install count max
        iApplicationInstallationCountMax = InstallationOrder.DONE


        'Panels
        Dim i As Integer
        For i = 0 To Panels.Length - 1
            'Panels(i).Top = 84
            'Panels(i).Left = 16
            Panels(i).Top = 76
            Panels(i).Left = 5

            Panels(i).Visible = False
        Next

        '---------------------------------------------------------
        gSkipSkype = False
        gUnattendedLicenseCode = String.Empty
        gUnattendedInstall = False

        oLogger.WriteToLog("loading command line arguments", , 1)
        For Each arg As String In Environment.GetCommandLineArgs()
            oLogger.WriteToLog("found: " & arg, , 2)

            If InStr(arg, "-unattended=") > 0 Then
                gUnattendedLicenseCode = arg.Replace("-unattended=", "")
                oLogger.WriteToLog("unattended: " & gUnattendedLicenseCode, , 3)
                gUnattendedInstall = True
            End If
            If arg = "-hideprogress" Then
                gHideProgress = True

                Me.Opacity = 0.0

                oLogger.WriteToLog("hide progress: " & gHideProgress.ToString, , 3)
            End If
            If arg = "-skipskype" Then
                gSkipSkype = True

                oLogger.WriteToLog("skip skype: " & gSkipSkype.ToString, , 3)
            End If
        Next

        If gUnattendedInstall Then
            txtLicenseCode.Text = gUnattendedLicenseCode

            iActiveGroupBox = GroupBoxes.LicenseKeyValidation - 1
        Else
            iActiveGroupBox = -1
        End If


        NextGroupBox()
    End Sub
#End Region

#Region "Private Functions"
#Region "Localization"
    Private Sub PopulateLanguageComboBox()
        '-----------------------------------
        cmbBoxLanguages.Items.Clear()
        Try

            Dim sLanguages() As String
            Dim i As Integer

            sLanguages = oLocalization.GetLanguageNames

            For i = 0 To sLanguages.Length - 1
                If Not sLanguages(i) Is Nothing Then
                    cmbBoxLanguages.Items.Add(sLanguages(i))
                End If
            Next

            cmbBoxLanguages.SelectedIndex = 0
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub ReloadLocalization()
        lblTitle.Text = oLocalization.CurrentLanguage.Title

        btnBack.Text = oLocalization.CurrentLanguage.ButtonBack
        btnCancel.Text = oLocalization.CurrentLanguage.ButtonCancel
        btnContinue.Text = oLocalization.CurrentLanguage.ButtonNext
        btnExit.Text = oLocalization.CurrentLanguage.ButtonExit

        lblSelectLanguage.Text = oLocalization.CurrentLanguage.WelcomeSelectLanguage

        lblPrerequisitesRestart.Text = oLocalization.CurrentLanguage.PrerequisitesErrorRestartText

        pnlTermsAndConditions.Text = oLocalization.CurrentLanguage.TermsAndConditionsTitle
        lblTCText.Text = oLocalization.CurrentLanguage.TermsAndConditionsText
        chkTCAgree.Text = oLocalization.CurrentLanguage.TermsAndConditionsCheckBox

        pnlLicenseKey.Text = oLocalization.CurrentLanguage.LicenseValidationTitle
        lblLicenseValidation.Text = oLocalization.CurrentLanguage.LicenseValidationText.Replace("%%LICENSE_CODE_LENGTH%%", cLICENSE_KeyLength.ToString)

        grpbxLicense.Text = oLocalization.CurrentLanguage.LicenseValidationTitle2

        pnlLicenseKeyValidation.Text = oLocalization.CurrentLanguage.LicenseValidationTitle
        lblLicenseValidationCopy.Text = oLocalization.CurrentLanguage.LicenseValidationText.Replace("%%LICENSE_CODE_LENGTH%%", cLICENSE_KeyLength.ToString)
        grpbxLicenseCopy.Text = oLocalization.CurrentLanguage.LicenseValidationTitle2
        lblLicenseProgress.Text = oLocalization.CurrentLanguage.LicenseValidationProgressText

        lblInstallationContinueText.Text = oLocalization.CurrentLanguage.InstallationContinueText

        pnlInstallation.Text = oLocalization.CurrentLanguage.InstallationProgressTitle
        lblInstallationProgressText.Text = oLocalization.CurrentLanguage.InstallationProgressText

        pnlDoneAndRestart.Text = oLocalization.CurrentLanguage.DoneTitle
        lblReboot.Text = oLocalization.CurrentLanguage.DoneRestartText
        lblReboot2.Text = oLocalization.CurrentLanguage.DoneRestartText2
        radRestartNow.Text = oLocalization.CurrentLanguage.DoneRestartNowText
        radRestartLater.Text = oLocalization.CurrentLanguage.DoneRestartLaterText
    End Sub
#End Region

#Region "License code validation"
    Private Sub CheckLicenseCode()
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog("Validating license code")

        oLicense.LicenseCode = txtLicenseCode.Text
        oLicense.ApplicationVersion = Double.Parse(myBuildInfo.FileMajorPart & "." & myBuildInfo.FileMinorPart)

        oLogger.WriteToLog("license code: " & oLicense.LicenseCode, , 1)

        oLicense.CheckLicense()
    End Sub

    Private Sub IncrementLicenseCode()
        oLogger.WriteToLog("Incrementing license code")

        oLicense.IncrementLicense()
    End Sub
#End Region

#Region "CheckPrerequisites"
    Private Sub CheckPrerequisites()
        Dim sError As String

        If oPrerequisites.CheckComplete Then
            If oPrerequisites.AllIsOk Then
                NextGroupBox()
            Else
                oLogger.WriteToLog("Prerequisites error", Logger.MESSAGE_TYPE.LOG_ERROR)

                sError = ""
                If Not oPrerequisites.InternetConnectivity Then
                    oLogger.WriteToLog("No internet connection", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorInternetConnection
                End If
                If Not oPrerequisites.CorrectWindowsVersion Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If

                    If oPrerequisites.HigherWindowsVersionsAreAccepted Then
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " < " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= oLocalization.CurrentLanguage.PrerequisitesErrorMinimumWindowsVersion
                    Else
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " != " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= oLocalization.CurrentLanguage.PrerequisitesErrorWindowsVersion
                    End If

                    sError = sError.Replace("%%NEEDED_WINDOWS_VERSION%%", oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion))
                    sError = sError.Replace("%%RUNNING_WINDOWS_VERSION%%", oPrerequisites.WindowsVersion)
                End If
                If Not oPrerequisites.UserIsAdmin Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("No admin privileges", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorAdminRights
                End If
                If oPrerequisites.IsAlreadyInstalled Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("Already installed", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= oLocalization.CurrentLanguage.PrerequisitesErrorAlreadyInstalled
                End If

                lblPrerequisitesError.Text = sError

                If gTestMode Then
                    UpdateButtons(0, 0, 3, 0)
                Else
                    UpdateButtons(0, 0, 0, 3)
                End If
            End If
        Else
            lblPrerequisitesError.Text = "Unknown error 31. Please contact the helpdesk."

            UpdateButtons(0, 0, 0, 3)
        End If
    End Sub
#End Region

#Region "Groupbox handling"
    Private Sub GotoGroupBox(ByVal iGroupBoxIndex As Integer)
        iActiveGroupBox = iGroupBoxIndex
        UpdateGroupBox()
    End Sub

    Private Sub NextGroupBox()
        iActiveGroupBox += 1
        UpdateGroupBox()
    End Sub

    Private Sub PreviousGroupBox()
        iActiveGroupBox -= 1
        'Skip these group box if we are going BACK
        'Windows version and internet connection error dialog
        If iActiveGroupBox = GroupBoxes.StartScreen Then
            iActiveGroupBox = -1
        End If
        'License validation dialog
        If iActiveGroupBox = GroupBoxes.LicenseKeyValidation Then
            iActiveGroupBox = GroupBoxes.LicenseKey
        End If
        UpdateGroupBox()
    End Sub

    Private Sub UpdateGroupBox()
        If iActiveGroupBox >= Panels.Length Then
            iActiveGroupBox = Panels.Length - 1
        End If
        'We handle the case of <0 in the following select case statement
        'If iActiveGroupBox < 0 Then
        '   iActiveGroupBox = 0
        'End If

        oLogger.WriteToLog("Groupbox / action: " & iActiveGroupBox.ToString, Logger.MESSAGE_TYPE.LOG_DEBUG)

        Select Case iActiveGroupBox
            Case GroupBoxes.Prerequisites
                UpdateButtons(0, 0, 0, 0)

                CheckPrerequisites()
            Case GroupBoxes.StartScreen
                UpdateButtons(0, 3, 3, 0)

                Me.ControlBox = True
            Case GroupBoxes.TermsAndConditions
                UpdateButtons(0, 0, 2, 0)
                chkTCAgree.Checked = False
            Case GroupBoxes.LicenseKey
                UpdateButtons(3, 0, 2, 0)

                Me.ControlBox = True

                txtLicenseCode.SelectAll()
            Case GroupBoxes.LicenseKeyValidation
                UpdateButtons(2, 0, 2, 0)

                Me.ControlBox = False

                txtLicenseCodeCopy.Text = txtLicenseCode.Text
                tmrLicenseValidationDelay.Enabled = True
            Case GroupBoxes.PreInstallWarning
                UpdateButtons(0, 3, 3, 0)

                Me.ControlBox = True

                PreInstallSteps()
            Case GroupBoxes.Installation
                UpdateButtons(0, 0, 0, 0)

                Me.ControlBox = False

                gBusyInstalling = True

                ProgressBarMarqueeStart()

                StartDeployment()
            Case GroupBoxes.DoneAndRestart
                UpdateButtons(0, 0, 0, 3)
                radRestartNow.Checked = True
                radRestartLater.Checked = False

                If gUnattendedInstall Then
                    ApplicationIsExited(0)
                End If
            Case GroupBoxes.ErrorDuringInstallation
                UpdateButtons(0, 0, 0, 3)

                lblErrorCode.Text = "Error code:  # " & iApplicationInstallationCount.ToString

                If gUnattendedInstall Then
                    ApplicationIsExited(-2)
                End If
            Case Else
                Application.Exit()
        End Select

        If iActiveGroupBox >= 0 And iActiveGroupBox < Panels.Length Then
            Panels(iActiveGroupBox).Visible = True
            pnlBackgroundBorder.Top = Panels(iActiveGroupBox).Top - 1
            pnlBackgroundBorder.Left = Panels(iActiveGroupBox).Left - 1
            pnlBackgroundBorder.Width = Panels(iActiveGroupBox).Width + 2
            pnlBackgroundBorder.Height = Panels(iActiveGroupBox).Height + 2
            pnlBackgroundBorder.Refresh()
        End If

        Dim i As Integer
        For i = 0 To Panels.Length - 1
            If i <> iActiveGroupBox Then
                Panels(i).Visible = False
            End If
        Next
    End Sub
#End Region

#Region "Thread Safe stuff"
    Private Sub LogToProcessTextBox(ByVal [text] As String)
        If Me.txtProgress.InvokeRequired Then
            Dim d As New LogToProcessTextBoxCallback(AddressOf LogToProcessTextBox)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.txtProgress.AppendText([text] & vbCrLf)
        End If
    End Sub

    Private Sub UupdateLicenseError(ByVal [text] As String)
        lblLicenseError.Text = [text]
    End Sub

    Private Sub SshowHideLicenseError(ByVal [bool] As Boolean)
        grpboxLicenseError.Visible = [bool]
    End Sub
#End Region

#Region "Button Handling"
    Private Sub UpdateButtons(ByVal iBack As Integer, ByVal iCancel As Integer, ByVal iContinue As Integer, ByVal iExit As Integer)
        btnBack.Enabled = iBack And 1
        btnBack.Visible = iBack And 2

        btnCancel.Enabled = iCancel And 1
        btnCancel.Visible = iCancel And 2

        btnContinue.Enabled = iContinue And 1
        btnContinue.Visible = iContinue And 2

        btnExit.Enabled = iExit And 1
        btnExit.Visible = iExit And 2
    End Sub
#End Region

#Region "Deployment and Installers"
#Region "Deployment"
    Private Sub PreInstallSteps()
        If gUnattendedInstall Then
            NextGroupBox()
        End If
    End Sub

    Private Sub StartDeployment()
        oLicenseKeySettings = New LicenseKeySettings

        oRegistryChanger.Start()

        '---------------------------------------------------------
        oLogger.WriteToLog("Saving license key to registry")
        Try
            oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE, "LicenseKey", oLicense.LicenseCode)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\" & cREGKEY_LPCSOFTWARE, "LicenseKey", oLicense.LicenseCode)
        Catch ex As Exception
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        '---------------------------------------------------------
        oLogger.WriteToLog("Storing deployment config in registry")

        '###########
        Dim sDrivesToBeBlocked As String
        sDrivesToBeBlocked = GetDrivesToBeBlocked()

        oLogger.WriteToLog("blocked drives", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "BlockedDrives", sDrivesToBeBlocked)

        '---------------------------------------------------------
        oLogger.WriteToLog("settings", , 1)

        '###########
        oLogger.WriteToLog("region", , 1)
        oLicenseKeySettings.ServerRegion = oLicense.ServerRegion
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "Region", oLicense.ServerRegion)

        '###########
        oLogger.WriteToLog("hotel information url", , 2)
        oLicenseKeySettings.HotelInformationUrl = oLicense.HotelInformationUrl
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "HotelInformationUrl", oLicense.HotelInformationUrl)

        '###########
        oLogger.WriteToLog("sitekiosk license", , 2)
        oLicenseKeySettings.SiteKioskLicenseName = oLicense.SiteKioskLicenseName
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseName", oLicense.SiteKioskLicenseName)

        oLicenseKeySettings.SiteKioskLicenseLicensee = oLicense.SiteKioskLicenseLicensee
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseLicensee", oLicense.SiteKioskLicenseLicensee)

        oLicenseKeySettings.SiteKioskLicenseSignature = oLicense.SiteKioskLicenseSignature
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseSignature", oLicense.SiteKioskLicenseSignature)


        '###########
        oLogger.WriteToLog("prepaid enabled", , 2)
        oLicenseKeySettings.PrepaidEnabled = oLicense.PrepaidEnabled
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "PrepaidEnabled", oLicense.PrepaidEnabled.ToString)

        '###########
        oLogger.WriteToLog("currency name", , 2)
        oLicenseKeySettings.CurrencyName = oLicense.CurrencyName
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CurrencyName", oLicense.CurrencyName)

        '###########
        oLogger.WriteToLog("currency code", , 2)
        oLicenseKeySettings.CurrencyCode = oLicense.CurrencyCode
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CurrencyCode", oLicense.CurrencyCode)

        '###########
        oLogger.WriteToLog("hourly rate", , 2)
        oLicenseKeySettings.HourlyRate = oLicense.HourlyRate
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "HourlyRate", oLicense.HourlyRate.ToString)

        '###########
        oLogger.WriteToLog("credit card enabled", , 2)
        oLicenseKeySettings.CreditCardEnabled = oLicense.CreditCardEnabled
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardEnabled", oLicense.CreditCardEnabled.ToString)

        '###########
        oLogger.WriteToLog("credit card vendor", , 2)
        oLicenseKeySettings.CreditCardVendorName = oLicense.CreditCardVendorName
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardVendorName", oLicense.CreditCardVendorName)

        oLicenseKeySettings.CreditCardVendorUser = oLicense.CreditCardVendorUser
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardVendorUser", oLicense.CreditCardVendorUser)

        oLicenseKeySettings.CreditCardVendorPass = oLicense.CreditCardVendorPass
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardVendorPass", oLicense.CreditCardVendorPass)

        oLicenseKeySettings.CreditCardVendorSize = oLicense.CreditCardVendorSize
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardVendorSize", oLicense.CreditCardVendorSize)

        '###########
        oLogger.WriteToLog("credit card amount - minimum", , 2)
        oLicenseKeySettings.CreditCardMin = oLicense.CreditCardMin
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardAmountMinimum", oLicense.CreditCardMin.ToString)

        '###########
        oLogger.WriteToLog("credit card amount - maximum", , 2)
        oLicenseKeySettings.CreditCardMax = oLicense.CreditCardMax
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardAmountMaximum", oLicense.CreditCardMax.ToString)

        '###########
        oLogger.WriteToLog("credit card amount - increment", , 2)
        oLicenseKeySettings.CreditCardStep = oLicense.CreditCardStep
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "CreditCardAmountIncrement", oLicense.CreditCardStep.ToString)

        '###########
        oLogger.WriteToLog("computer name - new", , 2)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "NewComputerName", oLicense.MachineName.ToString)

        '###########
        oLogger.WriteToLog("workgroup - new", , 2)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "NewWorkgroup", oLicense.Workgroup.ToString)

        '---------------------------------------------------------
        If gSkipSkype Then
            oLogger.WriteToLog("skip skype", , 1)
            oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Config", "SkipSkype", "yes")
        End If

        '---------------------------------------------------------
        oLogger.WriteToLog("Backing up some stuff")

        oLogger.WriteToLog("Computer name", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Backup", "Computer Name", oComputerName.ComputerName)

        '###########
        oLogger.WriteToLog("Workgroup name", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Backup", "Workgroup Name", oComputerName.Workgroup)

        '---------------------------------------------------------
        oLogger.WriteToLog("Updating default user profile")
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.Users, ".DEFAULT\Control Panel\PowerCfg", "CurrentPowerPolicy", 2)

        '---------------------------------------------------------
        oLogger.WriteToLog("Updating User Agent")
        oLogger.WriteToLog("User Agent", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "", "Mozilla/5.0")

        oLogger.WriteToLog("Version", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\5.0\User Agent", "Version", "MSIE 9.0")

        '---------------------------------------------------------
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V HonorAutoRunSetting  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveTypeAutoRun  /T REG_DWORD  /D 0x000000ff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoDriveAutoRun  /T REG_DWORD  /D 0x0003ffff  /F
        'reg.exe ADD "HKLM\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" /V NoAutoRun  /T REG_DWORD  /D 0x00000001  /F
        'reg.exe ADD "HKLM\SYSTEM\CurrentControlSet\Services\Cdrom" /V AutoRun  /T REG_DWORD  /D 0x00000000  /F
        'reg.exe ADD "HKLM\SOFTWARE\Microsoft\Windows NT\CurrentVersion\IniFileMapping\Autorun.inf" /VE      /T REG_SZ    /D "@SYS:DoesNotExist" /F

        oLogger.WriteToLog("Disabling AutoRun")
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER, "HonorAutoRunSetting", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER, "NoDriveTypeAutoRun", 255)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER, "NoDriveAutoRun", 262143)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER, "NoAutoRun", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_POLICIES_EXPLORER2, "NoAutoplayfornonVolume", 1)

        '---------------------------------------------------------
        oLogger.WriteToLog("Storing main installer version number")
        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE & "\Version", "Installer", myBuildInfo.FileVersion)

        '---------------------------------------------------------
        LogToProcessTextBox("Updating workgroup and computer name")
        oLogger.WriteToLog("Updating workgroup and computer name")
        oLogger.WriteToLog("workgroup   : " & oLicense.Workgroup, , 1)
        oLogger.WriteToLog("computername: " & oLicense.MachineName, , 1)
        If Not gTestMode Then
            oComputerName.Workgroup = oLicense.Workgroup
            oComputerName.ComputerName = oLicense.MachineName

            oLogger.WriteToLog("ok", , 1)
        Else
            oLogger.WriteToLog("SKIPPED, test mode", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        End If

        '---------------------------------------------------------
        UnpackResourcesInBackground()
    End Sub

    Private Sub PostDeployment()
        oLogger.WriteToLog("Post deployment steps")

        oLogger.WriteToLog("Set restarted to 'no'", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE, "Restarted", "no")

        oLogger.WriteToLog("Set PcHadRebooted to RunOnce", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce", "PcHasRebooted", oSettings.Path_PcHasRebooted, True)

        oLogger.WriteToLog("Set TaskbarAutohide to Run", 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, "SOFTWARE\Microsoft\Windows\CurrentVersion\Run", "TaskbarAutohide", oSettings.Path_TaskbarAutohide, True)


        oLogger.WriteToLog("Add to Programs list", , 1)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "DisplayIcon", oSettings.Path_Uninstaller)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "DisplayName", Application.ProductName)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "DisplayVersion", Application.ProductVersion)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "Publisher", Application.CompanyName)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "EstimatedSize", 200000)
        oRegistryChanger.UpdateKey(Microsoft.Win32.Registry.LocalMachine, cREGKEY_LPCSOFTWARE_UNINSTALL, "UninstallString", oSettings.Path_Uninstaller)


        'Copy C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp
        oLogger.WriteToLog("copy c:\Program Files\SiteKiosk\Bitmaps\iBAHN.bmp to C:\ProgramData\Microsoft\User Account Pictures\iBAHN.bmp", , 1)
        Try
            IO.File.Copy("c:\Program Files\SiteKiosk\Bitmaps\iBAHN.bmp", "C:\ProgramData\Microsoft\User Account Pictures\iBAHN.bmp")
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try

        If Not IO.File.Exists("C:\ProgramData\Microsoft\User Account Pictures\iBAHN.bmp") Then
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("Destination file does not exist!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            oLogger.WriteToLog("ok", , 2)
        End If


        'Copy some log files
        oLogger.WriteToLog("copy log files", , 1)
        oLogger.WriteToLog("copy " & cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder & "\unpacked\iBAHNUpdate\SiteKiosk7Installer.log to " & cPATHS_iBAHNProgramFilesFolder & "\SiteKiosk7Installer.log", , 2)
        Try
            IO.File.Copy( _
                cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesResourcesFolder & "\unpacked\iBAHNUpdate\SiteKiosk7Installer.log", _
                cPATHS_iBAHNProgramFilesFolder & "\SiteKiosk7Installer.log")
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        oLogger.WriteToLog("copy " & cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder & "\hotelpage.updater.log to " & cPATHS_iBAHNProgramFilesFolder & "\hotelpage.updater.log", , 2)
        Try
            IO.File.Copy( _
                cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder & "\hotelpage.updater.log", _
                cPATHS_iBAHNProgramFilesFolder & "\hotelpage.updater.log")
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        oLogger.WriteToLog("copy " & cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder & "\SkCfgUpdater.log to " & cPATHS_iBAHNProgramFilesFolder & "\SkCfgUpdater.log", , 2)
        Try
            IO.File.Copy( _
                cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder & "\SkCfgUpdater.log", _
                cPATHS_iBAHNProgramFilesFolder & "\SkCfgUpdater.log")
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try


        'Clean up temp install files
        oLogger.WriteToLog("clean up temp install files", , 1)
        Try
            IO.Directory.Delete(cPATHS_iBAHNProgramFilesFolder & cPATHS_iBAHNProgramFilesInstallationFolder, True)
            oLogger.WriteToLog("ok ", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("FAILED!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog("ex.Message = " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try


        oRegistryChanger.Done()
    End Sub

    Private Sub NextInstallationStep()
        tmrInstallerWait.Enabled = False

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub UnpackResourcesInBackground()
        LogToProcessTextBox("Unpacking resources")
        oLogger.WriteToLog("Unpacking resources")

        UpdateInstallationProgressCount()

        tmrUnpackWait.Enabled = True

        Me.installThread = New Thread(New ThreadStart(AddressOf UnpackResourcesModule.UnpackResources))
        Me.installThread.Start()
    End Sub

    Private Sub InstallationController()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner

        If iApplicationInstallationCount < iApplicationInstallationCountMax Then
            tmrInstallerWait.Enabled = True
        End If

        If gTestMode Then
            iApplicationInstallationCount = iApplicationInstallationCountMax
        End If

        oLogger.WriteToLog("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

        Select Case iApplicationInstallationCount
            Case InstallationOrder.IncrementLicense
                IncrementLicenseCode()
            Case InstallationOrder.LogMeIn
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLogMeIn))
                Me.installThread.Start()
            Case InstallationOrder.Altiris
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallAltiris))
                Me.installThread.Start()
            Case InstallationOrder.LobbyPCAgent
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLobbyPCAgent))
                Me.installThread.Start()
            Case InstallationOrder.LobbyPCWatchdog
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallLobbyPCWatchdog))
                Me.installThread.Start()
            Case InstallationOrder.CreateShortcut
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallCreateShortcut))
                Me.installThread.Start()
            Case InstallationOrder.SiteKiosk
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSiteKiosk))
                Me.installThread.Start()
            Case InstallationOrder.iBAHNUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstalliBAHNUpdate))
                Me.installThread.Start()
            Case InstallationOrder.NewUI
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallNewUI))
                Me.installThread.Start()
            Case InstallationOrder.SkCfgUpdaterSiteCash
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallSkCfgUpdaterSiteCash))
                Me.installThread.Start()
            Case InstallationOrder.VeriSignUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallVeriSignUpdate))
                Me.installThread.Start()
            Case InstallationOrder.HotelPageUpdate
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallHotelPageUpdate))
                Me.installThread.Start()
            Case InstallationOrder.Shortcuts
                Me.installThread = New Thread(New ThreadStart(AddressOf Me._InstallShortCuts))
                Me.installThread.Start()
            Case InstallationOrder.SiteKioskSecurityUpdater
                oLogger.WriteToLog("SiteKioskSecurityUpdater skipped", Logger.MESSAGE_TYPE.LOG_WARNING)

                oProcess.ProcessDone()
                'Me.installThread = New Thread(New ThreadStart(AddressOf Me._SiteKioskSecurity))
                'Me.installThread.Start()
            Case InstallationOrder.PostDeployment
                PostDeployment()

                oProcess.ProcessDone()
            Case Else
                oLogger.WriteToLog("DONE", , 0)

                gBusyInstalling = False

                NextGroupBox()
        End Select
    End Sub
#End Region

#Region "Installers"
    Private Sub _InstallLogMeIn()
        Dim sParams As String = ""

        sParams &= "-spdeployid="

        If oSettings.Path_HiltonUI <> "" Then
            sParams &= cLOGMEIN_DEPLOY_ID__HILTON
        Else
            sParams &= cLOGMEIN_DEPLOY_ID__DEFAULT
        End If

        ' LogMeIn.deploy.exe -spdeployid=01_rt1b7rkbgp37385pqrdp4xtdbbo5z907p625u

        oLogger.WriteToLog("Installing remote access application")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LogMeIn, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LogMeIn
        oProcess.Arguments = sParams
        oProcess.ExternalAppToWaitFor = "LogMeIn_Installer"
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLobbyPCAgent()
        oLogger.WriteToLog("Installing iBAHN service")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LobbyPCAgent, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCAgent
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "LobbyPCAgentPatchInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallLobbyPCWatchdog()
        oLogger.WriteToLog("Installing iBAHN security service")
        oLogger.WriteToLog("Path   : " & oSettings.Path_LobbyPCWatchdog, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_LobbyPCWatchdog
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "LobbyPCWatchDogInstaller"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstalliBAHNUpdate()
        oLogger.WriteToLog("Installing iBAHN files")
        oLogger.WriteToLog("Path   : " & oSettings.Path_iBAHNUpdate, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_iBAHNUpdate
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "SiteKiosk7Updater"
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallNewUI()
        If oSettings.Path_iBAHNUI <> "" Then
            oLogger.WriteToLog("Installing iBAHN files")
            oLogger.WriteToLog("Path   : " & oSettings.Path_iBAHNUI, , 1)
            oLogger.WriteToLog("Params : ", , 1)
            oLogger.WriteToLog("Timeout: 300", , 1)

            oProcess = New ProcessRunner
            oProcess.FileName = oSettings.Path_iBAHNUI
            oProcess.Arguments = ""
            oProcess.ExternalAppToWaitFor = "NewUI_iBAHN__Installer"
            oProcess.MaxTimeout = 300
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()
        Else
            oLogger.WriteToLog("Installing Hilton files")
            oLogger.WriteToLog("Path   : " & oSettings.Path_HiltonUI, , 1)
            oLogger.WriteToLog("Params : ", , 1)
            oLogger.WriteToLog("Timeout: 300", , 1)

            oProcess = New ProcessRunner
            oProcess.FileName = oSettings.Path_HiltonUI
            oProcess.Arguments = ""
            oProcess.ExternalAppToWaitFor = "NewUI_Hilton__Installer"
            oProcess.MaxTimeout = 300
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()
        End If
    End Sub

    Private Sub _InstallSkCfgUpdaterSiteCash()
        Dim sParams As String = ""

        If oLicenseKeySettings.PrepaidEnabled Then
            sParams &= "--enable-pp"
        Else
            sParams &= "--disable-pp"
        End If
        sParams &= " "

        sParams &= "--currency:" & oLicenseKeySettings.CurrencyCode
        sParams &= " "

        sParams &= "--hourly-rate:" & oLicenseKeySettings.HourlyRate
        sParams &= " "

        If oLicenseKeySettings.CreditCardEnabled Then
            sParams &= "--enable-cc"
        Else
            sParams &= "--disable-cc"
        End If
        sParams &= " "

        sParams &= "--min-amount-cc:" & oLicenseKeySettings.CreditCardMin
        sParams &= " "

        sParams &= "--max-amount-cc:" & oLicenseKeySettings.CreditCardMax
        sParams &= " "

        sParams &= "--increment-amount-cc:" & oLicenseKeySettings.CreditCardStep

        oLogger.WriteToLog("Installing SkCfgUpdaterSiteCash")
        oLogger.WriteToLog("Path   : " & oSettings.Path_SkCfgUpdaterSiteCash, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 30", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SkCfgUpdaterSiteCash
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 30
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallHotelPageUpdate()
        Dim sParams As String = ""

        sParams &= "--quiet:" & oLicenseKeySettings.HotelInformationUrl

        oLogger.WriteToLog("Installing HotelPageUpdate")
        oLogger.WriteToLog("Path   : " & oSettings.Path_HotelPageUpdater, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 30", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_HotelPageUpdater
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 30
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallVeriSignUpdate()
        Dim sParams As String = ""

        sParams &= "--from-registry"

        oLogger.WriteToLog("Installing VeriSign/PayFlow data")
        oLogger.WriteToLog("Path   : " & oSettings.Path_VeriSign, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 30", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_VeriSign
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 30
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallCreateShortcut()
        oLogger.WriteToLog("Installing shortcut creator")
        oLogger.WriteToLog("Path   : " & oSettings.Path_CreateShortcut, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 30", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_CreateShortcut
        If gTestMode Then
            oProcess.FileName = "c:\windows\notepad.exe"
        End If
        oProcess.Arguments = ""
        oProcess.MaxTimeout = 30
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        If gTestMode Then
            oProcess.ProcessStyle = ProcessWindowStyle.Normal
        End If
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallShortCuts()
        Dim sPath As String
        Dim sTemplate As String
        Dim sShortcut1 As String, sShortcut2 As String, sShortcut3 As String

        'oLogger.WriteToLog(oSettings.Path_CreateShortcut, Logger.MESSAGE_TYPE.LOG_DEBUG)

        'sPath = IO.Path.GetDirectoryName(oSettings.Path_CreateShortcut) & "\shortcuts.template.txt"
        sPath = cPATHS_iBAHNProgramFilesFolder & "\CreateShortcut"
        'sPath = IO.Path.GetDirectoryName(oSettings.Path_CreateShortcut) & "\CreateShortcut"

        'iBAHN\LobbyPCSoftware\CreateShortcut
        Try
            oLogger.WriteToLog("Preparing shortcut creator")

            Dim sr As New IO.StreamReader(sPath & "\shortcuts.template.txt")
            sTemplate = sr.ReadLine()

            sr.Close()

            sShortcut1 = sTemplate

            sShortcut1 = sShortcut1.Replace("%%NAME%%", "Start LobbyPC")
            sShortcut1 = sShortcut1.Replace("%%DESTINATION_FOLDER%%", GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory))
            sShortcut1 = sShortcut1.Replace("%%TARGET_PATH%%", oSettings.Path_AutoStart)
            sShortcut1 = sShortcut1.Replace("%%ARGUMENTS%%", "")
            sShortcut1 = sShortcut1.Replace("%%TARGET_WORKING_DIR%%", IO.Path.GetDirectoryName(oSettings.Path_AutoStart))
            sShortcut1 = sShortcut1.Replace("%%ICON_FILE%%", oSettings.Path_AutoStart)
            sShortcut1 = sShortcut1.Replace("%%ICON_INDEX%%", "0")

            sShortcut2 = sTemplate
            sShortcut2 = sShortcut2.Replace("%%NAME%%", "Start LobbyPC")
            sShortcut2 = sShortcut2.Replace("%%DESTINATION_FOLDER%%", GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu))
            sShortcut2 = sShortcut2.Replace("%%TARGET_PATH%%", oSettings.Path_AutoStart)
            sShortcut2 = sShortcut2.Replace("%%ARGUMENTS%%", "")
            sShortcut2 = sShortcut2.Replace("%%TARGET_WORKING_DIR%%", IO.Path.GetDirectoryName(oSettings.Path_AutoStart))
            sShortcut2 = sShortcut2.Replace("%%ICON_FILE%%", oSettings.Path_AutoStart)
            sShortcut2 = sShortcut2.Replace("%%ICON_INDEX%%", "0")

            sShortcut3 = sTemplate
            sShortcut3 = sShortcut3.Replace("%%NAME%%", "LobbyPC - Set Hotel Information Url")
            sShortcut3 = sShortcut3.Replace("%%DESTINATION_FOLDER%%", GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory))
            sShortcut3 = sShortcut3.Replace("%%TARGET_PATH%%", oSettings.Path_HotelPageUpdater)
            sShortcut3 = sShortcut3.Replace("%%ARGUMENTS%%", "")
            sShortcut3 = sShortcut3.Replace("%%TARGET_WORKING_DIR%%", IO.Path.GetDirectoryName(oSettings.Path_HotelPageUpdater))
            sShortcut3 = sShortcut3.Replace("%%ICON_FILE%%", oSettings.Path_HotelPageUpdater)
            sShortcut3 = sShortcut3.Replace("%%ICON_INDEX%%", "0")


            Dim sw As New IO.StreamWriter(sPath & "\shortcuts.txt", False)
            sw.WriteLine(sShortcut1)
            sw.WriteLine(sShortcut2)
            sw.WriteLine(sShortcut3)
            sw.Close()

            oLogger.WriteToLog("Installing shortcut creator")
            oLogger.WriteToLog("Path   : " & sPath & "\CreateShortcut.exe", , 1)
            oLogger.WriteToLog("Timeout: 30", , 1)

            oProcess = New ProcessRunner
            oProcess.FileName = sPath & "\CreateShortcut.exe"
            oProcess.Arguments = ""
            oProcess.MaxTimeout = 30
            oProcess.ProcessStyle = ProcessWindowStyle.Hidden
            oProcess.StartProcess()

        Catch ex As Exception
            oLogger.WriteToLog("CreateShortcut FAILED", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            oProcess.ProcessDone()
        End Try

    End Sub

    Private Sub _InstallAltiris()
        oLogger.WriteToLog("Installing remote administration application")
        oLogger.WriteToLog("Path   : " & oSettings.Path_Altiris, , 1)
        oLogger.WriteToLog("Params : ", , 1)
        oLogger.WriteToLog("Timeout: 600", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_Altiris
        oProcess.Arguments = ""
        oProcess.ExternalAppToWaitFor = "AeXNSC_Installer"
        oProcess.MaxTimeout = 600
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _InstallSiteKiosk()
        oLogger.WriteToLog("Installing kiosk software")
        oLogger.WriteToLog("Path   : " & oSettings.Path_SiteKiosk, , 1)
        oLogger.WriteToLog("Params : /S /V""/qn""", , 1)
        oLogger.WriteToLog("Timeout: 1200", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = oSettings.Path_SiteKiosk
        oProcess.Arguments = "/S /V""/qn"""
        oProcess.MaxTimeout = 1200
        oProcess.ProcessStyle = ProcessWindowStyle.Normal
        oProcess.StartProcess()
    End Sub
#End Region
#End Region

#Region "Progress bars"
    Private Sub UpdateInstallationProgressBar()
        If iApplicationInstallationCount <= iApplicationInstallationCountMax Then
            UpdateInstallationProgressCount()
            progressInstallation.Value = (100 / iApplicationInstallationCountMax) * iApplicationInstallationCount

            Try
                TaskbarManager.Instance.SetProgressValue(progressInstallation.Value, progressInstallation.Maximum)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub UpdateInstallationProgressCount()
        lblInstallationProgressStep.Text = (iApplicationInstallationCount + 1).ToString & " / " & (iApplicationInstallationCountMax + 1).ToString
        lblInstallationProgressStep.Visible = True
    End Sub


    Private Sub ProgressBarMarqueeStart()
        progressMarquee.Style = ProgressBarStyle.Marquee
        progressMarquee.MarqueeAnimationSpeed = 100
        progressMarquee.Value = 0
    End Sub

    Private Sub ProgressBarMarqueeStop()
        progressMarquee.Style = ProgressBarStyle.Blocks
        progressMarquee.MarqueeAnimationSpeed = 0
        progressMarquee.Value = 0
    End Sub
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationIsCanceled()
        Application.Exit()
        'gBusyInstalling = False

        'MsgBox("Installation cancelled by user", MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation, Application.ProductName)
    End Sub

    Private Sub ApplicationIsExited(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("Exiting application")

        If Not gUnattendedInstall Then
            If radRestartNow.Checked And iActiveGroupBox = GroupBoxes.DoneAndRestart Then
                oLogger.WriteToLog("restarting", , 1)

                If gTestMode Then
                    oLogger.WriteToLog("test mode, so not restarting after all", , 2)
                    MsgBox("I would have restarted here!", MsgBoxStyle.OkOnly Or MsgBoxStyle.Information, Application.ProductName)
                Else
                    oLogger.WriteToLog("bye bye", , 2)
                    WindowsController.ExitWindows(RestartOptions.Reboot, True)
                End If
            End If
        End If

        oLogger.WriteToLog("exit code: " & iExitCode.ToString, , 1)
        Environment.ExitCode = iExitCode
        Application.Exit()
    End Sub
#End Region
#End Region

#Region "Events"
#Region "DragWindow events"
    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picboxLogo.MouseDown
        Me.picboxLogo.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub lblTitle_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseDown
        Me.lblTitle.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        LogToProcessTextBox(vbTab & "Done - Time elapsed: " & TimeElapsed.ToString)
        oLogger.WriteToLog("done", , 1)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        LogToProcessTextBox(vbTab & "Process failed")
        oLogger.WriteToLog("failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        LogToProcessTextBox(vbTab & "Process started")
        oLogger.WriteToLog("started", , 1)
        oLogger.WriteToLog("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        LogToProcessTextBox(vbTab & "Process timed out")
        oLogger.WriteToLog("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
#End Region

#Region "Button Events"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ApplicationIsCanceled()
    End Sub

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        NextGroupBox()
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        PreviousGroupBox()
    End Sub

    Private Sub btnClearLicense_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearLicense.Click
        txtLicenseCode.Text = ""
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        ApplicationIsExited()
    End Sub
#End Region

#Region "Timer Events"
    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            If oProcess.ErrorsOccurred And Not gTestMode Then
                gBusyInstalling = False

                GotoGroupBox(GroupBoxes.ErrorDuringInstallation)
            Else
                iApplicationInstallationCount += 1
                InstallationController()

                UpdateInstallationProgressBar()
            End If
        End If
    End Sub

    Private Sub tmrUnpackWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrUnpackWait.Tick
        If Not Me.installThread.ThreadState = ThreadState.Running And Not Me.installThread.ThreadState = ThreadState.Unstarted Then
            tmrUnpackWait.Enabled = False

            iApplicationInstallationCount = 1
            UpdateInstallationProgressBar()

            InstallationController()
        End If
    End Sub

    Private Sub tmrLicenseValidationDelay_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrLicenseValidationDelay.Tick
        tmrLicenseValidationDelay.Enabled = False

        CheckLicenseCode()
    End Sub
#End Region

#Region "CheckBox Events"
    Private Sub chkTCAgree_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTCAgree.CheckedChanged
        btnContinue.Enabled = chkTCAgree.Checked
    End Sub
#End Region

#Region "RadioButton Events"
    Private Sub radRestartNow_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radRestartNow.CheckedChanged
        'UpdateButtons(0, 0, 0, 3)
    End Sub

    Private Sub radRestartLater_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radRestartLater.CheckedChanged
        'UpdateButtons(0, 0, 0, 2)
    End Sub
#End Region

#Region "ComboBox Events"
    Private Sub cmbBoxLanguages_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbBoxLanguages.SelectedIndexChanged
        oLocalization.SetCurrentLanguage = cmbBoxLanguages.SelectedIndex + 1

        ReloadLocalization()
    End Sub
#End Region

#Region "Textbox Events"
    Private Sub txtLicenseCode_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLicenseCode.TextChanged
        grpboxLicenseError.Visible = False

        If txtLicenseCode.Text.Length = cLICENSE_KeyLength Then
            UpdateButtons(3, 0, 3, 0)
        Else
            UpdateButtons(3, 0, 2, 0)
        End If
    End Sub
#End Region

#Region "Paint Events"
    Private Sub pnlBackgroundBorder_Paint(ByVal sender As System.Object, ByVal pe As System.Windows.Forms.PaintEventArgs) Handles pnlBackgroundBorder.Paint
        If cGUI_GroupBoxBorder_visible Then
            pe.Graphics.DrawRectangle(cGUI_GroupBoxBorder_color, pe.ClipRectangle.Left, pe.ClipRectangle.Top, pe.ClipRectangle.Width - 1, pe.ClipRectangle.Height - 1)
        End If
    End Sub
#End Region

#Region "PicBox Events"
    Private Sub picClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ApplicationIsCanceled()
    End Sub
#End Region

#Region "License Events"
    Private Sub oLicense_LicenseIncremented() Handles oLicense.LicenseIncremented
        oLogger.WriteToLog("done", , 2)

        iApplicationInstallationCount += 1
        UpdateInstallationProgressBar()

        InstallationController()
    End Sub

    Private Sub oLicense_LicenseNotIncremented(ByVal ReturnCode As Integer, ByVal ReturnMessage As String) Handles oLicense.LicenseNotIncremented
        oLogger.WriteToLog("invalid", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        oLogger.WriteToLog(ReturnCode, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        oLogger.WriteToLog(ReturnMessage, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

        iApplicationInstallationCount += 1
        UpdateInstallationProgressBar()

        InstallationController()
    End Sub

    Private Sub onLicenseIsInvalid(ByVal ReturnCode As Integer, ByVal ReturnMessage As String) Handles oLicense.LicenseIsInvalid
        Dim sError As String = ""

        oLogger.WriteToLog("invalid", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        oLogger.WriteToLog(ReturnCode, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        oLogger.WriteToLog(ReturnMessage, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        If gTestMode Then
            oLogger.WriteToLog(oLicense.RawServerResponse, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        sError = oLocalization.CurrentLanguage.LicenseValidationProgressError
        Select Case ReturnCode
            Case 1
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage1
            Case 2
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage2
            Case 3
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage3
            Case -1
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage4
            Case -2
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage5
            Case -12
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessage6
            Case Else
                sError &= oLocalization.CurrentLanguage.LicenseValidationErrorMessageUnknown
        End Select

        lblLicenseError.Text = sError
        grpboxLicenseError.Visible = True
        txtLicenseCode.SelectAll()

        If gUnattendedInstall Then
            ApplicationIsExited(-1)
        Else
            PreviousGroupBox()
        End If
    End Sub

    Private Sub onLicenseValid() Handles oLicense.LicenseIsValid
        oLogger.WriteToLog("valid", , 2)

        If gTestMode Then
            oLogger.WriteToLog(oLicense.RawServerResponse, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        'Go to next step
        NextGroupBox()
    End Sub


#End Region
#End Region
End Class
