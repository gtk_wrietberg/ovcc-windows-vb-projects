﻿Imports Microsoft.Win32

Public Class Helpers
    Public Class Prerequisites
        Public Enum VALUES
            DOT_NET = 1
            IE = 2
            DOMAIN = 4
            PROFILE = 8
        End Enum

        Public Class DomainOrWorkgroup
            Public Shared Function IsOnDomain() As Boolean
                Try
                    Return Not Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName.Equals(String.Empty)
                Catch ex As Exception
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                    Return False
                End Try
            End Function

            Public Shared Function GetDomainName() As String
                Dim sDomain As String = ""

                Try
                    sDomain = Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName
                Catch ex As Exception
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End Try

                Return sDomain
            End Function
        End Class

        Public Class IE
            Public Enum IEVersions As Integer 'useless ENUM
                NONE = 0
                IE5 = 5
                IE6 = 6
                IE7 = 7
                IE8 = 8
                IE9 = 9
                IE10 = 10
                IE11 = 11
                IE12 = 12
            End Enum
            Public Shared Function IsRunningVersion(version As IEVersions) As Boolean
                Return (version <= _GetIEFromRegistry())
            End Function

            Private Shared Function _GetIEFromRegistry() As IEVersions
                Dim iVersion As Integer = IEVersions.NONE

                Try
                    Using ieKey As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Internet Explorer\")
                        If ieKey IsNot Nothing AndAlso ieKey.GetValue("svcVersion") IsNot Nothing Then
                            Dim sTmp As String = DirectCast(ieKey.GetValue("svcVersion", ""), String)

                            Try
                                iVersion = CInt(sTmp.Substring(0, sTmp.IndexOf(".")))
                            Catch ex As Exception

                            End Try
                        End If
                    End Using
                Catch ex As Exception

                End Try

                Return iVersion
            End Function
        End Class

        Public Class DotNet
            Public Enum DotNetVersions As Integer
                NONE = 0
                v2 = 1
                v3 = 2
                v4 = 4
                v4_5 = 8
                v4_5_1 = 16
                v4_5_2 = 32
                v4_6 = 64
                v4_6_1 = 128
                v4_6_2 = 256
            End Enum

            Public Shared Function IsRunningVersion(version As DotNetVersions) As Boolean
                Dim fullVersion As Integer = GetVersion()

                Return (version <= fullVersion)
            End Function

            Public Shared Function GetVersion() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                iVersion = iVersion Or _Get4orLowerFromRegistry()
                iVersion = iVersion Or _Get45or451FromRegistry()

                Return iVersion
            End Function

            Private Shared Function _Get4orLowerFromRegistry() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                Using ndpKey As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\")
                    For Each versionKeyName As String In ndpKey.GetSubKeyNames()
                        Try
                            Dim versionKey As RegistryKey = ndpKey.OpenSubKey(versionKeyName)
                            Dim name As String = DirectCast(versionKey.GetValue("Version", ""), String)
                            Dim sp As String = versionKey.GetValue("SP", "").ToString()
                            Dim install As String = versionKey.GetValue("Install", "").ToString()

                            If sp <> "" AndAlso install = "1" Then
                                Select Case True
                                    Case versionKeyName.StartsWith("v2")
                                        iVersion = iVersion Or DotNetVersions.v2
                                    Case versionKeyName.StartsWith("v3")
                                        iVersion = iVersion Or DotNetVersions.v3
                                    Case versionKeyName.StartsWith("v4.0")
                                        iVersion = iVersion Or DotNetVersions.v4
                                End Select
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                End Using

                Return iVersion
            End Function

            Private Shared Function _Get45or451FromRegistry() As Integer
                Dim iVersion As Integer = DotNetVersions.NONE

                Try
                    Using ndpKey As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\")
                        If ndpKey IsNot Nothing AndAlso ndpKey.GetValue("Release") IsNot Nothing Then
                            iVersion = _CheckFor45DotVersion(CInt(ndpKey.GetValue("Release")))
                        End If
                    End Using
                Catch ex As Exception

                End Try

                Return iVersion
            End Function

            Private Shared Function _CheckFor45DotVersion(releaseKey As Integer) As Integer
                If releaseKey >= 394747 Then
                    Return DotNetVersions.v4_6_2
                End If
                If releaseKey >= 394254 Then
                    Return DotNetVersions.v4_6_1
                End If
                If releaseKey >= 393295 Then
                    Return DotNetVersions.v4_6
                End If
                If releaseKey >= 379893 Then
                    Return DotNetVersions.v4_5_2
                End If
                If releaseKey >= 378675 Then
                    Return DotNetVersions.v4_5_1
                End If
                If releaseKey >= 378389 Then
                    Return DotNetVersions.v4_5
                End If

                ' This line should never execute. A non-null release key should mean
                ' that 4.5 or later is installed.
                Return DotNetVersions.NONE
            End Function
        End Class

        Public Class Profile
            Public Shared ReadOnly ProfileName As String = "SiteKiosk"

            Public Shared Function SiteKiostProfileExists() As Boolean
                Return SiteKioskUserExists() Or SiteKioskDirectoryExists()
            End Function

            Public Shared Function SiteKioskUserExists() As Boolean
                Return _FindUser(ProfileName)
            End Function

            Public Shared Function SiteKioskDirectoryExists() As Boolean
                Dim sDirs As String() = IO.Directory.GetDirectories("C:\Users", ProfileName & "*")

                Return sDirs.Length > 0
            End Function

            Private Shared Function _FindUser(ByVal sUsername As String, Optional ByVal sGroup As String = "Users") As Boolean
                Try
                    Dim oUser As Object, oGroup As Object

                    oGroup = GetObject("WinNT://" & Environment.MachineName & "/" & sGroup)

                    For Each oUser In oGroup.Members
                        Try
                            If Trim(sUsername.ToLower) = Trim(oUser.Name.ToString().ToLower) Then
                                Return True
                            End If
                        Catch ex As Exception
                            Return False
                        End Try
                    Next

                    oGroup = Nothing
                    oUser = Nothing

                    'Return True
                Catch ex As Exception
                    'Return False
                End Try

                Return False
            End Function
        End Class
    End Class

    Public Class FilesAndFolders
        Public Shared Function GetProgramFilesFolder(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sPath.Equals(String.Empty) Then
                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetProgramFilesFolder64bit(Optional WithSlashAtTheEnd As Boolean = False) As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)

            If WithSlashAtTheEnd Then
                If Not sPath.EndsWith("/") Then
                    sPath &= "/"
                End If
            Else
                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If
            End If

            Return sPath
        End Function

        Public Shared Function GetGuestTekFolder() As String
            Return IO.Path.Combine(GetProgramFilesFolder, "GuestTek")
        End Function

        Public Class Files
            Public Shared Function BackupFile(ByVal sFile As String, sBackupFolder As String) As Boolean
                Dim sPath As String, sName As String, sFullDest As String
                Dim oFile As System.IO.FileInfo, oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
                Dim bRet As Boolean = False

                Try
                    If Not IO.File.Exists(sFile) Then
                        Throw New Exception(sFile & " doesn´t exist")
                    End If

                    oFile = New System.IO.FileInfo(sFile)

                    If Not IO.Directory.Exists(sBackupFolder) Then
                        Throw New Exception("base backup folder '" & sBackupFolder & "' not found")
                    End If

                    sName = oFile.Name

                    sPath = oFile.DirectoryName
                    sPath = sPath.Replace("C:", "")
                    sPath = sPath.Replace("c:", "")
                    sPath = sPath.Replace("\\", "\")
                    sPath = sBackupFolder & "\" & sPath

                    sFullDest = sPath & "\" & sName

                    oDirInfo = New System.IO.DirectoryInfo(sPath)
                    If Not oDirInfo.Exists Then oDirInfo.Create()

                    oLogger.WriteToLogRelative("backup: " & sFullDest, , 1)

                    oFile.CopyTo(sFullDest, True)

                    oFileInfo = New System.IO.FileInfo(sFullDest)
                    If oFileInfo.Exists Then
                        oLogger.WriteToLogRelative("ok", , 2)
                        bRet = True
                    Else
                        oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    End If
                Catch ex As Exception
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End Try

                oFile = Nothing
                oDirInfo = Nothing
                oFileInfo = Nothing

                Return bRet
            End Function
        End Class
    End Class
End Class
