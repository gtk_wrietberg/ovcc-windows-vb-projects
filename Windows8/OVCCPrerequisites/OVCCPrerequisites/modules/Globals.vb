Module Globals
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupBaseDirectory As String
    Public g_BackupDirectory As String
    Public g_BackupDate As String

    Public g_TESTMODE As Boolean
    Public g_QUIET As Boolean
    Public g_MAININSTALLER As String = ""

    Public ReadOnly c_DEFAULTMAININSTALLER As String = "OVCCSoftwareInstaller.exe"

    Public ReadOnly c_PREREQUISITE__DOTNET As Helpers.Prerequisites.DotNet.DotNetVersions = Helpers.Prerequisites.DotNet.DotNetVersions.v4_5_2
    Public ReadOnly c_PREREQUISITE__IE As Helpers.Prerequisites.IE.IEVersions = Helpers.Prerequisites.IE.IEVersions.IE11

    Public ReadOnly c_LINKS_DOTNET As String = "https://www.microsoft.com/en-us/download/details.aspx?id=42642"
    Public ReadOnly c_LINKS_IE11 As String = "https://www.microsoft.com/en-us/download/Internet-Explorer-11-for-Windows-7-details.aspx"


    Public ReadOnly VersionNumber As New Version(33, 1, 1, 230803)
    Public ReadOnly VersionsDirectory As String = IO.Path.Combine(Helpers.FilesAndFolders.GetGuestTekFolder(), "_versions")


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        Dim sProgFiles As String =  Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)


        g_BackupDate = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        g_LogFileDirectory = sProgFiles & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupBaseDirectory = sProgFiles & "\GuestTek\_backups"
        g_BackupDirectory = g_BackupBaseDirectory & "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & g_BackupDate

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try

        g_QUIET = False
        g_MAININSTALLER = c_DEFAULTMAININSTALLER
    End Sub
End Module
