﻿Imports System.Text

Public Class frmMain
    Private mCommandLineArgs As String = ""

    Private mStartDelay As Integer = 5

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        '---------------------------------------------------------
        oLogger.WriteToLog("Preserve command line params", , 0)
        'Preserve command line to pass them to main installer
        'remove first element, we don't need to pass it.
        If Environment.GetCommandLineArgs().Length >= 2 Then
            Dim GetCommandLineArgs_(Environment.GetCommandLineArgs().Length - 1) As String
            Dim iArg As Integer = -1

            For Each sArg As String In Environment.GetCommandLineArgs()
                If iArg >= 0 Then
                    If sArg = "--prereq-continue-silently" Then
                        oLogger.WriteToLog("found: " & sArg, , 2)
                        g_QUIET = True

                        sArg = ""
                    End If
                    If sArg.StartsWith("--prereq-main-installer:") Then
                        oLogger.WriteToLog("found: " & sArg, , 2)

                        g_MAININSTALLER = sArg.Replace("--prereq-main-installer:", "")

                        sArg = ""
                    End If

                    GetCommandLineArgs_(iArg) = sArg
                End If

                iArg += 1
            Next

            mCommandLineArgs = String.Join(" ", GetCommandLineArgs_)
            oLogger.WriteToLog("params for main installer:", , 1)
            oLogger.WriteToLog(" " & mCommandLineArgs, , 2)
        Else
            oLogger.WriteToLog("none found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If


        '---------------------------------------------------------
        oLogger.WriteToLog("Prerequisite checks", , 0)
        Dim iStuffFound As Integer = 0
        Dim bCancelInstall As Boolean = False
        Dim sbWarningText As New StringBuilder

        oLogger.WriteToLog(c_PREREQUISITE__DOTNET.ToString, , 1)
        If Helpers.Prerequisites.DotNet.IsRunningVersion(c_PREREQUISITE__DOTNET) Then

            lblOK_DotNet.ForeColor = Color.Green
            lblOK_DotNet.Text = "OK"

            iStuffFound = iStuffFound Or Helpers.Prerequisites.VALUES.DOT_NET
        Else
            lblOK_DotNet.ForeColor = Color.Red
            lblOK_DotNet.Text = "NO"

            bCancelInstall = True
        End If
        oLogger.WriteToLog(lblOK_DotNet.Text, , 2)


        oLogger.WriteToLog(c_PREREQUISITE__IE.ToString, , 1)
        If Helpers.Prerequisites.IE.IsRunningVersion(c_PREREQUISITE__IE) Then
            lblOK_IE.ForeColor = Color.Green
            lblOK_IE.Text = "OK"

            iStuffFound = iStuffFound Or Helpers.Prerequisites.VALUES.IE
        Else
            lblOK_IE.ForeColor = Color.Red
            lblOK_IE.Text = "NO"

            bCancelInstall = True
        End If
        oLogger.WriteToLog(lblOK_IE.Text, , 2)


        oLogger.WriteToLog("Domain check", , 1)
        If Not Helpers.Prerequisites.DomainOrWorkgroup.IsOnDomain() Then
            'Not on domain, ok
            iStuffFound = iStuffFound Or Helpers.Prerequisites.VALUES.DOMAIN
        Else
            'On domain, we need to warn, see below
            bCancelInstall = True
        End If


        'oLogger.WriteToLog("Profile check", , 1)
        'If Not Helpers.Prerequisites.Profile.SiteKiostProfileExists() Then
        '    'ok
        '    iStuffFound = iStuffFound Or Helpers.Prerequisites.VALUES.PROFILE
        'Else
        '    'profile exists, show wharning and cancel installation
        '    bCancelInstall = True
        'End If
        iStuffFound = iStuffFound Or Helpers.Prerequisites.VALUES.PROFILE  'ignore this prerequisite, we allow update/reinstall now


        linklblError.Visible = False
        linklblError.Location = New Point(12345, 12345)

        lblWarningTitle.Visible = False
        lblWarning.Visible = False

        If iStuffFound = 15 Then
            'Everything is fine
            ToggleButtons(True, True)

            If g_QUIET Then
                StartInstaller()

                MeClose()
            Else
                tmrStart.Interval = 1
                tmrStart.Enabled = True
            End If
        Else
            'Everything is NOT fine
            ToggleButtons(True, True)

            linklblError.Links.Clear()

            If Not iStuffFound And Helpers.Prerequisites.VALUES.DOMAIN Then
                lblWarningTitle.Visible = True
                lblWarning.Visible = True

                sbWarningText.Append("This pc appears to be connected to the domain '%%DOMAINNAME%%'.")
                sbWarningText.Append(vbCrLf)
                sbWarningText.Append("Certain security policies carried over from the domain might interfere with the software installation.")
            Else
                'domain is ok, take it off
                iStuffFound -= Helpers.Prerequisites.VALUES.DOMAIN
            End If

            If Not iStuffFound And Helpers.Prerequisites.VALUES.PROFILE Then
                lblWarningTitle.Visible = True
                lblWarning.Visible = True

                If sbWarningText.Length > 0 Then
                    sbWarningText.Append(vbCrLf)
                    sbWarningText.Append(vbCrLf)
                End If
                sbWarningText.Append("This pc already has a '%%PROFILENAME%%' profile. This will interfere with the installation, and the profile needs to be removed.")
            Else
                'domain is ok, take it off
                iStuffFound -= Helpers.Prerequisites.VALUES.PROFILE
            End If

            lblWarning.Text = sbWarningText.ToString
            lblWarning.Text = lblWarning.Text.Replace("%%DOMAINNAME%%", Helpers.Prerequisites.DomainOrWorkgroup.GetDomainName())
            lblWarning.Text = lblWarning.Text.Replace("%%PROFILENAME%%", Helpers.Prerequisites.Profile.ProfileName)


            Select Case iStuffFound
                Case 0
                    'everything is missing!
                    linklblError.Visible = True
                    linklblError.Location = New Point(16, 121)

                    linklblError.Text = "Please install .NET 4.5.2 and Internet Explorer 11 and retry the installation."
                    linklblError.Links.Add(15, 10, c_LINKS_DOTNET)
                    linklblError.Links.Add(30, 20, c_LINKS_IE11)

                    ToggleButtons(True, False)
                Case 1
                    'IE is missing
                    linklblError.Visible = True
                    linklblError.Location = New Point(16, 121)

                    linklblError.Text = "Please install Internet Explorer 11 and retry the installation."
                    linklblError.Links.Add(15, 20, c_LINKS_IE11)

                    ToggleButtons(True, False)
                Case 2
                    'dotNet is missing
                    linklblError.Visible = True
                    linklblError.Location = New Point(16, 121)

                    linklblError.Text = "Please install .NET 4.5.2 and retry the installation."
                    linklblError.Links.Add(15, 10, c_LINKS_DOTNET)

                    ToggleButtons(True, False)
                Case 3
                    'mStartDelay = 10

                    'tmrStart.Interval = 1
                    'tmrStart.Enabled = True
                    ToggleButtons(True, True)
                Case Else

            End Select


            If bCancelInstall Then
                ToggleButtons(True, False)
            End If
        End If
    End Sub

    Private Sub ToggleButtons(bAbort As Boolean, bContinue As Boolean)
        btnAbort.Visible = bAbort
        btnAbort.Enabled = bAbort

        btnContinue.Visible = bContinue
        btnContinue.Enabled = bContinue
    End Sub

    Private Sub linklblError_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles linklblError.LinkClicked
        Dim target As String = CType(e.Link.LinkData, String)

        If (target IsNot Nothing) AndAlso (target.StartsWith("http://") Or target.StartsWith("https://")) Then
            System.Diagnostics.Process.Start(target)
        End If
    End Sub

    Private Sub tmrStart_Tick(sender As Object, e As EventArgs) Handles tmrStart.Tick
        tmrStart.Interval = 1000

        btnContinue.Text = "Continue (" & mStartDelay.ToString & ")"

        mStartDelay -= 1
        If mStartDelay < 0 Then
            tmrStart.Enabled = False


            StartInstaller()

            MeClose()
        End If
    End Sub

    Private Sub StartInstaller()
        ToggleButtons(False, False)

        Try
            If Not g_MAININSTALLER.EndsWith(".exe") Then
                g_MAININSTALLER = g_MAININSTALLER & ".exe"
            End If

            oLogger.WriteToLog("Writing version number",, 0)
            oLogger.WriteToLog(Globals.VersionNumber.ToString, , 1)
            IO.Directory.CreateDirectory(Globals.VersionsDirectory)
            IO.File.WriteAllText(IO.Path.Combine(Globals.VersionsDirectory, "main.txt"), Globals.VersionNumber.ToString)


            'g_MAININSTALLER = IO.Path.Combine(My.Application.Info.DirectoryPath, g_MAININSTALLER)


            oLogger.WriteToLog("Starting main installer", , 0)
            oLogger.WriteToLog(g_MAININSTALLER & " " & mCommandLineArgs, , 1)

            System.Diagnostics.Process.Start(g_MAININSTALLER, mCommandLineArgs)

            oLogger.WriteToLog("ok", , 1)
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog(ex.GetType().ToString & ": " & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            MsgBox("FATAL ERROR: Did not start installer '" & g_MAININSTALLER & "'! " & vbCrLf & vbCrLf & ex.GetType().ToString & ": " & ex.Message & vbCrLf & vbCrLf & "Please contact the helpdesk.", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Me.Text)
        End Try
    End Sub

    Private Sub MeClose()
        oLogger.WriteToLog("done", , 0)
        oLogger.WriteToLog("bye", , 1)

        Me.Close()
        Application.Exit()
        End
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        StartInstaller()

        MeClose()
    End Sub

    Private Sub btnAbort_Click(sender As Object, e As EventArgs) Handles btnAbort.Click
        MeClose()
    End Sub
End Class
