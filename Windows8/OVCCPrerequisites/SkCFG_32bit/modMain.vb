﻿Module modMain
    Private ReadOnly c_SEARCH_1 As String = "C:\Program Files\SiteKiosk"
    Private ReadOnly c_SEARCH_2 As String = "C:\Program Files (x86)\SiteKiosk"

    Private ReadOnly c_REPLACE As String = "%SiteKioskPath%"

    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        '---------------------------------------------------------

        oLogger.WriteToLogRelative("making SkCfg file 32bit compatible", , 0)

        Dim sSkCFG As String = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        Dim lines As New List(Of String)

        oLogger.WriteToLogRelative("reading SkCfg file", , 1)
        oLogger.WriteToLogRelative(sSkCFG, , 2)

        If Not IO.File.Exists(sSkCFG) Then
            oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        Else
            Try
                oLogger.WriteToLogRelative("making backup", , 1)
                Helpers.FilesAndFolders.Files.BackupFile(sSkCFG, g_BackupDirectory)


                oLogger.WriteToLogRelative("updating", , 1)

                Using sr As New IO.StreamReader(sSkCFG)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains(c_SEARCH_1) Then
                            oLogger.WriteToLogRelative("found line", , 2)
                            oLogger.WriteEmptyLineToLog()
                            oLogger.WriteToLogWithoutDate(line)
                            oLogger.WriteEmptyLineToLog()

                            line = line.Replace(c_SEARCH_1, c_REPLACE)

                            oLogger.WriteToLogRelative("replacing by", , 2)
                            oLogger.WriteEmptyLineToLog()
                            oLogger.WriteToLogWithoutDate(line)
                            oLogger.WriteEmptyLineToLog()
                        End If

                        If line.Contains(c_SEARCH_2) Then
                            oLogger.WriteToLogRelative("found line", , 2)
                            oLogger.WriteEmptyLineToLog()
                            oLogger.WriteToLogWithoutDate(line)
                            oLogger.WriteEmptyLineToLog()

                            line = line.Replace(c_SEARCH_2, c_REPLACE)

                            oLogger.WriteToLogRelative("replacing by", , 2)
                            oLogger.WriteEmptyLineToLog()
                            oLogger.WriteToLogWithoutDate(line)
                            oLogger.WriteEmptyLineToLog()
                        End If

                        lines.Add(line)
                    End While
                End Using

                oLogger.WriteToLogRelative("writing script file", , 1)
                Using sw As New IO.StreamWriter(sSkCFG)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using

                oLogger.WriteToLogRelative("ok", , 2)
            Catch ex As Exception
                oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If

        oLogger.WriteToLogRelative("done", , 0)
        oLogger.WriteToLogRelative("ok, bye", , 1)
    End Sub

End Module
