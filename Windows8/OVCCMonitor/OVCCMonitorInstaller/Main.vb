﻿Imports System.ComponentModel.Design
Imports System.Globalization

Module Main
    Public Sub Main()
        'Forcing the service to use en-GB
        Dim culture_enGB As New CultureInfo("en-GB")
        CultureInfo.DefaultThreadCurrentCulture = culture_enGB



        Helpers.Logger.InitialiseLogger()


        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.Write("Installer started")


        Dim sAppPath As String = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\GuestTek\OVCCMonitor\OVCCMonitorService.exe"


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Stop service", 0)

        If ServiceInstaller.ServiceIsInstalled(InstallerHelper.Service.ServiceName) Then
            Helpers.Logger.WriteMessage("stopping", 1)

            ServiceInstaller.StopService(InstallerHelper.Service.ServiceName)

            Threading.Thread.Sleep(5000)
        Else
            Helpers.Logger.WriteMessage("not installed yet", 1)
        End If


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Removing current monitor identifier", 0)

        If Not IO.File.Exists(Constants.Paths.File.MonitorIdentifier) Then
            Helpers.Logger.WriteMessage("not needed", 1)
        Else
            Try
                IO.File.Delete(Constants.Paths.File.MonitorIdentifier)

                Helpers.Logger.WriteMessage("ok", 1)
            Catch ex As Exception
                Helpers.Logger.WriteError("delete failed!", 1)
                Helpers.Logger.WriteError(ex.Message, 2)
            End Try
        End If


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Copy files", 0)


        Dim sBackupDirectory As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\GuestTek\_backups"
        sBackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        sBackupDirectory &= "\" & Now().ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles = New Helpers.FilesAndFolders.CopyFiles
        oCopyFiles.BackupDirectory = sBackupDirectory
        oCopyFiles.SourceDirectory = IO.Path.Combine(My.Application.Info.DirectoryPath, "files")
        oCopyFiles.DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder() & "\"

        oCopyFiles.CopyFiles()


        '==================================================================================================================================================================================
        Helpers.Logger.Write("Create registry keys", , 0)

        Helpers.Logger.Write(InstallerHelper.RegistryKeys.REGKEY__Monitor, , 1)
        Helpers.Logger.Write(InstallerHelper.RegistryKeys.REGKEY__Monitor_Configuration, , 2)


        InstallerHelper.RegistryKeys.Application.Reset()
        InstallerHelper.RegistryKeys.Application.Add("ThreadDelay_Heartbeat", 0)
        InstallerHelper.RegistryKeys.Application.Add("ThreadLoopDelay_Heartbeat", 120)
        InstallerHelper.RegistryKeys.Application.Add("FilePath_SiteKiosk", "%%PROGRAMFILES32%%\\Sitekiosk\\Sitekiosk.exe")
        InstallerHelper.RegistryKeys.Application.Add("FilePath_Watchdog", "%%PROGRAMFILES32%%\\GuestTek\\OVCCWatchdog\\OVCCWatchdog_service.exe")
        InstallerHelper.RegistryKeys.Application.Add("FilePath_FileExplorer", "%%PROGRAMFILES32%%\\GuestTek\\FileExplorer\\FileExplorer.exe")
        InstallerHelper.RegistryKeys.Application.Add("LastMetricsSyncDelay", 43200)
        InstallerHelper.RegistryKeys.Application.Add("LastIntegritySyncDelay", 3600)
        InstallerHelper.RegistryKeys.Application.Add("LastInventorySyncDelay", 600)
        InstallerHelper.RegistryKeys.Application.Add("Debug", CBool(False))
        InstallerHelper.RegistryKeys.Application.Add("MachineName", "")
        InstallerHelper.RegistryKeys.Application.Add("LastHeartbeat", CLng(0))
        InstallerHelper.RegistryKeys.Application.Add("LastApplicationSettingsSync", CLng(0))
        InstallerHelper.RegistryKeys.Application.Add("LastOVCCSettingsSync", CLng(0))
        InstallerHelper.RegistryKeys.Application.Add("LastIntegritySync", CLng(0))
        InstallerHelper.RegistryKeys.Application.Add("LastInventorySync", CLng(0))
        InstallerHelper.RegistryKeys.Application.Add("LastMetricsSync", CLng(0))
        InstallerHelper.RegistryKeys.Application.Add("LastApplicationSettingsSyncDelay", 0)
        InstallerHelper.RegistryKeys.Application.Add("LastOVCCSettingsSyncDelay", 0)
        InstallerHelper.RegistryKeys.Application.Add("LastOVCCThemesSync", CLng(0))
        InstallerHelper.RegistryKeys.Application.Add("LastOVCCThemesSyncDelay", 3600)
        InstallerHelper.RegistryKeys.Application.Add("DowntimeThresholdSeconds", 300)
        InstallerHelper.RegistryKeys.Application.Add("SendDetailedMetrics", CBool(False))
        InstallerHelper.RegistryKeys.Application.Add("TakeScreenshots", CBool(False))
        InstallerHelper.RegistryKeys.Application.Add("PropertyId", 0)
        InstallerHelper.RegistryKeys.Application.Save()


        InstallerHelper.RegistryKeys.Download.Reset()
        InstallerHelper.RegistryKeys.Download.Add("BaseUrl", "https://ccms-public.s3.amazonaws.com/OVCC-Monitor")
        InstallerHelper.RegistryKeys.Download.Add("Subdir_Themes", "Themes")
        InstallerHelper.RegistryKeys.Download.Add("Subdir_Screensavers", "Screensavers")
        InstallerHelper.RegistryKeys.Download.Add("Subdir_SelfUpdate", "SelfUpdate")
        InstallerHelper.RegistryKeys.Download.Add("Subdir_ExternalApps", "ExternalApps")
        InstallerHelper.RegistryKeys.Download.Save()


        InstallerHelper.RegistryKeys.Server.Reset()
        InstallerHelper.RegistryKeys.Server.Add("BaseUrl", "https://api-ovcc.guesttek.cloud/ovcc-monitor")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_Heartbeat", "heartbeat")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_ApplicationSettings", "appsettings")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_Integrity", "integrity")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_Inventory", "inventory")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_Metrics", "metrics")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_OVCCSettings", "ovccsettings")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_SelfUpdate", "selfupdate")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_OVCCThemesScreensavers", "ovccthemes")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_SendLocalOVCCSettings", "receivelocalovccsettings")
        InstallerHelper.RegistryKeys.Server.Add("EndPoint_Screenshots", "screenshots")
        InstallerHelper.RegistryKeys.Server.Save()


        '==================================================================================================================================================================================
        Helpers.Logger.WriteMessage("Start service", 0)

        If Not ServiceInstaller.ServiceIsInstalled(InstallerHelper.Service.ServiceName) Then
            ServiceInstaller.InstallService(InstallerHelper.Service.ServiceName, InstallerHelper.Service.DisplayName, sAppPath, ServiceBootFlag.AutoStart)
            Threading.Thread.Sleep(5000)
        End If


        Helpers.Logger.WriteMessage("updating registry", 1)


        Dim _service As New ServiceInstallerRegistry

        _service.ServiceName = InstallerHelper.Service.ServiceName
        _service.DelayedAutostart = 0
        _service.DisplayName = InstallerHelper.Service.DisplayName
        _service.ErrorControl = 1
        _service.ImagePath = """" & sAppPath & """"
        _service.ObjectName = "LocalSystem"
        _service.Start = 2
        _service.Type = 16
        _service.WOW64 = 1
        _service.SaveAll()


        Helpers.Logger.WriteMessage("starting", 1)
        If ServiceInstaller.StartService(InstallerHelper.Service.ServiceName) Then
            Helpers.Logger.WriteMessage("ok", 2)
        Else
            Helpers.Logger.WriteError("failed", 2)
            Helpers.Logger.WriteError(ServiceInstaller.LastError, 3)
        End If

        Threading.Thread.Sleep(5000)


        '==================================================================================================================================================================================
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("ok, bye", , 1)
    End Sub
End Module
