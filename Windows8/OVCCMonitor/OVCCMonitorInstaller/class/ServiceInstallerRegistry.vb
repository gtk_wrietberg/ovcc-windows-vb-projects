﻿Imports System.ServiceProcess

Public Class ServiceInstallerRegistry
#Region "props"
    Private mString_ServiceName As String = ""

    Private mInteger_DelayedAutostart As Integer = -1
    Private mString_DisplayName As String = ""
    Private mInteger_ErrorControl As Integer = -1
    Private mString_ImagePath As String = ""
    Private mString_ObjectName As String = ""
    Private mInteger_Start As Integer = -1
    Private mInteger_Type As Integer = -1
    Private mInteger_WOW64 As Integer = -1

    Public Property ServiceName As String
        Get
            Return mString_ServiceName
        End Get
        Set(value As String)
            mString_ServiceName = value
        End Set
    End Property

    Public Property DelayedAutostart As Integer
        Get
            Return mInteger_DelayedAutostart
        End Get
        Set(value As Integer)
            mInteger_DelayedAutostart = value
        End Set
    End Property

    Public Property DisplayName As String
        Get
            Return mString_DisplayName
        End Get
        Set(value As String)
            mString_DisplayName = value
        End Set
    End Property

    Public Property ErrorControl As Integer
        Get
            Return mInteger_ErrorControl
        End Get
        Set(value As Integer)
            mInteger_ErrorControl = value
        End Set
    End Property

    Public Property ImagePath As String
        Get
            Return mString_ImagePath
        End Get
        Set(value As String)
            mString_ImagePath = value
        End Set
    End Property

    Public Property ObjectName As String
        Get
            Return mString_ObjectName
        End Get
        Set(value As String)
            mString_ObjectName = value
        End Set
    End Property

    Public Property Start As Integer
        Get
            Return mInteger_Start
        End Get
        Set(value As Integer)
            mInteger_Start = value
        End Set
    End Property

    Public Property Type As Integer
        Get
            Return mInteger_Type
        End Get
        Set(value As Integer)
            mInteger_Type = value
        End Set
    End Property

    Public Property WOW64 As Integer
        Get
            Return mInteger_WOW64
        End Get
        Set(value As Integer)
            mInteger_WOW64 = value
        End Set
    End Property
#End Region

    Private ReadOnly mRegRoot_Services As String = "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services"

    Public Function SaveAll() As Boolean
        Try
            Dim sReg As String = mRegRoot_Services & "\" & mString_ServiceName

            Helpers.Registry64.CreateKey(sReg)
            Helpers.Registry64.SetValue_Integer(sReg, "DelayedAutostart", mInteger_DelayedAutostart)
            Helpers.Registry64.SetValue_String(sReg, "DisplayName", mString_DisplayName)
            Helpers.Registry64.SetValue_Integer(sReg, "ErrorControl", mInteger_ErrorControl)
            Helpers.Registry64.SetValue_ExpandString(sReg, "ImagePath", mString_ImagePath)
            Helpers.Registry64.SetValue_String(sReg, "ObjectName", mString_ObjectName)
            Helpers.Registry64.SetValue_Integer(sReg, "Start", mInteger_Start)
            Helpers.Registry64.SetValue_Integer(sReg, "Type", mInteger_Type)
            Helpers.Registry64.SetValue_Integer(sReg, "WOW64", mInteger_WOW64)

            Return True
        Catch ex As Exception

        End Try


        Return False
    End Function

    Public Function Save_StartType() As Boolean
        Try
            Dim sReg As String = mRegRoot_Services & "\" & mString_ServiceName

            Helpers.Registry64.CreateKey(sReg)
            Helpers.Registry64.SetValue_Integer(sReg, "Start", mInteger_Start)

            Return True
        Catch ex As Exception

        End Try

        Return False
    End Function
End Class
