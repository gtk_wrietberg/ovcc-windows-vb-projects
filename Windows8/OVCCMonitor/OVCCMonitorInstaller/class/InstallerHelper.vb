﻿Public Class InstallerHelper
    Public Class Installer
        Public Shared ReadOnly ServiceStatusCheckTimeoutSeconds As Integer = 120
    End Class

    Public Class Service
        Public Shared ServiceName As String = "OVCCMonitor"

        Public Shared DelayedAutostart As Integer = 0
        Public Shared DisplayName As String = "OVCC Monitor"
        Public Shared ErrorControl As Integer = 1
        Public Shared ImagePath As String = """%%APP_PATH%%"""
        Public Shared ObjectName As String = "LocalSystem"
        Public Shared Start As Integer = 2
        Public Shared Type As Integer = 16
        Public Shared WOW64 As Integer = 1
    End Class

    Public Class RegistryKeys
        Public Shared ReadOnly REGKEY__Monitor As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCMonitor"
        Public Shared ReadOnly REGKEY__Monitor_Configuration As String = REGKEY__Monitor & "\Configuration"

        Private Class _regValue
            Private mName As String
            Public Property Name() As String
                Get
                    Return mName
                End Get
                Set(ByVal value As String)
                    mName = value
                End Set
            End Property

            Private mValue As Object
            Public Property Value() As Object
                Get
                    Return mValue
                End Get
                Set(ByVal value As Object)
                    mValue = value
                End Set
            End Property

            Public Sub New(name As String, value As Object)
                mName = name
                mValue = value
            End Sub
        End Class

        Public Class Application
            Private Shared ReadOnly REGKEY As String = REGKEY__Monitor_Configuration & "\Application"
            Private Shared mList As New List(Of _regValue)

            Public Shared Sub Reset()
                mList = New List(Of _regValue)
            End Sub

            Public Shared Sub Add(name As String, value As Object)
                mList.Add(New _regValue(name, value))
            End Sub

            Public Shared Sub Save()
                Helpers.Logger.WriteRelative(REGKEY, , 1)

                For Each regValue As _regValue In mList
                    Helpers.Logger.WriteRelative(regValue.Name, , 2)
                    Helpers.Logger.WriteRelative(regValue.Value.ToString, , 2)

                    If TypeOf regValue.Value Is Integer Then
                        Helpers.Registry.SetValue_Integer(REGKEY, regValue.Name, regValue.Value)
                    ElseIf TypeOf regValue.Value Is Long Then
                        Helpers.Registry.SetValue_Long(REGKEY, regValue.Name, regValue.Value)
                    ElseIf TypeOf regValue.Value Is Boolean Then
                        Helpers.Registry.SetValue_Boolean(REGKEY, regValue.Name, regValue.Value)
                    Else
                        Helpers.Registry.SetValue_String(REGKEY, regValue.Name, regValue.Value)
                    End If
                Next
            End Sub
        End Class

        Public Class Server
            Private Shared ReadOnly REGKEY As String = REGKEY__Monitor_Configuration & "\Application\Server"
            Private Shared mList As New List(Of _regValue)

            Public Shared Sub Reset()
                mList = New List(Of _regValue)
            End Sub

            Public Shared Sub Add(name As String, value As Object)
                mList.Add(New _regValue(name, value))
            End Sub

            Public Shared Sub Save()
                Helpers.Logger.WriteRelative(REGKEY, , 1)

                For Each regValue As _regValue In mList
                    Helpers.Logger.WriteRelative(regValue.Name, , 2)
                    Helpers.Logger.WriteRelative(regValue.Value.ToString, , 2)

                    If TypeOf regValue.Value Is Integer Then
                        Helpers.Registry.SetValue_Integer(REGKEY, regValue.Name, regValue.Value)
                    ElseIf TypeOf regValue.Value Is Long Then
                        Helpers.Registry.SetValue_Long(REGKEY, regValue.Name, regValue.Value)
                    ElseIf TypeOf regValue.Value Is Boolean Then
                        Helpers.Registry.SetValue_Boolean(REGKEY, regValue.Name, regValue.Value)
                    Else
                        Dim sObfus As String = Helpers.XOrObfuscation_v2.Obfuscate(regValue.Value.ToString)
                        Helpers.Registry.SetValue_String(REGKEY, regValue.Name, sObfus)
                    End If
                Next
            End Sub
        End Class

        Public Class Download
            Private Shared ReadOnly REGKEY As String = REGKEY__Monitor_Configuration & "\Application\Download"
            Private Shared mList As New List(Of _regValue)

            Public Shared Sub Reset()
                mList = New List(Of _regValue)
            End Sub

            Public Shared Sub Add(name As String, value As Object)
                mList.Add(New _regValue(name, value))
            End Sub

            Public Shared Sub Save()
                Helpers.Logger.WriteRelative(REGKEY, , 1)

                For Each regValue As _regValue In mList
                    Helpers.Logger.WriteRelative(regValue.Name, , 2)
                    Helpers.Logger.WriteRelative(regValue.Value.ToString, , 2)

                    If TypeOf regValue.Value Is Integer Then
                        Helpers.Registry.SetValue_Integer(REGKEY, regValue.Name, regValue.Value)
                    ElseIf TypeOf regValue.Value Is Long Then
                        Helpers.Registry.SetValue_Long(REGKEY, regValue.Name, regValue.Value)
                    ElseIf TypeOf regValue.Value Is Boolean Then
                        Helpers.Registry.SetValue_Boolean(REGKEY, regValue.Name, regValue.Value)
                    Else
                        Dim sObfus As String = Helpers.XOrObfuscation_v2.Obfuscate(regValue.Value.ToString)
                        Helpers.Registry.SetValue_String(REGKEY, regValue.Name, sObfus)
                    End If
                Next
            End Sub
        End Class
    End Class
End Class
