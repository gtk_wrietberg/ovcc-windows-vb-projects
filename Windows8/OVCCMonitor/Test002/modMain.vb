﻿Imports System.Globalization

Module modMain
    Public Sub Main(args As String())
        Dim sTest_Uppercase As String = "InformationUpdater"
        Dim sTest_Lowercase As String = "informationupdater"


        Dim cultureCurrent As CultureInfo
        cultureCurrent = CultureInfo.CurrentCulture

        Console.WriteLine("Current culture: ")
        Console.WriteLine(cultureCurrent.Name)


        Console.WriteLine("ToLower() test")
        Console.WriteLine(sTest_Uppercase)
        Console.WriteLine(sTest_Uppercase.ToLower)
        If sTest_Uppercase.ToLower().Equals(sTest_Lowercase) Then
            Console.WriteLine("MATCH!!!")
        Else
            Console.WriteLine("NO MATCH!!!")
        End If


        Dim culture_enGB As CultureInfo = New CultureInfo("en-GB")
        CultureInfo.DefaultThreadCurrentCulture = culture_enGB

        cultureCurrent = CultureInfo.CurrentCulture

        Console.WriteLine("Current culture: ")
        Console.WriteLine(cultureCurrent.Name)


        Console.WriteLine("ToLower() test")
        Console.WriteLine(sTest_Uppercase)
        Console.WriteLine(sTest_Uppercase.ToLower)
        If sTest_Uppercase.ToLower().Equals(sTest_Lowercase) Then
            Console.WriteLine("MATCH!!!")
        Else
            Console.WriteLine("NO MATCH!!!")
        End If
    End Sub
End Module
