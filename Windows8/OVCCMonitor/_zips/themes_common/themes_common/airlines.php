<?php
include('/opt/OVCCMac/wwwroot/ovcc/inc/include.php');

$UI__airlines=OVCC_Settings::GetAirlines();

$cols=5;
$rows=ceil(count($UI__airlines)/$cols);

$alphabet=array();
$alphabet_links_arr=array();
for($i=0;$i<count($UI__airlines);$i++) {
    $first=strtoupper(substr($UI__airlines[$i]['name'],0,1));

    if(!in_array($first,$alphabet)) {
        $alphabet[]=$first;
        $alphabet_links_arr[]="<a href='#airline_".$first."'>".$first."</a>";
    }
}

$alphabet_links=join("&nbsp;|&nbsp;",$alphabet_links_arr);
?>
<div id="divAirlinesLinks">
    <?=$alphabet_links?>
</div>
<div id="divAirlinesContent">
    <table align="center">
        <?php
            for($row=0;$row<$rows;$row++) {
                echo "<tr>\n";

                for($col=0;$col<$cols;$col++) {
                    echo "<td width='250' align='center'>\n";

                    if((($row*$cols)+$col)>=count($UI__airlines)) {
                        echo "&nbsp;";
                    } else {
                        $UI__airline=$UI__airlines[($row*$cols)+$col];
                        $first=strtoupper(substr($UI__airline['name'],0,1));

                        if(in_array($first,$alphabet)) {
                            echo "<a id='airline_".$first."'></a>\n";
                            array_shift($alphabet);
                        }

                        echo "<p class='airline'><a class='airline' onclick='UI__browse_airline(\"".$UI__airline['url']."\")'>";
                        echo "<img src='/ovcc/core/themes_common/img/airlines/".$UI__airline['image']."' alt=''><br>";
                        echo $UI__airline['name'];
                        echo "</a></p>\n";
                    }
                    echo "</td>\n";
                }

                echo "</tr>\n";
            }
        ?>
    </table>
</div>
