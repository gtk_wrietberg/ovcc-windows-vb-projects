<?php
include('/opt/OVCCMac/wwwroot/ovcc/inc/include.php');

header('Content-Type: image/png');

$office_type=OVCC_Settings::GetHomepageSetting("office","none");

$theme=OVCC_Settings::GetSetting("theme_name","__default");
$theme_img_folder="/opt/OVCCMac/wwwroot/ovcc/themes/".$theme."/img";

if($office_type=="apple") {
    readfile($theme_img_folder.'/buttons/iWork.png');
} else {
    readfile($theme_img_folder.'/buttons/office.png');
}
?>