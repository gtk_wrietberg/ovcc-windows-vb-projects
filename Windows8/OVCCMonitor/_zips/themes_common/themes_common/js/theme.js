var AppSignal={
    EMPTY:0,
    LOGIN:1,
    LOGOUT:2,
    START_APP:3,
    ENABLE_TC_BUTTONS:4,
    DISABLE_TC_BUTTONS:5,
    TC_ACTIVITY:6,
    SHOW_TERMS:7,
    RELOAD_LANGUAGE:8,
    LOAD_URL:9,
    LOAD_SPECIAL_URL:10,
    HOMEPAGE_BUTTON_PRESSED:11,
    MESSAGE:444,
    WARNING:555,
    ERROR:666
};

function OVCC_JSON_EnableTCButtons() {
    return _OVCC_JSON_Message("",AppSignal.ENABLE_TC_BUTTONS,"","");
}

function OVCC_JSON_DisableTCButtons() {
    return _OVCC_JSON_Message("",AppSignal.DISABLE_TC_BUTTONS,"","");
}

function OVCC_JSON_Login() {
    return _OVCC_JSON_Message("",AppSignal.LOGIN,"","");
}

function OVCC_JSON_Logout() {
    return _OVCC_JSON_Message("",AppSignal.LOGOUT,"","");
}

function OVCC_JSON_TCActivity() {
    return _OVCC_JSON_Message("",AppSignal.TC_ACTIVITY,"","");
}

function OVCC_JSON_ReloadLanguage(lang_id) {
    return _OVCC_JSON_Message("language_id="+lang_id,AppSignal.RELOAD_LANGUAGE,"","");
}

function OVCC_JSON_ShowTerms() {
    return _OVCC_JSON_Message("",AppSignal.SHOW_TERMS,"","");
}

function OVCC_JSON_StartApplication(app_id) {
    return _OVCC_JSON_Message("app_id="+app_id,AppSignal.START_APP,"","");
}

function OVCC_JSON_LoadUrl(url) {
    return _OVCC_JSON_Message("url="+url,AppSignal.LOAD_URL,"","");
}

function OVCC_JSON_LoadSpecialUrl(url) {
    return _OVCC_JSON_Message("special_url="+url,AppSignal.LOAD_SPECIAL_URL,"","");
}

function OVCC_JSON_HomePageButton(btn) {
    return _OVCC_JSON_Message("button="+btn,AppSignal.HOMEPAGE_BUTTON_PRESSED,"","");
}

function OVCC_JSON_Message(message_text) {
    return _OVCC_JSON_Message(message_text,AppSignal.MESSAGE,"","");
}

function OVCC_JSON_Warning(message_text) {
    return _OVCC_JSON_Message(message_text,AppSignal.WARNING,"","");
}

function OVCC_JSON_Error(error_title,error_text) {
    return _OVCC_JSON_Message("",AppSignal.ERROR,error_title,error_text);
}

function _OVCC_JSON_Message(message_text,signal_code,error_title,error_text) {
	var obj={};

	obj.message={};
	if(typeof message_text !== "undefined") {
		obj.message.text=message_text;
	} else {
		obj.message.text="";
	}

	obj.signal={};
	if(typeof signal_code !== "undefined") {
		obj.signal.code=signal_code;
	} else {
		obj.signal.code=AppSignal.EMPTY;
	}

	obj.error={};
	if(typeof error_title !== "undefined") {
		obj.error.title=error_title;
	} else {
		obj.error.title="";
	}
    if(typeof error_text !== "undefined") {
		obj.error.text=error_text;
	} else {
		obj.error.text="";
	}


    try {
        window.webkit.messageHandlers.OVCCMac.postMessage(JSON.stringify(obj));

        return true;
    }
    catch(e) {}

    return false;
}

function CreateRandomNumber(min,max) {
    if(min>max) {
        tmp=min;
        min=max;
        max=tmp;
    }
    return min+Math.floor(Math.random()*max);
}

function CreateRandomHexNumber(length) {
    return _CreateRandomString(length,"ABCDEF0123456789");
}

function CreateRandomString(length) {
    return _CreateRandomString(length,"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");
}

function _CreateRandomString(length,characters) {
    var result='';

    if(characters.length==0) return "";

    var charactersLength=characters.length;
    for(var i=0;i<length;i++) result+=characters.charAt(Math.floor(Math.random()*charactersLength));

    return result;
}


// +============+
// | AJAX stuff |
// +============+
function AjaxRequest(url,callback_done,callback_fail) {
    var request=$.ajax({
        url:url,
        method:"POST",
        dataType:"json"
    });

    request.done(function(data) {
        callback_done(data);
    });

    request.fail(function(jqXHR,textStatus) {
        //alert( "Request failed: " + textStatus );
        callback_fail(textStatus);
    });
}

function AjaxRequestWithRetry(url,callback_done,callback_fail,callback_complete) {
    $.ajax({
        method:"POST",
        url:url,
        dataType:"json",
        shouldRetry: function( jqXHR, retryCount, requestMethod ) {
            console.log("AjaxRequestWithRetry #"+(retryCount+1));

            return retryCount<10;
        }
    })
    .done(function(data) {
        callback_done(data);
    })
    .fail(function(jqXHR,textStatus) {
        callback_fail(jqXHR,textStatus);
    })
    .always(function(jqXHR,textStatus) {
        callback_complete(jqXHR,textStatus);
    });
}

function OVCC__session_check() {
    AjaxRequest("/ovcc/api/json__session.php?action=get",
        function(data) {
            if(data.terms_agreed==1) {
                $("#logged_in").text("LOGGED IN!!!");
            } else {
                $("#logged_in").text("LOGGED OUT!!!");
            }
        },
        function(errorText) {
            OVCC_JSON_Error("failed to get session","api reports: "+errorText);
        });
}

function OVCC__logout() {
    OVCC_JSON_Logout();
}



function OVCC__random_error() {
    try {
        OVCC_JSON_Error("title "+CreateRandomString(6),CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20))+" "+CreateRandomString(CreateRandomNumber(5,20)));
    }
    catch(e) {}
}

function OVCC__change_language(id) {
    AjaxRequest("/ovcc/api/json__set_language_id.php?name=language_id&value="+id,
        function(data) {
            //success
            OVCC_JSON_ReloadLanguage(id);
        },
        function(errorText) {
            //failed
            OVCC_JSON_Error("language change failed","api reports: "+errorText);
        });
}


// +==========+
// | UI stuff |
// +==========+

var _UI__TermsAgreed=false;
var _UI__TermsActive=false;
var _UI__Action;
var _UI__ActionTimeout=60;
function UI__action(action,skip_terms) {
    if(skip_terms===undefined) skip_terms=false;

    OVCC_JSON_Message("UI__action('"+action+"')");

    _UI__Action=action;

    if(_UI__TermsAgreed||skip_terms) {
        UI__do_action();
    } else {
        if(OVCC_JSON_ShowTerms()) {
            _UI__ActionTimeout=60;
            setTimeout('UI__action_wait()',1000);
        } else {
            alert("Error while showing terms");
        }
    }
}

function UI__action_wait() {
    //OVCC_JSON_Message("UI__action_wait() - _UI__ActionTimeout="+_UI__ActionTimeout+" - _UI__TermsAgreed="+_UI__TermsAgreed+" - _UI__TermsActive="+_UI__TermsActive);
    _UI__ActionTimeout--;

    if(_UI__TermsAgreed) {
        UI__do_action();
    } else {
        if(_UI__TermsActive&&_UI__ActionTimeout>0) {
            setTimeout('UI__action_wait()',1000);
        }
    }
}

function UI__do_action() {
    OVCC_JSON_Message("UI__do_action('"+_UI__Action+"')");

    var action=string_split(_UI__Action,":",1);

    if(action.length!=2) return;

    switch(action[0]) {
        case "url":
            OVCC_JSON_LoadUrl(action[1]);

            break;
        case "special_url":
            OVCC_JSON_LoadSpecialUrl(action[1]);

            break;
        case "javascript":
            eval(action[1]);

            break;
        case "app":
            OVCC_JSON_StartApplication(action[1]);

            break;
        default:

    }
}

function string_split(str,delimiter,limit) {
    arr=str.split(delimiter);
    result=arr.splice(0,limit);
	result.push(arr.join(delimiter));

    return result;
}

function _UI__getWeekdayName(num) {
    switch(num) {
        case 0:
            return GetLanguageString(201);
        case 1:
            return GetLanguageString(202);
        case 2:
            return GetLanguageString(203);
        case 3:
            return GetLanguageString(204);
        case 4:
            return GetLanguageString(205);
        case 5:
            return GetLanguageString(206);
        case 6:
            return GetLanguageString(207);
        default:
            return "??day";
    }
}

function _UI__getMonthName(num) {
     switch(num) {
        case 0:
            return GetLanguageString(208);
        case 1:
            return GetLanguageString(209);
        case 2:
            return GetLanguageString(210);
        case 3:
            return GetLanguageString(211);
        case 4:
            return GetLanguageString(212);
        case 5:
            return GetLanguageString(213);
        case 6:
            return GetLanguageString(214);
        case 7:
            return GetLanguageString(215);
        case 8:
            return GetLanguageString(216);
        case 9:
            return GetLanguageString(217);
        case 10:
            return GetLanguageString(218);
        case 11:
            return GetLanguageString(219);
        default:
            return "??month";
    }
}
