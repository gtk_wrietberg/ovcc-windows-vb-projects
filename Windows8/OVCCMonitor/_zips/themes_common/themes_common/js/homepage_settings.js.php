<?php
include('/opt/OVCCMac/wwwroot/ovcc/inc/include.php');

$active_language=OVCC_Language::ActiveLanguage();

$languages=OVCC_Language::AvailableLanguages();
$strings=OVCC_Language::LoadStrings($active_language);
$software_version=OVCC_Settings::GetSoftwareVersion();
$homepage_settings=OVCC_Settings::GetAllHomepageSettingsAsArray();

if($homepage_settings['office']=="apple") {
    $office_language_string=808;
} else {
    $office_language_string=805;
}
?>
//================================================================
//Office installed?
var _UI__IsOfficeAvailable=<?=(OVCC_Apps::IsOfficeAvailable()>0?'true':'false')?>;
var _UI__OfficeLanguageStringNumber=<?=$office_language_string?>;


//================================================================
//Language strings
var __language_active=<?=$active_language?>;
var __languages=[];
var __language_strings=[];
var _UI__Config={};

function GetLanguageString(id) {
    if(id in __language_strings) {
        return __language_strings[id];
    } else {
        return "[String #"+id+"]";
    }
}

function WriteLanguageString(id) {
    document.write(GetLanguageString(id));
}

<?php
foreach($languages as $language) {
    echo "__languages.push({id:".$language['language_id'].",name:'".addslashes($language['name'])."'});\n";
}

echo "\n";

foreach($strings as $string) {
    echo "__language_strings[".$string['id']."]='".addslashes($string['value'])."';\n";
}
?>

//================================================================
//UI clock and buttons
_UI__Config.clock_24hr=<?=$homepage_settings['clock_24hr']=="1"?"true":"false"?>;
_UI__Config.clock_color="<?=$homepage_settings['clock_color']?>";
_UI__Config.clock_shadowcolor="<?=$homepage_settings['clock_shadowcolor']?>";
_UI__Config.clock_show_seconds=<?=$homepage_settings['clock_show_seconds']=="1"?"true":"false"?>;

_UI__Config.button_Internet_url="<?=$homepage_settings['button_Internet_url']?>";
_UI__Config.button_Weather_url="<?=$homepage_settings['button_Weather_url']?>";
_UI__Config.button_Maps_url="<?=$homepage_settings['button_Maps_url']?>";
_UI__Config.button_HotelInfo_url="<?=$homepage_settings['button_HotelInfo_url']?>";

_UI__Config.hotel_address="<?=$homepage_settings['hotel_address']?>";


//================================================================
//Some functions
function WriteMachineName() {
    document.write("<?=addslashes(OVCC_UI::GetMachineName()); ?>");
}

function WriteSoftwareVersion() {
    document.write("<?=$software_version?>");
}