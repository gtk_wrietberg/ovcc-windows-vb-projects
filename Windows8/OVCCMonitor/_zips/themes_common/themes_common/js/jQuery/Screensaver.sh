#!/bin/bash


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi
#=============================================================================================================================


current_user=$(id -un)
debugging="$1"


LogMessage "screensaver triggered"


#remove trigger
if [ -d "$PATH__AgentTriggers/Screensaver" ]; then
    rm -f "$PATH__AgentTriggers/Screensaver/"*

    sleep 1

    fcount=$(ls -A "$PATH__AgentTriggers/Screensaver")
    if [ $fcount ]; then
        LogError "trigger not deleted! disabling"
        $TOOL__launchctl unload /Library/LaunchAgents/com.guesttek.ovcc.mac.screensaver.plist

        exit 1
    else
        if [ "$current_user" != "Guest" ]; then
            LogWarning "ignored, not Guest"
            if [ "$debugging" == "--debug" ]; then
                LogWarning "but we are debugging, so we are going to do it anyway"
            else
                exit 0
            fi
        fi
    fi
else
    LogError "trigger dir not found ($PATH__AgentTriggers/Screensaver), disabling!"
    $TOOL__launchctl unload /Library/LaunchAgents/com.guesttek.ovcc.mac.screensaver.plist
    exit 2
fi


db_return=$(curl --fail -s "${URL__api}/json__session_history.php?action=screensaver&message=triggered")
if [[ "$db_return" == "\"ok\"" ]]; then
    LogDebug "json__session_history ok"
else
    LogError "json__session_history error: $db_return"
fi


#-----------------------------------------------------------------------------------------------------------------------------
#Extra stuff for Sonoma
wallpaper_store_path="/Users/Guest/Library/Application Support/com.apple.wallpaper/Store"

if [ -d "$wallpaper_store_path" ]; then
    current_RFC3339_UTC_date="$(date -u '+%FT%TZ')"

    # Input the data from the 'Get Screen Saver and Wallpaper Settings' script to the variables below.
    wallpaperPath='file:///opt/OVCCMac/shared/backgrounds/desktop_GuestTek_20.png'
    wallpaperBase64='YnBsaXN0MDDSAQIDDF8QD2JhY2tncm91bmRDb2xvcllwbGFjZW1lbnTSBAUGC1pjb21wb25lbnRzWmNvbG9yU3BhY2WkBwgJCiM/0FBQUFBQUCM/2lpaWlpaWiM/5VVVVVVVVSM/8AAAAAAAAE8QQ2JwbGlzdDAwXxAXa0NHQ29sb3JTcGFjZUdlbmVyaWNSR0IIAAAAAAAAAQEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAACIQAQgNHykuOURJUltkbbMAAAAAAAABAQAAAAAAAAANAAAAAAAAAAAAAAAAAAAAtQ=='
    screensaverBase64='YnBsaXN0MDDRAQJWbW9kdWxl0QMEWHJlbGF0aXZlXxBBZmlsZTovLy9Vc2Vycy9HdWVzdC9MaWJyYXJ5L1NjcmVlbiUyMFNhdmVycy9PVkNDU2NyZWVuc2F2ZXIuc2F2ZXIICxIVHgAAAAAAAAEBAAAAAAAAAAUAAAAAAAAAAAAAAAAAAABi'

    # Index.plist contents.
    desktop_and_screensaver_settings_plist="$(plutil -create xml1 - |
        plutil -insert 'Desktop' -dictionary -o - - |
        plutil -insert 'Desktop.Content' -dictionary -o - - |
        plutil -insert 'Desktop.Content.Choices' -array -o - - |
        plutil -insert 'Desktop.Content.Choices' -dictionary -append -o - - |
        plutil -insert 'Desktop.Content.Choices.0.Configuration' -data "${wallpaperBase64}" -o - - |
        plutil -insert 'Desktop.Content.Choices.0.Files' -array -o - - |
        plutil -insert 'Desktop.Content.Choices.0.Files' -dictionary -append -o - - |
        plutil -insert 'Desktop.Content.Choices.0.Files.0.relative' -string "${wallpaperPath}" -o - - |
        plutil -insert 'Desktop.Content.Choices.0.Provider' -string 'com.apple.wallpaper.choice.image' -o - - |
        plutil -insert 'Desktop.Content.Shuffle' -string '$null' -o - - |
        plutil -insert 'Desktop.LastSet' -date "${current_RFC3339_UTC_date}" -o - - |
        plutil -insert 'Desktop.LastUse' -date "${current_RFC3339_UTC_date}" -o - - |
        plutil -insert 'Idle' -dictionary -o - - |
        plutil -insert 'Idle.Content' -dictionary -o - - |
        plutil -insert 'Idle.Content.Choices' -array -o - - |
        plutil -insert 'Idle.Content.Choices' -dictionary -append -o - - |
        plutil -insert 'Idle.Content.Choices.0.Configuration' -data "${screensaverBase64}" -o - - |
        plutil -insert 'Idle.Content.Choices.0.Files' -array -o - - |
        plutil -insert 'Idle.Content.Choices.0.Provider' -string 'com.apple.wallpaper.choice.screen-saver' -o - - |
        plutil -insert 'Idle.Content.Shuffle' -string '$null' -o - - |
        plutil -insert 'Idle.LastSet' -date "${current_RFC3339_UTC_date}" -o - - |
        plutil -insert 'Idle.LastUse' -date "${current_RFC3339_UTC_date}" -o - - |
        plutil -insert 'Type' -string 'individual' -o - -)"


    # Create the Index.plist
    plutil -create binary1 - |
        plutil -insert 'AllSpacesAndDisplays' -xml "${desktop_and_screensaver_settings_plist}" -o - - |
        plutil -insert 'Displays' -dictionary -o - - |
        plutil -insert 'Spaces' -dictionary -o - - |
        plutil -insert 'SystemDefault' -xml "${desktop_and_screensaver_settings_plist}" -o "${wallpaper_store_path}/Index.plist" -

    # Kill the wallpaperAgent to refresh and apply the screen saver/wallpaper settings.
    killall WallpaperAgent
fi
#=============================================================================================================================


#start screensaver
$TOOL__open -a ScreenSaverEngine
