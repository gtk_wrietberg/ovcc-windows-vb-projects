#!/bin/bash


DEBUG_FILE="/opt/OVCCMac/etc/debug"


if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi


if [ -f "$DEBUG_FILE" ]; then
    #-----------------------------------------------------------------------------------------------------------------------------
    #create temp var dump files
    TMP_FILE_VARS_BEFORE=$(mktemp /tmp/.vars.before.XXXXXXXX)
    TMP_FILE_VARS_AFTER=$(mktemp /tmp/.vars.after.XXXXXXXX)


    #-----------------------------------------------------------------------------------------------------------------------------
    #store variables before
    ( set -o posix ; set ) > "$TMP_FILE_VARS_BEFORE"
fi


#-----------------------------------------------------------------------------------------------------------------------------
#region "includes and inits"
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_watchdog_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_watchdog_globals.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi

#=============================================================================================================================

source "$parent_path/includes/_load_settings.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_load_settings.sh"
    exit 3
fi

#=============================================================================================================================


LogDebug "started"


#check if important tmp folders exist
UpdateTriggerFolder "$PATH__OVCCHidden"
UpdateTriggerFolder "$PATH__AgentTriggers"
UpdateTriggerFolder "$PATH__AgentTriggers/OVCCHomepageWrapper"
UpdateTriggerFolder "$PATH__AgentTriggers/WatchdogWarning"
UpdateTriggerFolder "$PATH__AgentTriggers/Reboot"
UpdateTriggerFolder "$PATH__AgentTriggers/Logout"
UpdateTriggerFolder "$PATH__AgentTriggers/Idle"
UpdateTriggerFolder "$PATH__AgentTriggers/Screensaver"
UpdateTriggerFolder "$PATH__AgentTriggers/SyncSettings"


LogDebug "watchdog triggered"


#get now timestamp
now_timestamp=$(date +%s)


#check debug mode
if [ -f "$PATH__Misc/debug" ]; then
    debug_mode_timestamp=$(stat -f %m "$PATH__Misc/debug")
    debug_mode_diff=$((now_timestamp - debug_mode_timestamp))

    if (( debug_mode_diff == 1 )); then
        LogDebug "debug mode is on for 1 second"
    else
        LogDebug "debug mode is on for $debug_mode_diff seconds"
    fi

    if (( debug_mode_diff > 3600 )); then
        LogWarning "debug mode is on for more than an hour. Disabling!"

        rm -f "$PATH__Misc/debug"
    fi
fi

#endregion


#=============================================================================================================================
#region "get values"
#get current values
idle_time=$WATCHDOG__idle_time
current__state=$WATCHDOG__state
loginscreen_timestamp=$WATCHDOG__loginscreen_timestamp
idle_time_loginscreen_max=$SETTINGS__idle_time_loginscreen_max
idle_time_max=$SETTINGS__idle_time_max
screensaver_idle_warning_delay=$SETTINGS__screensaver_idle_warning_delay
terms_agreed=$SETTINGS__terms_agreed
reboot_count=$WATCHDOG__reboot_count
watchdog_reboot_from_admin_account=$SETTINGS__watchdog_reboot_from_admin_account
watchdog_reboot_from_login_screen=$SETTINGS__watchdog_reboot_from_login_screen
watchdog_check_guest_user_auto_logon=$SETTINGS__watchdog_check_guest_user_auto_logon
watchdog_daily_reboot_enabled=$SETTINGS__watchdog_daily_reboot_enabled
watchdog_daily_reboot_hour=$SETTINGS__watchdog_daily_reboot_hour
last_reboot=$WATCHDOG__last_reboot
#active_user=$(who | awk '{print $1}' | sort | uniq)
active_user=$(echo "show State:/Users/ConsoleUser" | scutil | awk '/Name :/ && ! /loginwindow/ { print $3 }')

temporary_disable_timestamp=$WATCHDOG__temporary_disable_timestamp
temporary_disable_survive_reboot=$WATCHDOG__temporary_disable_survive_reboot
temporary_disable_diff=$((now_timestamp - temporary_disable_timestamp))
temporary_disable_remaining=$((3600 - temporary_disable_diff))
temporary_disabled=false
if (( temporary_disable_timestamp > 0 )); then
    if (( temporary_disable_diff < 0 )); then
        LogWarning "invalid value for 'temporary_disable_timestamp'"
        temporary_disable_diff=3601
    fi
    if (( temporary_disable_diff <= 3600 )); then
        LogMessage "watchdog is disabled for another $temporary_disable_remaining seconds"
        temporary_disabled=true
    fi
    if (( temporary_disable_diff > 3600 )); then
        LogMessage "watchdog is no longer disabled"
        temporary_disabled=false

        LogMessage "resetting timestamp"
        SetVarSetting "watchdog_temporary_disable_timestamp" "0"
        SetVarSetting "watchdog_temporary_disable_survive_reboot" "0"
    fi
fi

ninja__splashtop_connections=$(lsof -i -P | grep SRStream | wc -l)
ninja__ninja_remote_connections=$(lsof -i -P | grep ncstream | wc -l)


time_hour_of_day=$(date +%H)


if [ -f "$DEBUG_FILE" ]; then
    #-----------------------------------------------------------------------------------------------------------------------------
    #store variables after
    ( set -o posix ; set ) > "$TMP_FILE_VARS_AFTER"


    #-----------------------------------------------------------------------------------------------------------------------------
    #debug settings dump
    debug_settings_dump=$(diff "${TMP_FILE_VARS_BEFORE}" "${TMP_FILE_VARS_AFTER}" | grep -v "^[0-9c0-9]" | sed 's/\> //'  | sed 's/\< //')
    debug_settings_dump=$'\n===================\nstart variable dump\n\n'"${debug_settings_dump}"$'\n\nend variable dump\n=================\n'


    #-----------------------------------------------------------------------------------------------------------------------------
    #remove temp var dump files
    rm -f "$TMP_FILE_VARS_BEFORE"
    rm -f "$TMP_FILE_VARS_AFTER"


    #-----------------------------------------------------------------------------------------------------------------------------
    #log
    LogDebug "$debug_settings_dump"
fi


#-----------------------------------------------------------------------------------------------------------------------------
#new values
new__state=0


#some states require immediate action
skip_state_check_and_take_immediate_action=0
#endregion


#=============================================================================================================================
#region "check state"
#check state

#check it's time for daily reboot
if [ "$watchdog_daily_reboot_enabled" == "1" ]; then
    #strip leading zeroes
    time_hour_of_day=$(echo $time_hour_of_day | sed 's/^0*//')
    watchdog_daily_reboot_hour=$(echo $watchdog_daily_reboot_hour | sed 's/^0*//')

    #check hour
    if [ "$watchdog_daily_reboot_hour" == "$time_hour_of_day" ]; then
        LogDebug "time for daily reboot"

        last_reboot_diff=$((now_timestamp - last_reboot))

        #hours match, checking if reboot already happened
        if (( last_reboot_diff < 0 )); then
            LogWarning "machine is rebooted in the future?"
        fi
        if (( last_reboot_diff <= 3600 )); then
            LogDebug "machine already rebooted in the last hour"
        fi
        if (( last_reboot_diff > 3600 )); then
            LogDebug "machine rebooted long ago enough"

            if (( idle_time > idle_time_max )); then
                LogDebug "machine is idle"

                new__state=$((new__state | STATE__TIME_FOR_DAILY_REBOOT))

                #make sure reboot happens immediately
                skip_state_check_and_take_immediate_action=1
            else
                LogDebug "machine is not idle"
            fi
        fi
    fi
fi


#check if we're in a boot loop
if (( reboot_count > 3 )); then
    #REBOOT LOOP!
    LogError "IN A BOOT LOOP!"
fi


#check for remote connection
if (( ninja__ninja_remote_connections >= 2 )) || (( ninja__splashtop_connections >=4 )); then
    LogDebug "remote connection"

    new__state=$((new__state | STATE__REMOTE_CONNECTION))
fi

#check if we're in the login screen
if [ -z "$active_user" ]; then
    #in login screen
    LogDebug "in login screen"
    if (( loginscreen_timestamp <= 0 )); then
        SetVarSetting "watchdog_loginscreen_timestamp" "${now_timestamp}"
    fi

    new__state=$((new__state | STATE__LOGIN_WINDOW))
fi

#check if we're logged in as Guest
if [ "$active_user" != "Guest" ]; then
    #not Guest user
    LogDebug "not guest"

    new__state=$((new__state | STATE__NOT_GUEST))
fi

#check if Guest is set to auto logon
tmp_guest_auto_logon=$($TOOL__defaults read /Library/Preferences/com.apple.loginwindow autoLoginUser)
if [ "$?" != "0" ]; then
    LogWarning "(guest_auto_logon) - $TOOL__defaults returned error $?"
else
    if [ "$tmp_guest_auto_logon" != "Guest" ]; then
        if [ "$watchdog_check_guest_user_auto_logon" == "1" ]; then
            new__state=$((new__state | STATE__GUEST_NOT_AUTO_LOGON))
        else
            LogWarning "Guest is not set to auto logon, skipped by user request"
        fi
    fi
fi

#check if OVCCHomepageWrapper is running
process_count=$(ps aux | grep "OVCC - Homepage" | grep -v "grep" | awk 'END {print NR}')
if (( process_count < 1 )); then
    #not running
    LogDebug "wrapper not running"

    new__state=$((new__state | STATE__WRAPPER_NOT_RUNNING))

    #check if WRAPPER KEEP-ALIVE is already pending
    folder_item_count=$(ls -A "$PATH__AgentTriggers/OVCCHomepageWrapper" | wc -l)
    if (( folder_item_count > 0 )); then
        LogDebug "wrapper keep-alive is pending"

        new__state=$((new__state | STATE__WRAPPERKEEPALIVE_PENDING))
    fi
fi

#check if we're idle too long
if (( terms_agreed > 0 )); then
    if (( idle_time > idle_time_max )); then
        #idle too long
        LogDebug "idle too long, but screensaver should handle this"

        tmp_idle_delay_in_case_screensaver_fails=$(( idle_time_max + screensaver_idle_warning_delay*2 ))
        if (( idle_time > tmp_idle_delay_in_case_screensaver_fails )); then
            LogError "idle too long, and screensaver did not handle it!"
            new__state=$((new__state | STATE__IDLE_SCREENSAVER_FAIL))
        fi
    fi
fi

#check if we're in admin mode
if [ -f "$FILE__AdminModeTrigger" ]; then
    #admin mode
    LogDebug "in admin mode"

    if [ "$active_user" != "Guest" ]; then
        new__state=$((new__state | STATE__ADMIN_MODE))

        #check if admin mode is expired
        admin_mode_time_diff=$((now_timestamp - WATCHDOG__admin_mode_timestamp))

        if (( admin_mode_time_diff > SETTINGS__admin_mode_delay )); then
            LogDebug "admin mode expired"

            new__state=$((new__state | STATE__ADMIN_MODE_EXPIRED))
        fi
    else
        LogWarning "in admin mode, but logged in as Guest! Disabling admin mode!"
        rm "$FILE__AdminModeTrigger"
    fi
fi

#check if WARNING is already pending
folder_item_count=$(ls -A "$PATH__AgentTriggers/WatchdogWarning" | wc -l)
if (( folder_item_count > 0 )); then
    LogDebug "warning is pending"

    new__state=$((new__state | STATE__WARNING_PENDING))
fi

#check if LOGOUT is already pending
folder_item_count=$(ls -A "$PATH__AgentTriggers/Logout" | wc -l)
if (( folder_item_count > 0 )); then
    LogDebug "logout is pending"

    new__state=$((new__state | STATE__LOGOUT_PENDING))
fi

#check if REBOOT is already pending
folder_item_count=$(ls -A "$PATH__AgentTriggers/Reboot" | wc -l)
if (( folder_item_count > 0 )); then
    LogDebug "reboot is pending"

    new__state=$((new__state | STATE__REBOOT_PENDING))
fi
#endregion


#=============================================================================================================================
#region "check state change"
#check if state has changed, if so we need to take action
take_action=0
if (( current__state == new__state || skip_state_check_and_take_immediate_action == 1 )); then
    if (( current__state > 0 )); then
        LogMessage "state: $current__state === $new__state"
        if (( current__state != new__state && skip_state_check_and_take_immediate_action == 1 )); then
            LogMessage "state dont match but action is forced"
        fi

        #check which action we need to take

        # daily reboot
        if (( new__state & STATE__TIME_FOR_DAILY_REBOOT )); then
            take_action=$((take_action | ACTION__REBOOT))
        fi

        # not guest => warning
        if (( new__state & STATE__NOT_GUEST )); then
            take_action=$((take_action | ACTION__WARNING))
        fi

        # wrapper not running => keep alive
        if (( new__state & STATE__WRAPPER_NOT_RUNNING )); then
            take_action=$((take_action | ACTION__WRAPPERKEEPALIVE))
        fi

        # admin mode => no action
        if (( new__state & STATE__ADMIN_MODE )); then
            take_action=0
        fi

        # admin mode expired => warning
        if (( new__state & STATE__ADMIN_MODE_EXPIRED )); then
            take_action=$((take_action | ACTION__WARNING))
        fi

        # remote connection => postpone all actions
        if (( new__state & STATE__REMOTE_CONNECTION )); then
            take_action=$((take_action | ACTION__POSTPONED))
        fi

        # login window => reboot
        if (( new__state & STATE__LOGIN_WINDOW )); then
            time_spent_in_login_screen=$(( now_timestamp - loginscreen_timestamp ))
            LogMessage "in login window ($time_spent_in_login_screen / $idle_time_loginscreen_max)"

            if (( time_spent_in_login_screen > idle_time_loginscreen_max )); then
                SetVarSetting "watchdog_loginscreen_timestamp" "0"

                take_action=$((take_action | ACTION__REBOOT))
            else
                take_action=0
            fi
        fi


        #Postpone?
        if (( take_action & ACTION__POSTPONED )); then
            LogMessage "all actions postponed, remote connection"
        else
            #clean up to prevent multiple actions
            if (( take_action & ACTION__REBOOT )); then
                if (( watchdog_reboot_from_login_screen == 0 )); then
                    LogWarning "reboot is disabled by admin!"
                    take_action=0
                else
                    take_action=$ACTION__REBOOT
                fi
            fi
            if (( take_action & ACTION__LOGOUT )); then
                take_action=$ACTION__LOGOUT
            fi
            if (( take_action & ACTION__WARNING )); then
                if (( watchdog_reboot_from_admin_account == 0 )); then
                    LogWarning "warning is disabled by admin!"
                    take_action=0
                else
                    take_action=$ACTION__WARNING
                fi
            fi
            if (( take_action & ACTION__WRAPPERKEEPALIVE )); then
                take_action=$ACTION__WRAPPERKEEPALIVE
            fi


            if [ "$temporary_disabled" = true ] ; then
                #skip action, disabled
                if (( take_action & ACTION__REBOOT )); then
                    LogMessage "skipped triggering reboot, watchdog disabled"
                fi
                if (( take_action & ACTION__LOGOUT )); then
                    LogMessage "skipped triggering logout, watchdog disabled"
                fi
                if (( take_action & ACTION__WARNING )); then
                    LogMessage "skipped triggering warning, watchdog disabled"
                fi
                if (( take_action & ACTION__WRAPPERKEEPALIVE )); then
                    LogMessage "skipped triggering wrapper keep-alive, watchdog disabled"
                fi
            else
                #take action
                if (( take_action & ACTION__RESET_GUEST_AUTO_LOGON )); then
                    $TOOL__defaults write /Library/Preferences/com.apple.loginwindow autoLoginUser "Guest"

                    if [ "$?" != "0" ]; then
                        LogWarning "(ACTION__RESET_GUEST_AUTO_LOGON) - $TOOL__defaults returned $?"
                    fi
                fi
                if (( take_action & ACTION__REBOOT )); then
                    if (( new__state & STATE__REBOOT_PENDING )); then
                        LogDebug "reboot already triggered"
                    else
                        LogMessage "triggering reboot"

                        CreateTriggerFile "$PATH__AgentTriggers/Reboot/reb.oot"
                    fi
                fi
                if (( take_action & ACTION__LOGOUT )); then
                    if (( new__state & STATE__LOGOUT_PENDING )); then
                        LogDebug "logout already triggered"
                    else
                        LogMessage "triggering logout"

                        CreateTriggerFile "$PATH__AgentTriggers/Logout/log.out"
                    fi
                fi
                if (( take_action & ACTION__WARNING )); then
                    if (( new__state & STATE__WARNING_PENDING )); then
                        LogDebug "warning already triggered"
                    else
                        LogMessage "triggering warning"

                        CreateTriggerFile "$PATH__AgentTriggers/WatchdogWarning/war.ning"
                    fi
                fi
                if (( take_action & ACTION__WRAPPERKEEPALIVE )); then
                    if (( new__state & STATE__WRAPPERKEEPALIVE_PENDING )); then
                        LogDebug "wrapper keep-alive already triggered"
                    else
                        LogMessage "triggering wrapper keep-alive"

                        CreateTriggerFile "$PATH__AgentTriggers/OVCCHomepageWrapper/keep.alive"
                    fi
                fi
            fi
        fi
    else
        LogDebug "all still fine"
    fi
else
    LogMessage "state: $current__state --> $new__state"

    if (( new__state == 0 )); then
        LogMessage "all is fine again"
        LogDebug "reset reboot count"

        SetVarSetting "watchdog_reboot_count" "0"
    fi
fi
#endregion


#=============================================================================================================================
#save state
SetVarSetting "watchdog_state" "${new__state}"
SetVarSetting "watchdog_last_check" "${now_timestamp}"
