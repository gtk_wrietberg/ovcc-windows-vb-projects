#!/bin/bash


if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi

#=============================================================================================================================
LogMessage "syncing settings triggered"

UpdateTriggerFolder "$PATH__AgentTriggers/SyncSettings"

rm -f "$PATH__AgentTriggers/SyncSettings/"*


#Build sql for Settings and Watchdog, we don't want to import the var stuff
arr_settings=()
while IFS=  read -r -d $'\0'; do
    arr_settings+=("$REPLY")
done < <(find "$PATH__Config_Var" -type f \( -name "\.*" -not -name "\.watchdog*" \) -print0)
arr_settings_count=${#arr_settings[@]}

arr_watchdog=()
while IFS=  read -r -d $'\0'; do
    arr_watchdog+=("$REPLY")
done < <(find "$PATH__Config_Var" -type f -name "\.watchdog*" -print0)
arr_watchdog_count=${#arr_watchdog[@]}


sql_general_settings="SELECT name,value FROM Settings WHERE "
for i in "${arr_settings[@]}"
do
    stripped=$(basename "$i")
    stripped="${stripped#.}"

    sql_general_settings+="name!='${stripped}' AND "
done
sql_general_settings+="1=1 ORDER BY name ASC"


sql_watchdog_settings="SELECT name,value FROM Watchdog WHERE "
for i in "${arr_watchdog[@]}"
do
    stripped=$(basename "$i")
    stripped="${stripped#.watchdog_}"

    sql_watchdog_settings+="name!='${stripped}' AND "
done
sql_watchdog_settings+="1=1 ORDER BY name ASC"


LogDebug "sql for settings: ${arr_settings_count}"
LogDebug "sql for watchdog: ${arr_watchdog_count}"


#Load stuff from database
all_general_settings=$(DbQuery "${sql_general_settings}" "" "|")
all_homepage_settings=$(DbQuery "SELECT name,value FROM Homepage_Settings ORDER BY name ASC" "" "|")
all_watchdog_settings=$(DbQuery "${sql_watchdog_settings}" "" "|")
all_dock_items=$(DbQuery "SELECT [id],[name],[path],[enabled] FROM Dock ORDER BY [order] ASC,[name] ASC" "" "|")
all_apps=$(DbQuery "SELECT [id],[name],[value] FROM Applications ORDER BY [id]" "" "|")
all_blocked_apps=$(DbQuery "SELECT [id],[name],[bundle_identifier],[show_warning] FROM Blocked_Applications ORDER BY [id]" "" "|")
all_airlines=$(DbQuery "SELECT [id],[name],[url],[image] FROM Homepage_Airlines ORDER BY [name] COLLATE NOCASE ASC" "" "|")
version_from_db=$(DbQuery "SELECT version_major,version_minor,version_build FROM Version_History ORDER BY version_major DESC,version_minor DESC,version_build DESC LIMIT 1" "0")


#build Settings.ini file
LogMessage "building ${FILE__Settings}"

IFS=$'\n'
read -d '' -ra farray <<< "$all_general_settings"

echo -n "" > "$FILE__Settings"
for f in "${farray[@]}"; do
    IFS='|'
    read -r -a array <<< "$f"
    IFS=$'\n'

    name="${array[0]}"
    value="${array[1]}"

    echo "${name}=${value}" >> "$FILE__Settings"
done


#build Watchdog.ini file
LogMessage "building ${FILE__WatchdogSettings}"

IFS=$'\n'
read -d '' -ra farray <<< "$all_watchdog_settings"

echo -n "" > "$FILE__WatchdogSettings"
for f in "${farray[@]}"; do
    IFS='|'
    read -r -a array <<< "$f"
    IFS=$'\n'

    name="${array[0]}"
    value="${array[1]}"

    echo "${name}=${value}" >> "$FILE__WatchdogSettings"
done


#build Homepage_Settings.ini file
LogMessage "building ${FILE__HomepageSettings}"

IFS=$'\n'
read -d '' -ra farray <<< "$all_homepage_settings"

echo -n "" > "$FILE__HomepageSettings"
for f in "${farray[@]}"; do
    IFS='|'
    read -r -a array <<< "$f"
    IFS=$'\n'

    name="${array[0]}"
    value="${array[1]}"

    echo "${name}=${value}" >> "$FILE__HomepageSettings"
done


#build Dock.ini file
LogMessage "building ${FILE__DockItems}"

IFS=$'\n'
read -d '' -ra farray <<< "$all_dock_items"

echo -n "" > "$FILE__DockItems"
for f in "${farray[@]}"; do
    IFS='|'
    read -r -a array <<< "$f"
    IFS=$'\n'

    id="${array[0]}"
    name="${array[1]}"
    path="${array[2]}"
    enabled="${array[3]}"

    echo "[app${id}]" >> "$FILE__DockItems"
    echo "name=${name}" >> "$FILE__DockItems"
    echo "path=${path}" >> "$FILE__DockItems"
    echo "enabled=${enabled}" >> "$FILE__DockItems"
done


#build Applications.ini file
LogMessage "building ${FILE__Apps}"

IFS=$'\n'
read -d '' -ra farray <<< "$all_apps"

echo -n "" > "$FILE__Apps"
for f in "${farray[@]}"; do
    IFS='|'
    read -r -a array <<< "$f"
    IFS=$'\n'

    id="${array[0]}"
    name="${array[1]}"
    value="${array[2]}"

    echo "[app${id}]" >> "$FILE__Apps"
    echo "name=${name}" >> "$FILE__Apps"
    echo "value=${value}" >> "$FILE__Apps"
done


#build Blocked_apps.ini file
LogMessage "building ${FILE__BlockedApps}"

IFS=$'\n'
read -d '' -ra farray <<< "$all_blocked_apps"

echo -n "" > "$FILE__BlockedApps"
for f in "${farray[@]}"; do
    IFS='|'
    read -r -a array <<< "$f"
    IFS=$'\n'

    id="${array[0]}"
    name="${array[1]}"
    bundle_identifier="${array[2]}"
    show_warning="${array[3]}"

    echo "[app${id}]" >> "$FILE__BlockedApps"
    echo "name=${name}" >> "$FILE__BlockedApps"
    echo "bundle_identifier=${bundle_identifier}" >> "$FILE__BlockedApps"
    echo "show_warning=${show_warning}" >> "$FILE__BlockedApps"
done


#build Homepage_Airlines.ini file
LogMessage "building ${FILE__Airlines}"

IFS=$'\n'
read -d '' -ra farray <<< "$all_airlines"

echo -n "" > "$FILE__Airlines"
for f in "${farray[@]}"; do
    IFS='|'
    read -r -a array <<< "$f"
    IFS=$'\n'

    id="${array[0]}"
    name="${array[1]}"
    url="${array[2]}"
    image="${array[3]}"

    echo "[airline${id}]" >> "$FILE__Airlines"
    echo "name=${name}" >> "$FILE__Airlines"
    echo "url=${url}" >> "$FILE__Airlines"
    echo "image=${image}" >> "$FILE__Airlines"
done


#build version file
echo -n "" > "$FILE__Version"
IFS='|'
read -r -a array <<< "$version_from_db"
IFS=$'\n'
version_major="${array[0]}"
version_minor="${array[1]}"
version_build="${array[2]}"
echo "${version_major}.${version_minor}.${version_build}" >> "$FILE__Version"


#Set permissions
chown root:_www "$FILE__Settings"
chmod 644 "$FILE__Settings"

chown root:_www "$FILE__HomepageSettings"
chmod 644 "$FILE__HomepageSettings"

chown root:_www "$FILE__WatchdogSettings"
chmod 644 "$FILE__WatchdogSettings"

chown root:_www "$FILE__DockItems"
chmod 644 "$FILE__DockItems"

chown root:_www "$FILE__Apps"
chmod 644 "$FILE__Apps"

chown root:_www "$FILE__BlockedApps"
chmod 644 "$FILE__BlockedApps"

chown root:_www "$FILE__Airlines"
chmod 644 "$FILE__Airlines"

chown root:_www "$FILE__Version"
chmod 644 "$FILE__Version"


#make sure the .var subdir exists, with the proper permissions
mkdir -p "${PATH__Config_Var}"
chown -R root:_www "${PATH__Config_Var}"
chmod 775 "${PATH__Config_Var}"
chmod 664 "${PATH__Config_Var}/".[^.]*


#store in db
db_return=$(curl --fail -s "${URL__api}/json__session_history.php?action=syncsettings")
if [[ "$db_return" == "\"ok\"" ]]; then
    LogDebug "json__session_history ok"
else
    LogError "json__session_history error: $db_return"
fi
