#!/bin/bash

#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi
#=============================================================================================================================

LogDebug "started"

current_user=$(id -un)
LogDebug "current_user=$current_user"


#reset terms & idle
terms_reset=$(curl --fail -s "${URL__api}/json__session.php?action=reset_all")


#Guest
if [ "$current_user" == "Guest" ]; then
    sleep 5


    #update Dock
    dock_items_tries=1
    dock_items_return="1"
    while [ "$dock_items_return" != "0" ] && (( dock_items_tries<10 ))
    do
        dock_items=$(curl --fail -s "${URL__api}/dock_items.php")
        dock_items_return="$?"
        if [ "$dock_items_return" != "0" ]; then
            LogError "Error while retrieving dock items from api (#${dock_items_tries})"

            dock_items_tries=$((dock_items_tries+1))

            sleep 1
        fi
    done

    if (( dock_items_tries>=10)); then
        exit 4
    fi


    #get Dock icons sizes from db
    dock_tilesize=$(curl --fail -s "${URL__api}/json__setting_by_setting_name_from_file.php?name=dock_tilesize")
    dock_tilesize=$(echo "$dock_tilesize" | "$TOOL__python" -c "import sys, json; print(json.load(sys.stdin)['value'])")

    dock_largesize=$(curl --fail -s "${URL__api}/json__setting_by_setting_name_from_file.php?name=dock_largesize")
    dock_largesize=$(echo "$dock_largesize" | "$TOOL__python" -c "import sys, json; print(json.load(sys.stdin)['value'])")

    LogDebug "dock_tilesize=$dock_tilesize;dock_largesize=$dock_largesize"


    #sanity checks on values, making sure they are numbers and not too small/big
    re='^[0-9]+$'
    if ! [[ $dock_largesize =~ $re ]] ; then
        dock_largesize="53"
    fi
    if ! [[ $dock_tilesize =~ $re ]] ; then
        dock_tilesize="40"
    fi
    if (( dock_tilesize < 16 )); then
        dock_tilesize="16"
    fi
    if (( dock_tilesize > 128 )); then
        dock_tilesize="128"
    fi
    if (( dock_largesize < dock_tilesize )); then
        dock_largesize="$dock_tilesize"
    fi
    if (( dock_largesize > 128 )); then
        dock_largesize="128"
    fi

    LogDebug "dock_tilesize=$dock_tilesize;dock_largesize=$dock_largesize"

    #set Dock icons sizes
    $TOOL__defaults write com.apple.dock tilesize -int $dock_tilesize
    $TOOL__defaults write com.apple.dock largesize -int $dock_largesize


    #get current Dock items
    #current_dock_items=$("$TOOL__python" "$TOOL__dockutil" --list | awk '{split($0,a,"file:///"); print a[1]}')


    #disable Recent items in Dock
    $TOOL__defaults write com.apple.dock show-recents -bool FALSE


    #clear Dock
    LogDebug "clearing Dock"
    "$TOOL__python" "$TOOL__dockutil" --remove all --no-restart


    #add our items
    OIFS="$IFS" #store old separator
    IFS=$'\n' #temporarily make newline the separator
    for dock_item in $dock_items
    do
        if [ -d "$dock_item" ] || [ -f "$dock_item" ]; then
            LogDebug "adding $dock_item"
            "$TOOL__python" "$TOOL__dockutil" --add "$dock_item" --no-restart
        else
            LogWarning "Dock item skipped ($dock_item)"
        fi
    done
    IFS="$OIFS" #restore default separator


    #kick Dock in the bollocks
    $TOOL__killall Dock >/dev/null 2>&1


    #attempt to snooze notifications
    sleep 5


    #Notifications Do Not Disturb
    $TOOL__defaults -currentHost write com.apple.notificationcenterui dndStart -float 0
    $TOOL__defaults -currentHost write com.apple.notificationcenterui dndEnd -float 1439
    $TOOL__defaults -currentHost write com.apple.notificationcenterui doNotDisturb -bool TRUE

    $TOOL__killall NotificationCenter


    #-----------------------------------------------------------------------------------------------------------------------------
    #Extra stuff for Sonoma
    wallpaper_store_path="/Users/Guest/Library/Application Support/com.apple.wallpaper/Store"

    if [ -d "$wallpaper_store_path" ]; then
        current_RFC3339_UTC_date="$(date -u '+%FT%TZ')"

        # Input the data from the 'Get Screen Saver and Wallpaper Settings' script to the variables below.
        wallpaperPath='file:///opt/OVCCMac/shared/backgrounds/desktop_GuestTek_20.png'
        wallpaperBase64='YnBsaXN0MDDSAQIDDF8QD2JhY2tncm91bmRDb2xvcllwbGFjZW1lbnTSBAUGC1pjb21wb25lbnRzWmNvbG9yU3BhY2WkBwgJCiM/0FBQUFBQUCM/2lpaWlpaWiM/5VVVVVVVVSM/8AAAAAAAAE8QQ2JwbGlzdDAwXxAXa0NHQ29sb3JTcGFjZUdlbmVyaWNSR0IIAAAAAAAAAQEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAACIQAQgNHykuOURJUltkbbMAAAAAAAABAQAAAAAAAAANAAAAAAAAAAAAAAAAAAAAtQ=='
        screensaverBase64='YnBsaXN0MDDRAQJWbW9kdWxl0QMEWHJlbGF0aXZlXxBBZmlsZTovLy9Vc2Vycy9HdWVzdC9MaWJyYXJ5L1NjcmVlbiUyMFNhdmVycy9PVkNDU2NyZWVuc2F2ZXIuc2F2ZXIICxIVHgAAAAAAAAEBAAAAAAAAAAUAAAAAAAAAAAAAAAAAAABi'

        # Index.plist contents.
        desktop_and_screensaver_settings_plist="$(plutil -create xml1 - |
            plutil -insert 'Desktop' -dictionary -o - - |
            plutil -insert 'Desktop.Content' -dictionary -o - - |
            plutil -insert 'Desktop.Content.Choices' -array -o - - |
            plutil -insert 'Desktop.Content.Choices' -dictionary -append -o - - |
            plutil -insert 'Desktop.Content.Choices.0.Configuration' -data "${wallpaperBase64}" -o - - |
            plutil -insert 'Desktop.Content.Choices.0.Files' -array -o - - |
            plutil -insert 'Desktop.Content.Choices.0.Files' -dictionary -append -o - - |
            plutil -insert 'Desktop.Content.Choices.0.Files.0.relative' -string "${wallpaperPath}" -o - - |
            plutil -insert 'Desktop.Content.Choices.0.Provider' -string 'com.apple.wallpaper.choice.image' -o - - |
            plutil -insert 'Desktop.Content.Shuffle' -string '$null' -o - - |
            plutil -insert 'Desktop.LastSet' -date "${current_RFC3339_UTC_date}" -o - - |
            plutil -insert 'Desktop.LastUse' -date "${current_RFC3339_UTC_date}" -o - - |
            plutil -insert 'Idle' -dictionary -o - - |
            plutil -insert 'Idle.Content' -dictionary -o - - |
            plutil -insert 'Idle.Content.Choices' -array -o - - |
            plutil -insert 'Idle.Content.Choices' -dictionary -append -o - - |
            plutil -insert 'Idle.Content.Choices.0.Configuration' -data "${screensaverBase64}" -o - - |
            plutil -insert 'Idle.Content.Choices.0.Files' -array -o - - |
            plutil -insert 'Idle.Content.Choices.0.Provider' -string 'com.apple.wallpaper.choice.screen-saver' -o - - |
            plutil -insert 'Idle.Content.Shuffle' -string '$null' -o - - |
            plutil -insert 'Idle.LastSet' -date "${current_RFC3339_UTC_date}" -o - - |
            plutil -insert 'Idle.LastUse' -date "${current_RFC3339_UTC_date}" -o - - |
            plutil -insert 'Type' -string 'individual' -o - -)"


        # Create the Index.plist
        plutil -create binary1 - |
            plutil -insert 'AllSpacesAndDisplays' -xml "${desktop_and_screensaver_settings_plist}" -o - - |
            plutil -insert 'Displays' -dictionary -o - - |
            plutil -insert 'Spaces' -dictionary -o - - |
            plutil -insert 'SystemDefault' -xml "${desktop_and_screensaver_settings_plist}" -o "${wallpaper_store_path}/Index.plist" -

        # Kill the wallpaperAgent to refresh and apply the screen saver/wallpaper settings.
        killall WallpaperAgent
    fi
    #=============================================================================================================================


    #save event in session history
    db_return=$(curl --fail -s --data-urlencode "action=dock" --data-urlencode "message=guest updated" "${URL__api}/json__session_history.php")
    if [[ "$db_return" == "\"ok\"" ]]; then
        LogDebug "json__session_history ok"
    else
        LogError "json__session_history error: $db_return"
    fi
else
    sleep 5


    #Add config app to Dock of all other users
    "$TOOL__python" "$TOOL__dockutil" --add "$PATH__Bin/apps/OVCC - Config.app"


    #save event in session history
    db_return=$(curl --fail -s --data-urlencode "action=dock" --data-urlencode "message=${current_user} updated" "${URL__api}/json__session_history.php")
    if [[ "$db_return" == "\"ok\"" ]]; then
        LogDebug "json__session_history ok"
    else
        LogError "json__session_history error: $db_return"
    fi

    #the bollock-kicking is done by dockutil
fi
