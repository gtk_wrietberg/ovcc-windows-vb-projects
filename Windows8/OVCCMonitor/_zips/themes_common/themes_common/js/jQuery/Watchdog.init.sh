#!/bin/bash


if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi
#=============================================================================================================================

LogMessage "started"


#check if important tmp folders exist
LogMessage "trigger folders"
UpdateTriggerFolder "$PATH__OVCCHidden"
UpdateTriggerFolder "$PATH__AgentTriggers"
UpdateTriggerFolder "$PATH__AgentTriggers/OVCCHomepageWrapper"
UpdateTriggerFolder "$PATH__AgentTriggers/WatchdogWarning"
UpdateTriggerFolder "$PATH__AgentTriggers/Reboot"
UpdateTriggerFolder "$PATH__AgentTriggers/Logout"
UpdateTriggerFolder "$PATH__AgentTriggers/Screensaver"
UpdateTriggerFolder "$PATH__AgentTriggers/SyncSettings"
UpdateTriggerFolder "$PATH__AgentTriggers/SyncHistory"


#remove triggers
LogMessage "remove triggers"
rm -rf "$PATH__AgentTriggers/OVCCHomepageWrapper/"*
rm -rf "$PATH__AgentTriggers/WatchdogWarning/"*
rm -rf "$PATH__AgentTriggers/Reboot/"*
rm -rf "$PATH__AgentTriggers/Logout/"*
rm -rf "$PATH__AgentTriggers/Screensaver/"*
rm -rf "$PATH__AgentTriggers/SyncSettings/"*
rm -rf "$PATH__AgentTriggers/SyncHistory/"*
rm -rf "$FILE__AdminModeTrigger"


#create idle time log
if [[ -f "$PATH__idletimelog" ]]; then
    LogMessage "reset idle time log"
    rm -rf "$PATH__idletimelog"
fi
CreateTriggerFile "$PATH__idletimelog"


#PHP debug log privileges
PHP_TMP_LOG="/tmp/.OVCC.php.log"
if [ ! -f "${PHP_TMP_LOG}" ]; then
    touch "${PHP_TMP_LOG}"
fi
chown root:_www "${PHP_TMP_LOG}"
chmod 664 "${PHP_TMP_LOG}"


#create backup of database
#check if db is changed
LogMessage "daily db backup"
LogMessage "settings"

latest_db="${PATH__integrity_db}/${FILE__Database_Settings_Name}"

if [[ -f $latest_db ]]; then
    if [[ $(find "$latest_db" -mtime +1 -print) ]]; then
        # the latest backup is more than a day old

        date_str=$(date '+%Y%m%d_%H%M%S')

        "$TOOL__zip" -9 -j "${latest_db}.${date_str}.zip" "$latest_db"
        if [ -f "${latest_db}.${date_str}.zip" ]; then
            LogMessage "zipped '$latest_db' -> '${latest_db}.${date_str}.zip'"
            rm -f "$latest_db"
        fi

        cp "$FILE__Database_Settings" "$PATH__integrity_db"
        LogMessage "copied '$FILE__Database_Settings' -> '$PATH__integrity_db'"
    else
        LogMessage "not needed"
    fi

    if [[ $(find "$PATH__integrity_db"/* -mtime +28 -print) ]]; then
        LogMessage "deleting backups older than 4 weeks"
        find "$PATH__integrity_db"/* -mtime +28 -print -exec rm {} \;
    fi
else
    #no copy found
    cp "$FILE__Database_Settings" "$PATH__integrity_db"
    LogMessage "copied '$FILE__Database_Settings' -> '$PATH__integrity_db'"
fi


LogMessage "sessions"
latest_db="${PATH__integrity_db}/${FILE__Database_Sessions_Name}"

if [[ -f $latest_db ]]; then
    if [[ $(find "$latest_db" -mtime +1 -print) ]]; then
        # the latest backup is more than a day old

        date_str=$(date '+%Y%m%d_%H%M%S')

        "$TOOL__zip" -9 -j "${latest_db}.${date_str}.zip" "$latest_db"
        if [ -f "${latest_db}.${date_str}.zip" ]; then
            LogMessage "zipped '$latest_db' -> '${latest_db}.${date_str}.zip'"
            rm -f "$latest_db"
        fi

        cp "$FILE__Database_Sessions" "$PATH__integrity_db"
        LogMessage "copied '$FILE__Database_Sessions' -> '$PATH__integrity_db'"
    else
        LogMessage "not needed"
    fi

    if [[ $(find "$PATH__integrity_db"/* -mtime +28 -print) ]]; then
        LogMessage "deleting backups older than 4 weeks"
        find "$PATH__integrity_db"/* -mtime +28 -print -exec rm {} \;
    fi
else
    #no copy found
    cp "$FILE__Database_Sessions" "$PATH__integrity_db"
    LogMessage "copied '$FILE__Database_Sessions' -> '$PATH__integrity_db'"
fi


sleep 5


temporary_disable_survive_reboot=$(GetVarSetting "watchdog_temporary_disable_survive_reboot" "0")
if [ "$DEFAULT__WatchdogTemporaryDisableSurviveReboot" == "1"]; then
    temporary_disable_survive_reboot="1"
    SetVarSetting "watchdog_temporary_disable_survive_reboot" "1"
fi
if [ "$temporary_disable_survive_reboot" != "1"]; then
    SetVarSetting "watchdog_temporary_disable_timestamp" "0"
fi


SetVarSetting "watchdog_state" "-1"
