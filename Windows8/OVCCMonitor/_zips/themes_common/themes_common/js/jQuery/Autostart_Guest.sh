#!/bin/bash


text_special=$(tput bold; tput setaf 0; tput setab 7;)
text_normal=$(tput sgr0)


function _Usage {
    echo
    echo "===================================================================================="
    echo
    echo "You must be root (sudo) to run this script."
    echo
    echo "Usage examples"
    echo
    echo "Set the autostart"
    echo "  ${text_special}$0${text_normal}"
    echo
    echo "Set the autostart and reboot"
    echo "  ${text_special}$0 reboot${text_normal}"
    echo ""

    exit 1
}


if (( EUID != 0 )); then
    _Usage
fi


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi


#=============================================================================================================================
AUTOSTART__reboot="no"

if [ "$1" == "reboot" ]; then
    AUTOSTART__reboot="yes"
fi


# ==========================================================================================
# Check
KCPASSWORD_FILE="/etc/kcpassword"

if [ -f "/etc/kcpassword" ]; then
    rm -f /etc/kcpassword
fi


# ==========================================================================================
# Set autostart
$TOOL__defaults write /Library/Preferences/com.apple.loginwindow autoLoginUser "Guest"


# ==========================================================================================
# Set Watchdog to enabled
SetVarSetting "watchdog_temporary_disable_timestamp" "0"
SetVarSetting "watchdog_temporary_disable_survive_reboot" "0"


# ==========================================================================================
echo "Auto start set for user ${text_special}Guest${text_normal}"


# ==========================================================================================
if [ "$AUTOSTART__reboot" == "yes" ]; then
    echo "  rebooting!"

    shutdown -r now
fi
