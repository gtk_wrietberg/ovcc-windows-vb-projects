#!/bin/bash


if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi
#=============================================================================================================================


#-----------------------------------------------------------------------------------------------------------------------------
#disabling Guest account
LogMessage "Disabling Guest account"

$TOOL__dscl . -delete /Users/Guest
$TOOL__security delete-generic-password -a Guest -s com.apple.loginwindow.guest-account -D "application password" /Library/Keychains/System.keychain
$TOOL__defaults write /Library/Preferences/com.apple.loginwindow GuestEnabled -bool FALSE
#=============================================================================================================================


#-----------------------------------------------------------------------------------------------------------------------------
#auto start with Guest account
LogMessage "Remove Guest account auto-start"

$TOOL__defaults write /Library/Preferences/com.apple.loginwindow autoLoginUser ""

if [ "$?" != "0" ]; then
        echo "  Error ($?)"
fi
echo "  ok"
#=============================================================================================================================


LogMessage "wait a few seconds"
sleep 5


#-----------------------------------------------------------------------------------------------------------------------------
#enabling Guest account
LogMessage "Enabling Guest account"

LogMessage " creating account"
$TOOL__dscl . -create /Users/Guest
$TOOL__dscl . -create /Users/Guest dsAttrTypeNative:_defaultLanguage en
$TOOL__dscl . -create /Users/Guest dsAttrTypeNative:_guest true
$TOOL__dscl . -create /Users/Guest dsAttrTypeNative:_writers__defaultLanguage Guest
$TOOL__dscl . -create /Users/Guest dsAttrTypeNative:_writers__LinkedIdentity Guest
$TOOL__dscl . -create /Users/Guest dsAttrTypeNative:_writers__UserCertificate Guest
$TOOL__dscl . -create /Users/Guest AuthenticationHint ''
$TOOL__dscl . -create /Users/Guest NFSHomeDirectory /Users/Guest

# Give a little extra time for the password and kerberos to play nicely
LogMessage "  waiting for stuff to synchronise"
sleep 5
$TOOL__dscl . -passwd /Users/Guest ''
sleep 5

LogMessage " creating continued"
$TOOL__dscl . -create /Users/Guest Picture "$PATH__Shared_Icons/GuestTek_512x512.png"
$TOOL__dscl . -create /Users/Guest PrimaryGroupID 201
$TOOL__dscl . -create /Users/Guest RealName "OVCC Guest"
$TOOL__dscl . -create /Users/Guest RecordName Guest
$TOOL__dscl . -create /Users/Guest UniqueID 201
$TOOL__dscl . -create /Users/Guest UserShell /bin/bash
$TOOL__security add-generic-password -a Guest -s com.apple.loginwindow.guest-account -D "application password" /Library/Keychains/System.keychain

# This seems to be technically unnecessary; it controls whether the "Allow
# guests to log in to this computer" checkbox is enabled in SysPrefs
$TOOL__defaults write /Library/Preferences/com.apple.loginwindow GuestEnabled -bool TRUE

# Profiles created with PM have these two keys, but they don't seem to do anything
#defaults write /Library/Preferences/com.apple.loginwindow DisableGuestAccount -bool FALSE
#defaults write /Library/Preferences/com.apple.loginwindow EnableGuestAccount -bool TRUE

#this should enable content filtering
$TOOL__dscl . -delete /Users/Guest MCXSettings
guest_mcxsettings=$(<${PATH__integrity_config}/Guest/guest.mcxsettings)
$TOOL__dscl . -create /Users/Guest MCXSettings "$guest_mcxsettings"


#-----------------------------------------------------------------------------------------------------------------------------
#auto start with Guest account
LogMessage "Set Guest account auto-start"

$TOOL__defaults write /Library/Preferences/com.apple.loginwindow autoLoginUser "Guest"

if [ "$?" != "0" ]; then
    echo "  Error ($?)"
    exit 8
fi
#=============================================================================================================================


echo "done"
