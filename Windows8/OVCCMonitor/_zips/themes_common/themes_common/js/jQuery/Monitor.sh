#!/bin/bash


#=============================================================================================================================
#region "run as root"
if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "timeout handler"
declare -i MONITOR_TIMEOUT=30
declare -i MONITOR_TIMEOUT_INTERVAL=1
declare -i MONITOR_TIMEOUT_KILL_DELAY=1

(
    ((t = MONITOR_TIMEOUT))

    while ((t > 0)); do
        sleep $MONITOR_TIMEOUT_INTERVAL
        kill -0 $$ || exit 0
        ((t -= MONITOR_TIMEOUT_INTERVAL))
    done

    # Be nice, post SIGTERM first.
    # The 'exit 0' below will be executed if any preceeding command fails.
    kill -s SIGTERM $$ && kill -0 $$ || exit 0
    sleep $MONITOR_TIMEOUT_KILL_DELAY
    kill -s SIGKILL $$
) 2> /dev/null &
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "includes"
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 3
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 4
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 5
fi

source "$parent_path/includes/_watchdog_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_watchdog_globals.sh"
    exit 6
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 7
fi

source "$parent_path/includes/_monitor_version.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_monitor_version.sh"
    exit 8
fi

source "$parent_path/includes/_monitor_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_monitor_globals.sh"
    exit 8
fi

source "$parent_path/includes/_monitor_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_monitor_functions.sh"
    exit 9
fi

source "$parent_path/includes/_load_settings.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_load_settings.sh"
    exit 10
fi
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "init"
LogMessage "monitor v${VERSION_MONITOR} triggered"

#get now timestamp
now_timestamp=$(date +%s)

#check folders
if [ ! -d "$PATH__MonitorMain" ]; then
    LogWarning "Directory not found: ${PATH__MonitorMain}"
    mkdir -p "$PATH__MonitorMain"
    chmod 700 "$PATH__MonitorMain"
fi

#make sure permissions are right
find "$PATH__MonitorMain" -type f -exec chmod 600 {} +
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "load settings"
unset "${!MONITOR_APPSETTING__@}"
unset "${!MONITOR_OVCCSETTING__@}"

LogMessage "loading settings"
MONITOR_APPSETTING__LastInventorySyncDelay=$(DbQueryMonitorSettings "SELECT value FROM Monitor_Settings WHERE name='LastInventorySyncDelay'" "${MONITOR_APPSETTING_DEFAULT__LastInventorySyncDelay}")
MONITOR_APPSETTING__LastMetricsSyncDelay=$(DbQueryMonitorSettings "SELECT value FROM Monitor_Settings WHERE name='LastMetricsSyncDelay'" "${MONITOR_APPSETTING_DEFAULT__LastMetricsSyncDelay}")
MONITOR_APPSETTING__LastGeolocationSyncDelay=$(DbQueryMonitorSettings "SELECT value FROM Monitor_Settings WHERE name='LastGeolocationSyncDelay'" "${MONITOR_APPSETTING_DEFAULT__LastGeolocationSyncDelay}")
MONITOR_APPSETTING__DowntimeThresholdSeconds=$(DbQueryMonitorSettings "SELECT value FROM Monitor_Settings WHERE name='DowntimeThresholdSeconds'" "${MONITOR_APPSETTING_DEFAULT__DowntimeThresholdSeconds}")
MONITOR_APPSETTING__MonitorUpdateEnabled=$(DbQueryMonitorSettings "SELECT value FROM Monitor_Settings WHERE name='Monitor_UpdateEnabled'" "${MONITOR_APPSETTING_DEFAULT__MonitorUpdateEnabled}")

MONITOR_OVCCSETTING__ExpectedFileVersion_Monitor=$(DbQueryMonitorSettings "SELECT value FROM Monitor_Settings WHERE name='ExpectedFileVersion_Monitor'" "${MONITOR_OVCCSETTING_DEFAULT__ExpectedFileVersion_Monitor}")


DEBUGGING_MONITOR_APPSETTINGS=$(set -o posix; set | sed -e '/^_=*/d' | grep "MONITOR_APPSETTING__")
DEBUGGING_MONITOR_OVCCSETTINGS=$(set -o posix; set | sed -e '/^_=*/d' | grep "MONITOR_OVCCSETTING__")

LogDebug "loaded appsettings"
LogDebug "${DEBUGGING_MONITOR_APPSETTINGS}"
LogDebug "loaded ovccsettings"
LogDebug "${DEBUGGING_MONITOR_OVCCSETTINGS}"
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "check watchdog"
MONITOR__daemon_loaded__watchdog_init=$(GetDaemonStatus "watchdog\.init")
if [ "${MONITOR__daemon_loaded__watchdog_init}" = "0" ]; then
    LogWarning "watchdog.init not loaded"

    if [ -f "${FILE__MonitorDontReviveWatchdog}" ]; then
        LogWarning "ignored"
    else
        if [ ! -f "${FILE__MonitorWatchdogInitDisabledTimestamp}" ]; then
            echo "${now_timestamp}" > "${FILE__MonitorWatchdogInitDisabledTimestamp}"
        fi

        MONITOR__WatchdogInitDisabledTimestamp=$(<"${FILE__MonitorWatchdogInitDisabledTimestamp}")
        MONITOR__WatchdogInitDisabledTimestampDiff=$(($now_timestamp-$MONITOR__WatchdogInitDisabledTimestamp))
        if (( MONITOR__WatchdogInitDisabledTimestampDiff > MONITOR_APPSETTING_DEFAULT__WatchdogReloadDelay )); then
            MONITOR__WatchdogInitDisabledExitCode=$(SetDaemonStatus "watchdog.init" "load")

            if [ "${MONITOR__WatchdogInitDisabledExitCode}" = "0" ]; then
                LogMessage "watchdog.init reloaded ok"
            else
                LogError "watchdog.init not reloaded!"
            fi
        else
            MONITOR__WatchdogInitDisabledTimestampDiffRemainder=$(($MONITOR_APPSETTING_DEFAULT__WatchdogReloadDelay-$MONITOR__WatchdogInitDisabledTimestampDiff))
            LogMessage "watchdog.init will be reloaded soon (${MONITOR__WatchdogInitDisabledTimestampDiffRemainder})"
        fi
    fi
else
    if [ -f "${FILE__MonitorWatchdogInitDisabledTimestamp}" ]; then
        rm -rf "${FILE__MonitorWatchdogInitDisabledTimestamp}"
    fi
fi


MONITOR__daemon_loaded__watchdog_main=$(GetDaemonStatus "watchdog\.main")
if [ "${MONITOR__daemon_loaded__watchdog_main}" = "0" ]; then
    LogWarning "watchdog.main not loaded"

    if [ -f "${FILE__MonitorDontReviveWatchdog}" ]; then
        LogWarning "ignored"
    else
        if [ ! -f "${FILE__MonitorWatchdogMainDisabledTimestamp}" ]; then
            echo "${now_timestamp}" > "${FILE__MonitorWatchdogMainDisabledTimestamp}"
        fi

        MONITOR__WatchdogMainDisabledTimestamp=$(<"${FILE__MonitorWatchdogMainDisabledTimestamp}")
        MONITOR__WatchdogMainDisabledTimestampDiff=$(($now_timestamp-$MONITOR__WatchdogMainDisabledTimestamp))
        if (( MONITOR__WatchdogMainDisabledTimestampDiff > MONITOR_APPSETTING_DEFAULT__WatchdogReloadDelay )); then
            MONITOR__WatchdogMainDisabledExitCode=$(SetDaemonStatus "watchdog.main" "load")

            if [ "${MONITOR__WatchdogMainDisabledExitCode}" = "0" ]; then
                LogMessage "watchdog.main reloaded ok"
            else
                LogError "watchdog.main not reloaded!"
            fi
        else
            MONITOR__WatchdogInitDisabledTimestampDiffRemainder=$(($MONITOR_APPSETTING_DEFAULT__WatchdogReloadDelay-$MONITOR__WatchdogInitDisabledTimestampDiff))
            LogMessage "watchdog.main will be reloaded soon (${MONITOR__WatchdogInitDisabledTimestampDiffRemainder})"
        fi
    fi
else
    if [ -f "${FILE__MonitorWatchdogMainDisabledTimestamp}" ]; then
        rm -rf "${FILE__MonitorWatchdogMainDisabledTimestamp}"
    fi
fi


#endregion
#=============================================================================================================================

#=============================================================================================================================
#region "get values"
#get current values
LogMessage "get data"

MONITOR__machine_identifier=$("$TOOL__generate_identifier" --machine)
echo "${MONITOR__machine_identifier}" > "${FILE__MachineIdentifier}"


#Is renamed?
MONITOR__machine_was_renamed=""
MONITOR__machine_was_renamed__old_name=""
if [ -f "$FILE__MonitorMachineRenamed" ]; then
    MONITOR__machine_was_renamed="true"
    MONITOR__machine_was_renamed__old_name=$(<"$FILE__MonitorMachineRenamed")
fi


#Check if we already have a Monitor identifier, if not, generate it.
MONITOR__monitor_identifier=""
if [[ -f "$FILE__MonitorIdentifier" ]]; then
    MONITOR__monitor_identifier=$(<"$FILE__MonitorIdentifier")
fi
MONITOR_IDENTIFIER__LEN=${#MONITOR__monitor_identifier}

if [ $MONITOR_IDENTIFIER__LEN -ne 49 ]; then
    MONITOR__monitor_identifier=$("$TOOL__generate_identifier" --monitor)
    echo "${MONITOR__monitor_identifier}" > "${FILE__MonitorIdentifier}"
fi


MONITOR__machine_name=$(hostname -s) #Get machine name without domain info
MONITOR__idle_time=$WATCHDOG__idle_time
MONITOR__current_state=$WATCHDOG__state
MONITOR__terms_agreed=$SETTINGS__terms_agreed
MONITOR__reboot_count=$WATCHDOG__reboot_count
MONITOR__active_user=$(echo "show State:/Users/ConsoleUser" | scutil | awk '/Name :/ && ! /loginwindow/ { print $3 }')

#Get OS version
TMP__os_version=$(sw_vers -productVersion)
TMP__os_name=""
if [ -f "$OS_LICENSE_AGREEMENT_FILE" ]; then
    TMP__os_name=$( awk '/SOFTWARE LICENSE AGREEMENT FOR macOS/' "$OS_LICENSE_AGREEMENT_FILE" | awk -F 'macOS ' '{print $NF}' | awk '{print substr($0, 0, length($0)-1)}' )
    if [[ "$TMP__os_name" == *"SOFTWARE LICENSE AGREEMENT FOR macOS"* ]]; then
        TMP__os_name="NAME_NOT_FOUND"
    fi
else
    TMP__os_name="NAME_UNKNOWN"
fi
MONITOR__os_version_with_name=$(echo "$TMP__os_version ($TMP__os_name)")

MONITOR__chipset=$(sysctl -n machdep.cpu.brand_string)

#Get system shutdown/reboot timestamps, and uptime
TMP__ts_boot=`sysctl -n kern.boottime | cut -d" " -f4 | cut -d"," -f1`
TMP__ts_now=`date +%s`
MONITOR__os_uptime=$(echo $(($TMP__ts_now-$TMP__ts_boot)))
TMP__last_reboot_shutdown=$("$TOOL__GetLastRebootShutdown")
MONITOR__last_reboot=$(echo ${TMP__last_reboot_shutdown} | awk '{ print $5}')
if [ -z "${MONITOR__last_reboot}" ]; then
    MONITOR__last_reboot=0
fi
MONITOR__last_reboot=$(echo $(($TMP__ts_now-$MONITOR__last_reboot)))
MONITOR__last_shutdown=$(echo ${TMP__last_reboot_shutdown} | awk '{ print $2}')
if [ -z "${MONITOR__last_shutdown}" ]; then
    MONITOR__last_shutdown=0
fi
MONITOR__last_shutdown=$(echo $(($TMP__ts_now-$MONITOR__last_shutdown)))
if [[ -n "$MONITOR__active_user" ]]; then
    MONITOR__os_language=$(su ${MONITOR__active_user} -c 'defaults read .GlobalPreferences AppleLocale')
else
    MONITOR__os_language="unknown"
fi

#Get Ninja node id from Ninja Agent config
if [ -f "$NINJA_PROGRAMDATA_FILE" ]; then
    MONITOR__ninja_nodeid=$(cat "$NINJA_PROGRAMDATA_FILE" | grep nodeId | sed -E 's/^([^0-9]+)([0-9]+)$/\2/g')
else
    MONITOR__ninja_nodeid=0
fi


#Get timezone in form of UTC+0100
MONITOR__timezone_offset=$(date +"UTC%z")

#Get latest activity from internal db
MONITOR__last_usage_timestamp=$(DbQuerySessions "SELECT timestamp FROM History_Sessions WHERE action='terms|accepted' ORDER BY timestamp DESC LIMIT 1" "0")
MONITOR__last_screensaver_timestamp=$(DbQuerySessions "SELECT timestamp FROM History_Sessions WHERE action='screensaver|start' ORDER BY timestamp DESC LIMIT 1" "0")

if [ -z "${MONITOR__last_usage_timestamp}" ]; then
    MONITOR__last_usage_timestamp="0"
fi

if [ -z "${MONITOR__last_screensaver_timestamp}" ]; then
    MONITOR__last_screensaver_timestamp="0"
fi


#get network details
JSON_REQUEST__heartbeat_networkdetails="$($TOOL__jq -n --argjson network_details "[]" '$ARGS.named')"
while read -r line; do
    sname=$(echo "$line" | awk -F  "(, )|(: )|[)]" '{print $2}')
    sdev=$(echo "$line" | awk -F  "(, )|(: )|[)]" '{print $4}')
    if [ -n "$sdev" ]; then
        ifout="$(ifconfig "$sdev" 2>/dev/null)"
        echo "$ifout" | grep 'status: active' > /dev/null 2>&1
        rc="$?"
        if [ "$rc" -eq 0 ]; then
            currentservice="$sname"
            currentdevice="$sdev"
            currentmac=$(echo "$ifout" | awk '/ether/{print $2}')
            currentipv6=$(echo "$ifout" | awk '/inet6/{print $2}')
            currentipv4=$(echo "$ifout" | awk '/broadcast/{print $2}')

            JSON_REQUEST__heartbeat_networkdetails=$(AddNetworkDetailsJSON "${JSON_REQUEST__heartbeat_networkdetails}" "${currentservice}" "${currentipv4}" "${currentipv6}" "${currentmac}")
        fi
    fi
done <<< "$(networksetup -listnetworkserviceorder | grep 'Hardware Port')"

JSON_REQUEST__heartbeat_networkdetails=$(echo "$JSON_REQUEST__heartbeat_networkdetails" | $TOOL__jq '.network_details')


#get city & country
MONITOR__last_geolocation_sync=0
if [[ -f "$FILE__MonitorLastGeolocationSync" ]]; then
    MONITOR__last_geolocation_sync=$(<"$FILE__MonitorLastGeolocationSync")
fi
MONITOR__last_geolocation_diff=$(( now_timestamp - MONITOR__last_geolocation_sync ))
JSON_REQUEST__heartbeat_geolocation=""
if (( MONITOR__last_geolocation_diff > MONITOR_APPSETTING__LastGeolocationSyncDelay )); then
    LogMessage "geolocation"
    MONITOR__TMPFILE_geolocation=$(mktemp /tmp/.ovcc.monitor.geolocation.XXXXXXXX)

    JSON_RESPONSE__geolocation_httpcode=$("$TOOL__curl" -o "${MONITOR__TMPFILE_geolocation}" -w "%{http_code}" -s -H "Accept: application/json" "${URL_MONITOR__geolocation}")
    JSON_RESPONSE__geolocation__exit_code=$?

    if [ "$JSON_RESPONSE__geolocation__exit_code" != "0" ]; then
        LogError "Geolocation connection error ${JSON_RESPONSE__heartbeat__exit_code}"
    else
        LogDebug "Geolocation response http code:"
        LogDebug ${JSON_RESPONSE__geolocation_httpcode}

        if [ "${JSON_RESPONSE__geolocation_httpcode}" != "200" ]; then
            LogError "Invalid http response (${JSON_RESPONSE__geolocation_httpcode})"
        else
            JSON_RESPONSE__geolocation=$(<"$MONITOR__TMPFILE_geolocation")
            rm -rf "$MONITOR__TMPFILE_geolocation"

            TMP__MONITOR__geolocation__city=$(echo "$JSON_RESPONSE__geolocation" | $TOOL__jq '.city')
            TMP__MONITOR__geolocation__country=$(echo "$JSON_RESPONSE__geolocation" | $TOOL__jq '.country')
            TMP__MONITOR__geolocation__location=$(echo "$JSON_RESPONSE__geolocation" | $TOOL__jq '.loc')
            TMP__MONITOR__geolocation__timezone=$(echo "$JSON_RESPONSE__geolocation" | $TOOL__jq '.timezone')

            OLD_IFS=$IFS
            IFS=","
            read TMP__MONITOR__geolocation__latitude TMP__MONITOR__geolocation__longitude < <(echo "${TMP__MONITOR__geolocation__location}")
            IFS=$OLD_IFS

            #remove quotes
            TMP__MONITOR__geolocation__city=${TMP__MONITOR__geolocation__city//\"/}
            TMP__MONITOR__geolocation__country=${TMP__MONITOR__geolocation__country//\"/}
            TMP__MONITOR__geolocation__latitude=${TMP__MONITOR__geolocation__latitude//\"/}
            TMP__MONITOR__geolocation__longitude=${TMP__MONITOR__geolocation__longitude//\"/}
            TMP__MONITOR__geolocation__timezone=${TMP__MONITOR__geolocation__timezone//\"/}

            #build geolocation json
            JSON_REQUEST__heartbeat_geolocation=$($TOOL__jq -n \
                --arg City "${TMP__MONITOR__geolocation__city}" \
                --arg Country "${TMP__MONITOR__geolocation__country}" \
                --arg Latitude "${TMP__MONITOR__geolocation__latitude}" \
                --arg Longitude "${TMP__MONITOR__geolocation__longitude}" \
                --arg Timezone "${TMP__MONITOR__geolocation__timezone}" \
                '$ARGS.named')


            echo "${now_timestamp}" > "$FILE__MonitorLastGeolocationSync"

            LogMessage "geolocation ok"
        fi
    fi
fi

#debugging
DEBUGGING_MONITOR_DATA=$(set -o posix; set | sed -e '/^_=*/d' | grep "MONITOR__")
LogDebug "loaded data"
LogDebug "${DEBUGGING_MONITOR_DATA}"
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "heartbeat"
#Get previous successful heartbeat
LogDebug "============"
LogMessage "heartbeat"
MONITOR__last_heartbeat_success="${now_timestamp}"
if [[ -f "$FILE__MonitorLastHeartbeatSuccess" ]]; then
    MONITOR__last_heartbeat_success=$(<"$FILE__MonitorLastHeartbeatSuccess")
fi
MONITOR__last_heartbeat_error=""
if [[ -f "$FILE__MonitorLastHeartbeatError" ]]; then
    MONITOR__last_heartbeat_error=$(<"$FILE__MonitorLastHeartbeatError")
fi


#region "detect downtime"
#Detect downtime
JSON_REQUEST__heartbeat_downtime=""
MONITOR__heartbeat_diff=$(( now_timestamp - MONITOR__last_heartbeat_success ))
if (( MONITOR__heartbeat_diff > MONITOR_APPSETTING__DowntimeThresholdSeconds )); then
    LogMessage "downtime detected"

    #uh oh, downtime
    TMP__duration=$MONITOR__heartbeat_diff

    #find the starting day (set time to 00:00:00 for MONITOR__last_heartbeat_success)
    TMP__start_date=$(StartOfDayTimestamp ${MONITOR__last_heartbeat_success})

    #find the downtime on the first day
    TMP__first_day_duration=$(( MONITOR__last_heartbeat_success - TMP__start_date ))
    TMP__first_day_duration=$(echo ${TMP__first_day_duration#-})
    TMP__first_day_duration=$(( SECONDS_IN_DAY - TMP__first_day_duration))

    #initial downtime JSON
    JSON_REQUEST__heartbeat_downtime=$($TOOL__jq -n \
        --argjson Start "${MONITOR__last_heartbeat_success}" \
        --argjson End "${now_timestamp}" \
        --arg Error "${MONITOR__last_heartbeat_error}" \
        --argjson Duration [] \
        '$ARGS.named')

    if (( TMP__duration > TMP__first_day_duration )); then
        read TMP__duration_year TMP__duration_month TMP__duration_day < <(GetYearMonthDayFromTimestamp "${TMP__start_date}")

        #add to downtime JSON
        JSON_REQUEST__heartbeat_downtime=$(AddDowntimeJSON "${JSON_REQUEST__heartbeat_downtime}" "${TMP__duration_year}" "${TMP__duration_month}" "${TMP__duration_day}" "${TMP__first_day_duration}")

        #subtract initial downtime
        TMP__duration=$(( TMP__duration - TMP__first_day_duration ))

        #loop over all downtime up till now
        while [ $TMP__duration -gt 0 ];
        do
            TMP__start_date=$(( TMP__start_date + SECONDS_IN_DAY ))

            read TMP__duration_year TMP__duration_month TMP__duration_day < <(GetYearMonthDayFromTimestamp "${TMP__start_date}")

            if (( TMP__duration > SECONDS_IN_DAY )); then
                TMP_TMP__duration=$SECONDS_IN_DAY
                TMP__duration=$(( TMP__duration - SECONDS_IN_DAY ))
            else
                TMP_TMP__duration=$TMP__duration
                TMP__duration=0
            fi

            JSON_REQUEST__heartbeat_downtime=$(AddDowntimeJSON "${JSON_REQUEST__heartbeat_downtime}" "${TMP__duration_year}" "${TMP__duration_month}" "${TMP__duration_day}" "${TMP_TMP__duration}")
        done
    else
        #only downtime on one day
        read TMP__duration_year TMP__duration_month TMP__duration_day < <(GetYearMonthDayFromTimestamp "${TMP__start_date}")

        JSON_REQUEST__heartbeat_downtime=$(AddDowntimeJSON "${JSON_REQUEST__heartbeat_downtime}" "${TMP__duration_year}" "${TMP__duration_month}" "${TMP__duration_day}" "${TMP__duration}")
    fi
fi
#endregion


#Building heartbeat JSON
JSON_REQUEST__heartbeat=$($TOOL__jq -n \
    --arg is_mac "true" \
    --arg machine_identifier "${MONITOR__machine_identifier}" \
    --arg monitor_identifier "${MONITOR__monitor_identifier}" \
    --arg machine_name "${MONITOR__machine_name}" \
    --arg is_renamed "${MONITOR__machine_was_renamed}" \
    --arg old_machine_name "${MONITOR__machine_was_renamed__old_name}" \
    --arg ninja_nodeid "${MONITOR__ninja_nodeid}" \
    --arg current_user "${MONITOR__active_user}" \
    --arg os_version "${MONITOR__os_version_with_name}" \
    --arg os_language "${MONITOR__os_language}" \
    --arg chipset "${MONITOR__chipset}" \
    --arg timezone_offset "${MONITOR__timezone_offset}" \
    --argjson uptime_seconds "${MONITOR__os_uptime}" \
    --argjson last_reboot "${MONITOR__last_reboot}" \
    --argjson last_shutdown "${MONITOR__last_shutdown}" \
    --argjson last_usage_timestamp "${MONITOR__last_usage_timestamp}" \
    --argjson last_screensaver_timestamp "${MONITOR__last_screensaver_timestamp}" \
    --arg network_details "${JSON_REQUEST__heartbeat_networkdetails}" \
    --arg downtime "${JSON_REQUEST__heartbeat_downtime}" \
    --arg geolocation "${JSON_REQUEST__heartbeat_geolocation}" \
    '$ARGS.named')

LogDebug "Heartbeat request:"
LogDebug "${JSON_REQUEST__heartbeat}"

LogDebug "Heartbeat endpoint:"
LogDebug "${URL_MONITOR__heartbeat}"


#Sending heartbeat
JSON_RESPONSE__heartbeat=$("$TOOL__curl" -s -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "${JSON_REQUEST__heartbeat}" "${URL_MONITOR__heartbeat}")
JSON_RESPONSE__heartbeat__exit_code=$?

if [ "$JSON_RESPONSE__heartbeat__exit_code" != "0" ]; then
    echo "Heartbeat connection error ${JSON_RESPONSE__heartbeat__exit_code}" > "${FILE__MonitorLastHeartbeatError}"

    LogError "Heartbeat connection error ${JSON_RESPONSE__heartbeat__exit_code}"
    exit 11
fi


#Get the JSON body
JSON_RESPONSE__heartbeat__body=$(echo "$JSON_RESPONSE__heartbeat" | $TOOL__jq '.body')
JSON_RESPONSE__heartbeat__body=$(echo "$JSON_RESPONSE__heartbeat__body" | sed -E 's/^"(.+)"$/\1/g')
JSON_RESPONSE__heartbeat__body=$(echo "$JSON_RESPONSE__heartbeat__body" | sed -E 's/\\"/"/g')

LogDebug "Heartbeat response:"
LogDebug ${JSON_RESPONSE__heartbeat__body}

JSON_RESPONSE__heartbeat_response_code=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq '.code')
if [ "$JSON_RESPONSE__heartbeat_response_code" != "0" ]; then
    echo "Heartbeat response error ${JSON_RESPONSE__heartbeat_response_code}" > "${FILE__MonitorLastHeartbeatError}"

    LogError "Heartbeat response error ${JSON_RESPONSE__heartbeat_response_code}"
    exit 12
fi

MONITOR__machine_id=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq '.machine_id')
MONITOR__last_heartbeat=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq '.last_heartbeat')
MONITOR__last_inventory_sync=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq '.last_inventory_sync')
MONITOR__last_metrics_sync=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq '.last_metrics_sync')
MONITOR__last_appsettings_sync=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq '.last_appsettings_sync')
MONITOR__last_ovccsettings_sync=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq '.last_ovccsettings_sync')
MONITOR__hidden_reason=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq -r '.hidden_reason // ""')
MONITOR__new_machine_name=$(echo "$JSON_RESPONSE__heartbeat__body" | $TOOL__jq -r '.new_machine_name // ""')


#Store stuff
echo "${MONITOR__machine_id}" > "${FILE__MonitorMachineId}"
echo "${MONITOR__last_heartbeat}" > "${FILE__MonitorLastHeartbeatSuccess}"
echo -n "" > "$FILE__MonitorLastHeartbeatError"
echo "${MONITOR__last_inventory_sync}" > "${FILE__MonitorLastInventorySync}"
echo "${MONITOR__last_metrics_sync}" > "${FILE__MonitorLastMetricsSync}"


#Delete stuff
if [ "${MONITOR__machine_was_renamed}" == "true" ]; then
    MONITOR__machine_was_renamed=""
    MONITOR__machine_was_renamed__old_name=""
    rm -rf "${FILE__MonitorMachineRenamed}"
fi


LogMessage "heartbeat ok"
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "machine disabling"
LogDebug "MONITOR__hidden_reason=${MONITOR__hidden_reason}"
if [ "${MONITOR__hidden_reason}" == "Disabled with warning" ]; then
    LogWarning "machine is disabled!"

    TMP__is_already_disabled=$(GetSetting "Settings" "theme_name" "")

    if [ "${TMP__is_already_disabled}" != "__disabled" ]; then
        if [ -d "${PATH__themes}/__disabled" ]; then
            LogMessage "removing current __disabled theme"
            rm -rf "${PATH__themes}/__disabled"
        fi

        #extract __disabled theme
        LogMessage "unpacking __disabled theme"
        unzip -o "${PATH__Misc}/__disabled.zip" -d "${PATH__themes}/"

        #permissions
        chown -R root:_www "${PATH__themes}/__disabled"
        chown -R root:_www "${PATH__themes}/__disabled/"*
        chown -R root:_www "${PATH__themes}/__disabled/".[^.]* 2>/dev/null

        chmod -R g+rx "${PATH__themes}/__disabled"
        chmod -R g+rx "${PATH__themes}/__disabled/"*
        chmod -R g+rx "${PATH__themes}/__disabled/".[^.]* 2>/dev/null

        chmod -R o-rx "${PATH__themes}/__disabled"
        chmod -R o-rx "${PATH__themes}/__disabled/"*
        chmod -R o-rx "${PATH__themes}/__disabled/".[^.]* 2>/dev/null

        #set active to __disabled
        LogMessage "set current theme to __disabled"
        DbQuery "UPDATE Settings SET value='__disabled' WHERE name='theme_name'"

        #trigger settings sync
        LogMessage "trigger settings sync"
        touch "${PATH__AgentTriggers}/SyncSettings/sync.settings"

        #Restart Homepage
        sleep 5
        LogMessage "restart Homepage"
        kill -9 $(ps aux | grep "OVCC - Homepage" | grep -v grep | awk '{print $2}')
    else
        LogWarning "theme already set to __disabled"
    fi
fi
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "machine renaming"
TMP__MONITOR__new_machine_name__length=${#MONITOR__new_machine_name}
if (( TMP__MONITOR__new_machine_name__length > 11 )); then
    if [ "${MONITOR__new_machine_name}" != "${MONITOR__machine_name}" ]; then
        LogMessage "renaming machine: ${MONITOR__machine_name} ==> ${MONITOR__new_machine_name}"
        scutil --set HostName "${MONITOR__new_machine_name}"
        RENAME_SCUTIL__exit_code=$?
        if [ "$RENAME_SCUTIL__exit_code" != "0" ]; then
            LogError "Something went wrong when renaming!!"
        else
            MONITOR__machine_was_renamed__old_name=$MONITOR__machine_name
            echo "${MONITOR__machine_was_renamed__old_name}" > "${FILE__MonitorMachineRenamed}"
            LogMessage "ok"
        fi
    fi
fi
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "app settings"
#Check if it's time to update app settings
MONITOR__last_appsettings_sync__current=-1
if [[ -f "$FILE__MonitorLastAppSettingsSync" ]]; then
    MONITOR__last_appsettings_sync__current=$(<"${FILE__MonitorLastAppSettingsSync}")
fi
LogDebug "MONITOR__last_appsettings_sync: ${MONITOR__last_appsettings_sync}"
LogDebug "MONITOR__last_appsettings_sync__current: ${MONITOR__last_appsettings_sync__current}"
if [ -f "${FILE__MonitorForcePostUpdateRefresh}" ]; then
    LogMessage "post-update app settings"
    MONITOR__last_appsettings_sync__current=-1
fi
if (( MONITOR__last_appsettings_sync > MONITOR__last_appsettings_sync__current || MONITOR__last_appsettings_sync__current <0 )); then
    #App settings
    LogDebug "============"
    LogMessage "app settings"

    JSON_REQUEST__appsettings=$($TOOL__jq -n \
        --arg machine_id "${MONITOR__machine_id}" \
        --arg machine_identifier "${MONITOR__machine_identifier}" \
        '$ARGS.named')

    LogDebug "Appsettings request:"
    LogDebug "${JSON_REQUEST__appsettings}"

    #sending app settings request
    JSON_RESPONSE__appsettings=$("$TOOL__curl" -s -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "${JSON_REQUEST__appsettings}" "${URL_MONITOR__appsettings}")
    JSON_RESPONSE__appsettings__exit_code=$?

    if [ "$JSON_RESPONSE__appsettings__exit_code" != "0" ]; then
        LogError "Appsettings connection error ${JSON_RESPONSE__appsettings__exit_code}"
    else
        #Get the JSON body
        JSON_RESPONSE__appsettings__body=$(echo "$JSON_RESPONSE__appsettings" | $TOOL__jq '.body')
        JSON_RESPONSE__appsettings__body=$(echo "$JSON_RESPONSE__appsettings__body" | sed -E 's/^"(.+)"$/\1/g')
        JSON_RESPONSE__appsettings__body=$(echo "$JSON_RESPONSE__appsettings__body" | sed -E 's/\\"/"/g')


        LogDebug "Appsettings response:"
        LogDebug ${JSON_RESPONSE__appsettings__body}

        JSON_RESPONSE__appsettings_response_code=$(echo "$JSON_RESPONSE__appsettings__body" | $TOOL__jq '.code')
        if [ "$JSON_RESPONSE__appsettings_response_code" != "0" ]; then
            LogError "Appsettings response error ${JSON_RESPONSE__appsettings_response_code}"
        else
            MONITOR_APPSETTING__LastInventorySyncDelay=$(echo "$JSON_RESPONSE__appsettings__body" | $TOOL__jq -r '.app_settings.LastInventorySyncDelay')
            MONITOR_APPSETTING__LastGeolocationSyncDelay=$(echo "$JSON_RESPONSE__appsettings__body" | $TOOL__jq -r '.app_settings.LastGeolocationSyncDelay')
            MONITOR_APPSETTING__LastMetricsSyncDelay=$(echo "$JSON_RESPONSE__appsettings__body" | $TOOL__jq -r '.app_settings.LastMetricsSyncDelay')
            MONITOR_APPSETTING__DowntimeThresholdSeconds=$(echo "$JSON_RESPONSE__appsettings__body" | $TOOL__jq -r '.app_settings.DowntimeThresholdSeconds')
            MONITOR_APPSETTING__MonitorUpdateEnabled=$(echo "$JSON_RESPONSE__appsettings__body" | $TOOL__jq -r '.app_settings.Monitor_UpdateEnabled')

            DEBUGGING_MONITOR_APPSETTINGS=$(set -o posix; set | sed -e '/^_=*/d' | grep "MONITOR_APPSETTING__")
            LogDebug "received settings"
            LogDebug "${DEBUGGING_MONITOR_APPSETTINGS}"


            DbQueryMonitorSettings "INSERT OR IGNORE INTO Monitor_Settings (name,value) VALUES ('LastInventorySyncDelay','${MONITOR_APPSETTING__LastInventorySyncDelay}')"
            DbQueryMonitorSettings "UPDATE Monitor_Settings SET value='${MONITOR_APPSETTING__LastInventorySyncDelay}' WHERE name='LastInventorySyncDelay'"

            DbQueryMonitorSettings "INSERT OR IGNORE INTO Monitor_Settings (name,value) VALUES ('LastGeolocationSyncDelay','${MONITOR_APPSETTING__LastGeolocationSyncDelay}')"
            DbQueryMonitorSettings "UPDATE Monitor_Settings SET value='${MONITOR_APPSETTING__LastGeolocationSyncDelay}' WHERE name='LastGeolocationSyncDelay'"

            DbQueryMonitorSettings "INSERT OR IGNORE INTO Monitor_Settings (name,value) VALUES ('LastMetricsSyncDelay','${MONITOR_APPSETTING__LastMetricsSyncDelay}')"
            DbQueryMonitorSettings "UPDATE Monitor_Settings SET value='${MONITOR_APPSETTING__LastMetricsSyncDelay}' WHERE name='LastMetricsSyncDelay'"

            DbQueryMonitorSettings "INSERT OR IGNORE INTO Monitor_Settings (name,value) VALUES ('DowntimeThresholdSeconds','${MONITOR_APPSETTING__DowntimeThresholdSeconds}')"
            DbQueryMonitorSettings "UPDATE Monitor_Settings SET value='${MONITOR_APPSETTING__DowntimeThresholdSeconds}' WHERE name='DowntimeThresholdSeconds'"

            DbQueryMonitorSettings "INSERT OR IGNORE INTO Monitor_Settings (name,value) VALUES ('Monitor_UpdateEnabled','${MONITOR_APPSETTING__MonitorUpdateEnabled}')"
            DbQueryMonitorSettings "UPDATE Monitor_Settings SET value='${MONITOR_APPSETTING__MonitorUpdateEnabled}' WHERE name='Monitor_UpdateEnabled'"


            LogDebug "MONITOR__last_heartbeat=${MONITOR__last_heartbeat}"
            echo "${MONITOR__last_heartbeat}" > "$FILE__MonitorLastAppSettingsSync"

            LogMessage "appsettings ok"
        fi
    fi
fi
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "ovcc settings"
#Check if it's time to update ovcc settings
MONITOR__last_ovccsettings_sync__current=-1
if [[ -f "$FILE__MonitorLastOVCCSettingsSync" ]]; then
    MONITOR__last_ovccsettings_sync__current=$(<"${FILE__MonitorLastOVCCSettingsSync}")
fi
LogDebug "MONITOR__last_ovccsettings_sync: ${MONITOR__last_ovccsettings_sync}"
LogDebug "MONITOR__last_ovccsettings_sync__current: ${MONITOR__last_ovccsettings_sync__current}"
if [ -f "${FILE__MonitorForcePostUpdateRefresh}" ]; then
    LogMessage "post-update ovcc settings"
    MONITOR__last_ovccsettings_sync__current=-1
fi
if (( MONITOR__last_ovccsettings_sync > MONITOR__last_ovccsettings_sync__current || MONITOR__last_ovccsettings_sync__current <0 )); then
    #OVCC settings
    LogDebug "============"
    LogMessage "OVCC settings"

    JSON_REQUEST__ovccsettings=$($TOOL__jq -n \
        --arg machine_id "${MONITOR__machine_id}" \
        --arg machine_identifier "${MONITOR__machine_identifier}" \
        '$ARGS.named')

    LogDebug "OVCCsettings request:"
    LogDebug "${JSON_REQUEST__ovccsettings}"

    #sending app settings request
    JSON_RESPONSE__ovccsettings=$("$TOOL__curl" -s -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "${JSON_REQUEST__ovccsettings}" "${URL_MONITOR__ovccsettings}")
    JSON_RESPONSE__ovccsettings__exit_code=$?

    if [ "$JSON_RESPONSE__ovccsettings__exit_code" != "0" ]; then
        LogError "OVCCsettings connection error ${JSON_RESPONSE__ovccsettings__exit_code}"
    else
        #Get the JSON body
        JSON_RESPONSE__ovccsettings__body=$(echo "$JSON_RESPONSE__ovccsettings" | $TOOL__jq '.body')
        JSON_RESPONSE__ovccsettings__body=$(echo "$JSON_RESPONSE__ovccsettings__body" | sed -E 's/^"(.+)"$/\1/g')
        JSON_RESPONSE__ovccsettings__body=$(echo "$JSON_RESPONSE__ovccsettings__body" | sed -E 's/\\"/"/g')


        LogDebug "OVCCsettings response:"
        LogDebug ${JSON_RESPONSE__ovccsettings__body}

        JSON_RESPONSE__ovccsettings_response_code=$(echo "$JSON_RESPONSE__ovccsettings__body" | $TOOL__jq '.code')
        if [ "$JSON_RESPONSE__ovccsettings_response_code" != "0" ]; then
            LogError "OVCCsettings response error ${JSON_RESPONSE__ovccsettings_response_code}"
        else
            MONITOR_OVCCSETTING__ExpectedFileVersion_Monitor=$(echo "$JSON_RESPONSE__ovccsettings__body" | $TOOL__jq -r '.ovcc_settings.ExpectedFileVersion_Monitor')

            DEBUGGING_MONITOR_OVCCSETTINGS=$(set -o posix; set | sed -e '/^_=*/d' | grep "MONITOR_OVCCSETTING__")
            LogDebug "received settings"
            LogDebug "${DEBUGGING_MONITOR_OVCCSETTINGS}"

            DbQueryMonitorSettings "INSERT OR IGNORE INTO Monitor_Settings (name,value) VALUES ('ExpectedFileVersion_Monitor','${MONITOR_OVCCSETTING__ExpectedFileVersion_Monitor}')"
            DbQueryMonitorSettings "UPDATE Monitor_Settings SET value='${MONITOR_OVCCSETTING__ExpectedFileVersion_Monitor}' WHERE name='ExpectedFileVersion_Monitor'"

            echo $MONITOR__last_heartbeat > "$FILE__MonitorLastOVCCSettingsSync"

            LogMessage "OVCCsettings ok"
        fi
    fi
fi
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "inventory"
#Check if it's time for inventory
MONITOR__DIFF__last_inventory_sync=$(( now_timestamp - MONITOR__last_inventory_sync ))
LogDebug "MONITOR__DIFF__last_inventory_sync: ${MONITOR__DIFF__last_inventory_sync}"
if [ -f "${FILE__MonitorForcePostUpdateRefresh}" ]; then
    LogMessage "post-update inventory"
    MONITOR__DIFF__last_inventory_sync=$(( MONITOR_APPSETTING__LastInventorySyncDelay + 1 ))
fi
if (( MONITOR__DIFF__last_inventory_sync > MONITOR_APPSETTING__LastInventorySyncDelay )); then
    #Inventory
    LogDebug "============"
    LogMessage "inventory"

    MONITOR__ovccmac_version=$(DbQuery "SELECT version_major,version_minor,version_build FROM Version_History ORDER BY version_major DESC,version_minor DESC,version_build DESC LIMIT 1" "0")
    MONITOR__ovccmac_version=$(echo "$MONITOR__ovccmac_version" | sed -E 's/\|/\./g')

    MONITOR__homepage_running=$(GetApplicationStatus "Homepage")

    MONITOR__daemon_loaded__logger=$(GetDaemonStatus "loggerdaemon")
    MONITOR__daemon_loaded__watchdog_init=$(GetDaemonStatus "watchdog\.init")
    MONITOR__daemon_loaded__watchdog_main=$(GetDaemonStatus "watchdog\.main")
    MONITOR__daemon_loaded__logout=$(GetDaemonStatus "logout")
    MONITOR__daemon_loaded__reboot=$(GetDaemonStatus "reboot")


    JSON_REQUEST__inventory=$($TOOL__jq -n \
    --arg is_mac "true" \
    --arg machine_identifier "${MONITOR__machine_identifier}" \
    --arg machine_id "${MONITOR__machine_id}" \
    --arg ovccmac_version "${MONITOR__ovccmac_version}" \
    --arg monitor_version "${VERSION_MONITOR}" \
    --arg homepage_running "${MONITOR__homepage_running}" \
    --arg daemon_loaded__logger "${MONITOR__daemon_loaded__logger}" \
    --arg daemon_loaded__watchdog_init "${MONITOR__daemon_loaded__watchdog_init}" \
    --arg daemon_loaded__watchdog_main "${MONITOR__daemon_loaded__watchdog_main}" \
    --arg daemon_loaded__logout "${MONITOR__daemon_loaded__logout}" \
    --arg daemon_loaded__reboot "${MONITOR__daemon_loaded__reboot}" \
    '$ARGS.named')

    LogDebug "Inventory request:"
    LogDebug "${JSON_REQUEST__inventory}"

    #sending heartbeat
    JSON_RESPONSE__inventory=$("$TOOL__curl" -s -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "${JSON_REQUEST__inventory}" "${URL_MONITOR__inventory}")
    JSON_RESPONSE__inventory__exit_code=$?

    if [ "$JSON_RESPONSE__inventory__exit_code" != "0" ]; then
        LogError "Heartbeat connection error ${JSON_RESPONSE__inventory__exit_code}"
    else
        #Get the JSON body
        JSON_RESPONSE__inventory__body=$(echo "$JSON_RESPONSE__inventory" | $TOOL__jq '.body')
        JSON_RESPONSE__inventory__body=$(echo "$JSON_RESPONSE__inventory__body" | sed -E 's/^"(.+)"$/\1/g')
        JSON_RESPONSE__inventory__body=$(echo "$JSON_RESPONSE__inventory__body" | sed -E 's/\\"/"/g')


        LogDebug "Inventory response:"
        LogDebug ${JSON_RESPONSE__inventory__body}

        JSON_RESPONSE__inventory_response_code=$(echo "$JSON_RESPONSE__inventory__body" | $TOOL__jq '.code')
        if [ "$JSON_RESPONSE__inventory_response_code" != "0" ]; then
            LogError "Inventory response error ${JSON_RESPONSE__inventory_response_code}"
        else
            LogMessage "inventory ok"
        fi
    fi
fi
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "metrics"
#Check if it's time for metrics
MONITOR__DIFF__last_metrics_sync=$(( now_timestamp - MONITOR__last_metrics_sync ))
if (( MONITOR__DIFF__last_metrics_sync > MONITOR_APPSETTING__LastMetricsSyncDelay )); then
    #Metrics
    LogMessage "metrics"

    if (( MONITOR__DIFF__last_metrics_sync > MONITOR_APPSETTING_DEFAULT__MaxLogfileHistory*SECONDS_IN_DAY )); then
        TMP__metrics_firstday=$(( now_timestamp - MONITOR_APPSETTING_DEFAULT__MaxLogfileHistory*SECONDS_IN_DAY ))
        TMP__metrics_firstday=$(StartOfDayTimestamp ${TMP__metrics_firstday})
    else
        TMP__metrics_firstday=$(( now_timestamp - SECONDS_IN_DAY ))
        TMP__metrics_firstday=$(StartOfDayTimestamp ${TMP__metrics_firstday})
    fi

    TMP__metrics_tomorrow=$(( now_timestamp + SECONDS_IN_DAY ))
    TMP__metrics_tomorrow=$(StartOfDayTimestamp ${TMP__metrics_tomorrow})

    TMP__metrics_day=$TMP__metrics_firstday


    JSON_REQUEST__metrics_data=$($TOOL__jq -n \
        --argjson metrics [] \
        '$ARGS.named')


    #loop over days
    while [ $TMP__metrics_day -lt $TMP__metrics_tomorrow ];
    do
        TMP__metrics_day_date=$(date -r ${TMP__metrics_day} '+%Y-%m-%d %H:%M:%S')
        TMP__metrics_day_next=$(( TMP__metrics_day + SECONDS_IN_DAY ))
        read TMP__metrics_only_year TMP__metrics_only_month TMP__metrics_only_day < <(GetYearMonthDayFromTimestamp "${TMP__metrics_day}")

        TMP__metrics_usersessions_start=$(DbQuerySessions "SELECT timestamp,session_id FROM History_Sessions WHERE action='login' AND timestamp>=${TMP__metrics_day} AND timestamp<${TMP__metrics_day_next} ORDER BY timestamp ASC" "")
        TMP__metrics_screensavers_start=$(DbQuerySessions "SELECT timestamp,session_id FROM History_Sessions WHERE action='screensaver|start' AND timestamp>=${TMP__metrics_day} AND timestamp<${TMP__metrics_day_next} ORDER BY timestamp ASC" "")
        TMP__metrics_urlvisitedcount=$(DbQuerySessions "SELECT COUNT(*) FROM History_Browsing WHERE timestamp>=${TMP__metrics_day} AND timestamp<${TMP__metrics_day_next}" "")


        #region "user sessions"
        TMP_ARR__metrics_usersessions_start=($TMP__metrics_usersessions_start)
        TMP__metrics_usersession_count=0
        TMP__metrics_usersession_duration=0
        for TMP_VAL__metrics_usersession_start in "${TMP_ARR__metrics_usersessions_start[@]}"
        do
            TMP__timestamp=${TMP_VAL__metrics_usersession_start%|*}
            TMP__session_id=${TMP_VAL__metrics_usersession_start#*|}

            TMP__metrics_usersession_end=$(DbQuerySessions "SELECT timestamp FROM History_Sessions WHERE (action='bootout|ok' OR action='reset' OR action='reboot') AND timestamp>${TMP__timestamp} ORDER BY timestamp ASC LIMIT 1" "")
            if [ -z "${TMP__metrics_usersession_end}" ]; then
                TMP__metrics_usersession_end=$TMP__timestamp
            fi

            TMP__usersession_length=$(( TMP__metrics_usersession_end - TMP__timestamp ))

            TMP__metrics_usersession_count=$(( TMP__metrics_usersession_count + 1))
            TMP__metrics_usersession_duration=$(( TMP__metrics_usersession_duration + TMP__usersession_length ))
        done
        #endregion

        #region "screensaver sessions"
        TMP_ARR__metrics_screensavers_start=($TMP__metrics_screensavers_start)
        TMP__metrics_screensaver_count=0
        TMP__metrics_screensaver_duration=0
        for TMP_VAL__metrics_screensaver_start in "${TMP_ARR__metrics_screensavers_start[@]}"
        do
            TMP__timestamp=${TMP_VAL__metrics_screensaver_start%|*}
            TMP__session_id=${TMP_VAL__metrics_screensaver_start#*|}

            TMP__metrics_screensaver_end=$(DbQuerySessions "SELECT timestamp FROM History_Sessions WHERE (action='screensaver|stop' OR action='bootout|ok' OR action='reset' OR action='reboot') AND timestamp>${TMP__timestamp} ORDER BY timestamp ASC LIMIT 1" "")
            if [ -z "${TMP__metrics_screensaver_end}" ]; then
                TMP__metrics_screensaver_end=$TMP__timestamp
            fi

            TMP__screensaver_length=$(( TMP__metrics_screensaver_end - TMP__timestamp ))

            TMP__metrics_screensaver_count=$(( TMP__metrics_screensaver_count + 1))
            TMP__metrics_screensaver_duration=$(( TMP__metrics_screensaver_duration + TMP__screensaver_length ))
        done
        #endregion

        #some stats are empty or 0, because they are Windows ony
        JSON_REQUEST__metrics_data_item=$($TOOL__jq -n \
            --arg Date "${TMP__metrics_day_date}" \
            --argjson Timestamp "${TMP__metrics_day}" \
            --argjson TimestampWithoutTime "${TMP__metrics_day}" \
            --argjson Year "${TMP__metrics_only_year}" \
            --argjson Month "${TMP__metrics_only_month}" \
            --argjson Day "${TMP__metrics_only_day}" \
            --argjson UserSessions [] \
            --argjson UserSessionCount "${TMP__metrics_usersession_count}" \
            --argjson UserSessionDuration "${TMP__metrics_usersession_duration}" \
            --argjson Screensavers [] \
            --argjson ScreensaverCount "${TMP__metrics_screensaver_count}" \
            --argjson ScreensaverDuration "${TMP__metrics_screensaver_duration}" \
            --argjson EmergencyScreens [] \
            --argjson EmergencyScreenCount "0" \
            --argjson SiteKioskCrashes [] \
            --argjson SiteKioskCrashCount "0" \
            --argjson SiteKioskScriptError [] \
            --argjson SiteKioskScriptErrorCount "0" \
            --argjson UnexpectedShutdowns [] \
            --argjson UnexpectedShutdownsCount "0" \
            --argjson UrlsVisited [] \
            --argjson UrlsVisitedCount "${TMP__metrics_urlvisitedcount}" \
            --arg ReturnMessage "" \
            --arg LogfilePath "" \
            --argjson LogfileExists "false" \
            '$ARGS.named')


        JSON_REQUEST__metrics_data=$(echo "${JSON_REQUEST__metrics_data}" | $TOOL__jq --argjson item "${JSON_REQUEST__metrics_data_item}" '.metrics += [ $item ]' )


        TMP__metrics_day=$TMP__metrics_day_next
    done

    JSON_REQUEST__metrics_data_array=$( echo "${JSON_REQUEST__metrics_data}" | $TOOL__jq '.metrics')
    JSON_REQUEST__metrics=$($TOOL__jq -n \
        --arg is_mac "true" \
        --arg machine_identifier "${MONITOR__machine_identifier}" \
        --arg machine_id "${MONITOR__machine_id}" \
        --arg metrics "${JSON_REQUEST__metrics_data_array}" \
        '$ARGS.named')


    LogDebug "Metrics request:"
    LogDebug "${JSON_REQUEST__metrics}"


    #sending metrics
    JSON_RESPONSE__metrics=$("$TOOL__curl" -s -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "${JSON_REQUEST__metrics}" "${URL_MONITOR__metrics}")
    JSON_RESPONSE__metrics__exit_code=$?

    if [ "$JSON_RESPONSE__metrics__exit_code" != "0" ]; then
        LogError "Metrics connection error ${JSON_RESPONSE__metrics__exit_code}"
    else
        #Get the JSON body
        JSON_RESPONSE__metrics__body=$(echo "$JSON_RESPONSE__metrics" | $TOOL__jq '.body')
        JSON_RESPONSE__metrics__body=$(echo "$JSON_RESPONSE__metrics__body" | sed -E 's/^"(.+)"$/\1/g')
        JSON_RESPONSE__metrics__body=$(echo "$JSON_RESPONSE__metrics__body" | sed -E 's/\\"/"/g')

        LogDebug "Metrics response:"
        LogDebug ${JSON_RESPONSE__metrics__body}

        JSON_RESPONSE__metrics_response_code=$(echo "$JSON_RESPONSE__metrics__body" | $TOOL__jq '.code')
        if [ "$JSON_RESPONSE__metrics_response_code" != "0" ]; then
            LogError "Metrics response error ${JSON_RESPONSE__metrics_response_code}"
        else
            LogMessage "metrics ok"
        fi
    fi
fi
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "clean up"
if [ -f "${FILE__MonitorForcePostUpdateRefresh}" ]; then
    LogMessage "removing post-update refresh trigger"
    rm -rf "${FILE__MonitorForcePostUpdateRefresh}"
fi

#make sure permissions are right
find "$PATH__MonitorMain" -type f -exec chmod 600 {} +
#endregion
#=============================================================================================================================


#=============================================================================================================================
#region "self update"
VersionCompare "${VERSION_MONITOR}" "${MONITOR_OVCCSETTING__ExpectedFileVersion_Monitor}"
VersionCompareResult=$?

if [ "${VersionCompareResult}" = "2" ]; then
    LogMessage "Update needed"
    LogMessage "Current version: ${VERSION_MONITOR} - Expected version: ${MONITOR_OVCCSETTING__ExpectedFileVersion_Monitor}"

    if [ "${MONITOR_APPSETTING__MonitorUpdateEnabled}" = "yes" ]; then
        JSON_REQUEST__selfupdate=$($TOOL__jq -n \
        --arg machine_identifier "${MONITOR__machine_identifier}" \
        --arg machine_id "${MONITOR__machine_id}" \
        '$ARGS.named')

        LogDebug "SelfUpdate request:"
        LogDebug "${JSON_REQUEST__selfupdate}"

        #sending heartbeat
        JSON_RESPONSE__selfupdate=$("$TOOL__curl" -s -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "${JSON_REQUEST__selfupdate}" "${URL_MONITOR__selfupdate}")
        JSON_RESPONSE__selfupdate__exit_code=$?

        if [ "$JSON_RESPONSE__selfupdate__exit_code" != "0" ]; then
            LogError "SelfUpdate connection error ${JSON_RESPONSE__selfupdate__exit_code}"
        else
            #Get the JSON body
            JSON_RESPONSE__selfupdate__body=$(echo "$JSON_RESPONSE__selfupdate" | $TOOL__jq '.body')
            JSON_RESPONSE__selfupdate__body=$(echo "$JSON_RESPONSE__selfupdate__body" | sed -E 's/^"(.+)"$/\1/g')
            JSON_RESPONSE__selfupdate__body=$(echo "$JSON_RESPONSE__selfupdate__body" | sed -E 's/\\"/"/g')


            LogDebug "SelfUpdate response:"
            LogDebug ${JSON_RESPONSE__selfupdate__body}

            JSON_RESPONSE__selfupdate_response_code=$(echo "$JSON_RESPONSE__selfupdate__body" | $TOOL__jq '.code')
            if [ "$JSON_RESPONSE__selfupdate_response_code" != "0" ]; then
                LogError "SelfUpdate response error ${JSON_RESPONSE__selfupdate_response_code}"
            else
                MONITOR__selfupdate__monitor_last_version=$(echo "$JSON_RESPONSE__selfupdate__body" | $TOOL__jq -r '.monitor_last_version')
                MONITOR__selfupdate__monitor_updateenabled=$(echo "$JSON_RESPONSE__selfupdate__body" | $TOOL__jq -r '.monitor_updateenabled')
                MONITOR__selfupdate__monitor_updateenabled=$(String2Boolean "${MONITOR__selfupdate__monitor_updateenabled}")

                if [ "${MONITOR__selfupdate__monitor_updateenabled}" != "1" ]; then
                    LogWarning "Updating is not enabled!"
                else
                    LogMessage "Available version: ${MONITOR__selfupdate__monitor_last_version}"

                    MONITOR__selfupdate__filename="OVCCMac-Monitor-SelfUpdater-Payload-${MONITOR__selfupdate__monitor_last_version}.tar.gz"

                    #LogDebug "'$TOOL__curl' --output /dev/null --silent --fail -r 0-0 '${URL_MONITOR_DOWNLOAD}/${MONITOR__selfupdate__filename}'"
                    "$TOOL__curl" --output /dev/null --silent --fail -r 0-0 "${URL_MONITOR_DOWNLOAD}/${MONITOR__selfupdate__filename}"
                    #LogDebug "ret=$?"
                    if [ "$?" != "0" ]; then
                        LogError "File not found: ${URL_MONITOR_DOWNLOAD}/${MONITOR__selfupdate__filename}"
                    else
                        MONITOR__TMP_DOWNLOAD=$(mktemp -d /tmp/.ovcc.monitor.payload.XXXXXXXX)
                        LogDebug "Create temp path: ${MONITOR__TMP_DOWNLOAD}"
                        if [ "$?" != "0" ]; then
                            LogError "Could not create temp folder"
                        else
                            MONITOR__TMP_DOWNLOAD_FILE="${MONITOR__TMP_DOWNLOAD}/${MONITOR__selfupdate__filename}"
                            "$TOOL__curl" -o "${MONITOR__TMP_DOWNLOAD_FILE}" "${URL_MONITOR_DOWNLOAD}/${MONITOR__selfupdate__filename}"

                            if [ "$?" != "0" ] || [ ! -f "${MONITOR__TMP_DOWNLOAD_FILE}" ]; then
                                LogError "Download failed!"
                            else
                                LogDebug "Unpacking"

                                MONITOR__TMP_DOWNLOAD_UNPACKED="${MONITOR__TMP_DOWNLOAD}/unpacked"
                                mkdir -p "${MONITOR__TMP_DOWNLOAD_UNPACKED}"
                                tar zxf "${MONITOR__TMP_DOWNLOAD_FILE}" -C "${MONITOR__TMP_DOWNLOAD_UNPACKED}"

                                MONITOR__TMP_UPDATER_SCRIPT="${MONITOR__TMP_DOWNLOAD_UNPACKED}/payload/_MONITOR_SELFUPDATER_SCRIPT.init.sh"

                                if [ ! -f "${MONITOR__TMP_UPDATER_SCRIPT}" ]; then
                                    LogError "Package does not contain updater script"
                                else
                                    LogMessage "Running updater script, see you on the other side"
                                    source "${MONITOR__TMP_UPDATER_SCRIPT}"

                                    exit 0
                                fi
                            fi
                        fi
                    fi
                fi
            fi
        fi
    else
        LogWarning "Updating disabled!"
    fi
fi
#endregion
#=============================================================================================================================

LogMessage "done"
