#!/bin/bash

#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi
#=============================================================================================================================

LogDebug "started"

current_user=$(id -un)
LogDebug "current_user=$current_user"

current_user="Guest"


if [ -d "$PATH__AgentTriggers/SyncHistory" ]; then
    LogDebug "removing trigger"
    rm -f "$PATH__AgentTriggers/SyncHistory/"*

    if [ "$current_user" == "Guest" ]; then
        SafariHistory_database="~/Library/Safari/History.db"
        SafariHistory_database="${SafariHistory_database/#\~/$HOME}"

        SessionId=$(<${FILE__SessionId})
        Timestamp=$(date +%s)
        TMP_FOLDER=$(mktemp -d /tmp/.ovcc.XXXXXXXX)
        TMP_FILE="${TMP_FOLDER}/.browsing.history"

        LogDebug "SessionId=${SessionId}"
        LogDebug "TMP_FILE=${TMP_FILE}"


        # Load Safari history into temp file
$TOOL__sqlite "$SafariHistory_database" <<!
.headers off
.mode list
.output "${TMP_FILE}"
select [url],[domain_expansion],'${SessionId}',${Timestamp} from history_items;
!

        chmod +rx ${TMP_FOLDER}
        chmod +r ${TMP_FILE}

        db_return=$(curl --fail -s --data-urlencode "file=${TMP_FILE}" "${URL__api}/save_sync_history.php")
        if [[ "$db_return" == "\"ok\"" ]]; then
            LogDebug "json__session_history ok"
        else
            LogError "json__session_history error: $db_return"
        fi


        # Delete temp file
        rm -rf "${TMP_FOLDER}"
    else
        LogDebug "Not Guest user"
    fi
else
    LogError "trigger dir not found ($PATH__AgentTriggers/SyncHistory), disabling!"
    $TOOL__launchctl unload /Library/LaunchAgents/com.guesttek.ovcc.mac.synchistory.plist
    exit 2
fi
