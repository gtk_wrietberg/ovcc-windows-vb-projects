#!/bin/bash


if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi

source "$parent_path/includes/_monitor_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_monitor_globals.sh"
    exit 3
fi


#=============================================================================================================================
LogMessage "reboot triggered"

sleep 1


#remove other triggers
rm -f "$PATH__AgentTriggers/OVCCHomepageWrapper/"*
rm -f "$PATH__AgentTriggers/WatchdogWarning/"*
rm -f "$PATH__AgentTriggers/Logout/"*
rm -f "$PATH__AgentTriggers/Screensaver/"*
rm -f "$FILE__AdminModeTrigger"

#remove watchdog re-load disabler for Monitor
rm -f "${FILE__MonitorDontReviveWatchdog}"


#prevent reboot loop due to file permissions
if [ -d "$PATH__AgentTriggers/Reboot" ]; then
    rm -f "$PATH__AgentTriggers/Reboot/"*

    sleep 1

    fcount=$(ls -A "$PATH__AgentTriggers/Reboot")
    if [ $fcount ]; then
        LogError "trigger not deleted! skipping"

        exit 1
    fi
else
    LogError "trigger dir not found ($PATH__AgentTriggers/Reboot), skipping!"

    exit 2
fi

sleep 1

now_timestamp=$(date +%s)

prevent_reboot_loop=$(GetSetting "Settings" "watchdog_reboot_loop_prevention" "1")
restore_guest_user_in_reboot_loop=$(GetSetting "Settings" "watchdog_restore_guest_user_in_reboot_loop" "1")
reboot_count_max=$(GetSetting "Settings" "watchdog_reboot_loop_max_tries" "2")
reboot_count=$(GetVarSetting "watchdog_reboot_count" "2")


#sanitise
if (( reboot_count_max < 2 )); then
    # a reboot max less than 2 doesn't make any sense
    reboot_count_max=2
fi
if (( prevent_reboot_loop != 0 )); then
    prevent_reboot_loop=1
fi
if (( restore_guest_user_in_reboot_loop != 0 )); then
    restore_guest_user_in_reboot_loop=1
fi

sleep 1


#reboot loop check
if (( prevent_reboot_loop == 1 )); then
    if (( reboot_count == (reboot_count_max-1) )); then
        if (( restore_guest_user_in_reboot_loop == 1 )); then
            #we might be in a loop
            #reboot one more time after restoring the Guest account
            LogWarning "possible reboot loop, attempting to restore Guest account"

            db_return=$(curl --fail -s "${URL__api}/json__session_history.php?action=reboot&message=guest_restore")
            if [[ "$db_return" == "\"ok\"" ]]; then
                LogDebug "ok"
            else
                LogError "error: $db_return"
            fi

            source "$PATH__Bin/scripts/Restore.guest.sh"

            sleep 2
        fi
    fi

    if (( reboot_count >= reboot_count_max )); then
        LogError "Reboot is in a loop, skipping reboot"

        db_return=$(curl --fail -s "${URL__api}/json__session_history.php?action=reboot&message=skipped_reboot_loop")
        if [[ "$db_return" == "\"ok\"" ]]; then
            LogDebug "ok"
        else
            LogError "error: $db_return"
        fi

        exit 3
    fi
else
    if (( reboot_count >= reboot_count_max )); then
        LogError "Reboot is in a loop, but reboot loop prevention is turned off"
    fi
fi


sleep 1


#reset terms just to be sure
SetVarSetting "terms_agreed" "0"

#Syncing history
SyncHistory_Script="${PATH__Bin}/scripts/SyncHistory.sh"
max_wait_count=5


LogDebug "triggering SyncHistory"

CreateTriggerFile "$PATH__AgentTriggers/SyncHistory/sync.history"

sleep 1

count_processes=1
wait_count=0

while (( count_processes>0 )) && (( wait_count<$max_wait_count ))
do
    sleep 1

    count_processes=$(ps aux | grep -v grep | grep -c "$SyncHistory_Script")
    wait_count=$((wait_count+1))
done


#Reset session id
SetVarSetting "session_id" ""


#REBOOT!
reboot_count=$((reboot_count + 1))
SetVarSetting "watchdog_reboot_count" "${$reboot_count}"
SetVarSetting "watchdog_last_reboot" "${now_timestamp}"


db_return=$(curl --fail -s "${URL__api}/json__session_history.php?action=reboot&message=$reboot_count")
if [[ "$db_return" == "\"ok\"" ]]; then
    LogDebug "json__session_history ok"
else
    LogError "json__session_history error: $db_return"
fi


sleep 1

$TOOL__shutdown -r now
