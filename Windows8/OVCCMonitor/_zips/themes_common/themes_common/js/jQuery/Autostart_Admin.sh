#!/bin/bash


text_special=$(tput bold; tput setaf 0; tput setab 7;)
text_normal=$(tput sgr0)


function _Usage {
    echo
    echo "===================================================================================="
    echo
    echo "You must be root (sudo) to run this script."
    echo
    echo "Usage examples"
    echo
    echo "Set the autostart"
    echo "  ${text_special}$0 xxx yyy${text_normal}"
    echo "  (where xxx is the admin username, and yyy the password)"
    echo
    echo "Set the autostart and reboot"
    echo "  ${text_special}$0 xxx yyy reboot${text_normal}"
    echo "  (where xxx is the admin username, and yyy the password)"
    echo ""

    exit 1
}


if (( EUID != 0 )); then
    _Usage
fi


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi


#=============================================================================================================================
if [ -z "$1" ]; then
    _Usage
fi


if [ -z "$2" ]; then
    _Usage
fi


AUTOSTART__username="$1"
AUTOSTART__password="$2"
AUTOSTART__reboot="no"

if [ "$3" == "reboot" ]; then
    AUTOSTART__reboot="yes"
fi


# ==========================================================================================
# Create temp Python script
TMP_FILE=$(mktemp /tmp/.tmp.python.XXXXXXXX)
mv "$TMP_FILE" "${TMP_FILE}.py"
TMP_FILE="${TMP_FILE}.py"


cat << EOF > "$TMP_FILE"
#!/usr/bin/env python

# Port of Gavin Brock's Perl kcpassword generator to Python, by Tom Taylor
# <tom@tomtaylor.co.uk>.
# Perl version: http://www.brock-family.org/gavin/perl/kcpassword.html

import sys
import os

def kcpassword(passwd):
    # The magic 11 bytes - these are just repeated
    # 0x7D 0x89 0x52 0x23 0xD2 0xBC 0xDD 0xEA 0xA3 0xB9 0x1F
    key = [125,137,82,35,210,188,221,234,163,185,31]
    key_len = len(key)

    passwd = [ord(x) for x in list(passwd)]
    # pad passwd length out to an even multiple of key length
    r = len(passwd) % key_len
    if (r > 0):
        passwd = passwd + [0] * (key_len - r)

    for n in range(0, len(passwd), len(key)):
        ki = 0
        for j in range(n, min(n+len(key), len(passwd))):
            passwd[j] = passwd[j] ^ key[ki]
            ki += 1

    passwd = [chr(x) for x in passwd]
    return "".join(passwd)

if __name__ == "__main__":
    passwd = kcpassword(sys.argv[1])
    fd = os.open('/etc/kcpassword', os.O_WRONLY | os.O_CREAT, 0o600)
    file = os.fdopen(fd, 'w')
    file.write(passwd)
    file.close()

EOF


# ==========================================================================================
# Run temp Python script
chmod 700 "$TMP_FILE"
"$TMP_FILE" "$AUTOSTART__password"


# ==========================================================================================
# Delete temp Python script
rm -f "$TMP_FILE"


# ==========================================================================================
# Check
if [ ! -f "/etc/kcpassword" ]; then
    echo "Failed!"
    exit 2
fi


# ==========================================================================================
# Set autostart
$TOOL__defaults write /Library/Preferences/com.apple.loginwindow autoLoginUser "$AUTOSTART__username"


# ==========================================================================================
# Set Watchdog to disabled
time_now=$(date +%s)
SetVarSetting "watchdog_temporary_disable_timestamp" "${time_now}"
SetVarSetting "watchdog_temporary_disable_survive_reboot" "1"


# ==========================================================================================
echo "Auto start set for user ${text_special}${AUTOSTART__username}${text_normal}"


# ==========================================================================================
if [ "$AUTOSTART__reboot" == "yes" ]; then
    echo "  rebooting!"

    shutdown -r now
fi
