#!/bin/bash


if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi
#=============================================================================================================================

LogMessage "started"


logfile_max_days_before_zipped=$(GetSetting "Settings" "logfile_max_days_before_zipped" "3")
logfile_max_days_before_deleted=$(GetSetting "Settings" "logfile_max_days_before_deleted" "365")
are_we_status_go_for_deleting="0"
TMP_FOLDER=$(mktemp -d /tmp/.ovcc.logfile.cleanup.XXXXXXXX)
LOG_NAME_PATTERN="*.log"


logfile_max_days_before_zipped=$((logfile_max_days_before_zipped - 1))
if (( logfile_max_days_before_zipped < 1 )); then
    logfile_max_days_before_zipped="1"
fi

if (( logfile_max_days_before_deleted > logfile_max_days_before_zipped )); then
    are_we_status_go_for_deleting="1"
fi


echo -n "" > "$TMP_FOLDER/log.txt"
echo -n "" > "$TMP_FOLDER/zipcount.txt"
echo -n "" > "$TMP_FOLDER/delcount.txt"

echo " " >> "$TMP_FOLDER/log.txt"


FILE_COUNT_SUCCESS=0
FILE_COUNT_RM_FAILED=0

echo "su0rf0" > "$TMP_FOLDER/delcount.txt"

if [[ "${are_we_status_go_for_deleting}" = "1" ]]; then
    find "$PATH__Logs" -name "*.*" -mtime +$logfile_max_days_before_deleted -print0 | while IFS= read -r -d '' item
    do
        rm -f "$item"

        if [ ! -f "$item" ]; then
            echo "removed '$item'" >> "$TMP_FOLDER/log.txt"
            FILE_COUNT_SUCCESS=$((FILE_COUNT_SUCCESS+1))
        else
            echo "failed to remove '$item'" >> "$TMP_FOLDER/log.txt"
            FILE_COUNT_RM_FAILED=$((FILE_COUNT_RM_FAILED+1))
        fi

        echo "su${FILE_COUNT_SUCCESS}rf${FILE_COUNT_RM_FAILED}" > "$TMP_FOLDER/delcount.txt"
    done
else
    echo "config error" > "$TMP_FOLDER/delcount.txt"
fi


FILE_COUNT_SUCCESS=0
FILE_COUNT_ZIP_FAILED=0
FILE_COUNT_RM_FAILED=0

echo "su0zf0rf0" > "$TMP_FOLDER/zipcount.txt"

find "$PATH__Logs" -name "$LOG_NAME_PATTERN" -mtime +$logfile_max_days_before_zipped -print0 | while IFS= read -r -d '' item
do
    "$TOOL__zip" -9 -j "$item.zip" "$item"

    if [ -f "$item.zip" ]; then
        echo "created '$item.zip'" >> "$TMP_FOLDER/log.txt"

        rm -f "$item"

        if [ ! -f "$item" ]; then
            echo "removed '$item'" >> "$TMP_FOLDER/log.txt"
            FILE_COUNT_SUCCESS=$((FILE_COUNT_SUCCESS+1))
        else
            echo "failed to remove '$item'" >> "$TMP_FOLDER/log.txt"
            FILE_COUNT_RM_FAILED=$((FILE_COUNT_RM_FAILED+1))
        fi
    else
        echo "failed to create '$item.zip'" >> "$TMP_FOLDER/log.txt"
        FILE_COUNT_ZIP_FAILED=$((FILE_COUNT_ZIP_FAILED+1))
    fi

    echo "su${FILE_COUNT_SUCCESS}zf${FILE_COUNT_ZIP_FAILED}rf${FILE_COUNT_RM_FAILED}" > "$TMP_FOLDER/zipcount.txt"
done


LOG_TEXT=$(<"$TMP_FOLDER/log.txt")
ZIPCOUNT_TEXT=$(<"$TMP_FOLDER/zipcount.txt")
DELCOUNT_TEXT=$(<"$TMP_FOLDER/delcount.txt")
rm -rf "$TMP_FOLDER"
LogMessage "zipped/removed:"
LogMessage "$LOG_TEXT"


json_message="zip threshold: ${logfile_max_days_before_zipped} ; remove threshold: ${logfile_max_days_before_deleted} ; zip result: ${ZIPCOUNT_TEXT} ; del result: ${DELCOUNT_TEXT}"

db_return=$(curl --fail -s --data-urlencode "action=logfilecleanup" --data-urlencode "message=${json_message}" "${URL__api}/json__session_history.php")
if [[ "$db_return" == "\"ok\"" ]]; then
    LogDebug "json__session_history ok"
else
    LogError "json__session_history error: $db_return"
fi
