#!/bin/bash


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
        exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
        echo "  error: Missing $parent_path/includes/_globals.sh"
        exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
        echo "  error: Missing $parent_path/includes/_functions.sh"
        exit 3
fi

#=============================================================================================================================


idle_time=-1

LogDebug "idletime triggered"


if [ -f "$TOOL__idletime" ]; then
    #get idle time
    idle_time=$("$TOOL__idletime")
else
    LogError "$TOOL__idletime not found!"
fi

LogDebug "idletime: $idle_time"


#store in db
void=$(curl --fail -s "${URL__api}/json__session.php?action=idle&idle=$idle_time")


#store in file
dt=$(date "+%Y-%m-%d_%H%M%S")
echo "$dt - $idle_time" >> "$PATH__idletimelog"
