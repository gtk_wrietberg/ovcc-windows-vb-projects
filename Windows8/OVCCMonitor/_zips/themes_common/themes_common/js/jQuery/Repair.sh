#!/bin/bash

if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi

PATH__HomeBrewMontereyRoot="/usr/local"
if [ ! -f "${PATH__HomeBrewMontereyRoot}/bin/brew" ];
then
    PATH__HomeBrewMontereyRoot="/opt/homebrew"
fi

chmod 644 "${PATH__HomeBrewMontereyRoot}/etc/httpd/extra/ovcc.monterey.conf"
chmod 666 "${PATH__HomeBrewMontereyRoot}/var/log/httpd/access_log"
chmod 666 "${PATH__HomeBrewMontereyRoot}/var/log/httpd/error_log"
chmod 755 "${PATH__HomeBrewMontereyRoot}/bin"