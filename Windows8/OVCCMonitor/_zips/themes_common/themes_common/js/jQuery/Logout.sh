#!/bin/bash


if (( EUID != 0 )); then
    echo "You must be root (sudo) to run this script."
    exit 1
fi


#-----------------------------------------------------------------------------------------------------------------------------
#includes and inits
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

if [ ! -d "$parent_path/includes" ]; then
    echo "Error: Missing payload directory"
    exit 2
fi

source "$parent_path/includes/_globals.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sh"
    exit 3
fi

source "$parent_path/includes/_globals.sec.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_globals.sec.sh"
    exit 3
fi

source "$parent_path/includes/_functions.sh"
if [ "$?" != "0" ]; then
    echo "  error: Missing $parent_path/includes/_functions.sh"
    exit 3
fi

#=============================================================================================================================
LogMessage "logout triggered"


#remove other triggers
rm -f "$PATH__AgentTriggers/OVCCHomepageWrapper/"*
rm -f "$PATH__AgentTriggers/WatchdogWarning/"*
rm -f "$PATH__AgentTriggers/Reboot/"*
rm -f "$PATH__AgentTriggers/Screensaver/"*
rm -f "$FILE__AdminModeTrigger"


#prevent logout loop
if [ -d "$PATH__AgentTriggers/Logout" ]; then
    rm -f "$PATH__AgentTriggers/Logout/"*

    sleep 1

    fcount=$(ls -A "$PATH__AgentTriggers/Logout")
    if [ $fcount ]; then
        LogError "trigger not deleted! disabling"

        exit 1
    fi
else
    LogError "trigger dir not found ($PATH__AgentTriggers/Logout), disabling!"

    exit 2
fi

sleep 1


#reset terms
SetVarSetting "terms_agreed" "0"


#Syncing history
SyncHistory_Script="${PATH__Bin}/scripts/SyncHistory.sh"
max_wait_count=5


LogDebug "triggering SyncHistory"

CreateTriggerFile "$PATH__AgentTriggers/SyncHistory/sync.history"

sleep 1

count_processes=1
wait_count=0

while (( count_processes>0 )) && (( wait_count<$max_wait_count ))
do
        sleep 1

        count_processes=$(ps aux | grep -v grep | grep -c "$SyncHistory_Script")
        wait_count=$((wait_count+1))
done


#Booting out Guest user
$TOOL__launchctl bootout gui/$(id -u Guest) &> /dev/null
launchctl_exit_code=$?
json_message="ok"
if [ "$launchctl_exit_code" != "0" ]; then
    LogError "logout didn't happen, was Guest even logged in?"
    json_message="failed"
fi

db_return=$(curl --fail -s "${URL__api}/json__session_history.php?action=bootout&message=${json_message}")
if [[ "$db_return" == "\"ok\"" ]]; then
    LogDebug "json__session_history ok"
else
    LogError "json__session_history error: $db_return"
fi


#Reset session id
SetVarSetting "session_id" ""
