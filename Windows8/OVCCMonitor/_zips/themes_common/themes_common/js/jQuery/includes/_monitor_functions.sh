#!/bin/bash

function DbQueryMonitorSettings {
    local queryCount=0
    local queryDefault="$2"
    local queryResult="$queryDefault"
    local queryExitCode=""

    while (( queryCount <= 5 ))
    do
        LogDebug "db query: '$1'"

        queryResult=$("$TOOL__sqlite" "$FILE__Database_Monitor" "$1")
        queryExitCode="$?"
        if [ -z "${queryResult}" ]; then
            queryResult="$queryDefault"
        fi
        if [ "$queryExitCode" == "0" ]; then
            break
        fi

        queryResult="$queryDefault"

        queryCount=$((queryCount+1))
        LogDebug "db query failed, retry ($queryCount)"

        sleep 1
    done

    if [[ $1 == SELECT* ]]; then
        echo "$queryResult"
    fi
}

function GetDaemonStatus {
    local daemonName="$1"
    local daemonStatus=$(launchctl list | grep "com\.guesttek\.ovcc\.mac\.${daemonName}$")

    if [ -z "${daemonStatus}" ]; then
        echo "0"
    else
        echo "1"
    fi
}

function SetDaemonStatus {
    local daemonName="$1"
    local daemonStatus="$2"
    local daemonFile="com.guesttek.ovcc.mac.${daemonName}.plist"

    if [ -z "${daemonStatus}" ]; then
        daemonStatus="shouldve given me a status"
    fi

    if [ ! -f "/Library/LaunchDaemons/${daemonFile}" ]; then
        if [ -f "${PATH__integrity_plist}/daemons/${daemonFile}" ]; then
            cp -f "${PATH__integrity_plist}/daemons/${daemonFile}" "/Library/LaunchDaemons/${daemonFile}"
        fi
    fi

    if [ "${daemonStatus}" = "unload" ]; then
        launchctl unload -w "/Library/LaunchDaemons/${daemonFile}"
    else
        launchctl load -w "/Library/LaunchDaemons/${daemonFile}"
    fi

    echo "$?"
}

function GetApplicationStatus {
    local appName="$1"
    local appStatus="$(ps aux | grep "OVCC - ${appName}" | grep -v grep)"

    if [ -z "${appStatus}" ]; then
        echo "0"
    else
        echo "1"
    fi
}

function StartOfDayTimestamp {
    local timestamp=$1

    tmp_date=$(date -r ${timestamp} '+%m/%d/%Y')
    timestamp=$(date -j -f "%m/%d/%Y %H:%M:%S" "${tmp_date} 00:00:00" "+%s")

    echo "${timestamp}"
}

function GetYearMonthDayFromTimestamp {
    local timestamp=$1
    local year=$(date -r ${timestamp} '+%Y')
    local month=$(date -r ${timestamp} '+%m')
    local day=$(date -r ${timestamp} '+%d')

    #subtract 1 from the month, because JavaScript
    month=$(( month - 1 ))

    #remove leading zero's
    month=$((10#$month))
    day=$((10#$day))

    echo "${year}" "${month}" "${day}"
}

function AddDowntimeJSON {
    local newJSON=""
    local oldJSON="$1"
    local year=$2
    local month=$3
    local day=$4
    local duration=$5

    newJSON=$(echo "${oldJSON}" | $TOOL__jq --argjson tdy "${year}" --argjson tdm "${month}" --argjson tdd "${day}" --argjson td "${duration}" '.Duration += [{ "Year": $tdy, "Month": $tdm, "Day": $tdd, "Duration": $td }]' )

    echo "${newJSON}"
}

function AddNetworkDetailsJSON {
    local newJSON=""
    local oldJSON="$1"
    local connection_name=$2
    local ipv4address=$3
    local ipv6address=$4
    local mac_address=$5

    #newJSON=$(echo "${oldJSON}" | $TOOL__jq -n --arg ConnectionName "${connection_name}" --arg IPv4Address "${ipv4address}" --arg IPv6Address "${ipv6address}" --arg MacAddress "${mac_address}" '$ARGS.named' )
    newJSON=$(echo "${oldJSON}" | $TOOL__jq --arg ConnectionName "${connection_name}" --arg IPv4Address "${ipv4address}" --arg IPv6Address "${ipv6address}" --arg MacAddress "${mac_address}" '.network_details += [{ "ConnectionName": $ConnectionName, "IPv4Address": $IPv4Address, "IPv6Address": $IPv6Address, "MacAddress": $MacAddress }]' )

    echo "${newJSON}"
}

function VersionCompare {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}

function String2Boolean {
    local s="$1"

    if [[ "$s" == "true" || "$s" == "yes" || "$s" == "1" ]]; then
        echo "1";
    else
        echo "0";
    fi
}
