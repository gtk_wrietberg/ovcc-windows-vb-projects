#!/bin/bash

TOKEN__IpInfo="bb8acc31702c73"

PATH__MonitorMain="${PATH__Misc}/monitor"
FILE__UrlOverride="${PATH__MonitorMain}/.url.override"
FILE__MachineIdentifier="${PATH__MonitorMain}/.machine.identifier"
FILE__MonitorIdentifier="${PATH__MonitorMain}/.monitor.identifier"
FILE__MonitorMachineId="${PATH__MonitorMain}/.monitor.machine_id"
FILE__MonitorLastHeartbeatSuccess="${PATH__MonitorMain}/.monitor.last_heartbeat_success"
FILE__MonitorLastHeartbeatError="${PATH__MonitorMain}/.monitor.last_heartbeat_error"
FILE__MonitorLastInventorySync="${PATH__MonitorMain}/.monitor.last_inventory_sync"
FILE__MonitorForcePostUpdateRefresh="${PATH__MonitorMain}/.monitor.post_update_refresh"
FILE__MonitorLastMetricsSync="${PATH__MonitorMain}/.monitor.last_metrics_sync"
FILE__MonitorLastAppSettingsSync="${PATH__MonitorMain}/.monitor.last_appsettings_sync"
FILE__MonitorLastOVCCSettingsSync="${PATH__MonitorMain}/.monitor.last_ovccsettings_sync"
FILE__MonitorLastGeolocationSync="${PATH__MonitorMain}/.monitor.last_geolocation_sync"
FILE__MonitorWatchdogInitDisabledTimestamp="${PATH__MonitorMain}/.watchdog.init.disabled.timestamp"
FILE__MonitorWatchdogMainDisabledTimestamp="${PATH__MonitorMain}/.watchdog.main.disabled.timestamp"
FILE__MonitorDontReviveWatchdog="${PATH__MonitorMain}/.watchdog.dont.wake.sleeping.dogs"
FILE__MonitorMachineRenamed="${PATH__MonitorMain}/.monitor.renamed"


TOOL__generate_identifier="${PATH__Tools}/generate_identifier"
TOOL__GetLastRebootShutdown="${PATH__Tools}/GetLastRebootShutdown"
TOOL__jq="${PATH__Tools}/jq-macos-amd64"

TOOL__curl="$(which curl)"


FILE__Database_Monitor_Name="OVCCMac.monitor.db"
FILE__Database_Monitor="${PATH__Database}/${FILE__Database_Monitor_Name}"


URL_MONITOR__PROD="https://api-ovcc.guesttek.cloud"
URL_MONITOR__STAGING="https://api-ovcc-staging.guesttek.cloud"
URL_MONITOR__DEV="https://api-ovcc-dev.guesttek.cloud"
#URL_MONITOR__DEV="https://8a3spgj6kb.execute-api.us-east-1.amazonaws.com"


URL_MONITOR__base="${URL_MONITOR__PROD}/ovcc-monitor"
if [ -f "${FILE__UrlOverride}" ]; then
    tmp__URL_OVERRIDE=$(<${FILE__UrlOverride})
    if [[ "${tmp__URL_OVERRIDE}" == "staging" ]]; then
        URL_MONITOR__base="${URL_MONITOR__STAGING}/ovcc-monitor"
    fi
    if [[ "${tmp__URL_OVERRIDE}" == "dev" ]]; then
        URL_MONITOR__base="${URL_MONITOR__DEV}/ovcc-monitor"
    fi
    if [[ "${tmp__URL_OVERRIDE}" == "development" ]]; then
        URL_MONITOR__base="${URL_MONITOR__DEV}/ovcc-monitor"
    fi
else
    echo "production" > "${FILE__UrlOverride}"
fi
URL_MONITOR__heartbeat="${URL_MONITOR__base}/heartbeat"
URL_MONITOR__inventory="${URL_MONITOR__base}/inventory"
URL_MONITOR__metrics="${URL_MONITOR__base}/metrics"
URL_MONITOR__appsettings="${URL_MONITOR__base}/appsettings"
URL_MONITOR__ovccsettings="${URL_MONITOR__base}/ovccsettings"
URL_MONITOR__selfupdate="${URL_MONITOR__base}/selfupdate"
URL_MONITOR__test="${URL_MONITOR__base}/test"

URL_MONITOR_DOWNLOAD="https://ccms-public.s3.amazonaws.com/OVCC-Monitor/SelfUpdate"

URL_MONITOR__geolocation="https://ipinfo.io?token=${TOKEN__IpInfo}"


OS_LICENSE_AGREEMENT_FILE="/System/Library/CoreServices/Setup Assistant.app/Contents/Resources/en.lproj/OSXSoftwareLicense.rtf"
NINJA_PROGRAMDATA_FILE="/Applications/NinjaRMMAgent/programdata/jsoninput/ws.agent.bootdata.json"


MONITOR_APPSETTING_DEFAULT__LastInventorySyncDelay=300
MONITOR_APPSETTING_DEFAULT__LastMetricsSyncDelay=43200
MONITOR_APPSETTING_DEFAULT__LastGeolocationSyncDelay=86400
MONITOR_APPSETTING_DEFAULT__DowntimeThresholdSeconds=300
MONITOR_APPSETTING_DEFAULT__MaxLogfileHistory=30
MONITOR_APPSETTING_DEFAULT__ThreadDelay_Heartbeat=0
MONITOR_APPSETTING_DEFAULT__WatchdogReloadDelay=600
MONITOR_APPSETTING_DEFAULT__MonitorUpdateEnabled="yes"

MONITOR_OVCCSETTING_DEFAULT__ExpectedFileVersion_Monitor="0.0.0"


#just in case this ever changes ;-)
SECONDS_IN_DAY=86400
