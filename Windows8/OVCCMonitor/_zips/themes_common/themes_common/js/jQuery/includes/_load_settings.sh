#!/bin/bash


while IFS='=' read -r key value
do
    key=$(echo $key | tr '.' '_')
    value=$(echo $value | tr -d '"')

    eval SETTINGS__${key}=\${value}
done < "$FILE__Settings"


while IFS='=' read -r key value
do
    key=$(echo $key | tr '.' '_')
    value=$(echo $value | tr -d '"')

    eval HOMEPAGE__${key}=\${value}
done < "$FILE__HomepageSettings"


while IFS='=' read -r key value
do
    key=$(echo $key | tr '.' '_')
    value=$(echo $value | tr -d '"')

    eval WATCHDOG__${key}=\${value}
done < "$FILE__WatchdogSettings"


# variable setting stuff
SETTINGS__language_id=$(<${FILE__LanguageId})
SETTINGS__screensaver_last_run=$(<${FILE__ScreensaverLastRun})
SETTINGS__terms_agreed=$(<${FILE__TermsAgreed})
SETTINGS__terms_agreed_timestamp=$(<${FILE__TermsAgreedTimestamp})
WATCHDOG__last_check=$(<${FILE__WatchdogLastCheck})
WATCHDOG__active_user=$(<${FILE__WatchdogActiveUser})
WATCHDOG__state=$(<${FILE__WatchdogState})
WATCHDOG__idle_time=$(<${FILE__WatchdogIdleTime})
WATCHDOG__admin_mode=$(<${FILE__WatchdogAdminMode})
WATCHDOG__admin_mode_timestamp=$(<${FILE__WatchdogAdminModeTimestamp})
WATCHDOG__reboot_count=$(<${FILE__WatchdogRebootCount})
WATCHDOG__loginscreen_timestamp=$(<${FILE__WatchdogLoginScreenTimestamp})
WATCHDOG__temporary_disable_timestamp=$(<${FILE__WatchdogTemporaryDisableTimestamp})
WATCHDOG__last_reboot=$(<${FILE__WatchdogLastReboot})
WATCHDOG__temporary_disable_survive_reboot=$(<${FILE__WatchdogTemporaryDisableSurviveReboot})
