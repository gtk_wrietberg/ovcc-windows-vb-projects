#!/bin/bash

function _CreateFolder {
    if [ ! -d "$1" ]; then
        mkdir -pv -m $2 "$1"

        if [ "$?" != "0" ]; then
            echo "ERROR while creating folder: $1"
            echo "  error: $?"
            exit 4
        fi
    fi
}

function _CopyFile {
    cp -afX "$1" "$2"
    if [ "$?" != "0" ]; then
        echo "ERROR while copying file"
        echo "  source"
        echo "    $1"
        echo "  destination"
        echo "    $2"
        echo "  error: $?"
        exit 4
    fi
}

function _RemoveFolderAttributes {
    xattr -c -r "$1"
    if [ "$?" != "0" ]; then
        echo "ERROR while removing attributes from folder: $1"
        echo "  error: $?"
        exit 4
    fi
}

function _UpdateFolderPermissions {
    if [ ! -d "$1" ]; then
        echo "WARNING: _UpdateFolderPermission"
        echo "    folder not found: $1"
    else
        chmod $2 "$1"

        if [ "$?" != "0" ]; then
            echo "ERROR while updating folder permissions"
            echo "  folder"
            echo "    $1"
            echo "  permissions"
            echo "    $2"
            echo "  error: $?"
            exit 4
        fi
    fi
}

function _UpdateFilePermissions {
    if [ ! -f "$1" ]; then
        echo "WARNING: _UpdateFilePermission"
        echo "    file not found: $1"
    else
        chmod $2 "$1"

        if [ "$?" != "0" ]; then
            echo "ERROR while updating file permissions"
            echo "  file"
            echo "    $1"
            echo "  permissions"
            echo "    $2"
            echo "  error: $?"
            exit 4
        fi
    fi
}

function _UpdateFolderOwnership {
    if [ ! -d "$1" ]; then
        echo "WARNING: _UpdateFolderOwnership"
        echo "    folder not found: $1"
    else
        "$TOOL__chown" $2 "$1"

        if [ "$?" != "0" ]; then
            echo "ERROR while updating folder ownership"
            echo "  folder"
            echo "    $1"
            echo "  ownership"
            echo "    $2"
            echo "  error: $?"
            exit 4
        fi
    fi
}

function _UpdateFileOwnership {
    if [ ! -f "$1" ]; then
        echo "WARNING: _UpdateFileOwnership"
        echo "    file not found: $1"
    else
        "$TOOL__chown" $2 "$1"

        if [ "$?" != "0" ]; then
            echo "ERROR while updating file ownership"
            echo "  file"
            echo "    $1"
            echo "  ownership"
            echo "    $2"
            echo "  error: $?"
            exit 4
        fi
    fi
}

function _AddInstallLogEntry {
    if [ ! -z $1 ]; then
        echo "$(date +%Y%m%d_%H%M%S) - $1" >> "/tmp/OVCC_install.log"
    fi
}

function _AddTimeTableEntry {
    if [ ! -z $1 ]; then
        echo "$(date +%Y%m%d_%H%M%S) - $1" >> "$PATH__BASE/.timetable"
    else
        echo "$(date +%Y%m%d_%H%M%S) - empty action" >> "$PATH__BASE/.timetable"
    fi
}

# ===================================

function DbQuery {
    local queryCount=0
    local queryResult="$2"
    local delimiter="$3"

    if [ -z "$delimiter" ]; then
        delimiter="|"
    fi

    while (( queryCount <= 5 ))
    do
        LogDebug "db query: '$1'"

        queryResult=$("$TOOL__sqlite" -separator "$delimiter" "$FILE__Database_Settings" "$1")
        if [ "$?" == "0" ]; then
            break
        fi

        queryCount=$((queryCount+1))
        LogDebug "db query failed, retry ($queryCount)"

        sleep 1
    done

    if [[ $1 == SELECT* ]]; then
        echo "$queryResult"
    fi
}

function DbQuerySessions {
    local queryCount=0
    local queryResult="$2"

    while (( queryCount <= 5 ))
    do
        LogDebug "db query: '$1'"

        queryResult=$("$TOOL__sqlite" "$FILE__Database_Sessions" "$1")
        if [ "$?" == "0" ]; then
            break
        fi

        queryCount=$((queryCount+1))
        LogDebug "db query failed, retry ($queryCount)"

        sleep 1
    done

    if [[ $1 == SELECT* ]]; then
        echo "$queryResult"
    fi
}

function DbQueryCustomDatabase {
    local queryCount=0
    local queryResult="$3"

    while (( queryCount <= 5 ))
    do
        LogDebug "db query: '$1' - '$2'"

        queryResult=$("$TOOL__sqlite" "$1" "$2")
        if [ "$?" == "0" ]; then
            break
        fi

        queryCount=$((queryCount+1))
        LogDebug "db query failed, retry ($queryCount)"

        sleep 1
    done

    if [[ $2 == SELECT* ]]; then
        echo "$queryResult"
    fi
}


# ===================================
function SetVarSetting {
    local settingName="$1"
    local settingValue="$2"

    echo "${settingValue}" > "${PATH__Config_Var}/.${settingName}"
    "$TOOL__chown" root:_www "${PATH__Config_Var}/.${settingName}"
    chmod 664 "${PATH__Config_Var}/.${settingName}"
}

function GetVarSetting {
    local settingName="${PATH__Config_Var}/.$1"
    local settingValue="$2"

    if [ -f "${settingName}" ]; then
        settingValue=$(<${settingName})
    fi

    echo "${settingValue}"
}

function GetSetting {
    local settingFile="${PATH__Config}/$1.ini"
    local settingName="$2"
    local settingValue="$3"
    local settingDefaultValue="$3"

    if [ -f "${settingFile}" ]; then
        settingValue=$(awk -v setting="${settingName}" -F "=" '$0~setting {print $2}' "${settingFile}")

        if [ -z "${settingValue}" ]; then
            settingValue="${settingDefaultValue}"
        fi
    fi

    echo "${settingValue}"
}


# ===================================
function _path_check {
    local _check=$($TOOL__python -c "import os,sys; print(os.path.realpath(sys.argv[1]))" "$1")

    echo $_check
}


# ===================================
function LogMessage {
    _Logger 1 "$1"
}

function LogWarning {
    _Logger 2 "$1"
}

function LogError {
    _Logger 3 "$1"
}

function LogDebug {
    if [ -f "$PATH__Misc/debug" ]; then
        _Logger 4 "$1"
    fi
}

function _Logger {
    if [ ! -z "$1" ]; then
        if [ ! -z "$2" ]; then
            local msg="$2"
            msg=${2//\"/\\\"} #escape quotes
            local log_msg="$0 - $msg"
            local logger_test=$(echo '{"app":1,"type":'$1',"msg":"'$log_msg'"}' | nc -U /opt/OVCCMac/etc/LoggerDaemon.sock)
            local LogType=""
            if [[ $logger_test == "{\"Status\":0}"* ]]; then
                # ok
                local everything="ok"
            else
                case $1 in
                    "1")
                        LogType="."
                        ;;
                    "2")
                        LogType="?"
                        ;;
                    "3")
                        LogType="!"
                        ;;
                    "4")
                        LogType="d"
                        ;;
                    *)
                        LogType=" "
                        ;;
                esac

                echo "$(date +%Y%m%d_%H%M%S) [$LogType] - $log_msg" >> "/tmp/.OVCC.message.log"
            fi
        fi
    fi
}


# ===================================
function UpdateTriggerFolder {
    if [ ! -z "$1" ]; then
        LogDebug "checking folder: $1"

        local folder="$1"
        if [ ! -d "$folder" ]; then
            LogDebug "creating"
            mkdir -p "$folder"
        fi

        local permissions=$(stat -f '%A' "$folder")
        if [ "$permissions" != "777" ]; then
            LogDebug "permissions"
            chmod 777 "$folder"
        fi

        local owner=$(stat -f '%Su:%Sg' "$folder")
        if [ "$owner" != "nobody:nobody" ]; then
            LogDebug "owner"
            "$TOOL__chown" nobody:nobody "$folder"
        fi
    fi
}

function CreateTriggerFile {
    if [ ! -z "$1" ]; then
        LogDebug "creating trigger file: $1"
        local file="$1"
        touch "$file"
        chmod 777 "$file"
        "$TOOL__chown" nobody:nobody "$file"
    fi
}

# ===================

function ChangeSetting {
    local file=$1
    local key=$2
    local value=$3
    local delim="="

    sed -i.bak -e '/^${key}${delim}/s/${delim}.*/${delim}"${val}"/' "${file}"
    rm -f "${file}.bak"
}
