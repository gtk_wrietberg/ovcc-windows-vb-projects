#!/bin/bash

# =========================
PATH__BASE="/opt/OVCCMac"

PATH__Shared="${PATH__BASE}/shared"
PATH__Shared_BackgroundImages="${PATH__Shared}/backgrounds"
PATH__Shared_Icons="${PATH__Shared}/icons"

PATH__Bin="${PATH__BASE}/bin"

PATH__Backups="${PATH__BASE}/backups"


PATH__Logs="${PATH__BASE}/logs"

PATH__Misc="${PATH__BASE}/etc"
PATH__Languages="${PATH__Misc}/languages"

PATH__Config="${PATH__Misc}/config"
FILE__DockItems="${PATH__Config}/Dock.ini"
FILE__Settings="${PATH__Config}/Settings.ini"
FILE__HomepageSettings="${PATH__Config}/Homepage_Settings.ini"
FILE__WatchdogSettings="${PATH__Config}/Watchdog.ini"
FILE__Apps="${PATH__Config}/Applications.ini"
FILE__BlockedApps="${PATH__Config}/Blocked_Applications.ini"
FILE__Airlines="${PATH__Config}/Homepage_Airlines.ini"
FILE__Version="${PATH__Config}/Version.ini"

PATH__Config_Var="${PATH__Misc}/config/.var"
FILE__LanguageId="${PATH__Config_Var}/.language_id"
FILE__ScreensaverLastRun="${PATH__Config_Var}/.screensaver_last_run"
FILE__TermsAgreed="${PATH__Config_Var}/.terms_agreed"
FILE__TermsAgreedTimestamp="${PATH__Config_Var}/.terms_agreed_timestamp"
FILE__WatchdogLastCheck="${PATH__Config_Var}/.watchdog_last_check"
FILE__WatchdogActiveUser="${PATH__Config_Var}/.watchdog_active_user"
FILE__WatchdogState="${PATH__Config_Var}/.watchdog_state"
FILE__WatchdogIdleTime="${PATH__Config_Var}/.watchdog_idle_time"
FILE__WatchdogAdminMode="${PATH__Config_Var}/.watchdog_admin_mode"
FILE__WatchdogAdminModeTimestamp="${PATH__Config_Var}/.watchdog_admin_mode_timestamp"
FILE__WatchdogRebootCount="${PATH__Config_Var}/.watchdog_reboot_count"
FILE__WatchdogLoginScreenTimestamp="${PATH__Config_Var}/.watchdog_loginscreen_timestamp"
FILE__WatchdogTemporaryDisableTimestamp="${PATH__Config_Var}/.watchdog_temporary_disable_timestamp"
FILE__WatchdogLastReboot="${PATH__Config_Var}/.watchdog_last_reboot"
FILE__WatchdogTemporaryDisableSurviveReboot="${PATH__Config_Var}/.watchdog_temporary_disable_survive_reboot"

DEFAULT__WatchdogTemporaryDisableSurviveReboot="1"


FILE__SessionId="${PATH__Config_Var}/.session_id"


PATH__Database="${PATH__BASE}/db"
FILE__Database_Settings_Name="OVCCMac.db"
FILE__Database_Sessions_Name="OVCCMac.sessions.db"
FILE__Database_Settings="${PATH__Database}/${FILE__Database_Settings_Name}"
FILE__Database_Sessions="${PATH__Database}/${FILE__Database_Sessions_Name}"

PATH__integrity="${PATH__BASE}/.integrity"
PATH__integrity_config="${PATH__integrity}/config"
PATH__integrity_plist="${PATH__integrity}/plist"
PATH__integrity_db="${PATH__integrity}/db"

PATH__Tools="${PATH__Bin}/tools"
TOOL__idletime="${PATH__Tools}/IdleTime"
TOOL__dockutil="${PATH__Tools}/dockutil.py"

PATH__ll="/usr/local/bin"
TOOL__ll="${PATH__ll}/ll"


TOOL__dscl=$(which dscl)
TOOL__security=$(which security)
TOOL__sqlite=$(which sqlite3)
TOOL__python=$(which python)
TOOL__defaults=$(which defaults)
TOOL__plutil=$(which plutil)
TOOL__apachectl=$(which apachectl)
TOOL__htpasswd=$(which htpasswd)
TOOL__launchctl=$(which launchctl)
TOOL__zip=$(which zip)
TOOL__diff=$(which diff)
TOOL__killall=$(which killall)
TOOL__shutdown=$(which shutdown)
TOOL__open=$(which open)
TOOL__chown="/usr/sbin/chown"


PATH__wwwroot="${PATH__BASE}/wwwroot"
PATH__themes="${PATH__wwwroot}/ovcc/themes"

PATH__ApacheConfig="/etc/apache2"
PATH__ApacheConfigOther="${PATH__ApacheConfig}/other"

PATH__HomeBrewMontereyRoot="/usr/local"
CHECK__HomeBrew_M1="0"
if [ ! -f "${PATH__HomeBrewMontereyRoot}/bin/brew" ];
then
    PATH__HomeBrewMontereyRoot="/opt/homebrew"
    CHECK__HomeBrew_M1="1"
fi
PATH__Monterey_ApacheConfig="${PATH__HomeBrewMontereyRoot}/etc/httpd"
PATH__Monterey_ApacheConfigOther="${PATH__Monterey_ApacheConfig}/extra"
TOOL__brew="${PATH__HomeBrewMontereyRoot}/bin/brew"
TOOL__Monterey_htpasswd="${PATH__HomeBrewMontereyRoot}/bin/htpasswd"


PATH__Hosts="/etc/hosts"


PATH__OVCCHidden="/tmp/.ovcc"
PATH__AgentTriggers="${PATH__OVCCHidden}/agent_triggers"


PATH__idletimelog="/tmp/.idle"

URL__api="http://localhost/ovcc/api"

LAST_ERROR_CODE=0


#Sanity check
#if "which python" doesn't work
if [ ! -f "$TOOL__python" ];
then
    TOOL__python="/usr/local/bin/python"
fi
