#!/bin/bash

# =====================================
STATE__LOGIN_WINDOW=1
STATE__NOT_GUEST=2
STATE__WRAPPER_NOT_RUNNING=4
STATE__IDLE_SCREENSAVER_FAIL=8
STATE__ADMIN_MODE=16
STATE__ADMIN_MODE_EXPIRED=32
STATE__GUEST_NOT_AUTO_LOGON=64
STATE__TIME_FOR_DAILY_REBOOT=128
STATE__REMOTE_CONNECTION=256

STATE__WRAPPERKEEPALIVE_PENDING=1024
STATE__WARNING_PENDING=2048
STATE__LOGOUT_PENDING=4096
STATE__REBOOT_PENDING=8192


ACTION__WRAPPERKEEPALIVE=1
ACTION__WARNING=2
ACTION__LOGOUT=4
ACTION__REBOOT=8
ACTION__RESET_GUEST_AUTO_LOGON=16
ACTION__POSTPONED=256
