<?php
include('/opt/OVCCMac/wwwroot/ovcc/inc/include.php');

$office_type=OVCC_Settings::GetHomepageSetting("office","none");

if($office_type=="apple") {
    $pages=OVCC_Apps::IsPagesInstalled();
    $numbers=OVCC_Apps::IsNumbersInstalled();
    $keynote=OVCC_Apps::IsKeynoteInstalled();
?>
    <table>
        <tr>
            <td colspan="99" id="tdOfficeTitle">&nbsp;</td>
        </tr>
        <tr>
            <?php if($pages) { ?><td>&nbsp;</td><?php } ?>
            <?php if($numbers) { ?><td>&nbsp;</td><?php } ?>
            <?php if($keynote) { ?><td>&nbsp;</td><?php } ?>
        </tr>
        <tr>
        <?php if($pages) { ?><td><a href="javascript:UI__run_msoffice(<?=$pages?>)"><img src="/ovcc/core/themes_common/img/buttons/iWork-Pages.png" alt=""><br>Pages</a></td><?php } ?>
        <?php if($numbers) { ?><td><a href="javascript:UI__run_msoffice(<?=$numbers?>)"><img src="/ovcc/core/themes_common/img/buttons/iWork-Numbers.png" alt=""><br>Numbers</a></td><?php } ?>
        <?php if($keynote) { ?><td><a href="javascript:UI__run_msoffice(<?=$keynote?>)"><img src="/ovcc/core/themes_common/img/buttons/iWork-Keynote.png" alt=""><br>Keynote</a></td><?php } ?>
        </tr>
        <tr>
            <td colspan="99">&nbsp;</td>
        </tr>
    </table>
    <script type="text/javascript">
        document.getElementById("tdOfficeTitle").innerHTML=GetLanguageString(808);
    </script>
<?php
} else {
    $word=OVCC_Apps::IsWordInstalled();
    $excel=OVCC_Apps::IsExcelInstalled();
    $powerpoint=OVCC_Apps::IsPowerpointInstalled();
?>
    <table>
        <tr>
            <td colspan="99" id="tdOfficeTitle">&nbsp;</td>
        </tr>
        <tr>
            <?php if($word) { ?><td>&nbsp;</td><?php } ?>
            <?php if($excel) { ?><td>&nbsp;</td><?php } ?>
            <?php if($powerpoint) { ?><td>&nbsp;</td><?php } ?>
        </tr>
        <tr>
        <?php if($word) { ?><td><a href="javascript:UI__run_msoffice(<?=$word?>)"><img src="/ovcc/core/themes_common/img/buttons/MS-Word.png" alt=""><br>Word</a></td><?php } ?>
        <?php if($excel) { ?><td><a href="javascript:UI__run_msoffice(<?=$excel?>)"><img src="/ovcc/core/themes_common/img/buttons/MS-Excel.png" alt=""><br>Excel</a></td><?php } ?>
        <?php if($powerpoint) { ?><td><a href="javascript:UI__run_msoffice(<?=$powerpoint?>)"><img src="/ovcc/core/themes_common/img/buttons/MS-Powerpoint.png" alt=""><br>Powerpoint</a></td><?php } ?>
        </tr>
        <tr>
            <td colspan="<?=$install_count?>">&nbsp;</td>
        </tr>
    </table>
    <script type="text/javascript">
        document.getElementById("tdOfficeTitle").innerHTML=GetLanguageString(805);
    </script>
<?php
}
?>
