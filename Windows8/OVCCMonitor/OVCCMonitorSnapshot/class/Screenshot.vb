﻿Public Class Screenshot
    Public Shared Function Take(_filename) As Boolean
        Dim _size As Size = New Size(My.Computer.Screen.Bounds.Width, My.Computer.Screen.Bounds.Height)

        Return _Take(_filename, _size)
    End Function

    Private Shared Function _Take(_filename As String, _size As Size) As Boolean
        Try
            Using screenGrab As New Bitmap(_size.Width, _size.Height)
                Using g As Graphics = Graphics.FromImage(screenGrab)
                    g.CopyFromScreen(New Point(0, 0), New Point(0, 0), _size)
                End Using

                screenGrab.Save(_filename, Imaging.ImageFormat.Png)

                Return True
            End Using
        Catch ex As Exception
        End Try

        Return False
    End Function
End Class
