﻿Public Class frmMain
    Private mFilename As String = ""

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Color.Lime
        Me.Opacity = 0.5
        Me.Top = 0
        Me.Left = My.Computer.Screen.Bounds.Width - Me.Width

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.StartsWith("--file:") Then
                mFilename = arg.Replace("--file:", "")
            End If
        Next

        If mFilename.Equals("") Then
            Me.Close()
        End If

        Dim _dir As String = IO.Path.GetDirectoryName(mFilename)
        If Not IO.Directory.Exists(_dir) Then
            Me.Close()
        End If


        tmrSnapshot.Enabled = True
    End Sub

    Private Sub tmrSnapshot_Tick(sender As Object, e As EventArgs) Handles tmrSnapshot.Tick
        tmrSnapshot.Enabled = False

        Me.Hide()

        Screenshot.Take(mFilename)

        Me.Close()
    End Sub
End Class
