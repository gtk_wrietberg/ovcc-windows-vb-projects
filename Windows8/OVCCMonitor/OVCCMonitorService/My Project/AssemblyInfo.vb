Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCCMonitorService")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("Guest-Tek")>
<Assembly: AssemblyProduct("OVCCMonitorService")>
<Assembly: AssemblyCopyright("Copyright ©  2023-2024")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e5612998-9233-49d6-ae7d-bd628b5ded30")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.22.25057.1518")>
<Assembly: AssemblyFileVersion("1.22.25057.1518")>

'<assembly: AssemblyInformationalVersion("0.0.23341.1518")>
<Assembly: AssemblyInformationalVersion(" ")>