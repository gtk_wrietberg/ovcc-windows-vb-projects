﻿Imports System.Xml

Public Class Configuration
    Public Enum SaveResultCode
        OK = 0
        CONFIG_SCRIPT_SKIPPED = 1
        CONFIG_SCRIPT_ERROR = 2
        CENTRAL_SCRIPT_SKIPPED = 4
        CENTRAL_SCRIPT_ERROR = 8
        PASSWORD_FILE_SKIPPED = 16
        PASSWORD_FILE_ERROR = 32
        PAYMENT_DIALOG_SKIPPED = 64
        PAYMENT_DIALOG_ERROR = 128
        PAYMENT_SETTING_ERROR = 256
    End Enum

    Public Shared Function MakeSureFoldersExist() As Boolean
        IO.Directory.CreateDirectory(OVCCTheme.Directory)
        IO.Directory.CreateDirectory(Constants.Paths.Directory.Themes)
        IO.Directory.CreateDirectory(Constants.Paths.Directory.Versions)

        Return True
    End Function

    Public Class iBahnScript
        Public Class LocalSettings
            Private mHotelInformationUrl As String = ""
            Private mInternetUrl As String = ""
            Private mWeatherUrl As String = ""
            Private mMapUrl As String = ""
            Private mHotelAddress As String = ""
            Private mThemeIndex As Integer = 0

            Public Sub New()

            End Sub

            Public Sub New(HotelInformationUrl As String, InternetUrl As String, WeatherUrl As String, MapUrl As String, HotelAddress As String, ThemeIndex As Integer, ScreensaverIndex As Integer)
                Me.mHotelInformationUrl = HotelInformationUrl
                Me.mInternetUrl = InternetUrl
                Me.mWeatherUrl = WeatherUrl
                Me.mMapUrl = MapUrl
                Me.mHotelAddress = HotelAddress
                Me.mThemeIndex = ThemeIndex
            End Sub

            Public Property HotelInformationUrl As String
                Get
                    Return mHotelInformationUrl
                End Get
                Set(value As String)
                    mHotelInformationUrl = value
                End Set
            End Property

            Public Property InternetUrl As String
                Get
                    Return mInternetUrl
                End Get
                Set(value As String)
                    mInternetUrl = value
                End Set
            End Property

            Public Property WeatherUrl As String
                Get
                    Return mWeatherUrl
                End Get
                Set(value As String)
                    mWeatherUrl = value
                End Set
            End Property

            Public Property MapUrl As String
                Get
                    Return mMapUrl
                End Get
                Set(value As String)
                    mMapUrl = value
                End Set
            End Property

            Public Property HotelAddress As String
                Get
                    Return mHotelAddress
                End Get
                Set(value As String)
                    mHotelAddress = value
                End Set
            End Property

            Public Property ThemeIndex As Integer
                Get
                    Return mThemeIndex
                End Get
                Set(value As Integer)
                    mThemeIndex = value
                End Set
            End Property
        End Class

        Public Shared LastError As String = ""

        Public Shared ReadOnly Property HasError() As Boolean
            Get
                Return Not LastError.Equals("")
            End Get
        End Property

        Public Shared Function Load(ByRef oLocalSettings As LocalSettings) As Boolean
            If Not IO.File.Exists(Globals.Files.OVCC_UI_CONFIG_FILE) Then
                LastError = "File '" & Globals.Files.OVCC_UI_CONFIG_FILE & "' does not exist"
                Return False
            End If

            Try
                Dim bUpdated As Boolean = False

                Using sr As New IO.StreamReader(Globals.Files.OVCC_UI_CONFIG_FILE)
                    Dim line As String
                    Dim currentValue As String
                    Dim iValue As Integer

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var iBAHN_Global_HotelURL") Then
                            currentValue = line.Replace("var iBAHN_Global_HotelURL=""", "").Replace(""";", "")
                            oLocalSettings.HotelInformationUrl = currentValue
                        End If
                        If line.Contains("var iBAHN_Global_InternetURL") Then
                            currentValue = line.Replace("var iBAHN_Global_InternetURL=""", "").Replace(""";", "")
                            oLocalSettings.InternetUrl = currentValue
                        End If
                        If line.Contains("var iBAHN_Global_WeatherURL") Then
                            currentValue = line.Replace("var iBAHN_Global_WeatherURL=""", "").Replace(""";", "")
                            oLocalSettings.WeatherUrl = currentValue
                        End If
                        If line.Contains("var iBAHN_Global_MapURL") Then
                            currentValue = line.Replace("var iBAHN_Global_MapURL=""", "").Replace(""";", "")
                            oLocalSettings.MapUrl = currentValue
                        End If
                        If line.Contains("var iBAHN_Global_SiteAddress") Then
                            currentValue = line.Replace("var iBAHN_Global_SiteAddress=""", "").Replace(""";", "")
                            oLocalSettings.HotelAddress = currentValue
                        End If
                        If line.Contains("var iBAHN_Global_Brand") Then
                            currentValue = line.Replace("var iBAHN_Global_Brand=", "").Replace(";", "")
                            If Not Integer.TryParse(currentValue, iValue) Then
                                iValue = -1
                            End If
                            oLocalSettings.ThemeIndex = iValue
                        End If
                    End While
                End Using
            Catch ex As Exception
                LastError = ex.Message
                Return False
            End Try


            Return True
        End Function

        Public Shared Function Save() As SaveResultCode
            Dim lines As New List(Of String)

            If Not IO.File.Exists(Globals.Files.OVCC_UI_CONFIG_FILE) Then
                LastError = "File '" & Globals.Files.OVCC_UI_CONFIG_FILE & "' does not exist"
                Return SaveResultCode.CONFIG_SCRIPT_ERROR
            End If

            Try
                Dim bUpdated As Boolean = False

                Using sr As New IO.StreamReader(Globals.Files.OVCC_UI_CONFIG_FILE)
                    Dim line As String
                    Dim currentValue As String
                    Dim newValue As String
                    Dim iValue As Integer

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var iBAHN_Global_HotelURL") Then
                            newValue = Helpers.Generic.HtmlEncode(Settings.OVCC.Public.HotelInformationUrl)
                            currentValue = line.Replace("var iBAHN_Global_HotelURL=""", "").Replace(""";", "")
                            If Not currentValue.Equals(newValue) Then
                                line = "var iBAHN_Global_HotelURL=""" & newValue & """;"
                                bUpdated = True
                            End If
                        End If
                        If line.Contains("var iBAHN_Global_InternetURL") Then
                            newValue = Helpers.Generic.HtmlEncode(Settings.OVCC.Public.InternetUrl)
                            currentValue = line.Replace("var iBAHN_Global_InternetURL=""", "").Replace(""";", "")
                            If Not currentValue.Equals(newValue) Then
                                line = "var iBAHN_Global_InternetURL=""" & newValue & """;"
                                bUpdated = True
                            End If
                        End If
                        If line.Contains("var iBAHN_Global_WeatherURL") Then
                            newValue = Helpers.Generic.HtmlEncode(Settings.OVCC.Public.WeatherUrl)
                            currentValue = line.Replace("var iBAHN_Global_WeatherURL=""", "").Replace(""";", "")
                            If Not currentValue.Equals(newValue) Then
                                line = "var iBAHN_Global_WeatherURL=""" & newValue & """;"
                                bUpdated = True
                            End If
                        End If
                        If line.Contains("var iBAHN_Global_MapURL") Then
                            newValue = Helpers.Generic.HtmlEncode(Settings.OVCC.Public.MapUrl)
                            currentValue = line.Replace("var iBAHN_Global_MapURL=""", "").Replace(""";", "")
                            If Not currentValue.Equals(newValue) Then
                                line = "var iBAHN_Global_MapURL=""" & newValue & """;"
                                bUpdated = True
                            End If
                        End If
                        If line.Contains("var iBAHN_Global_SiteAddress") Then
                            newValue = Helpers.Generic.HtmlEncode(Settings.OVCC.Public.HotelAddress)
                            currentValue = line.Replace("var iBAHN_Global_SiteAddress=""", "").Replace(""";", "")
                            If Not currentValue.Equals(newValue) Then
                                line = "var iBAHN_Global_SiteAddress=""" & newValue & """;"
                                bUpdated = True
                            End If
                        End If
                        If line.Contains("var iBAHN_Global_Brand") Then
                            currentValue = line.Replace("var iBAHN_Global_Brand=", "").Replace(";", "")
                            If Not Integer.TryParse(currentValue, iValue) Then
                                iValue = -1
                            End If
                            If iValue <> Settings.OVCC.Public.ThemeIndex Then
                                line = "var iBAHN_Global_Brand=" & Settings.OVCC.Public.ThemeIndex & ";"
                                bUpdated = True
                            End If
                        End If

                        lines.Add(line)
                    End While
                End Using

                If bUpdated Then
                    Using sw As New IO.StreamWriter(Globals.Files.OVCC_UI_CONFIG_FILE)
                        For Each line As String In lines
                            sw.WriteLine(line)
                        Next
                    End Using

                    Return SaveResultCode.OK
                Else
                    Return SaveResultCode.CONFIG_SCRIPT_SKIPPED
                End If
            Catch ex As Exception
                LastError = ex.Message
                Return SaveResultCode.CONFIG_SCRIPT_ERROR
            End Try
        End Function
    End Class

    Public Class OVCCPaymentDialog
        Public Shared LastError As String = ""

        Public Shared ReadOnly Property HasError() As Boolean
            Get
                Return Not LastError.Equals("")
            End Get
        End Property

        Public Shared Function Load() As String
            Dim fCurrentFile As New IO.FileInfo(Constants.Paths.File.SiteKioskPaymentDialog)

            If IO.File.Exists(Globals.Files.PaymentDialogFiles.PASSWORD) Then
                Dim fTemp As New IO.FileInfo(Globals.Files.PaymentDialogFiles.PASSWORD)

                If fCurrentFile.Length.Equals(fTemp.Length) Then
                    Return Constants.PaymentDialogFileName.PASSWORD
                End If
            End If

            If IO.File.Exists(Globals.Files.PaymentDialogFiles.RESIDENCE_INN) Then
                Dim fTemp As New IO.FileInfo(Globals.Files.PaymentDialogFiles.RESIDENCE_INN)

                If fCurrentFile.Length.Equals(fTemp.Length) Then
                    Return Constants.PaymentDialogFileName.RESIDENCE_INN
                End If
            End If

            Return Constants.PaymentDialogFileName.DEFAULT
        End Function

        Public Shared Function Save() As SaveResultCode
            Dim sFile As String = ""

            Try
                Select Case Settings.OVCC.[Public].ThemePaymentDialog.ToLower
                    Case Constants.PaymentDialogFileName.PASSWORD
                        sFile = Globals.Files.PaymentDialogFiles.PASSWORD
                    Case Constants.PaymentDialogFileName.RESIDENCE_INN
                        sFile = Globals.Files.PaymentDialogFiles.RESIDENCE_INN
                    Case Else
                        sFile = Globals.Files.PaymentDialogFiles.DEFAULT
                End Select

                If Not IO.File.Exists(sFile) Then
                    Throw New Exception("payment dialog '" & sFile & "' not found")
                End If


                IO.File.Copy(sFile, Constants.Paths.File.SiteKioskPaymentDialog, True)


                If Not IO.File.Exists(Constants.Paths.File.SiteKioskPaymentDialog) Then
                    Throw New Exception("payment dialog '" & Constants.Paths.File.SiteKioskPaymentDialog & "' not found")
                End If


                Return SaveResultCode.OK
            Catch ex As Exception
                LastError = ex.Message
                Return SaveResultCode.PAYMENT_DIALOG_ERROR
            End Try
        End Function
    End Class


    Public Class OVCCPassword
        Public Class LocalSettings
            Private mPasswordEnabled As String = "false"
            Public Property PasswordEnabled() As String
                Get
                    Return mPasswordEnabled
                End Get
                Set(ByVal value As String)
                    mPasswordEnabled = value
                End Set
            End Property

            Private mPassword As String = ""
            Public Property Password() As String
                Get
                    Return mPassword
                End Get
                Set(ByVal value As String)
                    mPassword = value
                End Set
            End Property
        End Class

        Public Shared LastError As String = ""

        Public Shared ReadOnly Property HasError() As Boolean
            Get
                Return Not LastError.Equals("")
            End Get
        End Property

        Public Shared Function Load(ByRef oLocalSettings As LocalSettings) As Boolean
            Try
                LoadPasswordFile()

                If Not IO.File.Exists(Globals.Files.OVCC_UI_PASSWORD_FILE) Then
                    Throw New Exception("password file '" & Globals.Files.OVCC_UI_PASSWORD_FILE & "' not found")
                End If

                Dim sCurrentPassword As String = ""
                Dim sCurrentBackupPassword As String = ""
                Dim iReturn As SaveResultCode = SaveResultCode.OK

                iReturn = GetPassword(sCurrentPassword, sCurrentBackupPassword)
                If Not iReturn = SaveResultCode.OK Then
                    Throw New Exception("could not load password file '" & Globals.Files.OVCC_UI_PASSWORD_FILE & "' not found")
                End If

                If Not sCurrentPassword.Equals("") Then
                    oLocalSettings.Password = sCurrentPassword
                    oLocalSettings.PasswordEnabled = "true"
                Else
                    oLocalSettings.Password = sCurrentBackupPassword
                    oLocalSettings.PasswordEnabled = "false"
                End If

                Return True
            Catch ex As Exception
                LastError = ex.Message
                Return False
            End Try
        End Function

        Public Shared Function SavePassword() As SaveResultCode
            Try
                LoadPasswordFile()

                If Not IO.File.Exists(Globals.Files.OVCC_UI_PASSWORD_FILE) Then
                    Throw New Exception("password file '" & Globals.Files.OVCC_UI_PASSWORD_FILE & "' not found")
                End If

                Dim sCurrentPassword As String = ""
                Dim sCurrentBackupPassword As String = ""
                Dim iReturn As SaveResultCode = SaveResultCode.OK

                iReturn = GetPassword(sCurrentPassword, sCurrentBackupPassword)
                If Not iReturn = SaveResultCode.OK Then
                    Return iReturn
                End If

                If Settings.OVCC.Public.ThemePasswordEnabled.ToLower.Equals("true") And Not Settings.OVCC.Public.ThemePassword.Equals("") Then
                    SetPassword(Settings.OVCC.Public.ThemePassword, True)
                    SetSiteKioskPayment(True)
                Else
                    SetPassword(sCurrentBackupPassword, False)
                    SetSiteKioskPayment(False)
                End If

                Return SaveResultCode.OK
            Catch ex As Exception
                LastError = ex.Message
                Return SaveResultCode.PASSWORD_FILE_ERROR
            End Try
        End Function

        Private Shared Function SetSiteKioskPayment(bEnabled As Boolean) As SaveResultCode
            If Not SkCfg.XML.WaitForLock Then
                LastError = "XML Lock timeout, already locked by '" & SkCfg.XML.IsLockedBy & "'"
                Return SaveResultCode.PAYMENT_SETTING_ERROR
            End If

            SkCfg.XML.Lock()
            SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
            SkCfg.XML.Load()

            Dim sCurrentlyEnabled As String = ""
            SkCfg.XML.GetAttribute("/plugin", "name", "SiteCash", "enabled", sCurrentlyEnabled)

            If sCurrentlyEnabled.Equals("true") Then
                If bEnabled Then
                    'do nothing
                Else
                    SkCfg.XML.SetAttribute("/plugin", "name", "SiteCash", "enabled", "false")
                End If
            Else
                If bEnabled Then
                    SkCfg.XML.SetAttribute("/plugin", "name", "SiteCash", "enabled", "true")
                Else
                    'do nothing
                End If
            End If


            Return SkCfg.XML.Save()
        End Function

        Private Shared Function LoadPasswordFile() As SaveResultCode
            If Not IO.File.Exists(Globals.Files.OVCC_UI_SCRIPT_FILE) Then
                LastError = "File '" & Globals.Files.OVCC_UI_SCRIPT_FILE & "' does not exist"
                Return SaveResultCode.PASSWORD_FILE_ERROR
            End If

            Globals.Files.OVCC_UI_PASSWORD_FILE = ""

            Try
                Using sr As New IO.StreamReader(Globals.Files.OVCC_UI_SCRIPT_FILE)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var _OVCC_PasswordAccessModule__CONFIG_PasswordFile=") Then
                            line = line.Replace("var _OVCC_PasswordAccessModule__CONFIG_PasswordFile=", "")
                            line = line.Replace("""", "")
                            line = line.Replace(";", "")

                            Globals.Files.OVCC_UI_PASSWORD_FILE = line.Replace("\\", "\")
                        End If
                    End While
                End Using


                Return SaveResultCode.OK
            Catch ex As Exception
                LastError = ex.Message
                Return SaveResultCode.PASSWORD_FILE_ERROR
            End Try
        End Function

        Private Shared Function GetPassword(ByRef CurrentPassword As String, ByRef BackupPassword As String) As SaveResultCode
            CurrentPassword = ""
            BackupPassword = ""

            Try
                Dim xmlDoc As Xml.XmlDocument
                Dim xmlNode_Root As Xml.XmlNode
                Dim xmlNode_Password As Xml.XmlNode
                Dim xmlNode_Current As Xml.XmlNode
                Dim xmlNode_Backup As Xml.XmlNode


                xmlDoc = New Xml.XmlDocument

                xmlDoc.Load(Globals.Files.OVCC_UI_PASSWORD_FILE)

                xmlNode_Root = xmlDoc.SelectSingleNode("ovcc")
                xmlNode_Password = xmlNode_Root.SelectSingleNode("password")
                xmlNode_Current = xmlNode_Password.SelectSingleNode("current")
                xmlNode_Backup = xmlNode_Password.SelectSingleNode("backup")

                CurrentPassword = Trim(xmlNode_Current.InnerText)
                BackupPassword = Trim(xmlNode_Backup.InnerText)


                Return SaveResultCode.OK
            Catch ex As Exception
                LastError = ex.Message
                Return SaveResultCode.PASSWORD_FILE_ERROR
            End Try
        End Function

        Private Shared Function SetPassword(Password As String, Active As Boolean) As SaveResultCode
            Try
                Dim xmlDoc As Xml.XmlDocument
                Dim xmlNode_Root As Xml.XmlNode
                Dim xmlNode_Password As Xml.XmlNode
                Dim xmlNode_Current As Xml.XmlNode
                Dim xmlNode_Backup As Xml.XmlNode

                xmlDoc = New Xml.XmlDocument

                xmlDoc.Load(Globals.Files.OVCC_UI_PASSWORD_FILE)

                xmlNode_Root = xmlDoc.SelectSingleNode("ovcc")
                xmlNode_Password = xmlNode_Root.SelectSingleNode("password")
                xmlNode_Current = xmlNode_Password.SelectSingleNode("current")
                xmlNode_Backup = xmlNode_Password.SelectSingleNode("backup")

                If Active Then
                    xmlNode_Current.InnerText = Password
                Else
                    xmlNode_Current.InnerText = ""
                End If

                xmlNode_Backup.InnerText = Password


                xmlDoc.Save(Globals.Files.OVCC_UI_PASSWORD_FILE)


                Return SaveResultCode.OK
            Catch ex As Exception
                LastError = ex.Message
                Return SaveResultCode.PASSWORD_FILE_ERROR
            End Try

        End Function
    End Class


    Public Class SkCfg
        Public Class XML
            Private Shared ReadOnly SK_NAMESPACE As String = "sk"
            Private Shared ReadOnly DT_NAMESPACE As String = "dt"

            Private Shared ReadOnly LOCK_TIMEOUT_MAX_COUNT As Integer = 50 ' = 5 seconds

            Public Shared Document As System.Xml.XmlDocument
            Public Shared Node_Root As System.Xml.XmlNode
            Private Shared NameTable As System.Xml.XmlNameTable
            Private Shared NamespaceManager As System.Xml.XmlNamespaceManager

            Public Shared FilePath As String = ""
            Public Shared LastError As String = ""

            Private Shared mNeedsSaving As Boolean = False

            Private Shared mLoaded As Boolean = False
            Private Shared mIsLocked As Boolean = False
            Private Shared mIsLockedBy As String = ""


            Public Shared ReadOnly Property HasError() As Boolean
                Get
                    Return Not LastError.Equals("")
                End Get
            End Property

            Public Shared ReadOnly Property IsLoaded() As Boolean
                Get
                    Return mLoaded
                End Get
            End Property

            Public Shared ReadOnly Property IsLocked() As Boolean
                Get
                    Return mIsLocked
                End Get
            End Property

            Public Shared ReadOnly Property IsLockedBy() As String
                Get
                    Return mIsLockedBy
                End Get
            End Property

            Public Shared Sub Lock()
                Lock(Misc.GetCaller())
            End Sub

            Public Shared Sub Lock(Caller As String)
                mIsLocked = True
                mIsLockedBy = Caller
            End Sub

            Public Shared Sub Unlock()
                mIsLocked = False
                mIsLockedBy = ""
            End Sub

            Public Shared ReadOnly Property NeedsSaving() As Boolean
                Get
                    Return mNeedsSaving
                End Get
            End Property

            Public Shared Function WaitForLock() As Boolean
                Dim iTimeoutCounter As Integer = 0

                Do
                    If Not mIsLocked Then
                        Return True
                    End If

                    Threading.Thread.Sleep(100)

                    iTimeoutCounter += 1
                Loop While iTimeoutCounter < LOCK_TIMEOUT_MAX_COUNT

                Return False
            End Function

            Private Shared Sub _Reset()
                mNeedsSaving = False
                mLoaded = False
                mIsLocked = False
                mIsLockedBy = ""

                mNodeCount = 0
                LastError = ""
            End Sub

            Public Shared Function Load() As Boolean
                _Reset()

                mIsLocked = True

                If mLoaded Then
                    Return True
                End If

                Try
                    Document = New System.Xml.XmlDocument
                    Document.Load(FilePath)

                    NameTable = Document.NameTable
                    NamespaceManager = New System.Xml.XmlNamespaceManager(NameTable)
                    NamespaceManager.AddNamespace(SK_NAMESPACE, "urn:schemas-sitekiosk-com:configuration")
                    NamespaceManager.AddNamespace(DT_NAMESPACE, "urn:schemas-microsoft-com:datatypes")

                    Node_Root = Document.SelectSingleNode("sk:sitekiosk-configuration", NamespaceManager)

                    If Node_Root Is Nothing Then
                        Throw New Exception("XmlNodeRoot Is Nothing")
                    End If

                    mLoaded = True
                Catch ex As Exception
                    LastError = ex.Message

                    Unlock()

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function Save() As Boolean
                If mNeedsSaving Then
                    Try
                        Document.Save(FilePath)

                        mLoaded = False
                    Catch ex As Exception
                        LastError = ex.Message

                        Return False
                    End Try
                End If

                Unlock()

                Return True
            End Function

            Public Shared Function GetValue(sPath As String, ByRef sValue As String) As Boolean
                Try
                    Dim mXmlNode As System.Xml.XmlNode

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)
                    sValue = mXmlNode.InnerText
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Private Shared mNodeCount As Integer = 0
            Public Shared Function GetFirstValue(sPath As String, sAttrib As String, ByRef sValue As String) As Boolean
                mNodeCount = 0

                Try
                    Dim mXmlNodeList As System.Xml.XmlNodeList

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNodeList = Node_Root.SelectNodes(sPath, NamespaceManager)

                    If mXmlNodeList.Count > 0 Then
                        If Not sAttrib.Equals("") Then
                            sValue = mXmlNodeList.Item(0).Attributes.GetNamedItem(sAttrib).Value
                        Else
                            sValue = mXmlNodeList.Item(0).InnerText
                        End If
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function GetNextValue(sPath As String, sAttrib As String, ByRef sValue As String) As Boolean
                mNodeCount += 1

                Try
                    Dim mXmlNodeList As System.Xml.XmlNodeList

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNodeList = Node_Root.SelectNodes(sPath, NamespaceManager)

                    If mXmlNodeList.Count > mNodeCount Then
                        If Not sAttrib.Equals("") Then
                            sValue = mXmlNodeList.Item(mNodeCount).Attributes.GetNamedItem(sAttrib).Value
                        Else
                            sValue = mXmlNodeList.Item(mNodeCount).InnerText
                        End If
                    Else
                        Return False
                    End If
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function SetValue(sPath As String, ByVal sValue As String) As Boolean
                Try
                    Dim sCurrentValue As String = ""

                    GetValue(sPath, sCurrentValue)

                    If Not sCurrentValue.Equals(sValue) Then
                        mNeedsSaving = True
                    End If
                Catch ex As Exception

                End Try

                Try
                    Dim mXmlNode As System.Xml.XmlNode

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)
                    mXmlNode.InnerText = sValue
                Catch ex As Exception
                    LastError = "SetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function AddValue(sPath As String, sValueName As String, sValue As String) As Boolean
                Try
                    Dim mXmlNode As System.Xml.XmlNode
                    Dim mXmlNodeList As System.Xml.XmlNodeList
                    Dim bAlreadyThere As Boolean = False
                    Dim sFullPath As String = ""

                    sFullPath = sPath & "/" & sValueName

                    sPath = _AddNamespaceToXPath(sPath)
                    sFullPath = _AddNamespaceToXPath(sFullPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)
                    mXmlNodeList = Node_Root.SelectNodes(sFullPath, NamespaceManager)

                    For Each mXmlSubNode As System.Xml.XmlNode In mXmlNodeList
                        If mXmlSubNode.InnerText.Equals(sValue) Then
                            bAlreadyThere = True
                            Exit For
                        End If
                    Next


                    If Not bAlreadyThere Then
                        Dim mXmlNode_NewNode As System.Xml.XmlNode

                        mXmlNode_NewNode = Document.CreateNode(System.Xml.XmlNodeType.Element, sValueName, NamespaceManager.LookupNamespace(SK_NAMESPACE))
                        mXmlNode_NewNode.InnerText = sValue

                        mXmlNode.AppendChild(mXmlNode_NewNode)

                        mNeedsSaving = True
                    End If


                    Return True
                Catch ex As Exception
                    LastError = "SetValue: " & ex.Message

                    Return False
                End Try
            End Function

            Public Shared Function GetAttribute(sPath As String, sAttributeName As String, ByRef sAttributeValue As String) As Boolean
                Try
                    Dim mXmlNode As System.Xml.XmlNode

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)

                    sAttributeValue = mXmlNode.Attributes.GetNamedItem(sAttributeName).Value
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function GetAttribute(sPath As String, sSearchAttributeName As String, sSearchAttributeValue As String, sAttributeName As String, ByRef sAttributeValue As String) As Boolean
                Try
                    Dim mXmlNodeList As System.Xml.XmlNodeList

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNodeList = Node_Root.SelectNodes(sPath, NamespaceManager)

                    For Each mXmlNode As System.Xml.XmlNode In mXmlNodeList
                        If Not mXmlNode.Attributes.GetNamedItem(sSearchAttributeName) Is Nothing Then
                            If mXmlNode.Attributes.GetNamedItem(sSearchAttributeName).Value.Equals(sSearchAttributeValue) Then
                                sAttributeValue = mXmlNode.Attributes.GetNamedItem(sAttributeName).Value

                                Return True
                            End If
                        End If
                    Next
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message
                End Try

                Return False
            End Function

            Public Shared Function SetAttribute(sPath As String, sAttributeName As String, ByVal sAttributeValue As String) As Boolean
                Try
                    Dim mXmlNode As System.Xml.XmlNode

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNode = Node_Root.SelectSingleNode(sPath, NamespaceManager)

                    If mXmlNode.Attributes.GetNamedItem(sAttributeName) Is Nothing Then
                        Dim mXmlAttribute As System.Xml.XmlAttribute

                        mXmlAttribute = Document.CreateAttribute(sAttributeName)
                        mXmlAttribute.Value = sAttributeValue

                        mXmlNode.Attributes.SetNamedItem(mXmlAttribute)

                        mNeedsSaving = True
                    Else
                        Dim sCurrentAttributeValue As String = mXmlNode.Attributes.GetNamedItem(sAttributeName).Value

                        If Not sAttributeValue.Equals(sCurrentAttributeValue) Then
                            mXmlNode.Attributes.GetNamedItem(sAttributeName).Value = sAttributeValue

                            mNeedsSaving = True
                        End If
                    End If
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return False
                End Try

                Return True
            End Function

            Public Shared Function SetAttribute(sPath As String, sSearchAttributeName As String, sSearchAttributeValue As String, sAttributeName As String, ByVal sAttributeValue As String) As Boolean
                Dim bRet As Boolean = False

                Try
                    Dim mXmlNodeList As System.Xml.XmlNodeList

                    sPath = _AddNamespaceToXPath(sPath)

                    mXmlNodeList = Node_Root.SelectNodes(sPath, NamespaceManager)

                    For Each mXmlNode As System.Xml.XmlNode In mXmlNodeList
                        If Not mXmlNode.Attributes.GetNamedItem(sSearchAttributeName) Is Nothing Then
                            If mXmlNode.Attributes.GetNamedItem(sSearchAttributeName).Value.Equals(sSearchAttributeValue) Then
                                If mXmlNode.Attributes.GetNamedItem(sAttributeName) Is Nothing Then
                                    Dim mXmlAttribute As System.Xml.XmlAttribute

                                    mXmlAttribute = Document.CreateAttribute(sAttributeName)
                                    mXmlAttribute.Value = sAttributeValue

                                    mXmlNode.Attributes.SetNamedItem(mXmlAttribute)

                                    mNeedsSaving = True
                                Else
                                    Dim sCurrentAttributeValue As String = mXmlNode.Attributes.GetNamedItem(sAttributeName).Value

                                    If Not sAttributeValue.Equals(sCurrentAttributeValue) Then
                                        mXmlNode.Attributes.GetNamedItem(sAttributeName).Value = sAttributeValue

                                        mNeedsSaving = True
                                    End If
                                End If

                                bRet = True
                                Exit For
                            End If
                        End If
                    Next
                Catch ex As Exception
                    LastError = "GetValue: " & ex.Message

                    Return bRet
                End Try

                Return bRet
            End Function

            Private Shared Function _AddNamespaceToXPath(sPath As String) As String
                Dim aPath As String() = sPath.Split("/")

                For i As Integer = 0 To aPath.Length - 1
                    If Not aPath(i).StartsWith(SK_NAMESPACE & ":") And Not aPath(i).Equals("") Then
                        aPath(i) = SK_NAMESPACE & ":" & aPath(i)
                    End If
                Next
                sPath = String.Join("/", aPath.Skip(1).ToArray())

                Return sPath
            End Function
        End Class
    End Class

    Public Class SettingsReceivedFromNinja
        Public Class LocalSettings
            Private mOVCCCode As String = ""
            Public Property OVCCCode() As String
                Get
                    Return mOVCCCode
                End Get
                Set(ByVal value As String)
                    mOVCCCode = value
                End Set
            End Property

            Private mNavisionCode As String = ""
            Public Property NavisionCode() As String
                Get
                    Return mNavisionCode
                End Get
                Set(ByVal value As String)
                    mNavisionCode = value
                End Set
            End Property
        End Class

        Public Shared LastError As String = ""

        Public Shared Function Load(ByRef oLocalSettings As LocalSettings) As Boolean
            Dim bRet__ovcc_code As Boolean = False, bRet__navision_code As Boolean = False
            Dim sLines As String()

            Try
                If Not IO.File.Exists(Constants.Paths.File.Ninja__code_ovcc_txt) Then
                    Throw New Exception("code file '" & Constants.Paths.File.Ninja__code_ovcc_txt & "' not found")
                End If

                sLines = IO.File.ReadAllLines(Constants.Paths.File.Ninja__code_ovcc_txt)

                If sLines.Count > 0 Then
                    oLocalSettings.OVCCCode = Trim(sLines(0))
                End If


                bRet__ovcc_code = True
            Catch ex As Exception
                LastError = ex.Message
                bRet__ovcc_code = False
            End Try


            Try
                If Not IO.File.Exists(Constants.Paths.File.Ninja__navision_ovcc_txt) Then
                    Throw New Exception("code file '" & Constants.Paths.File.Ninja__navision_ovcc_txt & "' not found")
                End If

                sLines = IO.File.ReadAllLines(Constants.Paths.File.Ninja__navision_ovcc_txt)

                If sLines.Count > 0 Then
                    oLocalSettings.NavisionCode = Trim(sLines(0))
                End If


                bRet__navision_code = True
            Catch ex As Exception
                LastError = ex.Message
                bRet__navision_code = False
            End Try

            Return bRet__ovcc_code And bRet__navision_code
        End Function
    End Class
End Class
