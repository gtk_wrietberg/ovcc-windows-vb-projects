﻿Imports System.IO
Imports System.Text

Public Class DirectoryHash
    Private Shared sbRaw As StringBuilder

    Public Shared RawString As String = ""
    Public Shared HashString As String = ""

    Public Shared Function Calculate(DirPath As String) As String
        RawString = ""
        HashString = ""

        sbRaw = New StringBuilder


        _IterateFolder(DirPath, AddressOf __CALLBACK__Directory, AddressOf __CALLBACK__File)

        RawString = sbRaw.ToString
        HashString = Helpers.Crypto.SHA512.GetHash(RawString)


        Return HashString
    End Function


    Private Shared Sub _IterateFolder(ByVal sourceFolder As String, ByVal directoryCallBack As Action(Of DirectoryInfo), ByVal fileCallBack As Action(Of FileInfo))
        If Directory.Exists(sourceFolder) Then
            Try
                For Each foldername As String In Directory.GetDirectories(sourceFolder)
                    If directoryCallBack IsNot Nothing Then
                        directoryCallBack.Invoke(New DirectoryInfo(foldername))
                    End If

                    _IterateFolder(foldername, directoryCallBack, fileCallBack)
                Next
            Catch ex As UnauthorizedAccessException
                Trace.TraceWarning(ex.Message)
            End Try

            If fileCallBack IsNot Nothing Then
                For Each filename As String In Directory.GetFiles(sourceFolder)
                    fileCallBack.Invoke(New FileInfo(filename))
                Next
            End If
        End If
    End Sub

    Private Shared Sub __CALLBACK__Directory(ByVal di As DirectoryInfo)
        ' do something here '
    End Sub

    Private Shared Sub __CALLBACK__File(ByVal fi As FileInfo)
        sbRaw.Append(fi.Name & "-" & fi.LastWriteTimeUtc & "-" & fi.Length & vbCrLf)
    End Sub
End Class
