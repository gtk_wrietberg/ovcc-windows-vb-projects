﻿Imports System.IO
Imports System.Linq
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Net.Sockets

Public Class NetInterfacesInfo
    Private Shared mLastError As String

    Public Shared ReadOnly Property LastError() As String
        Get
            Return mLastError
        End Get
    End Property

    Public Shared Function GetConnectedNics() As List(Of NetWorkInterfacesInfoShort)
        mLastError = ""


        Dim nList As New List(Of NetWorkInterfacesInfoShort)

        Dim nics As List(Of NetWorkInterfacesInfo) = GetNetworkInterfaces()

        For Each nic As NetWorkInterfacesInfo In nics
            If nic.Status = Net.NetworkInformation.OperationalStatus.Up Then
                Dim _nic As New NetWorkInterfacesInfoShort

                _nic.ConnectionName = nic.ConnectionName
                _nic.MacAddress = nic.MacAddress

                Dim _ips_v4 As New List(Of String)
                Dim _ips_v6 As New List(Of String)
                For Each _ip In nic.IpAddresses
                    If _ip.IsIPv6Teredo Or _ip.IsIPv4MappedToIPv6 Or _ip.IsIPv6LinkLocal Or _ip.IsIPv6Multicast Then
                        _ips_v6.Add(_ip.ToString)
                    Else
                        _ips_v4.Add(_ip.ToString)
                    End If
                Next
                _nic.IPv4Address = String.Join(";", _ips_v4.ToArray)
                _nic.IPv6Address = String.Join(";", _ips_v6.ToArray)

                nList.Add(_nic)
            End If
        Next

        Return nList
    End Function

    Public Shared Function GetNetworkInterfaces() As List(Of NetWorkInterfacesInfo)
        Dim ifInfo As New List(Of NetWorkInterfacesInfo)()

        Try
            ifInfo.AddRange(NetworkInterface.GetAllNetworkInterfaces().
                Where(Function(nic) nic.NetworkInterfaceType <> NetworkInterfaceType.Loopback AndAlso
                                    nic.Supports(NetworkInterfaceComponent.IPv4)).
                Select(Function(nic) New NetWorkInterfacesInfo() With {
                    .Description = nic.Description,
                    .ConnectionName = nic.Name,
                    .IsDHCPEnabled = nic.GetIPProperties().GetIPv4Properties().IsDhcpEnabled,
                    .DHCPSservers = nic.GetIPProperties().DhcpServerAddresses.ToList(),
                    .DnsServers = nic.GetIPProperties().DnsAddresses.ToList(),
                    .Gateways = nic.GetIPProperties().GatewayAddresses.Select(Function(ipa) ipa.Address).ToList(),
                    .InterfaceType = nic.NetworkInterfaceType,
                    .IpAddresses = nic.GetIPProperties().UnicastAddresses.Select(Function(ipa) ipa.Address).ToList(),
                    .MacAddress = nic.GetPhysicalAddress.ToString,
                    .Status = nic.OperationalStatus,
                    .IPV4Addresses = GetIpV4AddressInfo(nic)
                }))
        Catch ex As Exception
            mLastError = "GetNetworkInterfaces(): " & ex.Message
        End Try

        Return ifInfo
    End Function

    Public Shared Function IpV4AddressSimpleList(nicName As String) As List(Of IpV4AddressInfo)
        Dim nic = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault(Function(_nic) _nic.Name = nicName)

        If nic Is Nothing Then Return Nothing
        Return nic.GetIPProperties().UnicastAddresses.
                Where(Function(ipa) ipa.Address.AddressFamily = AddressFamily.InterNetwork).
                Select(Function(ipa) New IpV4AddressInfo() With {
                    .IpAddress = ipa.Address?.ToString(),
                    .NetMask = ipa.IPv4Mask?.ToString(),
                    .IsDnsEligible = ipa.IsDnsEligible,
                    .DefaultGateway = nic.GetIPProperties().GatewayAddresses.FirstOrDefault()?.Address?.ToString()
                }).ToList()
    End Function

    Private Shared Function GetIpV4AddressInfo(nic As NetworkInterface) As List(Of IpV4AddressInfo)
        Return nic.GetIPProperties().UnicastAddresses.
                Where(Function(ipa) ipa.Address.AddressFamily = AddressFamily.InterNetwork).
                Select(Function(ipa) New IpV4AddressInfo() With {
                    .IpAddress = ipa.Address?.ToString(),
                    .NetMask = ipa.IPv4Mask?.ToString(),
                    .IsDnsEligible = ipa.IsDnsEligible,
                    .DefaultGateway = nic.GetIPProperties().GatewayAddresses.FirstOrDefault()?.Address?.ToString()
                }).ToList()
    End Function

    'Private Shared Function GetNetworInterfaces() As NetworkInterface()
    '    Return NetworkInterface.GetAllNetworkInterfaces()
    'End Function

    'Private Shared Function GetNetworkInterfaceByName(nicName As String) As NetworkInterface
    '    Return NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault(Function(nic) nic.Name = nicName)
    'End Function

    Public Class NetWorkInterfacesInfo
        Public Property ConnectionName As String
        Public Property Description As String
        Public Property IPV4Addresses As List(Of IpV4AddressInfo)
        Public Property IpAddresses As List(Of IPAddress)
        Public Property DHCPSservers As List(Of IPAddress)
        Public Property DnsServers As List(Of IPAddress)
        Public Property Gateways As List(Of IPAddress)
        Public Property IsDHCPEnabled As Boolean
        Public Property Status As OperationalStatus
        Public Property InterfaceType As NetworkInterfaceType

        Private mMacAddress As String
        Public Property MacAddress() As String
            Get
                If Not mMacAddress.Contains(":") Then
                    Dim sTmp As String = mMacAddress
                    Dim iTmp As Integer = 0

                    For i As Integer = 2 To mMacAddress.Length - 1 Step 2
                        sTmp = sTmp.Insert(i + iTmp, ":")
                        iTmp += 1
                    Next

                    Return sTmp
                Else
                    Return mMacAddress
                End If
            End Get
            Set(value As String)
                mMacAddress = value
            End Set
        End Property
    End Class

    Public Class NetWorkInterfacesInfoShort
        Public Property ConnectionName As String
        Public Property IPv4Address As String
        Public Property IPv6Address As String
        Public Property MacAddress As String
    End Class

    Public Class IpV4AddressInfo
        Public Property IpAddress As String
        Public Property NetMask As String
        Public Property DefaultGateway As String
        Public Property IsDnsEligible As Boolean
    End Class
End Class