﻿Imports System.Diagnostics.Eventing.Reader
Imports System.Text

Public Class WindowsEventLog
    Public Class System
        Public Shared Function GetYesterdaysEventsForEventIds(lEventIds As List(Of Integer)) As List(Of EventRecord)
            Dim lEventRecords As New List(Of EventRecord)
            Dim today As Date = DateTime.Today
            Dim yesterday As Date = today.AddDays(-1)

            Dim query As String = String.Format("*[System/TimeCreated/@SystemTime >= '{0}' and System/TimeCreated/@SystemTime < '{1}']", yesterday.ToString("yyyy-MM-ddThh:mm:ss.000000000Z"), today.ToString("yyyy-MM-ddThh:mm:ss.000000000Z"))
            Dim eventLogQuery As New EventLogQuery("System", PathType.LogName, query)

            Using logReader = New EventLogReader(eventLogQuery)

                Dim _eventInstance As EventRecord = logReader.ReadEvent()
                Try
                    While Not _eventInstance Is Nothing
                        Dim eventId As Integer = _eventInstance.Id

                        If lEventIds.Contains(eventId) Then
                            lEventRecords.Add(_eventInstance)
                        End If

                        _eventInstance = logReader.ReadEvent()
                    End While
                Catch ex As Exception
                    If _eventInstance IsNot Nothing Then
                        _eventInstance.Dispose()
                    End If
                End Try
            End Using

            Return lEventRecords
        End Function

        Public Shared Function GetEventsForEventIdAndDate(lEventId As Integer, dDate As Date, Optional sType As String = "Critical") As List(Of EventRecord)
            Dim lEventIds As New List(Of Integer)

            lEventIds.Add(lEventId)

            Return GetEventsForEventIdsAndDate(lEventIds, dDate, sType)
        End Function

        Public Shared Function GetEventsForEventIdsAndDate(lEventIds As List(Of Integer), dDate As Date, Optional sType As String = "Critical") As List(Of EventRecord)
            Dim lEventRecords As New List(Of EventRecord)
            Dim dDateNext As Date = dDate.AddDays(1)


            Dim query As String = String.Format("*[System/TimeCreated/@SystemTime >= '{0}' and System/TimeCreated/@SystemTime < '{1}']", dDate.ToString("yyyy-MM-ddThh:mm:ss.000000000Z"), dDateNext.ToString("yyyy-MM-ddThh:mm:ss.000000000Z"))
            Dim eventLogQuery As New EventLogQuery("System", PathType.LogName, query)

            Using logReader = New EventLogReader(eventLogQuery)
                Dim _eventInstance As EventRecord = logReader.ReadEvent()
                Try
                    While Not _eventInstance Is Nothing
                        Dim eventId As Integer = _eventInstance.Id

                        If lEventIds.Contains(eventId) Then
                            If _eventInstance.LevelDisplayName.Equals(sType) Or sType.Equals("") Then
                                lEventRecords.Add(_eventInstance)
                            End If
                        End If

                        _eventInstance = logReader.ReadEvent()
                    End While
                Catch ex As Exception
                    If _eventInstance IsNot Nothing Then
                        _eventInstance.Dispose()
                    End If
                End Try
            End Using

            Return lEventRecords
        End Function

        'Public Shared Function GetYesterdaysEventRecords()
        '    Dim today As Date = DateTime.Today.AddDays(1)
        '    Dim yesterday As Date = today.AddDays(-1)
        '    'yesterday = yesterday.AddHours(63)

        '    Dim query As String = String.Format("*[System/TimeCreated/@SystemTime >= '{0}' and System/TimeCreated/@SystemTime < '{1}']", yesterday.ToString("yyyy-MM-ddThh:mm:ss.000000000Z"), today.ToString("yyyy-MM-ddThh:mm:ss.000000000Z"))

        '    MsgBox(query)
        '    ' query = String.Format("*[System/Provider/@Name=\"{0}\" and TimeCreated/@SystemTime&gt;='{1}']", "MyApplication", FormattedDateTime);

        '    Dim eventLogQuery As New EventLogQuery("System", PathType.LogName, query)

        '    Dim tmpStr As New StringBuilder
        '    Dim tmpCounter As Integer = 0

        '    Using logReader = New EventLogReader(eventLogQuery)
        '        Dim _eventInstance As EventRecord = logReader.ReadEvent()
        '        Try
        '            While Not _eventInstance Is Nothing
        '                tmpStr.Append(_eventInstance.TimeCreated.ToString)
        '                tmpStr.Append(" - ")
        '                tmpStr.Append(_eventInstance.Id.ToString)
        '                tmpStr.Append(vbCrLf)
        '                tmpCounter += 1

        '                _eventInstance = logReader.ReadEvent()
        '            End While
        '        Catch ex As Exception
        '            MsgBox(ex.Message)
        '            If _eventInstance IsNot Nothing Then
        '                _eventInstance.Dispose()
        '            End If
        '        End Try
        '    End Using

        '    MsgBox(tmpCounter.ToString)
        'End Function

        Public Shared Function GetLastEventRecordForEventId(EventId As Integer, ByRef eventInstance As EventRecord) As Boolean
            Dim query As New EventLogQuery("System", PathType.LogName, "*[System/EventID = " & EventId.ToString & "]")
            Dim dDate As Date = #1970-01-01#
            Dim bFound As Boolean = False

            Using logReader = New EventLogReader(query)
                Dim _eventInstance As EventRecord = logReader.ReadEvent()
                Try
                    While Not _eventInstance Is Nothing
                        If dDate.CompareTo(_eventInstance.TimeCreated) < 0 Then
                            eventInstance = _eventInstance
                            dDate = _eventInstance.TimeCreated
                            bFound = True
                        End If

                        _eventInstance = logReader.ReadEvent()
                    End While
                Catch ex As Exception
                    If _eventInstance IsNot Nothing Then
                        _eventInstance.Dispose()
                    End If

                End Try
            End Using

            Return bFound
        End Function

        Public Shared Function GetEventsRecordForEventId(EventId As Integer) As List(Of EventRecord)
            Dim query As New EventLogQuery("System", PathType.LogName, "*[System/EventID = " & EventId.ToString & "]")
            Dim lEvents As New List(Of EventRecord)

            Using logReader = New EventLogReader(query)
                Dim _eventInstance As EventRecord = logReader.ReadEvent()
                Try
                    While Not _eventInstance Is Nothing
                        lEvents.Add(_eventInstance)

                        _eventInstance = logReader.ReadEvent()
                    End While
                Catch ex As Exception
                    If _eventInstance IsNot Nothing Then
                        _eventInstance.Dispose()
                    End If

                End Try
            End Using

            Return lEvents
        End Function
    End Class
End Class
