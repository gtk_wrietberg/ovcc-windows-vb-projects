﻿Imports System.Reflection
Imports Microsoft.Win32

Public Class Settings
    Public Class Application
        Public Class [Private]
            Public Shared Debugging As Boolean = False
            Public Shared ApiEnvironment As String = ""
            Public Shared ForceUpdate As Boolean = False
            Public Shared ForceRequestSettings As Boolean = False
            Public Shared NinjaNodeId As Integer = -1
            Public Shared PropertyId As Integer = -1

            Public Shared LastApplicationSettingsSync As Long = 0
            Public Shared LastOVCCSettingsSync As Long = 0
            Public Shared LastOVCCThemesSync As Long = 0
            Public Shared LastMetricsSync As Long = 0
            Public Shared LastIntegritySync As Long = 0
            Public Shared LastInventorySync As Long = 0
            Public Shared LastGeolocationSync As Long = 0

            Public Shared EmergencyModeDetected As Boolean = False
            Public Shared EmergencyModeDetectedCount As Long = 0
            Public Shared EmergencyModeDetectedTimestamp As Long = 0
            Public Shared EmergencyModeFixedCount As Long = 0
            Public Shared EmergencyModeFixedTimestamp As Long = 0
            Public Shared EmergencyModeLastRebootTimestamp As Long = 0
            Public Shared EmergencyModeFixType As Constants.SiteKiosk.EmergencyModeFixTypes = Constants.SiteKiosk.EmergencyModeFixTypes.NONE

            Public Shared OVCCThemeIndex As Integer = -1
            Public Shared OVCCThemePath As String = ""


            'Public Shared MachineWasRenamed As Boolean = False
            'Public Shared MachineWasRenamed_OldName As String = ""

            Public Class Server
                Public Shared BaseUrl As String = ""

                Public Class EndPoint
                    Public Shared Heartbeat As String = ""
                    Public Shared ApplicationSetting As String = ""
                    Public Shared Integrity As String = ""
                    Public Shared Inventory As String = ""
                    Public Shared Metrics As String = ""
                    Public Shared OVCCSettings As String = ""
                    Public Shared SendLocalOVCCSettings As String = ""
                    Public Shared OVCCThemes As String = ""
                    Public Shared Screenshots As String = ""
                    Public Shared SelfUpdate As String = ""
                End Class
            End Class

            Public Class Download
                Public Shared BaseUrl As String = ""

                Public Shared Themes As String = ""
                Public Shared Screensavers As String = ""
                Public Shared SelfUpdate As String = ""
                Public Shared ExternalApps As String = ""
            End Class
        End Class

        Public Class [Public]
            Public Shared ThreadDelay_Heartbeat As Integer = 30
            Public Shared ThreadLoopDelay_Heartbeat As Integer = 60

            Public Shared ScreenshotDelay As Integer = ThreadLoopDelay_Heartbeat - 1

            Public Shared FilePath_SiteKiosk As String = ""
            Public Shared FilePath_Watchdog As String = ""
            Public Shared FilePath_FileExplorer As String = ""
            Public Shared FilePath_InformationUpdater As String = ""
            Public Shared FilePath_ChromeWrapper As String = ""
            Public Shared FilePath_Starter As String = ""

            Public Shared SendDetailedMetrics As Boolean = False
            Public Shared ScreenshotEnabled As Boolean = False
            Public Shared ScreenshotMaxWidth = 320

            Public Shared IntegrityEnabled As Boolean = True
            Public Shared IntegrityEnableWatchdogIfDisabled As Boolean = False

            Public Shared LastApplicationSettingsSyncDelay As Integer = 0
            Public Shared LastOVCCSettingsSyncDelay As Integer = 0
            Public Shared LastOVCCThemesSyncDelay As Integer = 0
            Public Shared LastMetricsSyncDelay As Integer = 0
            Public Shared LastIntegritySyncDelay As Integer = 0
            Public Shared LastInventorySyncDelay As Integer = 0
            Public Shared LastGeolocationSyncDelay As Integer = 86400

            Public Shared DowntimeThresholdSeconds As Integer = 0

            Public Shared ConsoleControlsThemes As Boolean = False
            Public Shared ConsoleControlsSettings As Boolean = False

            Public Shared Monitor_UpdateEnabled As Boolean = False
            Public Shared Monitor_SyncSettingsAfterUpdate As Boolean = False
        End Class

        Public Shared Function Load_Publics() As Boolean
            Logger.Message("Loading application settings", 0)

            Dim sErrorMessage As String = ""
            Dim aFields() As Reflection.FieldInfo = GetType([Public]).GetFields

            Logger.Debug("loading from registry", 1)
            For Each aField As Reflection.FieldInfo In aFields
                Logger.Debug(aField.Name, 2)

                Dim oReg As Object = Helpers.Registry.FindValueByString(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, sErrorMessage)
                If Not oReg Is Nothing Then
                    Logger.Debug("found in registry", 3)
                    'found it in registry
                    Dim o As Object = aField.GetValue(Nothing)

                    If TypeOf o Is Integer Then
                        Dim i As Integer = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, -1)
                        If i > -1 Then
                            aField.SetValue(Nothing, i)
                        End If

                        Logger.Debug("Integer", 4)
                        Logger.Debug(i.ToString, 5)
                    ElseIf TypeOf o Is Long Then
                        Dim l As Long = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, -1)
                        If l > -1 Then
                            aField.SetValue(Nothing, l)
                        End If

                        Logger.Debug("Long", 4)
                        Logger.Debug(l.ToString, 5)
                    ElseIf TypeOf o Is Boolean Then
                        Dim b As Boolean = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, False)
                        aField.SetValue(Nothing, b)

                        Logger.Debug("Boolean", 4)
                        Logger.Debug(b.ToString, 5)
                    Else
                        Dim s As String = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, Constants.Settings.BogusDefaultStringValue)
                        If Not s.Equals(Constants.Settings.BogusDefaultStringValue) Then
                            aField.SetValue(Nothing, s)
                        End If


                        Logger.Debug("String", 4)
                        Logger.Debug(s.ToString, 5)
                    End If
                Else
                    'Save(aField.Name, aField.GetValue(Nothing))
                    Logger.Debug("'" & aField.Name & "' not found in registry", 3)

                    Try
                        Dim objFields As FieldInfo() = GetType(Constants.RegistryKeys).GetFields
                        For Each _field As FieldInfo In objFields
                            If _field.Name.Equals("DEFAULTVALUE__MonitorSettings_" & aField.Name) Then
                                Dim oTmp As Object = _field.GetValue(Nothing)

                                aField.SetValue(Nothing, _field.GetValue(Nothing))
                                Exit For
                            End If
                        Next
                    Catch ex As Exception
                        Logger.Debug("Exception!", 4)
                        Logger.Debug(ex.Message, 5)
                    End Try

                    Logger.Debug(sErrorMessage, 4)
                End If
            Next


            'value sanity check
            If [Public].ThreadLoopDelay_Heartbeat < Constants.Settings.MinThreadLoopDelay Then
                [Public].ThreadLoopDelay_Heartbeat = Constants.Settings.MinThreadLoopDelay
            End If


            If [Private].Debugging Then
                Logger.Debug("Debugging", 1)
                Logger.Debug([Private].Debugging.ToString, 2)

                Dim aFields2_Debug() As Reflection.FieldInfo = GetType([Public]).GetFields

                For Each aField As Reflection.FieldInfo In aFields2_Debug
                    Logger.Debug(aField.Name, 1)
                    Logger.Debug(aField.GetValue(Nothing), 2)
                Next
            End If


            Return True
        End Function

        Public Shared Function Load_Privates() As Boolean
            Load_Debugging()
            Load_ServerUrl()
            Load_DownloadUrl()

            Load_LastApplicationSettingsSync()
            Load_LastIntegritySync()
            Load_LastInventorySync()
            Load_LastMetricsSync()
            Load_LastOVCCSettingsSync()
            Load_LastOVCCThemesSync()
            Load_LastGeolocationSync()

            Load_ForceUpdate()
            Load_ForceRequestSettings()


            Logger.Debug("Loading private settings", 0)

            Dim aFields2_Debug() As Reflection.FieldInfo = GetType([Private]).GetFields

            For Each aField As Reflection.FieldInfo In aFields2_Debug
                Logger.Debug(aField.Name, 1)
                Logger.Debug(aField.GetValue(Nothing), 2)
            Next


            Return True
        End Function

        Public Shared Function Load_Debugging() As Boolean
            [Private].Debugging = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_Debug,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_Debug)

            Return True
        End Function

        Public Shared Function Load_ServerUrl() As Boolean
            Dim sTmpDevelopment As String, sTmpStaging As String, sTmpProduction As String


            sTmpDevelopment = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                                    Constants.RegistryKeys.VALUE__MonitorSettings_ServerBaseUrl_Development,
                                                    Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Development)

            If sTmpDevelopment.StartsWith("http") Then
                sTmpDevelopment = Helpers.XOrObfuscation_v2.Obfuscate(sTmpDevelopment)
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                                    Constants.RegistryKeys.VALUE__MonitorSettings_ServerBaseUrl_Development,
                                                    sTmpDevelopment)
            End If


            sTmpStaging = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                                    Constants.RegistryKeys.VALUE__MonitorSettings_ServerBaseUrl_Staging,
                                                    Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Staging)

            If sTmpStaging.StartsWith("http") Then
                sTmpStaging = Helpers.XOrObfuscation_v2.Obfuscate(sTmpStaging)
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                                    Constants.RegistryKeys.VALUE__MonitorSettings_ServerBaseUrl_Staging,
                                                    sTmpStaging)
            End If


            sTmpProduction = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                                    Constants.RegistryKeys.VALUE__MonitorSettings_ServerBaseUrl_Production,
                                                    Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Production)

            If sTmpProduction.StartsWith("http") Then
                sTmpProduction = Helpers.XOrObfuscation_v2.Obfuscate(sTmpProduction)
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                                    Constants.RegistryKeys.VALUE__MonitorSettings_ServerBaseUrl_Production,
                                                    sTmpProduction)
            End If


            [Private].ApiEnvironment = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_ApiEnvironment,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ApiEnvironment)

            Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_ApiEnvironment,
                                                           [Private].ApiEnvironment)



            If [Private].ApiEnvironment.Equals("development") Or [Private].ApiEnvironment.Equals("dev") Then
                [Private].Server.BaseUrl = Helpers.XOrObfuscation_v2.Deobfuscate(sTmpDevelopment)
            ElseIf [Private].ApiEnvironment.Equals("staging") Then
                [Private].Server.BaseUrl = Helpers.XOrObfuscation_v2.Deobfuscate(sTmpStaging)
            Else
                [Private].Server.BaseUrl = Helpers.XOrObfuscation_v2.Deobfuscate(sTmpProduction)
            End If



            If Not [Private].Server.BaseUrl.EndsWith("/") Then
                [Private].Server.BaseUrl = [Private].Server.BaseUrl & "/"
            End If


            [Private].Server.EndPoint.Heartbeat = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_Heartbeat,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_Heartbeat)

            [Private].Server.EndPoint.ApplicationSetting = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_ApplicationSetting,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_ApplicationSetting)

            [Private].Server.EndPoint.Integrity = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_Integrity,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_Integrity)

            [Private].Server.EndPoint.Inventory = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_Inventory,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_Inventory)

            [Private].Server.EndPoint.Metrics = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_Metrics,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_Metrics)

            [Private].Server.EndPoint.OVCCSettings = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_OVCCSettings,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_OVCCSettings)

            [Private].Server.EndPoint.SendLocalOVCCSettings = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_SendLocalOVCCSettings,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_SendLocalOVCCSettings)

            [Private].Server.EndPoint.OVCCThemes = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_OVCCThemes,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_OVCCThemes)

            [Private].Server.EndPoint.SelfUpdate = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_SelfUpdate,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_SelfUpdate)

            [Private].Server.EndPoint.Screenshots = _Load_ServerUrl_Endpoint(
                Constants.RegistryKeys.VALUE__MonitorSettings_ServerEndPoint_Screenshots,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_ServerEndPoint_Screenshots)

            Return True
        End Function

        Private Shared Function _Load_ServerUrl_Endpoint(valueName As String, valueDefault As String) As String
            Dim sTmpRaw As String = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                                                  valueName,
                                                                  valueDefault)

            Dim sTmp As String = Helpers.XOrObfuscation_v2.Deobfuscate(sTmpRaw)

            If sTmp.Equals("") Then
                sTmp = sTmpRaw

                'value should be obfuscated
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                                 valueName,
                                                 Helpers.XOrObfuscation_v2.Obfuscate(sTmpRaw))
            End If


            Return [Private].Server.BaseUrl & sTmp
        End Function

        Public Shared Function Load_DownloadUrl() As Boolean
            Dim sTmp As String = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Download,
                                                                  Constants.RegistryKeys.VALUE__MonitorSettings_DownloadBaseUrl,
                                                                  Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_DownloadBaseUrl)

            If sTmp.StartsWith("http") Or sTmp.StartsWith("ftp") Or sTmp.StartsWith("sftp") Then
                [Private].Download.BaseUrl = sTmp

                'value should be obfuscated
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Download,
                                                 Constants.RegistryKeys.VALUE__MonitorSettings_DownloadBaseUrl,
                                                 Helpers.XOrObfuscation_v2.Obfuscate(sTmp))
            Else
                [Private].Download.BaseUrl = Helpers.XOrObfuscation_v2.Deobfuscate(sTmp)
            End If

            If Not [Private].Download.BaseUrl.EndsWith("/") Then
                [Private].Download.BaseUrl = [Private].Download.BaseUrl & "/"
            End If


            [Private].Download.Themes = _Load_Download_SubDir(
                Constants.RegistryKeys.VALUE__MonitorSettings_DownloadSubdir_Themes,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_DownloadSubdir_Themes)

            [Private].Download.SelfUpdate = _Load_Download_SubDir(
                Constants.RegistryKeys.VALUE__MonitorSettings_DownloadSubdir_SelfUpdate,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_DownloadSubdir_SelfUpdate)

            [Private].Download.ExternalApps = _Load_Download_SubDir(
                Constants.RegistryKeys.VALUE__MonitorSettings_DownloadSubdir_ExternalApps,
                Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_DownloadSubdir_ExternalApps)


            Return True
        End Function

        Private Shared Function _Load_Download_SubDir(valueName As String, valueDefault As String) As String
            Dim sUrl As String = ""
            Dim sTmp As String = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Download,
                                                                  valueName,
                                                                  valueDefault)

            sTmp = Helpers.XOrObfuscation_v2.Deobfuscate(sTmp)

            If sTmp.Equals("") Then
                sTmp = valueDefault

                'value should be obfuscated
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Download,
                                                 valueName,
                                                 Helpers.XOrObfuscation_v2.Obfuscate(sTmp))
            End If

            sUrl = [Private].Download.BaseUrl & sTmp
            If Not sUrl.EndsWith("/") Then
                sUrl = sUrl & "/"
            End If


            Return sUrl
        End Function

        Public Shared Function Load_LastApplicationSettingsSync() As Boolean
            [Private].LastApplicationSettingsSync = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_LastApplicationSettingsSync,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_LastApplicationSettingsSync)

            Return True
        End Function

        Public Shared Function Load_LastOVCCSettingsSync() As Boolean
            [Private].LastOVCCSettingsSync = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_LastOVCCSettingsSync,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_LastOVCCSettingsSync)

            Return True
        End Function

        Public Shared Function Load_LastOVCCThemesSync() As Boolean
            [Private].LastOVCCThemesSync = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_LastOVCCThemesSync,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_LastOVCCThemesSync)

            Return True
        End Function

        Public Shared Function Load_LastMetricsSync() As Boolean
            [Private].LastMetricsSync = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_LastMetricsSync,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_LastMetricsSync)

            Return True
        End Function

        Public Shared Function Load_LastIntegritySync() As Boolean
            [Private].LastIntegritySync = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_LastIntegritySync,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_LastIntegritySync)

            Return True
        End Function

        Public Shared Function Load_LastInventorySync() As Boolean
            [Private].LastInventorySync = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_LastInventorySync,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_LastInventorySync)

            Return True
        End Function

        Public Shared Function Load_LastGeolocationSync() As Boolean
            [Private].LastGeolocationSync = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_LastGeolocationSync,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_LastGeolocationSync)

            Return True
        End Function

        Public Shared Function Load_ForceUpdate() As Boolean
            [Private].ForceUpdate = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor,
                                                           Constants.RegistryKeys.VALUE__Monitor_ForceUpdate,
                                                           Constants.RegistryKeys.DEFAULTVALUE__Monitor_ForceUpdate)

            Return True
        End Function

        Public Shared Function Load_ForceRequestSettings() As Boolean
            [Private].ForceRequestSettings = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor,
                                                           Constants.RegistryKeys.VALUE__Monitor_ForceRequestSettings,
                                                           Constants.RegistryKeys.DEFAULTVALUE__Monitor_ForceRequestSettings)

            Return True
        End Function

        Public Shared Function Load_NinjaNodeIde() As Boolean
            [Private].NinjaNodeId = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__NinjaAgent,
                                                            Constants.RegistryKeys.VALUE__NinjaNodeID,
                                                            Constants.RegistryKeys.DEFAULTVALUE__NinjaNodeID)

            Return True
        End Function

        Public Shared Function Load_EmergencyMode_Values() As Boolean
            [Private].EmergencyModeDetected = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeDetected,
                                 Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_EmergencyModeDetected)

            [Private].EmergencyModeDetectedCount = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeDetectedCount,
                                 Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_EmergencyModeDetectedCount)

            [Private].EmergencyModeDetectedTimestamp = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeDetectedTimestamp,
                                 Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_EmergencyModeDetectedTimestamp)

            [Private].EmergencyModeFixedCount = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeFixedCount,
                                 Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_EmergencyModeFixedCount)

            [Private].EmergencyModeFixedTimestamp = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeFixedTimestamp,
                                 Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_EmergencyModeFixedTimestamp)

            [Private].EmergencyModeLastRebootTimestamp = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeLastRebootTimestamp,
                                 Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_EmergencyModeLastRebootTimestamp)

            [Private].EmergencyModeFixType = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeFixType,
                                 Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_EmergencyModeFixType)


            Return True
        End Function

        Public Shared Function Load_OVCCTheme_Values() As Boolean
            [Private].OVCCThemeIndex = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                        Constants.RegistryKeys.VALUE__MonitorSettings_OVCCThemeIndex,
                                        Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_OVCCThemeIndex)

            [Private].OVCCThemePath = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                        Constants.RegistryKeys.VALUE__MonitorSettings_OVCCThemePath,
                                        Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_OVCCThemePath)

            Return True
        End Function

        'Public Shared Function Load_MachineRenamed() As Boolean
        '    [Private].MachineWasRenamed = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
        '                                                    Constants.RegistryKeys.VALUE__Monitor_MachineWasRenamed,
        '                                                    Constants.RegistryKeys.DEFAULTVALUE__Monitor_MachineWasRenamed)

        '    [Private].MachineWasRenamed_OldName = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
        '                                                    Constants.RegistryKeys.VALUE__Monitor_MachineWasRenamed_OldName,
        '                                                    Constants.RegistryKeys.DEFAULTVALUE__MachineWasRenamed_OldName)

        '    Return True
        'End Function

        Public Shared Function Save() As Boolean
            Dim aFields() As Reflection.FieldInfo = GetType([Public]).GetFields

            For Each aField As Reflection.FieldInfo In aFields
                Dim o As Object = aField.GetValue(Nothing)

                If TypeOf o Is Integer Then
                    Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, o)
                ElseIf TypeOf o Is Long Then
                    Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, o)
                ElseIf TypeOf o Is Boolean Then
                    Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, o)
                Else
                    Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, o.ToString)
                End If
            Next

            Return True
        End Function

        Public Shared Function Save(sName As String, oValue As Object) As Boolean
            Logger.Debug("save single setting")
            Logger.Debug("name", 1)
            Logger.Debug(sName, 2)
            Logger.Debug("value", 1)
            Logger.Debug(oValue.ToString, 2)

            Dim aFields() As Reflection.FieldInfo = GetType([Public]).GetFields

            For Each aField As Reflection.FieldInfo In aFields
                'Logger.Debug("comparing", 1)
                'Logger.Debug("local", 1)
                'Logger.Debug(aField.Name, 2)
                'Logger.Debug("new", 1)
                'Logger.Debug(sName, 2)

                If aField.Name.Equals(sName) Then
                    Logger.Debug("saving", 1)

                    Dim oCurrentValue As Object = aField.GetValue(Nothing)

                    If TypeOf oCurrentValue Is Integer Then
                        Logger.Debug("Integer", 2)

                        Dim iTmp As Integer
                        If Integer.TryParse(oValue.ToString, iTmp) Then
                            Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, iTmp)
                            Logger.Debug("ok", 3)
                        Else
                            Logger.Debug("not an integer", 3)
                        End If
                    ElseIf TypeOf oCurrentValue Is Long Then
                        Logger.Debug("Long", 2)

                        Dim lTmp As Long
                        If Long.TryParse(oValue.ToString, lTmp) Then
                            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, lTmp)
                            Logger.Debug("ok", 3)
                        Else
                            Logger.Debug("not a long", 3)
                        End If

                        Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, oCurrentValue)
                    ElseIf TypeOf oCurrentValue Is Boolean Then
                        Logger.Debug("Boolean", 2)

                        Dim sTmp As String = oValue.ToString.ToLower

                        If sTmp.Equals("1") Or sTmp.Equals("true") Or sTmp.Equals("yes") Then
                            Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, True)
                        Else
                            Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, False)
                        End If
                    Else
                        Logger.Debug("String", 2)

                        Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application, aField.Name, oValue.ToString)
                    End If

                    Exit For
                End If
            Next

            Return True
        End Function

        Public Shared Function Save_LastSyncs() As Boolean
            Save_LastApplicationSettingsSync()
            Save_LastOVCCSettingsSync()
            Save_LastMetricsSync()
            Save_LastIntegritySync()
            Save_LastInventorySync()
            Save_LastGeolocationSync()

            Return True
        End Function

        Public Shared Function Save_LastHeartbeat() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastHeartbeat,
                                         Globals.LastHeartbeat)

            Return True
        End Function

        Public Shared Function Save_LastScreenshot(value As Long) As Boolean
            Globals.LastScreenshot = value
            Return Save_LastScreenshot()
        End Function
        Public Shared Function Save_LastScreenshot() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastScreenshot,
                                         Globals.LastScreenshot)

            Return True
        End Function

        Public Shared Function Save_LastApplicationSettingsSync(value As Long) As Boolean
            [Private].LastApplicationSettingsSync = value
            Return Save_LastApplicationSettingsSync()
        End Function
        Public Shared Function Save_LastApplicationSettingsSync() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastApplicationSettingsSync,
                                         [Private].LastApplicationSettingsSync)

            Return True
        End Function

        Public Shared Function Save_LastOVCCSettingsSync(value As Long) As Boolean
            [Private].LastOVCCSettingsSync = value
            Return Save_LastOVCCSettingsSync()
        End Function
        Public Shared Function Save_LastOVCCSettingsSync() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastOVCCSettingsSync,
                                         [Private].LastOVCCSettingsSync)

            Return True
        End Function

        Public Shared Function Save_LastOVCCThemesSync(value As Long) As Boolean
            [Private].LastOVCCThemesSync = value
            Return Save_LastOVCCThemesSync()
        End Function
        Public Shared Function Save_LastOVCCThemesSync() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastOVCCThemesSync,
                                         [Private].LastOVCCThemesSync)

            Return True
        End Function

        Public Shared Function Save_LastMetricsSync() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastMetricsSync,
                                         [Private].LastMetricsSync)

            Return True
        End Function

        Public Shared Function Save_LastIntegritySync() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastIntegritySync,
                                         [Private].LastIntegritySync)

            Return True
        End Function

        Public Shared Function Save_LastInventorySync(value As Long) As Boolean
            [Private].LastInventorySync = value
            Return Save_LastInventorySync()
        End Function
        Public Shared Function Save_LastInventorySync() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastInventorySync,
                                         [Private].LastInventorySync)

            Return True
        End Function

        Public Shared Function Save_LastGeolocationSync() As Boolean
            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_LastGeolocationSync,
                                         [Private].LastGeolocationSync)

            Return True
        End Function

        Public Shared Function Save_PropertyId() As Boolean
            Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__Monitor_PropertyId,
                                         [Private].PropertyId)

            Return True
        End Function

        Public Shared Function Save_EmergencyMode_Values() As Boolean
            Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeDetected,
                                 [Private].EmergencyModeDetected)

            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeDetectedCount,
                                 [Private].EmergencyModeDetectedCount)

            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeDetectedTimestamp,
                                 [Private].EmergencyModeDetectedTimestamp)

            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeFixedCount,
                                  [Private].EmergencyModeFixedCount)

            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeFixedTimestamp,
                                 [Private].EmergencyModeFixedTimestamp)

            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeLastRebootTimestamp,
                                 [Private].EmergencyModeLastRebootTimestamp)

            Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                 Constants.RegistryKeys.VALUE__MonitorSettings_EmergencyModeFixType,
                                 [Private].EmergencyModeFixType)


            Return True
        End Function


        'Public Shared Function Save_MachineRenamed() As Boolean
        '    Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
        '                                        Constants.RegistryKeys.VALUE__Monitor_MachineWasRenamed,
        '                                        [Private].MachineWasRenamed)

        '    Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
        '                                        Constants.RegistryKeys.VALUE__Monitor_MachineWasRenamed_OldName,
        '                                        [Private].MachineWasRenamed_OldName)


        '    Return True
        'End Function

        'Public Shared Function Reset_MachineRenamed() As Boolean
        '    Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
        '                                        Constants.RegistryKeys.VALUE__Monitor_MachineWasRenamed)

        '    Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
        '                                        Constants.RegistryKeys.VALUE__Monitor_MachineWasRenamed_OldName)


        '    Return True
        'End Function

        Public Shared Function Reset_ForceUpdate() As Boolean
            [Private].ForceUpdate = False

            Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor,
                                         Constants.RegistryKeys.VALUE__Monitor_ForceUpdate)

            Return True
        End Function

        Public Shared Function Save_ForceUpdate() As Boolean
            Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor,
                                         Constants.RegistryKeys.VALUE__Monitor_ForceUpdate,
                                         [Private].ForceUpdate)

            Return True
        End Function

        Public Shared Function Save_ForceRequestSettings(value As Boolean) As Boolean
            [Private].ForceRequestSettings = value
            Return Save_ForceRequestSettings()
        End Function
        Public Shared Function Save_ForceRequestSettings() As Boolean
            Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor,
                                         Constants.RegistryKeys.VALUE__Monitor_ForceRequestSettings,
                                         [Private].ForceRequestSettings)

            Return True
        End Function

        Public Shared Function Reset_ForceRequestSettings() As Boolean
            [Private].ForceRequestSettings = False

            Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor,
                                         Constants.RegistryKeys.VALUE__Monitor_ForceRequestSettings)

            Return True
        End Function

        Public Shared Function Save_OVCCTheme_Values() As Boolean
            Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                        Constants.RegistryKeys.VALUE__MonitorSettings_OVCCThemeIndex,
                                        [Private].OVCCThemeIndex)

            Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                        Constants.RegistryKeys.VALUE__MonitorSettings_OVCCThemePath,
                                        [Private].OVCCThemePath)

            Return True
        End Function

        Public Shared Function Reset_Heartbeat() As Boolean
            Globals.LastHeartbeat = 0

            Globals.LastApplicationSettingsSync = 0
            Globals.LastIntegritySync = 0
            Globals.LastInventorySync = 0
            Globals.LastMetricsSync = 0
            Globals.LastOVCCSettingsSync = 0

            Return True
        End Function
    End Class

    Public Class Themes
        Public Class Theme
            Private mIndex As Integer
            Public Property Index() As Integer
                Get
                    Return mIndex
                End Get
                Set(ByVal value As Integer)
                    mIndex = value
                End Set
            End Property

            Private mName As String
            Public Property Name() As String
                Get
                    Return mName
                End Get
                Set(ByVal value As String)
                    mName = value
                End Set
            End Property

            Private mVersion As Integer
            Public Property Version() As Integer
                Get
                    Return mVersion
                End Get
                Set(ByVal value As Integer)
                    mVersion = value
                End Set
            End Property

            Public Sub New(Index As Integer, Name As String, Version As Integer)
                mIndex = Index
                mName = Name
                mVersion = Version
            End Sub
        End Class

        Private Shared mThemes As New List(Of Theme)

        Public Shared Sub Reset()
            mThemes = New List(Of Theme)
        End Sub

        Public Shared Sub Add(Index As Integer, Name As String, Version As Integer)
            mThemes.Add(New Theme(Index, Name, Version))
        End Sub

        Public Shared Sub Save()
            Dim sError As String = ""

            Helpers.Registry.DeleteSubKey(RegistryHive.LocalMachine, Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, "Themes", True)

            For Each _theme As Theme In mThemes
                Helpers.Registry.CreateKey(Constants.RegistryKeys.KEY__Monitor_Configuration_Themes & "\" & _theme.Index.ToString)
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Themes & "\" & _theme.Index.ToString, "Name", _theme.Name)
                Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Themes & "\" & _theme.Index.ToString, "Version", _theme.Version)
            Next
        End Sub
    End Class


    Public Class OVCC
        Public Class [Private]
            Public Shared HashTheme As String = ""
            Public Shared HashScreensaver As String = ""
        End Class

        Public Class [Public]
            Public Shared HotelInformationUrl As String = ""
            Public Shared InternetUrl As String = ""
            Public Shared WeatherUrl As String = ""
            Public Shared MapUrl As String = ""
            Public Shared HotelAddress As String = ""

            Public Shared ThemeIndex As Integer = 0
            Public Shared ThemePasswordEnabled As String = ""
            Public Shared ThemePassword As String = ""
            Public Shared ThemePaymentDialog As String = ""

            Public Shared ExpectedFileVersion_Monitor As String = ""
            Public Shared ExpectedFileVersion_SiteKiosk As String = ""
            Public Shared ExpectedFileVersion_Watchdog As String = ""
            Public Shared ExpectedFileVersion_FileExplorer As String = ""
        End Class

        Public Shared Function Load() As Boolean
            Logger.Message("Loading OVCC settings", 0)

            Dim sErrorMessage As String = ""
            Dim aFields() As Reflection.FieldInfo = GetType([Public]).GetFields

            Logger.Debug("loading from registry", 1)
            For Each aField As Reflection.FieldInfo In aFields
                Logger.Debug(aField.Name, 2)

                Dim oReg As Object = Helpers.Registry.FindValueByString(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, sErrorMessage)
                If Not oReg Is Nothing Then
                    Logger.Debug("found in registry", 3)
                    'found it in registry
                    Dim o As Object = aField.GetValue(Nothing)

                    If TypeOf o Is Integer Then
                        Dim i As Integer = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, -1)
                        If i > -1 Then
                            aField.SetValue(Nothing, i)
                        End If

                        Logger.Debug("Integer", 4)
                        Logger.Debug(i.ToString, 5)
                    ElseIf TypeOf o Is Long Then
                        Dim l As Long = Helpers.Registry.GetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, -1)
                        If l > -1 Then
                            aField.SetValue(Nothing, l)
                        End If

                        Logger.Debug("Long", 4)
                        Logger.Debug(l.ToString, 5)
                    ElseIf TypeOf o Is Boolean Then
                        Dim b As Boolean = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, False)
                        aField.SetValue(Nothing, b)

                        Logger.Debug("Boolean", 4)
                        Logger.Debug(b.ToString, 5)
                    Else
                        Dim s As String = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, Constants.Settings.BogusDefaultStringValue)
                        If Not s.Equals(Constants.Settings.BogusDefaultStringValue) Then
                            aField.SetValue(Nothing, s)
                        End If


                        Logger.Debug("String", 4)
                        Logger.Debug(s.ToString, 5)
                    End If
                Else
                    'Save(aField.Name, aField.GetValue(Nothing))
                    Logger.Debug("not found in registry", 3)
                    Logger.Debug(sErrorMessage, 4)
                End If
            Next


            Dim aFields2() As Reflection.FieldInfo = GetType([Public]).GetFields

            For Each aField As Reflection.FieldInfo In aFields2
                Logger.Debug(aField.Name, 1)
                Logger.Debug(aField.GetValue(Nothing), 2)
            Next


            Globals.Files.OVCC_UI_CONFIG_FILE = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "Skins\Public\iBAHN\Scripts\iBAHN_functions.js")
            Globals.Files.OVCC_UI_SCRIPT_FILE = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "Skins\default\Scripts\guest-tek.js")

            Globals.Files.PaymentDialogFiles.ACTIVE = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "SiteCash\Html\PaymentInfoDlg.htm")
            Globals.Files.PaymentDialogFiles.DEFAULT = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "SiteCash\Html\PaymentInfoDlg.DEFAULT.htm")
            Globals.Files.PaymentDialogFiles.PASSWORD = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "SiteCash\Html\PaymentInfoDlg.PASSWORD.htm")
            Globals.Files.PaymentDialogFiles.RESIDENCE_INN = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "SiteCash\Html\PaymentInfoDlg.RESIDENCEINN.htm")


            Return True
        End Function

        Public Shared Function Load_HashTheme() As Boolean
            [Private].HashTheme = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_HashTheme,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_HashTheme)

            Return True
        End Function

        Public Shared Function Load_HashScreensaver() As Boolean
            [Private].HashScreensaver = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC,
                                                           Constants.RegistryKeys.VALUE__MonitorSettings_HashScreensaver,
                                                           Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_HashScreensaver)

            Return True
        End Function


        Public Shared Function Save_HashTheme() As Boolean
            Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_HashTheme,
                                         [Private].HashTheme)

            Return True
        End Function

        Public Shared Function Save_HashScreensaver() As Boolean
            Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_HashScreensaver,
                                         [Private].HashScreensaver)

            Return True
        End Function

        Public Shared Function Save() As Boolean
            Dim aFields() As Reflection.FieldInfo = GetType([Public]).GetFields

            For Each aField As Reflection.FieldInfo In aFields
                Dim o As Object = aField.GetValue(Nothing)

                If TypeOf o Is Integer Then
                    Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, o)
                ElseIf TypeOf o Is Long Then
                    Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, o)
                ElseIf TypeOf o Is Boolean Then
                    Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, o)
                Else
                    Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, o.ToString)
                End If
            Next

            Return True
        End Function

        Public Shared Function Save(sName As String, oValue As Object) As Boolean
            Logger.Debug("save single setting")
            Logger.Debug("name", 1)
            Logger.Debug(sName, 2)
            Logger.Debug("value", 1)
            Logger.Debug(oValue.ToString, 2)

            Dim aFields() As Reflection.FieldInfo = GetType([Public]).GetFields

            For Each aField As Reflection.FieldInfo In aFields
                'Logger.Debug("comparing", 1)
                'Logger.Debug("local", 1)
                'Logger.Debug(aField.Name, 2)
                'Logger.Debug("new", 1)
                'Logger.Debug(sName, 2)

                If aField.Name.Equals(sName) Then
                    Logger.Debug("saving", 1)

                    Dim oCurrentValue As Object = aField.GetValue(Nothing)

                    If TypeOf oCurrentValue Is Integer Then
                        Logger.Debug("Integer", 2)

                        Dim iTmp As Integer
                        If Integer.TryParse(oValue.ToString, iTmp) Then
                            Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, iTmp)
                            Logger.Debug("ok", 3)
                        Else
                            Logger.Debug("not an integer", 3)
                        End If
                    ElseIf TypeOf oCurrentValue Is Long Then
                        Logger.Debug("Long", 2)

                        Dim lTmp As Long
                        If Long.TryParse(oValue.ToString, lTmp) Then
                            Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, lTmp)
                            Logger.Debug("ok", 3)
                        Else
                            Logger.Debug("not a long", 3)
                        End If

                        Helpers.Registry.SetValue_Long(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, oCurrentValue)
                    ElseIf TypeOf oCurrentValue Is Boolean Then
                        Logger.Debug("Boolean", 2)

                        Dim sTmp As String = oValue.ToString.ToLower

                        If sTmp.Equals("1") Or sTmp.Equals("true") Or sTmp.Equals("yes") Then
                            Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, True)
                        Else
                            Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, False)
                        End If
                    Else
                        Logger.Debug("String", 2)

                        Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_OVCC, aField.Name, oValue.ToString)
                    End If

                    Exit For
                End If
            Next

            Return True
        End Function
    End Class
End Class
