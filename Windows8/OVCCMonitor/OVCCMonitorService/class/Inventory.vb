﻿Public Class Inventory
    Public Class Policies
        Public Class SiteKioskUser
            Public Shared Function PolicyScore() As Integer
                Dim retCount As Integer
                Dim sSid As String = ""
                Dim sErrorMessage As String = ""

                If Helpers.WindowsUser.ConvertUsernameToSid(Constants.Profile.SiteKioskUsername, sSid) Then
                    Dim regKey As String = "HKEY_USERS\" & sSid & "\Software\Microsoft\Windows\CurrentVersion\Policies"
                    Dim _l As List(Of String) = Helpers.Registry.GetValuesRecursive(regKey, sErrorMessage)

                    sErrorMessage = sErrorMessage.Trim()
                    If Not sErrorMessage.Equals("") Then
                        Helpers.Logger.WriteWarning("Error occurred while counting the policy score:")
                        Helpers.Logger.WriteWithoutDate(vbCrLf & sErrorMessage & vbCrLf)
                    End If

                    If _l.Count <= 0 Then
                        retCount = -100
                    Else
                        'remove empty items
                        _l.RemoveAll(Function(str) String.IsNullOrWhiteSpace(str))
                        retCount = _l.Count
                    End If
                Else
                    retCount = -200

                    Helpers.Logger.WriteWarning("Error occurred while counting the policy score:")
                    Helpers.Logger.WriteWarning("Error while converting username '" & Constants.Profile.SiteKioskUsername & "' to sid:")
                    Helpers.Logger.WriteWarning(Helpers.Errors.GetLast())
                End If


                'save in registry
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor,
                                         Constants.RegistryKeys.VALUE__Monitor_PolicyScore,
                                         retCount.ToString)


                Return retCount
            End Function
        End Class
    End Class

    Public Class Themes
        Public Shared Function Index(AlreadyHaveTheInternalId As Integer) As Integer
            Return _Index(AlreadyHaveTheInternalId)
        End Function

        Public Shared Function Index() As Integer
            Return _Index(OVCCThemeMatcher.NOT_FOUND)
        End Function

        Private Shared Function _Index(AlreadyHaveTheInternalId As Integer) As Integer
            Dim sThemeIndexFile As String = ""
            Configuration.SkCfg.XML.Lock()
            Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
            Configuration.SkCfg.XML.Load()
            Configuration.SkCfg.XML.GetValue(Constants.SkCfg.ThemeUrl, sThemeIndexFile)
            Configuration.SkCfg.XML.Unlock()


            Dim iThemeIndex As Integer = OVCCThemeMatcher.FindIndex(sThemeIndexFile)

            If iThemeIndex <= OVCCThemeMatcher.NOT_FOUND Then
                'Couldn't find the theme index from path, let's just use the value in the script
                Return _GetIndexFromScript()
            End If

            If iThemeIndex < 666 And iThemeIndex >= 0 And iThemeIndex <> Constants.SiteKiosk.Themes.DisabledThemeIndex Then 'Hilton
                If AlreadyHaveTheInternalId <= OVCCThemeMatcher.NOT_FOUND Then
                    iThemeIndex = _GetIndexFromScript()
                Else
                    iThemeIndex = AlreadyHaveTheInternalId
                End If


                If iThemeIndex >= 666 Then
                    'very wrong, theme is hilton but index doesn't match
                    iThemeIndex = 0 ' default Hilton
                End If
            End If


            Return iThemeIndex
        End Function

        Private Shared Function _GetIndexFromScript() As Integer
            Dim oLocalSettings_iBahnScript As Configuration.iBahnScript.LocalSettings = New Configuration.iBahnScript.LocalSettings

            Configuration.iBahnScript.Load(oLocalSettings_iBahnScript)

            Return oLocalSettings_iBahnScript.ThemeIndex
        End Function
    End Class
End Class
