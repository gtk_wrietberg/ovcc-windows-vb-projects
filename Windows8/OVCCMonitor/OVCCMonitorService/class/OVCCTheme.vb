﻿Public Class OVCCTheme
    Public Shared ReadOnly Property VersionFile As String
        Get
            Return IO.Path.Combine(Constants.Paths.Directory.Versions, Settings.OVCC.Public.ThemeIndex.ToString & Constants.Paths.File.ThemeVersion)
        End Get
    End Property

    Public Shared ReadOnly Property Directory As String
        Get
            Return IO.Path.Combine(Constants.Paths.Directory.Themes, Settings.OVCC.Public.ThemeIndex.ToString)
        End Get
    End Property

    Public Shared ReadOnly Property DirectoryExists As Boolean
        Get
            Return IO.Directory.Exists(Directory)
        End Get
    End Property

    Public Shared Property InstalledVersion() As Integer
        Get
            If IO.File.Exists(VersionFile) Then
                Dim sContents As String = IO.File.ReadAllText(VersionFile)

                Dim iVersion As Integer

                If Integer.TryParse(sContents, iVersion) Then
                    Return iVersion
                Else
                    Return -2
                End If
            Else
                Return -1
            End If
        End Get
        Set(ByVal value As Integer)
            IO.File.WriteAllText(VersionFile, value.ToString)
        End Set
    End Property
End Class
