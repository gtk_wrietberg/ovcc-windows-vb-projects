﻿Public Class PostUpdateCleanup
    Public Class Registry
        Private Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_OVCCThemesScreensavers As String = "EndPoint_OVCCThemesScreensavers"
        Private Shared ReadOnly VALUE__MonitorSettings_DownloadSubdir_Screensavers As String = "Subdir_Screensavers"
        Private Shared ReadOnly VALUE__MonitorSettings_CurrentScreensaverBackup As String = "__DISABLED__CurrentScreensaverBackup"
        Private Shared ReadOnly VALUE__MonitorSettings_CurrentThemeBackup As String = "__DISABLED__CurrentThemeBackup"
        Private Shared ReadOnly VALUE__Monitor_ForcePostUpdateCleaning As String = "ForcePostUpdateCleaning"

        Public Shared Function Clean() As Boolean
            Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Server,
                                         VALUE__MonitorSettings_ServerEndPoint_OVCCThemesScreensavers)

            Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         VALUE__MonitorSettings_DownloadSubdir_Screensavers)

            Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         VALUE__MonitorSettings_CurrentScreensaverBackup)

            Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         VALUE__MonitorSettings_CurrentThemeBackup)

            Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor,
                                         VALUE__Monitor_ForcePostUpdateCleaning)

            Return True
        End Function

        Public Shared Function Update() As Boolean
            Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Configuration_Application_Download,
                                                Constants.RegistryKeys.VALUE__MonitorSettings_DownloadSubdir_Themes,
                                                Helpers.XOrObfuscation_v2.Obfuscate(Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_DownloadSubdir_Themes))


            Return True
        End Function
    End Class
End Class
