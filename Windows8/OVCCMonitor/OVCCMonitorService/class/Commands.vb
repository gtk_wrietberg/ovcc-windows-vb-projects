﻿Imports NAudio.Wave

Public Class Commands
    Public Class ExternalApp
        Private Shared mProcessRunner As ProcessRunner
        Private Shared mThreading_RunExternalApp As Threading.Thread

        Public Shared Sub Run(AppPath As String, AppParams As String)
            Logger.MessageRelative("starting external app", 1)
            Logger.MessageRelative(AppPath & " " & AppParams, 2)

            mProcessRunner = New ProcessRunner

            AddHandler mProcessRunner.Started, AddressOf ProcessRunner_Event__Started
            AddHandler mProcessRunner.Failed, AddressOf ProcessRunner_Event__Failed
            AddHandler mProcessRunner.Busy, AddressOf ProcessRunner_Event__Busy
            AddHandler mProcessRunner.TimeOut, AddressOf ProcessRunner_Event__Timeout
            AddHandler mProcessRunner.Done, AddressOf ProcessRunner_Event__Done
            AddHandler mProcessRunner.Killed, AddressOf ProcessRunner_Event__Killed

            mProcessRunner.FileName = AppPath
            mProcessRunner.Arguments = AppParams
            mProcessRunner.StartProcess()
        End Sub

        Public Shared Sub [Stop]()
            If mProcessRunner IsNot Nothing Then
                Logger.MessageRelative("stopping external app", 1)

                mProcessRunner.StopProcess()
            End If
        End Sub

        Private Shared Sub ProcessRunner_Event__Started(_p As Process, _t As Double)
            Globals.ExternalExecutable.IsRunning = True

            Logger.MessageRelative("EXTERNAL APP EVENT: " & _p.ProcessName & " started", 1)
        End Sub

        Private Shared Sub ProcessRunner_Event__Failed(_p As Process, _t As Double, _e As String)
            Globals.ExternalExecutable.IsRunning = False
            Globals.ExternalExecutable.IsExecuted = False
            Globals.ExternalExecutable.IsDone = True
            Globals.ExternalExecutable.State = "failed: " & _e

            Logger.ErrorRelative("EXTERNAL APP EVENT: " & _p.ProcessName & " failed after " & _t.ToString & " sec - " & _e, 1)
        End Sub

        Private Shared Sub ProcessRunner_Event__Busy(_p As Process, _t As Double)
            Logger.MessageRelative("EXTERNAL APP EVENT: " & _p.ProcessName & " running", 1)
        End Sub

        Private Shared Sub ProcessRunner_Event__Timeout(_p As Process, _t As Double)
            Globals.ExternalExecutable.IsRunning = False
            Globals.ExternalExecutable.IsExecuted = False
            Globals.ExternalExecutable.IsDone = True
            Globals.ExternalExecutable.State = "timeout"

            Logger.ErrorRelative("EXTERNAL APP EVENT: " & _p.ProcessName & " timed out after " & _t.ToString & " sec", 1)
        End Sub

        Private Shared Sub ProcessRunner_Event__Done(_p As Process, _t As Double)
            Globals.ExternalExecutable.IsRunning = False
            Globals.ExternalExecutable.IsExecuted = True
            Globals.ExternalExecutable.IsDone = True
            Globals.ExternalExecutable.State = "done"

            Logger.MessageRelative("EXTERNAL APP EVENT: " & _p.ProcessName & " finished", 1)
        End Sub

        Private Shared Sub ProcessRunner_Event__Killed(_p As Process, _t As Double)
            Globals.ExternalExecutable.IsRunning = False
            Globals.ExternalExecutable.IsExecuted = False
            Globals.ExternalExecutable.IsDone = True
            Globals.ExternalExecutable.State = "killed"

            Logger.MessageRelative("EXTERNAL APP EVENT: " & _p.ProcessName & " killed after " & _t.ToString & " sec", 1)
        End Sub
    End Class

    Public Class ComputerName
        Public Shared Function Rename(newName As String, ByRef sError As String) As Boolean
            Dim strComputer As String
            Dim objWMIService As Object
            Dim objComputers As Object, objComputer As Object

            sError = ""

            Logger.Message(Logger.THREAD.RENAME_MACHINE, "Renaming machine", 0)
            Logger.Message(Logger.THREAD.RENAME_MACHINE, "current name", 1)
            Logger.Message(Logger.THREAD.RENAME_MACHINE, Environment.MachineName, 2)
            Logger.Message(Logger.THREAD.RENAME_MACHINE, "new name", 1)
            Logger.Message(Logger.THREAD.RENAME_MACHINE, newName, 2)

            Try
                strComputer = "."
                objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
                objComputers = objWMIService.ExecQuery("Select * from Win32_ComputerSystem")

                For Each objComputer In objComputers
                    If Globals.TESTING Then
                        Logger.Debug(Logger.THREAD.RENAME_MACHINE, "I would have done 'objComputer.Rename(newName)' here", 1)

                    Else
                        objComputer.Rename(newName)
                    End If
                Next
            Catch ex As Exception
                sError = ex.Message
                Logger.Error(Logger.THREAD.RENAME_MACHINE, "error", 1)
                Logger.Error(Logger.THREAD.RENAME_MACHINE, sError, 2)

                Return False
            End Try


            Logger.Message(Logger.THREAD.RENAME_MACHINE, "ok", 1)


            Return True
        End Function
    End Class

    Public Class RebootShutdown
        Public Shared Sub ForcedReboot()
            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        End Sub
    End Class

    Public Class DisableMachine
        Public Shared Function IsThemeDisabled() As Boolean
            Dim sCurrentTheme As String = ""

            Configuration.SkCfg.XML.Lock()
            Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
            Configuration.SkCfg.XML.Load()
            Configuration.SkCfg.XML.GetValue(Constants.SkCfg.ThemeUrl, sCurrentTheme)
            Configuration.SkCfg.XML.Unlock()

            Return sCurrentTheme.Contains(Constants.SiteKiosk.Themes.DisabledThemeName)
        End Function


        Public Shared Function ForceDisabledTheme() As Boolean
            Dim bRet As Boolean = False

            Logger.Message("Forcing disabled theme", 1)

            Try
                Dim sTempString As String = Guid.NewGuid().ToString()
                Dim sTempBaseDirectory As String = IO.Path.GetTempPath()
                Dim sTempDirectory As String = IO.Path.Combine(sTempBaseDirectory, "_disabled_theme_" & sTempString)
                Dim sTempZipFile As String = IO.Path.Combine(sTempDirectory, Constants.SiteKiosk.Themes.DisabledThemeName & ".zip")
                Dim sTempThemeDirectory As String = IO.Path.Combine(sTempDirectory, Constants.SiteKiosk.Themes.DisabledThemeName)
                Dim sThemePath As String = IO.Path.Combine(Constants.Paths.Directory.Themes, Constants.SiteKiosk.Themes.DisabledThemeName)
                Dim sScreensaverPath As String = IO.Path.Combine(sThemePath, "screensaver")
                Dim iCurrentThemeIndex As Integer = Inventory.Themes.Index


                Logger.Message("Current theme", 2)
                Logger.Message(iCurrentThemeIndex.ToString, 3)

                Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                     Constants.RegistryKeys.VALUE__MonitorSettings_CurrentThemeIndexBackup,
                                                     iCurrentThemeIndex)


                If iCurrentThemeIndex = Constants.SiteKiosk.Themes.DisabledThemeIndex Then
                    'Already disabled
                    Logger.Message("Already disabled", 4)

                    Return True
                End If


                Logger.Message("Unpacking __disabled theme", 2)
                If Not IO.Directory.Exists(sThemePath) Then
                    If Not IO.Directory.Exists(sTempDirectory) Then
                        Logger.Debug("creating folder", 3)
                        Logger.Debug(sTempDirectory, 4)
                        IO.Directory.CreateDirectory(sTempDirectory)
                    End If

                    Logger.Debug("copying resource file", 3)
                    IO.File.WriteAllBytes(sTempZipFile, My.Resources.__disabled)

                    Logger.Debug("unpacking zip file", 3)
                    Logger.Debug("source", 4)
                    Logger.Debug(sTempZipFile, 5)
                    Logger.Debug("destination", 4)
                    Logger.Debug(sTempDirectory, 5)
                    IO.Compression.ZipFile.ExtractToDirectory(sTempZipFile, sTempDirectory)

                    If Not IO.Directory.Exists(sTempThemeDirectory) Then
                        'Apparently, unzipping failed
                        Throw New Exception("Unzip failed!")
                    End If

                    Logger.Debug("copy theme files", 3)
                    Logger.Debug("source", 4)
                    Logger.Debug(sTempDirectory, 5)
                    Logger.Debug("destination", 4)
                    Logger.Debug(sThemePath, 5)
                    CopyDirectoryContents.CopyDirectoryContents(sTempThemeDirectory, sThemePath)

                    Logger.Debug("remove temp folder", 3)
                    Logger.Debug(sTempDirectory, 4)
                    IO.Directory.Delete(sTempDirectory, True)
                Else
                    Logger.Debug("__disabled theme already exists", 3)
                End If


                Dim sThemeSitekioskPath As String = Helpers.SiteKiosk.TranslateNormalPathToSiteKioskPath(IO.Path.Combine(sThemePath, "index.html"))
                Dim sScreensaverSitekioskPath As String = Helpers.SiteKiosk.TranslateNormalPathToSiteKioskPath(IO.Path.Combine(sScreensaverPath, "index.html"))

                Logger.Debug("update sk config", 2)
                Configuration.SkCfg.XML.Lock()
                Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
                Configuration.SkCfg.XML.Load()
                Configuration.SkCfg.XML.SetValue(Constants.SkCfg.ThemeUrl, sThemeSitekioskPath)
                Configuration.SkCfg.XML.SetValue(Constants.SkCfg.ScreensaverUrl, sScreensaverSitekioskPath)
                Configuration.SkCfg.XML.Save()


                Logger.Message("ok", 2)

                bRet = True
            Catch ex As Exception
                Logger.Error("error", 2)
                Logger.Error(ex.Message, 3)

                bRet = False
            End Try

            Return bRet
        End Function

        Public Shared Sub RestoreOriginalTheme()
            Dim sBackupThemeIndex As Integer = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                            Constants.RegistryKeys.VALUE__MonitorSettings_CurrentThemeIndexBackup,
                                                            Constants.RegistryKeys.DEFAULTVALUE__MonitorSettings_CurrentThemeIndexBackup)


            Dim sThemePath As String = IO.Path.Combine(Constants.Paths.Directory.Themes, sBackupThemeIndex.ToString)

            Dim sThemeIndexFile As String = Helpers.SiteKiosk.TranslateNormalPathToSiteKioskPath(IO.Path.Combine(OVCCTheme.Directory, "index.html"))
            Dim sThemeLogoutFile As String = Helpers.SiteKiosk.TranslateNormalPathToSiteKioskPath(IO.Path.Combine(OVCCTheme.Directory, "logout.html"))
            Dim sScreensaverIndexFile As String = Helpers.SiteKiosk.TranslateNormalPathToSiteKioskPath(IO.Path.Combine(OVCCTheme.Directory, "screensaver\index.html"))


            Configuration.SkCfg.XML.Lock()
            Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
            Configuration.SkCfg.XML.Load()
            Configuration.SkCfg.XML.SetValue(Constants.SkCfg.ThemeUrl, sThemeIndexFile)
            Configuration.SkCfg.XML.SetValue(Constants.SkCfg.ScreensaverUrl, sScreensaverIndexFile)
            Configuration.SkCfg.XML.SetValue(Constants.SkCfg.LogoutUrl, sThemeLogoutFile)
            Configuration.SkCfg.XML.Save()


            Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                         Constants.RegistryKeys.VALUE__MonitorSettings_CurrentThemeIndexBackup)
        End Sub
    End Class

    Public Class Seppuku
        Public Shared Sub [Commit]()
            jisei()
        End Sub

        Private Shared Sub jisei()
            Logger.Message("Seppuku", 0)

            Logger.Message("我死なば", 1)
            Logger.Message("酒屋の瓶の", 1)
            Logger.Message("下にいけよ", 1)
            Logger.Message("もしや雫の", 1)
            Logger.Message("もりやせんなん", 1)

            ' Bury Me when I die
            ' beneath a wine barrel
            ' in a tavern.
            ' With luck
            ' the cask will leak.

            japanese_harp()

            Dim sRet As String = ServiceInstaller.ChangeStartupType(Constants.Service.Name, ServiceBootFlag.Disabled)
            ServiceInstaller.StopService(Constants.Service.Name)
        End Sub

        Private Shared Sub japanese_harp()
            Dim myMemStream As New IO.MemoryStream
            My.Resources.japanese_harp_shot_83bpm.CopyTo(myMemStream)
            Dim myBytes() As Byte = myMemStream.ToArray
            Dim myFile As String = IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.Temp & "japanese_harp_shot_83bpm.wav")

            IO.File.WriteAllBytes(myFile, myBytes)

            Using audioFile As New AudioFileReader(myFile)
                Using outputDevice As New WaveOutEvent
                    outputDevice.Init(audioFile)
                    outputDevice.Play()
                    While outputDevice.PlaybackState = PlaybackState.Playing
                        Threading.Thread.Sleep(500)
                    End While
                End Using
            End Using
        End Sub
    End Class
End Class
