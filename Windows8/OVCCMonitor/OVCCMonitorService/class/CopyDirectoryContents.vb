﻿Public Class CopyDirectoryContents
    Public Shared Function CopyDirectoryContents(ByVal SrcPath As String, ByVal DestPath As String) As Boolean
        Static bRet As Boolean = False

        If Not System.IO.Directory.Exists(SrcPath) Then
            Throw New System.IO.DirectoryNotFoundException("The directory " & SrcPath & " does not exists")
        End If


        If DestPath.Substring(DestPath.Length - 1, 1) <> System.IO.Path.DirectorySeparatorChar Then
            DestPath += System.IO.Path.DirectorySeparatorChar
        End If

        If Not System.IO.Directory.Exists(DestPath) Then System.IO.Directory.CreateDirectory(DestPath)

        Dim Files As String()
        Files = System.IO.Directory.GetFileSystemEntries(SrcPath)

        Dim element As String
        For Each element In Files
            If System.IO.Directory.Exists(element) Then
                bRet = CopyDirectoryContents(element, DestPath & System.IO.Path.GetFileName(element))
            Else
                Try
                    System.IO.File.Copy(element, DestPath & System.IO.Path.GetFileName(element), True)
                Catch ex As Exception
                    bRet = False
                End Try
            End If
        Next

        Return bRet
    End Function
End Class
