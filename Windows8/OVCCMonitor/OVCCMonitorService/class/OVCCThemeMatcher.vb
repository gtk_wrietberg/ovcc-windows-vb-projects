﻿Public Class OVCCThemeMatcher
    Private Class _theme
        Public Index As Integer
        Public DirectoryName As String
        Public HtmlName As String

        Public Sub New(_Index As Integer, _DirectoryName As String, _HtmlName As String)
            Me.Index = _Index
            Me.DirectoryName = _DirectoryName
            Me.HtmlName = _HtmlName
        End Sub
    End Class

    Private Shared mThemes As List(Of _theme)

    Public Shared ReadOnly NOT_FOUND As Integer = -1

    Public Shared Sub Initialise()
        mThemes = New List(Of _theme)

        _add(Constants.SiteKiosk.Themes.DisabledThemeIndex, Constants.SiteKiosk.Themes.DisabledThemeName, "")

        _add(0, "Hilton", "index.html")
        _add(1, "Hilton", "index.html")
        _add(2, "Hilton", "index.html")
        _add(3, "Hilton", "index.html")
        _add(4, "Hilton", "index.html")
        _add(5, "Hilton", "index.html")
        _add(6, "Hilton", "index.html")
        _add(7, "Hilton", "index.html")
        _add(8, "Hilton", "index.html")
        _add(9, "Hilton", "index.html")
        _add(10, "Hilton", "index.html")
        _add(11, "Hilton", "index.html")
        _add(12, "Hilton", "index.html")
        _add(13, "Hilton", "index.html")
        _add(14, "Hilton", "index.html")
        _add(15, "Hilton", "index.html")
        _add(16, "Hilton", "index.html")
        _add(17, "Hilton", "index.html")

        _add(666, "iBAHN", "")
        _add(667, "Guest-tek", "")
        _add(668, "ResidenceInn", "")
        _add(670, "TownePlace", "")
        _add(671, "HotelArts", "")
        _add(672, "Radisson_CIS", "")
        _add(673, "FairfieldInn", "")
        _add(674, "RitzCarlton", "")
        _add(675, "Element", "")
        _add(676, "Fairmont", "")

        _add(700, "Marriott", "")

        _add(701, "", "index_AC-Hotels.html")
        _add(702, "", "index_Autograph.html")
        _add(703, "", "index_Courtyard.html")
        _add(704, "", "index_Fairfield.html")
        _add(705, "", "index_Fourpoints.html")
        _add(706, "", "index_JW.html")
        _add(707, "", "index_LuxuryCollection.html")
        _add(708, "", "index_Meridien.html")
        _add(709, "", "index_Renaissance.html")
        _add(710, "", "index_Residence-Inn.html")
        _add(711, "", "index_Ritz-Carlton.html")
        _add(712, "", "index_Sheraton.html")
        _add(713, "", "index_Springhill-Suites.html")
        _add(714, "", "index_StRegis.html")
        _add(715, "", "index_Townplace-Suites.html")
        _add(716, "", "index_Westin.html")

        _add(701, "AC-Hotels", "")
        _add(702, "Autograph", "")
        _add(703, "Courtyard", "")
        _add(704, "Fairfield", "")
        _add(705, "Fourpoints", "")
        _add(706, "JW-Marriott", "")
        _add(707, "LuxuryCollection", "")
        _add(708, "Meridien", "")
        _add(709, "Renaissance", "")
        _add(710, "Residence-Inn", "")
        _add(711, "Ritz-Carlton", "")
        _add(712, "Sheraton", "")
        _add(713, "Springhill-Suites", "")
        _add(714, "Marriott-StRegis", "")
        _add(715, "Towneplace-Suites", "")
        _add(716, "Westin", "")

        _add(715, "Townplace-Suites", "")
    End Sub

    Private Shared Sub _add(Index As Integer, DirectoryName As String, HtmlName As String)
        mThemes.Add(New _theme(Index, DirectoryName, HtmlName))
    End Sub

    Public Shared Function Exists(FullPath As String) As Boolean
        Return FindIndex(FullPath) >= 0
    End Function

    Public Shared Function Exists(SearchDirectoryName As String, SearchHtmlName As String) As Integer
        Return FindIndex(SearchDirectoryName, SearchHtmlName) >= 0
    End Function

    Public Shared Function FindIndex(FullPath As String) As Integer
        Dim sFullPath As String = ""
        sFullPath = Helpers.SiteKiosk.TranslateSiteKioskPathToNormalPath(FullPath)

        Dim aDirs As String() = sFullPath.Split("\")

        If aDirs.Count < 2 Then
            'huh?
            Return NOT_FOUND
        End If

        Dim sLastDir As String = aDirs(aDirs.Count - 2)
        Dim sHtmlFile As String = aDirs(aDirs.Count - 1)

        Dim iLastDir As Integer = -1
        If Integer.TryParse(sLastDir, iLastDir) Then
            Return iLastDir
        End If

        Return FindIndex(sLastDir, sHtmlFile)
    End Function

    Public Shared Function FindIndex(SearchDirectoryName As String, SearchHtmlName As String) As Integer
        Dim iIndexMatch As Integer = NOT_FOUND

        If SearchHtmlName.Equals("index.html") Then
            SearchHtmlName = "OIHOIPSUm9v8374u98umv8p93mu4p8tvmu3p4utvmp"
        End If

        For Each _t As _theme In mThemes
            If _t.HtmlName.Equals(SearchHtmlName) Then
                iIndexMatch = _t.Index
                Continue For
            End If

            If _t.DirectoryName.Equals(SearchDirectoryName) Then
                iIndexMatch = _t.Index
                Continue For
            End If
        Next

        Return iIndexMatch
    End Function
End Class
