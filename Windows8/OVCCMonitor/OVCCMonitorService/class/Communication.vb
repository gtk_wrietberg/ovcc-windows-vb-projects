﻿Imports System.Net
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class Communication
    Public Shared Function ValidateRemoteCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
        Return True
    End Function

    Public Class JSON
        Private mServerUrl As String
        Public Property ServerUrl() As String
            Get
                Return mServerUrl
            End Get
            Set(ByVal value As String)
                mServerUrl = value
            End Set
        End Property

        Public Sub New()
            mServerUrl = ""
        End Sub

        Public Sub New(ServerUrl As String)
            mServerUrl = ServerUrl
        End Sub


        Public Request As New JObject
        Public RawResponse As New JObject
        Public Body As New JObject

        Public LastError As String = ""

        Public Function Send() As Boolean
            Dim webClient As New WebClient()
            Dim resByte As Byte()
            Dim resString As String = ""
            Dim reqString() As Byte

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12
            'ServicePointManager.ServerCertificateValidationCallback = AddressOf ValidateRemoteCertificate

            Try
                Logger.DebugRelative(Logger.THREAD.MAIN, "sending request to: " & mServerUrl, 1)

                webClient.Headers("content-type") = "application/json"
                webClient.Headers.Add("User-Agent: " & Constants.Communication.UserAgentString)
                reqString = Encoding.Default.GetBytes(JsonConvert.SerializeObject(Me.Request, Formatting.Indented))
                resByte = webClient.UploadData(mServerUrl, "post", reqString)
                resString = Encoding.Default.GetString(resByte)
                Me.RawResponse = JObject.Parse(resString)
                Me.Body = JObject.Parse(Me.RawResponse.GetValue("body"))

                webClient.Dispose()

                Me.LastError = ""

                Return True
            Catch ex As Exception
                Logger.ErrorRelative(Logger.THREAD.MAIN, "Sending request to '" & mServerUrl & "' failed!", 1)
                Logger.ErrorRelative(Logger.THREAD.MAIN, "raw response", 2)
                Logger.ErrorRelative(Logger.THREAD.MAIN, resString, 3)
                Logger.ErrorRelative(Logger.THREAD.MAIN, "exception", 2)
                Logger.ErrorRelative(Logger.THREAD.MAIN, ex.Message, 3)

                Me.LastError = ex.Message
            End Try

            Return False
        End Function
    End Class

    Public Class GeoLocation
        Private mServerUrl As String
        Public Property ServerUrl() As String
            Get
                Return mServerUrl
            End Get
            Set(ByVal value As String)
                mServerUrl = value
            End Set
        End Property

        Public Sub New()
            mServerUrl = ""
        End Sub

        Public Sub New(ServerUrl As String)
            mServerUrl = ServerUrl
        End Sub


        Public Body As New JObject

        Public LastError As String = ""

        Public Function Send() As Boolean
            Dim webClient As New WebClient()
            Dim resString As String = ""

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12
            'ServicePointManager.ServerCertificateValidationCallback = AddressOf ValidateRemoteCertificate

            Try
                Logger.DebugRelative(Logger.THREAD.MAIN, "sending request to: " & mServerUrl, 1)

                webClient.Headers("Accept") = "application/json"
                resString = webClient.UploadString(mServerUrl, "")
                Me.Body = JObject.Parse(resString)

                webClient.Dispose()

                Me.LastError = ""

                Return True
            Catch ex As Exception
                Logger.ErrorRelative(Logger.THREAD.MAIN, "Sending request to '" & mServerUrl & "' failed!", 1)
                Logger.ErrorRelative(Logger.THREAD.MAIN, "raw response", 2)
                Logger.ErrorRelative(Logger.THREAD.MAIN, resString, 3)
                Logger.ErrorRelative(Logger.THREAD.MAIN, "exception", 2)
                Logger.ErrorRelative(Logger.THREAD.MAIN, ex.Message, 3)

                Me.LastError = ex.Message
            End Try

            Return False
        End Function
    End Class

    Public Class Download
        Public Shared Function File(sUrl As String, sDestinationPath As String) As Boolean
            Dim sVoid As String = ""

            Return File(sUrl, "", "", sDestinationPath, sVoid)
        End Function

        Public Shared Function File(sUrl As String, sDestinationPath As String, ByRef ErrorMessage As String) As Boolean
            Return File(sUrl, "", "", sDestinationPath, ErrorMessage)
        End Function

        Public Shared Function File(sUrl As String, sUsername As String, sPassword As String, sDestinationPath As String, ByRef ErrorMessage As String) As Boolean
            ErrorMessage = ""

            Try
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 Or SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12

                Using client As New WebClient()
                    If Not sUsername.Equals("") And Not sPassword.Equals("") Then
                        client.Credentials = New NetworkCredential(sUsername, sPassword)
                    End If

                    client.CachePolicy = New Cache.RequestCachePolicy(Cache.RequestCacheLevel.NoCacheNoStore)
                    client.Headers.Add("User-Agent: " & Constants.Communication.UserAgentString)
                    client.DownloadFile(sUrl, sDestinationPath)
                End Using

                If IO.File.Exists(sDestinationPath) Then
                    Return True
                Else
                    Throw New Exception("File does not exist")
                End If
            Catch ex As Exception
                Logger.Error(Logger.THREAD.MAIN, "Downloading '" & sUrl & "' to '" & sDestinationPath & "' failed!")
                Logger.Error(Logger.THREAD.MAIN, "exception", 1)
                Logger.Error(Logger.THREAD.MAIN, ex.Message, 2)

                ErrorMessage = ex.Message
            End Try

            Return False
        End Function
    End Class
End Class
