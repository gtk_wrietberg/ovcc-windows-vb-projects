﻿Imports System.Globalization
Imports System.Text
Imports Newtonsoft.Json.Linq

Public Class Globals
    Public Shared TESTING As Boolean = False


    Public Shared LoopAbort As Boolean = False

    Public Shared MachineIdentifier As String = ""
    Public Shared MonitorIdentifier As String = ""

    Public Shared MachineId As Integer = 0
    Public Shared Verified As Integer = -1
    Public Shared Hidden As Integer = -1
    Public Shared RequestSettings As Integer = 0
    Public Shared LastHeartbeat As Long = 0
    Public Shared LastScreenshot As Long = 0
    Public Shared LastApplicationSettingsSync As Long = 0
    Public Shared LastOVCCSettingsSync As Long = 0
    Public Shared LastOVCCThemesSync As Long = 0
    Public Shared LastMetricsSync As Long = 0
    Public Shared LastIntegritySync As Long = 0
    Public Shared LastInventorySync As Long = 0
    Public Shared LastLogfileReceived As Long = 0
    Public Shared MonitorLastVersion As New Version

    Public Shared FirstRun As Boolean = True

    Public Shared MachineIsInUse As Boolean = False

    Public Shared NewMachineNameFromConsole As String = ""
    Public Shared MachineWasRenamed As Boolean = False
    Public Shared MachineRenameResult As Boolean = False

    Public Shared SystemCultureInfo As CultureInfo

    'Some globals for test use
    Public Shared MonitorUpdateEnabled As Boolean = True
    Public Shared EchoLoggerMessagesInConsole As Boolean = True
    Public Shared SkipScreenshotWhenRunningManually As Boolean = False


    Public Class ExternalExecutable
        Public Shared Sub [Reset]()
            Name = ""
            Params = ""
            DownloadedFile = ""
            IsBusy = False
            IsRunning = False
            IsDownloaded = False
            IsExecuted = False
            IsDone = False
            IsReadyToReport = False
            State = ""
        End Sub

        Public Shared Property Name() As String
            Get
                Return Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_Name,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_Name)
            End Get
            Set(ByVal value As String)
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_Name,
                                                  value)
            End Set
        End Property

        Public Shared Property Params() As String
            Get
                Return Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_Params,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_Params)
            End Get
            Set(ByVal value As String)
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_Params,
                                                  value)
            End Set
        End Property

        Public Shared Property DownloadedFile() As String
            Get
                Return Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_DownloadedFile,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_DownloadedFile)
            End Get
            Set(ByVal value As String)
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_DownloadedFile,
                                                  value)
            End Set
        End Property

        Public Shared Property IsBusy() As Boolean
            Get
                Return Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsBusy,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_IsBusy)
            End Get
            Set(ByVal value As Boolean)
                Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsBusy,
                                                  value)
            End Set
        End Property

        Public Shared Property IsRunning() As Boolean
            Get
                Return Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsRunning,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_IsRunning)
            End Get
            Set(ByVal value As Boolean)
                Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsRunning,
                                                  value)
            End Set
        End Property

        Public Shared Property IsDownloaded() As Boolean
            Get
                Return Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsDownloaded,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_IsDownloaded)
            End Get
            Set(ByVal value As Boolean)
                Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsDownloaded,
                                                  value)
            End Set
        End Property

        Public Shared Property IsExecuted() As Boolean
            Get
                Return Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsExecuted,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_IsExecuted)
            End Get
            Set(ByVal value As Boolean)
                Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsExecuted,
                                                  value)
            End Set
        End Property

        Public Shared Property IsDone() As Boolean
            Get
                Return Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsDone,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_IsDone)
            End Get
            Set(ByVal value As Boolean)
                Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsDone,
                                                  value)
            End Set
        End Property

        Public Shared Property IsReadyToReport() As Boolean
            Get
                Return Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsReadyToReport,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_IsReadyToReport)
            End Get
            Set(ByVal value As Boolean)
                Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_IsReadyToReport,
                                                  value)
            End Set
        End Property

        Public Shared Property State() As String
            Get
                Return Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_State,
                                                  Constants.RegistryKeys.DEFAULTVALUE__Monitor_ExternalAppExecute_State)
            End Get
            Set(ByVal value As String)
                Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_ExternalAppExecute,
                                                  Constants.RegistryKeys.VALUE__Monitor_ExternalAppExecute_State,
                                                  value)
            End Set
        End Property
    End Class

    Public Class CurrentMetrics
        Public Shared CurrentUser As String = ""
    End Class

    Public Class Files
        Public Shared OVCC_UI_SCRIPT_FILE As String = ""
        Public Shared OVCC_UI_CONFIG_FILE As String = ""
        Public Shared OVCC_UI_PASSWORD_FILE As String = ""

        Public Class PaymentDialogFiles
            Public Shared ACTIVE As String = ""
            Public Shared [DEFAULT] As String = ""
            Public Shared PASSWORD As String = ""
            Public Shared RESIDENCE_INN As String = ""
        End Class
    End Class

    Public Class MemoryCpu
        Public Shared CurrentMemoryPercentage As Integer = -1
        Public Shared CurrentCpuPercentage As Integer = -1

        Public Shared AverageMemoryPercentage As Integer = -1
        Public Shared AverageCpuPercentage As Integer = -1
        Public Shared AverageCounter As Integer = 0
    End Class
End Class
