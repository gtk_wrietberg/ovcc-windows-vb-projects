﻿Public Class Actions
    Private Shared mThreading_LogZipper As Threading.Thread
    Private Shared mThreading_Heartbeat As Threading.Thread
    Private Shared mThreading_MemoryCpu As Threading.Thread
    Private Shared mThreading_EmergencyMode As Threading.Thread

    Private Shared WithEvents mProcessRunner As ProcessRunner

    Public Shared ThreadAction As THREAD_ACTIONS = THREAD_ACTIONS.NOTHING


    Public Enum THREAD_ACTIONS
        [NOTHING] = 0
        APPLICATION_SETTINGS = 1
        OVCC_SETTINGS = 2
        OVCC_THEMES_SYNC = 4
        OVCC_THEME_DOWNLOAD = 8
        INTEGRITY = 16
        INVENTORY = 32
        METRICS = 64
        SELF_UPDATE = 128
        REQUESTED_SETTINGS = 256
        RUN_EXTERNAL_APP = 512
        POST_UPDATE_CLEANUP = 1024
        RESTART_SITEKIOSK = 2048
        MACHINE_NOT_VERIFIED_YET = 1048576
    End Enum

    Public Overloads Shared Function ToString() As String
        Dim sRet As String = ""

        sRet = ThreadAction.ToString

        Dim lTmp As New List(Of String)
        Dim aCodes As Array
        aCodes = System.Enum.GetValues(GetType(THREAD_ACTIONS))

        For Each iCode As Integer In aCodes
            If ThreadAction And iCode Then
                lTmp.Add(iCode.ToString & "=" & System.Enum.GetName(GetType(THREAD_ACTIONS), iCode))
            End If
        Next

        sRet = sRet & " (" & String.Join(" + ", lTmp) & ")"

        Return sRet
    End Function

    Public Shared Function HandleActions() As Boolean
        Try
            Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)

            Dim bIsFirstRunAfterUpdate As Boolean = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Monitor, Constants.RegistryKeys.VALUE__Monitor_IsFirstRunAfterUpdate, Constants.RegistryKeys.DEFAULTVALUE__Monitor_IsFirstRunAfterUpdate)
            If bIsFirstRunAfterUpdate Then
                ACTION_FirstRunAfterUpdate()

                Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
            End If


            If ThreadAction And THREAD_ACTIONS.POST_UPDATE_CLEANUP Then
                Logger.Message(Logger.THREAD.POST_UPDATE_CLEANUP, "Started", 0)
                ACTION_PostUpdateCleanup()

                Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
            End If


            If ThreadAction And THREAD_ACTIONS.RUN_EXTERNAL_APP Then
                Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, "Started", 0)
                ACTION_RunExternalApp()

                Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
            End If


            If ThreadAction And THREAD_ACTIONS.SELF_UPDATE Then
                Logger.Message(Logger.THREAD.SELF_UPDATE, "Started", 0)
                If ACTION_SelfUpdate() Then
                    Return True
                End If
            End If


            If ThreadAction And THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET Then
                Logger.Message(Logger.THREAD.SEND_LOCAL_OVCCSETTINGS, "Started", 0)
                ACTION_SendLocalOVCCSettings()

                Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
            End If


            If ThreadAction And THREAD_ACTIONS.REQUESTED_SETTINGS Then
                Logger.Message(Logger.THREAD.SEND_LOCAL_OVCCSETTINGS, "Started", 0)
                ACTION_SendLocalOVCCSettings(True)

                Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
            End If


            If ThreadAction And THREAD_ACTIONS.INTEGRITY Then
                If ThreadAction And THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET Then
                    Logger.Message(Logger.THREAD.INTEGRITY, "Skipped, machine not verified", 0)
                Else
                    Logger.Message(Logger.THREAD.INTEGRITY, "Started", 0)
                    ACTION_Integrity()

                    Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
                End If
            End If


            If ThreadAction And THREAD_ACTIONS.APPLICATION_SETTINGS Then
                If ThreadAction And THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET Then
                    Logger.Message(Logger.THREAD.APP_SETTINGS, "Skipped, machine not verified", 0)
                Else
                    Logger.Message(Logger.THREAD.APP_SETTINGS, "Started", 0)
                    ACTION_ApplicationSettings()

                    Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
                End If
            End If


            If ThreadAction And THREAD_ACTIONS.OVCC_SETTINGS Then
                If ThreadAction And THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET Then
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "Skipped, machine not verified", 0)
                Else
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "Started", 0)
                    ACTION_OVCCSettings()

                    Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
                End If
            End If


            If ThreadAction And THREAD_ACTIONS.OVCC_THEMES_SYNC Then
                If ThreadAction And THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET Then
                    Logger.Message(Logger.THREAD.THEMES_SYNC, "Skipped, machine not verified", 0)
                Else
                    Logger.Message(Logger.THREAD.THEMES_SYNC, "Started", 0)
                    ACTION_ThemeSync()

                    Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
                End If
            End If


            If ThreadAction And THREAD_ACTIONS.OVCC_THEME_DOWNLOAD Then
                If ThreadAction And THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET Then
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "Skipped, machine not verified", 0)
                Else
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "Started", 0)
                    ACTION_ThemeDownload()

                    Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
                End If
            End If


            If ThreadAction And THREAD_ACTIONS.INVENTORY Then
                If ThreadAction And THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET Then
                    Logger.Message(Logger.THREAD.INVENTORY, "Skipped, machine not verified", 0)
                Else
                    Logger.Message(Logger.THREAD.INVENTORY, "Started", 0)
                    ACTION_Inventory()

                    Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
                End If
            End If


            If ThreadAction And THREAD_ACTIONS.METRICS Then
                If ThreadAction And THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET Then
                    Logger.Message(Logger.THREAD.METRICS, "Skipped, machine not verified", 0)
                Else
                    Logger.Message(Logger.THREAD.METRICS, "Started", 0)
                    ACTION_Metrics()

                    Threading.Thread.Sleep(Constants.TimeoutsAndDelays.ActionBreather)
                End If
            End If


            If ThreadAction And THREAD_ACTIONS.RESTART_SITEKIOSK Then
                Logger.Message(Logger.THREAD.SITEKIOSK, "Restart triggered", 0)
                ACTION_RestartSitekiosk()
            End If
        Catch ex As Exception
            Logger.Error(Logger.THREAD.MAIN, "ERROR", 0)
            Logger.Error(Logger.THREAD.MAIN, ex.Message, 1)

            Return False
        End Try


        Return True
    End Function


#Region "threads"
#Region "Memory/CPU thread"
    Public Class MemoryCpuThread
        Private Shared mActive As Boolean = False
        Public Shared ReadOnly Property IsActive() As Boolean
            Get
                Return mActive
            End Get
        End Property

        Public Shared Sub Start()
            mThreading_MemoryCpu = New Threading.Thread(New Threading.ThreadStart(AddressOf _work))
            mThreading_MemoryCpu.Start()
        End Sub

        Public Shared Sub [Stop]()
            Logger.Message(Logger.THREAD.MEM_CPU, "Stopping", 0)
            Try
                mThreading_MemoryCpu.Abort()
            Catch ex As Exception

            End Try
        End Sub

        Private Shared Sub _work()
            mActive = True

            Try
                Globals.MemoryCpu.AverageCounter = 0
                Globals.MemoryCpu.AverageMemoryPercentage = 0
                Globals.MemoryCpu.AverageCpuPercentage = 0

                Dim cpu As New PerformanceCounter()
                With cpu
                    .CategoryName = "Processor Information"
                    .CounterName = "% Processor Utility"
                    .InstanceName = "_Total"
                End With

                Do
                    Dim percentage_AvailableMemory As Integer = Math.Ceiling((My.Computer.Info.AvailablePhysicalMemory / My.Computer.Info.TotalPhysicalMemory) * 100)
                    Dim percentage_CpuUsage As Integer = Math.Ceiling(cpu.NextValue())

                    Globals.MemoryCpu.CurrentMemoryPercentage = percentage_AvailableMemory
                    Globals.MemoryCpu.CurrentCpuPercentage = percentage_CpuUsage

                    Globals.MemoryCpu.AverageCounter += 1
                    If Globals.MemoryCpu.AverageCounter < 1 Then
                        Globals.MemoryCpu.AverageCounter = 1
                    End If

                    Globals.MemoryCpu.AverageMemoryPercentage = Math.Ceiling((((Globals.MemoryCpu.AverageCounter - 1) * Globals.MemoryCpu.AverageMemoryPercentage) + Globals.MemoryCpu.CurrentMemoryPercentage) / Globals.MemoryCpu.AverageCounter)
                    Globals.MemoryCpu.AverageCpuPercentage = Math.Ceiling((((Globals.MemoryCpu.AverageCounter - 1) * Globals.MemoryCpu.AverageCpuPercentage) + Globals.MemoryCpu.CurrentCpuPercentage) / Globals.MemoryCpu.AverageCounter)


                    If Not Globals.LoopAbort Then
                        Threading.Thread.Sleep(1000 * Constants.Settings.MemoryCPULoopDelayInSeconds)
                    End If
                Loop Until Globals.LoopAbort
            Catch ex As Exception
                Logger.Error(Logger.THREAD.MEM_CPU, "Error while reading mem/cpu usage", 1)
                Logger.Error(Logger.THREAD.MEM_CPU, ex.Message, 2)
            End Try


            Globals.MemoryCpu.AverageCounter = -1
            Globals.MemoryCpu.AverageMemoryPercentage = -1
            Globals.MemoryCpu.AverageCpuPercentage = -1


            mActive = False
        End Sub
    End Class
#End Region


#Region "LoggerZipper thread"
    Public Class LoggerZipperThread
        Private Shared mActive As Boolean = False
        Public Shared ReadOnly Property IsActive() As Boolean
            Get
                Return mActive
            End Get
        End Property

        Public Shared Sub Start()
            mThreading_LogZipper = New Threading.Thread(New Threading.ThreadStart(AddressOf _work))
            mThreading_LogZipper.Start()
        End Sub

        Public Shared Sub [Stop]()
            Logger.Message(Logger.THREAD.LOGZIPPER, "Stopping", 0)
            Try
                mThreading_LogZipper.Abort()
            Catch ex As Exception

            End Try
        End Sub

        Private Shared Sub _work()
            mActive = True

            LoggerZipper.ZipLogs()

            mActive = False
        End Sub
    End Class
#End Region


#Region "Heartbeat thread"
    Public Class HeartbeatThread
        Public Class _downtime
            Public Class _duration
                Private mYear As Integer
                Public Property Year() As Integer
                    Get
                        Return mYear
                    End Get
                    Set(ByVal value As Integer)
                        mYear = value
                    End Set
                End Property

                Private mMonth As Integer
                Public Property Month() As Integer
                    Get
                        Return mMonth
                    End Get
                    Set(ByVal value As Integer)
                        mMonth = value
                    End Set
                End Property

                Private mDay As Integer
                Public Property Day() As Integer
                    Get
                        Return mDay
                    End Get
                    Set(ByVal value As Integer)
                        mDay = value
                    End Set
                End Property

                Private mDuration As Long
                Public Property Duration() As Long
                    Get
                        Return mDuration
                    End Get
                    Set(ByVal value As Long)
                        mDuration = value
                    End Set
                End Property
            End Class

            Private mStart As Long
            Public Property Start() As Long
                Get
                    Return mStart
                End Get
                Set(ByVal value As Long)
                    mStart = value
                End Set
            End Property

            Private mEnd As Long
            Public Property [End]() As Long
                Get
                    Return mEnd
                End Get
                Set(ByVal value As Long)
                    mEnd = value
                End Set
            End Property

            Private mError As String
            Public Property [Error]() As String
                Get
                    Return mError
                End Get
                Set(ByVal value As String)
                    mError = value
                End Set
            End Property

            Private mDuration As List(Of _duration)
            Public ReadOnly Property Duration() As List(Of _duration)
                Get
                    Return mDuration
                End Get
            End Property

            Public Sub New()
                mDuration = New List(Of _duration)
                mStart = 0
                mEnd = 0
                mError = ""
            End Sub

            Public Sub Add(iStart As Integer, iEnd As Integer, sError As String)
                mStart = iStart
                mEnd = iEnd
                mError = sError

                Dim tmpDuration As Long = mEnd - mStart
                Dim tmpStartDate As Date = Helpers.TimeDate.FromUnix(mStart)
                Dim tmpStartDateDay As Date = New Date(tmpStartDate.Year, tmpStartDate.Month, tmpStartDate.Day)

                Dim tmpFirstDayDuration As Long = Constants.Misc.SecondsInDay - Math.Abs(DateDiff(DateInterval.Second, tmpStartDate, tmpStartDateDay))

                Dim _duration As _duration
                If tmpDuration > tmpFirstDayDuration Then
                    'Multiple days
                    _duration = New _duration()

                    _duration.Year = tmpStartDateDay.Year
                    _duration.Month = tmpStartDateDay.Month - 1 'Thank you JavaScript
                    _duration.Day = tmpStartDateDay.Day
                    _duration.Duration = tmpFirstDayDuration

                    mDuration.Add(_duration)

                    tmpDuration -= tmpFirstDayDuration
                    Do
                        tmpStartDateDay = tmpStartDateDay.AddDays(1)

                        _duration = New _duration()

                        _duration.Year = tmpStartDateDay.Year
                        _duration.Month = tmpStartDateDay.Month - 1 'Thank you JavaScript
                        _duration.Day = tmpStartDateDay.Day

                        If tmpDuration > Constants.Misc.SecondsInDay Then
                            _duration.Duration = Constants.Misc.SecondsInDay
                            tmpDuration -= Constants.Misc.SecondsInDay
                        Else
                            _duration.Duration = tmpDuration
                            tmpDuration -= tmpDuration
                        End If

                        mDuration.Add(_duration)
                    Loop While tmpDuration > 0
                Else
                    _duration = New _duration()

                    _duration.Year = tmpStartDateDay.Year
                    _duration.Month = tmpStartDateDay.Month - 1 'Thank you JavaScript
                    _duration.Day = tmpStartDateDay.Day
                    _duration.Duration = tmpDuration

                    mDuration.Add(_duration)
                End If
            End Sub
        End Class

        Public Class _memory_cpu_percentages
            Private mCpuCurrent As Integer
            Public Property CpuCurrent() As Integer
                Get
                    Return mCpuCurrent
                End Get
                Set(ByVal value As Integer)
                    mCpuCurrent = value
                End Set
            End Property

            Private mCpuAverage As Integer
            Public Property CpuAverage() As Integer
                Get
                    Return mCpuAverage
                End Get
                Set(ByVal value As Integer)
                    mCpuAverage = value
                End Set
            End Property

            Private mMemoryCurrent As Integer
            Public Property MemoryCurrent() As Integer
                Get
                    Return mMemoryCurrent
                End Get
                Set(ByVal value As Integer)
                    mMemoryCurrent = value
                End Set
            End Property

            Private mMemoryAverage As Integer
            Public Property MemoryAverage() As Integer
                Get
                    Return mMemoryAverage
                End Get
                Set(ByVal value As Integer)
                    mMemoryAverage = value
                End Set
            End Property
        End Class

        Public Class _geolocation
            Private mLatitude As String
            Public Property Latitude() As String
                Get
                    Return mLatitude
                End Get
                Set(ByVal value As String)
                    mLatitude = value
                End Set
            End Property

            Private mLongitude As String
            Public Property Longitude() As String
                Get
                    Return mLongitude
                End Get
                Set(ByVal value As String)
                    mLongitude = value
                End Set
            End Property

            Private mCity As String
            Public Property City() As String
                Get
                    Return mCity
                End Get
                Set(ByVal value As String)
                    mCity = value
                End Set
            End Property

            Private mCountry As String
            Public Property Country() As String
                Get
                    Return mCountry
                End Get
                Set(ByVal value As String)
                    mCountry = value
                End Set
            End Property

            Private mTimezone As String
            Public Property Timezone() As String
                Get
                    Return mTimezone
                End Get
                Set(ByVal value As String)
                    mTimezone = value
                End Set
            End Property
        End Class


        Private Shared mActive As Boolean = False
        Public Shared ReadOnly Property IsActive() As Boolean
            Get
                Return mActive
            End Get
        End Property

        Public Shared Sub Start()
            mThreading_Heartbeat = New Threading.Thread(New Threading.ThreadStart(AddressOf _work))
            mThreading_Heartbeat.Start()
        End Sub

        Public Shared Sub [Stop]()
            Logger.Message(Logger.THREAD.HEARTBEAT, "Stopping", 0)
            Try
                mThreading_Heartbeat.Abort()
            Catch ex As Exception

            End Try
        End Sub

        Private Shared Sub _work()
            Logger.Message(Logger.THREAD.HEARTBEAT, "Started", 0)


            If Settings.Application.[Public].ThreadDelay_Heartbeat > 0 Then
                If Settings.Application.[Public].ThreadDelay_Heartbeat > 1 Then
                    Logger.Message(Logger.THREAD.HEARTBEAT, "Delayed for " & Settings.Application.[Public].ThreadDelay_Heartbeat & " seconds", 0)
                Else
                    Logger.Message(Logger.THREAD.HEARTBEAT, "Delayed for 1 second", 0)
                End If


                Dim iDelayCount As Integer = 0

                Do
                    iDelayCount += 1

                    Threading.Thread.Sleep(1000)
                Loop Until iDelayCount >= Settings.Application.[Public].ThreadDelay_Heartbeat
            Else
                'Delay is zero, so we do a random delay
                Dim r As New Random

                Dim iRandomDelay As Integer = r.Next(10, 180)
                Dim iDelayCount As Integer = 0

                Logger.Message(Logger.THREAD.HEARTBEAT, "Delayed for " & iRandomDelay & " seconds", 0)

                Do
                    iDelayCount += 1

                    Threading.Thread.Sleep(1000)
                Loop Until iDelayCount >= iRandomDelay
            End If


            'reset rename toggle
            Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor_Configuration_Application,
                                                  Constants.RegistryKeys.VALUE__MonitorSettings_RenameHappened,
                                                  False)


            '-------------------------------------------------------------------------------------------------------------------------------------------
            'Main loop
            Do
                mActive = True

                Logger.Message(Logger.THREAD.MAIN, New String("=", 48))


                Logger.Message(Logger.THREAD.HEARTBEAT, "Starting heartbeat")

                Dim sMachineName As String = Environment.MachineName
                Dim lCurrentTime As Long = Helpers.TimeDate.UnixTimestamp
                Dim JSON_Heartbeat As New Communication.JSON()

                JSON_Heartbeat.ServerUrl = Settings.Application.Private.Server.EndPoint.Heartbeat


                Settings.Application.Load_NinjaNodeIde()
                'Settings.Application.Load_MachineRenamed()


                JSON_Heartbeat.Request.Add("machine_name", sMachineName)

                If Globals.MachineWasRenamed Then
                    If Globals.MachineRenameResult Then
                        JSON_Heartbeat.Request.Add("is_renamed", "true")
                    Else
                        JSON_Heartbeat.Request.Add("is_renamed", "false")
                    End If
                End If


                JSON_Heartbeat.Request.Add("machine_identifier", Globals.MachineIdentifier)
                JSON_Heartbeat.Request.Add("monitor_identifier", Globals.MonitorIdentifier)

                JSON_Heartbeat.Request.Add("ninja_nodeid", Settings.Application.Private.NinjaNodeId)
                JSON_Heartbeat.Request.Add("current_user", Metrics.WindowsUser.GetCurrentLoggedOnUser())
                JSON_Heartbeat.Request.Add("sitekiosk_profile", Integrity.FilesFolders.GetSiteKioskProfilePath())
                JSON_Heartbeat.Request.Add("unlinked_sitekiosk_apps", Integrity.OVCC.Applications.GetUnlinkedApplicationsCount())


                'last usage / in use
                Dim oUsage As New Metrics.SiteKiosk.QuickMetrics._Usage
                oUsage = Metrics.SiteKiosk.QuickMetrics.LastUsageTimestamp()
                Globals.MachineIsInUse = oUsage.InUse
                JSON_Heartbeat.Request.Add("in_use", Misc.BooleanToInteger(Globals.MachineIsInUse))
                JSON_Heartbeat.Request.Add("last_usage_timestamp", oUsage.LastUsageTimestamp)

                JSON_Heartbeat.Request.Add("last_screensaver_timestamp", Metrics.SiteKiosk.QuickMetrics.LastScreensaverTimestamp())
                JSON_Heartbeat.Request.Add("os_version", Metrics.OS.Version())
                JSON_Heartbeat.Request.Add("os_language", Metrics.OS.Language.[Long]())
                JSON_Heartbeat.Request.Add("timezone_offset", Metrics.Timezone.GetOffsetString())
                JSON_Heartbeat.Request.Add("uptime_seconds", Metrics.OS.UptimeDowntime.UptimeSeconds())
                JSON_Heartbeat.Request.Add("chipset", Metrics.SystemInformation.Hardware.Chipset())


                'Emergency state
                Dim bIsInEmergencyState As Boolean = Metrics.SiteKiosk.QuickMetrics.IsInEmergencyState()
                If bIsInEmergencyState Then
                    Logger.Warning(Logger.THREAD.HEARTBEAT, "emergency mode detected!")

                    If Not EmergencyModeThread.IsActive Then
                        EmergencyModeThread.Start()
                    Else
                        Logger.Warning(Logger.THREAD.HEARTBEAT, "emergency mode thread is already active!")
                    End If
                Else
                    Settings.Application.Load_EmergencyMode_Values()

                    If Settings.Application.Private.EmergencyModeDetected Then
                        'It appears we fixed it
                        Settings.Application.Private.EmergencyModeFixedCount += 1
                        Settings.Application.Private.EmergencyModeFixedTimestamp = Helpers.TimeDate.UnixTimestamp

                        EmergencyModeLogger.Write("EmergencyModeFixedCount=" & Settings.Application.Private.EmergencyModeFixedCount)
                        EmergencyModeLogger.Write("EmergencyModeFixedTimestamp=" & Settings.Application.Private.EmergencyModeFixedTimestamp)
                    End If

                    Settings.Application.Private.EmergencyModeDetected = False
                    Settings.Application.Private.EmergencyModeFixType = Constants.SiteKiosk.EmergencyModeFixTypes.NONE
                    Settings.Application.Save_EmergencyMode_Values()
                End If


                'software status
                JSON_Heartbeat.Request.Add("in_emergency_state", Misc.BooleanToInteger(bIsInEmergencyState))


                Dim lLastReboot As Long = 0, lLastShutdown As Long = 0
                Metrics.OS.UptimeDowntime.LastShutdownAndReboot(lLastReboot, lLastShutdown)
                JSON_Heartbeat.Request.Add("last_reboot", lLastReboot)
                JSON_Heartbeat.Request.Add("last_shutdown", lLastShutdown)

                JSON_Heartbeat.Request.Add("leftover_userfiles", Integrity.FilesFolders.ProfileCleaning.CountOldFiles())


                Dim oLocalSettings_SettingsReceivedFromNinja As New Configuration.SettingsReceivedFromNinja.LocalSettings
                Configuration.SettingsReceivedFromNinja.Load(oLocalSettings_SettingsReceivedFromNinja)
                JSON_Heartbeat.Request.Add("navision_code", oLocalSettings_SettingsReceivedFromNinja.NavisionCode)


                Dim oMemCpu As New _memory_cpu_percentages With {
                    .CpuCurrent = Globals.MemoryCpu.CurrentCpuPercentage,
                    .CpuAverage = Globals.MemoryCpu.AverageCpuPercentage,
                    .MemoryCurrent = Globals.MemoryCpu.CurrentMemoryPercentage,
                    .MemoryAverage = Globals.MemoryCpu.AverageMemoryPercentage
                }

                JSON_Heartbeat.Request.Add("memory_cpu", Newtonsoft.Json.JsonConvert.SerializeObject(oMemCpu))


                Dim nics As List(Of NetInterfacesInfo.NetWorkInterfacesInfoShort) = NetInterfacesInfo.GetConnectedNics()
                If nics.Count <= 0 Then
                    'No nics found, strange
                    Logger.Warning(Logger.THREAD.NETWORK_INFO, "Could not find any network info", 1)

                    If NetInterfacesInfo.LastError.Equals("") Then
                        Logger.Warning(Logger.THREAD.NETWORK_INFO, "unknown reason", 2)
                    Else
                        Logger.Warning(Logger.THREAD.NETWORK_INFO, NetInterfacesInfo.LastError, 2)
                    End If
                End If

                JSON_Heartbeat.Request.Add("network_details", Newtonsoft.Json.JsonConvert.SerializeObject(nics))


                'screenshot
                Logger.Debug(Logger.THREAD.SCREENSHOTS, "screenshot", 1)
                If Settings.Application.Public.ScreenshotEnabled And Not Globals.SkipScreenshotWhenRunningManually Then
                    If (Helpers.TimeDate.UnixTimestamp - Globals.LastScreenshot) > Settings.Application.Public.ScreenshotDelay Then
                        Logger.Message(Logger.THREAD.SCREENSHOTS, "taking screenshot", 1)
                        Try
                            If Screenshot.Take(Globals.MachineIsInUse) Then
                                Logger.Message(Logger.THREAD.SCREENSHOTS, "ok", 2)
                                Logger.Message(Logger.THREAD.SCREENSHOTS, Screenshot.Request.FileName, 3)

                                JSON_Heartbeat.Request.Add("screenshot", Newtonsoft.Json.JsonConvert.SerializeObject(Screenshot.Request))
                            Else
                                Logger.Warning(Logger.THREAD.SCREENSHOTS, "failed", 2)
                                Logger.Warning(Logger.THREAD.SCREENSHOTS, Screenshot.LastError, 3)
                            End If
                        Catch ex As Exception
                            Logger.Error(Logger.THREAD.SCREENSHOTS, "error", 2)
                            Logger.Error(Logger.THREAD.SCREENSHOTS, ex.Message, 3)
                        End Try

                        Settings.Application.Save_LastScreenshot(lCurrentTime)
                    Else
                        Logger.Debug(Logger.THREAD.SCREENSHOTS, "not time yet", 2)
                    End If
                Else
                    Logger.Debug(Logger.THREAD.SCREENSHOTS, "disabled", 2)
                End If


                'check if there is downtime
                Dim lCurrentHeartbeat As Long = lCurrentTime
                Dim lLastHeartbeatSuccess As Long = Helpers.Registry.GetValue_Long(
                    Constants.RegistryKeys.KEY__Monitor,
                    Constants.RegistryKeys.VALUE__Monitor_LastHeartbeatSuccessTimestamp,
                    Constants.RegistryKeys.DEFAULTVALUE__LastHeartbeatSuccessTimestamp)
                Dim sLastHeartbeatError As String = Helpers.Registry.GetValue_String(
                    Constants.RegistryKeys.KEY__Monitor,
                    Constants.RegistryKeys.VALUE__Monitor_LastHeartbeatError,
                    Constants.RegistryKeys.DEFAULTVALUE__LastHeartbeatError)


                If lLastHeartbeatSuccess <= 0 Then
                    'First heartbeat? Set current heartbeat to now, regardless of success.
                    lLastHeartbeatSuccess = lCurrentHeartbeat

                    Helpers.Registry.SetValue_Long(
                    Constants.RegistryKeys.KEY__Monitor,
                    Constants.RegistryKeys.VALUE__Monitor_LastHeartbeatSuccessTimestamp,
                    lLastHeartbeatSuccess)
                End If

                If (lCurrentHeartbeat - lLastHeartbeatSuccess) > Settings.Application.[Public].DowntimeThresholdSeconds _
                        And (lCurrentHeartbeat - lLastHeartbeatSuccess) > 2 * Settings.Application.[Public].ThreadLoopDelay_Heartbeat _
                    Then
                    Dim oDowntime As New _downtime
                    oDowntime.Add(lLastHeartbeatSuccess, lCurrentHeartbeat, sLastHeartbeatError)

                    JSON_Heartbeat.Request.Add("downtime", Newtonsoft.Json.JsonConvert.SerializeObject(oDowntime))
                End If


                'Geolocation
                If (lCurrentTime - Settings.Application.Private.LastGeolocationSync) > Settings.Application.Public.LastGeolocationSyncDelay Then
                    Logger.Debug(Logger.THREAD.HEARTBEAT, "getting geolocation", 1)

                    Dim JSON_Geolocation As New Communication.GeoLocation()
                    JSON_Geolocation.ServerUrl = Constants.Communication.IpInfoUrl

                    If JSON_Geolocation.Send() Then
                        Dim sTmp As String = JSON_Geolocation.Body.GetValue("loc").ToString
                        sTmp = sTmp.Replace("""", "")
                        Dim aTmp As String() = sTmp.Split(",")

                        Dim oGeolocation As New _geolocation
                        oGeolocation.City = JSON_Geolocation.Body.GetValue("city")
                        oGeolocation.Country = JSON_Geolocation.Body.GetValue("country")
                        oGeolocation.Latitude = aTmp(0)
                        oGeolocation.Longitude = aTmp(1)
                        oGeolocation.Timezone = JSON_Geolocation.Body.GetValue("timezone")

                        JSON_Heartbeat.Request.Add("geolocation", Newtonsoft.Json.JsonConvert.SerializeObject(oGeolocation))
                    Else
                        Logger.Warning(Logger.THREAD.HEARTBEAT, "getting geolocation failed", 1)
                    End If


                    Settings.Application.Private.LastGeolocationSync = lCurrentTime
                    Settings.Application.Save_LastGeolocationSync()
                Else
                    Logger.Debug(Logger.THREAD.HEARTBEAT, "geolocation skipped for now", 1)
                End If


                Logger.Message(Logger.THREAD.HEARTBEAT, "sending", 1)
                Logger.Debug(Logger.THREAD.HEARTBEAT, "sending request", 1)
                Logger.Debug(Logger.THREAD.HEARTBEAT, JSON_Heartbeat.Request.ToString, 2)

                If Not JSON_Heartbeat.Send() Then
                    'Uh oh, heartbeat failed
                    Logger.Error(Logger.THREAD.HEARTBEAT, "Heartbeat FAILED")

                    'Store current heartbeat error
                    Logger.Debug(Logger.THREAD.HEARTBEAT, "Storing current heartbeat error: " & JSON_Heartbeat.LastError, 2)

                    Helpers.Registry.SetValue_String(
                        Constants.RegistryKeys.KEY__Monitor,
                        Constants.RegistryKeys.VALUE__Monitor_LastHeartbeatError,
                        JSON_Heartbeat.LastError)
                Else
                    Logger.Message(Logger.THREAD.HEARTBEAT, "ok", 2)

                    Logger.Debug(Logger.THREAD.HEARTBEAT, "received raw response", 1)
                    Logger.Debug(Logger.THREAD.HEARTBEAT, JSON_Heartbeat.RawResponse.ToString, 2)

                    Logger.Debug(Logger.THREAD.HEARTBEAT, "received body response", 1)
                    Logger.Debug(Logger.THREAD.HEARTBEAT, JSON_Heartbeat.Body.ToString, 2)


                    Dim iCode As Integer = -999
                    Dim sMessage As String = "(no_response)"
                    Dim iMachineId As Integer = 0

                    If JSON_Heartbeat.Body.GetValue("code") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("code").ToString, iCode) Then
                            iCode = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("message") IsNot Nothing Then
                        sMessage = JSON_Heartbeat.Body.GetValue("message").ToString
                    End If

                    If JSON_Heartbeat.Body.GetValue("machine_id") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("machine_id").ToString, Globals.MachineId) Then
                            Globals.MachineId = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("verified") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("verified").ToString, Globals.Verified) Then
                            Globals.Verified = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("request_settings") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("request_settings").ToString, Globals.RequestSettings) Then
                            Globals.RequestSettings = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("last_heartbeat") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("last_heartbeat").ToString, Globals.LastHeartbeat) Then
                            Globals.LastHeartbeat = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("last_appsettings_sync") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("last_appsettings_sync").ToString, Globals.LastApplicationSettingsSync) Then
                            Globals.LastApplicationSettingsSync = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("last_ovccsettings_sync") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("last_ovccsettings_sync").ToString, Globals.LastOVCCSettingsSync) Then
                            Globals.LastOVCCSettingsSync = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("last_ovccthemes_sync") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("last_ovccthemes_sync").ToString, Globals.LastOVCCThemesSync) Then
                            Globals.LastOVCCThemesSync = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("last_metrics_sync") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("last_metrics_sync").ToString, Globals.LastMetricsSync) Then
                            Globals.LastMetricsSync = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("last_inventory_sync") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("last_inventory_sync").ToString, Globals.LastInventorySync) Then
                            Globals.LastInventorySync = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("last_integrity_sync") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("last_integrity_sync").ToString, Globals.LastIntegritySync) Then
                            Globals.LastIntegritySync = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("last_logfile_received") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("last_logfile_received").ToString, Globals.LastLogfileReceived) Then
                            Globals.LastLogfileReceived = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("new_machine_name") IsNot Nothing Then
                        Globals.NewMachineNameFromConsole = JSON_Heartbeat.Body.GetValue("new_machine_name").ToString
                    Else
                        Globals.NewMachineNameFromConsole = ""
                    End If

                    If JSON_Heartbeat.Body.GetValue("property_id") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("property_id").ToString, Settings.Application.Private.PropertyId) Then
                            Settings.Application.Private.PropertyId = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("hidden") IsNot Nothing Then
                        If Not Integer.TryParse(JSON_Heartbeat.Body.GetValue("hidden").ToString, Globals.Hidden) Then
                            Globals.Hidden = -1
                        End If
                    End If

                    If JSON_Heartbeat.Body.GetValue("monitor_last_version") IsNot Nothing Then
                        Try
                            Globals.MonitorLastVersion = New Version(JSON_Heartbeat.Body.GetValue("monitor_last_version").ToString)
                        Catch ex As Exception
                            Globals.MonitorLastVersion = New Version("0.0.0.0")
                        End Try
                    End If

                    If JSON_Heartbeat.Body.GetValue("run_external_exe") IsNot Nothing Then
                        Globals.ExternalExecutable.Name = JSON_Heartbeat.Body.GetValue("run_external_exe").ToString
                    End If


                    Logger.Debug(Logger.THREAD.HEARTBEAT, "response code", 1)
                    Logger.Debug(Logger.THREAD.HEARTBEAT, iCode.ToString, 1)
                    Logger.Debug(Logger.THREAD.HEARTBEAT, "response message", 1)
                    Logger.Debug(Logger.THREAD.HEARTBEAT, sMessage, 2)

                    If iCode <> 0 Then
                        'error
                        Logger.Error(Logger.THREAD.HEARTBEAT, "server responded with an error", 1)
                        Logger.Error(Logger.THREAD.HEARTBEAT, iCode.ToString, 2)

                        'Store current heartbeat error
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "Storing current heartbeat error: " & JSON_Heartbeat.LastError, 2)

                        Helpers.Registry.SetValue_String(
                            Constants.RegistryKeys.KEY__Monitor,
                            Constants.RegistryKeys.VALUE__Monitor_LastHeartbeatError,
                            JSON_Heartbeat.LastError)
                    Else
                        Settings.Application.Save_PropertyId()
                        Settings.Application.Save_LastHeartbeat()

                        Settings.Application.Load_ForceUpdate()

                        Logger.Debug(Logger.THREAD.HEARTBEAT, "heartbeat properties", 1)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "machine id", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.MachineId.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "verified", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.Verified.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "request settings", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.RequestSettings.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "last heartbeat", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastHeartbeat.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "last app settings sync", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastApplicationSettingsSync.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "last ovcc settings sync", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastOVCCSettingsSync.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "last ovcc themes sync", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastOVCCThemesSync.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "last metrics sync", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastMetricsSync.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "last inventory sync", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastInventorySync.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "last integrity sync", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastIntegritySync.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "last logfile received", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastLogfileReceived.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "new machine name", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.NewMachineNameFromConsole.ToString, 3)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "run external exe", 2)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.ExternalExecutable.Name, 3)

                        Logger.Debug(Logger.THREAD.HEARTBEAT, "forced update?", 1)
                        Logger.Debug(Logger.THREAD.HEARTBEAT, Settings.Application.Private.ForceUpdate.ToString, 2)


                        'Determine actions
                        Logger.Debug(Logger.THREAD.HEARTBEAT, "determining actions", 1)
                        Actions.ThreadAction = THREAD_ACTIONS.NOTHING
                        If Globals.LastHeartbeat > 0 Then
                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Is machine verified?", 2)
                            If Globals.Verified <= 0 Then
                                Logger.Debug(Logger.THREAD.HEARTBEAT, "no (" & Globals.Verified.ToString & ")", 3)
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.MACHINE_NOT_VERIFIED_YET
                            Else
                                Logger.Debug(Logger.THREAD.HEARTBEAT, "yes", 3)
                            End If

                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Is console requesting settings?", 2)
                            If Globals.RequestSettings > 0 Or Settings.Application.Private.ForceRequestSettings Then
                                If Globals.RequestSettings <= 0 And Not Settings.Application.Public.Monitor_SyncSettingsAfterUpdate And Settings.Application.Private.ForceRequestSettings Then
                                    Logger.MessageRelative(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "Disabled by Console", 2)
                                    Settings.Application.Save_ForceRequestSettings(True)
                                Else
                                    If Globals.RequestSettings > 0 Then
                                        Logger.Debug(Logger.THREAD.HEARTBEAT, "yes", 3)
                                    Else
                                        Logger.Debug(Logger.THREAD.HEARTBEAT, "no, but by local request", 3)
                                    End If


                                    Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.REQUESTED_SETTINGS

                                    Globals.RequestSettings = 0
                                    Settings.Application.Reset_ForceRequestSettings()
                                End If
                            Else
                                Logger.Debug(Logger.THREAD.HEARTBEAT, "no", 3)
                            End If


                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Globals.MonitorLastVersion.CompareTo(My.Application.Info.Version)", 2)
                            Logger.Debug(Logger.THREAD.HEARTBEAT, """" & Globals.MonitorLastVersion.ToString & """.CompareTo(""" & My.Application.Info.Version.ToString & """)=" & (Globals.MonitorLastVersion.CompareTo(My.Application.Info.Version)).ToString, 3)
                            If Globals.MonitorLastVersion.CompareTo(My.Application.Info.Version) > 0 Then
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.SELF_UPDATE
                            End If

                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Not Globals.ExternalExecutable.Name.Equals("""")", 2)
                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Not """ & Globals.ExternalExecutable.Name & """.Equals("""")", 2)
                            If Not Globals.ExternalExecutable.Name.Equals("") Then
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.RUN_EXTERNAL_APP
                            End If

                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Globals.LastApplicationSettingsSync > (Settings.Application.Private.LastApplicationSettingsSync + Settings.Application.Public.LastApplicationSettingsSyncDelay)", 2)
                            Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastApplicationSettingsSync.ToString & "> (" & Settings.Application.Private.LastApplicationSettingsSync.ToString & "+" & Settings.Application.Public.LastApplicationSettingsSyncDelay.ToString & ")", 3)
                            If Globals.LastApplicationSettingsSync > (Settings.Application.Private.LastApplicationSettingsSync + Settings.Application.Public.LastApplicationSettingsSyncDelay) Then
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.APPLICATION_SETTINGS
                            End If

                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Globals.LastOVCCSettingsSync > (Settings.Application.Private.LastOVCCSettingsSync + Settings.Application.Public.LastOVCCSettingsSyncDelay) Or Globals.FirstRun", 2)
                            Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastOVCCSettingsSync.ToString & "> (" & Settings.Application.Private.LastOVCCSettingsSync.ToString & "+" & Settings.Application.Public.LastOVCCSettingsSyncDelay.ToString & ") Or " & Globals.FirstRun.ToString, 3)
                            If (Globals.LastOVCCSettingsSync > (Settings.Application.Private.LastOVCCSettingsSync + Settings.Application.Public.LastOVCCSettingsSyncDelay)) Then
                                Globals.FirstRun = False
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_SETTINGS
                            End If

                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Globals.LastOVCCThemesSync > (Settings.Application.Private.LastOVCCThemesSync + Settings.Application.Public.LastOVCCThemesSyncDelay)", 2)
                            Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastOVCCThemesSync.ToString & "> (" & Settings.Application.Private.LastOVCCThemesSync.ToString & "+" & Settings.Application.Public.LastOVCCThemesSyncDelay.ToString & ")", 3)
                            If Globals.LastOVCCThemesSync > (Settings.Application.Private.LastOVCCThemesSync + Settings.Application.Public.LastOVCCThemesSyncDelay) Then
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEMES_SYNC
                            End If

                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Globals.LastHeartbeat > (Settings.Application.Private.LastIntegritySync + Settings.Application.Public.LastIntegritySyncDelay)", 2)
                            Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastHeartbeat.ToString & "> (" & Settings.Application.Private.LastIntegritySync.ToString & "+" & Settings.Application.Public.LastIntegritySyncDelay.ToString & ")", 3)
                            If Globals.LastHeartbeat > (Settings.Application.Private.LastIntegritySync + Settings.Application.Public.LastIntegritySyncDelay) Then
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.INTEGRITY
                            End If

                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Globals.LastHeartbeat > (Settings.Application.Private.LastInventorySync + Settings.Application.Public.LastInventorySyncDelay)", 2)
                            Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastHeartbeat.ToString & "> (" & Settings.Application.Private.LastInventorySync.ToString & "+" & Settings.Application.Public.LastInventorySyncDelay.ToString & ")", 3)
                            If Globals.LastHeartbeat > (Settings.Application.Private.LastInventorySync + Settings.Application.Public.LastInventorySyncDelay) Then
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.INVENTORY
                            End If

                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Globals.LastHeartbeat > (Settings.Application.Private.LastMetricsSync + Settings.Application.Public.LastMetricsSyncDelay)", 2)
                            Logger.Debug(Logger.THREAD.HEARTBEAT, Globals.LastHeartbeat.ToString & "> (" & Settings.Application.Private.LastMetricsSync.ToString & "+" & Settings.Application.Public.LastMetricsSyncDelay.ToString & ")", 3)
                            If Globals.LastHeartbeat > (Settings.Application.Private.LastMetricsSync + Settings.Application.Public.LastMetricsSyncDelay) Then
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.METRICS
                            End If

                            'extra stuff for external app
                            If Globals.ExternalExecutable.IsBusy Then
                                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.RUN_EXTERNAL_APP
                            End If
                        End If


                        'Store current heartbeat
                        If Not Actions.ThreadAction And THREAD_ACTIONS.SELF_UPDATE Or Not Globals.MonitorUpdateEnabled Then
                            lCurrentHeartbeat = lCurrentTime
                            Logger.Debug(Logger.THREAD.HEARTBEAT, "Storing current heartbeat: " & lCurrentHeartbeat.ToString, 2)

                            Helpers.Registry.SetValue_Long(
                                Constants.RegistryKeys.KEY__Monitor,
                                Constants.RegistryKeys.VALUE__Monitor_LastHeartbeatSuccessTimestamp,
                                lCurrentHeartbeat)

                            Helpers.Registry.SetValue_String(
                                Constants.RegistryKeys.KEY__Monitor,
                                Constants.RegistryKeys.VALUE__Monitor_LastHeartbeatError,
                                "")
                        End If


                        'Handle remote renaming
                        If Not Globals.NewMachineNameFromConsole.Equals("") Then
                            If Not Globals.NewMachineNameFromConsole.Equals(sMachineName) Then
                                If Not Globals.MachineWasRenamed Then
                                    Dim sRenameError As String = ""

                                    Globals.MachineRenameResult = Commands.ComputerName.Rename(Globals.NewMachineNameFromConsole, sRenameError)
                                    Globals.MachineWasRenamed = True
                                End If
                            End If
                        End If


                        'Handle disabling
                        If Globals.Hidden > 1 Then
                            If Not Commands.DisableMachine.IsThemeDisabled Then
                                Commands.DisableMachine.ForceDisabledTheme()
                            End If
                        End If


                        'Action time
                        Logger.Message(Logger.THREAD.HEARTBEAT, "actions from heartbeat", 2)
                        Logger.Message(Logger.THREAD.HEARTBEAT, Actions.ToString, 3)

                        HandleActions()

                        Logger.Message(Logger.THREAD.HEARTBEAT, "All done", 0)
                    End If
                End If


                mActive = False

                If Not Globals.LoopAbort Then
                    Threading.Thread.Sleep(1000 * Settings.Application.[Public].ThreadLoopDelay_Heartbeat)
                End If
            Loop Until Globals.LoopAbort
        End Sub
    End Class
#End Region


#Region "EmergencyMode thread"
    Public Class EmergencyModeThread
        Private Shared mActive As Boolean = False
        Public Shared ReadOnly Property IsActive() As Boolean
            Get
                Return mActive
            End Get
        End Property

        Public Shared Sub Start()
            Start(False)
        End Sub

        Public Shared Sub Start(WaitForComplete As Boolean)
            Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Starting", 0)

            mThreading_EmergencyMode = New Threading.Thread(New Threading.ThreadStart(AddressOf _work))
            mThreading_EmergencyMode.Start()

            If WaitForComplete Then
                mThreading_EmergencyMode.Join(123456789)
            End If
        End Sub

        Public Shared Sub [Stop]()
            Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Stopping", 0)
            Try
                mThreading_EmergencyMode.Abort()
            Catch ex As Exception

            End Try
        End Sub

        Private Shared Sub _work()
            mActive = True

            Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Started", 0)


            Try
                Dim iLoopProtection As Integer = 0


                Settings.Application.Load_EmergencyMode_Values()

                Settings.Application.Private.EmergencyModeDetected = True
                Settings.Application.Private.EmergencyModeDetectedCount += 1
                Settings.Application.Private.EmergencyModeDetectedTimestamp = Helpers.TimeDate.UnixTimestamp

                Select Case Settings.Application.Private.EmergencyModeFixType
                    Case Constants.SiteKiosk.EmergencyModeFixTypes.NONE
                        EmergencyModeLogger.Write("EmergencyModeDetectedCount=" & Settings.Application.Private.EmergencyModeDetectedCount)
                        EmergencyModeLogger.Write("EmergencyModeDetectedTimestamp=" & Settings.Application.Private.EmergencyModeDetectedTimestamp)

                        Settings.Application.Private.EmergencyModeFixedTimestamp = Helpers.TimeDate.UnixTimestamp
                        Settings.Application.Private.EmergencyModeFixType = Constants.SiteKiosk.EmergencyModeFixTypes.RESTART_SITEKIOSK


                        Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Attempting fix: sitekiosk restart", 1)

                        Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Restarting sitekiosk", 2)
                        If Helpers.SiteKiosk.Application.Restart() > 0 Then
                            Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Killed sitekiosk", 3)
                        End If

                        Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Wait", 1)
                        Threading.Thread.Sleep(Constants.TimeoutsAndDelays.EmergencyModeFixBreather)


                        iLoopProtection = 0
                        Do
                            iLoopProtection += 1
                            Threading.Thread.Sleep(1000)
                        Loop While Not Helpers.SiteKiosk.Application.IsRunning() And iLoopProtection <= 60


                        If Not Helpers.SiteKiosk.Application.IsRunning() And iLoopProtection > 60 Then
                            Logger.Error(Logger.THREAD.EMERGENCY_MODE, "Sitekiosk didn't restart", 3)
                            'uh oh, sitekiosk didn't restart, let's force a reboot
                            Globals.LoopAbort = True
                            Commands.RebootShutdown.ForcedReboot()

                            Settings.Application.Private.EmergencyModeFixType = Constants.SiteKiosk.EmergencyModeFixTypes.REBOOT_MACHINE
                            EmergencyModeLogger.Write("EmergencyModeFixType=" & Settings.Application.Private.EmergencyModeFixType)

                            Throw New Exceptions.ForcedRebootException()
                        Else
                            Settings.Application.Private.EmergencyModeFixType = Constants.SiteKiosk.EmergencyModeFixTypes.RESTART_SITEKIOSK
                            EmergencyModeLogger.Write("EmergencyModeFixType=" & Settings.Application.Private.EmergencyModeFixType)
                        End If
                    Case Constants.SiteKiosk.EmergencyModeFixTypes.RESTART_SITEKIOSK
                        Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Attempting fix: machine reboot", 1)
                        Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Restarting machine", 2)

                        Settings.Application.Private.EmergencyModeFixType = Constants.SiteKiosk.EmergencyModeFixTypes.REBOOT_MACHINE
                        EmergencyModeLogger.Write("EmergencyModeFixType=" & Settings.Application.Private.EmergencyModeFixType)

                        Throw New Exceptions.ForcedRebootException()
                    Case Constants.SiteKiosk.EmergencyModeFixTypes.REBOOT_MACHINE
                        Logger.Error(Logger.THREAD.EMERGENCY_MODE, "Needs human interaction", 1)

                        Settings.Application.Private.EmergencyModeFixType = Constants.SiteKiosk.EmergencyModeFixTypes.UNFIXABLE
                        EmergencyModeLogger.Write("EmergencyModeFixType=" & Settings.Application.Private.EmergencyModeFixType)
                    Case Constants.SiteKiosk.EmergencyModeFixTypes.UNFIXABLE
                        Logger.Warning(Logger.THREAD.EMERGENCY_MODE, "Waiting for human touch", 1)
                    Case Else
                        Logger.Warning(Logger.THREAD.EMERGENCY_MODE, "(unknown fix type)", 1)
                End Select
            Catch r_ex As Exceptions.ForcedRebootException
                Logger.Warning(Logger.THREAD.EMERGENCY_MODE, "Forced reboot!", 2)

                If (Settings.Application.Private.EmergencyModeLastRebootTimestamp + Constants.TimeoutsAndDelays.EmergencyModeRebootDelay) < Helpers.TimeDate.UnixTimestamp Then
                    Settings.Application.Private.EmergencyModeLastRebootTimestamp = Helpers.TimeDate.UnixTimestamp
                    Settings.Application.Save_EmergencyMode_Values()

                    EmergencyModeLogger.Write("EmergencyModeLastRebootTimestamp=" & Settings.Application.Private.EmergencyModeLastRebootTimestamp)

                    Logger.Warning(Logger.THREAD.EMERGENCY_MODE, "See you on the other side", 3)
                    Globals.LoopAbort = True
                    Commands.RebootShutdown.ForcedReboot()
                Else
                    Logger.Warning(Logger.THREAD.EMERGENCY_MODE, "skipped, we already had a recent reboot", 3)
                End If
            Catch ex As Exception
                Logger.Error(Logger.THREAD.EMERGENCY_MODE, "EXCEPTION", 1)
                Logger.Error(Logger.THREAD.EMERGENCY_MODE, ex.GetType.ToString, 2)
                Logger.Error(Logger.THREAD.EMERGENCY_MODE, ex.Message, 3)
            Finally
                Settings.Application.Save_EmergencyMode_Values()
            End Try


            Logger.Message(Logger.THREAD.EMERGENCY_MODE, "Done", 0)

            mActive = False
        End Sub
    End Class
#End Region
#End Region


#Region "actions"
#Region "FirstRunAfterUpdate"
    Public Shared Function ACTION_FirstRunAfterUpdate() As Boolean
        Logger.Message(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "First run after an update!", 0)
        Helpers.Registry.SetValue_Boolean(Constants.RegistryKeys.KEY__Monitor, Constants.RegistryKeys.VALUE__Monitor_IsFirstRunAfterUpdate, False)


        'Trigger post update cleanup
        Logger.Message(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "Trigger post update cleanup", 1)
        ThreadAction = ThreadAction Or THREAD_ACTIONS.POST_UPDATE_CLEANUP


        'Force inventory for next run
        Logger.Message(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "Forcing an inventory update", 1)
        Settings.Application.Save_LastInventorySync(0)
        ThreadAction = ThreadAction Or THREAD_ACTIONS.INVENTORY


        'Force application settings for next run
        Logger.Message(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "Forcing application settings update", 1)
        Settings.Application.Save_LastApplicationSettingsSync(0)
        ThreadAction = ThreadAction Or THREAD_ACTIONS.APPLICATION_SETTINGS


        'Force ovcc settings for next run
        Logger.Message(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "Forcing ovcc settings update", 1)
        Settings.Application.Save_LastOVCCSettingsSync(0)
        ThreadAction = ThreadAction Or THREAD_ACTIONS.OVCC_SETTINGS


        'Force settings sending
        Logger.Message(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "Forcing a settings sync", 1)
        Settings.Application.Save_ForceRequestSettings(True)


        'Make sure that the fullscreen homepage setting is updated
        Logger.Message(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "Add fullscreen setting for new themes folder", 1)
        Dim _url As String = ""

        Configuration.SkCfg.XML.Lock()
        Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
        Configuration.SkCfg.XML.Load()
        Configuration.SkCfg.XML.AddValue(Constants.SkCfg.BrowserbarUrls, Constants.SkCfg.BrowserbarUrls__value, "file://%SiteKioskPath%\Skins\Public\Themes\*")
        Configuration.SkCfg.XML.Save()
        Configuration.SkCfg.XML.Unlock()


        Return True
    End Function
#End Region

#Region "PostUpdateCleanup"
    Public Shared Function ACTION_PostUpdateCleanup() As Boolean
        Logger.Message(Logger.THREAD.POST_UPDATE_CLEANUP, "Cleaning registry", 1)
        PostUpdateCleanup.Registry.Clean()

        Logger.Message(Logger.THREAD.POST_UPDATE_CLEANUP, "Updating registry", 1)
        PostUpdateCleanup.Registry.Update()


        'reload settings
        Logger.Message(Logger.THREAD.POST_UPDATE_CLEANUP, "Reloading settings", 1)
        Settings.Application.Load_Privates()
        Settings.Application.Load_Publics()


        Return True
    End Function
#End Region

#Region "ApplicationSettings"
    Public Shared Function ACTION_ApplicationSettings() As Boolean
        'We need to download settings
        Dim JSON_ApplicationSettings As New Communication.JSON

        JSON_ApplicationSettings.ServerUrl = Settings.Application.Private.Server.EndPoint.ApplicationSetting

        JSON_ApplicationSettings.Request.Add("machine_id", Globals.MachineId.ToString)
        JSON_ApplicationSettings.Request.Add("machine_identifier", Globals.MachineIdentifier)

        Logger.Debug(Logger.THREAD.APP_SETTINGS, "sending request", 1)
        Logger.Debug(Logger.THREAD.APP_SETTINGS, JSON_ApplicationSettings.Request.ToString, 2)

        JSON_ApplicationSettings.Send()

        Logger.Debug(Logger.THREAD.APP_SETTINGS, "received response", 1)
        Logger.Debug(Logger.THREAD.APP_SETTINGS, JSON_ApplicationSettings.Body.ToString, 2)

        Logger.Message(Logger.THREAD.APP_SETTINGS, "syncing settings", 1)
        Logger.Debug(Logger.THREAD.APP_SETTINGS, JSON_ApplicationSettings.Body.GetValue("app_settings").ToString())

        Dim iErrorCode As Integer = 0

        If Not JSON_ApplicationSettings.Body.GetValue("app_settings") Is Nothing Then
            For Each child As Newtonsoft.Json.Linq.JProperty In JSON_ApplicationSettings.Body.GetValue("app_settings").Children
                Logger.Message(Logger.THREAD.APP_SETTINGS, "received setting", 1)
                Logger.Message(Logger.THREAD.APP_SETTINGS, "name", 2)
                Logger.Message(Logger.THREAD.APP_SETTINGS, child.Name, 3)
                Logger.Message(Logger.THREAD.APP_SETTINGS, "value", 2)
                Logger.Message(Logger.THREAD.APP_SETTINGS, child.Value.ToString, 3)

                If Settings.Application.Save(child.Name, child.Value) Then
                    Logger.Message(Logger.THREAD.APP_SETTINGS, "saved", 2)
                Else
                    Logger.Warning(Logger.THREAD.APP_SETTINGS, "not saved", 2)
                End If
            Next
        End If

        'Globals.LastApplicationSettingsSync = Globals.LastHeartbeat
        Settings.Application.Save_LastApplicationSettingsSync(Globals.LastApplicationSettingsSync)

        Settings.Application.Load_Publics()


        Logger.Message(Logger.THREAD.APP_SETTINGS, "ok", 1)

        Return True
    End Function
#End Region


#Region "OVCCSettings"
    Public Shared Function ACTION_OVCCSettings() As Boolean
        Dim JSON_OVCCSettings As New Communication.JSON

        JSON_OVCCSettings.ServerUrl = Settings.Application.Private.Server.EndPoint.OVCCSettings

        JSON_OVCCSettings.Request.Add("machine_id", Globals.MachineId.ToString)
        JSON_OVCCSettings.Request.Add("machine_identifier", Globals.MachineIdentifier)

        Logger.Debug(Logger.THREAD.OVCC_SETTINGS, "sending request", 1)
        Logger.Debug(Logger.THREAD.OVCC_SETTINGS, JSON_OVCCSettings.Request.ToString, 2)

        JSON_OVCCSettings.Send()

        Logger.Debug(Logger.THREAD.OVCC_SETTINGS, "received response", 1)
        Logger.Debug(Logger.THREAD.OVCC_SETTINGS, JSON_OVCCSettings.Body.ToString, 2)

        Logger.Message(Logger.THREAD.OVCC_SETTINGS, "syncing settings", 1)
        Logger.Debug(Logger.THREAD.OVCC_SETTINGS, JSON_OVCCSettings.Body.GetValue("ovcc_settings").ToString())

        Dim iErrorCode As Integer = 0

        If Not JSON_OVCCSettings.Body.GetValue("ovcc_settings") Is Nothing Then
            For Each child As Newtonsoft.Json.Linq.JProperty In JSON_OVCCSettings.Body.GetValue("ovcc_settings").Children
                Logger.Message(Logger.THREAD.OVCC_SETTINGS, "received setting", 1)
                Logger.Message(Logger.THREAD.OVCC_SETTINGS, "name", 2)
                Logger.Message(Logger.THREAD.OVCC_SETTINGS, child.Name, 3)
                Logger.Message(Logger.THREAD.OVCC_SETTINGS, "value", 2)
                Logger.Message(Logger.THREAD.OVCC_SETTINGS, child.Value.ToString, 3)

                If Settings.OVCC.Save(child.Name, child.Value) Then
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "saved", 2)
                Else
                    iErrorCode += 1
                    Logger.Warning(Logger.THREAD.OVCC_SETTINGS, "not saved", 2)
                End If
            Next
        End If


        '-----------------------------------------------------------------------------------------------------------------------------------
        'Save settings sync timestamp
        Dim iSaveResultCode As Configuration.SaveResultCode

        'Globals.LastOVCCSettingsSync = Globals.LastHeartbeat
        Settings.Application.Save_LastOVCCSettingsSync(Globals.LastOVCCSettingsSync)

        Settings.OVCC.Load()



        '-----------------------------------------------------------------------------------------------------------------------------------
        'Deploying settings
        Logger.Message(Logger.THREAD.OVCC_SETTINGS, "deploying settings", 1)

        Dim bForceSKRestart As Boolean = False

        If Settings.Application.Public.ConsoleControlsSettings Then
            '-----------------------------------------------------------------------------------------------------------------------------------
            'central script
            Logger.Message(Logger.THREAD.OVCC_SETTINGS, "central script", 2)

            iSaveResultCode = Configuration.iBahnScript.Save()
            Select Case iSaveResultCode
                Case Configuration.SaveResultCode.OK
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "ok", 3)
                    bForceSKRestart = True
                Case Configuration.SaveResultCode.CONFIG_SCRIPT_SKIPPED
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "no change", 3)
                Case Configuration.SaveResultCode.CONFIG_SCRIPT_ERROR
                    Logger.Error(Logger.THREAD.OVCC_SETTINGS, "error while saving settings", 3)
                    If Configuration.iBahnScript.HasError Then
                        Logger.Error(Logger.THREAD.OVCC_SETTINGS, Configuration.iBahnScript.LastError, 4)
                    Else
                        Logger.Error(Logger.THREAD.OVCC_SETTINGS, "unknown error", 4)
                    End If
            End Select


            '-----------------------------------------------------------------------------------------------------------------------------------
            'Save OVCC password
            Logger.Message(Logger.THREAD.OVCC_SETTINGS, "ovcc password", 2)

            iSaveResultCode = Configuration.OVCCPassword.SavePassword()
            Logger.Debug(Logger.THREAD.OVCC_SETTINGS, "iSaveResultCode=" & iSaveResultCode.ToString, 3)
            Select Case iSaveResultCode
                Case Configuration.SaveResultCode.OK
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "ok", 3)
                    bForceSKRestart = True
                Case Configuration.SaveResultCode.PASSWORD_FILE_SKIPPED
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "no change", 3)
                Case Configuration.SaveResultCode.PASSWORD_FILE_ERROR
                    Logger.Error(Logger.THREAD.OVCC_SETTINGS, "error while saving password", 3)
                    If Configuration.OVCCPassword.HasError Then
                        Logger.Error(Logger.THREAD.OVCC_SETTINGS, Configuration.OVCCPassword.LastError, 4)
                    Else
                        Logger.Error(Logger.THREAD.OVCC_SETTINGS, "unknown error", 4)
                    End If
            End Select


            '-----------------------------------------------------------------------------------------------------------------------------------
            'Save OVCC payment dialog
            Logger.Message(Logger.THREAD.OVCC_SETTINGS, "payment dialog", 2)

            iSaveResultCode = Configuration.OVCCPaymentDialog.Save()
            Logger.Debug(Logger.THREAD.OVCC_SETTINGS, "iSaveResultCode=" & iSaveResultCode.ToString, 3)
            Select Case iSaveResultCode
                Case Configuration.SaveResultCode.OK
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "ok", 3)
                    bForceSKRestart = True
                Case Configuration.SaveResultCode.PAYMENT_DIALOG_SKIPPED
                    Logger.Message(Logger.THREAD.OVCC_SETTINGS, "no change", 3)
                Case Configuration.SaveResultCode.PAYMENT_DIALOG_ERROR
                    Logger.Error(Logger.THREAD.OVCC_SETTINGS, "error while saving password", 3)
                    If Configuration.OVCCPaymentDialog.HasError Then
                        Logger.Error(Logger.THREAD.OVCC_SETTINGS, Configuration.OVCCPaymentDialog.LastError, 4)
                    Else
                        Logger.Error(Logger.THREAD.OVCC_SETTINGS, "unknown error", 4)
                    End If
            End Select

            Logger.Message(Logger.THREAD.OVCC_SETTINGS, "ok", 1)
        Else
            Logger.Message(Logger.THREAD.OVCC_SETTINGS, "disabled", 1)
        End If


        'Force a theme sync
        Logger.Message(Logger.THREAD.OVCC_SETTINGS, "Forcing a theme sync", 1)
        Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEMES_SYNC


        'Force sk restart
        Logger.Message(Logger.THREAD.OVCC_SETTINGS, "Forcing a SK restart", 1)
        Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.RESTART_SITEKIOSK


        Return True
    End Function

    Public Shared Function ACTION_SendLocalOVCCSettings() As Boolean
        Return ACTION_SendLocalOVCCSettings(False)
    End Function

    Public Shared Function ACTION_SendLocalOVCCSettings(SendEvenWhenVerified As Boolean) As Boolean
        Dim JSON_SendLocalOVCCSettings As New Communication.JSON

        JSON_SendLocalOVCCSettings.ServerUrl = Settings.Application.Private.Server.EndPoint.SendLocalOVCCSettings

        JSON_SendLocalOVCCSettings.Request.Add("machine_id", Globals.MachineId.ToString)
        JSON_SendLocalOVCCSettings.Request.Add("machine_identifier", Globals.MachineIdentifier)


        Dim oLocalSettings_iBahnScript As Configuration.iBahnScript.LocalSettings = New Configuration.iBahnScript.LocalSettings
        Dim oLocalSettings_Password As Configuration.OVCCPassword.LocalSettings = New Configuration.OVCCPassword.LocalSettings
        Dim sPaymentDialog As String = Configuration.OVCCPaymentDialog.Load
        Dim oLocalSettings_SettingsReceivedFromNinja As Configuration.SettingsReceivedFromNinja.LocalSettings = New Configuration.SettingsReceivedFromNinja.LocalSettings

        Configuration.iBahnScript.Load(oLocalSettings_iBahnScript)
        Configuration.OVCCPassword.Load(oLocalSettings_Password)
        Configuration.SettingsReceivedFromNinja.Load(oLocalSettings_SettingsReceivedFromNinja)


        If SendEvenWhenVerified Then
            JSON_SendLocalOVCCSettings.Request.Add("forced", "true")
        End If
        JSON_SendLocalOVCCSettings.Request.Add("HotelAddress", oLocalSettings_iBahnScript.HotelAddress)
        JSON_SendLocalOVCCSettings.Request.Add("HotelInformationUrl", oLocalSettings_iBahnScript.HotelInformationUrl)
        JSON_SendLocalOVCCSettings.Request.Add("InternetUrl", oLocalSettings_iBahnScript.InternetUrl)
        JSON_SendLocalOVCCSettings.Request.Add("MapUrl", oLocalSettings_iBahnScript.MapUrl)
        JSON_SendLocalOVCCSettings.Request.Add("ThemeIndex", Inventory.Themes.Index(oLocalSettings_iBahnScript.ThemeIndex))
        JSON_SendLocalOVCCSettings.Request.Add("WeatherUrl", oLocalSettings_iBahnScript.WeatherUrl)
        JSON_SendLocalOVCCSettings.Request.Add("ThemePaymentDialog", sPaymentDialog)
        JSON_SendLocalOVCCSettings.Request.Add("ThemePasswordEnabled", oLocalSettings_Password.PasswordEnabled)
        JSON_SendLocalOVCCSettings.Request.Add("ThemePassword", oLocalSettings_Password.Password)

        JSON_SendLocalOVCCSettings.Request.Add("OVCCCode", oLocalSettings_SettingsReceivedFromNinja.OVCCCode)
        JSON_SendLocalOVCCSettings.Request.Add("NavisionCode", oLocalSettings_SettingsReceivedFromNinja.NavisionCode)


        Logger.Debug(Logger.THREAD.SEND_LOCAL_OVCCSETTINGS, "sending request", 1)
        Logger.Debug(Logger.THREAD.SEND_LOCAL_OVCCSETTINGS, JSON_SendLocalOVCCSettings.Request.ToString, 2)

        JSON_SendLocalOVCCSettings.Send()

        Logger.Debug(Logger.THREAD.SEND_LOCAL_OVCCSETTINGS, "received response", 1)
        Logger.Debug(Logger.THREAD.SEND_LOCAL_OVCCSETTINGS, JSON_SendLocalOVCCSettings.Body.ToString, 2)


        Logger.Message(Logger.THREAD.SEND_LOCAL_OVCCSETTINGS, "ok", 1)

        Return True
    End Function
#End Region

#Region "OVCC Themes & Screensavers"
    Public Shared Function ACTION_ThemeSync() As Boolean
        Dim lReasons As New List(Of String)


        Dim JSON_OVCCThemes As New Communication.JSON

        JSON_OVCCThemes.ServerUrl = Settings.Application.Private.Server.EndPoint.OVCCThemes

        JSON_OVCCThemes.Request.Add("machine_id", Globals.MachineId.ToString)
        JSON_OVCCThemes.Request.Add("machine_identifier", Globals.MachineIdentifier)
        JSON_OVCCThemes.Request.Add("os", "win")

        Logger.Debug(Logger.THREAD.THEMES_SYNC, "sending request", 1)
        Logger.Debug(Logger.THREAD.THEMES_SYNC, JSON_OVCCThemes.Request.ToString, 2)

        JSON_OVCCThemes.Send()

        Logger.Debug(Logger.THREAD.THEMES_SYNC, "received response", 1)
        Logger.Debug(Logger.THREAD.THEMES_SYNC, JSON_OVCCThemes.Body.ToString, 2)


        If Not JSON_OVCCThemes.Body.GetValue("ovcc_themes") Is Nothing Then
            For Each child As Newtonsoft.Json.Linq.JProperty In JSON_OVCCThemes.Body.GetValue("ovcc_themes").Children
                Dim iThemeIndex As Integer = 0
                Dim sThemeName As String = ""
                Dim iThemeVersion As Integer = 0

                If Integer.TryParse(child.Name, iThemeIndex) Then
                    'oh, ok
                End If

                For Each o_theme As Newtonsoft.Json.Linq.JObject In child.ToList
                    sThemeName = o_theme.GetValue("ThemeName").ToString
                    If Integer.TryParse(o_theme.GetValue("ThemeVersion").ToString, iThemeVersion) Then
                        'oh, ok
                    End If
                Next

                Settings.Themes.Add(iThemeIndex, sThemeName, iThemeVersion)
            Next

            Settings.Themes.Save()
        End If


        '-----------------------------------------------------------------------------------------------------------------------------------
        'Check if we need to download and install theme
        Logger.Message(Logger.THREAD.THEMES_SYNC, "Checking theme", 2)

        Logger.Debug(Logger.THREAD.THEMES_SYNC, "do we need to download", 3)
        Dim iInstalledThemeVersion As Integer = OVCCTheme.InstalledVersion
        Dim iLatestThemeVersion As Integer

        If iInstalledThemeVersion < 0 Then
            If iInstalledThemeVersion = -1 Then
                Logger.Debug(Logger.THREAD.THEMES_SYNC, "theme version not found", 3)

                lReasons.Add("theme version not found")
            Else
                Logger.Debug(Logger.THREAD.THEMES_SYNC, "theme version invalid", 3)

                lReasons.Add("theme version invalid")
            End If

            Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEME_DOWNLOAD
            Logger.Debug(Logger.THREAD.THEMES_SYNC, "yes: no theme version found", 4)
        Else
            Logger.Debug(Logger.THREAD.THEMES_SYNC, "no: theme version found", 4)
        End If


        iLatestThemeVersion = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Themes & "\" & Settings.OVCC.Public.ThemeIndex, Constants.RegistryKeys.VALUE__Monitor_Configuration_Themes_Version, -1)

        If iInstalledThemeVersion < iLatestThemeVersion Then
            Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEME_DOWNLOAD
            Logger.Debug(Logger.THREAD.THEMES_SYNC, "yes: theme version found (" & iInstalledThemeVersion & ") < theme version available (" & iLatestThemeVersion & ")", 4)
            lReasons.Add("current version (" & iInstalledThemeVersion & ") < available version (" & iLatestThemeVersion & ")")
        Else
            Logger.Debug(Logger.THREAD.THEMES_SYNC, "no: theme version found (" & iInstalledThemeVersion & ") >= theme version available (" & iLatestThemeVersion & ")", 4)
        End If


        If Not OVCCTheme.DirectoryExists Then
            Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEME_DOWNLOAD

            Logger.Debug(Logger.THREAD.THEMES_SYNC, "yes: theme not installed", 4)

            lReasons.Add("theme not installed")
        Else
            Logger.Debug(Logger.THREAD.THEMES_SYNC, "no: theme installed", 4)
        End If


        If Not Actions.ThreadAction And THREAD_ACTIONS.OVCC_THEME_DOWNLOAD Then
            Logger.Debug(Logger.THREAD.THEMES_SYNC, "checking if path is new style", 4)


            Dim sThemeIndexFile As String = ""

            Configuration.SkCfg.XML.Lock()
            Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
            Configuration.SkCfg.XML.Load()
            Configuration.SkCfg.XML.GetValue(Constants.SkCfg.ThemeUrl, sThemeIndexFile)
            Configuration.SkCfg.XML.Unlock()

            Logger.Debug(Logger.THREAD.THEMES_SYNC, "current path", 5)
            Logger.Debug(Logger.THREAD.THEMES_SYNC, sThemeIndexFile, 6)
            sThemeIndexFile = Helpers.SiteKiosk.TranslateSiteKioskPathToNormalPath(sThemeIndexFile)
            Logger.Debug(Logger.THREAD.THEMES_SYNC, sThemeIndexFile, 6)

            Logger.Debug(Logger.THREAD.THEMES_SYNC, "path should be like ", 5)
            Logger.Debug(Logger.THREAD.THEMES_SYNC, OVCCTheme.Directory, 6)

            If Not sThemeIndexFile.Contains(OVCCTheme.Directory) Then
                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEME_DOWNLOAD

                Logger.Debug(Logger.THREAD.THEMES_SYNC, "yes: path is invalid", 5)

                lReasons.Add("theme path is invalid")
            Else
                Logger.Debug(Logger.THREAD.THEMES_SYNC, "no: path is valid", 5)
            End If
        End If


        If Not Actions.ThreadAction And THREAD_ACTIONS.OVCC_THEME_DOWNLOAD Then
            Dim sHashTheme_Current As String = DirectoryHash.Calculate(OVCCTheme.Directory)
            Settings.OVCC.Load_HashTheme()

            If Not sHashTheme_Current.Equals(Settings.OVCC.Private.HashTheme) Then
                Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEME_DOWNLOAD

                Logger.Debug(Logger.THREAD.THEMES_SYNC, "yes: theme hash not matching", 4)
                Logger.Debug(Logger.THREAD.THEMES_SYNC, "sHashTheme_Current", 5)
                Logger.Debug(Logger.THREAD.THEMES_SYNC, sHashTheme_Current, 6)
                Logger.Debug(Logger.THREAD.THEMES_SYNC, "Settings.OVCC.Private.HashTheme", 5)
                Logger.Debug(Logger.THREAD.THEMES_SYNC, Settings.OVCC.Private.HashTheme, 6)

                lReasons.Add("theme hash mismatch")
            Else
                Logger.Debug(Logger.THREAD.THEMES_SYNC, "no: hashes match", 4)
            End If
        Else
            Logger.Debug(Logger.THREAD.THEMES_SYNC, "no: hash skipped", 4)
        End If


        If Actions.ThreadAction And THREAD_ACTIONS.OVCC_THEME_DOWNLOAD Then
            Logger.Message(Logger.THREAD.THEMES_SYNC, "Triggering theme download", 3)

            If lReasons.Count > 1 Then
                Logger.Message(Logger.THREAD.THEMES_SYNC, "reasons", 4)
            Else
                Logger.Message(Logger.THREAD.THEMES_SYNC, "reason", 4)
            End If

            Logger.Message(Logger.THREAD.THEMES_SYNC, String.Join(", ", lReasons.ToArray))
        Else
            Logger.Message(Logger.THREAD.THEMES_SYNC, "ok", 2)
        End If


        Settings.Application.Save_LastOVCCThemesSync(Globals.LastOVCCThemesSync)

        Return True
    End Function

    Public Shared Function ACTION_ThemeDownload() As Boolean
        Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "Downloading theme", 1)


        Try
            Dim iLatestThemeVersion As Integer

            iLatestThemeVersion = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Configuration_Themes & "\" & Settings.OVCC.Public.ThemeIndex, Constants.RegistryKeys.VALUE__Monitor_Configuration_Themes_Version, -1)


            Dim sbDownloadUrl As New Text.StringBuilder

            sbDownloadUrl.Append(Settings.Application.[Private].Download.Themes)
            sbDownloadUrl.Append(Settings.OVCC.Public.ThemeIndex.ToString & ".zip")


            Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, "Creating temp directory", 2)
            Dim sTempString As String = Guid.NewGuid().ToString()
            Dim sTempBaseDirectory As String = IO.Path.GetTempPath()
            Dim sTempDirectory As String = IO.Path.Combine(sTempBaseDirectory, sTempString & "_" & Settings.OVCC.Public.ThemeIndex.ToString)
            Dim sDownloadedFile As String = IO.Path.Combine(sTempBaseDirectory, sTempString & "_" & Constants.FileFolders.Theme.DefaultName.Replace("%%INDEX%%", Settings.OVCC.Public.ThemeIndex.ToString))

            Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, sTempDirectory, 3)

            IO.Directory.CreateDirectory(sTempDirectory)

            Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, "ok", 4)

            If Communication.Download.File(sbDownloadUrl.ToString, sDownloadedFile) Then
                'Unzip package
                Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "Unpacking", 2)
                Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, "source", 3)
                Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, sDownloadedFile, 4)
                Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, "destination", 3)
                Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, sTempDirectory, 4)

                IO.Compression.ZipFile.ExtractToDirectory(sDownloadedFile, sTempDirectory)


                If IO.Directory.Exists(OVCCTheme.Directory) Then
                    IO.Directory.Delete(OVCCTheme.Directory, True)
                End If
                IO.Directory.CreateDirectory(OVCCTheme.Directory)


                Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "Copying", 2)
                CopyDirectoryContents.CopyDirectoryContents(sTempDirectory, OVCCTheme.Directory)


                Settings.OVCC.[Private].HashTheme = DirectoryHash.Calculate(OVCCTheme.Directory)
                Settings.OVCC.Save_HashTheme()


                OVCCTheme.InstalledVersion = iLatestThemeVersion


                Dim sThemeIndexFile As String = Helpers.SiteKiosk.TranslateNormalPathToSiteKioskPath(IO.Path.Combine(OVCCTheme.Directory, "index.html"))
                Dim sThemeLogoutFile As String = Helpers.SiteKiosk.TranslateNormalPathToSiteKioskPath(IO.Path.Combine(OVCCTheme.Directory, "logout.html"))
                Dim sScreensaverIndexFile As String = Helpers.SiteKiosk.TranslateNormalPathToSiteKioskPath(IO.Path.Combine(OVCCTheme.Directory, "screensaver\index.html"))

                Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "Updating SiteKiosk", 2)

                If Settings.Application.Public.ConsoleControlsThemes Then
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "theme", 3)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, sThemeIndexFile, 4)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "logout", 3)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, sThemeLogoutFile, 4)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "screensaver", 3)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, sScreensaverIndexFile, 4)

                    If Not Configuration.SkCfg.XML.WaitForLock Then
                        Throw New Exception("XML Lock timeout, already locked by '" & Configuration.SkCfg.XML.IsLockedBy & "'")
                    End If

                    Configuration.SkCfg.XML.Lock()
                    Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
                    Configuration.SkCfg.XML.Load()
                    Configuration.SkCfg.XML.SetValue(Constants.SkCfg.ThemeUrl, sThemeIndexFile)
                    Configuration.SkCfg.XML.SetValue(Constants.SkCfg.LogoutUrl, sThemeLogoutFile)
                    Configuration.SkCfg.XML.SetValue(Constants.SkCfg.ScreensaverUrl, sScreensaverIndexFile)
                    Configuration.SkCfg.XML.Save()
                Else
                    Logger.Warning(Logger.THREAD.THEME_DOWNLOAD, "disabled in Console", 3)

                    Logger.Warning(Logger.THREAD.THEME_DOWNLOAD, "I would have done this:", 4)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "set theme", 5)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "homepage", 6)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, sThemeIndexFile, 7)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "logout", 6)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, sThemeLogoutFile, 7)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "screensaver", 6)
                    Logger.Message(Logger.THREAD.THEME_DOWNLOAD, sScreensaverIndexFile, 7)
                End If
            Else
                Logger.Error(Logger.THREAD.THEME_DOWNLOAD, "Download failed!", 2)

                Return False
            End If


            If IO.Directory.Exists(sTempDirectory) Then
                Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, "deleting temp folder", 2)
                Logger.Debug(Logger.THREAD.THEME_DOWNLOAD, sTempDirectory, 3)

                Try
                    IO.Directory.Delete(sTempDirectory, True)
                Catch ex As Exception

                End Try
            End If

            Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "ok", 1)


            'Force an inventory
            Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "Forcing an inventory update", 1)
            Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.INVENTORY


            'Force sk restart
            Logger.Message(Logger.THREAD.THEME_DOWNLOAD, "Forcing a SK restart", 1)
            Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.RESTART_SITEKIOSK
        Catch ex As Exception
            Logger.Error(Logger.THREAD.THEME_DOWNLOAD, "SERIOUS ERROR!", 1)
            Logger.Error(Logger.THREAD.THEME_DOWNLOAD, ex.Message, 2)
        End Try


        Return True
    End Function
#End Region

#Region "Integrity"
    Public Shared Function ACTION_Integrity() As Boolean
        Try
            If Settings.Application.Public.IntegrityEnabled Then
                Dim JSON_Integrity As New Communication.JSON

                JSON_Integrity.ServerUrl = Settings.Application.Private.Server.EndPoint.Integrity

                JSON_Integrity.Request.Add("machine_id", Globals.MachineId.ToString)
                JSON_Integrity.Request.Add("machine_identifier", Globals.MachineIdentifier)


                If Settings.Application.Public.IntegrityEnableWatchdogIfDisabled Then
                    Logger.Message(Logger.THREAD.INTEGRITY, "Checking Watchdog status", 1)

                    Dim oWatchdog As New Integrity.Request

                    Dim bDisabledUntilOVCCStart As Boolean = Helpers.Registry.GetValue_Boolean(Constants.RegistryKeys.KEY__Watchdog, Constants.RegistryKeys.VALUE__Watchdog__DisabledUntilOVCCStart, False)
                    If Not bDisabledUntilOVCCStart Then
                        Logger.Message(Logger.THREAD.INTEGRITY, "set to autostart?", 2)
                        If Integrity.Services.Watchdog.IsServiceStartedAutomatically() Then
                            oWatchdog.WatchdogIsEnabled = True
                            Logger.Message(Logger.THREAD.INTEGRITY, "yes", 3)

                            If Integrity.Services.Watchdog.IsServiceStopped() Then
                                Logger.Message(Logger.THREAD.INTEGRITY, "starting", 2)

                                If Integrity.Services.Watchdog.StartService() Then
                                    oWatchdog.WatchdogIsStarted = True

                                    Logger.Message(Logger.THREAD.INTEGRITY, "ok", 3)
                                Else
                                    oWatchdog.WatchdogEnabledErrorMessage = "Failed to start"

                                    Logger.Error(Logger.THREAD.INTEGRITY, "FAILED", 3)
                                End If
                            End If
                        Else
                            Logger.Message(Logger.THREAD.INTEGRITY, "no", 3)

                            Logger.Message(Logger.THREAD.INTEGRITY, "enabling", 2)
                            If Integrity.Services.Watchdog.SetAutoStart() Then
                                oWatchdog.WatchdogIsEnabled = True

                                Logger.Message(Logger.THREAD.INTEGRITY, "ok", 3)

                                If Integrity.Services.Watchdog.IsServiceStopped() Then
                                    Logger.Message(Logger.THREAD.INTEGRITY, "starting", 2)

                                    If Integrity.Services.Watchdog.StartService() Then
                                        oWatchdog.WatchdogIsStarted = True

                                        Logger.Message(Logger.THREAD.INTEGRITY, "ok", 3)
                                    Else
                                        oWatchdog.WatchdogEnabledErrorMessage = "Failed to start"

                                        Logger.Error(Logger.THREAD.INTEGRITY, "FAILED", 3)
                                    End If
                                End If
                            Else
                                oWatchdog.WatchdogEnabledErrorMessage = "Failed to enable"

                                Logger.Error(Logger.THREAD.INTEGRITY, "FAILED", 3)
                            End If
                        End If
                    Else
                        Logger.Warning(Logger.THREAD.INTEGRITY, "Watchdog has not been activated yet by OVCC AutoStart", 2)

                        oWatchdog.WatchdogIsEnabled = False
                        oWatchdog.WatchdogIsStarted = False
                        oWatchdog.WatchdogEnabledErrorMessage = "Not activated"
                    End If


                    JSON_Integrity.Request.Add("watchdog", Newtonsoft.Json.JsonConvert.SerializeObject(oWatchdog))
                End If


                If Integrity.Services.WatchdogWatchdog.DoesServiceExist() Then
                    Integrity.Services.WatchdogWatchdog.DisableService()
                End If


                'theme integrity check
                Logger.Message(Logger.THREAD.INTEGRITY, "Checking theme integrity", 1)
                Dim bThemeIntegrityFailed As Boolean = False

                Dim sHashTheme_Current As String = DirectoryHash.Calculate(OVCCTheme.Directory)
                Settings.OVCC.Load_HashTheme()

                Logger.Message(Logger.THREAD.INTEGRITY, "Check theme hash", 2)
                If Not sHashTheme_Current.Equals(Settings.OVCC.Private.HashTheme) Then
                    Logger.Warning(Logger.THREAD.INTEGRITY, "theme compromised!", 3)

                    bThemeIntegrityFailed = True

                    Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEMES_SYNC
                    Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEME_DOWNLOAD
                Else
                    Logger.Message(Logger.THREAD.INTEGRITY, "ok", 3)
                End If


                If Not bThemeIntegrityFailed Then
                    Logger.Message(Logger.THREAD.INTEGRITY, "Check theme hash", 2)
                    Dim iThemeIndex_Current As Integer = Inventory.Themes.Index
                    Dim iThemeIndex_Console As Integer = Settings.OVCC.Public.ThemeIndex

                    If iThemeIndex_Current <> iThemeIndex_Console Then
                        Logger.Warning(Logger.THREAD.INTEGRITY, "theme compromised!", 3)

                        If Settings.Application.Public.ConsoleControlsThemes Then
                            Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEMES_SYNC
                            Actions.ThreadAction = Actions.ThreadAction Or THREAD_ACTIONS.OVCC_THEME_DOWNLOAD
                        Else
                            Logger.Warning(Logger.THREAD.INTEGRITY, "ignored, due to Console setting 'ConsoleControlsThemes'", 4)
                        End If
                    Else
                        Logger.Message(Logger.THREAD.INTEGRITY, "ok", 3)
                    End If
                End If


                '====================
                Logger.Debug(Logger.THREAD.INTEGRITY, "sending request", 1)
                Logger.Debug(Logger.THREAD.INTEGRITY, JSON_Integrity.Request.ToString, 2)

                JSON_Integrity.Send()

                Logger.Debug(Logger.THREAD.INTEGRITY, "received response", 1)
                Logger.Debug(Logger.THREAD.INTEGRITY, JSON_Integrity.Body.ToString, 2)


                Globals.LastIntegritySync = Globals.LastHeartbeat
                Settings.Application.Private.LastIntegritySync = Globals.LastIntegritySync
                Settings.Application.Save_LastIntegritySync()
            End If


            Logger.Message(Logger.THREAD.INTEGRITY, "ok", 1)

            Return True
        Catch ex As Exception
            Logger.Error(Logger.THREAD.INTEGRITY, "FAILED", 1)
            Logger.Error(Logger.THREAD.INTEGRITY, ex.Message, 2)

            Return False
        End Try
    End Function
#End Region


#Region "Inventory"
    Public Shared Function ACTION_Inventory() As Boolean
        Dim iErrorCode As Integer = 0


        Dim JSON_Inventory As New Communication.JSON

        JSON_Inventory.ServerUrl = Settings.Application.Private.Server.EndPoint.Inventory

        JSON_Inventory.Request.Add("machine_id", Globals.MachineId.ToString)
        JSON_Inventory.Request.Add("machine_identifier", Globals.MachineIdentifier)

        JSON_Inventory.Request.Add("theme_reported", Inventory.Themes.Index)


        Dim oFileVersions As New Dictionary(Of String, String)

        oFileVersions.Add("FileVersion_Monitor".ToLower, My.Application.Info.Version.ToString)
        oFileVersions.Add("FileRunning_Monitor".ToLower, "1")


        Dim aFields() As Reflection.FieldInfo = GetType(Settings.Application.[Public]).GetFields
        For Each aField As Reflection.FieldInfo In aFields
            If aField.Name.StartsWith("FilePath_") Then
                Dim sFileToCheck As String = aField.GetValue(Nothing).ToString
                Dim sFileVersionName As String = aField.Name.Replace("FilePath_", "FileVersion_").ToLower
                Dim sFileRunningName As String = aField.Name.Replace("FilePath_", "FileRunning_").ToLower

                sFileToCheck = sFileToCheck.Replace("%%PROGRAMFILES32%%", Helpers.FilesAndFolders.GetProgramFilesFolder)

                Try
                    If IO.File.Exists(sFileToCheck) Then
                        Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(sFileToCheck)

                        oFileVersions.Add(sFileVersionName, myFileVersionInfo.FileVersion)

                        If Helpers.Processes.IsProcessRunning(IO.Path.GetFileName(sFileToCheck)) Then
                            oFileVersions.Add(sFileRunningName, "1")
                        Else
                            oFileVersions.Add(sFileRunningName, "0")
                        End If
                    Else
                        oFileVersions.Add(sFileVersionName, "")
                        oFileVersions.Add(sFileRunningName, "0")
                    End If
                Catch ex As Exception
                    oFileVersions.Add(sFileVersionName, "")
                    oFileVersions.Add(sFileRunningName, "0")
                End Try
            End If
        Next

        For Each kv As KeyValuePair(Of String, String) In oFileVersions
            JSON_Inventory.Request.Add(kv.Key, kv.Value)
        Next


        'policy score
        JSON_Inventory.Request.Add("policy_score", Inventory.Policies.SiteKioskUser.PolicyScore().ToString)


        Logger.Debug(Logger.THREAD.INVENTORY, "sending request", 1)
        Logger.Debug(Logger.THREAD.INVENTORY, JSON_Inventory.Request.ToString, 2)

        JSON_Inventory.Send()

        Logger.Debug(Logger.THREAD.INVENTORY, "received response", 1)
        Logger.Debug(Logger.THREAD.INVENTORY, JSON_Inventory.Body.ToString, 2)


        Globals.LastInventorySync = Globals.LastHeartbeat
        Settings.Application.Private.LastInventorySync = Globals.LastInventorySync
        Settings.Application.Save_LastInventorySync()


        Logger.Message(Logger.THREAD.INVENTORY, "ok", 1)

        Return True
    End Function
#End Region


#Region "Metrics"
    Public Shared Function ACTION_Metrics() As Boolean
        Dim iErrorCode As Integer = 0


        Logger.Message(Logger.THREAD.METRICS, "Retrieving metrics", 1)

        Dim dNow As Date = Now
        Dim lDiff As Long = DateDiff(DateInterval.Day, Helpers.TimeDate.FromUnix(Globals.LastLogfileReceived), dNow.Date)

        If lDiff > Constants.Metrics.MaxLogfileHistory Then
            lDiff = Constants.Metrics.MaxLogfileHistory
        End If

        Logger.Debug(Logger.THREAD.METRICS, "Going " & lDiff & " days in the past", 2)


        Dim oMetrics As New List(Of Metrics.Data)
        Dim dCurrent As Date
        Dim oData As Metrics.Data
        Dim bData As Boolean

        Dim JSON_Metrics As New Communication.JSON

        JSON_Metrics.ServerUrl = Settings.Application.Private.Server.EndPoint.Metrics

        JSON_Metrics.Request.Add("machine_id", Globals.MachineId.ToString)
        JSON_Metrics.Request.Add("machine_identifier", Globals.MachineIdentifier)

        If lDiff > 0 Then
            For dIndex As Long = lDiff To 1 Step -1
                dCurrent = DateAdd(DateInterval.Day, -dIndex, dNow)

                Logger.Message(Logger.THREAD.METRICS, dCurrent.ToString("yyyy-MM-dd"), 2)


                oData = New Metrics.Data(dCurrent)

                Logger.Message(Logger.THREAD.METRICS, "sk log", 3)
                bData = Metrics.SiteKiosk.LogFile.Parse(oData)
                If Not bData Then
                    Logger.Warning(Logger.THREAD.METRICS, "Failed:  " & oData.ReturnMessage, 4)
                Else
                    Logger.Message(Logger.THREAD.METRICS, "ok", 4)
                End If


                'Count unexpected shutdowns
                Logger.Message(Logger.THREAD.METRICS, "event log", 3)
                bData = Metrics.ShutdownAndRestarts.Count(oData)
                If Not bData Then
                    Logger.Warning(Logger.THREAD.METRICS, "Failed: " & oData.ReturnMessage, 4)
                Else
                    Logger.Message(Logger.THREAD.METRICS, "ok", 4)
                End If


                oMetrics.Add(oData)
            Next
        End If


        JSON_Metrics.Request.Add("metrics", Newtonsoft.Json.JsonConvert.SerializeObject(oMetrics))


        Logger.Debug(Logger.THREAD.METRICS, "sending request", 1)
        Logger.Debug(Logger.THREAD.METRICS, JSON_Metrics.Request.ToString, 2)

        JSON_Metrics.Send()

        Logger.Debug(Logger.THREAD.METRICS, "received response", 1)
        Logger.Debug(Logger.THREAD.METRICS, JSON_Metrics.Body.ToString, 2)


        Globals.LastMetricsSync = Globals.LastHeartbeat
        Settings.Application.Private.LastMetricsSync = Globals.LastMetricsSync
        Settings.Application.Save_LastMetricsSync()


        Logger.Message(Logger.THREAD.METRICS, "ok", 1)

        Return True
    End Function
#End Region


#Region "Update"
    Public Shared Function ACTION_SelfUpdate() As Boolean
        Logger.Message(Logger.THREAD.SELF_UPDATE, "Current version", 1)
        Logger.Message(Logger.THREAD.SELF_UPDATE, My.Application.Info.Version.ToString, 2)

        If Not Globals.MonitorUpdateEnabled Then
            Logger.Warning(Logger.THREAD.SELF_UPDATE, "Update canceled by internal setting", 1)
            Return False
        End If

        If Not Settings.Application.Public.Monitor_UpdateEnabled Then
            Logger.Warning(Logger.THREAD.SELF_UPDATE, "Update canceled by console setting", 1)
            Return False
        End If


        Settings.Application.Reset_ForceUpdate()


        Dim JSON_Update As New Communication.JSON

        JSON_Update.ServerUrl = Settings.Application.Private.Server.EndPoint.SelfUpdate

        JSON_Update.Request.Add("machine_id", Globals.MachineId.ToString)
        JSON_Update.Request.Add("machine_identifier", Globals.MachineIdentifier)

        Logger.Debug(Logger.THREAD.SELF_UPDATE, "sending request", 1)
        Logger.Debug(Logger.THREAD.SELF_UPDATE, JSON_Update.Request.ToString, 2)

        JSON_Update.Send()

        Logger.Debug(Logger.THREAD.SELF_UPDATE, "received response", 1)
        Logger.Debug(Logger.THREAD.SELF_UPDATE, JSON_Update.Body.ToString, 2)


        Dim sMessage As String = "(no_response)"
        Dim sMonitorLastVersion As String = "0.0.0.0"
        Dim bMonitorUpdateEnabled As Boolean = False

        If Not JSON_Update.Body.GetValue("message") Is Nothing Then
            sMessage = JSON_Update.Body.GetValue("message").ToString
        End If

        If Not JSON_Update.Body.GetValue("monitor_last_version") Is Nothing Then
            sMonitorLastVersion = JSON_Update.Body.GetValue("monitor_last_version").ToString
        End If

        If Not JSON_Update.Body.GetValue("monitor_updateenabled") Is Nothing Then
            bMonitorUpdateEnabled = Helpers.Types.String2Boolean(JSON_Update.Body.GetValue("monitor_updateenabled").ToString)
        End If


        Try
            Logger.Message(Logger.THREAD.SELF_UPDATE, "Latest available version", 1)
            Logger.Message(Logger.THREAD.SELF_UPDATE, sMonitorLastVersion, 2)
            Logger.Message(Logger.THREAD.SELF_UPDATE, "Update Enabled", 1)
            Logger.Message(Logger.THREAD.SELF_UPDATE, bMonitorUpdateEnabled.ToString, 2)

            Dim vMonitorLastVersion As Version = New Version(sMonitorLastVersion)
            Dim result As Integer = vMonitorLastVersion.CompareTo(My.Application.Info.Version)
            If result > 0 Then
                If bMonitorUpdateEnabled Then
                    'We need a newer version
                    Dim sbDownloadUrl As New Text.StringBuilder

                    sbDownloadUrl.Append(Settings.Application.[Private].Download.SelfUpdate)
                    sbDownloadUrl.Append("OVCCMonitor.")
                    sbDownloadUrl.Append(sMonitorLastVersion)
                    sbDownloadUrl.Append(".zip")


                    Logger.Message(Logger.THREAD.SELF_UPDATE, "Downloading", 1)
                    Logger.Message(Logger.THREAD.SELF_UPDATE, sbDownloadUrl.ToString, 2)

                    'Create temp folder
                    Logger.Debug(Logger.THREAD.SELF_UPDATE, "Creating temp directory", 1)
                    Dim sTempString As String = Guid.NewGuid().ToString()
                    Dim sTempBaseDirectory As String = IO.Path.GetTempPath()
                    Dim sTempDirectory As String = IO.Path.Combine(sTempBaseDirectory, sTempString & "_" & sMonitorLastVersion)
                    Dim sDownloadedFile As String = IO.Path.Combine(sTempBaseDirectory, sTempString & "_" & Constants.FileFolders.Update.DefaultName.Replace("%%VERSIONNUMBER%%", sMonitorLastVersion))

                    Logger.Debug(Logger.THREAD.SELF_UPDATE, sTempDirectory, 2)

                    IO.Directory.CreateDirectory(sTempDirectory)

                    Logger.Debug(Logger.THREAD.SELF_UPDATE, "ok", 3)

                    If Communication.Download.File(sbDownloadUrl.ToString, sDownloadedFile) Then
                        'Unzip package
                        Logger.Message(Logger.THREAD.SELF_UPDATE, "Unpacking", 1)
                        Logger.Debug(Logger.THREAD.SELF_UPDATE, "source", 2)
                        Logger.Debug(Logger.THREAD.SELF_UPDATE, sDownloadedFile, 3)
                        Logger.Debug(Logger.THREAD.SELF_UPDATE, "destination", 2)
                        Logger.Debug(Logger.THREAD.SELF_UPDATE, sTempDirectory, 3)


                        IO.Compression.ZipFile.ExtractToDirectory(sDownloadedFile, sTempDirectory)


                        'Build update script
                        Logger.Message(Logger.THREAD.SELF_UPDATE, "Building update script", 1)
                        Dim sUpdateScriptContents As String = My.Resources.update
                        Dim sUpdateScriptPath As String = IO.Path.Combine(sTempBaseDirectory, sTempString & "_" & "update." & sMonitorLastVersion & ".bat")

                        sUpdateScriptContents = sUpdateScriptContents.Replace("__SERVICENAME__", Constants.Service.Name)
                        sUpdateScriptContents = sUpdateScriptContents.Replace("__PROCESSNAME__", Process.GetCurrentProcess().ProcessName & "*")
                        sUpdateScriptContents = sUpdateScriptContents.Replace("__SERVICEEXECUTABLE__", System.Reflection.Assembly.GetExecutingAssembly().Location)
                        sUpdateScriptContents = sUpdateScriptContents.Replace("__SOURCEDIRECTORY__", sTempDirectory)
                        sUpdateScriptContents = sUpdateScriptContents.Replace("__DESTINATIONDIRECTORY__", AppDomain.CurrentDomain.BaseDirectory)
                        If Settings.Application.[Private].Debugging Then
                            sUpdateScriptContents = sUpdateScriptContents.Replace("__CLEANUPSTRING_FOLDER__", "")
                            sUpdateScriptContents = sUpdateScriptContents.Replace("__CLEANUPSTRING_ZIPFILE__", "")
                            sUpdateScriptContents = sUpdateScriptContents.Replace("__CLEANUPSTRING_MYSELF__", "")
                        Else
                            sUpdateScriptContents = sUpdateScriptContents.Replace("__CLEANUPSTRING_FOLDER__", Constants.BatchFile.DeleteFolder.Replace("__CLEANUP_FOLDER__", sTempDirectory))
                            sUpdateScriptContents = sUpdateScriptContents.Replace("__CLEANUPSTRING_ZIPFILE__", Constants.BatchFile.DeleteFolder.Replace("__CLEANUP_ZIP__", sDownloadedFile))
                            sUpdateScriptContents = sUpdateScriptContents.Replace("__CLEANUPSTRING_MYSELF__", Constants.BatchFile.DeleteMyself)
                        End If


                        Dim objWriter As New IO.StreamWriter(sUpdateScriptPath, False)
                        objWriter.WriteLine(sUpdateScriptContents)
                        objWriter.Close()

                        If Not IO.File.Exists(sUpdateScriptPath) Then
                            Throw New IO.IOException("failed to create update script '" & sUpdateScriptPath & "'")
                        End If

                        Logger.Debug(Logger.THREAD.SELF_UPDATE, "ok", 2)


                        Logger.Debug(Logger.THREAD.FIRST_RUN_AFTER_UPDATE, "Remove VALUE__Monitor_IsFirstRunAfterUpdate", 1)
                        Helpers.Registry.RemoveValue(Constants.RegistryKeys.KEY__Monitor, Constants.RegistryKeys.VALUE__Monitor_IsFirstRunAfterUpdate)


                        'Run update script
                        Logger.Message(Logger.THREAD.SELF_UPDATE, "Running update script", 1)
                        Logger.Message(Logger.THREAD.SELF_UPDATE, "see you on the other side", 2)
                        Helpers.Processes.StartProcess(sUpdateScriptPath, "", False)

                        Return True
                    Else
                        Logger.Error(Logger.THREAD.SELF_UPDATE, "Download failed!", 2)

                        Return False
                    End If


                    'If IO.Directory.Exists(sTempDirectory) Then
                    '    Logger.Debug(Logger.THREAD.SELF_UPDATE, "deleting temp folder", 2)
                    '    Logger.Debug(Logger.THREAD.SELF_UPDATE, sTempDirectory, 3)

                    '    Try
                    '        IO.Directory.Delete(sTempDirectory)
                    '    Catch ex As Exception

                    '    End Try
                    'End If
                Else
                    Return False
                End If
            ElseIf result < 0 Then
                'We have a newer version already
                Logger.Warning(Logger.THREAD.SELF_UPDATE, "Local version is newer!", 1)

                Return False
            Else
                'We have the latest version
                Logger.Message(Logger.THREAD.SELF_UPDATE, "We have the latest version", 1)

                Return False
            End If
        Catch ex As Exception
            Logger.Error(Logger.THREAD.SELF_UPDATE, "Error:", 3)
            Logger.Error(Logger.THREAD.SELF_UPDATE, ex.Message, 4)

            Return False
        End Try
    End Function
#End Region


#Region "Restart Sitekiosk"
    Public Shared Function ACTION_RestartSitekiosk() As Boolean
        Logger.MessageRelative(Logger.THREAD.SITEKIOSK, "Restarting sitekiosk", 1)
        If Helpers.SiteKiosk.Application.Restart() > 0 Then
            Logger.MessageRelative(Logger.THREAD.SITEKIOSK, "Killed sitekiosk", 2)
        End If

        Return True
    End Function
#End Region
#End Region


#Region "Run external app"
    Public Shared Function ACTION_RunExternalApp() As Boolean
        Dim sErrorMessage As String = ""


        Try
            Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, "Running external app")
            Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, Globals.ExternalExecutable.Name, 1)

            If Globals.ExternalExecutable.IsBusy Then
                Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, "IsBusy      =" & Globals.ExternalExecutable.IsBusy.ToString, 2)
                Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, "IsDownloaded=" & Globals.ExternalExecutable.IsDownloaded.ToString, 2)
                Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, "IsRunning   =" & Globals.ExternalExecutable.IsRunning.ToString, 2)
                Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, "IsExecuted  =" & Globals.ExternalExecutable.IsExecuted.ToString, 2)
                Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, "IsDone      =" & Globals.ExternalExecutable.IsDone.ToString, 2)

                If Globals.ExternalExecutable.IsDone Then
                    'Let's send an update next heartbeat
                    Globals.ExternalExecutable.IsReadyToReport = True
                End If
            Else
                Globals.ExternalExecutable.IsBusy = True


                If Not Globals.ExternalExecutable.IsDownloaded Then
                    Dim sbDownloadUrl As New Text.StringBuilder

                    sbDownloadUrl.Append(Settings.Application.[Private].Download.ExternalApps)
                    sbDownloadUrl.Append(Globals.ExternalExecutable.Name)

                    Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, "Downloading", 1)
                    Logger.Message(Logger.THREAD.RUN_EXTERNAL_EXE, sbDownloadUrl.ToString, 2)


                    'Create temp folder
                    Logger.Debug(Logger.THREAD.RUN_EXTERNAL_EXE, "Creating temp directory", 1)
                    Dim sTempString As String = Guid.NewGuid().ToString()
                    Dim sTempBaseDirectory As String = IO.Path.GetTempPath()
                    Dim sTempDirectory As String = IO.Path.Combine(sTempBaseDirectory, sTempString)
                    Dim sDownloadedFile As String = IO.Path.Combine(sTempDirectory, Globals.ExternalExecutable.Name)


                    Logger.Debug(Logger.THREAD.RUN_EXTERNAL_EXE, sTempDirectory, 2)

                    IO.Directory.CreateDirectory(sTempDirectory)

                    Logger.Debug(Logger.THREAD.RUN_EXTERNAL_EXE, "ok", 3)


                    If Communication.Download.File(sbDownloadUrl.ToString, sDownloadedFile, sErrorMessage) Then
                        Globals.ExternalExecutable.IsDownloaded = True
                        Globals.ExternalExecutable.DownloadedFile = sDownloadedFile
                    Else
                        Globals.ExternalExecutable.State = "Download failed"
                        Globals.ExternalExecutable.IsDone = True

                        Return False
                    End If
                End If


                If Not Globals.ExternalExecutable.IsRunning Then
                    Commands.ExternalApp.Run(Globals.ExternalExecutable.DownloadedFile, Globals.ExternalExecutable.Params)
                End If
            End If
        Catch ex As Exception
            Globals.ExternalExecutable.State = ex.Message
            Globals.ExternalExecutable.IsDone = True

            Logger.Error(Logger.THREAD.RUN_EXTERNAL_EXE, ex.Message, 1)

            Return False
        End Try


        Return True
    End Function
#End Region
End Class
