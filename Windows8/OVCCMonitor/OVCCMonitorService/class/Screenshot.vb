﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports murrayju.ProcessExtensions

Public Class Screenshot
    Public Class RequestObject
        Public FileName As String
        Public Timestamp As Long
        Public Contents As String
        Public Score As Integer
    End Class

    Public Shared Request As New RequestObject

    Public Shared LastError As String

    Public Shared Function Take(bInUse As Boolean) As Boolean
        Dim now As Date = Date.Now
        Dim __app As String = IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, Constants.Paths.File.ScreenshotTaker)
        Dim __params As String = ""
        Dim __current_logged_in_user As String = ""
        Dim __current_logged_in_user_tmp_dir As String = ""

        LastError = ""

        __current_logged_in_user = Metrics.WindowsUser.GetCurrentLoggedOnUser()
        __current_logged_in_user_tmp_dir = Helpers.WindowsUser.GetProfileTempDirectory(__current_logged_in_user)


        If IO.Directory.Exists(__current_logged_in_user_tmp_dir) Or True Then
            Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "user tmp dir", 1)
            Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, __current_logged_in_user_tmp_dir, 2)

            Request.Timestamp = Helpers.TimeDate.ToUnix(now)
            Request.FileName = IO.Path.Combine(__current_logged_in_user_tmp_dir, Date.Now.ToString("yyyyMMdd_HHmmss") & ".jpg")
            Request.Contents = ""
            Request.Score = -1

            Try
                __params = "--file:""" & Request.FileName & """ --max-width:" & Settings.Application.Public.ScreenshotMaxWidth.ToString
                If bInUse Then
                    __params &= " --privacy-mode"
                End If


                Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "starting screenshotter", 1)
                Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "'" & __app & "' " & __params, 2)

                If ProcessExtensions.StartProcessAsCurrentUser(__app, __params) Then
                    Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "started screenshotter", 1)
                    Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "waiting", 2)

                    Dim iTimeout As Integer = 0
                    Do While iTimeout < 10 And (Not IO.File.Exists(Request.FileName) Or IsFileOpen(Request.FileName))
                        Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "tick tock", 3)
                        If Not IO.File.Exists(Request.FileName) Then
                            Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "file doesn't exist", 4)
                        End If
                        If IsFileOpen(Request.FileName) Then
                            Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "file is locked", 4)
                        End If

                        iTimeout += 1
                        Threading.Thread.Sleep(500)
                    Loop

                    If IO.File.Exists(Request.FileName) Then
                        Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "file exists", 1)
                        Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, Request.FileName, 2)

                        Request.Contents = ConvertFileToBase64(Request.FileName)


                        Dim sScoreFile As String = Request.FileName & ".score"
                        If IO.File.Exists(sScoreFile) Then
                            Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, "score file exists", 1)
                            Logger.DebugRelative(Logger.THREAD.SCREENSHOTS, sScoreFile, 2)

                            Try
                                Dim sScoreContents As String = My.Computer.FileSystem.ReadAllText(sScoreFile)
                                Dim iScore As Integer = -2

                                If Integer.TryParse(sScoreContents, iScore) Then
                                    Request.Score = iScore
                                Else
                                    LastError = "Invalid score: '" & sScoreContents & "'"
                                End If
                            Catch ex As Exception
                                LastError = "Error while reading file '" & sScoreFile & "': " & ex.Message
                            End Try
                        Else
                            LastError = "Could not find score file '" & sScoreFile & "'"
                        End If


                        Return True
                    Else
                        LastError = "Could not find file '" & Request.FileName & "'"
                    End If
                Else
                    LastError = "Could not start screenshotter"
                End If
            Catch ex As Exception
                LastError = "Error: " & ex.Message
            End Try
        Else
            LastError = "Could not get temp directory '" & __current_logged_in_user_tmp_dir & "'"
        End If


        Return False
    End Function

    Private Shared Function ConvertFileToBase64(ByVal fileName As String) As String
        Return Convert.ToBase64String(System.IO.File.ReadAllBytes(fileName))
    End Function

    Private Shared Function IsFileOpen(ByVal file As String) As Boolean
        Dim stream As FileStream = Nothing
        Try
            Dim fi As New FileInfo(file)
            stream = fi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None)
            stream.Close()
        Catch ex As Exception
            If TypeOf ex Is IOException AndAlso IsFileLocked(ex) Then
                Return True
            End If
        End Try

        Return False
    End Function

    Private Shared Function IsFileLocked(exception As Exception) As Boolean
        Dim errorCode As Integer = Marshal.GetHRForException(exception) And ((1 << 16) - 1)

        Return errorCode = 32 OrElse errorCode = 33
    End Function
End Class
