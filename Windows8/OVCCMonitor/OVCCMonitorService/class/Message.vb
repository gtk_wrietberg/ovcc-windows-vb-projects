﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports murrayju.ProcessExtensions

Public Class Message
    Public Shared Function Show([Title] As String, [Text] As String, [Buttons] As Integer) As Boolean
        Dim __app As String = IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, Constants.Paths.File.MessageShower)

        Helpers.Registry.SetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Message, Constants.RegistryKeys.VALUE__Monitor_MessageButtons, [Buttons])
        Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Message, Constants.RegistryKeys.VALUE__Monitor_MessageTitle, [Title])
        Helpers.Registry.SetValue_String(Constants.RegistryKeys.KEY__Monitor_Message, Constants.RegistryKeys.VALUE__Monitor_MessageText, [Text])

        Return ProcessExtensions.StartProcessAsCurrentUser(__app)
    End Function
End Class
