﻿Imports System.Globalization

Public Class ApplicationCultureInfo
    Public Shared Sub SetApplicationCultureInfoToEnglishBritish()
        'store system CultureInfo
        Globals.SystemCultureInfo = CultureInfo.CurrentCulture


        'Forcing the service to use en-GB
        Dim culture_enGB As New CultureInfo("en-GB")
        CultureInfo.DefaultThreadCurrentCulture = culture_enGB
    End Sub
End Class
