﻿Imports System.IO
Imports System.ServiceProcess

Public Class Integrity
    Public Class Request
        Private mWatchdogEnabledErrorMessage As String
        Public Property WatchdogEnabledErrorMessage() As String
            Get
                Return mWatchdogEnabledErrorMessage
            End Get
            Set(ByVal value As String)
                mWatchdogEnabledErrorMessage = value
            End Set
        End Property

        Private mWatchdogEnabledTimestamp As Integer
        Public Property WatchdogEnabledTimestamp() As Integer
            Get
                Return mWatchdogEnabledTimestamp
            End Get
            Set(ByVal value As Integer)
                mWatchdogEnabledTimestamp = value
            End Set
        End Property

        Private mWatchdogIsDisabled As Boolean
        Public Property WatchdogIsDisabled() As Boolean
            Get
                Return mWatchdogIsDisabled
            End Get
            Set(ByVal value As Boolean)
                mWatchdogIsDisabled = value
            End Set
        End Property

        Private mWatchdogIsEnabled As Boolean
        Public Property WatchdogIsEnabled() As Boolean
            Get
                Return mWatchdogIsEnabled
            End Get
            Set(ByVal value As Boolean)
                mWatchdogIsEnabled = value
            End Set
        End Property

        Private mWatchdogIsStarted As Boolean
        Public Property WatchdogIsStarted() As Boolean
            Get
                Return mWatchdogIsStarted
            End Get
            Set(ByVal value As Boolean)
                mWatchdogIsDisabled = value
            End Set
        End Property

        Public Sub New()
            mWatchdogEnabledErrorMessage = ""
            mWatchdogEnabledTimestamp = 0

            mWatchdogIsDisabled = False
            mWatchdogIsEnabled = False
            mWatchdogIsStarted = False
        End Sub
    End Class

    Public Class FilesFolders
        Private Shared mFoldersCleaned As New List(Of String)
        Private Shared mFoldersVisible As New List(Of String)

        Public Shared Sub AddFolder_Cleaned(sPath As String)
            If Not mFoldersCleaned.Contains(sPath) Then
                mFoldersCleaned.Add(sPath)
            End If
        End Sub

        Public Shared Sub AddFolder_Visible(sPath As String)
            If Not mFoldersVisible.Contains(sPath) Then
                mFoldersVisible.Add(sPath)
            End If
        End Sub

        Public Shared Sub Initialise()
            Dim sProfileDir As String = GetSiteKioskProfilePath()

            mFoldersCleaned.Add(IO.Path.Combine(sProfileDir, "Downloads"))
            mFoldersCleaned.Add(IO.Path.Combine(sProfileDir, "AppData\Roaming\Skype"))
            mFoldersCleaned.Add(IO.Path.Combine(sProfileDir, "Documents"))
            mFoldersCleaned.Add(IO.Path.Combine(sProfileDir, "Desktop"))
            mFoldersCleaned.Add(IO.Path.Combine(sProfileDir, "Pictures"))

            mFoldersVisible.Add(IO.Path.Combine(sProfileDir, "Documents"))
            mFoldersVisible.Add(IO.Path.Combine(sProfileDir, "Desktop"))
        End Sub

        Public Shared Function GetSiteKioskProfilePath() As String
            Dim sProfileDir As String = ""

            If Not Helpers.WindowsUser.GetProfileDirectory(Constants.Profile.SiteKioskUsername, sProfileDir) Then
                Return ""
            End If


            Return sProfileDir
        End Function

        Public Class BrokenProfile
            Public Shared LastError As String = ""

            Public Shared ReadOnly Property HasError() As Boolean
                Get
                    Return Not LastError.Equals("")
                End Get
            End Property

            Public Shared Function Check() As Boolean
                Dim sCurrentProfileDir As String = GetSiteKioskProfilePath()

                If Not sCurrentProfileDir.EndsWith("\" & Constants.Profile.SiteKioskUsername) Then
                    Return False
                End If

                Return True
            End Function

            Public Shared Function Fix() As Boolean
                If Not Configuration.SkCfg.XML.WaitForLock Then
                    LastError = "XML Lock timeout, already locked by '" & Configuration.SkCfg.XML.IsLockedBy & "'"
                    Return False
                End If

                Configuration.SkCfg.XML.Lock()
                Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
                Configuration.SkCfg.XML.Load()

                For Each sFolder As String In mFoldersCleaned
                    Configuration.SkCfg.XML.AddValue("/download-manager/delete-directories", "dir", sFolder)
                Next

                For Each sFolder As String In mFoldersVisible
                    Configuration.SkCfg.XML.AddValue("/filemanager/visible-paths", "url", sFolder)
                Next


                Return Configuration.SkCfg.XML.Save()
            End Function
        End Class

        Public Class ProfileCleaning
            Public Shared Function CountRecentFiles() As Long
                Dim lSize As Long = 0
                Dim lCount As Long = 0
                Dim dNewest As Date = DateAdd(DateInterval.Minute, -Constants.Settings.ProfileLeftOverFilesAgeThresholdInMinutes, Now())
                Dim dOldest As Date = #2099-01-01#

                Dim lRecentCount As Long = 0

                For Each sPath As String In mFoldersCleaned
                    Dim dNewestTmp As Date = #1970-01-01#

                    DirectoryFileSizeAndCountAndNewestAndOldest(sPath, lSize, lCount, dNewestTmp, dOldest, True)

                    Dim result As Integer = Date.Compare(dNewestTmp, dNewest)
                    If result > 0 Then
                        lRecentCount += 1
                    End If
                Next

                Return lRecentCount
            End Function

            Public Shared Function CountOldFiles() As Long
                Dim lSize As Long = 0
                Dim lCount As Long = 0
                Dim dNewest As Date = DateAdd(DateInterval.Minute, -Constants.Settings.ProfileLeftOverFilesAgeThresholdInMinutes, Now())
                Dim dOldest As Date = #2099-01-01#

                Dim lOldCount As Long = 0

                For Each sPath As String In mFoldersCleaned
                    Dim dNewestTmp As Date = #1970-01-01#

                    DirectoryFileSizeAndCountAndNewestAndOldest(sPath, lSize, lCount, dNewestTmp, dOldest, True)

                    Dim result As Integer = Date.Compare(dNewestTmp, dNewest)
                    If result < 0 Then
                        lOldCount += 1
                    End If
                Next

                Return lOldCount
            End Function

            Private Shared Function DirectoryFileSizeAndCountAndNewestAndOldest(ByVal sPath As String, ByRef lSize As Long, ByRef lCount As Long, ByRef dNewest As Date, ByRef dOldest As Date, Optional ByVal bRecursive As Boolean = False) As Boolean
                Dim diDir As New IO.DirectoryInfo(sPath)

                Try
                    Dim fil As FileInfo
                    Dim result As Integer

                    Logger.DebugRelative(Logger.THREAD.METRICS, sPath, 1)
                    For Each fil In diDir.GetFiles()
                        Logger.DebugRelative(Logger.THREAD.METRICS, fil.Name, 2)

                        If fil.Attributes.HasFlag(FileAttributes.Hidden) Then
                            Logger.DebugRelative(Logger.THREAD.METRICS, "skipped (hidden)", 3)

                            Continue For
                        End If

                        If fil.Extension = ".lnk" Then
                            Logger.DebugRelative(Logger.THREAD.METRICS, "skipped (link)", 3)

                            Continue For
                        End If

                        lSize += fil.Length
                        lCount += 1

                        result = Date.Compare(fil.LastWriteTime, dOldest)
                        If result < 0 Then
                            dOldest = fil.LastWriteTime
                        End If

                        result = Date.Compare(fil.LastWriteTime, dNewest)
                        If result > 0 Then
                            dNewest = fil.LastWriteTime
                        End If
                    Next fil

                    If bRecursive = True Then
                        Dim diSubDir As DirectoryInfo

                        For Each diSubDir In diDir.GetDirectories()
                            DirectoryFileSizeAndCountAndNewestAndOldest(diSubDir.FullName, lSize, lCount, dNewest, dOldest, True)
                        Next diSubDir
                    End If

                    Return True
                Catch ex As System.IO.FileNotFoundException
                    Return False
                Catch exx As Exception
                    Return False
                End Try

                Return True
            End Function
        End Class
    End Class

    Public Class OVCC
        Public Class SiteKiosk
            Public Shared Function Restart() As Integer
                Return Helpers.SiteKiosk.Application.Restart()
            End Function
        End Class

        Public Class Theme
            Public Shared Function CheckForSkinScriptErrors() As Integer
                Return CheckForSkinScriptErrors(2)
            End Function

            Public Shared Function CheckForSkinScriptErrors(days As Integer) As Integer
                Dim sSiteKioskLogFolder As String = Helpers.SiteKiosk.GetSiteKioskLogfileFolder()
                Dim dNow As Date = Now
                Dim objReader As System.IO.StreamReader
                Dim sLine As String
                Dim iSkinScriptErrors As Integer = 0

                For dIndex As Long = days To 1 Step -1
                    Dim dCurrent As Date = DateAdd(DateInterval.Day, -dIndex, dNow)
                    Dim sLogfile As String = IO.Path.Combine(sSiteKioskLogFolder, dCurrent.ToString("yyyy-MM-dd") & ".txt")

                    Logger.MessageRelative(Logger.THREAD.INTEGRITY, "Parsing logfile " & sLogfile, 1)

                    If Not IO.File.Exists(sLogfile) Then
                        Logger.ErrorRelative(Logger.THREAD.INTEGRITY, "Does not exist", 2)

                        Continue For
                    End If


                    objReader = New StreamReader(sLogfile)

                    Do While objReader.Peek() <> -1
                        sLine = objReader.ReadLine()

                        'Skin script error
                        If Constants.RegularExpressions.SkinScriptError.Match(sLine).Success Then
                            Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.SkinScriptError.Replace(sLine, Constants.RegularExpressions.SkinScriptError_Replace_Date))

                            iSkinScriptErrors += 1

                            Logger.MessageRelative(Logger.THREAD.INTEGRITY, "Found skin script error at " & dTmp.ToString, 2)
                        End If

                        'Skin script error
                        If Constants.RegularExpressions.SkinScriptHtmlDialogBoxError.Match(sLine).Success Then
                            Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.SkinScriptHtmlDialogBoxError.Replace(sLine, Constants.RegularExpressions.SkinScriptHtmlDialogBoxError_Replace_Date))

                            iSkinScriptErrors += 1

                            Logger.MessageRelative(Logger.THREAD.INTEGRITY, "Found skin script (HTMl dialog) error at " & dTmp.ToString, 2)
                        End If
                    Loop
                Next

                Return iSkinScriptErrors
            End Function

        End Class

        Public Class Applications
            Public Shared Function GetUnlinkedApplicationsCount() As Integer
                Dim iAppsFound As Integer = 0
                Dim iAppsOk As Integer = 0

                If CheckForUnlinkedApplications(iAppsFound, iAppsOk) Then
                    Return iAppsFound - iAppsOk
                Else
                    Return -1
                End If
            End Function

            Public Shared Function CheckForUnlinkedApplications(ByRef iAppsFound As Integer, ByRef iAppsOk As Integer) As Boolean
                Dim _app As String = ""
                Dim _apps As New List(Of String)
                Dim _counter As Integer = 0
                Dim _ret As Boolean = True

                iAppsFound = 0
                iAppsOk = 0

                Configuration.SkCfg.XML.Lock()
                Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
                Configuration.SkCfg.XML.Load()

                If Configuration.SkCfg.XML.GetFirstValue(Constants.SkCfg.Applications, Constants.SkCfg.Applications_Attrib_filename, _app) Then
                    _apps.Add(_app)

                    Do While Configuration.SkCfg.XML.GetNextValue(Constants.SkCfg.Applications, Constants.SkCfg.Applications_Attrib_filename, _app)
                        _apps.Add(_app)
                    Loop
                End If
                Configuration.SkCfg.XML.Unlock()

                If Configuration.SkCfg.XML.HasError Then
                    Logger.ErrorRelative(Logger.THREAD.INTEGRITY, "Error while retrieving apps from " & Helpers.SiteKiosk.GetSiteKioskActiveConfig, 1)
                    Logger.ErrorRelative(Logger.THREAD.INTEGRITY, Configuration.SkCfg.XML.LastError, 2)

                    _ret = False
                End If


                iAppsFound = _apps.Count


                For Each __app As String In _apps
                    Logger.DebugRelative(Logger.THREAD.INTEGRITY, "Checking if file exists", 1)
                    Logger.DebugRelative(Logger.THREAD.INTEGRITY, _RemoveStupidQuoteFromFilePath(__app), 2)
                    If IO.File.Exists(_RemoveStupidQuoteFromFilePath(__app)) Then
                        Logger.DebugRelative(Logger.THREAD.INTEGRITY, "ok", 3)
                        iAppsOk += 1
                    Else
                        Logger.DebugRelative(Logger.THREAD.INTEGRITY, "not found", 3)
                    End If
                Next

                Logger.DebugRelative(Logger.THREAD.INTEGRITY, "result", 2)
                Logger.DebugRelative(Logger.THREAD.INTEGRITY, "iAppsFound=" & iAppsFound.ToString, 3)
                Logger.DebugRelative(Logger.THREAD.INTEGRITY, "iAppsOk   =" & iAppsOk.ToString, 3)


                Return _ret
            End Function

            Private Shared Function _RemoveStupidQuoteFromFilePath(sFilePath As String) As String
                sFilePath = sFilePath.Replace("&quot;", "")
                sFilePath = sFilePath.Replace("""", "")

                Return sFilePath
            End Function
        End Class
    End Class

    Public Class Services
        Public Class Watchdog
            Private Shared ReadOnly _ServiceName As String = "OVCCWatchdog"


            Public Shared Function IsServiceDisabled() As Boolean
                If ServiceInstaller.ServiceIsInstalled(_ServiceName) Then
                    Dim oServiceBootFlag As ServiceBootFlag
                    Dim sRet As String = ServiceInstaller.GetStartupType(_ServiceName, oServiceBootFlag)
                    If sRet.Equals("") Then
                        Return (oServiceBootFlag And ServiceBootFlag.Disabled)
                    End If
                End If

                Return False
            End Function

            Public Shared Function IsServiceStopped() As Boolean
                If ServiceInstaller.ServiceIsInstalled(_ServiceName) Then
                    Dim oServiceControllerStatusFlag As ServiceControllerStatus
                    oServiceControllerStatusFlag = ServiceInstaller.GetServiceStatus(_ServiceName)

                    Return (oServiceControllerStatusFlag And ServiceControllerStatus.Stopped)
                End If

                Return False
            End Function

            Public Shared Function IsServiceStartedAutomatically() As Boolean
                If ServiceInstaller.ServiceIsInstalled(_ServiceName) Then
                    Dim oServiceBootFlag As ServiceBootFlag
                    Dim sRet As String = ServiceInstaller.GetStartupType(_ServiceName, oServiceBootFlag)
                    If sRet.Equals("") Then
                        Return (oServiceBootFlag = ServiceBootFlag.AutoStart)
                    End If
                End If

                Return False
            End Function

            Public Shared Function SetAutoStart() As Boolean
                Dim sRet As String = ServiceInstaller.ChangeStartupType(_ServiceName, ServiceBootFlag.AutoStart)
                Return sRet.Equals("")
            End Function

            Public Shared Function StartService() As Boolean
                Return ServiceInstaller.StartService(_ServiceName)
            End Function
        End Class

        Public Class WatchdogWatchdog
            Private Shared ReadOnly _ServiceName As String = "zzz_OVCCWatchdogWatchdog"

            Public Shared Function DoesServiceExist()
                Return ServiceInstaller.ServiceIsInstalled(_ServiceName)
            End Function

            Public Shared Function DisableService() As Boolean
                Dim sRet As String = ServiceInstaller.ChangeStartupType(_ServiceName, ServiceBootFlag.Disabled)
                Return sRet.Equals("")
            End Function

            Public Shared Function StopService() As Boolean
                Return ServiceInstaller.StartService(_ServiceName)
            End Function
        End Class
    End Class
End Class
