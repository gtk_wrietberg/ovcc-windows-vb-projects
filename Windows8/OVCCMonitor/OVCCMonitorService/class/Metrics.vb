﻿Imports System.Diagnostics.Eventing.Reader

Public Class Metrics
    Public Class Data
        Public Class Session
            Private mStart As Date
            Public Property Start() As Date
                Get
                    Return mStart
                End Get
                Set(ByVal value As Date)
                    mStart = value
                End Set
            End Property

            Private mEnd As Date
            Public Property [End]() As Date
                Get
                    Return mEnd
                End Get
                Set(ByVal value As Date)
                    mEnd = value
                End Set
            End Property

            Public ReadOnly Property StartTimestamp() As Long
                Get
                    Return DateDiff(DateInterval.Second, #1/1/1970#, mStart)
                End Get
            End Property

            Public ReadOnly Property EndTimestamp() As Long
                Get
                    Return DateDiff(DateInterval.Second, #1/1/1970#, mEnd)
                End Get
            End Property

            Public Sub New(dStart As Date, dEnd As Date)
                mStart = dStart
                mEnd = dEnd
            End Sub
        End Class


        Private mSessions As List(Of Session)
        Private mScreensaver As List(Of Session)
        Private mEmergencyScreen As List(Of Session)
        Private mUrlsVisited As Dictionary(Of String, Integer)
        Private mSiteKioskCrash As List(Of Date)
        Private mSiteKioskScriptError As List(Of Date)
        Private mUnexpectedShutdowns As List(Of Date)
        Private mDowntimeDuration As Long


        Private mDate As Date
        Public Property [Date]() As Date
            Get
                Return mDate
            End Get
            Set(ByVal value As Date)
                mDate = value
            End Set
        End Property

        Public ReadOnly Property Timestamp As Long
            Get
                Return Helpers.TimeDate.ToUnix(mDate)
            End Get
        End Property

        Public ReadOnly Property TimestampWithoutTime As Long
            Get
                Return Helpers.TimeDate.ToUnix(New Date(mDate.Year, mDate.Month, mDate.Day))
            End Get
        End Property

        Private mYear As Integer
        Public Property Year() As Integer
            Get
                Return mYear
            End Get
            Set(ByVal value As Integer)
                mYear = value
            End Set
        End Property

        Private mMonth As Integer
        Public Property Month() As Integer
            Get
                Return mMonth
            End Get
            Set(ByVal value As Integer)
                mMonth = value
            End Set
        End Property

        Private mDay As Integer
        Public Property Day() As Integer
            Get
                Return mDay
            End Get
            Set(ByVal value As Integer)
                mDay = value
            End Set
        End Property


        Public Sub New()
            Me.New(#1970-01-01#)
        End Sub

        Public Sub New(Year As Integer, Month As Integer, Day As Integer)
            Me.New(New Date(Year, Month, Day))
        End Sub

        Public Sub New(dDate As Date)
            mYear = dDate.Year
            mMonth = dDate.Month - 1 'Thank you JavaScript
            mDay = dDate.Day

            mSessions = New List(Of Session)
            mScreensaver = New List(Of Session)
            mEmergencyScreen = New List(Of Session)
            mUrlsVisited = New Dictionary(Of String, Integer)
            mSiteKioskCrash = New List(Of Date)
            mSiteKioskScriptError = New List(Of Date)
            mUnexpectedShutdowns = New List(Of Date)

            mDowntimeDuration = 0

            mReturnMessage = ""
            mDate = dDate
        End Sub

        Public Sub CleanupSessions()
            For Each tmp As Session In mSessions.ToList()
                If tmp.StartTimestamp > tmp.EndTimestamp Then
                    'invalid session, get rid of it
                    mSessions.Remove(tmp)
                End If
            Next

            For Each tmp As Session In mScreensaver.ToList()
                If tmp.StartTimestamp > tmp.EndTimestamp Then
                    'invalid session, get rid of it
                    mScreensaver.Remove(tmp)
                End If
            Next

            For Each tmp As Session In mEmergencyScreen.ToList()
                If tmp.StartTimestamp > tmp.EndTimestamp Then
                    'invalid session, get rid of it
                    mEmergencyScreen.Remove(tmp)
                End If
            Next
        End Sub

        Public Sub AddUserSessionStart(dStart As Date)
            mSessions.Add(New Session(dStart, #1970-01-01#))
        End Sub

        Public Sub AddUserSessionEnd(dEnd As Date)
            If mSessions.Count > 0 Then
                mSessions.Item(mSessions.Count - 1).End = dEnd
                mUserSessionDuration += (mSessions.Item(mSessions.Count - 1).End - mSessions.Item(mSessions.Count - 1).Start).TotalSeconds
            End If
        End Sub

        Public Sub AddScreensaverStart(dStart As Date)
            mScreensaver.Add(New Session(dStart, #1970-01-01#))
        End Sub

        Public Sub AddScreensaverEnd(dEnd As Date)
            If mScreensaver.Count > 0 Then
                mScreensaver.Item(mScreensaver.Count - 1).End = dEnd
                mScreensaverDuration += (mScreensaver.Item(mScreensaver.Count - 1).End - mScreensaver.Item(mScreensaver.Count - 1).Start).TotalSeconds
            End If
        End Sub

        Public Sub AddEmergencyScreenStart(dStart As Date)
            mEmergencyScreen.Add(New Session(dStart, #1970-01-01#))
        End Sub

        Public Sub AddEmergencyScreenEnd(dEnd As Date)
            If mEmergencyScreen.Count > 0 Then
                mEmergencyScreen.Item(mEmergencyScreen.Count - 1).End = dEnd
            End If
        End Sub

        Public Sub AddSiteKioskCrash(dDate As Date)
            mSiteKioskCrash.Add(dDate)
        End Sub

        Public Sub AddSiteKioskScriptError(dDate As Date)
            mSiteKioskScriptError.Add(dDate)
        End Sub

        Public Sub AddUnexpectedShutdown(dDate As Date)
            mUnexpectedShutdowns.Add(dDate)
        End Sub

        Public Sub AddUrl(sUrl As String)
            If mUrlsVisited.ContainsKey(sUrl) Then
                mUrlsVisited.Item(sUrl) += 1
            Else
                mUrlsVisited.Add(sUrl, 1)
            End If
        End Sub

        Public ReadOnly Property UserSessions() As List(Of Session)
            Get
                Return mSessions
            End Get
        End Property

        Public ReadOnly Property UserSessionCount() As Integer
            Get
                Return mSessions.Count
            End Get
        End Property

        Private mUserSessionDuration As Long = 0
        Public ReadOnly Property UserSessionDuration() As Long
            Get
                Return mUserSessionDuration
            End Get
        End Property

        Public ReadOnly Property Screensavers() As List(Of Session)
            Get
                Return mScreensaver
            End Get
        End Property

        Public ReadOnly Property ScreensaverCount As Integer
            Get
                Return mScreensaver.Count
            End Get
        End Property

        Private mScreensaverDuration As Long = 0
        Public ReadOnly Property ScreensaverDuration() As Long
            Get
                Return mScreensaverDuration
            End Get
        End Property

        Public ReadOnly Property EmergencyScreens() As List(Of Session)
            Get
                Return mEmergencyScreen
            End Get
        End Property

        Public ReadOnly Property EmergencyScreenCount() As Integer
            Get
                Return mEmergencyScreen.Count
            End Get
        End Property

        Public ReadOnly Property SiteKioskCrashes() As List(Of Date)
            Get
                Return mSiteKioskCrash
            End Get
        End Property

        Public ReadOnly Property SiteKioskCrashCount() As Integer
            Get
                Return mSiteKioskCrash.Count
            End Get
        End Property

        Public ReadOnly Property SiteKioskScriptError() As List(Of Date)
            Get
                Return mSiteKioskScriptError
            End Get
        End Property

        Public ReadOnly Property SiteKioskScriptErrorCount() As Integer
            Get
                Return mSiteKioskScriptError.Count
            End Get
        End Property

        Public ReadOnly Property UnexpectedShutdowns() As List(Of Date)
            Get
                Return mUnexpectedShutdowns
            End Get
        End Property

        Public ReadOnly Property UnexpectedShutdownsCount() As Integer
            Get
                Return mUnexpectedShutdowns.Count
            End Get
        End Property

        Public ReadOnly Property UrlsVisited() As Dictionary(Of String, Integer)
            Get
                Return mUrlsVisited
            End Get
        End Property

        Public ReadOnly Property UrlsVisitedCount() As Integer
            Get
                Dim iTotal As Integer = 0

                For Each kv As KeyValuePair(Of String, Integer) In mUrlsVisited
                    iTotal += kv.Value
                Next

                Return iTotal
            End Get
        End Property

        Public Property DowntimeDuration As Long
            Get
                Return mDowntimeDuration
            End Get
            Set(ByVal value As Long)
                mDowntimeDuration = value
            End Set
        End Property


        Private mReturnMessage As String
        Public Property ReturnMessage() As String
            Get
                Return mReturnMessage
            End Get
            Set(ByVal value As String)
                mReturnMessage = value
            End Set
        End Property

        Private mLogfilePath As String
        Public Property LogfilePath() As String
            Get
                Return mLogfilePath
            End Get
            Set(ByVal value As String)
                mLogfilePath = value
            End Set
        End Property

        Private mLogfileExists As Boolean
        Public Property LogfileExists() As Boolean
            Get
                Return mLogfileExists
            End Get
            Set(ByVal value As Boolean)
                mLogfileExists = value
            End Set
        End Property
    End Class

    Public Class WindowsUser
        Public Shared Function GetCurrentLoggedOnUser() As String
            Dim sLoggedOnUser As String = ""

            If Not GetCurrentLoggedOnUser(sLoggedOnUser) Then
                Return "(no user)"
            End If

            Return sLoggedOnUser
        End Function

        Public Shared Function GetCurrentLoggedOnUser(ByRef sLoggedOnUser As String) As Boolean
            Try
                Dim searcher As Management.ManagementObjectSearcher = New Management.ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem")
                Dim collection As Management.ManagementObjectCollection = searcher.Get()

                sLoggedOnUser = Convert.ToString(collection.Cast(Of Management.ManagementBaseObject).First()("UserName"))

                Dim sLoggedOnUserArr As String() = sLoggedOnUser.Split("\")

                If sLoggedOnUserArr.Length > 1 Then
                    sLoggedOnUser = sLoggedOnUserArr(sLoggedOnUserArr.Length - 1)
                End If

                Return True
            Catch ex As Exception

            End Try

            Return False
        End Function
    End Class

    Public Class SiteKiosk
        Public Class QuickMetrics
            Public Class _Usage
                Private mInUse As Boolean
                Public Property InUse() As Boolean
                    Get
                        Return mInUse
                    End Get
                    Set(ByVal value As Boolean)
                        mInUse = value
                    End Set
                End Property

                Private mHasUsage As Boolean
                Public Property HasUsage() As Boolean
                    Get
                        Return mHasUsage
                    End Get
                    Set(ByVal value As Boolean)
                        mHasUsage = value
                    End Set
                End Property

                Private mHasLogout As Boolean
                Public Property HasLogout() As Boolean
                    Get
                        Return mHasLogout
                    End Get
                    Set(ByVal value As Boolean)
                        mHasLogout = value
                    End Set
                End Property

                Private mLastUsageTimestamp As Long
                Public Property LastUsageTimestamp() As Long
                    Get
                        Return mLastUsageTimestamp
                    End Get
                    Set(ByVal value As Long)
                        mLastUsageTimestamp = value
                    End Set
                End Property

                Public Sub New()
                    mInUse = False
                    mHasLogout = False
                    mHasUsage = False
                    mLastUsageTimestamp = 0
                End Sub
            End Class

            Public Shared Function LastUsageTimestamp() As _Usage
                Dim _u As New _Usage

                Try
                    Dim sSiteKioskLogFolder As String = Helpers.SiteKiosk.GetSiteKioskLogfileFolder()
                    Dim sSiteKioskNewestLogFile As String = Helpers.FilesAndFolders.GetNewestFile(sSiteKioskLogFolder, "[0-9]{4}-[0-9]{2}-[0-9]{2}\.txt", True)

                    Dim bwReader As New BackwardReader
                    Dim sLine As String
                    Dim dDate As Date = #1970-01-01#

                    bwReader.Path = sSiteKioskNewestLogFile

                    If bwReader.ReadFile Then
                        Do While Not bwReader.SOF
                            sLine = bwReader.ReadLine

                            If Constants.RegularExpressions.Logout.Match(sLine).Success Then
                                Logger.DebugRelative("found 'logout' line", 1)
                                _u.InUse = False
                                _u.HasLogout = True
                            End If

                            If Constants.RegularExpressions.TermsAccepted.Match(sLine).Success Or Constants.RegularExpressions.TermsDetectedAction.Match(sLine).Success Then
                                If Constants.RegularExpressions.TermsAccepted.Match(sLine).Success Then
                                    Logger.DebugRelative("found 'terms accepted' line", 1)
                                    dDate = Date.Parse(Constants.RegularExpressions.TermsAccepted.Replace(sLine, Constants.RegularExpressions.TermsAccepted_Replace_Date))
                                End If

                                If Constants.RegularExpressions.TermsDetectedAction.Match(sLine).Success Then
                                    Logger.DebugRelative("found 'terms action' line", 1)
                                    dDate = Date.Parse(Constants.RegularExpressions.TermsDetectedAction.Replace(sLine, Constants.RegularExpressions.TermsDetectedAction_Replace_Date))
                                End If

                                If Not _u.HasLogout Then
                                    _u.InUse = True
                                End If

                                _u.HasUsage = True

                                Exit Do
                            End If
                        Loop
                    End If

                    bwReader.Close()


                    _u.LastUsageTimestamp = Helpers.TimeDate.ToUnix(dDate)
                Catch ex As Exception

                End Try

                Return _u
            End Function

            'Public Shared Function LastUsageTimestamp() As Long
            '    Dim sSiteKioskLogFolder As String = Helpers.SiteKiosk.GetSiteKioskLogfileFolder()
            '    Dim sSiteKioskNewestLogFile As String = Helpers.FilesAndFolders.GetNewestFile(sSiteKioskLogFolder, "[0-9]{4}-[0-9]{2}-[0-9]{2}\.txt", True)

            '    Dim bwReader As New BackwardReader
            '    Dim sLine As String
            '    Dim dDate As Date = #1970-01-01#
            '    Dim bEnded As Boolean = False

            '    bwReader.Path = sSiteKioskNewestLogFile

            '    If bwReader.ReadFile Then
            '        Do While Not bwReader.SOF
            '            sLine = bwReader.ReadLine

            '            If Constants.RegularExpressions.TermsAccepted.Match(sLine).Success Or Constants.RegularExpressions.TermsDetectedAction.Match(sLine).Success Then
            '                If Not bEnded Then
            '                    If Constants.RegularExpressions.TermsAccepted.Match(sLine).Success Then
            '                        dDate = Date.Parse(Constants.RegularExpressions.TermsAccepted.Replace(sLine, Constants.RegularExpressions.TermsAccepted_Replace_Date))
            '                    End If

            '                    If Constants.RegularExpressions.TermsDetectedAction.Match(sLine).Success Then
            '                        dDate = Date.Parse(Constants.RegularExpressions.TermsDetectedAction.Replace(sLine, Constants.RegularExpressions.TermsDetectedAction_Replace_Date))
            '                    End If
            '                End If

            '                Exit Do
            '            End If

            '            If Constants.RegularExpressions.Logout.Match(sLine).Success Then
            '                bEnded = True
            '            End If
            '        Loop
            '    End If

            '    Return Helpers.TimeDate.ToUnix(dDate)
            'End Function

            Public Shared Function LastScreensaverTimestamp() As Long
                Try
                    Dim sSiteKioskLogFolder As String = Helpers.SiteKiosk.GetSiteKioskLogfileFolder()
                    Dim sSiteKioskNewestLogFile As String = Helpers.FilesAndFolders.GetNewestFile(sSiteKioskLogFolder, "[0-9]{4}-[0-9]{2}-[0-9]{2}\.txt", True)

                    Dim bwReader As New BackwardReader
                    Dim sLine As String
                    Dim dDate As Date = #1970-01-01#
                    Dim bEnded As Boolean = False

                    bwReader.Path = sSiteKioskNewestLogFile

                    If bwReader.ReadFile Then
                        Do While Not bwReader.SOF
                            sLine = bwReader.ReadLine

                            If Constants.RegularExpressions.ScreensaverActivated.Match(sLine).Success Then
                                If Not bEnded Then
                                    dDate = Date.Parse(Constants.RegularExpressions.ScreensaverActivated.Replace(sLine, Constants.RegularExpressions.ScreensaverActivated_Replace_Date))
                                End If

                                Exit Do
                            End If

                            If Constants.RegularExpressions.ScreensaverDeactivated.Match(sLine).Success Then
                                bEnded = True
                            End If
                        Loop
                    End If

                    bwReader.Close()

                    Return Helpers.TimeDate.ToUnix(dDate)
                Catch ex As Exception

                End Try

                Return 0
            End Function

            Public Shared Function IsInEmergencyState() As Boolean
                Dim bRet As Boolean = False

                Try
                    Dim sSiteKioskLogFolder As String = Helpers.SiteKiosk.GetSiteKioskLogfileFolder()
                    Dim sSiteKioskNewestLogFile As String = Helpers.FilesAndFolders.GetNewestFile(sSiteKioskLogFolder, "[0-9]{4}-[0-9]{2}-[0-9]{2}\.txt", True)

                    Dim bwReader As New BackwardReader
                    Dim sLine As String
                    Dim dDate As Date = #1970-01-01#
                    Dim bInEmergencyState As Boolean = False

                    bwReader.Path = sSiteKioskNewestLogFile

                    If bwReader.ReadFile Then
                        Do While Not bwReader.SOF
                            sLine = bwReader.ReadLine

                            If Constants.RegularExpressions.EmergencyScreenStart.Match(sLine).Success Then
                                bInEmergencyState = True
                                Exit Do
                            End If

                            If Constants.RegularExpressions.EmergencyScreenEnd.Match(sLine).Success Then
                                bInEmergencyState = False
                                Exit Do
                            End If

                            If Constants.RegularExpressions.SiteKioskStarted.Match(sLine).Success Then
                                bInEmergencyState = False
                                Exit Do
                            End If

                            If Constants.RegularExpressions.SiteKioskClosed.Match(sLine).Success Then
                                bInEmergencyState = False
                                Exit Do
                            End If
                        Loop
                    End If

                    bwReader.Close()

                    bRet = bInEmergencyState
                Catch ex As Exception

                End Try


                If Constants.Settings.WriteEmergencyModeStateToLocalFileForNinja Then
                    Try
                        If bRet Then
                            Logger.MessageRelative("writing emergency state file", 1)
                            My.Computer.FileSystem.WriteAllText(Constants.Paths.File.Emergency_Mode, "1", False)
                            Logger.MessageRelative("ok", 2)
                        Else
                            If My.Computer.FileSystem.FileExists(Constants.Paths.File.Emergency_Mode) Then
                                Logger.MessageRelative("deleting emergency state file", 1)
                                My.Computer.FileSystem.DeleteFile(Constants.Paths.File.Emergency_Mode)
                                Logger.MessageRelative("ok", 2)
                            End If
                        End If
                    Catch ex As Exception
                        Logger.ErrorRelative("emergency state file error!", 2)
                        Logger.ErrorRelative(ex.Message, 3)
                    End Try
                End If


                Return bRet
            End Function
        End Class

        Public Class LogFile
            Public Shared Function Parse(oData As Metrics.Data) As Boolean
                Dim sSiteKioskLogFolder As String = Helpers.SiteKiosk.GetSiteKioskLogfileFolder()
                Dim sLogfile As String = IO.Path.Combine(sSiteKioskLogFolder, oData.Date.ToString("yyyy-MM-dd") & ".txt")

                oData.LogfilePath = sLogfile

                If Not IO.File.Exists(sLogfile) Then
                    oData.LogfileExists = False
                    oData.ReturnMessage = "File not found"

                    Return False
                End If

                oData.LogfileExists = True


                Dim objReader As New System.IO.StreamReader(sLogfile)
                Dim sLine As String


                Do While objReader.Peek() <> -1
                    sLine = objReader.ReadLine()

                    'Emergency mode
                    If Constants.RegularExpressions.EmergencyScreenStart.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.EmergencyScreenStart.Replace(sLine, Constants.RegularExpressions.EmergencyScreenStart_Replace_Date))
                        oData.AddEmergencyScreenStart(dTmp)

                        Continue Do
                    End If

                    If Constants.RegularExpressions.EmergencyScreenEnd.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.EmergencyScreenEnd.Replace(sLine, Constants.RegularExpressions.EmergencyScreenEnd_Replace_Date))
                        oData.AddEmergencyScreenEnd(dTmp)

                        Continue Do
                    End If


                    'Session start
                    If Constants.RegularExpressions.TermsAccepted.Match(sLine).Success Or Constants.RegularExpressions.TermsDetectedAction.Match(sLine).Success Then
                        If Constants.RegularExpressions.TermsAccepted.Match(sLine).Success Then
                            Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.TermsAccepted.Replace(sLine, Constants.RegularExpressions.TermsAccepted_Replace_Date))
                            oData.AddUserSessionStart(dTmp)

                            Continue Do
                        End If

                        If Constants.RegularExpressions.TermsDetectedAction.Match(sLine).Success Then
                            Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.TermsDetectedAction.Replace(sLine, Constants.RegularExpressions.TermsDetectedAction_Replace_Date))
                            oData.AddUserSessionStart(dTmp)

                            Continue Do
                        End If
                    End If

                    'session end - logout
                    If Constants.RegularExpressions.Logout.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.Logout.Replace(sLine, Constants.RegularExpressions.Logout_Replace_Date))
                        oData.AddUserSessionEnd(dTmp)

                        Continue Do
                    End If


                    'Screensaver
                    If Constants.RegularExpressions.ScreensaverActivated.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.ScreensaverActivated.Replace(sLine, Constants.RegularExpressions.ScreensaverActivated_Replace_Date))
                        oData.AddScreensaverStart(dTmp)

                        Continue Do
                    End If

                    If Constants.RegularExpressions.ScreensaverDeactivated.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.ScreensaverDeactivated.Replace(sLine, Constants.RegularExpressions.ScreensaverDeactivated_Replace_Date))
                        oData.AddScreensaverEnd(dTmp)

                        Continue Do
                    End If

                    If Constants.RegularExpressions.SiteKioskClosed.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.SiteKioskClosed.Replace(sLine, Constants.RegularExpressions.SiteKioskClosed_Replace_Date))
                        oData.AddScreensaverEnd(dTmp)

                        Continue Do
                    End If


                    'SiteKiosk crash
                    If Constants.RegularExpressions.SiteKioskCrashed.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.SiteKioskCrashed.Replace(sLine, Constants.RegularExpressions.SiteKioskCrashed_Replace_Date))
                        oData.AddSiteKioskCrash(dTmp)

                        Continue Do
                    End If


                    'SiteKiosk Script errors
                    If Constants.RegularExpressions.SkinScriptError.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.SkinScriptError.Replace(sLine, Constants.RegularExpressions.SkinScriptError_Replace_Date))
                        oData.AddSiteKioskScriptError(dTmp)

                        Continue Do
                    End If
                    If Constants.RegularExpressions.SkinScriptHtmlDialogBoxError.Match(sLine).Success Then
                        Dim dTmp As Date = Date.Parse(Constants.RegularExpressions.SkinScriptHtmlDialogBoxError.Replace(sLine, Constants.RegularExpressions.SkinScriptHtmlDialogBoxError_Replace_Date))
                        oData.AddSiteKioskScriptError(dTmp)

                        Continue Do
                    End If


                    'Visited urls
                    If Constants.RegularExpressions.UrlVisited.Match(sLine).Success Then
                        Dim sTmp As String = Constants.RegularExpressions.UrlVisited.Replace(sLine, Constants.RegularExpressions.UrlVisited_Replace_HostOnly)
                        oData.AddUrl(sTmp)

                        Continue Do
                    End If
                Loop


                oData.CleanupSessions()

                oData.ReturnMessage = "ok"
                Return True
            End Function
        End Class

    End Class

    'Public Class Downtime
    '    Public Shared Function Calculate(oData As Metrics.Data) As Boolean
    '        'Calculate downtime
    '        Dim lDowntimeTotalDuration As Long = Globals.DownTime.End - Globals.DownTime.Start

    '        If lDowntimeTotalDuration > 0 Then
    '            'Check if downtime was started on this day
    '            If Globals.DownTime.Start >= oData.TimestampWithoutTime And Globals.DownTime.Start < (oData.TimestampWithoutTime + Constants.Misc.SecondsInDay) Then
    '                Dim lDowntimeEnd As Long = oData.TimestampWithoutTime + Constants.Misc.SecondsInDay
    '                If lDowntimeEnd > Globals.DownTime.End Then
    '                    lDowntimeEnd = Globals.DownTime.End
    '                End If
    '                Dim lDowntimeDuration As Long = lDowntimeEnd - Globals.DownTime.Start

    '                oData.DowntimeDuration = lDowntimeDuration
    '                Globals.DownTime.Start += lDowntimeDuration
    '            End If
    '        End If


    '        oData.ReturnMessage = "ok"

    '        Return True
    '    End Function
    'End Class

    Public Class ShutdownAndRestarts
        Public Shared Function Count(oData As Metrics.Data) As Boolean
            Dim dDate As Date = New DateTime(oData.Date.Year, oData.Date.Month, oData.Date.Day)
            Dim lTmp As List(Of EventRecord) = WindowsEventLog.System.GetEventsForEventIdAndDate(41, dDate)

            For Each _event As EventRecord In lTmp
                oData.AddUnexpectedShutdown(_event.TimeCreated)
            Next

            Return True
        End Function
    End Class

    Public Class Timezone
        Public Shared Function GetOffset() As Double
            Return Helpers.TimeDate.Timezone.getOffset
        End Function

        Public Shared Function GetFullName() As String
            Dim zone As System.TimeZone = System.TimeZone.CurrentTimeZone
            Dim zoneName As String
            If zone.IsDaylightSavingTime(DateTime.Now) Then
                zoneName = zone.DaylightName
            Else
                zoneName = zone.StandardName
            End If

            Return zoneName
        End Function

        Public Shared Function GetOffsetString() As String
            Dim now As DateTime = DateTime.Now
            Dim nowUTC As DateTime = now.ToUniversalTime
            Dim minuteDiff As Integer = DateDiff(DateInterval.Minute, nowUTC, now)
            Dim hourDiff As Integer = Math.Floor(minuteDiff / 60)
            Dim sTimezone As String = "UTC"

            If minuteDiff >= 0 Then
                sTimezone &= "+"
            End If

            sTimezone &= Format(hourDiff, "00").ToString
            minuteDiff -= (hourDiff * 60)
            sTimezone &= ":" & Format(minuteDiff, "00").ToString

            Return sTimezone
        End Function
    End Class

    Public Class OS
        Public Shared Function Version() As String
            Dim sVersion As String

            sVersion = OSVersionExtension.OSVersion.GetOperatingSystem().ToString
            If OSVersionExtension.OSVersion.Is64BitOperatingSystem Then
                sVersion &= " (64-bit)"
            End If

            Return sVersion
        End Function

        Public Class Language
            Public Shared Function [Long]() As String
                Return Globals.SystemCultureInfo.EnglishName
            End Function

            Public Shared Function [Short]() As String
                Return Globals.SystemCultureInfo.ThreeLetterISOLanguageName
            End Function
        End Class

        Public Class UptimeDowntime
            Public Shared Function UptimeSeconds() As Integer
                Try
                    Dim uptime As New PerformanceCounter("System", "System Up Time")
                    uptime.NextValue()
                    Return TimeSpan.FromSeconds(uptime.NextValue).TotalSeconds
                Catch ex As Exception
                    Return -1
                End Try
            End Function

            Public Shared Sub LastShutdownAndReboot(ByRef lLastReboot As Long, ByRef lLastShutdown As Long)
                Try
                    Dim lEvents As List(Of EventRecord) = WindowsEventLog.System.GetEventsRecordForEventId(1074)
                    Dim dDate_Reboot As Date = #1970-01-01#
                    Dim dDate_Shutdown As Date = #1970-01-01#

                    For Each _eventRecord As EventRecord In lEvents
                        For Each _prop As EventProperty In _eventRecord.Properties
                            If _prop.Value.ToString.Contains("power off") Then
                                If dDate_Shutdown.CompareTo(_eventRecord.TimeCreated) < 0 Then
                                    dDate_Shutdown = _eventRecord.TimeCreated

                                    Exit For
                                End If
                            End If
                            If _prop.Value.ToString.Contains("restart") Then
                                If dDate_Reboot.CompareTo(_eventRecord.TimeCreated) < 0 Then
                                    dDate_Reboot = _eventRecord.TimeCreated

                                    Exit For
                                End If
                            End If
                        Next
                    Next

                    lLastReboot = Helpers.TimeDate.ToUnix(dDate_Reboot)
                    lLastShutdown = Helpers.TimeDate.ToUnix(dDate_Shutdown)
                Catch ex As Exception

                End Try
            End Sub
        End Class
    End Class

    Public Class SystemInformation
        Public Class Hardware
            Public Shared Function Chipset() As String
                Try
                    Dim searcher As Management.ManagementObjectSearcher = New Management.ManagementObjectSearcher("SELECT Name FROM Win32_Processor")
                    Dim collection As Management.ManagementObjectCollection = searcher.Get()

                    Return Convert.ToString(collection.Cast(Of Management.ManagementBaseObject).First()("Name"))
                Catch ex As Exception

                End Try

                Return ""
            End Function
        End Class
    End Class
End Class
