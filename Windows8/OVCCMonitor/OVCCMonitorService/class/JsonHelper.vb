﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Class JsonHelper
    Public Shared Function DictionaryToJson(dict As Dictionary(Of String, String)) As String
        Dim root As New JObject()
        For Each kvp As KeyValuePair(Of String, String) In dict
            FindOrAdd(root, kvp.Key, kvp.Value)
        Next
        Return root.ToString()
    End Function

    Private Shared Function FindOrAdd(parent As JObject, key As String, value As String) As JObject
        If key Is Nothing Then Return parent
        Dim i As Integer = key.IndexOf(".")
        If i > -1 Then
            Dim obj As JObject = FindOrAdd(parent, key.Substring(0, i), Nothing)
            Return FindOrAdd(obj, key.Substring(i + 1), value)
        End If
        Dim prop As JProperty = parent.Property(key)
        If value Is Nothing Then
            Dim child As JObject
            If prop Is Nothing Then
                child = New JObject()
                parent.Add(key, child)
            ElseIf prop.Value.Type = JTokenType.Object Then
                child = DirectCast(prop.Value, JObject)
            Else
                Throw New JsonException("The key """ + parent.Path + "." + key + """ already has a value.")
            End If
            Return child
        Else
            If prop Is Nothing Then
                parent.Add(key, New JValue(value))
                Return parent
            Else
                Throw New JsonException("The key """ + parent.Path + "." + key + """ already has a value.")
            End If
        End If
    End Function
End Class