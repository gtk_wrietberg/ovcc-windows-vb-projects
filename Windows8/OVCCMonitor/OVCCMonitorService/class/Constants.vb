﻿Imports System.Text.RegularExpressions

Public Class Constants
    Public Class Service
        Public Shared ReadOnly Name As String = "OVCCMonitor"
    End Class

    Public Class Paths
        Public Class Directory
            Public Shared ReadOnly Versions As String = IO.Path.Combine(Helpers.FilesAndFolders.GetGuestTekFolder(), "_versions")

            Public Shared ReadOnly Themes As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder, "Skins\Public\Themes")
        End Class

        Public Class File
            Public Shared ReadOnly ThemeVersion As String = "theme.txt"

            Public Shared ReadOnly SiteKioskPaymentDialog As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder, "SiteCash\Html\PaymentInfoDlg.htm")

            Public Shared ReadOnly Ninja__code_ovcc_txt As String = "C:\code.ovcc.txt"
            Public Shared ReadOnly Ninja__navision_ovcc_txt As String = "C:\code.navision.txt"

            Public Shared ReadOnly Emergency_Mode As String = "C:\sitekiosk.emergency.mode"

            Public Shared ReadOnly ScreenshotTaker As String = "OVCCMonitorScreenshot.exe"

            Public Shared ReadOnly MessageShower As String = "OVCCMonitorMessage.exe"

            Public Shared ReadOnly MonitorIdentifier As String = IO.Path.Combine(Helpers.FilesAndFolders.GetGuestTekFolder(), "OVCCSoftware\_monitor_identifier.id")
        End Class
    End Class

    Public Class Settings
        Public Shared ReadOnly BogusDefaultStringValue As String = "QWERTY__NOTSET__UIOP"
        Public Shared ReadOnly MinThreadLoopDelay As Integer = 30
        Public Shared ReadOnly ProfileLeftOverFilesAgeThresholdInMinutes As Integer = 60
        Public Shared ReadOnly LastUsageThresholdInSeconds As Integer = 120
        Public Shared ReadOnly MemoryCPULoopDelayInSeconds As Integer = 10
        Public Shared ReadOnly WriteEmergencyModeStateToLocalFileForNinja As Boolean = False
    End Class

    Public Class BatchFile
        Public Shared ReadOnly DeleteFolder As String = "RMDIR / S / Q ""__CLEANUP_FOLDER__"""
        Public Shared ReadOnly DeleteZipFile As String = "RMDIR / S / Q ""__CLEANUP_ZIP__"""
        Public Shared ReadOnly DeleteMyself As String = "(GOTO) 2>NUL & DEL ""%~f0"""
    End Class

    Public Class Communication
        Public Shared ReadOnly UserAgentString As String = "OVCCMonitor"
        Public Shared ReadOnly IpInfoToken As String = "bb8acc31702c73"
        Public Shared ReadOnly IpInfoUrl As String = "https://ipinfo.io?token=" & IpInfoToken
    End Class

    Public Class SiteKiosk
        Public Shared ReadOnly ProcessName As String = "sitekiosk"

        Public Enum EmergencyModeFixTypes As Integer
            NONE = 0
            RESTART_SITEKIOSK = 1
            REBOOT_MACHINE = 2
            UNFIXABLE = 3
        End Enum

        Public Class Themes
            Public Shared ReadOnly DisabledThemeIndex As Integer = 123
            Public Shared ReadOnly DisabledThemeName As String = "__disabled"
        End Class
    End Class

    Public Class FileFolders
        Public Class Update
            Public Shared ReadOnly DefaultName As String = "OVCCMonitor.%%VERSIONNUMBER%%.zip"
        End Class

        Public Class Theme
            Public Shared ReadOnly DefaultName As String = "Theme.%%INDEX%%.zip"
        End Class
    End Class

    Public Class RegistryKeys
        Public Shared ReadOnly KEY__Monitor As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCMonitor"

        Public Shared ReadOnly VALUE__Monitor_IsFirstRunAfterUpdate As String = "IsFirstRunAfterUpdate"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_IsFirstRunAfterUpdate As Boolean = True

        Public Shared ReadOnly VALUE__Monitor_ForceUpdate As String = "ForceUpdate"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ForceUpdate As Boolean = False

        Public Shared ReadOnly VALUE__Monitor_ForceRequestSettings As String = "ForceRequestSettings"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ForceRequestSettings As Boolean = True

        Public Shared ReadOnly VALUE__Monitor_LastHeartbeatSuccessTimestamp As String = "LastHeartbeatSuccessTimestamp"
        Public Shared ReadOnly DEFAULTVALUE__LastHeartbeatSuccessTimestamp As Long = -1

        Public Shared ReadOnly VALUE__Monitor_LastHeartbeatError As String = "LastHeartbeatError"
        Public Shared ReadOnly DEFAULTVALUE__LastHeartbeatError As String = ""

        Public Shared ReadOnly VALUE__Monitor_MachineIdentifier As String = "MachineIdentifier"
        Public Shared ReadOnly DEFAULTVALUE__MachineIdentifier As String = ""

        Public Shared ReadOnly VALUE__Monitor_MonitorIdentifier As String = "MonitorIdentifier"
        Public Shared ReadOnly DEFAULTVALUE__MonitorIdentifier As String = ""

        Public Shared ReadOnly VALUE__Monitor_PolicyScore As String = "PolicyScore"
        Public Shared ReadOnly DEFAULTVALUE__PolicyScore As String = ""


        Public Shared ReadOnly KEY__Monitor_Message As String = KEY__Monitor & "\Message"

        Public Shared ReadOnly VALUE__Monitor_MessageTitle As String = "Title"
        Public Shared ReadOnly DEFAULTVALUE__MessageTitle As String = ""

        Public Shared ReadOnly VALUE__Monitor_MessageText As String = "Text"
        Public Shared ReadOnly DEFAULTVALUE__MessageText As String = ""

        Public Shared ReadOnly VALUE__Monitor_MessageButtons As String = "Buttons"
        Public Shared ReadOnly DEFAULTVALUE__MessageButtons As Integer = 0


        Public Shared ReadOnly KEY__Monitor_Configuration As String = KEY__Monitor & "\Configuration"


        Public Shared ReadOnly KEY__Monitor_Configuration_Application As String = KEY__Monitor_Configuration & "\Application"

        Public Shared ReadOnly VALUE__MonitorSettings_Debug As String = "Debug"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_Debug As Boolean = False

        Public Shared ReadOnly VALUE__MonitorSettings_ApiEnvironment As String = "ApiEnvironment"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ApiEnvironment As String = "production"

        Public Shared ReadOnly VALUE__Monitor_MachineName As String = "MachineName"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_MachineName As String = ""

        Public Shared ReadOnly VALUE__Monitor_PropertyId As String = "PropertyId"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_PropertyId As Integer = -1

        Public Shared ReadOnly VALUE__MonitorSettings_LastHeartbeat As String = "LastHeartbeat"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastHeartbeat As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastScreenshot As String = "LastScreenshot"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastScreenshot As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastApplicationSettingsSync As String = "LastApplicationSettingsSync"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastApplicationSettingsSync As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastOVCCSettingsSync As String = "LastOVCCSettingsSync"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastOVCCSettingsSync As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastOVCCThemesSync As String = "LastOVCCThemesSync"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastOVCCThemesSync As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastMetricsSync As String = "LastMetricsSync"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastMetricsSync As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastIntegritySync As String = "LastIntegritySync"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastIntegritySync As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastInventorySync As String = "LastInventorySync"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastInventorySync As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastGeolocationSync As String = "LastGeolocationSync"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastGeolocationSync As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastApplicationSettingsSyncDelay As String = "LastApplicationSettingsSyncDelay"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastApplicationSettingsSyncDelay As Integer = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastOVCCSettingsSynDelay As String = "LastOVCCSettingsSyncDelay"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastOVCCSettingsSyncDelay As Integer = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastOVCCThemesSyncDelay As String = "LastOVCCThemesSyncDelay"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastOVCCThemesSyncDelay As Integer = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastMetricsSyncDelay As String = "LastMetricsSyncDelay"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastMetricsSyncDelay As Integer = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastIntegritySyncDelay As String = "LastIntegritySyncDelay"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastIntegritySyncDelay As Integer = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastInventorySyncDelay As String = "LastInventorySyncDelay"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastInventorySyncDelay As Integer = 0

        Public Shared ReadOnly VALUE__MonitorSettings_LastGeolocationSyncDelay As String = "LastGeolocationSyncDelay"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_LastGeolocationSyncDelay As Integer = 86400

        Public Shared ReadOnly VALUE__MonitorSettings_CurrentThemeIndexBackup As String = "__DISABLED__CurrentThemeIndexBackup"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_CurrentThemeIndexBackup As Integer = 667

        Public Shared ReadOnly VALUE__MonitorSettings_RenameHappened As String = "RenameHappened"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_RenameHappened As Boolean = False

        Public Shared ReadOnly VALUE__MonitorSettings_EmergencyModeDetected As String = "EmergencyModeDetected"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_EmergencyModeDetected As Boolean = False

        Public Shared ReadOnly VALUE__MonitorSettings_EmergencyModeDetectedCount As String = "EmergencyModeDetectedCount"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_EmergencyModeDetectedCount As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_EmergencyModeDetectedTimestamp As String = "EmergencyModeDetectedTimestamp"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_EmergencyModeDetectedTimestamp As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_EmergencyModeFixedCount As String = "EmergencyModeFixedCount"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_EmergencyModeFixedCount As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_EmergencyModeFixedTimestamp As String = "EmergencyModeFixedTimestamp"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_EmergencyModeFixedTimestamp As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_EmergencyModeLastRebootTimestamp As String = "EmergencyModeLastRebootTimestamp"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_EmergencyModeLastRebootTimestamp As Long = 0

        Public Shared ReadOnly VALUE__MonitorSettings_EmergencyModeFixType As String = "EmergencyModeFixType"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_EmergencyModeFixType As Integer = SiteKiosk.EmergencyModeFixTypes.NONE

        Public Shared ReadOnly VALUE__MonitorSettings_OVCCThemeIndex As String = "OVCCThemeIndex"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_OVCCThemeIndex As Integer = -1

        Public Shared ReadOnly VALUE__MonitorSettings_OVCCThemePath As String = "OVCCThemePath"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_OVCCThemePath As String = ""


        Public Shared ReadOnly KEY__Monitor_Configuration_Application_Server As String = KEY__Monitor_Configuration_Application & "\Server"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerBaseUrl_Production As String = "BaseUrl_Production"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Production As String = "https://api-ovcc.guesttek.cloud/ovcc-monitor"
        'Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Production__UGLY As String = "https://flim8yrnh7.execute-api.us-east-1.amazonaws.com/ovcc-monitor"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerBaseUrl_Staging As String = "BaseUrl_Staging"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Staging As String = "https://api-ovcc-staging.guesttek.cloud/ovcc-monitor"
        'Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Staging__UGLY As String = "https://jxwo8hjyw0.execute-api.us-east-1.amazonaws.com/ovcc-monitor"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerBaseUrl_Development As String = "BaseUrl_Development"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Development As String = "https://api-ovcc-dev.guesttek.cloud/ovcc-monitor"
        'Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerBaseUrl_Development__UGLY As String = "https://8a3spgj6kb.execute-api.us-east-1.amazonaws.com/ovcc-monitor"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_Heartbeat As String = "EndPoint_Heartbeat"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_Heartbeat As String = "heartbeat"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_ApplicationSetting As String = "EndPoint_ApplicationSettings"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_ApplicationSetting As String = "appsettings"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_Integrity As String = "EndPoint_Integrity"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_Integrity As String = "integrity"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_Inventory As String = "EndPoint_Inventory"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_Inventory As String = "inventory"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_Metrics As String = "EndPoint_Metrics"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_Metrics As String = "metrics"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_OVCCSettings As String = "EndPoint_OVCCSettings"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_OVCCSettings As String = "ovccsettings"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_SendLocalOVCCSettings As String = "EndPoint_SendLocalOVCCSettings"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_SendLocalOVCCSettings As String = "receivelocalovccsettings"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_OVCCThemes As String = "EndPoint_OVCCThemes"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_OVCCThemes As String = "ovccthemes"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_SelfUpdate As String = "EndPoint_SelfUpdate"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_SelfUpdate As String = "selfupdate"

        Public Shared ReadOnly VALUE__MonitorSettings_ServerEndPoint_Screenshots As String = "EndPoint_Screenshots"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_ServerEndPoint_Screenshots As String = "screenshots"


        Public Shared ReadOnly KEY__Monitor_Configuration_Application_Download As String = KEY__Monitor_Configuration_Application & "\Download"

        Public Shared ReadOnly VALUE__MonitorSettings_DownloadBaseUrl As String = "BaseUrl"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_DownloadBaseUrl As String = "https://ccms-public.s3.amazonaws.com/OVCC-Monitor"

        Public Shared ReadOnly VALUE__MonitorSettings_DownloadSubdir_Themes As String = "Subdir_Themes"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_DownloadSubdir_Themes As String = "Themes/win"

        Public Shared ReadOnly VALUE__MonitorSettings_DownloadSubdir_SelfUpdate As String = "Subdir_SelfUpdate"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_DownloadSubdir_SelfUpdate As String = "SelfUpdate"

        Public Shared ReadOnly VALUE__MonitorSettings_DownloadSubdir_ExternalApps As String = "Subdir_ExternalApps"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_DownloadSubdir_ExternalApps As String = "ExternalApps"


        Public Shared ReadOnly KEY__Monitor_Configuration_OVCC As String = KEY__Monitor_Configuration & "\OVCC"

        Public Shared ReadOnly VALUE__MonitorSettings_HashTheme As String = "HashTheme"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_HashTheme As String = ""

        Public Shared ReadOnly VALUE__MonitorSettings_HashScreensaver As String = "HashScreensaver"
        Public Shared ReadOnly DEFAULTVALUE__MonitorSettings_HashScreensaver As String = ""


        Public Shared ReadOnly KEY__Monitor_Configuration_Themes As String = KEY__Monitor_Configuration_OVCC & "\Themes"
        Public Shared ReadOnly KEY__Monitor_Configuration_Screensavers As String = KEY__Monitor_Configuration_OVCC & "\Screensavers"

        Public Shared ReadOnly VALUE__Monitor_Configuration_Themes_Version As String = "Version"
        Public Shared ReadOnly VALUE__Monitor_Configuration_Screensavers_Version As String = "Version"


        Public Shared ReadOnly KEY__Monitor_ExternalAppExecute As String = KEY__Monitor & "\ExternalAppExecute"

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_Name As String = "Name"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_Name As String = ""

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_Params As String = "Params"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_Params As String = ""

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_DownloadedFile As String = "DownloadedFile"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_DownloadedFile As String = ""

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_IsBusy As String = "IsBusy"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_IsBusy As Boolean = False

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_IsRunning As String = "IsRunning"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_IsRunning As Boolean = False

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_IsDownloaded As String = "IsDownloaded"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_IsDownloaded As Boolean = False

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_IsExecuted As String = "IsExecuted"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_IsExecuted As Boolean = False

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_IsDone As String = "IsDone"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_IsDone As Boolean = False

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_IsReadyToReport As String = "IsReadyToReport"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_IsReadyToReport As Boolean = False

        Public Shared ReadOnly VALUE__Monitor_ExternalAppExecute_State As String = "State"
        Public Shared ReadOnly DEFAULTVALUE__Monitor_ExternalAppExecute_State As String = ""


        'Watchdog stuff
        Public Shared ReadOnly KEY__Watchdog As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog"
        Public Shared ReadOnly VALUE__Watchdog__DisabledUntilOVCCStart As String = "DisabledUntilOVCCStart"

        Public Shared ReadOnly KEY__Watchdog__LastActivity As String = KEY__Watchdog & "\_\LastActivity"
        Public Shared ReadOnly VALUE__Watchdog__LastActivity As String = "LastActivity"


        'Windows shutdown time
        Public Shared ReadOnly KEY__SystemCurrentControlSetControlWindows As String = "HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Windows"
        Public Shared ReadOnly VALUE__WindowsShutdownTime As String = "ShutdownTime"


        'Ninja stuff
        Public Shared ReadOnly KEY__NinjaAgent As String = "HKEY_LOCAL_MACHINE\SOFTWARE\NinjaRMM LLC\NinjaRMMAgent\Agent"
        Public Shared ReadOnly VALUE__NinjaNodeID As String = "NodeId"
        Public Shared ReadOnly DEFAULTVALUE__NinjaNodeID As Integer = -1
    End Class

    Public Class Metrics
        Public Shared ReadOnly MaxLogfileHistory As Integer = 7
    End Class

    Public Class Misc
        Public Shared ReadOnly SecondsInDay As Long = 24 * 60 * 60
    End Class

    Public Class PaymentDialogFileName
        Public Shared ReadOnly PASSWORD As String = "password"
        Public Shared ReadOnly RESIDENCE_INN As String = "residence_inn"
        Public Shared ReadOnly [DEFAULT] As String = "default"
    End Class

    Public Class SkCfg
        Public Shared ReadOnly LogoutUrl As String = "/browserbar/logout-navigation/url"
        Public Shared ReadOnly ThemeUrl As String = "/startpageconfig/startpage"
        Public Shared ReadOnly ScreensaverUrl As String = "/screensaver/url"

        Public Shared ReadOnly Applications As String = "/programs/file"
        Public Shared ReadOnly Applications_Attrib_filename As String = "filename"

        Public Shared ReadOnly BrowserbarUrls As String = "/browserbar/browserbar-urls"
        Public Shared ReadOnly BrowserbarUrls__value As String = "url"
    End Class

    Public Class Profile
        Public Shared ReadOnly SiteKioskUsername As String = "SiteKiosk"
    End Class

    Public Class TimeoutsAndDelays
        Public Shared ReadOnly ActionBreather As Integer = 5000

        Public Shared ReadOnly EmergencyModeFixBreather As Integer = 30000
        Public Shared ReadOnly EmergencyModeRebootDelay As Long = 3600 * 6
    End Class

    Public Class RegularExpressions
        Public Shared ReadOnly EmergencyScreenStart As Regex = New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\])(.*)(emergency screen has been started)(.+)$")
        Public Shared ReadOnly EmergencyScreenStart_Replace_Date As String = "$3 $4"

        Public Shared ReadOnly EmergencyScreenEnd As Regex = New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\])(.*)(exited the emergency mode)(.+)$")
        Public Shared ReadOnly EmergencyScreenEnd_Replace_Date As String = "$3 $4"

        Public Shared ReadOnly UrlVisited As Regex = New Regex("^(.+)(\[SiteKiosk\] )(Frame-Navigation|Navigation)(: )(http[s]*://)([^/]+)(.*)$")
        Public Shared ReadOnly UrlVisited_Replace_HostOnly As String = "$5$6"


        Public Shared ReadOnly TermsAccepted As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[TermsAndConditions\] Accepted)(.*)$")
        Public Shared ReadOnly TermsAccepted_Replace_Date As String = "$3 $4"
        Public Shared ReadOnly TermsAccepted_Replace_Time As String = "$4"

        Public Shared ReadOnly TermsDetectedAction As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[TermsAndConditions\] detected action)(.*)$")
        Public Shared ReadOnly TermsDetectedAction_Replace_Date As String = "$3 $4"
        Public Shared ReadOnly TermsDetectedAction_Replace_Time As String = "$4"


        Public Shared ReadOnly Logout As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[LogoutScript\])(.+)$")
        Public Shared ReadOnly Logout_Replace_Date As String = "$3 $4"


        Public Shared ReadOnly ScreensaverActivated As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\] Screen Saver activated)(.*)$")
        Public Shared ReadOnly ScreensaverActivated_Replace_Date As String = "$3 $4"

        Public Shared ReadOnly ScreensaverDeactivated As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\] Screen Saver deactivated)(.*)$")
        Public Shared ReadOnly ScreensaverDeactivated_Replace_Date As String = "$3 $4"

        Public Shared ReadOnly SiteKioskStarted As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\] started)(.*)$")
        Public Shared ReadOnly SiteKioskStarted_Replace_Date As String = "$3 $4"

        Public Shared ReadOnly SiteKioskClosed As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\] closed)(.*)$")
        Public Shared ReadOnly SiteKioskClosed_Replace_Date As String = "$3 $4"


        Public Shared ReadOnly SiteKioskCrashed As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\] abnormal sitekiosk program termination)(.*)$")
        Public Shared ReadOnly SiteKioskCrashed_Replace_Date As String = "$3 $4"

        Public Shared ReadOnly SkinScriptError As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\] Skin script error occured)(.*)$")
        Public Shared ReadOnly SkinScriptError_Replace_Date As String = "$3 $4"

        Public Shared ReadOnly SkinScriptHtmlDialogBoxError As New Regex("^([0-9]{2}) ([0-9a-z]{4}) (.+) (.+) (.+) (\[SiteKiosk\] Script error in HTML dialog box)(.*)$")
        Public Shared ReadOnly SkinScriptHtmlDialogBoxError_Replace_Date As String = "$3 $4"


        '20 03ea 2024-11-12 22:44:41 +0200 [SiteKiosk] closed
        '20 03e9 2024-11-12 22:44:44 +0200 [SiteKiosk] started
    End Class
End Class
