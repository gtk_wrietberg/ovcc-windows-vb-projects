﻿Public Class EmergencyModeLogger
    Public Shared Sub Write(sMsg As String)
        Dim dUtcNow As DateTime = DateTime.UtcNow
        Dim sMsgDatePrefix As String = Helpers.TimeDate.UnixTimestamp(dUtcNow) & " - " & dUtcNow.ToString

        _write(sMsgDatePrefix & " - " & sMsg)
    End Sub

    Private Shared Sub _write(ByVal sString As String)
        Try
            Dim sFile As String = IO.Path.Combine(Logger.LogFileBasePath, "emergency_mode_log.txt")

            Using sw As New IO.StreamWriter(sFile, True)
                sw.WriteLine(sString)
            End Using
        Catch ex As Exception

        End Try
    End Sub
End Class
