﻿Imports System.Drawing

Public Class Logger
    Public Enum THREAD
        MAIN = 0
        FIRST_RUN_AFTER_UPDATE
        POST_UPDATE_CLEANUP
        LOGZIPPER
        MEM_CPU
        HANDSHAKE
        HEARTBEAT
        SCREENSHOTS
        NETWORK_INFO
        MESSAGE
        APP_SETTINGS
        OVCC_SETTINGS
        THEMES_SYNC
        THEME_DOWNLOAD
        INTEGRITY
        INVENTORY
        METRICS
        SELF_UPDATE
        CURRENT_METRICS
        SEND_LOCAL_OVCCSETTINGS
        RENAME_MACHINE
        RUN_EXTERNAL_EXE
        EMERGENCY_MODE
        SITEKIOSK
    End Enum

    Public Shared Debugging As Boolean = True

    Private Shared MaxThreadNameLength As Integer = 0

    Public Shared Function Initialise()
        Dim sTmp As String
        Dim aTHREADs As Array

        aTHREADs = System.Enum.GetValues(GetType(THREAD))

        For Each iTHREAD As Integer In aTHREADs
            sTmp = System.Enum.GetName(GetType(THREAD), iTHREAD)

            If sTmp.Length > MaxThreadNameLength Then
                MaxThreadNameLength = sTmp.Length
            End If
        Next

        Return Helpers.Logger.InitialiseLogger()
    End Function

    Public Shared ReadOnly Property LogFileBasePath() As String
        Get
            Return Helpers.Logger.LogFileBasePath
        End Get
    End Property

    Public Shared ReadOnly Property LogFileName() As String
        Get
            Return Helpers.Logger.LogFileName
        End Get
    End Property

    Public Shared Function Message(sMessage As String) As String
        Return Message(THREAD.MAIN, sMessage, 0)
    End Function

    Public Shared Function Message(sMessage As String, iDepth As Integer) As String
        Return Message(THREAD.MAIN, sMessage, iDepth)
    End Function

    Public Shared Function Message(tThread As THREAD, sMessage As String) As String
        Return Message(tThread, sMessage, 0)
    End Function

    Public Shared Function Message(tThread As THREAD, sMessage As String, iDepth As Integer) As String
        Return _write(tThread, sMessage, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, iDepth, False, False)
    End Function

    Public Shared Function MessageRelative(sMessage As String) As String
        Return MessageRelative(THREAD.MAIN, sMessage, 0)
    End Function

    Public Shared Function MessageRelative(sMessage As String, iDepth As Integer) As String
        Return MessageRelative(THREAD.MAIN, sMessage, iDepth)
    End Function

    Public Shared Function MessageRelative(tThread As THREAD, sMessage As String, iDepth As Integer) As String
        Return _write(tThread, sMessage, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, iDepth, False, True)
    End Function


    Public Shared Function Warning(sMessage As String) As String
        Return Warning(THREAD.MAIN, sMessage, 0)
    End Function

    Public Shared Function Warning(sMessage As String, iDepth As Integer) As String
        Return Warning(THREAD.MAIN, sMessage, iDepth)
    End Function

    Public Shared Function Warning(tThread As THREAD, sMessage As String) As String
        Return Warning(tThread, sMessage, 0)
    End Function

    Public Shared Function Warning(tThread As THREAD, sMessage As String, iDepth As Integer) As String
        Return _write(tThread, sMessage, Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, iDepth, False, False)
    End Function

    Public Shared Function WarningRelative(sMessage As String) As String
        Return WarningRelative(THREAD.MAIN, sMessage, 0)
    End Function

    Public Shared Function WarningRelative(sMessage As String, iDepth As Integer) As String
        Return WarningRelative(THREAD.MAIN, sMessage, iDepth)
    End Function

    Public Shared Function WarningRelative(tThread As THREAD, sMessage As String, iDepth As Integer) As String
        Return _write(tThread, sMessage, Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, iDepth, False, True)
    End Function


    Public Shared Function [Error](sMessage As String) As String
        Return [Error](THREAD.MAIN, sMessage, 0)
    End Function

    Public Shared Function [Error](sMessage As String, iDepth As Integer) As String
        Return [Error](THREAD.MAIN, sMessage, iDepth)
    End Function

    Public Shared Function [Error](tThread As THREAD, sMessage As String) As String
        Return [Error](tThread, sMessage, 0)
    End Function

    Public Shared Function [Error](tThread As THREAD, sMessage As String, iDepth As Integer) As String
        Return _write(tThread, sMessage, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, iDepth, False, False)
    End Function

    Public Shared Function ErrorRelative(sMessage As String) As String
        Return ErrorRelative(THREAD.MAIN, sMessage, 0)
    End Function

    Public Shared Function ErrorRelative(sMessage As String, iDepth As Integer) As String
        Return ErrorRelative(THREAD.MAIN, sMessage, iDepth)
    End Function

    Public Shared Function ErrorRelative(tThread As THREAD, sMessage As String, iDepth As Integer) As String
        Return _write(tThread, sMessage, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, iDepth, False, True)
    End Function


    Public Shared Function Debug(sMessage As String) As String
        Return Debug(THREAD.MAIN, sMessage, 0)
    End Function

    Public Shared Function Debug(sMessage As String, iDepth As Integer) As String
        Return Debug(THREAD.MAIN, sMessage, iDepth)
    End Function

    Public Shared Function Debug(tThread As THREAD, sMessage As String) As String
        Return Debug(tThread, sMessage, 0)
    End Function

    Public Shared Function Debug(tThread As THREAD, sMessage As String, iDepth As Integer) As String
        Return _write(tThread, sMessage, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, iDepth, False, False)
    End Function

    Public Shared Function DebugRelative(sMessage As String) As String
        Return DebugRelative(THREAD.MAIN, sMessage, 0)
    End Function

    Public Shared Function DebugRelative(sMessage As String, iDepth As Integer) As String
        Return DebugRelative(THREAD.MAIN, sMessage, iDepth)
    End Function

    Public Shared Function DebugRelative(tThread As THREAD, sMessage As String, iDepth As Integer) As String
        Return _write(tThread, sMessage, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, iDepth, False, True)
    End Function


    Private Shared Function _write(tThread As THREAD, sMessage As String, cMessageType As Helpers.Logger.MESSAGE_TYPE, iDepth As Integer, bDoNotWriteToFile As Boolean, bRelative As Boolean) As String
        Helpers.Logger.Debugging = Debugging

        Dim sThread As String = ""

        If System.Enum.IsDefined(GetType(THREAD), tThread) Then
            sThread = System.Enum.GetName(GetType(THREAD), tThread)
        Else
            sThread = "UNKNOWN"
        End If


        Dim iPadLeft As Integer = Math.Floor((MaxThreadNameLength - sThread.Length) / 2)
        Dim iPadRight As Integer = Math.Ceiling((MaxThreadNameLength - sThread.Length) / 2)

        sThread = (New String(" ", iPadLeft)) & sThread & (New String(" ", iPadRight))

        If Globals.EchoLoggerMessagesInConsole Then
            Dim sTmpMessageType As String = ""

            Select Case cMessageType
                Case Helpers.Logger.MESSAGE_TYPE.LOG_WARNING
                    sTmpMessageType = "[*] "
                Case Helpers.Logger.MESSAGE_TYPE.LOG_ERROR
                    sTmpMessageType = "[!] "
                Case Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG
                    sTmpMessageType = "[#] "
                Case Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT
                    sTmpMessageType = "[.] "
                Case Else
                    sTmpMessageType = "[?] "
            End Select

            Console.WriteLine(sTmpMessageType & " - " & sThread & " - " & sMessage)
        End If


        If Not bRelative Then
            Return Helpers.Logger.Write(sThread, sMessage, cMessageType, iDepth, bDoNotWriteToFile)
        Else
            Return Helpers.Logger.WriteRelative(sThread, sMessage, cMessageType, iDepth, bDoNotWriteToFile)
        End If
    End Function
End Class
