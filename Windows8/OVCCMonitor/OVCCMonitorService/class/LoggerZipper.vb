﻿Imports System.IO
Imports System.IO.Compression

Public Class LoggerZipper
    Public Shared Sub ZipLogs()
        Logger.Message(Logger.THREAD.LOGZIPPER, "Zipping old logs", 0)
        Logger.Message(Logger.THREAD.LOGZIPPER, "dir", 1)
        Logger.Message(Logger.THREAD.LOGZIPPER, Logger.LogFileBasePath, 2)

        Dim zipNum As Integer = 0, ReturnMessage As String = ""
        zipNum = LoggerZipper._ZipOldLogs("log", Logger.LogFileBasePath, Logger.LogFileName, True, 0, ReturnMessage)

        If zipNum >= 0 Then
            If zipNum = 1 Then
                Logger.Message(Logger.THREAD.LOGZIPPER, "1 file zipped", 1)
            Else
                Logger.Message(Logger.THREAD.LOGZIPPER, zipNum.ToString & " files zipped", 1)
            End If
        Else
            Logger.Error(Logger.THREAD.LOGZIPPER, "something went wrong", 1)
            Logger.Error(Logger.THREAD.LOGZIPPER, ReturnMessage, 2)
        End If
    End Sub

    Private Shared Function _ZipOldLogs(Extension As String, Path As String, CurrentLogfile As String, Recursive As Boolean, Threshold As Integer, ByRef ReturnMessage As String) As Integer
        ReturnMessage = ""

        If Threshold < 0 Then
            ReturnMessage = "Threshold is incorrect (" & Threshold.ToString & ") ; should be >=0"

            Return -1
        End If

        Try
            Dim dLogs As New DirectoryInfo(Path)
            Dim oSearch As SearchOption = SearchOption.TopDirectoryOnly

            If Recursive Then
                oSearch = SearchOption.AllDirectories
            End If


            Dim iCount As Integer = 0

            For Each fLog As IO.FileInfo In dLogs.EnumerateFiles("*." & Extension, oSearch).Where(Function(f) f.LastWriteTime < Date.Now.AddDays(-Threshold))
                If Not IO.Path.GetFileNameWithoutExtension(fLog.Name) = CurrentLogfile Then
                    If _zipFile(fLog.FullName) Then
                        Try
                            fLog.Delete()
                        Catch ex As Exception

                        End Try

                        iCount += 1
                    End If
                End If
            Next

            Return iCount
        Catch ex As Exception
            ReturnMessage = ex.Message

            Return -1
        End Try
    End Function

    Private Shared Function _zipFile(filePath As String) As Boolean
        Try
            Dim zipPath As String = filePath & ".zip"

            Using newFile As ZipArchive = ZipFile.Open(zipPath, IO.Compression.ZipArchiveMode.Update)
                newFile.CreateEntryFromFile(filePath, Path.GetFileName(filePath), CompressionLevel.Optimal)
            End Using

            Return File.Exists(zipPath)
        Catch ex As Exception
            Return False
        End Try
    End Function
End Class
