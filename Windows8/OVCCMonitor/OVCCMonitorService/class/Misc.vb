﻿Public Class Misc
    Public Shared Function GetCaller() As String
        Try
            Dim stackTrace As New Diagnostics.StackFrame(1)

            Return stackTrace.GetMethod.Name
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function BooleanToInteger(_b As Boolean) As Integer
        Return IIf(_b, 1, 0)
    End Function
End Class
