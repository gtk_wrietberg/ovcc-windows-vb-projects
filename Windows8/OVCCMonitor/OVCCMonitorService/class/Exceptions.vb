﻿Public Class Exceptions
    Public Class ForcedRebootException
        Inherits Exception

        Public Sub New()
            MyBase.New("Forced reboot!")
        End Sub

        Public Sub New(message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(message As String, inner As Exception)
            MyBase.New(message, inner)
        End Sub
    End Class
End Class
