﻿Public Class OVCCMonitorService
    Protected Overrides Sub OnStart(ByVal args() As String)
        'Forcing the service to use en-GB
        ApplicationCultureInfo.SetApplicationCultureInfoToEnglishBritish()


        '-----------------------------------------------------------------------------------------------------------------------------
        Settings.Application.Load_Debugging()
        Logger.Debugging = Settings.Application.[Private].Debugging

        Logger.Initialise()
        Logger.Message(New String("*", 50), 0)
        Logger.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Logger.Message("service started", 0)


        Identifiers.Generate()


        Settings.Application.Load_Privates()
        Settings.Application.Load_Publics()

        Settings.OVCC.Load()


        Configuration.MakeSureFoldersExist()


        Integrity.FilesFolders.Initialise()


        OVCCThemeMatcher.Initialise()


        If Settings.Application.Private.Debugging Then
            Settings.Application.Public.ThreadDelay_Heartbeat = 1
        End If


        '-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        'Start Memory/Cpu thread
        Actions.MemoryCpuThread.Start()

        'Start logger zipper thread
        Actions.LoggerZipperThread.Start()

        'Start heartbeat thread
        Actions.HeartbeatThread.Start()
    End Sub

    Protected Overrides Sub OnStop()
        Logger.Message("service stopping", 0)

        'make sure that any external app is stopped too, otherwise its thread will hold up the service
        Commands.ExternalApp.Stop()

        'stop all threads
        Actions.MemoryCpuThread.Stop()
        Actions.LoggerZipperThread.Stop()
        Actions.HeartbeatThread.Stop()
    End Sub
End Class
