var SCREENSAVER_DEBUG=false;

function DEBUG(txt) {
	try {
		if(SCREENSAVER_DEBUG) $('#txtDebug').append(txt+"\n"); 
	}
	catch(e) {}
}

$(document).ready(function() {
	StartScreensaver();
});


var curX=0;
var curY=0;
var maxX=0;
var maxY=0;


function StartScreensaver() {
	if(SCREENSAVER_DEBUG) $('#divDebug').css('display','block');

	_randomPos();
	_setInitialPos();
	
	_moveToNewPos();
}


function _randomPos() {
	maxX=$(document).width()-$('#divAnimation').width();
	maxY=$(document).height()-$('#divAnimation').height();

	curX=Math.floor(Math.random()*maxX);
	curY=Math.floor(Math.random()*maxY);
}

function _setInitialPos() {
	$("#divAnimation").css('top',curY);
	$("#divAnimation").css('left',curX);
}

function _moveToNewPos() {
	_animationDone=0;
	_randomPos();
	
	$.when(
		$("#divAnimation").animate({'top':curY+'px','left':curX+'px'},2000).promise()
	).done(function() {
		_canWeContinue();
	});
}

var timeoutAnimation;
function _canWeContinue() {
	clearTimeout(timeoutAnimation);
	timeoutAnimation=setTimeout(function() { _moveToNewPos() },5000);
}
