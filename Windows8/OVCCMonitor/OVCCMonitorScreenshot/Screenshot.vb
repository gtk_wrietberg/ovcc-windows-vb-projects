﻿Imports System.Drawing.Imaging

Public Class Screenshot
    Public Shared MaxWidth As Integer = -1
    Public Shared ImageQuality As Integer = 20

    Public Shared Function Take(_filename As String) As Boolean
        Return Take(_filename, False)
    End Function

    Public Shared Function Take(_filename As String, _quality As Long) As Boolean
        Dim _size As Size = New Size(My.Computer.Screen.Bounds.Width, My.Computer.Screen.Bounds.Height)

        Return _Take(_filename, _size, _quality)
    End Function

    Public Shared Function Take(_filename As String, _privacy_mode As Boolean) As Boolean
        Dim _size As Size = New Size(My.Computer.Screen.Bounds.Width, My.Computer.Screen.Bounds.Height)

        Return _Take(_filename, _size, IIf(_privacy_mode, 0, ImageQuality))
    End Function

    Public Shared Function Take(_filename As String, _size As Size, _privacy_mode As Boolean) As Boolean
        Return _Take(_filename, _size, IIf(_privacy_mode, 0, ImageQuality))
    End Function

    Private Shared Function _Take(_filename As String, _size As Size, _quality As Long) As Boolean
        Try
            Using screenGrab As New Bitmap(_size.Width, _size.Height)
                Using g As Graphics = Graphics.FromImage(screenGrab)
                    g.CopyFromScreen(New Point(0, 0), New Point(0, 0), _size)
                End Using

                If MaxWidth > 0 Then
                    Dim newWidth As Integer = MaxWidth
                    Dim newHeight As Integer = screenGrab.Height / (screenGrab.Width / MaxWidth)

                    SaveJpeg(_filename, ResizeImage(screenGrab, New Size(newWidth, newHeight)), _quality)
                Else
                    SaveJpeg(_filename, screenGrab, _quality)
                End If

                Return True
            End Using
        Catch ex As Exception
        End Try

        Return False
    End Function

    Private Shared Sub SaveJpeg(ByVal path As String, ByVal img As Bitmap, ByVal quality As Long)
        If quality < 0 Then quality = 0
        If quality > 100 Then quality = 100

        Dim qualityParam As New EncoderParameter(Encoder.Quality, quality)
        Dim jpegCodec As ImageCodecInfo = GetEncoderInfo("image/jpeg")

        Dim encoderParams As New EncoderParameters(1)
        encoderParams.Param(0) = qualityParam
        img.Save(path, jpegCodec, encoderParams)


        'save image score file
        Try
            Dim file As IO.StreamWriter
            file = My.Computer.FileSystem.OpenTextFileWriter(path & ".score", False)
            file.Write(CalculateImageScore(img).ToString)
            file.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Function GetEncoderInfo(ByVal mimeType As String) As ImageCodecInfo
        Dim codecs As ImageCodecInfo() = ImageCodecInfo.GetImageEncoders()

        For i As Integer = 0 To codecs.Length - 1
            If (codecs(i).MimeType = mimeType) Then
                Return codecs(i)
            End If
        Next i

        Return Nothing
    End Function

    Private Shared Function ResizeImage(ByVal InputImage As Image, newSize As Size) As Image
        Return New Bitmap(InputImage, newSize)
    End Function

    Public Shared Function CalculateImageScore(ByVal InputImage As Image) As Integer
        Try
            Dim _tmp_width As Integer = 32
            Dim _tmp_height As Integer = 20
            Dim _tmp_bitmap As Bitmap = New Bitmap(InputImage, New Size(_tmp_width, _tmp_height))
            Dim _r_total As Integer = 0, _g_total As Integer = 0, _b_total As Integer = 0
            Dim _c As Color
            Dim _denominator As Double = (256 / 1000) * 3 * _tmp_width * _tmp_height  ' (256 colors) / (scoring = 0-1000) * (3 (r+g+b)) * ( width * height)

            For _x As Integer = 0 To _tmp_width - 1
                For _y As Integer = 0 To _tmp_height - 1
                    _c = _tmp_bitmap.GetPixel(_x, _y)

                    _r_total += CInt(_c.R)
                    _g_total += CInt(_c.G)
                    _b_total += CInt(_c.B)
                Next
            Next

            Return CInt(Math.Round((_r_total + _g_total + _b_total) / _denominator))
        Catch ex As Exception
            Return -1
        End Try
    End Function
End Class
