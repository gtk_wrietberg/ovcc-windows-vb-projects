﻿Imports System.Globalization

Public Class frmMain
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Forcing the service to use en-GB
        Dim culture_enGB As New CultureInfo("en-GB")
        CultureInfo.DefaultThreadCurrentCulture = culture_enGB


        Me.TransparencyKey = Color.Lime
        Me.Opacity = 0.5
        Me.Top = My.Computer.Screen.Bounds.Height + Me.Height
        Me.Left = My.Computer.Screen.Bounds.Width + Me.Width

        Dim _filename As String = ""
        Dim _privacymode As Boolean = False
        ExitCode.SetValue(ExitCode.ExitCodes.OK)

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.StartsWith("--file:") Then
                _filename = arg.Replace("--file:", "")
            End If

            If arg.StartsWith("--max-width:") Then
                If Integer.TryParse(arg.Replace("--max-width:", ""), Screenshot.MaxWidth) Then
                    'bla
                End If
            End If

            If arg.Equals("--privacy-mode") Then
                _privacymode = True
            End If
        Next

        If _filename.Equals("") Then
            ExitCode.SetValue(ExitCode.ExitCodes.NO_FILENAME)
            ExitApplication()
            Exit Sub
        End If

        Dim _dir As String = IO.Path.GetDirectoryName(_filename)
        If Not IO.Directory.Exists(_dir) Then
            ExitCode.SetValue(ExitCode.ExitCodes.FOLDER_DOES_NOT_EXIST)
            ExitApplication()
            Exit Sub
        End If

        Screenshot.Take(_filename, _privacymode)
        ExitApplication()
    End Sub

    Private Sub ExitApplication()
        Environment.ExitCode = ExitCode.GetValue()
        Me.Close()
    End Sub
End Class