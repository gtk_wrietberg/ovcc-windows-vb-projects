Imports System

Module Program
    Sub Main(args As String())
        Dim _filename As String = ""

        For Each arg As String In args
            If arg.StartsWith("--file:") Then
                _filename = arg.Replace("--file:", "")
            End If
        Next


        Dim sReturnString As String = ""
        If IO.File.Exists(_filename) Then
            Try
                Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(_filename)

                sReturnString = myFileVersionInfo.FileVersion
            Catch ex As Exception
            End Try
        End If

        Console.Write(sReturnString)
    End Sub
End Module
