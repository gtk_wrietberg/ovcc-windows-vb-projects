﻿Imports System.Windows.Forms.AxHost

Public Class Form1
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        'TakeScreenShot()
        Screenshot.MaxWidth = 320
        Screenshot.Take("D:\Temp\temp\1_true.jpg", True)
        Screenshot.Take("D:\Temp\temp\1_false.jpg", False)
    End Sub

    Private Function TakeScreenShot() As Boolean
        Me.Hide()

        Try
            Dim screenSize As Size = New Size(My.Computer.Screen.Bounds.Width * 2, My.Computer.Screen.Bounds.Height * 2)

            Using screenGrab As New Bitmap(My.Computer.Screen.Bounds.Width * 2, My.Computer.Screen.Bounds.Height * 2)
                Using g As Graphics = Graphics.FromImage(screenGrab)
                    g.CopyFromScreen(New Point(0, 0), New Point(0, 0), screenSize)
                End Using

                Dim sFilename As String = Date.Now.ToString("yyyyMMdd_HHmmss") & ".png"
                screenGrab.Save("C:\Temp\" & sFilename, Imaging.ImageFormat.Png)
            End Using
        Catch ex As Exception


        End Try

        Me.Show()

        Return True
    End Function

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        OpenFileDialog1.ShowDialog(Me)

        Dim img As Image = New Bitmap(OpenFileDialog1.FileName)

        Dim _score_new As Integer = Screenshot.CalculateImageScore(img)

        MsgBox(_score_new.ToString)
    End Sub
End Class
