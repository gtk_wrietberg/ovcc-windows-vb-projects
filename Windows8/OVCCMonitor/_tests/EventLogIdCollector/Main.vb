﻿Imports System.Diagnostics.Eventing.Reader
Imports System.Text
Imports System.Windows.Forms.ImageList

Module Main
    Private dEventRecords As New SortedDictionary(Of Integer, EventRecord)

    Public Sub Main()
        MsgBox("Started")
        dEventRecords = New SortedDictionary(Of Integer, EventRecord)

        Dim query As New EventLogQuery("System", PathType.LogName, "*[System]")

        Dim tmpCounter As Integer = 0

        Using fileOut = New IO.StreamWriter("c:\temp\test.txt", True)


            Using logReader = New EventLogReader(query)
                Dim _eventInstance As EventRecord = logReader.ReadEvent()

                Try
                    While Not _eventInstance Is Nothing
                        tmpCounter += 1

                        Console.WriteLine(tmpCounter)

                        Try
                            dEventRecords.Add(_eventInstance.Id, _eventInstance)

                            Dim cleanedUpFormatDescription As String = _eventInstance.FormatDescription

                            If cleanedUpFormatDescription Is Nothing Then
                                cleanedUpFormatDescription = ""
                            End If

                            cleanedUpFormatDescription = cleanedUpFormatDescription.Replace(",", "-")
                            cleanedUpFormatDescription = cleanedUpFormatDescription.Replace(vbCrLf, " ")

                            Dim result As String = String.Format("{0},{1},{2},{3},{4}", _eventInstance.TimeCreated.ToString, _eventInstance.Id.ToString, _eventInstance.LevelDisplayName, _eventInstance.ProviderName, cleanedUpFormatDescription)
                            fileOut.WriteLine(result)
                        Catch ex As Exception
                        End Try


                        _eventInstance = logReader.ReadEvent()
                    End While
                Catch ex As Exception
                    If _eventInstance IsNot Nothing Then
                        _eventInstance.Dispose()
                    End If

                Finally
                    If _eventInstance IsNot Nothing Then
                        _eventInstance.Dispose()
                    End If

                    fileOut.Close()
                End Try

            End Using
        End Using


        'Dim fileOut As System.IO.StreamWriter


        'Using fileOut = New IO.StreamWriter("c:\temp\test.txt", True)
        '    For Each kvp As KeyValuePair(Of Integer, EventRecord) In dEventRecords
        '        Dim cleanedUpFormatDescription As String = kvp.Value.FormatDescription

        '        If cleanedUpFormatDescription Is Nothing Then
        '            cleanedUpFormatDescription = ""
        '        End If

        '        cleanedUpFormatDescription = cleanedUpFormatDescription.Replace(",", "-")
        '        cleanedUpFormatDescription = cleanedUpFormatDescription.Replace(vbCrLf, " ")

        '        Dim result As String = String.Format("{0},{1},{2},{3}", kvp.Key.ToString, kvp.Value.LevelDisplayName, kvp.Value.ProviderName, cleanedUpFormatDescription)
        '        fileOut.WriteLine(result)
        '    Next

        '    fileOut.Close()
        'End Using
    End Sub
End Module
