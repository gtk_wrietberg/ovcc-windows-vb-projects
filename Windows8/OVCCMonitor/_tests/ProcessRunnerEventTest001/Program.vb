Imports System
Imports System.Runtime.InteropServices
Imports System.Threading

Module Program
    Private mProcessRunner As ProcessRunner

    Private mThreading_LogZipper As Threading.Thread

    Sub Main(args As String())
        Console.WriteLine("Hello World!")


        mThreading_LogZipper = New Threading.Thread(New Threading.ThreadStart(AddressOf Piemel))
        mThreading_LogZipper.Start()


        Console.WriteLine("waiting....")

        Threading.Thread.Sleep(5000)

        Console.WriteLine("main thread done")

    End Sub

    Sub Piemel()
        ExternalApp.Run("C:\Windows\System32\notepad.exe", "")
    End Sub

    Public Class ExternalApp
        Private Shared mProcessRunner As ProcessRunner
        Private Shared mThreading_RunExternalApp As Threading.Thread

        Public Shared Sub Run(AppPath As String, AppParams As String)
            mProcessRunner = New ProcessRunner

            AddHandler mProcessRunner.Started, AddressOf ProcessRunner_Event__Started
            AddHandler mProcessRunner.Busy, AddressOf ProcessRunner_Event__Busy
            AddHandler mProcessRunner.TimeOut, AddressOf ProcessRunner_Event__Timeout
            AddHandler mProcessRunner.Done, AddressOf ProcessRunner_Event__Done

            mProcessRunner.FileName = AppPath
            mProcessRunner.Arguments = AppParams

            mProcessRunner.StartProcess()
        End Sub

        Private Shared Sub ProcessRunner_Event__Started(_p As Process, _t As Double)
            Console.WriteLine(_p.ProcessName & " started!")
        End Sub

        Private Shared Sub ProcessRunner_Event__Busy(_p As Process, _t As Double)
            Console.WriteLine(_p.ProcessName & " busy: " & _t.ToString)
        End Sub

        Private Shared Sub ProcessRunner_Event__Timeout(_p As Process, _t As Double)
            Console.WriteLine(_p.ProcessName & " timeout!")
        End Sub

        Private Shared Sub ProcessRunner_Event__Done(_p As Process, _t As Double)
            Console.WriteLine(_p.ProcessName & " done!")
        End Sub
    End Class
End Module
