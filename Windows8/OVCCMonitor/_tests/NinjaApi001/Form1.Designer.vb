﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Button1 = New Button()
        txt__code_verifier = New TextBox()
        Label1 = New Label()
        Label2 = New Label()
        txt__code_challenge = New TextBox()
        txt__request_url = New TextBox()
        Label3 = New Label()
        SuspendLayout()
        ' 
        ' Button1
        ' 
        Button1.Location = New Point(12, 12)
        Button1.Name = "Button1"
        Button1.Size = New Size(209, 23)
        Button1.TabIndex = 0
        Button1.Text = "Button1"
        Button1.UseVisualStyleBackColor = True
        ' 
        ' txt__code_verifier
        ' 
        txt__code_verifier.BackColor = Color.White
        txt__code_verifier.Location = New Point(12, 80)
        txt__code_verifier.Name = "txt__code_verifier"
        txt__code_verifier.ReadOnly = True
        txt__code_verifier.Size = New Size(516, 23)
        txt__code_verifier.TabIndex = 1
        ' 
        ' Label1
        ' 
        Label1.AutoSize = True
        Label1.Location = New Point(12, 62)
        Label1.Name = "Label1"
        Label1.Size = New Size(72, 15)
        Label1.TabIndex = 2
        Label1.Text = "code verifier"
        ' 
        ' Label2
        ' 
        Label2.AutoSize = True
        Label2.Location = New Point(12, 123)
        Label2.Name = "Label2"
        Label2.Size = New Size(87, 15)
        Label2.TabIndex = 4
        Label2.Text = "code challenge"
        ' 
        ' txt__code_challenge
        ' 
        txt__code_challenge.BackColor = Color.White
        txt__code_challenge.Location = New Point(12, 141)
        txt__code_challenge.Name = "txt__code_challenge"
        txt__code_challenge.ReadOnly = True
        txt__code_challenge.Size = New Size(516, 23)
        txt__code_challenge.TabIndex = 3
        ' 
        ' txt__request_url
        ' 
        txt__request_url.BackColor = Color.White
        txt__request_url.Location = New Point(12, 206)
        txt__request_url.Name = "txt__request_url"
        txt__request_url.ReadOnly = True
        txt__request_url.Size = New Size(1256, 23)
        txt__request_url.TabIndex = 5
        ' 
        ' Label3
        ' 
        Label3.AutoSize = True
        Label3.Location = New Point(12, 188)
        Label3.Name = "Label3"
        Label3.Size = New Size(63, 15)
        Label3.TabIndex = 6
        Label3.Text = "request url"
        ' 
        ' Form1
        ' 
        AutoScaleDimensions = New SizeF(7F, 15F)
        AutoScaleMode = AutoScaleMode.Font
        ClientSize = New Size(1280, 450)
        Controls.Add(Label3)
        Controls.Add(txt__request_url)
        Controls.Add(Label2)
        Controls.Add(txt__code_challenge)
        Controls.Add(Label1)
        Controls.Add(txt__code_verifier)
        Controls.Add(Button1)
        Name = "Form1"
        Text = "Form1"
        ResumeLayout(False)
        PerformLayout()
    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents txt__code_verifier As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txt__code_challenge As TextBox
    Friend WithEvents txt__request_url As TextBox
    Friend WithEvents Label3 As Label
End Class
