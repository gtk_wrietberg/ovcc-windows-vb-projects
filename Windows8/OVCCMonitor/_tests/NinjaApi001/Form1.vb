﻿Imports System.Formats.Asn1.AsnWriter
Imports System.IO
Imports System.Net
Imports System.Net.Http
Imports System.Net.WebRequestMethods
Imports System.Security
Imports System.Security.Cryptography
Imports System.Security.Policy
Imports System.Text
Imports System.Web
Imports System.Windows.Forms.AxHost



Public Class Form1
    Private ReadOnly mClientId As String = "Qq6DWgzu9Uj7oBjtlE9zWxIfbBE"
    Private ReadOnly mBaseUrlAuthorise As String = "https://eu.ninjarmm.com/ws/oauth/authorize"

    Private mCodeVerifier As String = ""
    Private mCodeChallenge As String = ""

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        mCodeVerifier = Generate__Code_Verifier()
        mCodeChallenge = Generate__Code_Challenge()

        txt__code_verifier.Text = mCodeVerifier
        txt__code_challenge.Text = mCodeChallenge

        sendAuthorisation()
    End Sub

    Private Function Generate__Code_Verifier() As String
        Return Base64UrlEncode(RandomNumberGenerator.GetBytes(32))
    End Function

    Private Function Base64UrlEncode(inputBytes As Byte()) As String
        Return Convert.ToBase64String(inputBytes).Replace("+", "-").Replace("/", "_").Replace("=", "")
    End Function

    Private Function Generate__Code_Challenge() As String
        Dim sha256 As SHA256 = SHA256.Create()

        Return Base64UrlEncode(sha256.ComputeHash(Encoding.Unicode.GetBytes(mCodeVerifier)))
    End Function

    Private Sub sendAuthorisation()

        'https://{app|eu|oc}.ninjarmm.com/ws/oauth/authorize?
        '    response_type = code&
        '        client_id = YOUR_CLIENT_ID&
        '        redirect_uri = https : //YOUR_APP_REDIRECT_URL&
        '    scope = SCOPE&
        '        code_challenge = CODE_CHALLENGE&
        '        code_challenge_method = S256&
        '        state = STATE

        Dim httpClient As HttpClient = New HttpClient
        Dim httpRequest As HttpRequestMessage = New HttpRequestMessage

        Dim httpOptions As New HttpRequestOptions

        Dim uriBuilder As New UriBuilder(mBaseUrlAuthorise)
        Dim uriQuery As Specialized.NameValueCollection = HttpUtility.ParseQueryString(uriBuilder.Query)
        uriQuery.Add("response_type", "code")
        uriQuery.Add("client_id", mClientId)
        uriQuery.Add("redirect_uri", "http://127.0.0.1")
        uriQuery.Add("scope", "monitoring")
        uriQuery.Add("code_challenge", mCodeChallenge)
        uriQuery.Add("code_challenge_method", "S256")
        uriQuery.Add("state", "OVCCMonitor")

        uriBuilder.Query = uriQuery.ToString

        httpRequest.RequestUri = uriBuilder.Uri

        txt__request_url.Text = uriBuilder.ToString

        'Dim httpResponse As HttpResponseMessage = httpClient.Send(httpRequest)

        'Dim tmpStr As String = httpResponse.Content.ReadAsStringAsync().Result

        MsgBox(uriBuilder.ToString)

        'Dim webRequest As WebRequest = WebRequest.Create(mUrlAuthorise)
        'Dim request As HttpWebRequest = CType(webRequest, HttpWebRequest)
        'request.Method = "GET"
        'request.ContentType = "application/json"

        'Dim response As HttpWebResponse = CType(request.GetResponse(), HttpWebResponse)

        'Using reader As StreamReader = New StreamReader(response.GetResponseStream())
        '    tbResult.Text = reader.ReadToEnd()
        'End Using

    End Sub
End Class
