﻿

Imports NAudio.Wave

Public Class Service1
    Private Shared mThreading_Heartbeat As Threading.Thread

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        __log("OnStart")



        mThreading_Heartbeat = New Threading.Thread(New Threading.ThreadStart(AddressOf _work___sound))
        mThreading_Heartbeat.Start()
    End Sub

    Private Shared Sub _work()
        Dim tmpCounter As Integer = 0

        Dim sFile As String = "C:\temp\t.t"

        Do
            Using sw As New IO.StreamWriter(sFile, True)
                sw.WriteLine(tmpCounter & New String("*", tmpCounter))
            End Using

            tmpCounter += 1

            Threading.Thread.Sleep(2000)
        Loop Until tmpCounter > 10
    End Sub

    Private Shared Sub _work___sound()
        Try
            __log("_work___sound0")
            Threading.Thread.Sleep(2000)
            __log("_work___sound1")



            __log("japanese_harp")


            Dim myMemStream As New IO.MemoryStream
            My.Resources.japanese_harp_shot_83bpm.CopyTo(myMemStream)
            Dim myBytes() As Byte = myMemStream.ToArray
            Dim myFile As String = IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.Temp & "japanese_harp_shot_83bpm.wav")

            __log(myFile)

            IO.File.WriteAllBytes(myFile, myBytes)

            Using audioFile As New AudioFileReader(myFile)
                Using outputDevice As New WaveOutEvent
                    outputDevice.Init(audioFile)
                    outputDevice.Play()
                    While outputDevice.PlaybackState = PlaybackState.Playing
                        Threading.Thread.Sleep(500)
                    End While
                End Using
            End Using
        Catch ex As Exception
            __log(ex.Message)
        End Try

    End Sub


    Private Shared Sub __log(msg As String)
        Dim sFile As String = "C:\temp\t.t"
        Using sw As New IO.StreamWriter(sFile, True)
            sw.WriteLine(msg)
        End Using
    End Sub

    Private Shared Sub japanese_harp()

        __log("japanese_harp")


        Dim myMemStream As New IO.MemoryStream
        My.Resources.japanese_harp_shot_83bpm.CopyTo(myMemStream)
        Dim myBytes() As Byte = myMemStream.ToArray
        Dim myFile As String = IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.Temp & "japanese_harp_shot_83bpm.wav")

        __log(myFile)

        IO.File.WriteAllBytes(myFile, myBytes)

        Using audioFile As New AudioFileReader(myFile)
            Using outputDevice As New WaveOutEvent
                outputDevice.Init(audioFile)
                outputDevice.Play()
                While outputDevice.PlaybackState = PlaybackState.Playing
                    Threading.Thread.Sleep(500)
                End While
            End Using
        End Using
    End Sub


    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
    End Sub

End Class
