﻿Imports System.Drawing.Imaging
Imports System.IO
Imports System.Text

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        OpenFileDialog1.ShowDialog(Me)

        TextBox1.Text = OpenFileDialog1.FileName
    End Sub

    Private Function ColorStringToNumber(_s As String) As Integer
        '  #C9CACA
        _s = _s.Replace("#", "")

        If _s.Length <> 6 Then
            Return -1
        End If

        Dim _s_r As String = ""
        Dim _s_g As String = ""
        Dim _s_b As String = ""

        _s_r = _s.Substring(0, 2)
        _s_g = _s.Substring(2, 2)
        _s_b = _s.Substring(4, 2)


        Dim _i_r As Integer = Convert.ToInt32(_s_r, 16)
        Dim _i_g As Integer = Convert.ToInt32(_s_g, 16)
        Dim _i_b As Integer = Convert.ToInt32(_s_b, 16)
        Dim _i_mean As Integer = Math.Round((_i_r + _i_g + _i_b) / (2.56 * 3))


        Return _i_mean

    End Function

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim _i As Image = Image.FromFile(TextBox1.Text)
        Dim _i_s As Bitmap = New Bitmap(_i, New Size(1, 1))

        Dim _sb As New StringBuilder

        _sb.Append("color: ")

        'For Each _c As Color In _p.Entries
        '    _sb.Append(_c.ToString)
        '    _sb.Append(" - ")
        'Next

        Dim _c As Color = _i_s.GetPixel(0, 0)

        _sb.Append(_c.ToString)

        _sb.Append(" - brightness: " & _c.GetBrightness())


        Dim _i_mean As Integer = Math.Round((CInt(_c.R.ToString) + CInt(_c.G.ToString) + CInt(_c.B.ToString)) / ((256 / 1000) * 3))

        _sb.Append(" - mean: " & _i_mean.ToString)



        TextBox3.Text = _sb.ToString


        '-------------------------------------
        Try
            Dim file As IO.StreamWriter
            file = My.Computer.FileSystem.OpenTextFileWriter(TextBox1.Text & ".score", False)
            file.Write(ImageScore(_i).ToString)
            file.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Function ImageScore(ByVal InputImage As Image) As Integer
        Try
            Dim _tmp_bitmap As Bitmap = New Bitmap(InputImage, New Size(1, 1))
            Dim _c As Color = _tmp_bitmap.GetPixel(0, 0)
            Dim _i_mean As Integer = Math.Round((CInt(_c.R.ToString) + CInt(_c.G.ToString) + CInt(_c.B.ToString)) / ((256 / 1000) * 3))

            Return _i_mean
        Catch ex As Exception
            Return -1
        End Try
    End Function
End Class
