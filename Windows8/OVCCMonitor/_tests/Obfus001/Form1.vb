﻿Public Class frmMain
    Private Sub btnObfuscate_Click(sender As Object, e As EventArgs) Handles btnObfuscate.Click
        txtObfuscated.Text = Helpers.XOrObfuscation_v2.Obfuscate(txtPlain.Text)
    End Sub

    Private Sub btnDeobfuscate_Click(sender As Object, e As EventArgs) Handles btnDeobfuscate.Click
        txtPlain.Text = Helpers.XOrObfuscation_v2.Deobfuscate(txtObfuscated.Text)
    End Sub
End Class
