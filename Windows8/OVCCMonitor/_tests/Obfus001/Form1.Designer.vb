﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtPlain = New System.Windows.Forms.TextBox()
        Me.btnObfuscate = New System.Windows.Forms.Button()
        Me.txtObfuscated = New System.Windows.Forms.TextBox()
        Me.btnDeobfuscate = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtPlain
        '
        Me.txtPlain.Location = New System.Drawing.Point(12, 12)
        Me.txtPlain.Multiline = True
        Me.txtPlain.Name = "txtPlain"
        Me.txtPlain.Size = New System.Drawing.Size(776, 163)
        Me.txtPlain.TabIndex = 0
        '
        'btnObfuscate
        '
        Me.btnObfuscate.Location = New System.Drawing.Point(12, 181)
        Me.btnObfuscate.Name = "btnObfuscate"
        Me.btnObfuscate.Size = New System.Drawing.Size(333, 23)
        Me.btnObfuscate.TabIndex = 1
        Me.btnObfuscate.Text = "Obfuscate"
        Me.btnObfuscate.UseVisualStyleBackColor = True
        '
        'txtObfuscated
        '
        Me.txtObfuscated.Location = New System.Drawing.Point(12, 210)
        Me.txtObfuscated.Multiline = True
        Me.txtObfuscated.Name = "txtObfuscated"
        Me.txtObfuscated.Size = New System.Drawing.Size(776, 163)
        Me.txtObfuscated.TabIndex = 2
        '
        'btnDeobfuscate
        '
        Me.btnDeobfuscate.Location = New System.Drawing.Point(455, 181)
        Me.btnDeobfuscate.Name = "btnDeobfuscate"
        Me.btnDeobfuscate.Size = New System.Drawing.Size(333, 23)
        Me.btnDeobfuscate.TabIndex = 3
        Me.btnDeobfuscate.Text = "Deobfuscate"
        Me.btnDeobfuscate.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 385)
        Me.Controls.Add(Me.btnDeobfuscate)
        Me.Controls.Add(Me.txtObfuscated)
        Me.Controls.Add(Me.btnObfuscate)
        Me.Controls.Add(Me.txtPlain)
        Me.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmMain"
        Me.ShowIcon = False
        Me.Text = "Obfuscate / Deobfuscate"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtPlain As TextBox
    Friend WithEvents btnObfuscate As Button
    Friend WithEvents txtObfuscated As TextBox
    Friend WithEvents btnDeobfuscate As Button
End Class
