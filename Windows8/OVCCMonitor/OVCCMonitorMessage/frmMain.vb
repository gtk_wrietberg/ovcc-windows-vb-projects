﻿Imports System.Globalization

Public Class frmMain
    Private Enum _buttons
        NO_BUTTONS = 0
        BUTTON_OK = 1
        BUTTON_CANCEL = 2
        BUTTON_YES = 4
        BUTTON_NO = 8
    End Enum

    Private mButtons As Integer = _buttons.NO_BUTTONS

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Forcing the service to use en-GB
        Dim culture_enGB As New CultureInfo("en-GB")
        CultureInfo.DefaultThreadCurrentCulture = culture_enGB


        Me.TransparencyKey = Color.Lime
        Me.Opacity = 0.9
        Me.Visible = False


        Dim _tmp As String = ""
        ExitCode.SetValue(ExitCode.ExitCodes.OK)

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.StartsWith("--buttons:") Then
                _tmp = arg.Replace("--buttons:", "")
                If Not Integer.TryParse(_tmp, mButtons) Then
                    mButtons = _buttons.NO_BUTTONS
                End If
            End If
        Next

        mButtons = Helpers.Registry.GetValue_Integer(Constants.RegistryKeys.KEY__Monitor_Message, Constants.RegistryKeys.VALUE__Monitor_MessageButtons, -1)
        lblTitle.Text = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Message, Constants.RegistryKeys.VALUE__Monitor_MessageTitle, "")
        txtMessage.Text = Helpers.Registry.GetValue_String(Constants.RegistryKeys.KEY__Monitor_Message, Constants.RegistryKeys.VALUE__Monitor_MessageText, "")

        If mButtons < 0 Or lblTitle.Text.Equals("") Or txtMessage.Text.Equals("") Then
            ExitCode.SetValue(ExitCode.ExitCodes.NO_MESSAGE_SET)
            ExitApplication()
        End If


        HideControls(Me)

        If (mButtons And _buttons.BUTTON_OK) Or (mButtons And _buttons.BUTTON_CANCEL) Then
            pnlButtons__OK_Cancel.Visible = True
            pnlButtons__OK_Cancel.Location = New Point(3, 167)

            If mButtons And _buttons.BUTTON_OK Then
                btnOk.Visible = True
            End If

            If mButtons And _buttons.BUTTON_CANCEL Then
                btnCancel.Visible = True
            End If
        End If

        If (mButtons And _buttons.BUTTON_YES) Or (mButtons And _buttons.BUTTON_NO) Then
            pnlButtons__Yes_No.Visible = True
            pnlButtons__Yes_No.Location = New Point(3, 167)

            pnlButtons__OK_Cancel.Visible = False

            If mButtons And _buttons.BUTTON_YES Then
                btnYes.Visible = True
            End If

            If mButtons And _buttons.BUTTON_NO Then
                btnNo.Visible = True
            End If
        End If
    End Sub

    Private Sub HideControls(_parent As Control)
        For Each _c As Control In _parent.Controls
            If _c.HasChildren Then
                HideControls(_c)
            End If

            If _c.Name.StartsWith("pnlButtons__") Or TypeOf _c Is Button Then
                _c.Visible = False
            End If
        Next
    End Sub

    Private Sub ExitApplication()
        Environment.ExitCode = ExitCode.GetValue()
        Me.Close()
    End Sub


    '----------------------------------------------------------------------------------------------------------------
    Private isMouseDown As Boolean = False
    Private mouseOffset As Point

    Private Sub picTitle_MouseDown(sender As Object, e As MouseEventArgs) Handles picTitle.MouseDown
        If e.Button = MouseButtons.Left Then
            ' Get the new position
            mouseOffset = New Point(-e.X - pnlBorder.Left, -e.Y - pnlBorder.Top)
            ' Set that left button is pressed
            isMouseDown = True
        End If
    End Sub

    Private Sub picTitle_MouseMove(sender As Object, e As MouseEventArgs) Handles picTitle.MouseMove
        If isMouseDown Then
            Dim mousePos As Point = Control.MousePosition
            ' Get the new form position
            mousePos.Offset(mouseOffset.X, mouseOffset.Y)
            Me.Location = mousePos
        End If
    End Sub

    Private Sub picTitle_MouseUp(sender As Object, e As MouseEventArgs) Handles picTitle.MouseUp
        If e.Button = MouseButtons.Left Then
            isMouseDown = False
        End If
    End Sub

    Private Sub lblTitle_MouseDown(sender As Object, e As MouseEventArgs) Handles lblTitle.MouseDown
        If e.Button = MouseButtons.Left Then
            ' Get the new position
            mouseOffset = New Point(-e.X - pnlBorder.Left, -e.Y - pnlBorder.Top)
            ' Set that left button is pressed
            isMouseDown = True
        End If
    End Sub

    Private Sub lblTitle_MouseMove(sender As Object, e As MouseEventArgs) Handles lblTitle.MouseMove
        If isMouseDown Then
            Dim mousePos As Point = Control.MousePosition
            ' Get the new form position
            mousePos.Offset(mouseOffset.X, mouseOffset.Y)
            Me.Location = mousePos
        End If
    End Sub

    Private Sub lblTitle_MouseUp(sender As Object, e As MouseEventArgs) Handles lblTitle.MouseUp
        If e.Button = MouseButtons.Left Then
            isMouseDown = False
        End If
    End Sub


    '----------------------------------------------------------------------------------------------------------------
    Private Sub picClose_Click(sender As Object, e As EventArgs) Handles picClose.Click
        ExitApplication()
    End Sub

    Private Sub picClose_MouseEnter(sender As Object, e As EventArgs) Handles picClose.MouseEnter
        picClose.Image = My.Resources.button__close_on
    End Sub

    Private Sub picClose_MouseLeave(sender As Object, e As EventArgs) Handles picClose.MouseLeave
        picClose.Image = My.Resources.button__close_off
    End Sub


    '----------------------------------------------------------------------------------------------------------------
    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        ExitCode.SetValue(ExitCode.ExitCodes.BUTTON_CLICKED__OK)
        ExitApplication()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ExitCode.SetValue(ExitCode.ExitCodes.BUTTON_CLICKED__CANCEL)
        ExitApplication()
    End Sub

    Private Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click
        ExitCode.SetValue(ExitCode.ExitCodes.BUTTON_CLICKED__YES)
        ExitApplication()
    End Sub

    Private Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        ExitCode.SetValue(ExitCode.ExitCodes.BUTTON_CLICKED__NO)
        ExitApplication()
    End Sub
End Class