﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.pnlBorder = New System.Windows.Forms.Panel()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.picClose = New System.Windows.Forms.PictureBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.picTitle = New System.Windows.Forms.PictureBox()
        Me.pnlShadow = New System.Windows.Forms.Panel()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.pnlButtons__OK_Cancel = New System.Windows.Forms.Panel()
        Me.pnlButtons__Yes_No = New System.Windows.Forms.Panel()
        Me.btnYes = New System.Windows.Forms.Button()
        Me.btnNo = New System.Windows.Forms.Button()
        Me.txtMessage = New System.Windows.Forms.TextBox()
        Me.tmrAutoClose = New System.Windows.Forms.Timer(Me.components)
        Me.pnlBorder.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picTitle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlButtons__OK_Cancel.SuspendLayout()
        Me.pnlButtons__Yes_No.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlBorder
        '
        Me.pnlBorder.BackColor = System.Drawing.Color.Red
        Me.pnlBorder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBorder.Controls.Add(Me.pnlMain)
        Me.pnlBorder.Location = New System.Drawing.Point(12, 12)
        Me.pnlBorder.Name = "pnlBorder"
        Me.pnlBorder.Size = New System.Drawing.Size(454, 210)
        Me.pnlBorder.TabIndex = 0
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.pnlButtons__Yes_No)
        Me.pnlMain.Controls.Add(Me.pnlButtons__OK_Cancel)
        Me.pnlMain.Controls.Add(Me.txtMessage)
        Me.pnlMain.Controls.Add(Me.picClose)
        Me.pnlMain.Controls.Add(Me.lblTitle)
        Me.pnlMain.Controls.Add(Me.picTitle)
        Me.pnlMain.Location = New System.Drawing.Point(3, 3)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(445, 202)
        Me.pnlMain.TabIndex = 1
        '
        'picClose
        '
        Me.picClose.Image = Global.OVCCMonitorMessage.My.Resources.Resources.button__close_off
        Me.picClose.Location = New System.Drawing.Point(400, 0)
        Me.picClose.Name = "picClose"
        Me.picClose.Size = New System.Drawing.Size(45, 40)
        Me.picClose.TabIndex = 3
        Me.picClose.TabStop = False
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(227, Byte), Integer), CType(CType(233, Byte), Integer))
        Me.lblTitle.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(0, 3)
        Me.lblTitle.Margin = New System.Windows.Forms.Padding(0)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(400, 34)
        Me.lblTitle.TabIndex = 3
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picTitle
        '
        Me.picTitle.Image = CType(resources.GetObject("picTitle.Image"), System.Drawing.Image)
        Me.picTitle.Location = New System.Drawing.Point(0, 0)
        Me.picTitle.Name = "picTitle"
        Me.picTitle.Size = New System.Drawing.Size(400, 40)
        Me.picTitle.TabIndex = 3
        Me.picTitle.TabStop = False
        '
        'pnlShadow
        '
        Me.pnlShadow.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.pnlShadow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlShadow.Location = New System.Drawing.Point(16, 16)
        Me.pnlShadow.Name = "pnlShadow"
        Me.pnlShadow.Size = New System.Drawing.Size(454, 210)
        Me.pnlShadow.TabIndex = 2
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(4, 3)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(130, 24)
        Me.btnOk.TabIndex = 4
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(304, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(130, 24)
        Me.btnCancel.TabIndex = 5
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'pnlButtons__OK_Cancel
        '
        Me.pnlButtons__OK_Cancel.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlButtons__OK_Cancel.Controls.Add(Me.btnOk)
        Me.pnlButtons__OK_Cancel.Controls.Add(Me.btnCancel)
        Me.pnlButtons__OK_Cancel.Location = New System.Drawing.Point(3, 167)
        Me.pnlButtons__OK_Cancel.Name = "pnlButtons__OK_Cancel"
        Me.pnlButtons__OK_Cancel.Size = New System.Drawing.Size(437, 30)
        Me.pnlButtons__OK_Cancel.TabIndex = 3
        '
        'pnlButtons__Yes_No
        '
        Me.pnlButtons__Yes_No.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlButtons__Yes_No.Controls.Add(Me.btnYes)
        Me.pnlButtons__Yes_No.Controls.Add(Me.btnNo)
        Me.pnlButtons__Yes_No.Location = New System.Drawing.Point(3, 85)
        Me.pnlButtons__Yes_No.Name = "pnlButtons__Yes_No"
        Me.pnlButtons__Yes_No.Size = New System.Drawing.Size(437, 30)
        Me.pnlButtons__Yes_No.TabIndex = 6
        '
        'btnYes
        '
        Me.btnYes.Location = New System.Drawing.Point(4, 3)
        Me.btnYes.Name = "btnYes"
        Me.btnYes.Size = New System.Drawing.Size(130, 24)
        Me.btnYes.TabIndex = 4
        Me.btnYes.Text = "Yes"
        Me.btnYes.UseVisualStyleBackColor = True
        '
        'btnNo
        '
        Me.btnNo.Location = New System.Drawing.Point(304, 3)
        Me.btnNo.Name = "btnNo"
        Me.btnNo.Size = New System.Drawing.Size(130, 24)
        Me.btnNo.TabIndex = 5
        Me.btnNo.Text = "No"
        Me.btnNo.UseVisualStyleBackColor = True
        '
        'txtMessage
        '
        Me.txtMessage.BackColor = System.Drawing.Color.WhiteSmoke
        Me.txtMessage.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtMessage.Enabled = False
        Me.txtMessage.Location = New System.Drawing.Point(3, 46)
        Me.txtMessage.Multiline = True
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.ReadOnly = True
        Me.txtMessage.Size = New System.Drawing.Size(437, 115)
        Me.txtMessage.TabIndex = 3
        '
        'tmrAutoClose
        '
        Me.tmrAutoClose.Interval = 1000
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.ClientSize = New System.Drawing.Size(481, 239)
        Me.Controls.Add(Me.pnlBorder)
        Me.Controls.Add(Me.pnlShadow)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMain"
        Me.pnlBorder.ResumeLayout(False)
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        CType(Me.picClose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picTitle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlButtons__OK_Cancel.ResumeLayout(False)
        Me.pnlButtons__Yes_No.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlBorder As Panel
    Friend WithEvents pnlMain As Panel
    Friend WithEvents pnlShadow As Panel
    Friend WithEvents lblTitle As Label
    Friend WithEvents picClose As PictureBox
    Friend WithEvents picTitle As PictureBox
    Friend WithEvents btnOk As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents pnlButtons__OK_Cancel As Panel
    Friend WithEvents pnlButtons__Yes_No As Panel
    Friend WithEvents btnYes As Button
    Friend WithEvents btnNo As Button
    Friend WithEvents txtMessage As TextBox
    Friend WithEvents tmrAutoClose As Timer
End Class
