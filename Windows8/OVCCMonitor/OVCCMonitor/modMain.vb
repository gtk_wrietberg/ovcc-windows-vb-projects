﻿
Imports System.Globalization
Imports System.Runtime.InteropServices
Imports System.Security.Cryptography
Imports System.Text


Module modMain
    Enum Format_Message
        FORMAT_MESSAGE_IGNORE_INSERTS = &H200
        FORMAT_MESSAGE_FROM_SYSTEM = &H1000
        FORMAT_MESSAGE_FROM_HMODULE = &H800
    End Enum


    <DllImport("Kernel32", SetLastError:=True, CharSet:=CharSet.Unicode)>
    Public Function FormatMessage(ByVal dwFlags As Format_Message, ByVal lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, lpBuffer As StringBuilder, ByVal nSize As Integer, ByVal Arguments As IntPtr) As Integer
    End Function


    Public Sub Main()
        'Forcing the service to use en-GB
        ApplicationCultureInfo.SetApplicationCultureInfoToEnglishBritish()


        Settings.Application.Load_Debugging()
        Logger.Debugging = Settings.Application.[Private].Debugging


        Globals.TESTING = True
        Globals.EchoLoggerMessagesInConsole = True
        Logger.Debugging = True


        Logger.Initialise()
        Logger.Message(New String("*", 50), 0)
        Logger.Message(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)
        Logger.Message("service started", 0)


        Identifiers.Generate()


        Settings.Application.Load_Privates()
        Settings.Application.Load_Publics()

        Settings.OVCC.Load()


        Configuration.MakeSureFoldersExist()


        Integrity.FilesFolders.Initialise()


        OVCCThemeMatcher.Initialise()


        If Settings.Application.Private.Debugging Then
            Settings.Application.Public.ThreadDelay_Heartbeat = 1
        End If



        Settings.Application.Public.ThreadDelay_Heartbeat = 1
        'Settings.Application.Public.ScreenshotEnabled = False
        Globals.MonitorUpdateEnabled = False
        Globals.EchoLoggerMessagesInConsole = True
        Globals.SkipScreenshotWhenRunningManually = True
        Logger.Debugging = True



        'Dim sErrorMessage As String = ""
        'Dim _l As List(Of String) = Helpers.Registry.GetValuesRecursive("HKEY_USERS\S-1-5-21-1830506630-444973163-401143199-1001\Software\Microsoft\Windows\CurrentVersion\Policies", sErrorMessage)

        'MsgBox(String.Join(vbCrLf & "-" & vbCrLf, _l.ToArray))
        '_l.RemoveAll(Function(str) String.IsNullOrWhiteSpace(str))
        'MsgBox(String.Join(vbCrLf & "-" & vbCrLf, _l.ToArray))

        'MsgBox(Inventory.Policies.SiteKioskUser.PolicyScore().ToString)

        'Exit Sub
        'MsgBox(Settings.Application.Private.Server.BaseUrl)

        'Exit Sub


        'Dim stmp As String = ""
        'stmp = Helpers.Registry.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography", "MachineGuid")

        'MsgBox("MachineGuid (32): " & stmp)


        'stmp = Helpers.Registry64.GetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Cryptography", "MachineGuid")

        'MsgBox("MachineGuid (64): " & stmp)

        'MsgBox(Globals.MachineIdentifier)
        'MsgBox(Globals.MonitorIdentifier)


        'MsgBox(Globals.SystemCultureInfo.EnglishName)
        'MsgBox(CultureInfo.CurrentCulture.EnglishName)


        'Actions.ACTION_Integrity()

        'Exit Sub


        'Dim sToken As String = "bb8acc31702c73"




        'Dim myIPInfoBuilder As New IPinfoClient.Builder
        'myIPInfoBuilder.AccessToken(sToken)

        'Dim myIpInfoClient As IPinfoClient = myIPInfoBuilder.Build()

        'Dim myIpInfoResponse As New IPResponse
        'myIpInfoResponse = myIpInfoClient.IPApi.GetDetails()

        'MsgBox(myIpInfoResponse.City)

        'Exit Sub

        'Dim ipInfo As New IPinfoClient.Builder
        'ipInfo.AccessToken(sToken)



        'Globals.MachineId = 122
        'Actions.ACTION_Inventory()
        'Exit Sub


        'Globals.MachineId = 122
        'Actions.ACTION_Inventory()
        'Exit Sub

        'Dim JSON_Heartbeat As New Communication.JSON()

        'JSON_Heartbeat.Request.Add("test", "test123")



        'Dim nics As List(Of NetInterfacesInfo.NetWorkInterfacesInfoShort) = NetInterfacesInfo.GetConnectedNics()

        'JSON_Heartbeat.Request.Add("network_details", Newtonsoft.Json.JsonConvert.SerializeObject(nics))

        'MsgBox(JSON_Heartbeat.Request.ToString)

        'Dim sb As New StringBuilder

        'For Each nic As NetInterfacesInfo.NetWorkInterfacesInfo In nics
        '    If nic.Status = Net.NetworkInformation.OperationalStatus.Up Then
        '        sb.Append(nic.ConnectionName)
        '        sb.Append(vbCrLf)
        '        sb.Append(vbTab & nic.MacAddress)
        '        sb.Append(vbCrLf)
        '        If nic.IPV4Addresses.Count > 0 Then
        '            For i As Integer = 0 To nic.IPV4Addresses.Count - 1
        '                sb.Append(vbTab & vbTab & nic.IPV4Addresses.Item(i).IpAddress)
        '                sb.Append(vbCrLf)
        '            Next
        '        End If

        '        sb.Append(vbCrLf)
        '    End If
        'Next

        'Exit Sub


        'If Screenshot.Take() Then
        '    MsgBox(Screenshot.Request.FileName)
        'Else
        '    MsgBox(Screenshot.LastError)
        'End If



        'Dim s As String = "poep"

        'MsgBox(GetHash(s).ToString)
        'MsgBox(GetHexString(GetHash(s)))


        'MsgBox("Chipset: " & Metrics.SystemInformation.Hardware.Chipset())

        'MsgBox(Settings.Application.[Private].Server.BaseUrl)
        'Exit Sub


        'MsgBox(Settings.Application.Public.LastGeolocationSyncDelay.ToString)
        'Exit Sub



        'Commands.DisableMachine.ForceDisabledTheme()
        'Commands.DisableMachine.RestoreOriginalTheme()
        'Exit Sub


        'Dim sApp As String = ""
        'Dim _sb As New StringBuilder

        'Configuration.SkCfg.XML.Lock()
        'Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
        'Configuration.SkCfg.XML.Load()
        ''Configuration.SkCfg.XML.SetValue(Constants.SkCfg.ScreensaverUrl, sScreensaverIndexFile)
        'If Configuration.SkCfg.XML.GetFirstValue(Constants.SkCfg.Applications, Constants.SkCfg.Applications_Attrib_filename, sApp) Then
        '    _sb.Append("First item: " & sApp)
        '    _sb.Append(vbCrLf)

        '    Do While Configuration.SkCfg.XML.GetNextValue(Constants.SkCfg.Applications, Constants.SkCfg.Applications_Attrib_filename, sApp)
        '        _sb.Append("next item: " & sApp)
        '        _sb.Append(vbCrLf)
        '    Loop
        'End If
        'Configuration.SkCfg.XML.Unlock()

        'MsgBox(Configuration.SkCfg.XML.LastError)

        'MsgBox(_sb.ToString)


        'Dim _apps As Integer, _ok As Integer

        'If Integrity.OVCC.Applications.CheckForUnlinkedApplications(_apps, _ok) Then
        '    MsgBox("apps found: " & _apps.ToString & " - ok: " & _ok.ToString)
        'Else
        '    MsgBox("oops")
        'End If



        'Dim sTest As String = ""


        'sTest = "C:/Program Files (x86)/SiteKiosk/Skins/Public/Startpages/Towneplace-Suites/index.html"
        'MsgBox(sTest & ": " & OVCCThemeMatcher.FindIndex(sTest))
        'sTest = "C:/Program Files (x86)/SiteKiosk/Skins/Public/Startpages/Ritz-Carlton/index.html"
        'MsgBox(sTest & ": " & OVCCThemeMatcher.FindIndex(sTest))
        'sTest = "C:/Program Files (x86)/SiteKiosk/Skins/Public/Startpages/Marriott/index_Sheraton.html"
        'MsgBox(sTest & ": " & OVCCThemeMatcher.FindIndex(sTest))
        'sTest = "C:/Program Files (x86)/SiteKiosk/Skins/Public/Startpages/JW-Marriott/index.html"
        'MsgBox(sTest & ": " & OVCCThemeMatcher.FindIndex(sTest))
        'sTest = "C:/Program Files (x86)/SiteKiosk/Skins/Public/Startpages/Marriott/index.html"
        'MsgBox(sTest & ": " & OVCCThemeMatcher.FindIndex(sTest))
        'sTest = "C:/Program Files (x86)/SiteKiosk/Skins/Public/Themes/123/index.html"
        'MsgBox(sTest & ": " & OVCCThemeMatcher.FindIndex(sTest))





        'MsgBox(Inventory.Themes.Index)
        'Commands.DisableMachine.ForceDisabledTheme()
        'MsgBox(Inventory.Themes.Index)
        'Commands.DisableMachine.RestoreOriginalTheme()
        'MsgBox(Inventory.Themes.Index)




        'Dim sThemeIndexFile As String = ""
        'Configuration.SkCfg.XML.Lock()
        'Configuration.SkCfg.XML.FilePath = Helpers.SiteKiosk.GetSiteKioskActiveConfig
        'Configuration.SkCfg.XML.Load()
        'Configuration.SkCfg.XML.GetValue(Constants.SkCfg.ThemeUrl, sThemeIndexFile)
        'Configuration.SkCfg.XML.Unlock()


        'MsgBox(sThemeIndexFile)

        'Dim iThemeIndex As Integer = OVCCThemeMatcher.FindIndex(sThemeIndexFile)


        'MsgBox(iThemeIndex.ToString)


        Actions.ACTION_FirstRunAfterUpdate()


        Exit Sub



        'Start Memory/Cpu thread
        Actions.MemoryCpuThread.Start()

        'Start logger zipper thread
        Actions.LoggerZipperThread.Start()

        'Start heartbeat thread
        Actions.HeartbeatThread.Start()
    End Sub



    Private Function GetHash(ByVal data As String) As Byte()
        Using SHA As New SHA1CryptoServiceProvider()
            Dim hash As Byte() = SHA.ComputeHash(Encoding.Default.GetBytes(data))

            Return hash

            'Return GetHexString(hash)
        End Using
    End Function

    Private Function GetHexString(ByVal bt As Byte()) As String
        Dim sBuilder = New StringBuilder()
        For i As Integer = 0 To bt.Count - 1
            Dim b = bt(i)
            Dim n = b
            Dim n1 = n And 15
            Dim n2 = (n >> 4) And 15
            If n2 > 9 Then
                sBuilder.Append(ChrW((n2 - 10 + AscW("A"c))).ToString(CultureInfo.InvariantCulture))
            Else
                sBuilder.Append(n2.ToString(CultureInfo.InvariantCulture))
            End If
            If n1 > 9 Then
                sBuilder.Append(ChrW((n1 - 10 + AscW("A"c))).ToString(CultureInfo.InvariantCulture))
            Else
                sBuilder.Append(n1.ToString(CultureInfo.InvariantCulture))
            End If
            If (i + 1) <> bt.Count AndAlso (i + 1) Mod 2 = 0 Then
                sBuilder.Append("-")
            End If
        Next
        Return sBuilder.ToString()
    End Function
End Module
