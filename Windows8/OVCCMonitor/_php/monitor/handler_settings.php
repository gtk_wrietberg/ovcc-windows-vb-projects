<?php
require 'inc/inc.php';

error_reporting(E_ALL);
ini_set('display_errors',1);
ini_set('display_startup_errors',1);

try {
	$conn=new PDO("mysql:host=$db_servername;dbname=$db_dbname",$db_username,$db_password);
	$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo "Connection failed: ".$e->getMessage();
	die();
}


$action=$_REQUEST['action'];

if($action=="save") {
	$setting_id=$_REQUEST['setting_id'];
	$new_value=$_REQUEST['new_value'];
	$old_value=$_REQUEST['old_value'];
	$machine_id=$_REQUEST['machine_id'];
	$last_ovccsettings_sync=time();
	
	$sql="SELECT `name` FROM `tblOVCCSettings` WHERE `id`=:setting_id";
	$stmt=$conn->prepare($sql);
	$stmt->execute(['setting_id'=>$setting_id]);
	
	if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		$setting_name=$row['name'];
	} else {
		header("Location: index.php");
		die();
	}
	
	
	$sql="SELECT `id` FROM `tblOVCCSettings` WHERE `name`=:setting_name AND `machine_id`=:machine_id";
	$stmt=$conn->prepare($sql);
	$stmt->execute(['setting_name'=>$setting_name,'machine_id'=>$machine_id]);
	
	if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		$sql ="UPDATE `tblOVCCSettings` SET `value`=:new_value WHERE `name`=:setting_name AND `machine_id`=:machine_id";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['new_value'=>$new_value,'setting_name'=>$setting_name,'machine_id'=>$machine_id]);
	} else {
		$sql ="INSERT INTO `tblOVCCSettings` (`machine_id`,`name`,`value`) VALUES (:machine_id,:setting_name,:new_value)";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['new_value'=>$new_value,'setting_name'=>$setting_name,'machine_id'=>$machine_id]);
	}
	
	
	if($machine_id>0) {
		$sql="UPDATE `tblMachines` SET `last_ovccsettings_sync`=:last_ovccsettings_sync WHERE `id`=:machine_id";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['last_ovccsettings_sync'=>$last_ovccsettings_sync,'machine_id'=>$machine_id]);
	
		header("Location: machine.php?id=".$machine_id);
	} else {
		$sql="UPDATE `tblMachines` SET `last_ovccsettings_sync`=:last_ovccsettings_sync";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['last_ovccsettings_sync'=>$last_ovccsettings_sync]);
		
		header("Location: settings.php");
	}
	
	die();
}

if($action=="to_default") {
	$setting_id=$_REQUEST['setting_id'];
	$machine_id=$_REQUEST['machine_id'];
	$last_ovccsettings_sync=time();
	
	$sql="DELETE FROM `tblOVCCSettings` WHERE `id`=:setting_id AND `machine_id`=:machine_id";
	$stmt=$conn->prepare($sql);
	$stmt->execute(['setting_id'=>$setting_id,'machine_id'=>$machine_id]);
	
	
	$sql="UPDATE `tblMachines` SET `last_ovccsettings_sync`=:last_ovccsettings_sync WHERE `id`=:machine_id";
	$stmt=$conn->prepare($sql);
	$stmt->execute(['last_ovccsettings_sync'=>$last_ovccsettings_sync,'machine_id'=>$machine_id]);
	
	
	header("Location: machine.php?id=".$machine_id);
	die();
}


header("Location: index.php");
die();
?>