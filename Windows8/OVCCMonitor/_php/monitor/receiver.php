<?php
require 'inc/inc.php';

date_default_timezone_set('UTC');

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


/*
if($_SERVER['HTTP_USER_AGENT']!="OVCCMonitor") {
	$protocol=(isset($_SERVER['SERVER_PROTOCOL'])?$_SERVER['SERVER_PROTOCOL']:'HTTP/1.0');
	header($protocol.' 418 I\'m a teapot');
	die();	
}
*/




/*
$data_request_teststring='{
  "action": "metrics",
  "machine_id": "7",
  "year": "2023",
  "month": "5",
  "day": "21",
  "emergency_states": "1",
  "urls_visited": "97",
  "user_sessions": "2",
  "sitekiosk_crashes": "1",
  "screensaver": "0"
}';
$data_request=json_decode($data_request_teststring,true);
*/
$data_request=json_decode(file_get_contents('php://input'),true);
$data_response=array();


if(isset($data_request['action'])) {
	try {
		$conn=new PDO("mysql:host=$db_servername;dbname=$db_dbname",$db_username,$db_password);
		$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		echo "Connection failed: ".$e->getMessage();
		die();
	}

	if($data_request['action']=="heartbeat") {
		$message="updated";
		$timestamp=time();
		$machine_id=0;
		
		if(array_key_exists("machine_name_previous",$data_request)) {
			$machine_name=$data_request['machine_name_previous'];
			$machine_name_new=$data_request['machine_name'];
		} else {
			$machine_name=$data_request['machine_name'];
			$machine_name_new="";
		}
		$current_user=isset($data_request['current_user'])?$data_request['current_user']:'0';;
		$usage_state=isset($data_request['usage_state'])?$data_request['usage_state']:'0';;
		$last_usage_in_seconds=isset($data_request['last_usage_in_seconds'])?$data_request['last_usage_in_seconds']:'0';;

		
		$sql="SELECT `id`,`last_appsettings_sync`,`last_ovccsettings_sync`,`last_metrics_sync`,`last_logfile_received` FROM `tblMachines` WHERE `machine_name`=:machine_name";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['machine_name'=>$machine_name]);
		if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$machine_id=$row["id"];
			$last_appsettings_sync=$row['last_appsettings_sync'];
			$last_ovccsettings_sync=$row['last_ovccsettings_sync'];
			$last_metrics_sync=$row['last_metrics_sync'];
			$last_logfile_received=$row['last_logfile_received'];
			
			$sql="INSERT INTO `tblHeartbeat` (`machine_id`,`timestamp`) VALUES (:machine_id,:timestamp) ON DUPLICATE KEY UPDATE timestamp=:timestamp";
			$stmt=$conn->prepare($sql);
			$stmt->execute(['machine_id'=>$machine_id,'timestamp'=>$timestamp]);
			
			if($machine_name_new!="") {
				$sql="UPDATE `tblMachines` SET `machine_name`=:machine_name WHERE `id`=:machine_id";
				$stmt=$conn->prepare($sql);
				$stmt->execute(['machine_id'=>$machine_id,'machine_name'=>$machine_name_new]);
				
				$message="renamed";
			}
		} else {
			$sql="INSERT INTO `tblMachines` (`machine_name`) VALUES (:machine_name)";
			$stmt=$conn->prepare($sql);
			$stmt->execute(['machine_name'=>$machine_name]);
			$machine_id=$conn->lastInsertId();
			$last_appsettings_sync=0;
			$last_ovccsettings_sync=0;
			$last_metrics_sync=0;
			$last_logfile_received=0;
			
			$sql="INSERT INTO `tblHeartbeat` (`machine_id`,`timestamp`) VALUES (:machine_id,:timestamp)";
			$stmt=$conn->prepare($sql);
			$stmt->execute(['machine_id'=>$machine_id,'timestamp'=>$timestamp]);
			
			$message="inserted";
		}
		
		//Quick Metrics
		$sql ="INSERT INTO `tblQuickMetrics` (`machine_id`,`current_user`,`usage_state`,`last_usage_in_seconds`) VALUES (:machine_id,:current_user,:usage_state,:last_usage_in_seconds) ON DUPLICATE KEY ";
		$sql.="UPDATE `current_user`=:current_user,`usage_state`=:usage_state,`last_usage_in_seconds`=:last_usage_in_seconds";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['machine_id'=>$machine_id,'current_user'=>$current_user,'usage_state'=>$usage_state,'last_usage_in_seconds'=>$last_usage_in_seconds]);
		
		
		$data_response["code"]=0;
		$data_response["message"]=$message;
		$data_response["machine_id"]=$machine_id;
		$data_response["last_heartbeat"]=$timestamp;
		$data_response["last_appsettings_sync"]=$last_appsettings_sync;
		$data_response["last_ovccsettings_sync"]=$last_ovccsettings_sync;
		$data_response["last_metrics_sync"]=$last_metrics_sync;
		$data_response["last_logfile_received"]=$last_logfile_received;
	}


	if($data_request['action']=="app_settings_sync") {
		$machine_id=$data_request['machine_id'];
		$message="ok";
		
		$sql="SELECT `name`,`value` FROM `tblAppSettings` WHERE (`machine_id`=:machine_id OR `machine_id`=0) ORDER BY `machine_id` ASC,`settingsgroup_id` ASC";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['machine_id'=>$machine_id]);
		
		$data_response["app_settings"]=array();
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$data_response["app_settings"][$row['name']]=$row['value'];
		}
		
		$data_response["code"]=0;
		$data_response["message"]=$message;
	}


	if($data_request['action']=="ovcc_settings_sync") {
		$machine_id=$data_request['machine_id'];
		$message="ok";
		
		
		$sql="SELECT `name`,`value` FROM `tblOVCCSettings` WHERE (`machine_id`=:machine_id OR `machine_id`=0) ORDER BY `machine_id` ASC,`settingsgroup_id` ASC";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['machine_id'=>$machine_id]);
		
		$data_response["ovcc_settings"]=array();
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$data_response["ovcc_settings"][$row['name']]=$row['value'];
		}
		
		
		$sql="SELECT `theme_index`,`name`,`internal_name`,`version` FROM `tblOVCCThemes` ORDER BY `name` ASC";
		$stmt=$conn->prepare($sql);
		$stmt->execute();
		
		$data_response["ovcc_themes"]=array();
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$theme_tmp_arr=array();
			$theme_tmp_arr['ThemeName']=$row['name'];
			$theme_tmp_arr['ThemeInternalName']=$row['internal_name'];
			$theme_tmp_arr['ThemeVersion']=$row['version'];
			
			$data_response["ovcc_themes"][$row['theme_index']]=$theme_tmp_arr;
		}
		
		
		$sql="SELECT `screensaver_index`,`name`,`internal_name`,`version` FROM `tblOVCCScreensavers` ORDER BY `name` ASC";
		$stmt=$conn->prepare($sql);
		$stmt->execute();
		
		$data_response["ovcc_screensavers"]=array();
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			$screensaver_tmp_arr=array();
			$screensaver_tmp_arr['ScreensaverName']=$row['name'];
			$screensaver_tmp_arr['ScreensaverInternalName']=$row['internal_name'];
			$screensaver_tmp_arr['ScreensaverVersion']=$row['version'];
			
			$data_response["ovcc_screensavers"][$row['screensaver_index']]=$screensaver_tmp_arr;
		}
		
		
		$data_response["code"]=0;
		$data_response["message"]=$message;
	}


	if($data_request['action']=="inventory") {
		$machine_id=isset($data_request['machine_id'])?$data_request['machine_id']:'0';
		$message="ok";
		
		$fileversion_monitor=isset($data_request['fileversion_monitor'])?$data_request['fileversion_monitor']:'';
		$filerunning_monitor=isset($data_request['filerunning_monitor'])?$data_request['filerunning_monitor']:'';
		$fileversion_sitekiosk=isset($data_request['fileversion_sitekiosk'])?$data_request['fileversion_sitekiosk']:'';
		$filerunning_sitekiosk=isset($data_request['filerunning_sitekiosk'])?$data_request['filerunning_sitekiosk']:'';
		$fileversion_watchdog=isset($data_request['fileversion_watchdog'])?$data_request['fileversion_watchdog']:'';
		$filerunning_watchdog=isset($data_request['filerunning_watchdog'])?$data_request['filerunning_watchdog']:'';
		$fileversion_fileexplorer=isset($data_request['fileversion_fileexplorer'])?$data_request['fileversion_fileexplorer']:'';
		$filerunning_fileexplorer=isset($data_request['filerunning_fileexplorer'])?$data_request['filerunning_fileexplorer']:'';
		
		$filerunning_monitor=$filerunning_monitor?1:0;
		$filerunning_sitekiosk=$filerunning_sitekiosk?1:0;
		$filerunning_watchdog=$filerunning_watchdog?1:0;
		$filerunning_fileexplorer=$filerunning_fileexplorer?1:0;

		
		$sql_array=[
			'machine_id'=>$machine_id,
			'last_update'=>time(),
			'fileversion_monitor'=>$fileversion_monitor,
			'fileversion_sitekiosk'=>$fileversion_sitekiosk,
			'fileversion_watchdog'=>$fileversion_watchdog,
			'fileversion_fileexplorer'=>$fileversion_fileexplorer,
			'filerunning_monitor'=>$filerunning_monitor,
			'filerunning_sitekiosk'=>$filerunning_sitekiosk,
			'filerunning_watchdog'=>$filerunning_watchdog,
			'filerunning_fileexplorer'=>$filerunning_fileexplorer
		];
		
		$sql ="INSERT INTO `tblInventory` ";
		$sql.="(`machine_id`,`last_update`,`fileversion_monitor`,`fileversion_sitekiosk`,`fileversion_watchdog`,`fileversion_fileexplorer`,`filerunning_monitor`,`filerunning_sitekiosk`,`filerunning_watchdog`,`filerunning_fileexplorer`) ";
		$sql.="VALUES ";
		$sql.="(:machine_id,:last_update,:fileversion_monitor,:fileversion_sitekiosk,:fileversion_watchdog,:fileversion_fileexplorer,:filerunning_monitor,:filerunning_sitekiosk,:filerunning_watchdog,:filerunning_fileexplorer) ";
		$sql.="ON DUPLICATE KEY UPDATE ";
		$sql.='`last_update`=:last_update,';
		$sql.="`fileversion_monitor`=:fileversion_monitor,`fileversion_sitekiosk`=:fileversion_sitekiosk,`fileversion_watchdog`=:fileversion_watchdog,`fileversion_fileexplorer`=:fileversion_fileexplorer,";
		$sql.="`filerunning_monitor`=:filerunning_monitor,`filerunning_sitekiosk`=:filerunning_sitekiosk,`filerunning_watchdog`=:filerunning_watchdog,`filerunning_fileexplorer`=:filerunning_fileexplorer ";
		
		
		$stmt=$conn->prepare($sql);
		$stmt->execute($sql_array);
			
		$data_response["code"]=0;
		$data_response["message"]=$message;
	}


	if($data_request['action']=="metrics") {
		if(isset($data_request['machine_id'])) {
			if(isset($data_request['year'])&&isset($data_request['month'])&&isset($data_request['day'])) {
				$message="ok";
				$timestamp=time();
				$machine_id=$data_request['machine_id'];
				$year=$data_request['year'];
				$month=$data_request['month'];
				$day=$data_request['day'];
				
				$logfile_exists=$data_request['logfile_exists'];
				
				$last_logfile_received=mktime(1,2,3,$month,$day,$year);
				
				$emergency_states=isset($data_request['emergency_states'])?$data_request['emergency_states']:'0';
				$urls_visited=isset($data_request['urls_visited'])?$data_request['urls_visited']:'0';
				$user_sessions=isset($data_request['user_sessions'])?$data_request['user_sessions']:'0';
				$sitekiosk_crashes=isset($data_request['sitekiosk_crashes'])?$data_request['sitekiosk_crashes']:'0';
				$screensaver=isset($data_request['screensaver'])?$data_request['screensaver']:'0';
				
				$urls=isset($data_request['urls'])?$data_request['urls']:'{}';
				$urls=json_decode($urls);
				
				
				$sql_array=[
					'machine_id'=>$machine_id,
					'year'=>$year,
					'month'=>$month,
					'day'=>$day
				];
				
				$sql ="DELETE FROM `tblMetrics` WHERE `machine_id`=:machine_id AND `year`=:year AND`month`=:month AND`day`=:day";
				$stmt=$conn->prepare($sql);
				$stmt->execute($sql_array);
				
				
				$sql_array=[
					'machine_id'=>$machine_id,
					'timestamp'=>$timestamp,
					'year'=>$year,
					'month'=>$month,
					'day'=>$day,
					'logfile_exists'=>$logfile_exists,
					'emergency_states'=>$emergency_states,
					'urls_visited'=>$urls_visited,
					'user_sessions'=>$user_sessions,
					'sitekiosk_crashes'=>$sitekiosk_crashes,
					'screensaver'=>$screensaver
				];
				
				$sql ="INSERT INTO `tblMetrics` ";
				$sql.="(`machine_id`,`timestamp`,`year`,`month`,`day`,`logfile_exists`,`emergency_states`,`urls_visited`,`user_sessions`,`sitekiosk_crashes`,`screensaver`) ";
				$sql.="VALUES ";
				$sql.="(:machine_id,:timestamp,:year,:month,:day,:logfile_exists,:emergency_states,:urls_visited,:user_sessions,:sitekiosk_crashes,:screensaver) ";
				$sql.="ON DUPLICATE KEY UPDATE ";
				$sql.="`timestamp`=:timestamp,`year`=:year,`month`=:month,`day`=:day,`logfile_exists`=:logfile_exists,`emergency_states`=:emergency_states,`urls_visited`=:urls_visited,`user_sessions`=:user_sessions,`sitekiosk_crashes`=:sitekiosk_crashes,`screensaver`=:screensaver ";
				
				$stmt=$conn->prepare($sql);
				$stmt->execute($sql_array);
				
				
				$last_metrics_id=$conn->lastInsertId();
				
				$sql_array=array();
				foreach($urls as $url=>$count) {
					$sql_array_tmp=[
						'machine_id'=>$machine_id,
						'metrics_id'=>$last_metrics_id,
						'url'=>$url,
						'count'=>$count
					];
					
					$sql_array[]=$sql_array_tmp;
				}
				
				$sql ="INSERT INTO `tblMetrics_Urls` ";
				$sql.="(`machine_id`,`metrics_id`,`url`,`count`) ";
				$sql.="VALUES ";
				$sql.="(:machine_id,:metrics_id,:url,:count) ";
				$stmt=$conn->prepare($sql);
				foreach($sql_array as $sql_array_row) {
					$stmt->execute($sql_array_row); 
				}
				
				
				$sql_array=[
					'machine_id'=>$machine_id,
					'last_metrics_sync'=>$timestamp,
					'last_logfile_received'=>$last_logfile_received
				];
				
				$sql="UPDATE `tblMachines` SET `last_metrics_sync`=:last_metrics_sync,`last_logfile_received`=:last_logfile_received WHERE `id`=:machine_id";
				$stmt=$conn->prepare($sql);
				$stmt->execute($sql_array);
				
				
				$data_response["code"]=0;
				$data_response["message"]=$message;
			} else {
				$data_response["code"]=1;
				$data_response["message"]="invalid date";
			}
		} else {
			$data_response["code"]=2;
			$data_response["message"]="invalid id";
		}
	}


	if($data_request['action']=="monitor_self_update") {
		$machine_id=$data_request['machine_id'];
		$message="ok";
		$monitor_updateenabled="";
		$monitor_last_version="";
		
		
		$sql="SELECT `name`,`value` FROM `tblAppSettings` WHERE `name`='Monitor_UpdateEnabled' AND (`machine_id`=:machine_id OR `machine_id`=0) ORDER BY `machine_id` ASC,`settingsgroup_id` ASC";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['machine_id'=>$machine_id]);
		
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			if($row['name']=="Monitor_UpdateEnabled") {
				$monitor_updateenabled=$row['value'];
			}
		}
		
		
		$sql="SELECT `name`,`value` FROM `tblOVCCSettings` WHERE `name`='ExpectedFileVersion_Monitor' AND (`machine_id`=:machine_id OR `machine_id`=0) ORDER BY `machine_id` ASC,`settingsgroup_id` ASC";
		$stmt=$conn->prepare($sql);
		$stmt->execute(['machine_id'=>$machine_id]);
		
		while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
			if($row['name']=="ExpectedFileVersion_Monitor") {
				$monitor_last_version=$row['value'];
			}
		}
		

		$data_response["code"]=0;
		$data_response["message"]=$message;
		$data_response["monitor_updateenabled"]=$monitor_updateenabled;
		$data_response["monitor_last_version"]=$monitor_last_version;
	}
	
	
	if($data_request['action']=="set_status_code") {
		$machine_id=$data_request['machine_id'];
		$status_name=$data_request['status_name'];
		$status_code=$data_request['status_code'];
		$message="ok";
		
		
		switch($status_name) {
			case 'appsettings';
				$sql="UPDATE `tblMachines` SET `last_appsettings_statuscode`=:last_appsettings_statuscode WHERE `id`=:machine_id";
				$stmt=$conn->prepare($sql);
				$stmt->execute(['machine_id'=>$machine_id,'last_appsettings_statuscode'=>$status_code]);
				
				break;
			case 'ovccsettings';
				$sql="UPDATE `tblMachines` SET `last_ovccettings_statuscode`=:last_ovccettings_statuscode WHERE `id`=:machine_id";
				$stmt=$conn->prepare($sql);
				$stmt->execute(['machine_id'=>$machine_id,'last_ovccettings_statuscode'=>$status_code]);
				
				break;
			default;
				break;
		}
		
		$data_response["code"]=0;
		$data_response["message"]=$message;
	}


	$sql="INSERT INTO `tblLog` (`machine_id`,`activity`,`timestamp`) VALUES (:machine_id,:activity,:timestamp)";
	$stmt=$conn->prepare($sql);
	$stmt->execute(['machine_id'=>$machine_id,'activity'=>$data_request['action'],'timestamp'=>time()]);
}

$conn=null;

echo json_encode($data_response);
?>