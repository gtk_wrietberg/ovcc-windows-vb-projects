<?php include('header.php'); ?>
<?php
$sql="SELECT `name`,`value` FROM `tblSettings` WHERE `name`='monitor_last_version' OR `name`='monitor_last_download'";
$stmt=$conn->prepare($sql);
$stmt->execute();

while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	if($row['name']=="monitor_last_version") {
		$monitor_last_version=$row['value'];
	}
	if($row['name']=="monitor_last_download") {
		$monitor_last_download=$row['value'];
	}
}
?>
<h1>Latest available Monitor version</h1>
<h3><?=$monitor_last_version?></h3>


<?php include('footer.php'); ?>
