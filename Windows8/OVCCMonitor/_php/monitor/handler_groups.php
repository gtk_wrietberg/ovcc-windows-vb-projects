<?php
require 'inc/inc.php';

error_reporting(E_ALL);
ini_set('display_errors',1);
ini_set('display_startup_errors',1);

try {
	$conn=new PDO("mysql:host=$db_servername;dbname=$db_dbname",$db_username,$db_password);
	$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo "Connection failed: ".$e->getMessage();
	die();
}


$action=$_REQUEST['action'];

if($action=="save") {
	$group_id=$_REQUEST['group_id'];
	$new_name=$_REQUEST['new_name'];
	
	$sql="UPDATE `tblGroups` SET `name`=:name WHERE `id`=:group_id";
	$stmt=$conn->prepare($sql);
	$stmt->execute(['name'=>$new_name,'group_id'=>$group_id]);
	
	header("Location: settings.php");
	die();
}

header("Location: index.php");
die();
?>