<?php include('header.php'); ?>
	<h1>OVCC Settings</h1>
	<p>&nbsp;</p>
	<h2>Default settings list</h2>
	<table>
		<tr>
			<td><strong>Name</strong></td>
			<td><strong>Value</strong></td>
			<td><strong># custom</strong></td>
			<td>&nbsp;</td>
		</tr>
<?php
$sql ="SELECT COUNT(`id`) as `count`,`id`,`name`,`value` FROM `tblOVCCSettings` ";
$sql.="WHERE `name`= ANY ";
$sql.="(SELECT `name` FROM `tblOVCCSettings` WHERE `machine_id`=0 AND `settingsgroup_id`=0) ";
$sql.="GROUP BY `name` ORDER BY `name` ASC";

$stmt=$conn->prepare($sql);
$stmt->execute();

while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	$val_id=$row['id'];
	$val_name=$row['name'];
	$val_val=$row['value'];
	$val_count=$row['count'];
?>
		<tr>
			<td><?=$val_name?></td>
			<td><?=$val_val?></td>
			<td><?=($val_count-1)?></td>
			<td><a href="edit_setting.php?id=<?=$val_id?>">edit</a></td>
		</tr>
<?php
}
?>
	</table>
	
	<p>&nbsp;</p>
	<h2>Settings group</h2>
	<table>
		<tr>
			<td><strong>Name</strong></td>
			<td>&nbsp;</td>
		</tr>
<?php
$sql ="SELECT `id`,`name` FROM `tblOVCCSettingsGroup` ORDER BY `name` ASC";

$stmt=$conn->prepare($sql);
$stmt->execute();

while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	$val_id=$row['id'];
	$val_name=$row['name'];
?>
		<tr>
			<td><?=$val_name?></td>
			<td><a href="edit_group.php?id=<?=$val_id?>">edit</a></td>
		</tr>
<?php
}
?>
	</table>
	
	<p>&nbsp;</p>
	<h2>Settings group</h2>
	<table>
		<tr>
			<td><strong>Name</strong></td>
			<td>&nbsp;</td>
		</tr>
<?php
$sql ="SELECT `id`,`name` FROM `tblOVCCSettingsGroup` ORDER BY `name` ASC";

$stmt=$conn->prepare($sql);
$stmt->execute();

while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	$val_id=$row['id'];
	$val_name=$row['name'];
?>
		<tr>
			<td><?=$val_name?></td>
			<td><a href="edit_group.php?id=<?=$val_id?>">edit</a></td>
		</tr>
<?php
}
?>
	</table>
<?php include('footer.php'); ?>
