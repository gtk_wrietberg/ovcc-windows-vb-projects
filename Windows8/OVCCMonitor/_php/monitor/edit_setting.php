<?php include('header.php'); ?>
<?php
$setting_id=$_GET['id'];
$machine_id=isset($_GET['machine_id'])?$_GET['machine_id']:0;

$sql ="SELECT `name`,`value` FROM `tblOVCCSettings` WHERE `id`=:setting_id";
$stmt=$conn->prepare($sql);
$stmt->execute(['setting_id'=>$setting_id]);

if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	$val_name=$row['name'];
	$val_val=$row['value'];
?>
	<h1>OVCC Settings</h1>
	<p>&nbsp;</p>
	<h2>Edit setting</h2>
	<form action="handler_settings.php" method="GET" id="frmSettings">
	<input type="hidden" id="inputAction" name="action" value="">
	<input type="hidden" name="setting_id" value="<?=$setting_id?>">
	<input type="hidden" name="old_value" value="<?=$val_val?>">
	<input type="hidden" name="machine_id" value="<?=$machine_id?>">
	<table>
		<tr>
			<td><strong><?=$val_name?></strong></td>
			<td><input type="text" name="new_value" value="<?=$val_val?>" size="50"></td>
		</tr>
		<tr>
			<td colspan="2"><input type="button" value="save" onclick="submitForm('save')">&nbsp;<input type="button" value="cancel" onclick="submitForm('cancel')"></td>
		</tr>
	</table>
	</form>
	<script type="text/javascript">
		function submitForm(action) {
			$('#inputAction').val(action);
			$('#frmSettings').submit();
		}
	</script>
<?php
} else {
?>
<p class="redText">error, nothing found</p>
<?php
}
?>
<?php include('footer.php'); ?>
