<?php
require 'inc/inc.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

try {
	$conn=new PDO("mysql:host=$db_servername;dbname=$db_dbname",$db_username,$db_password);
	$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo "Connection failed: ".$e->getMessage();
	die();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>OVCC Monitor</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		
		<link rel='stylesheet' href='style/style.css'>
	</head>
	<body>
		<div id="header">OVCC Monitor</div>
		<div id="menu"><a class="menu" href="index.php">home</a> - <a class="menu" href="heartbeats.php">heartbeats</a> - <a class="menu" href="settings.php">settings</a> - <a class="menu" href="update.php">update</a></div>
		<div id="content">