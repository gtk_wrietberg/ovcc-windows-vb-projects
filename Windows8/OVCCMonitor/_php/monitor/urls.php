<?php include('header.php'); ?>
<?php
$machine_id=$_GET['id'];
$metrics_id=$_GET['metrics_id'];

$sql ="SELECT year,month,day FROM tblMetrics WHERE machine_id=:machine_id AND `id`=:metrics_id ";
$stmt=$conn->prepare($sql);
$stmt->execute(['machine_id'=>$machine_id,'metrics_id'=>$metrics_id]);

if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	$metrics_year=$row['year'];
	$metrics_year=$row['month'];
	$metrics_year=$row['day'];
} else {
	$metrics_year=0;
	$metrics_year=0;
	$metrics_year=0;
}

$sql ="SELECT tblMachines.id as machine_id,machine_name,property_name,ovcc_code,navision_code ";
$sql.="FROM tblMachines ";
$sql.="LEFT JOIN tblProperties on tblProperties.id=tblMachines.property_id ";
$sql.="WHERE tblMachines.id=:machine_id";

$stmt=$conn->prepare($sql);
$stmt->execute(['machine_id'=>$machine_id]);

if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
?>
	<h1>Details for: <?=$row['machine_name']?></h1>
	<p>&nbsp;</p>
	<h2>Property details</h2>
	<table>
		<tr>
			<td>Property name</td>
			<td><?=$row['property_name']?></td>
		</tr>
		<tr>
			<td>Navison code</td>
			<td><?=$row['navision_code']?></td>
		</tr>
	</table>
	
	<p>&nbsp;</p>
	<h2>Urls</h2>
	<h3>Date: <?=date("Y-m-d",mktime(0,0,0,$metrics_month,$metrics_day,$metrics_year))?></h3>
	<table>
		<tr>
			<td><strong>Url</strong></td>
			<td><strong>Count</strong></td>
		</tr>
<?php
$sql ="SELECT `url`,`count` ";
$sql.="FROM tblMetrics_Urls ";
$sql.="WHERE `machine_id`=:machine_id AND `metrics_id`=:metrics_id ";
$sql.="ORDER BY `count` DESC,`url` ASC";

$stmt_metrics=$conn->prepare($sql);
$stmt_metrics->execute(['machine_id'=>$machine_id,'metrics_id'=>$metrics_id]);

while($row_metrics=$stmt_metrics->fetch(PDO::FETCH_ASSOC)) {
?>
		<tr>
			<td><?=$row_metrics['url']?></td>
			<td><?=$row_metrics['count']?></td>
		</tr>
<?php
}
?>
	</table>
<?php
} else {
?>
<p class="redText">error, nothing found</p>
<?php
}
?>
<?php include('footer.php'); ?>
