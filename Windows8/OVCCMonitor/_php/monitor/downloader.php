<?php
require 'inc/inc.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$machine_id=$_REQUEST['machine_id'];
$action=$_REQUEST['action'];
$index=$_REQUEST['index'];
$error_status=0;

if(!isset($machine_id)||!isset($action)||!isset($index)) {
	die("Invalid request");
}

try {
	$conn=new PDO("mysql:host=$db_servername;dbname=$db_dbname",$db_username,$db_password);
	$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
	echo "Connection failed: ".$e->getMessage();
	die();
}

$filepath="";

if($action=='download_theme') {
	$sql="SELECT `zipfile` FROM `tblOVCCThemes` WHERE `theme_index`=:theme_index";
	$stmt=$conn->prepare($sql);
	$stmt->execute(['theme_index'=>$index]);
	
	if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		$filepath=dirname(__FILE__)."/_download/themes/".$row['zipfile'];
	} else {
		$error_status=3;
	}
}

if($action=='download_screensaver') {
	$sql="SELECT `zipfile` FROM `tblOVCCScreensavers` WHERE `screensaver_index`=:screensaver_index";
	$stmt=$conn->prepare($sql);
	$stmt->execute(['screensaver_index'=>$index]);
	
	if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
		$filepath=dirname(__FILE__)."/_download/screensavers/".$row['zipfile'];
	} else {
		$error_status=3;
	}
}

if($action=='download_monitor') {
	$filepath=dirname(__FILE__)."/_download/apps/OVCCMonitor.zip";
}


$sql="INSERT INTO `tblLog` (`machine_id`,`activity`,`status`,`timestamp`) VALUES (:machine_id,:activity,:status,:timestamp)";
$stmt=$conn->prepare($sql);
$stmt->execute(['machine_id'=>$machine_id,'activity'=>$action.':'.$index,'status'=>$error_status,'timestamp'=>time()]);


$conn=null;

if(strlen($filepath)) {
	$filedata=@file_get_contents($filepath);
	$basename=basename($filepath);
	
	if($filedata) {
		header("Content-Type: application-x/force-download");
		header("Content-Disposition: attachment; filename=$basename");
		header("Content-length: ".(string)(strlen($filedata)));
		header("Expires: ".gmdate("D, d M Y H:i:s",mktime(date("H")+2,date("i"),date("s"),date("m"),date("d"),date("Y")))." GMT");
		header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");

		if(FALSE===strpos($_SERVER["HTTP_USER_AGENT"],'MSIE ')) {
			header("Cache-Control: no-cache, must-revalidate");
		}

		header("Pragma: no-cache");

		flush();

		ob_start();
		echo $filedata;
	} else {
		die("ERROR: UNABLE TO OPEN $basename");
	}
}
?>