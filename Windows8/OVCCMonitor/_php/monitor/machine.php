<?php include('header.php'); ?>
<?php
$machine_id=$_GET['id'];

$sql="SELECT `name`,`value` FROM `tblOVCCSettings` WHERE `name` LIKE 'ExpectedFileVersion_%' AND (`machine_id`=:machine_id OR `machine_id`=0) ORDER BY `machine_id` ASC,`settingsgroup_id` ASC";
$stmt=$conn->prepare($sql);
$stmt->execute(['machine_id'=>$machine_id]);

while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	if($row['name']=="ExpectedFileVersion_Monitor") {
		$expected_fileversion_monitor=$row['value'];
	}
	if($row['name']=="ExpectedFileVersion_SiteKiosk") {
		$expected_fileversion_sitekiosk=$row['value'];
	}
	if($row['name']=="ExpectedFileVersion_Watchdog") {
		$expected_fileversion_watchdog=$row['value'];
	}
	if($row['name']=="ExpectedFileVersion_FileExplorer") {
		$expected_fileversion_fileexplorer=$row['value'];
	}
}


$sql ="SELECT tblMachines.id as machine_id,machine_name,property_name,ovcc_code,navision_code,tblHeartbeat.timestamp as heartbeat_timestamp,last_appsettings_sync,last_ovccsettings_sync,last_metrics_sync,";
$sql.="last_update,fileversion_monitor,filerunning_monitor,fileversion_sitekiosk,filerunning_sitekiosk,fileversion_watchdog,filerunning_watchdog,fileversion_fileexplorer,filerunning_fileexplorer ";
$sql.="FROM tblMachines ";
$sql.="LEFT JOIN tblHeartbeat on tblHeartbeat.machine_id=tblMachines.id ";
$sql.="LEFT JOIN tblProperties on tblProperties.id=tblMachines.property_id ";
$sql.="LEFT JOIN tblInventory on tblInventory.machine_id=tblMachines.id ";
$sql.="WHERE tblMachines.id=:machine_id";

$stmt=$conn->prepare($sql);
$stmt->execute(['machine_id'=>$machine_id]);

if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	$machine_name=$row['machine_name'];
	$property_name=$row['property_name'];
	$navision_code=$row['navision_code'];
	$last_appsettings_sync=$row['last_appsettings_sync'];
	$last_update=$row['last_update'];
	$fileversion_monitor=$row['fileversion_monitor'];
	$filerunning_monitor=$row['filerunning_monitor'];
	$fileversion_sitekiosk=$row['fileversion_sitekiosk'];
	$filerunning_sitekiosk=$row['filerunning_sitekiosk'];
	$fileversion_watchdog=$row['fileversion_watchdog'];
	$filerunning_watchdog=$row['filerunning_watchdog'];
	$fileversion_fileexplorer=$row['fileversion_fileexplorer'];
	$filerunning_fileexplorer=$row['filerunning_fileexplorer'];
	$last_ovccsettings_sync=$row['last_ovccsettings_sync'];
	$last_metrics_sync=$row['last_metrics_sync'];
	
?>
	<h1>Details for: <?=$machine_name?></h1>
	<p>&nbsp;</p>
	<h2>Property details</h2>
	<table>
		<tr>
			<td>Property name</td>
			<td><?=$property_name?></td>
		</tr>
		<tr>
			<td>Navison code</td>
			<td><?=$navision_code?></td>
		</tr>
		<tr>
			<td>Last app settings sync</td>
			<td><?=date("Y-m-d H:i:s",$last_appsettings_sync)?></td>
		</tr>
	</table>
	
	<p>&nbsp;</p>
	<h2>Inventory</h2>
	<h3>Last update: <?=date("Y-m-d H:i:s",$last_update)?></h3>
	<table>
		<tr>
			<td><strong>Process name</strong></td>
			<td><strong>version</strong></td>
			<td><strong>running</strong></td>
		</tr>
		<tr>
			<td>Monitor</td>
			<td><span class="<?=($fileversion_monitor!=$expected_fileversion_monitor)?'redText':'greenText'?>"><?=$fileversion_monitor?></span></td>
			<td><?=$filerunning_monitor?></td>
		</tr>
		<tr>
			<td>SiteKiosk</td>
			<td><span class="<?=($fileversion_sitekiosk!=$expected_fileversion_sitekiosk)?'redText':'greenText'?>"><?=$fileversion_sitekiosk?></span></td>
			<td><?=$filerunning_sitekiosk?></td>
		</tr>
		<tr>
			<td>Watchdog</td>
			<td><span class="<?=($fileversion_watchdog!=$expected_fileversion_watchdog)?'redText':'greenText'?>"><?=$fileversion_watchdog?></span></td>
			<td><?=$filerunning_watchdog?></td>
		</tr>
		<tr>
			<td>FileExplorer</td>
			<td><span class="<?=($fileversion_fileexplorer!=$expected_fileversion_fileexplorer)?'redText':'greenText'?>"><?=$fileversion_fileexplorer?></span></td>
			<td><?=$filerunning_fileexplorer?></td>
		</tr>
	</table>
	
<?php
$sql="SELECT `id`,`machine_id`,`name`,`value` FROM `tblOVCCSettings` WHERE `machine_id`=:machine_id OR `machine_id`=0 ORDER BY `machine_id` ASC,`name` ASC";
$stmt_settings=$conn->prepare($sql);
$stmt_settings->execute(['machine_id'=>$machine_id]);

$settings_arr=[];
while($row_settings=$stmt_settings->fetch(PDO::FETCH_ASSOC)) {
	$settings_arr[$row_settings['name']]=$row_settings;
}
?>
	<p>&nbsp;</p>
	<h2>OVCC Settings</h2>
	<h3>Last update: <?=date("Y-m-d H:i:s",$last_ovccsettings_sync)?></h3>
	<table cellspacing="5">
		<tr>
			<td><strong>Name</strong></td>
			<td><strong>Value</strong></td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
<?php
foreach($settings_arr as $name=>$o) {
?>
		<tr>
			<td><?=$o['name']?></td>
			<td><?=$o['value']?></td>
			<td><a href="edit_setting.php?id=<?=$o['id']?>&machine_id=<?=$machine_id?>">edit</a></td>
<?php
	if($o['machine_id']!=0) {
?>
			<td><a href="handler_settings.php?action=to_default&setting_id=<?=$o['id']?>&machine_id=<?=$machine_id?>">set to default</a></td>
<?php
	} else {
?>
			<td>&nbsp;</td>
<?php
	}
?>
		</tr>
<?php
}
?>
	</table>

<?php
$sql ="SELECT `id`,`timestamp`,`year`,`month`,`day`,`emergency_states`,`urls_visited`,`user_sessions`,`sitekiosk_crashes`,`screensaver` ";
$sql.="FROM tblMetrics ";
$sql.="WHERE `machine_id`=:machine_id ";
$sql.="ORDER BY `year`,`month`,`day`";

$stmt_metrics=$conn->prepare($sql);
$stmt_metrics->execute(['machine_id'=>$machine_id]);

?>
	<p>&nbsp;</p>
	<h2>Metrics</h2>
	<h3>Last update: <?=date("Y-m-d H:i:s",$last_metrics_sync)?></h3>
	<table>
		<tr>
			<td><strong>Date</strong></td>
			<td><strong>Emergencies</strong></td>
			<td><strong>Crashes</strong></td>
			<td><strong>Sessions</strong></td>
			<td><strong>Urls</strong></td>
		</tr>
<?php
while($row_metrics=$stmt_metrics->fetch(PDO::FETCH_ASSOC)) {
?>
		<tr>
			<td><?=$row_metrics['year']?>-<?=$row_metrics['month']?>-<?=$row_metrics['day']?></td>
			<td><?=$row_metrics['emergency_states']?></td>
			<td><?=$row_metrics['sitekiosk_crashes']?></td>
			<td><?=$row_metrics['user_sessions']?></td>
<?php
	if($row_metrics['urls_visited']>0) {
?>
			<td><a href="urls.php?id=<?=$machine_id?>&metrics_id=<?=$row_metrics['id']?>"><?=$row_metrics['urls_visited']?></a></td>
<?php 
	} else {
?>
			<td>0</td>
<?php
	}
?>
		</tr>
<?php
}
?>
	</table>
<?php
} else {
?>
<p class="redText">error, nothing found</p>
<?php
}
?>
<?php include('footer.php'); ?>
