<?php include('header.php'); ?>
		<h1>Heartbeats</h1>
			<table border="1">
				<thead>
					<tr>
						<th style="width:10vw">Machine name</th>
						<th style="width:10vw">Property name</th>
						<th style="width:10vw">Navision code</th>
						<th style="width:10vw">Last heartbeat</th>
						<th style="width:10vw">OVCC Status</th>
						<th style="width:10vw">In use</th>
					</tr>
				</thead>
				<tbody>
<?php
$sql ="SELECT ";
$sql.="tblMachines.id as machine_id,tblMachines.machine_name,";
$sql.="tblProperties.property_name,tblProperties.ovcc_code,tblProperties.navision_code,";
$sql.="tblHeartbeat.timestamp as heartbeat_timestamp,";
$sql.="tblInventory.filerunning_sitekiosk,tblInventory.filerunning_watchdog,";
$sql.="tblQuickMetrics.`current_user`,tblQuickMetrics.`usage_state`,tblQuickMetrics.`last_usage_in_seconds` ";
$sql.="FROM tblMachines ";
$sql.="LEFT JOIN tblHeartbeat on tblHeartbeat.machine_id=tblMachines.id ";
$sql.="LEFT JOIN tblProperties on tblProperties.id=tblMachines.property_id ";
$sql.="LEFT JOIN tblInventory on tblInventory.machine_id=tblMachines.id ";
$sql.="LEFT JOIN tblQuickMetrics on tblQuickMetrics.machine_id=tblMachines.id ";
$sql.="ORDER BY tblHeartbeat.timestamp ASC,tblMachines.machine_name ASC";
$stmt=$conn->prepare($sql);
$stmt->execute();

while($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	$ovcc_ok=0;
	$ovcc_ok-=($row['filerunning_sitekiosk']>0)?1:0;
	$ovcc_ok-=($row['filerunning_watchdog']>0)?2:0;
	$ovcc_ok-=(strtolower($row['current_user'])!="sitekiosk")?4:0;
	
	$heartbeat_diff=time()-$row['heartbeat_timestamp'];
	if($heartbeat_diff>120) {
		$heartbeat_diff_string='<span class="redText">'.date("Y-m-d H:i:s",$row['heartbeat_timestamp']).'</a>';
	} elseif($heartbeat_diff>60) {
		$heartbeat_diff_string='<span class="yellowText">'.date("Y-m-d H:i:s",$row['heartbeat_timestamp']).'</a>';
	} else {
		$heartbeat_diff_string='<span class="greenText">'.date("Y-m-d H:i:s",$row['heartbeat_timestamp']).'</a>';
	}
	
	if($ovcc_ok==0) {
		$ovcc_ok_string='<span class="greenText">OK</a>';
	} else {
		$ovcc_ok_string='<span class="redText">FAIL ('.$ovcc_ok.')</a>';
	}

	if(($row['usage_state']>0||$row['last_usage_in_seconds']<120)&&$heartbeat_diff<120&&$row['filerunning_sitekiosk']>0&&strtolower($row['current_user'])=="sitekiosk") {
		$ovcc_in_use_string='<span class="greenText">YES</a>';
	} else {
		$ovcc_in_use_string='<span class="redText">NO</a>';
	}
?>
					<tr>
						<td><a href="machine.php?id=<?=$row['machine_id']?>"><?=$row['machine_name']?></a></td>
						<td><?=$row['property_name']?></td>
						<td><?=$row['navision_code']?></td>
						<td><?=$heartbeat_diff_string?></td>
						<td><?=$ovcc_ok_string?></td>
						<td><?=$ovcc_in_use_string?></td>
					</tr>
<?php
}
?>
				</tbody>
			</table>
<?php include('footer.php'); ?>
