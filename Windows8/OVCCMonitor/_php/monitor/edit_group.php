<?php include('header.php'); ?>
<?php
$settingsgroup_id=$_GET['id'];

$sql ="SELECT `name` FROM `tblOVCCSettingsGroup` WHERE `id`=:settingsgroup_id";
$stmt=$conn->prepare($sql);
$stmt->execute(['settingsgroup_id'=>$settingsgroup_id]);

if($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
	$val_name=$row['name'];
?>
	<h1>Settings group</h1>
	<p>&nbsp;</p>
	<h2>Edit setting</h2>
	<form action="handler_group.php" method="GET" id="frmSettings">
	<input type="hidden" id="inputAction" name="action" value="">
	<input type="hidden" name="settingsgroup_id" value="<?=$settingsgroup_id?>">
	<table>
		<tr>
			<td><strong><?=$val_name?></strong></td>
			<td><input type="text" name="new_name" value="<?=$val_name?>" size="50"></td>
		</tr>
		<tr>
			<td colspan="2"><input type="button" value="save" onclick="submitForm('save')">&nbsp;<input type="button" value="cancel" onclick="submitForm('cancel')"></td>
		</tr>
	</table>
	</form>
	<script type="text/javascript">
		function submitForm(action) {
			$('#inputAction').val(action);
			$('#frmSettings').submit();
		}
	</script>
<?php
} else {
?>
<p class="redText">error, nothing found</p>
<?php
}
?>
<?php include('footer.php'); ?>
