﻿Module MainModule

    Sub Main()
        Dim sUserNameObf As String = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower, LPCConstants.RegistryKeys.VALUE__PuppyPowerUserName, "")
        Dim sPassWordObf As String = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower, LPCConstants.RegistryKeys.VALUE__PuppyPowerPassWord, "")

        Dim sUserName As String = Helpers.XOrObfuscation.Deobfuscate(sUserNameObf)
        Dim sPassWord As String = Helpers.XOrObfuscation.Deobfuscate(sPassWordObf)

        Console.WriteLine("username: " & sUserName)
        Console.WriteLine("password: " & sPassWord)
    End Sub

End Module
