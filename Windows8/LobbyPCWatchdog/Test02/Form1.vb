﻿Imports System.Security.AccessControl
Imports System.Security.Principal
Imports Microsoft.Win32

Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim sid As SecurityIdentifier = New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
        Dim account As NTAccount = TryCast(sid.Translate(GetType(NTAccount)), NTAccount)

        Using rk As RegistryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\GuestTek\Watchdog\TEST", RegistryKeyPermissionCheck.ReadWriteSubTree)
            Dim rs As RegistrySecurity = rk.GetAccessControl
            Dim rar As RegistryAccessRule = New RegistryAccessRule( _
                                            account.ToString, _
                                            RegistryRights.FullControl, _
                                            InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, _
                                            PropagationFlags.None, _
                                            AccessControlType.Allow)


            rs.SetAccessRuleProtection(True, False)
            rs.AddAccessRule(rar)

            rk.SetAccessControl(rs)
        End Using


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MsgBox(Helpers.Registry.AllowAllForEveryone("HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\TEST_TEST"))
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'My.Computer.Audio.PlaySystemSound(System.Media.SystemSounds.Exclamation)
        My.Computer.Audio.Play(My.Resources.Dog_bark, AudioPlayMode.Background)
    End Sub
End Class
