﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain_warning
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain_warning))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.comboAdmins = New System.Windows.Forms.ComboBox()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.btnCheck = New System.Windows.Forms.Button()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnlWait = New System.Windows.Forms.Panel()
        Me.progressFake = New System.Windows.Forms.ProgressBar()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.tmrCountdown = New System.Windows.Forms.Timer(Me.components)
        Me.pnlCountdown = New System.Windows.Forms.Panel()
        Me.lblCountdownSeconds = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblCountdown = New System.Windows.Forms.Label()
        Me.pnlError = New System.Windows.Forms.Panel()
        Me.btnRetry = New System.Windows.Forms.Button()
        Me.lblError = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.tmrErrorRetry = New System.Windows.Forms.Timer(Me.components)
        Me.pnlOk = New System.Windows.Forms.Panel()
        Me.lblDisabledMessage = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.tmrExit = New System.Windows.Forms.Timer(Me.components)
        Me.tmrFadeIn = New System.Windows.Forms.Timer(Me.components)
        Me.tmrFadeOut = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        Me.pnlWait.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCountdown.SuspendLayout()
        Me.pnlError.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOk.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.BackgroundImage = Global.OVCCWatchdog_warning.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(-1, -1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(231, 68)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'comboAdmins
        '
        Me.comboAdmins.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.comboAdmins.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.comboAdmins.BackColor = System.Drawing.Color.White
        Me.comboAdmins.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable
        Me.comboAdmins.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comboAdmins.ForeColor = System.Drawing.Color.Black
        Me.comboAdmins.FormattingEnabled = True
        Me.comboAdmins.ItemHeight = 29
        Me.comboAdmins.Location = New System.Drawing.Point(3, 161)
        Me.comboAdmins.Name = "comboAdmins"
        Me.comboAdmins.Size = New System.Drawing.Size(234, 35)
        Me.comboAdmins.TabIndex = 3
        '
        'lblUsername
        '
        Me.lblUsername.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsername.Location = New System.Drawing.Point(3, 138)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(234, 20)
        Me.lblUsername.TabIndex = 4
        Me.lblUsername.Text = "username"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'txtPassword
        '
        Me.txtPassword.BackColor = System.Drawing.Color.White
        Me.txtPassword.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(243, 161)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(234, 35)
        Me.txtPassword.TabIndex = 6
        Me.txtPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnCheck
        '
        Me.btnCheck.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnCheck.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCheck.Location = New System.Drawing.Point(3, 199)
        Me.btnCheck.Name = "btnCheck"
        Me.btnCheck.Size = New System.Drawing.Size(474, 32)
        Me.btnCheck.TabIndex = 7
        Me.btnCheck.Text = "Validate"
        Me.btnCheck.UseVisualStyleBackColor = False
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(243, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(234, 64)
        Me.lblTitle.TabIndex = 1
        Me.lblTitle.Text = "OVCC Watchdog"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.Label5)
        Me.pnlMain.Controls.Add(Me.Label1)
        Me.pnlMain.Controls.Add(Me.lblTitle)
        Me.pnlMain.Controls.Add(Me.PictureBox1)
        Me.pnlMain.Controls.Add(Me.txtPassword)
        Me.pnlMain.Controls.Add(Me.lblUsername)
        Me.pnlMain.Controls.Add(Me.btnCheck)
        Me.pnlMain.Controls.Add(Me.comboAdmins)
        Me.pnlMain.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMain.Location = New System.Drawing.Point(545, 92)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(483, 236)
        Me.pnlMain.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(244, 138)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(234, 20)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "password"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Segoe UI Semibold", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(474, 30)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "please select user and provide password"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlWait
        '
        Me.pnlWait.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlWait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWait.Controls.Add(Me.progressFake)
        Me.pnlWait.Controls.Add(Me.Label3)
        Me.pnlWait.Controls.Add(Me.Label2)
        Me.pnlWait.Controls.Add(Me.PictureBox2)
        Me.pnlWait.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlWait.Location = New System.Drawing.Point(30, 92)
        Me.pnlWait.Name = "pnlWait"
        Me.pnlWait.Size = New System.Drawing.Size(483, 236)
        Me.pnlWait.TabIndex = 9
        '
        'progressFake
        '
        Me.progressFake.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.progressFake.Location = New System.Drawing.Point(3, 199)
        Me.progressFake.MarqueeAnimationSpeed = 50
        Me.progressFake.Name = "progressFake"
        Me.progressFake.Size = New System.Drawing.Size(475, 32)
        Me.progressFake.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.progressFake.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(3, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(474, 123)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "one moment please"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(243, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(234, 64)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "OVCC Watchdog"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.BackgroundImage = Global.OVCCWatchdog_warning.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Location = New System.Drawing.Point(-1, -1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(231, 68)
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'tmrCountdown
        '
        Me.tmrCountdown.Interval = 1000
        '
        'pnlCountdown
        '
        Me.pnlCountdown.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.pnlCountdown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCountdown.Controls.Add(Me.lblCountdownSeconds)
        Me.pnlCountdown.Controls.Add(Me.Label4)
        Me.pnlCountdown.Controls.Add(Me.lblCountdown)
        Me.pnlCountdown.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCountdown.Location = New System.Drawing.Point(12, 12)
        Me.pnlCountdown.Name = "pnlCountdown"
        Me.pnlCountdown.Size = New System.Drawing.Size(182, 69)
        Me.pnlCountdown.TabIndex = 10
        '
        'lblCountdownSeconds
        '
        Me.lblCountdownSeconds.BackColor = System.Drawing.Color.Transparent
        Me.lblCountdownSeconds.ForeColor = System.Drawing.Color.Gainsboro
        Me.lblCountdownSeconds.Location = New System.Drawing.Point(3, 48)
        Me.lblCountdownSeconds.Name = "lblCountdownSeconds"
        Me.lblCountdownSeconds.Size = New System.Drawing.Size(174, 19)
        Me.lblCountdownSeconds.TabIndex = 12
        Me.lblCountdownSeconds.Text = "seconds"
        Me.lblCountdownSeconds.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Gainsboro
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(174, 24)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Automatic reboot in"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCountdown
        '
        Me.lblCountdown.BackColor = System.Drawing.Color.Transparent
        Me.lblCountdown.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountdown.ForeColor = System.Drawing.Color.White
        Me.lblCountdown.Location = New System.Drawing.Point(3, 0)
        Me.lblCountdown.Name = "lblCountdown"
        Me.lblCountdown.Size = New System.Drawing.Size(174, 67)
        Me.lblCountdown.TabIndex = 13
        Me.lblCountdown.Text = "xx"
        Me.lblCountdown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlError
        '
        Me.pnlError.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlError.Controls.Add(Me.btnRetry)
        Me.pnlError.Controls.Add(Me.lblError)
        Me.pnlError.Controls.Add(Me.Label7)
        Me.pnlError.Controls.Add(Me.PictureBox3)
        Me.pnlError.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlError.Location = New System.Drawing.Point(545, 378)
        Me.pnlError.Name = "pnlError"
        Me.pnlError.Size = New System.Drawing.Size(483, 236)
        Me.pnlError.TabIndex = 11
        '
        'btnRetry
        '
        Me.btnRetry.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnRetry.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetry.Location = New System.Drawing.Point(3, 199)
        Me.btnRetry.Name = "btnRetry"
        Me.btnRetry.Size = New System.Drawing.Size(474, 32)
        Me.btnRetry.TabIndex = 10
        Me.btnRetry.Text = "Retry"
        Me.btnRetry.UseVisualStyleBackColor = False
        '
        'lblError
        '
        Me.lblError.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblError.ForeColor = System.Drawing.Color.Red
        Me.lblError.Location = New System.Drawing.Point(3, 70)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(474, 123)
        Me.lblError.TabIndex = 3
        Me.lblError.Text = "error"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(243, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(234, 64)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "OVCC Watchdog"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.White
        Me.PictureBox3.BackgroundImage = Global.OVCCWatchdog_warning.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.PictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox3.Location = New System.Drawing.Point(-1, -1)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(231, 68)
        Me.PictureBox3.TabIndex = 1
        Me.PictureBox3.TabStop = False
        '
        'tmrErrorRetry
        '
        Me.tmrErrorRetry.Interval = 5000
        '
        'pnlOk
        '
        Me.pnlOk.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlOk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlOk.Controls.Add(Me.lblDisabledMessage)
        Me.pnlOk.Controls.Add(Me.Label6)
        Me.pnlOk.Controls.Add(Me.Label8)
        Me.pnlOk.Controls.Add(Me.PictureBox4)
        Me.pnlOk.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlOk.Location = New System.Drawing.Point(30, 378)
        Me.pnlOk.Name = "pnlOk"
        Me.pnlOk.Size = New System.Drawing.Size(483, 236)
        Me.pnlOk.TabIndex = 11
        '
        'lblDisabledMessage
        '
        Me.lblDisabledMessage.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDisabledMessage.ForeColor = System.Drawing.Color.Black
        Me.lblDisabledMessage.Location = New System.Drawing.Point(4, 145)
        Me.lblDisabledMessage.Name = "lblDisabledMessage"
        Me.lblDisabledMessage.Size = New System.Drawing.Size(474, 86)
        Me.lblDisabledMessage.TabIndex = 4
        Me.lblDisabledMessage.Text = "disabled for xx seconds"
        Me.lblDisabledMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(3, 87)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(474, 58)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Thank you"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(243, 3)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(234, 64)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "OVCC Watchdog"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.White
        Me.PictureBox4.BackgroundImage = Global.OVCCWatchdog_warning.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.PictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox4.Location = New System.Drawing.Point(-1, -1)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(231, 68)
        Me.PictureBox4.TabIndex = 1
        Me.PictureBox4.TabStop = False
        '
        'tmrExit
        '
        Me.tmrExit.Interval = 3000
        '
        'tmrFadeIn
        '
        Me.tmrFadeIn.Interval = 50
        '
        'tmrFadeOut
        '
        Me.tmrFadeOut.Interval = 50
        '
        'frmMain_warning
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.ClientSize = New System.Drawing.Size(1218, 655)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlOk)
        Me.Controls.Add(Me.pnlError)
        Me.Controls.Add(Me.pnlCountdown)
        Me.Controls.Add(Me.pnlWait)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain_warning"
        Me.Opacity = 0R
        Me.Text = "LobbyPC Watchdog"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.pnlWait.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCountdown.ResumeLayout(False)
        Me.pnlError.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOk.ResumeLayout(False)
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents comboAdmins As System.Windows.Forms.ComboBox
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents btnCheck As System.Windows.Forms.Button
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents pnlWait As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents progressFake As System.Windows.Forms.ProgressBar
    Friend WithEvents tmrCountdown As System.Windows.Forms.Timer
    Friend WithEvents pnlCountdown As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblCountdown As System.Windows.Forms.Label
    Friend WithEvents lblCountdownSeconds As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pnlError As System.Windows.Forms.Panel
    Friend WithEvents btnRetry As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents tmrErrorRetry As System.Windows.Forms.Timer
    Friend WithEvents pnlOk As System.Windows.Forms.Panel
    Friend WithEvents lblDisabledMessage As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents tmrExit As System.Windows.Forms.Timer
    Friend WithEvents tmrFadeIn As System.Windows.Forms.Timer
    Friend WithEvents tmrFadeOut As System.Windows.Forms.Timer

End Class
