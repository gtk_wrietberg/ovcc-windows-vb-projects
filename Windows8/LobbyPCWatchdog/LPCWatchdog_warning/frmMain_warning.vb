﻿Public Class frmMain_warning
    Private mThread_GetAdmins As Threading.Thread
    Private mThread_CheckLogon As Threading.Thread

    Delegate Sub AddAdminToCombobox_ThreadSafe_Callback(ByVal [text] As String)
    Delegate Sub SetComboboxText_ThreadSafe_Callback(ByVal [text] As String)
    Delegate Sub SetPasswordText_Threadsafe_Callback(ByVal [text] As String)
    Delegate Sub SetErrorText_Threadsafe_Callback(ByVal [text] As String)
    Delegate Sub SetCountdownText_Threadsafe_Callback(ByVal [text] As String)
    Delegate Sub SetCountdownSecondsText_Threadsafe_Callback(ByVal [text] As String)
    Delegate Sub SetErrorRetryTimer_Threadsafe_Callback(ByVal [bool] As Boolean)
    Delegate Sub ToggleControls_Threadsafe_Callback(ByVal [bool1] As Boolean, ByVal [bool2] As Boolean, ByVal [bool3] As Boolean, ByVal [bool4] As Boolean)
    Delegate Sub ClearCombobox_Threadsafe_Callback()

    Delegate Sub StartTimer_Threadsafe_Callback(ByVal [timername] As String)
    Delegate Sub StopTimer_Threadsafe_Callback(ByVal [timername] As String)

    Private mLoose As Boolean
    Private mTestMode As Boolean
    Private mCountDown As Integer = 0
    Private mRetries As Integer = 0

    Private mUsername As String = ""
    Private mPassword As String = ""

    Private mFadeValue As Double = 0.0
    Private mFadeValueMin As Double = 0.0
    Private mFadeValueMax As Double = 0.9
    Private mFadeStep As Double = 0.05


    'Private Sub frmMain_Activated(sender As Object, e As EventArgs) Handles Me.Activated
    '    mFadeValue = mFadeValueMin
    '    tmrFadeIn.Enabled = True

    '    If LPCGlobals.Settings.PlayStupidSound Then
    '        My.Computer.Audio.Play(My.Resources.Dog_bark, AudioPlayMode.Background)
    '    End If
    'End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Helpers.Processes.IsAppAlreadyRunning Then
            Me.Close()
        End If

        'init logger
        Helpers.Logger.InitialiseLogger()


        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("warning started", , 0)


        'load settings from registry
        LPCGlobals.Settings.Load()
        Helpers.Logger.Debugging = LPCGlobals.Settings.Debug


        'change labels
        lblDisabledMessage.Text = "disabled for " & LPCConstants.TimeoutsAndDelaysAndCounters.TIMEOUT__AdminMode.ToString & " seconds"


        'reset the warning popup trigger thingy
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount, 0)


        'Set fullscreen
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Me.Location = New Point(0, 0)
        Me.Size = SystemInformation.PrimaryMonitorSize
        'Helpers.Forms.TopMost(Me.Handle, True)


        'Empty combobox
        comboAdmins.Items.Clear()


        're-position ui
        Dim p As New Point

        p.X = (Me.Size.Width - pnlMain.Size.Width) / 2
        p.Y = (Me.Size.Height - pnlMain.Size.Height) / 2
        pnlMain.Location = p

        pnlWait.Location = pnlMain.Location
        pnlError.Location = pnlMain.Location
        pnlOk.Location = pnlMain.Location

        ToggleControls_Threadsafe(False, False, False, False)


        'set countdown timer
        Helpers.Logger.Write("countdown is set to", , 0)
        mLoose = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupLoose)
        mTestMode = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode)
        If mLoose Then
            mCountDown = LPCConstants.TimeoutsAndDelaysAndCounters.COUNTDOWN__WarningPopupRebootLoose
        Else
            mCountDown = LPCConstants.TimeoutsAndDelaysAndCounters.COUNTDOWN__WarningPopupReboot
        End If
        Helpers.Logger.Write(mCountDown.ToString & " seconds", , 1)

        SetCountdownText_Threadsafe(mCountDown.ToString)

        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupCountdown, mCountDown)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTimestamp, Helpers.TimeDate.ToUnix())


        'activate
        mFadeValue = mFadeValueMin
        tmrFadeIn.Enabled = True

        If LPCGlobals.Settings.PlayStupidSound Then
            My.Computer.Audio.Play(My.Resources.Dog_bark, AudioPlayMode.Background)
        End If
    End Sub

    Private Sub ResetRegistrySettings()
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount, 0)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode, False)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupLoose, False)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupCountdown, 0)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTimestamp, 0)
    End Sub

    Private Sub StartApplication()
        ToggleControls_Threadsafe(False, False, False, True)

        mThread_GetAdmins = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_FillAdminCombobox))
        mThread_GetAdmins.Start()
    End Sub

    Private Sub ExitApplication()
        Me.Close()
    End Sub

    Private Sub Thread_FillAdminCombobox()
        Dim lUsers As New List(Of String)

        lUsers = WindowsUser.GetAdminUsers

        ClearCombobox_Threadsafe()
        SetComboboxText_Threadsafe("(select user)")

        'AddAdminToCombobox_Threadsafe("")
        For Each item As String In lUsers
            Console.WriteLine("PPFER: " & item)

            If Not item.Equals(LPCConstants.UsersAndPasswords.USERNAME__LobbyPCPuppy) Then
                AddAdminToCombobox_Threadsafe(item)
            End If
        Next

        ToggleControls_Threadsafe(True, False, False, False)
    End Sub

    Private Sub Thread_CheckLogon()
        ToggleControls_Threadsafe(False, False, False, True)

        Dim sError As String = ""

        If WindowsUser.CheckLogon(mUsername, mPassword, sError) Then
            ToggleControls_Threadsafe(False, False, True, False)
            Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, LPCConstants.RegistryKeys.VALUE__AdminModeActive, True)
            Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, LPCConstants.RegistryKeys.VALUE__AdminModeActiveTime, Helpers.TimeDate.ToUnix())

            ResetRegistrySettings()

            StartTimer_Threadsafe("exit")
        Else
            mRetries += 1
            If mRetries >= LPCConstants.TimeoutsAndDelaysAndCounters.COUNT__Maxfails Then
                'restarting
                RebootAndAutostart()
            Else
                SetPasswordText_Threadsafe("")
                If (LPCConstants.TimeoutsAndDelaysAndCounters.COUNT__Maxfails - mRetries) = 1 Then
                    SetErrorText_Threadsafe(sError & vbCrLf & vbCrLf & "(1 retry left)")
                Else
                    SetErrorText_Threadsafe(sError & vbCrLf & vbCrLf & "(" & (LPCConstants.TimeoutsAndDelaysAndCounters.COUNT__Maxfails - mRetries) & " retries left)")
                End If

                Me.Invoke(New SetErrorRetryTimer_Threadsafe_Callback(AddressOf SetErrorRetryTimer_Threadsafe), New Object() {True})

                ToggleControls_Threadsafe(False, True, False, False)
            End If
        End If
    End Sub

    Private Sub SetErrorRetryTimer_Threadsafe(ByVal [bool] As Boolean)
        tmrErrorRetry.Enabled = [bool]
    End Sub

    Private Sub ClearCombobox_Threadsafe()
        If Me.comboAdmins.InvokeRequired Then
            Dim d As New ClearCombobox_Threadsafe_Callback(AddressOf ClearCombobox_Threadsafe)
            Me.Invoke(d, New Object() {})
        Else
            comboAdmins.Items.Clear()
        End If
    End Sub

    Private Sub SetCountdownText_Threadsafe(ByVal [text] As String)
        If Me.lblCountdown.InvokeRequired Then
            Dim d As New SetCountdownText_Threadsafe_Callback(AddressOf SetCountdownText_Threadsafe)
            Me.Invoke(d, New Object() {[text]})
        Else
            lblCountdown.Text = [text]
        End If
    End Sub

    Private Sub SetCountdownSecondsText_Threadsafe(ByVal [text] As String)
        If Me.lblCountdownSeconds.InvokeRequired Then
            Dim d As New SetCountdownSecondsText_Threadsafe_Callback(AddressOf SetCountdownSecondsText_Threadsafe)
            Me.Invoke(d, New Object() {[text]})
        Else
            lblCountdownSeconds.Text = [text]
        End If
    End Sub

    Private Sub AddAdminToCombobox_Threadsafe(ByVal [text] As String)
        If Me.comboAdmins.InvokeRequired Then
            Dim d As New AddAdminToCombobox_ThreadSafe_Callback(AddressOf AddAdminToCombobox_Threadsafe)
            Me.Invoke(d, New Object() {[text]})
        Else
            comboAdmins.Items.Add([text])
        End If
    End Sub

    Private Sub SetComboboxText_Threadsafe(ByVal [text] As String)
        If Me.comboAdmins.InvokeRequired Then
            Dim d As New SetComboboxText_ThreadSafe_Callback(AddressOf SetComboboxText_Threadsafe)
            Me.Invoke(d, New Object() {[text]})
        Else
            comboAdmins.Text = [text]
        End If
    End Sub

    Private Sub SetPasswordText_Threadsafe(ByVal [text] As String)
        If Me.txtPassword.InvokeRequired Then
            Dim d As New SetPasswordText_Threadsafe_Callback(AddressOf SetPasswordText_Threadsafe)
            Me.Invoke(d, New Object() {[text]})
        Else
            txtPassword.Text = [text]
        End If
    End Sub

    Private Sub SetErrorText_Threadsafe(ByVal [text] As String)
        If Me.lblError.InvokeRequired Then
            Dim d As New SetErrorText_Threadsafe_Callback(AddressOf SetErrorText_Threadsafe)
            Me.Invoke(d, New Object() {[text]})
        Else
            lblError.Text = [text]
        End If
    End Sub

    Private Sub ToggleControls_Threadsafe(ByVal [boolReady] As Boolean, ByVal [boolError] As Boolean, ByVal [boolOk] As Boolean, ByVal [boolWait] As Boolean)
        If Me.pnlMain.InvokeRequired Then
            Dim d As New ToggleControls_Threadsafe_Callback(AddressOf ToggleControls_Threadsafe)
            Me.Invoke(d, New Object() {[boolReady], [boolError], [boolOk], [boolWait]})
        Else
            ToggleControls([boolReady], [boolError], [boolOk], [boolWait])
        End If
    End Sub

    Private Sub StartTimer_Threadsafe(ByVal [timername] As String)
        Dim d As New StartTimer_Threadsafe_Callback(AddressOf StartTimer)
        Me.Invoke(d, New Object() {[timername]})
    End Sub

    Private Sub StopTimer_Threadsafe(ByVal [timername] As String)
        Dim d As New StopTimer_Threadsafe_Callback(AddressOf StopTimer)
        Me.Invoke(d, New Object() {[timername]})
    End Sub

    Private Sub StartTimer(timername As String)
        If timername.Equals("exit") Then
            tmrExit.Enabled = True
        End If
        If timername.Equals("fadein") Then
            tmrFadeIn.Enabled = True
        End If
        If timername.Equals("fadeout") Then
            tmrFadeOut.Enabled = True
        End If
    End Sub

    Private Sub StopTimer(timername As String)
        If timername.Equals("exit") Then
            tmrExit.Enabled = False
        End If
        If timername.Equals("fadein") Then
            tmrFadeIn.Enabled = False
        End If
        If timername.Equals("fadeout") Then
            tmrFadeOut.Enabled = False
        End If
    End Sub

    Private Sub ToggleControls(bReady As Boolean, bError As Boolean, bOk As Boolean, bWait As Boolean)
        If bReady Then
            pnlMain.Visible = True
            pnlCountdown.Visible = True
            pnlWait.Visible = False
            pnlError.Visible = False
            pnlOk.Visible = False

            btnCheck.Enabled = True

            progressFake.MarqueeAnimationSpeed = 0
            progressFake.Enabled = False

            tmrCountdown.Enabled = True
        ElseIf bError Then
            pnlMain.Visible = False
            pnlCountdown.Visible = False
            pnlWait.Visible = False
            pnlError.Visible = True
            pnlOk.Visible = False

            btnCheck.Enabled = False

            progressFake.MarqueeAnimationSpeed = 0
            progressFake.Enabled = False

            tmrCountdown.Enabled = False
        ElseIf bOk Then
            pnlMain.Visible = False
            pnlCountdown.Visible = False
            pnlWait.Visible = False
            pnlError.Visible = False
            pnlOk.Visible = True

            btnCheck.Enabled = False

            progressFake.MarqueeAnimationSpeed = 0
            progressFake.Enabled = False

            tmrCountdown.Enabled = False
        ElseIf bWait Then
            pnlMain.Visible = False
            pnlCountdown.Visible = False
            pnlWait.Visible = True
            pnlError.Visible = False
            pnlOk.Visible = False

            btnCheck.Enabled = False

            progressFake.MarqueeAnimationSpeed = 25
            progressFake.Enabled = True

            tmrCountdown.Enabled = False
        Else
            pnlMain.Visible = False
            pnlCountdown.Visible = False
            pnlWait.Visible = False
            pnlError.Visible = False
            pnlOk.Visible = False

            btnCheck.Enabled = False

            progressFake.MarqueeAnimationSpeed = 0
            progressFake.Enabled = False

            tmrCountdown.Enabled = False
        End If
    End Sub

    Private Sub tmrCountdown_Tick(sender As Object, e As EventArgs) Handles tmrCountdown.Tick
        SetCountdownText_Threadsafe(mCountDown.ToString)

        If mCountDown = 1 Then
            SetCountdownSecondsText_Threadsafe("second")
        Else
            SetCountdownSecondsText_Threadsafe("seconds")
        End If

        mCountDown -= 1

        If mCountDown < 0 Then
            tmrCountdown.Enabled = False

            'Too late, rebooting...
            RebootAndAutostart()
        End If
    End Sub

    Private Sub btnCheck_Click(sender As Object, e As EventArgs) Handles btnCheck.Click
        If btnCheck.Enabled Then
            If comboAdmins.SelectedItem Is Nothing Then
                'oops, skip this. No exceptions for us!

                SetErrorText_Threadsafe("Something went wrong, please retry...")
                Me.Invoke(New SetErrorRetryTimer_Threadsafe_Callback(AddressOf SetErrorRetryTimer_Threadsafe), New Object() {True})
                ToggleControls_Threadsafe(False, True, False, False)
            Else
                mUsername = comboAdmins.SelectedItem.ToString
                mPassword = txtPassword.Text

                mThread_CheckLogon = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_CheckLogon))
                mThread_CheckLogon.Start()
            End If
        End If
    End Sub

    Private Sub btnRetry_Click(sender As Object, e As EventArgs) Handles btnRetry.Click
        ToggleControls_Threadsafe(True, False, False, False)
    End Sub

    Private Sub tmrErrorRetry_Tick(sender As Object, e As EventArgs) Handles tmrErrorRetry.Tick
        tmrErrorRetry.Enabled = False

        ToggleControls_Threadsafe(True, False, False, False)
    End Sub

    Private Sub RebootAndAutostart()
        'restarting
        SetErrorText_Threadsafe("REBOOTING")
        btnRetry.Visible = False
        ToggleControls_Threadsafe(False, True, False, False)

        'set auto start
        SetSiteKioskAutoStart()

        ResetRegistrySettings()

        'Regardless of auto start was succesful, we are going to reboot
        RebootWindows()
    End Sub

    Private Sub SetSiteKioskAutoStart()
        Helpers.Logger.Write("Forcing auto start!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)

        SiteKioskController.ResetError()
        SiteKioskController.ReadRegistryValues()

        Helpers.Logger.Write("updating startup xml file", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
        If SiteKioskController.UpdateAutoStartupFile() Then
            Helpers.Logger.Write("set startup", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            If SiteKioskController.SetAutoStartup() Then
                Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Else
                Helpers.Logger.Write("Something went wrong", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                Helpers.Logger.Write(SiteKioskController.SiteKiosk_Error.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Else
            Helpers.Logger.Write("Something went wrong", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Helpers.Logger.Write(SiteKioskController.SiteKiosk_Error.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If
    End Sub

    Private Sub RebootWindows()
        If LPCGlobals.Settings.TestMode Then
            Helpers.Logger.Write("I would have rebooted here, but we´re in test mode...", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        Else
            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        End If
    End Sub

    Private Sub tmrExit_Tick(sender As Object, e As EventArgs) Handles tmrExit.Tick
        tmrExit.Enabled = False

        ToggleControls_Threadsafe(False, False, False, False)

        mFadeValue = mFadeValueMax
        tmrFadeOut.Enabled = True
    End Sub

    Private Sub tmrFadeIn_Tick(sender As Object, e As EventArgs) Handles tmrFadeIn.Tick
        mFadeValue += mFadeStep

        If mFadeValue > mFadeValueMax Then
            mFadeValue = mFadeValueMax
        End If

        Me.Opacity = mFadeValue

        If mFadeValue = mFadeValueMax Then
            tmrFadeIn.Enabled = False

            StartApplication()
        End If
    End Sub

    Private Sub tmrFadeOut_Tick(sender As Object, e As EventArgs) Handles tmrFadeOut.Tick
        mFadeValue -= mFadeStep

        If mFadeValue < mFadeValueMin Then
            mFadeValue = mFadeValueMin
        End If

        Me.Opacity = mFadeValue

        If mFadeValue = mFadeValueMin Then
            tmrFadeOut.Enabled = False

            ExitApplication()
        End If
    End Sub

    Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPassword.KeyDown
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True

            If btnCheck.Enabled Then btnCheck.PerformClick()
        End If
    End Sub

    Private Sub comboAdmins_LostFocus(sender As Object, e As EventArgs) Handles comboAdmins.LostFocus
        If comboAdmins.SelectedItem Is Nothing Then
            btnCheck.Enabled = False
        End If
        If comboAdmins.SelectedIndex > -1 Then
            btnCheck.Enabled = True
        Else
            btnCheck.Enabled = False
        End If

    End Sub

    Private Sub comboAdmins_SelectedValueChanged(sender As Object, e As EventArgs) Handles comboAdmins.SelectedValueChanged
        If comboAdmins.SelectedIndex > -1 Then
            btnCheck.Enabled = True
        Else
            btnCheck.Enabled = False
        End If
    End Sub

    Private Sub comboAdmins_TextUpdate(sender As Object, e As EventArgs) Handles comboAdmins.TextUpdate
        If comboAdmins.SelectedIndex > -1 Then
            btnCheck.Enabled = True
        Else
            btnCheck.Enabled = False
        End If
    End Sub


    Private Sub comboAdmins_DrawItem(sender As Object, e As DrawItemEventArgs) Handles comboAdmins.DrawItem
        e.DrawBackground()

        If e.Index > -1 Then
            e.Graphics.DrawString(comboAdmins.Items(e.Index).ToString(), e.Font, New SolidBrush(e.ForeColor), e.Bounds)
        End If
    End Sub
End Class
