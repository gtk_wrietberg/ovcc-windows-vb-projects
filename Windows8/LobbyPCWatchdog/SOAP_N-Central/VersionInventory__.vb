﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Xml

Public Class VersionInventory__
    Public Shared Function Initialise() As Boolean
        Dim bFailed As Boolean = True

        bFailed = bFailed And DataCollection.Initialise()
        bFailed = bFailed And N_Central.Data.Initialise()
        bFailed = bFailed And N_Central.Soap.Initialise()

        Return Not bFailed
    End Function

    Public Class DataCollection
        Public Shared Function Initialise() As Boolean
            GetInfo.Exes.Init()
            GetInfo.Exes.Add("SiteKiosk", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "SiteKiosk\SiteKiosk.exe"))
            GetInfo.Exes.Add("ChromeWrapper", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\ChromeWrapper\ChromeWrapper.exe"))
            If IO.File.Exists(IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "Google\Chrome\Application\Chrome.exe")) Then
                GetInfo.Exes.Add("Chrome", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "Google\Chrome\Application\Chrome.exe"))
            Else
                GetInfo.Exes.Add("Chrome", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder64bit(), "Google\Chrome\Application\Chrome.exe"))
            End If
            GetInfo.Exes.Add("FileExplorer", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\FileExplorer\FileExplorer.exe"))
            GetInfo.Exes.Add("OVCCWatchdog", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\OVCCWatchdog\OVCCWatchdog_service.exe"))
            GetInfo.Exes.Add("OVCCInformationUpdater", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\OVCCSoftware\tools\OVCCInformationUpdater.exe"))



            GetInfo.AdminAccount.Users.Init()
            GetInfo.AdminAccount.Users.Add("ibahn")
            GetInfo.AdminAccount.Users.Add("guesttek")
            GetInfo.AdminAccount.Users.Add("guest-tek")
            GetInfo.AdminAccount.Users.Add("sitekiosk")

            'passwords (obfuscated with v2)
            GetInfo.AdminAccount.Passwords.Init()
            GetInfo.AdminAccount.Passwords.Add("Hilton_recent", "58091155545d2979253021053e4b4c140e045c004a545e060f03064016080b0e59560408150256512b7c246173053e4b11115d035153115b53560d0a0d171d585b540c5e535f4a5e54517d76276b7200674e11175f050a0717035d040f505114150c08590a5158534a5e5a057379")
            GetInfo.AdminAccount.Passwords.Add("Hilton_oldH", "530f10545201282b2766265d3a494a135e535a56435053540d510c164609500d5e5f040e150600512e7b233026546e4d4f105b500f57105609575756521314085f5d5c5f070c11550600297b203777533d4e4a1a0c550b0610060f535a525211100b510f0950025b4b505a577f7d")
            GetInfo.AdminAccount.Passwords.Add("Hilton_oldK", "565b47525407282974302202694b4f405a5f5d534a5052525e035746410b080a585f525d450351537276236774056b191e165d070f56405b53015f550d1a105f590f0c5e040b475403537377243173556a1b18475f5f0c0316015d015d560513165d0c0a5853030c460600507d77")
            GetInfo.AdminAccount.Passwords.Add("GuestTek_recent", "535b435707072c2a243026506d1f181551555b0045035a060c0104141253505b0a51555b425003007f7d703625076e191a13500051014b0609035a070345425d5d5c5855560e105554567b2d226222506d4b1b120d57580645040956585202151c5f0b0d5150050f420256572f76")
            GetInfo.AdminAccount.Passwords.Add("SiteKiosk_recent", "565a44515707797a233623563e4e1d130b5e5f0615565c0459575742450c5955515e515f12565b567b76773722506b181d155e525f57100108060a0a061a460859580b52540e435203517329726b26056b4e4c170e025e51460052525c010745155f5c5b0e53595c155e515d7329")
            GetInfo.AdminAccount.Passwords.Add("SiteKiosk_old", "595c10045602792c22647600691d1b110c070d544a0652030f0b0d11170b515d0957550e465f55072c7a226424523d1e4f405f515d0111575d505f00004145530b0a0e54075a100203517c2a256321516e1f1a4058000b5212500c530c030d42130e590e0e50575d46565601737c")

            Return True
        End Function

        Public Shared Function Collect() As Boolean
            Dim sName As String = "", sPath As String = "", sVersionFull As String = "", sVersionMajor As String = "", sVersionMajorMinor As String = ""

            N_Central.Data.DeviceProperties.SetLocalValue("AppVersion_OVCCVersionInventory", "")

            While GetInfo.Exes.GetNext(sName, sPath)
                sVersionFull = Helpers.FilesAndFolders.GetFileVersion(sPath)
                sVersionMajor = Helpers.FilesAndFolders.GetFileVersion(sPath, 1)
                sVersionMajorMinor = Helpers.FilesAndFolders.GetFileVersion(sPath, GetInfo.Constants.ShortVersionParts)

                N_Central.Data.DeviceProperties.SetLocalValue("AppVersion_full_" & sName, sVersionFull)
                N_Central.Data.DeviceProperties.SetLocalValue("AppVersion_major_" & sName, sVersionMajor)
                N_Central.Data.DeviceProperties.SetLocalValue("AppVersion_majorminor_" & sName, sVersionMajorMinor)
            End While


            'Accounts
            Dim sUserName As String = "", sPassName As String = "", sPass As String = "", sError As String = ""
            Dim bUserFound As Boolean = False, bPasswordFound As Boolean = False

            Helpers.Logger.WriteMessage("getting account info")

            While GetInfo.AdminAccount.Users.GetNext(sUserName)
                bUserFound = False
                bPasswordFound = False

                Helpers.Logger.WriteMessage(sUserName, 1)
                If Helpers.WindowsUser.FindUser(sUserName) Then
                    bUserFound = True
                    GetInfo.AdminAccount.Passwords.Reset()

                    While GetInfo.AdminAccount.Passwords.GetNext(sPassName, sPass)
                        Helpers.Logger.WriteMessage(sPassName, 3)

                        If Helpers.WindowsUser.CheckLogon(sUserName, sPass, sError) Then
                            bPasswordFound = True

                            Exit While
                        End If
                    End While
                End If

                If bUserFound Then
                    If bPasswordFound Then
                        N_Central.Data.DeviceProperties.SetLocalValue("AccountsPassword_" & sUserName, Helpers.Types.MaskString(sPass))
                    Else
                        N_Central.Data.DeviceProperties.SetLocalValue("AccountsPassword_" & sUserName, "no_match")
                    End If
                Else
                    N_Central.Data.DeviceProperties.SetLocalValue("AccountsPassword_" & sUserName, "not_found")
                End If
            End While


            'sitekiosk
            Dim sLastCfg As String = "", sLastCfgDate As String = "", sLastCfgDateUTC As String = "", sTheme As String = ""

            sLastCfg = GetInfo.SiteKiosk.Config.LastCfg
            sLastCfgDate = GetInfo.SiteKiosk.Config.LastCfgDate
            sLastCfgDateUTC = GetInfo.SiteKiosk.Config.LastCfgDateUTC
            sTheme = GetInfo.SiteKiosk.Theme.Name

            N_Central.Data.DeviceProperties.SetLocalValue("SiteKioskLastCfg", sLastCfg)
            N_Central.Data.DeviceProperties.SetLocalValue("SiteKioskLastCfgDateUTC", sLastCfgDateUTC)
            N_Central.Data.DeviceProperties.SetLocalValue("SiteKioskTheme", sTheme)


            'machinename
            Dim sMachineName As String = "", sMachineNameFromScript As String = "", sMatching As String = ""

            sMachineName = GetInfo.MachineName.Name
            sMachineNameFromScript = GetInfo.MachineName.NameFromScript

            N_Central.Data.DeviceProperties.SetLocalValue("MachineName", sMachineName)
            N_Central.Data.DeviceProperties.SetLocalValue("MachineNameFromScript", sMachineNameFromScript)

            If sMachineName.Equals(sMachineNameFromScript) Then
                sMatching = "true"
            Else
                sMatching = "false"
            End If
            N_Central.Data.DeviceProperties.SetLocalValue("MachineNameMatching", sMatching)


            'last check date
            Dim dDate As Date = Date.Now
            Dim sFormat As String = "yyyy-MM-dd HH:mm:ss"

            N_Central.Data.DeviceProperties.SetLocalValue("VersionInventoryLastCheckUTC", dDate.ToUniversalTime.ToString(sFormat))


            Return True
        End Function

        Public Class GetInfo
            Public Class Constants
                Public Shared ReadOnly ShortVersionParts As Integer = 2
            End Class

            Public Class AdminAccount
                Public Class Users
                    Private Shared mUsers As New List(Of String)
                    Private Shared mCount As Integer = 0

                    Public Shared Sub Init()
                        mUsers = New List(Of String)
                    End Sub

                    Public Shared Sub Add(Name As String)
                        mUsers.Add(Name)
                    End Sub

                    Public Shared Sub Reset()
                        mCount = 0
                    End Sub

                    Public Shared Function GetNext(ByRef Name As String) As Boolean
                        If mCount < 0 Or mCount >= mUsers.Count Then
                            Return False
                        End If

                        Name = mUsers.Item(mCount)

                        mCount += 1

                        Return True
                    End Function
                End Class

                Public Class Passwords
                    Private Class _password
                        Public Sub New(Name As String, ObfuscatedPassword As String)
                            mName = Name
                            mPass = ObfuscatedPassword
                        End Sub

                        Private mName As String
                        Public Property Name() As String
                            Get
                                Return mName
                            End Get
                            Set(ByVal value As String)
                                mName = value
                            End Set
                        End Property

                        Private mPass As String
                        Public Property ObfuscatedPassword() As String
                            Get
                                Return mPass
                            End Get
                            Set(ByVal value As String)
                                mPass = value
                            End Set
                        End Property
                    End Class

                    Private Shared mPasswords As New List(Of _password)
                    Private Shared mCount As Integer = 0

                    Public Shared Sub Init()
                        mCount = 0
                        mPasswords = New List(Of _password)
                    End Sub

                    Public Shared Sub Add(Name As String, ObfuscatedPassword As String)
                        mPasswords.Add(New _password(Name, ObfuscatedPassword))
                    End Sub

                    Public Shared Sub Reset()
                        mCount = 0
                    End Sub

                    Public Shared Function GetNext(ByRef Name As String, ByRef Pass As String) As Boolean
                        If mCount < 0 Or mCount >= mPasswords.Count Then
                            Return False
                        End If

                        Name = mPasswords.Item(mCount).Name
                        Pass = Helpers.XOrObfuscation_v2.Deobfuscate(mPasswords.Item(mCount).ObfuscatedPassword)

                        mCount += 1

                        Return True
                    End Function
                End Class
            End Class

            Public Class Exes
                Private Class _exe
                    Public Sub New(Name As String, Path As String)
                        mName = Name
                        mPath = Path
                    End Sub

                    Private mName As String
                    Public Property Name() As String
                        Get
                            Return mName
                        End Get
                        Set(ByVal value As String)
                            mName = value
                        End Set
                    End Property

                    Private mPath As String
                    Public Property Path() As String
                        Get
                            Return mPath
                        End Get
                        Set(ByVal value As String)
                            mPath = value
                        End Set
                    End Property
                End Class

                Private Shared mExes As New List(Of _exe)
                Private Shared mCount As Integer = 0

                Public Shared Sub Init()
                    mCount = 0
                    mExes = New List(Of _exe)
                End Sub

                Public Shared Sub Add(Name As String, Path As String)
                    Helpers.Logger.WriteMessageRelative(Name, 1)
                    Helpers.Logger.WriteMessageRelative(Path, 2)
                    mExes.Add(New _exe(Name, Path))
                End Sub

                Public Shared Sub Reset()
                    mCount = 0
                End Sub

                Public Shared Function GetNext(ByRef Name As String, ByRef Path As String) As Boolean
                    If mCount < 0 Or mCount >= mExes.Count Then
                        Return False
                    End If

                    Name = mExes.Item(mCount).Name
                    Path = mExes.Item(mCount).Path

                    mCount += 1

                    Return True
                End Function
            End Class

            Public Class SiteKiosk
                Public Class Config
                    Public Shared Function LastCfg() As String
                        Return Helpers.SiteKiosk.GetSiteKioskActiveConfig
                    End Function

                    Public Shared Function LastCfgDate() As String
                        Dim d As Date = Helpers.FilesAndFolders.GetFileLastModifiedDateAsDate(LastCfg())

                        Return d.ToString("yyyy-MM-dd HH:mm:ss zzz")
                    End Function

                    Public Shared Function LastCfgDateUTC() As String
                        Dim d As Date = Helpers.FilesAndFolders.GetFileLastModifiedDateAsDate(LastCfg())

                        d = d.ToUniversalTime

                        Return d.ToString("yyyy-MM-dd HH:mm:ss")
                    End Function
                End Class

                Public Class Theme
                    Public Shared Function Name() As String
                        Dim sStartPage As String = ""

                        Try
                            Dim sSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()

                            Dim nt As Xml.XmlNameTable
                            Dim ns As Xml.XmlNamespaceManager
                            Dim xmlSkCfg As Xml.XmlDocument
                            Dim xmlNode_Root As Xml.XmlNode

                            xmlSkCfg = New Xml.XmlDocument
                            xmlSkCfg.Load(sSkCfgFile)

                            nt = xmlSkCfg.NameTable
                            ns = New Xml.XmlNamespaceManager(nt)
                            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
                            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

                            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)


                            Dim xmlNode_startpageconfig As Xml.XmlNode
                            Dim xmlNode_startpage As Xml.XmlNode

                            xmlNode_startpageconfig = xmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)
                            xmlNode_startpage = xmlNode_startpageconfig.SelectSingleNode("sk:startpage", ns)


                            sStartPage = xmlNode_startpage.InnerText
                        Catch ex As Exception
                            Helpers.Logger.WriteErrorRelative("exception", 1)
                            Helpers.Logger.WriteErrorRelative(ex.Message, 2)
                        End Try


                        sStartPage = sStartPage.Replace("/", "\")

                        Dim sParts As String() = sStartPage.Split("\")

                        If sParts.Length < 2 Then
                            sStartPage = ""
                        Else
                            sStartPage = String.Join("", sParts.Skip(sParts.Length - 2).Take(1))
                        End If


                        Return sStartPage
                    End Function
                End Class
            End Class

            Public Class MachineName
                Public Shared Function Name() As String
                    Return Environment.MachineName
                End Function

                Public Shared Function NameFromScript() As String
                    Return Helpers.SiteKiosk.GetMachineNameFromScript()
                End Function
            End Class
        End Class
    End Class

    Public Class N_Central
        Public Class Data
            Public Shared ReadOnly Property ApplianceConfigXmlFile() As String = IO.Path.Combine(Misc._GetProgramFilesFolder(), "N-able Technologies\Windows Agent\config\ApplianceConfig.xml")

            Public Shared Property ApplianceId() As String = ""

            Public Shared Property DeviceId() As String = ""

            Public Shared Property SettingsFile As String = "inventory_settings.txt"

            Public Shared ReadOnly Property SoapUri() As String = "http://ovcc.gtkcentral.net/dms2/services2/ServerEI2"

            Public Shared ReadOnly Property SoapUsername() As String = "ovccapiuser@gmail.com"

            Public Shared ReadOnly Property SoapPassword() As String = "0VCC.Ap1Us3r"

            Public Shared ReadOnly Property SoapDeviceIdSearchString() As String = "device.deviceid"

            Public Shared Property LastError() As String = ""


            Public Shared Function Initialise() As Boolean
                Return DeviceProperties.Initialise()
            End Function

            Public Shared ReadOnly Property DevicePropertiesCount As Integer
                Get
                    Return DeviceProperties.Count
                End Get
            End Property

            Public Class DeviceProperties
                Public Class _DeviceProperty
                    Public PropertyId As String
                    Public Label As String
                    Public Type As String
                    Public Value As String
                    Public LocalValue As String

                    Public Sub New(PropertyId As String, Label As String, Type As String, Value As String, LocalValue As String)
                        Me.PropertyId = PropertyId
                        Me.Label = Label
                        Me.Type = Type
                        Me.Value = Value
                        Me.LocalValue = LocalValue
                    End Sub
                End Class

                Private Shared mDeviceProperties As List(Of _DeviceProperty)

                Public Shared Function Initialise() As Boolean
                    mDeviceProperties = New List(Of _DeviceProperty)

                    Return True
                End Function

                Public Shared Sub Add(PropertyId As String, Label As String, Type As String, Value As String)
                    mDeviceProperties.Add(New _DeviceProperty(PropertyId, Label, Type, Value, ""))
                End Sub

                Public Shared Function Count() As Integer
                    Return mDeviceProperties.Count
                End Function

                Public Shared Function Find(Label As String, ByRef DeviceProperty As _DeviceProperty) As Boolean
                    Dim bRet As Boolean = False

                    For Each _prop As _DeviceProperty In mDeviceProperties
                        If _prop.Label.Equals(Label) Then
                            DeviceProperty = _prop
                            bRet = True
                            Exit For
                        End If
                    Next

                    Return bRet
                End Function

                Public Shared Function GetValue(Label As String, ByRef Value As String) As Boolean
                    Dim bRet As Boolean = False

                    For Each _prop As _DeviceProperty In mDeviceProperties
                        If _prop.Label.Equals(Label) Then
                            Value = _prop.Value
                            bRet = True
                            Exit For
                        End If
                    Next

                    Return bRet
                End Function

                Public Shared Function SetLocalValue(Label As String, Value As String) As Boolean
                    Dim bRet As Boolean = False

                    For i As Integer = 0 To mDeviceProperties.Count - 1
                        If mDeviceProperties.Item(i).Label.Equals(Label) Then
                            mDeviceProperties.Item(i).LocalValue = Value
                            bRet = True
                            Exit For
                        End If
                    Next

                    Return bRet
                End Function

                Public Shared Iterator Function IterateAll() As IEnumerable(Of _DeviceProperty)
                    Dim index As Integer = 0

                    While index < mDeviceProperties.Count
                        Yield mDeviceProperties.Item(index)

                        index += 1
                    End While
                End Function

                Public Shared Iterator Function IterateChanged() As IEnumerable(Of _DeviceProperty)
                    Dim index As Integer = 0

                    While index < mDeviceProperties.Count
                        If Not mDeviceProperties.Item(index).Value.Equals(mDeviceProperties.Item(index).LocalValue) Then
                            Yield mDeviceProperties.Item(index)
                        End If

                        index += 1
                    End While
                End Function
            End Class
        End Class

        Public Class Soap
            Private Shared Function _LoadApplianceId() As Boolean
                Try
                    Dim xmlDocument As New XmlDocument()
                    Dim xmlRootNode As XmlNode
                    Dim xmlNodeApplianceId As XmlNode

                    xmlDocument.Load(Data.ApplianceConfigXmlFile)

                    xmlRootNode = xmlDocument.SelectSingleNode("ApplianceConfig")
                    xmlNodeApplianceId = xmlRootNode.SelectSingleNode("ApplianceID")

                    Data.ApplianceId = xmlNodeApplianceId.InnerText
                Catch ex As Exception
                    Return False
                End Try

                Return True
            End Function

            Public Shared Function Initialise() As Boolean
                Dim bFailed As Boolean = True

                bFailed = bFailed And _LoadApplianceId()

                Return Not bFailed
            End Function

            Public Shared Function Action_deviceGet() As Boolean
                Dim bRet As Boolean = False
                Dim Body As String = "<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:ei2=""http://ei2.nobj.nable.com/"">
    <soap:Header/>
    <soap:Body>
        <ei2:deviceGet>
            <ei2:username>" & Data.SoapUsername & "</ei2:username>
            <ei2:password>" & Data.SoapPassword & "</ei2:password>
            <ei2:settings>
                <ei2:key>applianceid</ei2:key>
                <ei2:value>" & Data.ApplianceId & "</ei2:value>
            </ei2:settings>
        </ei2:deviceGet>
    </soap:Body>
</soap:Envelope>
"

                Data.DeviceId = ""
                Data.LastError = ""

                Try
                    Dim Response As New XmlDocument
                    Dim nt As System.Xml.XmlNameTable
                    Dim ns As System.Xml.XmlNamespaceManager

                    Response = PostWebservice("deviceGet", Body)

                    nt = Response.NameTable
                    ns = New System.Xml.XmlNamespaceManager(nt)
                    ns.AddNamespace("soap", "http://www.w3.org/2003/05/soap-envelope")


                    Dim NodeEnvelope As Xml.XmlNode
                    Dim NodeBody As Xml.XmlNode
                    Dim NodeFault As Xml.XmlNode

                    NodeEnvelope = Response.SelectSingleNode("soap:Envelope", ns)
                    NodeBody = NodeEnvelope.SelectSingleNode("soap:Body", ns)
                    NodeFault = NodeBody.SelectSingleNode("soap:Fault", ns)

                    If Not NodeFault Is Nothing Then
                        Dim NodeFaultReason As Xml.XmlNode
                        Dim NodeFaultReasonText As Xml.XmlNode

                        NodeFaultReason = NodeFault.SelectSingleNode("soap:Reason", ns)
                        NodeFaultReasonText = NodeFaultReason.SelectSingleNode("soap:Text", ns)

                        Throw New Exception(NodeFaultReasonText.InnerText)
                    End If


                    Dim InnerResponseXml As String = NodeBody.InnerXml

                    InnerResponseXml = Regex.Replace(NodeBody.InnerXml, "<deviceGetResponse([^>]+)>", "<deviceGetResponse>")


                    Dim InnerResponse As New Xml.XmlDocument

                    InnerResponse.LoadXml(InnerResponseXml)


                    Dim NodeInfos As Xml.XmlNodeList

                    NodeInfos = InnerResponse.SelectNodes("//info")

                    For Each NodeInfo As Xml.XmlNode In NodeInfos
                        Dim key As String, value As String

                        Try
                            key = NodeInfo.SelectSingleNode("key").InnerText
                            value = NodeInfo.SelectSingleNode("value").InnerText

                            If key.Equals(Data.SoapDeviceIdSearchString) Then
                                Data.DeviceId = value
                                bRet = True

                                Exit For
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                Catch ex As Exception
                    Data.LastError = ex.Message
                End Try

                Return bRet
            End Function

            Public Shared Function Action_devicePropertyList() As Boolean
                Dim bRet As Boolean = False
                Dim Body As String = "<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:ei2=""http://ei2.nobj.nable.com/"">
    <soap:Header/>
    <soap:Body>
        <ei2:devicePropertyList>
            <ei2:username>" & Data.SoapUsername & "</ei2:username>
            <ei2:password>" & Data.SoapPassword & "</ei2:password>
            <ei2:deviceIDs>" & Data.DeviceId & "</ei2:deviceIDs>
            <ei2:reverseOrder>false</ei2:reverseOrder>
        </ei2:devicePropertyList>
    </soap:Body>
</soap:Envelope>
"

                Data.DeviceProperties.Initialise()
                Data.LastError = ""

                Try
                    Dim Response As New XmlDocument
                    Dim nt As System.Xml.XmlNameTable
                    Dim ns As System.Xml.XmlNamespaceManager

                    Response = PostWebservice("devicePropertyList", Body)

                    nt = Response.NameTable
                    ns = New System.Xml.XmlNamespaceManager(nt)
                    ns.AddNamespace("soap", "http://www.w3.org/2003/05/soap-envelope")

                    Dim NodeEnvelope As Xml.XmlNode
                    Dim NodeBody As Xml.XmlNode
                    Dim NodeFault As Xml.XmlNode

                    NodeEnvelope = Response.SelectSingleNode("soap:Envelope", ns)
                    NodeBody = NodeEnvelope.SelectSingleNode("soap:Body", ns)
                    NodeFault = NodeBody.SelectSingleNode("soap:Fault", ns)

                    If Not NodeFault Is Nothing Then
                        Dim NodeFaultReason As Xml.XmlNode
                        Dim NodeFaultReasonText As Xml.XmlNode

                        NodeFaultReason = NodeFault.SelectSingleNode("soap:Reason", ns)
                        NodeFaultReasonText = NodeFaultReason.SelectSingleNode("soap:Text", ns)

                        Throw New Exception(NodeFaultReasonText.InnerText)
                    End If

                    Dim InnerResponseXml As String = NodeBody.InnerXml
                    InnerResponseXml = Regex.Replace(InnerResponseXml, "<devicePropertyListResponse([^>]+)>", "<devicePropertyListResponse>")


                    Dim InnerResponse As New Xml.XmlDocument

                    InnerResponse.LoadXml(InnerResponseXml)

                    Dim NodeProperties As Xml.XmlNodeList

                    NodeProperties = InnerResponse.SelectNodes("//properties")

                    For Each NodeProperty As Xml.XmlNode In NodeProperties
                        Dim devicePropertyID As String, label As String, type As String, value As String

                        Try
                            devicePropertyID = NodeProperty.SelectSingleNode("devicePropertyID").InnerText
                            label = NodeProperty.SelectSingleNode("label").InnerText
                            type = NodeProperty.SelectSingleNode("type").InnerText
                            value = NodeProperty.SelectSingleNode("value").InnerText

                            Data.DeviceProperties.Add(devicePropertyID, label, type, value)
                        Catch ex As Exception
                            Console.WriteLine(4)
                        End Try
                    Next

                    bRet = True
                Catch ex As Exception
                    Data.LastError = ex.Message
                End Try

                Return bRet
            End Function

            Public Shared Function Action_devicePropertyModify() As Boolean
                Dim bRet As Boolean = False
                Dim Body As String = "<soap:Envelope xmlns:soap=""http://www.w3.org/2003/05/soap-envelope"" xmlns:ei2=""http://ei2.nobj.nable.com/"">
<ei2:username>" & Data.SoapUsername & "</ei2:username>
            <ei2:password>" & Data.SoapPassword & "</ei2:password>
            <ei2:deviceIDs>" & Data.DeviceId & "</ei2:deviceIDs>
    <soap:Header/>
    <soap:Body>
        <ei2:devicePropertyModify>
            <ei2:username>" & Data.SoapUsername & "</ei2:username>
            <ei2:password>" & Data.SoapPassword & "</ei2:password>
            <ei2:deviceProperties>
                <ei2:deviceID>" & Data.DeviceId & "</ei2:deviceID>
"

                For Each _prop As N_Central.Data.DeviceProperties._DeviceProperty In Data.DeviceProperties.IterateChanged
                    Body &= "
                <ei2:properties>
                    <ei2:devicePropertyID>" & _prop.PropertyId & "</ei2:devicePropertyID>
                    <ei2:label>" & _prop.Label & "</ei2:label>
                    <ei2:value>" & _prop.LocalValue & "</ei2:value>
                </ei2:properties>
"
                Next

                Body &= "
            </ei2:deviceProperties>
        </ei2:devicePropertyModify>
    </soap:Body>
</soap:Envelope>
"

                MsgBox(Body)

                Data.DeviceProperties.Initialise()
                Data.LastError = ""

                Try
                    Dim Response As New XmlDocument
                    Dim nt As System.Xml.XmlNameTable
                    Dim ns As System.Xml.XmlNamespaceManager

                    Response = PostWebservice("devicePropertyModify", Body)

                    nt = Response.NameTable
                    ns = New System.Xml.XmlNamespaceManager(nt)
                    ns.AddNamespace("soap", "http://www.w3.org/2003/05/soap-envelope")

                    Dim NodeEnvelope As Xml.XmlNode
                    Dim NodeBody As Xml.XmlNode
                    Dim NodeFault As Xml.XmlNode

                    NodeEnvelope = Response.SelectSingleNode("soap:Envelope", ns)
                    NodeBody = NodeEnvelope.SelectSingleNode("soap:Body", ns)
                    NodeFault = NodeBody.SelectSingleNode("soap:Fault", ns)

                    If Not NodeFault Is Nothing Then
                        Dim NodeFaultReason As Xml.XmlNode
                        Dim NodeFaultReasonText As Xml.XmlNode

                        NodeFaultReason = NodeFault.SelectSingleNode("soap:Reason", ns)
                        NodeFaultReasonText = NodeFaultReason.SelectSingleNode("soap:Text", ns)

                        Throw New Exception(NodeFaultReasonText.InnerText)
                    End If

                    bRet = True
                Catch ex As Exception
                    Data.LastError = ex.Message
                End Try

                Return bRet
            End Function

            Private Shared Function PostWebservice(soapAction As String, xmlBody As String) As XmlDocument
                Dim uTF8Encoding As New UTF8Encoding()
                Dim bytes As Byte() = uTF8Encoding.GetBytes(xmlBody)

                Dim httpWebRequest As HttpWebRequest = CType(WebRequest.Create(Data.SoapUri), HttpWebRequest)

                httpWebRequest.ContentType = "application/soap+xml;charset=UTF-8"

                'httpWebRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip, deflate")

                httpWebRequest.Headers.Add("SOAPAction", soapAction)
                httpWebRequest.Method = "POST"
                httpWebRequest.ContentType = "text/xml; charset=utf-8"
                httpWebRequest.ContentLength = CLng(bytes.Length)

                Dim requestStream As Stream = httpWebRequest.GetRequestStream()
                requestStream.Write(bytes, 0, bytes.Length)
                requestStream.Close()

                Dim httpWebResponse As HttpWebResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                Dim streamReader As StreamReader = New StreamReader(httpWebResponse.GetResponseStream(), uTF8Encoding)

                Dim xmlDocument As New XmlDocument()

                xmlDocument.LoadXml(streamReader.ReadToEnd())
                httpWebResponse.Close()

                Return xmlDocument
            End Function
        End Class

        Private Class Misc
            Public Shared Function _GetProgramFilesFolder() As String
                Dim sPath As String = ""

                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

                If sPath.Equals(String.Empty) Then
                    sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
                End If

                If sPath.EndsWith("/") Then
                    sPath = sPath.Substring(0, sPath.Length - 1)
                End If

                Return sPath
            End Function
        End Class
    End Class
End Class
