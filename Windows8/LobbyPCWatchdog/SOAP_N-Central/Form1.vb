﻿Imports System.Text.RegularExpressions
Imports System.Xml

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim bRet As Boolean = False


        bRet = VersionInventory__.N_Central.Soap.Action_deviceGet()

        If bRet Then
            MsgBox("Ok: " & VersionInventory__.N_Central.Data.DeviceId)
        Else
            MsgBox("Fail: " & VersionInventory__.N_Central.Data.LastError)
        End If



        bRet = VersionInventory__.N_Central.Soap.Action_devicePropertyList

        If bRet Then
            MsgBox("Ok: " & VersionInventory__.N_Central.Data.DevicePropertiesCount)
        Else
            MsgBox("Fail: " & VersionInventory__.N_Central.Data.LastError)
        End If


        VersionInventory__.DataCollection.Collect()


        MsgBox("sending data")

        VersionInventory__.N_Central.Soap.Action_devicePropertyModify()


        MsgBox("done")
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.Debugging = True
        VersionInventory__.Initialise()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        '        <soap:Envelope xmlns: soap = ""http://www.w3.org/2003/05/soap-envelope"" xmlns:ei2 = ""http://ei2.nobj.nable.com/"">
        '    <soap:Header/>
        '    <soap:Body>
        '        <ei2:deviceGet>
        '            <ei2:username>" & Data.SoapUsername & "</ei2:username>
        '            <ei2:password>" & Data.SoapPassword & "</ei2:password>
        '            <ei2:settings>
        '                <ei2:key>applianceid</ei2:key>
        '                <ei2:value>" & Data.ApplianceId & "</ei2:value>
        '            </ei2:settings>
        '        </ei2:deviceGet>
        '    </soap:Body>
        '</soap:Envelope>


        Dim Response As New XmlDocument
        'Dim nt As System.Xml.XmlNameTable
        'Dim ns As System.Xml.XmlNamespaceManager

        'nt = Response.NameTable
        'ns = New System.Xml.XmlNamespaceManager(nt)
        'ns.AddNamespace("soap", "http://www.w3.org/2003/05/soap-envelope")
        'ns.AddNamespace("ei2", "http://ei2.nobj.nable.com/")

        '        XmlElement root = doc.CreateElement("JOBS");
        'root.SetAttribute("xmlns:JOBS", "http://www.example.com");
        'root.SetAttribute("xmlns:JOB", "http://www.example.com");
        'doc.AppendChild(root);


        Dim Node_SoapEnvelope As Xml.XmlElement
        Dim NodeAttribute1 As Xml.XmlAttribute
        Dim NodeAttribute2 As Xml.XmlAttribute
        Dim Node_SoapHeader As Xml.XmlElement
        Dim Node_SoapBody As Xml.XmlElement
        Dim Node_Ei2deviceGet As Xml.XmlElement
        Dim Node_Ei2Username As Xml.XmlElement

        Dim nsmgr As New XmlNamespaceManager(New NameTable())
        nsmgr.AddNamespace("soap", "http://www.w3.org/2003/05/soap-envelope")
        nsmgr.AddNamespace("ei2", "http://ei2.nobj.nable.com/")

        Console.WriteLine(1)
        Node_SoapEnvelope = Response.CreateElement("soap:Envelope", "http://www.w3.org/2003/05/soap-envelope")

        Dim s1 As String = nsmgr.LookupNamespace("soap")
        Dim schemaLocation1 As XmlAttribute = Response.CreateAttribute("xmlns:soap", s1)
        schemaLocation1.InnerText = "http://www.w3.org/2003/05/soap-envelope"
        Dim s2 As String = nsmgr.LookupNamespace("ei2")
        Dim schemaLocation2 As XmlAttribute = Response.CreateAttribute("xmlns:ei2", s2)
        schemaLocation2.InnerText = "http://ei2.nobj.nable.com/"
        Node_SoapEnvelope.Attributes.Append(schemaLocation1)
        Node_SoapEnvelope.Attributes.Append(schemaLocation2)


        Console.WriteLine(1)
        Node_SoapHeader = Response.CreateElement("soap:Envelope", "http://www.w3.org/2003/05/soap-envelope")
        Console.WriteLine(1)
        Node_SoapEnvelope.AppendChild(Node_SoapHeader)

        Console.WriteLine(1)
        Node_SoapBody = Response.CreateElement("soap:Body", "http://www.w3.org/2003/05/soap-envelope")
        Console.WriteLine(1)
        Node_Ei2deviceGet = Response.CreateElement("ei2:deviceGet", "http://ei2.nobj.nable.com/")
        Console.WriteLine(1)
        Node_Ei2Username = Response.CreateElement("ei2:username", "http://ei2.nobj.nable.com/")
        Console.WriteLine(1)
        Node_Ei2Username.InnerText = "test value"
        Console.WriteLine(1)
        Node_Ei2deviceGet.AppendChild(Node_Ei2Username)
        Console.WriteLine(1)
        Node_SoapBody.AppendChild(Node_Ei2deviceGet)
        Console.WriteLine(1)
        Node_SoapEnvelope.AppendChild(Node_SoapBody)

        Response.AppendChild(Node_SoapEnvelope)



        Console.WriteLine(Response.OuterXml)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim xmlDoc As New XmlDocument()
        Dim xmldecl As XmlDeclaration
        xmldecl = xmlDoc.CreateXmlDeclaration("1.0", Nothing, Nothing)
        xmldecl.Encoding = "UTF-8"
        Dim root As XmlElement = xmlDoc.DocumentElement
        xmlDoc.InsertBefore(xmldecl, root)
        xmlDoc.InsertBefore(xmldecl, root)
        xmlDoc.InsertBefore(xmldecl, root)
        xmlDoc.InsertBefore(xmldecl, root)
        Dim nsmgr As New XmlNamespaceManager(New NameTable())
        nsmgr.AddNamespace("itunes", "http://www.w3.org/2000/xmlns/")
        nsmgr.AddNamespace("vers", "2.0")

        Dim xRoot As XmlElement = xmlDoc.CreateElement("rss")
        Dim s As String = nsmgr.LookupNamespace("itunes")
        Dim schemaLocation As XmlAttribute = xmlDoc.CreateAttribute("xmlns:itunes", s)
        schemaLocation.InnerText = "http://www.itunes.com"
        xRoot.Attributes.Append(schemaLocation)
        Dim ver As XmlAttribute = xmlDoc.CreateAttribute("version")
        ver.InnerText = "2.0"
        xRoot.Attributes.Append(ver)
        xmlDoc.AppendChild(xRoot)

        Console.WriteLine(xmlDoc.OuterXml)
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("service started", , 0)

        Version_Inventory_Main()
    End Sub
End Class
