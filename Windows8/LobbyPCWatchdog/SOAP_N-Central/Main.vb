﻿Module Main
    Public Sub Main()

        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("soap test started", , 0)


        Version_Inventory_Main()

        Exit Sub

        VersionInventory.Initialise()

        Helpers.Logger.WriteMessage("(version) settings", 1)
        Helpers.Logger.WriteMessage("(version) ApplianceConfigXmlFile", 2)
        Helpers.Logger.WriteMessage("(version) " & VersionInventory.N_Central.Data.ApplianceConfigXmlFile, 3)
        Helpers.Logger.WriteMessage("(version) ApplianceId", 2)
        Helpers.Logger.WriteMessage("(version) " & VersionInventory.N_Central.Data.ApplianceId, 3)
        Helpers.Logger.WriteMessage("(version) SoapUri", 2)
        Helpers.Logger.WriteMessage("(version) " & VersionInventory.N_Central.Data.SoapUri, 3)


        Helpers.Logger.WriteMessage("(version) getting device id", 1)
        If Not VersionInventory.N_Central.Soap.Action_deviceGet() Then
            Helpers.Logger.WriteError("(version) failed", 2)
            Helpers.Logger.WriteError("(version) " & VersionInventory.N_Central.Data.LastError, 3)

            Exit Sub
        End If

        Helpers.Logger.WriteMessage("(version) DeviceId", 2)
        Helpers.Logger.WriteMessage("(version) " & VersionInventory.N_Central.Data.DeviceId, 3)
    End Sub
End Module
