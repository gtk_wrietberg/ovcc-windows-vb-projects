﻿Module modMain_trigger
    Dim timerWarningCheck As New System.Timers.Timer

    Private mThreading_WarningCheck As Threading.Thread

    Private mFailCount As Integer = 0
    Private mMaxFailCount As Integer = 2

    Public Sub Main()
        If Helpers.Processes.IsAppAlreadyRunning Then
            Application.Exit()
        End If

        Dim sError As String = ""
        If Not RegistryWriteCheck.CanIWriteToOpenToAllRegistrySection(sError) Then
            Helpers.WindowsEventLog.WriteError("CanIWriteToOpenToAllRegistrySection() reported FALSE ('" & sError & "')")
            Errortrigger.TriggerErrorPopup("Unable to write data", "The system was unable to write critical data.\nPlease contact the helpdesk.", 2, False)
            Application.Exit()
        End If

        mThreading_WarningCheck = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_WarningCheck))
        mThreading_WarningCheck.Start()
    End Sub

    Private Sub Thread_WarningCheck()
        timerWarningCheck.AutoReset = True
        timerWarningCheck.Interval = 5000
        AddHandler timerWarningCheck.Elapsed, AddressOf timerWarningCheck_Elapsed
        timerWarningCheck.Start()

        Threading.Thread.Sleep(Threading.Timeout.Infinite)
    End Sub

    Private Sub timerWarningCheck_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Dim iTrigger As Integer = 0, bProcess As Boolean, sError As String = ""

        If Not RegistryWriteCheck.CanIWriteToOpenToAllRegistrySection(sError) Then
            'Uh oh
            'Let's try and eventlog this
            Helpers.WindowsEventLog.WriteError("CanIWriteToOpenToAllRegistrySection() reported FALSE ('" & sError & "')")
        End If

        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerStarted, True)

        iTrigger = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount)

        If iTrigger > 0 Then
            'Warning needed
            If Helpers.Processes.IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogWarning) Then
                'It's already running
                Helpers.WindowsEventLog.WriteWarning("Can not trigger Warning, it´s already running!")
            Else
                Dim sUsername As String, sPassword As String, sLastError As String, sPath As String

                sPath = Helpers.Types.DoubleQuoteString(LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.FILE__WatchdogWarning)
                sUsername = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower, LPCConstants.RegistryKeys.VALUE__PuppyPowerUserName)
                sPassword = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower, LPCConstants.RegistryKeys.VALUE__PuppyPowerPassWord)

                sUsername = Helpers.XOrObfuscation.Deobfuscate(sUsername)
                sPassword = Helpers.XOrObfuscation.Deobfuscate(sPassword)

                'Try
                '    Dim sw As New IO.StreamWriter("c:\_wrtemp\run.txt", True)
                '    sw.WriteLine(New String("=", 20))
                '    sw.WriteLine(Now.ToString)
                '    sw.WriteLine(sPath)
                '    sw.WriteLine(sUsername)
                '    sw.WriteLine(sPassword)
                '    sw.WriteLine(New String("=", 20))
                '    sw.Close()
                'Catch ex As Exception

                'End Try

                'bProcess = Helpers.Processes.StartProcessAsUser(sPath, sUsername, sPassword, "", False, 0)
                'StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, bLoadUserProfile As Boolean, cmdArguments As String, sWorkingDirectory As String, waitForExit As Boolean, timeOutMs As Integer, hidden As Boolean)
                bProcess = Helpers.Processes.StartProcessAsUser(sPath, sUsername, sPassword, True, "", "", False, 0, False)

                If Not bProcess Then
                    'something went wrong when starting the warning, uh oh.
                    sLastError = Helpers.Processes.LastError

                    mFailCount += 1

                    Helpers.WindowsEventLog.WriteError("Something went wrong when starting the warning (FailCount=" & mFailCount & " ; LastError='" & sLastError & "')!")

                    If mFailCount > mMaxFailCount Then
                        'stop checking
                        timerWarningCheck.Stop()

                        Helpers.WindowsEventLog.WriteError("Giving up!")

                        'signal service to reboot
                        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailure, True)
                        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError, sLastError)
                    End If
                End If
            End If
        End If
    End Sub

    '#Region "DEBUGGING"
    '    Private mLastError As String = ""
    '    Private Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer) As Boolean
    '        WriteToLog("                         ")
    '        WriteToLog("_StartProcess")

    '        Dim pInfo As New ProcessStartInfo()

    '        pInfo.FileName = cmdLine
    '        pInfo.Arguments = cmdArguments
    '        If sUserName <> "" Then
    '            pInfo.UserName = sUserName
    '            pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
    '            pInfo.Domain = Nothing
    '        End If
    '        pInfo.UseShellExecute = False

    '        WriteToLog("pInfo.FileName = " & cmdLine)
    '        WriteToLog("pInfo.Arguments = " & cmdArguments)
    '        WriteToLog("pInfo.UserName = " & sUserName)
    '        WriteToLog("pInfo.Password = " & ConvertPasswordStringToSecureString(sPassword).ToString)


    '        Try
    '            Dim p As New Process

    '            WriteToLog("Process.Start")

    '            p = Process.Start(pInfo)

    '            WriteToLog("p.ExitCode=" & p.ExitCode.ToString)


    '            If p.Id > 0 Then
    '                'all ok
    '                WriteToLog("p.Id = " & p.Id.ToString & " -> OK")
    '            Else
    '                'wtf, not started
    '                WriteToLog("not started (pid<=0)")
    '                mLastError = "not started (pid<=0)"
    '                Return False
    '            End If

    '            If waitForExit Then
    '                Try
    '                    'This is here for processes without graphical UI
    '                    p.WaitForInputIdle()
    '                Catch ex As Exception

    '                End Try

    '                p.WaitForExit(timeOut)

    '                If p.HasExited = False Then
    '                    If p.Responding Then
    '                        p.CloseMainWindow()
    '                    Else
    '                        p.Kill()
    '                    End If

    '                    mLastError = "timed out!"
    '                    Return False
    '                End If
    '            End If
    '        Catch ex As Exception
    '            WriteToLog("not started (" & ex.Message & ")")
    '            mLastError = "not started (" & ex.Message & ")"
    '            Return False
    '        End Try


    '        Return True
    '    End Function

    '    Private Function ConvertPasswordStringToSecureString(ByVal str As String) As Security.SecureString
    '        Dim password As New Security.SecureString

    '        For Each c As Char In str.ToCharArray
    '            password.AppendChar(c)
    '        Next

    '        Return password
    '    End Function


    '    Public Sub WriteToLog(ByVal sMessage As String)
    '        Try
    '            Dim sw As New IO.StreamWriter("c:\_wrtemp\trigger.log", True)
    '            sw.WriteLine(Now.ToString & " - " & sMessage)
    '            sw.Close()
    '        Catch ex As Exception

    '        End Try
    '    End Sub

    '#End Region

End Module
