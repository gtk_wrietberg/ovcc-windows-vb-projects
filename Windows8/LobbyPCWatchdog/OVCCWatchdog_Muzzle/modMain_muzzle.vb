﻿Imports System.ServiceProcess

Module modMain
    Public Sub Main()
        Helpers.Logger.InitialiseLogger()


        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.Write("Muzzle started")


        'Kill processes and stop service, if any
        Helpers.Logger.Write("Kill processes", , 0)

        Dim listProcesses As New List(Of String)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogWarning.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogServiceEnable.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogServiceDisable.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogError.Replace(".exe", "").ToLower)

        For Each p As Process In Process.GetProcesses
            If listProcesses.IndexOf(p.ProcessName.ToLower) >= 0 Then
                Helpers.Logger.Write("killing: " & p.ProcessName, , 1)
                Try
                    p.Kill()
                    Helpers.Logger.Write("ok", , 2)
                Catch ex As Exception
                    Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    Helpers.Logger.Write(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                End Try
            End If
        Next


        Helpers.Logger.Write("Change service startup type to disabled")
        ServiceInstaller.ChangeStartupType(LPCConstants.Service.Watchdog.Name, ServiceBootFlag.Disabled)


        Helpers.Logger.Write("Stop service", , 0)
        StopService(LPCConstants.Service.Watchdog.Name)
        Threading.Thread.Sleep(5000)


        Helpers.Logger.Write("Remove autostart",, 0)
        'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\AutoAdminLogon
        Helpers.Logger.Write("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon",, 1)

        Helpers.Logger.Write("AutoAdminLogon",, 2)
        Helpers.Logger.Write("'0'",, 3)
        Helpers.Registry.SetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "AutoAdminLogon", "0")

        Helpers.Logger.Write("DefaultDomainName",, 2)
        Helpers.Logger.Write("''",, 3)
        Helpers.Registry.SetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "DefaultDomainName", "")

        Helpers.Logger.Write("DefaultUserName",, 2)
        Helpers.Logger.Write("''",, 3)
        Helpers.Registry.SetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "DefaultUserName", "")

        Helpers.Logger.Write("DefaultPassword",, 2)
        Helpers.Logger.Write("''",, 3)
        Helpers.Registry.SetValue_String("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon", "DefaultPassword", "")


        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("bye", , 1)
    End Sub

    Private Sub StopService(Name As String)
        Helpers.Logger.Write(Name, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            Helpers.Logger.Write("stopping", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            ServiceInstaller.StopService(Name)

            Threading.Thread.Sleep(15000)

            If ServiceInstaller.GetServiceStatus(Name) = ServiceControllerStatus.Stopped Then
                Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            Else
                Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Helpers.Logger.Write("service status: " & ServiceInstaller.GetServiceStatus(Name).ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If

            Threading.Thread.Sleep(5000)
        Else
            Helpers.Logger.Write("not needed", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        End If
    End Sub
End Module
