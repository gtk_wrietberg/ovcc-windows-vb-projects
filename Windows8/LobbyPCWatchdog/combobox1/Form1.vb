﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Button1.Enabled = False

        ComboBox1.Items.Clear()
        ComboBox2.Items.Clear()


        ComboBox1.AutoCompleteMode = AutoCompleteMode.Append
        ComboBox1.AutoCompleteSource = AutoCompleteSource.ListItems


        Dim l As New List(Of String)

        l.Add("ABC")
        l.Add("DEF")
        l.Add("GHI")
        l.Add("JKL")
        l.Add("MNO")
        l.Add("PQR")
        l.Add("STU")
        l.Add("VWX")
        l.Add("YZ0")

        For Each s As String In l
            ComboBox1.Items.Add(s)
            ComboBox2.Items.Add(s)
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If ComboBox1.SelectedItem Is Nothing Then
            MsgBox("nOtHiNg")
        Else
            MsgBox(ComboBox1.SelectedItem.ToString)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If ComboBox2.SelectedItem Is Nothing Then
            MsgBox("nOtHiNg")
        Else
            MsgBox(ComboBox2.SelectedItem.ToString)
        End If
    End Sub

    Private Sub ComboBox1_LostFocus(sender As Object, e As EventArgs) Handles ComboBox1.LostFocus
        If ComboBox1.SelectedItem Is Nothing Then
            Button1.Enabled = False
        End If
        If ComboBox1.SelectedIndex > -1 Then
            Button1.Enabled = True
        Else
            Button1.Enabled = False
        End If

    End Sub

    Private Sub ComboBox1_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedValueChanged
        If ComboBox1.SelectedIndex > -1 Then
            Button1.Enabled = True
        Else
            Button1.Enabled = False
        End If
    End Sub

    Private Sub ComboBox1_TextUpdate(sender As Object, e As EventArgs) Handles ComboBox1.TextUpdate
        If ComboBox1.SelectedIndex > -1 Then
            Button1.Enabled = True
        Else
            Button1.Enabled = False
        End If
    End Sub
End Class
