﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnObfuscate = New System.Windows.Forms.Button()
        Me.txtPlain = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnDeobfuscate = New System.Windows.Forms.Button()
        Me.txtObfuscated = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnObfuscate
        '
        Me.btnObfuscate.Location = New System.Drawing.Point(12, 101)
        Me.btnObfuscate.Name = "btnObfuscate"
        Me.btnObfuscate.Size = New System.Drawing.Size(349, 23)
        Me.btnObfuscate.TabIndex = 0
        Me.btnObfuscate.Text = "Obfuscate"
        Me.btnObfuscate.UseVisualStyleBackColor = True
        '
        'txtPlain
        '
        Me.txtPlain.Location = New System.Drawing.Point(12, 9)
        Me.txtPlain.Multiline = True
        Me.txtPlain.Name = "txtPlain"
        Me.txtPlain.Size = New System.Drawing.Size(776, 86)
        Me.txtPlain.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 13)
        Me.Label1.TabIndex = 2
        '
        'btnDeobfuscate
        '
        Me.btnDeobfuscate.Location = New System.Drawing.Point(439, 101)
        Me.btnDeobfuscate.Name = "btnDeobfuscate"
        Me.btnDeobfuscate.Size = New System.Drawing.Size(349, 23)
        Me.btnDeobfuscate.TabIndex = 3
        Me.btnDeobfuscate.Text = "Deobfuscate"
        Me.btnDeobfuscate.UseVisualStyleBackColor = True
        '
        'txtObfuscated
        '
        Me.txtObfuscated.Location = New System.Drawing.Point(12, 130)
        Me.txtObfuscated.Multiline = True
        Me.txtObfuscated.Name = "txtObfuscated"
        Me.txtObfuscated.Size = New System.Drawing.Size(776, 86)
        Me.txtObfuscated.TabIndex = 4
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 227)
        Me.Controls.Add(Me.txtObfuscated)
        Me.Controls.Add(Me.btnDeobfuscate)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPlain)
        Me.Controls.Add(Me.btnObfuscate)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.ShowIcon = False
        Me.Text = "Obfuscator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnObfuscate As Button
    Friend WithEvents txtPlain As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnDeobfuscate As Button
    Friend WithEvents txtObfuscated As TextBox
End Class
