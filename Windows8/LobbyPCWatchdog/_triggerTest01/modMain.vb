﻿Module modMain
    Dim timerWarningCheck As New System.Timers.Timer

    Private mThreading_WarningCheck As Threading.Thread

    Private mFailCount As Integer = 0
    Private mMaxFailCount As Integer = 2

    Public Sub Main()
        mThreading_WarningCheck = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_WarningCheck))
        mThreading_WarningCheck.Start()
    End Sub

    Private Sub Thread_WarningCheck()
        timerWarningCheck.AutoReset = True
        timerWarningCheck.Interval = 10000
        AddHandler timerWarningCheck.Elapsed, AddressOf timerWarningCheck_Elapsed
        timerWarningCheck.Start()

        Threading.Thread.Sleep(Threading.Timeout.Infinite)
    End Sub

    Private Sub timerWarningCheck_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Dim iTrigger As Integer = 0
        Dim sTrigger As String = ""

        iTrigger = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount)
        sTrigger = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount)
        MsgBox(LPCConstants.RegistryKeys.KEY__Watchdog_Warning & "!" & LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount & " = " & iTrigger.ToString & " (" & sTrigger & ")")

        'If Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount) > 0 Then
        '    'Warning needed
        '    Dim sUsername As String, sPassword As String, sLastError As String

        '    sUsername = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower, LPCConstants.RegistryKeys.VALUE__PuppyPowerUserName)
        '    sPassword = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower, LPCConstants.RegistryKeys.VALUE__PuppyPowerPassWord)

        '    sUsername = Helpers.XOrObfuscation.Deobfuscate(sUsername)
        '    sPassword = Helpers.XOrObfuscation.Deobfuscate(sPassword)


        '    If Not Helpers.Processes.StartProcessAsUser(LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.FILE__WatchdogWarning, sUsername, sPassword, "", False, 0) Then
        '        'something went wrong when starting the warning, uh oh.
        '        sLastError = Helpers.Processes.LastError

        '        mFailCount += 1
        '        If mFailCount > mMaxFailCount Then
        '            'stop checking
        '            timerWarningCheck.Stop()

        '            'signal service to reboot
        '            Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailure, True)
        '            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError, sLastError)
        '        End If
        '    End If
        'End If
    End Sub
End Module
