﻿Public Class frmMain_disable

    Private Sub btnVerify_Click(sender As Object, e As EventArgs) Handles btnVerify.Click
        Verify()
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Helpers.Processes.IsAppAlreadyRunning Then
            Me.Close()
        End If

        Me.TransparencyKey = Color.Lime

        Helpers.Forms.TopMost(Me.Handle, True)


        If LPCConstants.Misc.DisablePassword.Equals("") Then
            ToggleControls(False)

            Disable()
        End If
    End Sub

    Private Sub ToggleControls(b As Boolean)
        txtPassword.Enabled = b
        btnVerify.Enabled = b
    End Sub

    Private Sub Verify()
        ToggleControls(False)

        If txtPassword.Text = LPCConstants.Misc.DisablePassword Then
            Disable()
        Else
            Failed()
        End If
    End Sub

    Private Sub Disable()
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Disabled, LPCConstants.RegistryKeys.VALUE__WatchdogDisabled, Helpers.XOrObfuscation.Obfuscate(LPCConstants.Misc.MagicalDisableString))

        MsgBox("ok, disabled", MsgBoxStyle.OkOnly, Application.ProductName)

        ExitApplication()
    End Sub

    Private Sub Failed(Optional msg As String = "incorrect password, sorry")
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Disabled, LPCConstants.RegistryKeys.VALUE__WatchdogDisabled, "")

        MsgBox(msg, MsgBoxStyle.OkOnly Or MsgBoxStyle.Exclamation, Application.ProductName)

        ExitApplication()
    End Sub

    Private Sub ExitApplication()
        Me.Close()
    End Sub

    Private Sub txtPassword_KeyDown(sender As Object, e As KeyEventArgs) Handles txtPassword.KeyDown
        If e.KeyCode = Keys.Enter Then
            Verify()
        End If
    End Sub

    Private Sub pnlWait_Paint(sender As Object, e As PaintEventArgs) Handles pnlWait.Paint

    End Sub
End Class