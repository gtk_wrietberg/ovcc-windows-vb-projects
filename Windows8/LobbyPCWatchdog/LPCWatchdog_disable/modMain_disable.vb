﻿Module modMain_disable
    Public Sub Main()
        If Helpers.Windows.IsRunningAsAdmin Then
            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings, LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDisabled, Helpers.XOrObfuscation.Obfuscate(LPCConstants.Misc.MagicalDisableString))

            Environment.Exit(0)
        Else
            Environment.Exit(1)
        End If
    End Sub
End Module
