﻿Public Class Integrity
    Public Shared Sub FileAccess()
        Dim sFile_LastActivityFile As String = ""

        Helpers.Logger.WriteRelative("check file", , 1)
        sFile_LastActivityFile = IO.Path.Combine(LPCConstants.FilesAndFolders.FILE__LastActivityTempFilePath, LPCConstants.FilesAndFolders.FILE__LastActivityTempFileName)

        Helpers.Logger.WriteRelative(sFile_LastActivityFile, , 2)
        If Not IO.File.Exists(sFile_LastActivityFile) Then
            Helpers.Logger.WriteRelative("doesn't exist, creating", , 3)
            Try
                Dim sWriter As New IO.StreamWriter(sFile_LastActivityFile, False)
                sWriter.Write("<lastactivity><date>2517393587</date><idle>0</idle></lastactivity>") ' 2517393587 = 2049-10-09T11:59:47+00:00
                sWriter.Close()
            Catch ex As Exception
                Helpers.Logger.WriteRelative("error", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End Try
        End If


        If IO.File.Exists(sFile_LastActivityFile) Then
            Helpers.Logger.WriteRelative("updating permissions", , 3)
            If Helpers.FilesAndFolders.Permissions.FileSecurity_REPLACE__Full_Everyone(sFile_LastActivityFile) Then
                Helpers.Logger.WriteRelative("ok", , 4)
            Else
                Helpers.Logger.WriteRelative("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                Helpers.Logger.WriteRelative(Helpers.Errors.GetLast(False), Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End If
        Else
            Helpers.Logger.WriteRelative("not found!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
    End Sub

    Public Shared Sub SiteKioskProfilePath()
        Helpers.Logger.WriteMessageRelative("checking ", 1)

        Dim sProfilePath As String = ""

        Helpers.Logger.WriteMessageRelative("profile path", 2)

        If SitekioskUserProfile.GetProfileFolder(sProfilePath) Then
            Helpers.Logger.WriteMessageRelative(sProfilePath, 3)
        Else
            Helpers.Logger.WriteMessageRelative("not found in registry, reverting to default", 3)
            sProfilePath = LPCConstants.FilesAndFolders.PATH__DefaultSitekioskProfilePath
        End If

        If IO.Directory.Exists(sProfilePath) Then
            Helpers.Logger.WriteMessageRelative("ok, exists", 4)
        Else
            Helpers.Logger.WriteErrorRelative("uh oh, does not exist!", 4)

            Exit Sub
        End If

        'check all places that use sitekiosk profile path ; sitekiosk, file explorer, skype, etc.
        Try
            Dim sSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()
            Dim bConfigChanged As Boolean = False

            Helpers.Logger.WriteMessageRelative("checking " & sSkCfgFile, 2)


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument
            xmlSkCfg.Load(sSkCfgFile)

            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)


            Dim xmlNode_DownloadManager As Xml.XmlNode
            Dim xmlNode_DownloadFolder As Xml.XmlNode
            Dim xmlNode_DeleteDirectories As Xml.XmlNode
            Dim xmlNode_Dirs As Xml.XmlNodeList
            Dim xmlNode_Filemanager As Xml.XmlNode
            Dim xmlNode_VisiblePaths As Xml.XmlNode
            Dim xmlNode_Urls As Xml.XmlNodeList
            Dim sValue0 As String = "", sValue1 As String = ""

            xmlNode_DownloadManager = xmlNode_Root.SelectSingleNode("sk:download-manager", ns)

            Helpers.Logger.WriteMessageRelative("download-manager", 3)

            Helpers.Logger.WriteMessageRelative("download-folder", 4)
            xmlNode_DownloadFolder = xmlNode_DownloadManager.SelectSingleNode("sk:download-folder", ns)
            sValue0 = xmlNode_DownloadFolder.InnerText
            sValue1 = _CheckAndCorrectPath(sProfilePath, sValue0)
            If Not sValue0.Equals(sValue1) Then
                xmlNode_DownloadFolder.InnerText = sValue1

                Helpers.Logger.WriteMessageRelative(sValue0 & " --> " & sValue1, 5)

                bConfigChanged = True
            Else
                'ok
                Helpers.Logger.WriteMessageRelative("ok", 5)
            End If


            Helpers.Logger.WriteMessageRelative("delete-directories", 4)
            xmlNode_DeleteDirectories = xmlNode_DownloadManager.SelectSingleNode("sk:delete-directories", ns)
            xmlNode_Dirs = xmlNode_DeleteDirectories.SelectNodes("sk:dir", ns)

            For Each xmlNode_Dir As Xml.XmlNode In xmlNode_Dirs
                Helpers.Logger.WriteMessageRelative("dir", 5)

                sValue0 = xmlNode_Dir.InnerText
                sValue1 = _CheckAndCorrectPath(sProfilePath, sValue0)

                If Not sValue0.Equals(sValue1) Then
                    xmlNode_Dir.InnerText = sValue1

                    Helpers.Logger.WriteMessageRelative(sValue0 & " --> " & sValue1, 6)

                    bConfigChanged = True
                Else
                    'ok
                    Helpers.Logger.WriteMessageRelative("ok", 6)
                End If
            Next


            Helpers.Logger.WriteMessageRelative("filemanager", 3)
            Helpers.Logger.WriteMessageRelative("visible-paths", 4)


            xmlNode_Filemanager = xmlNode_Root.SelectSingleNode("sk:filemanager", ns)
            xmlNode_VisiblePaths = xmlNode_Filemanager.SelectSingleNode("sk:visible-paths", ns)
            xmlNode_Urls = xmlNode_VisiblePaths.SelectNodes("sk:url", ns)

            For Each xmlNode_Url As Xml.XmlNode In xmlNode_Urls
                Helpers.Logger.WriteMessageRelative("url", 5)

                sValue0 = xmlNode_Url.InnerText
                sValue1 = _CheckAndCorrectPath(sProfilePath, sValue0)

                If Not sValue0.Equals(sValue1) Then
                    xmlNode_Url.InnerText = sValue1

                    Helpers.Logger.WriteMessageRelative(sValue0 & " --> " & sValue1, 6)

                    bConfigChanged = True
                Else
                    'ok
                    Helpers.Logger.WriteMessageRelative("ok", 6)
                End If
            Next

            If bConfigChanged Then
                Helpers.Logger.WriteRelative("backup", , 3)

                Try
                    Dim sBackupFolder As String = LPCConstants.FilesAndFolders.FOLDER__Backup

                    sBackupFolder = sBackupFolder.Replace("%%Application.ProductName%%", My.Application.Info.ProductName)
                    sBackupFolder = sBackupFolder.Replace("%%Application.ProductVersion%%", My.Application.Info.Version.ToString)
                    sBackupFolder = sBackupFolder.Replace("%%Backup.Date%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))

                    Helpers.Logger.WriteRelative("destination", , 4)
                    Helpers.Logger.WriteRelative(sBackupFolder, , 5)

                    IO.Directory.CreateDirectory(sBackupFolder)

                    Helpers.FilesAndFolders.Backup.File(sSkCfgFile, sBackupFolder, 3, My.Application.Info.ProductName)
                Catch ex As Exception
                    Helpers.Logger.WriteErrorRelative("FAILED", 4)
                    Helpers.Logger.WriteErrorRelative(ex.Message, 5)
                End Try


                Helpers.Logger.WriteMessageRelative("writing", 3)
                Helpers.Logger.WriteMessageRelative(sSkCfgFile, 4)
                xmlSkCfg.Save(sSkCfgFile)
                Helpers.Logger.WriteMessageRelative("ok", 5)
            Else
                Helpers.Logger.WriteWarningRelative("skipped, nothing was changed", 3)
            End If


            Dim sFileExplorerRegRoot As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\FileExplorer"
            Dim sFileExplorerPathTemplate As String = "Folder_%%_Path"
            Dim sFileExplorerPath As String = ""

            For i As Integer = 1 To 10
                If i < 10 Then
                    sFileExplorerPath = sFileExplorerPathTemplate.Replace("%%", "0" & i.ToString)
                Else
                    sFileExplorerPath = sFileExplorerPathTemplate.Replace("%%", i.ToString)
                End If

                sValue0 = Helpers.Registry.GetValue_String(sFileExplorerRegRoot, sFileExplorerPath)
                sValue1 = _CheckAndCorrectPath(sProfilePath, sValue0)

                If Not sValue0.Equals(sValue1) Then
                    Helpers.Registry.SetValue_String(sFileExplorerRegRoot, sFileExplorerPath, sValue1)

                    Helpers.Logger.WriteMessageRelative(sValue0 & " --> " & sValue1, 6)

                    bConfigChanged = True
                Else
                    'ok
                    Helpers.Logger.WriteMessageRelative("ok", 6)
                End If
            Next


        Catch ex As Exception

        End Try
    End Sub

    Private Shared Function _CheckAndCorrectPath(SiteKioskProfilePath As String, CurrentPath As String) As String
        Dim aPath As String(), uValue As String = "", pValue As String = ""

        aPath = CurrentPath.Split("\")

        If aPath.Count < 3 Then
            'skip
            Return CurrentPath
        Else
            uValue = aPath(1).ToLower
            pValue = String.Join("\", aPath.Take(3))
        End If

        If Not uValue.Equals("Users".ToLower) Then
            'not a Users folder, abort
            Return CurrentPath
        End If

        If pValue.Equals(SiteKioskProfilePath) Then
            'all ok
            Return CurrentPath
        Else
            Return CurrentPath.Replace(pValue, SiteKioskProfilePath).Replace("\\", "\")
        End If
    End Function

    Private Sub SiteKioskProfileBroken()

    End Sub

    Private Sub ThemeSettings()
        Try
            Dim sSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()
            Dim bConfigChanged As Boolean = False

            Helpers.Logger.WriteMessageRelative("checking " & sSkCfgFile, 1)


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument
            xmlSkCfg.Load(sSkCfgFile)

            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)


            Dim xmlNode_Global As Xml.XmlNode
            Dim xmlNode_IsUsingChrome As Xml.XmlNode
            Dim sValue As String = ""

            xmlNode_Global = xmlNode_Root.SelectSingleNode("sk:global", ns)
            xmlNode_IsUsingChrome = xmlNode_Global.SelectSingleNode("sk:is-using-chrome", ns)

            If Not xmlNode_IsUsingChrome Is Nothing Then
                sValue = xmlNode_IsUsingChrome.InnerText

                If sValue = "true" Then
                    Helpers.Logger.WriteWarningRelative("(integrity - theme) uh oh", 2)
                    Helpers.Logger.WriteWarningRelative("(integrity - theme) is-using-chrome=true", 3)

                    xmlNode_IsUsingChrome.InnerText = "false"

                    bConfigChanged = True
                End If
            Else
                Helpers.Logger.WriteMessageRelative("(integrity - theme) is-using-chrome not found", 2)
            End If


            Dim xmlNode_SiteSkin As Xml.XmlNode
            Dim xmlNode_DefaultSkin As Xml.XmlNode

            xmlNode_SiteSkin = xmlNode_Root.SelectSingleNode("sk:siteskin", ns)
            xmlNode_DefaultSkin = xmlNode_SiteSkin.SelectSingleNode("sk:default-skin", ns)

            sValue = xmlNode_DefaultSkin.InnerText

            If Not sValue.Equals(LPCGlobals.Settings.Integrity.DefaultSkin) Then
                Helpers.Logger.WriteWarningRelative("(integrity - theme) uh oh", 2)
                Helpers.Logger.WriteWarningRelative("(integrity - theme) default-skin='" & sValue & "'", 3)

                xmlNode_DefaultSkin.InnerText = LPCGlobals.Settings.Integrity.DefaultSkin

                bConfigChanged = True
            End If


            '---------------------------------------------------
            Helpers.Logger.WriteMessageRelative("saving", 2)
            If bConfigChanged Then
                Helpers.Logger.WriteRelative("backup", , 3)

                Try
                    Dim sBackupFolder As String = LPCConstants.FilesAndFolders.FOLDER__Backup

                    sBackupFolder = sBackupFolder.Replace("%%Application.ProductName%%", My.Application.Info.ProductName)
                    sBackupFolder = sBackupFolder.Replace("%%Application.ProductVersion%%", My.Application.Info.Version.ToString)
                    sBackupFolder = sBackupFolder.Replace("%%Backup.Date%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))

                    Helpers.Logger.WriteRelative("destination", , 4)
                    Helpers.Logger.WriteRelative(sBackupFolder, , 5)

                    IO.Directory.CreateDirectory(sBackupFolder)

                    Helpers.FilesAndFolders.Backup.File(sSkCfgFile, sBackupFolder, 3, My.Application.Info.ProductName)
                Catch ex As Exception
                    Helpers.Logger.WriteErrorRelative("FAILED", 4)
                    Helpers.Logger.WriteErrorRelative(ex.Message, 5)
                End Try


                Helpers.Logger.WriteMessageRelative("writing", 3)
                Helpers.Logger.WriteMessageRelative(sSkCfgFile, 4)
                xmlSkCfg.Save(sSkCfgFile)
                Helpers.Logger.WriteMessageRelative("ok", 5)
            Else
                Helpers.Logger.WriteWarningRelative("skipped, nothing was changed", 3)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteMessageRelative("(integrity - theme) exception", 1)
            Helpers.Logger.WriteMessageRelative("(integrity - theme) " & ex.Message, 2)
        End Try
    End Sub
End Class
