﻿Module Main
    Public Sub Main()

    End Sub

    Public Sub Check_Integrity_Main()
        Helpers.Logger.Write("(integrity) starting thread", , 0)
        If Not LPCGlobals.Settings.CheckIntegrity Then
            Helpers.Logger.Write("(integrity) disabled, so ignored", , 1)

            Exit Sub
        End If


        Helpers.Logger.Write("(integrity) file access", , 1)

        If LPCGlobals.Settings.Integrity.CheckFileAccess Then
            '_Integrity_FileAccess()
        Else
            Helpers.Logger.Write("(integrity) disabled", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If


        Helpers.Logger.Write("(integrity) theme settings", , 1)

        If LPCGlobals.Settings.Integrity.CheckThemeSettings Then
            '_Integrity_ThemeSettings()
        Else
            Helpers.Logger.Write("(integrity) disabled", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If


        Helpers.Logger.Write("(integrity) sitekiosk profile", , 1)

        If LPCGlobals.Settings.Integrity.CheckSiteKioskProfileBroken Then
            '_Integrity_SiteKioskProfileBroken()
        Else
            Helpers.Logger.Write("(integrity) disabled", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If


        Helpers.Logger.Write("(integrity) sitekiosk profile path", , 1)

        If LPCGlobals.Settings.Integrity.CheckSiteKioskProfilePath Then
            '_Integrity_SiteKioskProfilePath()
        Else
            Helpers.Logger.Write("(integrity) disabled", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If
    End Sub
End Module
