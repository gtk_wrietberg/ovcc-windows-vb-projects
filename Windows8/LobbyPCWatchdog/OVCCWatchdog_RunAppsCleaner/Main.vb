﻿Module Main
    Public Sub Main()
        Helpers.Logger.InitialiseLogger()


        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("started", , 0)


        Helpers.Logger.Write("fix helper apps auto run", , 0)
        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger, , 1)
        Helpers.Logger.Write(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger), , 2)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger))
        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity, , 1)
        Helpers.Logger.Write(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity), , 2)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity))
        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogError, , 1)
        Helpers.Logger.Write(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogError), , 2)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogError,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogError))


    End Sub
End Module
