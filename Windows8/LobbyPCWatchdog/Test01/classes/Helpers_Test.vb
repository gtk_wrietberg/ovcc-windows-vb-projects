﻿Public Class Helpers_Test
    Public Class Types
        Public Structure TryReturnStruct
            Public Success As Boolean
            Public StringValue As String
            Public IntegerValue As Integer
            Public LongValue As Long
            Public DoubleValue As Double
        End Structure

        Public Shared Function Cast_safe(ByVal o As Object) As TryReturnStruct
            Dim ret As New TryReturnStruct

            ret.Success = False
            ret.StringValue = Nothing
            ret.IntegerValue = Nothing
            ret.LongValue = Nothing
            ret.DoubleValue = Nothing

            If TypeOf o Is Double Then
                ret.Success = True

            End If

            Return ret
        End Function

        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function


        Public Shared Function RandomString(Length As Integer, Optional CharsToChooseFrom As String = "") As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function
    End Class

End Class
