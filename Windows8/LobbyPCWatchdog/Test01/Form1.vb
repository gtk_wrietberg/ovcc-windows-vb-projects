﻿Public Class Form1
    Private oLPCState As LPCState

    Public Structure LASTINPUTINFO
        Public cbSize As Integer
        Public dwTime As Integer
    End Structure

    Private mLastInputInfo As LASTINPUTINFO
    Private mIdleTime As Integer = -1
    Private Declare Function GetTickCount Lib "kernel32" () As Long
    Private Declare Function GetLastInputInfo Lib "User32.dll" (ByRef lii As LASTINPUTINFO) As Boolean

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'If oLPCState.IsUserLoggedIn(TextBox1.Text) Then
        '    Label1.Text = "user '" & TextBox1.Text & "' is logged in"
        'Else
        '    Label1.Text = "user '" & TextBox1.Text & "' is not logged in"
        'End If
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'oLPCState = New LPCState
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'LPCGlobals.Objects.InitialiseLogger()

        'MsgBox(MyLogger.MESSAGE_SENDER.WATCHDOG_LASTACTIVITY.ToString)

        Dim a As LPCState.LOBBYPC_STATE

        a = a Or LPCState.LOBBYPC_STATE.GENERAL_FAILURE
        a = a Or LPCState.LOBBYPC_STATE.EXPLORER_PROCESS_RUNNING

        'MsgBox(a)
        'Dim iCount As Integer
        'iCount = [Enum].GetValues(GetType(LPCState.LOBBYPC_STATE)).Cast(Of LPCState.LOBBYPC_STATE)().Max()

        'For i As Integer = 0 To iCount
        '    [Enum].
        'Next()

        Dim aValues() As LPCState.LOBBYPC_STATE, b As Boolean
        aValues = [Enum].GetValues(GetType(LPCState.LOBBYPC_STATE)).Cast(Of LPCState.LOBBYPC_STATE)()

        For i As Integer = 0 To aValues.Count - 1
            b = False

            If a And aValues(i) Then
                b = True
            End If

            MsgBox(i & " ; " & aValues(i) & " ; " & aValues(i).ToString & " ; " & b.ToString)
        Next
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Enabled = True
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        MsgBox(Helpers.Types.RandomString(16))
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        MsgBox(Helpers.XOrObfuscation.Deobfuscate(TextBox2.Text))
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        mLastInputInfo = New LASTINPUTINFO
        mLastInputInfo.cbSize = Runtime.InteropServices.Marshal.SizeOf(mLastInputInfo)

        ' get last input info from Windows
        If GetLastInputInfo(mLastInputInfo) Then     ' if we have an input info     
            ' compute idle time
            mIdleTime = (GetTickCount() - mLastInputInfo.dwTime)
        End If

        TextBox3.Text = "Idle: " & mIdleTime.ToString
    End Sub
End Class
