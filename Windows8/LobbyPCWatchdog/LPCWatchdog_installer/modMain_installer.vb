﻿Module modMain_installer
    Private mStartServiceAfterInstallation As Boolean = False

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()


        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.Write("Installer started")


        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals("--start-service-after-installation") Then
                mStartServiceAfterInstallation = True
            End If
        Next


        'Kill processes and stop service, if any
        Helpers.Logger.Write("Kill processes", , 0)

        Dim listProcesses As New List(Of String)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogWarning.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogServiceEnable.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogServiceDisable.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogError.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogMuzzle.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogEmergencyDeployApp.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__OLD_WatchdogService.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__OLD_WatchdogController.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__OLD_WatchdogWarning.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__OLD_WatchdogTrigger.Replace(".exe", "").ToLower)

        For Each p As Process In Process.GetProcesses
            If listProcesses.IndexOf(p.ProcessName.ToLower) >= 0 Then
                Helpers.Logger.Write("killing: " & p.ProcessName, , 1)
                Try
                    p.Kill()
                    Helpers.Logger.Write("ok", , 2)
                Catch ex As Exception
                    Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    Helpers.Logger.Write(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                End Try
            End If
        Next

        Helpers.Logger.Write("Stop OLD service", , 0)
        StopService(LPCConstants.Service.Old.Name)
        'Threading.Thread.Sleep(15000)

        Helpers.Logger.Write("Stop services", , 0)
        StopService(LPCConstants.Service.Watchdog.Name)
        StopService(LPCConstants.Service.EmergencyDeploy.Name)
        StopService(LPCConstants.Service.WatchdogWatchdog.Name)
        'Threading.Thread.Sleep(15000)

        Helpers.Logger.Write("Disable deprecated services", , 0)
        DisableService(LPCConstants.Service.Old.Name)
        DisableService(LPCConstants.Service.EmergencyDeploy.Name)
        DisableService(LPCConstants.Service.WatchdogWatchdog.Name)


        'Copy files
        '------------------------------------------------------
        Dim sBackupFolder As String = ""
        Dim dDate As Date = Now()

        sBackupFolder = LPCConstants.FilesAndFolders.FOLDER__Backup

        sBackupFolder = sBackupFolder.Replace("%%Application.ProductName%%", Application.ProductName)
        sBackupFolder = sBackupFolder.Replace("%%Application.ProductVersion%%", Application.ProductVersion)
        sBackupFolder = sBackupFolder.Replace("%%Backup.Date%%", dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))


        Dim oCopyFiles As CopyFiles = New CopyFiles
        oCopyFiles.SuppressCopyLogging = False
        oCopyFiles.BackupDirectory = sBackupFolder

        oCopyFiles.SourceDirectory = "files"
        oCopyFiles.DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder()

        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()


        'Prepare registry keys
        Dim sTmp As String
        Helpers.Logger.Write("Create registry keys", , 0)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__LastState, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__LastState.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog,
                                          LPCConstants.RegistryKeys.VALUE__LastState,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__LastState)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__DisabledUntilOVCCStart, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__DisabledUntilOVCCStart.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog,
                                          LPCConstants.RegistryKeys.VALUE__DisabledUntilOVCCStart,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__DisabledUntilOVCCStart)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_Settings, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDebug, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsDebug.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDebug,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsDebug)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTestMode, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsTestMode.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTestMode,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsTestMode)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsSkipFirstRunDelay, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsSkipFirstRunDelay.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsSkipFirstRunDelay,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsSkipFirstRunDelay)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckFoRemoteConnection, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsCheckFoRemoteConnection.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckFoRemoteConnection,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsCheckFoRemoteConnection)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsPlaysound, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsPlaysound.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsPlaysound,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsPlaysound)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsLogMeInThreshold, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsLogMeInThreshold.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsLogMeInThreshold,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsLogMeInThreshold)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTakeControlThreshold, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsTakeControlThreshold.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTakeControlThreshold,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsTakeControlThreshold)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogs, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsZipOldLogs.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogs,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsZipOldLogs)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogsThreshold, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsZipOldLogsThreshold.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogsThreshold,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsZipOldLogsThreshold)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckIntegrity, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsCheckIntegrity.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckIntegrity,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsCheckIntegrity)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsVersionInventory, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsVersionInventory.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsVersionInventory,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsVersionInventory)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsMuzzled, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsMuzzled.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsMuzzled,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsMuzzled)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsConsecutiveRebootsMax, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsConsecutiveRebootsMax.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsConsecutiveRebootsMax,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsConsecutiveRebootsMax)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsEnableWatchdogWatchdog, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsEnableWatchdogWatchdog.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsEnableWatchdogWatchdog,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsEnableWatchdogWatchdog)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckFileAccess, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckFileAccess.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckFileAccess,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckFileAccess)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckEmergencyConfig, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckEmergencyConfig.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckEmergencyConfig,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckEmergencyConfig)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckThemeSettings, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckThemeSettings.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckThemeSettings,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckThemeSettings)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckSiteKioskProfileBroken, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckSiteKioskProfileBroken.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckSiteKioskProfileBroken,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckSiteKioskProfileBroken)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckSiteKioskProfilePath, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckSiteKioskProfilePath.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckSiteKioskProfilePath,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckSiteKioskProfilePath)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_DefaultSkin, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_DefaultSkin.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_DefaultSkin,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_DefaultSkin)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_EmergencyConfig, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfig.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_EmergencyConfig,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfig)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_EmergencyConfigCount, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfigCount.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_EmergencyConfigCount,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfigCount)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_Logging, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__CurrentLogfilePath, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__CurrentLogfilePath.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Logging,
                                         LPCConstants.RegistryKeys.VALUE__CurrentLogfilePath,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__CurrentLogfilePath)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__CurrentLogfileName, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__CurrentLogfileName.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Logging,
                                         LPCConstants.RegistryKeys.VALUE__CurrentLogfileName,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__CurrentLogfileName)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll, , 1)
        If Helpers.Registry.CreateKey(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll) Then
            Helpers.Logger.Write("created", , 2)
            Helpers.Logger.Write("make sure acl is in canonical form", , 2)
            Helpers.Logger.Write(RegistryWriteCheck.CanonicalizeDacl(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll), , 3)

            Helpers.Logger.Write("open to everyone", , 2)
            Helpers.Logger.Write(RegistryWriteCheck.AllowAllForEveryone(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll), , 3)
        Else
            Helpers.Logger.Write("FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If
        sTmp = Helpers.Types.RandomString(32)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheck, , 2)
        Helpers.Logger.Write(sTmp, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll,
                                         LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheck,
                                         sTmp)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_Disabled, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogDisabled, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogDisabled.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Disabled,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogDisabled,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogDisabled)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_Error, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogError_Timestamp, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Timestamp, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error,
                                         LPCConstants.RegistryKeys.VALUE__WatchdogError_Timestamp,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Timestamp)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogError_Title, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Title.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error,
                                         LPCConstants.RegistryKeys.VALUE__WatchdogError_Title,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Title)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogError_Description, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Description.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error,
                                         LPCConstants.RegistryKeys.VALUE__WatchdogError_Description,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Description)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogError_Severity, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Severity.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Error,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogError_Severity,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Severity)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogError_RebootOption, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_RebootOption.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Error,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogError_RebootOption,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_RebootOption)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_ErrorHistory, , 1)

        For i As Integer = 0 To LPCConstants.RegistryKeys.VALUE__WatchdogError_History.Count - 1
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogError_History(i), , 2)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_History, , 3)
            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_ErrorHistory,
                                             LPCConstants.RegistryKeys.VALUE__WatchdogError_History(i),
                                             LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_History)
        Next


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerStarted, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupTriggerStarted.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning,
                                          LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerStarted,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupTriggerStarted)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupTriggerCount.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning,
                                          LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupTriggerCount)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupTestmode.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning,
                                          LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupTestmode)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WarningPopupLoose, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupLoose.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning,
                                          LPCConstants.RegistryKeys.VALUE__WarningPopupLoose,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupLoose)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WarningPopupFailure, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupFailure.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning,
                                          LPCConstants.RegistryKeys.VALUE__WarningPopupFailure,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupFailure)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupFailureLastError.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning,
                                         LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupFailureLastError)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WarningPopupCountdown, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupCountdown.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning,
                                          LPCConstants.RegistryKeys.VALUE__WarningPopupCountdown,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupCountdown)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WarningPopupTimestamp, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupTimestamp.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning,
                                          LPCConstants.RegistryKeys.VALUE__WarningPopupTimestamp,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WarningPopupTimestamp)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_LastActivity, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__LastActivity, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__LastActivity.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_LastActivity,
                                          LPCConstants.RegistryKeys.VALUE__LastActivity,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__LastActivity)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__AdminModeActive, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__AdminModeActive.ToString, , 3)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode,
                                          LPCConstants.RegistryKeys.VALUE__AdminModeActive,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__AdminModeActive)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__AdminModeActiveTime, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__AdminModeActiveTime.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode,
                                          LPCConstants.RegistryKeys.VALUE__AdminModeActiveTime,
                                          LPCConstants.RegistryKeys.DEFAULTVALUE__AdminModeActiveTime)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Puppypower, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__PuppyPowerUserName, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__PuppyPowerUserName.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower,
                                         LPCConstants.RegistryKeys.VALUE__PuppyPowerUserName,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__PuppyPowerUserName)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__PuppyPowerPassWord, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__PuppyPowerPassWord.ToString, , 3)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower,
                                         LPCConstants.RegistryKeys.VALUE__PuppyPowerPassWord,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__PuppyPowerPassWord)


        Helpers.Logger.Write(LPCConstants.RegistryKeys.KEY__Reboot, , 1)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__RebootCount, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__RebootCount.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Reboot,
                                         LPCConstants.RegistryKeys.VALUE__RebootCount,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__RebootCount)

        Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__RebootLast, , 2)
        Helpers.Logger.Write(LPCConstants.RegistryKeys.DEFAULTVALUE__RebootLast.ToString, , 3)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Reboot,
                                         LPCConstants.RegistryKeys.VALUE__RebootLast,
                                         LPCConstants.RegistryKeys.DEFAULTVALUE__RebootLast)



        'Create LobbyPC Puppy admin user
        Helpers.Logger.Write("Create LobbyPC Puppy Power admin user", , 0)

        Dim sUsername As String = LPCConstants.UsersAndPasswords.USERNAME__LobbyPCPuppy
        Dim sPassword As String = Helpers.Types.RandomString(12)
        Dim sRet As String = "", sError As String = ""

        Helpers.Logger.Write("details", , 1)
        Helpers.Logger.Write("username", , 2)
        Helpers.Logger.Write(sUsername, , 3)
        Helpers.Logger.Write("password", , 2)
        Helpers.Logger.Write("*** (yes, that's the password, three asterisks)", , 3)

        If WindowsUserManagement.DoesUserExist(sUsername, sError) Then
            Helpers.Logger.Write("already exists", , 1)
            Helpers.Logger.Write("updating password", , 1)
            sRet = WindowsUserManagement.ChangePassword(sUsername, sPassword)
            Helpers.Logger.Write(sRet, , 2)

            Helpers.Logger.Write("adding to admin group", , 1)
            sRet = WindowsUserManagement.PromoteUserToAdmin(sUsername)
            Helpers.Logger.Write(sRet, , 2)
        Else
            If Not sError.Equals("") Then
                Helpers.Logger.Write("something went wrong while checking if user exists", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                Helpers.Logger.Write(sError, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If

            Helpers.Logger.Write("creating", , 1)
            sRet = WindowsUserManagement.AddAdminUser(sUsername, "OVCC Puppy Powerrrrrrrrrrrrrr", sPassword)
            Helpers.Logger.Write(sRet, , 2)
        End If



        Helpers.Logger.Write("hiding from Windows logon screen", , 1)
        sRet = WindowsUserManagement.HideUserInLogonScreen(sUsername)
        Helpers.Logger.Write(sRet, , 2)

        Helpers.Logger.Write("removing password expiry", , 1)
        sRet = WindowsUserManagement.RemovePasswordExpiry(sUsername)
        Helpers.Logger.Write(sRet, , 2)

        Helpers.Logger.Write("saving", , 1)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower,
                                         LPCConstants.RegistryKeys.VALUE__PuppyPowerUserName,
                                         Helpers.XOrObfuscation.Obfuscate(sUsername))
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower,
                                         LPCConstants.RegistryKeys.VALUE__PuppyPowerPassWord,
                                         Helpers.XOrObfuscation.Obfuscate(sPassword))


        'Install service part
        Helpers.Logger.Write("Install services", , 0)
        InstallService(LPCConstants.Service.Watchdog.Name,
                       LPCConstants.Service.Watchdog.DisplayName,
                       LPCConstants.Service.Watchdog.Description,
                       LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.FILE__WatchdogService)

        'InstallService(LPCConstants.Service.WatchdogWatchdog.Name,
        '               LPCConstants.Service.WatchdogWatchdog.DisplayName,
        '               LPCConstants.Service.WatchdogWatchdog.Description,
        '               LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.FILE__WatchdogWatchdogService)


        'Set some stuff to run at user login
        Helpers.Logger.Write("Some apps need to run at user logon", , 0)

        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger, , 1)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY_Windows_RunAtStartup,
                                         LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger,
                                         LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger)

        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity, , 1)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY_Windows_RunAtStartup,
                                         LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity,
                                         LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity)

        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogError, , 1)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY_Windows_RunAtStartup,
                                         LPCConstants.FilesAndFolders.FILE__WatchdogError,
                                         LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.FILE__WatchdogError)

        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogServiceEnable, , 1)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY_Windows_RunOnceAtStartup,
                                         LPCConstants.FilesAndFolders.FILE__WatchdogServiceEnable,
                                         LPCConstants.FilesAndFolders.FOLDER__ServicePath & "\" & LPCConstants.FilesAndFolders.FILE__WatchdogServiceEnable)


        If mStartServiceAfterInstallation Then
            'Starting
            StartService(LPCConstants.Service.Watchdog.Name)
        End If


        'Done
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("ok, bye", , 1)
    End Sub

    Private Function StartService(Name As String) As Boolean
        Helpers.Logger.Write(Name, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            Dim retries As Integer = 0
            Helpers.Logger.Write("starting", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            If ServiceInstaller.StartService(Name, LPCConstants.TimeoutsAndDelaysAndCounters.INSTALLER__ServiceStatusCheckTimeoutSeconds) Then
                Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            Else
                Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Helpers.Logger.Write("service status: " & ServiceInstaller.GetServiceStatus(Name).ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If


            Threading.Thread.Sleep(5000)

            Return True
        Else
            Helpers.Logger.Write("not found, so impossible to start", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            Return False
        End If
    End Function

    Private Function StopService(Name As String) As Boolean
        Helpers.Logger.Write(Name, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            Dim retries As Integer = 0
            Helpers.Logger.Write("stopping", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            If ServiceInstaller.StopService(Name, LPCConstants.TimeoutsAndDelaysAndCounters.INSTALLER__ServiceStatusCheckTimeoutSeconds) Then
                Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            Else
                Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Helpers.Logger.Write("service status: " & ServiceInstaller.GetServiceStatus(Name).ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If


            Threading.Thread.Sleep(5000)

            Return True
        Else
            Helpers.Logger.Write("not found, so no need to stop", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            Return False
        End If
    End Function

    Private Function DisableService(Name As String) As Boolean
        Helpers.Logger.WriteRelative(Name, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            Helpers.Logger.WriteRelative("setting flag to DISABLED", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            Dim ret As String = ServiceInstaller.ChangeStartupType(Name, ServiceBootFlag.Disabled)

            If ret = "" Then
                Return True
            Else
                Helpers.Logger.WriteRelative("fail: " & ret, Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 3)
            End If
        Else
            Helpers.Logger.WriteRelative("not found, so no need to stop", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        End If

        Return False
    End Function

    Private Sub InstallService(Name As String, DisplayName As String, Description As String, ServicePath As String)
        Dim sStmp As String = ""

        Helpers.Logger.Write(Name, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        For i As Integer = 1 To 5
            Helpers.Logger.Write("try #" & i.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            Helpers.Logger.Write("is service already installed?", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)

            If ServiceInstaller.ServiceIsInstalled(Name) Then
                Helpers.Logger.Write("yes", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)
            Else
                Helpers.Logger.Write("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)


                Threading.Thread.Sleep(5000)

                Helpers.Logger.Write("installing", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
                ServiceInstaller.InstallService(Name, DisplayName, ServicePath, ServiceBootFlag.AutoStart)
            End If


            'Helpers.Logger.Write("make sure service is disabled from the start", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            'Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings, LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDisabled, Helpers.XOrObfuscation.Obfuscate(LPCConstants.Misc.MagicalDisableString))

            'Helpers.Logger.Write("installing", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            'ServiceInstaller.InstallServiceAndStart(Name, DisplayName, ServicePath)

            'Threading.Thread.Sleep(5000)

            'Helpers.Logger.Write("installing", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            'ServiceInstaller.InstallService(Name, DisplayName, ServicePath)

            Threading.Thread.Sleep(15000)

            Helpers.Logger.Write("set description", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            ServiceInstaller.ChangeServiceDescription(Name, Description)

            Helpers.Logger.Write("installed doublecheck", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            If ServiceInstaller.ServiceIsInstalled(Name) Then
                Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

                Exit For
            Else
                Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If

            Threading.Thread.Sleep(5000)
        Next
    End Sub
End Module
