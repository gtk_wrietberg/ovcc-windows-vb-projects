﻿Imports System.DirectoryServices.AccountManagement

Public Class WindowsUserManagement
    Private Shared ReadOnly ADS_UF_DONT_EXPIRE_PASSWD As Integer = &H10000

    Private Shared ReadOnly SID_USERS As String = "S-1-5-32-545"
    Private Shared ReadOnly SID_ADMINISTRATORS As String = "S-1-5-32-544"


    Public Shared Function AddUser(ByVal sFullName As String, ByVal sUsername As String, ByVal sPassWord As String, Optional ByVal sGroup As String = "") As String
        Try
            Dim oSystem As Object, oUser As Object
            Dim sComputerName As String

            sComputerName = Environment.MachineName

            oSystem = GetObject("WinNT://" & sComputerName)
            oUser = oSystem.Create("user", sUsername)
            oUser.FullName = sFullName
            oUser.SetPassword(sPassWord)
            oUser.SetInfo()

            oUser = Nothing
            oSystem = Nothing

            If sGroup = "" Then
                sGroup = GetUsersGroupName()
            End If

            Return AddUserToGroup(sUsername, sGroup)
        Catch ex As Exception

            'How's that for exception handling
            Return ex.Message
        End Try


    End Function

    Public Shared Function GetAdministratorsGroupName() As String
        Return GetGroupNameFromSid(SID_ADMINISTRATORS)
    End Function

    Public Shared Function GetUsersGroupName() As String
        Return GetGroupNameFromSid(SID_USERS)
    End Function

    Private Shared Function GetGroupNameFromSid(ByVal _sid As String) As String
        Dim context As PrincipalContext, group As GroupPrincipal

        context = New PrincipalContext(ContextType.Machine)
        group = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, _sid)

        Return group.SamAccountName
    End Function

    Public Shared Function AddUserToAdministratorsGroup(ByVal sUsername As String) As String
        Return AddUserToGroup(sUsername, GetAdministratorsGroupName)
    End Function

    Public Shared Function AddUserToGroup(ByVal sUsername As String, ByVal sGroup As String) As String
        Try
            Dim oGroup As Object
            Dim sComputerName As String

            sComputerName = Environment.MachineName

            oGroup = GetObject("WinNT://" & sComputerName & "/" & sGroup)
            oGroup.Add("WinNT://" & sComputerName & "/" & sUsername)

            oGroup = Nothing

            Return "ok"

        Catch ex As Exception

            'How's that for exception handling
            Return ex.Message
        End Try
    End Function

    Public Shared Function RemoveUserFromGroup(ByVal sUsername As String, ByVal sGroup As String) As Boolean
        Try
            Dim oGroup As Object
            Dim sComputerName As String

            sComputerName = Environment.MachineName

            oGroup = GetObject("WinNT://" & sComputerName & "/" & sGroup)
            oGroup.Remove("WinNT://" & sComputerName & "/" & sUsername)

            oGroup = Nothing

            Return True

        Catch ex As Exception
            'MsgBox(ex.Message)
            'How's that for exception handling
            Return False
        End Try
    End Function

    Public Shared Function RemoveUser(ByVal sUserName As String) As Boolean
        Try
            Dim oSystem As Object

            oSystem = GetObject("WinNT://" & Environment.MachineName)
            oSystem.Delete("user", sUserName)

            oSystem = Nothing

            Return True

        Catch ex As Exception
            RemoveUser = False
        End Try
    End Function

    Public Shared Function HideUserInLogonScreen(ByVal sUsername As String) As String
        Try
            'Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", sUsername, 0)

            Dim reg64 As Microsoft.Win32.RegistryKey = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, Microsoft.Win32.RegistryView.Registry64)
            Dim key64 As Microsoft.Win32.RegistryKey = reg64.OpenSubKey("SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", True)

            If key64 Is Nothing Then
                reg64.CreateSubKey("SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList")
                key64 = reg64.OpenSubKey("SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", True)
            End If

            key64.SetValue(sUsername, 0, Microsoft.Win32.RegistryValueKind.DWord)

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function PasswordExpiry(ByVal sUsername As String) As Integer
        Dim oUser As Object
        Dim iPassExp As Integer

        oUser = GetObject("WinNT://" & Environment.MachineName & "/" & sUsername)

        iPassExp = Int(oUser.MaxPasswordAge / 86400) - Int(oUser.PasswordAge / 86400)

        Return iPassExp
    End Function

    Public Shared Function RemovePasswordExpiry(ByVal sUsername As String) As String
        Try
            Dim oUser As Object
            Dim iFlagsOld As Integer, iFlagsNew As Integer

            oUser = GetObject("WinNT://" & Environment.MachineName & "/" & sUsername)

            iFlagsOld = oUser.UserFlags
            iFlagsNew = iFlagsOld Or ADS_UF_DONT_EXPIRE_PASSWD

            oUser.UserFlags = iFlagsNew
            oUser.SetInfo()

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function ChangeUserPassword(ByVal sUsername As String, ByVal sPassWord As String) As String
        Try
            Dim oUser As Object

            oUser = GetObject("WinNT://" & Environment.MachineName & "/" & sUsername)
            oUser.SetPassword(sPassWord)

            oUser = Nothing

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function FindUser(ByVal sUsername As String, Optional ByVal sGroup As String = "Users") As Boolean
        Try
            Dim oUser As Object, oGroup As Object

            'oGroup = GetObject("WinNT://" & Environment.MachineName & "/" & sGroup & ",group")
            oGroup = GetObject("WinNT://" & Environment.MachineName & "/" & sGroup)

            For Each oUser In oGroup.Members
                Try
                    If Trim(sUsername.ToLower) = Trim(oUser.Name.ToString().ToLower) Then
                        Return True
                    End If
                Catch ex As Exception
                    Return False
                End Try
            Next

            oGroup = Nothing
            oUser = Nothing

            'Return True
        Catch ex As Exception
            'Return False
        End Try

        Return False
    End Function
End Class
