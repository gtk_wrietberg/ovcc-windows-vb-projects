﻿
Imports System.Collections.Generic
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.ServiceProcess

Public Enum ServiceManagerRights
    Connect = &H1
    CreateService = &H2
    EnumerateService = &H4
    Lock = &H8
    QueryLockStatus = &H10
    ModifyBootConfig = &H20
    StandardRightsRequired = &HF0000
    AllAccess = (StandardRightsRequired Or Connect Or CreateService Or EnumerateService Or Lock Or QueryLockStatus Or ModifyBootConfig)
End Enum

Public Enum ServiceRights
    QueryConfig = &H1
    ChangeConfig = &H2
    QueryStatus = &H4
    EnumerateDependants = &H8
    Start = &H10
    [Stop] = &H20
    PauseContinue = &H40
    Interrogate = &H80
    UserDefinedControl = &H100
    Delete = &H10000
    StandardRightsRequired = &HF0000
    AllAccess = (StandardRightsRequired Or QueryConfig Or ChangeConfig Or QueryStatus Or EnumerateDependants Or Start Or [Stop] Or PauseContinue Or Interrogate Or UserDefinedControl)
End Enum

Public Enum ServiceBootFlag
    UNDEFINED = &H666
    Start = &H0
    SystemStart = &H1
    AutoStart = &H2
    DemandStart = &H3
    Disabled = &H4
    DelayedAutoStart = &H12
End Enum

'
' Summary:
'     Indicates the current state of the service.
Public Enum ServiceStatus2
    Error__NotFound = -2
    Error__General = -1
    Unknown = 0
    '
    ' Summary:
    '     The service is not running. This corresponds to the Win32 SERVICE_STOPPED constant,
    '     which is defined as 0x00000001.
    Stopped = 1
    '
    ' Summary:
    '     The service is starting. This corresponds to the Win32 SERVICE_START_PENDING
    '     constant, which is defined as 0x00000002.
    StartPending = 2
    '
    ' Summary:
    '     The service is stopping. This corresponds to the Win32 SERVICE_STOP_PENDING constant,
    '     which is defined as 0x00000003.
    StopPending = 3
    '
    ' Summary:
    '     The service is running. This corresponds to the Win32 SERVICE_RUNNING constant,
    '     which is defined as 0x00000004.
    Running = 4
    '
    ' Summary:
    '     The service continue is pending. This corresponds to the Win32 SERVICE_CONTINUE_PENDING
    '     constant, which is defined as 0x00000005.
    ContinuePending = 5
    '
    ' Summary:
    '     The service pause is pending. This corresponds to the Win32 SERVICE_PAUSE_PENDING
    '     constant, which is defined as 0x00000006.
    PausePending = 6
    '
    ' Summary:
    '     The service is paused. This corresponds to the Win32 SERVICE_PAUSED constant,
    '     which is defined as 0x00000007.
    Paused = 7
End Enum

Public Enum ServiceControl
    [Stop] = &H1
    Pause = &H2
    [Continue] = &H3
    Interrogate = &H4
    Shutdown = &H5
    ParamChange = &H6
    NetBindAdd = &H7
    NetBindRemove = &H8
    NetBindEnable = &H9
    NetBindDisable = &HA
End Enum

Public Enum ServiceError
    Ignore = &H0
    Normal = &H1
    Severe = &H2
    Critical = &H3
End Enum

Public Class ServiceInstaller
    Private Const STANDARD_RIGHTS_REQUIRED As Integer = &HF0000
    Private Const SERVICE_WIN32_OWN_PROCESS As Integer = &H10
    Private Const SERVICE_INTERACTIVE_PROCESS As Integer = &H100

    Private Const SERVICE_CONFIG_DESCRIPTION As Integer = &H1

    Private Const SERVICE_NO_CHANGE As UInteger = &HFFFFFFFFUI

    Private Const ERROR_INSUFFICIENT_BUFFER As Integer = 122


    <StructLayout(LayoutKind.Sequential)>
    Private Class SERVICE_STATUS
        Public dwServiceType As Integer = 0
        Public dwCurrentState As ServiceControllerStatus = 0
        Public dwControlsAccepted As Integer = 0
        Public dwWin32ExitCode As Integer = 0
        Public dwServiceSpecificExitCode As Integer = 0
        Public dwCheckPoint As Integer = 0
        Public dwWaitHint As Integer = 0
    End Class

    <StructLayout(LayoutKind.Sequential)>
    Private Structure QUERY_SERVICE_CONFIG
        Public dwServiceType As UInteger
        Public dwStartType As UInteger
        Public dwErrorControl As UInteger
        Public lpBinaryPathName As String
        Public lpLoadOrderGroup As String
        Public dwTagId As UInteger
        Public lpDependencies As String
        Public lpServiceStartName As String
        Public lpDisplayName As String
    End Structure

    <DllImport("advapi32.dll", EntryPoint:="OpenSCManagerA")>
    Private Shared Function OpenSCManager(lpMachineName As String, lpDatabaseName As String, dwDesiredAccess As ServiceManagerRights) As IntPtr
    End Function

    <DllImport("advapi32.dll", EntryPoint:="OpenServiceA", CharSet:=CharSet.Ansi)>
    Private Shared Function OpenService(hSCManager As IntPtr, lpServiceName As String, dwDesiredAccess As ServiceRights) As IntPtr
    End Function

    <DllImport("advapi32.dll", EntryPoint:="CreateServiceA")>
    Private Shared Function CreateService(hSCManager As IntPtr, lpServiceName As String, lpDisplayName As String, dwDesiredAccess As ServiceRights, dwServiceType As Integer, dwStartType As ServiceBootFlag, dwErrorControl As ServiceError, lpBinaryPathName As String, lpLoadOrderGroup As String, lpdwTagId As IntPtr, lpDependencies As String, lpServiceStartName As String, lpPassword As String) As IntPtr
    End Function

    <DllImport("advapi32.dll")>
    Private Shared Function CloseServiceHandle(hSCObject As IntPtr) As Integer
    End Function

    <DllImport("advapi32.dll")>
    Private Shared Function QueryServiceStatus(hService As IntPtr, lpServiceStatus As SERVICE_STATUS) As Integer
    End Function

    <DllImport("advapi32.dll", SetLastError:=True)>
    Private Shared Function DeleteService(hService As IntPtr) As Integer
    End Function

    <DllImport("advapi32.dll")>
    Private Shared Function ControlService(hService As IntPtr, dwControl As ServiceControl, lpServiceStatus As SERVICE_STATUS) As Integer
    End Function

    <DllImport("advapi32.dll", EntryPoint:="StartServiceA")>
    Private Shared Function StartService(hService As IntPtr, dwNumServiceArgs As Integer, lpServiceArgVectors As Integer) As Integer
    End Function

    <DllImport("advapi32.dll")>
    Private Shared Function ChangeServiceConfig2(hService As IntPtr, dwInfoLevel As Integer, lpInfo As IntPtr) As Integer
    End Function

    <DllImport("advapi32.dll")>
    Private Shared Function ChangeServiceConfig(ByVal hService As Integer, ByVal dwServiceType As UInt32, ByVal dwStartType As UInt32, ByVal dwErrorControl As UInt32, ByVal lpBinaryPathName As String, ByVal lpLoadOrderGroup As String, ByVal lpdwTagId As Integer, ByVal lpDependencies As String, ByVal lpServiceStartName As String, ByVal lpPassword As String, ByVal lpDisplayName As String) As Boolean
    End Function

    '    [DllImport( "advapi32.dll", CharSet = CharSet.Unicode, SetLastError = true, EntryPoint = "QueryServiceConfig2W" )]
    'Public Static extern Boolean QueryServiceConfig2( IntPtr hService, UInt32 dwInfoLevel, IntPtr buffer, UInt32 cbBufSize, out UInt32 pcbBytesNeeded );
    <DllImport("advapi32.dll")>
    Private Shared Function QueryServiceConfig2(ByVal hService As IntPtr, ByVal dwInfoLevel As Integer, ByVal buffer As IntPtr, ByVal cbBufSize As Integer, ByRef pcbBytesNeeded As Integer) As Boolean
    End Function

    <DllImport("advapi32.dll")>
    Private Shared Function QueryServiceConfig(ByVal hService As IntPtr, ByVal lpServiceConfig As IntPtr, ByVal cbBufSize As UInteger, ByRef pcbBytesNeeded As UInteger) As Boolean
    End Function


    Public Shared Function Uninstall(ServiceName As String) As String
        Dim sRet As String = ""
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect)

        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.StandardRightsRequired Or ServiceRights.[Stop] Or ServiceRights.QueryStatus)
            If service = IntPtr.Zero Then
                Throw New ApplicationException("Service not installed.")
            End If
            Try
                StopService(service)
                Dim ret As Integer = DeleteService(service)
                If ret = 0 Then
                    Dim [error] As Integer = Marshal.GetLastWin32Error()
                    Throw New ApplicationException("Could not delete service " + [error])
                End If
                sRet = "ok"
            Catch ex As Exception
                sRet = ex.GetType().ToString & " - " & ex.Message
            Finally
                CloseServiceHandle(service)
            End Try
        Catch ex As Exception
            sRet = ex.GetType().ToString & " - " & ex.Message
        Finally
            CloseServiceHandle(scman)
        End Try


        Return sRet
    End Function

    Public Shared Function ServiceIsInstalled(ServiceName As String) As Boolean
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect)

        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryStatus)

            If service = IntPtr.Zero Then
                Return False
            End If

            CloseServiceHandle(service)

            Return True
        Finally
            CloseServiceHandle(scman)
        End Try
    End Function

    Public Shared Function InstallService(ServiceName As String, DisplayName As String, FileName As String) As String
        Return InstallService(ServiceName, DisplayName, FileName, ServiceBootFlag.AutoStart)
    End Function

    Public Shared Function InstallService(ServiceName As String, DisplayName As String, FileName As String, dwStartType As ServiceBootFlag) As String
        Dim sRet As String = ""
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect Or ServiceManagerRights.CreateService)

        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryStatus Or ServiceRights.Start)

            If service = IntPtr.Zero Then
                service = CreateService(scman, ServiceName, DisplayName, ServiceRights.QueryStatus Or ServiceRights.Start, SERVICE_WIN32_OWN_PROCESS, dwStartType,
                    ServiceError.Normal, FileName, Nothing, IntPtr.Zero, Nothing, Nothing, Nothing)
            End If

            If service = IntPtr.Zero Then
                Throw New ApplicationException("Failed to install service.")
            End If

            sRet = "ok"
        Catch ex As Exception
            sRet = ex.GetType().ToString & " - " & ex.Message
        Finally
            CloseServiceHandle(scman)
        End Try

        Return sRet
    End Function

    Public Shared Function InstallServiceAndStart(ServiceName As String, DisplayName As String, FileName As String) As String
        Dim sRet As String = ""
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect Or ServiceManagerRights.CreateService)

        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryStatus Or ServiceRights.Start)

            If service = IntPtr.Zero Then
                service = CreateService(scman, ServiceName, DisplayName, ServiceRights.QueryStatus Or ServiceRights.Start, SERVICE_WIN32_OWN_PROCESS, ServiceBootFlag.AutoStart,
                    ServiceError.Normal, FileName, Nothing, IntPtr.Zero, Nothing, Nothing, Nothing)
            End If

            If service = IntPtr.Zero Then
                Throw New ApplicationException("Failed to install service.")
            End If

            Try
                StartService(service)

                sRet = "ok"
            Catch ex As Exception
                sRet = ex.GetType().ToString & " - " & ex.Message
            Finally
                CloseServiceHandle(service)
            End Try
        Catch ex As Exception
            sRet = ex.GetType().ToString & " - " & ex.Message
        Finally
            CloseServiceHandle(scman)
        End Try

        Return sRet
    End Function

    Public Shared Function GetStartupType(ServiceName As String, ByRef dStartType As ServiceBootFlag) As String
        Dim sRet As String = ""
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.AllAccess)

        Try
            Dim regRoot As Microsoft.Win32.RegistryKey

            regRoot = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & ServiceName, True)

            If regRoot Is Nothing Then
                Throw New Exception("registry key not found for service '" & ServiceName & "'")
            End If


            Dim regValue As Integer = -1, _tmp As String

            _tmp = regRoot.GetValue("DelayedAutostart", -1).ToString
            If Not Integer.TryParse(_tmp, regValue) Then
                Throw New Exception("Incorrect registry value fo 'DelayedAutoStart', expected an integer, not '" & _tmp & "'")
            End If


            Dim hService As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryConfig Or ServiceRights.ChangeConfig)
            If hService = IntPtr.Zero Then
                Throw New ApplicationException("Failed to open service '" & ServiceName & "'.")
            End If

            Dim bytesNeeded As UInteger
            If QueryServiceConfig(hService, IntPtr.Zero, 0, bytesNeeded) Then
                Throw New Exception("Expecting ERROR_INSUFFICIENT_BUFFER.")
            End If
            If Marshal.GetLastWin32Error() = ERROR_INSUFFICIENT_BUFFER Then
                Throw New System.ComponentModel.Win32Exception()
            End If

            'Query service configuration.  
            Dim buffer(0 To CInt(bytesNeeded) - 1) As Byte
            Dim gch As GCHandle = GCHandle.Alloc(buffer, Runtime.InteropServices.GCHandleType.Pinned)
            Try
                If Not QueryServiceConfig(hService, gch.AddrOfPinnedObject(), bytesNeeded, bytesNeeded) Then
                    Throw New System.ComponentModel.Win32Exception()
                End If

                Dim config As QUERY_SERVICE_CONFIG = DirectCast(Marshal.PtrToStructure(gch.AddrOfPinnedObject(), GetType(QUERY_SERVICE_CONFIG)), QUERY_SERVICE_CONFIG)

                If regValue = 1 And config.dwStartType = ServiceBootFlag.AutoStart Then
                    dStartType = ServiceBootFlag.DelayedAutoStart
                Else
                    dStartType = config.dwStartType
                End If
            Finally
                gch.Free()
            End Try
        Catch ex As Exception
            sRet = ex.GetType().ToString & " - " & ex.Message
        End Try

        Return sRet
    End Function

    Public Shared Function ChangeStartupType(ServiceName As String, dwStartType As ServiceBootFlag) As String
        Dim sRet As String = ""
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.AllAccess)


        Try
            Dim regRoot As Microsoft.Win32.RegistryKey

            regRoot = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & ServiceName, True)

            If regRoot Is Nothing Then
                Throw New Exception("registry key not found for service '" & ServiceName & "'")
            End If

            If dwStartType = ServiceBootFlag.DelayedAutoStart Then
                regRoot.SetValue("DelayedAutostart", 1, Microsoft.Win32.RegistryValueKind.DWord)
                dwStartType = ServiceBootFlag.AutoStart
            Else
                regRoot.SetValue("DelayedAutostart", 0, Microsoft.Win32.RegistryValueKind.DWord)
            End If


            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryConfig Or ServiceRights.ChangeConfig)
            If service = IntPtr.Zero Then
                Throw New ApplicationException("Failed to open service '" & ServiceName & "'.")
            End If

            If Not ChangeServiceConfig(service, SERVICE_NO_CHANGE, dwStartType, SERVICE_NO_CHANGE, Nothing, Nothing, IntPtr.Zero, Nothing, Nothing, Nothing, Nothing) Then
                Throw New System.Runtime.InteropServices.ExternalException("Could not change StartType for service '" & ServiceName & "'.")
            End If

            CloseServiceHandle(service)
        Catch ex As Exception
            sRet = ex.GetType().ToString & " - " & ex.Message
        Finally
            CloseServiceHandle(scman)
        End Try


        Return sRet
    End Function

    Public Shared Sub ChangeServiceDescription(Name As String, Description As String)
        Try
            Dim regRoot As Microsoft.Win32.RegistryKey

            regRoot = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & Name, True)

            If regRoot Is Nothing Then
                Exit Sub
            End If

            regRoot.SetValue("Description", Description, Microsoft.Win32.RegistryValueKind.String)
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Function StartService(ServiceName As String) As Boolean
        Return StartService(ServiceName, -1)
    End Function

    Public Shared Function StartService(ServiceName As String, TimeoutSeconds As Integer) As Boolean
        Dim ret As Boolean = False

        Try
            Dim sc As New ServiceController(ServiceName)

            sc.Start()

            If TimeoutSeconds <= 0 Then
                sc.WaitForStatus(ServiceControllerStatus.Running)
            Else
                Dim ts As New TimeSpan(0, 0, TimeoutSeconds)

                sc.WaitForStatus(ServiceControllerStatus.Running, ts)
            End If

            If sc.Status = ServiceControllerStatus.Running Then
                ret = True
            End If

            sc.Dispose()
        Catch ex As Exception

        End Try


        Return ret
    End Function

    Public Shared Function StopService(ServiceName As String) As Boolean
        Return StopService(ServiceName, -1)
    End Function

    Public Shared Function StopService(ServiceName As String, TimeoutSeconds As Integer) As Boolean
        Dim ret As Boolean = False

        Try
            Dim sc As New ServiceController(ServiceName)

            sc.Stop()

            If TimeoutSeconds <= 0 Then
                sc.WaitForStatus(ServiceControllerStatus.Stopped)
            Else
                Dim ts As New TimeSpan(0, 0, TimeoutSeconds)

                sc.WaitForStatus(ServiceControllerStatus.Stopped, ts)
            End If

            sc.Dispose()
            sc = New ServiceController(ServiceName)

            If sc.Status = ServiceControllerStatus.Stopped Then
                ret = True
            End If

            sc.Dispose()
        Catch ex As Exception

        End Try


        Return ret
    End Function

    Public Shared Function GetServiceStatus(ServiceName As String) As ServiceControllerStatus
        Dim sc As New ServiceController
        Dim ss As ServiceControllerStatus = 0

        Try
            sc.ServiceName = ServiceName
            ss = sc.Status
        Catch ex As InvalidOperationException
            ss = -1
        Catch ex As Exception
            ss = -2
        Finally
            sc.Dispose()
        End Try


        Return ss
    End Function

    Private Shared Function OpenSCManager(Rights As ServiceManagerRights) As IntPtr
        Dim scman As IntPtr = OpenSCManager(Nothing, Nothing, Rights)

        If scman = IntPtr.Zero Then
            Throw New ApplicationException("Could not connect to service control manager.")
        End If

        Return scman
    End Function
End Class
