Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCCWatchdog_installer")> 
<Assembly: AssemblyDescription("OVCC Watchdog installer")> 
<Assembly: AssemblyCompany("GuestTek")> 
<Assembly: AssemblyProduct("OVCCWatchdog_installer")>
<Assembly: AssemblyCopyright("Copyright ©  2021")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("3241c391-4738-46e2-9b03-5fd6676ce297")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.24030.1058")>

<Assembly: AssemblyFileVersion("2.0.24030.1058")>
<assembly: AssemblyInformationalVersion("0.0.24030.1058")>