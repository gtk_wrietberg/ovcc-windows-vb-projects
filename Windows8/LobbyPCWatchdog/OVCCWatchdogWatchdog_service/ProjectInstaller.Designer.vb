﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.OVCCWatchdogWatchdogServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.OVCCWatchdogWatchdogServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'OVCCWatchdogWatchdogServiceProcessInstaller
        '
        Me.OVCCWatchdogWatchdogServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.OVCCWatchdogWatchdogServiceProcessInstaller.Password = Nothing
        Me.OVCCWatchdogWatchdogServiceProcessInstaller.Username = Nothing
        '
        'OVCCWatchdogWatchdogServiceInstaller
        '
        Me.OVCCWatchdogWatchdogServiceInstaller.Description = "Keeps OVCC Wacthdog running"
        Me.OVCCWatchdogWatchdogServiceInstaller.DisplayName = "OVCCWatchdogWatchdog"
        Me.OVCCWatchdogWatchdogServiceInstaller.ServiceName = "zzz_OVCCWatchdogWatchdog"
        Me.OVCCWatchdogWatchdogServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.OVCCWatchdogWatchdogServiceProcessInstaller, Me.OVCCWatchdogWatchdogServiceInstaller})

    End Sub

    Friend WithEvents OVCCWatchdogWatchdogServiceProcessInstaller As ServiceProcess.ServiceProcessInstaller
    Friend WithEvents OVCCWatchdogWatchdogServiceInstaller As ServiceProcess.ServiceInstaller
End Class
