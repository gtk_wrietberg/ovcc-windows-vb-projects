﻿Imports System.ServiceProcess

Public Class zzz_OVCCWatchdogWatchdog
    Public Structure ServiceState
        Dim Startup As ServiceBootFlag
        Dim Status As ServiceControllerStatus
    End Structure


    Private mThread_StartupApps As Threading.Thread
    Private mThread_Watchdog As Threading.Thread
    Private mThread_StartupApps__Abort As Boolean = False
    Private mThread_Watchdog__Abort As Boolean = False
    Private mFailCount As Long = 0
    Private mServiceState_current As ServiceState
    Private mServiceState_previous As ServiceState


    Protected Overrides Sub OnStart(ByVal args() As String)
        LoadSettings()

        'init logger
        Helpers.Logger.InitialiseLogger()

        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("service started", , 0)
        Helpers.Logger.Write("debugging = " & Helpers.Logger.Debugging.ToString, , 1)

        'check if we're needed
        If LPCGlobals.Settings.EnableWatchdogWatchdog Then
            'Start the main thread
            StartThreads()
        Else
            Helpers.Logger.Write("service disabled, stopping", , 0)
        End If
    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()

        Helpers.Logger.Write("service stopped")
    End Sub

    Private Sub LoadSettings()
        LPCGlobals.Settings.Load()

        Helpers.Logger.Debugging = LPCGlobals.Settings.Debug
        Helpers.Logger.IncludeCaller = LPCGlobals.Settings.Debug
    End Sub

    Private Sub StartThreads()
        mThread_StartupApps = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_StartupApps))
        mThread_StartupApps.Start()

        mThread_Watchdog = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Watchdog))
        mThread_Watchdog.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThread_StartupApps.Abort()
        Catch ex As Exception

        End Try

        Try
            mThread_Watchdog.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Thread_Watchdog()
        'Wait for a while
        Helpers.Logger.Write("first run delay", , 0)
        If LPCGlobals.Settings.SkipFirstRunDelay Then
            Helpers.Logger.Write("skipped", , 1)
        Else
            Helpers.Logger.Write(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__WatchdogWatchdogStart & " sec", , 1)
            Threading.Thread.Sleep(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__WatchdogWatchdogStart * 1000)
        End If


        mServiceState_current.Startup = -1
        mServiceState_current.Status = -1

        mServiceState_previous = mServiceState_current

        Dim sError As String = "", bMuzzleCounter As Integer = 0

        'main loop
        Do While Not mThread_Watchdog__Abort
            Helpers.Logger.WriteMessage("checking " & LPCConstants.Service.Watchdog.Name, 0)

            If Not ServiceInstaller.ServiceIsInstalled(LPCConstants.Service.Watchdog.Name) Then
                Helpers.Logger.WriteError("not installed!", 1)

                mThread_Watchdog__Abort = True
                Exit Do
            End If

            Helpers.Logger.WriteMessage("getting startup type", 1)
            sError = ServiceInstaller.GetStartupType(LPCConstants.Service.Watchdog.Name, mServiceState_current.Startup)
            If Not sError.Equals("") Then
                Helpers.Logger.WriteError("error", 2)
                Helpers.Logger.WriteError(sError, 3)
            End If
            Helpers.Logger.WriteMessage("startup type", 2)
            Helpers.Logger.WriteMessage(mServiceState_current.Startup.ToString, 3)


            Helpers.Logger.WriteMessage("getting service status", 1)
            mServiceState_current.Status = ServiceInstaller.GetServiceStatus(LPCConstants.Service.Watchdog.Name)
            Helpers.Logger.WriteMessage("status", 2)
            Helpers.Logger.WriteMessage(mServiceState_current.Status.ToString, 3)


            If mServiceState_current.Equals(mServiceState_previous) Then
                bMuzzleCounter = 0

                If Not mServiceState_current.Startup = ServiceBootFlag.AutoStart Then
                    Helpers.Logger.WriteWarning("not set as '" & ServiceBootFlag.AutoStart & "'", 1)
                    Helpers.Logger.WriteMessage("setting", 2)
                    ServiceInstaller.ChangeStartupType(LPCConstants.Service.Watchdog.Name, ServiceBootFlag.AutoStart)

                    bMuzzleCounter += 1
                End If

                If Not mServiceState_current.Status = ServiceControllerStatus.Running Then
                    Helpers.Logger.WriteWarning("not running", 1)
                    Helpers.Logger.WriteMessage("starting", 2)

                    If ServiceInstaller.StartService(LPCConstants.Service.Watchdog.Name, LPCConstants.TimeoutsAndDelaysAndCounters.TIMEOUT__ServiceStart) Then
                        Helpers.Logger.WriteMessage("ok", 3)
                    Else
                        Helpers.Logger.WriteError("fail", 3)
                    End If

                    bMuzzleCounter += 1
                End If

                If bMuzzleCounter >= 2 Then
                    'Watchdog might be disabled for a reason, muzzle it.
                    Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                 LPCConstants.RegistryKeys.VALUE__WatchdogSettingsMuzzled,
                                                 Helpers.XOrObfuscation.Obfuscate(LPCConstants.Misc.MagicalDisableString))
                End If
            End If

            mServiceState_previous = mServiceState_current

            Threading.Thread.Sleep(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__ServiceLoop * 1000)
        Loop
    End Sub

    Private Sub Thread_StartupApps()
        Helpers.Logger.WriteWarning("making sure that helper apps are in autorun", 0)
        SetStartupApps()
    End Sub

    Private Sub SetStartupApps()
        'check run settings in registry
        Helpers.Logger.WriteMessageRelative("helper apps auto run", 1)
        Helpers.Logger.WriteMessageRelative(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger, 2)
        Helpers.Logger.WriteMessageRelative(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger), 3)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger))
        Helpers.Logger.WriteMessageRelative(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity, 2)
        Helpers.Logger.WriteMessageRelative(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity), 3)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity))
        Helpers.Logger.WriteMessageRelative(LPCConstants.FilesAndFolders.FILE__WatchdogError, 2)
        Helpers.Logger.WriteMessageRelative(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogError), 3)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogError,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogError))
    End Sub
End Class
