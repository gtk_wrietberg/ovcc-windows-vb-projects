﻿Module modMain_lastactivity
    Dim timerInputCheck As New System.Timers.Timer

    Private mThreading_InputCheck As Threading.Thread

    Private mDebug As Boolean = False


    Private Declare Function AttachConsole Lib "kernel32.dll" (ByVal dwProcessId As Int32) As Boolean
    Private Declare Function FreeConsole Lib "kernel32.dll" () As Boolean
    Private ReadOnly ATTACH_PARENT_PROCESS As Int32 = -1


    Public Sub Main()
        If Helpers.Processes.IsAppAlreadyRunning Then
            Application.Exit()
        End If

        For Each _arg As String In Environment.GetCommandLineArgs
            If _arg.Equals("--debug") Then
                mDebug = True

                AttachConsole(ATTACH_PARENT_PROCESS)
            End If
        Next

        If mDebug Then
            Helpers.Logger.InitialiseLogger()
        End If

        Debug(New String("*", 50))
        Debug(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Debug("started")

        'MsgBox(Helpers.FilesAndFolders.GetWindowsFolder() & "\Temp\" & LPCConstants.FilesAndFolders.FILE__LastActivityTempFile)

        Dim sError As String = ""
        If Not RegistryWriteCheck.CanIWriteToOpenToAllRegistrySection(sError) Then
            Debug("unable to write registry data")

            ErrorTrigger.TriggerErrorPopup("Unable to write data", "The system was unable to write critical data.\nPlease contact the helpdesk.", 2, False)
            Application.Exit()
        End If


        Debug("starting main thread")

        mThreading_InputCheck = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_InputCheck))
        mThreading_InputCheck.Start()
    End Sub

    Private Sub Debug(s As String)
        If mDebug Then
            Console.WriteLine(s)
            Helpers.Logger.Write(s, , 0)
        End If
    End Sub

    Private Sub Thread_InputCheck()
        Debug("starting timer")

        timerInputCheck.AutoReset = True
        timerInputCheck.Interval = LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__ActivityCheck * 1000
        AddHandler timerInputCheck.Elapsed, AddressOf timerInputCheck_Elapsed
        timerInputCheck.Start()

        Threading.Thread.Sleep(Threading.Timeout.Infinite)
    End Sub

    Private Sub timerInputCheck_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Dim lIdle As Long, value As Long, msg As String = ""

        Debug("timer loop start")

        Debug("check idle time")
        Try
            lIdle = clsInputCheck.SystemIdleTime(msg)
            Debug("SystemIdleTime return message: " & msg)
        Catch ex As Exception
            Debug("error: " & ex.Message)
        End Try

        Debug("system idle time: " & lIdle.ToString)
        If lIdle < 0 Then
            'something went wrong
            value = 0

            Debug("error")
        Else
            'calculate the date
            Dim dIdle As Date = DateAdd(DateInterval.Second, -lIdle, Date.Now)

            value = Helpers.TimeDate.ToUnix(dIdle)

            Debug(lIdle.ToString & " --> " & dIdle.ToString & " --> " & value)
        End If

        Debug("save to registry")
        Try
            Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_LastActivity, LPCConstants.RegistryKeys.VALUE__LastActivity, value)
            Debug("ok")
        Catch ex As Exception
            Debug("error: " & ex.Message)
        End Try


        Debug("save to file")
        Try
            Dim sWriter As New System.IO.StreamWriter(IO.Path.Combine(LPCConstants.FilesAndFolders.FILE__LastActivityTempFilePath, LPCConstants.FilesAndFolders.FILE__LastActivityTempFileName), False)

            Debug("file: " & IO.Path.Combine(LPCConstants.FilesAndFolders.FILE__LastActivityTempFilePath, LPCConstants.FilesAndFolders.FILE__LastActivityTempFileName))

            sWriter.Write("<lastactivity><date>" & value & "</date><idle>" & lIdle & "</idle></lastactivity>")
            sWriter.Close()

            Debug("ok")
        Catch ex As Exception
            Debug("error: " & ex.Message)
        End Try

        Debug("timer loop end")
    End Sub
End Module
