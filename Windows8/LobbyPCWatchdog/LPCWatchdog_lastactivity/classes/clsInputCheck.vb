﻿Public Class clsInputCheck
    Private Structure LASTINPUTINFO
        Public cbSize As Integer
        Public dwTime As Integer
    End Structure

    'Private Declare Function GetTickCount Lib "kernel32" () As Long
    Private Declare Function GetLastInputInfo Lib "User32.dll" (ByRef lii As LASTINPUTINFO) As Boolean

    Private Shared Function GetTickCount() As Long
        Return Environment.TickCount
    End Function

    ''' <summary>
    ''' returns system idle time in seconds, -1 if failure
    ''' </summary>
    ''' <remarks></remarks>
    Public Shared Function SystemIdleTime(ByRef ReturnMessage As String) As Long
        Dim mMessage As String = ""
        Dim mIdleTime As Integer = -1

        Try
            Dim mLastInputInfo As LASTINPUTINFO = New LASTINPUTINFO

            Try
                mLastInputInfo.cbSize = Runtime.InteropServices.Marshal.SizeOf(mLastInputInfo)
            Catch ex As Exception
                mIdleTime = -3
            End Try

            ' get last input info from Windows
            If GetLastInputInfo(mLastInputInfo) Then     ' if we have an input info     
                ' compute idle time
                Try
                    mMessage = "mIdleTime calculation"

                    mMessage = "mIdleTime = (GetTickCount() - mLastInputInfo.dwTime) / 1000 = (" & GetTickCount().ToString & " - " & mLastInputInfo.dwTime.ToString & ") / 1000"
                    mIdleTime = (GetTickCount() - mLastInputInfo.dwTime) / 1000
                Catch ex As Exception
                    mIdleTime = -4
                End Try
            Else
                mIdleTime = -5
            End If
        Catch ex As Exception
            mIdleTime = -2
        End Try


        ReturnMessage = mMessage
        Return mIdleTime
    End Function
End Class
