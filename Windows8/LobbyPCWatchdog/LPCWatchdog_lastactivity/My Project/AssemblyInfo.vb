Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCCWatchdog_lastactivity")> 
<Assembly: AssemblyDescription("OVCC Watchdog lastactivity checker")> 
<Assembly: AssemblyCompany("GuestTek")> 
<Assembly: AssemblyProduct("OVCCWatchdog_lastactivity")>
<Assembly: AssemblyCopyright("Copyright ©  2021")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("95fb63a4-91f9-4e31-83d0-6247a4bdc3cc")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.2.24030.1058")>

<assembly: AssemblyFileVersion("0.0.24030.1058")>
<assembly: AssemblyInformationalVersion("0.0.24030.1058")>