﻿Public Class Form1
    Private mFadeValue As Double = 0.0
    Private mFadeValueMax As Double = 0.9
    Private mFadeStep As Double = 0.02

    Private Sub Form1_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        mFadeValue = 0.0
        mFadeStep = 0.02

        tmrFadeIn.Enabled = True
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Button1.Visible = False
        Button1.Enabled = False
    End Sub

    Private Sub ContinueStuff()
        Button1.Visible = True
        Button1.Enabled = True
    End Sub

    Private Sub tmrFadeIn_Tick(sender As Object, e As EventArgs) Handles tmrFadeIn.Tick
        mFadeValue += mFadeStep

        If mFadeValue > mFadeValueMax Then
            mFadeValue = mFadeValueMax
        End If

        Me.Opacity = mFadeValue

        If mFadeValue = mFadeValueMax Then
            tmrFadeIn.Enabled = False

            ContinueStuff()
        End If
    End Sub
End Class
