﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkDebugging = New System.Windows.Forms.CheckBox()
        Me.chkIncludeCaller = New System.Windows.Forms.CheckBox()
        Me.btnLog = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'chkDebugging
        '
        Me.chkDebugging.AutoSize = True
        Me.chkDebugging.Checked = True
        Me.chkDebugging.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDebugging.Location = New System.Drawing.Point(418, 126)
        Me.chkDebugging.Name = "chkDebugging"
        Me.chkDebugging.Size = New System.Drawing.Size(78, 17)
        Me.chkDebugging.TabIndex = 0
        Me.chkDebugging.Text = "Debugging"
        Me.chkDebugging.UseVisualStyleBackColor = True
        '
        'chkIncludeCaller
        '
        Me.chkIncludeCaller.AutoSize = True
        Me.chkIncludeCaller.Checked = True
        Me.chkIncludeCaller.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeCaller.Location = New System.Drawing.Point(418, 149)
        Me.chkIncludeCaller.Name = "chkIncludeCaller"
        Me.chkIncludeCaller.Size = New System.Drawing.Size(87, 17)
        Me.chkIncludeCaller.TabIndex = 1
        Me.chkIncludeCaller.Text = "IncludeCaller"
        Me.chkIncludeCaller.UseVisualStyleBackColor = True
        '
        'btnLog
        '
        Me.btnLog.Location = New System.Drawing.Point(533, 126)
        Me.btnLog.Name = "btnLog"
        Me.btnLog.Size = New System.Drawing.Size(127, 40)
        Me.btnLog.TabIndex = 2
        Me.btnLog.Text = "LOG"
        Me.btnLog.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(306, 274)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(227, 86)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnLog)
        Me.Controls.Add(Me.chkIncludeCaller)
        Me.Controls.Add(Me.chkDebugging)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents chkDebugging As CheckBox
    Friend WithEvents chkIncludeCaller As CheckBox
    Friend WithEvents btnLog As Button
    Friend WithEvents Button1 As Button
End Class
