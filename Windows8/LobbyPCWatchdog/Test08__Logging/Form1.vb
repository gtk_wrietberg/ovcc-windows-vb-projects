﻿Imports System.ServiceProcess

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.InitialiseLogger()
    End Sub

    Private testCount As Integer = 0

    Private Sub btnLog_Click(sender As Object, e As EventArgs) Handles btnLog.Click
        testCount += 1

        Helpers.Logger.Debugging = chkDebugging.Checked
        Helpers.Logger.IncludeCaller = False

        Helpers.Logger.Write("test #" & testCount.ToString, , 0)
        Helpers.Logger.Write("Debugging = " & chkDebugging.Checked.ToString, , 1)
        Helpers.Logger.Write("IncludeCaller = " & chkIncludeCaller.Checked.ToString, , 1)

        Helpers.Logger.Write(New String("*", 50), , 0)

        Helpers.Logger.IncludeCaller = chkIncludeCaller.Checked

        '-------------------------------------------------------------------------
        Helpers.Logger.WriteMessage("Helpers.Logger.WriteMessage", 0)
        Helpers.Logger.WriteWarning("Helpers.Logger.WriteWarning", 0)
        Helpers.Logger.WriteError("Helpers.Logger.WriteError", 0)
        Helpers.Logger.WriteDebug("Helpers.Logger.WriteDebug", 0)
        Helpers.Logger.WriteMessageRelative("Helpers.Logger.WriteMessageRelative", 0)
        Helpers.Logger.WriteWarningRelative("Helpers.Logger.WriteWarningRelative", 0)
        Helpers.Logger.WriteErrorRelative("Helpers.Logger.WriteErrorRelative", 0)
        Helpers.Logger.WriteDebugRelative("Helpers.Logger.WriteDebugRelative", 0)

        Helpers.Logger.Write("Helpers.Logger.Write", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        Helpers.Logger.Write("Helpers.Logger.Write", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        Helpers.Logger.Write("Helpers.Logger.Write", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)
        Helpers.Logger.Write("Helpers.Logger.Write", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

        Helpers.Logger.WriteEmptyLine()

        TestSub()
    End Sub

    Private Sub TestSub()
        Helpers.Logger.WriteMessage("Helpers.Logger.WriteMessage", 0)
        Helpers.Logger.WriteWarning("Helpers.Logger.WriteWarning", 0)
        Helpers.Logger.WriteError("Helpers.Logger.WriteError", 0)
        Helpers.Logger.WriteDebug("Helpers.Logger.WriteDebug", 0)
        Helpers.Logger.WriteMessageRelative("Helpers.Logger.WriteMessageRelative", 0)
        Helpers.Logger.WriteWarningRelative("Helpers.Logger.WriteWarningRelative", 0)
        Helpers.Logger.WriteErrorRelative("Helpers.Logger.WriteErrorRelative", 0)
        Helpers.Logger.WriteDebugRelative("Helpers.Logger.WriteDebugRelative", 0)

        Helpers.Logger.Write("Helpers.Logger.Write", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        Helpers.Logger.Write("Helpers.Logger.Write", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        Helpers.Logger.Write("Helpers.Logger.Write", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)
        Helpers.Logger.Write("Helpers.Logger.Write", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 0)
    End Sub

    Public Structure ServiceState
        Dim Startup As ServiceBootFlag
        Dim Status As ServiceControllerStatus
    End Structure

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim poep1 As ServiceState
        Dim poep2 As ServiceState

        poep1.Startup = ServiceBootFlag.DelayedAutoStart
        poep1.Status = ServiceControllerStatus.ContinuePending

        MsgBox(poep2.Equals(poep1))

        poep2 = poep1

        MsgBox(poep2.Equals(poep1))

        poep2.Status = ServiceControllerStatus.Paused

        MsgBox(poep2.Equals(poep1))
    End Sub
End Class
