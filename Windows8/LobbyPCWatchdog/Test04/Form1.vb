﻿Public Class Form1
    Public Enum LOBBYPC_STATE As Integer
        NOT_SET = 0
        SITEKIOSK_NOT_RUNNING = 1
        NOT_RUNNING_AS_SECURE_USER = 2
        EXPLORER_RUNNING = 4
        TRIGGER_NOT_RUNNING = 8
        LASTACTIVITY_NOT_UPDATED = 16
        WINDOWS_LOGON_SCREEN = 32
        WATCHDOG_WARNING_RUNNING = 64
        WATCHDOG_WARNING_FAILED = 128
        ADMIN_MODE = 256
        ADMIN_MODE_CANCELLED = 512
        SITEKIOSK_NOT_INSTALLED = 1024
        CAN_NOT_WRITE_TO_REGISTRY = 2048
        GENERAL_FAILURE = 4096
    End Enum

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox1.Clear()

        ShowEnumFlags()
    End Sub

    Private Sub ShowEnumFlags()
        Dim value As Integer
        If Not Integer.TryParse(TextBox2.Text, value) Then
            value = 0
            TextBox2.Text = value.ToString
        End If

        Dim items As Array
        items = System.Enum.GetValues(GetType(LOBBYPC_STATE))
        Dim item As Integer
        For Each item In items
            If value And item Then
                TextBox1.AppendText(System.Enum.GetName(GetType(LOBBYPC_STATE), item) & vbCrLf)
            End If
            'TextBox1.AppendText(System.Enum.GetName(GetType(LOBBYPC_STATE), item) & " = " & item.ToString & vbCrLf)
        Next

        'items = System.Enum.GetNames(GetType(LOBBYPC_STATE))

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim value As Integer
        If Not Integer.TryParse(TextBox2.Text, value) Then
            value = 0
            TextBox2.Text = value.ToString
        End If

        value = value And Not LOBBYPC_STATE.CAN_NOT_WRITE_TO_REGISTRY
        TextBox2.Text = value.ToString
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        TestHelper.TestValue = Not TestHelper.TestValue
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        TextBox3.Text = Helpers.MD5Class.GetMd5Hash(TextBox3.Text)
    End Sub

    Private Sub UptimeSchmuptime()
        'Dim query As New System.Management.SelectQuery("SELECT LastBootUpTime FROM Win32_OperatingSystem WHERE Primary='true'")
        'Dim searcher As New System.Management.ManagementObjectSearcher(query)
        'Dim mo As System.Management.ManagementObject
        'Dim dtBootTime As New DateTime()
        'Dim sTmp As String = ""

        'For Each mo In searcher.Get()
        '    dtBootTime = System.Management.ManagementDateTimeConverter.ToDateTime(mo.Properties("LastBootUpTime").Value.ToString())

        '    sTmp &= dtBootTime.ToLongDateString()
        '    sTmp &= " "
        '    sTmp &= dtBootTime.ToLongTimeString()
        'Next

        'TextBox4.Text = sTmp

        'searcher = Nothing
        'mo = Nothing

        Dim str_ServerName As String = Environment.MachineName
        Dim pc As New PerformanceCounter("System", "System Up Time", "", str_ServerName)
        pc.NextValue()
        Dim ts As TimeSpan = TimeSpan.FromSeconds(pc.NextValue)
        MsgBox("This system " & str_ServerName & " has been up for " & ts.Days & " days " & ts.Hours & " hours, " & ts.Minutes & " and " & ts.Seconds & " seconds.")
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        UptimeSchmuptime()
    End Sub
End Class
