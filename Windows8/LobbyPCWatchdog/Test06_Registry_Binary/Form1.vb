﻿Public Class Form1
    Private StartupApproved_enabled_bytes As Byte() = {2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    Private StartupApproved_disabled_bytes As Byte() = {3, 0, 29, 16, 113, 209, 7, 29, 16, 113, 209, 7}

    Private Sub _enable()
        Helpers.Windows.Startup.EnableStartupProgram("IgfxTray")

        'Helpers.Registry64.SetValue_Binary("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run", "IgfxTray", StartupApproved_enabled_bytes)
    End Sub

    Private Sub _disable()
        Helpers.Windows.Startup.DisableStartupProgram("IgfxTray")

        'Helpers.Registry64.SetValue_Binary("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run", "IgfxTray", StartupApproved_disabled_bytes)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        _enable()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        _disable()
    End Sub

End Class
