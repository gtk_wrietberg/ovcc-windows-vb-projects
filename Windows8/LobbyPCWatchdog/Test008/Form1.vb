﻿Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox1.Clear()

        For Each p As Process In Process.GetProcesses
            Dim file As String
            Try
                file = IO.Path.GetFileName(p.Modules(0).FileName)
            Catch ex As Exception
                file = "n/a"
            End Try

            TextBox1.AppendText(file & vbCrLf)
        Next
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MsgBox(LPCConstants.FilesAndFolders.FILE__SiteKiosk)

        If Helpers.Processes.IsProcessRunningExactMatch(LPCConstants.FilesAndFolders.FILE__SiteKiosk) Then
            MsgBox("IsProcessRunningExactMatch: yes")
        Else
            MsgBox("IsProcessRunningExactMatch: no")
        End If

        If Helpers.Processes.IsProcessRunning(LPCConstants.FilesAndFolders.FILE__SiteKiosk) Then
            MsgBox("IsProcessRunning: yes")
        Else
            MsgBox("IsProcessRunning: no")
        End If

        If Helpers.Processes.IsProcessRunning_broken(LPCConstants.FilesAndFolders.FILE__SiteKiosk) Then
            MsgBox("IsProcessRunning_broken: yes")
        Else
            MsgBox("IsProcessRunning_broken: no")
        End If
    End Sub
End Class
