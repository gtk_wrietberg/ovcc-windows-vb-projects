﻿Public Class LPCState
    Public Enum LOBBYPC_STATE As Integer
        OK = 0
        SITEKIOSK_PROCESS_NOT_RUNNING = 1
        NOT_RUNNING_AS_SECURE_USER = 2
        EXPLORER_PROCESS_RUNNING = 4
        TRIGGER_PROCESS_NOT_RUNNING = 8
        LASTACTIVITY_PROCESS_NOT_RUNNING = 16
        ERROR_PROCESS_NOT_RUNNING = 32
        WINDOWS_LOGON_SCREEN = 64
        WATCHDOG_WARNING_RUNNING = 128
        WATCHDOG_WARNING_FAILED = 256
        REMOTE_CONTROL_CONNECTED = 512
        REBOOT_LOOP = 1024
        EMERGENCY_CONFIG_MODE = 2048
        ADMIN_MODE = 4096
        ADMIN_MODE_CANCELLED = 8192
        SITEKIOSK_NOT_INSTALLED = 16384
        CAN_NOT_WRITE_TO_REGISTRY = 32768
        GENERAL_FAILURE = 65536
    End Enum

    Public Structure LOBBYPC_ACTION
        Dim IGNORED As Boolean 'ignore all actions
        Dim LAST_ACTIVITY As Integer 'timestamp of last keyboard/mouse activity; comes from OVCCWatchdog_lastactivity
        Dim RESTART As Boolean 'put machine in secure autostart and reboot
        Dim KILLSITEKIOSK As Boolean
        Dim REBOOT As Boolean 'hard reboot
        Dim REBOOT_LOOP As Boolean 'reboot loop
        Dim STOP_SERVICE As Boolean 'stop service, serious error occurred (software not installed, broken install). Prevents Watchdog from putting machine in reboot loop.
        Dim SHOW_WARNING As Boolean 'show warning popup, user can type admin password to temporarily cancel Watchdog
        Dim WARNING_LOOSE As Boolean 'show warning popup with longer timeout, user can type admin password to temporarily cancel Watchdog
        Dim OK As Boolean 'All is ok
        Dim FAILURE As Boolean 'Error, do nothing and try again next loop
        Dim LAST_ERROR As String 'Holds description of above error
        Dim STATE As Long 'number describing state of machine, see LOBBYPC_STATE above
    End Structure

    Private Shared mCurrentState As LOBBYPC_STATE
    Public Shared ReadOnly Property CurrentState As LOBBYPC_STATE
        Get
            Return mCurrentState
        End Get
    End Property

    Private Shared mPreviousState As LOBBYPC_STATE
    Public Shared ReadOnly Property PreviousState As LOBBYPC_STATE
        Get
            Return mPreviousState
        End Get
    End Property

    Private Shared mAction As LOBBYPC_ACTION
    Public Shared ReadOnly Property Action As LOBBYPC_ACTION
        Get
            Return mAction
        End Get
    End Property

    Private Shared mUsernameToCheck As String
    Public Shared Property UsernameToCheck As String
        Get
            Return mUsernameToCheck
        End Get
        Set(value As String)
            mUsernameToCheck = value
        End Set
    End Property

    Public Shared Sub ResetAll()
        mAction.IGNORED = False
        mAction.LAST_ACTIVITY = 0
        mAction.KILLSITEKIOSK = False
        mAction.REBOOT = False
        mAction.REBOOT_LOOP = False
        mAction.RESTART = False
        mAction.SHOW_WARNING = False
        mAction.STOP_SERVICE = False
        mAction.OK = False
        mAction.STATE = mCurrentState
        mAction.WARNING_LOOSE = False
    End Sub

    Public Shared Sub ResetActions()
        mAction.IGNORED = False
        mAction.KILLSITEKIOSK = False
        mAction.REBOOT = False
        mAction.RESTART = False
        mAction.SHOW_WARNING = False
        mAction.STOP_SERVICE = False
        mAction.OK = False
        mAction.WARNING_LOOSE = False
    End Sub

    Public Shared Sub ForceReboot()
        ResetActions()

        mAction.REBOOT = True
    End Sub

    Public Shared Sub ForceRestart()
        ResetActions()

        mAction.RESTART = True
    End Sub

    Public Shared Sub ForceShowWarning()
        ResetActions()

        mAction.SHOW_WARNING = True
    End Sub

    Public Shared Sub ActionCorrection()
        'Action correction
        Helpers.Logger.WriteRelative("action correction", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)

        'service is quitting due to a serious error, cancel all other actions
        If mAction.STOP_SERVICE Then
            If mAction.REBOOT Then
                Helpers.Logger.WriteRelative("STOP_SERVICE => REBOOT = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.RESTART Then
                Helpers.Logger.WriteRelative("STOP_SERVICE => RESTART = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.SHOW_WARNING Then
                Helpers.Logger.WriteRelative("STOP_SERVICE => SHOW_WARNING = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.KILLSITEKIOSK Then
                Helpers.Logger.WriteRelative("STOP_SERVICE => KILLSITEKIOSK = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.REBOOT = False
            mAction.RESTART = False
            mAction.SHOW_WARNING = False
            mAction.KILLSITEKIOSK = False
        End If

        'All is ok, clean all actions, just to be sure
        If mAction.OK Then
            If mAction.REBOOT Then
                Helpers.Logger.WriteRelative("OK => REBOOT = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.RESTART Then
                Helpers.Logger.WriteRelative("OK => RESTART = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.SHOW_WARNING Then
                Helpers.Logger.WriteRelative("OK => SHOW_WARNING = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.KILLSITEKIOSK Then
                Helpers.Logger.WriteRelative("OK => KILLSITEKIOSK = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.REBOOT = False
            mAction.RESTART = False
            mAction.SHOW_WARNING = False
            mAction.KILLSITEKIOSK = False
        End If

        'cancel all actions, due to admin mode or maintenance
        If mAction.IGNORED Then
            If mAction.SHOW_WARNING Then
                Helpers.Logger.WriteRelative("IGNORED => SHOW_WARNING = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.REBOOT Then
                Helpers.Logger.WriteRelative("IGNORED => REBOOT = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.RESTART Then
                Helpers.Logger.WriteRelative("IGNORED => RESTART = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.KILLSITEKIOSK Then
                Helpers.Logger.WriteRelative("IGNORED => KILLSITEKIOSK = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.SHOW_WARNING = False
            mAction.REBOOT = False
            mAction.RESTART = False
            mAction.KILLSITEKIOSK = False
        End If

        'If RESTART is set, disable REBOOT and SHOW_WARNING to prevent conflicts
        If mAction.RESTART Then
            If mAction.REBOOT Then
                Helpers.Logger.WriteRelative("RESTART => REBOOT = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.SHOW_WARNING Then
                Helpers.Logger.WriteRelative("RESTART => SHOW_WARNING = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.KILLSITEKIOSK Then
                Helpers.Logger.WriteRelative("RESTART => KILLSITEKIOSK = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.REBOOT = False
            mAction.SHOW_WARNING = False
            mAction.KILLSITEKIOSK = False
        End If


        If mAction.REBOOT Then
            If mAction.SHOW_WARNING Then
                Helpers.Logger.WriteRelative("REBOOT => SHOW_WARNING = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.KILLSITEKIOSK Then
                Helpers.Logger.WriteRelative("REBOOT => KILLSITEKIOSK = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.SHOW_WARNING = False
            mAction.KILLSITEKIOSK = False
        End If

        ''  if RESTART and SHOW_WARNING are both set, RESTART is reset
        'If mAction.RESTART And mAction.SHOW_WARNING Then
        '    mAction.RESTART = False
        'End If
        ''  if RESTART and REBOOT are both set, RESTART is reset
        'If mAction.RESTART And mAction.REBOOT Then
        '    mAction.RESTART = False
        'End If
        ''  if REBOOT and SHOW_WARNING are both set, SHOW_WARNING is reset
        'If mAction.REBOOT And mAction.SHOW_WARNING Then
        '    mAction.SHOW_WARNING = False
        'End If
    End Sub

    Public Shared Function DetermineAction() As Boolean
        Try
            Dim iLastActivity As Integer, iNow As Integer, iWarningStarted As Integer, iWarningCountdown As Integer
            Dim iLastCancelDate As Integer

            ResetAll()

            mCurrentState = LOBBYPC_STATE.OK
            mPreviousState = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__LastState)


            'Collect the info

            'check if SiteKiosk is installed
            Helpers.Logger.WriteRelative("check if SiteKiosk is installed", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Helpers.SiteKiosk.IsInstalled Then
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                mCurrentState = mCurrentState Or LOBBYPC_STATE.SITEKIOSK_NOT_INSTALLED
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if Explorer is running
            Helpers.Logger.WriteRelative("check if Explorer is running", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Helpers.Processes.IsProcessRunning(LPCConstants.FilesAndFolders.FILE__Explorer) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.EXPLORER_PROCESS_RUNNING
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if SiteKiosk is running
            Helpers.Logger.WriteRelative("check if SiteKiosk is running", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not Helpers.Processes.IsProcessRunning(LPCConstants.FilesAndFolders.FILE__SiteKiosk) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.SITEKIOSK_PROCESS_NOT_RUNNING
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if secure user is logged in
            Helpers.Logger.WriteRelative("check if secure user '" & mUsernameToCheck & "' is logged in", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not Helpers.Processes.IsUserLoggedIn(mUsernameToCheck) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if trigger exe is running
            Helpers.Logger.WriteRelative("check if trigger process is running", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not Helpers.Processes.IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.TRIGGER_PROCESS_NOT_RUNNING
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                'also check if it can write to the registry, VERY IMPORTANT!!!
                Helpers.Logger.WriteRelative("yep, but does it write to the registry?", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                If Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerStarted) Then
                    'it says it started, so we should be fine
                    Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                Else
                    'very bad
                    Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                    mCurrentState = mCurrentState Or LOBBYPC_STATE.CAN_NOT_WRITE_TO_REGISTRY
                End If
            End If


            'check if lastactivity exe is running
            Helpers.Logger.WriteRelative("check if lastactivity process is running", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not Helpers.Processes.IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.LASTACTIVITY_PROCESS_NOT_RUNNING
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if error exe is running
            Helpers.Logger.WriteRelative("check if error process is running", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not Helpers.Processes.IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogError) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.ERROR_PROCESS_NOT_RUNNING
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if warning is running
            Helpers.Logger.WriteRelative("check if warning is running", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Helpers.Processes.IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogWarning) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if warning is signalling an error
            Helpers.Logger.WriteRelative("check if warning is signalling an error", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailure) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.WATCHDOG_WARNING_FAILED
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'Check if admin mode is active, or if it´s active for too long
            Helpers.Logger.WriteRelative("check if pc is in admin mode", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, LPCConstants.RegistryKeys.VALUE__AdminModeActive) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE
                Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)

                Helpers.Logger.WriteRelative("check if admin mode is active for too long", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)

                iLastCancelDate = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, LPCConstants.RegistryKeys.VALUE__AdminModeActiveTime)
                iNow = Helpers.TimeDate.ToUnix()

                If (iNow - iLastCancelDate) > LPCConstants.TimeoutsAndDelaysAndCounters.TIMEOUT__AdminMode Then
                    Helpers.Logger.WriteRelative("yep, cancelling admin mode", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE_CANCELLED

                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, LPCConstants.RegistryKeys.VALUE__AdminModeActive, False)
                    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, LPCConstants.RegistryKeys.VALUE__AdminModeActiveTime, 0)
                End If
            Else
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if LogMeIn is connected
            If LPCGlobals.Settings.CheckFoRemoteConnection Then
                Dim iLmiConnCount As Integer = Helpers.Network.LogMeIn.GetConnectionCount()
                Helpers.Logger.WriteRelative("check if LogMeIn is connected", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                Helpers.Logger.WriteRelative("found " & iLmiConnCount & " active LogMeIn connections, threshold is >=" & LPCGlobals.Settings.LogMeInThreshold, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)

                If iLmiConnCount >= LPCGlobals.Settings.LogMeInThreshold Then
                    mCurrentState = mCurrentState Or LOBBYPC_STATE.REMOTE_CONTROL_CONNECTED
                    Helpers.Logger.WriteRelative("remote user is connected", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                Else
                    Helpers.Logger.WriteRelative("no remote user", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                End If


                'check if TakeControl is connected
                Dim iTakeControlConnCount As Integer = Helpers.Network.TakeControl.GetConnectionCount()
                Helpers.Logger.WriteRelative("check if TakeControl is connected", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                Helpers.Logger.WriteRelative("found " & iTakeControlConnCount & " active TakeControl connections, threshold is >=" & LPCGlobals.Settings.TakeControlThreshold, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)

                If iTakeControlConnCount >= LPCGlobals.Settings.TakeControlThreshold Then
                    mCurrentState = mCurrentState Or LOBBYPC_STATE.REMOTE_CONTROL_CONNECTED
                    Helpers.Logger.WriteRelative("remote user is connected", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                Else
                    Helpers.Logger.WriteRelative("no remote user", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                End If
            End If


            'get last activity
            Helpers.Logger.WriteRelative("get last activity", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)

            iLastActivity = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_LastActivity, LPCConstants.RegistryKeys.VALUE__LastActivity)
            iNow = Helpers.TimeDate.ToUnix()
            mAction.LAST_ACTIVITY = iLastActivity
            If (iNow - iLastActivity) <> 0 Then
                Helpers.Logger.WriteRelative((iNow - iLastActivity).ToString & " seconds ago", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                Helpers.Logger.WriteRelative("1 second ago", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'are we in logon screen?
            If Not mCurrentState And LOBBYPC_STATE.EXPLORER_PROCESS_RUNNING Then
                If mCurrentState And LOBBYPC_STATE.SITEKIOSK_PROCESS_NOT_RUNNING Then
                    If mCurrentState And LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER Then
                        Helpers.Logger.WriteRelative("it seems we are in Windows logon screen", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                        mCurrentState = mCurrentState Or LOBBYPC_STATE.WINDOWS_LOGON_SCREEN
                    End If
                End If
            End If


            'Is warning running too long?
            If mCurrentState And LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING Then
                Helpers.Logger.WriteRelative("check if warning is running for too long", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                iWarningStarted = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTimestamp)
                iWarningCountdown = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupCountdown)
                iNow = Helpers.TimeDate.ToUnix()

                If (iNow - iWarningStarted) > (iWarningCountdown + 2 * LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__ServiceLoop) Then
                    Helpers.Logger.WriteRelative("yep", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    mCurrentState = mCurrentState Or LOBBYPC_STATE.WATCHDOG_WARNING_FAILED
                End If
            End If


            'check if we are in a boot loop
            Helpers.Logger.WriteRelative("check if there is a boot loop", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Dim iRebootCount As Integer = 0
            Dim iRebootLast As Integer = 0

            iRebootCount = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Reboot, LPCConstants.RegistryKeys.VALUE__RebootCount, LPCConstants.RegistryKeys.DEFAULTVALUE__RebootCount)
            iRebootLast = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Reboot, LPCConstants.RegistryKeys.VALUE__RebootLast, LPCConstants.RegistryKeys.DEFAULTVALUE__RebootLast)
            iNow = Helpers.TimeDate.ToUnix()

            Helpers.Logger.WriteRelative("iRebootCount=" & iRebootCount.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Helpers.Logger.WriteRelative("iRebootLast=" & iRebootLast.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Helpers.Logger.WriteRelative("LPCGlobals.Settings.ConsecutiveRebootsMax=" & LPCGlobals.Settings.ConsecutiveRebootsMax.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)


            If iRebootCount > LPCGlobals.Settings.ConsecutiveRebootsMax Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.REBOOT_LOOP
                If (iNow - iRebootLast) < 600 Then
                    'Booted in the last 10 minutes
                    Helpers.Logger.WriteRelative("yes", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                Else
                    Helpers.Logger.WriteRelative("yes, but last reboot was longer than 10 minutes ago", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                End If
            Else
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if we are in emergency config mode
            Helpers.Logger.WriteRelative("check if we are in emergency config mode", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Dim sCurrentSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()

            LPCGlobals.Settings.Integrity.EmergencyConfig = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                    LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_EmergencyConfig,
                                                                    LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfig)
            LPCGlobals.Settings.Integrity.EmergencyConfigCount = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                    LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_EmergencyConfigCount,
                                                                    LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfigCount)

            Helpers.Logger.WriteRelative("current config", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Helpers.Logger.WriteRelative(sCurrentSkCfgFile, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
            Helpers.Logger.WriteRelative("emergency config", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Helpers.Logger.WriteRelative(LPCGlobals.Settings.Integrity.EmergencyConfig.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
            Helpers.Logger.WriteRelative("emergency config count", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Helpers.Logger.WriteRelative(LPCGlobals.Settings.Integrity.EmergencyConfigCount.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)

            If sCurrentSkCfgFile.ToLower.Equals(LPCGlobals.Settings.Integrity.EmergencyConfig.ToLower) Then
                Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_EmergencyConfigCount,
                                          LPCGlobals.Settings.Integrity.EmergencyConfigCount + 1)

                mCurrentState = mCurrentState Or LOBBYPC_STATE.EMERGENCY_CONFIG_MODE
            Else
                Helpers.Logger.WriteRelative("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'Helpers.Logger.WriteRelative("resetting back to normal config", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)

            'Helpers.Logger.WriteRelative("finding youngest config", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
            'Helpers.Logger.WriteRelative("path", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)

            'Dim sConfigFolder As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "Config")
            'Dim sYoungestConfig As String = ""

            'Helpers.Logger.WriteRelative(sConfigFolder, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)

            'sYoungestConfig = Helpers.FilesAndFolders.GetNewestFile(sConfigFolder, "*.skcfg")

            'Helpers.Logger.WriteRelative("found", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
            'Helpers.Logger.WriteRelative(sYoungestConfig, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)

            'Helpers.Logger.WriteRelative("checking", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
            'Try
            '    Dim xmlSkCfg As Xml.XmlDocument

            '    xmlSkCfg = New Xml.XmlDocument
            '    xmlSkCfg.Load(sYoungestConfig)

            '    Helpers.Logger.WriteRelative("seems valid", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)

            '    Helpers.Logger.WriteRelative("setting in registry", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
            '    If Helpers.SiteKiosk.SetSiteKioskActiveConfig(sYoungestConfig) Then
            '        Helpers.Logger.WriteRelative("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)
            '    Else

            '    End If
            'Catch ex As Exception
            '    Helpers.Logger.WriteRelative("invalid", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 5)
            'End Try



            'ok, sort out the action
            Helpers.Logger.WriteRelative("set appropriate action", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_INSTALLED Then
                Helpers.Logger.WriteRelative("SITEKIOSK_NOT_INSTALLED => STOP_SERVICE", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                mAction.STOP_SERVICE = True
                ErrorTrigger.TriggerErrorPopup("Service stopped", "OVCC is not installed on this machine.\nPlease contact the helpdesk.", ErrorTrigger.ERROR_SEVERITY.FATAL_ERROR, False)
            Else
                If mCurrentState And LOBBYPC_STATE.EMERGENCY_CONFIG_MODE Then
                    Helpers.Logger.WriteRelative("EMERGENCY_CONFIG_MODE => reset config", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)

                    Helpers.Logger.WriteRelative("finding youngest config", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                    Helpers.Logger.WriteRelative("path", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)

                    Dim sConfigFolder As String = IO.Path.Combine(Helpers.SiteKiosk.GetSiteKioskInstallFolder(), "Config")
                    Dim sYoungestConfig As String = ""

                    Helpers.Logger.WriteRelative(sConfigFolder, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)

                    sYoungestConfig = Helpers.FilesAndFolders.GetNewestFile(sConfigFolder, "*.skcfg")

                    Helpers.Logger.WriteRelative("found", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
                    Helpers.Logger.WriteRelative(sYoungestConfig, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)

                    Helpers.Logger.WriteRelative("checking", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
                    Try
                        Dim xmlSkCfg As Xml.XmlDocument

                        xmlSkCfg = New Xml.XmlDocument
                        xmlSkCfg.Load(sYoungestConfig)

                        Helpers.Logger.WriteRelative("seems valid", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)

                        Helpers.Logger.WriteRelative("setting in registry", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)
                        If Helpers.SiteKiosk.SetSiteKioskActiveConfig(sYoungestConfig) Then
                            Helpers.Logger.WriteRelative("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)
                        Else

                        End If
                    Catch ex As Exception
                        Helpers.Logger.WriteRelative("invalid", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    End Try
                End If

                If mCurrentState And LOBBYPC_STATE.WINDOWS_LOGON_SCREEN Then
                    Helpers.Logger.WriteRelative("WINDOWS_LOGON_SCREEN => RESTART", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    mAction.RESTART = True
                Else
                    If mCurrentState And LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER Then
                        If Not mCurrentState And LOBBYPC_STATE.EXPLORER_PROCESS_RUNNING Then
                            Helpers.Logger.WriteRelative("NOT_RUNNING_AS_SECURE_USER & !EXPLORER_RUNNING => RESTART", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.RESTART = True
                        Else
                            Helpers.Logger.WriteRelative("check for mouse/keyboard activity", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            iLastActivity = mAction.LAST_ACTIVITY
                            iNow = Helpers.TimeDate.ToUnix()

                            Helpers.Logger.WriteRelative(iNow.ToString & " - " & iLastActivity.ToString & " > " & LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__Warning.ToString & " ?", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                            If (iNow - iLastActivity) > LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__Warning Then
                                'Pc has been inactive for too long
                                Helpers.Logger.WriteRelative("inactive for too long", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 4)

                                'If mCurrentState And LOBBYPC_STATE.ADMIN_MODE Then
                                '    mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE_CANCELLED

                                '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, LPCConstants.RegistryKeys.VALUE__AdminModeActive, False)
                                '    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_AdminMode, LPCConstants.RegistryKeys.VALUE__AdminModeActiveTime, 0)
                                'End If

                                Helpers.Logger.WriteRelative("NOT_RUNNING_AS_SECURE_USER & EXPLORER_RUNNING => SHOW_WARNING", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 5)
                                mAction.SHOW_WARNING = True

                                If Not mCurrentState And LOBBYPC_STATE.SITEKIOSK_PROCESS_NOT_RUNNING Then
                                    Helpers.Logger.WriteRelative("SHOW_WARNING & !SITEKIOSK_NOT_RUNNING => WARNING_LOOSE", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 6)
                                    mAction.WARNING_LOOSE = True
                                End If
                            Else
                                Helpers.Logger.WriteRelative("NOT_RUNNING_AS_SECURE_USER & EXPLORER_RUNNING & ACTIVITY => IGNORED", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 3)
                                mAction.IGNORED = True
                            End If
                        End If
                    Else
                        If mCurrentState And LOBBYPC_STATE.SITEKIOSK_PROCESS_NOT_RUNNING Then
                            Helpers.Logger.WriteRelative("!NOT_RUNNING_AS_SECURE_USER & SITEKIOSK_NOT_RUNNING => SHOW_WARNING", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.SHOW_WARNING = True
                        Else
                            Helpers.Logger.WriteRelative("!NOT_RUNNING_AS_SECURE_USER & !SITEKIOSK_NOT_RUNNING => OK", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.OK = True
                        End If
                    End If

                    If mAction.SHOW_WARNING Then
                        'do some extra checks before triggering the warning popup

                        'the trigger doesn't seem to be able to write to the registry, very bad!!!
                        If mCurrentState And LOBBYPC_STATE.CAN_NOT_WRITE_TO_REGISTRY Then
                            Helpers.Logger.WriteRelative("CANCELLING THE WARNING, CAN NOT WRITE TO REGISTRY!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                            Helpers.Logger.WriteRelative("SHOW_WARNING & CAN_NOT_WRITE_TO_REGISTRY => SHOW_WARNING = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.SHOW_WARNING = False
                        End If

                        If mCurrentState And LOBBYPC_STATE.TRIGGER_PROCESS_NOT_RUNNING Then
                            Helpers.Logger.WriteRelative("SHOW_WARNING & TRIGGER_NOT_RUNNING => RESTART", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.RESTART = True
                        End If
                        If mCurrentState And LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING Then
                            Helpers.Logger.WriteRelative("SHOW_WARNING & WATCHDOG_WARNING_RUNNING => SHOW_WARNING = false", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.SHOW_WARNING = False
                        End If
                    End If
                End If

                If mCurrentState And LOBBYPC_STATE.WATCHDOG_WARNING_FAILED Then
                    'The warning popup signalled an error, let's restart
                    Helpers.Logger.WriteRelative("warning is signalling an error", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    Helpers.Logger.WriteRelative("error: " & Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError), Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    mAction.RESTART = True

                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailure, False)
                    Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError, "")
                End If
                If mCurrentState And LOBBYPC_STATE.ADMIN_MODE Then
                    Helpers.Logger.WriteRelative("admin mode is active", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)

                    If mCurrentState And LOBBYPC_STATE.ADMIN_MODE_CANCELLED Then
                        Helpers.Logger.WriteRelative("never mind, it's cancelled", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    Else
                        If mCurrentState And LOBBYPC_STATE.WINDOWS_LOGON_SCREEN Then
                            Helpers.Logger.WriteRelative("but machine was left in logon screen.", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                        Else
                            'Admin mode, ignore all actions
                            mAction.IGNORED = True
                        End If
                    End If
                End If

                'remote connection protection!!
                If mCurrentState And LOBBYPC_STATE.REMOTE_CONTROL_CONNECTED Then
                    'Someone is connected through LogMeIn or N-Central/TakeControl, ignore all actions
                    mAction.IGNORED = True
                End If

                'reboot loop?
                If mCurrentState And LOBBYPC_STATE.REBOOT_LOOP Then
                    mAction.REBOOT_LOOP = True
                End If
            End If

            'Action correction
            ActionCorrection()
        Catch ex As Exception
            'Something went very wrong!!!
            Helpers.Logger.Write("Exception in LPCState.DetermineAction", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)
            Helpers.Logger.Write(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Helpers.Logger.Write(ex.StackTrace, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            mCurrentState = mCurrentState Or LOBBYPC_STATE.GENERAL_FAILURE
            mAction.FAILURE = True
            mAction.LAST_ERROR = ex.Message

            ResetAll()

            Return False
        End Try

        Return True
    End Function

    Public Shared Sub ShowPreviousLPCStateFlags()
        _ShowActiveLPCStateFlags(mPreviousState)
    End Sub

    Public Shared Sub ShowCurrentLPCStateFlags()
        _ShowActiveLPCStateFlags(mCurrentState)
    End Sub

    Private Shared Sub _ShowActiveLPCStateFlags(state As LOBBYPC_STATE)
        Dim aStates As Array
        aStates = [Enum].GetValues(GetType(LOBBYPC_STATE))

        For Each iState As Integer In aStates
            If state And iState Then
                Helpers.Logger.WriteRelative([Enum].GetName(GetType(LOBBYPC_STATE), iState), Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            End If
        Next
    End Sub


    Public Shared Function ShowPreviousLPCStateFlagsAsString() As String
        Return _ShowLPCStateFlagsAsString(mPreviousState)
    End Function

    Public Shared Function ShowCurrentLPCStateFlagsAsString() As String
        Return _ShowLPCStateFlagsAsString(mCurrentState)
    End Function

    Private Shared Function _ShowLPCStateFlagsAsString(state As LOBBYPC_STATE) As String
        Dim aStates As Array
        aStates = [Enum].GetValues(GetType(LOBBYPC_STATE))

        Dim str As String = ""

        For Each iState As Integer In aStates
            If state And iState Then
                str &= [Enum].GetName(GetType(LOBBYPC_STATE), iState) & vbCrLf
            End If
        Next

        Return str
    End Function
End Class
