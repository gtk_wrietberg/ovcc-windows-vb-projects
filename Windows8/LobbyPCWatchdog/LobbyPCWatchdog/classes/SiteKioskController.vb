﻿Public Class SiteKioskController
    Private Shared mBuild As String = ""
    Private Shared mInstallDir As String = ""
    Private Shared mLastCfg As String = ""

    Private Shared mInitialised As Boolean = False

    Private Shared mStuffSet As Boolean = False


    Public Enum ERRORCODES As Integer
        ALL_FINE = 0
        NOT_INSTALLED = 1
        CONFIG_FILE_MISSING = 2
        STARTUP_PATH_MISSING = 4
        STARTUP_XML_MISSING = 8
        STARTUP_XML_UPDATE_FAILED = 16
        STARTUP_EXE_MISSING = 32
        EXECUTING_SKSTARTUP_FAILED = 64
    End Enum

    Private Shared mError As ERRORCODES = ERRORCODES.ALL_FINE
    Public Shared ReadOnly Property SiteKiosk_Error As ERRORCODES
        Get
            Return mError
        End Get
    End Property

    Public Shared Sub ResetError()
        mError = ERRORCODES.ALL_FINE
    End Sub

    Public Shared ReadOnly Property Build As String
        Get
            Return mBuild
        End Get
    End Property

    Public Shared ReadOnly Property InstallDir As String
        Get
            Return mInstallDir
        End Get
    End Property

    Public Shared ReadOnly Property LastCfg As String
        Get
            Return mLastCfg
        End Get
    End Property

    Public Shared Sub ReadRegistryValues()
        mBuild = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__SiteKiosk, LPCConstants.RegistryKeys.VALUE__Build)
        mInstallDir = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__SiteKiosk, LPCConstants.RegistryKeys.VALUE__InstallDir)
        mLastCfg = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__SiteKiosk, LPCConstants.RegistryKeys.VALUE__LastCfg)

        mInitialised = True
    End Sub

    Public Shared Function UpdateAutoStartupFile() As Boolean
        Dim sFile1 As String, sFile2 As String

        sFile1 = mInstallDir
        sFile2 = mInstallDir

        Helpers.Logger.WriteRelative("UpdateAutoStartupFile", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        Helpers.Logger.WriteRelative("from registry", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        Helpers.Logger.WriteRelative(sFile1, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

        sFile1 = LPCConstants.FilesAndFolders.FILE__AutoStartupXmlFile.Replace("%%SITEKIOSK_FOLDER%%", mInstallDir)
        sFile2 = LPCConstants.FilesAndFolders.FILE__AutoStartupCleanXmlFile.Replace("%%SITEKIOSK_FOLDER%%", mInstallDir)

        Helpers.Logger.WriteRelative("full", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        Helpers.Logger.WriteRelative(sFile1, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        Helpers.Logger.WriteRelative(sFile2, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)


        If Not IO.Directory.Exists(IO.Path.GetDirectoryName(sFile1)) Then
            Helpers.Logger.WriteRelative("ERROR", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Helpers.Logger.WriteRelative("Path '" & IO.Path.GetDirectoryName(sFile1) & "' not found!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            mError = mError Or ERRORCODES.STARTUP_PATH_MISSING

            Return False
        End If


        'Helpers.Logger.WriteRelative("UpdateAutoStartupFile", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        'Helpers.Logger.WriteRelative(sFile, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If Not IO.File.Exists(sFile1) Then
            'Uh oh, startup.xml does not exist. Let's create it from default, and hope it works....
            Helpers.Logger.WriteRelative("'" & sFile1 & "' doesn't exist, trying to create", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            Try
                Dim objWriter As New System.IO.StreamWriter(sFile1)
                objWriter.Write(My.Resources.startup_xml)
                objWriter.Close()
            Catch ex As Exception
                Helpers.Logger.WriteRelative("FAIL: " & ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            End Try
        End If

        If Not IO.File.Exists(sFile1) Then
            'If it still doesn't exist, we're in trouble
            Helpers.Logger.WriteRelative("still doesn't exist, we're screwed", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            mError = mError Or ERRORCODES.STARTUP_XML_MISSING

            Return False
        End If


        If Not IO.File.Exists(sFile2) Then
            'Uh oh, startup.xml does not exist. Let's create it from default, and hope it works....
            Helpers.Logger.WriteRelative("'" & sFile2 & "' doesn't exist, trying to create", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            Try
                Dim objWriter As New System.IO.StreamWriter(sFile2)
                objWriter.Write(My.Resources.startup_clean)
                objWriter.Close()
            Catch ex As Exception
                Helpers.Logger.WriteRelative("FAIL: " & ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            End Try
        End If

        If Not IO.File.Exists(sFile2) Then
            'If it still doesn't exist, we're in trouble
            Helpers.Logger.WriteRelative("still doesn't exist, we're screwed", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            mError = mError Or ERRORCODES.STARTUP_XML_MISSING

            Return False
        End If


        'We need to make sure the config file in startup.xml is up to date
        Helpers.Logger.WriteRelative("updating", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
        If Not IO.File.Exists(mLastCfg) Then
            'Config file doesn't exist
            Helpers.Logger.WriteRelative("config file '" & mLastCfg & "' doesn't exist", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            mError = mError Or ERRORCODES.CONFIG_FILE_MISSING

            Return False
        End If

        Helpers.Logger.WriteRelative(sFile1, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            Dim xmlNode_Config As Xml.XmlNode
            Dim xmlNode_LastCfg As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument
            xmlSkCfg.Load(sFile1)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-startup", ns)
            xmlNode_Config = xmlNode_Root.SelectSingleNode("sk:config", ns)
            xmlNode_LastCfg = xmlNode_Config.SelectSingleNode("sk:LastCfg", ns)

            xmlNode_LastCfg.InnerText = mLastCfg

            xmlSkCfg.Save(sFile1)

            Helpers.Logger.WriteRelative("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
        Catch ex As Exception
            Helpers.Logger.WriteRelative("exception", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            Helpers.Logger.WriteRelative(ex.StackTrace, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            mError = mError Or ERRORCODES.STARTUP_XML_UPDATE_FAILED

            Return False
        End Try


        Helpers.Logger.WriteRelative(sFile2, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            Dim xmlNode_Config As Xml.XmlNode
            Dim xmlNode_LastCfg As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument
            xmlSkCfg.Load(sFile1)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-startup", ns)
            xmlNode_Config = xmlNode_Root.SelectSingleNode("sk:config", ns)
            xmlNode_LastCfg = xmlNode_Config.SelectSingleNode("sk:LastCfg", ns)

            xmlNode_LastCfg.InnerText = mLastCfg

            xmlSkCfg.Save(sFile2)

            Helpers.Logger.WriteRelative("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
        Catch ex As Exception
            Helpers.Logger.WriteRelative("exception", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            Helpers.Logger.WriteRelative(ex.StackTrace, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            mError = mError Or ERRORCODES.STARTUP_XML_UPDATE_FAILED

            Return False
        End Try


        mStuffSet = True
        Return True
    End Function

    Public Shared Function SetAutoStartup() As Boolean
        If Not mStuffSet Then
            Return False
        End If

        Dim sExeFile As String, sXmlFile As String, sArg As String

        sExeFile = LPCConstants.FilesAndFolders.FILE__AutoStartupExe.Replace("%%SITEKIOSK_FOLDER%%", mInstallDir)
        sXmlFile = LPCConstants.FilesAndFolders.FILE__AutoStartupXmlFile.Replace("%%SITEKIOSK_FOLDER%%", mInstallDir)

        sArg = "/i """ & sXmlFile & """"

        Helpers.Logger.WriteRelative("SetAutoStartup", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
        Helpers.Logger.WriteRelative(sExeFile & " " & sXmlFile, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If Not IO.File.Exists(sExeFile) Then
            'WTF
            Helpers.Logger.WriteRelative("file '" & sExeFile & "' doesn't exist", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            mError = mError Or ERRORCODES.STARTUP_EXE_MISSING

            Return False
        End If
        If Not IO.File.Exists(sXmlFile) Then
            'WTF
            Helpers.Logger.WriteRelative("file '" & sXmlFile & "' doesn't exist", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            mError = mError Or ERRORCODES.STARTUP_XML_MISSING

            Return False
        End If

        Try
            Helpers.Logger.WriteRelative("setting", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            Helpers.Processes.StartProcess(sExeFile, sArg, True, 10000)
            Helpers.Logger.WriteRelative("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
        Catch ex As Exception
            Helpers.Logger.WriteRelative("exception", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.WriteRelative(ex.StackTrace, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            mError = mError Or ERRORCODES.EXECUTING_SKSTARTUP_FAILED

            Return False

        End Try

        Return True
    End Function
End Class
