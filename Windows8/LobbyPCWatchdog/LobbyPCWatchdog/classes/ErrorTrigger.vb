﻿Public Class ErrorTrigger
    Public Enum ERROR_SEVERITY
        WARNING = 0
        NORMAL_ERROR = 1
        FATAL_ERROR = 2
    End Enum

    Public Shared Sub TriggerErrorPopup(ErrorTitle As String, ErrorDescription As String, ErrorSeverity As ERROR_SEVERITY, Optional RebootOption As Boolean = False)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Timestamp, Now.ToString)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Title, ErrorTitle)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Description, ErrorDescription)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Severity, ErrorSeverity)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_RebootOption, RebootOption)
    End Sub
End Class
