﻿Public Class LPCConstants
    Public Class TimeoutsAndDelaysAndCounters
        ''' <summary>
        ''' Service loop delay in seconds
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly DELAY__ServiceLoop As Long = 300

        ''' <summary>
        ''' Another service loop delay in seconds
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly DELAY__AnotherServiceLoop As Long = 3600

        ''' <summary>
        ''' First run delay in seconds
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly DELAY__FirstRun As Long = 60

        ''' <summary>
        ''' First run delay in seconds
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly DELAY__WatchdogWatchdogStart As Long = 60

        ''' <summary>
        ''' Amount of seconds system is inactive before warning
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly DELAY__Warning As Long = 30

        ''' <summary>
        ''' Check for keyboard/mouse activity every x seconds
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly DELAY__ActivityCheck As Long = 5

        ''' <summary>
        ''' Amount of seconds admin mode is active
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly TIMEOUT__AdminMode As Long = 300

        ''' <summary>
        ''' Amount of seconds before service start times out
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly TIMEOUT__ServiceStart As Long = 300

        ''' <summary>
        ''' Amount of fails before hard reboot
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared ReadOnly COUNT__Maxfails As Long = 3


        Public Shared ReadOnly COUNTERS__WarningPopupTriggerCountMax As Integer = 2

        Public Shared ReadOnly COUNTDOWN__WarningPopupReboot As Integer = 60
        Public Shared ReadOnly COUNTDOWN__WarningPopupRebootLoose As Integer = 300



        Public Shared ReadOnly INSTALLER__ServiceStatusCheckTimeoutSeconds As Integer = 30
    End Class

    Public Class FilesAndFolders
        Public Shared ReadOnly FILE__Explorer As String = "explorer.exe"
        Public Shared ReadOnly FILE__SiteKiosk As String = "sitekiosk.exe"

        Public Shared ReadOnly FILE__OLD_WatchdogService As String = "LobbyPCWatchDogService"
        Public Shared ReadOnly FILE__OLD_WatchdogController As String = "LobbyPCWatchDogController"
        Public Shared ReadOnly FILE__OLD_WatchdogWarning As String = "LobbyPCWatchDogWarning"
        Public Shared ReadOnly FILE__OLD_WatchdogTrigger As String = "LobbyPCWatchDogTrigger"

        Public Shared ReadOnly FILE__WatchdogService As String = "ovccwatchdog_service.exe"
        Public Shared ReadOnly FILE__WatchdogWarning As String = "ovccwatchdog_warning.exe"
        Public Shared ReadOnly FILE__WatchdogWarningTrigger As String = "ovccwatchdog_trigger.exe"
        Public Shared ReadOnly FILE__WatchdogLastActivity As String = "ovccwatchdog_lastactivity.exe"
        Public Shared ReadOnly FILE__WatchdogServiceEnable As String = "ovccwatchdog_enable.exe"
        Public Shared ReadOnly FILE__WatchdogServiceDisable As String = "ovccwatchdog_disable.exe"
        Public Shared ReadOnly FILE__WatchdogError As String = "ovccwatchdog_error.exe"
        Public Shared ReadOnly FILE__WatchdogMuzzle As String = "ovccwatchdog_muzzle.exe"
        'Public Shared ReadOnly FILE__WatchdogEmergencyDeployService As String = "ovccwatchdog_emergencydeploy_service.exe"
        Public Shared ReadOnly FILE__WatchdogWatchdogService As String = "ovccwatchdogwatchdog_service.exe"
        Public Shared ReadOnly FILE__WatchdogEmergencyDeployApp As String = "ovccwatchdog_emergencydeploy.exe"

        Public Shared ReadOnly FILE__AutoStartupXmlFile As String = "%%SITEKIOSK_FOLDER%%\Config\startup.xml"
        Public Shared ReadOnly FILE__AutoStartupCleanXmlFile As String = "%%SITEKIOSK_FOLDER%%\Config\startup_clean.xml"
        Public Shared ReadOnly FILE__AutoStartupExe As String = "%%SITEKIOSK_FOLDER%%\SkStartup.exe"

        Public Shared ReadOnly FILE__LastActivityTempFilePath As String = Helpers.FilesAndFolders.GetAppDataLocalFolder() & "\Temp"

        Public Shared ReadOnly FILE__LastActivityTempFileName As String = "_LastActivity.tmp"

        Public Shared ReadOnly PATH__DefaultSitekioskProfilePath As String = "C:\Users\SiteKiosk"


        Public Shared ReadOnly Property FOLDER__ProgramFiles As String
            Get
                Dim sTmp As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

                If sTmp.Equals(String.Empty) Then
                    sTmp = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
                End If

                Return sTmp
            End Get
        End Property

        Public Shared ReadOnly Property FOLDER__ProgramFiles_64bit As String
            Get
                Return Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End Get
        End Property

        Public Shared ReadOnly Property FOLDER__ServicePath As String
            Get
                Dim sTmp As String = FOLDER__ProgramFiles & "\GuestTek\OVCCWatchdog"

                Return sTmp
            End Get
        End Property

        Public Shared ReadOnly Property FOLDER__Backup As String
            Get
                Dim sTmp As String = FOLDER__ProgramFiles & "\GuestTek\_backups"

                sTmp &= "\%%Application.ProductName%%\%%Application.ProductVersion%%\%%Backup.Date%%"

                Return sTmp
            End Get
        End Property

        Public Shared ReadOnly Property FOLDER__Logs As String
            Get
                Dim sTmp As String = FOLDER__ProgramFiles & "\GuestTek\_logs"

                Return sTmp
            End Get
        End Property
    End Class

    Public Class UsersAndPasswords
        Public Shared ReadOnly USERNAME__SiteKiosk As String = "sitekiosk"
        Public Shared ReadOnly USERNAME__LobbyPCPuppy As String = "__OVCC_Puppy__"
        Public Shared ReadOnly USERNAME__GuestTekAdmin As String = "guesttek"

        Public Shared ReadOnly PASSWORD__SiteKiosk As String = "Provisi0"
    End Class

    Public Class RegistryKeys
        Public Shared ReadOnly KEY_Windows_RunAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run"
        Public Shared ReadOnly KEY_Windows_RunOnceAtStartup As String = "HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\RunOnce"


        Public Shared ReadOnly KEY__Watchdog As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog"
        Public Shared ReadOnly VALUE__LastState As String = "LastState"
        Public Shared ReadOnly VALUE__DisabledUntilOVCCStart As String = "DisabledUntilOVCCStart"

        Public Shared ReadOnly DEFAULTVALUE__LastState As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__DisabledUntilOVCCStart As Boolean = False


        Public Shared ReadOnly KEY__Watchdog_Settings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\Settings"
        Public Shared ReadOnly VALUE__WatchdogSettingsDebug As String = "Debug"
        Public Shared ReadOnly VALUE__WatchdogSettingsTestMode As String = "TestMode"
        Public Shared ReadOnly VALUE__WatchdogSettingsSkipFirstRunDelay As String = "SkipFirstRunDelay"
        Public Shared ReadOnly VALUE__WatchdogSettingsCheckFoRemoteConnection As String = "CheckFoRemoteConnection"
        Public Shared ReadOnly VALUE__WatchdogSettingsPlaysound As String = "PlayStupidDogSound"
        Public Shared ReadOnly VALUE__WatchdogSettingsLogMeInThreshold As String = "LogMeInThreshold"
        Public Shared ReadOnly VALUE__WatchdogSettingsTakeControlThreshold As String = "TakeControlThreshold"
        Public Shared ReadOnly VALUE__WatchdogSettingsZipOldLogs As String = "ZipOldLogs"
        Public Shared ReadOnly VALUE__WatchdogSettingsZipOldLogsThreshold As String = "ZipOldLogsThreshold"
        Public Shared ReadOnly VALUE__WatchdogSettingsZipOldLogsForAllGuestTekApps As String = "ZipOldLogsForAllGuestTekApps"
        Public Shared ReadOnly VALUE__WatchdogSettingsCheckIntegrity As String = "CheckIntegrity"
        Public Shared ReadOnly VALUE__WatchdogSettingsVersionInventory As String = "VersionInventory"
        Public Shared ReadOnly VALUE__WatchdogSettingsMuzzled As String = "Muzzled"
        Public Shared ReadOnly VALUE__WatchdogSettingsConsecutiveRebootsMax As String = "ConsecutiveRebootsMax"
        Public Shared ReadOnly VALUE__WatchdogSettingsEnableWatchdogWatchdog As String = "EnableWatchdogWatchdog"

        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsDebug As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsTestMode As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsSkipFirstRunDelay As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsCheckFoRemoteConnection As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsPlaysound As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsLogMeInThreshold As Integer = 2
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsTakeControlThreshold As Integer = 2
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsZipOldLogs As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsZipOldLogsThreshold As Integer = 6
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsZipOldLogsForAllGuestTekApps As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsCheckIntegrity As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsVersionInventory As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsMuzzled As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsConsecutiveRebootsMax As Integer = 3
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettingsEnableWatchdogWatchdog As Boolean = False


        Public Shared ReadOnly KEY__WatchdogSettings_Integrity As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\Settings\Integrity"
        Public Shared ReadOnly VALUE__WatchdogSettings_Integrity_CheckFileAccess As String = "CheckFileAccess"
        Public Shared ReadOnly VALUE__WatchdogSettings_Integrity_CheckEmergencyConfig As String = "CheckEmergencyConfig"
        Public Shared ReadOnly VALUE__WatchdogSettings_Integrity_CheckThemeSettings As String = "CheckThemeSettings"
        Public Shared ReadOnly VALUE__WatchdogSettings_Integrity_CheckSiteKioskProfileBroken As String = "CheckSiteKioskProfileBroken"
        Public Shared ReadOnly VALUE__WatchdogSettings_Integrity_CheckSiteKioskProfilePath As String = "CheckSiteKioskProfilePath"
        Public Shared ReadOnly VALUE__WatchdogSettings_Integrity_DefaultSkin As String = "DefaultSkin"
        Public Shared ReadOnly VALUE__WatchdogSettings_Integrity_EmergencyConfig As String = "EmergencyConfig"
        Public Shared ReadOnly VALUE__WatchdogSettings_Integrity_EmergencyConfigCount As String = "EmergencyConfigCount"


        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettings_Integrity_CheckFileAccess As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettings_Integrity_CheckEmergencyConfig As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettings_Integrity_CheckThemeSettings As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettings_Integrity_CheckSiteKioskProfileBroken As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettings_Integrity_CheckSiteKioskProfilePath As Boolean = True
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettings_Integrity_DefaultSkin As String = "Windows 7 IE8 Skin"
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfig As String = "Emergency.skcfg"
        Public Shared ReadOnly DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfigCount As Integer = 0


        Public Shared ReadOnly KEY__Watchdog_Logging As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\Logging"
        Public Shared ReadOnly VALUE__CurrentLogfilePath As String = "CurrentLogfilePath"
        Public Shared ReadOnly VALUE__CurrentLogfileName As String = "CurrentLogfileName"

        Public Shared ReadOnly DEFAULTVALUE__CurrentLogfilePath As String = ""
        Public Shared ReadOnly DEFAULTVALUE__CurrentLogfileName As String = ""


        Public Shared ReadOnly KEY__Watchdog_OpenToAll As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_"
        Public Shared ReadOnly VALUE__OpenToAll_WriteCheck As String = "WriteCheck"
        Public Shared ReadOnly VALUE__OpenToAll_WriteCheckTimestamp As String = "WriteCheckTimestamp"

        Public Shared ReadOnly DEFAULTVALUE__OpenToAll_WriteCheck As String = ""


        Public Shared ReadOnly KEY__Watchdog_Disabled As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\Disabled"
        Public Shared ReadOnly VALUE__WatchdogDisabled As String = "Disabled"

        Public Shared ReadOnly DEFAULTVALUE__WatchdogDisabled As String = ""


        Public Shared ReadOnly KEY__Watchdog_Error As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\Error"
        Public Shared ReadOnly VALUE__WatchdogError_Timestamp As String = "Timestamp"
        Public Shared ReadOnly VALUE__WatchdogError_Title As String = "Title"
        Public Shared ReadOnly VALUE__WatchdogError_Description As String = "Description"
        Public Shared ReadOnly VALUE__WatchdogError_Severity As String = "Severity"
        Public Shared ReadOnly VALUE__WatchdogError_RebootOption As String = "RebootOption"

        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_Timestamp As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_Title As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_Description As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_Severity As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_RebootOption As Boolean = False


        Public Shared ReadOnly KEY__Watchdog_ErrorHistory As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\Error\History"
        Public Shared ReadOnly VALUE__WatchdogError_History As String() = New String() {"History00", "History01", "History02", "History03", "History04", "History05", "History06", "History07", "History08", "History09", "History10", "History11", "History12", "History13", "History14", "History15", "History16", "History17", "History18", "History19"}

        Public Shared ReadOnly DEFAULTVALUE__WatchdogError_History As String = ""


        Public Shared ReadOnly KEY__Watchdog_Warning As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\Warning"
        Public Shared ReadOnly VALUE__WarningPopupTriggerStarted As String = "TriggerStarted"
        Public Shared ReadOnly VALUE__WarningPopupTriggerCount As String = "TriggerCount"
        Public Shared ReadOnly VALUE__WarningPopupTestmode As String = "Testmode"
        Public Shared ReadOnly VALUE__WarningPopupLoose As String = "Loose"
        Public Shared ReadOnly VALUE__WarningPopupFailure As String = "Failure"
        Public Shared ReadOnly VALUE__WarningPopupFailureLastError As String = "FailureLastError"
        Public Shared ReadOnly VALUE__WarningPopupCountdown As String = "Countdown"
        Public Shared ReadOnly VALUE__WarningPopupTimestamp As String = "Timestamp"

        Public Shared ReadOnly DEFAULTVALUE__WarningPopupTriggerStarted As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupTriggerCount As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupTestmode As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupLoose As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupFailure As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupFailureLastError As String = ""
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupCountdown As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__WarningPopupTimestamp As Integer = 0


        Public Shared ReadOnly KEY__Watchdog_LastActivity As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\LastActivity"
        Public Shared ReadOnly VALUE__LastActivity As String = "LastActivity"

        Public Shared ReadOnly DEFAULTVALUE__LastActivity As Integer = 0


        Public Shared ReadOnly KEY__Watchdog_AdminMode As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\_\AdminMode"
        Public Shared ReadOnly VALUE__AdminModeActive As String = "Active"
        Public Shared ReadOnly VALUE__AdminModeActiveTime As String = "Time"

        Public Shared ReadOnly DEFAULTVALUE__AdminModeActive As Boolean = False
        Public Shared ReadOnly DEFAULTVALUE__AdminModeActiveTime As Integer = 0


        Public Shared ReadOnly KEY__Puppypower As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\PuppyPower"
        Public Shared ReadOnly VALUE__PuppyPowerUserName As String = "username"
        Public Shared ReadOnly VALUE__PuppyPowerPassWord As String = "password"

        Public Shared ReadOnly DEFAULTVALUE__PuppyPowerUserName As String = ""
        Public Shared ReadOnly DEFAULTVALUE__PuppyPowerPassWord As String = ""


        Public Shared ReadOnly KEY__Reboot As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\Watchdog\Reboot"
        Public Shared ReadOnly VALUE__RebootCount As String = "count"
        Public Shared ReadOnly VALUE__RebootLast As String = "last"

        Public Shared ReadOnly DEFAULTVALUE__RebootCount As Integer = 0
        Public Shared ReadOnly DEFAULTVALUE__RebootLast As Integer = 0


        Public Shared ReadOnly KEY__SiteKiosk As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"
        Public Shared ReadOnly VALUE__Build As String = "Build"
        Public Shared ReadOnly VALUE__InstallDir As String = "InstallDir"
        Public Shared ReadOnly VALUE__LastCfg As String = "LastCfg"
    End Class

    Public Class Service
        Public Class Old
            Public Shared ReadOnly Name As String = "LobbyPCWatchdogService"
        End Class

        Public Class Watchdog
            Public Shared ReadOnly Name As String = "OVCCWatchdog"
            Public Shared ReadOnly DisplayName As String = "OVCC Watchdog service"
            Public Shared ReadOnly Description As String = "Keeps OVCC in secure mode"
        End Class

        Public Class WatchdogWatchdog
            Public Shared ReadOnly Name As String = "zzz_OVCCWatchdogWatchdog" ' the zzz_ prefix is so that it is at the bottom of services list in Windows, less likely to be disabled by a well-meaning but clueless support person
            Public Shared ReadOnly DisplayName As String = "OVCC WatchdogWatchdog service"
            Public Shared ReadOnly Description As String = "Keeps OVCC Watchdog running"
        End Class

        Public Class EmergencyDeploy
            Public Shared ReadOnly Name As String = "OVCCWatchdog_EmergencyDeploy"
            Public Shared ReadOnly DisplayName As String = "OVCC Watchdog Emergency Deploy service"
            Public Shared ReadOnly Description As String = " "
        End Class
    End Class

    Public Class Misc
        Public Shared ReadOnly MagicalDisableString As String = "Blahblahblah"
        Public Shared ReadOnly DisablePassword As String = "d1sabl3"
        Public Shared ReadOnly CompletelyArbitraryProcessCountThreshold As Integer = 2
    End Class
End Class
