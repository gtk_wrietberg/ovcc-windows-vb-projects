Imports System.IO
Imports System.IO.Compression

Public Class MyLogger
    Private ReadOnly DefaultLogFile As String = "LobbyPCSoftwareOnlyInstallation.log"
    Private mLogFilePath As String
    Private mLogFile As String = ""
    Private pPrevDepth As Integer = 0
    Private pDoNotStoreLogLevel As Boolean = False

    Private mDebugging As Boolean = True
    Public Property Debugging() As Boolean
        Get
            Return mDebugging
        End Get
        Set(ByVal value As Boolean)
            mDebugging = value
        End Set
    End Property

    Private mExtraPrefix As String
    Public Property ExtraPrefix() As String
        Get
            Return mExtraPrefix
        End Get
        Set(ByVal value As String)
            mExtraPrefix = value
        End Set
    End Property


    Public Enum MESSAGE_TYPE
        LOG_DEFAULT = 0
        LOG_WARNING = 1
        LOG_ERROR = 2
        LOG_DEBUG = 6
    End Enum

    Public Property LogFilePath() As String
        Get
            Return mLogFilePath
        End Get
        Set(ByVal value As String)
            If value.EndsWith("\") Then
                mLogFilePath = value
            Else
                mLogFilePath = value & "\"
            End If
        End Set
    End Property

    Public Property LogFileName() As String
        Get
            Return mLogFile
        End Get
        Set(ByVal value As String)
            mLogFile = value
        End Set
    End Property

    Public Function WriteMessage(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Return Write(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
    End Function

    Public Function WriteMessageRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Return Write(sMessage, MESSAGE_TYPE.LOG_DEFAULT, iDepth, bDoNotWriteToFile)
    End Function

    Public Function WriteWarning(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Return Write(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
    End Function

    Public Function WriteWarningRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Return Write(sMessage, MESSAGE_TYPE.LOG_WARNING, iDepth, bDoNotWriteToFile)
    End Function

    Public Function WriteError(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Return Write(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
    End Function

    Public Function WriteErrorRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Return Write(sMessage, MESSAGE_TYPE.LOG_ERROR, iDepth, bDoNotWriteToFile)
    End Function

    Public Function WriteDebug(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Return Write(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
    End Function

    Public Function WriteDebugRelative(ByVal sMessage As String, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Return Write(sMessage, MESSAGE_TYPE.LOG_DEBUG, iDepth, bDoNotWriteToFile)
    End Function

    Public Function WriteRelative(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iRelativeDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        pDoNotStoreLogLevel = True
        Return Write(sMessage, cMessageType, pPrevDepth + iRelativeDepth)
    End Function

    Public Function Write(ByVal sMessage As String, Optional ByVal cMessageType As MESSAGE_TYPE = MESSAGE_TYPE.LOG_DEFAULT, Optional ByVal iDepth As Integer = 0, Optional ByVal bDoNotWriteToFile As Boolean = False) As String
        Dim sMsgTypePrefix As String, sMsgDatePrefix As String, sMsgPrefix As String, iDepthStep As Integer

        If iDepth < 0 Then
            iDepth = 0
        End If

        If Not mDebugging And cMessageType = MESSAGE_TYPE.LOG_DEBUG Then
            bDoNotWriteToFile = True
        End If

        sMessage = Trim(sMessage)

        Select Case cMessageType
            Case MESSAGE_TYPE.LOG_WARNING
                sMsgTypePrefix = "[*] "
            Case MESSAGE_TYPE.LOG_ERROR
                sMsgTypePrefix = "[!] "
            Case MESSAGE_TYPE.LOG_DEBUG
                sMsgTypePrefix = "[#] "
            Case MESSAGE_TYPE.LOG_DEFAULT
                sMsgTypePrefix = "[.] "
            Case Else
                sMsgTypePrefix = "[?] "
        End Select


        sMsgDatePrefix = Now.ToString & " - "

        sMsgPrefix = sMsgTypePrefix & sMsgDatePrefix

        If iDepth < pPrevDepth Then
            For iDepthStep = 1 To iDepth
                sMsgPrefix = sMsgPrefix & "| "
            Next

            sMsgPrefix = sMsgPrefix & vbCrLf
            sMsgPrefix = sMsgPrefix & sMsgTypePrefix & sMsgDatePrefix
        End If

        If iDepth > 0 Then
            For iDepthStep = 1 To iDepth - 1
                sMsgPrefix = sMsgPrefix & "| "
            Next

            sMsgPrefix = sMsgPrefix & "|-"
        End If

        If Not bDoNotWriteToFile Then
            UpdateLogfile(sMsgPrefix & sMessage)
        End If

        If Not pDoNotStoreLogLevel Then
            pPrevDepth = iDepth
        End If

        pDoNotStoreLogLevel = False

        Return sMsgPrefix & sMessage
    End Function

    Public Sub WriteWithoutDate(ByVal sMessage As String)
        UpdateLogfile(sMessage)
    End Sub

    Public Sub WriteEmptyLine()
        WriteWithoutDate(" ")
    End Sub

    Private Sub UpdateLogfile(ByVal sString As String)
        Try
            Dim sw As New IO.StreamWriter(mLogFilePath & mLogFile, True)
            sw.WriteLine(sString)
            sw.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Function ZipOldLogs() As Integer
        Dim dLogs As New DirectoryInfo(mLogFilePath)
        Dim fLogs As FileInfo() = dLogs.GetFiles("*.txt")
        Dim iCount As Integer = 0

        For Each fLog As IO.FileInfo In fLogs
            If Not fLog.Name = mLogFile Then
                If _zipFile(fLog.FullName) Then
                    iCount += 1
                End If
            End If
        Next

        Return iCount
    End Function

    Private Function _zipFile(filePath As String) As Boolean
        Try
            Dim zipPath As String = filePath & ".zip"

            Using newFile As ZipArchive = ZipFile.Open(zipPath, IO.Compression.ZipArchiveMode.Update)
                newFile.CreateEntryFromFile(filePath, Path.GetFileName(filePath), CompressionLevel.Optimal)
            End Using

            Return File.Exists(zipPath)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub New()
        mLogFile = DefaultLogFile
        mLogFilePath = ""
    End Sub
End Class
