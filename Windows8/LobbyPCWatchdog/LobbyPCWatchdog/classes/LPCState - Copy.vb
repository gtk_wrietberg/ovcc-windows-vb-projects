﻿Imports System.Runtime.InteropServices

Public Class LPCState
    <DllImport("advapi32.dll", SetLastError:=True)> _
    Private Shared Function OpenProcessToken( _
    ByVal ProcessHandle As IntPtr, _
    ByVal DesiredAccess As Integer, _
    ByRef TokenHandle As IntPtr _
    ) As Boolean
    End Function

    <DllImport("kernel32.dll", SetLastError:=True)> _
    Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
    End Function

    Public Enum LOBBYPC_STATE As Integer
        NOT_SET = 0
        SITEKIOSK_NOT_RUNNING = 1
        NOT_RUNNING_AS_SECURE_USER = 2
        EXPLORER_RUNNING = 4
        TRIGGER_NOT_RUNNING = 8
        WINDOWS_LOGON_SCREEN = 16
        WATCHDOG_WARNING_RUNNING = 32
        WATCHDOG_WARNING_FAILED = 64
        ADMIN_MODE = 128
        ADMIN_MODE_CANCELLED = 256
        SITEKIOSK_NOT_INSTALLED = 512
        GENERAL_FAILURE = 1024
    End Enum

    Public Structure LOBBYPC_ACTION
        Dim IGNORED As Boolean
        Dim LAST_ACTIVITY As Long
        Dim RESTART As Boolean
        Dim REBOOT As Boolean
        Dim STOP_SERVICE As Boolean
        Dim SHOW_WARNING As Boolean
        Dim WARNING_LOOSE As Boolean
        Dim OK As Boolean
        Dim STATE As Long
        Dim LAST_ERROR As String
    End Structure

    Private Shared mCurrentState As LOBBYPC_STATE
    Public Shared ReadOnly Property CurrentState As LOBBYPC_STATE
        Get
            Return mCurrentState
        End Get
    End Property

    Private Shared mPreviousState As LOBBYPC_STATE
    Public Shared ReadOnly Property PreviousState As LOBBYPC_STATE
        Get
            Return mPreviousState
        End Get
    End Property

    Private Shared mAction As LOBBYPC_ACTION
    Public Shared ReadOnly Property Action As LOBBYPC_ACTION
        Get
            Return mAction
        End Get
    End Property

    Private Shared mUsernameToCheck As String
    Public Shared Property UsernameToCheck As String
        Get
            Return mUsernameToCheck
        End Get
        Set(value As String)
            mUsernameToCheck = value
        End Set
    End Property

    Public Shared Sub ResetAction()
        mAction.IGNORED = False
        mAction.LAST_ACTIVITY = 0
        mAction.REBOOT = False
        mAction.RESTART = False
        mAction.SHOW_WARNING = False
        mAction.STOP_SERVICE = False
        mAction.OK = False
        mAction.STATE = mCurrentState
        mAction.WARNING_LOOSE = False
    End Sub

    Public Shared Sub ForceReboot()
        mAction.REBOOT = True
    End Sub

    Public Shared Sub ForceRestart()
        mAction.RESTART = True
    End Sub

    Public Shared Sub ForceShowWarning()
        mAction.SHOW_WARNING = True
    End Sub

    Public Shared Sub ActionCorrection()
        'Action correction
        LPCGlobals.Objects.Logger.WriteToLogRelative("action correction", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
        If mAction.STOP_SERVICE Then
            If mAction.REBOOT Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("STOP_SERVICE => REBOOT = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.RESTART Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("STOP_SERVICE => RESTART = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.SHOW_WARNING Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("STOP_SERVICE => SHOW_WARNING = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.REBOOT = False
            mAction.RESTART = False
            mAction.SHOW_WARNING = False
        End If
        If mAction.OK Then
            If mAction.REBOOT Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("OK => REBOOT = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.RESTART Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("OK => RESTART = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.SHOW_WARNING Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("OK => SHOW_WARNING = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.REBOOT = False
            mAction.RESTART = False
            mAction.SHOW_WARNING = False
        End If
        If mAction.RESTART Then
            If mAction.REBOOT Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("RESTART => REBOOT = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If
            If mAction.SHOW_WARNING Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("RESTART => SHOW_WARNING = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.REBOOT = False
            mAction.SHOW_WARNING = False
        End If
        If mAction.REBOOT Then
            If mAction.SHOW_WARNING Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("REBOOT => SHOW_WARNING = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.SHOW_WARNING = False
        End If
        If mAction.IGNORED Then
            If mAction.SHOW_WARNING Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("IGNORED => SHOW_WARNING = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If

            mAction.SHOW_WARNING = False
        End If

        ''  if RESTART and SHOW_WARNING are both set, RESTART is reset
        'If mAction.RESTART And mAction.SHOW_WARNING Then
        '    mAction.RESTART = False
        'End If
        ''  if RESTART and REBOOT are both set, RESTART is reset
        'If mAction.RESTART And mAction.REBOOT Then
        '    mAction.RESTART = False
        'End If
        ''  if REBOOT and SHOW_WARNING are both set, SHOW_WARNING is reset
        'If mAction.REBOOT And mAction.SHOW_WARNING Then
        '    mAction.SHOW_WARNING = False
        'End If
    End Sub

    Public Shared Function DetermineAction() As Boolean
        Try
            Dim iLastActivity As Integer, iNow As Integer
            Dim iLastCancelDate As Integer
            Dim sSkPath As String

            ResetAction()

            mCurrentState = LOBBYPC_STATE.NOT_SET
            mPreviousState = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__LastState)


            'Collect the info

            'check if SiteKiosk is installed
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if SiteKiosk is installed", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            sSkPath = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__SiteKiosk, LPCConstants.RegistryKeys.VALUE__InstallDir) & "\" & LPCConstants.FilesAndFolders.FILE__SiteKiosk
            If Not IO.File.Exists(sSkPath) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.SITEKIOSK_NOT_INSTALLED
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if Explorer is running
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if Explorer is running", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If _IsProcessRunning(LPCConstants.FilesAndFolders.FILE__Explorer) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.EXPLORER_RUNNING
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if SiteKiosk is running
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if SiteKiosk is running", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not _IsProcessRunning(LPCConstants.FilesAndFolders.FILE__SiteKiosk) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if secure user is logged in
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if secure user '" & mUsernameToCheck & "' is logged in", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not _IsUserLoggedIn() Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if trigger is running
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if trigger is running", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not _IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.TRIGGER_NOT_RUNNING
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if warning is running
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if warning is running", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If _IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'Check if admin mode is active, or if it´s active for too long
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if pc is in admin mode", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelled) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)

                LPCGlobals.Objects.Logger.WriteToLogRelative("check if admin mode is active for too long", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)

                iLastCancelDate = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelledTime)
                iNow = Helpers.TimeDate.ToUnix()

                If (iNow - iLastCancelDate) > LPCConstants.TimeoutsAndDelaysAndCounters.TIMEOUT__AdminMode Then
                    LPCGlobals.Objects.Logger.WriteToLogRelative("yep, cancelling admin mode", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE_CANCELLED
                End If
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'get last activity
            LPCGlobals.Objects.Logger.WriteToLogRelative("get last activity", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)

            iLastActivity = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_LastActivity, LPCConstants.RegistryKeys.VALUE__LastActivity)
            iNow = Helpers.TimeDate.ToUnix()
            mAction.LAST_ACTIVITY = iLastActivity


            'are we in logon screen?
            If Not mCurrentState And LOBBYPC_STATE.EXPLORER_RUNNING Then
                If mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING Then
                    If mCurrentState And LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER Then
                        LPCGlobals.Objects.Logger.WriteToLogRelative("it seems we are in Windows logon screen", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                        mCurrentState = mCurrentState Or LOBBYPC_STATE.WINDOWS_LOGON_SCREEN
                    End If
                End If
            End If


            'ok, sort out the action
            LPCGlobals.Objects.Logger.WriteToLogRelative("set appropriate action", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_INSTALLED Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("SITEKIOSK_NOT_INSTALLED => STOP_SERVICE", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                mAction.STOP_SERVICE = True
            Else
                If mCurrentState And LOBBYPC_STATE.WINDOWS_LOGON_SCREEN Then
                    LPCGlobals.Objects.Logger.WriteToLogRelative("WINDOWS_LOGON_SCREEN => RESTART", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    mAction.RESTART = True
                Else
                    If mCurrentState And LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER Then
                        If Not mCurrentState And LOBBYPC_STATE.EXPLORER_RUNNING Then
                            LPCGlobals.Objects.Logger.WriteToLogRelative("NOT_RUNNING_AS_SECURE_USER & !EXPLORER_RUNNING => RESTART", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.RESTART = True
                        Else
                            LPCGlobals.Objects.Logger.WriteToLogRelative("check for mouse/keyboard activity", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            iLastActivity = mAction.LAST_ACTIVITY
                            iNow = Helpers.TimeDate.ToUnix()

                            LPCGlobals.Objects.Logger.WriteToLogRelative(iNow.ToString & " - " & iLastActivity.ToString & " > " & LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__Warning.ToString & " ?", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 3)
                            If (iNow - iLastActivity) > LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__Warning Then
                                'Pc has been inactive for too long
                                LPCGlobals.Objects.Logger.WriteToLogRelative("inactive for too long", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 4)

                                If mCurrentState And LOBBYPC_STATE.ADMIN_MODE Then
                                    mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE_CANCELLED

                                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelled, False)
                                    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelledTime, 0)
                                End If

                                LPCGlobals.Objects.Logger.WriteToLogRelative("NOT_RUNNING_AS_SECURE_USER & EXPLORER_RUNNING => SHOW_WARNING", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 5)
                                mAction.SHOW_WARNING = True

                                If Not mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING Then
                                    LPCGlobals.Objects.Logger.WriteToLogRelative("SHOW_WARNING & !SITEKIOSK_NOT_RUNNING => WARNING_LOOSE", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 6)
                                    mAction.WARNING_LOOSE = True
                                End If
                            Else
                                LPCGlobals.Objects.Logger.WriteToLogRelative("NOT_RUNNING_AS_SECURE_USER & EXPLORER_RUNNING & ACTIVITY => IGNORED", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 3)
                                mAction.IGNORED = True
                            End If
                        End If
                    Else
                        If mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING Then
                            LPCGlobals.Objects.Logger.WriteToLogRelative("!NOT_RUNNING_AS_SECURE_USER & SITEKIOSK_NOT_RUNNING => SHOW_WARNING", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.SHOW_WARNING = True
                        Else
                            LPCGlobals.Objects.Logger.WriteToLogRelative("!NOT_RUNNING_AS_SECURE_USER & !SITEKIOSK_NOT_RUNNING => OK", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.OK = True
                        End If
                    End If

                    If mAction.SHOW_WARNING Then
                        If mCurrentState And LOBBYPC_STATE.TRIGGER_NOT_RUNNING Then
                            LPCGlobals.Objects.Logger.WriteToLogRelative("SHOW_WARNING & TRIGGER_NOT_RUNNING => RESTART", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.RESTART = True
                        End If
                        If mCurrentState And LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING Then
                            LPCGlobals.Objects.Logger.WriteToLogRelative("SHOW_WARNING & WATCHDOG_WARNING_RUNNING => SHOW_WARNING = false", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.SHOW_WARNING = False
                        End If
                    End If
                End If
                If mCurrentState And LOBBYPC_STATE.ADMIN_MODE Then
                    LPCGlobals.Objects.Logger.WriteToLogRelative("admin mode is active", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                    If mCurrentState And LOBBYPC_STATE.ADMIN_MODE_CANCELLED Then
                        LPCGlobals.Objects.Logger.WriteToLogRelative("never mind, it's cancelled", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    Else
                        If mAction.SHOW_WARNING Then
                            LPCGlobals.Objects.Logger.WriteToLogRelative("SHOW_WARNING => False", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.SHOW_WARNING = False
                        End If
                        If mAction.REBOOT Then
                            LPCGlobals.Objects.Logger.WriteToLogRelative("REBOOT => False", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.REBOOT = False
                        End If
                        If mAction.RESTART Then
                            LPCGlobals.Objects.Logger.WriteToLogRelative("RESTART => False", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                            mAction.RESTART = False
                        End If

                        mAction.IGNORED = True
                    End If
                End If
            End If














        Catch ex As Exception

        End Try
    End Function

    Public Shared Function _DetermineAction() As Boolean
        Try
            Dim iLastActivity As Integer, iNow As Integer
            Dim iLastCancelDate As Integer
            Dim sSkPath As String


            ResetAction()

            mCurrentState = LOBBYPC_STATE.NOT_SET
            mPreviousState = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__LastState)


            'check if SiteKiosk is installed
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if SiteKiosk is installed", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            sSkPath = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__SiteKiosk, LPCConstants.RegistryKeys.VALUE__InstallDir) & "\" & LPCConstants.FilesAndFolders.FILE__SiteKiosk
            If Not IO.File.Exists(sSkPath) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.SITEKIOSK_NOT_INSTALLED
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if Explorer is running
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if Explorer is running", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If _IsProcessRunning(LPCConstants.FilesAndFolders.FILE__Explorer) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.EXPLORER_RUNNING
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if SiteKiosk is running
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if SiteKiosk is running", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not _IsProcessRunning(LPCConstants.FilesAndFolders.FILE__SiteKiosk) Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'check if secure user is logged in
            LPCGlobals.Objects.Logger.WriteToLogRelative("check if secure user '" & mUsernameToCheck & "' is logged in", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If Not _IsUserLoggedIn() Then
                mCurrentState = mCurrentState Or LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER
                LPCGlobals.Objects.Logger.WriteToLogRelative("nope", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLogRelative("yep", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
            End If


            'are we perhaps in logon screen?
            If Not mCurrentState And LOBBYPC_STATE.EXPLORER_RUNNING Then
                If mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING Then
                    If mCurrentState And LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER Then
                        LPCGlobals.Objects.Logger.WriteToLogRelative("it seems we are in Windows logon screen", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                        mCurrentState = mCurrentState Or LOBBYPC_STATE.WINDOWS_LOGON_SCREEN
                    End If
                End If
            End If


            'set appropriate action
            LPCGlobals.Objects.Logger.WriteToLogRelative("set appropriate action", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            If mCurrentState And LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("RESTART = True", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                mAction.RESTART = True

                If (Not (mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING)) Or (mCurrentState And LOBBYPC_STATE.EXPLORER_RUNNING) Then
                    LPCGlobals.Objects.Logger.WriteToLogRelative("SHOW_WARNING = True", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    mAction.SHOW_WARNING = True

                    If (Not (mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING)) Then
                        LPCGlobals.Objects.Logger.WriteToLogRelative("WARNING_LOOSE = True", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                        mAction.WARNING_LOOSE = True
                    End If
                End If
            Else
                If (mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING) Then
                    LPCGlobals.Objects.Logger.WriteToLogRelative("SHOW_WARNING = True", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    mAction.SHOW_WARNING = True
                End If
            End If


            'Activity check
            If mCurrentState And LOBBYPC_STATE.NOT_RUNNING_AS_SECURE_USER Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("Pc is not secure, but might be running in admin mode", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                'Pc is not secure, but might be running in admin mode
                If mCurrentState And LOBBYPC_STATE.EXPLORER_RUNNING Then
                    'explorer is running, let's check for activity
                    LPCGlobals.Objects.Logger.WriteToLogRelative("explorer is running, let's check for activity", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    iLastActivity = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_LastActivity, LPCConstants.RegistryKeys.VALUE__LastActivity)
                    iNow = Helpers.TimeDate.ToUnix()

                    mAction.LAST_ACTIVITY = iLastActivity

                    LPCGlobals.Objects.Logger.WriteToLogRelative(iNow.ToString & " - " & iLastActivity.ToString & " > " & LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__Warning.ToString & " ?", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 3)
                    If (iNow - iLastActivity) > LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__Warning Then
                        'Pc has been inactive for too long
                        LPCGlobals.Objects.Logger.WriteToLogRelative("Pc has been inactive for too long", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 4)
                        mCurrentState = mCurrentState Or LOBBYPC_STATE.NO_ACTIVITY

                        If Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelled) Then
                            mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE_CANCELLED

                            Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelled, False)
                            Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelledTime, 0)
                        End If

                        LPCGlobals.Objects.Logger.WriteToLogRelative("SHOW_WARNING = True", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 4)
                        mAction.SHOW_WARNING = True
                    Else
                        LPCGlobals.Objects.Logger.WriteToLogRelative("Pc has been active", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 4)
                        LPCGlobals.Objects.Logger.WriteToLogRelative("IGNORED = True", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 4)
                        mAction.IGNORED = True
                    End If
                End If
            End If


            'Check if admin mode is active for too long
            If Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelled) Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("Check if admin mode is active for too long", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)

                iLastCancelDate = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelledTime)
                iNow = Helpers.TimeDate.ToUnix()

                If (iNow - iLastCancelDate) > LPCConstants.TimeoutsAndDelaysAndCounters.TIMEOUT__AdminMode Then
                    mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE_CANCELLED
                    LPCGlobals.Objects.Logger.WriteToLogRelative("yep, cancelling admin mode", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)

                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelled, False)
                    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelledTime, 0)
                End If
            End If


            'Check if admin mode is active 
            If Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Alert, LPCConstants.RegistryKeys.VALUE__AlertCancelled) Then
                'Pc is in admin mode
                LPCGlobals.Objects.Logger.WriteToLogRelative("Pc is in admin mode", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                mCurrentState = mCurrentState Or LOBBYPC_STATE.ADMIN_MODE

                LPCGlobals.Objects.Logger.WriteToLogRelative("cancelling actions", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                mAction.RESTART = False
                mAction.REBOOT = False
                mAction.SHOW_WARNING = False

                If Not mCurrentState And LOBBYPC_STATE.EXPLORER_RUNNING Then
                    If mCurrentState And LOBBYPC_STATE.SITEKIOSK_NOT_RUNNING Then
                        'PC is sitting in windows login screen, in admin mode. Bad admin user, you forgot to reboot.
                        LPCGlobals.Objects.Logger.WriteToLogRelative("PC is sitting in windows login screen, in admin mode. Bad admin user, you probably forgot to reboot.", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 3)
                        mAction.RESTART = True
                        LPCGlobals.Objects.Logger.WriteToLogRelative("RESTART = True", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 3)
                    End If
                End If
            Else
                If mAction.IGNORED Then
                    LPCGlobals.Objects.Logger.WriteToLogRelative("IGNORED = False", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 4)
                    mAction.IGNORED = False
                End If
            End If


            'check if Warning is already running
            If _IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogWarning) Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("Warning is already running, reset all actions", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)

                'Reset all actions, warning popup is already taking care of things
                mCurrentState = mCurrentState Or LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING
                mAction.RESTART = False
                mAction.SHOW_WARNING = False
                mAction.REBOOT = False
            End If


            'Show summary if debugging is set
            If LPCGlobals.Settings.Debug Then
                LPCGlobals.Objects.Logger.WriteToLogRelative("LobbyPC state summary", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                LPCGlobals.Objects.Logger.WriteToLogRelative("state score: " & mCurrentState, MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)

                Dim aAllStateValues() As LPCState.LOBBYPC_STATE
                aAllStateValues = [Enum].GetValues(GetType(LPCState.LOBBYPC_STATE)).Cast(Of LPCState.LOBBYPC_STATE)()

                For iStateValueIndex As Integer = 0 To aAllStateValues.Count - 1
                    If mCurrentState And aAllStateValues(iStateValueIndex) Then
                        LPCGlobals.Objects.Logger.WriteToLogRelative(aAllStateValues(iStateValueIndex).ToString, MyLogger.MESSAGE_TYPE.LOG_DEBUG, 3)
                    End If
                Next
            End If


            mAction.STATE = mCurrentState


            If mAction.SHOW_WARNING Then
                'Check if the process that starts the warning is running, if not, just reboot
                LPCGlobals.Objects.Logger.WriteToLogRelative("Check if the process that starts the warning is running, if not, just reboot", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                If Not _IsProcessRunning(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger) Then
                    mAction.SHOW_WARNING = False
                    mAction.RESTART = True

                    LPCGlobals.Objects.Logger.WriteToLogRelative("SHOW_WARNING = False", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    LPCGlobals.Objects.Logger.WriteToLogRelative("RESTART = True", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                Else
                    LPCGlobals.Objects.Logger.WriteToLogRelative("ok", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                End If
            End If


            'Action correction
            ActionCorrection()

            Return True
        Catch ex As Exception
            'Something went very wrong!!!
            LPCGlobals.Objects.Logger.WriteToLog("Exception in LPCState.GetAction", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)
            LPCGlobals.Objects.Logger.WriteToLog(ex.Message, MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
            LPCGlobals.Objects.Logger.WriteToLog(ex.StackTrace, MyLogger.MESSAGE_TYPE.LOG_ERROR, 2)

            ResetAction()
        End Try


        Return False
    End Function


    Private Shared ReadOnly c_CompletelyArbitraryProcessCountThreshold As Integer = 2



    Private Shared Function _IsUserLoggedIn() As Boolean
        Return _IsUserLoggedIn(mUsernameToCheck)
    End Function

    Private Shared Function _IsUserLoggedIn(UserName As String) As Boolean
        Dim iCount As Integer

        iCount = _CountProcessesOwnedByUser(UserName)

        If iCount >= c_CompletelyArbitraryProcessCountThreshold Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Shared Function _IsProcessRunning(sProcessName As String) As Boolean
        Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String

        sSearchProcessName__ToLower = sProcessName.ToLower

        If sProcessName.EndsWith(".exe") Then
            sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
        End If

        For Each p As Process In Process.GetProcesses
            sProcessName__ToLower = p.ProcessName.ToLower
            If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                bRet = True
                Exit For
            End If
        Next

        Return bRet
    End Function

    Private Shared Function _CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
        Dim wiName As String, lCount As Long

        lCount = 0

        For Each p As Process In Process.GetProcesses
            Try
                Dim hToken As IntPtr
                If OpenProcessToken(p.Handle, Security.Principal.TokenAccessLevels.Query, hToken) Then
                    Using wi As New Security.Principal.WindowsIdentity(hToken)
                        wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                        If wiName.ToLower = sUserNameToCheck.ToLower Then
                            lCount += 1
                        End If
                    End Using
                    CloseHandle(hToken)
                End If
            Catch ex As Exception

            End Try
        Next

        Return lCount
    End Function

End Class
