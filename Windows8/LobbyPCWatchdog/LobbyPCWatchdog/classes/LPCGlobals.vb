﻿Public Class LPCGlobals
    Public Class Service
        Public Shared LoopAbort As Boolean = False
    End Class

    Public Class Settings
        Public Class Integrity
            Private Shared mCheckFileAccess As Boolean = False
            Public Shared Property CheckFileAccess As Boolean
                Get
                    Return mCheckFileAccess
                End Get
                Set(value As Boolean)
                    mCheckFileAccess = value
                End Set
            End Property

            Private Shared mCheckEmergencyConfig As Boolean = False
            Public Shared Property CheckEmergencyConfig As Boolean
                Get
                    Return mCheckEmergencyConfig
                End Get
                Set(value As Boolean)
                    mCheckEmergencyConfig = value
                End Set
            End Property

            Private Shared mCheckThemeSettings As Boolean = False
            Public Shared Property CheckThemeSettings As Boolean
                Get
                    Return mCheckThemeSettings
                End Get
                Set(value As Boolean)
                    mCheckThemeSettings = value
                End Set
            End Property

            Private Shared mCheckSiteKioskProfileBroken As Boolean = False
            Public Shared Property CheckSiteKioskProfileBroken As Boolean
                Get
                    Return mCheckSiteKioskProfileBroken
                End Get
                Set(value As Boolean)
                    mCheckSiteKioskProfileBroken = value
                End Set
            End Property

            Private Shared mCheckSiteKioskProfilePath As Boolean = False
            Public Shared Property CheckSiteKioskProfilePath As Boolean
                Get
                    Return mCheckSiteKioskProfilePath
                End Get
                Set(value As Boolean)
                    mCheckSiteKioskProfilePath = value
                End Set
            End Property

            Private Shared mDefaultSkin As String = ""
            Public Shared Property DefaultSkin As String
                Get
                    Return mDefaultSkin
                End Get
                Set(value As String)
                    mDefaultSkin = value
                End Set
            End Property

            Private Shared mEmergencyConfig As String = ""
            Public Shared Property EmergencyConfig As String
                Get
                    Return mEmergencyConfig
                End Get
                Set(value As String)
                    mEmergencyConfig = value
                End Set
            End Property

            Private Shared mEmergencyConfigCount As Integer = 0
            Public Shared Property EmergencyConfigCount As Integer
                Get
                    Return mEmergencyConfigCount
                End Get
                Set(value As Integer)
                    mEmergencyConfigCount = value
                End Set
            End Property
        End Class

        Private Shared mDebug As Boolean = False
        Public Shared Property Debug As Boolean
            Get
                Return mDebug
            End Get
            Set(value As Boolean)
                mDebug = value
            End Set
        End Property

        Private Shared mTestMode As Boolean = False
        Public Shared Property TestMode As Boolean
            Get
                Return mTestMode
            End Get
            Set(value As Boolean)
                mTestMode = value
            End Set
        End Property

        Private Shared mDisabled As Boolean
        Public Shared Property Disabled() As Boolean
            Get
                Return mDisabled
            End Get
            Set(ByVal value As Boolean)
                mDisabled = value
            End Set
        End Property

        Private Shared mMuzzled As Boolean
        Public Shared Property Muzzled() As Boolean
            Get
                Return mMuzzled
            End Get
            Set(ByVal value As Boolean)
                mMuzzled = value
            End Set
        End Property

        Private Shared mCheckFoRemoteConnection As Boolean
        Public Shared Property CheckFoRemoteConnection() As Boolean
            Get
                Return mCheckFoRemoteConnection
            End Get
            Set(ByVal value As Boolean)
                mCheckFoRemoteConnection = value
            End Set
        End Property

        Private Shared mDisabledUntilFirstStart As Boolean
        Public Shared Property DisabledUntilFirstStart() As Boolean
            Get
                Return mDisabledUntilFirstStart
            End Get
            Set(ByVal value As Boolean)
                mDisabledUntilFirstStart = value
            End Set
        End Property

        Private Shared mSkipFirstRunDelay As Boolean
        Public Shared Property SkipFirstRunDelay() As Boolean
            Get
                Return mSkipFirstRunDelay
            End Get
            Set(ByVal value As Boolean)
                mSkipFirstRunDelay = value
            End Set
        End Property

        Private Shared mPlayStupidSound As Boolean
        Public Shared Property PlayStupidSound() As Boolean
            Get
                Return mPlayStupidSound
            End Get
            Set(ByVal value As Boolean)
                mPlayStupidSound = value
            End Set
        End Property

        Private Shared mLogMeInThreshold As Integer
        Public Shared Property LogMeInThreshold() As Integer
            Get
                Return mLogMeInThreshold
            End Get
            Set(ByVal value As Integer)
                mLogMeInThreshold = value
            End Set
        End Property

        Private Shared mTakeControlThreshold As Integer
        Public Shared Property TakeControlThreshold() As Integer
            Get
                Return mTakeControlThreshold
            End Get
            Set(ByVal value As Integer)
                mTakeControlThreshold = value
            End Set
        End Property

        Private Shared mZipOldLogs As Boolean
        Public Shared Property ZipOldLogs() As Boolean
            Get
                Return mZipOldLogs
            End Get
            Set(ByVal value As Boolean)
                mZipOldLogs = value
            End Set
        End Property

        Private Shared mZipOldLogsThreshold As Integer
        Public Shared Property ZipOldLogsThreshold() As Integer
            Get
                Return mZipOldLogsThreshold
            End Get
            Set(ByVal value As Integer)
                mZipOldLogsThreshold = value
            End Set
        End Property

        Private Shared mConsecutiveRebootsMax As Integer
        Public Shared Property ConsecutiveRebootsMax() As Integer
            Get
                Return mConsecutiveRebootsMax
            End Get
            Set(ByVal value As Integer)
                mConsecutiveRebootsMax = value
            End Set
        End Property

        Private Shared mZipOldLogsForAllGuestTekApps As Boolean
        Public Shared Property ZipOldLogsForAllGuestTekApps() As Boolean
            Get
                Return mZipOldLogsForAllGuestTekApps
            End Get
            Set(ByVal value As Boolean)
                mZipOldLogsForAllGuestTekApps = value
            End Set
        End Property

        Private Shared mCheckIntegrity As Boolean
        Public Shared Property CheckIntegrity() As Boolean
            Get
                Return mCheckIntegrity
            End Get
            Set(ByVal value As Boolean)
                mCheckIntegrity = value
            End Set
        End Property

        Private Shared mVersionInventory As Boolean
        Public Shared Property VersionInventory() As Boolean
            Get
                Return mVersionInventory
            End Get
            Set(ByVal value As Boolean)
                mVersionInventory = value
            End Set
        End Property

        Private Shared mEmergencyDeploy_Enabled As Boolean
        Public Shared Property EmergencyDeploy_Enabled() As Boolean
            Get
                Return mEmergencyDeploy_Enabled
            End Get
            Set(ByVal value As Boolean)
                mEmergencyDeploy_Enabled = value
            End Set
        End Property

        Private Shared mEmergencyDeploy_Debug As Boolean
        Public Shared Property EmergencyDeploy_Debug() As Boolean
            Get
                Return mEmergencyDeploy_Debug
            End Get
            Set(ByVal value As Boolean)
                mEmergencyDeploy_Debug = value
            End Set
        End Property

        Private Shared mEmergencyDeploy_Url As String
        Public Shared Property EmergencyDeploy_Url() As String
            Get
                Return mEmergencyDeploy_Url
            End Get
            Set(ByVal value As String)
                mEmergencyDeploy_Url = value
            End Set
        End Property

        Private Shared mEnableWatchdogWatchdog As Boolean
        Public Shared Property EnableWatchdogWatchdog() As Boolean
            Get
                Return mEnableWatchdogWatchdog
            End Get
            Set(ByVal value As Boolean)
                mEnableWatchdogWatchdog = value
            End Set
        End Property

        Public Shared Sub Load()
            mDebug = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                       LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDebug,
                                                       LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsDebug)

            mTestMode = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                          LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTestMode,
                                                          LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsTestMode)

            mCheckFoRemoteConnection = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                           LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckFoRemoteConnection,
                                                                           LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsCheckFoRemoteConnection)

            mSkipFirstRunDelay = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                   LPCConstants.RegistryKeys.VALUE__WatchdogSettingsSkipFirstRunDelay,
                                                                   LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsSkipFirstRunDelay)

            mPlayStupidSound = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                 LPCConstants.RegistryKeys.VALUE__WatchdogSettingsPlaysound,
                                                                 LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsPlaysound)

            mLogMeInThreshold = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                  LPCConstants.RegistryKeys.VALUE__WatchdogSettingsLogMeInThreshold,
                                                                  LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsLogMeInThreshold)

            mLogMeInThreshold = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                  LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTakeControlThreshold,
                                                                  LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsTakeControlThreshold)

            mZipOldLogs = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                            LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogs,
                                                            LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsZipOldLogs)

            mZipOldLogsThreshold = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                            LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogsThreshold,
                                                            LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsZipOldLogsThreshold)

            mZipOldLogsForAllGuestTekApps = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                            LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogsForAllGuestTekApps,
                                                            LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsZipOldLogsForAllGuestTekApps)

            mCheckIntegrity = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckIntegrity,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsCheckIntegrity)

            mVersionInventory = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettingsVersionInventory,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsVersionInventory)

            mEnableWatchdogWatchdog = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettingsEnableWatchdogWatchdog,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsEnableWatchdogWatchdog)

            Integrity.CheckEmergencyConfig = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckEmergencyConfig,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckEmergencyConfig)

            Integrity.CheckFileAccess = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckFileAccess,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckFileAccess)

            Integrity.CheckThemeSettings = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckThemeSettings,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckThemeSettings)

            Integrity.CheckSiteKioskProfileBroken = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckSiteKioskProfileBroken,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckSiteKioskProfileBroken)

            Integrity.CheckSiteKioskProfilePath = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_CheckSiteKioskProfilePath,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_CheckSiteKioskProfilePath)

            Integrity.DefaultSkin = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_DefaultSkin,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_DefaultSkin)

            Integrity.EmergencyConfig = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__WatchdogSettings_Integrity,
                                                                LPCConstants.RegistryKeys.VALUE__WatchdogSettings_Integrity_EmergencyConfig,
                                                                LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettings_Integrity_EmergencyConfig)


            Dim sDisabled As String = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Disabled,
                                                                  LPCConstants.RegistryKeys.VALUE__WatchdogDisabled,
                                                                  "")
            sDisabled = Helpers.XOrObfuscation.Deobfuscate(sDisabled)
            If sDisabled = LPCConstants.Misc.MagicalDisableString Then
                mDisabled = True
            Else
                mDisabled = False
            End If


            Dim sMuzzled As String = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                                  LPCConstants.RegistryKeys.VALUE__WatchdogSettingsMuzzled,
                                                                  "")
            sMuzzled = Helpers.XOrObfuscation.Deobfuscate(sMuzzled)
            If sMuzzled = LPCConstants.Misc.MagicalDisableString Then
                mMuzzled = True
            Else
                mMuzzled = False
            End If


            mConsecutiveRebootsMax = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
                                                            LPCConstants.RegistryKeys.VALUE__WatchdogSettingsConsecutiveRebootsMax,
                                                            LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsConsecutiveRebootsMax)


            mDisabledUntilFirstStart = Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog,
                                                                         LPCConstants.RegistryKeys.VALUE__DisabledUntilOVCCStart,
                                                                         LPCConstants.RegistryKeys.DEFAULTVALUE__DisabledUntilOVCCStart)
        End Sub

        'Public Shared Sub Save()
        '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDebug,
        '                                      mDebug)
        '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTestMode,
        '                                      mTestMode)
        '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckForLogMeInConnection,
        '                                      mCheckForLogMeInConnection)
        '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsSkipFirstRunDelay,
        '                                      mSkipFirstRunDelay)
        '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsPlaysound,
        '                                      mPlayStupidSound)
        '    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsLogMeInThreshold,
        '                                      mLogMeInThreshold)
        '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogs,
        '                                      mZipOldLogs)
        '    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogsThreshold,
        '                                      mZipOldLogsThreshold)
        '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsZipOldLogsForAllGuestTekApps,
        '                                      mZipOldLogsForAllGuestTekApps)
        '    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                      LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckIntegrity,
        '                                      mCheckIntegrity)

        '    If mDisabled Then
        '        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Disabled,
        '                                         LPCConstants.RegistryKeys.VALUE__WatchdogDisabled,
        '                                         Helpers.XOrObfuscation.Obfuscate(LPCConstants.Misc.MagicalDisableString))
        '    Else
        '        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Disabled,
        '                                         LPCConstants.RegistryKeys.VALUE__WatchdogDisabled,
        '                                         "")
        '    End If


        '    If mMuzzled Then
        '        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                         LPCConstants.RegistryKeys.VALUE__WatchdogSettingsMuzzled,
        '                                         Helpers.XOrObfuscation.Obfuscate(LPCConstants.Misc.MagicalDisableString))
        '    Else
        '        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings,
        '                                         LPCConstants.RegistryKeys.VALUE__WatchdogSettingsMuzzled,
        '                                         "")
        '    End If
        'End Sub
    End Class

    Public Class Counters
        Private Shared mWarningPopupTriggerCount As Integer = 0
        Public Shared Property WarningPopupTrigger As Integer
            Set(value As Integer)
                mWarningPopupTriggerCount = value
            End Set
            Get
                Return mWarningPopupTriggerCount
            End Get
        End Property
    End Class
End Class
