﻿Imports System.Security.AccessControl
Imports System.Security.Principal
Imports Microsoft.Win32

Public Class RegistryWriteCheck
    Public Shared Function CanIWriteToOpenToAllRegistrySection(ByRef sError As String) As Boolean
        sError = ""

        Try
            Dim sTmp As String = "", sTmp_Check As String = "", sTmp_Hash As String

            'Get current value of WriteCheck key
            sTmp = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll, LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheck)

            'hash it.
            sTmp_Hash = Helpers.Crypto.SHA512.GetHash(sTmp)

            'Write value back to WriteCheck key
            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll, LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheck, sTmp_Hash)

            'Write timestamp to WriteCheck key
            Dim dDate As DateTime = DateTime.Now
            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll, LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheckTimestamp, dDate.ToString("O"))

            'Get new value of WriteCheck key
            sTmp_Check = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll, LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheck)

            'Values should be not equal
            If Not sTmp.Equals(sTmp_Check) Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            sError = ex.Message

            Return False
        End Try
    End Function

    Public Shared Function AllowAllForEveryone(key As String) As String
        Dim sRet As String = ""

        Try
            Dim sid As SecurityIdentifier = New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
            Dim account As NTAccount = TryCast(sid.Translate(GetType(NTAccount)), NTAccount)

            If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                key = key.Replace("HKEY_LOCAL_MACHINE\", "")
            End If

            Using rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, RegistryKeyPermissionCheck.ReadWriteSubTree)
                If rk Is Nothing Then
                    Throw New InvalidOperationException("key '" & key & "' does not exist in HKEY_LOCAL_MACHINE")
                End If

                Dim rs As RegistrySecurity = rk.GetAccessControl

                rs.SetAccessRuleProtection(True, False)


                Dim rules As AuthorizationRuleCollection = rs.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
                For Each rule As RegistryAccessRule In rules
                    rs.RemoveAccessRule(rule)
                Next

                rs.AddAccessRule(New RegistryAccessRule(account.ToString, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow))
                rs.AddAccessRule(New RegistryAccessRule(account.ToString, FileSystemRights.FullControl, InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow))

                rk.SetAccessControl(rs)
            End Using

            sRet = "ok"
        Catch ex As Exception
            sRet = ex.Message
        End Try

        Return sRet
    End Function

    Public Shared Function CanonicalizeDacl(key As String) As String
        Dim sRet As String = ""

        Try
            Using rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, RegistryKeyPermissionCheck.ReadWriteSubTree)
                If rk Is Nothing Then
                    Throw New InvalidOperationException("key '" & key & "' does not exist in HKEY_LOCAL_MACHINE")
                End If

                Dim rs As RegistrySecurity = rk.GetAccessControl

                sRet = _CanonicalizeDacl(rs)

                rk.SetAccessControl(rs)
            End Using
        Catch ex As Exception
            sRet = ex.Message
        End Try

        Return sRet
    End Function

    Private Shared Function _CanonicalizeDacl(objectSecurity As NativeObjectSecurity) As String
        Try
            If objectSecurity Is Nothing Then
                Throw New ArgumentNullException("objectSecurity is null")
            End If
            If objectSecurity.AreAccessRulesCanonical Then
                Throw New Exception("Rules are already canonical")
            End If

            ' A canonical ACL must have ACES sorted according to the following order:
            '   1. Access-denied on the object
            '   2. Access-denied on a child or property
            '   3. Access-allowed on the object
            '   4. Access-allowed on a child or property
            '   5. All inherited ACEs 
            Dim descriptor As New RawSecurityDescriptor(objectSecurity.GetSecurityDescriptorSddlForm(AccessControlSections.Access))

            Dim implicitDenyDacl As New List(Of CommonAce)()
            Dim implicitDenyObjectDacl As New List(Of CommonAce)()
            Dim inheritedDacl As New List(Of CommonAce)()
            Dim implicitAllowDacl As New List(Of CommonAce)()
            Dim implicitAllowObjectDacl As New List(Of CommonAce)()

            For Each ace As CommonAce In descriptor.DiscretionaryAcl
                If (ace.AceFlags And AceFlags.Inherited) = AceFlags.Inherited Then
                    inheritedDacl.Add(ace)
                Else
                    Select Case ace.AceType
                        Case AceType.AccessAllowed
                            implicitAllowDacl.Add(ace)
                            Exit Select

                        Case AceType.AccessDenied
                            implicitDenyDacl.Add(ace)
                            Exit Select

                        Case AceType.AccessAllowedObject
                            implicitAllowObjectDacl.Add(ace)
                            Exit Select

                        Case AceType.AccessDeniedObject
                            implicitDenyObjectDacl.Add(ace)
                            Exit Select
                    End Select
                End If
            Next

            Dim aceIndex As Int32 = 0
            Dim newDacl As New RawAcl(descriptor.DiscretionaryAcl.Revision, descriptor.DiscretionaryAcl.Count)

            For Each tmp As CommonAce In implicitDenyDacl
                'newDacl.InsertAce(System.Math.Max(System.Threading.Interlocked.Increment(aceIndex), aceIndex - 1), tmp)
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next

            For Each tmp As CommonAce In implicitDenyObjectDacl
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next

            For Each tmp As CommonAce In implicitAllowDacl
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next

            For Each tmp As CommonAce In implicitAllowObjectDacl
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next

            For Each tmp As CommonAce In inheritedDacl
                newDacl.InsertAce(aceIndex, tmp)
                aceIndex += 1
            Next


            If aceIndex <> descriptor.DiscretionaryAcl.Count Then
                Throw New Exception("The DACL cannot be canonicalized since it would potentially result in a loss of information")
            End If

            descriptor.DiscretionaryAcl = newDacl
            objectSecurity.SetSecurityDescriptorSddlForm(descriptor.GetSddlForm(AccessControlSections.Access), AccessControlSections.Access)

            Return "ok"
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function



End Class
