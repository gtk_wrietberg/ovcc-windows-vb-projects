﻿Imports System.Diagnostics
Imports System.DirectoryServices
Imports System.DirectoryServices.AccountManagement
Imports System.Runtime.InteropServices
Imports System.Security.AccessControl
Imports System.Security.Principal
Imports Microsoft.Win32
Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Security

Public Class Helpers
#Region "Types"

    Public Class Types
        Public Shared Function CastToInteger_safe(ByVal o As Object) As Integer
            Dim result As Integer

            If TypeOf o Is Integer Then
                result = DirectCast(o, Integer)
            Else
                result = -1
            End If

            Return result
        End Function

        Public Shared Function CastToLong_safe(ByVal o As Object) As Long
            Dim result As Long

            If TypeOf o Is Long Then
                result = DirectCast(o, Long)
            Else
                result = -1
            End If

            Return result
        End Function


        Public Shared Function RandomString(Length As Integer, Optional CharsToChooseFrom As String = "") As String
            If CharsToChooseFrom = "" Then
                'CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{]},<.>/?"
                CharsToChooseFrom = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            End If
            If Length < 1 Then
                Length = 1
            End If

            Dim sTmp As String = "", iRnd As Integer

            Randomize()

            For i As Integer = 0 To Length - 1
                iRnd = Int(CharsToChooseFrom.Length * Rnd())
                sTmp &= Mid(CharsToChooseFrom, iRnd + 1, 1)
            Next

            Return sTmp
        End Function

        Public Shared Function DoubleQuoteString(str As String) As String
            Dim newStr As String = ""

            newStr = """" & str & """"

            Return newStr
        End Function
    End Class
#End Region

#Region "Registry"
    Public Class Registry
#Region "boolean"
        Public Shared Function GetValue_Boolean(keyName As String, valueName As String) As Boolean
            Return GetValue_Boolean(keyName, valueName, False)
        End Function

        Public Shared Function GetValue_Boolean(keyName As String, valueName As String, defaultValue As Boolean) As Boolean
            Dim oTmp As Object, sTmp As String

            Try
                If defaultValue Then
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "yes")
                Else
                    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, "no")
                End If

                sTmp = Convert.ToString(oTmp)
            Catch ex As Exception
                sTmp = ""
            End Try

            If sTmp = "yes" Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Sub SetValue_Boolean(keyName As String, valueName As String, value As Boolean)
            Try
                If value Then
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "yes", Microsoft.Win32.RegistryValueKind.String)
                Else
                    Microsoft.Win32.Registry.SetValue(keyName, valueName, "no", Microsoft.Win32.RegistryValueKind.String)
                End If
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "string"
        Public Shared Function GetValue_String(keyName As String, valueName As String) As String
            Return GetValue_String(keyName, valueName, "")
        End Function

        Public Shared Function GetValue_String(keyName As String, valueName As String, defaultValue As String) As String
            Dim oTmp As Object, sTmp As String

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            sTmp = Convert.ToString(oTmp)

            Return sTmp
        End Function

        Public Shared Sub SetValue_String(keyName As String, valueName As String, value As String)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.String)
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "long"
        'Public Shared Function GetValue_Long(keyName As String, valueName As String) As Long
        '    Return GetValue_Long(keyName, valueName, 0)
        'End Function

        'Public Shared Function GetValue_Long(keyName As String, valueName As String, defaultValue As Long) As Long
        '    Dim oTmp As Object, lTmp As Long

        '    oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
        '    lTmp = Helpers.Types.CastToLong_safe(oTmp)

        '    Return lTmp
        'End Function

        'Public Shared Sub SetValue_Long(keyName As String, valueName As String, value As Long)
        '    Try
        '        Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
        '    Catch ex As Exception

        '    End Try
        'End Sub
#End Region

#Region "integer"
        Public Shared Function GetValue_Integer(keyName As String, valueName As String) As Integer
            Return GetValue_Integer(keyName, valueName, 0)
        End Function

        Public Shared Function GetValue_Integer(keyName As String, valueName As String, defaultValue As Long) As Integer
            Dim oTmp As Object, iTmp As Integer

            oTmp = Microsoft.Win32.Registry.GetValue(keyName, valueName, defaultValue)
            iTmp = Helpers.Types.CastToInteger_safe(oTmp)

            Return iTmp
        End Function

        Public Shared Sub SetValue_Integer(keyName As String, valueName As String, value As Integer)
            Try
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "misc"
        Public Shared Function DeleteValue(key As String, valuename As String)
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key)

                If rk Is Nothing Then
                    Throw New Exception("not found")
                End If

                rk.DeleteValue(valuename, True)

                Return True
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Function CreateKey(key As String) As Boolean
            Dim bRet As Boolean = False

            Try
                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Dim rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(key)

                If Not rk Is Nothing Then
                    bRet = True
                End If
            Catch ex As Exception

            End Try

            Return bRet
        End Function

        Public Shared Function AllowAllForEveryone(key As String) As String
            Dim sRet As String = ""

            Try
                Dim sid As SecurityIdentifier = New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
                Dim account As NTAccount = TryCast(sid.Translate(GetType(NTAccount)), NTAccount)

                If key.StartsWith("HKEY_LOCAL_MACHINE\") Then
                    key = key.Replace("HKEY_LOCAL_MACHINE\", "")
                End If

                Using rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, RegistryKeyPermissionCheck.ReadWriteSubTree)
                    If rk Is Nothing Then
                        Throw New InvalidOperationException("key '" & key & "' does not exist in HKEY_LOCAL_MACHINE")
                    End If

                    Dim rs As RegistrySecurity = rk.GetAccessControl

                    rs.SetAccessRuleProtection(True, False)


                    Dim rules As AuthorizationRuleCollection = rs.GetAccessRules(True, True, GetType(System.Security.Principal.NTAccount))
                    For Each rule As RegistryAccessRule In rules
                        rs.RemoveAccessRule(rule)
                    Next

                    rs.AddAccessRule(New RegistryAccessRule(account.ToString, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow))
                    rs.AddAccessRule(New RegistryAccessRule(account.ToString, FileSystemRights.FullControl, InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow))

                    rk.SetAccessControl(rs)
                End Using

                sRet = "ok"
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Public Shared Function CanonicalizeDacl(key As String) As String
            Dim sRet As String = ""

            Try
                Using rk As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(key, RegistryKeyPermissionCheck.ReadWriteSubTree)
                    If rk Is Nothing Then
                        Throw New InvalidOperationException("key '" & key & "' does not exist in HKEY_LOCAL_MACHINE")
                    End If

                    Dim rs As RegistrySecurity = rk.GetAccessControl

                    sRet = _CanonicalizeDacl(rs)

                    rk.SetAccessControl(rs)
                End Using
            Catch ex As Exception
                sRet = ex.Message
            End Try

            Return sRet
        End Function

        Private Shared Function _CanonicalizeDacl(objectSecurity As NativeObjectSecurity) As String
            Try
                If objectSecurity Is Nothing Then
                    Throw New ArgumentNullException("objectSecurity is null")
                End If
                If objectSecurity.AreAccessRulesCanonical Then
                    Throw New Exception("Rules are already canonical")
                End If

                ' A canonical ACL must have ACES sorted according to the following order:
                '   1. Access-denied on the object
                '   2. Access-denied on a child or property
                '   3. Access-allowed on the object
                '   4. Access-allowed on a child or property
                '   5. All inherited ACEs 
                Dim descriptor As New RawSecurityDescriptor(objectSecurity.GetSecurityDescriptorSddlForm(AccessControlSections.Access))

                Dim implicitDenyDacl As New List(Of CommonAce)()
                Dim implicitDenyObjectDacl As New List(Of CommonAce)()
                Dim inheritedDacl As New List(Of CommonAce)()
                Dim implicitAllowDacl As New List(Of CommonAce)()
                Dim implicitAllowObjectDacl As New List(Of CommonAce)()

                For Each ace As CommonAce In descriptor.DiscretionaryAcl
                    If (ace.AceFlags And AceFlags.Inherited) = AceFlags.Inherited Then
                        inheritedDacl.Add(ace)
                    Else
                        Select Case ace.AceType
                            Case AceType.AccessAllowed
                                implicitAllowDacl.Add(ace)
                                Exit Select

                            Case AceType.AccessDenied
                                implicitDenyDacl.Add(ace)
                                Exit Select

                            Case AceType.AccessAllowedObject
                                implicitAllowObjectDacl.Add(ace)
                                Exit Select

                            Case AceType.AccessDeniedObject
                                implicitDenyObjectDacl.Add(ace)
                                Exit Select
                        End Select
                    End If
                Next

                Dim aceIndex As Int32 = 0
                Dim newDacl As New RawAcl(descriptor.DiscretionaryAcl.Revision, descriptor.DiscretionaryAcl.Count)

                For Each tmp As CommonAce In implicitDenyDacl
                    'newDacl.InsertAce(System.Math.Max(System.Threading.Interlocked.Increment(aceIndex), aceIndex - 1), tmp)
                    newDacl.InsertAce(aceIndex, tmp)
                    aceIndex += 1
                Next

                For Each tmp As CommonAce In implicitDenyObjectDacl
                    newDacl.InsertAce(aceIndex, tmp)
                    aceIndex += 1
                Next

                For Each tmp As CommonAce In implicitAllowDacl
                    newDacl.InsertAce(aceIndex, tmp)
                    aceIndex += 1
                Next

                For Each tmp As CommonAce In implicitAllowObjectDacl
                    newDacl.InsertAce(aceIndex, tmp)
                    aceIndex += 1
                Next

                For Each tmp As CommonAce In inheritedDacl
                    newDacl.InsertAce(aceIndex, tmp)
                    aceIndex += 1
                Next


                If aceIndex <> descriptor.DiscretionaryAcl.Count Then
                    Throw New Exception("The DACL cannot be canonicalized since it would potentially result in a loss of information")
                End If

                descriptor.DiscretionaryAcl = newDacl
                objectSecurity.SetSecurityDescriptorSddlForm(descriptor.GetSddlForm(AccessControlSections.Access), AccessControlSections.Access)

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

#End Region


        ''' <summary>
        ''' Checks if we can write to certain section of HKEY_LOCAL_MACHINE
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Function CanIWriteToOpenToAllRegistrySection(ByRef sError As String) As Boolean
            sError = ""

            Try
                Dim sTmp As String = "", sTmp_Check As String = "", sTmp_Hash As String

                'Get current value of WriteCheck key
                sTmp = GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll, LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheck)

                'hash it.
                sTmp_Hash = Crypto.SHA512.GetHash(sTmp)

                'Write value back to WriteCheck key
                SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll, LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheck, sTmp_Hash)

                'Get new value of WriteCheck key
                sTmp_Check = GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_OpenToAll, LPCConstants.RegistryKeys.VALUE__OpenToAll_WriteCheck)

                'Values should be not equal
                If Not sTmp.Equals(sTmp_Check) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                sError = ex.Message

                Return False
            End Try
        End Function
    End Class
#End Region

#Region "Error"
    Public Class Errors
        Public Enum ERROR_SEVERITY
            WARNING = 0
            NORMAL_ERROR = 1
            FATAL_ERROR = 2
        End Enum

        Public Shared Sub TriggerErrorPopup(ErrorTitle As String, ErrorDescription As String, ErrorSeverity As ERROR_SEVERITY, Optional RebootOption As Boolean = False)
            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Timestamp, Now.ToString)
            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Title, ErrorTitle)
            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Description, ErrorDescription)
            Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Severity, ErrorSeverity)
            Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_RebootOption, RebootOption)
        End Sub
    End Class
#End Region

#Region "Crypto"
    Public Class Crypto
        Public Class SHA512
            Private Shared ReadOnly _sha1 As Cryptography.SHA512 = Cryptography.SHA512.Create()

            Public Shared Function GetHash(source As String) As String
                Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
                Dim sb As New System.Text.StringBuilder()

                Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

                Return sb.ToString()
            End Function

            Public Shared Function VerifyHash(source As String, hash As String) As Boolean
                Dim sourceHash = GetHash(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function

            Public Shared Function GetHashBase64(source As String) As String
                Dim data = _sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

                Return Convert.ToBase64String(data)
            End Function

            Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
                Dim sourceHash = GetHashBase64(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function
        End Class

        Public Class MD5
            Private Shared ReadOnly _md5 As Cryptography.MD5 = Cryptography.MD5.Create()

            Public Shared Function GetHash(source As String) As String
                Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))
                Dim sb As New System.Text.StringBuilder()

                Array.ForEach(data, Function(x) sb.Append(x.ToString("X2")))

                Return sb.ToString()
            End Function

            Public Shared Function VerifyHash(source As String, hash As String) As Boolean
                Dim sourceHash = GetHash(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function

            Public Shared Function GetHashBase64(source As String) As String
                Dim data = _md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(source))

                Return Convert.ToBase64String(data)
            End Function

            Public Shared Function VerifyHashBase64(source As String, hash As String) As Boolean
                Dim sourceHash = GetHashBase64(source)
                Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

                Return If(comparer.Compare(sourceHash, hash) = 0, True, False)
            End Function
        End Class
    End Class
#End Region

#Region "TimeDate"
    Public Class TimeDate
        Public Shared Function FromUnix() As Date
            Return FromUnix(0)
        End Function

        Public Shared Function FromUnix(ByVal UnixTime As Long) As Date
            Dim dTmp As Date

            dTmp = DateAdd(DateInterval.Second, UnixTime, #1/1/1970#)

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, 1, dTmp)
            End If

            Return dTmp
        End Function

        Public Shared Function ToUnix() As Long
            Return ToUnix(Date.Now)
        End Function

        Public Shared Function ToUnix(ByVal dTmp As Date) As Long
            Dim lTmp As Long

            If dTmp.IsDaylightSavingTime = True Then
                dTmp = DateAdd(DateInterval.Hour, -1, dTmp)
            End If

            lTmp = DateDiff(DateInterval.Second, #1/1/1970#, dTmp)

            Return lTmp
        End Function
    End Class
#End Region

#Region "PuppyPower"
    Public Class PuppyPower
        Public Shared ReadOnly Property Username As String
            Get
                Dim sTmp As String

                sTmp = Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower, LPCConstants.RegistryKeys.VALUE__PuppyPowerUserName, "")

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
        End Property

        Public Shared ReadOnly Property Password As String
            Get
                Dim sTmp As String

                sTmp = Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Puppypower, LPCConstants.RegistryKeys.VALUE__PuppyPowerPassWord, "")

                Return XOrObfuscation.Deobfuscate(sTmp)
            End Get
        End Property
    End Class
#End Region

#Region "Processes"
    Public Class Processes
        <DllImport("advapi32.dll", SetLastError:=True)> _
        Private Shared Function OpenProcessToken(ByVal ProcessHandle As IntPtr, ByVal DesiredAccess As Integer, ByRef TokenHandle As IntPtr) As Boolean
        End Function

        <DllImport("kernel32.dll", SetLastError:=True)> _
        Private Shared Function CloseHandle(ByVal hObject As IntPtr) As <MarshalAs(UnmanagedType.Bool)> Boolean
        End Function

        Private Shared mLastError As String = ""
        Public Shared ReadOnly Property LastError As String
            Get
                Dim sTmp As String = mLastError

                mLastError = ""

                Return sTmp
            End Get
        End Property

        Private Shared Function ConvertPasswordStringToSecureString(ByVal str As String) As System.Security.SecureString
            Dim password As New System.Security.SecureString

            For Each c As Char In str.ToCharArray
                password.AppendChar(c)
            Next

            Return password
        End Function

        Public Shared Function IsProcessRunning(sProcessName As String) As Boolean
            Dim bRet As Boolean = False, sSearchProcessName__ToLower As String, sProcessName__ToLower As String

            sSearchProcessName__ToLower = sProcessName.ToLower

            If sProcessName.EndsWith(".exe") Then
                sSearchProcessName__ToLower = sSearchProcessName__ToLower.Replace(".exe", "")
            End If

            For Each p As Process In Process.GetProcesses
                sProcessName__ToLower = p.ProcessName.ToLower
                If sProcessName__ToLower.Contains(sSearchProcessName__ToLower) Then
                    bRet = True
                    Exit For
                End If
            Next

            Return bRet
        End Function

        Public Shared Function IsUserLoggedIn(UserName As String) As Boolean
            Dim iCount As Integer

            iCount = CountProcessesOwnedByUser(UserName)

            If iCount >= LPCConstants.Misc.CompletelyArbitraryProcessCountThreshold Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function CountProcessesOwnedByUser(sUserNameToCheck As String) As Long
            Dim wiName As String, lCount As Long

            lCount = 0

            For Each p As Process In Process.GetProcesses
                Try
                    Dim hToken As IntPtr
                    If OpenProcessToken(p.Handle, TokenAccessLevels.Query, hToken) Then
                        Using wi As New WindowsIdentity(hToken)
                            wiName = wi.Name.Remove(0, wi.Name.LastIndexOf("\") + 1)

                            If wiName.ToLower = sUserNameToCheck.ToLower Then
                                lCount += 1
                            End If
                        End Using
                        CloseHandle(hToken)
                    End If
                Catch ex As Exception

                End Try
            Next

            Return lCount
        End Function

        Public Shared Function StartProcessAsUser(cmdLine As String, sUserName As String, sPassword As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOut As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, sUserName, sPassword, cmdArguments, waitForExit, timeOut)
        End Function

        Public Shared Function StartProcess(cmdLine As String, Optional cmdArguments As String = "", Optional waitForExit As Boolean = True, Optional timeOut As Integer = -1) As Boolean
            Return _StartProcess(cmdLine, "", "", cmdArguments, waitForExit, timeOut)
        End Function

        Private Shared Function _StartProcess(cmdLine As String, sUserName As String, sPassword As String, cmdArguments As String, waitForExit As Boolean, timeOut As Integer) As Boolean
            Dim pInfo As New ProcessStartInfo()

            pInfo.FileName = cmdLine
            pInfo.Arguments = cmdArguments
            If sUserName <> "" Then
                pInfo.UserName = sUserName
                pInfo.Password = ConvertPasswordStringToSecureString(sPassword)
                pInfo.Domain = Nothing
                pInfo.UseShellExecute = False
            End If

            Try
                Dim p As New Process

                p = Process.Start(pInfo)

                If p.Id > 0 Then
                    'all ok
                Else
                    'wtf, not started
                    mLastError = "not started (pid<=0)"
                    Return False
                End If

                If waitForExit Then
                    Try
                        'This is here for processes without graphical UI
                        p.WaitForInputIdle()
                    Catch ex As Exception

                    End Try

                    p.WaitForExit(timeOut)

                    If p.HasExited = False Then
                        If p.Responding Then
                            p.CloseMainWindow()
                        Else
                            p.Kill()
                        End If

                        mLastError = "timed out!"
                        Return False
                    End If
                End If
            Catch ex As Exception
                mLastError = "not started (" & ex.Message & ")"
                Return False
            End Try

            Return True
        End Function

        Public Shared Function IsAppAlreadyRunning() As Boolean
            Dim appProc() As Process
            Dim strModName, strProcName As String

            Try
                strModName = Process.GetCurrentProcess.MainModule.ModuleName
                strProcName = System.IO.Path.GetFileNameWithoutExtension(strModName)

                appProc = Process.GetProcessesByName(strProcName)

                If appProc.Length > 1 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception

            End Try

            Return False
        End Function
    End Class
#End Region

#Region "XOrObfuscation"
    Public Class XOrObfuscation
        Private Shared ReadOnly PassPhraseLength As Integer = 23
        Private Shared ReadOnly ExtraPassPhrase As String = "$#(98)&_Ndf9m0987e5WRW#%rtefgm03947"
        Private Shared arrPass() As Integer

        Private Shared arrExtraPass() As Integer

        Public Shared Function Obfuscate(ByVal PlainText As String) As String
            Dim i As Integer, p As Integer
            Dim ObscuredText_RandomPassphrase As String, ObscuredText_ExtraPassphrase As String

            If PlainText.Length > 1000 Then
                Return "(string too long)"
            End If

            InitializePasshrases()

            Try
                p = 0
                ObscuredText_RandomPassphrase = ""
                For i = 0 To PassPhraseLength - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(arrPass(i))
                Next
                For i = 0 To Len(PlainText) - 1
                    ObscuredText_RandomPassphrase = ObscuredText_RandomPassphrase & _D2H(Asc(Mid(PlainText, i + 1, 1)) Xor arrPass(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next

                p = 0
                ObscuredText_ExtraPassphrase = ""
                For i = 0 To Len(ObscuredText_RandomPassphrase) - 1
                    ObscuredText_ExtraPassphrase = ObscuredText_ExtraPassphrase & _D2H(Asc(Mid(ObscuredText_RandomPassphrase, i + 1, 1)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                ObscuredText_ExtraPassphrase = ""
            End Try

            Return ObscuredText_ExtraPassphrase
        End Function

        Public Shared Function Deobfuscate(ByVal ObscuredText As String) As String
            Dim a() As Integer
            Dim i As Integer, j As Integer, p As Integer
            Dim PlainText_ExtraPassphrase As String, PlainText_RandomPassphrase As String

            InitializePasshrases()

            Try
                p = 0
                PlainText_ExtraPassphrase = ""
                For i = 1 To Len(ObscuredText) Step 2
                    PlainText_ExtraPassphrase = PlainText_ExtraPassphrase & Chr(_H2D(Mid(ObscuredText, i, 2)) Xor arrExtraPass(p))
                    p = p + 1
                    If p >= Len(ExtraPassPhrase) Then
                        p = 0
                    End If
                Next

                ReDim a(PassPhraseLength)
                p = 0
                PlainText_RandomPassphrase = ""
                For i = 1 To PassPhraseLength * 2 Step 2
                    j = ((i + 1) / 2) - 1
                    a(j) = _H2D(Mid(PlainText_ExtraPassphrase, i, 2))
                Next
                For i = PassPhraseLength * 2 + 1 To Len(PlainText_ExtraPassphrase) Step 2
                    PlainText_RandomPassphrase = PlainText_RandomPassphrase & Chr(_H2D(Mid(PlainText_ExtraPassphrase, i, 2)) Xor a(p))
                    p = p + 1
                    If p >= PassPhraseLength Then
                        p = 0
                    End If
                Next
            Catch ex As Exception
                PlainText_RandomPassphrase = ""
            End Try


            Return PlainText_RandomPassphrase
        End Function

        Private Shared Sub InitializePasshrases()
            Dim i As Integer

            Randomize()

            ReDim arrPass(PassPhraseLength)
            For i = 0 To PassPhraseLength - 1
                arrPass(i) = Int(255 * Rnd())
            Next

            ReDim arrExtraPass(Len(ExtraPassPhrase))
            For i = 0 To Len(ExtraPassPhrase) - 1
                arrExtraPass(i) = Asc(Mid(ExtraPassPhrase, i + 1, 1))
            Next
        End Sub

        Private Shared Function _D2H(ByVal Value As Integer) As String
            Dim iVal As Integer, temp As Integer, ret As Integer, i As Integer, Str As String = ""
            Dim BinVal() As String

            iVal = Value
            Do
                temp = Int(iVal / 16)
                ret = iVal Mod 16
                ReDim Preserve BinVal(i)
                BinVal(i) = _NoToHex(ret)
                i = i + 1
                iVal = temp
            Loop While temp > 0
            For i = UBound(BinVal) To 0 Step -1
                Str = Str & CStr(BinVal(i))
            Next
            If Value < 16 Then
                Str = "0" & Str
            End If

            _D2H = Str
        End Function

        Private Shared Function _H2D(ByVal BinVal As String) As Integer
            Dim iVal As Integer, temp As Integer

            temp = _HexToNo(Mid(BinVal, 2, 1))
            iVal = iVal + temp
            temp = _HexToNo(Mid(BinVal, 1, 1))
            iVal = iVal + (temp * 16)

            _H2D = iVal
        End Function

        Private Shared Function _NoToHex(ByVal i As Integer) As String
            Select Case i
                Case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9
                    _NoToHex = CStr(i)
                Case 10
                    _NoToHex = "a"
                Case 11
                    _NoToHex = "b"
                Case 12
                    _NoToHex = "c"
                Case 13
                    _NoToHex = "d"
                Case 14
                    _NoToHex = "e"
                Case 15
                    _NoToHex = "f"
                Case Else
                    _NoToHex = ""
            End Select
        End Function

        Private Shared Function _HexToNo(ByVal s As String) As Integer
            Select Case s
                Case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
                    _HexToNo = CInt(s)
                Case "A", "a"
                    _HexToNo = 10
                Case "B", "b"
                    _HexToNo = 11
                Case "C", "c"
                    _HexToNo = 12
                Case "D", "d"
                    _HexToNo = 13
                Case "E", "e"
                    _HexToNo = 14
                Case "F", "f"
                    _HexToNo = 15
                Case Else
                    _HexToNo = -1
            End Select
        End Function
    End Class
#End Region

#Region "Windows"
    Public Class Windows
        Public Shared Function IsRunningAsAdmin() As Boolean
            Dim bTmp As Boolean

            Try
                Dim user As WindowsIdentity = WindowsIdentity.GetCurrent()
                Dim principal As New WindowsPrincipal(user)

                bTmp = principal.IsInRole(WindowsBuiltInRole.Administrator)
            Catch ex As UnauthorizedAccessException
                bTmp = False
            Catch ex As Exception
                bTmp = False
            End Try

            Return bTmp
        End Function
    End Class
#End Region

#Region "Forms"
    Public Class Forms
        <DllImport("user32.dll", SetLastError:=True)> _
        Private Shared Function SetWindowPos(ByVal hWnd As IntPtr, ByVal hWndInsertAfter As IntPtr, ByVal X As Integer, ByVal Y As Integer, ByVal cx As Integer, ByVal cy As Integer, ByVal uFlags As Integer) As Boolean
        End Function

        Private Const SWP_NOSIZE As Integer = &H1
        Private Const SWP_NOMOVE As Integer = &H2

        Private Shared ReadOnly HWND_TOPMOST As New IntPtr(-1)
        Private Shared ReadOnly HWND_NOTOPMOST As New IntPtr(-2)

        Public Shared Sub TopMost(FormHandle As System.IntPtr, bTopMost As Boolean)
            If bTopMost Then
                SetWindowPos(FormHandle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            Else
                SetWindowPos(FormHandle, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE Or SWP_NOSIZE)
            End If
        End Sub
    End Class
#End Region

#Region "Users"
    Public Class WindowsUser
        Private Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000

        Private Const SID_USERS As String = "S-1-5-32-545"
        Private Const SID_ADMINISTRATORS As String = "S-1-5-32-544"

        Public Shared Function DoesUserExist(sUsername As String) As Boolean
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    Return True
                End If
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Function IsUserAdmin(sUsername As String) As Boolean
            Try
                Dim DC = New PrincipalContext(ContextType.Machine)

                Dim user = UserPrincipal.FindByIdentity(DC, sUsername)
                Dim groups = user.GetGroups()
                Dim adminGroup As String = GetAdministratorsGroupName()

                For Each group As Principal In groups
                    If group.Name = adminGroup Then
                        Return True
                    End If
                Next
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            Return False
        End Function

        Public Shared Function AddAdminUser(ByVal sUsername As String, sDescription As String, ByVal sPassWord As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim NewUser As DirectoryEntry = AD.Children.Add(sUsername, "user")
                NewUser.Invoke("SetPassword", New Object() {sPassWord})
                NewUser.Invoke("Put", New Object() {"Description", sDescription})
                NewUser.CommitChanges()

                Dim grp As DirectoryEntry, sGrp As String = GetAdministratorsGroupName()

                If sGrp = "" Then
                    sGrp = "Administrators"
                End If

                grp = AD.Children.Find(sGrp, "group")
                If grp.Name <> "" Then
                    grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
                Else
                    Return "group failed"
                End If

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function AddUser(ByVal sUsername As String, sDescription As String, ByVal sPassWord As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim NewUser As DirectoryEntry = AD.Children.Add(sUsername, "user")
                NewUser.Invoke("SetPassword", New Object() {sPassWord})
                NewUser.Invoke("Put", New Object() {"Description", sDescription})
                NewUser.CommitChanges()

                Dim grp As DirectoryEntry, sGrp As String = GetUsersGroupName()

                If sGrp = "" Then
                    sGrp = "Users"
                End If

                grp = AD.Children.Find(sGrp, "group")
                If grp.Name <> "" Then
                    grp.Invoke("Add", New Object() {NewUser.Path.ToString()})
                End If

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function ChangePassword(ByVal sUsername As String, ByVal sPassWord As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    usr.Invoke("SetPassword", New Object() {sPassWord})
                    usr.CommitChanges()

                    Return "ok"
                Else
                    Return "not found"
                End If
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function RemovePasswordExpiry(ByVal sUsername As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    usr.Properties("UserFlags").Value = ADS_UF_DONT_EXPIRE_PASSWD
                    usr.CommitChanges()

                    Return "ok"
                Else
                    Return "not found"
                End If
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function PromoteUserToAdmin(sUsername As String) As String
            Try
                Dim AD As DirectoryEntry = New DirectoryEntry("WinNT://" + Environment.MachineName + ",computer")
                Dim usr As DirectoryEntry = AD.Children.Find(sUsername, "user")

                If usr.Name <> "" Then
                    Dim grp As DirectoryEntry, sGrp As String = GetAdministratorsGroupName()

                    If sGrp = "" Then
                        sGrp = "Administrators"
                    End If

                    grp = AD.Children.Find(sGrp, "group")
                    If grp.Name <> "" Then
                        grp.Invoke("Add", New Object() {usr.Path.ToString()})
                    End If
                End If

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function

        Public Shared Function GetAdministratorsGroupName() As String
            Return GetGroupNameFromSid(SID_ADMINISTRATORS)
        End Function

        Public Shared Function GetUsersGroupName() As String
            Return GetGroupNameFromSid(SID_USERS)
        End Function

        Private Shared Function GetGroupNameFromSid(ByVal _sid As String) As String
            Try
                Dim context As PrincipalContext, group As GroupPrincipal

                context = New PrincipalContext(ContextType.Machine)
                group = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, _sid)

                Return group.SamAccountName
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Public Shared Function HideUserInLogonScreen(ByVal sUsername As String) As Boolean
            Try
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\SpecialAccounts\UserList", sUsername, 0)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
    End Class

#End Region

#Region "Event log"
    Public Class WindowsEventLog
        Public Shared Function WriteInformation(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Information)
        End Function

        Public Shared Function WriteError(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Error)
        End Function

        Public Shared Function WriteWarning(sEvent As String) As String
            Return WriteEntry(sEvent, EventLogEntryType.Warning)
        End Function

        Public Shared Function WriteEntry(sEvent As String, oType As EventLogEntryType) As String
            Try
                Dim sSource As String = My.Application.Info.ProductName
                Dim sLog As String = "Application"

                If Not EventLog.SourceExists(sSource) Then
                    EventLog.CreateEventSource(New EventSourceCreationData(sSource, sLog))
                End If

                Dim ELog As New EventLog(sLog, ".", sSource)
                ELog.WriteEntry(sEvent, EventLogEntryType.Error)

                Return "ok"
            Catch ex As Exception
                Return ex.Message
            End Try
        End Function
    End Class
#End Region

#Region "FileFolder"
    Public Class FileFolder
        Public Shared Function GetProgramFilesFolder() As String
            Dim sPath As String = ""

            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sPath.Equals(String.Empty) Then
                sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            Return sPath
        End Function
    End Class
#End Region

#Region "Network"
    Public Class Network
        Public Class NetStat
            Private Const AF_INET As Integer = 2

            <StructLayout(LayoutKind.Sequential)> _
            Private Structure MIB_TCPTABLE_OWNER_PID
                Public dwNumEntries As UInteger
                Private table As MIB_TCPROW_OWNER_PID
            End Structure

            Private Enum TCP_TABLE_CLASS
                TCP_TABLE_BASIC_LISTENER
                TCP_TABLE_BASIC_CONNECTIONS
                TCP_TABLE_BASIC_ALL
                TCP_TABLE_OWNER_PID_LISTENER
                TCP_TABLE_OWNER_PID_CONNECTIONS
                TCP_TABLE_OWNER_PID_ALL
                TCP_TABLE_OWNER_MODULE_LISTENER
                TCP_TABLE_OWNER_MODULE_CONNECTIONS
                TCP_TABLE_OWNER_MODULE_ALL
            End Enum

            <DllImport("iphlpapi.dll", SetLastError:=True)> _
            Private Shared Function GetExtendedTcpTable(ByVal tcpTable As IntPtr, ByRef tcpTableLength As Integer, ByVal sort As Boolean, ByVal ipVersion As Integer, ByVal tcpTableType As TCP_TABLE_CLASS, ByVal reserved As Integer) As UInteger
            End Function

            <StructLayout(LayoutKind.Sequential)> _
            Public Structure MIB_TCPROW_OWNER_PID
                ' DWORD is System.UInt32 in C#
                <MarshalAs(UnmanagedType.U4)> Public state As UInt32
                <MarshalAs(UnmanagedType.U4)> Public localAddr As UInt32
                <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> Public m_localPort As Byte()
                <MarshalAs(UnmanagedType.U4)> Public remoteAddr As UInt32
                <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> Public m_remotePort As Byte()
                <MarshalAs(UnmanagedType.U4)> Public owningPid As UInt32

                Public ReadOnly Property LocalAddress() As IPAddress
                    Get
                        Return New IPAddress(localAddr)
                    End Get
                End Property

                Public ReadOnly Property LocalPort() As UShort
                    Get
                        Return BitConverter.ToUInt16(New Byte(1) {m_localPort(1), m_localPort(0)}, 0)
                    End Get
                End Property

                Public ReadOnly Property RemoteAddress() As IPAddress
                    Get
                        Return New IPAddress(remoteAddr)
                    End Get
                End Property

                Public ReadOnly Property RemotePort() As UShort
                    Get
                        Return BitConverter.ToUInt16(New Byte(1) {m_remotePort(1), m_remotePort(0)}, 0)
                    End Get
                End Property
            End Structure

            Public Class TcpRow
                Private ReadOnly _localEndPoint As IPEndPoint
                Private ReadOnly _remoteEndPoint As IPEndPoint
                Private ReadOnly _state As TcpState
                Private ReadOnly _process As Process

                Public Sub New(ByVal tcpRow As MIB_TCPROW_OWNER_PID)
                    _state = tcpRow.state
                    _process = Process.GetProcessById(tcpRow.owningPid)

                    Dim localPort As Integer = tcpRow.LocalPort
                    Dim localAddress As IPAddress = tcpRow.LocalAddress
                    _localEndPoint = New IPEndPoint(localAddress, localPort)

                    Dim remotePort As Integer = tcpRow.RemotePort
                    Dim remoteAddress As IPAddress = tcpRow.RemoteAddress
                    _remoteEndPoint = New IPEndPoint(remoteAddress, remotePort)
                End Sub

                Public ReadOnly Property LocalEndPoint() As IPEndPoint
                    Get
                        Return _localEndPoint
                    End Get
                End Property

                Public ReadOnly Property RemoteEndPoint() As IPEndPoint
                    Get
                        Return _remoteEndPoint
                    End Get
                End Property

                Public ReadOnly Property State() As TcpState
                    Get
                        Return _state
                    End Get
                End Property

                Public ReadOnly Property ProcessId() As Integer
                    Get
                        Return _process.Id
                    End Get
                End Property

                Public ReadOnly Property ProcessName() As String
                    Get
                        Return _process.ProcessName
                    End Get
                End Property

                Public Overrides Function ToString() As String
                    Return String.Format("TCP    {0,-23}{1, -23}{2,-14}{3,-6}{4}", LocalEndPoint, RemoteEndPoint, State, ProcessId, ProcessName)
                End Function
            End Class

            Public Shared Function GetTcpTable(Optional ByVal sorted As Boolean = False) As List(Of TcpRow)
                Dim tcpRows As New List(Of TcpRow)

                Dim tcpTable As IntPtr = IntPtr.Zero
                Dim tcpTableLength As Integer = 0

                If GetExtendedTcpTable(tcpTable, tcpTableLength, sorted, AF_INET, TCP_TABLE_CLASS.TCP_TABLE_OWNER_PID_ALL, 0) <> 0 Then
                    Try
                        tcpTable = Marshal.AllocHGlobal(tcpTableLength)
                        If GetExtendedTcpTable(tcpTable, tcpTableLength, True, AF_INET, TCP_TABLE_CLASS.TCP_TABLE_OWNER_PID_ALL, 0) = 0 Then
                            Dim table As MIB_TCPTABLE_OWNER_PID = CType(Marshal.PtrToStructure(tcpTable, GetType(MIB_TCPTABLE_OWNER_PID)), MIB_TCPTABLE_OWNER_PID)

                            Dim rowPtr As IntPtr = New IntPtr(tcpTable.ToInt64() + Marshal.SizeOf(table.dwNumEntries))
                            For i As Integer = 0 To table.dwNumEntries - 1
                                tcpRows.Add(New TcpRow(CType(Marshal.PtrToStructure(rowPtr, GetType(MIB_TCPROW_OWNER_PID)), MIB_TCPROW_OWNER_PID)))
                                rowPtr = New IntPtr(rowPtr.ToInt64() + Marshal.SizeOf(GetType(MIB_TCPROW_OWNER_PID)))
                            Next
                        End If
                    Finally
                        If tcpTable <> IntPtr.Zero Then
                            Marshal.FreeHGlobal(tcpTable)
                        End If
                    End Try
                End If

                Return tcpRows
            End Function
        End Class

        Public Class LogMeIn
            Public Shared Function GetConnectionCount() As Integer
                Dim iCount As Integer = 0

                For Each tcpr As NetStat.TcpRow In NetStat.GetTcpTable()
                    If tcpr.ProcessName.Equals("LogMeIn") And tcpr.State = TcpState.Established Then
                        iCount += 1
                    End If
                Next

                Return iCount
            End Function
        End Class
    End Class
#End Region
End Class
