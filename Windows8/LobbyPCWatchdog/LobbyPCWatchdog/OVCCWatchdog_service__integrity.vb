﻿Module OVCCWatchdog_service__integrity
    Public Sub Check_Integrity_Main()
        Helpers.Logger.WriteDebug("(integrity) nope", , 0)
    End Sub

    Public Sub Check_Integrity_Main_()
        Helpers.Logger.Write("(integrity) starting thread", , 0)
        If Not LPCGlobals.Settings.CheckIntegrity Then
            Helpers.Logger.Write("(integrity) disabled, so ignored", , 1)

            Exit Sub
        End If


        Helpers.Logger.Write("(integrity) file access", , 1)

        If LPCGlobals.Settings.Integrity.CheckFileAccess Then
            _Integrity_FileAccess()
        Else
            Helpers.Logger.Write("(integrity) disabled", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If


        Helpers.Logger.Write("(integrity) theme settings", , 1)

        If LPCGlobals.Settings.Integrity.CheckThemeSettings Then
            _Integrity_ThemeSettings()
        Else
            Helpers.Logger.Write("(integrity) disabled", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If


        Helpers.Logger.Write("(integrity) sitekiosk profile", , 1)

        If LPCGlobals.Settings.Integrity.CheckSiteKioskProfileBroken Then
            _Integrity_SiteKioskProfileBroken()
        Else
            Helpers.Logger.Write("(integrity) disabled", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If


        Helpers.Logger.Write("(integrity) sitekiosk profile path", , 1)

        If LPCGlobals.Settings.Integrity.CheckSiteKioskProfilePath Then
            _Integrity_SiteKioskProfilePath()
        Else
            Helpers.Logger.Write("(integrity) disabled", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If
    End Sub

    Private Sub _Integrity_FileAccess()
        Dim sFile_LastActivityFile As String = ""

        Helpers.Logger.WriteRelative("check file", , 1)
        sFile_LastActivityFile = IO.Path.Combine(LPCConstants.FilesAndFolders.FILE__LastActivityTempFilePath, LPCConstants.FilesAndFolders.FILE__LastActivityTempFileName)

        Helpers.Logger.WriteRelative(sFile_LastActivityFile, , 2)
        If Not IO.File.Exists(sFile_LastActivityFile) Then
            Helpers.Logger.WriteRelative("doesn't exist, creating", , 3)
            Try
                Dim sWriter As New IO.StreamWriter(sFile_LastActivityFile, False)
                sWriter.Write("<lastactivity><date>2517393587</date><idle>0</idle></lastactivity>") ' 2517393587 = 2049-10-09T11:59:47+00:00
                sWriter.Close()
            Catch ex As Exception
                Helpers.Logger.WriteRelative("error", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End Try
        End If


        If IO.File.Exists(sFile_LastActivityFile) Then
            Helpers.Logger.WriteRelative("updating permissions", , 3)
            If Helpers.FilesAndFolders.Permissions.FileSecurity_REPLACE__Full_Everyone(sFile_LastActivityFile) Then
                Helpers.Logger.WriteRelative("ok", , 4)
            Else
                Helpers.Logger.WriteRelative("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                Helpers.Logger.WriteRelative(Helpers.Errors.GetLast(False), Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End If
        Else
            Helpers.Logger.WriteRelative("not found!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If
    End Sub

    Private Sub _Integrity_SiteKioskProfilePath()
        Helpers.Logger.WriteMessageRelative("(integrity - profile path) checking ", 1)

        Dim sProfilePath As String = ""

        Helpers.Logger.WriteMessageRelative("(integrity - profile path) profile path", 2)

        If SitekioskUserProfile.GetProfileFolder(sProfilePath) Then
            Helpers.Logger.WriteMessageRelative("(integrity - profile path) " & sProfilePath, 3)
        Else
            Helpers.Logger.WriteMessageRelative("(integrity - profile path) not found in registry, reverting to default", 3)
            sProfilePath = LPCConstants.FilesAndFolders.PATH__DefaultSitekioskProfilePath
        End If

        If IO.Directory.Exists(sProfilePath) Then
            Helpers.Logger.WriteMessageRelative("(integrity - profile path) ok, exists", 4)
        Else
            Helpers.Logger.WriteErrorRelative("(integrity - profile path) uh oh, does not exist!", 4)

            Exit Sub
        End If

        'check all places that use sitekiosk profile path ; sitekiosk, file explorer, skype, etc.
        Try
            Dim sSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()
            Dim bConfigChanged As Boolean = False

            Helpers.Logger.WriteMessageRelative("(integrity - profile path) checking " & sSkCfgFile, 2)


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument
            xmlSkCfg.Load(sSkCfgFile)

            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)


            Dim xmlNode_DownloadManager As Xml.XmlNode
            Dim xmlNode_DownloadFolder As Xml.XmlNode
            'Dim xmlNode_DeleteDirectories As Xml.XmlNode
            'Dim xmlNode_Dirs As Xml.XmlNodeList
            Dim sValue0 As String = "", sValue1 As String = ""

            xmlNode_DownloadManager = xmlNode_Root.SelectSingleNode("sk:download-manager", ns)

            Helpers.Logger.WriteMessageRelative("(integrity - profile path) download-folder ", 3)
            xmlNode_DownloadFolder = xmlNode_DownloadManager.SelectSingleNode("sk:download-folder", ns)
            sValue0 = xmlNode_DownloadFolder.InnerText
            sValue1 = _CheckAndCorrectPath(sProfilePath, sValue0)
            If Not sValue0.Equals(sValue1) Then
                xmlNode_DownloadFolder.InnerText = sValue1

                Helpers.Logger.WriteMessageRelative("(integrity - profile path) download-folder ", 3)

                bConfigChanged = True
            Else
                'ok
            End If

            '         <download-folder>C:\Users\SiteKiosk\Downloads</download-folder>
            '<delete-downloads>true</delete-downloads>
            '<delete-at-logout>true</delete-at-logout>
            '<delete-old-files>false</delete-old-files>
            '<max-download-age>7200</max-download-age>
            '<limit-download-size>false</limit-download-size>
            '<max-download-size>10485760</max-download-size>
            '<delete-directories>
            '  <dir>C : \Users\Public\Documents\SiteKiosk</dir>
        Catch ex As Exception

        End Try
    End Sub

    Private Function _CheckAndCorrectPath(SiteKioskProfilePath As String, CurrentPath As String) As String
        Dim aPath As String(), pValue As String = ""

        aPath = CurrentPath.Split("\")

        If aPath.Count < 3 Then
            'skip
            Return CurrentPath
        Else
            pValue = String.Join("\", aPath.Take(3))
        End If

        If pValue.Equals(SiteKioskProfilePath) Then
            'all ok
            Return CurrentPath
        Else
            Return CurrentPath.Replace(pValue, SiteKioskProfilePath).Replace("\\", "\")
        End If
    End Function

    Private Sub _Integrity_SiteKioskProfileBroken()

    End Sub

    Private Sub _Integrity_ThemeSettings()
        Try
            Dim sSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()
            Dim bConfigChanged As Boolean = False

            Helpers.Logger.WriteMessageRelative("(integrity - theme) checking " & sSkCfgFile, 1)


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument
            xmlSkCfg.Load(sSkCfgFile)

            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)


            Dim xmlNode_Global As Xml.XmlNode
            Dim xmlNode_IsUsingChrome As Xml.XmlNode
            Dim sValue As String = ""

            xmlNode_Global = xmlNode_Root.SelectSingleNode("sk:global", ns)
            xmlNode_IsUsingChrome = xmlNode_Global.SelectSingleNode("sk:is-using-chrome", ns)

            If Not xmlNode_IsUsingChrome Is Nothing Then
                sValue = xmlNode_IsUsingChrome.InnerText

                If sValue = "true" Then
                    Helpers.Logger.WriteWarningRelative("(integrity - theme) uh oh", 2)
                    Helpers.Logger.WriteWarningRelative("(integrity - theme) is-using-chrome=true", 3)

                    xmlNode_IsUsingChrome.InnerText = "false"

                    bConfigChanged = True
                End If
            Else
                Helpers.Logger.WriteMessageRelative("(integrity - theme) is-using-chrome not found", 2)
            End If


            Dim xmlNode_SiteSkin As Xml.XmlNode
            Dim xmlNode_DefaultSkin As Xml.XmlNode

            xmlNode_SiteSkin = xmlNode_Root.SelectSingleNode("sk:siteskin", ns)
            xmlNode_DefaultSkin = xmlNode_SiteSkin.SelectSingleNode("sk:default-skin", ns)

            sValue = xmlNode_DefaultSkin.InnerText

            If Not sValue.Equals(LPCGlobals.Settings.Integrity.DefaultSkin) Then
                Helpers.Logger.WriteWarningRelative("(integrity - theme) uh oh", 2)
                Helpers.Logger.WriteWarningRelative("(integrity - theme) default-skin='" & sValue & "'", 3)

                xmlNode_DefaultSkin.InnerText = LPCGlobals.Settings.Integrity.DefaultSkin

                bConfigChanged = True
            End If


            '---------------------------------------------------
            Helpers.Logger.WriteMessageRelative("saving", 2)
            If bConfigChanged Then
                Helpers.Logger.WriteRelative("backup", , 3)

                Try
                    Dim sBackupFolder As String = LPCConstants.FilesAndFolders.FOLDER__Backup

                    sBackupFolder = sBackupFolder.Replace("%%Application.ProductName%%", My.Application.Info.ProductName)
                    sBackupFolder = sBackupFolder.Replace("%%Application.ProductVersion%%", My.Application.Info.Version.ToString)
                    sBackupFolder = sBackupFolder.Replace("%%Backup.Date%%", Now.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_"))

                    Helpers.Logger.WriteRelative("destination", , 4)
                    Helpers.Logger.WriteRelative(sBackupFolder, , 5)

                    IO.Directory.CreateDirectory(sBackupFolder)

                    Helpers.FilesAndFolders.Backup.File(sSkCfgFile, sBackupFolder, 3, My.Application.Info.ProductName)
                Catch ex As Exception
                    Helpers.Logger.WriteErrorRelative("FAILED", 4)
                    Helpers.Logger.WriteErrorRelative(ex.Message, 5)
                End Try


                Helpers.Logger.WriteMessageRelative("writing", 3)
                Helpers.Logger.WriteMessageRelative(sSkCfgFile, 4)
                xmlSkCfg.Save(sSkCfgFile)
                Helpers.Logger.WriteMessageRelative("ok", 5)
            Else
                Helpers.Logger.WriteWarningRelative("skipped, nothing was changed", 3)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteMessageRelative("(integrity - theme) exception", 1)
            Helpers.Logger.WriteMessageRelative("(integrity - theme) " & ex.Message, 2)
        End Try
    End Sub
End Module
