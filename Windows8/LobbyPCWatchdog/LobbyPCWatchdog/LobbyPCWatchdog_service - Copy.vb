﻿Public Class LobbyPCWatchdog
    Private mThreading_Main As Threading.Thread
    Private mLoopAbort As Boolean = False
    Private mFailCount As Long = 0

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        LPCGlobals.Objects.InitialiseLogger()

        LPCGlobals.Objects.Logger.WriteToLog(New String("*", 50))
        LPCGlobals.Objects.Logger.WriteToLog(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        LPCGlobals.Objects.Logger.WriteToLog("service started")

        LoadSettings()

        StartMainThread()
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__LastState, LPCState.LOBBYPC_STATE.NOT_SET)

        KillThreads()

        LPCGlobals.Objects.Logger.WriteToLog("service stopped")
    End Sub

    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadSettings(Optional bHideFromLog As Boolean = False)
        LPCGlobals.Settings.Load()
        LPCGlobals.Objects.Logger.Debugging = LPCGlobals.Settings.Debug

        LPCState.UsernameToCheck = LPCConstants.Users.USERNAME__SiteKiosk

        If Not bHideFromLog Or LPCGlobals.Settings.Debug Then
            LPCGlobals.Objects.Logger.WriteToLog("loading settings", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 0)
            LPCGlobals.Objects.Logger.WriteToLog("Debug: " & LPCGlobals.Settings.Debug.ToString, MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            LPCGlobals.Objects.Logger.WriteToLog("Disabled: " & LPCGlobals.Settings.Disabled.ToString, MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
            LPCGlobals.Objects.Logger.WriteToLog("TestMode: " & LPCGlobals.Settings.TestMode.ToString, MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
        End If
    End Sub

    Private Sub Thread_Main()
        'Wait for a while
        LPCGlobals.Objects.Logger.WriteToLog("first run delay (" & LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__FirstRun & " sec)")
        Threading.Thread.Sleep(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__FirstRun * 1000)

        Do While Not mLoopAbort
            LoadSettings(True)

            If LPCGlobals.Settings.Disabled Then
                LPCGlobals.Objects.Logger.WriteToLog("Watchdog is disabled!", MyLogger.MESSAGE_TYPE.LOG_WARNING, 0)
            Else
                LPCGlobals.Objects.Logger.WriteToLog("checking state", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 0)
                If LPCState.DetermineAction() Then
                    'Got the LPC state
                    mFailCount = 0

                    LPCGlobals.Objects.Logger.WriteToLog("current state", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                    LPCGlobals.Objects.Logger.WriteToLog(LPCState.CurrentState, MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)
                    LPCGlobals.Objects.Logger.WriteToLog("previous state", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                    LPCGlobals.Objects.Logger.WriteToLog(LPCState.PreviousState, MyLogger.MESSAGE_TYPE.LOG_DEBUG, 2)

                    If LPCState.Action.STOP_SERVICE Then
                        LPCGlobals.Objects.Logger.WriteToLog("Quitting, something is really wrong!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                        mLoopAbort = True
                        Exit Do
                    End If

                    If LPCState.Action.FAILURE Then
                        'General failure somewhere. Let´s try again next time.
                        LPCGlobals.Objects.Logger.WriteToLog("General failure", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                    Else
                        If LPCState.CurrentState = LPCState.PreviousState Then
                            'Ok, action time
                            LPCGlobals.Objects.Logger.WriteToLog("state is same as before, action time", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                            If LPCState.CurrentState And LPCState.LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING Then
                                'Warning is running, should not take so long. Reboot!
                                LPCGlobals.Objects.Logger.WriteToLog("Forcing a restart, Warning is still running!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                LPCState.ForceRestart()
                            End If

                            'The actual action stuff
                            If LPCState.Action.IGNORED Then
                                'user activity, ignore action
                                LPCGlobals.Objects.Logger.WriteToLog("ignoring actions", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                If LPCState.Action.REBOOT Then
                                    LPCGlobals.Objects.Logger.WriteToLog("ignoring REBOOT", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                                If LPCState.Action.RESTART Then
                                    LPCGlobals.Objects.Logger.WriteToLog("ignoring RESTART", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                                If LPCState.Action.SHOW_WARNING Then
                                    LPCGlobals.Objects.Logger.WriteToLog("ignoring SHOW_WARNING", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                            ElseIf LPCState.Action.REBOOT Then
                                'rebooting
                                LPCGlobals.Objects.Logger.WriteToLog("Forced reboot!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                RebootWindows()
                            ElseIf LPCState.Action.RESTART Then
                                'restarting
                                SetSiteKioskAutoStart()

                                'Regardless of auto start was succesful, we are going to reboot
                                RebootWindows()

                                Exit Do
                            ElseIf LPCState.Action.SHOW_WARNING Then
                                'show warning
                                LPCGlobals.Objects.Logger.WriteToLog("Triggering warning!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                If LPCGlobals.Counters.WarningPopupTrigger < LPCConstants.TimeoutsAndDelaysAndCounters.COUNTERS__WarningPopupTriggerCountMax Then
                                    LPCGlobals.Counters.WarningPopupTrigger = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount)
                                    LPCGlobals.Counters.WarningPopupTrigger += 1
                                    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount, LPCGlobals.Counters.WarningPopupTrigger)

                                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode, False)
                                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupLoose, False)

                                    If LPCGlobals.Settings.TestMode Then
                                        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode, True)
                                    End If
                                    If LPCState.Action.WARNING_LOOSE Then
                                        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupLoose, True)
                                    End If

                                    LPCGlobals.Objects.Logger.WriteToLog("trigger (" & LPCGlobals.Counters.WarningPopupTrigger & "/" & LPCConstants.TimeoutsAndDelaysAndCounters.COUNTERS__WarningPopupTriggerCountMax & ")", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                                Else
                                    LPCGlobals.Objects.Logger.WriteToLog("Warning trigger failed, forced reboot!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                    RebootWindows()
                                    Exit Do
                                End If
                            End If
                        Else
                            LPCGlobals.Objects.Logger.WriteToLog("state is different", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                        End If




                    End If






                    If LPCState.CurrentState And LPCState.LOBBYPC_STATE.SITEKIOSK_NOT_INSTALLED Then
                        'Let's stop right here, SiteKiosk is not installed

                        LPCGlobals.Objects.Logger.WriteToLog("Quitting, SiteKiosk is not installed!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                        Exit Do
                    End If

                    If LPCState.CurrentState And LPCState.LOBBYPC_STATE.GENERAL_FAILURE Then
                        'General failure somewhere. Let´s try again next time.
                        LPCGlobals.Objects.Logger.WriteToLog("General failure", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                    Else
                        'Check if current state is the same as last time
                        If LPCState.CurrentState = LPCState.PreviousState Then
                            'Ok, action time
                            LPCGlobals.Objects.Logger.WriteToLog("state is same as before, action time", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                            If LPCState.CurrentState And LPCState.LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING Then
                                'Warning is running, should not take so long. Reboot!
                                LPCGlobals.Objects.Logger.WriteToLog("Forcing a restart, Warning is still running!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                LPCState.ForceRestart()
                            End If

                            'The actual action stuff
                            If LPCState.Action.IGNORED Then
                                'user activity, ignore action
                                LPCGlobals.Objects.Logger.WriteToLog("Keyboard/mouse activity detected!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                If LPCState.Action.REBOOT Then
                                    LPCGlobals.Objects.Logger.WriteToLog("ignoring REBOOT", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                                If LPCState.Action.RESTART Then
                                    LPCGlobals.Objects.Logger.WriteToLog("ignoring RESTART", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                                If LPCState.Action.SHOW_WARNING Then
                                    LPCGlobals.Objects.Logger.WriteToLog("ignoring SHOW_WARNING", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                            ElseIf LPCState.Action.REBOOT Then
                                'rebooting
                                LPCGlobals.Objects.Logger.WriteToLog("Forced reboot!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                RebootWindows()
                            ElseIf LPCState.Action.RESTART Then
                                'restarting
                                SetSiteKioskAutoStart()

                                'Regardless of auto start was succesful, we are going to reboot
                                RebootWindows()

                                Exit Do
                            ElseIf LPCState.Action.SHOW_WARNING Then
                                'show warning
                                LPCGlobals.Objects.Logger.WriteToLog("Triggering warning!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                If LPCGlobals.Counters.WarningPopupTrigger < LPCConstants.TimeoutsAndDelaysAndCounters.COUNTERS__WarningPopupTriggerCountMax Then
                                    LPCGlobals.Counters.WarningPopupTrigger = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount)
                                    LPCGlobals.Counters.WarningPopupTrigger += 1
                                    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount, LPCGlobals.Counters.WarningPopupTrigger)

                                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode, False)
                                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupLoose, False)

                                    If LPCGlobals.Settings.TestMode Then
                                        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode, True)
                                    End If
                                    If LPCState.Action.WARNING_LOOSE Then
                                        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupLoose, True)
                                    End If

                                    LPCGlobals.Objects.Logger.WriteToLog("trigger (" & LPCGlobals.Counters.WarningPopupTrigger & "/" & LPCConstants.TimeoutsAndDelaysAndCounters.COUNTERS__WarningPopupTriggerCountMax & ")", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                                Else
                                    LPCGlobals.Objects.Logger.WriteToLog("Warning trigger failed, forced reboot!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                                    RebootWindows()
                                    Exit Do
                                End If
                            End If
                        Else
                            LPCGlobals.Objects.Logger.WriteToLog("state is different", MyLogger.MESSAGE_TYPE.LOG_DEBUG, 1)
                        End If



                    End If

                    'Write state to registry
                    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__LastState, LPCState.CurrentState)
                Else
                    'Failed to get LPC state
                    'Let's count fails, and reboot after a few
                    mFailCount += 1

                    LPCGlobals.Objects.Logger.WriteToLog("failed to get LPC state! (" & mFailCount & "/" & LPCConstants.TimeoutsAndDelaysAndCounters.COUNT__Maxfails & ")", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

                    If mFailCount >= LPCConstants.TimeoutsAndDelaysAndCounters.COUNT__Maxfails Then
                        RebootWindows()
                    End If
                End If

                'Just an extra check, if the warning reports failure
                If Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailure) Then
                    Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailure, False)

                    LPCGlobals.Objects.Logger.WriteToLog("Warning reported fail, forced auto start!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)
                    LPCGlobals.Objects.Logger.WriteToLog("error from trigger:", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                    LPCGlobals.Objects.Logger.WriteToLog(Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError), MyLogger.MESSAGE_TYPE.LOG_ERROR, 2)
                    Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError, "")

                    SetSiteKioskAutoStart()
                    RebootWindows()

                    Exit Do
                End If
            End If

            Threading.Thread.Sleep(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__ServiceLoop * 1000)
        Loop
    End Sub

    Private Sub SetSiteKioskAutoStart()
        LPCGlobals.Objects.Logger.WriteToLog("Forcing auto start!", MyLogger.MESSAGE_TYPE.LOG_ERROR, 0)

        SiteKioskController.ResetError()
        SiteKioskController.ReadRegistryValues()

        LPCGlobals.Objects.Logger.WriteToLog("updating startup xml file", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
        If SiteKioskController.UpdateAutoStartupFile() Then
            LPCGlobals.Objects.Logger.WriteToLog("set startup", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
            If SiteKioskController.SetAutoStartup() Then
                LPCGlobals.Objects.Logger.WriteToLog("ok", MyLogger.MESSAGE_TYPE.LOG_ERROR, 2)
            Else
                LPCGlobals.Objects.Logger.WriteToLog("Something went wrong", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
                LPCGlobals.Objects.Logger.WriteToLog(SiteKioskController.SiteKiosk_Error.ToString, MyLogger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Else
            LPCGlobals.Objects.Logger.WriteToLog("Something went wrong", MyLogger.MESSAGE_TYPE.LOG_ERROR, 1)
            LPCGlobals.Objects.Logger.WriteToLog(SiteKioskController.SiteKiosk_Error.ToString, MyLogger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If
    End Sub

    Private Sub RebootWindows()
        If LPCGlobals.Settings.TestMode Then
            LPCGlobals.Objects.Logger.WriteToLog("I would have rebooted here, but we´re in test mode...", MyLogger.MESSAGE_TYPE.LOG_WARNING, 0)
        Else
            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        End If
    End Sub
End Class
