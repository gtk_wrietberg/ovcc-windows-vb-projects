﻿Public Class OVCCWatchdog
    Private mThreading_Main As Threading.Thread
    Private mThreading_Integrity As Threading.Thread
    Private mThreading_CleanLogs As Threading.Thread
    Private mThreadingVersionInventory As Threading.Thread

    Private mFailCount As Long = 0

    Protected Overrides Sub OnStart(ByVal args() As String)
        'init logger
        Helpers.Logger.InitialiseLogger()


        'save log path and name to registry
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Logging, LPCConstants.RegistryKeys.VALUE__CurrentLogfilePath, Helpers.Logger.LogFilePath)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Logging, LPCConstants.RegistryKeys.VALUE__CurrentLogfileName, Helpers.Logger.LogFileName)


        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("service started", , 0)


        'check run settings in registry
        Helpers.Logger.Write("helper apps auto run", , 0)
        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger, , 1)
        Helpers.Logger.Write(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger), , 2)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger))
        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity, , 1)
        Helpers.Logger.Write(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity), , 2)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity))
        Helpers.Logger.Write(LPCConstants.FilesAndFolders.FILE__WatchdogError, , 1)
        Helpers.Logger.Write(IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogError), , 2)
        Helpers.Windows.Startup.SetStartupProgram(LPCConstants.FilesAndFolders.FILE__WatchdogError,
                                                  IO.Path.Combine(LPCConstants.FilesAndFolders.FOLDER__ServicePath, LPCConstants.FilesAndFolders.FILE__WatchdogError))



        'load settings from registry
        LoadSettings()


        'reset disabled flag on service start
        If LPCGlobals.Settings.Disabled Then
            Helpers.Logger.Write("resetting disabled flag", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Disabled, LPCConstants.RegistryKeys.VALUE__WatchdogDisabled, "")
        End If

        'reset some registry stuff
        ResetRegistryValues()


        'Start cleaning the logs
        StartCleanLogsThread()

        'Start the main thread
        StartMainThread()

        'Start Integrity thread
        StartIntegrityThread()

        'Start Version Inventory thread
        'StartVersionInventoryThread()
    End Sub

    Protected Overrides Sub OnStop()
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__LastState, LPCState.LOBBYPC_STATE.OK)

        KillThreads()

        Helpers.Logger.Write("service stopped")
    End Sub

    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub StartCleanLogsThread()
        mThreading_CleanLogs = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_CleanLogs))
        mThreading_CleanLogs.Start()
    End Sub

    Private Sub StartIntegrityThread()
        mThreading_Integrity = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Integrity))
        mThreading_Integrity.Start()
    End Sub

    Private Sub StartVersionInventoryThread()
        mThreadingVersionInventory = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_VersionInventory))
        mThreadingVersionInventory.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try

        Try
            mThreading_CleanLogs.Abort()
        Catch ex As Exception

        End Try

        Try
            mThreading_Integrity.Abort()
        Catch ex As Exception

        End Try

        'Try
        '    mThreadingVersionInventory.Abort()
        'Catch ex As Exception

        'End Try
    End Sub

    Private Sub LoadSettings()
        LPCGlobals.Settings.Load()
        Helpers.Logger.Debugging = LPCGlobals.Settings.Debug

        LPCState.UsernameToCheck = LPCConstants.UsersAndPasswords.USERNAME__SiteKiosk

        If LPCGlobals.Settings.Debug Then
            Helpers.Logger.Write("loading settings", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsCheckFoRemoteConnection & ": " & LPCGlobals.Settings.CheckFoRemoteConnection.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDebug & ": " & LPCGlobals.Settings.Debug.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write("Disabled: " & LPCGlobals.Settings.Disabled.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsLogMeInThreshold & ": " & LPCGlobals.Settings.LogMeInThreshold.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsPlaysound & ": " & LPCGlobals.Settings.PlayStupidSound.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsSkipFirstRunDelay & ": " & LPCGlobals.Settings.SkipFirstRunDelay.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTestMode & ": " & LPCGlobals.Settings.TestMode.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsConsecutiveRebootsMax & ": " & LPCGlobals.Settings.ConsecutiveRebootsMax.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        End If

        If LPCGlobals.Settings.ConsecutiveRebootsMax < 0 Then
            LPCGlobals.Settings.ConsecutiveRebootsMax = LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogSettingsConsecutiveRebootsMax

            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsConsecutiveRebootsMax & ": " & LPCGlobals.Settings.ConsecutiveRebootsMax.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        End If
    End Sub

    Private Sub Thread_Main()
        'Wait for a while
        Helpers.Logger.Write("first run delay", , 0)
        If LPCGlobals.Settings.SkipFirstRunDelay Then
            Helpers.Logger.Write("skipped", , 1)
        Else
            Helpers.Logger.Write(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__FirstRun & " sec", , 1)
            Threading.Thread.Sleep(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__FirstRun * 1000)
        End If

        'main loop
        Do While Not LPCGlobals.Service.LoopAbort
            LoadSettings()

            If LPCGlobals.Settings.Muzzled Then
                Helpers.Logger.Write("Watchdog is muzzled!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
            End If

            If LPCGlobals.Settings.Disabled Or LPCGlobals.Settings.DisabledUntilFirstStart Then
                If LPCGlobals.Settings.Disabled Then
                    Helpers.Logger.Write("Watchdog is disabled!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
                End If

                If LPCGlobals.Settings.DisabledUntilFirstStart Then
                    Helpers.Logger.Write("Watchdog is disabled until software is started!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
                End If
            Else
                Helpers.Logger.Write("checking state", , 0)
                If LPCState.DetermineAction() Then
                    'Got the LPC state
                    mFailCount = 0

                    If LPCGlobals.Settings.Debug Then
                        Helpers.Logger.Write("previous", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                        Helpers.Logger.Write(LPCState.PreviousState.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                        LPCState.ShowPreviousLPCStateFlags()

                        Helpers.Logger.Write("current", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                        Helpers.Logger.Write(LPCState.CurrentState.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                        LPCState.ShowCurrentLPCStateFlags()
                    Else
                        Helpers.Logger.Write(LPCState.PreviousState & " => " & LPCState.CurrentState, , 1)
                    End If


                    If LPCState.Action.STOP_SERVICE Then
                        Helpers.Logger.Write("Quitting, something is really wrong!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)
                        LPCGlobals.Service.LoopAbort = True
                        Exit Do
                    End If

                    If LPCState.Action.FAILURE Then
                        'General failure somewhere. Let´s try again next time.
                        Helpers.Logger.Write("General failure", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)
                    Else
                        If LPCState.CurrentState = LPCState.PreviousState Then
                            'Ok, action time
                            Helpers.Logger.Write("state is same as before, action time", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
                            If LPCState.CurrentState And LPCState.LOBBYPC_STATE.WATCHDOG_WARNING_RUNNING Then
                                'Warning is running, should not take so long. Reboot!
                                Helpers.Logger.Write("Forcing a restart, Warning is still running!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)

                                LPCState.ForceRestart()
                            End If

                            'The actual action stuff
                            If LPCState.Action.IGNORED Then
                                'user activity, ignore action
                                Helpers.Logger.Write("temporarily ignoring actions", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)

                                If LPCState.Action.REBOOT Then
                                    Helpers.Logger.Write("ignoring REBOOT", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                                If LPCState.Action.RESTART Then
                                    Helpers.Logger.Write("ignoring RESTART", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                                If LPCState.Action.SHOW_WARNING Then
                                    Helpers.Logger.Write("ignoring SHOW_WARNING", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                                End If
                            ElseIf LPCState.Action.KILLSITEKIOSK Then
                                If LPCGlobals.Settings.Muzzled Then
                                    Helpers.Logger.Write("kill sitekiosk SKIPPED, muzzled!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
                                Else
                                    Helpers.Logger.Write("killing sitekiosk, hopefully it comes back on its own", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)

                                    Dim iKilled As Integer = 0

                                    iKilled = Helpers.SiteKiosk.Application.Kill()

                                    If iKilled = 1 Then
                                        Helpers.Logger.Write("killed 1 instance", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
                                    Else
                                        Helpers.Logger.Write("killed " & iKilled & " instances", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
                                    End If
                                End If
                            ElseIf LPCState.Action.REBOOT Then
                                If LPCGlobals.Settings.Muzzled Then
                                    Helpers.Logger.Write("forced reboot SKIPPED, muzzled!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
                                Else
                                    If LPCState.Action.REBOOT_LOOP Then
                                        Helpers.Logger.Write("reboot cancelled, reboot loop!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                                    Else
                                        'rebooting
                                        Helpers.Logger.Write("forced reboot", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)

                                        RebootWindows()
                                    End If
                                End If

                                Exit Do
                            ElseIf LPCState.Action.RESTART Then
                                If LPCGlobals.Settings.Muzzled Then
                                    Helpers.Logger.Write("restarting in secure mode SKIPPED, muzzled!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
                                Else
                                    'restarting
                                    Helpers.Logger.Write("restarting in secure mode", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
                                    SetSiteKioskAutoStart()

                                    'Don't care if auto start was succesful, we are going to reboot
                                    RebootWindows()
                                End If

                                Exit Do
                            ElseIf LPCState.Action.SHOW_WARNING Then
                                If LPCGlobals.Settings.Muzzled Then
                                    Helpers.Logger.Write("triggering warning SKIPPED, muzzled!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
                                Else
                                    'show warning
                                    Helpers.Logger.Write("triggering warning!", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)

                                    If LPCGlobals.Counters.WarningPopupTrigger < LPCConstants.TimeoutsAndDelaysAndCounters.COUNTERS__WarningPopupTriggerCountMax Then
                                        LPCGlobals.Counters.WarningPopupTrigger = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount)
                                        LPCGlobals.Counters.WarningPopupTrigger += 1

                                        'set value in registry so trigger can start the warning
                                        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount, LPCGlobals.Counters.WarningPopupTrigger)
                                        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTestmode, LPCGlobals.Settings.TestMode)
                                        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupLoose, LPCState.Action.WARNING_LOOSE)

                                        Helpers.Logger.Write("trigger (" & LPCGlobals.Counters.WarningPopupTrigger & "/" & LPCConstants.TimeoutsAndDelaysAndCounters.COUNTERS__WarningPopupTriggerCountMax & ")", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                                    Else
                                        Helpers.Logger.Write("Warning trigger failed, forced reboot!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)

                                        RebootWindows()
                                    End If
                                End If
                            End If
                        Else
                            If LPCState.Action.OK Then
                                Helpers.Logger.Write("all is ok again", , 1)
                                Helpers.Logger.Write("resetting RebootCount to 0", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)
                                Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Reboot, LPCConstants.RegistryKeys.VALUE__RebootCount, 0)
                            End If
                        End If
                    End If

                    'Write state to registry
                    Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__LastState, LPCState.CurrentState)
                Else
                    'Failed to get LPC state
                    'Let's count fails, and reboot after a few
                    mFailCount += 1

                    Helpers.Logger.Write("failed to get LPC state! (" & mFailCount & "/" & LPCConstants.TimeoutsAndDelaysAndCounters.COUNT__Maxfails & ")", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)

                    If mFailCount >= LPCConstants.TimeoutsAndDelaysAndCounters.COUNT__Maxfails Then
                        If LPCGlobals.Settings.Muzzled Then
                            Helpers.Logger.Write("failed too many times, rebooting SKIPPED, muzzled!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
                        Else
                            Helpers.Logger.Write("failed too many times, rebooting", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)

                            RebootWindows()
                        End If

                        Exit Do
                    End If
                End If
            End If

            Threading.Thread.Sleep(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__ServiceLoop * 1000)
        Loop
    End Sub

    Private Sub Thread_CleanLogs()
        Helpers.Logger.Write("starting log cleaning thread", , 0)

        If LPCGlobals.Settings.ZipOldLogs Then
            Helpers.Logger.Write("compressing old logs in '" & Helpers.Logger.LogFileBasePath & "'", , 1)

            Dim zipNum As Integer = 0, ReturnMessage As String = ""


            zipNum = LoggerZipper.ZipOldLogs("log", Helpers.Logger.LogFileBasePath, True, LPCGlobals.Settings.ZipOldLogsThreshold, ReturnMessage)

            If zipNum >= 0 Then
                If zipNum = 1 Then
                    Helpers.Logger.Write("1 file zipped", , 2)
                Else
                    Helpers.Logger.Write(zipNum.ToString & " files zipped", , 2)
                End If
            Else
                Helpers.Logger.Write("something went wrong", , 2)
            End If

            If Not ReturnMessage.Equals("") Then
                Helpers.Logger.Write("LoggerZipper says:", , 2)
                Helpers.Logger.Write(ReturnMessage, , 3)
            End If
        Else
            Helpers.Logger.Write("not enabled, ignored", , 1)
        End If


        Helpers.Logger.Write("ending log cleaning thread", , 0)
    End Sub

    Private Sub Thread_Integrity()
        Helpers.Logger.Write("starting integrity thread", , 0)
        If Not LPCGlobals.Settings.CheckIntegrity Then
            Helpers.Logger.Write("disabled, so ignored", , 1)

            Exit Sub
        End If

        Check_Integrity_Main()
    End Sub

    Private Sub Thread_VersionInventory()
        Helpers.Logger.Write("starting VersionInventory thread", , 0)
        If Not LPCGlobals.Settings.VersionInventory Then
            Helpers.Logger.Write("disabled, so ignored", , 1)

            Exit Sub
        End If

        Version_Inventory_Main()
    End Sub

    Private Sub SetSiteKioskAutoStart()
        If LPCGlobals.Settings.TestMode Then
            Helpers.Logger.Write("Forcing auto start! (skipped, test mode)", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)
        Else
            Helpers.Logger.Write("Forcing auto start!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 0)

            SiteKioskController.ResetError()
            SiteKioskController.ReadRegistryValues()

            Helpers.Logger.Write("updating startup xml file", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            If SiteKioskController.UpdateAutoStartupFile() Then
                Helpers.Logger.Write("set startup", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                If SiteKioskController.SetAutoStartup() Then
                    Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                Else
                    Helpers.Logger.Write("Something went wrong", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    Helpers.Logger.Write(SiteKioskController.SiteKiosk_Error.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End If
            Else
                Helpers.Logger.Write("Something went wrong", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                Helpers.Logger.Write(SiteKioskController.SiteKiosk_Error.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        End If
    End Sub

    Private Sub RebootWindows()
        'Increment boot counter
        Dim iRebootCountCurrent As Integer = 0

        iRebootCountCurrent = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Reboot, LPCConstants.RegistryKeys.VALUE__RebootCount, LPCConstants.RegistryKeys.DEFAULTVALUE__RebootCount)
        If iRebootCountCurrent < 0 Then
            iRebootCountCurrent = 0
        End If
        iRebootCountCurrent += 1
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Reboot, LPCConstants.RegistryKeys.VALUE__RebootCount, iRebootCountCurrent)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Reboot, LPCConstants.RegistryKeys.VALUE__RebootLast, Helpers.TimeDate.ToUnix())


        If LPCGlobals.Settings.TestMode Then
            Helpers.Logger.Write("I would have rebooted here, but we´re in test mode...", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        Else
            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        End If
    End Sub

    Private Sub ResetRegistryValues()
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerStarted, False)

        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTriggerCount, 0)
        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailure, False)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupFailureLastError, "")
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupCountdown, 0)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Warning, LPCConstants.RegistryKeys.VALUE__WarningPopupTimestamp, 0)

        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Title, LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Title)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Description, LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Description)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Severity, LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Severity)
    End Sub
End Class
