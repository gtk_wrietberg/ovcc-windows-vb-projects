﻿Module OVCCWatchdog_service__VersionInventory
    Public Sub Version_Inventory_Main()
        Helpers.Logger.WriteMessage("(version) starting", 0)

        VersionInventory.Initialise()

        Helpers.Logger.WriteMessage("(version) settings", 1)
        Helpers.Logger.WriteMessage("(version) ApplianceConfigXmlFile", 2)
        Helpers.Logger.WriteMessage("(version) " & VersionInventory.N_Central.Data.ApplianceConfigXmlFile, 3)
        Helpers.Logger.WriteMessage("(version) ApplianceId", 2)
        Helpers.Logger.WriteMessage("(version) " & VersionInventory.N_Central.Data.ApplianceId, 3)
        Helpers.Logger.WriteMessage("(version) SoapUri", 2)
        Helpers.Logger.WriteMessage("(version) " & VersionInventory.N_Central.Data.SoapUri, 3)


        Helpers.Logger.WriteMessage("(version) getting device id", 1)
        If Not VersionInventory.N_Central.Soap.Action_deviceGet() Then
            Helpers.Logger.WriteError("(version) failed", 2)
            Helpers.Logger.WriteError("(version) " & VersionInventory.N_Central.Data.LastError, 3)

            Exit Sub
        End If

        Helpers.Logger.WriteMessage("(version) DeviceId", 2)
        Helpers.Logger.WriteMessage("(version) " & VersionInventory.N_Central.Data.DeviceId, 3)


        Helpers.Logger.WriteMessage("(version) getting property list", 1)
        If Not VersionInventory.N_Central.Soap.Action_devicePropertyList Then
            Helpers.Logger.WriteError("(version) failed", 2)
            Helpers.Logger.WriteError("(version) " & VersionInventory.N_Central.Data.LastError, 3)

            Exit Sub
        End If

        If VersionInventory.N_Central.Data.DevicePropertiesCount = 1 Then
            Helpers.Logger.WriteMessage("(version) found 1 property", 2)
        Else
            Helpers.Logger.WriteMessage("(version) found " & VersionInventory.N_Central.Data.DevicePropertiesCount & " properties", 2)
        End If

        For Each _prop As VersionInventory.N_Central.Data.DeviceProperties._DeviceProperty In VersionInventory.N_Central.Data.DeviceProperties.IterateAll
            Helpers.Logger.WriteMessage("(version) " & _prop.Label & "=" & _prop.Value, 3)
        Next


        Helpers.Logger.WriteMessage("(version) collecting data", 1)
        VersionInventory.DataCollection.Collect()

        Helpers.Logger.WriteMessage("(version) list of changed properties", 2)
        For Each _prop As VersionInventory.N_Central.Data.DeviceProperties._DeviceProperty In VersionInventory.N_Central.Data.DeviceProperties.IterateChanged
            Helpers.Logger.WriteMessage("(version) " & _prop.Label & "=" & _prop.LocalValue & " (" & _prop.Value & ")", 3)
        Next


        Helpers.Logger.WriteMessage("(version) sending data", 1)
        If Not VersionInventory.N_Central.Soap.Action_devicePropertyModify Then
            Helpers.Logger.WriteError("(version) failed", 2)
            Helpers.Logger.WriteError("(version) " & VersionInventory.N_Central.Data.LastError, 3)

            Exit Sub
        End If

        Helpers.Logger.WriteMessage("(version) done", 0)
    End Sub
End Module
