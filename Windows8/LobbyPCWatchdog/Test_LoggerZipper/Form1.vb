﻿Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)


        LPCGlobals.Settings.Load()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Thread_CleanLogs()
    End Sub


    Private Sub Thread_CleanLogs()
        Helpers.Logger.Write("starting log cleaning thread", , 0)

        If LPCGlobals.Settings.ZipOldLogs Then
            Helpers.Logger.Write("compressing old logs in '" & Helpers.Logger.LogFileBasePath & "'", , 0)

            Dim zipNum As Integer = 0, ReturnMessage As String = ""

            zipNum = LoggerZipper.ZipOldLogs("log", Helpers.Logger.LogFileBasePath, True, LPCGlobals.Settings.ZipOldLogsThreshold, ReturnMessage)

            If zipNum >= 0 Then
                If zipNum = 1 Then
                    Helpers.Logger.Write("1 file zipped", , 1)
                Else
                    Helpers.Logger.Write(zipNum.ToString & " files zipped", , 1)
                End If
            Else
                Helpers.Logger.Write("something went wrong", , 1)
            End If

            If Not ReturnMessage.Equals("") Then
                Helpers.Logger.Write("LoggerZipper says:", , 1)
                Helpers.Logger.Write(ReturnMessage, , 2)
            End If
        Else
            Helpers.Logger.Write("not enabled in settings", , 1)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        'LPCGlobals.Settings.Save()
    End Sub
End Class
