﻿Module modMain
    Private timerErrorCheck As New System.Timers.Timer
    Private mThreading_ErrorCheck As Threading.Thread

    Public Sub Main()
        If Helpers.Processes.IsAppAlreadyRunning Then
            Application.Exit()
        End If

        mThreading_ErrorCheck = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_ErrorCheck))
        mThreading_ErrorCheck.Start()
    End Sub

    Private Sub Thread_ErrorCheck()
        timerErrorCheck.AutoReset = True
        timerErrorCheck.Interval = 1000
        AddHandler timerErrorCheck.Elapsed, AddressOf timerErrorCheck_Elapsed
        timerErrorCheck.Start()

        Threading.Thread.Sleep(Threading.Timeout.Infinite)
    End Sub

    Private Sub timerErrorCheck_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
        Dim sTimestamp As String = "", sTitle As String = "", sDescription As String = "", iSeverity As Integer = 0

        sTimestamp = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Timestamp)
        sTitle = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Title)
        sDescription = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Description)
        iSeverity = Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Severity)

        'check if error is set
        If Not sTitle.Equals(LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Title) Then
            If Not ErrorHelper.FormIsShowing Then
                'clear error, to prevent double stuff
                StoreErrorHistory()

                Dim frm As New frmMain_error
                frm.ErrorTimestamp = sTimestamp
                frm.ErrorTitle = sTitle
                frm.ErrorDescription = sDescription
                frm.ErrorSeverity = iSeverity

                If Not IsFormOpen(frm) Then
                    ErrorHelper.FormIsShowing = True
                    frm.Show()
                    Application.Run(frm)
                End If
            End If
        End If
    End Sub

    Private Sub StoreErrorHistory()
        Dim sTmp As String = ""

        'shift history one step down
        For i As Integer = LPCConstants.RegistryKeys.VALUE__WatchdogError_History.Count - 1 To 1 Step -1
            sTmp = Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_ErrorHistory, LPCConstants.RegistryKeys.VALUE__WatchdogError_History(i - 1))

            Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_ErrorHistory, LPCConstants.RegistryKeys.VALUE__WatchdogError_History(i), sTmp)
        Next

        'store latest error
        sTmp = sTmp & Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Timestamp)
        sTmp = sTmp & "|"
        sTmp = sTmp & Helpers.Registry.GetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Severity).ToString
        sTmp = sTmp & "|"
        sTmp = sTmp & Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Title)
        sTmp = sTmp & "|"
        sTmp = sTmp & Helpers.Registry.GetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Description)

        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_ErrorHistory, LPCConstants.RegistryKeys.VALUE__WatchdogError_History(0), sTmp)

        'clean up error
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Timestamp, LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Timestamp)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Title, LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Title)
        Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Description, LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Description)
        Helpers.Registry.SetValue_Integer(LPCConstants.RegistryKeys.KEY__Watchdog_Error, LPCConstants.RegistryKeys.VALUE__WatchdogError_Severity, LPCConstants.RegistryKeys.DEFAULTVALUE__WatchdogError_Severity)
    End Sub

    Private Function IsFormOpen(ByVal frm As Form) As Boolean
        If Application.OpenForms.OfType(Of Form).Contains(frm) Then
            Return True
        Else
            Return False
        End If
    End Function
End Module
