﻿Public Class frmMain_error
    Private mTimestamp As String
    Public Property ErrorTimestamp() As String
        Get
            Return mTimestamp
        End Get
        Set(ByVal value As String)
            mTimestamp = value
        End Set
    End Property

    Private mTitle As String
    Public Property ErrorTitle() As String
        Get
            Return mTitle
        End Get
        Set(ByVal value As String)
            mTitle = value
        End Set
    End Property

    Private mDescription As String
    Public Property ErrorDescription() As String
        Get
            Return mDescription
        End Get
        Set(ByVal value As String)
            mDescription = value
        End Set
    End Property

    Private mSeverity As ErrorTrigger.ERROR_SEVERITY
    Public Property ErrorSeverity() As ErrorTrigger.ERROR_SEVERITY
        Get
            Return mSeverity
        End Get
        Set(ByVal value As ErrorTrigger.ERROR_SEVERITY)
            mSeverity = value
        End Set
    End Property

    Private mRebootOption As Boolean
    Public Property RebootOption() As Boolean
        Get
            Return mRebootOption
        End Get
        Set(ByVal value As Boolean)
            mRebootOption = value
        End Set
    End Property


    Private ReadOnly mAutoCloseDelay As Integer = 30
    Private mAutoCloseCount As Integer = -1


    Private Sub PopulateForm()
        lblTitle.Text = mTitle
        lblDescription.Text = mDescription
        lblTimestamp.Text = mTimestamp

        Select Case mSeverity
            Case ErrorTrigger.ERROR_SEVERITY.WARNING
                lblSeverity.Text = "warning"
            Case ErrorTrigger.ERROR_SEVERITY.NORMAL_ERROR
                lblSeverity.Text = "error"
            Case ErrorTrigger.ERROR_SEVERITY.FATAL_ERROR
                lblSeverity.Text = "fatal error"
            Case Else
                lblSeverity.Text = "huh?"
        End Select

        btnReboot.Enabled = mRebootOption
        btnReboot.Visible = mRebootOption
    End Sub

    Private Sub UpdateAutoCloseTimer()
        If mAutoCloseCount < 0 Then
            lblCountdown.Text = ""
        Else
            If mAutoCloseCount = 1 Then
                lblCountdown.Text = "Auto-closing in 1 second"
            Else
                lblCountdown.Text = "Auto-closing in " & mAutoCloseCount & " seconds"
            End If
        End If
    End Sub

    Private Sub ParseDescription()
        mDescription = mDescription.Replace("\n", vbCrLf)
    End Sub

    Private Sub CloseForm()
        Me.Close()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        CloseForm()
    End Sub

    Private Sub frmMain_error_Activated(sender As Object, e As EventArgs) Handles Me.Activated
        tmrAutoClose.Enabled = True

        Helpers.Forms.TopMost(Me.Handle, True)
    End Sub

    Private Sub frmMain_error_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ErrorHelper.FormIsShowing = False
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Forms.TopMost(Me.Handle, True)

        Me.TransparencyKey = Color.Lime

        mAutoCloseCount = mAutoCloseDelay

        ParseDescription()
        PopulateForm()
        UpdateAutoCloseTimer()
    End Sub

    Private Sub btnReboot_Click(sender As Object, e As EventArgs) Handles btnReboot.Click
        If Not Helpers.Registry.GetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog_Settings, LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTestMode) Then
            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        End If
    End Sub

    Private Sub tmrAutoClose_Tick(sender As Object, e As EventArgs) Handles tmrAutoClose.Tick
        mAutoCloseCount -= 1

        If mAutoCloseCount <= 0 Then
            CloseForm()
        Else
            UpdateAutoCloseTimer()
        End If
    End Sub
End Class
