﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain_error
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain_error))
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.lblSeverity = New System.Windows.Forms.Label()
        Me.lblCountdown = New System.Windows.Forms.Label()
        Me.pnlLabels = New System.Windows.Forms.Panel()
        Me.lblTimestamp = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnReboot = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlBG = New System.Windows.Forms.Panel()
        Me.tmrAutoClose = New System.Windows.Forms.Timer(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.pnlLabels.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBG.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.lblSeverity)
        Me.pnlMain.Controls.Add(Me.lblCountdown)
        Me.pnlMain.Controls.Add(Me.pnlLabels)
        Me.pnlMain.Controls.Add(Me.btnClose)
        Me.pnlMain.Controls.Add(Me.btnReboot)
        Me.pnlMain.Controls.Add(Me.Label2)
        Me.pnlMain.Controls.Add(Me.PictureBox2)
        Me.pnlMain.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMain.ForeColor = System.Drawing.Color.Black
        Me.pnlMain.Location = New System.Drawing.Point(3, 3)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(693, 343)
        Me.pnlMain.TabIndex = 10
        '
        'lblSeverity
        '
        Me.lblSeverity.BackColor = System.Drawing.Color.Transparent
        Me.lblSeverity.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeverity.ForeColor = System.Drawing.Color.Red
        Me.lblSeverity.Location = New System.Drawing.Point(236, 39)
        Me.lblSeverity.Name = "lblSeverity"
        Me.lblSeverity.Size = New System.Drawing.Size(447, 21)
        Me.lblSeverity.TabIndex = 4
        Me.lblSeverity.Text = "%% error severity %%"
        Me.lblSeverity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCountdown
        '
        Me.lblCountdown.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountdown.ForeColor = System.Drawing.Color.DarkRed
        Me.lblCountdown.Location = New System.Drawing.Point(543, 53)
        Me.lblCountdown.Name = "lblCountdown"
        Me.lblCountdown.Size = New System.Drawing.Size(140, 17)
        Me.lblCountdown.TabIndex = 13
        Me.lblCountdown.Text = "Auto-closing in xxx seconds"
        Me.lblCountdown.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'pnlLabels
        '
        Me.pnlLabels.BackColor = System.Drawing.Color.WhiteSmoke
        Me.pnlLabels.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLabels.Controls.Add(Me.lblTimestamp)
        Me.pnlLabels.Controls.Add(Me.lblTitle)
        Me.pnlLabels.Controls.Add(Me.lblDescription)
        Me.pnlLabels.Location = New System.Drawing.Point(8, 73)
        Me.pnlLabels.Name = "pnlLabels"
        Me.pnlLabels.Size = New System.Drawing.Size(675, 210)
        Me.pnlLabels.TabIndex = 12
        '
        'lblTimestamp
        '
        Me.lblTimestamp.Font = New System.Drawing.Font("Arial", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTimestamp.ForeColor = System.Drawing.Color.DarkRed
        Me.lblTimestamp.Location = New System.Drawing.Point(501, 41)
        Me.lblTimestamp.Name = "lblTimestamp"
        Me.lblTimestamp.Size = New System.Drawing.Size(169, 17)
        Me.lblTimestamp.TabIndex = 14
        Me.lblTimestamp.Text = "%% error timestamp %%"
        Me.lblTimestamp.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.DarkRed
        Me.lblTitle.Location = New System.Drawing.Point(3, 0)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(671, 58)
        Me.lblTitle.TabIndex = 3
        Me.lblTitle.Text = "%% error title %%"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.ForeColor = System.Drawing.Color.Black
        Me.lblDescription.Location = New System.Drawing.Point(0, 73)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(674, 135)
        Me.lblDescription.TabIndex = 5
        Me.lblDescription.Text = "%% error description %%"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnClose.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(437, 292)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(246, 40)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'btnReboot
        '
        Me.btnReboot.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnReboot.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReboot.ForeColor = System.Drawing.Color.Maroon
        Me.btnReboot.Location = New System.Drawing.Point(8, 292)
        Me.btnReboot.Name = "btnReboot"
        Me.btnReboot.Size = New System.Drawing.Size(246, 40)
        Me.btnReboot.TabIndex = 6
        Me.btnReboot.Text = "Reboot computer"
        Me.btnReboot.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(236, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(452, 36)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "OVCC Watchdog"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.BackgroundImage = Global.OVCCWatchdog_error.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Location = New System.Drawing.Point(8, -1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(222, 68)
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'pnlBG
        '
        Me.pnlBG.BackColor = System.Drawing.Color.Red
        Me.pnlBG.Controls.Add(Me.pnlMain)
        Me.pnlBG.Location = New System.Drawing.Point(12, 12)
        Me.pnlBG.Name = "pnlBG"
        Me.pnlBG.Size = New System.Drawing.Size(699, 349)
        Me.pnlBG.TabIndex = 11
        '
        'tmrAutoClose
        '
        Me.tmrAutoClose.Interval = 1000
        '
        'frmMain_error
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lime
        Me.ClientSize = New System.Drawing.Size(723, 373)
        Me.Controls.Add(Me.pnlBG)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain_error"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LPCWatchdog Error"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlLabels.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBG.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents lblSeverity As System.Windows.Forms.Label
    Friend WithEvents pnlBG As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnReboot As System.Windows.Forms.Button
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents pnlLabels As System.Windows.Forms.Panel
    Friend WithEvents lblCountdown As System.Windows.Forms.Label
    Friend WithEvents tmrAutoClose As System.Windows.Forms.Timer
    Friend WithEvents lblTimestamp As System.Windows.Forms.Label

End Class
