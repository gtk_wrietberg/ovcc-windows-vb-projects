﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.OVCCWdEDServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.OVCCWdEDServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'OVCCWdEDServiceProcessInstaller
        '
        Me.OVCCWdEDServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.OVCCWdEDServiceProcessInstaller.Password = Nothing
        Me.OVCCWdEDServiceProcessInstaller.Username = Nothing
        '
        'OVCCWdEDServiceInstaller
        '
        Me.OVCCWdEDServiceInstaller.Description = "OVCCWatchdog_ED"
        Me.OVCCWdEDServiceInstaller.DisplayName = "OVCCWatchdog_ED"
        Me.OVCCWdEDServiceInstaller.ServiceName = "OVCCWatchdog_ED_service"
        Me.OVCCWdEDServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.OVCCWdEDServiceProcessInstaller, Me.OVCCWdEDServiceInstaller})

    End Sub

    Friend WithEvents OVCCWdEDServiceProcessInstaller As ServiceProcess.ServiceProcessInstaller
    Friend WithEvents OVCCWdEDServiceInstaller As ServiceProcess.ServiceInstaller
End Class
