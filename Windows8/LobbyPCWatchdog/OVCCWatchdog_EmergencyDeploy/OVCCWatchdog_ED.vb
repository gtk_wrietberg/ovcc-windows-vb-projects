﻿Public Class OVCCWatchdog_ED
    Private mThreading_Main As Threading.Thread

    Protected Overrides Sub OnStart(ByVal args() As String)
        'init logger
        Helpers.Logger.InitialiseLogger()

        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("service started", , 0)

        'load settings from registry
        LoadSettings()

        'Start the main thread
        StartMainThread()
    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()

        Helpers.Logger.Write("service stopped")
    End Sub


    Private Sub LoadSettings()
        LPCGlobals.Settings.Load()
        Helpers.Logger.Debugging = LPCGlobals.Settings.Debug

        If LPCGlobals.Settings.Debug Then
            Helpers.Logger.Write("loading settings", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 0)

            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDebug & ": " & LPCGlobals.Settings.Debug.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write("Disabled: " & LPCGlobals.Settings.Disabled.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsSkipFirstRunDelay & ": " & LPCGlobals.Settings.SkipFirstRunDelay.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
            Helpers.Logger.Write(LPCConstants.RegistryKeys.VALUE__WatchdogSettingsTestMode & ": " & LPCGlobals.Settings.TestMode.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        End If
    End Sub


    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Thread_Main()
        Helpers.Logger.Write("not operational yet...", , 0)

        ''Wait for a while
        'Helpers.Logger.Write("first run delay", , 0)
        'If LPCGlobals.Settings.SkipFirstRunDelay Then
        '    Helpers.Logger.Write("skipped", , 1)
        'Else
        '    Helpers.Logger.Write(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__FirstRun & " sec", , 1)
        '    Threading.Thread.Sleep(LPCConstants.TimeoutsAndDelaysAndCounters.DELAY__FirstRun * 1000)
        'End If
    End Sub

End Class
