﻿Public Class frmMain
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)

        ToggleButtons(False)


        '--------------------
        'Check if SK is even there
        SiteKioskController.ResetError()
        SiteKioskController.ReadRegistryValues()

        If Not Helpers.SiteKiosk.IsInstalled Then
            MessageBox.Show("Could not find SiteKiosk! Check if the OVCC software is installed!", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            Me.Close()
            Exit Sub
        End If


        '------------------------------------------
        'we need to check for applications, such as office
        oSkCfg = New SkCfg
        oApplications = New Applications


        Helpers.Logger.Write("loading apps")


        Helpers.Logger.Write(".doc", , 1)
        oApplications.AddApplication(".doc", "Office Word")
        Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)

        Helpers.Logger.Write(".xls", , 1)
        oApplications.AddApplication(".xls", "Office Excel")
        Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)

        Helpers.Logger.Write(".ppt", , 1)
        oApplications.AddApplication(".ppt", "Office Powerpoint")
        Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)

        Helpers.Logger.Write(".pdf", , 1)
        oApplications.AddApplication(".pdf", "PDF Reader")
        Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)

        Helpers.Logger.Write("Skype", , 1)
        oApplications.AddApplication_Skype()
        Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)

        Helpers.Logger.Write("Pinta", , 1)
        oApplications.AddApplication_Pinta()
        Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)

        'Helpers.Logger.Write("Windows Media Player", , 1)
        'Helpers.Logger.Write("skipped, doesn't work in SK", , 2)
        ''oApplications.AddApplication_MediaPlayer()
        ''Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)

        'Helpers.Logger.Write("Windows Photo Gallery", , 1)
        'Helpers.Logger.Write("skipped, no longer supported", , 2)
        ''oApplications.AddApplication_PhotoGallery()
        ''Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)

        'Helpers.Logger.Write("Windows Movie Maker", , 1)
        'Helpers.Logger.Write("skipped, no longer supported", , 2)
        ''oApplications.AddApplication_MovieMaker()
        ''Helpers.Logger.Write(oApplications.GetLastApplicationAsString(), , 2)


        ToggleButtons(False)
        Helpers.Forms.TopMost(Me.Handle, True)


        tmrNewApps.Enabled = True
    End Sub


    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Helpers.Logger.WriteError("canceled", 0)

        Me.Close()
    End Sub

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
        ToggleButtons(False)

        Helpers.Logger.Write("Enable '" & LPCConstants.Service.Watchdog.Name & "'", , 0)
        UnleashTheHound()

        If SetSiteKioskAutoStart() Then
            Helpers.Logger.WriteWarning("Rebooting", 0)

            WindowsController.ExitWindows(RestartOptions.Reboot, True)
        Else
            MsgBox("Something went wrong (error code #" & SiteKioskController.SiteKiosk_Error & "). Please try again, or reboot manually", MsgBoxStyle.Critical Or MsgBoxStyle.OkOnly, Application.ProductName)

            ToggleButtons(True)
        End If
    End Sub

    Private Function UnleashTheHound() As Boolean
        Helpers.Logger.Write("removing muzzle", , 1)

        Helpers.Registry.SetValue_Boolean(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__DisabledUntilOVCCStart, False)
        Helpers.Registry.RemoveValue(LPCConstants.RegistryKeys.KEY__Watchdog, LPCConstants.RegistryKeys.VALUE__DisabledUntilOVCCStart)

        Return True
    End Function

    Private Function SetSiteKioskAutoStart() As Boolean
        Helpers.Logger.Write("Setting auto start!", , 0)

        SiteKioskController.ResetError()
        SiteKioskController.ReadRegistryValues()

        Helpers.Logger.Write("updating startup xml file", , 1)
        If SiteKioskController.UpdateAutoStartupFile() Then
            Helpers.Logger.Write("set startup", , 1)
            If SiteKioskController.SetAutoStartup() Then
                Helpers.Logger.Write("ok", , 2)
            Else
                Helpers.Logger.WriteError("Something went wrong", 1)
                Helpers.Logger.WriteError(SiteKioskController.SiteKiosk_Error.ToString, 2)

                Return False
            End If
        Else
            Helpers.Logger.WriteError("Something went wrong", 1)
            Helpers.Logger.WriteError(SiteKioskController.SiteKiosk_Error.ToString, 2)

            Return False
        End If

        Return True
    End Function

    Private Sub ToggleButtons(state As Boolean)
        btnStart.Enabled = state
        btnCancel.Enabled = state
    End Sub

    Private Sub _CheckForNewApps()
        Helpers.Logger.Write("doing SkCfg work")

        Dim ReturnMessage As String = ""
        Dim sMsgBoxText As String = ""

        If oSkCfg.Check(ReturnMessage) Then
            'Found new apps!
            For i As Integer = frmApps.Controls.Count - 1 To 0 Step -1
                If TypeOf frmApps.Controls.Item(i) Is Label Then
                    If frmApps.Controls.Item(i).Name.StartsWith("lbl_dyn_") Then
                        frmApps.Controls.RemoveAt(i)
                    End If
                End If
            Next

            Dim _counter As Integer = -1
            For Each _app As Applications._Application In oApplications.GetApplications
                _counter += 1

                Dim _lbl As New Label

                frmApps.Controls.Add(_lbl)
                _lbl.Name = "lbl_dyn_" & _counter.ToString
                _lbl.Text = _app.Name

                _lbl.TextAlign = ContentAlignment.MiddleCenter
                _lbl.Location = New Point(frmApps.lblAppsFound.Location.X, frmApps.lblAppsFound.Location.Y + frmApps.lblAppsFound.Height + 20 + (_counter * (frmApps.lblAppsFound.Height + 10)))
                _lbl.Size = frmApps.lblAppsFound.Size
            Next

            frmApps.btnYes.Top = frmApps.lblAppsFound.Location.Y + frmApps.lblAppsFound.Height + 20 + ((_counter + 1) * (frmApps.lblAppsFound.Height + 10) + 20)
            frmApps.btnNo.Top = frmApps.btnYes.Top


            Dim _size As New Size

            _size.Height = frmApps.btnYes.Top + frmApps.btnYes.Height + 10
            _size.Width = frmApps.ClientSize.Width

            frmApps.ClientSize = _size

            frmApps.Moveable = False


            If frmApps.ShowDialog(Me) = DialogResult.Yes Then
                oSkCfg.Update()
            End If
        Else
            If ReturnMessage = "" Then
                'no error, just didn't find any
            Else
                'uh oh, error!
                MessageBox.Show("An error occurred while checking for apps: " & ReturnMessage, My.Application.Info.ProductName & " - ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If

        ToggleButtons(True)
    End Sub

    Private Sub tmrNewApps_Tick(sender As Object, e As EventArgs) Handles tmrNewApps.Tick
        tmrNewApps.Enabled = False

        _CheckForNewApps()
    End Sub
End Class
