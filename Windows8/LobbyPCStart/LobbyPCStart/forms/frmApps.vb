﻿Public Class frmApps
    Private Declare Function EnableMenuItem Lib "user32.dll" Alias "EnableMenuItem" (ByVal hMenu As IntPtr, ByVal uIDEnableItem As Int32, ByVal uEnable As Int32) As Int32

    Private _Moveable As Boolean = True
    Public Overridable Property Moveable() As Boolean
        Get
            Return _Moveable
        End Get
        Set(ByVal Value As Boolean)
            If _Moveable <> Value Then
                _Moveable = Value
            End If
        End Set
    End Property

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = &H117& Then
            'Handles popup of system menu.
            If m.LParam.ToInt32 \ 65536 <> 0 Then 'divide by 65536 to get hiword.
                Dim AbleFlags As Int32 = &H0&
                If Not _Moveable Then AbleFlags = &H2& Or &H1&
                EnableMenuItem(m.WParam, &HF010&, &H0& Or AbleFlags)
            End If
        End If

        If Not _Moveable Then
            'Cancels any attempt to drag the window by it's caption.
            If m.Msg = &HA1 Then If m.WParam.ToInt32 = &H2 Then Return
            'Redundant but cancels any clicks on the Move system menu item.
            If m.Msg = &H112 Then If (m.WParam.ToInt32 And &HFFF0) = &HF010& Then Return
        End If

        'Return control to base message handler.
        MyBase.WndProc(m)
    End Sub


    Private Sub frmApps_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click
        Me.DialogResult = DialogResult.Yes

        Me.Close()
    End Sub

    Private Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        Me.DialogResult = DialogResult.No

        Me.Close()
    End Sub

    Private Sub frmApps_Move(sender As Object, e As EventArgs) Handles Me.Move

    End Sub
End Class