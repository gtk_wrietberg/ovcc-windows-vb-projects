﻿Public Class Applications
    Private Shared ReadOnly KEY__Skype As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Skype\Phone"
    Private Shared ReadOnly VALUE__SkypePath As String = "SkypePath"

    Public Class _Application
        Private mIsOffice As Boolean
        Public Property IsOffice() As Boolean
            Get
                Return mIsOffice
            End Get
            Set(ByVal value As Boolean)
                mIsOffice = value
            End Set
        End Property

        Private mExtension As String
        Public Property Extension() As String
            Get
                Return mExtension
            End Get
            Set(ByVal value As String)
                mExtension = value
            End Set
        End Property

        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mExecutable As String
        Public Property Executable() As String
            Get
                Return mExecutable
            End Get
            Set(ByVal value As String)
                mExecutable = value
            End Set
        End Property

        Private mError As String
        Public Property _Error() As String
            Get
                Return mError
            End Get
            Set(ByVal value As String)
                mError = value
            End Set
        End Property

    End Class

    Private mAppList As List(Of _Application)


    Public Sub New()
        mAppList = New List(Of _Application)
    End Sub

    Public Sub AddApplication(extension As String, Optional name As String = "")
        If Not extension.StartsWith(".") Then
            extension = "." & extension
        End If

        Dim oTmp As New _Application
        Dim sError As String = ""
        Dim sTmp As String = ""

        oTmp.Extension = extension

        If name.Equals("") Then
            If GetApplicationForExtension.GetName(extension, sTmp) Then
                oTmp.Name = sTmp
            Else
                oTmp.Name = name
                sError &= "GetName: " & sTmp
            End If
        Else
            oTmp.Name = name
        End If

        sTmp = ""
        If GetApplicationForExtension.GetExecutable(extension, sTmp) Then
            sTmp = sTmp.Replace("\\", "\")
            oTmp.Executable = sTmp

            If sTmp.EndsWith("\OpenWith.exe") Then
                'This is useless, this is the "Open with ..." dialog. Let´s ignore...
                oTmp.Executable = ""
                sError &= "GetExecutable: " & sTmp
            End If

            If sTmp.Contains("Office") Then
                oTmp.IsOffice = True
            End If
        Else
            oTmp.Executable = ""
            sError &= "GetExecutable: " & sTmp
        End If

        oTmp._Error = sError

        mAppList.Add(oTmp)
    End Sub

    Public Function GetApplication(extension As String) As _Application
        For Each _app As _Application In mAppList
            If _app.Extension.Equals(extension) Then
                Return _app
            End If
        Next

        Return Nothing
    End Function

    Public Function GetLastApplicationAsString() As String
        Dim sTmp As String = "nothing"

        If mAppList.Count > 0 Then
            Dim _app As New _Application
            _app = mAppList.Item(mAppList.Count - 1)
            sTmp = _app.Name & " ; " & _app.Executable & " ; " & _app.IsOffice & " ; " & _app._Error
        End If

        Return sTmp
    End Function

    Public Function GetApplicationAsString(extension As String) As String
        Dim sTmp As String = "nothing"

        For Each _app As _Application In mAppList
            If _app.Extension.Equals(extension) Then
                sTmp = _app.Name & " ; " & _app.Executable & " ; " & _app.IsOffice & " ; " & _app._Error
            End If
        Next

        Return sTmp
    End Function

    Public Function DeleteApplication(executable As String) As Boolean
        Dim bRet As Boolean = False

        For i = mAppList.Count - 1 To 0 Step -1
            Dim _app As _Application = mAppList(i)

            If _app.Executable.ToLower.Equals(executable.ToLower) Then
                mAppList.RemoveAt(i)

                bRet = True
            End If
        Next

        Return bRet
    End Function

    Public Function Count(Optional WithCleanup As Boolean = False) As Integer
        If WithCleanup Then
            Dim _count As Integer = 0

            For Each _app As _Application In mAppList
                If Not _app.Executable.Equals("") Then
                    _count += 1
                End If
            Next

            Return _count
        Else
            Return mAppList.Count
        End If
    End Function

    'extra apps
    'skype , media player , etc
    Public Function AddApplication_Skype() As Boolean
        '   To check if Skype is installed, in regedit check if the following key exists: HKCUSoftwareSkypePhone, SkypePath . 
        '   This key points to the location of the skype.exe file. 
        '   If this key does not exist, check if the HKLMSoftwareSkypePhone, SkypePath key exists. 
        '   If the HKCU key does not exist but the HKLM key is present, 
        '   Skype has been installed from an administrator account but not been used from the current account.
        Dim bRet As Boolean = False
        Dim sSkypePath As String = ""
        Dim oTmp As New _Application

        oTmp.Extension = ""
        oTmp.Name = "Skype"
        oTmp._Error = ""
        oTmp.Executable = ""

        Try
            sSkypePath = Helpers.Registry.GetValue_String(KEY__Skype, VALUE__SkypePath, "")

            If sSkypePath.Equals("") Then
                sSkypePath = OVCCStartGlobals.Applications.SKYPE.Replace("%%PROGRAM FILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder())
            End If

            If Not IO.File.Exists(sSkypePath) Then
                Throw New Exception("Skype path does not exist: '" & sSkypePath & "'")
            End If

            oTmp.Executable = sSkypePath

            bRet = True
        Catch ex As Exception
            oTmp._Error = ex.Message
            bRet = False
        End Try

        mAppList.Add(oTmp)

        Return bRet
    End Function

    Public Function AddApplication_AdobePDF() As Boolean
        Dim bRet As Boolean = False


        Return bRet
    End Function

    Public Function AddApplication_MediaPlayer() As Boolean
        Dim bRet As Boolean = False
        Dim sMediaPlayerPath As String = ""
        Dim oTmp As New _Application

        oTmp.Extension = ""
        oTmp.Name = "Windows Media Player"
        oTmp._Error = ""
        oTmp.Executable = ""

        Try
            sMediaPlayerPath = LPCConstants.FilesAndFolders.FOLDER__ProgramFiles & "\Windows Media Player\wmplayer.exe"

            If Not IO.File.Exists(sMediaPlayerPath) Then
                Throw New Exception("Media Player path does not exist: '" & sMediaPlayerPath & "'")
            End If

            oTmp.Executable = sMediaPlayerPath

            bRet = True
        Catch ex As Exception
            oTmp._Error = ex.Message
            bRet = False
        End Try

        mAppList.Add(oTmp)

        Return bRet
    End Function

    Public Function AddApplication_PhotoGallery() As Boolean
        Dim bRet As Boolean = False
        Dim sPhotoGalleryPath As String = ""
        Dim oTmp As New _Application

        oTmp.Extension = ""
        oTmp.Name = "Photo Gallery"
        oTmp._Error = ""
        oTmp.Executable = ""

        Try
            sPhotoGalleryPath = LPCConstants.FilesAndFolders.FOLDER__ProgramFiles & "\Windows Live\Photo Gallery\WLXPhotoGallery.exe"

            If Not IO.File.Exists(sPhotoGalleryPath) Then
                Throw New Exception("Photo Gallery path does not exist: '" & sPhotoGalleryPath & "'")
            End If

            oTmp.Executable = sPhotoGalleryPath

            bRet = True
        Catch ex As Exception
            oTmp._Error = ex.Message
            bRet = False
        End Try

        mAppList.Add(oTmp)

        Return bRet
    End Function

    Public Function AddApplication_MovieMaker() As Boolean
        Dim bRet As Boolean = False
        Dim sMovieMakerPath As String = ""
        Dim oTmp As New _Application

        oTmp.Extension = ""
        oTmp.Name = "Movie Maker"
        oTmp._Error = ""
        oTmp.Executable = ""

        Try
            sMovieMakerPath = LPCConstants.FilesAndFolders.FOLDER__ProgramFiles & "\Windows Live\Photo Gallery\MovieMaker.exe"

            If Not IO.File.Exists(sMovieMakerPath) Then
                Throw New Exception("Movie Maker path does not exist: '" & sMovieMakerPath & "'")
            End If

            oTmp.Executable = sMovieMakerPath

            bRet = True
        Catch ex As Exception
            oTmp._Error = ex.Message
            bRet = False
        End Try

        mAppList.Add(oTmp)

        Return bRet
    End Function

    Public Function AddApplication_Pinta() As Boolean
        Dim bRet As Boolean = False
        Dim sPintaPath As String = ""
        Dim oTmp As New _Application

        oTmp.Extension = ""
        oTmp.Name = "Pinta"
        oTmp._Error = ""
        oTmp.Executable = ""

        Try
            sPintaPath = "C:\Program Files\Pinta\Pinta.exe"

            If Not IO.File.Exists(sPintaPath) Then
                sPintaPath = "C:\Program Files (x86)\Pinta\Pinta.exe"
            End If

            If Not IO.File.Exists(sPintaPath) Then
                Throw New Exception("Pinta path does not exist: '" & sPintaPath & "'")
            End If

            oTmp.Executable = sPintaPath

            bRet = True
        Catch ex As Exception
            oTmp._Error = ex.Message
            bRet = False
        End Try

        mAppList.Add(oTmp)

        Return bRet
    End Function


    Public Iterator Function GetApplications() As IEnumerable(Of _Application)
        Dim counter As Integer = -1

        If mAppList.Count > 0 Then
            While counter < mAppList.Count
                counter += 1

                If counter < mAppList.Count Then
                    Yield mAppList.Item(counter)
                End If
            End While
        End If
    End Function

    'Private mCount As Integer = 0
    'Public Function GetNext(ByRef _app As _Application) As Boolean
    '    If mCount < 0 Then
    '        Return False
    '    End If

    '    If mCount >= mAppList.Count Then
    '        Return False
    '    End If

    '    _app = mAppList.ElementAt(mCount)
    '    mCount += 1

    '    Return True
    'End Function

    'Public Sub ResetGetNext()
    '    mCount = 0
    'End Sub
End Class
