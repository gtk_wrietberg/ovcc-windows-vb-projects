Public Class SkCfg
    Private mSkCfgFile As String
    Private mSkBuild As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String
    Private mConfigChanged As Boolean

    Private ReadOnly c_REGROOT_SITEKIOSK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public ReadOnly Property SiteKioskFolder As String
        Get
            Return mSiteKioskFolder
        End Get
    End Property


    Public Function Check(ByRef ReturnMessage As String) As Boolean
        Try
            ReturnMessage = ""

            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim sTmpFilename As String = ""
            Dim sTmpDescription As String = ""


            mSkCfgXml = New Xml.XmlDocument

            Helpers.Logger.WriteRelative("loading", , 1)
            Helpers.Logger.WriteRelative(mSkCfgFile, , 2)
            mSkCfgXml.Load(mSkCfgFile)
            Helpers.Logger.WriteRelative("ok", , 3)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            Helpers.Logger.WriteRelative("root node", , 2)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                Helpers.Logger.WriteRelative("ok", , 3)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            Helpers.Logger.WriteRelative("find programs node", , 2)

            Dim mXmlNode_Programs As Xml.XmlNode
            Dim mXmlNode_Files As Xml.XmlNodeList
            Dim mXmlNode_File As Xml.XmlNode


            mXmlNode_Programs = mXmlNode_Root.SelectSingleNode("sk:programs", ns)
            mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)
            For Each mXmlNode_File In mXmlNode_Files
                sTmpFilename = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower
                sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("description").Value.ToLower
                If sTmpDescription.Equals("") Then
                    sTmpDescription = mXmlNode_File.Attributes.GetNamedItem("title").Value.ToLower
                End If

                Helpers.Logger.WriteRelative("found program", , 3)
                Helpers.Logger.WriteRelative(sTmpDescription, , 4)
                Helpers.Logger.WriteRelative(sTmpFilename, , 5)

                If Not oApplications.DeleteApplication(sTmpFilename) Then
                    Helpers.Logger.WriteWarningRelative("not found or not needed", 5)
                End If
            Next

            Helpers.Logger.WriteRelative("are there programs to add?", , 2)
            If oApplications.Count(True) > 0 Then
                Helpers.Logger.WriteRelative("yep", , 3)

                Helpers.Logger.WriteRelative("compiling app list for msgbox", , 3)
                For Each _app As Applications._Application In oApplications.GetApplications
                    Helpers.Logger.WriteRelative(_app.Extension & " -> " & _app.Name, , 3)

                    If _app.Executable.Equals("") Then
                        Helpers.Logger.WriteWarningRelative("no proper executable found!", 4)
                    Else
                        Helpers.Logger.WriteRelative("runs with " & _app.Executable, , 4)

                        ReturnMessage &= "*) " & _app.Name & vbCrLf
                    End If
                Next

                Return True
            Else
                Helpers.Logger.WriteRelative("nope", , 3)

                ReturnMessage = ""

                Return False
            End If
        Catch ex As Exception
            Helpers.Logger.WriteRelative("FAIL", , 2)
            Helpers.Logger.WriteRelative(ex.Message, , 3)

            ReturnMessage = "error: " & ex.Message

            Return False
        End Try

        Return False
    End Function

    Public Function Update() As Boolean
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim mSkCfgXml As Xml.XmlDocument
            Dim mXmlNode_Root As Xml.XmlNode
            Dim sTmp As String = ""


            mSkCfgXml = New Xml.XmlDocument

            Helpers.Logger.WriteRelative("loading", , 1)
            Helpers.Logger.WriteRelative(mSkCfgFile, , 2)
            mSkCfgXml.Load(mSkCfgFile)
            Helpers.Logger.WriteRelative("ok", , 3)

            nt = mSkCfgXml.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            Helpers.Logger.WriteRelative("root node", , 2)
            mXmlNode_Root = mSkCfgXml.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not mXmlNode_Root Is Nothing Then
                Helpers.Logger.WriteRelative("ok", , 3)
            Else
                Throw New Exception("XmlNodeRoot Is Nothing")
            End If


            'Create base node for program

            '      <file autostart="false" filename="C:\Windows\notepad.exe" description="description" title="title" screenshot-path="">
            '           <runas enabled="false">
            '               <username></username>
            '               <password></password>
            '               <domainname></domainname>
            '           </runas>
            '           <startup enabled="false">
            '               <password></password>
            '           </startup>
            '       </file>

            Dim mXmlNode_Template_Program__File As Xml.XmlNode
            Dim mXmlNode_Template_Program__Runas As Xml.XmlNode
            Dim mXmlNode_Template_Program__Startup As Xml.XmlNode
            Dim mXmlNode_Template_Program__tmp As Xml.XmlNode

            mXmlNode_Template_Program__File = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "file", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__Runas = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "runas", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__Startup = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "startup", ns.LookupNamespace("sk"))


            mXmlNode_Template_Program__tmp = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Startup.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__Startup.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("enabled"))
            mXmlNode_Template_Program__Startup.Attributes.GetNamedItem("enabled").Value = "false"

            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Startup)


            mXmlNode_Template_Program__tmp = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "username", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__tmp = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "password", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__tmp = mSkCfgXml.CreateNode(Xml.XmlNodeType.Element, "domainname", ns.LookupNamespace("sk"))
            mXmlNode_Template_Program__tmp.InnerText = ""
            mXmlNode_Template_Program__Runas.AppendChild(mXmlNode_Template_Program__tmp)

            mXmlNode_Template_Program__Runas.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("enabled"))
            mXmlNode_Template_Program__Runas.Attributes.GetNamedItem("enabled").Value = "false"

            mXmlNode_Template_Program__File.AppendChild(mXmlNode_Template_Program__Runas)


            mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("autostart"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("autostart").Value = "false"

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("filename"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("filename").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("description"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("description").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("title"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("title").Value = ""

            mXmlNode_Template_Program__File.Attributes.SetNamedItem(mSkCfgXml.CreateAttribute("screenshot-path"))
            mXmlNode_Template_Program__File.Attributes.GetNamedItem("screenshot-path").Value = ""


            Helpers.Logger.WriteRelative("find programs node", , 2)

            Dim mXmlNode_Programs As Xml.XmlNode
            Dim mXmlNode_Files As Xml.XmlNodeList
            Dim mXmlNode_File As Xml.XmlNode


            mXmlNode_Programs = mXmlNode_Root.SelectSingleNode("sk:programs", ns)
            mXmlNode_Files = mXmlNode_Programs.SelectNodes("sk:file", ns)
            For Each mXmlNode_File In mXmlNode_Files
                sTmp = mXmlNode_File.Attributes.GetNamedItem("filename").Value.ToLower

                Helpers.Logger.WriteRelative("found program", , 3)
                Helpers.Logger.WriteRelative(sTmp, , 4)

                If Not oApplications.DeleteApplication(sTmp) Then
                    Helpers.Logger.WriteWarningRelative("not found or not needed", 5)
                End If
            Next

            Helpers.Logger.WriteRelative("adding programs", , 2)
            If oApplications.Count(True) > 0 Then
                Dim mXmlNode_FileClone As Xml.XmlNode

                For Each _app As Applications._Application In oApplications.GetApplications
                    Helpers.Logger.WriteRelative(_app.Extension & " -> " & _app.Name, , 3)

                    If _app.Executable.Equals("") Then
                        Helpers.Logger.WriteWarningRelative("no proper executable found!", 4)
                    Else
                        Helpers.Logger.WriteRelative("runs with " & _app.Executable, , 4)

                        mXmlNode_FileClone = mXmlNode_Template_Program__File.CloneNode(True)
                        mXmlNode_FileClone.Attributes.GetNamedItem("filename").Value = _app.Executable
                        mXmlNode_FileClone.Attributes.GetNamedItem("title").Value = _app.Name
                        mXmlNode_FileClone.Attributes.GetNamedItem("description").Value = _app.Name

                        mXmlNode_Programs.AppendChild(mXmlNode_FileClone)

                        mConfigChanged = True
                    End If
                Next
            Else
                Helpers.Logger.WriteWarningRelative("nothing to add", 3)
            End If


            If mConfigChanged Then
                Helpers.Logger.WriteRelative("saving", , 1)
                Helpers.Logger.WriteRelative(mSkCfgFile, , 2)
                mSkCfgXml.Save(mSkCfgFile)
                Helpers.Logger.WriteRelative("ok", , 3)
            Else
                Helpers.Logger.WriteRelative("not saving, nothing was changed", , 1)
            End If
        Catch ex As Exception
            Helpers.Logger.WriteRelative("FAIL", , 2)
            Helpers.Logger.WriteRelative(ex.Message, , 3)

            Return False
        End Try

        Return True
    End Function


    Public Sub New()
        mSiteKioskFolder = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "InstallDir", "")
        mSkCfgFile = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "LastCfg", "")
        mSkBuild = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "Build", "")
        mConfigChanged = False
    End Sub
End Class
