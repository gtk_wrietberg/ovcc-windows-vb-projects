Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("OVCCStart")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("GuestTek")> 
<Assembly: AssemblyProduct("OVCCStart")>
<Assembly: AssemblyCopyright("Copyright ©  2015-2024")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("ff0398c2-5c3f-4bc2-a34f-c633a8b50b01")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("33.7.23354.1618")>

<Assembly: AssemblyFileVersion("33.7.23354.1618")>
<assembly: AssemblyInformationalVersion("0.0.23354.1618")>