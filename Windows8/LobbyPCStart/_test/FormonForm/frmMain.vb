﻿Public Class frmMain
    Private mCount As Integer = 0

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        mCount += 1

        AddCheckboxesTo_frmOnTop()

        If frmOnTop.ShowDialog(Me) = DialogResult.Yes Then
            Console.WriteLine("frmOnTop.ShowDialog(Me) = DialogResult.Yes")
        Else
            Console.WriteLine("frmOnTop.ShowDialog(Me) = DialogResult.No")
        End If
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load



        Helpers.Forms.TopMost(Me.Handle, True)
    End Sub

    Private Sub AddCheckboxesTo_frmOnTop()
        Helpers.Types.RandomString(10)

        Dim data As String() = New String() {
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10),
            Helpers.Types.RandomString(10)
        }

        Dim random_number As Integer = (New Random).Next(1, data.Length)

        For i As Integer = frmOnTop.Controls.Count - 1 To 0 Step -1
            If TypeOf frmOnTop.Controls.Item(i) Is CheckBox Then
                If frmOnTop.Controls.Item(i).Name.StartsWith("chkApp_") Then
                    frmOnTop.Controls.RemoveAt(i)
                End If
            End If
        Next

        For i As Integer = 0 To random_number
            Dim checkBox = New CheckBox()

            frmOnTop.Controls.Add(checkBox)
            checkBox.Location = New Point(10, 10 + frmOnTop.lblAppsFound.Height + 10 + 20 * i)
            checkBox.Text = data(i)
            checkBox.Name = "chkApp_" & i.ToString
            checkBox.Checked = True
            checkBox.Size = New Size(400, 20)
            checkBox.BackColor = Color.Red
        Next



    End Sub
End Class
