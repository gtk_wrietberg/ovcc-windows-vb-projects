﻿Public Class frmOnTop
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.DialogResult = DialogResult.Yes

        MieKloos()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.DialogResult = DialogResult.No

        MieKloos()
    End Sub

    Private Sub MieKloos()
        Me.Close()
    End Sub

    Private Sub frmOnTop_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim count_checkbox As Integer = 0

        For i As Integer = Me.Controls.Count - 1 To 0 Step -1
            If TypeOf Me.Controls.Item(i) Is CheckBox Then
                If Me.Controls.Item(i).Name.StartsWith("chkApp_") Then
                    count_checkbox += 1
                End If
            End If
        Next

        Dim saais As New Size

        saais.Height = 10 + lblAppsFound.Height + 10 + 20 * count_checkbox + 10 + Button1.Height + 10
        saais.Width = 10 + 400 + 10

        Me.ClientSize = saais

        Button1.Top = 10 + lblAppsFound.Height + 10 + 20 * count_checkbox + 10
        Button2.Top = 10 + lblAppsFound.Height + 10 + 20 * count_checkbox + 10

        Me.Top = frmMain.Top - (Me.Height - frmMain.Height) / 2
        Me.Left = frmMain.Left - (Me.Width - frmMain.Width) / 2

        Console.WriteLine("frmOnTop_Load!")
    End Sub


End Class