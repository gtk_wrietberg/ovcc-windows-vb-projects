﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not TextBox1.Text.StartsWith(".") Then
            TextBox1.Text = "." & TextBox1.Text
        End If

        Dim sName As String = "", sPath As String = ""

        If Not GetApplicationForExtension.GetName(TextBox1.Text, sName) Then
            sName = "(not found)"
        End If

        If Not GetApplicationForExtension.GetExecutable(TextBox1.Text, sPath) Then
            sPath = "(not found)"
        End If

        Console.WriteLine(TextBox1.Text & ": " & sName & " - " & sPath)
        MsgBox(TextBox1.Text & ": " & sName & " - " & sPath)
    End Sub
End Class
