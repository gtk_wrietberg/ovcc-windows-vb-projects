﻿Imports System.ComponentModel
Imports System.IO
Imports System.Security

Public Class frmMain
    Private Event CopyComplete()
    Private Event UnzipComplete()

    Private Event AllCompleted()

    Private Const BufferSize As Integer = 4096

    Private WithEvents bgCopier As New BackgroundWorker
    Private WithEvents bgUnzipper As New BackgroundWorker

    Private Class WorkerPacket
        Public DataRef As Byte()
        Public Filename As String
    End Class

    Private taskList As New List(Of WorkerPacket)
    Private _cancel As Boolean = False

    '
    '-- Setup worker
    '
    Private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        bgCopier.WorkerReportsProgress = True
        bgCopier.WorkerSupportsCancellation = True
    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        bgCopier.Dispose()
        bgUnzipper.Dispose()
    End Sub

    '
    '-- UI
    '
    Private Sub StartCopy()
        AddCopyTask(My.Resources._files, "c:\test1.dat")
        'AddCopyTask(My.Resources.TestData, "c:\test2.dat")
        'AddCopyTask(My.Resources.TestData, "c:\test3.dat")

        StartCopyQue()
    End Sub

    '
    '-- The methods to build and perform task-list
    '
    Private Sub AddCopyTask(data As Byte(), filename As String)
        '
        '-- Create argument packet for worker
        '
        Dim wp As New WorkerPacket
        wp.DataRef = data
        wp.Filename = filename

        taskList.Add(wp)
    End Sub

    Private Sub StartCopyQue()
        '
        '-- Init total progressbar
        '
        progressIO.Value = 0
        progressIO.Maximum = 100

        _cancel = False

        '
        '-- Go
        '
        CopyBytesToFileMT()
    End Sub

    Private Sub CopyBytesToFileMT()
        '
        '-- Get work packet
        '
        Dim wp As WorkerPacket = taskList(0)
        '
        '-- Init progress bars
        '
        progressIO.Value = 0

        '
        '-- Start worker
        '
        If Not _cancel Then
            bgCopier.RunWorkerAsync(wp)
        End If
    End Sub

    Private Sub DoWork_Copier(s As Object, e As DoWorkEventArgs) Handles bgCopier.DoWork
        Dim wp As WorkerPacket = CType(e.Argument, WorkerPacket)
        '
        '-- Calculate segments
        '
        '   note: byte().length returns integer which means we're limited to 2 Gb files
        '
        Dim length As Integer = wp.DataRef.Length
        Dim segments As Integer = CInt(Math.Floor(length / BufferSize))
        Dim leftOver As Integer = length - segments * BufferSize

        Dim bf As Integer = BufferSize
        If bf > length Then bf = length

        Dim fs As New FileStream(wp.Filename, FileMode.Create, FileAccess.Write, FileShare.None)
        '
        '-- Copy blocks
        '
        For i As Integer = 0 To segments - 1
            '
            '-- Cancelled?
            '
            If e.Cancel Then
                leftOver = 0
                Exit For
            End If
            '
            '-- Write a segment to file
            '
            Dim pos As Integer = i * BufferSize
            fs.Write(wp.DataRef, pos, bf)
            '
            '-- Report current progress
            '
            bgCopier.ReportProgress(CInt(pos / length * 100))

        Next
        '
        '-- Copy any remainer
        '
        If leftOver > 0 Then
            fs.Write(wp.DataRef, segments * BufferSize, leftOver)
            bgCopier.ReportProgress(100)
        End If
        '
        '-- Done
        '
        fs.Flush()
        fs.Dispose()
    End Sub

    Private Sub CopyProgress(s As Object, e As ProgressChangedEventArgs) Handles bgCopier.ProgressChanged
        progressIO.Value = e.ProgressPercentage
    End Sub

    Private Sub CopyCompleted(s As Object, e As RunWorkerCompletedEventArgs) Handles bgCopier.RunWorkerCompleted
        '
        '-- Remove task just finished
        '
        taskList.RemoveAt(0)
        '
        '-- Do we have another task?
        '
        If taskList.Count > 0 AndAlso Not _cancel Then
            CopyBytesToFileMT()
        Else
            'Done

            RaiseEvent CopyComplete()
        End If
    End Sub

    Private Sub Done() Handles Me.AllCompleted
        progressIO.Style = ProgressBarStyle.Blocks
    End Sub

End Class
