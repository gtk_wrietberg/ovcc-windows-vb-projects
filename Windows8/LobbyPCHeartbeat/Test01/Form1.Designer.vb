﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.chkUserAgent = New System.Windows.Forms.CheckBox()
        Me.chkIgnoreFaultySSL = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(86, 94)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(126, 92)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(261, 287)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(156, 156)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'chkUserAgent
        '
        Me.chkUserAgent.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkUserAgent.Location = New System.Drawing.Point(97, 287)
        Me.chkUserAgent.Name = "chkUserAgent"
        Me.chkUserAgent.Size = New System.Drawing.Size(158, 75)
        Me.chkUserAgent.TabIndex = 2
        Me.chkUserAgent.Text = "UserAgent"
        Me.chkUserAgent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkUserAgent.UseVisualStyleBackColor = True
        '
        'chkIgnoreFaultySSL
        '
        Me.chkIgnoreFaultySSL.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkIgnoreFaultySSL.Location = New System.Drawing.Point(97, 368)
        Me.chkIgnoreFaultySSL.Name = "chkIgnoreFaultySSL"
        Me.chkIgnoreFaultySSL.Size = New System.Drawing.Size(158, 75)
        Me.chkIgnoreFaultySSL.TabIndex = 3
        Me.chkIgnoreFaultySSL.Text = "Ignore SSL error"
        Me.chkIgnoreFaultySSL.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkIgnoreFaultySSL.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(579, 512)
        Me.Controls.Add(Me.chkIgnoreFaultySSL)
        Me.Controls.Add(Me.chkUserAgent)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents chkUserAgent As System.Windows.Forms.CheckBox
    Friend WithEvents chkIgnoreFaultySSL As System.Windows.Forms.CheckBox

End Class
