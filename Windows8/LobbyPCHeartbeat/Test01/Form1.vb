﻿Public Class Form1

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        '-----------------------------------

        'check if we need a name sync
        If NameSync.IsMachineNameDifferent Then
            MsgBox("NAMESYNC!!!!!!!!!!!!!")
            Globals.Logger.WriteMessage("NameSync needed")
            NameSync.UpdateCoreScript()
            NameSync.UpdateSiteKioskConfig()
            NameSync.StoreMachineName()
        Else
            MsgBox("NAMESYNC not needed")
        End If

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Globals.InitGlobals()

        Globals.Logger.LogFileDirectory = Globals.LogFileDirectory
        Globals.Logger.LogFileName = Globals.LogFileName
        Globals.Logger.Debugging = Globals.Settings.Debugging


        Globals.Logger.WriteMessage(New String("*", 50))
        Globals.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Globals.Logger.WriteMessage("service started")


    End Sub


    Private Sub Thread_Heartbeat()
        Try
            'https://lpchbserver01.ibahn.com/axis/servlet/HeartbeatReceiver


            'Dim request As Net.HttpWebRequest = Net.HttpWebRequest.Create(Globals.Settings.ServerUrl_Heartbeat)
            Dim request As Net.HttpWebRequest = Net.HttpWebRequest.Create("https://ppc02.ibahn.com/axis/servlet/HeartbeatReceiverServlet")

            If chkUserAgent.Checked Then
                request.UserAgent = "Mozilla/5.0 (MSIE 9.0; Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko"

                MsgBox("KAKSMURRIEZUIGHOER")
            End If



            If chkIgnoreFaultySSL.Checked Then
                request.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
            End If


            request.Method = "POST"

            Globals.Logger.WriteDebug("(HeartbeatThread) init heartbeat")

            Globals.Heartbeat = New clsHeartbeat
            Globals.Heartbeat.ComputerName = Environment.MachineName
            Globals.Heartbeat.Version = Globals.CONSTANTS__HEARTBEAT_APP_VERSION
            Globals.Heartbeat.LoadInternalIps()

            Globals.Logger.WriteDebug("(HeartbeatThread) sending heartbeat")
            Globals.Logger.WriteDebug(Globals.Heartbeat.Request, 1)

            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(Globals.Heartbeat.Request)

            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length

            Dim dataStream As IO.Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            Globals.Logger.WriteDebug("(HeartbeatThread) receiving server response")

            Dim response As Net.WebResponse = request.GetResponse()

            dataStream = response.GetResponseStream()

            Dim reader As New IO.StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()


            Globals.Heartbeat.Response = responseFromServer

            Globals.Logger.WriteDebug(Globals.Heartbeat.Response, 1)

            reader.Close()
            dataStream.Close()
            response.Close()
        Catch ex As Exception
            Globals.Logger.WriteError("(HeartbeatThread) heartbeat exception", 0)
            Globals.Logger.WriteError("server: " & Globals.Settings.ServerUrl_Heartbeat, 1)
            Globals.Logger.WriteError(ex.Message, 2)
        End Try

        'Analyse the response ; should look something like this:
        '  <lobbypc><heartbeat-ok/><update>0</update><lastlog>2015-02-16</lastlog><lastline>null</lastline><lastlinedate></lastlinedate></lobbypc>
        Globals.Logger.WriteDebug("(HeartbeatThread) parsing server response")

        Dim xmlDoc As Xml.XmlDocument
        Dim xmlNode_Root As Xml.XmlNode
        Dim xmlNode__heartbeatok As Xml.XmlNode
        Dim xmlNode__lastlog As Xml.XmlNode

        Try
            xmlDoc = New Xml.XmlDocument
            xmlDoc.LoadXml(Globals.Heartbeat.Response)
            xmlNode_Root = xmlDoc.SelectSingleNode("lobbypc")
            xmlNode__heartbeatok = xmlNode_Root.SelectSingleNode("heartbeat-ok")
            xmlNode__lastlog = xmlNode_Root.SelectSingleNode("lastlog")

            If Not xmlNode__heartbeatok Is Nothing Then
                Dim dUploadHistoryDays As Date = DateAdd(DateInterval.Day, -Globals.Settings.Log_UploadHistoryDays - 1, Date.Now)
                Dim sLastLog As String = xmlNode__lastlog.InnerText
                If sLastLog.Equals("") Then
                    Globals.Logger.WriteDebug("(HeartbeatThread) no last log date")

                    Globals.Settings.Date_LastLog = Helpers.FromNormalDateToHBDate(dUploadHistoryDays)

                    Globals.Logger.WriteDebug("set to: " & Globals.Settings.Date_LastLog.ToString, 1)
                Else
                    Globals.Logger.WriteDebug("(HeartbeatThread) received last log date")
                    Globals.Logger.WriteDebug("received: " & sLastLog, 1)

                    If Helpers.IsValidHBDate(sLastLog) Then
                        Globals.Settings.Date_LastLog = sLastLog
                    Else
                        Globals.Logger.WriteDebug("invalid date", 2)

                        Globals.Settings.Date_LastLog = Helpers.FromNormalDateToHBDate(dUploadHistoryDays)

                        Globals.Logger.WriteDebug("set to: " & Globals.Settings.Date_LastLog.ToString, 2)
                    End If
                End If

                Globals.Settings.Date_LastHeartbeat = Helpers.FromNormalDateToHBDate(Date.Now)
                Globals.Settings.SaveDates()

                Globals.Logger.WriteMessage("(HeartbeatThread) heartbeat ok", 0)
            Else
                Globals.Logger.WriteError("(HeartbeatThread) heartbeat failed", 0)
                Globals.Logger.WriteError("this was the response:", 1)
                Globals.Logger.WriteError(Globals.Heartbeat.Response, 2)
            End If
        Catch ex As Exception
            Globals.Logger.WriteError("(HeartbeatThread) response exception", 0)
            Globals.Logger.WriteError(ex.Message, 1)
        End Try
    End Sub


    Private Function AcceptAllCertifications(ByVal sender As Object, ByVal certification As System.Security.Cryptography.X509Certificates.X509Certificate, ByVal chain As System.Security.Cryptography.X509Certificates.X509Chain, ByVal sslPolicyErrors As System.Net.Security.SslPolicyErrors) As Boolean
        Return True
    End Function


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Thread_Heartbeat()
    End Sub
End Class
