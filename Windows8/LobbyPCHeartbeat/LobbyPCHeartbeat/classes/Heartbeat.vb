﻿Public Class clsHeartbeat
    Private mComputerName As String
    Private mVersion As String
    Private mInternalIps As List(Of String)
    Private mResponse As String

    Private ReadOnly c_HeartbeatTemplate As String = "<lobbypc><build>%%V%%</build><heartbeat><terminal-id>%%N%%</terminal-id><internal-ip>%%I%%</internal-ip></heartbeat></lobbypc>"

    Public Sub New()
        mComputerName = ""
        mVersion = ""
        mInternalIps = New List(Of String)
    End Sub

    Public Property ComputerName As String
        Get
            Return mComputerName
        End Get
        Set(value As String)
            mComputerName = value
        End Set
    End Property

    Public Property Version As String
        Get
            Return mVersion
        End Get
        Set(value As String)
            mVersion = value
        End Set
    End Property

    Public Property Response As String
        Get
            Return mResponse
        End Get
        Set(value As String)
            mResponse = value
        End Set
    End Property

    Public Sub LoadInternalIps()
        Dim strIP As String
        mInternalIps = New List(Of String)

        For Each ip As Net.IPAddress In Net.Dns.GetHostEntry(Net.Dns.GetHostName).AddressList
            Dim regexIP As System.Text.RegularExpressions.Regex
            regexIP = New System.Text.RegularExpressions.Regex("^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$")
            strIP = regexIP.Match(ip.ToString).Value

            If Not strIP.Equals("") Then
                mInternalIps.Add(strIP)
            End If
        Next
    End Sub

    '-------------------------------------
    Public ReadOnly Property Request As String
        Get
            Return c_HeartbeatTemplate.Replace("%%V%%", mVersion).Replace("%%N%%", mComputerName).Replace("%%I%%", String.Join(",", mInternalIps))
        End Get
    End Property


End Class
