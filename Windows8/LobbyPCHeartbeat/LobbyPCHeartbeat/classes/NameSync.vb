﻿Public Class NameSync
    Public Shared ReadOnly Property IsMachineNameDifferent() As Boolean
        Get
            Dim sCurrentName As String = Environment.MachineName
            Dim sStoredName As String = Globals.Settings.StoredMachineName

            Return Not sCurrentName.Equals(sStoredName)
        End Get
    End Property

    Public Shared Function StoreMachineName() As Boolean
        Try
            Globals.Settings.StoredMachineName = Environment.MachineName

            Return Not IsMachineNameDifferent
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function UpdateCoreScript() As Boolean
        Try
            Dim sFile As String, sPath As String, mBackupFolder As String

            sPath = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sPath &= "\Skins\default\Scripts\"

            Globals.Logger.WriteRelativeMessage("looking for script file", 1)
            If Not IO.File.Exists(sPath & "guest-tek.js") Then
                If Not IO.File.Exists(sPath & "ibahn.js") Then
                    If Not IO.File.Exists(sPath & "mycall.js") Then
                        Globals.Logger.WriteRelativeError("could not find any script file", 2)
                        Return False
                    Else
                        sFile = sPath & "mycall.js"
                    End If
                Else
                    sFile = sPath & "ibahn.js"
                End If
            Else
                sFile = sPath & "guest-tek.js"
            End If

            Globals.Logger.WriteRelativeMessage("found", 2)
            Globals.Logger.WriteRelativeMessage(sFile, 3)


            '*************************************************************************************************************
            Globals.Logger.WriteRelativeMessage("backing up", 2)
            mBackupFolder = Globals.BackupDirectory

            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            oFile = New IO.FileInfo(sFile)

            Dim sBackupPath As String = oFile.DirectoryName
            Dim sBackupName As String = oFile.Name
            Dim sBackupFile As String
            Dim oDirInfo As System.IO.DirectoryInfo
            sBackupPath = sBackupPath.Replace("C:", "")
            sBackupPath = sBackupPath.Replace("c:", "")
            sBackupPath = sBackupPath.Replace("\\", "\")
            sBackupPath = mBackupFolder & "\" & sBackupPath

            sBackupFile = sBackupPath & "\" & sBackupName

            Globals.Logger.WriteRelativeMessage("backup: " & sFile & " => " & sBackupFile, 3)

            oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oFile = New IO.FileInfo(sFile)
            oFile.CopyTo(sBackupFile, True)

            Globals.Logger.WriteRelativeMessage("ok", 4)


            '*************************************************************************************************************
            Globals.Logger.WriteRelativeMessage("updating", 2)
            Dim lines As New List(Of String)
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var LocationID") Then
                        line = "var LocationID=""" & Environment.MachineName & """;"
                    End If

                    lines.Add(line)
                End While
            End Using


            '*************************************************************************************************************
            Globals.Logger.WriteRelativeMessage("saving", 2)
            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using

            Globals.Logger.WriteRelativeMessage("ok", 2)
            Return True
        Catch ex As Exception
            Globals.Logger.WriteRelativeError("FAIL", 1)
            Globals.Logger.WriteRelativeError(ex.Message, 2)
            Return False
        End Try
    End Function

    Public Shared Function UpdateSiteKioskConfig() As Boolean
        Dim mSiteKioskFolder As String, mOldSkCfgFile As String, mNewSkCfgFile As String, mBackupFolder As String

        Try
            Globals.Logger.WriteRelativeMessage("finding SiteKiosk install directory", 1)
            mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            Globals.Logger.WriteRelativeMessage("found: " & mSiteKioskFolder, 2)

            Globals.Logger.WriteRelativeMessage("finding active skcfg file", 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            Globals.Logger.WriteRelativeMessage("found: " & mOldSkCfgFile, 2)


            Globals.Logger.WriteRelativeMessage("backing up current config", 1)
            mBackupFolder = Globals.BackupDirectory

            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            oFile = New IO.FileInfo(mOldSkCfgFile)

            Dim sBackupPath As String = oFile.DirectoryName
            Dim sBackupName As String = oFile.Name
            Dim sBackupFile As String
            Dim oDirInfo As System.IO.DirectoryInfo
            sBackupPath = sBackupPath.Replace("C:", "")
            sBackupPath = sBackupPath.Replace("c:", "")
            sBackupPath = sBackupPath.Replace("\\", "\")
            sBackupPath = mBackupFolder & "\" & sBackupPath

            sBackupFile = sBackupPath & "\" & sBackupName

            Globals.Logger.WriteRelativeMessage("backup: " & mOldSkCfgFile & " => " & sBackupFile, 2)

            oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oFile = New IO.FileInfo(mOldSkCfgFile)
            oFile.CopyTo(sBackupFile, True)

            mNewSkCfgFile = mOldSkCfgFile
            Globals.Logger.WriteRelativeMessage("ok", 2)


            Globals.Logger.WriteRelativeMessage("updating", 1)

            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            xmlSkCfg = New Xml.XmlDocument


            Globals.Logger.WriteRelativeMessage("loading", 2)
            Globals.Logger.WriteRelativeMessage(mNewSkCfgFile, 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            Globals.Logger.WriteRelativeMessage("ok", 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            Globals.Logger.WriteRelativeMessage("loading root node", 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                Globals.Logger.WriteRelativeMessage("ok", 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If



            '*************************************************************************************************************
            'SiteCash - replace comment field with new name
            Globals.Logger.WriteRelativeMessage("SiteCash CC comment field update", 2)
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode

            Globals.Logger.WriteRelativeMessage("loading SiteCash node", 3)
            xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

            If Not xmlNode_SiteCash Is Nothing Then
                Globals.Logger.WriteRelativeMessage("ok", 4)
            Else
                Throw New Exception("SiteCash node not found")
            End If


            Dim xmlNodeList_Devices As Xml.XmlNodeList
            Dim xmlNode_Device As Xml.XmlNode
            Dim xmlNode_CC As Xml.XmlNode

            Globals.Logger.WriteRelativeMessage("loading CC node", 3)
            xmlNodeList_Devices = xmlNode_SiteCash.SelectNodes("sk:device-data", ns)

            For Each xmlNode_Device In xmlNodeList_Devices
                If xmlNode_Device.Attributes.GetNamedItem("clsid").InnerText = "dbbdad62-8c09-45f3-b189-9777ec1c8e47" Then
                    xmlNode_CC = xmlNode_Device

                    Exit For
                End If
            Next

            If Not xmlNode_CC Is Nothing Then
                Globals.Logger.WriteRelativeMessage("ok", 4)
            Else
                Throw New Exception("CC node not found")
            End If


            Dim xmlNodeList_Gateways As Xml.XmlNodeList
            Dim xmlNode_Gateway As Xml.XmlNode
            Dim xmlNode_VeriSign As Xml.XmlNode

            Globals.Logger.WriteRelativeMessage("loading CC gateway node", 3)
            xmlNodeList_Gateways = xmlNode_CC.SelectNodes("sk:gateway", ns)

            For Each xmlNode_Gateway In xmlNodeList_Gateways
                If xmlNode_Gateway.Attributes.GetNamedItem("clsid").InnerText = "b9bae269-bad6-41eb-a84f-fdfc938cb93a" Then
                    xmlNode_VeriSign = xmlNode_Gateway

                    Exit For
                End If
            Next

            If Not xmlNode_VeriSign Is Nothing Then
                Globals.Logger.WriteRelativeMessage("ok", 4)
            Else
                Throw New Exception("CC gateway node not found")
            End If


            Dim xmlNode_Comment As Xml.XmlNode
            xmlNode_Comment = xmlNode_VeriSign.SelectSingleNode("sk:comment", ns)
            xmlNode_Comment.InnerText = Environment.MachineName


            '*************************************************************************************************************
            'Saving
            Globals.Logger.WriteRelativeMessage("saving", 2)
            Globals.Logger.WriteRelativeMessage(mNewSkCfgFile, 3)
            xmlSkCfg.Save(mNewSkCfgFile)
            Globals.Logger.WriteRelativeMessage("ok", 4)

            Return True
        Catch ex As Exception
            Globals.Logger.WriteRelativeError("ERROR!!!", 1)
            Globals.Logger.WriteRelativeError(ex.Message, 2)

            Return False
        End Try
    End Function
End Class
