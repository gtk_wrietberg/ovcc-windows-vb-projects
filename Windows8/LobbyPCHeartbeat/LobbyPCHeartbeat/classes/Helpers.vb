﻿Public Class Helpers
    Public Shared Function LeadingZero(num As Integer, Optional len As Integer = 2) As String
        Dim sNum As String = num.ToString

        If sNum.Length < len Then
            sNum = (New String("0", (len - sNum.Length))) & sNum
        End If

        Return sNum
    End Function

    Public Shared Function FromNormalDateToHBDate(d As Date, Optional WithTime As Boolean = False) As String
        Dim sTmp As String = d.ToString("yyyy-MM-dd")

        If WithTime Then
            sTmp &= " " & d.ToString("HH:mm:ss")
        End If

        Return sTmp
    End Function

    Public Shared Function FromHBDateToNormalDate(s As String) As Date
        Dim d As Date

        Try
            d = Date.Parse(s)
        Catch ex As Exception
            d = Nothing
        End Try

        Return d
    End Function

    Public Shared Function IsValidHBDate(s As String) As Boolean
        Dim d As Date

        Try
            d = Date.Parse(s)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function GetSiteKioskVersion(Optional bShort As Boolean = False) As String
        Dim reg As Microsoft.Win32.RegistryKey

        reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Provisio\\SiteKiosk", False)
        If reg Is Nothing Then
            Return ""
        Else
            Dim sTmp As String = reg.GetValue("Build", "0.0")

            If bShort Then
                Return sTmp.Split(".")(0)
            Else
                Return sTmp
            End If
        End If
    End Function

    Public Shared Function GetSiteKioskInstallFolder() As String
        Dim reg As Microsoft.Win32.RegistryKey

        reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Provisio\\SiteKiosk", False)
        If reg Is Nothing Then
            Return ""
        Else
            Return reg.GetValue("InstallDir", "")
        End If
    End Function

    Public Shared Function GetSiteKioskLogfileFolder() As String
        Return Helpers.GetSiteKioskInstallFolder & "\logfiles"
    End Function

    Private Shared ReadOnly _sReplacements() As String = { _
        "[SiteKiosk] Notification: [Memory Status]", _
        "[SiteKiosk] Notification: [Process Status]", _
        ""}

    Public Shared Function CleanSiteKioskLogLine(ByVal input As String) As String
        Dim result As String = input

        For Each item As String In _sReplacements
            If Not item.Equals("") Then
                If result.Contains(item) Then
                    result = ""
                    Exit For
                End If
            End If
        Next

        If result <> "" Then
            'remove unwanted chars
            For i = 0 To 31
                result = result.Replace(Chr(i), "")
            Next
            For i = 127 To 255
                result = result.Replace(Chr(i), "")
            Next
            'Additionally, let's remove </ < and >
            result = result.Replace("</", "[")
            result = result.Replace("<", "[")
            result = result.Replace("/>", "]")
            result = result.Replace(">", "]")
        End If

        Return result
    End Function

End Class
