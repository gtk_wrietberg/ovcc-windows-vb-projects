﻿Public Class Globals
    Public Shared Logger As clsLogger
    Public Shared XOrObfuscation As clsXOrObfuscation
    Public Shared Settings As clsSettings
    Public Shared Heartbeat As clsHeartbeat

    Public Shared Property LogFileDirectory As String
    Public Shared Property LogFileName As String
    Public Shared Property BackupDirectory As String
    Public Shared Property OverwriteExistingConfig As Boolean
    Public Shared Property TESTMODE As Boolean

    Public Shared ReadOnly CONSTANTS__LOOP_DELAY As Integer = 5000
    Public Shared ReadOnly CONSTANTS__HEARTBEAT_APP_VERSION As String = "2.1.136"

    Public Shared ReadOnly CONSTANTS__SERVICE_NAME_HEARTBEAT As String = "OVCCAgent_Heartbeat"
    Public Shared ReadOnly CONSTANTS__SERVICE_DISPLAYNAME_HEARTBEAT As String = "OVCC Agent - Heartbeat service"
    Public Shared ReadOnly CONSTANTS__SERVICE_DESCRIPTION_HEARTBEAT As String = "Sends heartbeat to OVCC servers"

    Public Shared ReadOnly CONSTANTS__SERVICE_NAME_LOGFILE As String = "OVCCAgent_Logfile"
    Public Shared ReadOnly CONSTANTS__SERVICE_DISPLAYNAME_LOGFILE As String = "OVCC Agent - Logfile service"
    Public Shared ReadOnly CONSTANTS__SERVICE_DESCRIPTION_LOGFILE As String = "Uploads OVCC log files to OVCC servers"


    Public Shared ReadOnly Property FOLDER__ProgramFiles As String
        Get
            Dim sTmp As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sTmp.Equals(String.Empty) Then
                sTmp = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            Return sTmp
        End Get
    End Property

    Public Shared ReadOnly Property FILE_AND_FOLDERS__ServicePath As String
        Get
            Dim sTmp As String = FOLDER__ProgramFiles & "\GuestTek\OVCCAgent"

            Return sTmp
        End Get
    End Property

    Public Shared ReadOnly FILE_AND_FOLDERS__AgentHeartbeattService As String = "OVCCHeartbeat.exe"
    Public Shared ReadOnly FILE_AND_FOLDERS__AgentLogfileService As String = "OVCCLogfileUploader.exe"



    Public Shared Sub InitGlobals()
        LogFileDirectory = FOLDER__ProgramFiles & "\GuestTek\_logs"
        LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        Dim dDate As Date = Now()

        BackupDirectory = FOLDER__ProgramFiles & "\GuestTek\_backups"
        BackupDirectory = BackupDirectory & "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString
        BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        Logger = New clsLogger
        Settings = New clsSettings
        XOrObfuscation = New clsXOrObfuscation

        InitLogFileName()


        Try
            IO.Directory.CreateDirectory(LogFileDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_LogFileDirectory & "'): " & ex.Message)
        End Try

        Try
            IO.Directory.CreateDirectory(BackupDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_BackupDirectory & "'): " & ex.Message)
        End Try

    End Sub

    Public Shared Sub InitLogFileName()
        Dim dDate As Date = Now()

        LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"
    End Sub

End Class
