﻿Imports System.ComponentModel
Imports System.Configuration.Install

Public Class ProjectInstaller

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add initialization code after the call to InitializeComponent
        Me.ServiceInstaller_LPCHeartbeat.Description = "Sends heartbeat to LPC servers"
    End Sub

    Private Sub ServiceProcessInstaller_LPCHeartbeat_AfterInstall(sender As Object, e As InstallEventArgs) Handles ServiceProcessInstaller_LPCHeartbeat.AfterInstall

    End Sub
End Class
