﻿
Public Class OVCCHeartbeat_service
    Private mThreading_Main As Threading.Thread
    Private mThreading_Heartbeat As Threading.Thread

    Private mLoop_Abort As Boolean
    Private mLoop_RandomStartDelay As Integer
    Private mLoop_RandomStartDelayStep As Integer


    '======================================================================================================================================================================
    Protected Overrides Sub OnStart(ByVal args() As String)
        Globals.InitGlobals()

        Globals.Logger.LogFileDirectory = Globals.LogFileDirectory
        Globals.Logger.LogFileName = Globals.LogFileName
        Globals.Logger.Debugging = Globals.Settings.Debugging


        Globals.Logger.WriteMessage(New String("*", 50))
        Globals.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Globals.Logger.WriteMessage("service started")

        '-----------------------------------

        'check if we need a name sync
        If NameSync.IsMachineNameDifferent Then
            Globals.Logger.WriteWarning("NameSync needed")
            NameSync.UpdateCoreScript()
            NameSync.UpdateSiteKioskConfig()
            NameSync.StoreMachineName()
        End If

        StartMainThread()
    End Sub

    Protected Overrides Sub OnStop()
        KillThreads()

        Globals.Logger.WriteMessage("service stopped")
    End Sub

    '======================================================================================================================================================================

    Private Sub StartMainThread()
        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Private Sub KillThreads()
        Try
            mThreading_Heartbeat.Abort()
        Catch ex As Exception

        End Try
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Thread_Heartbeat()
        Try
            Dim request As Net.WebRequest = Net.WebRequest.Create(Globals.Settings.ServerUrl_Heartbeat)

            request.Method = "POST"

            Globals.Logger.WriteDebug("(HeartbeatThread) init heartbeat")

            Globals.Heartbeat = New clsHeartbeat
            Globals.Heartbeat.ComputerName = Environment.MachineName
            Globals.Heartbeat.Version = Globals.CONSTANTS__HEARTBEAT_APP_VERSION
            Globals.Heartbeat.LoadInternalIps()

            Globals.Logger.WriteDebug("(HeartbeatThread) sending heartbeat")
            Globals.Logger.WriteDebug(Globals.Heartbeat.Request, 1)

            Dim byteArray As Byte() = System.Text.Encoding.UTF8.GetBytes(Globals.Heartbeat.Request)

            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = byteArray.Length

            Dim dataStream As IO.Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            Globals.Logger.WriteDebug("(HeartbeatThread) receiving server response")

            Dim response As Net.WebResponse = request.GetResponse()

            dataStream = response.GetResponseStream()

            Dim reader As New IO.StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()


            Globals.Heartbeat.Response = responseFromServer

            Globals.Logger.WriteDebug(Globals.Heartbeat.Response, 1)

            reader.Close()
            dataStream.Close()
            response.Close()
        Catch ex As Exception
            Globals.Logger.WriteError("(HeartbeatThread) heartbeat exception", 0)
            Globals.Logger.WriteError("server: " & Globals.Settings.ServerUrl_Heartbeat, 1)
            Globals.Logger.WriteError(ex.Message, 2)

            Globals.Settings.HeartbeatSuccess = False

            Globals.Logger.WriteDebug("(HeartbeatThread) parsing server response SKIPPED")

            Exit Sub
        End Try

        'Analyse the response ; should look something like this:
        '  <lobbypc><heartbeat-ok/><update>0</update><lastlog>2015-02-16</lastlog><lastline>null</lastline><lastlinedate></lastlinedate></lobbypc>
        Globals.Logger.WriteDebug("(HeartbeatThread) parsing server response")

        Dim xmlDoc As Xml.XmlDocument
        Dim xmlNode_Root As Xml.XmlNode
        Dim xmlNode__heartbeatok As Xml.XmlNode
        Dim xmlNode__lastlog As Xml.XmlNode

        Try
            xmlDoc = New Xml.XmlDocument
            xmlDoc.LoadXml(Globals.Heartbeat.Response)
            xmlNode_Root = xmlDoc.SelectSingleNode("lobbypc")
            xmlNode__heartbeatok = xmlNode_Root.SelectSingleNode("heartbeat-ok")
            xmlNode__lastlog = xmlNode_Root.SelectSingleNode("lastlog")

            If Not xmlNode__heartbeatok Is Nothing Then
                Dim dUploadHistoryDays As Date = DateAdd(DateInterval.Day, -Globals.Settings.Log_UploadHistoryDays - 1, Date.Now)
                Dim sLastLog As String = xmlNode__lastlog.InnerText
                If sLastLog.Equals("") Then
                    Globals.Logger.WriteDebug("(HeartbeatThread) no last log date")

                    Globals.Settings.Date_LastLog = Helpers.FromNormalDateToHBDate(dUploadHistoryDays)

                    Globals.Logger.WriteDebug("set to: " & Globals.Settings.Date_LastLog.ToString, 1)
                Else
                    Globals.Logger.WriteDebug("(HeartbeatThread) received last log date")
                    Globals.Logger.WriteDebug("received: " & sLastLog, 1)

                    If Helpers.IsValidHBDate(sLastLog) Then
                        Globals.Settings.Date_LastLog = sLastLog
                    Else
                        Globals.Logger.WriteDebug("invalid date", 2)

                        Globals.Settings.Date_LastLog = Helpers.FromNormalDateToHBDate(dUploadHistoryDays)

                        Globals.Logger.WriteDebug("set to: " & Globals.Settings.Date_LastLog.ToString, 2)
                    End If
                End If

                Globals.Settings.Date_LastHeartbeat = Helpers.FromNormalDateToHBDate(Date.Now)
                Globals.Settings.SaveDates()

                Globals.Logger.WriteMessage("(HeartbeatThread) heartbeat ok", 0)

                Globals.Settings.HeartbeatSuccess = True
            Else
                Globals.Logger.WriteError("(HeartbeatThread) heartbeat failed", 0)
                Globals.Logger.WriteError("this was the response:", 1)
                Globals.Logger.WriteError(Globals.Heartbeat.Response, 2)

                Globals.Settings.HeartbeatSuccess = False
            End If
        Catch ex As Exception
            Globals.Logger.WriteError("(HeartbeatThread) response exception", 0)
            Globals.Logger.WriteError(ex.Message, 1)

            Globals.Settings.HeartbeatSuccess = False
        End Try
    End Sub

    Private Sub Thread_Main()
        Globals.Logger.WriteDebug("(MainThread) init")

        Globals.Settings.LoadSettings()
        Globals.Settings.HeartbeatSuccess = False
        Globals.Logger.WriteDebug("(MainThread) loading settings")
        If Globals.Settings.Debugging Then
            Globals.Logger.WriteDebug(Globals.Settings.DebugString)
        End If


        Dim iLoopDelay As Integer = 0


        'First start delay
        If Globals.Settings.Loop_SkipRandomisation Then
            Globals.Logger.WriteDebug("(MainThread) skipping randomisation", 1)

            iLoopDelay = 10000
        Else
            Globals.Logger.WriteDebug("(MainThread) random start delay")

            Randomize()

            mLoop_RandomStartDelay = 60 + CInt(Math.Ceiling(Rnd() * 3600))
            mLoop_RandomStartDelayStep = 10

            Globals.Logger.WriteDebug("(MainThread) waiting " & mLoop_RandomStartDelay & " seconds")

            'long sleep
            Do While mLoop_RandomStartDelay > 0
                mLoop_RandomStartDelay -= mLoop_RandomStartDelayStep

                Threading.Thread.Sleep(mLoop_RandomStartDelayStep * 1000)
            Loop


            Globals.Logger.WriteDebug("(MainThread) awake again!")

            iLoopDelay = 30000
        End If


        'Main loop
        Dim dStart As Date = Date.Now

        Globals.Logger.WriteDebug("(MainThread) started")

        Do While Not mLoop_Abort
            If Math.Abs(DateDiff(DateInterval.Second, dStart, Date.Now)) > (iLoopDelay / 1000) Then
                dStart = Date.Now

                Globals.Logger.WriteDebug("(MainThread) reloading settings")

                Globals.Settings.LoadSettings()

                iLoopDelay = Globals.Settings.Loop_Delay

                Globals.Logger.WriteDebug("(MainThread) starting heartbeat")
                mThreading_Heartbeat = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Heartbeat))
                mThreading_Heartbeat.Start()
            End If

            Threading.Thread.Sleep(Globals.CONSTANTS__LOOP_DELAY)
        Loop

    End Sub

End Class
