﻿Public Class Upload
    Private mComputerName As String
    Private mSitekioskVersion As String
    Private mLogName As String
    Private mLogContents As List(Of String)

    Private mResponse As String

    Private ReadOnly c_UploadTemplate As String = "<lobbypc><logfile><pcname>%%N%%</pcname><version>%%V%%</version><logfile-name>%%D%%</logfile-name><lines><![CDATA[%%L%%]]></lines></logfile></lobbypc>"

    ' "<lobbypc><logfile><pcname>%%N%%</pcname><version>%%V%%</version><logfile-name>%%D%%</logfile-name><lines><![CDATA[%%L%%]]></lines></logfile></lobbypc>"

    Public Sub New()
        mComputerName = ""
        mSitekioskVersion = ""
        mLogName = ""
        mLogContents = New List(Of String)
        mResponse = ""
    End Sub

    Public Property ComputerName As String
        Get
            Return mComputerName
        End Get
        Set(value As String)
            mComputerName = value
        End Set
    End Property

    Public Property SitekioskVersion As String
        Get
            Return mSitekioskVersion
        End Get
        Set(value As String)
            mSitekioskVersion = value
        End Set
    End Property

    Public Property LogName As String
        Get
            Return mLogName
        End Get
        Set(value As String)
            mLogName = value
        End Set
    End Property

    Public Property Response As String
        Get
            Return mResponse
        End Get
        Set(value As String)
            mResponse = value
        End Set
    End Property

    Public Sub AddLine(sLine As String)
        mLogContents.Add(sLine)
    End Sub

    '-------------------------------------
    Public ReadOnly Property Request As String
        Get
            Return c_UploadTemplate.Replace("%%V%%", mSitekioskVersion).Replace("%%N%%", mComputerName).Replace("%%D%%", mLogName).Replace("%%L%%", String.Join(Environment.NewLine, mLogContents))
        End Get
    End Property



End Class
