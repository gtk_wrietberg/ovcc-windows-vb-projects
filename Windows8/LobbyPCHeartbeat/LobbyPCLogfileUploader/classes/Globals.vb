﻿Public Class Globals
    Public Shared Logger As clsLogger
    Public Shared XOrObfuscation As clsXOrObfuscation
    Public Shared Settings As clsSettings


    Public Shared ProgramFilesFolder As String
    Public Shared LogFileDirectory As String
    Public Shared LogFileName As String
    'Public Shared BackupDirectory As String

    Public Shared OverwriteExistingConfig As Boolean

    Public Shared TESTMODE As Boolean


    Public Shared ReadOnly CONSTANTS__MAX_UPLOAD_DELAY_SECONDS As Integer = 300
    Public Shared ReadOnly CONSTANTS__LOOP_DELAY_SECONDS As Integer = 600
    Public Shared ReadOnly CONSTANTS__REGEXP_LOGFILE As String = "([0-9]{4})\-([0-9]{2})\-([0-9]{2})\.txt"

    Public Shared Sub InitGlobals()
        ProgramFilesFolder = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)
        If ProgramFilesFolder.Equals(String.Empty) Then
            ProgramFilesFolder = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        LogFileDirectory = ProgramFilesFolder & "\GuestTek\_logs"
        LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        Logger = New clsLogger
        Settings = New clsSettings
        XOrObfuscation = New clsXOrObfuscation


        InitLogFileName()


        Try
            IO.Directory.CreateDirectory(LogFileDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_LogFileDirectory & "'): " & ex.Message)
        End Try

        'Try
        '    IO.Directory.CreateDirectory(BackupDirectory)
        'Catch ex As Exception
        '    'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_BackupDirectory & "'): " & ex.Message)
        'End Try
    End Sub

    Public Shared Sub InitLogFileName()
        Dim dDate As Date = Now()

        LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"
    End Sub
End Class
