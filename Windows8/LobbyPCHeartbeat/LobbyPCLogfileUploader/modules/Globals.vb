Module Globals
    Public oLogger As Logger
    Public oXor As XOrObfuscation
    Public oSettings As Settings


    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String

    Public g_OverwriteExistingConfig As Boolean

    Public g_TESTMODE As Boolean


    Public ReadOnly c_MAX_UPLOAD_DELAY As Integer = 300000
    Public ReadOnly c_LOOP_DELAY As Integer = 60000
    Public ReadOnly c_REGEXP_LOGFILE As String = "([0-9]{4})\-([0-9]{2})\-([0-9]{2})\.txt"

    Public Sub InitGlobals()
        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\Guest-tek\_logs"
        g_LogFileDirectory &= "\" & My.Application.Info.ProductName & "\" & My.Application.Info.Version.ToString


        oSettings = New Settings

        oXor = New XOrObfuscation

        
        InitLogFileName()


        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_LogFileDirectory & "'): " & ex.Message)
        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception
            'MessageBox.Show("FAIL IO.Directory.CreateDirectory('" & g_BackupDirectory & "'): " & ex.Message)
        End Try

    End Sub

    Public Sub InitLogFileName()
        Dim dDate As Date = Now()

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"
    End Sub
End Module
