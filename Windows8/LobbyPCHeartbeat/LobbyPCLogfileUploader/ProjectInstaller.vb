﻿Imports System.ComponentModel
Imports System.Configuration.Install

Public Class ProjectInstaller

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add initialization code after the call to InitializeComponent
        Me.ServiceInstaller_LPCLogfiles.Description = "Uploads LPC log files to LPC servers"
    End Sub

End Class
