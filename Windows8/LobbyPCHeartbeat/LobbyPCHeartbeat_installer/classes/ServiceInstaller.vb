﻿
Imports System.Collections.Generic
Imports System.Text
Imports System.Runtime.InteropServices

Public Enum ServiceManagerRights
    Connect = &H1
    CreateService = &H2
    EnumerateService = &H4
    Lock = &H8
    QueryLockStatus = &H10
    ModifyBootConfig = &H20
    StandardRightsRequired = &HF0000
    AllAccess = (StandardRightsRequired Or Connect Or CreateService Or EnumerateService Or Lock Or QueryLockStatus Or ModifyBootConfig)
End Enum

Public Enum ServiceRights
    QueryConfig = &H1
    ChangeConfig = &H2
    QueryStatus = &H4
    EnumerateDependants = &H8
    Start = &H10
    [Stop] = &H20
    PauseContinue = &H40
    Interrogate = &H80
    UserDefinedControl = &H100
    Delete = &H10000
    StandardRightsRequired = &HF0000
    AllAccess = (StandardRightsRequired Or QueryConfig Or ChangeConfig Or QueryStatus Or EnumerateDependants Or Start Or [Stop] Or PauseContinue Or Interrogate Or UserDefinedControl)
End Enum

Public Enum ServiceBootFlag
    Start = &H0
    SystemStart = &H1
    AutoStart = &H2
    DemandStart = &H3
    Disabled = &H4
End Enum

Public Enum ServiceState
    Unknown = -1
    ' The state cannot be (has not been) retrieved.
    NotFound = 0
    ' The service is not known on the host server.
    [Stop] = 1
    ' The service is NET stopped.
    Run = 2
    ' The service is NET started.
    Stopping = 3
    Starting = 4
End Enum

Public Enum ServiceControl
    [Stop] = &H1
    Pause = &H2
    [Continue] = &H3
    Interrogate = &H4
    Shutdown = &H5
    ParamChange = &H6
    NetBindAdd = &H7
    NetBindRemove = &H8
    NetBindEnable = &H9
    NetBindDisable = &HA
End Enum

Public Enum ServiceError
    Ignore = &H0
    Normal = &H1
    Severe = &H2
    Critical = &H3
End Enum

Public Class ServiceInstaller
    Private Const STANDARD_RIGHTS_REQUIRED As Integer = &HF0000
    Private Const SERVICE_WIN32_OWN_PROCESS As Integer = &H10
    Private Const SERVICE_INTERACTIVE_PROCESS As Integer = &H100

    Private Const SERVICE_CONFIG_DESCRIPTION As Integer = &H1

    Private Const SERVICE_NO_CHANGE As UInteger = &HFFFFFFFFUI


    <StructLayout(LayoutKind.Sequential)> _
    Private Class SERVICE_STATUS
        Public dwServiceType As Integer = 0
        Public dwCurrentState As ServiceState = 0
        Public dwControlsAccepted As Integer = 0
        Public dwWin32ExitCode As Integer = 0
        Public dwServiceSpecificExitCode As Integer = 0
        Public dwCheckPoint As Integer = 0
        Public dwWaitHint As Integer = 0
    End Class

    <DllImport("advapi32.dll", EntryPoint:="OpenSCManagerA")> _
    Private Shared Function OpenSCManager(lpMachineName As String, lpDatabaseName As String, dwDesiredAccess As ServiceManagerRights) As IntPtr
    End Function
    <DllImport("advapi32.dll", EntryPoint:="OpenServiceA", CharSet:=CharSet.Ansi)> _
    Private Shared Function OpenService(hSCManager As IntPtr, lpServiceName As String, dwDesiredAccess As ServiceRights) As IntPtr
    End Function
    <DllImport("advapi32.dll", EntryPoint:="CreateServiceA")> _
    Private Shared Function CreateService(hSCManager As IntPtr, lpServiceName As String, lpDisplayName As String, dwDesiredAccess As ServiceRights, dwServiceType As Integer, dwStartType As ServiceBootFlag, dwErrorControl As ServiceError, lpBinaryPathName As String, lpLoadOrderGroup As String, lpdwTagId As IntPtr, lpDependencies As String, lpServiceStartName As String, lpPassword As String) As IntPtr
    End Function
    <DllImport("advapi32.dll")> _
    Private Shared Function CloseServiceHandle(hSCObject As IntPtr) As Integer
    End Function
    <DllImport("advapi32.dll")> _
    Private Shared Function QueryServiceStatus(hService As IntPtr, lpServiceStatus As SERVICE_STATUS) As Integer
    End Function
    <DllImport("advapi32.dll", SetLastError:=True)> _
    Private Shared Function DeleteService(hService As IntPtr) As Integer
    End Function
    <DllImport("advapi32.dll")> _
    Private Shared Function ControlService(hService As IntPtr, dwControl As ServiceControl, lpServiceStatus As SERVICE_STATUS) As Integer
    End Function
    <DllImport("advapi32.dll", EntryPoint:="StartServiceA")> _
    Private Shared Function StartService(hService As IntPtr, dwNumServiceArgs As Integer, lpServiceArgVectors As Integer) As Integer
    End Function
    <DllImport("advapi32.dll")>
    Private Shared Function ChangeServiceConfig2(hService As IntPtr, dwInfoLevel As Integer, lpInfo As IntPtr) As Integer
    End Function
    <DllImport("advapi32.dll")>
    Private Shared Function ChangeServiceConfig(ByVal hService As Integer, ByVal dwServiceType As UInt32, ByVal dwStartType As UInt32, ByVal dwErrorControl As UInt32, ByVal lpBinaryPathName As String, ByVal lpLoadOrderGroup As String, ByVal lpdwTagId As Integer, ByVal lpDependencies As String, ByVal lpServiceStartName As String, ByVal lpPassword As String, ByVal lpDisplayName As String) As Boolean
    End Function


    Public Sub New()
    End Sub

    Public Shared Sub Uninstall(ServiceName As String)
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect)
        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.StandardRightsRequired Or ServiceRights.[Stop] Or ServiceRights.QueryStatus)
            If service = IntPtr.Zero Then
                Throw New ApplicationException("Service not installed.")
            End If
            Try
                StopService(service)
                Dim ret As Integer = DeleteService(service)
                If ret = 0 Then
                    Dim [error] As Integer = Marshal.GetLastWin32Error()
                    Throw New ApplicationException("Could not delete service " + [error])
                End If
            Finally
                CloseServiceHandle(service)
            End Try
        Finally
            CloseServiceHandle(scman)
        End Try
    End Sub

    Public Shared Function ServiceIsInstalled(ServiceName As String) As Boolean
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect)
        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryStatus)
            If service = IntPtr.Zero Then
                Return False
            End If
            CloseServiceHandle(service)
            Return True
        Finally
            CloseServiceHandle(scman)
        End Try
    End Function

    Public Shared Sub InstallService(ServiceName As String, DisplayName As String, FileName As String)
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect Or ServiceManagerRights.CreateService)
        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryStatus Or ServiceRights.Start)
            If service = IntPtr.Zero Then
                service = CreateService(scman, ServiceName, DisplayName, ServiceRights.QueryStatus Or ServiceRights.Start, SERVICE_WIN32_OWN_PROCESS, ServiceBootFlag.AutoStart, _
                    ServiceError.Normal, FileName, Nothing, IntPtr.Zero, Nothing, Nothing, Nothing)
            End If
            If service = IntPtr.Zero Then
                Throw New ApplicationException("Failed to install service.")
            End If
        Finally
            CloseServiceHandle(scman)
        End Try
    End Sub

    Public Shared Sub InstallServiceAndStart(ServiceName As String, DisplayName As String, FileName As String)
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect Or ServiceManagerRights.CreateService)
        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryStatus Or ServiceRights.Start)
            If service = IntPtr.Zero Then
                service = CreateService(scman, ServiceName, DisplayName, ServiceRights.QueryStatus Or ServiceRights.Start, SERVICE_WIN32_OWN_PROCESS, ServiceBootFlag.AutoStart, _
                    ServiceError.Normal, FileName, Nothing, IntPtr.Zero, Nothing, Nothing, Nothing)
            End If
            If service = IntPtr.Zero Then
                Throw New ApplicationException("Failed to install service.")
            End If
            Try
                StartService(service)
            Finally
                CloseServiceHandle(service)
            End Try
        Finally
            CloseServiceHandle(scman)
        End Try
    End Sub


    Public Shared Sub StartService(Name As String)
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect)
        Try
            Dim hService As IntPtr = OpenService(scman, Name, ServiceRights.QueryStatus Or ServiceRights.Start)
            If hService = IntPtr.Zero Then
                Throw New ApplicationException("Could not open service.")
            End If
            Try
                StartService(hService)
            Finally
                CloseServiceHandle(hService)
            End Try
        Finally
            CloseServiceHandle(scman)
        End Try
    End Sub

    Public Shared Sub StopService(Name As String)
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect)
        Try
            Dim hService As IntPtr = OpenService(scman, Name, ServiceRights.QueryStatus Or ServiceRights.[Stop])
            If hService = IntPtr.Zero Then
                Throw New ApplicationException("Could not open service.")
            End If
            Try
                StopService(hService)
            Finally
                CloseServiceHandle(hService)
            End Try
        Finally
            CloseServiceHandle(scman)
        End Try
    End Sub

    Public Shared Sub ChangeServiceDescription(Name As String, Description As String)
        Try
            Dim regRoot As Microsoft.Win32.RegistryKey

            regRoot = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services\" & Name, True)

            If regRoot Is Nothing Then
                Exit Sub
            End If

            regRoot.SetValue("Description", Description, Microsoft.Win32.RegistryValueKind.String)
        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub StartService(hService As IntPtr)
        Dim status As New SERVICE_STATUS()
        StartService(hService, 0, 0)
        WaitForServiceStatus(hService, ServiceState.Starting, ServiceState.Run)
    End Sub

    Private Shared Sub StopService(hService As IntPtr)
        Dim status As New SERVICE_STATUS()
        ControlService(hService, ServiceControl.[Stop], status)
        WaitForServiceStatus(hService, ServiceState.Stopping, ServiceState.[Stop])
    End Sub

    Public Shared Function GetServiceStatus(ServiceName As String) As ServiceState
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.Connect)
        Try
            Dim hService As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryStatus)
            If hService = IntPtr.Zero Then
                Return ServiceState.NotFound
            End If
            Try
                Return GetServiceStatus(hService)
            Finally
                CloseServiceHandle(scman)
            End Try
        Finally
            CloseServiceHandle(scman)
        End Try
    End Function

    Private Shared Function GetServiceStatus(hService As IntPtr) As ServiceState
        Dim ssStatus As New SERVICE_STATUS()
        If QueryServiceStatus(hService, ssStatus) = 0 Then
            Throw New ApplicationException("Failed to query service status.")
        End If
        Return ssStatus.dwCurrentState
    End Function

    Private Shared Function WaitForServiceStatus(hService As IntPtr, WaitStatus As ServiceState, DesiredStatus As ServiceState) As Boolean
        Dim ssStatus As New SERVICE_STATUS()
        Dim dwOldCheckPoint As Integer
        Dim dwStartTickCount As Integer

        QueryServiceStatus(hService, ssStatus)
        If ssStatus.dwCurrentState = DesiredStatus Then
            Return True
        End If
        dwStartTickCount = Environment.TickCount
        dwOldCheckPoint = ssStatus.dwCheckPoint

        While ssStatus.dwCurrentState = WaitStatus
            ' Do not wait longer than the wait hint. A good interval is
            ' one tenth the wait hint, but no less than 1 second and no
            ' more than 10 seconds.

            Dim dwWaitTime As Integer = ssStatus.dwWaitHint / 10

            If dwWaitTime < 1000 Then
                dwWaitTime = 1000
            ElseIf dwWaitTime > 10000 Then
                dwWaitTime = 10000
            End If

            System.Threading.Thread.Sleep(dwWaitTime)

            ' Check the status again.

            If QueryServiceStatus(hService, ssStatus) = 0 Then
                Exit While
            End If

            If ssStatus.dwCheckPoint > dwOldCheckPoint Then
                ' The service is making progress.
                dwStartTickCount = Environment.TickCount
                dwOldCheckPoint = ssStatus.dwCheckPoint
            Else
                If Environment.TickCount - dwStartTickCount > ssStatus.dwWaitHint Then
                    ' No progress made within the wait hint
                    Exit While
                End If
            End If
        End While
        Return (ssStatus.dwCurrentState = DesiredStatus)
    End Function

    Private Shared Function OpenSCManager(Rights As ServiceManagerRights) As IntPtr
        Dim scman As IntPtr = OpenSCManager(Nothing, Nothing, Rights)
        If scman = IntPtr.Zero Then
            Throw New ApplicationException("Could not connect to service control manager.")
        End If
        Return scman
    End Function

    Public Shared Function ChangeStartupType(ServiceName As String, dwStartType As ServiceBootFlag) As String
        Dim sRet As String = ""
        Dim scman As IntPtr = OpenSCManager(ServiceManagerRights.AllAccess)

        Try
            Dim service As IntPtr = OpenService(scman, ServiceName, ServiceRights.QueryConfig Or ServiceRights.ChangeConfig)
            If service = IntPtr.Zero Then
                Throw New ApplicationException("Failed to open service '" & ServiceName & "'.")
            End If

            If Not ChangeServiceConfig(service, SERVICE_NO_CHANGE, dwStartType, SERVICE_NO_CHANGE, Nothing, Nothing, IntPtr.Zero, Nothing, Nothing, Nothing, Nothing) Then
                Throw New System.Runtime.InteropServices.ExternalException("Could not change StartType for service '" & ServiceName & "'.")
            End If

            CloseServiceHandle(service)
        Catch ex As Exception
            sRet = ex.GetType().ToString & " - " & ex.Message
        Finally
            CloseServiceHandle(scman)
        End Try

        Return sRet
    End Function
End Class
