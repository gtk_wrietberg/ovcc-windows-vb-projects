﻿Module modMain
    Public Sub Main()
        Globals.InitGlobals()

        Globals.Logger.LogFileDirectory = Globals.LogFileDirectory
        Globals.Logger.LogFileName = Globals.LogFileName
        Globals.Logger.Debugging = Globals.Settings.Debugging


        Globals.Logger.WriteMessage(New String("*", 50))
        Globals.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Globals.Logger.WriteMessage("installer started")

        '-----------------------------------

        'Copy files
        '------------------------------------------------------
        Dim oCopyFiles As CopyFiles = New CopyFiles
        oCopyFiles.SuppressCopyLogging = False
        oCopyFiles.BackupDirectory = Globals.BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        Globals.Logger.WriteMessage("Copy files", 0)
        oCopyFiles.CopyFiles()


        'Save default settings
        Globals.Settings.SaveSettings()


        'Install services
        Globals.Logger.WriteMessage("Install services", 0)

        InstallService(Globals.CONSTANTS__SERVICE_NAME_HEARTBEAT, Globals.CONSTANTS__SERVICE_DISPLAYNAME_HEARTBEAT, Globals.CONSTANTS__SERVICE_DESCRIPTION_HEARTBEAT, Globals.FILE_AND_FOLDERS__ServicePath & "\" & Globals.FILE_AND_FOLDERS__AgentHeartbeattService)
        InstallService(Globals.CONSTANTS__SERVICE_NAME_LOGFILE, Globals.CONSTANTS__SERVICE_DISPLAYNAME_LOGFILE, Globals.CONSTANTS__SERVICE_DESCRIPTION_LOGFILE, Globals.FILE_AND_FOLDERS__ServicePath & "\" & Globals.FILE_AND_FOLDERS__AgentLogfileService)


        'Not working at the moment, so disable these services for now
        DisableService(Globals.CONSTANTS__SERVICE_NAME_HEARTBEAT)
        DisableService(Globals.CONSTANTS__SERVICE_NAME_LOGFILE)


        'Done
        Globals.Logger.WriteMessage("Done", 0)
        Globals.Logger.WriteMessage("ok, bye", 1)
    End Sub

    Private Sub DisableService(Name As String)
        Globals.Logger.WriteMessage(Name, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            Globals.Logger.WriteMessage("changing startup type", 2)
            Globals.Logger.WriteMessage(ServiceBootFlag.Disabled.ToString, 3)
            ServiceInstaller.ChangeStartupType(Name, ServiceBootFlag.Disabled)

            Threading.Thread.Sleep(5000)

            Globals.Logger.WriteMessage("done", 2)
        Else
            Globals.Logger.WriteMessage("not found!", 2)
        End If
    End Sub

    Private Sub InstallService(Name As String, DisplayName As String, Description As String, ServicePath As String)
        Globals.Logger.WriteMessage(Name, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            Globals.Logger.WriteMessage("already exists", 2)
            Globals.Logger.WriteMessage("stopping", 3)
            ServiceInstaller.StopService(Name)

            Threading.Thread.Sleep(5000)

            Globals.Logger.WriteMessage("uninstalling", 3)
            ServiceInstaller.Uninstall(Name)
        End If

        Threading.Thread.Sleep(5000)

        If Not ServiceInstaller.ServiceIsInstalled(Name) Then
            Globals.Logger.WriteMessage("ok", 3)
        Else
            Globals.Logger.WriteError("fail", 3)
        End If

        Globals.Logger.WriteMessage("installing", 2)
        ServiceInstaller.InstallServiceAndStart(Name, DisplayName, ServicePath)

        Threading.Thread.Sleep(5000)

        Globals.Logger.WriteMessage("set description", 2)
        ServiceInstaller.ChangeServiceDescription(Name, Description)




        If ServiceInstaller.ServiceIsInstalled(Name) Then
            Globals.Logger.WriteMessage("ok", 3)
            If ServiceInstaller.GetServiceStatus(Name) = ServiceState.Run Then
                Globals.Logger.WriteMessage("and it's running!", 4)
            ElseIf ServiceInstaller.GetServiceStatus(Name) = ServiceState.Starting Then
                Globals.Logger.WriteMessage("and it's starting!", 4)
            Else
                Globals.Logger.WriteWarning("but it's not running!", 4)
            End If
        Else
            Globals.Logger.WriteError("fail", 3)
        End If
    End Sub
End Module
