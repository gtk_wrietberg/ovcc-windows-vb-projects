﻿Public Class frmMain
    Private ReadOnly c_INSTALLER_OVCC As String = "OVCCSoftwareInstaller.exe"
    Private ReadOnly c_INSTALLER_DOTNET As String = "NDP451-KB2859818-Web.exe"

    Private ReadOnly c_TEXT__OK As String = ".NET 4.5.1 or higher is installed" & vbCrLf & vbCrLf & "Continuing installation"
    Private ReadOnly c_TEXT__NOK As String = ".NET 4.5.1 or higher is NOT installed" & vbCrLf & vbCrLf & "Starting .NET 4.5.1 install"


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If dotNetFramework.Is__4_5_1__installed Then
            lblText.Text = c_TEXT__OK
            tmrOk.Enabled = True
        Else
            lblText.Text = c_TEXT__NOK
            tmrNOk.Enabled = True
        End If
    End Sub

    Private Sub tmrOk_Tick(sender As Object, e As EventArgs) Handles tmrOk.Tick
        tmrOk.Enabled = False

        Process.Start(c_INSTALLER_OVCC, String.Join(" ", Environment.GetCommandLineArgs))

        tmrExit.Enabled = True
    End Sub

    Private Sub tmrNOk_Tick(sender As Object, e As EventArgs) Handles tmrNOk.Tick
        tmrNOk.Enabled = False

        Process.Start(c_INSTALLER_DOTNET, "/passive /showrmui /promptrestart")

        tmrExit.Enabled = True
    End Sub

    Private Sub tmrExit_Tick(sender As Object, e As EventArgs) Handles tmrExit.Tick
        Me.Close()
    End Sub

    Private Sub StartProcess(sFile As String, sParams As String)

    End Sub
End Class