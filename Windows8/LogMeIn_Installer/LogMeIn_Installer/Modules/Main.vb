
Module Main
    Private WithEvents oProcess As ProcessRunner
    Private oLogger As Logger
    Private oComputerName As ComputerName

    Private ReadOnly cParamName_DeployId As String = "deployid="

    ' Private ReadOnly cLogMeIn_Path As String = "C:\Program Files\iBAHN\installation_files\LogMeIn\files\EngineeringLogMeIn.msi"
    'Private ReadOnly cLogMeIn_Path_Base As String = "%%PROGRAM FILES%%\GuestTek\LobbyPCSoftware\installation_files\Resources\unpacked\LogMeIn"
    Private ReadOnly cLogMeIn_Path_Base As String = "%%PROGRAM FILES%%\GuestTek\OVCCSoftware\installation_files\Resources\unpacked\LogMeIn"
    Private ReadOnly cLogMeIn_Path_MSI As String = "\files\LogMeIn.msi"

    Public Sub Main()
        Dim sLogMeInPath As String

        sLogMeInPath = LogMeInInstallerPath()


        '--------------------
        oProcess = New ProcessRunner
        oComputerName = New ComputerName

        oLogger = New Logger
        oLogger.LogFilePath = LogMeInInstallerLogPath()
        oLogger.LogFileName = LogMeInInstallerLogFile()

        oComputerName = New ComputerName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        oLogger.WriteToLog("Computer name: " & oComputerName.ComputerName)

        oLogger.WriteToLog(New String("-", 10))



        'Command line params
        Dim sDeployId As String = ""

        For Each arg As String In Environment.GetCommandLineArgs()
            If InStr(arg, cParamName_DeployId) > 0 Then
                sDeployId = arg.Replace(cParamName_DeployId, "")
            End If
        Next

        oLogger.WriteToLog("Initializing", , 0)
        If sDeployId = "" Then
            oLogger.WriteToLog("Deploy Id is empty!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            ExitApplication(1)
        Else
            oLogger.WriteToLog("Deploying with Deploy Id: " & sDeployId, , 1)
        End If


        Dim sParams As String

        'msiexec.exe /i EngineeringLogMeIn.msi /quiet DEPLOYID=01_ly2bqho5b6exj49y4hdi5s392c55sd48ebpxx INSTALLMETHOD=5 FQDNDESC=1
        sParams = "/i """ & sLogMeInPath & cLogMeIn_Path_MSI & """ /quiet DEPLOYID=" & sDeployId & " INSTALLMETHOD=5 FQDNDESC=1 /l* """ & sLogMeInPath & cLogMeIn_Path_MSI & ".log"""

        oLogger.WriteToLog("Installing LogMeIn")
        oLogger.WriteToLog("Path   : msiexec.exe", , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 1800", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = "msiexec.exe"
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1800
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()

        Do
            System.Threading.Thread.Sleep(1000)
        Loop Until oProcess.IsProcessDone

        If oProcess.ProcessTimedOut Then
            ExitApplication(2)
        End If

        If oProcess.ErrorsOccurred Then
            ExitApplication(3)
        End If

        ExitApplication(0)
    End Sub

    Private Function LogMeInInstallerPath() As String
        Dim sTmpPath As String = ""

        Dim sTmp As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sTmp.Equals(String.Empty) Then
            sTmp = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        sTmpPath = cLogMeIn_Path_Base.Replace("%%PROGRAM FILES%%", sTmp)


        Return sTmpPath
    End Function

    Private Function LogMeInInstallerLogPath() As String
        Dim sPath As String = ""
        Dim sTmp As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sTmp.Equals(String.Empty) Then
            sTmp = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        sPath = sTmp & "\GuestTek\_logs"
        sPath &= "\" & Application.ProductName & "\" & Application.ProductVersion


        Try
            IO.Directory.CreateDirectory(sPath)
        Catch ex As Exception

        End Try

        Return sPath
    End Function

    Private Function LogMeInInstallerLogFile() As String
        Dim dDate As Date = Now()
        Dim sName As String = ""

        sName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        Return sName
    End Function

    Public Sub ExitApplication(ByVal iExitCode As Integer)
        Try
            oLogger.WriteToLog("Exiting...", Logger.MESSAGE_TYPE.LOG_DEFAULT, 0)
            oLogger.WriteToLog("exit code: " & iExitCode & " (" & iExitCode.ToString & ")", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            oLogger.WriteToLog("bye", Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)
            oLogger.WriteToLogWithoutDate(New String("=", 100))
        Catch ex As Exception
        End Try

        System.Environment.Exit(iExitCode)
    End Sub

    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLog("done", , 1)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLog("failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLog("started", , 1)
        oLogger.WriteToLog("pid: " & EventProcess.Id.ToString, , 1)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLog("timed out", , 1)
        oProcess.ProcessDone()
    End Sub
End Module
