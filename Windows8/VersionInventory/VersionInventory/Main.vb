﻿Module Main
    Public Sub Main()
        Dim is64bit As Boolean = Environment.Is64BitOperatingSystem

        Helpers.Logger.InitialiseLogger()

        Helpers.Logger.WriteMessage(New String("*", 50))
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.WriteMessage("started")


        Helpers.Logger.WriteMessage("initialising")


        'exes
        Helpers.Logger.WriteMessage("exes", 1)
        GetInfo.Exes.Init()
        GetInfo.Exes.Add("SiteKiosk", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "SiteKiosk\SiteKiosk.exe"))
        GetInfo.Exes.Add("ChromeWrapper", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\ChromeWrapper\ChromeWrapper.exe"))
        If IO.File.Exists(IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "Google\Chrome\Application\Chrome.exe")) Then
            GetInfo.Exes.Add("Chrome", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "Google\Chrome\Application\Chrome.exe"))
        Else
            GetInfo.Exes.Add("Chrome", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder64bit(), "Google\Chrome\Application\Chrome.exe"))
        End If
        GetInfo.Exes.Add("FileExplorer", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\FileExplorer\FileExplorer.exe"))
        GetInfo.Exes.Add("OVCCWatchdog", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\OVCCWatchdog\OVCCWatchdog_service.exe"))
        GetInfo.Exes.Add("OVCCInformationUpdater", IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\OVCCSoftware\tools\OVCCInformationUpdater.exe"))

        'users
        Helpers.Logger.WriteMessage("users", 1)
        GetInfo.AdminAccount.Users.Init()
        GetInfo.AdminAccount.Users.Add("ibahn")
        GetInfo.AdminAccount.Users.Add("guesttek")
        GetInfo.AdminAccount.Users.Add("guest-tek")
        GetInfo.AdminAccount.Users.Add("sitekiosk")

        'passwords (obfuscated with v2)
        Helpers.Logger.WriteMessage("passwords", 1)
        GetInfo.AdminAccount.Passwords.Init()
        GetInfo.AdminAccount.Passwords.Add("Hilton_recent", "58091155545d2979253021053e4b4c140e045c004a545e060f03064016080b0e59560408150256512b7c246173053e4b11115d035153115b53560d0a0d171d585b540c5e535f4a5e54517d76276b7200674e11175f050a0717035d040f505114150c08590a5158534a5e5a057379")
        GetInfo.AdminAccount.Passwords.Add("Hilton_oldH", "530f10545201282b2766265d3a494a135e535a56435053540d510c164609500d5e5f040e150600512e7b233026546e4d4f105b500f57105609575756521314085f5d5c5f070c11550600297b203777533d4e4a1a0c550b0610060f535a525211100b510f0950025b4b505a577f7d")
        GetInfo.AdminAccount.Passwords.Add("Hilton_oldK", "565b47525407282974302202694b4f405a5f5d534a5052525e035746410b080a585f525d450351537276236774056b191e165d070f56405b53015f550d1a105f590f0c5e040b475403537377243173556a1b18475f5f0c0316015d015d560513165d0c0a5853030c460600507d77")
        GetInfo.AdminAccount.Passwords.Add("GuestTek_recent", "535b435707072c2a243026506d1f181551555b0045035a060c0104141253505b0a51555b425003007f7d703625076e191a13500051014b0609035a070345425d5d5c5855560e105554567b2d226222506d4b1b120d57580645040956585202151c5f0b0d5150050f420256572f76")
        GetInfo.AdminAccount.Passwords.Add("SiteKiosk_recent", "565a44515707797a233623563e4e1d130b5e5f0615565c0459575742450c5955515e515f12565b567b76773722506b181d155e525f57100108060a0a061a460859580b52540e435203517329726b26056b4e4c170e025e51460052525c010745155f5c5b0e53595c155e515d7329")
        GetInfo.AdminAccount.Passwords.Add("SiteKiosk_old", "595c10045602792c22647600691d1b110c070d544a0652030f0b0d11170b515d0957550e465f55072c7a226424523d1e4f405f515d0111575d505f00004145530b0a0e54075a100203517c2a256321516e1f1a4058000b5212500c530c030d42130e590e0e50575d46565601737c")

        'make sure registry keys exist
        GetInfo.Registry.CreateKeys()


        'Version numbers
        Dim sName As String = "", sPath As String = "", sVersionFull As String = "", sVersionMajor As String = "", sVersionMajorMinor As String = ""

        Helpers.Logger.WriteMessage("getting versions")

        While GetInfo.Exes.GetNext(sName, sPath)
            Helpers.Logger.WriteMessage(sName, 1)
            Helpers.Logger.WriteMessage(sPath, 2)

            sVersionFull = Helpers.FilesAndFolders.GetFileVersion(sPath)
            sVersionMajor = Helpers.FilesAndFolders.GetFileVersion(sPath, 1)
            sVersionMajorMinor = Helpers.FilesAndFolders.GetFileVersion(sPath, GetInfo.Constants.ShortVersionParts)
            Helpers.Logger.WriteMessage("full version", 3)
            Helpers.Logger.WriteMessage(sVersionFull, 4)
            Helpers.Logger.WriteMessage("major version", 3)
            Helpers.Logger.WriteMessage(sVersionMajor, 4)
            Helpers.Logger.WriteMessage("major.minor version", 3)
            Helpers.Logger.WriteMessage(sVersionMajorMinor, 4)

            GetInfo.Registry.WriteAppVersion(sName, sVersionFull, sVersionMajor, sVersionMajorMinor)
        End While

        'self
        sName = My.Application.Info.ProductName
        sPath = Application.ExecutablePath

        Helpers.Logger.WriteMessage(My.Application.Info.ProductName, 1)
        Helpers.Logger.WriteMessage(Application.ExecutablePath, 2)

        sVersionFull = My.Application.Info.Version.ToString
        sVersionMajor = Helpers.FilesAndFolders.ParseFileVersion(sVersionFull, 1)
        sVersionMajorMinor = Helpers.FilesAndFolders.ParseFileVersion(sVersionFull, GetInfo.Constants.ShortVersionParts)
        Helpers.Logger.WriteMessage("full version", 3)
        Helpers.Logger.WriteMessage(sVersionFull, 4)
        Helpers.Logger.WriteMessage("major version", 3)
        Helpers.Logger.WriteMessage(sVersionMajor, 4)
        Helpers.Logger.WriteMessage("major.minor version", 3)
        Helpers.Logger.WriteMessage(sVersionMajorMinor, 4)

        GetInfo.Registry.WriteAppVersion(sName, sVersionFull, sVersionMajor, sVersionMajorMinor)


        'Accounts
        Dim sUserName As String = "", sPassName As String = "", sPass As String = "", sError As String = ""
        Dim bUserFound As Boolean = False, bPasswordFound As Boolean = False

        Helpers.Logger.WriteMessage("getting account info")

        While GetInfo.AdminAccount.Users.GetNext(sUserName)
            bUserFound = False
            bPasswordFound = False

            Helpers.Logger.WriteMessage(sUserName, 1)
            If WindowsUser.FindUser(sUserName) Then
                bUserFound = True
                GetInfo.AdminAccount.Passwords.Reset()

                Helpers.Logger.WriteMessage("exists", 2)

                GetInfo.Registry.WriteAccountInfo(sUserName, "", "")

                Helpers.Logger.WriteMessage("checking passwords", 2)

                While GetInfo.AdminAccount.Passwords.GetNext(sPassName, sPass)
                    Helpers.Logger.WriteMessage(sPassName, 3)

                    If WindowsUser.CheckLogon(sUserName, sPass, sError) Then
                        Helpers.Logger.WriteMessage("yep", 4)

                        bPasswordFound = True

                        Exit While
                    Else
                        Helpers.Logger.WriteMessage("nope", 4)
                    End If
                End While
            End If


            If bUserFound Then
                If bPasswordFound Then
                    GetInfo.Registry.WriteAccountInfo(sUserName, sPassName, sPass)
                Else
                    GetInfo.Registry.WriteAccountInfo(sUserName, "no_match", "")
                End If
            Else
                GetInfo.Registry.WriteAccountInfo(sUserName, "not_found", "")
            End If
        End While


        'sitekiosk
        Dim sLastCfg As String = "", sLastCfgDate As String = "", sLastCfgDateUTC As String = "", sTheme As String = ""

        Helpers.Logger.WriteMessage("getting sitekiosk info")

        sLastCfg = GetInfo.SiteKiosk.Config.LastCfg
        sLastCfgDate = GetInfo.SiteKiosk.Config.LastCfgDate
        sLastCfgDateUTC = GetInfo.SiteKiosk.Config.LastCfgDateUTC
        sTheme = GetInfo.SiteKiosk.Theme.Name

        Helpers.Logger.WriteMessage("LastCfg", 1)
        Helpers.Logger.WriteMessage(sLastCfg, 2)
        Helpers.Logger.WriteMessage("LastCfgDate", 1)
        Helpers.Logger.WriteMessage(sLastCfgDate, 2)
        Helpers.Logger.WriteMessage("LastCfgDateUTC", 1)
        Helpers.Logger.WriteMessage(sLastCfgDateUTC, 2)
        Helpers.Logger.WriteMessage("Theme Name", 1)
        Helpers.Logger.WriteMessage(sTheme, 2)

        GetInfo.Registry.WriteSiteKioskInfo(sLastCfg, sLastCfgDate, sLastCfgDateUTC, sTheme)


        'machinename
        Dim sMachineName As String = "", sMachineNameFromScript As String = ""

        Helpers.Logger.WriteMessage("getting machinename")

        sMachineName = GetInfo.MachineName.Name
        sMachineNameFromScript = GetInfo.MachineName.NameFromScript

        Helpers.Logger.WriteMessage("MachineName", 1)
        Helpers.Logger.WriteMessage(sMachineName, 2)
        Helpers.Logger.WriteMessage("MachineNameFromScript", 1)
        Helpers.Logger.WriteMessage(sMachineNameFromScript, 2)

        GetInfo.Registry.WriteMachineName(sMachineName, sMachineNameFromScript)


        'last check date
        Helpers.Logger.WriteMessage("writing last check date")
        GetInfo.Registry.WriteLastCheck()


        Helpers.Logger.Write("done", , 0)
        Helpers.Logger.Write("bye", , 1)
    End Sub
End Module
