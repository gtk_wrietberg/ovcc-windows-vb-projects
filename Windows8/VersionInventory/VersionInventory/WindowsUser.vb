﻿Imports System.DirectoryServices.AccountManagement
Imports System.Runtime.InteropServices
Imports System.Security.Principal

Public Class WindowsUser
    Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String], ByVal lpszDomain As [String], ByVal lpszPassword As [String], ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Boolean
    <DllImport("kernel32.dll")> Public Shared Function FormatMessage(ByVal dwFlags As Integer, ByRef lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByRef lpBuffer As [String], ByVal nSize As Integer, ByRef Arguments As IntPtr) As Integer
    End Function

    'The CloseHandle function closes the handle to an open object such as an Access token.
    Public Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean

    Private Const ADS_UF_DONT_EXPIRE_PASSWD = &H10000

    Private Const SID_USERS As String = "S-1-5-32-545"
    Private Const SID_ADMINISTRATORS As String = "S-1-5-32-544"


    Public Shared Function GetAdministratorsGroupName() As String
        Return GetGroupNameFromSid(SID_ADMINISTRATORS)
    End Function

    Public Shared Function GetUsersGroupName() As String
        Return GetGroupNameFromSid(SID_USERS)
    End Function

    Private Shared Function GetGroupNameFromSid(ByVal _sid As String) As String
        Dim context As PrincipalContext, group As GroupPrincipal

        context = New PrincipalContext(ContextType.Machine)
        group = GroupPrincipal.FindByIdentity(context, IdentityType.Sid, _sid)

        Return group.SamAccountName
    End Function

    Public Shared Function GetAdminUsers() As List(Of String)
        Dim sGroup As String
        Dim lUsers As New List(Of String)

        Try
            Dim oUser As Object, oGroup As Object

            sGroup = GetAdministratorsGroupName()
            oGroup = GetObject("WinNT://" & Environment.MachineName & "/" & sGroup)

            For Each oUser In oGroup.Members
                lUsers.Add(oUser.Name.ToString())
            Next

            oGroup = Nothing
            oUser = Nothing
        Catch ex As Exception

        End Try

        Return lUsers
    End Function

    Public Shared Function FindUser(ByVal sUsername As String, Optional ByVal sGroup As String = "Users") As Boolean
        Try
            Dim oUser As Object, oGroup As Object

            oGroup = GetObject("WinNT://" & Environment.MachineName & "/" & sGroup)

            For Each oUser In oGroup.Members
                Try
                    If Trim(sUsername.ToLower) = Trim(oUser.Name.ToString().ToLower) Then
                        Return True
                    End If
                Catch ex As Exception
                    Return False
                End Try
            Next

            oGroup = Nothing
            oUser = Nothing

            'Return True
        Catch ex As Exception
            'Return False
        End Try

        Return False
    End Function

    Public Shared Function CheckLogon(sUsername As String, sPassword As String, ByRef ErrorMessage As String) As Boolean
        Dim bRet As Boolean = False

        Try
            Dim tokenHandle As New IntPtr(0)
            Dim UserName, MachineName, Pwd As String
            'The MachineName property gets the name of your computer.
            MachineName = System.Environment.MachineName
            UserName = sUsername
            Pwd = sPassword

            Const LOGON32_PROVIDER_DEFAULT As Integer = 0
            Const LOGON32_LOGON_INTERACTIVE As Integer = 2
            tokenHandle = IntPtr.Zero
            'Call the LogonUser function to obtain a handle to an access token.
            Dim returnValue As Boolean = LogonUser(UserName, MachineName, Pwd, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, tokenHandle)

            If returnValue = False Then
                'This function returns the error code that the last unmanaged function returned.
                Dim ret As Integer = Marshal.GetLastWin32Error()
                ErrorMessage = GetErrorMessage(ret)
                bRet = False
            Else
                'Create the WindowsIdentity object for the Windows user account that is
                'represented by the tokenHandle token.
                Dim newId As New WindowsIdentity(tokenHandle)
                Dim userperm As New WindowsPrincipal(newId)
                'Verify whether the Windows user has administrative credentials.
                If userperm.IsInRole(WindowsBuiltInRole.Administrator) Then
                    ErrorMessage = "ok"
                Else
                    ErrorMessage = "ok, but not really an admin admin"
                End If

                bRet = True
            End If

            'Free the access token.
            If Not System.IntPtr.op_Equality(tokenHandle, IntPtr.Zero) Then
                CloseHandle(tokenHandle)
            End If
        Catch ex As Exception
            ErrorMessage = "Exception occurred. " + ex.Message
        End Try

        Return bRet
    End Function

    Private Shared Function GetErrorMessage(ByVal errorCode As Integer) As String
        Dim FORMAT_MESSAGE_ALLOCATE_BUFFER As Integer = &H100
        Dim FORMAT_MESSAGE_IGNORE_INSERTS As Integer = &H200
        Dim FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000

        Dim msgSize As Integer = 255
        Dim lpMsgBuf As String = ""
        Dim dwFlags As Integer = FORMAT_MESSAGE_ALLOCATE_BUFFER Or FORMAT_MESSAGE_FROM_SYSTEM Or FORMAT_MESSAGE_IGNORE_INSERTS

        Dim lpSource As IntPtr = IntPtr.Zero
        Dim lpArguments As IntPtr = IntPtr.Zero
        'Call the FormatMessage function to format the message.
        Dim returnVal As Integer = FormatMessage(dwFlags, lpSource, errorCode, 0, lpMsgBuf,
                msgSize, lpArguments)
        If returnVal = 0 Then
            Throw New Exception("Failed to format message for error code " + errorCode.ToString() + ". ")
        End If
        Return lpMsgBuf
    End Function
End Class
