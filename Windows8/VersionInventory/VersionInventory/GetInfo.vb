﻿Public Class GetInfo
    Public Class Constants
        Public Shared ReadOnly ShortVersionParts As Integer = 2
    End Class

    Public Class Registry
        Public Class Keys_Values
            Public Shared ReadOnly KEY__ROOT As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\VersionInventory"
            Public Shared ReadOnly VALUE__LastCheck As String = "LastCheck"
            Public Shared ReadOnly VALUE__LastCheckUTC As String = "LastCheckUTC"

            Public Shared ReadOnly KEY__VERSIONS As String = KEY__ROOT & "\Versions"
            Public Shared ReadOnly KEY__VERSIONS_FULL As String = KEY__VERSIONS & "\Full"
            Public Shared ReadOnly KEY__VERSIONS_MAJOR As String = KEY__VERSIONS & "\Major"
            Public Shared ReadOnly KEY__VERSIONS_MAJORMINOR As String = KEY__VERSIONS & "\MajorMinor"

            Public Shared ReadOnly KEY__ACCOUNTS As String = KEY__ROOT & "\Accounts"
            Public Shared ReadOnly VALUE__PasswordName As String = "PasswordName"
            Public Shared ReadOnly VALUE__PasswordString As String = "PasswordString"

            Public Shared ReadOnly KEY__SITEKIOSK As String = KEY__ROOT & "\SiteKiosk"
            Public Shared ReadOnly VALUE__LastCfg As String = "LastCfg"
            Public Shared ReadOnly VALUE__LastCfgDate As String = "LastCfgDate"
            Public Shared ReadOnly VALUE__LastCfgDateUTC As String = "LastCfgDateUTC"
            Public Shared ReadOnly VALUE__Theme As String = "Theme"

            Public Shared ReadOnly KEY__MACHINENAME As String = KEY__ROOT & "\MachineName"
            Public Shared ReadOnly VALUE__MachineName As String = "Name"
            Public Shared ReadOnly VALUE__MachineNameFromScript As String = "NameFromScript"
        End Class

        Public Shared Sub CreateKeys()
            If Environment.Is64BitOperatingSystem Then
                Helpers.Registry64.CreateKey(Keys_Values.KEY__VERSIONS_FULL)
                Helpers.Registry64.CreateKey(Keys_Values.KEY__VERSIONS_MAJOR)
                Helpers.Registry64.CreateKey(Keys_Values.KEY__VERSIONS_MAJORMINOR)

                Helpers.Registry64.CreateKey(Keys_Values.KEY__SITEKIOSK)
                Helpers.Registry64.CreateKey(Keys_Values.KEY__MACHINENAME)
            Else
                Helpers.Registry.CreateKey(Keys_Values.KEY__VERSIONS_FULL)
                Helpers.Registry.CreateKey(Keys_Values.KEY__VERSIONS_MAJOR)
                Helpers.Registry.CreateKey(Keys_Values.KEY__VERSIONS_MAJORMINOR)

                Helpers.Registry.CreateKey(Keys_Values.KEY__SITEKIOSK)
                Helpers.Registry.CreateKey(Keys_Values.KEY__MACHINENAME)
            End If
        End Sub

        Public Shared Sub WriteAppVersion(sName As String, sVersionFull As String, sVersionMajor As String, sVersionMajorMinor As String)
            If Environment.Is64BitOperatingSystem Then
                If Not Helpers.Registry64.SetValue_String(Keys_Values.KEY__VERSIONS_FULL, sName, sVersionFull) Then
                    'Console.WriteLine(Helpers.Errors.ToString(False, True))
                End If
                If Not Helpers.Registry64.SetValue_String(Keys_Values.KEY__VERSIONS_MAJOR, sName, sVersionMajor) Then
                    'Console.WriteLine(Helpers.Errors.ToString(False, True))
                End If
                If Not Helpers.Registry64.SetValue_String(Keys_Values.KEY__VERSIONS_MAJORMINOR, sName, sVersionMajorMinor) Then
                    'Console.WriteLine(Helpers.Errors.ToString(False, True))
                End If
            Else
                Helpers.Registry.SetValue_String(Keys_Values.KEY__VERSIONS_FULL, sName, sVersionFull)
                Helpers.Registry.SetValue_String(Keys_Values.KEY__VERSIONS_MAJOR, sName, sVersionMajor)
                Helpers.Registry.SetValue_String(Keys_Values.KEY__VERSIONS_MAJORMINOR, sName, sVersionMajorMinor)
            End If
        End Sub

        Public Shared Sub WriteAccountInfo(sUserName As String, sPasswordName As String, sPassword As String)
            If sUserName.Length <= 0 Then
                Exit Sub
            End If

            Dim sKey As String = Keys_Values.KEY__ACCOUNTS & "\" & sUserName
            Dim sVoid As String = ""

            If Environment.Is64BitOperatingSystem Then
                If Not Helpers.Registry64.KeyExists(sKey, sVoid) Then
                    Helpers.Registry64.CreateKey(sKey)
                End If
            Else
                If Not Helpers.Registry.KeyExists(sKey, sVoid) Then
                    Helpers.Registry.CreateKey(sKey)
                End If
            End If


            If Environment.Is64BitOperatingSystem Then
                If Not Helpers.Registry64.SetValue_String(sKey, Keys_Values.VALUE__PasswordName, sPasswordName) Then
                    'Console.WriteLine(Helpers.Errors.ToString(False, True))
                End If
                If Not Helpers.Registry64.SetValue_String(sKey, Keys_Values.VALUE__PasswordString, Helpers.Types.MaskString(sPassword)) Then
                    'Console.WriteLine(Helpers.Errors.ToString(False, True))
                End If
            Else
                Helpers.Registry.SetValue_String(sKey, Keys_Values.VALUE__PasswordName, sPasswordName)
                Helpers.Registry.SetValue_String(sKey, Keys_Values.VALUE__PasswordString, Helpers.Types.MaskString(sPassword))
            End If
        End Sub

        Public Shared Sub WriteSiteKioskInfo(LastCfg As String, LastCfgDate As String, LastCfgDateUTC As String, Theme As String)
            If Environment.Is64BitOperatingSystem Then
                Helpers.Registry64.SetValue_String(Keys_Values.KEY__SITEKIOSK, Keys_Values.VALUE__LastCfg, LastCfg)
                Helpers.Registry64.SetValue_String(Keys_Values.KEY__SITEKIOSK, Keys_Values.VALUE__LastCfgDate, LastCfgDate)
                Helpers.Registry64.SetValue_String(Keys_Values.KEY__SITEKIOSK, Keys_Values.VALUE__LastCfgDateUTC, LastCfgDateUTC)
                Helpers.Registry64.SetValue_String(Keys_Values.KEY__SITEKIOSK, Keys_Values.VALUE__Theme, Theme)
            Else
                Helpers.Registry.SetValue_String(Keys_Values.KEY__SITEKIOSK, Keys_Values.VALUE__LastCfg, LastCfg)
                Helpers.Registry.SetValue_String(Keys_Values.KEY__SITEKIOSK, Keys_Values.VALUE__LastCfgDate, LastCfgDate)
                Helpers.Registry.SetValue_String(Keys_Values.KEY__SITEKIOSK, Keys_Values.VALUE__LastCfgDateUTC, LastCfgDateUTC)
                Helpers.Registry.SetValue_String(Keys_Values.KEY__SITEKIOSK, Keys_Values.VALUE__Theme, Theme)
            End If
        End Sub

        Public Shared Sub WriteMachineName(Name As String, NameFromScript As String)
            If Environment.Is64BitOperatingSystem Then
                Helpers.Registry64.SetValue_String(Keys_Values.KEY__MACHINENAME, Keys_Values.VALUE__MachineName, Name)
                Helpers.Registry64.SetValue_String(Keys_Values.KEY__MACHINENAME, Keys_Values.VALUE__MachineNameFromScript, NameFromScript)
            Else
                Helpers.Registry.SetValue_String(Keys_Values.KEY__MACHINENAME, Keys_Values.VALUE__MachineName, Name)
                Helpers.Registry.SetValue_String(Keys_Values.KEY__MACHINENAME, Keys_Values.VALUE__MachineNameFromScript, NameFromScript)
            End If
        End Sub

        Public Shared Sub WriteLastCheck()
            Dim dDate As Date = Date.Now
            Dim sFormat As String = "yyyy-MM-dd HH:mm:ss"

            If Environment.Is64BitOperatingSystem Then
                Helpers.Registry64.SetValue_String(Keys_Values.KEY__ROOT, Keys_Values.VALUE__LastCheck, dDate.ToString(sFormat & " zzz"))
                Helpers.Registry64.SetValue_String(Keys_Values.KEY__ROOT, Keys_Values.VALUE__LastCheckUTC, dDate.ToUniversalTime.ToString(sFormat))
            Else
                Helpers.Registry.SetValue_String(Keys_Values.KEY__ROOT, Keys_Values.VALUE__LastCheck, dDate.ToString(sFormat & " zzz"))
                Helpers.Registry.SetValue_String(Keys_Values.KEY__ROOT, Keys_Values.VALUE__LastCheckUTC, dDate.ToUniversalTime.ToString(sFormat))
            End If
        End Sub
    End Class

    Public Class AdminAccount
        Public Class Users
            Private Shared mUsers As New List(Of String)
            Private Shared mCount As Integer = 0

            Public Shared Sub Init()
                mUsers = New List(Of String)
            End Sub

            Public Shared Sub Add(Name As String)
                Helpers.Logger.WriteMessageRelative(Name, 1)
                mUsers.Add(Name)
            End Sub

            Public Shared Sub Reset()
                mCount = 0
            End Sub

            Public Shared Function GetNext(ByRef Name As String) As Boolean
                If mCount < 0 Or mCount >= mUsers.Count Then
                    Return False
                End If

                Name = mUsers.Item(mCount)

                mCount += 1

                Return True
            End Function
        End Class

        Public Class Passwords
            Private Class _password
                Public Sub New(Name As String, ObfuscatedPassword As String)
                    mName = Name
                    mPass = ObfuscatedPassword
                End Sub

                Private mName As String
                Public Property Name() As String
                    Get
                        Return mName
                    End Get
                    Set(ByVal value As String)
                        mName = value
                    End Set
                End Property

                Private mPass As String
                Public Property ObfuscatedPassword() As String
                    Get
                        Return mPass
                    End Get
                    Set(ByVal value As String)
                        mPass = value
                    End Set
                End Property
            End Class

            Private Shared mPasswords As New List(Of _password)
            Private Shared mCount As Integer = 0

            Public Shared Sub Init()
                mCount = 0
                mPasswords = New List(Of _password)
            End Sub

            Public Shared Sub Add(Name As String, ObfuscatedPassword As String)
                Helpers.Logger.WriteMessageRelative(Name, 1)
                mPasswords.Add(New _password(Name, ObfuscatedPassword))
            End Sub

            Public Shared Sub Reset()
                mCount = 0
            End Sub

            Public Shared Function GetNext(ByRef Name As String, ByRef Pass As String) As Boolean
                If mCount < 0 Or mCount >= mPasswords.Count Then
                    Return False
                End If

                Name = mPasswords.Item(mCount).Name
                Pass = Helpers.XOrObfuscation_v2.Deobfuscate(mPasswords.Item(mCount).ObfuscatedPassword)

                mCount += 1

                Return True
            End Function
        End Class
    End Class

    Public Class Exes
        Private Class _exe
            Public Sub New(Name As String, Path As String)
                mName = Name
                mPath = Path
            End Sub

            Private mName As String
            Public Property Name() As String
                Get
                    Return mName
                End Get
                Set(ByVal value As String)
                    mName = value
                End Set
            End Property

            Private mPath As String
            Public Property Path() As String
                Get
                    Return mPath
                End Get
                Set(ByVal value As String)
                    mPath = value
                End Set
            End Property
        End Class

        Private Shared mExes As New List(Of _exe)
        Private Shared mCount As Integer = 0

        Public Shared Sub Init()
            mCount = 0
            mExes = New List(Of _exe)
        End Sub

        Public Shared Sub Add(Name As String, Path As String)
            Helpers.Logger.WriteMessageRelative(Name, 1)
            Helpers.Logger.WriteMessageRelative(Path, 2)
            mExes.Add(New _exe(Name, Path))
        End Sub

        Public Shared Sub Reset()
            mCount = 0
        End Sub

        Public Shared Function GetNext(ByRef Name As String, ByRef Path As String) As Boolean
            If mCount < 0 Or mCount >= mExes.Count Then
                Return False
            End If

            Name = mExes.Item(mCount).Name
            Path = mExes.Item(mCount).Path

            mCount += 1

            Return True
        End Function
    End Class

    Public Class SiteKiosk
        Public Class Config
            Public Shared Function LastCfg() As String
                Return Helpers.SiteKiosk.GetSiteKioskActiveConfig
            End Function

            Public Shared Function LastCfgDate() As String
                Dim d As Date = Helpers.FilesAndFolders.GetFileLastModifiedDateAsDate(LastCfg())

                Return d.ToString("yyyy-MM-dd HH:mm:ss zzz")
            End Function

            Public Shared Function LastCfgDateUTC() As String
                Dim d As Date = Helpers.FilesAndFolders.GetFileLastModifiedDateAsDate(LastCfg())

                d = d.ToUniversalTime

                Return d.ToString("yyyy-MM-dd HH:mm:ss")
            End Function
        End Class

        Public Class Theme
            Public Shared Function Name() As String
                Dim sStartPage As String = ""

                Try
                    Dim sSkCfgFile As String = Helpers.SiteKiosk.GetSiteKioskActiveConfig()

                    Dim nt As Xml.XmlNameTable
                    Dim ns As Xml.XmlNamespaceManager
                    Dim xmlSkCfg As Xml.XmlDocument
                    Dim xmlNode_Root As Xml.XmlNode

                    xmlSkCfg = New Xml.XmlDocument
                    xmlSkCfg.Load(sSkCfgFile)

                    nt = xmlSkCfg.NameTable
                    ns = New Xml.XmlNamespaceManager(nt)
                    ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
                    ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

                    xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)


                    Dim xmlNode_startpageconfig As Xml.XmlNode
                    Dim xmlNode_startpage As Xml.XmlNode

                    xmlNode_startpageconfig = xmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)
                    xmlNode_startpage = xmlNode_startpageconfig.SelectSingleNode("sk:startpage", ns)


                    sStartPage = xmlNode_startpage.InnerText
                Catch ex As Exception
                    Helpers.Logger.WriteErrorRelative("exception", 1)
                    Helpers.Logger.WriteErrorRelative(ex.Message, 2)
                End Try


                sStartPage = sStartPage.Replace("/", "\")

                Dim sParts As String() = sStartPage.Split("\")

                If sParts.Length < 2 Then
                    sStartPage = ""
                Else
                    sStartPage = String.Join("", sParts.Skip(sParts.Length - 2).Take(1))
                End If


                Return sStartPage
            End Function
        End Class
    End Class

    Public Class MachineName
        Public Shared Function Name() As String
            Return Environment.MachineName
        End Function

        Public Shared Function NameFromScript() As String
            Return Helpers.SiteKiosk.GetMachineNameFromScript()
        End Function
    End Class
End Class
