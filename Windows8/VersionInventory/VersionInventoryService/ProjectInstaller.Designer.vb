﻿<System.ComponentModel.RunInstaller(True)> Partial Class ProjectInstaller
    Inherits System.Configuration.Install.Installer

    'Installer overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.VersionInformationServiceProcessInstaller = New System.ServiceProcess.ServiceProcessInstaller()
        Me.VersionInformationServiceInstaller = New System.ServiceProcess.ServiceInstaller()
        '
        'VersionInformationServiceProcessInstaller
        '
        Me.VersionInformationServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem
        Me.VersionInformationServiceProcessInstaller.Password = Nothing
        Me.VersionInformationServiceProcessInstaller.Username = Nothing
        '
        'VersionInformationServiceInstaller
        '
        Me.VersionInformationServiceInstaller.DisplayName = "OVCC Version Information Service"
        Me.VersionInformationServiceInstaller.ServiceName = "OVCCVersionInformationService"
        Me.VersionInformationServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic
        '
        'ProjectInstaller
        '
        Me.Installers.AddRange(New System.Configuration.Install.Installer() {Me.VersionInformationServiceProcessInstaller, Me.VersionInformationServiceInstaller})

    End Sub

    Friend WithEvents VersionInformationServiceProcessInstaller As ServiceProcess.ServiceProcessInstaller
    Friend WithEvents VersionInformationServiceInstaller As ServiceProcess.ServiceInstaller
End Class
