﻿Public Class service_main
    Private mThreading_Main As Threading.Thread
    Private bLoopAbort As Boolean = False
    Private iDelay As Integer = 60 * 60 * 12 '12 hours
    Private sVersionInventoryPath As String = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\VersionInventory\OVCCVersionInventory.exe")

    Protected Overrides Sub OnStart(ByVal args() As String)
        'init logger
        Helpers.Logger.InitialiseLogger()

        'first logging
        Helpers.Logger.Write(New String("*", 50), , 0)
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, , 0)
        Helpers.Logger.Write("service started", , 0)


        mThreading_Main = New Threading.Thread(New Threading.ThreadStart(AddressOf Thread_Main))
        mThreading_Main.Start()
    End Sub

    Protected Overrides Sub OnStop()
        Try
            mThreading_Main.Abort()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Thread_Main()
        Helpers.Logger.Write("thread started", , 0)
        Helpers.Logger.Write("waiting 5 minutes", , 0)

        'wait 5 minutes
        Threading.Thread.Sleep(300 * 1000)

        Do While Not bLoopAbort
            Helpers.Logger.Write("starting VersionInventory exe", , 0)

            If Not IO.File.Exists(sVersionInventoryPath) Then
                Helpers.Logger.WriteError("not found", 1)
                bLoopAbort = True
                Exit Do
            End If

            Helpers.Logger.Write(sVersionInventoryPath, , 1)
            Process.Start(sVersionInventoryPath)


            Threading.Thread.Sleep(iDelay * 1000)
        Loop
    End Sub
End Class
