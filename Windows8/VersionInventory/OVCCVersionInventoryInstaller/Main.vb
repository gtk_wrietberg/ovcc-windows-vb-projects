﻿Module Main
    Private AppName As String = "OVCCVersionInventory"
    Private ServiceName As String = "OVCCVersionInventoryService"
    Private ServiceDisplayName As String = "OVCC Version Inventory Service"
    Private ServiceDescription As String = "OVCC Version Inventory Service"
    Private ServicePath As String = IO.Path.Combine(Helpers.FilesAndFolders.GetProgramFilesFolder(), "GuestTek\VersionInventory\OVCCVersionInventoryService.exe")

    Public Sub Main()
        Helpers.Logger.InitialiseLogger()


        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.Write("Installer started")


        'Kill processes and stop service, if any
        Helpers.Logger.Write("Kill processes", , 0)
        For Each p As Process In Process.GetProcesses
            If AppName.ToLower.Equals(p.ProcessName) Then
                Helpers.Logger.Write("killing: " & p.ProcessName, , 1)
                Try
                    p.Kill()
                    Helpers.Logger.Write("ok", , 2)
                Catch ex As Exception
                    Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    Helpers.Logger.Write(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                End Try
            End If
        Next


        Helpers.Logger.Write("Stop service", , 0)
        StopService(ServiceName)


        'Copy files
        '------------------------------------------------------
        Dim sBackupFolder As String = ""
        Dim dDate As Date = Now()

        sBackupFolder = Helpers.FilesAndFolders.GetDefaultBackupFolder


        Dim oCopyFiles As Helpers.FilesAndFolders.CopyFiles = New Helpers.FilesAndFolders.CopyFiles
        oCopyFiles.SuppressCopyLogging = False
        oCopyFiles.BackupDirectory = sBackupFolder

        oCopyFiles.SourceDirectory = "files"
        oCopyFiles.DestinationDirectory = Helpers.FilesAndFolders.GetProgramFilesFolder()

        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()


        'Install service part
        Helpers.Logger.Write("Install services", , 0)
        InstallService(ServiceName, ServiceDisplayName, ServiceDescription, ServicePath)
    End Sub

    Private Function StopService(Name As String) As Boolean
        Helpers.Logger.Write(Name, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        If ServiceInstaller.ServiceIsInstalled(Name) Then
            Dim retries As Integer = 0
            Helpers.Logger.Write("stopping", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            If ServiceInstaller.StopService(Name, 30) Then
                Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            Else
                Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Helpers.Logger.Write("service status: " & ServiceInstaller.GetServiceStatus(Name).ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If


            Threading.Thread.Sleep(5000)

            Return True
        Else
            Helpers.Logger.Write("not found, so no need to stop", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            Return False
        End If
    End Function

    Private Sub InstallService(Name As String, DisplayName As String, Description As String, ServicePath As String)
        Dim sStmp As String = ""

        Helpers.Logger.Write(Name, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 1)

        For i As Integer = 1 To 5
            Helpers.Logger.Write("try #" & i.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)

            Helpers.Logger.Write("is service already installed?", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)

            If ServiceInstaller.ServiceIsInstalled(Name) Then
                Helpers.Logger.Write("yes", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)


                Helpers.Logger.Write("stopping", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
                For j = 1 To 5
                    Helpers.Logger.Write("#" & j.ToString, Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

                    If ServiceInstaller.StopService(Name, 30) Then
                        Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 5)

                        Exit For
                    Else
                        Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                        Helpers.Logger.Write("service status: " & ServiceInstaller.GetServiceStatus(Name).ToString, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 6)

                        Threading.Thread.Sleep(5000)
                    End If
                Next
            Else
                Helpers.Logger.Write("nope", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)


                Threading.Thread.Sleep(5000)

                Helpers.Logger.Write("installing", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
                ServiceInstaller.InstallService(Name, DisplayName, ServicePath, ServiceBootFlag.DelayedAutoStart)
            End If


            'Helpers.Logger.Write("make sure service is disabled from the start", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            'Helpers.Registry.SetValue_String(LPCConstants.RegistryKeys.KEY__Watchdog_Settings, LPCConstants.RegistryKeys.VALUE__WatchdogSettingsDisabled, Helpers.XOrObfuscation.Obfuscate(LPCConstants.Misc.MagicalDisableString))

            'Helpers.Logger.Write("installing", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 2)
            'ServiceInstaller.InstallServiceAndStart(Name, DisplayName, ServicePath)

            'Threading.Thread.Sleep(5000)

            'Helpers.Logger.Write("installing", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            'ServiceInstaller.InstallService(Name, DisplayName, ServicePath)

            Threading.Thread.Sleep(15000)

            Helpers.Logger.Write("set description", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            ServiceInstaller.ChangeServiceDescription(Name, Description)

            Helpers.Logger.Write("installed doublecheck", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)
            If ServiceInstaller.ServiceIsInstalled(Name) Then
                Helpers.Logger.Write("ok", Helpers.Logger.MESSAGE_TYPE.LOG_DEFAULT, 4)

                Exit For
            Else
                Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If

            Threading.Thread.Sleep(5000)
        Next
    End Sub
End Module
