Imports System.Diagnostics

Module RestartSitekiosk
    Public Function KillSiteKiosk() As Integer
        Dim iCount As Integer = 0
        Dim iTmp As Integer

        iTmp = Process.GetProcessesByName("sitekiosk").Length

        If iTmp > 0 Then
            Helpers.Logger.WriteRelative("found SiteKiosk (" & iTmp & "x)", , 1)
        Else
            Helpers.Logger.WriteRelative("found no SiteKiosk processes", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If

        For Each p As Process In Process.GetProcessesByName("sitekiosk")
            Try
                Helpers.Logger.WriteRelative("killing", , 2)
                p.Kill()
                Helpers.Logger.WriteRelative("ok", , 3)
                iCount += 1
            Catch ex As Exception
                Helpers.Logger.WriteRelative("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End Try
        Next

        If iCount > 0 Then
            Helpers.Logger.WriteRelative("killed SiteKiosk (" & iCount & "x)", , 1)
        Else
            Helpers.Logger.WriteRelative("killed nothing", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)
        End If

        Return iCount
    End Function
End Module
