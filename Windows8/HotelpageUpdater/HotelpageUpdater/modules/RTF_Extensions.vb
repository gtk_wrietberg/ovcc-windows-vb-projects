﻿Module RTF_Extensions
    <Runtime.CompilerServices.Extension()>
    Public Function ToRtf(s As String) As String
        Return "{\rtf1\ansi" + s + "}"
    End Function

    <Runtime.CompilerServices.Extension()>
    Public Function ToBold(s As String) As String
        Return String.Format("\b {0}\b0 ", s)
    End Function
End Module
