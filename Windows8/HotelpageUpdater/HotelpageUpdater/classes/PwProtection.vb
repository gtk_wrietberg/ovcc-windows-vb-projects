﻿Public Class PwProtection
    Private Shared mStyle As String = ""

    Private Shared ReadOnly MAIN_SCRIPT_FILE As String = "%%PROGRAM_FILES%%\SiteKiosk\Skins\default\Scripts\guest-tek.js"
    Private Shared ReadOnly MAIN_SCRIPT_FILE_OLD As String = "%%PROGRAM_FILES%%\SiteKiosk\Skins\default\Scripts\ibahn.js"

    Public Shared Function WhichStyle() As Boolean
        Dim sFile__MAIN_SCRIPT As String = MAIN_SCRIPT_FILE.Replace("%%PROGRAM_FILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder())

        Helpers.Logger.WriteRelative("searching '" & sFile__MAIN_SCRIPT & "'", , 1)
        If Not IO.File.Exists(sFile__MAIN_SCRIPT) Then
            Helpers.Logger.WriteErrorRelative("not found", 2)

            sFile__MAIN_SCRIPT = MAIN_SCRIPT_FILE_OLD.Replace("%%PROGRAM_FILES%%", Helpers.FilesAndFolders.GetProgramFilesFolder())
            Helpers.Logger.WriteRelative("searching '" & sFile__MAIN_SCRIPT & "'",, 1)

            If Not IO.File.Exists(sFile__MAIN_SCRIPT) Then
                Helpers.Logger.WriteErrorRelative("not found", 2)

                Return False
            End If
        End If
        Helpers.Logger.WriteErrorRelative("ok", 2)


        Helpers.Logger.WriteRelative("finding pw style",, 1)
        Try
            Using sr As New IO.StreamReader(sFile__MAIN_SCRIPT)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("OVCC_PasswordAccessModule__IsEnabled()") Then
                        mStyle = "new"
                        Exit While
                    End If

                    If line.Contains("HiltonPasswordAccessModule__IsEnabled()") Then
                        mStyle = "old"
                        Exit While
                    End If
                End While
            End Using

            Helpers.Logger.WriteRelative("found style",, 2)
            Helpers.Logger.WriteRelative(mStyle,, 3)
        Catch ex As Exception
            Helpers.Logger.WriteErrorRelative("error", 2)
            Helpers.Logger.WriteErrorRelative(ex.Message, 3)

            Return False
        End Try

        Return True
    End Function

    Public Shared Function LoadPasswordFile() As Boolean
        If mStyle.Equals("new") Then
            Return NewStyle.LoadPasswordFile()
        ElseIf mStyle.Equals("old") Then
            Return OldStyle.LoadPasswordFile()
        Else
            Return False
        End If
    End Function

    Public Shared Function GetPassword(ByRef CurrentPassword As String, ByRef BackupPassword As String, ByRef ErrorMessage As String) As Boolean
        If mStyle.Equals("new") Then
            Return NewStyle.GetPassword(CurrentPassword, BackupPassword, ErrorMessage)
        ElseIf mStyle.Equals("old") Then
            Return OldStyle.GetPassword(CurrentPassword, BackupPassword, ErrorMessage)
        Else
            Return False
        End If
    End Function

    Public Shared Function SetPassword(Password As String, Active As Boolean) As Boolean
        If mStyle.Equals("new") Then
            Return NewStyle.SetPassword(Password, Active)
        ElseIf mStyle.Equals("old") Then
            Return OldStyle.SetPassword(Password, Active)
        Else
            Return False
        End If
    End Function


    Public Class NewStyle
        Public Shared Function LoadPasswordFile() As Boolean
            Try
                Helpers.Logger.WriteRelative("searching '" & clsGlobals.g_UIScriptFile & "' for pw file", , 1)
                Helpers.Logger.WriteRelative("opening", , 2)

                If Not IO.File.Exists(clsGlobals.g_UIScriptFile) Then
                    Helpers.Logger.WriteRelative("FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    Return False
                Else
                    Helpers.Logger.WriteRelative("ok", , 3)
                End If


                Using sr As New IO.StreamReader(clsGlobals.g_UIScriptFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var _OVCC_PasswordAccessModule__CONFIG_PasswordFile=") Then
                            line = line.Replace("var _OVCC_PasswordAccessModule__CONFIG_PasswordFile=", "")
                            line = line.Replace("""", "")
                            line = line.Replace(";", "")

                            clsGlobals.g_PasswordFile = line.Replace("\\", "\")


                            Helpers.Logger.WriteRelative("found password file", , 2)
                            Helpers.Logger.WriteRelative(clsGlobals.g_PasswordFile, , 3)
                        End If
                    End While
                End Using

                Return True

            Catch ex As Exception
                Helpers.Logger.WriteRelative("FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                Return False
            End Try
        End Function

        Public Shared Function GetPassword(ByRef CurrentPassword As String, ByRef BackupPassword As String, ByRef ErrorMessage As String) As Boolean
            CurrentPassword = ""
            BackupPassword = ""
            ErrorMessage = ""

            Try
                Dim xmlDoc As Xml.XmlDocument
                Dim xmlNode_Root As Xml.XmlNode
                Dim xmlNode_Password As Xml.XmlNode
                Dim xmlNode_Current As Xml.XmlNode
                Dim xmlNode_Backup As Xml.XmlNode


                xmlDoc = New Xml.XmlDocument

                xmlDoc.Load(clsGlobals.g_PasswordFile)

                xmlNode_Root = xmlDoc.SelectSingleNode("ovcc")
                xmlNode_Password = xmlNode_Root.SelectSingleNode("password")
                xmlNode_Current = xmlNode_Password.SelectSingleNode("current")
                xmlNode_Backup = xmlNode_Password.SelectSingleNode("backup")

                CurrentPassword = xmlNode_Current.InnerText
                BackupPassword = xmlNode_Backup.InnerText

                Return True
            Catch ex As Exception
                ErrorMessage = ex.Message

                Return False
            End Try
        End Function

        Public Shared Function SetPassword(Password As String, Active As Boolean) As Boolean
            Try
                Dim xmlDoc As Xml.XmlDocument
                Dim xmlNode_Root As Xml.XmlNode
                Dim xmlNode_Password As Xml.XmlNode
                Dim xmlNode_Current As Xml.XmlNode
                Dim xmlNode_Backup As Xml.XmlNode

                xmlDoc = New Xml.XmlDocument

                xmlDoc.Load(clsGlobals.g_PasswordFile)

                xmlNode_Root = xmlDoc.SelectSingleNode("ovcc")
                xmlNode_Password = xmlNode_Root.SelectSingleNode("password")
                xmlNode_Current = xmlNode_Password.SelectSingleNode("current")
                xmlNode_Backup = xmlNode_Password.SelectSingleNode("backup")

                If Active Then
                    xmlNode_Current.InnerText = Password
                Else
                    xmlNode_Current.InnerText = ""
                End If

                xmlNode_Backup.InnerText = Password


                xmlDoc.Save(clsGlobals.g_PasswordFile)

                Return True
            Catch ex As Exception
                Password = ex.Message

                Return False
            End Try

        End Function
    End Class

    Public Class OldStyle
        Public Shared Function LoadPasswordFile() As Boolean
            Try
                Helpers.Logger.WriteRelative("searching '" & clsGlobals.g_UIScriptFile & "' for pw file", , 1)
                Helpers.Logger.WriteRelative("opening", , 2)

                If Not IO.File.Exists(clsGlobals.g_UIScriptFile) Then
                    Helpers.Logger.WriteRelative("FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    Return False
                Else
                    Helpers.Logger.WriteRelative("ok", , 3)
                End If


                Using sr As New IO.StreamReader(clsGlobals.g_UIScriptFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var _OVCC_PasswordAccessModule__CONFIG_PasswordFile=") Then
                            line = line.Replace("var _OVCC_PasswordAccessModule__CONFIG_PasswordFile=", "")
                            line = line.Replace("""", "")
                            line = line.Replace(";", "")

                            clsGlobals.g_PasswordFile = line.Replace("\\", "\")


                            Helpers.Logger.WriteRelative("found password file", , 2)
                            Helpers.Logger.WriteRelative(clsGlobals.g_PasswordFile, , 3)
                        End If
                    End While
                End Using

                Return True

            Catch ex As Exception
                Helpers.Logger.WriteRelative("FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                Return False
            End Try
        End Function

        Public Shared Function GetPassword(ByRef CurrentPassword As String, ByRef BackupPassword As String, ByRef ErrorMessage As String) As Boolean
            CurrentPassword = ""
            BackupPassword = ""
            ErrorMessage = ""

            Try
                Dim xmlDoc As Xml.XmlDocument
                Dim xmlNode_Root As Xml.XmlNode
                Dim xmlNode_Password As Xml.XmlNode
                Dim xmlNode_Current As Xml.XmlNode
                Dim xmlNode_Backup As Xml.XmlNode


                xmlDoc = New Xml.XmlDocument

                xmlDoc.Load(clsGlobals.g_PasswordFile)

                xmlNode_Root = xmlDoc.SelectSingleNode("ovcc")
                xmlNode_Password = xmlNode_Root.SelectSingleNode("password")
                xmlNode_Current = xmlNode_Password.SelectSingleNode("current")
                xmlNode_Backup = xmlNode_Password.SelectSingleNode("backup")

                CurrentPassword = xmlNode_Current.InnerText
                BackupPassword = xmlNode_Backup.InnerText

                Return True
            Catch ex As Exception
                ErrorMessage = ex.Message

                Return False
            End Try
        End Function

        Public Shared Function SetPassword(Password As String, Active As Boolean) As Boolean
            Try
                Dim xmlDoc As Xml.XmlDocument
                Dim xmlNode_Root As Xml.XmlNode
                Dim xmlNode_Password As Xml.XmlNode
                Dim xmlNode_Current As Xml.XmlNode
                Dim xmlNode_Backup As Xml.XmlNode

                xmlDoc = New Xml.XmlDocument

                xmlDoc.Load(clsGlobals.g_PasswordFile)

                xmlNode_Root = xmlDoc.SelectSingleNode("ovcc")
                xmlNode_Password = xmlNode_Root.SelectSingleNode("password")
                xmlNode_Current = xmlNode_Password.SelectSingleNode("current")
                xmlNode_Backup = xmlNode_Password.SelectSingleNode("backup")

                If Active Then
                    xmlNode_Current.InnerText = Password
                Else
                    xmlNode_Current.InnerText = ""
                End If

                xmlNode_Backup.InnerText = Password


                xmlDoc.Save(clsGlobals.g_PasswordFile)

                Return True
            Catch ex As Exception
                Password = ex.Message

                Return False
            End Try

        End Function
    End Class
End Class
