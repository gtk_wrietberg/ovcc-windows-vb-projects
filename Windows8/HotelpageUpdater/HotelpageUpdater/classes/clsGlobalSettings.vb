﻿Imports System.Reflection

Public Class clsGlobalSettings
    Private Shared ReadOnly cStringId__PhoneNumber As String = "1100000"




    Public Shared Function GetConfigFilesForFrontend() As Boolean
        clsGlobals.g_SKConfigFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        clsGlobals.g_UIConfigFile = IO.Path.Combine(Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", ""), "Skins\Public\iBAHN\Scripts\iBAHN_functions.js")
        clsGlobals.g_UIScriptFile = IO.Path.Combine(Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", ""), "Skins\default\Scripts\guest-tek.js")

        Return True
    End Function

    Public Shared Function LoadThemes() As Boolean
        Dim bOk As Boolean = False
        Dim _index As Integer = 0
        Dim _external_file As String = ""
        Dim _config_string As String = ""

        Try
            HotelBrands.Reset()

            Helpers.Logger.WriteRelative("loading theme resource file", , 1)
            Helpers.Logger.WriteRelative("looking for external file", , 2)


            Dim bDontUseExternalFile As Boolean = True
            _external_file = AppDomain.CurrentDomain.BaseDirectory
            _external_file = IO.Path.Combine(New Uri(_external_file).LocalPath, "ovcc_themes.xml")

            If IO.File.Exists(_external_file) And bDontUseExternalFile Then
                _config_string = IO.File.ReadAllText(_external_file)

                Helpers.Logger.WriteRelative("ok", , 3)
            Else
                If bDontUseExternalFile Then
                    Helpers.Logger.WriteRelative("not found", , 3)
                    Helpers.Logger.WriteRelative(_external_file, , 4)
                End If

                Helpers.Logger.WriteRelative("using internal default", , 3)
                _config_string = My.Resources.ovcc_themes

                Helpers.Logger.WriteRelative("writing to external file", , 3)

                Try
                    IO.File.WriteAllText(_external_file, _config_string)

                    If IO.File.Exists(_external_file) Then
                        Helpers.Logger.WriteRelative("ok", , 4)
                    Else
                        Helpers.Logger.WriteRelative("failed to write", , 4)
                        Helpers.Logger.WriteRelative("reasons unknown", , 5)
                    End If
                Catch ex As Exception
                    Helpers.Logger.WriteRelative("failed to write", , 4)
                    Helpers.Logger.WriteRelative(ex.Message, , 5)
                End Try
            End If

            Helpers.Logger.WriteDebugRelative("file contents:" & vbCrLf & vbCrLf & _config_string & vbCrLf & vbCrLf, 1)


            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode, xmlNode_Brands As Xml.XmlNode, xmlNode_BrandList As Xml.XmlNodeList, xmlNode_Brand As Xml.XmlNode
            Dim isHiltonTheme As Boolean = False
            Dim fieldName As String
            Dim propertyValue As Object
            Dim _type As Type = (New HotelBrands.HotelBrand).GetType

            xmlSkCfg = New Xml.XmlDocument

            Helpers.Logger.WriteRelative("loading themes", , 2)
            xmlSkCfg.LoadXml(_config_string)

            xmlNode_Root = xmlSkCfg.SelectSingleNode("OVCCThemes")
            xmlNode_Brands = xmlNode_Root.SelectSingleNode("brands")
            xmlNode_BrandList = xmlNode_Brands.SelectNodes("brand")

            For Each xmlNode_Brand In xmlNode_BrandList
                Helpers.Logger.WriteDebugRelative("found theme", 3)

                Dim _brand As New HotelBrands.HotelBrand

                _brand.Index = _index
                _brand.BrandIndex = CInt(xmlNode_Brand.SelectSingleNode("index").InnerText)
                _brand.Name = xmlNode_Brand.SelectSingleNode("name").InnerText
                _brand.Path_Theme = xmlNode_Brand.SelectSingleNode("path_theme").InnerText
                _brand.Path_Screensaver = xmlNode_Brand.SelectSingleNode("path_screensaver").InnerText
                _brand.Path_Logout = xmlNode_Brand.SelectSingleNode("path_logout").InnerText
                _brand.Path_PaymentDialog = xmlNode_Brand.SelectSingleNode("path_paymentdialog").InnerText
                _brand.IsLastHilton = False

                If clsGlobals.g_Debug Then
                    For Each pi As PropertyInfo In _type.GetProperties(BindingFlags.Instance Or BindingFlags.Public Or BindingFlags.NonPublic)
                        fieldName = pi.Name
                        propertyValue = pi.GetValue(_brand, Nothing)

                        Helpers.Logger.WriteDebugRelative(fieldName & ": " & If(propertyValue Is Nothing, "Nothing", propertyValue.ToString), 4)
                    Next
                End If


                If xmlNode_Brand.SelectSingleNode("payment").InnerText.Equals("True") Then
                    _brand.PaymentEnabled = True
                Else
                    _brand.PaymentEnabled = False
                End If

                If xmlNode_Brand.SelectSingleNode("password").InnerText.Equals("True") Then
                    _brand.PasswordEnabled = True
                Else
                    _brand.PasswordEnabled = False
                End If


                HotelBrands.Add(_brand)

                _index += 1
                bOk = True
            Next

            HotelBrands.SetLastHilton()


            For Each _brand As HotelBrands.HotelBrand In HotelBrands.GetBrands
                Helpers.Logger.WriteRelative("theme", , 3)
                Helpers.Logger.WriteRelative(_brand.BrandIndex.ToString & " ; " & _brand.Name, , 4)

                If _brand.BrandIndex >= HotelBrands.HiltonBrandIndexLimit Or _brand.IsLastHilton Then
                    Helpers.Logger.WriteRelative("theme paths valid?", , 4)

                    If _brand.ArePathsValid Then
                        Helpers.Logger.WriteRelative("YES", , 5)
                    Else
                        Helpers.Logger.WriteRelative("NO", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    End If
                Else

                End If
            Next
        Catch ex As Exception
            bOk = False

            Helpers.Logger.WriteRelative("uh oh", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.WriteRelative("error: " & ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try



#Region "ye olde shite"
        'Using strReader As New IO.StringReader(My.Resources.ovcc_themes_txt)
        '    Using textparser As New FileIO.TextFieldParser(strReader)
        '        textparser.TextFieldType = FileIO.FieldType.Delimited
        '        textparser.Delimiters = {","}
        '        textparser.HasFieldsEnclosedInQuotes = True

        '        While Not textparser.EndOfData
        '            Try
        '                Dim curRow As String() = textparser.ReadFields()

        '                If curRow.Count <> c_ThemeResourceFields Then
        '                    Throw New FileIO.MalformedLineException("incorrect number of fields (" & curRow.Count.ToString & " instead of " & c_ThemeResourceFields.ToString)
        '                End If


        '                Helpers.Logger.WriteRelative("found theme", , 2)
        '                Helpers.Logger.WriteRelative(curRow(1), , 3)

        '                Dim _brand As New HotelBrands.HotelBrand

        '                _brand.BrandIndex = CInt(curRow(0))
        '                _brand.Name = curRow(1)
        '                _brand.Path_Theme = curRow(2)
        '                _brand.Path_Screensaver = curRow(3)
        '                _brand.Path_Logout = curRow(4)
        '                _brand.Path_PaymentDialog = curRow(5)

        '                If curRow(6).Equals("True") Then
        '                    _brand.PaymentEnabled = True
        '                Else
        '                    _brand.PaymentEnabled = False
        '                End If

        '                _brand.Index = _index


        '                HotelBrands.Add(_brand)

        '                _index += 1
        '                bOk = True
        '            Catch ex As FileIO.MalformedLineException
        '                bOk = False

        '                Helpers.Logger.WriteRelative("uh oh", , 2)
        '                Helpers.Logger.WriteRelative("error: " & ex.Message, , 3)

        '                Stop
        '            End Try
        '        End While
        '    End Using
        'End Using
#End Region

        Return bOk
    End Function

    Public Shared Function ReadPassword() As Boolean
        Helpers.Logger.WriteRelative("loading password file", , 1)

        If Not PwProtection.LoadPasswordFile() Then
            Helpers.Logger.WriteRelative("uh oh", , 2)

            Return False
        End If

        Dim sTmp As String = "", sTmp2 As String = "", sErr As String = ""

        Helpers.Logger.WriteRelative("loading password", , 1)
        If PwProtection.GetPassword(sTmp, sTmp2, sErr) Then
            Helpers.Logger.WriteRelative("current password=" & sTmp, , 2)
            Helpers.Logger.WriteRelative("backup password=" & sTmp2, , 2)

            clsGlobals.g_Password = sTmp2
            If sTmp.Equals(sTmp2) And Not sTmp.Equals("") Then
                clsGlobals.g_PasswordEnabled = True
            Else
                clsGlobals.g_PasswordEnabled = False
            End If

            Return True
        Else
            Helpers.Logger.WriteRelative("uh oh", , 2)
            Helpers.Logger.WriteRelative("error: " & sErr, , 3)
        End If

        Return False
    End Function

    Public Shared Function WritePassword() As Boolean
        Helpers.Logger.WriteRelative("saving password file", , 1)

        PwProtection.SetPassword(clsGlobals.g_Password, clsGlobals.g_PasswordEnabled)

        Return True
    End Function

    Public Shared Function ReadGlobalSettings() As Boolean
        Helpers.Logger.WriteRelative("reading script file", , 1)

        Dim sFile As String

        sFile = clsGlobals.g_UIConfigFile

        Helpers.Logger.WriteRelative(sFile, , 2)

        If Not IO.File.Exists(sFile) Then
            Helpers.Logger.WriteRelative("FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        Else
            Helpers.Logger.WriteRelative("ok", , 3)
        End If

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = line.Replace("var iBAHN_Global_HotelURL", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Url_Hotel = line

                        Helpers.Logger.WriteRelative("found hotel url", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = line.Replace("var iBAHN_Global_InternetURL", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Url_Internet = line

                        Helpers.Logger.WriteRelative("found internet url", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = line.Replace("var iBAHN_Global_WeatherURL", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Url_Weather = line

                        Helpers.Logger.WriteRelative("found weather url", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = line.Replace("var iBAHN_Global_MapURL", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Url_Map = line

                        Helpers.Logger.WriteRelative("found map url", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = line.Replace("var iBAHN_Global_SiteAddress", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Address_Site = line

                        Helpers.Logger.WriteRelative("found site address", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        line = line.Replace("var iBAHN_Global_Brand=", "")
                        line = Helpers.Generic.NumbersOnly(line)

                        Helpers.Logger.WriteRelative("found brand index", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)

                        HotelBrands.ResetCurrent()

                        If IsNumeric(line) Then
                            If HotelBrands.SetCurrent(CInt(line)) Then
                                'ok
                                Helpers.Logger.WriteRelative("theme info found", , 4)
                            Else
                                Helpers.Logger.WriteRelative("no theme info found for brand index '" & line & "'", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                                Helpers.Logger.WriteRelative("default to 0", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                                HotelBrands.SetCurrent(0)
                            End If
                        Else
                            Helpers.Logger.WriteRelative("brand index '" & line & "' is not even a number", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                            Helpers.Logger.WriteRelative("default to 0", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                            HotelBrands.SetCurrent(0)
                        End If
                    End If
                End While
            End Using

            Return True
        Catch ex As Exception

            Return False
        End Try
    End Function

    Public Shared Function ReadPhoneNumberFromEnglishLanguageFile() As Boolean
        ' read english.xml from c_Path_LanguageFiles
        Dim bFoundIt As Boolean = False
        Dim sFile As String = ""
        Dim sPhoneNumber As String = clsGlobals.g_Support_PhoneNumber

        sFile = clsGlobals.c_Path_LanguageFiles & "\english.xml"


        Try
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            Helpers.Logger.WriteRelative("loading", , 1)
            Helpers.Logger.WriteRelative(sFile, , 2)
            xmlSkCfg.Load(sFile)
            Helpers.Logger.WriteRelative("ok", , 3)

            Helpers.Logger.WriteRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("language")

            If Not xmlNode_Root Is Nothing Then
                Helpers.Logger.WriteRelative("ok", , 3)
            Else
                Throw New Exception("root node (language) not found")
            End If


            Helpers.Logger.WriteRelative("loading string nodelist", , 2)
            Dim xmlNodeList_Strings As Xml.XmlNodeList
            Dim xmlNode_String As Xml.XmlNode
            Dim sTmp As String = ""

            xmlNodeList_Strings = xmlNode_Root.SelectNodes("string")
            For Each xmlNode_String In xmlNodeList_Strings
                If xmlNode_String.Attributes.GetNamedItem("id").InnerText = cStringId__PhoneNumber Then
                    'found the specific language string id, mark it for a backup
                    Helpers.Logger.WriteRelative("found phone number string", , 3)

                    'get existing phone number
                    Helpers.Logger.WriteRelative("retrieving current phone number", , 3)

                    Dim sString As String = xmlNode_String.InnerText
                    ' <string id="1100000">OneView Connection Centre helpdesk (gratis): 00800 3400 1111</string>

                    Dim pattern As String = "(.+)(:|：) (.+)" 'Notice the funky colon (U+FF1A FULLWIDTH COLON), thanks China
                    Dim replacement As String = "$3"
                    Dim rgx As New System.Text.RegularExpressions.Regex(pattern)
                    sPhoneNumber = rgx.Replace(sString, replacement)
                    sPhoneNumber = Helpers.Generic.HtmlDecode(sPhoneNumber)

                    If Not sPhoneNumber.Equals(sString) Then
                        clsGlobals.g_Support_PhoneNumber = sPhoneNumber
                        Helpers.Logger.WriteRelative(sPhoneNumber, , 4)

                        bFoundIt = True
                    Else
                        Helpers.Logger.WriteRelative("something went wrong, could not extract phone number", , 4)

                        Throw New Exception("Error while extracting phone number")
                    End If
                End If
            Next

            If Not bFoundIt Then
                Throw New Exception("Could not find find string id 1100000 in '" & sFile & "'")
            End If

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteRelative("ERROR!!!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.WriteRelative(ex.StackTrace, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

        Return False
    End Function

    Public Shared Function WritePhoneNumberToLanguageFiles() As Boolean
        Helpers.Logger.WriteRelative("iterating through list of language files, Gotta Catch 'Em All", , 2)

        Try
            For Each file As String In My.Computer.FileSystem.GetFiles(clsGlobals.c_Path_LanguageFiles, FileIO.SearchOption.SearchTopLevelOnly, "*.xml")
                Helpers.Logger.WriteRelative("found: " & file, , 3)

                If _WritePhoneNumberToLanguageFile(file) Then
                    Helpers.Logger.WriteRelative("ok", , 4)
                Else
                    Helpers.Logger.WriteRelative("FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            Next
        Catch ex As Exception
            Helpers.Logger.WriteRelative("ERROR!!!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            Helpers.Logger.WriteRelative(ex.StackTrace, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)

            Return False
        End Try

        Return True
    End Function

    Private Shared Function _WritePhoneNumberToLanguageFile(sFile As String) As Boolean
        Try
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            Dim bFoundIt As Boolean = False
            Dim bUpdated As Boolean = False


            xmlSkCfg = New Xml.XmlDocument

            Helpers.Logger.WriteRelative("loading", , 1)
            Helpers.Logger.WriteRelative(sFile, , 2)
            xmlSkCfg.Load(sFile)
            Helpers.Logger.WriteRelative("ok", , 3)

            Helpers.Logger.WriteRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("language")

            If Not xmlNode_Root Is Nothing Then
                Helpers.Logger.WriteRelative("ok", , 3)
            Else
                Throw New Exception("root node (language) not found")
            End If


            Helpers.Logger.WriteRelative("loading string nodelist", , 2)
            Dim xmlNodeList_Strings As Xml.XmlNodeList
            Dim xmlNode_String As Xml.XmlNode

            xmlNodeList_Strings = xmlNode_Root.SelectNodes("string")
            For Each xmlNode_String In xmlNodeList_Strings
                If xmlNode_String.Attributes.GetNamedItem("id").InnerText = cStringId__PhoneNumber Then
                    'found the specific language string id, mark it for a backup
                    Helpers.Logger.WriteRelative("found phone number string", , 3)
                    bFoundIt = True

                    'get existing phone number
                    Helpers.Logger.WriteRelative("updating current phone number", , 3)

                    Dim sCurrent As String = xmlNode_String.InnerText
                    Dim sNew As String = ""
                    ' <string id="1100000">OneView Connection Centre helpdesk (gratis): 00800 3400 1111</string>

                    Dim pattern As String = "(.+)(:|：) (.+)"
                    Dim replacement As String = "$1$2 " & Helpers.Generic.HtmlEncode(clsGlobals.g_Support_PhoneNumber)
                    Dim rgx As New System.Text.RegularExpressions.Regex(pattern)
                    sNew = rgx.Replace(sCurrent, replacement)

                    If Not sNew.Equals(sCurrent) Then
                        Helpers.Logger.WriteRelative("old", , 4)
                        Helpers.Logger.WriteEmptyLine()
                        Helpers.Logger.WriteEmptyLine()
                        Helpers.Logger.WriteWithoutDate(sCurrent)
                        Helpers.Logger.WriteEmptyLine()
                        Helpers.Logger.WriteRelative("new", , 4)
                        Helpers.Logger.WriteEmptyLine()
                        Helpers.Logger.WriteWithoutDate(sNew)
                        Helpers.Logger.WriteEmptyLine()

                        'xmlNode_String.InnerText = sNew
                        xmlNode_String.InnerXml = sNew
                        Helpers.Logger.WriteRelative("updated", , 4)
                        Helpers.Logger.WriteRelative("ok", , 5)

                        bUpdated = True

                        Exit For
                    Else
                        Helpers.Logger.WriteRelative("something went wrong, could not update phone number", , 4)
                    End If
                End If
            Next

            If bFoundIt And bUpdated Then
                MakeBackup(sFile)

                Helpers.Logger.WriteRelative("saving", , 1)
                xmlSkCfg.Save(sFile)
                Helpers.Logger.WriteRelative("ok", , 2)
            Else
                If Not bFoundIt Then
                    Helpers.Logger.WriteRelative("String id '" & cStringId__PhoneNumber & "' not found!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 3)
                    Helpers.Logger.WriteRelative("File not changed!", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                Else
                    Helpers.Logger.WriteRelative("String id '" & cStringId__PhoneNumber & "' was found, but was not updated!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                End If
            End If

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteRelative("ERROR!!!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.WriteRelative(ex.StackTrace, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

        Return False
    End Function

    Public Shared Function WriteGlobalSettings() As Boolean
        Dim lines As New List(Of String)

        Helpers.Logger.WriteRelative("reading script file", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

        If Not IO.File.Exists(sFile) Then
            Helpers.Logger.WriteRelative("FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        Else
            Helpers.Logger.WriteRelative("ok", , 2)
        End If

        Try
            Helpers.Logger.WriteRelative("setting values", , 1)

            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = "var iBAHN_Global_HotelURL=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Url_Hotel) & """;"
                        Helpers.Logger.WriteRelative("HotelUrl", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = "var iBAHN_Global_InternetURL=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Url_Internet) & """;"
                        Helpers.Logger.WriteRelative("InternetURL", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = "var iBAHN_Global_WeatherURL=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Url_Weather) & """;"
                        Helpers.Logger.WriteRelative("WeatherURL", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = "var iBAHN_Global_MapURL=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Url_Map) & """;"
                        Helpers.Logger.WriteRelative("MapURL", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = "var iBAHN_Global_SiteAddress=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Address_Site) & """;"
                        Helpers.Logger.WriteRelative("SiteAddress", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        line = "var iBAHN_Global_Brand=" & HotelBrands.GetCurrent.BrandIndex & ";"

                        Helpers.Logger.WriteRelative("Brand", , 2)
                        Helpers.Logger.WriteRelative(line, , 3)
                    End If

                    lines.Add(line)
                End While
            End Using

            Helpers.Logger.WriteRelative("writing script file", , 1)
            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using

            Helpers.Logger.WriteRelative("ok", , 2)
            Return True
        Catch ex As Exception
            Helpers.Logger.WriteRelative("FAIL", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End Try
    End Function

    Private Shared Function MakeBackup(sFile As String) As Boolean
        Helpers.Logger.WriteRelative("creating backup", , 1)

        Try
            Dim oFile As System.IO.FileInfo

            oFile = New System.IO.FileInfo(sFile)

            Dim sBackupPath As String = oFile.DirectoryName
            Dim sBackupName As String = oFile.Name
            Dim sBackupFile As String
            Dim oDirInfo As System.IO.DirectoryInfo
            sBackupPath = sBackupPath.Replace("C:", "")
            sBackupPath = sBackupPath.Replace("c:", "")
            sBackupPath = sBackupPath.Replace("\\", "\")
            sBackupPath = clsGlobals.g_BackupDirectory & "\" & sBackupPath

            sBackupFile = sBackupPath & "\" & sBackupName

            Helpers.Logger.WriteRelative("backup: " & sFile & " => " & sBackupFile, , 2)

            oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oFile = New System.IO.FileInfo(sFile)
            oFile.CopyTo(sBackupFile, True)

            Helpers.Logger.WriteRelative("ok", , 3)

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteRelative("ERROR!!!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Helpers.Logger.WriteRelative(ex.StackTrace, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            Return False
        End Try
    End Function

    Private Shared Function CleanText(txt As String) As String
        txt = txt.Replace("=""", "")
        txt = txt.Replace(""";", "")

        Return txt
    End Function
End Class
