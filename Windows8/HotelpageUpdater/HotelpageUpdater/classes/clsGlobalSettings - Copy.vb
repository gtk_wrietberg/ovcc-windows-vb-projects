﻿Public Class clsGlobalSettings
    Private Shared ReadOnly c_ThemeResourceFields As Integer = 6
    Private Shared ReadOnly cStringId__PhoneNumber As String = "1100000"

    Public Shared Function GetConfigFilesForFrontend() As Boolean
        clsGlobals.g_SKConfigFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
        clsGlobals.g_UIConfigFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "") & "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"
        clsGlobals.g_UIScriptFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "") & "\Skins\default\Scripts\guest-tek.js"

        Return True
    End Function

    Public Shared Function LoadThemes() As Boolean
        Dim bOk As Boolean = False

        HotelBrands.Reset()

        clsGlobals.oLogger.WriteToLogRelative("loading theme resource file", , 1)

        Using strReader As New IO.StringReader(My.Resources.ovcc_themes)
            Using textparser As New FileIO.TextFieldParser(strReader)
                textparser.TextFieldType = FileIO.FieldType.Delimited
                textparser.Delimiters = {","}
                textparser.HasFieldsEnclosedInQuotes = True

                While Not textparser.EndOfData
                    Try
                        Dim curRow As String() = textparser.ReadFields()

                        If curRow.Count <> c_ThemeResourceFields Then
                            Throw New FileIO.MalformedLineException("incorrect number of fields (" & curRow.Count.ToString & " instead of " & c_ThemeResourceFields.ToString)
                        End If


                        clsGlobals.oLogger.WriteToLogRelative("found theme", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(curRow(1), , 3)

                        Dim _brand As New HotelBrands.HotelBrand With {
                            .BrandIndex = CInt(curRow(0)),
                            .Name = curRow(1),
                            .Path_Theme = curRow(2),
                            .Path_Screensaver = curRow(3),
                            .Path_Logout = curRow(4),
                            .Path_PaymentDialog = curRow(5)
                        }

                        HotelBrands.Add(_brand)

                        bOk = True
                    Catch ex As FileIO.MalformedLineException
                        bOk = False

                        clsGlobals.oLogger.WriteToLogRelative("uh oh", , 2)
                        clsGlobals.oLogger.WriteToLogRelative("error: " & ex.Message, , 3)

                        Stop
                    End Try
                End While
            End Using
        End Using

        Return bOk
    End Function

    Public Shared Function ReadPassword() As Boolean
        clsGlobals.oLogger.WriteToLogRelative("loading password file", , 1)

        If Not clsGlobals.oPw.LoadPasswordFile() Then
            clsGlobals.oLogger.WriteToLogRelative("uh oh", , 2)

            Return False
        End If

        Dim sTmp As String = "", sTmp2 As String = "", sErr As String = ""

        clsGlobals.oLogger.WriteToLogRelative("loading password", , 1)
        If clsGlobals.oPw.GetPassword(sTmp, sTmp2, sErr) Then
            clsGlobals.oLogger.WriteToLogRelative("current password=" & sTmp, , 2)
            clsGlobals.oLogger.WriteToLogRelative("backup password=" & sTmp2, , 2)

            clsGlobals.g_Password = sTmp2
            If sTmp.Equals(sTmp2) And Not sTmp.Equals("") Then
                clsGlobals.g_PasswordEnabled = True
            Else
                clsGlobals.g_PasswordEnabled = False
            End If

            Return True
        Else
            clsGlobals.oLogger.WriteToLogRelative("uh oh", , 2)
            clsGlobals.oLogger.WriteToLogRelative("error: " & sErr, , 3)
        End If

        Return False
    End Function

    Public Shared Function WritePassword() As Boolean
        clsGlobals.oLogger.WriteToLogRelative("saving password file", , 1)

        clsGlobals.oPw.SetPassword(clsGlobals.g_Password, clsGlobals.g_PasswordEnabled)

        Return True
    End Function

    Public Shared Function ReadGlobalSettings() As Boolean
        clsGlobals.oLogger.WriteToLogRelative("reading script file", , 1)

        Dim sFile As String

        sFile = clsGlobals.g_UIConfigFile

        clsGlobals.oLogger.WriteToLogRelative(sFile, , 2)

        If Not IO.File.Exists(sFile) Then
            clsGlobals.oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        Else
            clsGlobals.oLogger.WriteToLogRelative("ok", , 3)
        End If

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = line.Replace("var iBAHN_Global_HotelURL", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)
                        clsGlobals.g_Url_Hotel = line

                        clsGlobals.oLogger.WriteToLogRelative("found hotel url", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = line.Replace("var iBAHN_Global_InternetURL", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Url_Internet = line

                        clsGlobals.oLogger.WriteToLogRelative("found internet url", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = line.Replace("var iBAHN_Global_WeatherURL", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Url_Weather = line

                        clsGlobals.oLogger.WriteToLogRelative("found weather url", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = line.Replace("var iBAHN_Global_MapURL", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Url_Map = line

                        clsGlobals.oLogger.WriteToLogRelative("found map url", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = line.Replace("var iBAHN_Global_SiteAddress", "")
                        line = CleanText(line)
                        line = Helpers.Generic.HtmlDecode(line)

                        clsGlobals.g_Address_Site = line

                        clsGlobals.oLogger.WriteToLogRelative("found site address", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        line = line.Replace("var iBAHN_Global_Brand=", "")
                        line = Helpers.Generic.NumbersOnly(line)

                        clsGlobals.oLogger.WriteToLogRelative("found brand index", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)

                        clsGlobals.oHotelBrand.Reset()
                        If IsNumeric(line) Then
                            clsGlobals.g_Brand = line

                            If line = 666 Then
                                clsGlobals.oHotelBrand.BrandiBAHN = True
                            ElseIf line = 667 Then
                                clsGlobals.oHotelBrand.BrandGuestTek = True
                            ElseIf line = 668 Then
                                clsGlobals.oHotelBrand.BrandResidenceInn = True
                            ElseIf line = 669 Then
                                clsGlobals.oHotelBrand.BrandMarriott = True
                            ElseIf line = 670 Then
                                clsGlobals.oHotelBrand.BrandTowneplace = True
                            ElseIf line = 671 Then
                                clsGlobals.oHotelBrand.BrandHotelArts = True
                            Else
                                clsGlobals.oHotelBrand.BrandNumber = line
                            End If

                            clsGlobals.oHotelBrand.BrandNumber = line
                        End If
                    End If
                End While
            End Using

            Return True
        Catch ex As Exception : Return False : End Try
    End Function

    Public Shared Function ReadPhoneNumberFromEnglishLanguageFile() As Boolean
        ' read english.xml from c_Path_LanguageFiles
        Dim bFoundIt As Boolean = False
        Dim sFile As String = ""
        Dim sPhoneNumber As String = clsGlobals.g_Support_PhoneNumber

        sFile = clsGlobals.c_Path_LanguageFiles & "\english.xml"


        Try
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            clsGlobals.oLogger.WriteToLogRelative("loading", , 1)
            clsGlobals.oLogger.WriteToLogRelative(sFile, , 2)
            xmlSkCfg.Load(sFile)
            clsGlobals.oLogger.WriteToLogRelative("ok", , 3)

            clsGlobals.oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("language")

            If Not xmlNode_Root Is Nothing Then
                clsGlobals.oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("root node (language) not found")
            End If


            clsGlobals.oLogger.WriteToLogRelative("loading string nodelist", , 2)
            Dim xmlNodeList_Strings As Xml.XmlNodeList
            Dim xmlNode_String As Xml.XmlNode
            Dim sTmp As String = ""

            xmlNodeList_Strings = xmlNode_Root.SelectNodes("string")
            For Each xmlNode_String In xmlNodeList_Strings
                If xmlNode_String.Attributes.GetNamedItem("id").InnerText = cStringId__PhoneNumber Then
                    'found the specific language string id, mark it for a backup
                    clsGlobals.oLogger.WriteToLogRelative("found phone number string", , 3)

                    'get existing phone number
                    clsGlobals.oLogger.WriteToLogRelative("retrieving current phone number", , 3)

                    Dim sString As String = xmlNode_String.InnerText
                    ' <string id="1100000">OneView Connection Centre helpdesk (gratis): 00800 3400 1111</string>

                    Dim pattern As String = "(.+)(:|：) (.+)" 'Notice the funky colon (U+FF1A FULLWIDTH COLON), thanks China
                    Dim replacement As String = "$3"
                    Dim rgx As New System.Text.RegularExpressions.Regex(pattern)
                    sPhoneNumber = rgx.Replace(sString, replacement)
                    sPhoneNumber = Helpers.Generic.HtmlDecode(sPhoneNumber)

                    If Not sPhoneNumber.Equals(sString) Then
                        clsGlobals.g_Support_PhoneNumber = sPhoneNumber
                        clsGlobals.oLogger.WriteToLogRelative(sPhoneNumber, , 4)

                        bFoundIt = True
                    Else
                        clsGlobals.oLogger.WriteToLogRelative("something went wrong, could not extract phone number", , 4)

                        Throw New Exception("Error while extracting phone number")
                    End If
                End If
            Next

            If Not bFoundIt Then
                Throw New Exception("Could not find find string id 1100000 in '" & sFile & "'")
            End If

            Return True
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            clsGlobals.oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            clsGlobals.oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

        Return False
    End Function

    Public Shared Function WritePhoneNumberToLanguageFiles() As Boolean
        clsGlobals.oLogger.WriteToLog("iterating through list of language files, Gotta Catch 'Em All", , 2)

        Try
            For Each file As String In My.Computer.FileSystem.GetFiles(clsGlobals.c_Path_LanguageFiles, FileIO.SearchOption.SearchTopLevelOnly, "*.xml")
                clsGlobals.oLogger.WriteToLog("found: " & file, , 3)

                If _WritePhoneNumberToLanguageFile(file) Then
                    clsGlobals.oLogger.WriteToLog("ok", , 4)
                Else
                    clsGlobals.oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            Next
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            clsGlobals.oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            clsGlobals.oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

            Return False
        End Try

        Return True
    End Function

    Private Shared Function _WritePhoneNumberToLanguageFile(sFile As String) As Boolean
        Try
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode

            Dim bFoundIt As Boolean = False
            Dim bUpdated As Boolean = False


            xmlSkCfg = New Xml.XmlDocument

            clsGlobals.oLogger.WriteToLogRelative("loading", , 1)
            clsGlobals.oLogger.WriteToLogRelative(sFile, , 2)
            xmlSkCfg.Load(sFile)
            clsGlobals.oLogger.WriteToLogRelative("ok", , 3)

            clsGlobals.oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("language")

            If Not xmlNode_Root Is Nothing Then
                clsGlobals.oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("root node (language) not found")
            End If


            clsGlobals.oLogger.WriteToLogRelative("loading string nodelist", , 2)
            Dim xmlNodeList_Strings As Xml.XmlNodeList
            Dim xmlNode_String As Xml.XmlNode

            xmlNodeList_Strings = xmlNode_Root.SelectNodes("string")
            For Each xmlNode_String In xmlNodeList_Strings
                If xmlNode_String.Attributes.GetNamedItem("id").InnerText = cStringId__PhoneNumber Then
                    'found the specific language string id, mark it for a backup
                    clsGlobals.oLogger.WriteToLogRelative("found phone number string", , 3)
                    bFoundIt = True

                    'get existing phone number
                    clsGlobals.oLogger.WriteToLogRelative("updating current phone number", , 3)

                    Dim sCurrent As String = xmlNode_String.InnerText
                    Dim sNew As String = ""
                    ' <string id="1100000">OneView Connection Centre helpdesk (gratis): 00800 3400 1111</string>

                    Dim pattern As String = "(.+)(:|：) (.+)"
                    Dim replacement As String = "$1$2 " & Helpers.Generic.HtmlEncode(clsGlobals.g_Support_PhoneNumber)
                    Dim rgx As New System.Text.RegularExpressions.Regex(pattern)
                    sNew = rgx.Replace(sCurrent, replacement)

                    If Not sNew.Equals(sCurrent) Then
                        clsGlobals.oLogger.WriteToLogRelative("old", , 4)
                        clsGlobals.oLogger.WriteEmptyLineToLog()
                        clsGlobals.oLogger.WriteToLogWithoutDate(sCurrent)
                        clsGlobals.oLogger.WriteEmptyLineToLog()
                        clsGlobals.oLogger.WriteToLogRelative("new", , 4)
                        clsGlobals.oLogger.WriteEmptyLineToLog()
                        clsGlobals.oLogger.WriteToLogWithoutDate(sNew)
                        clsGlobals.oLogger.WriteEmptyLineToLog()

                        'xmlNode_String.InnerText = sNew
                        xmlNode_String.InnerXml = sNew
                        clsGlobals.oLogger.WriteToLogRelative("updated", , 4)
                        clsGlobals.oLogger.WriteToLogRelative("ok", , 5)

                        bUpdated = True

                        Exit For
                    Else
                        clsGlobals.oLogger.WriteToLogRelative("something went wrong, could not update phone number", , 4)
                    End If
                End If
            Next

            If bFoundIt And bUpdated Then
                MakeBackup(sFile)

                clsGlobals.oLogger.WriteToLogRelative("saving", , 1)
                xmlSkCfg.Save(sFile)
                clsGlobals.oLogger.WriteToLogRelative("ok", , 2)
            Else
                If Not bFoundIt Then
                    clsGlobals.oLogger.WriteToLogRelative("String id '" & cStringId__PhoneNumber & "' not found!", Logger.MESSAGE_TYPE.LOG_WARNING, 3)
                    clsGlobals.oLogger.WriteToLogRelative("File not changed!", Logger.MESSAGE_TYPE.LOG_WARNING, 4)
                Else
                    clsGlobals.oLogger.WriteToLogRelative("String id '" & cStringId__PhoneNumber & "' was found, but was not updated!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                End If
            End If

            Return True
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            clsGlobals.oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            clsGlobals.oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

        Return False
    End Function

    Public Shared Function WriteGlobalSettings() As Boolean
        Dim lines As New List(Of String)

        clsGlobals.oLogger.WriteToLogRelative("reading script file", , 1)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

        If Not IO.File.Exists(sFile) Then
            clsGlobals.oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Return False
        Else
            clsGlobals.oLogger.WriteToLogRelative("ok", , 2)
        End If

        Try
            clsGlobals.oLogger.WriteToLogRelative("setting values", , 1)

            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = "var iBAHN_Global_HotelURL=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Url_Hotel) & """;"
                        clsGlobals.oLogger.WriteToLogRelative("HotelUrl", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = "var iBAHN_Global_InternetURL=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Url_Internet) & """;"
                        clsGlobals.oLogger.WriteToLogRelative("InternetURL", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = "var iBAHN_Global_WeatherURL=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Url_Weather) & """;"
                        clsGlobals.oLogger.WriteToLogRelative("WeatherURL", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = "var iBAHN_Global_MapURL=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Url_Map) & """;"
                        clsGlobals.oLogger.WriteToLogRelative("MapURL", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = "var iBAHN_Global_SiteAddress=""" & Helpers.Generic.HtmlEncode(clsGlobals.g_Address_Site) & """;"
                        clsGlobals.oLogger.WriteToLogRelative("SiteAddress", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        line = "var iBAHN_Global_Brand=" & clsGlobals.oHotelBrand.BrandNumber & ";"

                        'If clsGlobals.oHotelBrand.BrandGuestTek Then
                        '    line = "var iBAHN_Global_Brand=667;"
                        'ElseIf clsGlobals.oHotelBrand.BrandiBAHN Then
                        '    line = "var iBAHN_Global_Brand=666;"
                        'ElseIf clsGlobals.oHotelBrand.BrandResidenceInn Then
                        '    line = "var iBAHN_Global_Brand=668;"
                        'Else
                        '    line = "var iBAHN_Global_Brand=" & clsGlobals.oHotelBrand.BrandNumber & ";"
                        'End If
                        clsGlobals.oLogger.WriteToLogRelative("Brand", , 2)
                        clsGlobals.oLogger.WriteToLogRelative(line, , 3)
                    End If

                    lines.Add(line)
                End While
            End Using

            clsGlobals.oLogger.WriteToLogRelative("writing script file", , 1)
            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using

            clsGlobals.oLogger.WriteToLogRelative("ok", , 2)
            Return True
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLogRelative("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            clsGlobals.oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            Return False
        End Try
    End Function

    Private Shared Function MakeBackup(sFile As String) As Boolean
        clsGlobals.oLogger.WriteToLogRelative("creating backup", , 1)

        Try
            Dim oFile As System.IO.FileInfo

            oFile = New System.IO.FileInfo(sFile)

            Dim sBackupPath As String = oFile.DirectoryName
            Dim sBackupName As String = oFile.Name
            Dim sBackupFile As String
            Dim oDirInfo As System.IO.DirectoryInfo
            sBackupPath = sBackupPath.Replace("C:", "")
            sBackupPath = sBackupPath.Replace("c:", "")
            sBackupPath = sBackupPath.Replace("\\", "\")
            sBackupPath = clsGlobals.g_BackupDirectory & "\" & sBackupPath

            sBackupFile = sBackupPath & "\" & sBackupName

            clsGlobals.oLogger.WriteToLogRelative("backup: " & sFile & " => " & sBackupFile, , 2)

            oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oFile = New System.IO.FileInfo(sFile)
            oFile.CopyTo(sBackupFile, True)

            clsGlobals.oLogger.WriteToLogRelative("ok", , 3)

            Return True
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            clsGlobals.oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            clsGlobals.oLogger.WriteToLogRelative(ex.StackTrace, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            Return False
        End Try
    End Function

    Private Shared Function CleanText(txt As String) As String
        txt = txt.Replace("=""", "")
        txt = txt.Replace(""";", "")

        Return txt
    End Function
End Class
