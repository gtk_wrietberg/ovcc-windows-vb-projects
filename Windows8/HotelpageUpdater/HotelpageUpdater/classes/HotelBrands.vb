﻿Public Class HotelBrands
    Public Class HotelBrand
        Private mName As String
        Public Property Name() As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property

        Private mPath_Theme As String
        Public Property Path_Theme() As String
            Get
                Return mPath_Theme
            End Get
            Set(ByVal value As String)
                mPath_Theme = value
            End Set
        End Property
        Public ReadOnly Property Path_Theme_Exists As Boolean
            Get
                Return IO.File.Exists(mPath_Theme)
            End Get
        End Property

        Private mPath_Logout As String
        Public Property Path_Logout() As String
            Get
                Return mPath_Logout
            End Get
            Set(ByVal value As String)
                mPath_Logout = value
            End Set
        End Property
        Public ReadOnly Property Path_Logout_Exists As Boolean
            Get
                Return IO.File.Exists(mPath_Logout)
            End Get
        End Property

        Private mPath_Screensaver As String
        Public Property Path_Screensaver() As String
            Get
                Return mPath_Screensaver
            End Get
            Set(ByVal value As String)
                mPath_Screensaver = value
            End Set
        End Property
        Public ReadOnly Property Path_Screensaver_Exists As Boolean
            Get
                Return IO.File.Exists(mPath_Screensaver)
            End Get
        End Property

        Private mPath_PaymentDialog As String
        Public Property Path_PaymentDialog() As String
            Get
                Return mPath_PaymentDialog
            End Get
            Set(ByVal value As String)
                mPath_PaymentDialog = value
            End Set
        End Property
        Public ReadOnly Property Path_PaymentDialog_Exists As Boolean
            Get
                Return IO.File.Exists(mPath_PaymentDialog)
            End Get
        End Property

        Public ReadOnly Property ArePathsValid As Boolean
            Get
                Return Path_Logout_Exists And Path_PaymentDialog_Exists And Path_Screensaver_Exists And Path_Theme_Exists
            End Get
        End Property

        Private mBrandIndex As Integer
        Public Property BrandIndex() As Integer
            Get
                Return mBrandIndex
            End Get
            Set(ByVal value As Integer)
                mBrandIndex = value
            End Set
        End Property

        Private mIsLastHilton As Boolean
        Public Property IsLastHilton() As Boolean
            Get
                Return mIsLastHilton
            End Get
            Set(ByVal value As Boolean)
                mIsLastHilton = value
            End Set
        End Property

        Private mIndex As Integer
        Public Property Index() As Integer
            Get
                Return mIndex
            End Get
            Set(ByVal value As Integer)
                mIndex = value
            End Set
        End Property

        Private mExtraInfo As String
        Public Property ExtraInfo() As String
            Get
                Return mExtraInfo
            End Get
            Set(ByVal value As String)
                mExtraInfo = value
            End Set
        End Property

        Private mPaymentEnabled As Boolean
        Public Property PaymentEnabled() As Boolean
            Get
                Return mPaymentEnabled
            End Get
            Set(ByVal value As Boolean)
                mPaymentEnabled = value
            End Set
        End Property

        Private mPasswordEnabled As Boolean
        Public Property PasswordEnabled() As Boolean
            Get
                Return mPasswordEnabled
            End Get
            Set(ByVal value As Boolean)
                mPasswordEnabled = value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Dim _s As String = ""
            Dim _t As Type = Me.GetType
            Dim _pi As Reflection.PropertyInfo() = _t.GetProperties()

            For Each _p As Reflection.PropertyInfo In _pi
                _s &= _p.Name & "=" & _p.GetValue(Me, Nothing) & vbCrLf
            Next

            Return _s
        End Function
    End Class

    'Public Enum BrandNumbers As Integer
    '    Hilton_-_Standard = 0
    '    Hilton_-_Garden_Inn = 1
    '    Hilton_-_Hampton = 2
    '    Hilton_-_Waldord_Astoria = 3
    '    Hilton_-_Conrad = 4
    '    Hilton_-_DoubleTree = 5
    '    Hilton_-_Canopy = 6
    '    Hilton_-_Curio = 7
    '    Hilton_-_Embassy_Suites = 8
    '    Hilton_-_Homewood_Suites = 9
    '    Hilton_-_Home2 = 10
    '    Hilton_-_Hilton_Grand_Vacations = 11
    '    Hilton_-_Tru = 12
    '    Hilton_-_Tapestry_Collection = 13
    '    iBahn = 666
    '    GuestTek = 667
    '    ResidenceInn = 668
    '    Marriott = 669
    '    Towneplace = 670
    '    HotelArts = 671
    'End Enum

    Public Shared ReadOnly HiltonBrandIndexLimit As Integer = 600

    Private Shared mItems As New List(Of HotelBrand)
    Private Shared mCurrent As New HotelBrand

    Public Shared Sub Reset()
        mItems = New List(Of HotelBrand)

        ResetCurrent()
    End Sub

    Public Shared Sub ResetCurrent()
        mCurrent = New HotelBrand
    End Sub

    Public Shared Function Count() As Integer
        Return mItems.Count
    End Function

    Public Shared Function Add(_item As HotelBrand) As Boolean
        If Not Exists(_item) Then
            mItems.Add(_item)

            Return True
        End If

        Return False
    End Function

    Public Shared Function Exists(_item As HotelBrand) As Boolean
        For Each _searchitem As HotelBrand In mItems
            If _searchitem.Name.ToLower.Equals(_item.Name.ToLower) Then
                Return True
            End If
        Next

        Return False
    End Function

    Public Shared Function SetLastHilton() As Boolean
        For _i As Integer = 1 To mItems.Count - 1
            If mItems.Item(_i).BrandIndex > HotelBrands.HiltonBrandIndexLimit Then
                mItems.Item(_i - 1).IsLastHilton = True

                Exit For
            End If
        Next

        Return True
    End Function

    Public Shared Function GetMaxBrandIndex() As Integer
        Dim _bi As Integer = -1

        For _i As Integer = 0 To mItems.Count - 1
            If mItems.Item(_i).BrandIndex > _bi Then
                _bi = mItems.Item(_i).BrandIndex
            End If
        Next

        Return _bi
    End Function

    Public Shared Function HiltonDefault() As HotelBrand
        Return Find(0)
    End Function

    Public Shared Function Find(_name As String) As HotelBrand
        For Each _searchitem As HotelBrand In mItems
            If _searchitem.Name.Equals(_name) Then
                Return _searchitem
            End If
        Next

        Return Nothing
    End Function

    Public Shared Function Find(_BrandIndex As Integer) As HotelBrand
        For Each _searchitem As HotelBrand In mItems
            If _searchitem.BrandIndex = _BrandIndex Then
                Return _searchitem
            End If
        Next

        Return Nothing
    End Function

    Public Shared Function FindByIndex(_Index As Integer) As HotelBrand
        For Each _searchitem As HotelBrand In mItems
            If _searchitem.Index = _Index Then
                Return _searchitem
            End If
        Next

        Return Nothing
    End Function

    Public Shared Function IsValid(_item As HotelBrand) As Boolean
        Dim _ret As Boolean = True

        _ret = _ret And _item.Path_Logout_Exists
        _ret = _ret And _item.Path_PaymentDialog_Exists
        _ret = _ret And _item.Path_Screensaver_Exists
        _ret = _ret And _item.Path_Theme_Exists

        Return _ret
    End Function

    Public Shared Function CountInvalidThemes() As Integer
        Dim _count As Integer = 0

        For Each _brand As HotelBrand In mItems
            If Not _brand.ArePathsValid Then
                _count += 1
            End If
        Next

        Return _count
    End Function

    Public Shared Function SetCurrent(_name As String) As Boolean
        Dim _brand As New HotelBrand

        _brand = Find(_name)

        If _brand IsNot Nothing Then
            mCurrent = _brand

            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function SetCurrent(_index As Integer) As Boolean
        Dim _brand As New HotelBrand

        _brand = Find(_index)

        If _brand IsNot Nothing Then
            mCurrent = _brand

            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Sub SetCurrentExtraString(_string As String)
        mCurrent.ExtraInfo = _string
    End Sub

    Public Shared Function GetCurrent() As HotelBrand
        Return mCurrent
    End Function

    Public Shared Iterator Function GetBrands() As IEnumerable(Of HotelBrand)
        Dim counter As Integer = -1

        If mItems.Count > 0 Then
            While counter < mItems.Count
                counter += 1

                If counter < mItems.Count Then
                    Yield mItems.Item(counter)
                End If
            End While
        End If
    End Function
End Class
