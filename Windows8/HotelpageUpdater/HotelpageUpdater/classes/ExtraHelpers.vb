﻿Public Class ExtraHelpers
    Public Class Path
        Private Const MAX_PATH As Integer = 260
        Private Const INTERNET_MAX_URL_LENGTH As Integer = 2048 + 32 + 3

        Private Declare Unicode Function PathCreateFromUrl Lib "shlwapi.dll" Alias "PathCreateFromUrlW" (ByVal url As String, ByVal path As System.Text.StringBuilder, ByRef pathLength As System.UInt32, ByVal reserved As Integer) As Integer

        Public Shared Function PathCreateFromUrl(Url As String) As String
            Dim sz As System.UInt32
            Dim sb As System.Text.StringBuilder
            Dim rc As Integer

            sz = Convert.ToUInt32(INTERNET_MAX_URL_LENGTH)
            sb = New System.Text.StringBuilder(INTERNET_MAX_URL_LENGTH)

            rc = PathCreateFromUrl(Url, sb, sz, 0)

            Return sb.ToString()
        End Function
    End Class
End Class
