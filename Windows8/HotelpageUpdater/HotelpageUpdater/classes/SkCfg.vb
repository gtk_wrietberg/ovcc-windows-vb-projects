Public Class SkCfg
    Private mOldSkCfgFile As String
    Private mNewSkCfgFile As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String
    Private mUrlList As List(Of String)

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Function MakeListOfUrls() As Boolean
        mUrlList = New List(Of String)

        Try
            Dim tmpList As New List(Of String)

            tmpList.Add("http://" & Helpers.Generic.GetHostnameFromUrl(clsGlobals.g_Url_Hotel))
            tmpList.Add("https://" & Helpers.Generic.GetHostnameFromUrl(clsGlobals.g_Url_Hotel))

            tmpList.Add("http://" & Helpers.Generic.GetHostnameFromUrl(clsGlobals.g_Url_Weather))
            tmpList.Add("https://" & Helpers.Generic.GetHostnameFromUrl(clsGlobals.g_Url_Weather))

            tmpList.Add("http://" & Helpers.Generic.GetHostnameFromUrl(clsGlobals.g_Url_Map))
            tmpList.Add("https://" & Helpers.Generic.GetHostnameFromUrl(clsGlobals.g_Url_Map))


            'tmpList.Add(Helpers.GetResponseHostname(clsGlobals.g_Url_Hotel))
            'tmpList.Add(Helpers.GetResponseHostname(clsGlobals.g_Url_Weather))
            'tmpList.Add(Helpers.GetResponseHostname(clsGlobals.g_Url_Map))

            mUrlList = tmpList.Distinct().ToList

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Load payment setting into global variable 'clsGlobals.g_PaymentEnabled'
    ''' </summary>
    ''' <returns>true on success, false on exception</returns>
    ''' <remarks></remarks>
    Public Function LoadPaymentEnabled() As Boolean
        Try
            Helpers.Logger.WriteRelative("finding active skcfg file", , 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            Helpers.Logger.WriteRelative("found: " & mOldSkCfgFile, , 2)


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            Helpers.Logger.WriteRelative("loading", , 1)
            Helpers.Logger.WriteRelative(mOldSkCfgFile, , 2)
            xmlSkCfg.Load(mOldSkCfgFile)
            Helpers.Logger.WriteRelative("ok", , 3)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            Helpers.Logger.WriteRelative("loading root node", , 1)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                Helpers.Logger.WriteRelative("ok", , 2)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If


            '*************************************************************************************************************
            'SiteCash
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode

            Helpers.Logger.WriteRelative("loading SiteCash node", , 1)
            xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

#Disable Warning BC42104 ' Variable is used before it has been assigned a value
            If Not xmlNode_SiteCash Is Nothing Then
#Enable Warning BC42104 ' Variable is used before it has been assigned a value
                Helpers.Logger.WriteRelative("ok", , 2)
            Else
                Throw New Exception("SiteCash node not found")
            End If

            Helpers.Logger.WriteRelative("getting SiteCash state", , 1)
            Try
                If xmlNode_SiteCash.Attributes.GetNamedItem("enabled").Value = "true" Then
                    clsGlobals.g_PaymentEnabled = True
                    Helpers.Logger.WriteRelative("enabled", , 2)
                Else
                    clsGlobals.g_PaymentEnabled = False
                    Helpers.Logger.WriteRelative("not enabled", , 2)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteRelative("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteRelative("ERROR!!!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Return False
        End Try
    End Function

    Public Function Update(Optional ByVal bOverWriteOldFile As Boolean = False) As Boolean
        Try
            Helpers.Logger.WriteRelative("finding SiteKiosk install directory", , 1)
            mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            Helpers.Logger.WriteRelative("found: " & mSiteKioskFolder, , 2)

            Helpers.Logger.WriteRelative("finding active skcfg file", , 1)
            mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
            Helpers.Logger.WriteRelative("found: " & mOldSkCfgFile, , 2)


            Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

            sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

            If bOverWriteOldFile Then
                Helpers.Logger.WriteRelative("backing up current config", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)

                Dim sBackupPath As String = oFile.DirectoryName
                Dim sBackupName As String = oFile.Name
                Dim sBackupFile As String
                Dim oDirInfo As System.IO.DirectoryInfo
                sBackupPath = sBackupPath.Replace("C:", "")
                sBackupPath = sBackupPath.Replace("c:", "")
                sBackupPath = sBackupPath.Replace("\\", "\")
                sBackupPath = mBackupFolder & "\" & sBackupPath

                sBackupFile = sBackupPath & "\" & sBackupName

                oDirInfo = New System.IO.DirectoryInfo(sBackupPath)
                If Not oDirInfo.Exists Then oDirInfo.Create()

                oFile = New IO.FileInfo(mOldSkCfgFile)
                oFile.CopyTo(sBackupFile, True)

                Helpers.Logger.WriteRelative("backup: " & mOldSkCfgFile & " => " & sBackupFile, , 2)

                mNewSkCfgFile = mOldSkCfgFile
            Else
                Helpers.Logger.WriteRelative("making copy", , 1)

                oFile = New IO.FileInfo(mOldSkCfgFile)
                mNewSkCfgFile = oFile.DirectoryName & "\GuestTek__InfoUpdater__" & sDateString & ".skcfg"
                oFile.CopyTo(mNewSkCfgFile, True)

                Helpers.Logger.WriteRelative("copy: " & mNewSkCfgFile, , 2)
            End If


            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            Helpers.Logger.WriteRelative("updating", , 1)

            Helpers.Logger.WriteRelative("loading", , 2)
            Helpers.Logger.WriteRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            Helpers.Logger.WriteRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            Helpers.Logger.WriteRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                Helpers.Logger.WriteRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If



            '*************************************************************************************************************
            'SiteCash - replace current url zones
            Helpers.Logger.WriteRelative("SiteCash url zones update", , 2)
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode

            Helpers.Logger.WriteRelative("loading SiteCash node", , 3)
            xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

#Disable Warning BC42104 ' Variable is used before it has been assigned a value
            If Not xmlNode_SiteCash Is Nothing Then
#Enable Warning BC42104 ' Variable is used before it has been assigned a value
                Helpers.Logger.WriteRelative("ok", , 4)
            Else
                Throw New Exception("SiteCash node not found")
            End If

            Helpers.Logger.WriteRelative("setting SiteCash state", , 4)
            Try
                If clsGlobals.g_PaymentEnabled Or clsGlobals.g_PasswordEnabled Then
                    xmlNode_SiteCash.Attributes.GetNamedItem("enabled").Value = "true"
                    Helpers.Logger.WriteRelative("enabled", , 5)
                Else
                    xmlNode_SiteCash.Attributes.GetNamedItem("enabled").Value = "false"
                    Helpers.Logger.WriteRelative("disabled", , 5)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteRelative("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End Try


            Dim xmlNodeList_UrlZones As Xml.XmlNodeList
            Dim xmlNode_UrlZone As Xml.XmlNode
            Dim xmlNodeList_Urls As Xml.XmlNodeList
            Dim xmlNode_Url As Xml.XmlNode
            Dim xmlNode_UrlClone As Xml.XmlNode
            Dim xmlNode_UrlCloneClone As Xml.XmlNode
            Dim xmlNode_FreeUrlZone As Xml.XmlNode

            Helpers.Logger.WriteRelative("removing '" & clsGlobals.c_FreeUrlZoneName & "' url zone, if there", , 3)
            xmlNodeList_UrlZones = xmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
            For Each xmlNode_UrlZone In xmlNodeList_UrlZones
                If xmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = clsGlobals.c_FreeUrlZoneName Then
                    xmlNode_SiteCash.RemoveChild(xmlNode_UrlZone)
                    Helpers.Logger.WriteRelative("ok", , 4)
                End If
            Next

            Helpers.Logger.WriteRelative("adding '" & clsGlobals.c_FreeUrlZoneName & "' url zone", , 3)
            xmlNode_FreeUrlZone = xmlNode_SiteCash.SelectSingleNode("sk:url-zone", ns).CloneNode(True)
            xmlNode_FreeUrlZone.SelectSingleNode("sk:name", ns).InnerText = clsGlobals.c_FreeUrlZoneName

            xmlNodeList_Urls = xmlNode_FreeUrlZone.SelectNodes("sk:url", ns)
            xmlNode_UrlClone = xmlNode_FreeUrlZone.SelectSingleNode("sk:url", ns).CloneNode(True)
            For Each xmlNode_Url In xmlNodeList_Urls
                xmlNode_FreeUrlZone.RemoveChild(xmlNode_Url)
            Next

            For Each sUrl As String In mUrlList
                Helpers.Logger.WriteRelative("adding: " & sUrl, , 4)
                xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
                xmlNode_UrlCloneClone.InnerText = sUrl
                xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            Next

            'Helpers.Logger.WriteRelative("adding: " & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Hotel), , 4)
            'xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            'xmlNode_UrlCloneClone.InnerText = "http://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Hotel)
            'xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            'xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            'xmlNode_UrlCloneClone.InnerText = "https://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Hotel)
            'xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            'Helpers.Logger.WriteRelative("adding: " & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Weather), , 4)
            'xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            'xmlNode_UrlCloneClone.InnerText = "http://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Weather)
            'xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            'xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            'xmlNode_UrlCloneClone.InnerText = "https://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Weather)
            'xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            'Helpers.Logger.WriteRelative("adding: " & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Map), , 4)
            'xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            'xmlNode_UrlCloneClone.InnerText = "http://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Map)
            'xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            'xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            'xmlNode_UrlCloneClone.InnerText = "https://" & Helpers.GetHostnameFromUrl(clsGlobals.g_Url_Map)
            'xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            xmlNode_SiteCash.AppendChild(xmlNode_FreeUrlZone)


            '*************************************************************************************************************
            'StartpageConfig 
            Helpers.Logger.WriteRelative("startpageconfig update", , 2)
            Dim xmlNode_StartpageConfig As Xml.XmlNode
            Dim xmlNode_Startpage As Xml.XmlNode
            Dim xmlNode_TemplateName As Xml.XmlNode
            Dim xmlNode_TemplateEnabled As Xml.XmlNode

            Helpers.Logger.WriteRelative("loading startpageconfig node", , 3)
            xmlNode_StartpageConfig = xmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)
            xmlNode_Startpage = xmlNode_StartpageConfig.SelectSingleNode("sk:startpage", ns)
            xmlNode_TemplateName = xmlNode_StartpageConfig.SelectSingleNode("sk:templatename", ns)
            xmlNode_TemplateEnabled = xmlNode_StartpageConfig.SelectSingleNode("sk:templateenabled", ns)

            xmlNode_TemplateEnabled.InnerText = "false"
            xmlNode_TemplateName.InnerText = ""

            Helpers.Logger.WriteRelative("setting startpage", , 3)
            xmlNode_Startpage.InnerText = HotelBrands.GetCurrent.Path_Theme


            '*************************************************************************************************************
            'Siteskin restore, just to be sure
            Helpers.Logger.WriteRelative("siteskin update", , 2)
            Dim xmlNode_SiteSkin As Xml.XmlNode
            Dim xmlNode_SkinBrowserwindows As Xml.XmlNode
            Dim xmlNode_DefaultSkin As Xml.XmlNode

            Helpers.Logger.WriteRelative("loading siteskin node", , 3)
            xmlNode_SiteSkin = xmlNode_Root.SelectSingleNode("sk:siteskin", ns)
            xmlNode_SkinBrowserwindows = xmlNode_SiteSkin.SelectSingleNode("sk:skin-browserwindows", ns)
            xmlNode_DefaultSkin = xmlNode_SiteSkin.SelectSingleNode("sk:default-skin", ns)

            xmlNode_SkinBrowserwindows.InnerText = "true"
            xmlNode_DefaultSkin.InnerText = "Windows 7 IE8 Skin"


            '*************************************************************************************************************
            'Browserbar config (fullscreen for start page) 
            Helpers.Logger.WriteRelative("browserbar update", , 2)
            Dim xmlNode_Browserbar As Xml.XmlNode
            Dim xmlNode_Hidemode As Xml.XmlNode
            Dim xmlNode_HideWindowBarIfFullscreen As Xml.XmlNode
            Dim xmlNode_BrowserbarUrls As Xml.XmlNode
            Dim xmlNode_LogoutNavigation As Xml.XmlNode
            Dim xmlNode_LogoutNavigationUrl As Xml.XmlNode
            Dim xmlNodeExisting_Urls As Xml.XmlNodeList
            Dim xmlNodeNew_Url As Xml.XmlNode
            Dim bUpdateThisMofo As Boolean = False

            Helpers.Logger.WriteRelative("loading browserbar node", , 3)
            xmlNode_Browserbar = xmlNode_Root.SelectSingleNode("sk:browserbar", ns)

            Helpers.Logger.WriteRelative("setting hidemode node", , 3)
            xmlNode_Hidemode = xmlNode_Browserbar.SelectSingleNode("sk:hidemode", ns)
            xmlNode_Hidemode.InnerText = "2"
            Helpers.Logger.WriteRelative("value", , 4)
            Helpers.Logger.WriteRelative("2", , 5)

            Helpers.Logger.WriteRelative("setting hide-window-bar-if-fullscreen node", , 3)
            xmlNode_HideWindowBarIfFullscreen = xmlNode_Browserbar.SelectSingleNode("sk:hide-window-bar-if-fullscreen", ns)
            xmlNode_HideWindowBarIfFullscreen.InnerText = "true"
            Helpers.Logger.WriteRelative("value", , 4)
            Helpers.Logger.WriteRelative("true", , 5)

            Helpers.Logger.WriteRelative("setting logout-navigation url node", , 3)
            xmlNode_LogoutNavigation = xmlNode_Browserbar.SelectSingleNode("sk:logout-navigation", ns)
            xmlNode_LogoutNavigationUrl = xmlNode_LogoutNavigation.SelectSingleNode("sk:url", ns)
            If Not HotelBrands.GetCurrent.Path_Logout.StartsWith("file://") Then
                xmlNode_LogoutNavigationUrl.InnerText = "file://" & HotelBrands.GetCurrent.Path_Logout
                Helpers.Logger.WriteRelative("value", , 4)
                Helpers.Logger.WriteRelative("file://" & HotelBrands.GetCurrent.Path_Logout, , 5)
            Else
                xmlNode_LogoutNavigationUrl.InnerText = HotelBrands.GetCurrent.Path_Logout
                Helpers.Logger.WriteRelative("value", , 4)
                Helpers.Logger.WriteRelative(HotelBrands.GetCurrent.Path_Logout, , 5)
            End If


            xmlNode_BrowserbarUrls = xmlNode_Browserbar.SelectSingleNode("sk:browserbar-urls", ns)

            xmlNodeExisting_Urls = xmlNode_BrowserbarUrls.SelectNodes("sk:url", ns)
            If xmlNodeExisting_Urls Is Nothing Then
                bUpdateThisMofo = True
            ElseIf xmlNodeExisting_Urls.Count = 0 Then
                bUpdateThisMofo = True
            End If

            Helpers.Logger.WriteRelative("adding fullscreen url", , 4)
            If bUpdateThisMofo Then
                xmlNodeNew_Url = xmlSkCfg.CreateNode(Xml.XmlNodeType.Element, "url", ns.LookupNamespace("sk"))
                xmlNodeNew_Url.InnerText = "file://C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\*"
                xmlNode_BrowserbarUrls.AppendChild(xmlNodeNew_Url)

                Helpers.Logger.WriteRelative("ok", , 5)
            Else
                Helpers.Logger.WriteRelative("skipped", , 5)
            End If


            '*************************************************************************************************************
            'Screensaver
            Dim xmlNode_Screensaver As Xml.XmlNode

            Helpers.Logger.WriteRelative("Screensaver update", , 2)
            xmlNode_Screensaver = xmlNode_Root.SelectSingleNode("sk:screensaver", ns)

            Helpers.Logger.WriteRelative("making sure screensaver is enabled", , 3)
            xmlNode_Screensaver.Attributes.GetNamedItem("enabled").InnerText = "true"

            Helpers.Logger.WriteRelative("setting screensaver", , 3)
            If Not HotelBrands.GetCurrent.Path_Screensaver.StartsWith("file://") Then
                xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://" & HotelBrands.GetCurrent.Path_Screensaver
                Helpers.Logger.WriteRelative("value", , 4)
                Helpers.Logger.WriteRelative("file://" & HotelBrands.GetCurrent.Path_Screensaver, , 5)
            Else
                xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = HotelBrands.GetCurrent.Path_Screensaver
                Helpers.Logger.WriteRelative("value", , 4)
                Helpers.Logger.WriteRelative(HotelBrands.GetCurrent.Path_Screensaver, , 5)
            End If


            '*************************************************************************************************************
            'Saving
            Helpers.Logger.WriteRelative("saving", , 2)
            Helpers.Logger.WriteRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Save(mNewSkCfgFile)
            Helpers.Logger.WriteRelative("ok", , 4)

            If Not bOverWriteOldFile Then
                Helpers.Logger.WriteRelative("activating", , 2)
                Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

                Dim sTempRegVal As String
                sTempRegVal = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

                If sTempRegVal = mNewSkCfgFile Then
                    Helpers.Logger.WriteRelative("ok", , 3)
                Else
                    Helpers.Logger.WriteRelative("fail", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 3)

                    Helpers.Logger.WriteRelative("trying to fix this by overwriting the existing config", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 4)

                    Helpers.Logger.WriteRelative("backing up existing config", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mOldSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile & "." & sDateString & "." & Application.ProductName & ".backup", True)

                    'Application .ProductName 

                    Helpers.Logger.WriteRelative("overwriting existing config", Helpers.Logger.MESSAGE_TYPE.LOG_WARNING, 5)
                    oFile = New IO.FileInfo(mNewSkCfgFile)
                    oFile.CopyTo(mOldSkCfgFile, True)
                End If
            End If

            Return True
        Catch ex As Exception
            Helpers.Logger.WriteRelative("ERROR!!!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Return False
        End Try
    End Function
End Class
