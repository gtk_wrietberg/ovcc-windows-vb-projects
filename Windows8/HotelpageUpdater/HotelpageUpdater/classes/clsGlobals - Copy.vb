﻿Public Class clsGlobals
    Public Shared oLogger As Logger
    Public Shared oComputerName As ComputerName

    Public Shared oSkCfg As SkCfg
    Public Shared oPw As PwProtection

    Public Shared oHotelBrand As clsHotelBrand

    Public Shared g_LogFileDirectory As String
    Public Shared g_LogFileName As String
    Public Shared g_BackupDirectory As String

    Public Shared g_TESTMODE As Boolean

    Public Shared g_OverwriteExistingConfig As Boolean
    Public Shared g_LeaveGlobalSettingsAlone As Boolean

    Public Shared g_Quiet As Boolean


    Public Shared g_Url_Hotel As String = "http://www.guesttek.com"
    Public Shared g_Url_Internet As String = "http://www.google.com"
    Public Shared g_Url_Weather As String = "http://www.weather.com"
    Public Shared g_Url_Map As String = "http://maps.google.com"
    Public Shared g_Address_Site As String = "Suite 600, 777 8 Ave SW, Calgary, Alberta Canada T2P 3R5"
    Public Shared g_Support_PhoneNumber As String = "00800 3400 1111"
    Public Shared g_Brand As Integer = -1000
    Public Shared g_PaymentEnabled As Boolean = False
    Public Shared g_PasswordEnabled As Boolean = False
    Public Shared g_Password As String = ""

    Public Shared g_Url_Hotel__Changed As Boolean = False
    Public Shared g_Url_Internet__Changed As Boolean = False
    Public Shared g_Url_Weather__Changed As Boolean = False
    Public Shared g_Url_Map__Changed As Boolean = False
    Public Shared g_Address_Site__Changed As Boolean = False
    Public Shared g_Support_PhoneNumber__Changed As Boolean = False
    Public Shared g_Brand__Changed As Boolean = False
    Public Shared g_PaymentEnabled__Changed As Boolean = False
    Public Shared g_PasswordEnabled__Changed As Boolean = False
    Public Shared g_Password__Changed As String = ""


    Public Shared g_AnyChangesFunctionDisabled As Boolean = False
    Public Shared g_AnyChangesDetected As Boolean = False


    Public Shared g_SKConfigFile As String = ""
    Public Shared g_UIConfigFile As String = ""
    Public Shared g_UIScriptFile As String = ""
    Public Shared g_PasswordFile As String = ""


    Public Shared ReadOnly c_FreeUrlZoneName As String = "No Charge (UI)"

    Public Shared ReadOnly c_Template_Guesttek As String = "Guest-tek Startpage"
    Public Shared ReadOnly c_Template_iBAHN As String = "iBAHN Startpage"
    Public Shared ReadOnly c_Template_ResidenceInn As String = "ResidenceInn Startpage"
    Public Shared ReadOnly c_Template_Hilton As String = "Hilton Startpage"

    Public Shared ReadOnly c_Path_Guesttek__UI As String = "%SiteKioskPath%/Skins/Public/Startpages/Guest-tek/index.html"
    Public Shared ReadOnly c_Path_iBAHN__UI As String = "%SiteKioskPath%/Skins/Public/Startpages/iBAHN/index.html"
    Public Shared ReadOnly c_Path_ResidenceInn__UI As String = "%SiteKioskPath%/Skins/Public/Startpages/ResidenceInn/index.html"
    Public Shared ReadOnly c_Path_Marriott__UI As String = "%SiteKioskPath%/Skins/Public/Startpages/Marriott/index.html"
    Public Shared ReadOnly c_Path_Towneplace__UI As String = "%SiteKioskPath%/Skins/Public/Startpages/Towneplace/index.html"
    Public Shared ReadOnly c_Path_HotelArts__UI As String = "%SiteKioskPath%/Skins/Public/Startpages/HotelArts/index.html"
    Public Shared ReadOnly c_Path_Hilton__UI As String = "%SiteKioskPath%/Skins/Public/StartPages/Hilton/index.html"

    Public Shared ReadOnly c_Path_Guesttek__screensaver As String = _GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\Guest-tek\screensaver\index.html"
    Public Shared ReadOnly c_Path_iBAHN__screensaver As String = _GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\iBAHN\screensaver\index.html"
    Public Shared ReadOnly c_Path_ResidenceInn__screensaver As String = _GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\ResidenceInn\screensaver\index.html"
    Public Shared ReadOnly c_Path_Marriott__screensaver As String = _GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\Marriott\screensaver\index.html"
    Public Shared ReadOnly c_Path_Towneplace__screensaver As String = _GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\Towneplace\screensaver\index.html"
    'Public Shared ReadOnly c_Path_Hilton__screensaver As String = "C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\Hilton\screensaver\main.html"
    'Public Shared ReadOnly c_Path_Hilton__screensaver As String = _GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\StartPages\Hilton\screensaver\01_PF_Hilton_Screensaver_02A_150204\swf\main_swf.html"
    Public Shared ReadOnly c_Path_HotelArts__screensaver As String = _GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\HotelArts\screensaver\index.html"
    Public Shared ReadOnly c_Path_Hilton__screensaver As String = _GetProgramFilesFolder() & "\SiteKiosk\Skins\Public\Startpages\Hilton\screensaver\index.html"


    Public Shared ReadOnly c_Path_SK__paymentdialog As String = _GetProgramFilesFolder() & "\SiteKiosk\SiteCash\Html\PaymentInfoDlg.htm"
    Public Shared ReadOnly c_Path_Hilton__paymentdialog As String = _GetProgramFilesFolder() & "\SiteKiosk\SiteCash\Html\PaymentInfoDlg.HILTON.htm"
    'Public Shared ReadOnly c_Path_Hilton__paymentdialog As String = _GetProgramFilesFolder() & "\SiteKiosk\SiteCash\Html\PaymentInfoDlg.HILTON.htm"
    Public Shared ReadOnly c_Path_ResidenceInn__paymentdialog As String = _GetProgramFilesFolder() & "\SiteKiosk\SiteCash\Html\PaymentInfoDlg.RESIDENCEINN.htm"
    Public Shared ReadOnly c_Path_Default__paymentdialog As String = _GetProgramFilesFolder() & "\SiteKiosk\SiteCash\Html\PaymentInfoDlg.DEFAULT.htm"

    Public Shared ReadOnly c_Path_LanguageFiles As String = _GetProgramFilesFolder() & "\SiteKiosk\Language\Browserskins\Default-Ieskin"


    Public Shared ReadOnly c_URL_Townplace_URL_Template As String = "http://www.towneplacemap.com/map/show/%%TOWNEPLACEID%%"


    Public Shared ReadOnly c_UI_ShadowSize As Integer = 3

    Private Shared Function _GetProgramFilesFolder() As String
        Dim sPath As String = ""

        sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sPath.Equals(String.Empty) Then
            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        Return sPath
    End Function

    Public Shared Function _DoesSiteKioskFileExist(FileName As String) As Boolean
        If FileName.StartsWith("%SiteKioskPath%") Then
            FileName = FileName.Replace("%SiteKioskPath%", Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", ""))
            FileName = FileName.Replace("//", "/")
        End If

        Return IO.File.Exists(FileName)
    End Function


    Public Shared Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = _GetProgramFilesFolder() & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = _GetProgramFilesFolder() & "\GuestTek\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)
        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)
        Catch ex As Exception

        End Try

        g_OverwriteExistingConfig = False
        g_LeaveGlobalSettingsAlone = False

        g_Quiet = False


        oSkCfg = New SkCfg
        oPw = New PwProtection
    End Sub

End Class
