﻿Public Class clsHotelBrand
    Private mBrandNumber As Integer
    Public Property BrandNumber() As Integer
        Get
            Return mBrandNumber
        End Get
        Set(ByVal value As Integer)
            mBrandNumber = value
        End Set
    End Property

    Private mBrandiBAHN As Boolean
    Public Property BrandiBAHN() As Boolean
        Get
            Return mBrandiBAHN
        End Get
        Set(ByVal value As Boolean)
            mBrandiBAHN = value
        End Set
    End Property

    Private mBrandGuestTek As Boolean
    Public Property BrandGuestTek() As Boolean
        Get
            Return mBrandGuestTek
        End Get
        Set(ByVal value As Boolean)
            mBrandGuestTek = value
        End Set
    End Property

    Private mBrandResidenceInn As Boolean
    Public Property BrandResidenceInn() As Boolean
        Get
            Return mBrandResidenceInn
        End Get
        Set(ByVal value As Boolean)
            mBrandResidenceInn = value
        End Set
    End Property

    Private mBrandMarriott As Boolean
    Public Property BrandMarriott() As Boolean
        Get
            Return mBrandMarriott
        End Get
        Set(ByVal value As Boolean)
            mBrandMarriott = value
        End Set
    End Property

    Private mBrandTowneplace As Boolean
    Public Property BrandTowneplace() As Boolean
        Get
            Return mBrandTowneplace
        End Get
        Set(ByVal value As Boolean)
            mBrandTowneplace = value
        End Set
    End Property

    Private mBrandTowneplace_String As String
    Public Property BrandTowneplace_String() As String
        Get
            Return mBrandTowneplace_String
        End Get
        Set(ByVal value As String)
            mBrandTowneplace_String = value
        End Set
    End Property

    Private mBrandHotelArts As Boolean
    Public Property BrandHotelArts() As Boolean
        Get
            Return mBrandHotelArts
        End Get
        Set(ByVal value As Boolean)
            mBrandHotelArts = value
        End Set
    End Property

    Public ReadOnly Property BrandHilton() As Boolean
        Get
            Return (Not (mBrandiBAHN Or mBrandGuestTek Or mBrandMarriott Or mBrandResidenceInn Or mBrandTowneplace Or mBrandHotelArts)) Or (mBrandNumber < 666)
        End Get
    End Property


    Public Sub Reset()
        mBrandNumber = -1
        mBrandGuestTek = False
        mBrandiBAHN = False
        mBrandResidenceInn = False
        mBrandMarriott = False
        mBrandTowneplace = False
        mBrandHotelArts = False
    End Sub

    Public Sub New()
        Me.Reset()
    End Sub
End Class
