﻿Public Class frmMain
    Private Function FindTopLocation(ByVal item As Control) As Integer
        Dim i_top As Integer = 0

        i_top = item.Top

        If Not TypeOf item Is Form Then
            i_top += FindTopLocation(item.Parent)
        End If

        Return i_top
    End Function

    Private Function FindLeftLocation(ByVal item As Control) As Integer
        Dim i_left As Integer = 0

        i_left = item.Left

        If Not TypeOf item Is Form Then
            i_left += FindLeftLocation(item.Parent)
        End If

        Return i_left
    End Function

#Region "Thread Safe stuff"
    Delegate Sub UpdateSaveProgress__Callback(ByVal [text] As String)

    Public Sub UpdateSaveProgress(ByVal [text] As String)
        If Me.lblSaveProgress.InvokeRequired Then
            Dim d As New UpdateSaveProgress__Callback(AddressOf UpdateSaveProgress)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblSaveProgress.Text = [text]
        End If
    End Sub
#End Region


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        clsGlobals.InitGlobals()


        Helpers.Logger.Debugging = False
        Helpers.Logger.InitialiseLogger(clsGlobals.g_LogFileDirectory, False)
        Helpers.Logger.WriteMessage(New String("*", 50), 0)
        Helpers.Logger.WriteMessage(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString, 0)


        'check for debugging
        For Each arg As String In Environment.GetCommandLineArgs()
            If arg.Equals(clsGlobals.Params.debug) Then
                clsGlobals.g_Debug = True
                Helpers.Logger.Debugging = True
            End If
        Next


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        txtVersion.Text = "version: " & myBuildInfo.FileVersion & vbCrLf & "build date: " & Helpers.FilesAndFolders.GetFileCompileDate(Application.ExecutablePath)


        HotelBrands.Reset()

        clsGlobalSettings.LoadThemes()
        clsGlobalSettings.GetConfigFilesForFrontend()
        clsGlobalSettings.ReadGlobalSettings()
        clsGlobalSettings.ReadPhoneNumberFromEnglishLanguageFile()
        clsGlobalSettings.ReadPassword()


        txtSKConfigFile.Text = clsGlobals.g_SKConfigFile
        txtUIConfigFile.Text = clsGlobals.g_UIConfigFile
        txtUIScriptFile.Text = clsGlobals.g_UIScriptFile
        txtSKPasswordFile.Text = clsGlobals.g_PasswordFile


        Helpers.Logger.Write("loading command line arguments", , 1)
        For Each arg As String In Environment.GetCommandLineArgs()
            Helpers.Logger.Write("found: " & arg, , 2)

            If arg.StartsWith(clsGlobals.Params.check_default_values_when_switching_themes) Then
                Dim _s As String = arg.Replace(clsGlobals.Params.check_default_values_when_switching_themes, "")
                Dim _b As Boolean = False

                If Boolean.TryParse(_s, _b) Then
                    My.Settings.Application_CheckIfPasswordOrPaymentMatchTheDefaultValue = _b
                End If

                Helpers.Logger.Write("Application_CheckIfPasswordOrPaymentMatchTheDefaultValue: " & My.Settings.Application_CheckIfPasswordOrPaymentMatchTheDefaultValue.ToString, , 3)
            End If

            If arg.StartsWith(clsGlobals.Params.quiet__hotel_info_url) Then
                clsGlobals.g_Url_Hotel = arg.Replace(clsGlobals.Params.quiet__hotel_info_url, "")
                Helpers.Logger.Write("quiet - set hotel info url: " & clsGlobals.g_Url_Hotel, , 3)
                clsGlobals.g_Quiet = True
            End If

            If arg.StartsWith(clsGlobals.Params.quiet__brand) Then
                Dim _s As String = arg.Replace(clsGlobals.Params.quiet__brand, "")

                If IsNumeric(_s) Then
                    If HotelBrands.SetCurrent(CInt(_s)) Then
                        'ok
                        Helpers.Logger.Write("quiet - set brand to index " & _s, , 3)
                        clsGlobals.g_Quiet = True
                    Else
                        Helpers.Logger.WriteRelative("no theme info found for brand index '" & _s & "'", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                    End If
                Else
                    Helpers.Logger.WriteRelative("brand index '" & _s & "' is not even a number", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            End If

            If arg.Equals(clsGlobals.Params.quiet__brand_residenceinn) Then
                HotelBrands.SetCurrent(668)

                Helpers.Logger.Write("quiet - set brand to ResidenceInn", , 3)
                clsGlobals.g_Quiet = True
            End If

            If arg.StartsWith(clsGlobals.Params.quiet__brand_towneplace) Then
                Dim _s As String = arg.Replace(clsGlobals.Params.quiet__brand_towneplace, "")

                HotelBrands.SetCurrent(670)
                HotelBrands.SetCurrentExtraString(_s)

                If Not _s.Equals("") Then
                    'Update url
                    clsGlobals.g_Url_Internet = clsGlobals.c_URL_Townplace_URL_Template.Replace("%%TOWNEPLACEID%%", _s)
                    clsGlobals.g_Url_Internet__Changed = True
                End If

                Helpers.Logger.Write("quiet - set brand to Towneplace (" & _s & ")", , 3)
                clsGlobals.g_Quiet = True
            End If
        Next


        'test
        btnTest.Visible = Helpers.Generic.IsDevMachine()


        'Ui stuff
        Me.ClientSize = New Size(pnlAll.Width + 2 * pnlAll.Left, pnlAll.Height + 2 * pnlAll.Top)


        If clsGlobals.g_Quiet Then
            'we're not going to show the UI, so no use for all that fancy stuff

            If Not Helpers.Generic.IsRunningAsAdmin Then
                Helpers.Logger.Write("NOT AN ADMIN!!!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                Me.Close()
            End If

            ToggleControls(False)

            SaveAllTheStuff()
        Else
            'themes
            tree_Brands.Nodes.Clear()

            For Each _brand As HotelBrands.HotelBrand In HotelBrands.GetBrands
                Dim _node As TreeNode = tree_Brands.Nodes.Add(_brand.Name)

                If _brand.ArePathsValid Then
                    _node.NodeFont = tree_Brands.Font
                    _node.ForeColor = tree_Brands.ForeColor
                Else
                    _node.NodeFont = New Font(tree_Brands.Font, FontStyle.Strikeout)
                    '_node.ForeColor = Color.Gray
                    '_node.ForeColor = Color.FromArgb(128, 64, 64)
                    _node.ForeColor = Color.Red

                    Dim _tooltipString As String = "'" & _brand.Name & "' is broken/missing:"

                    _tooltipString &= vbCrLf

                    If Not _brand.Path_Theme_Exists Then
                        _tooltipString &= vbCrLf
                        _tooltipString &= "theme path not found"
                    End If
                    If Not _brand.Path_Logout_Exists Then
                        _tooltipString &= vbCrLf
                        _tooltipString &= "logout file not found"
                    End If
                    If Not _brand.Path_Screensaver_Exists Then
                        _tooltipString &= vbCrLf
                        _tooltipString &= "screensaver path not found"
                    End If
                    If Not _brand.Path_PaymentDialog_Exists Then
                        _tooltipString &= vbCrLf
                        _tooltipString &= "payment dialog file not found"
                    End If

                    _node.ToolTipText = _tooltipString
                End If
            Next


            'shadow
            pnlShadowDialog.Width = pnlWait.Width
            pnlShadowDialog.Height = pnlWait.Height
            pnlShadowDialog.Top = pnlWait.Top + clsGlobals.c_UI_ShadowSize
            pnlShadowDialog.Left = pnlWait.Left + clsGlobals.c_UI_ShadowSize
            pnlShadowDialog.SendToBack()


            'txt fields
            txtHotelAddress.Text = clsGlobals.g_Address_Site
            txtHotelinformation.Text = clsGlobals.g_Url_Hotel
            txtInternet.Text = clsGlobals.g_Url_Internet
            txtMap.Text = clsGlobals.g_Url_Map
            txtWeather.Text = clsGlobals.g_Url_Weather
            txtSupportNumber.Text = clsGlobals.g_Support_PhoneNumber


            'Theme ui
            clsGlobals.oSkCfg.LoadPaymentEnabled()
            chk_Password.Checked = clsGlobals.g_PasswordEnabled
            If clsGlobals.g_PasswordEnabled Then
                clsGlobals.g_PaymentEnabled = False
            End If
            chk_SiteCash.Checked = clsGlobals.g_PaymentEnabled


            For Each _node As TreeNode In tree_Brands.Nodes
                Try
                    If _node.Text.Equals(HotelBrands.GetCurrent.Name) Then
                        _node.Checked = True
                        _node.EnsureVisible()

                        CheckIfPasswordOrPaymentMatchTheDefaultValue(_node.Index)
                    Else
                        _node.Checked = False
                    End If
                Catch ex As Exception

                End Try
            Next



            txtPassword.Enabled = chk_Password.Checked
            txtPassword.Text = clsGlobals.g_Password


            If Not Helpers.Generic.IsRunningAsAdmin Then
                ToggleControls(False)

                Dim pTmp As New Point
                pTmp.X = pnlButtons.Left + pnlMain.Left + 1
                pTmp.Y = pnlButtons.Top + pnlMain.Top + 1

                pnlDisabled.Location = pTmp
                pnlDisabled.BringToFront()
            End If

            'Add mouse click and key press event to all controls
            AddEventsToFormChildControls()
        End If
    End Sub

    Private Sub ToggleControls(_enabled As Boolean)
        btnSave.Enabled = _enabled
        btnCancel.Enabled = _enabled

        txtHotelAddress.Enabled = _enabled
        txtHotelinformation.Enabled = _enabled
        txtInternet.Enabled = _enabled
        txtMap.Enabled = _enabled
        txtWeather.Enabled = _enabled
        txtSupportNumber.Enabled = _enabled

        btnHotelInformation.Enabled = _enabled
        btnInternet.Enabled = _enabled
        btnMap.Enabled = _enabled
        btnWeather.Enabled = _enabled


        chk_SiteCash.Enabled = _enabled

        chk_Password.Enabled = _enabled
        txtPassword.Enabled = _enabled


        btnTest.Enabled = _enabled


        tree_Brands.Enabled = _enabled
    End Sub

    Private Sub ToggleWaitPopup(_enabled As Boolean)
        Dim pTmp As New Point, pTmpShadow As New Point

        If _enabled Then
            progressFake.Style = ProgressBarStyle.Marquee

            pnlWait.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlWait.Width) / 2
            pTmp.Y = (Me.Height - pnlWait.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlWait.Size
            pnlShadowDialog.BringToFront()

            pnlWait.Location = pTmp
            pnlWait.BringToFront()
        Else
            progressFake.Style = ProgressBarStyle.Blocks

            pnlWait.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlWait.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub

    Private Sub ToggleErrorPopup(sErr As String)
        Dim pTmp As New Point, pTmpShadow As New Point

        If Not sErr.Equals("") Then
            ToggleControls(False)

            btnCloseError.Enabled = True

            lblErrorText.Text = sErr

            pnlError.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlError.Width) / 2
            pTmp.Y = (Me.Height - pnlError.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlError.Size
            pnlShadowDialog.BringToFront()

            pnlError.Location = pTmp
            pnlError.BringToFront()
        Else
            ToggleControls(True)

            btnCloseError.Enabled = False

            pnlError.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlError.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub

    Private Sub ToggleChangeConfirmPopup(_enabled As Boolean)
        Dim pTmp As New Point, pTmpShadow As New Point

        If _enabled Then
            ToggleControls(False)

            btnYesCancel.Enabled = True
            btnNoReturnToApp.Enabled = True

            pnlChangeConfirm.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlChangeConfirm.Width) / 2
            pTmp.Y = (Me.Height - pnlChangeConfirm.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlChangeConfirm.Size
            pnlShadowDialog.BringToFront()

            pnlChangeConfirm.Location = pTmp
            pnlChangeConfirm.BringToFront()
        Else
            ToggleControls(True)

            btnYesCancel.Enabled = False
            btnNoReturnToApp.Enabled = False


            pnlChangeConfirm.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlChangeConfirm.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub

    Private Sub ToggleNoChangesConfirmPopup(_enabled As Boolean)
        Dim pTmp As New Point, pTmpShadow As New Point

        If _enabled Then
            ToggleControls(False)

            btnYesSaveAnyway.Enabled = True
            btnNoReturnToApp2.Enabled = True

            pnlNoChangesConfirm.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlNoChangesConfirm.Width) / 2
            pTmp.Y = (Me.Height - pnlNoChangesConfirm.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlNoChangesConfirm.Size
            pnlShadowDialog.BringToFront()

            pnlNoChangesConfirm.Location = pTmp
            pnlNoChangesConfirm.BringToFront()
        Else
            ToggleControls(True)

            btnYesSaveAnyway.Enabled = False
            btnNoReturnToApp2.Enabled = False


            pnlNoChangesConfirm.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlNoChangesConfirm.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub

    Private Sub ToggleCurrentOrDefaultPopup(_enabled As Boolean)
        Dim pTmp As New Point, pTmpShadow As New Point

        If _enabled Then
            ToggleControls(False)

            btnUseCurrent.Enabled = True
            btnUseDefault.Enabled = True

            pnlCurrentOrDefault.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlCurrentOrDefault.Width) / 2
            pTmp.Y = (Me.Height - pnlCurrentOrDefault.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlCurrentOrDefault.Size
            pnlShadowDialog.BringToFront()

            pnlCurrentOrDefault.Location = pTmp
            pnlCurrentOrDefault.BringToFront()
        Else
            ToggleControls(True)

            btnUseCurrent.Enabled = False
            btnUseDefault.Enabled = False

            pnlCurrentOrDefault.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlCurrentOrDefault.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub

    Private Sub AddEventsToFormChildControls()
        Dim c As Control = Me

        Do Until c Is Nothing
            AddHandler c.MouseClick, AddressOf GenericEventHandler
            AddHandler c.KeyUp, AddressOf GenericEventHandler

            c = GetNextControl(c, True)
        Loop
    End Sub

    Private Sub GenericEventHandler(ByVal sender As Object, ByVal e As EventArgs)
        UpdateSaveButton()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If AnyChanges() Then
            ToggleChangeConfirmPopup(True)
        Else
            btnYesCancel.PerformClick()
        End If
    End Sub

    Private Sub btnYesCancel_Click(sender As Object, e As EventArgs) Handles btnYesCancel.Click
        Me.Close()
    End Sub

    Private Sub btnYesSaveAnyway_Click(sender As Object, e As EventArgs) Handles btnYesSaveAnyway.Click
        PrepareToSaveAllTheStuff()

        SaveAllTheStuff()
    End Sub

    Private Sub btnNoReturnToApp_Click(sender As Object, e As EventArgs) Handles btnNoReturnToApp.Click
        ToggleChangeConfirmPopup(False)
    End Sub

    Private Sub btnNoReturnToApp2_Click(sender As Object, e As EventArgs) Handles btnNoReturnToApp2.Click
        ToggleNoChangesConfirmPopup(False)
    End Sub



    Private Function AnyChanges() As Boolean
        If clsGlobals.g_AnyChangesFunctionDisabled Then
            Return False
        End If

        clsGlobals.g_Address_Site__Changed = (clsGlobals.g_Address_Site <> txtHotelAddress.Text)
        clsGlobals.g_Url_Hotel__Changed = (clsGlobals.g_Url_Hotel <> txtHotelinformation.Text)
        clsGlobals.g_Url_Internet__Changed = (clsGlobals.g_Url_Internet <> txtInternet.Text)
        clsGlobals.g_Url_Map__Changed = (clsGlobals.g_Url_Map <> txtMap.Text)
        clsGlobals.g_Url_Weather__Changed = (clsGlobals.g_Url_Weather <> txtWeather.Text)
        clsGlobals.g_Support_PhoneNumber__Changed = (clsGlobals.g_Support_PhoneNumber <> txtSupportNumber.Text)

        Try
            clsGlobals.g_Brand__Changed = (HotelBrands.GetCurrent.Name <> tree_Brands.SelectedNode.Text)
        Catch ex As Exception

        End Try

        clsGlobals.g_PaymentEnabled__Changed = (clsGlobals.g_PaymentEnabled <> chk_SiteCash.Checked)
        clsGlobals.g_PasswordEnabled__Changed = (clsGlobals.g_PasswordEnabled <> chk_Password.Checked)
        clsGlobals.g_Password__Changed = (clsGlobals.g_Password <> txtPassword.Text)

        'Dim sTmp As String = ""

        'sTmp &= clsGlobals.g_Address_Site__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Url_Hotel__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Url_Internet__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Url_Map__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Url_Weather__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Support_PhoneNumber__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Brand__Changed.ToString & " "
        'sTmp &= clsGlobals.g_PaymentEnabled__Changed.ToString & " "

        'Helpers.Logger.Write("You changed:", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        'Helpers.Logger.Write(sTmp, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)


        'sTmp = vbCrLf
        'sTmp &= "'" & clsGlobals.g_Address_Site.ToString & "' <> '" & txtHotelAddress.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Url_Hotel.ToString & "' <> '" & txtHotelinformation.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Url_Internet.ToString & "' <> '" & txtInternet.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Url_Map.ToString & "' <> '" & txtMap.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Url_Weather.ToString & "' <> '" & txtWeather.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Support_PhoneNumber.ToString & "' <> '" & txtSupportNumber.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Brand.ToString & "' <> '" & clsGlobals.oHotelBrand.BrandNumber.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_PaymentEnabled.ToString & "' <> '" & chk_SiteCash.Checked.ToString & "'" & vbCrLf

        'Helpers.Logger.Write("You changed:", Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        'Helpers.Logger.Write(sTmp, Helpers.Logger.MESSAGE_TYPE.LOG_DEBUG, 2)



        Return clsGlobals.g_Address_Site__Changed Or
            clsGlobals.g_Url_Hotel__Changed Or
            clsGlobals.g_Url_Internet__Changed Or
            clsGlobals.g_Url_Map__Changed Or
            clsGlobals.g_Url_Weather__Changed Or
            clsGlobals.g_Support_PhoneNumber__Changed Or
            clsGlobals.g_Brand__Changed Or
            clsGlobals.g_PaymentEnabled__Changed Or
            clsGlobals.g_PasswordEnabled__Changed Or
            clsGlobals.g_Password__Changed
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Not AnyChanges() Then
            ToggleNoChangesConfirmPopup(True)
        Else
            PrepareToSaveAllTheStuff()

            SaveAllTheStuff()
        End If
    End Sub

    Private Sub PrepareToSaveAllTheStuff()
        clsGlobals.g_AnyChangesFunctionDisabled = True

        clsGlobals.g_Address_Site = txtHotelAddress.Text
        clsGlobals.g_Url_Hotel = txtHotelinformation.Text
        clsGlobals.g_Url_Internet = txtInternet.Text
        clsGlobals.g_Url_Map = txtMap.Text
        clsGlobals.g_Url_Weather = txtWeather.Text
        clsGlobals.g_Support_PhoneNumber = txtSupportNumber.Text
        clsGlobals.g_PaymentEnabled = chk_SiteCash.Checked
        clsGlobals.g_PasswordEnabled = chk_Password.Checked
        clsGlobals.g_Password = txtPassword.Text

        HotelBrands.SetCurrent(tree_Brands.SelectedNode.Text)
    End Sub

    Private Sub SaveAllTheStuff()
        UpdateSaveProgress("")

        ToggleControls(False)
        ToggleWaitPopup(True)

        bgwSave.RunWorkerAsync()
    End Sub

    Private Sub DoneSavingAllTheStuff()
        DoneBye()
    End Sub

    Private Sub bgwSave_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwSave.DoWork
        Helpers.Logger.Write("Saving config", , 0)


        '------------------------------------------------------
        UpdateSaveProgress("ui config")

        Helpers.Logger.Write("Update global config", , 1)
        clsGlobalSettings.WriteGlobalSettings()

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("password config")

        Helpers.Logger.Write("Update password config", , 1)
        clsGlobalSettings.WritePassword()

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("support phone number")
        Helpers.Logger.Write("Update support phone number", , 1)
        If clsGlobals.g_Support_PhoneNumber__Changed Then
            clsGlobalSettings.WritePhoneNumberToLanguageFiles()
        Else
            Helpers.Logger.Write("no change, skipped", , 2)
        End If

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("sitekiosk config")
        Helpers.Logger.Write("Update SiteKiosk config", , 1)
        'clsGlobals.oSkCfg = New SkCfg
        clsGlobals.oSkCfg.BackupFolder = clsGlobals.g_BackupDirectory

        Helpers.Logger.Write("Prepare url list", , 2)
        If clsGlobals.oSkCfg.MakeListOfUrls Then
            Helpers.Logger.Write("ok", , 3)
        Else
            Helpers.Logger.Write("OOPS!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        Helpers.Logger.Write("Update skcfg file", , 2)
        clsGlobals.oSkCfg.Update(True)

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("payment dialog")
        'Update payment dialog
        Try
            Helpers.Logger.Write("Update payment dialog", , 1)


            Helpers.Logger.Write("copying", , 2)
            If clsGlobals.g_PasswordEnabled Then
                'Password, use the Hilton one (brand index 0)
                Helpers.Logger.Write("using the Hilton payment dialog, for the password thing", , 3)

                If HotelBrands.HiltonDefault.Path_PaymentDialog_Exists Then
                    IO.File.Copy(HotelBrands.HiltonDefault.Path_PaymentDialog, clsGlobals.c_Path_SK__paymentdialog, True)
                    Helpers.Logger.Write(HotelBrands.HiltonDefault.Path_PaymentDialog & " => " & clsGlobals.c_Path_SK__paymentdialog, , 4)
                Else
                    Helpers.Logger.Write("missing file!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                    Helpers.Logger.Write(HotelBrands.HiltonDefault.Path_PaymentDialog, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                End If
            Else
                If HotelBrands.GetCurrent.Path_PaymentDialog_Exists Then
                    IO.File.Copy(HotelBrands.GetCurrent.Path_PaymentDialog, clsGlobals.c_Path_SK__paymentdialog, True)
                    Helpers.Logger.Write(HotelBrands.GetCurrent.Path_PaymentDialog & " => " & clsGlobals.c_Path_SK__paymentdialog, , 3)
                Else
                    Helpers.Logger.Write("missing file!", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    Helpers.Logger.Write(HotelBrands.GetCurrent.Path_PaymentDialog, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            End If
        Catch ex As Exception
            Helpers.Logger.Write("ERROR", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            Helpers.Logger.Write(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("done")

        Threading.Thread.Sleep(250)
    End Sub

    Private Sub bgwSave_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwSave.ProgressChanged
        'Yeah! Progress!
    End Sub

    Private Sub bgwSave_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwSave.RunWorkerCompleted
        DoneSavingAllTheStuff()
    End Sub

    Private Sub DoneBye()
        'done
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("bye", , 1)

        Me.Close()
    End Sub

    Private Sub btnDisabledClose_Click(sender As Object, e As EventArgs) Handles btnDisabledClose.Click
        Me.Close()
    End Sub

    Private Sub btnInternet_Click(sender As Object, e As EventArgs) Handles btnInternet.Click
        TestUrl(txtInternet.Text)
    End Sub

    Private Sub btnHotelInformation_Click(sender As Object, e As EventArgs) Handles btnHotelInformation.Click
        TestUrl(txtHotelinformation.Text)
    End Sub

    Private Sub btnWeather_Click(sender As Object, e As EventArgs) Handles btnWeather.Click
        TestUrl(txtWeather.Text)
    End Sub

    Private Sub btnMap_Click(sender As Object, e As EventArgs) Handles btnMap.Click
        TestUrl(txtMap.Text)
    End Sub

    Private Sub TestUrl(sUrl As String)
        Try
            If (sUrl IsNot Nothing) AndAlso (sUrl.StartsWith("http://") Or sUrl.StartsWith("https://")) Then
                System.Diagnostics.Process.Start(sUrl)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub UpdateSaveButton()
        Dim tmpFont As Font

        clsGlobals.g_AnyChangesDetected = AnyChanges()

        If clsGlobals.g_AnyChangesDetected Then
            tmpFont = New Font(btnSave.Font.Name, 16, FontStyle.Bold)

            btnSave.ForeColor = Color.DarkRed
        Else
            tmpFont = New Font(btnSave.Font.Name, 12, FontStyle.Regular)
            btnSave.ForeColor = Color.Black
        End If

        btnSave.Font = tmpFont

        tmpFont.Dispose()
    End Sub

    Private Sub txtSupportNumber_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSupportNumber.KeyPress
        '    If Not (Asc(e.KeyChar) = 8) Then
        '        Dim allowedChars As String = "0123456789 :-()+"

        '        If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
        '            e.KeyChar = ChrW(0)
        '            e.Handled = True
        '        End If
        '    End If
    End Sub

    Private Sub btnCloseError_Click(sender As Object, e As EventArgs) Handles btnCloseError.Click
        ToggleErrorPopup("")
    End Sub

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        Dim s As String = ""

        s = "This is a random test message: " & Helpers.Types.RandomString(20)

        ToggleErrorPopup(s)
    End Sub

    Private Sub txtPassword_MouseHover(sender As Object, e As EventArgs) Handles txtPassword.MouseHover
        txtPassword.PasswordChar = ""
    End Sub

    Private Sub txtPassword_MouseLeave(sender As Object, e As EventArgs) Handles txtPassword.MouseLeave
        txtPassword.PasswordChar = "*"
    End Sub

    Private Sub chk_SiteCash_Click(sender As Object, e As EventArgs) Handles chk_SiteCash.Click
        If chk_SiteCash.Checked Then
            chk_Password.Checked = False
            txtPassword.Enabled = False
        End If
    End Sub

    Private Sub chk_Password_Click(sender As Object, e As EventArgs) Handles chk_Password.Click
        If chk_Password.Checked Then
            chk_SiteCash.Checked = False
        End If

        txtPassword.Enabled = chk_Password.Checked
    End Sub

    Private Function CheckIfPasswordOrPaymentMatchTheDefaultValue() As Boolean
        Return CheckIfPasswordOrPaymentMatchTheDefaultValue(HotelBrands.GetCurrent.Index)
    End Function

    Private mTemporaryIndex As Integer = -1
    Private Function CheckIfPasswordOrPaymentMatchTheDefaultValue(_index As Integer) As Boolean
        If Not My.Settings.Application_CheckIfPasswordOrPaymentMatchTheDefaultValue Then
            Return True
        Else
            Dim _brand As New HotelBrands.HotelBrand

            _brand = HotelBrands.FindByIndex(_index)

            mTemporaryIndex = -1

            If _brand IsNot Nothing Then
                If chk_Password.Checked <> _brand.PasswordEnabled Or chk_SiteCash.Checked <> _brand.PaymentEnabled Then
                    mTemporaryIndex = _index
                    ToggleCurrentOrDefaultPopup(True)
                End If

                Return True
            End If

            Return False
        End If
    End Function

    Private Sub btnUseCurrent_Click(sender As Object, e As EventArgs) Handles btnUseCurrent.Click
        'Do Nothing
        ToggleCurrentOrDefaultPopup(False)
    End Sub

    Private Sub btnUseDefault_Click(sender As Object, e As EventArgs) Handles btnUseDefault.Click
        If mTemporaryIndex > -1 Then
            Dim _brand As New HotelBrands.HotelBrand

            _brand = HotelBrands.FindByIndex(mTemporaryIndex)

            If _brand IsNot Nothing Then
                chk_SiteCash.Checked = _brand.PaymentEnabled
                chk_Password.Checked = _brand.PasswordEnabled
            End If
        End If

        ToggleCurrentOrDefaultPopup(False)
    End Sub

#Region "txtVersion"
    Private mActiveControl As Control
    Private Sub txtVersion_MouseHover(sender As Object, e As EventArgs) Handles txtVersion.MouseHover
        mActiveControl = Me.ActiveControl

        txtVersion.ForeColor = Color.FromArgb(35, 55, 70)
        txtVersion.SelectAll()
        txtVersion.Focus()
    End Sub

    Private Sub txtVersion_MouseLeave(sender As Object, e As EventArgs) Handles txtVersion.MouseLeave
        txtVersion.ForeColor = Color.FromArgb(210, 210, 210)
        txtVersion.Select(0, 0)

        If mActiveControl Is Nothing Then
            lblTitle.Focus()
        Else
            mActiveControl.Focus()
        End If
    End Sub
#End Region

#Region "tree_Brands"
    Private _event_skip__Check As Boolean = False
    Private _event_skip__Select As Boolean = False

    Private Sub tree_Brands_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tree_Brands.AfterSelect
        If _event_skip__Select Then
            Exit Sub
        End If

        tree_Brands.SelectedNode.Checked = True
        tree_Brands.SelectedNode.EnsureVisible()
    End Sub

    Private Sub tree_Brands_AfterCheck(sender As Object, e As TreeViewEventArgs) Handles tree_Brands.AfterCheck
        If _event_skip__Check Then
            Exit Sub
        End If

        _event_skip__Check = True

        If e.Node.Checked Then
            For Each _node As TreeNode In e.Node.TreeView.Nodes
                If _node.NodeFont.Strikeout Then
                    Continue For
                End If

                If _node.Equals(e.Node) Then
                    _event_skip__Select = True
                    tree_Brands.SelectedNode = _node
                    _event_skip__Select = False

                    _node.NodeFont = New Font(tree_Brands.Font, FontStyle.Bold)
                    _node.ForeColor = Color.Black
                Else
                    _node.Checked = False

                    _node.NodeFont = New Font(tree_Brands.Font, FontStyle.Regular)
                    _node.ForeColor = Color.FromArgb(32, 32, 32)
                End If
            Next
        End If

        _event_skip__Check = False
    End Sub

    Private Sub tree_Brands_BeforeSelect(sender As Object, e As TreeViewCancelEventArgs) Handles tree_Brands.BeforeSelect
        Try
            If Not e.Node.NodeFont Is Nothing Then
                If e.Node.NodeFont.Strikeout Then
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tree_Brands_BeforeCheck(sender As Object, e As TreeViewCancelEventArgs) Handles tree_Brands.BeforeCheck
        Try
            If Not e.Node.NodeFont Is Nothing Then
                If e.Node.NodeFont.Strikeout Then
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function tree_Brands_SelectNode(_index As Integer) As Boolean
        Dim _ret As Boolean = True

        For Each _node As TreeNode In tree_Brands.Nodes
            If _node.Index = _index Then
                If Not _node.NodeFont Is Nothing Then
                    If _node.NodeFont.Strikeout Then
                        _ret = False
                    End If
                End If

                tree_Brands.SelectedNode = _node

                Exit For
            End If
        Next

        Return _ret
    End Function

    Private Function tree_Brands_SelectNode(_text As String) As Boolean
        Dim _ret As Boolean = True

        For Each _node As TreeNode In tree_Brands.Nodes
            If _node.Name = _text Then
                If Not _node.NodeFont Is Nothing Then
                    If _node.NodeFont.Strikeout Then
                        _ret = False
                    End If
                End If

                tree_Brands.SelectedNode = _node

                Exit For
            End If
        Next

        Return _ret
    End Function

    Private Function tree_Brands_FindFirstAllowedNode() As Integer
        Dim _ret As Integer = -1

        For Each _node As TreeNode In tree_Brands.Nodes
            If Not _node.NodeFont Is Nothing Then
                If Not _node.NodeFont.Strikeout Then
                    _ret = _node.Index

                    Exit For
                End If
            Else
                _ret = _node.Index

                Exit For
            End If
        Next

        Return _ret
    End Function



#End Region
End Class
