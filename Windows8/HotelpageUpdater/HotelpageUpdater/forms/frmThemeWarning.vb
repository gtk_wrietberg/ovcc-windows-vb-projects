﻿Imports System.Text


Module RTF_Extensions
    <Runtime.CompilerServices.Extension()>
    Public Function String2RTF(s As String) As String
        Return "{\rtf1\ansi " + s + " }"
    End Function

    <Runtime.CompilerServices.Extension()>
    Public Function RTF_ToBold(s As String) As String
        Return String.Format("\b {0}\b0 ", s)
    End Function

    <Runtime.CompilerServices.Extension()>
    Public Function RTF_EscapeBackslash(s As String) As String
        Return s.Replace("\", "\\")
    End Function
End Module


Public Class frmThemeWarning
    Private _vbTab As String = New String(" ", 4)

    Private Sub frmThemeWarning_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.TransparencyKey = Me.BackColor


        Dim _sb As New StringBuilder, _hilton_flag As Boolean = False
        Dim _cnt As Integer = HotelBrands.CountInvalidThemes

        If _cnt = 1 Then
            _sb.Append("The following theme is unavailable on this machine:")
        Else
            _sb.Append("The following themes are unavailable on this machine:")
        End If
        _sb.Append("\line")
        _sb.Append("\line")

        For Each _brand As HotelBrands.HotelBrand In HotelBrands.GetBrands
            If Not _hilton_flag Or _brand.BrandIndex >= HotelBrands.HiltonBrandIndexLimit Then
                If Not _brand.ArePathsValid Then
                    _sb.Append("\line")
                    _sb.Append(_brand.Name.RTF_ToBold)
                    _sb.Append("\line")

                    If Not _brand.Path_Theme_Exists Then
                        _sb.Append("\tab " & "theme path not found")
                        _sb.Append("\line")
                        _sb.Append("\tab " & "\tab " & _brand.Path_Theme.RTF_EscapeBackslash)
                        _sb.Append("\line")
                    End If
                    If Not _brand.Path_Screensaver_Exists Then
                        _sb.Append("\tab " & "screensaver path not found")
                        _sb.Append("\line")
                        _sb.Append("\tab " & "\tab " & _brand.Path_Screensaver.RTF_EscapeBackslash)
                        _sb.Append("\line")
                    End If
                    If Not _brand.Path_PaymentDialog_Exists Then
                        _sb.Append("\tab " & "payment dialog path not found")
                        _sb.Append("\line")
                        _sb.Append("\tab " & "\tab " & _brand.Path_PaymentDialog.RTF_EscapeBackslash)
                        _sb.Append("\line")
                    End If
                    If Not _brand.Path_Logout_Exists Then
                        _sb.Append("\tab " & "logout file path not found")
                        _sb.Append("\line")
                        _sb.Append("\tab " & "\tab " & _brand.Path_Logout.RTF_EscapeBackslash)
                        _sb.Append("\line")
                    End If
                End If

                If _brand.BrandIndex < HotelBrands.HiltonBrandIndexLimit Then
                    _hilton_flag = True
                End If
            End If
        Next


        txtrichMessage.Clear()
        txtrichMessage.Rtf = _sb.ToString.String2RTF
    End Sub

    Private Sub frmThemeWarning_Move(sender As Object, e As EventArgs) Handles Me.Move

    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub pnlMain_Paint(sender As Object, e As PaintEventArgs) Handles pnlMain.Paint
        Dim borderWidth As Integer = 2
        Dim borderColor As Color = Color.Red

        ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, borderColor, borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid, borderColor, borderWidth, ButtonBorderStyle.Solid)
    End Sub
End Class