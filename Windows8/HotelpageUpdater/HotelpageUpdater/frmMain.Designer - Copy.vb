﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnHotelInformation = New System.Windows.Forms.Button()
        Me.txtHotelinformation = New System.Windows.Forms.TextBox()
        Me.cmbBrand = New System.Windows.Forms.ComboBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.pnlTop = New System.Windows.Forms.Panel()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pnlMain = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtSupportNumber = New System.Windows.Forms.TextBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.chk_SiteCash = New System.Windows.Forms.CheckBox()
        Me.chk_Password = New System.Windows.Forms.CheckBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.pnlButtons = New System.Windows.Forms.Panel()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.chk_HotelArts = New System.Windows.Forms.CheckBox()
        Me.chk_Towneplace = New System.Windows.Forms.CheckBox()
        Me.chk_ResidenceInn = New System.Windows.Forms.CheckBox()
        Me.chk_Hilton = New System.Windows.Forms.CheckBox()
        Me.chk_Marriott = New System.Windows.Forms.CheckBox()
        Me.chk_GuestTek = New System.Windows.Forms.CheckBox()
        Me.chk_iBAHN = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtHotelAddress = New System.Windows.Forms.TextBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMap = New System.Windows.Forms.TextBox()
        Me.btnMap = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtWeather = New System.Windows.Forms.TextBox()
        Me.btnWeather = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtInternet = New System.Windows.Forms.TextBox()
        Me.btnInternet = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pnlDisabled = New System.Windows.Forms.Panel()
        Me.btnDisabledClose = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tmrChanges = New System.Windows.Forms.Timer(Me.components)
        Me.bgwSave = New System.ComponentModel.BackgroundWorker()
        Me.pnlWait = New System.Windows.Forms.Panel()
        Me.lblSaveProgress = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.progressFake = New System.Windows.Forms.ProgressBar()
        Me.pnlShadowTop = New System.Windows.Forms.Panel()
        Me.pnlShadowMain = New System.Windows.Forms.Panel()
        Me.pnlInfo = New System.Windows.Forms.Panel()
        Me.txtSKPasswordFile = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtUIScriptFile = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtUIConfigFile = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtSKConfigFile = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.pnlShadowInfo = New System.Windows.Forms.Panel()
        Me.pnlShadowDialog = New System.Windows.Forms.Panel()
        Me.pnlError = New System.Windows.Forms.Panel()
        Me.btnCloseError = New System.Windows.Forms.Button()
        Me.lblErrorText = New System.Windows.Forms.Label()
        Me.pnlChangeConfirm = New System.Windows.Forms.Panel()
        Me.btnNoReturnToApp = New System.Windows.Forms.Button()
        Me.btnYesCancel = New System.Windows.Forms.Button()
        Me.lblChangeConfirm = New System.Windows.Forms.Label()
        Me.pnlNoChangesConfirm = New System.Windows.Forms.Panel()
        Me.btnNoReturnToApp2 = New System.Windows.Forms.Button()
        Me.btnYesSaveAnyway = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.tooltipBla = New System.Windows.Forms.ToolTip(Me.components)
        Me.pnlAll = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.chkList_Brands = New System.Windows.Forms.CheckedListBox()
        Me.pnlTop.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.pnlButtons.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.pnlDisabled.SuspendLayout()
        Me.pnlWait.SuspendLayout()
        Me.pnlInfo.SuspendLayout()
        Me.pnlError.SuspendLayout()
        Me.pnlChangeConfirm.SuspendLayout()
        Me.pnlNoChangesConfirm.SuspendLayout()
        Me.pnlAll.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnHotelInformation
        '
        Me.btnHotelInformation.Location = New System.Drawing.Point(500, 23)
        Me.btnHotelInformation.Name = "btnHotelInformation"
        Me.btnHotelInformation.Size = New System.Drawing.Size(75, 24)
        Me.btnHotelInformation.TabIndex = 0
        Me.btnHotelInformation.Text = "test"
        Me.btnHotelInformation.UseVisualStyleBackColor = True
        '
        'txtHotelinformation
        '
        Me.txtHotelinformation.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHotelinformation.Location = New System.Drawing.Point(7, 24)
        Me.txtHotelinformation.Name = "txtHotelinformation"
        Me.txtHotelinformation.Size = New System.Drawing.Size(489, 23)
        Me.txtHotelinformation.TabIndex = 1
        '
        'cmbBrand
        '
        Me.cmbBrand.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBrand.FormattingEnabled = True
        Me.cmbBrand.Location = New System.Drawing.Point(73, 68)
        Me.cmbBrand.Name = "cmbBrand"
        Me.cmbBrand.Size = New System.Drawing.Size(270, 21)
        Me.cmbBrand.TabIndex = 2
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(170, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(781, 54)
        Me.lblTitle.TabIndex = 3
        Me.lblTitle.Text = "OVCC Information Updater"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlTop
        '
        Me.pnlTop.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.pnlTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTop.Controls.Add(Me.btnTest)
        Me.pnlTop.Controls.Add(Me.lblVersion)
        Me.pnlTop.Controls.Add(Me.PictureBox1)
        Me.pnlTop.Controls.Add(Me.lblTitle)
        Me.pnlTop.Location = New System.Drawing.Point(3, 3)
        Me.pnlTop.Name = "pnlTop"
        Me.pnlTop.Size = New System.Drawing.Size(956, 60)
        Me.pnlTop.TabIndex = 5
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(174, 3)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(46, 52)
        Me.btnTest.TabIndex = 15
        Me.btnTest.Text = "test"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'lblVersion
        '
        Me.lblVersion.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVersion.ForeColor = System.Drawing.Color.FromArgb(CType(CType(108, Byte), Integer), CType(CType(118, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.lblVersion.Location = New System.Drawing.Point(171, 43)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(780, 14)
        Me.lblVersion.TabIndex = 14
        Me.lblVersion.Text = "version number"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.BackgroundImage = Global.OVCCInformationUpdater.My.Resources.Resources.GuestTek_Header_Logo_small
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Location = New System.Drawing.Point(-1, -1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(172, 60)
        Me.PictureBox1.TabIndex = 4
        Me.PictureBox1.TabStop = False
        '
        'pnlMain
        '
        Me.pnlMain.BackColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.pnlMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMain.Controls.Add(Me.Panel1)
        Me.pnlMain.Controls.Add(Me.Panel2)
        Me.pnlMain.Controls.Add(Me.Panel9)
        Me.pnlMain.Controls.Add(Me.pnlButtons)
        Me.pnlMain.Controls.Add(Me.Panel7)
        Me.pnlMain.Controls.Add(Me.Panel6)
        Me.pnlMain.Controls.Add(Me.Panel5)
        Me.pnlMain.Controls.Add(Me.Panel4)
        Me.pnlMain.Controls.Add(Me.Panel3)
        Me.pnlMain.Location = New System.Drawing.Point(3, 185)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(956, 418)
        Me.pnlMain.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.txtSupportNumber)
        Me.Panel2.Location = New System.Drawing.Point(3, 288)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(582, 54)
        Me.Panel2.TabIndex = 12
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 3)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(152, 17)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Support phone number"
        '
        'txtSupportNumber
        '
        Me.txtSupportNumber.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSupportNumber.Location = New System.Drawing.Point(7, 24)
        Me.txtSupportNumber.Name = "txtSupportNumber"
        Me.txtSupportNumber.ReadOnly = True
        Me.txtSupportNumber.Size = New System.Drawing.Size(567, 23)
        Me.txtSupportNumber.TabIndex = 1
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.White
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel9.Controls.Add(Me.txtPassword)
        Me.Panel9.Controls.Add(Me.chk_SiteCash)
        Me.Panel9.Controls.Add(Me.chk_Password)
        Me.Panel9.Controls.Add(Me.Label10)
        Me.Panel9.Location = New System.Drawing.Point(600, 242)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(351, 100)
        Me.Panel9.TabIndex = 12
        '
        'txtPassword
        '
        Me.txtPassword.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(6, 68)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(337, 23)
        Me.txtPassword.TabIndex = 8
        '
        'chk_SiteCash
        '
        Me.chk_SiteCash.AutoSize = True
        Me.chk_SiteCash.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_SiteCash.Location = New System.Drawing.Point(6, 24)
        Me.chk_SiteCash.Name = "chk_SiteCash"
        Me.chk_SiteCash.Size = New System.Drawing.Size(164, 21)
        Me.chk_SiteCash.TabIndex = 8
        Me.chk_SiteCash.Text = "Enable payment system"
        Me.chk_SiteCash.UseVisualStyleBackColor = True
        '
        'chk_Password
        '
        Me.chk_Password.AutoSize = True
        Me.chk_Password.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_Password.Location = New System.Drawing.Point(6, 47)
        Me.chk_Password.Name = "chk_Password"
        Me.chk_Password.Size = New System.Drawing.Size(127, 21)
        Me.chk_Password.TabIndex = 8
        Me.chk_Password.Text = "Enable password"
        Me.chk_Password.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(3, 3)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(139, 17)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Session management"
        '
        'pnlButtons
        '
        Me.pnlButtons.BackColor = System.Drawing.Color.White
        Me.pnlButtons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlButtons.Controls.Add(Me.btnCancel)
        Me.pnlButtons.Controls.Add(Me.btnSave)
        Me.pnlButtons.Location = New System.Drawing.Point(3, 356)
        Me.pnlButtons.Name = "pnlButtons"
        Me.pnlButtons.Size = New System.Drawing.Size(948, 57)
        Me.pnlButtons.TabIndex = 12
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.btnCancel.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Location = New System.Drawing.Point(658, 4)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(284, 48)
        Me.btnCancel.TabIndex = 8
        Me.btnCancel.Text = "cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.btnSave.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(4, 4)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(284, 48)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "save && exit"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.White
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.chk_HotelArts)
        Me.Panel8.Controls.Add(Me.chk_Towneplace)
        Me.Panel8.Controls.Add(Me.chk_ResidenceInn)
        Me.Panel8.Controls.Add(Me.chk_Hilton)
        Me.Panel8.Controls.Add(Me.chk_Marriott)
        Me.Panel8.Controls.Add(Me.chk_GuestTek)
        Me.Panel8.Controls.Add(Me.chk_iBAHN)
        Me.Panel8.Controls.Add(Me.Label7)
        Me.Panel8.Controls.Add(Me.cmbBrand)
        Me.Panel8.Location = New System.Drawing.Point(1363, 266)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(351, 184)
        Me.Panel8.TabIndex = 11
        '
        'chk_HotelArts
        '
        Me.chk_HotelArts.AutoCheck = False
        Me.chk_HotelArts.AutoSize = True
        Me.chk_HotelArts.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_HotelArts.Location = New System.Drawing.Point(7, 156)
        Me.chk_HotelArts.Name = "chk_HotelArts"
        Me.chk_HotelArts.Size = New System.Drawing.Size(85, 21)
        Me.chk_HotelArts.TabIndex = 14
        Me.chk_HotelArts.Text = "Hotel Arts"
        Me.chk_HotelArts.UseVisualStyleBackColor = True
        '
        'chk_Towneplace
        '
        Me.chk_Towneplace.AutoCheck = False
        Me.chk_Towneplace.AutoSize = True
        Me.chk_Towneplace.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_Towneplace.Location = New System.Drawing.Point(7, 134)
        Me.chk_Towneplace.Name = "chk_Towneplace"
        Me.chk_Towneplace.Size = New System.Drawing.Size(96, 21)
        Me.chk_Towneplace.TabIndex = 13
        Me.chk_Towneplace.Text = "Towneplace"
        Me.chk_Towneplace.UseVisualStyleBackColor = True
        '
        'chk_ResidenceInn
        '
        Me.chk_ResidenceInn.AutoCheck = False
        Me.chk_ResidenceInn.AutoSize = True
        Me.chk_ResidenceInn.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_ResidenceInn.Location = New System.Drawing.Point(7, 90)
        Me.chk_ResidenceInn.Name = "chk_ResidenceInn"
        Me.chk_ResidenceInn.Size = New System.Drawing.Size(107, 21)
        Me.chk_ResidenceInn.TabIndex = 11
        Me.chk_ResidenceInn.Text = "Residence Inn"
        Me.chk_ResidenceInn.UseVisualStyleBackColor = True
        '
        'chk_Hilton
        '
        Me.chk_Hilton.AutoCheck = False
        Me.chk_Hilton.AutoSize = True
        Me.chk_Hilton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_Hilton.Location = New System.Drawing.Point(7, 68)
        Me.chk_Hilton.Name = "chk_Hilton"
        Me.chk_Hilton.Size = New System.Drawing.Size(61, 21)
        Me.chk_Hilton.TabIndex = 10
        Me.chk_Hilton.Text = "Hilton"
        Me.chk_Hilton.UseVisualStyleBackColor = True
        '
        'chk_Marriott
        '
        Me.chk_Marriott.AutoCheck = False
        Me.chk_Marriott.AutoSize = True
        Me.chk_Marriott.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_Marriott.Location = New System.Drawing.Point(7, 112)
        Me.chk_Marriott.Name = "chk_Marriott"
        Me.chk_Marriott.Size = New System.Drawing.Size(75, 21)
        Me.chk_Marriott.TabIndex = 12
        Me.chk_Marriott.Text = "Marriott"
        Me.chk_Marriott.UseVisualStyleBackColor = True
        '
        'chk_GuestTek
        '
        Me.chk_GuestTek.AutoCheck = False
        Me.chk_GuestTek.AutoSize = True
        Me.chk_GuestTek.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_GuestTek.Location = New System.Drawing.Point(7, 24)
        Me.chk_GuestTek.Name = "chk_GuestTek"
        Me.chk_GuestTek.Size = New System.Drawing.Size(80, 21)
        Me.chk_GuestTek.TabIndex = 9
        Me.chk_GuestTek.Text = "GuestTek"
        Me.chk_GuestTek.UseVisualStyleBackColor = True
        '
        'chk_iBAHN
        '
        Me.chk_iBAHN.AutoCheck = False
        Me.chk_iBAHN.AutoSize = True
        Me.chk_iBAHN.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chk_iBAHN.Location = New System.Drawing.Point(7, 46)
        Me.chk_iBAHN.Name = "chk_iBAHN"
        Me.chk_iBAHN.Size = New System.Drawing.Size(143, 21)
        Me.chk_iBAHN.TabIndex = 8
        Me.chk_iBAHN.Text = "iBAHN (deprecated)"
        Me.chk_iBAHN.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(99, 17)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Hotel brand UI"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.White
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.Controls.Add(Me.txtHotelAddress)
        Me.Panel7.Location = New System.Drawing.Point(3, 231)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(582, 54)
        Me.Panel7.TabIndex = 11
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 17)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Hotel address"
        '
        'txtHotelAddress
        '
        Me.txtHotelAddress.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHotelAddress.Location = New System.Drawing.Point(7, 24)
        Me.txtHotelAddress.Name = "txtHotelAddress"
        Me.txtHotelAddress.Size = New System.Drawing.Size(567, 23)
        Me.txtHotelAddress.TabIndex = 1
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.Label5)
        Me.Panel6.Controls.Add(Me.txtMap)
        Me.Panel6.Controls.Add(Me.btnMap)
        Me.Panel6.Location = New System.Drawing.Point(3, 174)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(582, 54)
        Me.Panel6.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(3, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 17)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Map url"
        '
        'txtMap
        '
        Me.txtMap.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMap.Location = New System.Drawing.Point(7, 24)
        Me.txtMap.Name = "txtMap"
        Me.txtMap.Size = New System.Drawing.Size(489, 23)
        Me.txtMap.TabIndex = 1
        '
        'btnMap
        '
        Me.btnMap.Location = New System.Drawing.Point(500, 23)
        Me.btnMap.Name = "btnMap"
        Me.btnMap.Size = New System.Drawing.Size(75, 24)
        Me.btnMap.TabIndex = 0
        Me.btnMap.Text = "test"
        Me.btnMap.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.txtWeather)
        Me.Panel5.Controls.Add(Me.btnWeather)
        Me.Panel5.Location = New System.Drawing.Point(3, 117)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(582, 54)
        Me.Panel5.TabIndex = 11
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Weather url"
        '
        'txtWeather
        '
        Me.txtWeather.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWeather.Location = New System.Drawing.Point(7, 24)
        Me.txtWeather.Name = "txtWeather"
        Me.txtWeather.Size = New System.Drawing.Size(489, 23)
        Me.txtWeather.TabIndex = 1
        '
        'btnWeather
        '
        Me.btnWeather.Location = New System.Drawing.Point(500, 23)
        Me.btnWeather.Name = "btnWeather"
        Me.btnWeather.Size = New System.Drawing.Size(75, 24)
        Me.btnWeather.TabIndex = 0
        Me.btnWeather.Text = "test"
        Me.btnWeather.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.White
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(Me.txtInternet)
        Me.Panel4.Controls.Add(Me.btnInternet)
        Me.Panel4.Location = New System.Drawing.Point(3, 60)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(582, 54)
        Me.Panel4.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 17)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Internet url"
        '
        'txtInternet
        '
        Me.txtInternet.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInternet.Location = New System.Drawing.Point(7, 24)
        Me.txtInternet.Name = "txtInternet"
        Me.txtInternet.Size = New System.Drawing.Size(489, 23)
        Me.txtInternet.TabIndex = 1
        '
        'btnInternet
        '
        Me.btnInternet.Location = New System.Drawing.Point(500, 23)
        Me.btnInternet.Name = "btnInternet"
        Me.btnInternet.Size = New System.Drawing.Size(75, 24)
        Me.btnInternet.TabIndex = 0
        Me.btnInternet.Text = "test"
        Me.btnInternet.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.txtHotelinformation)
        Me.Panel3.Controls.Add(Me.btnHotelInformation)
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(582, 54)
        Me.Panel3.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(141, 17)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Hotel information url"
        '
        'pnlDisabled
        '
        Me.pnlDisabled.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.pnlDisabled.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlDisabled.Controls.Add(Me.btnDisabledClose)
        Me.pnlDisabled.Controls.Add(Me.Label2)
        Me.pnlDisabled.Location = New System.Drawing.Point(19, 648)
        Me.pnlDisabled.Name = "pnlDisabled"
        Me.pnlDisabled.Size = New System.Drawing.Size(948, 57)
        Me.pnlDisabled.TabIndex = 13
        '
        'btnDisabledClose
        '
        Me.btnDisabledClose.Font = New System.Drawing.Font("Segoe UI", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDisabledClose.Location = New System.Drawing.Point(818, 4)
        Me.btnDisabledClose.Name = "btnDisabledClose"
        Me.btnDisabledClose.Size = New System.Drawing.Size(123, 47)
        Me.btnDisabledClose.TabIndex = 9
        Me.btnDisabledClose.Text = "close"
        Me.btnDisabledClose.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(809, 48)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "You need to run this app as an administrator "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tmrChanges
        '
        Me.tmrChanges.Enabled = True
        Me.tmrChanges.Interval = 1000
        '
        'bgwSave
        '
        Me.bgwSave.WorkerReportsProgress = True
        '
        'pnlWait
        '
        Me.pnlWait.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.pnlWait.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlWait.Controls.Add(Me.lblSaveProgress)
        Me.pnlWait.Controls.Add(Me.Label11)
        Me.pnlWait.Controls.Add(Me.progressFake)
        Me.pnlWait.Location = New System.Drawing.Point(1021, 59)
        Me.pnlWait.Name = "pnlWait"
        Me.pnlWait.Size = New System.Drawing.Size(309, 146)
        Me.pnlWait.TabIndex = 14
        '
        'lblSaveProgress
        '
        Me.lblSaveProgress.ForeColor = System.Drawing.Color.FromArgb(CType(CType(108, Byte), Integer), CType(CType(118, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.lblSaveProgress.Location = New System.Drawing.Point(3, 99)
        Me.lblSaveProgress.Name = "lblSaveProgress"
        Me.lblSaveProgress.Size = New System.Drawing.Size(301, 16)
        Me.lblSaveProgress.TabIndex = 3
        Me.lblSaveProgress.Text = "progress text goes here"
        Me.lblSaveProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(3, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(301, 99)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Saving settings"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'progressFake
        '
        Me.progressFake.Location = New System.Drawing.Point(3, 118)
        Me.progressFake.MarqueeAnimationSpeed = 50
        Me.progressFake.Name = "progressFake"
        Me.progressFake.Size = New System.Drawing.Size(301, 23)
        Me.progressFake.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.progressFake.TabIndex = 1
        '
        'pnlShadowTop
        '
        Me.pnlShadowTop.BackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.pnlShadowTop.Location = New System.Drawing.Point(1141, 6)
        Me.pnlShadowTop.Name = "pnlShadowTop"
        Me.pnlShadowTop.Size = New System.Drawing.Size(34, 33)
        Me.pnlShadowTop.TabIndex = 15
        '
        'pnlShadowMain
        '
        Me.pnlShadowMain.BackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.pnlShadowMain.Location = New System.Drawing.Point(1021, 6)
        Me.pnlShadowMain.Name = "pnlShadowMain"
        Me.pnlShadowMain.Size = New System.Drawing.Size(34, 33)
        Me.pnlShadowMain.TabIndex = 16
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.Color.FromArgb(CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.txtSKPasswordFile)
        Me.pnlInfo.Controls.Add(Me.Label14)
        Me.pnlInfo.Controls.Add(Me.txtUIScriptFile)
        Me.pnlInfo.Controls.Add(Me.Label16)
        Me.pnlInfo.Controls.Add(Me.txtUIConfigFile)
        Me.pnlInfo.Controls.Add(Me.Label12)
        Me.pnlInfo.Controls.Add(Me.txtSKConfigFile)
        Me.pnlInfo.Controls.Add(Me.Label8)
        Me.pnlInfo.Location = New System.Drawing.Point(3, 69)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(956, 110)
        Me.pnlInfo.TabIndex = 17
        '
        'txtSKPasswordFile
        '
        Me.txtSKPasswordFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.txtSKPasswordFile.Location = New System.Drawing.Point(98, 82)
        Me.txtSKPasswordFile.Name = "txtSKPasswordFile"
        Me.txtSKPasswordFile.ReadOnly = True
        Me.txtSKPasswordFile.Size = New System.Drawing.Size(853, 22)
        Me.txtSKPasswordFile.TabIndex = 23
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label14.Location = New System.Drawing.Point(7, 82)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(87, 19)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "Password file:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtUIScriptFile
        '
        Me.txtUIScriptFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.txtUIScriptFile.Location = New System.Drawing.Point(98, 56)
        Me.txtUIScriptFile.Name = "txtUIScriptFile"
        Me.txtUIScriptFile.ReadOnly = True
        Me.txtUIScriptFile.Size = New System.Drawing.Size(853, 22)
        Me.txtUIScriptFile.TabIndex = 21
        '
        'Label16
        '
        Me.Label16.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label16.Location = New System.Drawing.Point(7, 56)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(87, 19)
        Me.Label16.TabIndex = 20
        Me.Label16.Text = "UI script file:"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtUIConfigFile
        '
        Me.txtUIConfigFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.txtUIConfigFile.Location = New System.Drawing.Point(98, 30)
        Me.txtUIConfigFile.Name = "txtUIConfigFile"
        Me.txtUIConfigFile.ReadOnly = True
        Me.txtUIConfigFile.Size = New System.Drawing.Size(853, 22)
        Me.txtUIConfigFile.TabIndex = 19
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label12.Location = New System.Drawing.Point(8, 31)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(87, 19)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "UI config file:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSKConfigFile
        '
        Me.txtSKConfigFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(210, Byte), Integer))
        Me.txtSKConfigFile.Location = New System.Drawing.Point(98, 4)
        Me.txtSKConfigFile.Name = "txtSKConfigFile"
        Me.txtSKConfigFile.ReadOnly = True
        Me.txtSKConfigFile.Size = New System.Drawing.Size(853, 22)
        Me.txtSKConfigFile.TabIndex = 18
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label8.Location = New System.Drawing.Point(8, 5)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(87, 19)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "SK config file:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pnlShadowInfo
        '
        Me.pnlShadowInfo.BackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.pnlShadowInfo.Location = New System.Drawing.Point(1101, 6)
        Me.pnlShadowInfo.Name = "pnlShadowInfo"
        Me.pnlShadowInfo.Size = New System.Drawing.Size(34, 33)
        Me.pnlShadowInfo.TabIndex = 16
        '
        'pnlShadowDialog
        '
        Me.pnlShadowDialog.BackColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.pnlShadowDialog.Location = New System.Drawing.Point(1061, 6)
        Me.pnlShadowDialog.Name = "pnlShadowDialog"
        Me.pnlShadowDialog.Size = New System.Drawing.Size(34, 33)
        Me.pnlShadowDialog.TabIndex = 18
        '
        'pnlError
        '
        Me.pnlError.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.pnlError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlError.Controls.Add(Me.btnCloseError)
        Me.pnlError.Controls.Add(Me.lblErrorText)
        Me.pnlError.Location = New System.Drawing.Point(1021, 217)
        Me.pnlError.Name = "pnlError"
        Me.pnlError.Size = New System.Drawing.Size(309, 146)
        Me.pnlError.TabIndex = 19
        '
        'btnCloseError
        '
        Me.btnCloseError.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnCloseError.Location = New System.Drawing.Point(106, 114)
        Me.btnCloseError.Name = "btnCloseError"
        Me.btnCloseError.Size = New System.Drawing.Size(94, 27)
        Me.btnCloseError.TabIndex = 20
        Me.btnCloseError.Text = "OK"
        Me.btnCloseError.UseVisualStyleBackColor = True
        '
        'lblErrorText
        '
        Me.lblErrorText.BackColor = System.Drawing.Color.Transparent
        Me.lblErrorText.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblErrorText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblErrorText.Location = New System.Drawing.Point(3, 0)
        Me.lblErrorText.Name = "lblErrorText"
        Me.lblErrorText.Size = New System.Drawing.Size(301, 99)
        Me.lblErrorText.TabIndex = 2
        Me.lblErrorText.Text = "error"
        Me.lblErrorText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlChangeConfirm
        '
        Me.pnlChangeConfirm.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.pnlChangeConfirm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlChangeConfirm.Controls.Add(Me.btnNoReturnToApp)
        Me.pnlChangeConfirm.Controls.Add(Me.btnYesCancel)
        Me.pnlChangeConfirm.Controls.Add(Me.lblChangeConfirm)
        Me.pnlChangeConfirm.Location = New System.Drawing.Point(1021, 544)
        Me.pnlChangeConfirm.Name = "pnlChangeConfirm"
        Me.pnlChangeConfirm.Size = New System.Drawing.Size(309, 146)
        Me.pnlChangeConfirm.TabIndex = 21
        '
        'btnNoReturnToApp
        '
        Me.btnNoReturnToApp.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNoReturnToApp.Location = New System.Drawing.Point(167, 114)
        Me.btnNoReturnToApp.Name = "btnNoReturnToApp"
        Me.btnNoReturnToApp.Size = New System.Drawing.Size(137, 27)
        Me.btnNoReturnToApp.TabIndex = 21
        Me.btnNoReturnToApp.Text = "No, return to app"
        Me.btnNoReturnToApp.UseVisualStyleBackColor = True
        '
        'btnYesCancel
        '
        Me.btnYesCancel.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYesCancel.Location = New System.Drawing.Point(3, 114)
        Me.btnYesCancel.Name = "btnYesCancel"
        Me.btnYesCancel.Size = New System.Drawing.Size(137, 27)
        Me.btnYesCancel.TabIndex = 20
        Me.btnYesCancel.Text = "Yes, cancel"
        Me.btnYesCancel.UseVisualStyleBackColor = True
        '
        'lblChangeConfirm
        '
        Me.lblChangeConfirm.BackColor = System.Drawing.Color.Transparent
        Me.lblChangeConfirm.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChangeConfirm.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblChangeConfirm.Location = New System.Drawing.Point(3, 0)
        Me.lblChangeConfirm.Name = "lblChangeConfirm"
        Me.lblChangeConfirm.Size = New System.Drawing.Size(301, 111)
        Me.lblChangeConfirm.TabIndex = 2
        Me.lblChangeConfirm.Text = "There are unsaved changes." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Are you sure you want to cancel and exit?"
        Me.lblChangeConfirm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlNoChangesConfirm
        '
        Me.pnlNoChangesConfirm.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.pnlNoChangesConfirm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlNoChangesConfirm.Controls.Add(Me.btnNoReturnToApp2)
        Me.pnlNoChangesConfirm.Controls.Add(Me.btnYesSaveAnyway)
        Me.pnlNoChangesConfirm.Controls.Add(Me.Label13)
        Me.pnlNoChangesConfirm.Location = New System.Drawing.Point(1021, 372)
        Me.pnlNoChangesConfirm.Name = "pnlNoChangesConfirm"
        Me.pnlNoChangesConfirm.Size = New System.Drawing.Size(309, 146)
        Me.pnlNoChangesConfirm.TabIndex = 22
        '
        'btnNoReturnToApp2
        '
        Me.btnNoReturnToApp2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNoReturnToApp2.Location = New System.Drawing.Point(167, 114)
        Me.btnNoReturnToApp2.Name = "btnNoReturnToApp2"
        Me.btnNoReturnToApp2.Size = New System.Drawing.Size(137, 27)
        Me.btnNoReturnToApp2.TabIndex = 21
        Me.btnNoReturnToApp2.Text = "No, return to app"
        Me.btnNoReturnToApp2.UseVisualStyleBackColor = True
        '
        'btnYesSaveAnyway
        '
        Me.btnYesSaveAnyway.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYesSaveAnyway.Location = New System.Drawing.Point(3, 114)
        Me.btnYesSaveAnyway.Name = "btnYesSaveAnyway"
        Me.btnYesSaveAnyway.Size = New System.Drawing.Size(137, 27)
        Me.btnYesSaveAnyway.TabIndex = 20
        Me.btnYesSaveAnyway.Text = "Yes, save anyway"
        Me.btnYesSaveAnyway.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(3, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(301, 111)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "You made no changes to the current configuration." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Do you want to save anyway (" &
    "this will overwrite the current configuration with the same settings)?"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlAll
        '
        Me.pnlAll.Controls.Add(Me.pnlTop)
        Me.pnlAll.Controls.Add(Me.pnlMain)
        Me.pnlAll.Controls.Add(Me.pnlInfo)
        Me.pnlAll.Location = New System.Drawing.Point(12, 12)
        Me.pnlAll.Name = "pnlAll"
        Me.pnlAll.Size = New System.Drawing.Size(962, 606)
        Me.pnlAll.TabIndex = 23
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.chkList_Brands)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Location = New System.Drawing.Point(600, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(351, 236)
        Me.Panel1.TabIndex = 13
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(3, 3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(99, 17)
        Me.Label15.TabIndex = 7
        Me.Label15.Text = "Hotel brand UI"
        '
        'chkList_Brands
        '
        Me.chkList_Brands.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.chkList_Brands.FormattingEnabled = True
        Me.chkList_Brands.Location = New System.Drawing.Point(4, 26)
        Me.chkList_Brands.Name = "chkList_Brands"
        Me.chkList_Brands.Size = New System.Drawing.Size(341, 204)
        Me.chkList_Brands.TabIndex = 8
        '
        'frmMain
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.LightGray
        Me.ClientSize = New System.Drawing.Size(1784, 723)
        Me.Controls.Add(Me.pnlAll)
        Me.Controls.Add(Me.pnlNoChangesConfirm)
        Me.Controls.Add(Me.pnlChangeConfirm)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.pnlError)
        Me.Controls.Add(Me.pnlShadowDialog)
        Me.Controls.Add(Me.pnlShadowInfo)
        Me.Controls.Add(Me.pnlWait)
        Me.Controls.Add(Me.pnlDisabled)
        Me.Controls.Add(Me.pnlShadowTop)
        Me.Controls.Add(Me.pnlShadowMain)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(55, Byte), Integer), CType(CType(70, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "OVCC Information Updater"
        Me.pnlTop.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.pnlButtons.ResumeLayout(False)
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.pnlDisabled.ResumeLayout(False)
        Me.pnlWait.ResumeLayout(False)
        Me.pnlInfo.ResumeLayout(False)
        Me.pnlInfo.PerformLayout()
        Me.pnlError.ResumeLayout(False)
        Me.pnlChangeConfirm.ResumeLayout(False)
        Me.pnlNoChangesConfirm.ResumeLayout(False)
        Me.pnlAll.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnHotelInformation As System.Windows.Forms.Button
    Friend WithEvents txtHotelinformation As System.Windows.Forms.TextBox
    Friend WithEvents cmbBrand As System.Windows.Forms.ComboBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents pnlTop As System.Windows.Forms.Panel
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtInternet As System.Windows.Forms.TextBox
    Friend WithEvents btnInternet As System.Windows.Forms.Button
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtHotelAddress As System.Windows.Forms.TextBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtMap As System.Windows.Forms.TextBox
    Friend WithEvents btnMap As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtWeather As System.Windows.Forms.TextBox
    Friend WithEvents btnWeather As System.Windows.Forms.Button
    Friend WithEvents pnlButtons As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents chk_GuestTek As System.Windows.Forms.CheckBox
    Friend WithEvents chk_iBAHN As System.Windows.Forms.CheckBox
    Friend WithEvents pnlDisabled As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnDisabledClose As System.Windows.Forms.Button
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents chk_SiteCash As System.Windows.Forms.CheckBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tmrChanges As System.Windows.Forms.Timer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtSupportNumber As System.Windows.Forms.TextBox
    Friend WithEvents bgwSave As System.ComponentModel.BackgroundWorker
    Friend WithEvents pnlWait As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents progressFake As System.Windows.Forms.ProgressBar
    Friend WithEvents lblSaveProgress As System.Windows.Forms.Label
    Friend WithEvents chk_Hilton As CheckBox
    Friend WithEvents pnlShadowTop As Panel
    Friend WithEvents pnlShadowMain As Panel
    Friend WithEvents chk_ResidenceInn As CheckBox
    Friend WithEvents chk_Towneplace As CheckBox
    Friend WithEvents chk_Marriott As CheckBox
    Friend WithEvents pnlInfo As Panel
    Friend WithEvents Label12 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents pnlShadowInfo As Panel
    Friend WithEvents txtUIConfigFile As TextBox
    Friend WithEvents txtSKConfigFile As TextBox
    Friend WithEvents pnlShadowDialog As Panel
    Friend WithEvents pnlError As Panel
    Friend WithEvents btnCloseError As Button
    Friend WithEvents lblErrorText As Label
    Friend WithEvents btnTest As Button
    Friend WithEvents pnlChangeConfirm As Panel
    Friend WithEvents btnYesCancel As Button
    Friend WithEvents lblChangeConfirm As Label
    Friend WithEvents btnNoReturnToApp As Button
    Friend WithEvents pnlNoChangesConfirm As Panel
    Friend WithEvents btnNoReturnToApp2 As Button
    Friend WithEvents btnYesSaveAnyway As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents tooltipBla As ToolTip
    Friend WithEvents chk_Password As CheckBox
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents pnlAll As Panel
    Friend WithEvents txtUIScriptFile As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents chk_HotelArts As CheckBox
    Friend WithEvents txtSKPasswordFile As TextBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label15 As Label
    Friend WithEvents chkList_Brands As CheckedListBox
End Class
