﻿Public Class frmMain
    Private Function FindTopLocation(ByVal item As Control) As Integer
        Dim i_top As Integer = 0

        i_top = item.Top

        If Not TypeOf item Is Form Then
            i_top += FindTopLocation(item.Parent)
        End If

        Return i_top
    End Function

    Private Function FindLeftLocation(ByVal item As Control) As Integer
        Dim i_left As Integer = 0

        i_left = item.Left

        If Not TypeOf item Is Form Then
            i_left += FindLeftLocation(item.Parent)
        End If

        Return i_left
    End Function

#Region "Thread Safe stuff"
    Delegate Sub UpdateSaveProgress__Callback(ByVal [text] As String)

    Public Sub UpdateSaveProgress(ByVal [text] As String)
        If Me.lblSaveProgress.InvokeRequired Then
            Dim d As New UpdateSaveProgress__Callback(AddressOf UpdateSaveProgress)
            Me.Invoke(d, New Object() {[text]})
        Else
            Me.lblSaveProgress.Text = [text]
        End If
    End Sub
#End Region


    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        clsGlobals.InitGlobals()

        clsGlobals.oLogger = New Logger

        clsGlobals.oLogger.LogFileDirectory = clsGlobals.g_LogFileDirectory
        clsGlobals.oLogger.LogFileName = clsGlobals.g_LogFileName

        clsGlobals.oLogger.WriteToLog(New String("*", 50))


        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        clsGlobals.oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        lblVersion.Text = "version: " & myBuildInfo.FileVersion & " - build date: " & Helpers.FilesAndFolders.GetFileCompileDate(Application.ExecutablePath)


        clsGlobals.oHotelBrand = New clsHotelBrand
        clsGlobalSettings.GetConfigFilesForFrontend()
        clsGlobalSettings.ReadGlobalSettings()
        clsGlobalSettings.ReadPhoneNumberFromEnglishLanguageFile()
        clsGlobalSettings.ReadPassword()
        clsGlobalSettings.LoadThemes()



        txtSKConfigFile.Text = clsGlobals.g_SKConfigFile
        txtUIConfigFile.Text = clsGlobals.g_UIConfigFile
        txtUIScriptFile.Text = clsGlobals.g_UIScriptFile
        txtSKPasswordFile.Text = clsGlobals.g_PasswordFile


        clsGlobals.oLogger.WriteToLog("loading command line arguments", , 1)
        For Each arg As String In Environment.GetCommandLineArgs()
            clsGlobals.oLogger.WriteToLog("found: " & arg, , 2)

            If InStr(arg, "--quiet--hotel-info-url:") > 0 Then
                clsGlobals.g_Url_Hotel = arg.Replace("--quiet--hotel-info-url:", "")
                clsGlobals.oLogger.WriteToLog("quiet - set hotel info url: " & clsGlobals.g_Url_Hotel, , 3)
                clsGlobals.g_Quiet = True
            End If

            If InStr(arg, "--quiet--brand-residenceinn") > 0 Then
                clsGlobals.g_Brand = 668
                clsGlobals.oHotelBrand.BrandResidenceInn = True
                clsGlobals.oHotelBrand.BrandNumber = 668
                clsGlobals.g_PaymentEnabled = True
                clsGlobals.oLogger.WriteToLog("quiet - set brand to ResidenceInn", , 3)
                clsGlobals.g_Quiet = True
            End If

            If InStr(arg, "--quiet--brand-towneplace:") > 0 Then
                clsGlobals.oHotelBrand.BrandTowneplace_String = arg.Replace("--quiet--brand-towneplace:", "")

                If clsGlobals.oHotelBrand.BrandTowneplace_String.Equals("") Then
                    'lets leave it alone
                    clsGlobals.oHotelBrand.BrandTowneplace_String = "empty"
                Else
                    'Update url
                    clsGlobals.g_Url_Internet = clsGlobals.c_URL_Townplace_URL_Template.Replace("%%TOWNEPLACEID%%", clsGlobals.oHotelBrand.BrandTowneplace_String)
                    clsGlobals.g_Url_Internet__Changed = True
                End If

                clsGlobals.g_Brand = 670
                clsGlobals.oHotelBrand.BrandTowneplace = True
                clsGlobals.oHotelBrand.BrandNumber = 670
                clsGlobals.g_PaymentEnabled = False
                clsGlobals.oLogger.WriteToLog("quiet - set brand to Towneplace (" & clsGlobals.oHotelBrand.BrandTowneplace_String & ")", , 3)
                clsGlobals.g_Quiet = True
            End If

        Next


        'test
        btnTest.Visible = Helpers.Generic.IsDevMachine()


        'Ui stuff
        'Me.Size = New Size(620, 744) ' 700 = (panel.height) + (panel.top) + 122
        'Me.Size = New Size(623, pnlMain.Top + pnlMain.Height + 42)
        'Me.Size = New Size(620, pnlMain.Top + pnlMain.Height + 42)
        Me.ClientSize = New Size(pnlAll.Width + 2 * pnlAll.Left, pnlAll.Height + 2 * pnlAll.Top)


        If clsGlobals.g_Quiet Then
            'we're not going to show the UI, so no use for all that fancy stuff

            If Not Helpers.Generic.IsRunningAsAdmin Then
                clsGlobals.oLogger.WriteToLog("NOT AN ADMIN!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                Me.Close()
            End If

            ToggleControls(False)

            SaveAllTheStuff()
        Else
            'themes
            chkList_Brands.Items.Clear()

            For Each _brand As HotelBrands.HotelBrand In HotelBrands.GetBrands
                chkList_Brands.Items.Add(_brand.Name)
            Next


            'shadows
            pnlShadowTop.Width = pnlTop.Width
            pnlShadowTop.Height = pnlTop.Height
            pnlShadowTop.Top = pnlTop.Top + clsGlobals.c_UI_ShadowSize
            pnlShadowTop.Left = pnlTop.Left + clsGlobals.c_UI_ShadowSize
            pnlShadowTop.SendToBack()

            pnlShadowTop.Visible = False


            pnlShadowInfo.Width = pnlInfo.Width
            pnlShadowInfo.Height = pnlInfo.Height
            pnlShadowInfo.Top = pnlInfo.Top + clsGlobals.c_UI_ShadowSize
            pnlShadowInfo.Left = pnlInfo.Left + clsGlobals.c_UI_ShadowSize
            pnlShadowInfo.SendToBack()

            pnlShadowInfo.Visible = False


            pnlShadowMain.Width = pnlMain.Width
            pnlShadowMain.Height = pnlMain.Height
            pnlShadowMain.Top = pnlMain.Top + clsGlobals.c_UI_ShadowSize
            pnlShadowMain.Left = pnlMain.Left + clsGlobals.c_UI_ShadowSize
            pnlShadowMain.SendToBack()

            pnlShadowMain.Visible = False


            pnlShadowDialog.Width = pnlWait.Width
            pnlShadowDialog.Height = pnlWait.Height
            pnlShadowDialog.Top = pnlWait.Top + clsGlobals.c_UI_ShadowSize
            pnlShadowDialog.Left = pnlWait.Left + clsGlobals.c_UI_ShadowSize
            pnlShadowDialog.SendToBack()



            cmbBrand.Items.Clear()

            cmbBrand.Items.Add("Hilton Hotel & Resorts")
            cmbBrand.Items.Add("Garden Inn")
            cmbBrand.Items.Add("Hampton")
            cmbBrand.Items.Add("Waldord Astoria")
            cmbBrand.Items.Add("Conrad")
            cmbBrand.Items.Add("DoubleTree")
            cmbBrand.Items.Add("Canopy")
            cmbBrand.Items.Add("Curio")
            cmbBrand.Items.Add("Embassy Suites")
            cmbBrand.Items.Add("Homewood Suites")
            cmbBrand.Items.Add("Home2")
            cmbBrand.Items.Add("Hilton Grand Vacations")
            cmbBrand.Items.Add("Tru")
            cmbBrand.Items.Add("Tapestry Collection")

            txtHotelAddress.Text = clsGlobals.g_Address_Site
            txtHotelinformation.Text = clsGlobals.g_Url_Hotel
            txtInternet.Text = clsGlobals.g_Url_Internet
            txtMap.Text = clsGlobals.g_Url_Map
            txtWeather.Text = clsGlobals.g_Url_Weather
            txtSupportNumber.Text = clsGlobals.g_Support_PhoneNumber

            Try
                If clsGlobals.oHotelBrand.BrandNumber >= cmbBrand.Items.Count Then
                    Throw New Exception("Brand number exceeds brand index")
                End If
                If clsGlobals.oHotelBrand.BrandNumber < 0 Then
                    Throw New Exception("Brand number smaller than 0")
                End If

                cmbBrand.SelectedIndex = clsGlobals.oHotelBrand.BrandNumber
            Catch ex As Exception
            End Try
            If clsGlobals.oHotelBrand.BrandGuestTek Then
                chk_GuestTek.Checked = True
                cmbBrand.Enabled = False
            End If
            If clsGlobals.oHotelBrand.BrandiBAHN Then
                chk_iBAHN.Checked = True
                cmbBrand.Enabled = False
            End If
            If clsGlobals.oHotelBrand.BrandResidenceInn Then
                chk_ResidenceInn.Checked = True
                cmbBrand.Enabled = False
            End If
            If clsGlobals.oHotelBrand.BrandMarriott Then
                chk_Marriott.Checked = True
                cmbBrand.Enabled = False
            End If
            If clsGlobals.oHotelBrand.BrandTowneplace Then
                chk_Towneplace.Checked = True
                cmbBrand.Enabled = False
            End If
            If clsGlobals.oHotelBrand.BrandHotelArts Then
                chk_HotelArts.Checked = True
                cmbBrand.Enabled = False
            End If

            If Not clsGlobals.oHotelBrand.BrandGuestTek And
                Not clsGlobals.oHotelBrand.BrandiBAHN And
                Not clsGlobals.oHotelBrand.BrandResidenceInn And
                Not clsGlobals.oHotelBrand.BrandMarriott And
                Not clsGlobals.oHotelBrand.BrandTowneplace And
                Not clsGlobals.oHotelBrand.BrandHotelArts Then

                chk_Hilton.Checked = True
                cmbBrand.Enabled = True
            End If


            clsGlobals.oSkCfg.LoadPaymentEnabled()
            chk_SiteCash.Checked = clsGlobals.g_PaymentEnabled


            chk_Password.Checked = clsGlobals.g_PasswordEnabled
            txtPassword.Enabled = chk_Password.Checked
            txtPassword.Text = clsGlobals.g_Password


            If Not Helpers.Generic.IsRunningAsAdmin Then
                ToggleControls(False)

                Dim pTmp As New Point
                pTmp.X = pnlButtons.Left + pnlMain.Left + 1
                pTmp.Y = pnlButtons.Top + pnlMain.Top + 1

                pnlDisabled.Location = pTmp
                pnlDisabled.BringToFront()
            Else
                ToggleControls(True)
            End If



            'Add mouse click and key press event to all controls
            AddEventsToChildControls(Me.Controls)
        End If
    End Sub

    Private Sub ToggleControls(_enabled As Boolean)
        btnSave.Enabled = _enabled
        btnCancel.Enabled = _enabled

        txtHotelAddress.Enabled = _enabled
        txtHotelinformation.Enabled = _enabled
        txtInternet.Enabled = _enabled
        txtMap.Enabled = _enabled
        txtWeather.Enabled = _enabled
        txtSupportNumber.Enabled = _enabled

        btnHotelInformation.Enabled = _enabled
        btnInternet.Enabled = _enabled
        btnMap.Enabled = _enabled
        btnWeather.Enabled = _enabled

        'chk_iBAHN.Enabled = enabled And clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_iBAHN__UI)
        'chk_GuestTek.Enabled = enabled And clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Guesttek__UI)
        'chk_Hilton.Enabled = enabled And clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Hilton__UI)
        'chk_ResidenceInn.Enabled = enabled And clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_ResidenceInn__UI)
        'chk_Marriott.Enabled = enabled And clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Marriott__UI)
        'chk_Towneplaza.Enabled = enabled And clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Towneplaza__UI)

        If clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_iBAHN__UI) Then
            chk_iBAHN.Enabled = _enabled
            chk_iBAHN.Font = New Font(chk_iBAHN.Font, chk_iBAHN.Font.Style And Not FontStyle.Strikeout)
        Else
            chk_iBAHN.Enabled = False
            chk_iBAHN.Font = New Font(chk_iBAHN.Font, chk_iBAHN.Font.Style Or FontStyle.Strikeout)
        End If
        If clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Guesttek__UI) Then
            chk_GuestTek.Enabled = _enabled
            chk_GuestTek.Font = New Font(chk_GuestTek.Font, chk_GuestTek.Font.Style And Not FontStyle.Strikeout)
        Else
            chk_GuestTek.Enabled = False
            chk_GuestTek.Font = New Font(chk_GuestTek.Font, chk_GuestTek.Font.Style Or FontStyle.Strikeout)
        End If
        If clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Hilton__UI) Then
            chk_Hilton.Enabled = _enabled
            chk_Hilton.Font = New Font(chk_Hilton.Font, chk_Hilton.Font.Style And Not FontStyle.Strikeout)
        Else
            chk_Hilton.Enabled = False
            chk_Hilton.Font = New Font(chk_Hilton.Font, chk_Hilton.Font.Style Or FontStyle.Strikeout)
        End If
        If clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_ResidenceInn__UI) Then
            chk_ResidenceInn.Enabled = _enabled
            chk_ResidenceInn.Font = New Font(chk_ResidenceInn.Font, chk_ResidenceInn.Font.Style And Not FontStyle.Strikeout)
        Else
            chk_ResidenceInn.Enabled = False
            chk_ResidenceInn.Font = New Font(chk_ResidenceInn.Font, chk_ResidenceInn.Font.Style Or FontStyle.Strikeout)
        End If
        If clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Marriott__UI) Then
            chk_Marriott.Enabled = _enabled
            chk_Marriott.Font = New Font(chk_Marriott.Font, chk_Marriott.Font.Style And Not FontStyle.Strikeout)
        Else
            chk_Marriott.Enabled = False
            chk_Marriott.Font = New Font(chk_Marriott.Font, chk_Marriott.Font.Style Or FontStyle.Strikeout)
        End If
        If clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Towneplace__UI) Then
            chk_Towneplace.Enabled = _enabled
            chk_Towneplace.Font = New Font(chk_Towneplace.Font, chk_Towneplace.Font.Style And Not FontStyle.Strikeout)
        Else
            chk_Towneplace.Enabled = False
            chk_Towneplace.Font = New Font(chk_Towneplace.Font, chk_Towneplace.Font.Style Or FontStyle.Strikeout)
        End If
        If clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_HotelArts__UI) Then
            chk_HotelArts.Enabled = _enabled
            chk_HotelArts.Font = New Font(chk_HotelArts.Font, chk_HotelArts.Font.Style And Not FontStyle.Strikeout)
        Else
            chk_HotelArts.Enabled = False
            chk_HotelArts.Font = New Font(chk_HotelArts.Font, chk_HotelArts.Font.Style Or FontStyle.Strikeout)
        End If


        cmbBrand.Enabled = chk_Hilton.Enabled And chk_Hilton.Checked And _enabled

        chk_SiteCash.Enabled = _enabled

        chk_Password.Enabled = _enabled
        txtPassword.Enabled = _enabled


        btnTest.Enabled = _enabled


        txtSKConfigFile.Enabled = _enabled
        txtUIConfigFile.Enabled = _enabled
    End Sub

    Private Sub ToggleWaitPopup(_enabled As Boolean)
        Dim pTmp As New Point, pTmpShadow As New Point

        If _enabled Then
            pnlWait.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlWait.Width) / 2
            pTmp.Y = (Me.Height - pnlWait.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlWait.Size
            pnlShadowDialog.BringToFront()

            pnlWait.Location = pTmp
            pnlWait.BringToFront()
        Else
            pnlWait.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlWait.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub

    Private Sub ToggleErrorPopup(sErr As String)
        Dim pTmp As New Point, pTmpShadow As New Point

        If Not sErr.Equals("") Then
            ToggleControls(False)

            btnCloseError.Enabled = True

            lblErrorText.Text = sErr

            pnlError.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlError.Width) / 2
            pTmp.Y = (Me.Height - pnlError.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlError.Size
            pnlShadowDialog.BringToFront()

            pnlError.Location = pTmp
            pnlError.BringToFront()
        Else
            ToggleControls(True)

            btnCloseError.Enabled = False

            pnlError.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlError.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub

    Private Sub ToggleChangeConfirmPopup(_enabled As Boolean)
        Dim pTmp As New Point, pTmpShadow As New Point

        If _enabled Then
            ToggleControls(False)

            btnYesCancel.Enabled = True
            btnNoReturnToApp.Enabled = True

            pnlChangeConfirm.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlChangeConfirm.Width) / 2
            pTmp.Y = (Me.Height - pnlChangeConfirm.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlChangeConfirm.Size
            pnlShadowDialog.BringToFront()

            pnlChangeConfirm.Location = pTmp
            pnlChangeConfirm.BringToFront()
        Else
            ToggleControls(True)

            btnYesCancel.Enabled = False
            btnNoReturnToApp.Enabled = False


            pnlChangeConfirm.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlChangeConfirm.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub

    Private Sub ToggleNoChangesConfirmPopup(_enabled As Boolean)
        Dim pTmp As New Point, pTmpShadow As New Point

        If _enabled Then
            ToggleControls(False)

            btnYesSaveAnyway.Enabled = True
            btnNoReturnToApp2.Enabled = True

            pnlNoChangesConfirm.Visible = True
            pnlShadowDialog.Visible = True

            pTmp.X = (Me.Width - pnlNoChangesConfirm.Width) / 2
            pTmp.Y = (Me.Height - pnlNoChangesConfirm.Height) / 2
            pTmpShadow.X = pTmp.X + clsGlobals.c_UI_ShadowSize
            pTmpShadow.Y = pTmp.Y + clsGlobals.c_UI_ShadowSize

            pnlShadowDialog.Location = pTmpShadow
            pnlShadowDialog.Size = pnlNoChangesConfirm.Size
            pnlShadowDialog.BringToFront()

            pnlNoChangesConfirm.Location = pTmp
            pnlNoChangesConfirm.BringToFront()
        Else
            ToggleControls(True)

            btnYesSaveAnyway.Enabled = False
            btnNoReturnToApp2.Enabled = False


            pnlNoChangesConfirm.Visible = False
            pnlShadowDialog.Visible = False

            pTmp.X = 790
            pTmp.Y = 148

            pnlNoChangesConfirm.Location = pTmp
            pnlShadowDialog.Location = pTmp
        End If
    End Sub


    Private Sub AddEventsToChildControls(cc As ControlCollection)
        Dim c As Control = Me

        Do Until c Is Nothing
            AddHandler c.MouseClick, AddressOf GenericEventHandler
            AddHandler c.KeyUp, AddressOf GenericEventHandler

            c = GetNextControl(c, True)
        Loop
    End Sub

    Private Sub GenericEventHandler(ByVal sender As Object, ByVal e As EventArgs)
        UpdateSaveButton()
    End Sub

    Private Sub chk_GuestTek_Clicked(sender As Object, e As EventArgs) Handles chk_GuestTek.Click
        If Not clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Guesttek__UI) Then
            ToggleErrorPopup("GuestTek theme not found!")

            chk_GuestTek.Checked = False

            Exit Sub
        End If

        If Not chk_GuestTek.Checked Then
            chk_GuestTek.Checked = True

            chk_iBAHN.Checked = False
            chk_ResidenceInn.Checked = False
            chk_Hilton.Checked = False
            chk_Marriott.Checked = False
            chk_Towneplace.Checked = False
            chk_HotelArts.Checked = False

            cmbBrand.Enabled = False

            clsGlobals.oHotelBrand.BrandGuestTek = True
            clsGlobals.oHotelBrand.BrandiBAHN = False
            clsGlobals.oHotelBrand.BrandResidenceInn = False
            clsGlobals.oHotelBrand.BrandMarriott = False
            clsGlobals.oHotelBrand.BrandTowneplace = False
            clsGlobals.oHotelBrand.BrandHotelArts = False
            clsGlobals.oHotelBrand.BrandNumber = 667
        End If
    End Sub

    Private Sub chk_iBAHN_Clicked(sender As Object, e As EventArgs) Handles chk_iBAHN.Click
        If Not clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_iBAHN__UI) Then
            ToggleErrorPopup("iBAHN theme not found!")

            chk_iBAHN.Checked = False

            Exit Sub
        End If

        If Not chk_iBAHN.Checked Then
            chk_iBAHN.Checked = True

            chk_GuestTek.Checked = False
            chk_ResidenceInn.Checked = False
            chk_Hilton.Checked = False
            chk_Marriott.Checked = False
            chk_Towneplace.Checked = False
            chk_HotelArts.Checked = False

            cmbBrand.Enabled = False

            clsGlobals.oHotelBrand.BrandGuestTek = False
            clsGlobals.oHotelBrand.BrandiBAHN = True
            clsGlobals.oHotelBrand.BrandResidenceInn = False
            clsGlobals.oHotelBrand.BrandMarriott = False
            clsGlobals.oHotelBrand.BrandTowneplace = False
            clsGlobals.oHotelBrand.BrandHotelArts = False
            clsGlobals.oHotelBrand.BrandNumber = 666
        End If
    End Sub

    Private Sub chk_Hilton_Clicked(sender As Object, e As EventArgs) Handles chk_Hilton.Click
        If Not clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Hilton__UI) Then
            ToggleErrorPopup("Hilton theme not found!")

            chk_Hilton.Checked = False

            Exit Sub
        End If

        If Not chk_Hilton.Checked Then
            chk_Hilton.Checked = True

            chk_ResidenceInn.Checked = False
            chk_GuestTek.Checked = False
            chk_iBAHN.Checked = False
            chk_Marriott.Checked = False
            chk_Towneplace.Checked = False
            chk_HotelArts.Checked = False

            cmbBrand.Enabled = True

            If cmbBrand.SelectedIndex < 0 Or cmbBrand.SelectedIndex > 600 Then
                cmbBrand.SelectedIndex = 0
            End If
            clsGlobals.oHotelBrand.BrandGuestTek = False
            clsGlobals.oHotelBrand.BrandiBAHN = False
            clsGlobals.oHotelBrand.BrandResidenceInn = False
            clsGlobals.oHotelBrand.BrandMarriott = False
            clsGlobals.oHotelBrand.BrandTowneplace = False
            clsGlobals.oHotelBrand.BrandHotelArts = False
            clsGlobals.oHotelBrand.BrandNumber = cmbBrand.SelectedIndex
        End If
    End Sub

    Private Sub chk_ResidenceInn_Clicked(sender As Object, e As EventArgs) Handles chk_ResidenceInn.Click
        If Not clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_ResidenceInn__UI) Then
            ToggleErrorPopup("Residence Inn theme not found!")

            chk_ResidenceInn.Checked = False

            Exit Sub
        End If

        If Not chk_ResidenceInn.Checked Then
            chk_ResidenceInn.Checked = True

            chk_Hilton.Checked = False
            chk_GuestTek.Checked = False
            chk_iBAHN.Checked = False
            chk_Marriott.Checked = False
            chk_Towneplace.Checked = False
            chk_HotelArts.Checked = False

            cmbBrand.Enabled = False

            clsGlobals.oHotelBrand.BrandGuestTek = False
            clsGlobals.oHotelBrand.BrandiBAHN = False
            clsGlobals.oHotelBrand.BrandResidenceInn = True
            clsGlobals.oHotelBrand.BrandMarriott = False
            clsGlobals.oHotelBrand.BrandTowneplace = False
            clsGlobals.oHotelBrand.BrandHotelArts = False
            clsGlobals.oHotelBrand.BrandNumber = 668
        End If
    End Sub

    Private Sub chk_Marriott_Clicked(sender As Object, e As EventArgs) Handles chk_Marriott.Click
        If Not clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Marriott__UI) Then
            ToggleErrorPopup("Marriott theme not found!")

            chk_Marriott.Checked = False

            Exit Sub
        End If

        If Not chk_Marriott.Checked Then
            chk_Marriott.Checked = True

            chk_Hilton.Checked = False
            chk_GuestTek.Checked = False
            chk_iBAHN.Checked = False
            chk_ResidenceInn.Checked = False
            chk_Towneplace.Checked = False
            chk_HotelArts.Checked = False

            cmbBrand.Enabled = False

            clsGlobals.oHotelBrand.BrandGuestTek = False
            clsGlobals.oHotelBrand.BrandiBAHN = False
            clsGlobals.oHotelBrand.BrandResidenceInn = False
            clsGlobals.oHotelBrand.BrandMarriott = True
            clsGlobals.oHotelBrand.BrandTowneplace = False
            clsGlobals.oHotelBrand.BrandHotelArts = False
            clsGlobals.oHotelBrand.BrandNumber = 669
        End If
    End Sub

    Private Sub chk_Towneplace_Clicked(sender As Object, e As EventArgs) Handles chk_Towneplace.Click
        If Not clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_Towneplace__UI) Then
            ToggleErrorPopup("Towneplace theme not found!")

            chk_Towneplace.Checked = False

            Exit Sub
        End If

        If Not chk_Towneplace.Checked Then
            chk_Towneplace.Checked = True

            chk_Hilton.Checked = False
            chk_GuestTek.Checked = False
            chk_iBAHN.Checked = False
            chk_ResidenceInn.Checked = False
            chk_Marriott.Checked = False
            chk_HotelArts.Checked = False

            cmbBrand.Enabled = False

            clsGlobals.oHotelBrand.BrandGuestTek = False
            clsGlobals.oHotelBrand.BrandiBAHN = False
            clsGlobals.oHotelBrand.BrandResidenceInn = False
            clsGlobals.oHotelBrand.BrandMarriott = False
            clsGlobals.oHotelBrand.BrandTowneplace = True
            clsGlobals.oHotelBrand.BrandHotelArts = False
            clsGlobals.oHotelBrand.BrandNumber = 670
        End If
    End Sub

    Private Sub chk_HotelArts_Clicked(sender As Object, e As EventArgs) Handles chk_HotelArts.Click
        If Not clsGlobals._DoesSiteKioskFileExist(clsGlobals.c_Path_HotelArts__UI) Then
            ToggleErrorPopup("HotelArts theme not found!")

            chk_HotelArts.Checked = False

            Exit Sub
        End If

        If Not chk_HotelArts.Checked Then
            chk_HotelArts.Checked = True

            chk_Hilton.Checked = False
            chk_GuestTek.Checked = False
            chk_iBAHN.Checked = False
            chk_ResidenceInn.Checked = False
            chk_Marriott.Checked = False
            chk_Towneplace.Checked = False


            cmbBrand.Enabled = False

            clsGlobals.oHotelBrand.BrandGuestTek = False
            clsGlobals.oHotelBrand.BrandiBAHN = False
            clsGlobals.oHotelBrand.BrandResidenceInn = False
            clsGlobals.oHotelBrand.BrandMarriott = False
            clsGlobals.oHotelBrand.BrandTowneplace = False
            clsGlobals.oHotelBrand.BrandHotelArts = True
            clsGlobals.oHotelBrand.BrandNumber = 671
        End If
    End Sub


    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        If AnyChanges() Then
            ToggleChangeConfirmPopup(True)
        Else
            btnYesCancel.PerformClick()
        End If
    End Sub

    Private Sub btnYesCancel_Click(sender As Object, e As EventArgs) Handles btnYesCancel.Click
        Me.Close()
    End Sub

    Private Sub btnYesSaveAnyway_Click(sender As Object, e As EventArgs) Handles btnYesSaveAnyway.Click
        PrepareToSaveAllTheStuff()

        SaveAllTheStuff()
    End Sub

    Private Sub btnNoReturnToApp_Click(sender As Object, e As EventArgs) Handles btnNoReturnToApp.Click
        ToggleChangeConfirmPopup(False)
    End Sub

    Private Sub btnNoReturnToApp2_Click(sender As Object, e As EventArgs) Handles btnNoReturnToApp2.Click
        ToggleNoChangesConfirmPopup(False)
    End Sub



    Private Function AnyChanges() As Boolean
        If clsGlobals.g_AnyChangesFunctionDisabled Then
            Return False
        End If

        clsGlobals.g_Address_Site__Changed = (clsGlobals.g_Address_Site <> txtHotelAddress.Text)
        clsGlobals.g_Url_Hotel__Changed = (clsGlobals.g_Url_Hotel <> txtHotelinformation.Text)
        clsGlobals.g_Url_Internet__Changed = (clsGlobals.g_Url_Internet <> txtInternet.Text)
        clsGlobals.g_Url_Map__Changed = (clsGlobals.g_Url_Map <> txtMap.Text)
        clsGlobals.g_Url_Weather__Changed = (clsGlobals.g_Url_Weather <> txtWeather.Text)
        clsGlobals.g_Support_PhoneNumber__Changed = (clsGlobals.g_Support_PhoneNumber <> txtSupportNumber.Text)
        clsGlobals.g_Brand__Changed = (clsGlobals.g_Brand <> clsGlobals.oHotelBrand.BrandNumber)
        clsGlobals.g_PaymentEnabled__Changed = (clsGlobals.g_PaymentEnabled <> chk_SiteCash.Checked)
        clsGlobals.g_PasswordEnabled__Changed = (clsGlobals.g_PasswordEnabled <> chk_Password.Checked)
        clsGlobals.g_Password__Changed = (clsGlobals.g_Password <> txtPassword.Text)

        'Dim sTmp As String = ""

        'sTmp &= clsGlobals.g_Address_Site__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Url_Hotel__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Url_Internet__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Url_Map__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Url_Weather__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Support_PhoneNumber__Changed.ToString & " "
        'sTmp &= clsGlobals.g_Brand__Changed.ToString & " "
        'sTmp &= clsGlobals.g_PaymentEnabled__Changed.ToString & " "

        'clsGlobals.oLogger.WriteToLog("You changed:", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        'clsGlobals.oLogger.WriteToLog(sTmp, Logger.MESSAGE_TYPE.LOG_DEBUG, 2)


        'sTmp = vbCrLf
        'sTmp &= "'" & clsGlobals.g_Address_Site.ToString & "' <> '" & txtHotelAddress.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Url_Hotel.ToString & "' <> '" & txtHotelinformation.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Url_Internet.ToString & "' <> '" & txtInternet.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Url_Map.ToString & "' <> '" & txtMap.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Url_Weather.ToString & "' <> '" & txtWeather.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Support_PhoneNumber.ToString & "' <> '" & txtSupportNumber.Text.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_Brand.ToString & "' <> '" & clsGlobals.oHotelBrand.BrandNumber.ToString & "'" & vbCrLf
        'sTmp &= "'" & clsGlobals.g_PaymentEnabled.ToString & "' <> '" & chk_SiteCash.Checked.ToString & "'" & vbCrLf

        'clsGlobals.oLogger.WriteToLog("You changed:", Logger.MESSAGE_TYPE.LOG_DEBUG, 1)
        'clsGlobals.oLogger.WriteToLog(sTmp, Logger.MESSAGE_TYPE.LOG_DEBUG, 2)



        Return clsGlobals.g_Address_Site__Changed Or
            clsGlobals.g_Url_Hotel__Changed Or
            clsGlobals.g_Url_Internet__Changed Or
            clsGlobals.g_Url_Map__Changed Or
            clsGlobals.g_Url_Weather__Changed Or
            clsGlobals.g_Support_PhoneNumber__Changed Or
            clsGlobals.g_Brand__Changed Or
            clsGlobals.g_PaymentEnabled__Changed Or
            clsGlobals.g_PasswordEnabled__Changed Or
            clsGlobals.g_Password__Changed
    End Function

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Not AnyChanges() Then
            ToggleNoChangesConfirmPopup(True)
        Else
            PrepareToSaveAllTheStuff()

            SaveAllTheStuff()
        End If
    End Sub

    Private Sub PrepareToSaveAllTheStuff()
        clsGlobals.g_AnyChangesFunctionDisabled = True

        clsGlobals.g_Address_Site = txtHotelAddress.Text
        clsGlobals.g_Url_Hotel = txtHotelinformation.Text
        clsGlobals.g_Url_Internet = txtInternet.Text
        clsGlobals.g_Url_Map = txtMap.Text
        clsGlobals.g_Url_Weather = txtWeather.Text
        clsGlobals.g_Support_PhoneNumber = txtSupportNumber.Text
        clsGlobals.g_PaymentEnabled = chk_SiteCash.Checked
        clsGlobals.g_PasswordEnabled = chk_Password.Checked
        clsGlobals.g_Password = txtPassword.Text
    End Sub

    Private Sub SaveAllTheStuff()
        UpdateSaveProgress("")

        ToggleControls(False)
        ToggleWaitPopup(True)

        bgwSave.RunWorkerAsync()
    End Sub

    Private Sub DoneSavingAllTheStuff()
        DoneBye()
    End Sub

    Private Sub bgwSave_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwSave.DoWork
        clsGlobals.oLogger.WriteToLog("Saving config", , 0)


        '------------------------------------------------------
        UpdateSaveProgress("ui config")

        clsGlobals.oLogger.WriteToLog("Update global config", , 1)
        clsGlobalSettings.WriteGlobalSettings()

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("password config")

        clsGlobals.oLogger.WriteToLog("Update password config", , 1)
        clsGlobalSettings.WritePassword()

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("support phone number")
        clsGlobals.oLogger.WriteToLog("Update support phone number", , 1)
        If clsGlobals.g_Support_PhoneNumber__Changed Then
            clsGlobalSettings.WritePhoneNumberToLanguageFiles()
        Else
            clsGlobals.oLogger.WriteToLog("no change, skipped", , 2)
        End If

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("sitekiosk config")
        clsGlobals.oLogger.WriteToLog("Update SiteKiosk config", , 1)
        'clsGlobals.oSkCfg = New SkCfg
        clsGlobals.oSkCfg.BackupFolder = clsGlobals.g_BackupDirectory

        clsGlobals.oLogger.WriteToLog("Prepare url list", , 2)
        If clsGlobals.oSkCfg.MakeListOfUrls Then
            clsGlobals.oLogger.WriteToLog("ok", , 3)
        Else
            clsGlobals.oLogger.WriteToLog("OOPS!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If

        clsGlobals.oLogger.WriteToLog("Update skcfg file", , 2)
        clsGlobals.oSkCfg.Update(True)

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("payment dialog")
        'Update payment dialog
        Try
            clsGlobals.oLogger.WriteToLog("Update payment dialog", , 1)


            clsGlobals.oLogger.WriteToLog("copying", , 2)
            If clsGlobals.g_PasswordEnabled Then
                'Password, use the Hilton one
                clsGlobals.oLogger.WriteToLog("using the Hilton payment dialog, for the password thing", , 3)
                If IO.File.Exists(clsGlobals.c_Path_Hilton__paymentdialog) Then
                    IO.File.Copy(clsGlobals.c_Path_Hilton__paymentdialog, clsGlobals.c_Path_SK__paymentdialog, True)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Hilton__paymentdialog & " => " & clsGlobals.c_Path_SK__paymentdialog, , 4)
                Else
                    clsGlobals.oLogger.WriteToLog("missing file!", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Hilton__paymentdialog, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                End If
            ElseIf clsGlobals.oHotelBrand.BrandResidenceInn Then
                'ResidenceInn
                If IO.File.Exists(clsGlobals.c_Path_ResidenceInn__paymentdialog) Then
                    IO.File.Copy(clsGlobals.c_Path_ResidenceInn__paymentdialog, clsGlobals.c_Path_SK__paymentdialog, True)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_ResidenceInn__paymentdialog & " => " & clsGlobals.c_Path_SK__paymentdialog, , 3)
                Else
                    clsGlobals.oLogger.WriteToLog("missing file!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_ResidenceInn__paymentdialog, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            ElseIf clsGlobals.oHotelBrand.BrandHilton Then
                'Hilton
                If IO.File.Exists(clsGlobals.c_Path_Hilton__paymentdialog) Then
                    IO.File.Copy(clsGlobals.c_Path_Hilton__paymentdialog, clsGlobals.c_Path_SK__paymentdialog, True)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Hilton__paymentdialog & " => " & clsGlobals.c_Path_SK__paymentdialog, , 3)
                Else
                    clsGlobals.oLogger.WriteToLog("missing file!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Hilton__paymentdialog, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            Else
                'normal
                If IO.File.Exists(clsGlobals.c_Path_Default__paymentdialog) Then
                    IO.File.Copy(clsGlobals.c_Path_Default__paymentdialog, clsGlobals.c_Path_SK__paymentdialog, True)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Default__paymentdialog & " => " & clsGlobals.c_Path_SK__paymentdialog, , 3)
                Else
                    clsGlobals.oLogger.WriteToLog("missing file!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                    clsGlobals.oLogger.WriteToLog(clsGlobals.c_Path_Default__paymentdialog, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                End If
            End If
        Catch ex As Exception
            clsGlobals.oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            clsGlobals.oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Threading.Thread.Sleep(250)


        '------------------------------------------------------
        UpdateSaveProgress("done")

        Threading.Thread.Sleep(250)
    End Sub

    Private Sub bgwSave_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwSave.ProgressChanged
        'Yeah! Progress!
    End Sub

    Private Sub bgwSave_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwSave.RunWorkerCompleted
        DoneSavingAllTheStuff()
    End Sub

    Private Sub DoneBye()
        'done
        clsGlobals.oLogger.WriteToLog("Done", , 0)
        clsGlobals.oLogger.WriteToLog("bye", , 1)

        Me.Close()
    End Sub

    Private Sub btnDisabledClose_Click(sender As Object, e As EventArgs) Handles btnDisabledClose.Click
        Me.Close()
    End Sub

    Private Sub cmbBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbBrand.SelectedIndexChanged
        clsGlobals.oHotelBrand.BrandNumber = cmbBrand.SelectedIndex
    End Sub

    Private Sub btnInternet_Click(sender As Object, e As EventArgs) Handles btnInternet.Click
        TestUrl(txtInternet.Text)
    End Sub

    Private Sub btnHotelInformation_Click(sender As Object, e As EventArgs) Handles btnHotelInformation.Click
        TestUrl(txtHotelinformation.Text)
    End Sub

    Private Sub btnWeather_Click(sender As Object, e As EventArgs) Handles btnWeather.Click
        TestUrl(txtWeather.Text)
    End Sub

    Private Sub btnMap_Click(sender As Object, e As EventArgs) Handles btnMap.Click
        TestUrl(txtMap.Text)
    End Sub

    Private Sub TestUrl(sUrl As String)
        Try
            If (sUrl IsNot Nothing) AndAlso (sUrl.StartsWith("http://") Or sUrl.StartsWith("https://")) Then
                System.Diagnostics.Process.Start(sUrl)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub tmrChanges_Tick(sender As Object, e As EventArgs) Handles tmrChanges.Tick
        'UpdateSaveButton()
    End Sub

    Private Sub UpdateSaveButton()
        Dim tmpFont As Font

        clsGlobals.g_AnyChangesDetected = AnyChanges()

        If clsGlobals.g_AnyChangesDetected Then
            tmpFont = New Font(btnSave.Font.Name, 16, FontStyle.Bold)

            btnSave.ForeColor = Color.DarkRed
        Else
            tmpFont = New Font(btnSave.Font.Name, 12, FontStyle.Regular)
            btnSave.ForeColor = Color.Black
        End If

        btnSave.Font = tmpFont

        tmpFont.Dispose()
    End Sub

    Private Sub txtSupportNumber_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSupportNumber.KeyPress
        '    If Not (Asc(e.KeyChar) = 8) Then
        '        Dim allowedChars As String = "0123456789 :-()+"

        '        If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
        '            e.KeyChar = ChrW(0)
        '            e.Handled = True
        '        End If
        '    End If
    End Sub

    Private Sub btnCloseError_Click(sender As Object, e As EventArgs) Handles btnCloseError.Click
        ToggleErrorPopup("")
    End Sub

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        Dim s As String = ""

        s = "This is a random test message: " & Helpers.Types.RandomString(20)

        ToggleErrorPopup(s)
    End Sub

    Private Sub txtPassword_TextChanged(sender As Object, e As EventArgs) Handles txtPassword.TextChanged

    End Sub

    Private Sub txtPassword_MouseHover(sender As Object, e As EventArgs) Handles txtPassword.MouseHover
        txtPassword.PasswordChar = ""
    End Sub

    Private Sub txtPassword_MouseLeave(sender As Object, e As EventArgs) Handles txtPassword.MouseLeave
        txtPassword.PasswordChar = "*"
    End Sub


    Private Sub chk_SiteCash_Click(sender As Object, e As EventArgs) Handles chk_SiteCash.Click
        If chk_SiteCash.Checked Then
            chk_Password.Checked = False
            txtPassword.Enabled = False
        End If
    End Sub

    Private Sub chk_Password_Click(sender As Object, e As EventArgs) Handles chk_Password.Click
        If chk_Password.Checked Then
            chk_SiteCash.Checked = False
        End If

        txtPassword.Enabled = chk_Password.Checked
    End Sub
End Class
