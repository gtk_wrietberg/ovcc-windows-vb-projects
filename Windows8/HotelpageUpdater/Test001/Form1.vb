﻿Imports System.Globalization
Imports System.Text.RegularExpressions

Public Class Form1
    Public Shared Function Func1(ByRef s As String) As Boolean
        Return Func2(s)
    End Function

    Public Shared Function Func2(ByRef s As String) As Boolean
        Return Func3(s)
    End Function

    Public Shared Function Func3(ByRef s As String) As Boolean
        s = "_" & s & "-"
        Return True
    End Function


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox2.Text = Web.HttpUtility.JavaScriptStringEncode(TextBox1.Text)

        TextBox3.Text = JavaScriptStringDecode(TextBox2.Text)
    End Sub

    Public Shared Function JavaScriptStringDecode(source As String) As String
        ' Replace some chars.
        Dim decoded = source.Replace("\'", "'").Replace("\""", """").Replace("\/", "/").Replace("\\", "\").Replace("\t", vbTab).Replace("\r\n", vbCrLf).Replace("\r", vbCr).Replace("\n", vbLf)

        ' Replace unicode escaped text.
        Dim rx = New Regex("\\[uU]([0-9A-F]{4})")
        Dim evaluator As MatchEvaluator = AddressOf ASCII_to_CHAR

        decoded = rx.Replace(decoded, evaluator)

        Return decoded
    End Function

    Private Shared Function ASCII_to_CHAR(match As Match) As String

        Return ChrW(Int32.Parse(match.Value.Substring(2), NumberStyles.HexNumber)).ToString(CultureInfo.InvariantCulture)
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        MsgBox(Helpers.Types.CastStringToInteger_safe("44;"))
    End Sub

    Private Sub TextBox1_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TextBox4.KeyPress
        If Not (Asc(e.KeyChar) = 8) Then
            Dim allowedChars As String = "0123456789 :-()+"

            If Not allowedChars.Contains(e.KeyChar.ToString.ToLower) Then
                e.KeyChar = ChrW(0)
                e.Handled = True
            End If
        End If

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim s As String = ":;:"
        Dim s2 As String = Helpers.XOrObfuscation.Obfuscate(s)
        Dim s3 As String = Helpers.XOrObfuscation.Deobfuscate(s2)

        MsgBox(s & vbCrLf & s2 & vbCrLf & s3)
        MsgBox(Helpers.XOrObfuscation.LastError)

    End Sub

    Private Function RetrieveLinkerTimestamp(ByVal filePath As String) As DateTime
        Const PeHeaderOffset As Integer = 60
        Const LinkerTimestampOffset As Integer = 8

        Dim b(2047) As Byte
        'Dim s As IO.Stream
        Dim s As New Object

        Try
            s = New IO.FileStream(filePath, IO.FileMode.Open, IO.FileAccess.Read)
            s.Read(b, 0, 2048)
        Finally
            If Not s Is Nothing Then s.Close()
        End Try

        Dim i As Integer = BitConverter.ToInt32(b, PeHeaderOffset)

        Dim SecondsSince1970 As Integer = BitConverter.ToInt32(b, i + LinkerTimestampOffset)
        Dim dt As New DateTime(1970, 1, 1, 0, 0, 0)
        dt = dt.AddSeconds(SecondsSince1970)
        dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours)

        Return dt
    End Function

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim s As String = ""

        s &= Application.ExecutablePath & vbCrLf
        s &= RetrieveLinkerTimestamp(Application.ExecutablePath).ToString & vbCrLf
        s &= Helpers.FilesAndFolders.GetFileCompileDate(Application.ExecutablePath) & vbCrLf

        MsgBox(s)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim s As String = ""

        s = TextBox5.Text

        MsgBox(s)


        's = Web.HttpUtility.HtmlEncode(s)

        'MsgBox(s)


        's = Web.HttpUtility.HtmlDecode(s)

        'MsgBox(s)

        s = Helpers.Generic.HtmlEncode(s)
        MsgBox(s)

        s = Helpers.Generic.HtmlDecode(s)
        MsgBox(s)

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim q As String = "poep"
        Dim b As Boolean = False

        b = Func1(q)
        MsgBox("b=" & b & " - q=" & q)

        b = Func1(q)
        MsgBox("b=" & b & " - q=" & q)

        b = Func1(q)
        MsgBox("b=" & b & " - q=" & q)

        b = Func1(q)
        MsgBox("b=" & b & " - q=" & q)
    End Sub
End Class
