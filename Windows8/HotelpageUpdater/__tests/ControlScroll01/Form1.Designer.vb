﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlInner = New System.Windows.Forms.Panel()
        Me.pnlOuter = New System.Windows.Forms.Panel()
        Me.VScrollBar1 = New System.Windows.Forms.VScrollBar()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pnlOuter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlInner
        '
        Me.pnlInner.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.pnlInner.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInner.Location = New System.Drawing.Point(3, 3)
        Me.pnlInner.Name = "pnlInner"
        Me.pnlInner.Size = New System.Drawing.Size(319, 256)
        Me.pnlInner.TabIndex = 0
        '
        'pnlOuter
        '
        Me.pnlOuter.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.pnlOuter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlOuter.Controls.Add(Me.VScrollBar1)
        Me.pnlOuter.Controls.Add(Me.pnlInner)
        Me.pnlOuter.Location = New System.Drawing.Point(91, 24)
        Me.pnlOuter.Name = "pnlOuter"
        Me.pnlOuter.Size = New System.Drawing.Size(354, 265)
        Me.pnlOuter.TabIndex = 1
        '
        'VScrollBar1
        '
        Me.VScrollBar1.Location = New System.Drawing.Point(327, 0)
        Me.VScrollBar1.Maximum = 10
        Me.VScrollBar1.Name = "VScrollBar1"
        Me.VScrollBar1.Size = New System.Drawing.Size(22, 264)
        Me.VScrollBar1.TabIndex = 2
        '
        'TreeView1
        '
        Me.TreeView1.CheckBoxes = True
        Me.TreeView1.FullRowSelect = True
        Me.TreeView1.Indent = 5
        Me.TreeView1.Location = New System.Drawing.Point(519, 28)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(310, 246)
        Me.TreeView1.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(495, 430)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(203, 103)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(1126, 622)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TreeView1)
        Me.Controls.Add(Me.pnlOuter)
        Me.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.pnlOuter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlInner As Panel
    Friend WithEvents pnlOuter As Panel
    Friend WithEvents VScrollBar1 As VScrollBar
    Friend WithEvents TreeView1 As TreeView
    Friend WithEvents Button1 As Button
End Class
