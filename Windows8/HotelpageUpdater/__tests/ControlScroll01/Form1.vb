﻿Public Class Form1
    Private ReadOnly c_OSIDFHSDIOFHSDO As Integer = 100


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        pnlInner.AutoSize = True 'set the InnerPanel`s AutoSize to True so that it will resize itself when controls are added to it
        pnlInner.AutoSizeMode = AutoSizeMode.GrowAndShrink 'set its AutoSizeMode so that it will auto size itself bigger or smaller when controls are added or removed
        pnlInner.Location = New Point(0, 0)
        pnlInner.BorderStyle = BorderStyle.None
        pnlInner.BackColor = pnlOuter.BackColor
        VScrollBar1.Left = pnlOuter.Width - VScrollBar1.Width

        'here i am just adding a bunch of labels to the InnerPanel for this example. This will make the InnerPanel AutoSize to fit all the Labels

        TreeView1.ShowLines = False
        TreeView1.CheckBoxes = True

        Dim MyColors() As Color = {Color.Black, Color.Blue, Color.Red, Color.Green, Color.Aqua}
        Dim MyColorCount As Integer = 0

        Dim _r As New Random()
        For i As Integer = 0 To c_OSIDFHSDIOFHSDO - 1


            If _r.Next(2) = 0 Then
                Dim lbl As New Label

                lbl.AutoSize = True
                lbl.Text = "Label " & i.ToString
                'lbl.Location = New Point(6, i * lbl.Height)
                lbl.Location = New Point(6, i * 20)

                pnlInner.Controls.Add(lbl)
            Else
                Dim chk As New CheckBox

                chk.AutoSize = True
                chk.Text = "Checkbox " & i.ToString
                'chk.Location = New Point(6, i * chk.Height)
                chk.Location = New Point(6, i * 20)

                pnlInner.Controls.Add(chk)
            End If




            Dim NewNode As TreeNode = Me.TreeView1.Nodes.Add("Node" & i.ToString)

            NewNode.ForeColor = MyColors(MyColorCount)

            If MyColorCount = 1 Or MyColorCount = 3 Or i < 10 Then
                NewNode.NodeFont = New Font(TreeView1.Font, FontStyle.Strikeout)
                'Else
                '    NewNode.NodeFont = TreeView1.Font
            End If

            MyColorCount += 1
            If MyColorCount >= MyColors.Length Then
                MyColorCount = 0
            End If
        Next
    End Sub

    'use the InnerPanel`s SizeChanged event to reset the Maximum property of the VScrollBar when the InnerPanel`s Size is changed
    Private Sub InnerPanel_SizeChanged(sender As Object, e As EventArgs) Handles pnlInner.SizeChanged
        Dim max As Integer = pnlInner.Height - pnlOuter.ClientSize.Height

        If max > 0 Then
            VScrollBar1.Enabled = True
            VScrollBar1.Maximum = max + SystemInformation.VerticalScrollBarArrowHeight
        Else
            VScrollBar1.Enabled = False
        End If
    End Sub

    'use the VScrollBar`s ValueChanged event to set the location of the InnerPanel to the negative of the VScrollBar`s Value
    Private Sub VScrollBar1_ValueChanged(sender As Object, e As EventArgs) Handles VScrollBar1.ValueChanged
        pnlInner.Top = -VScrollBar1.Value
    End Sub

    Private _event_skip__Check As Boolean = False
    Private _event_skip__Select As Boolean = False

    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterSelect
        If _event_skip__Select Then
            Exit Sub
        End If

        Console.WriteLine("TreeView1_AfterSelect: " & TreeView1.SelectedNode.Text)
        TreeView1.SelectedNode.Checked = True
        TreeView1.SelectedNode.EnsureVisible()
    End Sub

    Private Sub TreeView1_AfterCheck(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterCheck
        If _event_skip__Check Then
            Exit Sub
        End If

        _event_skip__Check = True

        If e.Node.Checked Then
            For Each _node As TreeNode In e.Node.TreeView.Nodes
                If _node.Equals(e.Node) Then
                    _event_skip__Select = True
                    TreeView1.SelectedNode = _node
                    _event_skip__Select = False
                Else
                    _node.Checked = False
                End If
            Next
        End If

        _event_skip__Check = False

        Console.WriteLine("TreeView1_AfterCheck: " & TreeView1.SelectedNode.Text)
    End Sub

    Private Sub TreeView1_BeforeSelect(sender As Object, e As TreeViewCancelEventArgs) Handles TreeView1.BeforeSelect
        Try
            If Not e.Node.NodeFont Is Nothing Then
                If e.Node.NodeFont.Strikeout Then
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub TreeView1_BeforeCheck(sender As Object, e As TreeViewCancelEventArgs) Handles TreeView1.BeforeCheck
        Try
            If Not e.Node.NodeFont Is Nothing Then
                If e.Node.NodeFont.Strikeout Then
                    e.Cancel = True
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim _r As New Random()
        Dim _i As Integer = _r.Next(c_OSIDFHSDIOFHSDO)
        Dim _b As Boolean = False

        _b = _SelectNode(_i)

        Console.WriteLine("_SelectNode(" & _i.ToString & ")=" & _b.ToString)

        If Not _b Then
            _i = _FindFirstAllowedNode()

            If _i >= 0 Then
                _b = _SelectNode(_i)
                Console.WriteLine("_SelectNode(" & _i.ToString & ")=" & _b.ToString)
            Else
                Console.WriteLine("_FindFirstAllowedNode()=" & _i.ToString)
            End If
        End If
    End Sub

    Private Function _SelectNode(_index As Integer) As Boolean
        Dim _ret As Boolean = True

        For Each _node As TreeNode In TreeView1.Nodes
            If _node.Index = _index Then
                If Not _node.NodeFont Is Nothing Then
                    If _node.NodeFont.Strikeout Then
                        _ret = False
                    End If
                End If

                TreeView1.SelectedNode = _node

                Exit For
            End If
        Next

        Return _ret
    End Function

    Private Function _FindFirstAllowedNode() As Integer
        Dim _ret As Integer = -1

        For Each _node As TreeNode In TreeView1.Nodes
            If Not _node.NodeFont Is Nothing Then
                If Not _node.NodeFont.Strikeout Then
                    _ret = _node.Index

                    Exit For
                End If
            Else
                _ret = _node.Index

                Exit For
            End If
        Next

        Return _ret
    End Function
End Class
