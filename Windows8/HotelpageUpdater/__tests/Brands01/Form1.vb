﻿Public Class Form1
    Private mI As Integer = 0

    Private ReadOnly c_UI_Margin_Left__Checkbox As Integer = 10
    Private ReadOnly c_UI_Margin_Top__Checkbox As Integer = 10
    Private ReadOnly c_UI_Size_Width__Checkbox As Integer = 100
    Private ReadOnly c_UI_Size_Height__Checkbox As Integer = 20





    <Runtime.InteropServices.DllImport("user32")>
    Public Shared Function ShowScrollBar(ByVal hWnd As System.IntPtr, ByVal wBar As Integer, ByVal bShow As Boolean) As Boolean
    End Function

    Private Const SB_HOR As Integer = 0
    Private Const SB_VERT As Integer = 1
    Private Const SB_CTL As Integer = 2
    Private Const SB_BOTH As Integer = 3




    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'chkListBox_Themes.DrawToBitmap()


        _LoadThemes()
        _FillThemeListBox()
    End Sub

    Private Sub ____SELECTRANDOMTHEME()
        Dim _r As New Random()
        Dim _i As Integer = 0

        _i = _r.Next(0, HotelBrands.Count)


        For _index As Integer = 0 To chkListBox_Themes.Items.Count - 1
            If _index = _i Then
                chkListBox_Themes.SetItemChecked(_index, True)
            Else
                chkListBox_Themes.SetItemChecked(_index, False)
            End If
        Next

        chkListBox_Themes.SelectedIndex = _i
        chkListBox_Themes.TopIndex = _i
    End Sub

    Private Sub _LoadThemes()
        Using strReader As New IO.StringReader(My.Resources.ovcc_themes)
            Using textparser As New FileIO.TextFieldParser(strReader)
                textparser.TextFieldType = FileIO.FieldType.Delimited
                textparser.Delimiters = {","}
                textparser.HasFieldsEnclosedInQuotes = True

                While Not textparser.EndOfData
                    Try
                        Dim curRow As String() = textparser.ReadFields()

                        If curRow.Count <> 7 Then
                            Throw New FileIO.MalformedLineException("incorrect number of fields (" & curRow.Count.ToString & " instead of 7")
                        End If


                        Dim _brand As New HotelBrands.HotelBrand

                        _brand.BrandIndex = CInt(curRow(0))
                        _brand.Name = curRow(1)
                        _brand.Path_Theme = curRow(2)
                        _brand.Path_Screensaver = curRow(3)
                        _brand.Path_Logout = curRow(4)
                        _brand.Path_PaymentDialog = curRow(5)

                        If curRow(6).Equals("True") Then
                            _brand.PaymentEnabled = True
                        Else
                            _brand.PaymentEnabled = False
                        End If

                        HotelBrands.Add(_brand)
                    Catch ex As FileIO.MalformedLineException
                        Stop
                    End Try
                End While
            End Using
        End Using
    End Sub

    Private Sub _FillThemeListBox()
        chkListBox_Themes.Items.Clear()

        For Each _brand As HotelBrands.HotelBrand In HotelBrands.GetBrands
            chkListBox_Themes.Items.Add(_brand.Name)
        Next
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        'For idx = 0 To chkListBox_Themes.Items.Count - 1
        '    If chkListBox_Themes.GetItemChecked(idx) Then

        '        MsgBox(HotelBrands.Find(chkListBox_Themes.GetItemText(chkListBox_Themes.Items(idx))).ToString)
        '        'MsgBox(chkListBox_Themes.GetItemText(chkListBox_Themes.Items(idx)))
        '    End If
        'Next

        MsgBox(HotelBrands.Find(chkListBox_Themes.GetItemText(chkListBox_Themes.Items(chkListBox_Themes.SelectedIndex))).ToString)
    End Sub

    Private Sub chkListBox_Themes_Click(sender As Object, e As EventArgs) Handles chkListBox_Themes.Click
        Dim idx, sidx As Integer

        sidx = chkListBox_Themes.SelectedIndex

        For idx = 0 To chkListBox_Themes.Items.Count - 1
            If idx <> sidx Then
                chkListBox_Themes.SetItemChecked(idx, False)
            Else
                chkListBox_Themes.SetItemChecked(sidx, True)
            End If
        Next
    End Sub




    Private Sub Button2_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ____SELECTRANDOMTHEME()
    End Sub



End Class
