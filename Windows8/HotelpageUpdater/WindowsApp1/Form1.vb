﻿Public Class Form1
    Private mPnlLocTop As Integer = 0
    Private mPnlLocLeft As Integer = 0

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mPnlLocTop = (pnlRed.Top + pnlRed.Height) / 2
        mPnlLocLeft = (pnlRed.Left + pnlRed.Width) / 2

        pnlBLue.Height = pnlGreen.Height
        pnlBLue.Width = pnlGreen.Width
    End Sub


    Private mActive As Integer = 0
    Public Sub SwitchBlueGreen()
        If mActive = 0 Then
            pnlGreen.Top = mPnlLocTop
            pnlGreen.Left = mPnlLocLeft

            pnlBLue.Top = pnlGreen.Top + 4
            pnlBLue.Left = pnlGreen.Left + 4

            pnlBLue.BringToFront()
            pnlGreen.BringToFront()
        Else
            pnlBLue.Top = mPnlLocTop
            pnlBLue.Left = mPnlLocLeft

            pnlGreen.Top = pnlBLue.Top + 4
            pnlGreen.Left = pnlBLue.Left + 4

            pnlGreen.BringToFront()
            pnlBLue.BringToFront()
        End If

        mActive = 1 - mActive
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        SwitchBlueGreen()
    End Sub
End Class
