﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.pnlRed = New System.Windows.Forms.Panel()
        Me.pnlBLue = New System.Windows.Forms.Panel()
        Me.pnlGreen = New System.Windows.Forms.Panel()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(37, 98)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(208, 76)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'pnlRed
        '
        Me.pnlRed.BackColor = System.Drawing.Color.Red
        Me.pnlRed.Location = New System.Drawing.Point(353, 55)
        Me.pnlRed.Name = "pnlRed"
        Me.pnlRed.Size = New System.Drawing.Size(294, 269)
        Me.pnlRed.TabIndex = 1
        '
        'pnlBLue
        '
        Me.pnlBLue.BackColor = System.Drawing.Color.Blue
        Me.pnlBLue.Location = New System.Drawing.Point(157, 327)
        Me.pnlBLue.Name = "pnlBLue"
        Me.pnlBLue.Size = New System.Drawing.Size(164, 127)
        Me.pnlBLue.TabIndex = 2
        '
        'pnlGreen
        '
        Me.pnlGreen.BackColor = System.Drawing.Color.Lime
        Me.pnlGreen.Location = New System.Drawing.Point(128, 180)
        Me.pnlGreen.Name = "pnlGreen"
        Me.pnlGreen.Size = New System.Drawing.Size(152, 93)
        Me.pnlGreen.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(739, 509)
        Me.Controls.Add(Me.pnlGreen)
        Me.Controls.Add(Me.pnlBLue)
        Me.Controls.Add(Me.pnlRed)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents pnlRed As Panel
    Friend WithEvents pnlBLue As Panel
    Friend WithEvents pnlGreen As Panel
End Class
