﻿Imports System.IO

Module modMain
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        SetSystemDriveInSystemSecurity()


        '------------------------------------------------------
        oLogger.WriteToLog("Apply security settings", , 0)
        ApplyDefaultSecurity()


        '---------
        oLogger.WriteToLog("reboot", , 0)
        WindowsController.ExitWindows(RestartOptions.Reboot, True)


        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub

    Public Sub ApplyDefaultSecurity()
        oLogger.WriteToLogRelative("start security manager", , 1)
        oLogger.WriteToLogRelative(g_SystemSecurityManagerFile & " /applydefault", , 2)

        Dim watch As Stopwatch = Stopwatch.StartNew()

        Dim p As Process = Process.Start(g_SystemSecurityManagerFile, "/applydefault")

        oLogger.WriteToLogRelative("pid: " & p.Id.ToString, , 2)

        If Not p.WaitForExit(30000) Then
            oLogger.WriteToLogRelative("TIMEOUT!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End If

        watch.Stop()
        oLogger.WriteToLogRelative("elapsed time: " & watch.ElapsedMilliseconds.ToString & " ms", , 2)
    End Sub

    Public Sub SetSystemDriveInSystemSecurity()
        Try
            Dim sSwConfig As String = ""

            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(g_SwConfigFile, , 3)
            xmlSkCfg.Load(g_SwConfigFile)
            oLogger.WriteToLogRelative("ok", , 4)


            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("config")

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("config")
            End If


            '*************************************************************************************************************
            oLogger.WriteToLogRelative("Update drives", , 2)
            Dim xmlNode_Drives As Xml.XmlNode
            Dim xmlNodeList_Drives As Xml.XmlNodeList
            Dim xmlNode_Drive As Xml.XmlNode
            Dim xmlNode_DriveClone As Xml.XmlNode
            Dim xmlNode_DriveCloneClone As Xml.XmlNode

            oLogger.WriteToLogRelative("loading drives node", , 3)
            xmlNode_Drives = xmlNode_Root.SelectSingleNode("drives")

            xmlNode_DriveClone = xmlNode_Drives.SelectSingleNode("drive").CloneNode(True)
            xmlNodeList_Drives = xmlNode_Drives.SelectNodes("drive")

            oLogger.WriteToLogRelative("removing current drives", , 4)
            For Each xmlNode_Drive In xmlNodeList_Drives
                xmlNode_Drives.RemoveChild(xmlNode_Drive)
            Next

            oLogger.WriteToLogRelative("adding system drive", , 4)
            oLogger.WriteToLogRelative(Drive.GetSystemDriveLetter, , 5)
            xmlNode_DriveCloneClone = xmlNode_DriveClone.CloneNode(True)
            xmlNode_DriveCloneClone.Attributes.GetNamedItem("letter").Value = Drive.GetSystemDriveLetter
            xmlNode_Drives.AppendChild(xmlNode_DriveCloneClone)


            Dim xmlNode_Directories As Xml.XmlNode
            Dim xmlNodeList_Directories As Xml.XmlNodeList
            Dim xmlNode_DirectoryClone As Xml.XmlNode
            Dim xmlNode_Directory As Xml.XmlNode

            xmlNode_Directories = xmlNode_Root.SelectSingleNode("directories")
            xmlNodeList_Directories = xmlNode_Directories.SelectNodes("directory")
            xmlNode_DirectoryClone = xmlNode_Directories.SelectSingleNode("directory").CloneNode(True)

            oLogger.WriteToLogRelative("Adding sitekiosk user folder",, 4)

            For Each xmlNode_Directory In xmlNodeList_Directories
                If xmlNode_Directory.Attributes.GetNamedItem("path").Value = "$(UserProfileDirectory)" Then
                    xmlNode_Directory.Attributes.GetNamedItem("read").Value = "true"
                    xmlNode_Directory.Attributes.GetNamedItem("write").Value = "true"
                    xmlNode_Directory.Attributes.GetNamedItem("execute").Value = "true"




                End If
            Next

            xmlNode_DirectoryClone.Attributes.GetNamedItem("path").Value = "C:\Users\SiteKiosk"
            xmlNode_DirectoryClone.Attributes.GetNamedItem("read").Value = "true"
            xmlNode_DirectoryClone.Attributes.GetNamedItem("write").Value = "true"
            xmlNode_DirectoryClone.Attributes.GetNamedItem("execute").Value = "true"
            xmlNode_Directories.AppendChild(xmlNode_DirectoryClone)



            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(g_SwConfigFile, , 3)
            xmlSkCfg.Save(g_SwConfigFile)
            oLogger.WriteToLogRelative("ok", , 4)
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

        End Try
    End Sub



    'Private Sub ExamineDrives()
    '    Dim allDrives() As DriveInfo = DriveInfo.GetDrives()
    '    Dim systemDrive As String = Environment.GetFolderPath(Environment.SpecialFolder.System).Substring(0, 3)

    '    For Each d As DriveInfo In allDrives
    '        Console.WriteLine("Drive {0}", d.Name)
    '        Console.WriteLine("Is system drive {0}", (d.Name = systemDrive))
    '        Console.WriteLine("  Drive type: {0}", d.DriveType)
    '        If d.IsReady = True Then
    '            Console.WriteLine("  Volume label: {0}", d.VolumeLabel)
    '            Console.WriteLine("  File system: {0}", d.DriveFormat)
    '            Console.WriteLine( _
    '                "  Available space to current user:{0, 15} bytes", _
    '                d.AvailableFreeSpace)

    '            Console.WriteLine( _
    '                "  Total available space:          {0, 15} bytes", _
    '                d.TotalFreeSpace)

    '            Console.WriteLine( _
    '                "  Total size of drive:            {0, 15} bytes ", _
    '                d.TotalSize)
    '        End If
    '    Next
    'End Sub
End Module
