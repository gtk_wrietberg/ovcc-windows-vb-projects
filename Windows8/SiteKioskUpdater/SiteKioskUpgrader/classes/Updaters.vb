﻿Imports System.Text.RegularExpressions

Public Class Updaters
    Public Class GlobalSettings
        Public Shared Function ReadGlobalSettings() As Boolean
            Return _ReadGlobalSettings()
        End Function

        Private Shared Function _ReadGlobalSettings() As Boolean
            Dim sFile As String
            sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

            Dim reString As Regex = New Regex("^(var )([a-zA-Z0-9_]+)=""(.+)"";$")
            Dim reInteger As Regex = New Regex("^(var )([a-zA-Z0-9_]+)=(.+);$")
            Dim re_replace As String = "$3"

            If Not IO.File.Exists(sFile) Then
                Return False
            End If

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var iBAHN_Global_HotelURL=") Then
                            g_Url_Hotel = reString.Replace(line, re_replace)
                        End If
                        If line.Contains("var iBAHN_Global_InternetURL") Then
                            g_Url_Internet = reString.Replace(line, re_replace)
                        End If
                        If line.Contains("var iBAHN_Global_WeatherURL") Then
                            g_Url_Weather = reString.Replace(line, re_replace)
                        End If
                        If line.Contains("var iBAHN_Global_MapURL") Then
                            g_Url_Map = reString.Replace(line, re_replace)
                        End If
                        If line.Contains("var iBAHN_Global_SiteAddress") Then
                            g_Address_Site = reString.Replace(line, re_replace)
                        End If
                        If line.Contains("var iBAHN_Global_Brand") Then
                            Dim sTemp As String = reInteger.Replace(line, re_replace)

                            If Not Integer.TryParse(sTemp, g_Brand_Hotel) Then
                                g_Brand_Hotel = 667
                            End If
                        End If
                    End While
                End Using

                Return True
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Function UpdateGlobalSettings() As Boolean
            Return _ReplaceGlobalSettings()
        End Function

        Private Shared Function _ReplaceGlobalSettings() As Boolean
            Dim lines As New List(Of String)

            Dim sFile As String
            sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

            If Not IO.File.Exists(sFile) Then
                Return False
            End If

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var iBAHN_Global_HotelURL") Then
                            line = "var iBAHN_Global_HotelURL=""" & g_Url_Hotel & """;"
                        End If
                        If line.Contains("var iBAHN_Global_InternetURL") Then
                            line = "var iBAHN_Global_InternetURL=""" & g_Url_Internet & """;"
                        End If
                        If line.Contains("var iBAHN_Global_WeatherURL") Then
                            line = "var iBAHN_Global_WeatherURL=""" & g_Url_Weather & """;"
                        End If
                        If line.Contains("var iBAHN_Global_MapURL") Then
                            line = "var iBAHN_Global_MapURL=""" & g_Url_Map & """;"
                        End If
                        If line.Contains("var iBAHN_Global_SiteAddress") Then
                            line = "var iBAHN_Global_SiteAddress=""" & g_Address_Site & """;"
                        End If
                        If line.Contains("var iBAHN_Global_Brand") Then
                            line = "var iBAHN_Global_Brand=" & g_Brand_Hotel.ToString & ";"
                        End If

                        lines.Add(line)
                    End While
                End Using

                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
                Return True
            Catch ex As Exception : Return False : End Try
        End Function

        'Public Function GetHostnameFromUrl(ByVal sUrl As String) As String
        '    Dim u As New Uri(sUrl)

        '    Return u.Host
        'End Function
    End Class

    Public Class GuestTekJs
        Public Shared Function ReadGuestTekJs() As Boolean
            Return _ReadGuestTekJs()
        End Function

        Private Shared Function _ReadGuestTekJs() As Boolean
            Dim lines As New List(Of String)
            Dim sProgFiles As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sProgFiles.Equals(String.Empty) Then
                sProgFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            sProgFiles = sProgFiles.Replace("\", "\\")


            Dim reString As Regex = New Regex("^(var )([a-zA-Z0-9_]+)=""(.+)"";$")
            Dim reInteger As Regex = New Regex("^(var )([a-zA-Z0-9_]+)=(.+);$")
            Dim re_replace As String = "$3"


            Dim sFile As String
            sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFile &= "\Skins\default\Scripts\guest-tek.js"


            If Not IO.File.Exists(sFile) Then
                sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
                sFile &= "\Skins\default\Scripts\ibanh.js"
            End If

            If Not IO.File.Exists(sFile) Then
                Return False
            End If


            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var LocationID=") Then
                            g_LocationID = reString.Replace(line, re_replace)
                        End If
                    End While
                End Using

                Return True
            Catch ex As Exception

            End Try

            Return False
        End Function

        Public Shared Function UpdateGuestTekJs() As Boolean
            Try
                Return _UpdateGuestTekJs()
            Catch ex As Exception
                Return False
            End Try
        End Function

        Private Shared Function _UpdateGuestTekJs() As Boolean
            Dim lines As New List(Of String)
            Dim sProgFiles As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sProgFiles.Equals(String.Empty) Then
                sProgFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            sProgFiles = sProgFiles.Replace("\", "\\")


            Dim sFileTemplate As String
            sFileTemplate = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFileTemplate &= "\Skins\default\Scripts\guest-tek.js.template"

            Dim sFile As String
            sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFile &= "\Skins\default\Scripts\guest-tek.js"


            If Not IO.File.Exists(sFileTemplate) Then
                Return False
            End If

            Try
                Using sr As New IO.StreamReader(sFileTemplate)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("%%PROGRAMFILES%%") Then
                            line = line.Replace("%%PROGRAMFILES%%", sProgFiles)
                        End If

                        lines.Add(line)
                    End While
                End Using

                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
                Return True
            Catch ex As Exception : Return False : End Try
        End Function
    End Class

    Public Class UpdateBritishAmericanFlag
        Public Shared Function CheckRegionAndSetFlag() As Boolean
            Dim sFlag As String = ""

            Helpers.Logger.WriteRelative("checking registry", , 1)

            Try
                sFlag = Microsoft.Win32.Registry.GetValue(cREGKEY_LPCSOFTWARE & "\Config", "EnglishFlag", "")
                Helpers.Logger.WriteRelative("found: '" & sFlag & "'", , 1)

                If Not sFlag.Equals("gb") And Not sFlag.Equals("us") Then
                    Throw New Exception("incorrect or missing setting in registry")
                End If
            Catch ex As Exception
                Helpers.Logger.WriteRelative("error", , 1)
                Helpers.Logger.WriteRelative(ex.Message, , 2)

                Helpers.Logger.WriteRelative("set to default 'gb'", , 1)
                sFlag = "gb"
            End Try


            Helpers.Logger.WriteRelative("setting flag", , 1)

            Dim sBasePath As String = ""
            sBasePath = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sBasePath &= "\Skins"

            Dim sFlagPath_SK As String = "", sFlagPath_UI As String = ""
            sFlagPath_SK = sBasePath & "\Win7IE8\Img\Languages"
            sFlagPath_UI = sBasePath & "\Public\Startpages\Img\Flags"

            Try
                Helpers.Logger.WriteRelative("copying", , 2)

                If sFlag = "us" Then
                    'sk flag
                    Helpers.Logger.WriteRelative(sFlagPath_SK & "\us_flag.gif" & " ==> " & sFlagPath_SK & "\us.gif", , 3)
                    IO.File.Copy(sFlagPath_SK & "\us_flag.gif", sFlagPath_SK & "\us.gif", True)

                    'ui flag
                    Helpers.Logger.WriteRelative(sFlagPath_UI & "\round\9_us.png" & " ==> " & sFlagPath_UI & "\round\9.png", , 3)
                    IO.File.Copy(sFlagPath_UI & "\round\9_us.png", sFlagPath_UI & "\round\9.png", True)

                    Helpers.Logger.WriteRelative(sFlagPath_UI & "\square\9_us.png" & " ==> " & sFlagPath_UI & "\square\9.png", , 3)
                    IO.File.Copy(sFlagPath_UI & "\square\9_us.png", sFlagPath_UI & "\square\9.png", True)
                Else
                    'sk flag
                    Helpers.Logger.WriteRelative(sFlagPath_SK & "\gb_flag.gif" & " ==> " & sFlagPath_SK & "\us.gif", , 3)
                    IO.File.Copy(sFlagPath_SK & "\gb_flag.gif", sFlagPath_SK & "\us.gif", True) 'sk always uses us.gif for english flag

                    'ui flag
                    Helpers.Logger.WriteRelative(sFlagPath_UI & "\round\9_gb.png" & " ==> " & sFlagPath_UI & "\round\9.png", , 3)
                    IO.File.Copy(sFlagPath_UI & "\round\9_gb.png", sFlagPath_UI & "\round\9.png", True)

                    Helpers.Logger.WriteRelative(sFlagPath_UI & "\square\9_gb.png" & " ==> " & sFlagPath_UI & "\square\9.png", , 3)
                    IO.File.Copy(sFlagPath_UI & "\square\9_gb.png", sFlagPath_UI & "\square\9.png", True)
                End If
            Catch ex As Exception
                Helpers.Logger.WriteRelative("error", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End Try

            Return True
        End Function
    End Class

    Public Class PaymentInfoDialog
        Public Shared Function SetDialog() As Boolean
            '------------------------------------------------------
            'Update payment dialog
            Try
                Helpers.Logger.WriteRelative("Update payment dialog", , 0)
                If Not IO.File.Exists(c_Path_Hilton__paymentdialog) Or Not IO.File.Exists(c_Path_Default__paymentdialog) Then
                    'Missing files, not touching it!
                    If Not IO.File.Exists(c_Path_Hilton__paymentdialog) Then
                        Helpers.Logger.WriteRelative("missing file", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        Helpers.Logger.WriteRelative(c_Path_Hilton__paymentdialog, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    End If
                    If Not IO.File.Exists(c_Path_Default__paymentdialog) Then
                        Helpers.Logger.WriteRelative("missing file", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        Helpers.Logger.WriteRelative(c_Path_Default__paymentdialog, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    End If
                Else
                    Helpers.Logger.WriteRelative("copying", , 1)
                    If g_Force_Hilton Then
                        'Hilton
                        Helpers.Logger.WriteRelative(c_Path_Hilton__paymentdialog & " => " & c_Path_SK__paymentdialog, , 2)
                        IO.File.Copy(c_Path_Hilton__paymentdialog, c_Path_SK__paymentdialog, True)
                        Helpers.Logger.WriteRelative("ok", , 3)
                    Else
                        'normal
                        Helpers.Logger.WriteRelative(c_Path_Default__paymentdialog & " => " & c_Path_SK__paymentdialog, , 2)
                        IO.File.Copy(c_Path_Default__paymentdialog, c_Path_SK__paymentdialog, True)
                        Helpers.Logger.WriteRelative("ok", , 3)
                    End If
                End If

                Return True
            Catch ex As Exception
                Helpers.Logger.WriteRelative("ERROR", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                Helpers.Logger.WriteRelative(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End Try

            Return False
        End Function
    End Class
End Class
