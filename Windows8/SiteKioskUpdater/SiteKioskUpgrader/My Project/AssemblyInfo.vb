Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SiteKioskUpgrader")>
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("GuestTek")>
<Assembly: AssemblyProduct("SiteKioskUpgrader")>
<Assembly: AssemblyCopyright("Copyright ©  2024")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("3cf28e9b-2e11-4eac-a1a1-7d4a963ddb3a")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("33.1.24009.1451")>
<Assembly: AssemblyFileVersion("33.1.24009.1451")>

'<Assembly: AssemblyFileVersion("1.0.0.0")> 

<assembly: AssemblyInformationalVersion("0.0.24009.1451")>