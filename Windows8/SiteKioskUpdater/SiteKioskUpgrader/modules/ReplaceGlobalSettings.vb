Module ReplaceGlobalSettings
    Public Function UpdateGlobalSettings() As Boolean
        Try
            'Dim xmlDoc As Xml.XmlDocument
            'Dim xmlNodeList As Xml.XmlNodeList
            'Dim i As Integer, j As Integer

            'oLogger.WriteToLog("loading settings.xml", , 1)
            'xmlDoc = New Xml.XmlDocument
            'xmlDoc.Load("settings.xml")
            'oLogger.WriteToLog("ok", , 2)

            'oLogger.WriteToLog("loading global settings", , 1)
            'xmlNodeList = xmlDoc.GetElementsByTagName("settings")
            'If xmlNodeList.Count > 0 Then
            '    For i = 0 To xmlNodeList.Count - 1
            '        For j = 0 To xmlNodeList(i).ChildNodes.Count - 1
            '            If xmlNodeList(i).ChildNodes(j).Name = "HotelURL" Then
            '                g_Url_Hotel = xmlNodeList(i).ChildNodes(j).InnerText
            '                oLogger.WriteToLog("HotelURL", , 2)
            '            End If
            '            If xmlNodeList(i).ChildNodes(j).Name = "InternetURL" Then
            '                g_Url_Internet = xmlNodeList(i).ChildNodes(j).InnerText
            '                oLogger.WriteToLog("InternetURL", , 2)
            '            End If
            '            If xmlNodeList(i).ChildNodes(j).Name = "WeatherURL" Then
            '                g_Url_Weather = xmlNodeList(i).ChildNodes(j).InnerText
            '                oLogger.WriteToLog("WeatherURL", , 2)
            '            End If
            '            If xmlNodeList(i).ChildNodes(j).Name = "MapURL" Then
            '                g_Url_Map = xmlNodeList(i).ChildNodes(j).InnerText
            '                oLogger.WriteToLog("MapURL", , 2)
            '            End If
            '            If xmlNodeList(i).ChildNodes(j).Name = "SiteAddress" Then
            '                g_Address_Site = xmlNodeList(i).ChildNodes(j).InnerText
            '                oLogger.WriteToLog("SiteAddress", , 2)
            '            End If
            '            If xmlNodeList(i).ChildNodes(j).Name = "BrandNumber" Then
            '                g_Brand_Hotel = xmlNodeList(i).ChildNodes(j).InnerText
            '                oLogger.WriteToLog("BrandNumber", , 2)
            '            End If
            '        Next
            '    Next
            'End If

            Return _ReplaceGlobalSettings()
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function _ReplaceGlobalSettings() As Boolean
        Dim lines As New List(Of String)

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

        If Not IO.File.Exists(sFile) Then
            Return False
        End If

        Try
            Using sr As New IO.StreamReader(sFile)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("var iBAHN_Global_HotelURL") Then
                        line = "var iBAHN_Global_HotelURL=""" & g_Url_Hotel & """;"
                    End If
                    If line.Contains("var iBAHN_Global_InternetURL") Then
                        line = "var iBAHN_Global_InternetURL=""" & g_Url_Internet & """;"
                    End If
                    If line.Contains("var iBAHN_Global_WeatherURL") Then
                        line = "var iBAHN_Global_WeatherURL=""" & g_Url_Weather & """;"
                    End If
                    If line.Contains("var iBAHN_Global_MapURL") Then
                        line = "var iBAHN_Global_MapURL=""" & g_Url_Map & """;"
                    End If
                    If line.Contains("var iBAHN_Global_SiteAddress") Then
                        line = "var iBAHN_Global_SiteAddress=""" & g_Address_Site & """;"
                    End If
                    If line.Contains("var iBAHN_Global_Brand") Then
                        line = "var iBAHN_Global_Brand=" & g_Brand_Hotel & ";"
                    End If

                    lines.Add(line)
                End While
            End Using

            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using
            Return True
        Catch ex As Exception : Return False : End Try
    End Function

    'Public Function GetHostnameFromUrl(ByVal sUrl As String) As String
    '    Dim u As New Uri(sUrl)

    '    Return u.Host
    'End Function
End Module
