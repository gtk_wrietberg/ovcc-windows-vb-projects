Module Globals
    Public oComputerName As ComputerName

    Public oCopyFiles As CopyFiles
    Public oSkCfg As SkCfg

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupBaseDirectory As String
    Public g_BackupDirectory As String
    Public g_BackupDate As String

    Public g_TESTMODE As Boolean

    Public g_OverwriteExistingConfig As Boolean

    Public g_BackupEntireSiteKioskFolder As Boolean


    'Settings from ibahn_function.js
    Public g_Url_Hotel As String = "http://www.guesttek.com"
    Public g_Url_Internet As String = "http://www.google.com"
    Public g_Url_Weather As String = "http://www.weather.com"
    Public g_Url_Map As String = "http://maps.google.com"
    Public g_Address_Site As String = "GuestTek, Calgary, Alberta Canada"
    Public g_Brand_Hotel As Integer = 667
    Public g_Force_Hilton As Boolean = False

    'Settings from ibahn.js/guesttek.js
    Public g_LocationID As String = ""
    Public g_Airlines As New List(Of String)


    Public ReadOnly c_FreeUrlZoneName As String = "No Charge (UI)"

    Public ReadOnly c_DEFAULT_SITEKIOSK8_CONFIG_FILENAME As String = "GuestTek_SK8.skcfg"
    Public ReadOnly c_DEFAULT_SITEKIOSK9_CONFIG_FILENAME As String = "GuestTek_SK9.skcfg"
    Public ReadOnly c_DEFAULT_SITEKIOSK9_HILTON_CONFIG_FILENAME As String = "GuestTek_SK9_Hilton.skcfg"

    Public ReadOnly c_Path_SK__paymentdialog As String = _GetProgramFilesFolder() & "\SiteKiosk\SiteCash\Html\PaymentInfoDlg.htm"
    Public ReadOnly c_Path_Hilton__paymentdialog As String = _GetProgramFilesFolder() & "\SiteKiosk\SiteCash\Html\PaymentInfoDlg.HILTON.htm"
    Public ReadOnly c_Path_Default__paymentdialog As String = _GetProgramFilesFolder() & "\SiteKiosk\SiteCash\Html\PaymentInfoDlg.DEFAULT.htm"


    Public c_SITEKIOSK_LICENSE_FILE As String = "sk-license.xml"

    Public ReadOnly cREGKEY_LPCSOFTWARE As String = "HKEY_LOCAL_MACHINE\SOFTWARE\GuestTek\OVCCSoftware"

    Private Function _GetProgramFilesFolder() As String
        Dim sPath As String = ""

        sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sPath.Equals(String.Empty) Then
            sPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        Return sPath
    End Function


    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        Dim sProgFiles As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sProgFiles.Equals(String.Empty) Then
            sProgFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If


        g_BackupDate = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")


        g_LogFileDirectory = sProgFiles & "\GuestTek\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupBaseDirectory = sProgFiles & "\GuestTek\_backups"
        g_BackupDirectory = g_BackupBaseDirectory & "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & g_BackupDate

        Try
            IO.Directory.CreateDirectory(g_LogFileDirectory)

        Catch ex As Exception

        End Try

        Try
            IO.Directory.CreateDirectory(g_BackupDirectory)

        Catch ex As Exception

        End Try


        g_OverwriteExistingConfig = False
        g_BackupEntireSiteKioskFolder = False
    End Sub


End Module
