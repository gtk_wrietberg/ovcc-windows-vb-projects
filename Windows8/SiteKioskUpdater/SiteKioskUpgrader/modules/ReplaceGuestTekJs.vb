Module ReplaceGuestTekJs
    Public Function UpdateGuestTekJs() As Boolean
        Try
            Return _UpdateGuestTekJs()
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function _UpdateGuestTekJs() As Boolean
        Dim lines As New List(Of String)
        Dim sProgFiles As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

        If sProgFiles.Equals(String.Empty) Then
            sProgFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
        End If

        sProgFiles = sProgFiles.Replace("\", "\\")


        Dim sFileTemplate As String
        sFileTemplate = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFileTemplate &= "\Skins\default\Scripts\guest-tek.js.template"

        Dim sFile As String
        sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
        sFile &= "\Skins\default\Scripts\guest-tek.js"


        If Not IO.File.Exists(sFileTemplate) Then
            Return False
        End If

        Try
            Using sr As New IO.StreamReader(sFileTemplate)
                Dim line As String

                While Not sr.EndOfStream
                    line = sr.ReadLine

                    If line.Contains("%%PROGRAMFILES%%") Then
                        line = line.Replace("%%PROGRAMFILES%%", sProgFiles)
                    End If

                    lines.Add(line)
                End While
            End Using

            Using sw As New IO.StreamWriter(sFile)
                For Each line As String In lines
                    sw.WriteLine(line)
                Next
            End Using
            Return True
        Catch ex As Exception : Return False : End Try
    End Function
End Module
