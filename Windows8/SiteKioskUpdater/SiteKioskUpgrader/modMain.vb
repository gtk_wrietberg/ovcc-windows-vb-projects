﻿Module Main
    Public Sub Main()
        Helpers.Logger.InitialiseLogger()


        Helpers.Logger.Write(New String("*", 50))
        Helpers.Logger.Write(My.Application.Info.ProductName & " v" & My.Application.Info.Version.ToString)
        Helpers.Logger.Write("Installer started")


        InitGlobals()



        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If



        MsgBox(Updaters.GlobalSettings.ReadGlobalSettings())
        MsgBox(g_Address_Site)
        MsgBox(g_Brand_Hotel.ToString)

        MsgBox(Updaters.GuestTekJs.ReadGuestTekJs())
        MsgBox(g_LocationID)


        Exit Sub


        '------------------------------------------------------
        'Stop Watchdog
        Dim listProcesses As New List(Of String)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogWarning.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogWarningTrigger.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogLastActivity.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogServiceEnable.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogServiceDisable.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogError.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogMuzzle.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__WatchdogEmergencyDeployApp.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__OLD_WatchdogService.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__OLD_WatchdogController.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__OLD_WatchdogWarning.Replace(".exe", "").ToLower)
        listProcesses.Add(LPCConstants.FilesAndFolders.FILE__OLD_WatchdogTrigger.Replace(".exe", "").ToLower)

        For Each p As Process In Process.GetProcesses
            If listProcesses.IndexOf(p.ProcessName.ToLower) >= 0 Then
                Helpers.Logger.Write("killing: " & p.ProcessName, , 1)
                Try
                    p.Kill()
                    Helpers.Logger.Write("ok", , 2)
                Catch ex As Exception
                    Helpers.Logger.Write("fail", Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    Helpers.Logger.Write(ex.Message, Helpers.Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                End Try
            End If
        Next


        '------------------------------------------------------
        If g_BackupEntireSiteKioskFolder Then
            oCopyFiles = New CopyFiles
            oCopyFiles.SuppressCopyLogging = True
            oCopyFiles.BackupDirectory = g_BackupBaseDirectory & "\_SkFB_\" & g_BackupDate

            If IO.Directory.Exists("C:\Program Files (x86)\") Then
                oCopyFiles.SourceDirectory = "C:\Program Files (x86)\SiteKiosk"
            Else
                oCopyFiles.SourceDirectory = "C:\Program Files\SiteKiosk"
            End If
            oCopyFiles.DestinationDirectory = g_BackupBaseDirectory & "\SiteKioskFullBackup\" & g_BackupDate & "\SiteKiosk"

            Helpers.Logger.Write("Copying SiteKiosk files", , 0)
            oCopyFiles.CopyFiles()
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.SuppressCopyLogging = False
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        Helpers.Logger.Write("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        Helpers.Logger.Write("Update global settings", , 0)
        Updaters.GlobalSettings.UpdateGlobalSettings()


        '------------------------------------------------------
        Helpers.Logger.Write("Update guesttek.js", , 0)
        Updaters.GuestTekJs.UpdateGuestTekJs()


        '------------------------------------------------------
        Helpers.Logger.Write("Update gb/uk flag", , 0)
        Updaters.UpdateBritishAmericanFlag.CheckRegionAndSetFlag()


        '------------------------------------------------------
        Helpers.Logger.Write("Update payment info dialog", , 0)
        Updaters.PaymentInfoDialog.SetDialog()


        '------------------------------------------------------
        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        Helpers.Logger.Write("Checking for external license", , 0)
        oSkCfg.CheckForExternalLicense()

        Helpers.Logger.Write("Update SiteKiosk config", , 0)

        If oSkCfg.SiteKioskVersion = 9 Then
            If g_Force_Hilton Then
                oSkCfg.NewSkCfgFile = oSkCfg.SiteKioskFolder & "\Config\" & c_DEFAULT_SITEKIOSK9_HILTON_CONFIG_FILENAME
            Else
                oSkCfg.NewSkCfgFile = oSkCfg.SiteKioskFolder & "\Config\" & c_DEFAULT_SITEKIOSK9_CONFIG_FILENAME
            End If
        Else
            oSkCfg.NewSkCfgFile = oSkCfg.SiteKioskFolder & "\Config\" & c_DEFAULT_SITEKIOSK8_CONFIG_FILENAME
        End If

        Helpers.Logger.Write("file", , 1)
        Helpers.Logger.Write(oSkCfg.NewSkCfgFile, , 2)

        Helpers.Logger.Write("AddPagesToFreezone", , 1)
        oSkCfg.AddPagesToFreezone()

        Helpers.Logger.Write("ActivateNewSkCfg", , 1)
        oSkCfg.ActivateNewSkCfg()

        Helpers.Logger.Write("UpdateLicense", , 1)
        oSkCfg.UpdateLicense()

        Helpers.Logger.Write("HideFreshInstallDialogs", , 1)
        oSkCfg.HideFreshInstallDialogs()


        '------------------------------------------------------
        Helpers.Logger.Write("Killing SiteKiosk", , 0)
        KillSiteKiosk()

        '------------------------------------------------------
        Helpers.Logger.Write("Done", , 0)
        Helpers.Logger.Write("bye", , 1)
    End Sub
End Module
