﻿Module Main
    Public Sub Main()
        InitGlobals()

        oLogger = New Logger

        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        Dim arg As String, arg_index As Integer
        For arg_index = 1 To Environment.GetCommandLineArgs.Length - 1
            arg = Environment.GetCommandLineArgs(arg_index)

            oLogger.WriteToLog("found: " & arg, , 1)

            If arg = "--overwrite-existing-config" Then
                g_OverwriteExistingConfig = True
            End If

            If arg = "--backup-entire-folder" Then
                g_BackupEntireSiteKioskFolder = True
            End If

            If arg = "forcehilton" Or arg = "-spforcehilton" Then
                g_Force_Hilton = True
                g_Brand_Hotel = 0
            End If

            If arg.StartsWith("brand-number:") Then
                Dim iTmp As Integer = 0
                If Integer.TryParse(arg.Replace("brand-number:", ""), iTmp) = True Then
                    g_Brand_Hotel = iTmp
                Else
                    oLogger.WriteToLog("invalid!", , 2)
                End If
            End If
        Next


        '------------------------------------------------------
        oComputerName = New ComputerName

        If InStr(oComputerName.ComputerName.ToLower, "rietberg") > 0 Or _
        InStr(oComputerName.ComputerName.ToLower, "superlekkerding") > 0 Then
            g_TESTMODE = True
        End If


        '------------------------------------------------------
        If g_BackupEntireSiteKioskFolder Then
            oCopyFiles = New CopyFiles
            oCopyFiles.SuppressCopyLogging = True
            oCopyFiles.BackupDirectory = g_BackupBaseDirectory & "\_SkFB_\" & g_BackupDate

            If IO.Directory.Exists("C:\Program Files (x86)\") Then
                oCopyFiles.SourceDirectory = "C:\Program Files (x86)\SiteKiosk"
            Else
                oCopyFiles.SourceDirectory = "C:\Program Files\SiteKiosk"
            End If
            oCopyFiles.DestinationDirectory = g_BackupBaseDirectory & "\SiteKioskFullBackup\" & g_BackupDate & "\SiteKiosk"

            oLogger.WriteToLog("Copying SiteKiosk files", , 0)
            oCopyFiles.CopyFiles()
        End If


        '------------------------------------------------------
        oCopyFiles = New CopyFiles
        oCopyFiles.SuppressCopyLogging = False
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oCopyFiles.SourceDirectory = "files"
        If IO.Directory.Exists("C:\Program Files (x86)\") Then
            oCopyFiles.DestinationDirectory = "C:\Program Files (x86)\"
        Else
            oCopyFiles.DestinationDirectory = "C:\Program Files\"
        End If
        oLogger.WriteToLog("Copy files", , 0)
        oCopyFiles.CopyFiles()


        '------------------------------------------------------
        oLogger.WriteToLog("Update global settings", , 0)
        Updaters.ReplaceGlobalSettings.UpdateGlobalSettings()


        '------------------------------------------------------
        oLogger.WriteToLog("Update guesttek.js", , 0)
        Updaters.ReplaceGuestTekJs.UpdateGuestTekJs()


        '------------------------------------------------------
        oLogger.WriteToLog("Update gb/uk flag", , 0)
        Updaters.UpdateBritishAmericanFlag.CheckRegionAndSetFlag()


        '------------------------------------------------------
        oLogger.WriteToLog("Update payment info dialog", , 0)
        Updaters.PaymentInfoDialog.SetDialog()


        '------------------------------------------------------
        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Checking for external license", , 0)
        oSkCfg.CheckForExternalLicense()

        oLogger.WriteToLog("Update SiteKiosk config", , 0)

        If oSkCfg.SiteKioskVersion = 9 Then
            If g_Force_Hilton Then
                oSkCfg.NewSkCfgFile = oSkCfg.SiteKioskFolder & "\Config\" & c_DEFAULT_SITEKIOSK9_HILTON_CONFIG_FILENAME
            Else
                oSkCfg.NewSkCfgFile = oSkCfg.SiteKioskFolder & "\Config\" & c_DEFAULT_SITEKIOSK9_CONFIG_FILENAME
            End If
        Else
            oSkCfg.NewSkCfgFile = oSkCfg.SiteKioskFolder & "\Config\" & c_DEFAULT_SITEKIOSK8_CONFIG_FILENAME
        End If

        oLogger.WriteToLog("file", , 1)
        oLogger.WriteToLog(oSkCfg.NewSkCfgFile, , 2)

        oLogger.WriteToLog("AddPagesToFreezone", , 1)
        oSkCfg.AddPagesToFreezone()

        oLogger.WriteToLog("ActivateNewSkCfg", , 1)
        oSkCfg.ActivateNewSkCfg()

        oLogger.WriteToLog("UpdateLicense", , 1)
        oSkCfg.UpdateLicense()

        oLogger.WriteToLog("HideFreshInstallDialogs", , 1)
        oSkCfg.HideFreshInstallDialogs()


        '------------------------------------------------------
        oLogger.WriteToLog("Killing SiteKiosk", , 0)
        KillSiteKiosk()

        '------------------------------------------------------
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("bye", , 1)
    End Sub
End Module
