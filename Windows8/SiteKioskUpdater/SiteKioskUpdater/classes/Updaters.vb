﻿Public Class Updaters
    Public Class ReplaceGlobalSettings
        Public Shared Function UpdateGlobalSettings() As Boolean
            Try
                'Dim xmlDoc As Xml.XmlDocument
                'Dim xmlNodeList As Xml.XmlNodeList
                'Dim i As Integer, j As Integer

                'oLogger.WriteToLog("loading settings.xml", , 1)
                'xmlDoc = New Xml.XmlDocument
                'xmlDoc.Load("settings.xml")
                'oLogger.WriteToLog("ok", , 2)

                'oLogger.WriteToLog("loading global settings", , 1)
                'xmlNodeList = xmlDoc.GetElementsByTagName("settings")
                'If xmlNodeList.Count > 0 Then
                '    For i = 0 To xmlNodeList.Count - 1
                '        For j = 0 To xmlNodeList(i).ChildNodes.Count - 1
                '            If xmlNodeList(i).ChildNodes(j).Name = "HotelURL" Then
                '                g_Url_Hotel = xmlNodeList(i).ChildNodes(j).InnerText
                '                oLogger.WriteToLog("HotelURL", , 2)
                '            End If
                '            If xmlNodeList(i).ChildNodes(j).Name = "InternetURL" Then
                '                g_Url_Internet = xmlNodeList(i).ChildNodes(j).InnerText
                '                oLogger.WriteToLog("InternetURL", , 2)
                '            End If
                '            If xmlNodeList(i).ChildNodes(j).Name = "WeatherURL" Then
                '                g_Url_Weather = xmlNodeList(i).ChildNodes(j).InnerText
                '                oLogger.WriteToLog("WeatherURL", , 2)
                '            End If
                '            If xmlNodeList(i).ChildNodes(j).Name = "MapURL" Then
                '                g_Url_Map = xmlNodeList(i).ChildNodes(j).InnerText
                '                oLogger.WriteToLog("MapURL", , 2)
                '            End If
                '            If xmlNodeList(i).ChildNodes(j).Name = "SiteAddress" Then
                '                g_Address_Site = xmlNodeList(i).ChildNodes(j).InnerText
                '                oLogger.WriteToLog("SiteAddress", , 2)
                '            End If
                '            If xmlNodeList(i).ChildNodes(j).Name = "BrandNumber" Then
                '                g_Brand_Hotel = xmlNodeList(i).ChildNodes(j).InnerText
                '                oLogger.WriteToLog("BrandNumber", , 2)
                '            End If
                '        Next
                '    Next
                'End If

                Return _ReplaceGlobalSettings()
            Catch ex As Exception
                Return False
            End Try
        End Function

        Private Shared Function _ReplaceGlobalSettings() As Boolean
            Dim lines As New List(Of String)

            Dim sFile As String
            sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFile &= "\Skins\Public\iBAHN\Scripts\iBAHN_functions.js"

            If Not IO.File.Exists(sFile) Then
                Return False
            End If

            Try
                Using sr As New IO.StreamReader(sFile)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("var iBAHN_Global_HotelURL") Then
                            line = "var iBAHN_Global_HotelURL=""" & g_Url_Hotel & """;"
                        End If
                        If line.Contains("var iBAHN_Global_InternetURL") Then
                            line = "var iBAHN_Global_InternetURL=""" & g_Url_Internet & """;"
                        End If
                        If line.Contains("var iBAHN_Global_WeatherURL") Then
                            line = "var iBAHN_Global_WeatherURL=""" & g_Url_Weather & """;"
                        End If
                        If line.Contains("var iBAHN_Global_MapURL") Then
                            line = "var iBAHN_Global_MapURL=""" & g_Url_Map & """;"
                        End If
                        If line.Contains("var iBAHN_Global_SiteAddress") Then
                            line = "var iBAHN_Global_SiteAddress=""" & g_Address_Site & """;"
                        End If
                        If line.Contains("var iBAHN_Global_Brand") Then
                            line = "var iBAHN_Global_Brand=" & g_Brand_Hotel.ToString & ";"
                        End If

                        lines.Add(line)
                    End While
                End Using

                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
                Return True
            Catch ex As Exception : Return False : End Try
        End Function

        'Public Function GetHostnameFromUrl(ByVal sUrl As String) As String
        '    Dim u As New Uri(sUrl)

        '    Return u.Host
        'End Function
    End Class

    Public Class ReplaceGuestTekJs
        Public Shared Function UpdateGuestTekJs() As Boolean
            Try
                Return _UpdateGuestTekJs()
            Catch ex As Exception
                Return False
            End Try
        End Function

        Private Shared Function _UpdateGuestTekJs() As Boolean
            Dim lines As New List(Of String)
            Dim sProgFiles As String = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86)

            If sProgFiles.Equals(String.Empty) Then
                sProgFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)
            End If

            sProgFiles = sProgFiles.Replace("\", "\\")


            Dim sFileTemplate As String
            sFileTemplate = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFileTemplate &= "\Skins\default\Scripts\guest-tek.js.template"

            Dim sFile As String
            sFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sFile &= "\Skins\default\Scripts\guest-tek.js"


            If Not IO.File.Exists(sFileTemplate) Then
                Return False
            End If

            Try
                Using sr As New IO.StreamReader(sFileTemplate)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("%%PROGRAMFILES%%") Then
                            line = line.Replace("%%PROGRAMFILES%%", sProgFiles)
                        End If

                        lines.Add(line)
                    End While
                End Using

                Using sw As New IO.StreamWriter(sFile)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
                Return True
            Catch ex As Exception : Return False : End Try
        End Function
    End Class

    Public Class UpdateBritishAmericanFlag
        Public Shared Function CheckRegionAndSetFlag() As Boolean
            Dim sFlag As String = ""

            oLogger.WriteToLogRelative("checking registry", , 1)

            Try
                sFlag = Microsoft.Win32.Registry.GetValue(cREGKEY_LPCSOFTWARE & "\Config", "EnglishFlag", "")
                oLogger.WriteToLogRelative("found: '" & sFlag & "'", , 1)

                If Not sFlag.Equals("gb") And Not sFlag.Equals("us") Then
                    Throw New Exception("incorrect or missing setting in registry")
                End If
            Catch ex As Exception
                oLogger.WriteToLogRelative("error", , 1)
                oLogger.WriteToLogRelative(ex.Message, , 2)

                oLogger.WriteToLogRelative("set to default 'gb'", , 1)
                sFlag = "gb"
            End Try


            oLogger.WriteToLogRelative("setting flag", , 1)

            Dim sBasePath As String = ""
            sBasePath = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
            sBasePath &= "\Skins"

            Dim sFlagPath_SK As String = "", sFlagPath_UI As String = ""
            sFlagPath_SK = sBasePath & "\Win7IE8\Img\Languages"
            sFlagPath_UI = sBasePath & "\Public\Startpages\Img\Flags"

            Try
                oLogger.WriteToLogRelative("copying", , 2)

                If sFlag = "us" Then
                    'sk flag
                    oLogger.WriteToLogRelative(sFlagPath_SK & "\us_flag.gif" & " ==> " & sFlagPath_SK & "\us.gif", , 3)
                    IO.File.Copy(sFlagPath_SK & "\us_flag.gif", sFlagPath_SK & "\us.gif", True)

                    'ui flag
                    oLogger.WriteToLogRelative(sFlagPath_UI & "\round\9_us.png" & " ==> " & sFlagPath_UI & "\round\9.png", , 3)
                    IO.File.Copy(sFlagPath_UI & "\round\9_us.png", sFlagPath_UI & "\round\9.png", True)

                    oLogger.WriteToLogRelative(sFlagPath_UI & "\square\9_us.png" & " ==> " & sFlagPath_UI & "\square\9.png", , 3)
                    IO.File.Copy(sFlagPath_UI & "\square\9_us.png", sFlagPath_UI & "\square\9.png", True)
                Else
                    'sk flag
                    oLogger.WriteToLogRelative(sFlagPath_SK & "\gb_flag.gif" & " ==> " & sFlagPath_SK & "\us.gif", , 3)
                    IO.File.Copy(sFlagPath_SK & "\gb_flag.gif", sFlagPath_SK & "\us.gif", True) 'sk always uses us.gif for english flag

                    'ui flag
                    oLogger.WriteToLogRelative(sFlagPath_UI & "\round\9_gb.png" & " ==> " & sFlagPath_UI & "\round\9.png", , 3)
                    IO.File.Copy(sFlagPath_UI & "\round\9_gb.png", sFlagPath_UI & "\round\9.png", True)

                    oLogger.WriteToLogRelative(sFlagPath_UI & "\square\9_gb.png" & " ==> " & sFlagPath_UI & "\square\9.png", , 3)
                    IO.File.Copy(sFlagPath_UI & "\square\9_gb.png", sFlagPath_UI & "\square\9.png", True)
                End If
            Catch ex As Exception
                oLogger.WriteToLogRelative("error", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End Try

            Return True
        End Function
    End Class

    Public Class PaymentInfoDialog
        Public Shared Function SetDialog() As Boolean
            '------------------------------------------------------
            'Update payment dialog
            Try
                oLogger.WriteToLogRelative("Update payment dialog", , 0)
                If Not IO.File.Exists(c_Path_Hilton__paymentdialog) Or Not IO.File.Exists(c_Path_Default__paymentdialog) Then
                    'Missing files, not touching it!
                    If Not IO.File.Exists(c_Path_Hilton__paymentdialog) Then
                        oLogger.WriteToLogRelative("missing file", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        oLogger.WriteToLogRelative(c_Path_Hilton__paymentdialog, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    End If
                    If Not IO.File.Exists(c_Path_Default__paymentdialog) Then
                        oLogger.WriteToLogRelative("missing file", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                        oLogger.WriteToLogRelative(c_Path_Default__paymentdialog, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                    End If
                Else
                    oLogger.WriteToLogRelative("copying", , 1)
                    If g_Force_Hilton Then
                        'Hilton
                        oLogger.WriteToLogRelative(c_Path_Hilton__paymentdialog & " => " & c_Path_SK__paymentdialog, , 2)
                        IO.File.Copy(c_Path_Hilton__paymentdialog, c_Path_SK__paymentdialog, True)
                        oLogger.WriteToLogRelative("ok", , 3)
                    Else
                        'normal
                        oLogger.WriteToLogRelative(c_Path_Default__paymentdialog & " => " & c_Path_SK__paymentdialog, , 2)
                        IO.File.Copy(c_Path_Default__paymentdialog, c_Path_SK__paymentdialog, True)
                        oLogger.WriteToLogRelative("ok", , 3)
                    End If
                End If

                Return True
            Catch ex As Exception
                oLogger.WriteToLogRelative("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End Try

            Return False
        End Function
    End Class
End Class
