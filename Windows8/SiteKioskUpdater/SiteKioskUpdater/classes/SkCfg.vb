Public Class SkCfg
    Private mOldSkCfgFile As String
    Private mNewSkCfgFile As String
    Private mSkBuild As String
    Private mBackupFolder As String
    Private mSiteKioskFolder As String
    Private mSiteKioskVersion As Integer
    Private mSiteKioskLicense_Licensee As String
    Private mSiteKioskLicense_Signature As String

    Private ReadOnly c_REGROOT_SITEKIOSK As String = "HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk"

    Private ReadOnly c_LICENSE_SITEKIOSK8_LICENSEE As String = """MASTER-TEST Guest-Tek SK8 971873 - 1 License(s)"""
    Private ReadOnly c_LICENSE_SITEKIOSK8_SIGNATURE As String = """6MTQC-DFWR7-22XBT-YGVC7-9MTGB"""

    Private ReadOnly c_LICENSE_SITEKIOSK9_LICENSEE As String = """Guest-Tek TEST SKP 99968 - 1 License(s)"""
    Private ReadOnly c_LICENSE_SITEKIOSK9_SIGNATURE As String = """MCH9B-D2VW7-VBFMK-FF8VV-CHPXV"""

    Public Property SiteKioskLicense_Licensee() As String
        Get
            Return mSiteKioskLicense_Licensee
        End Get
        Set(ByVal value As String)
            mSiteKioskLicense_Licensee = value
        End Set
    End Property

    Public Property SiteKioskLicense_Signature() As String
        Get
            Return mSiteKioskLicense_Signature
        End Get
        Set(ByVal value As String)
            mSiteKioskLicense_Signature = value
        End Set
    End Property

    Public Property BackupFolder() As String
        Get
            Return mBackupFolder
        End Get
        Set(ByVal value As String)
            mBackupFolder = value
        End Set
    End Property

    Public Property NewSkCfgFile As String
        Get
            Return mNewSkCfgFile
        End Get
        Set(ByVal value As String)
            mNewSkCfgFile = value
        End Set
    End Property

    Public ReadOnly Property SiteKioskFolder As String
        Get
            Return mSiteKioskFolder
        End Get
    End Property

    Public ReadOnly Property SiteKioskVersion As Integer
        Get
            Return mSiteKioskVersion
        End Get
    End Property


    'Public Sub Update(Optional ByVal bOverWriteOldFile As Boolean = False)
    '    Try
    '        oLogger.WriteToLogRelative("finding SiteKiosk install directory", , 1)
    '        mSiteKioskFolder = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "InstallDir", "")
    '        oLogger.WriteToLogRelative("found: " & mSiteKioskFolder, , 2)

    '        oLogger.WriteToLogRelative("finding active skcfg file", , 1)
    '        mOldSkCfgFile = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")
    '        oLogger.WriteToLogRelative("found: " & mOldSkCfgFile, , 2)



    '        Dim oFile As IO.FileInfo, dDate As Date = Now(), sDateString As String

    '        sDateString = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

    '        If bOverWriteOldFile Then
    '            oLogger.WriteToLogRelative("backing up current config", Logger.MESSAGE_TYPE.LOG_WARNING, 1)
    '            oFile = New IO.FileInfo(mOldSkCfgFile)
    '            oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".iBAHNnewUI.backup", True)

    '            oLogger.WriteToLogRelative("backup: " & mOldSkCfgFile & "." & sDateString & ".iBAHNnewUI.backup", , 2)

    '            mNewSkCfgFile = mOldSkCfgFile
    '        Else
    '            oLogger.WriteToLogRelative("making copy", , 1)

    '            oFile = New IO.FileInfo(mOldSkCfgFile)
    '            mNewSkCfgFile = oFile.DirectoryName & "\iBAHN_newUI__" & sDateString & ".skcfg"
    '            oFile.CopyTo(mNewSkCfgFile, True)

    '            oLogger.WriteToLogRelative("copy: " & mNewSkCfgFile, , 2)
    '        End If


    '        Dim nt As Xml.XmlNameTable
    '        Dim ns As Xml.XmlNamespaceManager
    '        Dim xmlSkCfg As Xml.XmlDocument
    '        Dim xmlNode_Root As Xml.XmlNode

    '        Dim xmlPatchReplacements As Xml.XmlDocument
    '        Dim xmlNode_Patch_Root As Xml.XmlNode

    '        xmlSkCfg = New Xml.XmlDocument

    '        oLogger.WriteToLogRelative("updating", , 1)

    '        oLogger.WriteToLogRelative("loading", , 2)
    '        oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
    '        xmlSkCfg.Load(mNewSkCfgFile)
    '        oLogger.WriteToLogRelative("ok", , 4)


    '        nt = xmlSkCfg.NameTable
    '        ns = New Xml.XmlNamespaceManager(nt)
    '        ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
    '        ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

    '        oLogger.WriteToLogRelative("loading root node", , 2)
    '        xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

    '        If Not xmlNode_Root Is Nothing Then
    '            oLogger.WriteToLogRelative("ok", , 3)
    '        Else
    '            Throw New Exception("sitekiosk-configuration node not found")
    '        End If


    '        '*************************************************************************************************************
    '        'Patch
    '        oLogger.WriteToLogRelative("loading replacement patches", , 2)
    '        xmlPatchReplacements = New Xml.XmlDocument
    '        oLogger.WriteToLogRelative(System.AppDomain.CurrentDomain.BaseDirectory & "\skcfg.replacements.xml", , 3)
    '        xmlPatchReplacements.Load(System.AppDomain.CurrentDomain.BaseDirectory & "\skcfg.replacements.xml")
    '        xmlNode_Patch_Root = xmlPatchReplacements.SelectSingleNode("sk:replacements", ns)

    '        If Not xmlNode_Patch_Root Is Nothing Then
    '            oLogger.WriteToLogRelative("ok", , 4)
    '        Else
    '            Throw New Exception("sitekiosk-configuration node not found")
    '        End If



    '        '*************************************************************************************************************
    '        'SiteSkin - remove current section and replace with patch
    '        oLogger.WriteToLogRelative("SiteSkin update", , 2)
    '        Dim xmlNode_SiteSkin As Xml.XmlNode

    '        oLogger.WriteToLogRelative("loading SiteSkin node", , 3)
    '        xmlNode_SiteSkin = xmlNode_Root.SelectSingleNode("sk:siteskin", ns)

    '        oLogger.WriteToLogRelative("removing current SiteSkin settings", , 3)
    '        xmlNode_SiteSkin.RemoveAll()

    '        Dim xmlNodeList_Patch_SiteSkin As Xml.XmlNodeList
    '        Dim xmlNode_Patch_SiteSkin As Xml.XmlNode

    '        oLogger.WriteToLogRelative("loading patch", , 3)
    '        xmlNode_Patch_SiteSkin = xmlNode_Patch_Root.SelectSingleNode("sk:siteskin", ns)
    '        xmlNodeList_Patch_SiteSkin = xmlNode_Patch_SiteSkin.ChildNodes

    '        oLogger.WriteToLogRelative("applying patch", , 3)
    '        Dim xmlNode_Patch_SiteSkin_SubNode As Xml.XmlNode
    '        Dim xmlNode_SiteSkinImport As Xml.XmlNode
    '        For Each xmlNode_Patch_SiteSkin_SubNode In xmlNodeList_Patch_SiteSkin
    '            xmlNode_SiteSkinImport = xmlSkCfg.ImportNode(xmlNode_Patch_SiteSkin_SubNode, True)
    '            xmlNode_SiteSkin.AppendChild(xmlNode_SiteSkinImport)
    '        Next


    '        '*************************************************************************************************************
    '        'StartpageConfig - remove current section and replace with patch
    '        oLogger.WriteToLogRelative("startpageconfig update", , 2)
    '        Dim xmlNode_StartpageConfig As Xml.XmlNode

    '        oLogger.WriteToLogRelative("loading startpageconfig node", , 3)
    '        xmlNode_StartpageConfig = xmlNode_Root.SelectSingleNode("sk:startpageconfig", ns)

    '        oLogger.WriteToLogRelative("removing current startpageconfig settings", , 3)
    '        xmlNode_StartpageConfig.RemoveAll()

    '        Dim xmlNodeList_Patch_StartpageConfig As Xml.XmlNodeList
    '        Dim xmlNode_Patch_StartpageConfig As Xml.XmlNode

    '        oLogger.WriteToLogRelative("loading patch", , 3)
    '        xmlNode_Patch_StartpageConfig = xmlNode_Patch_Root.SelectSingleNode("sk:startpageconfig", ns)
    '        xmlNodeList_Patch_StartpageConfig = xmlNode_Patch_StartpageConfig.ChildNodes

    '        oLogger.WriteToLogRelative("applying patch", , 3)
    '        Dim xmlNode_Patch_StartpageConfig_SubNode As Xml.XmlNode
    '        Dim xmlNode_StartpageConfigImport As Xml.XmlNode
    '        For Each xmlNode_Patch_StartpageConfig_SubNode In xmlNodeList_Patch_StartpageConfig
    '            xmlNode_StartpageConfigImport = xmlSkCfg.ImportNode(xmlNode_Patch_StartpageConfig_SubNode, True)
    '            xmlNode_StartpageConfig.AppendChild(xmlNode_StartpageConfigImport)
    '        Next


    '        '*************************************************************************************************************
    '        'SiteCash - replace current url zones
    '        oLogger.WriteToLogRelative("SiteCash url zones update", , 2)
    '        Dim xmlNodeList_Plugins As Xml.XmlNodeList
    '        Dim xmlNode_Plugin As Xml.XmlNode
    '        Dim xmlNode_SiteCash As Xml.XmlNode

    '        oLogger.WriteToLogRelative("loading SiteCash node", , 3)
    '        xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

    '        For Each xmlNode_Plugin In xmlNodeList_Plugins
    '            If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
    '                xmlNode_SiteCash = xmlNode_Plugin

    '                Exit For
    '            End If
    '        Next

    '        If Not xmlNode_SiteCash Is Nothing Then
    '            oLogger.WriteToLogRelative("ok", , 4)
    '        Else
    '            Throw New Exception("SiteCash node not found")
    '        End If

    '        '            oLogger.WriteToLogRelative("making sure SiteCash is enabled", , 3)
    '        '           xmlNode_SiteCash.Attributes.GetNamedItem("enabled").InnerText = "true"
    '        '
    '        '            oLogger.WriteToLogRelative("making applications free of charge", , 3)
    '        '          xmlNode_SiteCash.SelectSingleNode("sk:applicationprice", ns).Attributes.GetNamedItem("enabled").InnerText = "false"
    '        '         'xmlNode_SiteCash.SelectSingleNode("sk:applicationmultiplier", ns).Attributes.GetNamedItem("enabled").InnerText = "false"
    '        '
    '        '           oLogger.WriteToLogRelative("disabling time warning", , 3)
    '        '          xmlNode_SiteCash.SelectSingleNode("sk:warning-seconds", ns).Attributes.GetNamedItem("enabled").InnerText = "false"
    '        '
    '        '           oLogger.WriteToLogRelative("disabling progress bar", , 3)
    '        '          xmlNode_SiteCash.SelectSingleNode("sk:progressbar", ns).Attributes.GetNamedItem("display").InnerText = "false"


    '        Dim xmlNodeList_UrlZones As Xml.XmlNodeList
    '        Dim xmlNode_UrlZone As Xml.XmlNode
    '        Dim xmlNodeList_Urls As Xml.XmlNodeList
    '        Dim xmlNode_Url As Xml.XmlNode
    '        Dim xmlNode_UrlClone As Xml.XmlNode
    '        Dim xmlNode_UrlCloneClone As Xml.XmlNode

    '        oLogger.WriteToLogRelative("removing current url zones", , 3)
    '        xmlNodeList_UrlZones = xmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
    '        For Each xmlNode_UrlZone In xmlNodeList_UrlZones
    '            xmlNode_SiteCash.RemoveChild(xmlNode_UrlZone)
    '        Next

    '        Dim xmlNodeList_UrlZonesPatch As Xml.XmlNodeList
    '        Dim xmlNode_UrlZonesPatch As Xml.XmlNode
    '        oLogger.WriteToLogRelative("loading patch", , 3)
    '        xmlNodeList_UrlZonesPatch = xmlNode_Patch_Root.SelectSingleNode("sk:sitecash-plugin", ns).ChildNodes

    '        oLogger.WriteToLogRelative("applying patch", , 3)
    '        Dim xmlNode_SiteCashImport As Xml.XmlNode
    '        For Each xmlNode_UrlZonesPatch In xmlNodeList_UrlZonesPatch
    '            xmlNode_SiteCashImport = xmlSkCfg.ImportNode(xmlNode_UrlZonesPatch, True)
    '            xmlNode_SiteCash.AppendChild(xmlNode_SiteCashImport)
    '        Next

    '        oLogger.WriteToLogRelative("adding extra url zone", , 3)
    '        xmlNode_UrlZone = xmlNode_SiteCash.SelectSingleNode("sk:url-zone", ns).CloneNode(True)
    '        xmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = "No Charge (Extra)"

    '        xmlNodeList_Urls = xmlNode_UrlZone.SelectNodes("sk:url", ns)
    '        xmlNode_UrlClone = xmlNode_UrlZone.SelectSingleNode("sk:url", ns).CloneNode(True)
    '        For Each xmlNode_Url In xmlNodeList_Urls
    '            xmlNode_UrlZone.RemoveChild(xmlNode_Url)
    '        Next

    '        oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Hotel), , 4)
    '        xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
    '        xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Hotel)
    '        xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
    '        xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
    '        xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Hotel)
    '        xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)

    '        oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Weather), , 4)
    '        xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
    '        xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Weather)
    '        xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
    '        xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
    '        xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Weather)
    '        xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)

    '        oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Map), , 4)
    '        xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
    '        xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Map)
    '        xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)
    '        xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
    '        xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Map)
    '        xmlNode_UrlZone.AppendChild(xmlNode_UrlCloneClone)

    '        xmlNode_SiteCash.AppendChild(xmlNode_UrlZone)

    '        '*************************************************************************************************************
    '        'Printing
    '        Dim xmlNode_Printing As Xml.XmlNode

    '        oLogger.WriteToLogRelative("Printing update", , 2)
    '        xmlNode_Printing = xmlNode_Root.SelectSingleNode("sk:printing", ns)

    '        oLogger.WriteToLogRelative("enabling print button", , 3)
    '        xmlNode_Printing.SelectSingleNode("sk:show-print-button", ns).InnerText = "true"

    '        oLogger.WriteToLogRelative("setting print mode", , 3)
    '        xmlNode_Printing.SelectSingleNode("sk:mode", ns).InnerText = "1"

    '        oLogger.WriteToLogRelative("disabling print monitoring", , 3)
    '        xmlNode_Printing.SelectSingleNode("sk:enable-monitoring", ns).InnerText = "false"


    '        '*************************************************************************************************************
    '        'Screensaver
    '        Dim xmlNode_Screensaver As Xml.XmlNode

    '        oLogger.WriteToLogRelative("Screensaver update", , 2)
    '        xmlNode_Screensaver = xmlNode_Root.SelectSingleNode("sk:screensaver", ns)

    '        oLogger.WriteToLogRelative("making sure screensaver is enabled", , 3)
    '        xmlNode_Screensaver.Attributes.GetNamedItem("enabled").InnerText = "true"

    '        oLogger.WriteToLogRelative("setting interval to 3600 (hack)", , 3)
    '        xmlNode_Screensaver.SelectSingleNode("sk:interval", ns).InnerText = "3600"

    '        oLogger.WriteToLogRelative("setting screensaver", , 3)
    '        If IO.File.Exists("C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\" & g_UIname & "\screensaver\index.html") Then
    '            xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\" & g_UIname & "\screensaver\index.html"
    '        ElseIf IO.File.Exists("C:\Program Files\SiteKiosk\Skins\Public\StartPages\" & g_UIname & "\screensaver\index.html") Then
    '            xmlNode_Screensaver.SelectSingleNode("sk:url", ns).InnerText = "file://C:\Program Files\SiteKiosk\Skins\Public\StartPages\" & g_UIname & "\screensaver\index.html"
    '        Else
    '            oLogger.WriteToLogRelative("nothing found for " & g_UIname, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
    '        End If


    '        '*************************************************************************************************************
    '        'Logout navigation
    '        Dim xmlNode_BrowserBar As Xml.XmlNode
    '        Dim xmlNode_Logout As Xml.XmlNode

    '        oLogger.WriteToLogRelative("Logout navigation update", , 2)
    '        xmlNode_BrowserBar = xmlNode_Root.SelectSingleNode("sk:browserbar", ns)
    '        xmlNode_Logout = xmlNode_BrowserBar.SelectSingleNode("sk:logout-navigation", ns)

    '        oLogger.WriteToLogRelative("making sure logout navigation is enabled", , 3)
    '        xmlNode_Logout.Attributes.GetNamedItem("enabled").InnerText = "true"

    '        oLogger.WriteToLogRelative("setting logout navigation", , 3)
    '        If IO.File.Exists("C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\" & g_UIname & "\logout.html") Then
    '            xmlNode_Logout.SelectSingleNode("sk:url", ns).InnerText = "file://C:\Program Files (x86)\SiteKiosk\Skins\Public\StartPages\" & g_UIname & "\logout.html"
    '        ElseIf IO.File.Exists("C:\Program Files\SiteKiosk\Skins\Public\StartPages\" & g_UIname & "\logout.html") Then
    '            xmlNode_Logout.SelectSingleNode("sk:url", ns).InnerText = "file://C:\Program Files\SiteKiosk\Skins\Public\StartPages\" & g_UIname & "\logout.html"
    '        Else
    '            oLogger.WriteToLogRelative("nothing found for " & g_UIname, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
    '        End If


    '        '*************************************************************************************************************
    '        'Saving
    '        oLogger.WriteToLogRelative("saving", , 2)
    '        oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
    '        xmlSkCfg.Save(mNewSkCfgFile)
    '        oLogger.WriteToLogRelative("ok", , 4)

    '        If Not bOverWriteOldFile Then
    '            oLogger.WriteToLogRelative("activating", , 2)
    '            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

    '            Dim sTempRegVal As String
    '            sTempRegVal = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\PROVISIO\SiteKiosk", "LastCfg", "")

    '            If sTempRegVal = mNewSkCfgFile Then
    '                oLogger.WriteToLogRelative("ok", , 3)
    '            Else
    '                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_WARNING, 3)

    '                oLogger.WriteToLogRelative("trying to fix this by overwriting the existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 4)

    '                oLogger.WriteToLogRelative("backing up existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
    '                oFile = New IO.FileInfo(mOldSkCfgFile)
    '                oFile.CopyTo(mOldSkCfgFile & "." & sDateString & ".HiltonnewUI.backup", True)

    '                oLogger.WriteToLogRelative("overwriting existing config", Logger.MESSAGE_TYPE.LOG_WARNING, 5)
    '                oFile = New IO.FileInfo(mNewSkCfgFile)
    '                oFile.CopyTo(mOldSkCfgFile, True)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
    '        oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
    '    End Try
    'End Sub

    Public Sub HideFreshInstallDialogs()
        ' "ImportantSettingsUndesired"=dword:00000001
        ' "LastHistoryVersion"="8.91.3042"
        ' "48159"=dword:00000001
        ' "HideConfigWarnDlg"=dword:00000001
        ' "HideBrowserEngineInfo"=dword:00000001
        oLogger.WriteToLogRelative("ImportantSettingsUndesired", , 1)
        oLogger.WriteToLogRelative("1", , 2)
        Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK & "\Dlgs", "ImportantSettingsUndesired", 1, Microsoft.Win32.RegistryValueKind.DWord)

        oLogger.WriteToLogRelative("LastHistoryVersion", , 1)
        oLogger.WriteToLogRelative(mSkBuild, , 2)
        Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK & "\Dlgs", "LastHistoryVersion", mSkBuild, Microsoft.Win32.RegistryValueKind.String)

        oLogger.WriteToLogRelative("48159", , 1)
        oLogger.WriteToLogRelative("1", , 2)
        Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK & "\Dlgs", "48159", 1, Microsoft.Win32.RegistryValueKind.DWord)

        oLogger.WriteToLogRelative("HideConfigWarnDlg", , 1)
        oLogger.WriteToLogRelative("1", , 2)
        Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK & "\Dlgs", "HideConfigWarnDlg", 1, Microsoft.Win32.RegistryValueKind.DWord)

        oLogger.WriteToLogRelative("HideBrowserEngineInfo", , 1)
        oLogger.WriteToLogRelative("1", , 2)
        Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK & "\Dlgs", "HideBrowserEngineInfo", 1, Microsoft.Win32.RegistryValueKind.DWord)
    End Sub

    Public Sub CheckForExternalLicense()
        mSiteKioskLicense_Licensee = ""
        mSiteKioskLicense_Signature = ""

        oLogger.WriteToLogRelative("Is there an external license code?", , 1)

        Dim sFile As String, sRegion As String

        Try
            Dim sSiteKioskLicenseName As String

            sSiteKioskLicenseName = Microsoft.Win32.Registry.GetValue(cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseName", "")
            If sSiteKioskLicenseName <> "" Then
                oLogger.WriteToLogRelative("found license in registry", , 2)

                mSiteKioskLicense_Licensee = Microsoft.Win32.Registry.GetValue(cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseLicensee", "")
                mSiteKioskLicense_Signature = Microsoft.Win32.Registry.GetValue(cREGKEY_LPCSOFTWARE & "\Config", "SiteKioskLicenseSignature", "")

                oLogger.WriteToLogRelative("licensee", , 3)
                oLogger.WriteToLogRelative(mSiteKioskLicense_Licensee, , 4)
                oLogger.WriteToLogRelative("signature", , 3)
                oLogger.WriteToLogRelative(mSiteKioskLicense_Signature, , 4)

                If Not mSiteKioskLicense_Licensee.Equals("") And Not mSiteKioskLicense_Signature.Equals("") Then
                    oLogger.WriteToLogRelative("ok", , 3)

                    Exit Sub
                End If
            Else
                oLogger.WriteToLogRelative("found no license in registry", , 2)
            End If

        Catch ex As Exception
            oLogger.WriteToLogRelative("error while retrieving license info from registry", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative("Exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try


        Try
            sFile = Microsoft.Win32.Registry.GetValue(cREGKEY_LPCSOFTWARE & "\Config", "LicenseFile", "")

            If IO.File.Exists(sFile) Then
                oLogger.WriteToLogRelative("found path in registry", , 2)
                oLogger.WriteToLogRelative(sFile, , 3)
            Else
                oLogger.WriteToLogRelative("found path in registry, but it doesn't exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative(sFile, , 3)
                oLogger.WriteToLogRelative("using default", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                sFile = c_SITEKIOSK_LICENSE_FILE
                oLogger.WriteToLogRelative(sFile, , 3)
            End If
        Catch ex As Exception
            sFile = c_SITEKIOSK_LICENSE_FILE
        End Try

        'Get license from registry
        Try
            sRegion = Microsoft.Win32.Registry.GetValue(cREGKEY_LPCSOFTWARE & "\Config", "License", "")

            If sRegion.Equals("") Then
                oLogger.WriteToLogRelative("found no license region in registry", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
                oLogger.WriteToLogRelative("using default", , 3)
                sRegion = "default"
            Else
                oLogger.WriteToLogRelative("found license region in registry", , 2)
                oLogger.WriteToLogRelative(sRegion, , 3)
            End If

            sRegion = sRegion.Replace("-", "")
        Catch ex As Exception
            oLogger.WriteToLogRelative("error while retrieving license region from registry", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative("Exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLogRelative("using default", , 3)
            sRegion = "default"
        End Try


        If IO.File.Exists(sFile) Then
            oLogger.WriteToLogRelative("opening xml file", , 2)

            Try
                Dim xmlSkCfg As Xml.XmlDocument
                Dim xmlNode_Root As Xml.XmlNode
                Dim xmlNodes_License As Xml.XmlNodeList
                Dim xmlNode_License As Xml.XmlNode
                Dim xmlNode_Licensee As Xml.XmlNode
                Dim xmlNode_Signature As Xml.XmlNode
                Dim sTmpRegion As String

                xmlSkCfg = New Xml.XmlDocument

                oLogger.WriteToLogRelative("reading", , 3)
                oLogger.WriteToLogRelative(sFile, , 4)
                xmlSkCfg.Load(sFile)
                oLogger.WriteToLogRelative("ok", , 5)

                oLogger.WriteToLogRelative("root node", , 4)
                xmlNode_Root = xmlSkCfg.SelectSingleNode("sitekiosk-license")
                oLogger.WriteToLogRelative("ok", , 5)

                oLogger.WriteToLogRelative("license nodelist", , 4)
                xmlNodes_License = xmlNode_Root.SelectNodes("license")
                oLogger.WriteToLogRelative("ok", , 5)

                For Each xmlNode_License In xmlNodes_License
                    sTmpRegion = xmlNode_License.Attributes.GetNamedItem("region").Value.ToLower

                    oLogger.WriteToLogRelative("license node for region '" & sTmpRegion & "'", , 4)

                    If xmlNode_License.Attributes.GetNamedItem("region").Value = sRegion Then
                        oLogger.WriteToLogRelative("ok", , 5)

                        xmlNode_Licensee = xmlNode_License.SelectSingleNode("licensee")
                        xmlNode_Signature = xmlNode_License.SelectSingleNode("signature")

                        mSiteKioskLicense_Licensee = xmlNode_Licensee.InnerText
                        mSiteKioskLicense_Signature = xmlNode_Signature.InnerText
                    Else
                        oLogger.WriteToLogRelative("this is not the license you are looking for", , 5)
                    End If
                Next

                oLogger.WriteToLogRelative("found license", , 3)
                oLogger.WriteToLogRelative("licensee", , 4)
                oLogger.WriteToLogRelative(mSiteKioskLicense_Licensee, , 5)
                oLogger.WriteToLogRelative("signature", , 4)
                oLogger.WriteToLogRelative(mSiteKioskLicense_Signature, , 5)

            Catch ex As Exception
                oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End Try

        Else
            oLogger.WriteToLogRelative("nope, internal license code used", , 2)
        End If
    End Sub

    Public Sub UpdateLicense()
        'We need to find out which license to use

        oLogger.WriteToLogRelative("writing license", , 1)
        If Not mSiteKioskLicense_Licensee.Equals("") And Not mSiteKioskLicense_Signature.Equals("") Then
            oLogger.WriteToLogRelative("licensee", , 2)
            oLogger.WriteToLogRelative(mSiteKioskLicense_Licensee, , 3)
            oLogger.WriteToLogRelative("signature", , 2)
            oLogger.WriteToLogRelative(mSiteKioskLicense_Signature, , 3)

            Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK, "Licensee", mSiteKioskLicense_Licensee, Microsoft.Win32.RegistryValueKind.String)
            Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK, "Signature", mSiteKioskLicense_Signature, Microsoft.Win32.RegistryValueKind.String)
        Else
            If mSiteKioskVersion = 9 Then
                oLogger.WriteToLogRelative("licensee", , 2)
                oLogger.WriteToLogRelative(c_LICENSE_SITEKIOSK9_LICENSEE, , 3)
                oLogger.WriteToLogRelative("signature", , 2)
                oLogger.WriteToLogRelative(c_LICENSE_SITEKIOSK9_SIGNATURE, , 3)

                Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK, "Licensee", c_LICENSE_SITEKIOSK9_LICENSEE, Microsoft.Win32.RegistryValueKind.String)
                Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK, "Signature", c_LICENSE_SITEKIOSK9_SIGNATURE, Microsoft.Win32.RegistryValueKind.String)
            Else
                oLogger.WriteToLogRelative("licensee", , 2)
                oLogger.WriteToLogRelative(c_LICENSE_SITEKIOSK8_LICENSEE, , 3)
                oLogger.WriteToLogRelative("signature", , 2)
                oLogger.WriteToLogRelative(c_LICENSE_SITEKIOSK8_SIGNATURE, , 3)

                Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK, "Licensee", c_LICENSE_SITEKIOSK8_LICENSEE, Microsoft.Win32.RegistryValueKind.String)
                Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK, "Signature", c_LICENSE_SITEKIOSK8_SIGNATURE, Microsoft.Win32.RegistryValueKind.String)
            End If
        End If
    End Sub

    'Public Sub ActivateHilton()
    '    If Not g_Force_Hilton Then
    '        Exit Sub
    '    End If


    'End Sub

    Public Sub AddPagesToFreezone()
        Try
            Dim nt As Xml.XmlNameTable
            Dim ns As Xml.XmlNamespaceManager
            Dim xmlSkCfg As Xml.XmlDocument
            Dim xmlNode_Root As Xml.XmlNode


            xmlSkCfg = New Xml.XmlDocument

            oLogger.WriteToLogRelative("updating", , 1)

            oLogger.WriteToLogRelative("loading", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Load(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)


            nt = xmlSkCfg.NameTable
            ns = New Xml.XmlNamespaceManager(nt)
            ns.AddNamespace("sk", "urn:schemas-sitekiosk-com:configuration")
            ns.AddNamespace("dt", "urn:schemas-microsoft-com:datatypes")

            oLogger.WriteToLogRelative("loading root node", , 2)
            xmlNode_Root = xmlSkCfg.SelectSingleNode("sk:sitekiosk-configuration", ns)

            If Not xmlNode_Root Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                Throw New Exception("sitekiosk-configuration node not found")
            End If


            'If g_Force_Hilton Then
            '    Dim xmlNode_Screensaver As Xml.XmlNode
            '    Dim xmlNode_ScreensaverUrl As Xml.XmlNode

            '    oLogger.WriteToLogRelative("loading screensaver node", , 3)
            '    xmlNode_Screensaver = xmlNode_Root.SelectSingleNode("sk:screensaver", ns)

            '    oLogger.WriteToLogRelative("loading url node", , 3)
            '    xmlNode_ScreensaverUrl = xmlNode_Screensaver.SelectSingleNode("sk:url", ns)

            '    xmlNode_ScreensaverUrl.InnerText = "file://%SiteKioskPath%\Skins\Public\Startpages\Hilton\screensaver\index.html"
            'End If

            '*************************************************************************************************************
            'SiteCash - replace current url zones
            oLogger.WriteToLogRelative("SiteCash url zones update", , 2)
            Dim xmlNodeList_Plugins As Xml.XmlNodeList
            Dim xmlNode_Plugin As Xml.XmlNode
            Dim xmlNode_SiteCash As Xml.XmlNode

            oLogger.WriteToLogRelative("loading SiteCash node", , 3)
            xmlNodeList_Plugins = xmlNode_Root.SelectNodes("sk:plugin", ns)

            For Each xmlNode_Plugin In xmlNodeList_Plugins
                If xmlNode_Plugin.Attributes.GetNamedItem("name").InnerText = "SiteCash" Then
                    xmlNode_SiteCash = xmlNode_Plugin

                    Exit For
                End If
            Next

            If Not xmlNode_SiteCash Is Nothing Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                Throw New Exception("SiteCash node not found")
            End If



            Dim xmlNodeList_UrlZones As Xml.XmlNodeList
            Dim xmlNode_UrlZone As Xml.XmlNode
            Dim xmlNodeList_Urls As Xml.XmlNodeList
            Dim xmlNode_Url As Xml.XmlNode
            Dim xmlNode_UrlClone As Xml.XmlNode
            Dim xmlNode_UrlCloneClone As Xml.XmlNode
            Dim xmlNode_FreeUrlZone As Xml.XmlNode

            oLogger.WriteToLogRelative("removing '" & c_FreeUrlZoneName & "' url zone, if there", , 3)
            xmlNodeList_UrlZones = xmlNode_SiteCash.SelectNodes("sk:url-zone", ns)
            For Each xmlNode_UrlZone In xmlNodeList_UrlZones
                If xmlNode_UrlZone.SelectSingleNode("sk:name", ns).InnerText = c_FreeUrlZoneName Then
                    xmlNode_SiteCash.RemoveChild(xmlNode_UrlZone)
                    oLogger.WriteToLogRelative("ok", , 4)
                End If
            Next

            oLogger.WriteToLogRelative("adding '" & c_FreeUrlZoneName & "' url zone", , 3)
            xmlNode_FreeUrlZone = xmlNode_SiteCash.SelectSingleNode("sk:url-zone", ns).CloneNode(True)
            xmlNode_FreeUrlZone.SelectSingleNode("sk:name", ns).InnerText = c_FreeUrlZoneName

            xmlNodeList_Urls = xmlNode_FreeUrlZone.SelectNodes("sk:url", ns)
            xmlNode_UrlClone = xmlNode_FreeUrlZone.SelectSingleNode("sk:url", ns).CloneNode(True)
            For Each xmlNode_Url In xmlNodeList_Urls
                xmlNode_FreeUrlZone.RemoveChild(xmlNode_Url)
            Next

            oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Hotel), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Hotel)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Hotel)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Weather), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Weather)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Weather)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            oLogger.WriteToLogRelative("adding: " & GetHostnameFromUrl(g_Url_Map), , 4)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "http://" & GetHostnameFromUrl(g_Url_Map)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)
            xmlNode_UrlCloneClone = xmlNode_UrlClone.CloneNode(True)
            xmlNode_UrlCloneClone.InnerText = "https://" & GetHostnameFromUrl(g_Url_Map)
            xmlNode_FreeUrlZone.AppendChild(xmlNode_UrlCloneClone)

            xmlNode_SiteCash.AppendChild(xmlNode_FreeUrlZone)

            oLogger.WriteToLogRelative("saving", , 2)
            oLogger.WriteToLogRelative(mNewSkCfgFile, , 3)
            xmlSkCfg.Save(mNewSkCfgFile)
            oLogger.WriteToLogRelative("ok", , 4)
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)

        End Try
    End Sub

    Public Sub ActivateNewSkCfg()
        Try
            oLogger.WriteToLogRelative("finding SiteKiosk install directory", , 1)
            oLogger.WriteToLogRelative("found: " & mSiteKioskFolder, , 2)

            oLogger.WriteToLogRelative("finding current active skcfg file", , 1)
            oLogger.WriteToLogRelative("found: " & mOldSkCfgFile, , 2)




            '*************************************************************************************************************
            'Saving
            oLogger.WriteToLogRelative("activating new file", , 1)
            oLogger.WriteToLogRelative("file: " & mNewSkCfgFile, , 2)
            Microsoft.Win32.Registry.SetValue(c_REGROOT_SITEKIOSK, "LastCfg", mNewSkCfgFile, Microsoft.Win32.RegistryValueKind.String)

            Dim sTempRegVal As String
            sTempRegVal = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "LastCfg", "This is just a stupid string")

            If sTempRegVal = mNewSkCfgFile Then
                oLogger.WriteToLogRelative("ok", , 3)
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("file is: " & sTempRegVal, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("should be: " & mNewSkCfgFile, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("ERROR!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        End Try
    End Sub

    Public Sub New()
        mSiteKioskFolder = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "InstallDir", "")
        mOldSkCfgFile = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "LastCfg", "")
        mSkBuild = Microsoft.Win32.Registry.GetValue(c_REGROOT_SITEKIOSK, "Build", "")
        mSiteKioskLicense_Licensee = ""
        mSiteKioskLicense_Signature = ""

        Try
            Dim versionPart As String = mSkBuild.Substring(0, 2).Replace(".", "")
            mSiteKioskVersion = Integer.Parse(versionPart)
        Catch ex As Exception
            mSiteKioskVersion = -1
        End Try
    End Sub

    Public Function GetHostnameFromUrl(ByVal sUrl As String) As String
        Dim u As New Uri(sUrl)

        Return u.Host
    End Function

End Class
