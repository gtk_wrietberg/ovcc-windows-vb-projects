Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SiteKioskUpdater")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("GuestTek")> 
<Assembly: AssemblyProduct("SiteKioskUpdater")> 
<Assembly: AssemblyCopyright("Copyright ©  2015")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f15c5bf3-59f9-4bf3-8ebf-fd46ee21f9bf")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.1.23353.1613")>
<Assembly: AssemblyFileVersion("2.1.23353.1613")>

'<Assembly: AssemblyFileVersion("1.0.0.0")> 

<assembly: AssemblyInformationalVersion("0.0.23353.1613")>