﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.listviewFiles = New System.Windows.Forms.ListView()
        Me.colHeader_Name = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Date = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Type = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeader_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.colHeaderHidden_Size = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.treeFolders = New System.Windows.Forms.TreeView()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(58, 585)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'listviewFiles
        '
        Me.listviewFiles.BackColor = System.Drawing.Color.White
        Me.listviewFiles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.listviewFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colHeader_Name, Me.colHeader_Date, Me.colHeader_Type, Me.colHeader_Size, Me.colHeaderHidden_Size})
        Me.listviewFiles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listviewFiles.Location = New System.Drawing.Point(257, 12)
        Me.listviewFiles.MultiSelect = False
        Me.listviewFiles.Name = "listviewFiles"
        Me.listviewFiles.Size = New System.Drawing.Size(763, 371)
        Me.listviewFiles.TabIndex = 4
        Me.listviewFiles.UseCompatibleStateImageBehavior = False
        Me.listviewFiles.View = System.Windows.Forms.View.Details
        '
        'colHeader_Name
        '
        Me.colHeader_Name.Text = "Name"
        Me.colHeader_Name.Width = 400
        '
        'colHeader_Date
        '
        Me.colHeader_Date.Text = "Date modified"
        Me.colHeader_Date.Width = 120
        '
        'colHeader_Type
        '
        Me.colHeader_Type.Text = "Type"
        Me.colHeader_Type.Width = 160
        '
        'colHeader_Size
        '
        Me.colHeader_Size.Text = "Size"
        '
        'colHeaderHidden_Size
        '
        Me.colHeaderHidden_Size.Text = ""
        Me.colHeaderHidden_Size.Width = 0
        '
        'treeFolders
        '
        Me.treeFolders.BackColor = System.Drawing.Color.White
        Me.treeFolders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.treeFolders.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText
        Me.treeFolders.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeFolders.HideSelection = False
        Me.treeFolders.Indent = 20
        Me.treeFolders.ItemHeight = 20
        Me.treeFolders.Location = New System.Drawing.Point(12, 12)
        Me.treeFolders.Name = "treeFolders"
        Me.treeFolders.ShowLines = False
        Me.treeFolders.ShowRootLines = False
        Me.treeFolders.Size = New System.Drawing.Size(242, 371)
        Me.treeFolders.TabIndex = 3
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1349, 620)
        Me.Controls.Add(Me.listviewFiles)
        Me.Controls.Add(Me.treeFolders)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents listviewFiles As ListView
    Friend WithEvents colHeader_Name As ColumnHeader
    Friend WithEvents colHeader_Date As ColumnHeader
    Friend WithEvents colHeader_Type As ColumnHeader
    Friend WithEvents colHeader_Size As ColumnHeader
    Friend WithEvents colHeaderHidden_Size As ColumnHeader
    Friend WithEvents treeFolders As TreeView
End Class
