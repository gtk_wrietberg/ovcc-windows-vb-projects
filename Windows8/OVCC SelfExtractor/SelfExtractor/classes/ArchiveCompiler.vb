﻿Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.IO.Compression
Imports Microsoft.CSharp
Imports System.CodeDom.Compiler
Imports System.Diagnostics
Imports System.Reflection
Imports System.Windows.Forms

Namespace ArchiveCompiler
    Class SelfExtractor
        Implements IDisposable

        Protected ReadOnly FilesToCompile As String() = {
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "My Project\AssemblyInfo.vb"),
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\Shared_Constants.vb"),
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\StreamString.vb"),
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\Settings.vb"),
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "classes\shared\FileList.vb"),
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\frmUnpack.Designer.vb"),
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\frmUnpack.vb"),
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\modMain.vb")
        }

        Protected ReadOnly FilesToEmbed As String() = {
            Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "_selfextractor_src\frmUnpack.resx")
        }

        'Protected ReadOnly sourceName As String = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "modMain_external.vb")
        Protected filenames As List(Of String) = New List(Of String)()


        Public Sub AddSettingsFile()
            Using file As Stream = StreamString.StringToStream(Settings.Dump)
                Dim buffer As Byte() = New Byte(file.Length - 1) {}
                If file.Length <> file.Read(buffer, 0, buffer.Length) Then Throw New IOException("Unable to read settings")

                Dim fi_gz As New IO.FileInfo(Shared_Constants.SettingsFileName)
                Using gzFile As Stream = fi_gz.Create()
                    'Using gzip As Stream = New GZipStream(gzFile, CompressionMode.Compress)
                    Using gzip As Stream = New GZipStream(gzFile, CompressionLevel.NoCompression)
                        gzip.Write(buffer, 0, buffer.Length)
                    End Using
                End Using
            End Using

            Console.WriteLine(Shared_Constants.SettingsFileName)

            filenames.Add(Shared_Constants.SettingsFileName)

            'FileList.Add(Shared_Constants.SettingsFileName, "bla")
        End Sub

        Public Sub AddFileListFile()
            Using file As Stream = StreamString.StringToStream(FileList.Dump)
                Dim buffer As Byte() = New Byte(file.Length - 1) {}
                If file.Length <> file.Read(buffer, 0, buffer.Length) Then Throw New IOException("Unable to read settings")

                Dim fi_gz As New IO.FileInfo(Shared_Constants.FileListFileName)
                Using gzFile As Stream = fi_gz.Create()
                    'Using gzip As Stream = New GZipStream(gzFile, CompressionMode.Compress)
                    Using gzip As Stream = New GZipStream(gzFile, CompressionLevel.NoCompression)
                        gzip.Write(buffer, 0, buffer.Length)
                    End Using
                End Using
            End Using

            Console.WriteLine(Shared_Constants.FileListFileName)

            filenames.Add(Shared_Constants.FileListFileName)
        End Sub

        Public Function AddFile(ByVal filename As String, ByVal path As String) As Boolean
            Return AddFile(filename, path, False, "")
        End Function

        Public Function AddFile(ByVal filename As String, ByVal path As String, exec As Boolean, params As String) As Boolean
            'Using file As Stream = file.OpenRead(filename)

            Try
                If Not IO.File.Exists(filename) Then
                    Throw New IOException("'" & filename & "' not found!")
                End If


                Dim fi As New IO.FileInfo(filename)

                Using file As Stream = fi.OpenRead
                    Dim buffer As Byte() = New Byte(file.Length - 1) {}
                    If file.Length <> file.Read(buffer, 0, buffer.Length) Then Throw New IOException("Unable to read " & filename)

                    Dim fi_gz As New IO.FileInfo(filename & ".gz")
                    Using gzFile As Stream = fi_gz.Create()
                        'Using gzip As Stream = New GZipStream(gzFile, CompressionMode.Compress)
                        Using gzip As Stream = New GZipStream(gzFile, CompressionLevel.NoCompression)
                            gzip.Write(buffer, 0, buffer.Length)
                        End Using
                    End Using
                End Using

                Console.WriteLine(filename & ".gz")

                filenames.Add(filename & ".gz")

                FileList.Add(filename, path, fi.Length, exec, params)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Sub CompileArchive(ByVal archiveFilename As String)
            CompileArchive(archiveFilename, False, Nothing)
        End Sub

        Public Sub CompileArchive(ByVal archiveFilename As String, ByVal iconFilename As String)
            CompileArchive(archiveFilename, False, iconFilename)
        End Sub

        Public Sub CompileArchive(ByVal archiveFilename As String, ByVal run1stItem As Boolean, ByVal iconFilename As String)
            Dim vbCP As New VBCodeProvider
            Dim cp As New CompilerParameters


            'Dim vbCP As CodeDomProvider = New CSharpCodeProvider()
            'Dim cp As CompilerParameters = New CompilerParameters()

            cp.GenerateExecutable = True
            cp.OutputAssembly = archiveFilename
            cp.CompilerOptions = "/target:winexe"


            If run1stItem Then
                cp.CompilerOptions &= " /define:RUN_1ST_ITEM"
            End If

            If Not String.IsNullOrEmpty(iconFilename) Then
                cp.CompilerOptions &= " /win32icon:" & iconFilename
            End If

            cp.ReferencedAssemblies.Add("System.dll")
            cp.ReferencedAssemblies.Add("System.Collections.dll")
            cp.ReferencedAssemblies.Add("System.Drawing.dll")
            cp.ReferencedAssemblies.Add("System.Windows.Forms.dll")
            'cp.LinkedResources.AddRange(FilesToEmbed)
            cp.EmbeddedResources.AddRange(filenames.ToArray())



            Dim cr As CompilerResults = vbCP.CompileAssemblyFromFile(cp, FilesToCompile)

            If cr.Errors.Count > 0 Then
                Dim msg As String = "Errors building " & cr.PathToAssembly

                For Each ce As CompilerError In cr.Errors
                    msg += Environment.NewLine & ce.ToString()
                Next

                'Throw New ApplicationException(msg)
                MessageBox.Show(msg)
            End If
        End Sub

        Public Sub Dispose() Implements System.IDisposable.Dispose
            For Each path As String In filenames
                File.Delete(path)
            Next

            filenames.Clear()
            GC.SuppressFinalize(Me)
        End Sub
    End Class
End Namespace
