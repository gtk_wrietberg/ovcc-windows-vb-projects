﻿Imports System.Windows.Forms
Imports System.Drawing

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmUnpack
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtProgress = New System.Windows.Forms.TextBox()
        Me.bgWorker_Unpack = New System.ComponentModel.BackgroundWorker()
        Me.txtFileList = New System.Windows.Forms.TextBox()
        Me.txtSettings = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 12)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(208, 155)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "load config"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtProgress
        '
        Me.txtProgress.Location = New System.Drawing.Point(445, 375)
        Me.txtProgress.Multiline = True
        Me.txtProgress.Name = "txtProgress"
        Me.txtProgress.Size = New System.Drawing.Size(482, 129)
        Me.txtProgress.TabIndex = 1
        '
        'bgWorker_Unpack
        '
        Me.bgWorker_Unpack.WorkerReportsProgress = True
        '
        'txtFileList
        '
        Me.txtFileList.Location = New System.Drawing.Point(527, 12)
        Me.txtFileList.Multiline = True
        Me.txtFileList.Name = "txtFileList"
        Me.txtFileList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFileList.Size = New System.Drawing.Size(482, 129)
        Me.txtFileList.TabIndex = 2
        '
        'txtSettings
        '
        Me.txtSettings.Location = New System.Drawing.Point(226, 12)
        Me.txtSettings.Multiline = True
        Me.txtSettings.Name = "txtSettings"
        Me.txtSettings.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSettings.Size = New System.Drawing.Size(295, 129)
        Me.txtSettings.TabIndex = 3
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(28, 323)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(208, 155)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "unpack"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'frmUnpack
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1256, 516)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.txtSettings)
        Me.Controls.Add(Me.txtFileList)
        Me.Controls.Add(Me.txtProgress)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmUnpack"
        Me.Text = "frmUnpack"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents txtProgress As TextBox
    Friend WithEvents bgWorker_Unpack As System.ComponentModel.BackgroundWorker
    Friend WithEvents txtFileList As TextBox
    Friend WithEvents txtSettings As TextBox
    Friend WithEvents Button2 As Button
End Class
