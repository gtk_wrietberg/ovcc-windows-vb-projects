Module Constants___Globals
    'Constants
    Public c__SetProxyServer_App As String = "SetProxyServer.exe"

    Public c__SquidInstallScript_Cmd As String = "install_script\squid_install.cmd"

    Public c__SetProxyServer_App_Log_Path As String = "SetProxyServer"
    Public c__SetProxyServer_App_Log_Name As String = "SetProxyServer.log"

    'Registry
    Public Const cPolicy_Key__ProxySettingsPerUser As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings "
    Public Const cPolicy_Name__ProxySettingsPerUser As String = "ProxySettingsPerUser"
    Public Const cPolicy_Value__ProxySettingsPerUser As Integer = 1

    Public Const cRun_Key__ProxySettings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run"
    Public Const cRun_Name__ProxySettings As String = "SetProxyServer"

    Public Const ciBAHN_Key__ProxySettings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\ProxySettings"
    Public Const ciBAHN_Name__ProxySetForUsers As String = "ProxySetForUsers"
    Public Const ciBAHN_Value__ProxySetForUsers As String = "sitekiosk"

    'Misc
    Public Const cSiteKiosk_Username As String = "woutertest"


    'Globals. Me like globals, me are lazy
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_LogFileName As String

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"


        IO.Directory.CreateDirectory(g_LogFileDirectory)
    End Sub
End Module
