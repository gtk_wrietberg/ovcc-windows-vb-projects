Module Main
    Private c__SiteKiosk_Username As String = "sitekiosk"
    Private c__SiteKiosk_Password As String = "provisio"

    Public Sub Main()
        Dim oRunas As New RunAs
        Dim sRunasArgs As String = ""

        If Environment.GetCommandLineArgs.Length <> 2 Then
            Console.WriteLine("incorrect params")
            Environment.Exit(-3)
        End If

        sRunasArgs = Environment.GetCommandLineArgs(1)


        Console.WriteLine("starting """ & sRunasArgs & """ as sitekiosk user...")

        oRunas.UserName = c__SiteKiosk_Username
        oRunas.Password = c__SiteKiosk_Password
        oRunas.CommandLine = sRunasArgs


        Dim iExitCode As UInt32
        iExitCode = 666
        Try
            iExitCode = oRunas.StartProcess()

            If iExitCode = 0 Then
                Console.WriteLine("done")
            Else
                Console.WriteLine("done, with errors")
            End If
        Catch ex As Exception
            Console.WriteLine("error: " & ex.ToString)
        End Try

        Environment.Exit(iExitCode)
    End Sub
End Module
