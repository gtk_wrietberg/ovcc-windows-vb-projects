Module Main
    Public Sub main()
        '------------------------------------------------------
        InitGlobals()
        g_LogInfoForEmailReport = New LogInfoForEmailReport

        '------------------------------------------------------
        'Logger initialisation
        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)


        '------------------------------------------------------
        'Disable SiteKiosk content filter
        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Update SiteKiosk config", , 0)
        g_LogInfoForEmailReport.SkCFG = oSkCfg.Update(True)


        '------------------------------------------------------
        'File copying
        oLogger.WriteToLog("Copying files", , 0)

        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oLogger.WriteToLog("Squid files", , 1)
        oCopyFiles.FileCounterReset()
        oCopyFiles.SourceDirectory = "files\Squid"
        oCopyFiles.DestinationDirectory = "C:\Squid"
        oCopyFiles.CopyFiles()
        g_LogInfoForEmailReport.FileCopy_Squid__Total = oCopyFiles.FileCounterTotal
        g_LogInfoForEmailReport.FileCopy_Squid__Success = oCopyFiles.FileCounterSuccess
        g_LogInfoForEmailReport.FileCopy_Squid__Failed = oCopyFiles.FileCounterFailed

        oLogger.WriteToLog("SquidGuard files", , 1)
        oCopyFiles.FileCounterReset()
        oCopyFiles.SourceDirectory = "files\SquidGuard"
        oCopyFiles.DestinationDirectory = "C:\usr\local\SquidGuard"
        oCopyFiles.CopyFiles()
        g_LogInfoForEmailReport.FileCopy_SquidGuard__Total = oCopyFiles.FileCounterTotal
        g_LogInfoForEmailReport.FileCopy_SquidGuard__Success = oCopyFiles.FileCounterSuccess
        g_LogInfoForEmailReport.FileCopy_SquidGuard__Failed = oCopyFiles.FileCounterFailed

        oLogger.WriteToLog("Misc tools and dlls", , 1)
        oCopyFiles.FileCounterReset()
        oCopyFiles.SourceDirectory = "files\Windows"
        oCopyFiles.DestinationDirectory = "C:\Windows\"
        oCopyFiles.CopyFiles()
        g_LogInfoForEmailReport.FileCopy_Windows__Total = oCopyFiles.FileCounterTotal
        g_LogInfoForEmailReport.FileCopy_Windows__Success = oCopyFiles.FileCounterSuccess
        g_LogInfoForEmailReport.FileCopy_Windows__Failed = oCopyFiles.FileCounterFailed

        oLogger.WriteToLog("Helpers", , 1)
        oCopyFiles.FileCounterReset()
        oCopyFiles.SourceDirectory = "helpers"
        oCopyFiles.DestinationDirectory = g_SetProxyServerDirectory
        oCopyFiles.CopyFiles()
        g_LogInfoForEmailReport.FileCopy_Squid__Total = oCopyFiles.FileCounterTotal
        g_LogInfoForEmailReport.FileCopy_Squid__Success = oCopyFiles.FileCounterSuccess
        g_LogInfoForEmailReport.FileCopy_Squid__Failed = oCopyFiles.FileCounterFailed

        Try
            oLogger.WriteToLog("Allowing SiteKiosk user read/execute rights for """ & g_SetProxyServerDirectory & """", , 1)
            AddDirectorySecurity(g_SetProxyServerDirectory, "sitekiosk", Security.AccessControl.FileSystemRights.ReadAndExecute, Security.AccessControl.AccessControlType.Allow)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
            oLogger.WriteToLog(ex.ToString, , 3)
        End Try

        Try
            Dim sTempPath As String
            sTempPath = g_SetProxyServerDirectory & "\" & c__SetProxyServer_App
            oLogger.WriteToLog("Allowing SiteKiosk user read/execute rights for """ & sTempPath & """", , 1)
            AddFileSecurity(sTempPath, "sitekiosk", Security.AccessControl.FileSystemRights.ReadAndExecute, Security.AccessControl.AccessControlType.Allow)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
            oLogger.WriteToLog(ex.ToString, , 3)
        End Try



        '------------------------------------------------------
        'Initialise and start Squid
        oLogger.WriteToLog("Starting Squid", , 0)

        oLogger.WriteToLog("starting installer script", , 1)
        oLogger.WriteToLog("script", , 2)
        oLogger.WriteToLog(c__SquidInstallScript_Cmd, , 3)

        Dim startinfo_SquidInstallScript As New Diagnostics.ProcessStartInfo(c__SquidInstallScript_Cmd)
        startinfo_SquidInstallScript.UseShellExecute = False
        startinfo_SquidInstallScript.CreateNoWindow = True
        startinfo_SquidInstallScript.RedirectStandardOutput = True
        startinfo_SquidInstallScript.RedirectStandardError = True

        ' Make the process and set its start information.
        Dim procSquidInstallerScript As New Diagnostics.Process()
        procSquidInstallerScript.StartInfo = startinfo_SquidInstallScript

        ' Start the process.
        procSquidInstallerScript.Start()

        ' Attach to stdout and stderr.
        Dim std_out_SquidInstallerScript As IO.StreamReader = procSquidInstallerScript.StandardOutput()
        Dim std_err_SquidInstallerScript As IO.StreamReader = procSquidInstallerScript.StandardError()

        ' Display the results.
        oLogger.WriteToLog("output (stderr)", , 2)
        g_LogInfoForEmailReport.InstallScript_err = std_err_SquidInstallerScript.ReadToEnd
        oLogger.WriteToLogWithoutDate(g_LogInfoForEmailReport.InstallScript_err)

        oLogger.WriteToLog("output (stdout)", , 2)
        g_LogInfoForEmailReport.InstallScript_log = std_out_SquidInstallerScript.ReadToEnd
        oLogger.WriteToLogWithoutDate(g_LogInfoForEmailReport.InstallScript_log)

        ' Clean up.
        std_out_SquidInstallerScript.Close()
        std_err_SquidInstallerScript.Close()
        procSquidInstallerScript.Close()


        '------------------------------------------------------
        'Set proxysetter in auto run, and enable it for sitekiosk user
        oLogger.WriteToLog("Set proxysetter in auto run, and enable it for sitekiosk user", , 0)
        oLogger.WriteToLog("make sure proxy settings are per user", , 1)

        Try
            oLogger.WriteToLog("registry", , 2)
            oLogger.WriteToLog("key", , 3)
            oLogger.WriteToLog(cPolicy_Key__ProxySettingsPerUser, , 4)
            oLogger.WriteToLog("name", , 3)
            oLogger.WriteToLog(cPolicy_Name__ProxySettingsPerUser, , 4)
            oLogger.WriteToLog("value", , 3)
            oLogger.WriteToLog(cPolicy_Value__ProxySettingsPerUser.ToString, , 4)

            Microsoft.Win32.Registry.SetValue(cPolicy_Key__ProxySettingsPerUser, cPolicy_Name__ProxySettingsPerUser, cPolicy_Value__ProxySettingsPerUser, Microsoft.Win32.RegistryValueKind.DWord)

            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
            oLogger.WriteToLog(ex.ToString, , 3)
        End Try

        oLogger.WriteToLog("Set proxysetter in auto run, and enable it for sitekiosk user", , 0)
        oLogger.WriteToLog("set proxysetter in auto run", , 1)

        Try
            oLogger.WriteToLog("registry", , 2)
            oLogger.WriteToLog("key", , 3)
            oLogger.WriteToLog(cRun_Key__ProxySettings, , 4)
            oLogger.WriteToLog("name", , 3)
            oLogger.WriteToLog(cRun_Name__ProxySettings, , 4)
            oLogger.WriteToLog("value", , 3)
            oLogger.WriteToLog(g_SetProxyServerDirectory & "\" & c__SetProxyServer_App, , 4)

            Microsoft.Win32.Registry.SetValue(cRun_Key__ProxySettings, cRun_Name__ProxySettings, """" & g_SetProxyServerDirectory & "\" & c__SetProxyServer_App & """")

            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
            oLogger.WriteToLog(ex.ToString, , 3)
        End Try


        oLogger.WriteToLog("enable it for sitekiosk user", , 1)
        Try
            oLogger.WriteToLog("registry", , 2)
            oLogger.WriteToLog("key", , 3)
            oLogger.WriteToLog(ciBAHN_Key__ProxySettings, , 4)
            oLogger.WriteToLog("name", , 3)
            oLogger.WriteToLog(ciBAHN_Name__ProxySetForUsers, , 4)
            oLogger.WriteToLog("value", , 3)
            oLogger.WriteToLog(ciBAHN_Value__ProxySetForUsers, , 4)

            Microsoft.Win32.Registry.SetValue(ciBAHN_Key__ProxySettings, ciBAHN_Name__ProxySetForUsers, ciBAHN_Value__ProxySetForUsers)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
            oLogger.WriteToLog(ex.ToString, , 3)
        End Try

        oLogger.WriteToLog("give sitekiosk user rights to change", , 1)
        ChangeRegistrySecurityForProxySetter()


        '------------------------------------------------------
        'Send report by email
        oLogger.WriteToLog("Sending email report", , 0)
        Try
            Dim oMail As New SendMail
            Dim sBody As String

            oMail.Server = c__SmtpServerName
            oMail.Port = c__SmtpServerPort
            oMail.Username = c__SmtpServerUserName
            oMail.Password = c__SmtpServerPassWord
            oMail.Sender = Environment.MachineName & "_noreply@lobbypc.com"
            oMail.Recipient = c__Recipient

            oMail.Subject = Environment.MachineName & " squid install report"

            oLogger.WriteToLog("composing body", , 1)
            sBody = My.Resources.DefaultMailBody
            sBody = sBody.Replace("%%MACHINENAME%%", Environment.MachineName)
            sBody = sBody.Replace("%%INSTALLERVERSION%%", myBuildInfo.FileVersion)
            sBody = sBody.Replace("%%EXECUTIONTIME%%", g_LogInfoForEmailReport.GetExecutionTime(LogInfoForEmailReport.ExecutionTimeUnits.Milliseconds))
            sBody = sBody.Replace("%%VERDICT%%", g_LogInfoForEmailReport.CalculateVerdict)
            sBody = sBody.Replace("%%RESULT_SKCFG%%", g_LogInfoForEmailReport.SkCFG.ToString)
            sBody = sBody.Replace("%%COPY_SQUID%%", g_LogInfoForEmailReport.FileCopy_Squid)
            sBody = sBody.Replace("%%COPY_SQUIDGUARD%%", g_LogInfoForEmailReport.FileCopy_SquidGuard)
            sBody = sBody.Replace("%%COPY_WINDOWS%%", g_LogInfoForEmailReport.FileCopy_Windows)
            sBody = sBody.Replace("%%COPY_HELPERS%%", g_LogInfoForEmailReport.FileCopy_helpers)
            sBody = sBody.Replace("%%STDERR_INSTALL_SCRIPT%%", g_LogInfoForEmailReport.InstallScript_err)
            sBody = sBody.Replace("%%STDOUT_INSTALL_SCRIPT%%", g_LogInfoForEmailReport.InstallScript_log)
            oMail.Body = sBody

            oLogger.WriteToLog("sending", , 1)
            oMail.SendEmail()
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.ToString", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(ex.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.InnerException.ToString", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(ex.InnerException.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        '------------------------------------------------------
        'Done
        oLogger.WriteToLog("Done", , 0)
        oLogger.WriteToLog("restarting", , 1)

        WindowsController.ExitWindows(RestartOptions.Reboot, True)
        oLogger.WriteToLog("bye", , 2)

        Environment.Exit(0)
    End Sub
End Module
