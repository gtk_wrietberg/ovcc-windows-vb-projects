Public Class LogInfoForEmailReport
    Private m_ExecutionTimeStart As DateTime
    Private m_ExecutionTimeSpan As TimeSpan
    Private m_SkCFG As Boolean
    Private m_FileCopy_Squid__Total As Integer
    Private m_FileCopy_Squid__Success As Integer
    Private m_FileCopy_Squid__Failed As Integer
    Private m_FileCopy_SquidGuard__Total As Integer
    Private m_FileCopy_SquidGuard__Success As Integer
    Private m_FileCopy_SquidGuard__Failed As Integer
    Private m_FileCopy_Windows__Total As Integer
    Private m_FileCopy_Windows__Success As Integer
    Private m_FileCopy_Windows__Failed As Integer
    Private m_FileCopy_Helpers__Total As Integer
    Private m_FileCopy_Helpers__Success As Integer
    Private m_FileCopy_Helpers__Failed As Integer
    Private m_InstallScript_err As String
    Private m_InstallScript_log As String

    Public Enum ExecutionTimeUnits
        Ticks = 0
        Milliseconds = 1
        Seconds = 2
        Minutes = 3
    End Enum

#Region "Properties"
    Public Property SkCFG() As Boolean
        Get
            Return m_SkCFG
        End Get
        Set(ByVal value As Boolean)
            m_SkCFG = value
        End Set
    End Property

    Public ReadOnly Property FileCopy_Squid() As String
        Get
            Return m_FileCopy_Squid__Total.ToString & "/" & m_FileCopy_Squid__Success & "/" & m_FileCopy_Squid__Failed
        End Get
    End Property

    Public Property FileCopy_Squid__Total() As Integer
        Get
            Return m_FileCopy_Squid__Total
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Squid__Total = value
        End Set
    End Property

    Public Property FileCopy_Squid__Success() As Integer
        Get
            Return m_FileCopy_Squid__Success
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Squid__Success = value
        End Set
    End Property

    Public Property FileCopy_Squid__Failed() As Integer
        Get
            Return m_FileCopy_Squid__Failed
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Squid__Failed = value
        End Set
    End Property

    Public ReadOnly Property FileCopy_SquidGuard() As String
        Get
            Return m_FileCopy_SquidGuard__Total.ToString & "/" & m_FileCopy_SquidGuard__Success & "/" & m_FileCopy_SquidGuard__Failed
        End Get
    End Property

    Public Property FileCopy_SquidGuard__Total() As Integer
        Get
            Return m_FileCopy_SquidGuard__Total
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_SquidGuard__Total = value
        End Set
    End Property

    Public Property FileCopy_SquidGuard__Success() As Integer
        Get
            Return m_FileCopy_SquidGuard__Success
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_SquidGuard__Success = value
        End Set
    End Property

    Public Property FileCopy_SquidGuard__Failed() As Integer
        Get
            Return m_FileCopy_SquidGuard__Failed
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_SquidGuard__Failed = value
        End Set
    End Property

    Public ReadOnly Property FileCopy_Windows() As String
        Get
            Return m_FileCopy_Windows__Total.ToString & "/" & m_FileCopy_Windows__Success & "/" & m_FileCopy_Windows__Failed
        End Get
    End Property

    Public Property FileCopy_Windows__Total() As Integer
        Get
            Return m_FileCopy_Windows__Total
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Windows__Total = value
        End Set
    End Property

    Public Property FileCopy_Windows__Success() As Integer
        Get
            Return m_FileCopy_Windows__Success
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Windows__Success = value
        End Set
    End Property

    Public Property FileCopy_Windows__Failed() As Integer
        Get
            Return m_FileCopy_Windows__Failed
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Windows__Failed = value
        End Set
    End Property

    Public ReadOnly Property FileCopy_Helpers() As String
        Get
            Return m_FileCopy_Helpers__Total.ToString & "/" & m_FileCopy_Helpers__Success & "/" & m_FileCopy_Helpers__Failed
        End Get
    End Property

    Public Property FileCopy_Helpers__Total() As Integer
        Get
            Return m_FileCopy_Helpers__Total
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Helpers__Total = value
        End Set
    End Property

    Public Property FileCopy_Helpers__Success() As Integer
        Get
            Return m_FileCopy_Helpers__Success
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Helpers__Success = value
        End Set
    End Property

    Public Property FileCopy_Helpers__Failed() As Integer
        Get
            Return m_FileCopy_Helpers__Failed
        End Get
        Set(ByVal value As Integer)
            m_FileCopy_Helpers__Failed = value
        End Set
    End Property

    Public Property InstallScript_err() As String
        Get
            Return m_InstallScript_err
        End Get
        Set(ByVal value As String)
            m_InstallScript_err = value
        End Set
    End Property

    Public Property InstallScript_log() As String
        Get
            Return m_InstallScript_log
        End Get
        Set(ByVal value As String)
            m_InstallScript_log = value
        End Set
    End Property
#End Region

    Public Sub New()
        m_ExecutionTimeStart = DateTime.Now
    End Sub

#Region "GetExecutionTime"
    Public Function GetExecutionTime(ByVal unit As ExecutionTimeUnits) As String
        Dim l As Long, sUnit As String = ""

        m_ExecutionTimeSpan = DateTime.Now - m_ExecutionTimeStart

        Select Case unit
            Case ExecutionTimeUnits.Ticks
                l = GetExecutionTimeTicks()
                sUnit = " ticks"
            Case ExecutionTimeUnits.Milliseconds
                l = GetExecutionTimeMilliSeconds()
                sUnit = " ms"
            Case ExecutionTimeUnits.Seconds
                l = GetExecutionTimeSeconds()
                sUnit = " s"
            Case ExecutionTimeUnits.Minutes
                l = GetExecutionTimeMinute()
                sUnit = " m"
            Case Else
                l = 0
                sUnit = " ??"
        End Select

        Return CStr(l) & sUnit
    End Function

    Private Function GetExecutionTimeTicks() As Long
        Return CLng(m_ExecutionTimeSpan.Ticks)
    End Function

    Private Function GetExecutionTimeMilliSeconds() As Long
        Return CLng(Math.Round(m_ExecutionTimeSpan.Ticks / TimeSpan.TicksPerMillisecond))
    End Function

    Private Function GetExecutionTimeSeconds() As Long
        Return CLng(Math.Round(m_ExecutionTimeSpan.Ticks / TimeSpan.TicksPerSecond))
    End Function

    Private Function GetExecutionTimeMinute() As Long
        Return CLng(Math.Round(m_ExecutionTimeSpan.Ticks / TimeSpan.TicksPerMinute))
    End Function
#End Region

    Public Function CalculateVerdict() As String
        Dim iVerdict As Integer = 0
        Dim iVerdictPossible As Integer = 0

        iVerdictPossible += 1
        If m_SkCFG Then
            iVerdict += 1
        End If

        iVerdictPossible += 1
        If m_FileCopy_Squid__Failed = 0 Then
            iVerdict += 1
        End If

        iVerdictPossible += 1
        If m_FileCopy_SquidGuard__Failed = 0 Then
            iVerdict += 1
        End If

        iVerdictPossible += 1
        If m_FileCopy_Windows__Failed = 0 Then
            iVerdict += 1
        End If

        iVerdictPossible += 1
        If m_FileCopy_Helpers__Failed = 0 Then
            iVerdict += 1
        End If

        iVerdictPossible += 1
        If m_InstallScript_log.Contains("Squid service was started successfully") Then
            iVerdict += 1
        End If

        Return iVerdict.ToString & "/" & iVerdictPossible.ToString
    End Function
End Class
