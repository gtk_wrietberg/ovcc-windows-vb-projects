Module UpdateProxySettings
    Public Function UpdateProxySettings_SiteKioskHive() As Boolean
        Try
            Dim oHive As RegistryHive
            Dim sSiteKioskRegistryHive As String

            oLogger.WriteToLogRelative("updating registry for SiteKiosk user", , 1)

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("initialising", , 2)
            oHive = New RegistryHive

            If oHive.Initialise Then
                oLogger.WriteToLog("ok", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                Return False
            End If

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("loading SiteKiosk hive", , 2)
            If IO.File.Exists(cSiteKioskRegistryHive_Win7) Then
                sSiteKioskRegistryHive = cSiteKioskRegistryHive_Win7
            ElseIf IO.File.Exists(cSiteKioskRegistryHive_WinXP) Then
                sSiteKioskRegistryHive = cSiteKioskRegistryHive_WinXP
            Else
                oLogger.WriteToLogRelative("No hives found!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                Return False
            End If
            oLogger.WriteToLogRelative("hive found", , 3)
            oLogger.WriteToLogRelative(sSiteKioskRegistryHive, , 4)


            If oHive.LoadHive(sSiteKioskRegistryHive) Then
                oLogger.WriteToLogRelative("loaded", , 3)
            Else
                oLogger.WriteToLogRelative("loading failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                oHive.UnloadHive()

                Return False
            End If


            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("setting values", , 2)

            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_MigrateProxy, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_MigrateProxy.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_MigrateProxy, cSiteKioskRegistry_Value_MigrateProxy) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_ProxyEnable, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_ProxyEnable.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_ProxyEnable, cSiteKioskRegistry_Value_ProxyEnable) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_ProxyHttp1_1, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_ProxyHttp1_1.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_ProxyHttp1_1, cSiteKioskRegistry_Value_ProxyHttp1_1) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_ProxyServer, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_ProxyServer.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_ProxyServer, cSiteKioskRegistry_Value_ProxyServer) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("key  : " & cSiteKioskRegistry_Key_InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name_ProxyOverride, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value_ProxyOverride.ToString, , 3)
            If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_InternetSettings, cSiteKioskRegistry_Name_ProxyOverride, cSiteKioskRegistry_Value_ProxyOverride) Then
                oLogger.WriteToLogRelative("ok", , 4)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End If


            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("unloading SiteKiosk hive", , 2)
            If oHive.UnloadHive() Then
                oLogger.WriteToLogRelative("unloaded", , 3)
            Else
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

                Return False
            End If

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLogRelative("ex.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative("ex.Source", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End Try
    End Function

End Module
