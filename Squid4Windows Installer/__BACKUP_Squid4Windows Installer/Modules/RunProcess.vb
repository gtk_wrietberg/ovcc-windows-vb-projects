Module RunProcess
    Public Function ShellandWait(ByVal ProcessPath As String, Optional ByVal ProcessArguments As String = "", Optional ByVal WaitTimeOut As Integer = -1, Optional ByVal ProcessStyle As ProcessWindowStyle = ProcessWindowStyle.Normal) As Boolean
        Dim objProcess As System.Diagnostics.Process

        oLogger.WriteToLogRelative("starting process", , 1)
        oLogger.WriteToLogRelative("process info", , 2)
        oLogger.WriteToLogRelative("Path     :  " & ProcessPath, , 3)
        oLogger.WriteToLogRelative("Arguments:  " & ProcessArguments, , 3)
        oLogger.WriteToLogRelative("Timeout  :  " & WaitTimeOut.ToString, , 3)

        Try
            objProcess = New System.Diagnostics.Process()
            objProcess.StartInfo.FileName = ProcessPath
            objProcess.StartInfo.Arguments = ProcessArguments
            objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden

            oLogger.WriteToLogRelative("starting", , 2)
            objProcess.Start()

            'Wait until the process passes back an exit code 
            objProcess.WaitForExit(WaitTimeOut)

            oLogger.WriteToLogRelative("ended (or timed out, ooooh)", Logger.MESSAGE_TYPE.LOG_WARNING, 2)

            'Free resources associated with this process
            objProcess.Close()

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("Error", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try
    End Function
End Module
