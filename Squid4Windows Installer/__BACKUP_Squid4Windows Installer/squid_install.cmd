@echo off

echo 
echo Installing and Starting Squid
C:\Squid4Win\Squid\sbin\squid -z
C:\Squid4Win\Squid\sbin\squid -i
net start squid
echo 
echo 

echo Installing maintenance scripts
schtasks /create /sc DAILY /tn "Squid Daily Maintenance" /tr C:\Squid4Win\Squid\sbin\sqrot.cmd /ru System /st 23:50:00

