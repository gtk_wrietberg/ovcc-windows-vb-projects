Module Main
    Public Sub Main()
        Dim myWebRequest As Net.HttpWebRequest = CType(Net.WebRequest.Create(c__Porn_Url), Net.HttpWebRequest)

        ' Obtain the 'Proxy' of the  Default browser.   

        Dim proxy As Net.IWebProxy = CType(myWebRequest.Proxy, Net.IWebProxy)

        ' Print the Proxy Url to the console. 
        Console.WriteLine("Checking proxy server...")
        If Not proxy Is Nothing Then
            If proxy.GetProxy(myWebRequest.RequestUri).ToString.Contains(c__Porn_Url) Then
                Console.WriteLine("Proxy server = (null) ; No proxy set")
                Environment.Exit(-1)
            Else
                Console.WriteLine("Proxy server = {0}", proxy.GetProxy(myWebRequest.RequestUri))
            End If
        Else
            Console.WriteLine("Proxy server = (null) ; No proxy set")
            Environment.Exit(-1)
        End If


        Console.WriteLine("Checking content filter...")

        Try
            Dim myWebResponse As Net.HttpWebResponse = CType(myWebRequest.GetResponse(), Net.HttpWebResponse)


            Console.WriteLine("Received response ({0} bytes)", myWebResponse.ContentLength)

            ' Get the stream associated with the response. 
            Dim receiveStream As IO.Stream = myWebResponse.GetResponseStream()

            ' Pipes the stream to a higher level stream reader with the required encoding format.  
            Dim readStream As New IO.StreamReader(receiveStream, Text.Encoding.UTF8)
            Dim sResponse As String = readStream.ReadToEnd()
            myWebResponse.Close()
            readStream.Close()

            If sResponse.Contains(c__Blocked_SearchPattern) Then
                Console.WriteLine("blocking is working")
                Environment.Exit(0)
            Else
                Console.WriteLine("blocking not detected")
                Environment.Exit(-2)
            End If
        Catch ex As Exception
            Console.WriteLine("connection failed")
            Environment.Exit(-4)
        End Try

    End Sub
End Module
