Module Main
    Public Sub Main()
        Dim today As DateTime = DateTime.Now
        Dim time As DateTime
        Dim daysToAdd As Integer = 0
        Dim format As String = "yyyyMMdd_HHmmss"

        If Environment.GetCommandLineArgs.Length <> 2 Then
            daysToAdd = 0
        Else
            If IsNumeric(Environment.GetCommandLineArgs(1)) Then
                Try
                    daysToAdd = CInt(Environment.GetCommandLineArgs(1))
                Catch ex As Exception
                    daysToAdd = 0
                End Try
            End If
        End If

        time = today.AddDays(daysToAdd)

        Console.WriteLine(time.ToString(format))
    End Sub
End Module
