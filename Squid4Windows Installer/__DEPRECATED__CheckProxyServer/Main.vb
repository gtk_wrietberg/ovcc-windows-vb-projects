Module Main
    Public Sub main()
        InitGlobals()

        '------------------------------------------------------
        'Logger initialisation
        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        Dim myWebRequest As Net.HttpWebRequest = CType(Net.WebRequest.Create(c__Porn_Url), Net.HttpWebRequest)

        ' Obtain the 'Proxy' of the  Default browser.   
        Dim proxy As Net.IWebProxy = CType(myWebRequest.Proxy, Net.IWebProxy)
        ' Print the Proxy Url to the console. 
        If Not proxy Is Nothing Then
            Console.WriteLine("Proxy: {0}", proxy.GetProxy(myWebRequest.RequestUri))
        Else
            Console.WriteLine("Proxy is null; no proxy will be used")
        End If
        If ConsoleEx.OutputRedirected Then
            attachconsole(-1)

        End If
    End Sub
End Module
