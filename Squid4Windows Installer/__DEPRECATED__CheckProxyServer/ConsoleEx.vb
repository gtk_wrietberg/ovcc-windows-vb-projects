Imports System.Runtime.InteropServices

Public NotInheritable Class ConsoleEx
    Private Sub New()
    End Sub
    Public Shared ReadOnly Property OutputRedirected() As Boolean
        Get
            Return FileType.[Char] <> GetFileType(GetStdHandle(StdHandle.Stdout))
        End Get
    End Property
    Public Shared ReadOnly Property InputRedirected() As Boolean
        Get
            Return FileType.[Char] <> GetFileType(GetStdHandle(StdHandle.Stdin))
        End Get
    End Property
    Public Shared ReadOnly Property ErrorRedirected() As Boolean
        Get
            Return FileType.[Char] <> GetFileType(GetStdHandle(StdHandle.Stderr))
        End Get
    End Property

    ' P/Invoke:
    Private Enum FileType
        Unknown
        Disk
        [Char]
        Pipe
    End Enum
    Private Enum StdHandle
        Stdin = -10
        Stdout = -11
        Stderr = -12
    End Enum
    <DllImport("kernel32.dll")> _
    Private Shared Function GetFileType(ByVal hdl As IntPtr) As FileType
    End Function
    <DllImport("kernel32.dll")> _
    Private Shared Function GetStdHandle(ByVal std As StdHandle) As IntPtr
    End Function
End Class