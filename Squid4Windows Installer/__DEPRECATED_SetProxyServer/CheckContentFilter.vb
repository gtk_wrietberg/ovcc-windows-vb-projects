Imports System.IO
Imports System.Net
Imports System.Text

Module CheckContentFilter
    Public Function CheckContentFiltering() As Integer
        Dim myWebRequest As HttpWebRequest = CType(WebRequest.Create(c__Porn_Url), HttpWebRequest)
        Dim proxy As IWebProxy = CType(HttpWebRequest.GetSystemWebProxy, IWebProxy)

        Try
            If Not proxy Is Nothing Then
                If proxy.GetProxy(myWebRequest.RequestUri).ToString.Contains(c__Porn_Url) Then
                    If g_DEBUG Then
                        oLogger.WriteToLog("Proxy server = (?) ; No proxy set?", , 1)
                    End If
                Else
                    If g_DEBUG Then
                        oLogger.WriteToLog("Proxy server = " & proxy.GetProxy(myWebRequest.RequestUri).ToString, , 1)
                    End If
                End If
            Else
                If g_DEBUG Then
                    oLogger.WriteToLog("Proxy server = (null) ; No proxy set?", , 1)
                End If
            End If
        Catch ex As Exception
            oLogger.WriteToLog("Proxy checking failed", , 1)
            oLogger.WriteToLog(ex.ToString, , 2)
        End Try

        Try
            Dim myWebResponse As HttpWebResponse = CType(myWebRequest.GetResponse(), HttpWebResponse)
            Dim receiveStream As Stream = myWebResponse.GetResponseStream()

            If myWebResponse.StatusCode = HttpStatusCode.OK Then
                'I can browse!
                Return 0
            Else
                oLogger.WriteToLog("WebResponse status: " & myWebResponse.StatusDescription & "(" & myWebResponse.StatusCode & ")", , 1)
                Return 1
            End If
        Catch ex As Exception
            oLogger.WriteToLog("connection failed", , 1)
            oLogger.WriteToLog(ex.ToString, , 2)
            Return 2
        End Try

    End Function

End Module
