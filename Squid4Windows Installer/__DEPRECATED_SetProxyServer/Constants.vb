Module Constants
    Public c__Porn_Url As String = "http://www.sex.com"
    Public c__Blocked_SearchPattern As String = "BLOCKED by iBAHN"

    Public c__SetProxyServer_App_Log_Path As String = "SetProxyServer"
    Public c__SetProxyServer_App_Log_Name As String = "SetProxyServer.log"

    Public Const ciBAHN_Key__ProxySettings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\ProxySettings"
    Public Const ciBAHN_Name__ProxySetForUsers As String = "ProxySetForUsers"

    Public Const cSiteKioskRegistry_Key__InternetSettings As String = "HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings"

    Public Const cSiteKioskRegistry_Name__MigrateProxy As String = "MigrateProxy"
    Public Const cSiteKioskRegistry_Value__MigrateProxy As Long = 1

    Public Const cSiteKioskRegistry_Name__ProxyEnable As String = "ProxyEnable"
    Public Const cSiteKioskRegistry_Value__ProxyEnable As Long = 1
    Public Const cSiteKioskRegistry_Value__ProxyDisable As Long = 0

    Public Const cSiteKioskRegistry_Name__ProxyHttp1_1 As String = "ProxyHttp1.1"
    Public Const cSiteKioskRegistry_Value__ProxyHttp1_1 As Long = 1

    Public Const cSiteKioskRegistry_Name__ProxyServer As String = "ProxyServer"
    Public Const cSiteKioskRegistry_Value__ProxyServer As String = "127.0.0.1:8080"

    Public Const cSiteKioskRegistry_Name__ProxyOverride As String = "ProxyOverride"
    Public Const cSiteKioskRegistry_Value__ProxyOverride As String = "<local>"

    Public g_DEBUG As Boolean = False
    Public oLogger As Logger
    Public g_SetProxyServer_LogFileDirectory As String

    Public Sub InitGlobals()
        g_SetProxyServer_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_SetProxyServer_LogFileDirectory &= "\" & c__SetProxyServer_App_Log_Path


    End Sub
End Module
