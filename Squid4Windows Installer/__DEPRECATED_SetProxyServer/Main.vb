Imports System.Threading

Module Main
    Public Sub Main()
        InitGlobals()

        For Each arg As String In Environment.GetCommandLineArgs()
            If arg = "-debug" Or arg = "--debug" Then
                g_DEBUG = True
            End If
        Next

        '------------------------------------------------------
        'Logger initialisation
        oLogger = New Logger
        oLogger.LogFileDirectory = g_SetProxyServer_LogFileDirectory
        oLogger.LogFileName = c__SetProxyServer_App_Log_Name

        oLogger.WriteToLog(New String("*", 20))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)
        If g_DEBUG Then
            oLogger.WriteToLog("Debugging on", Logger.MESSAGE_TYPE.LOG_WARNING)
        End If


        Dim users As String = GetRegistryValueAsString(ciBAHN_Key__ProxySettings, ciBAHN_Name__ProxySetForUsers)

        users = users.ToLower

        If users.Equals("") Then
            oLogger.WriteToLog("No user set in registry!!!", Logger.MESSAGE_TYPE.LOG_ERROR)
            Environment.Exit(1)
            Exit Sub
        End If

        Dim bSetProxy As Boolean = False
        Dim users_parts As String() = users.Split(";")
        For i As Integer = 0 To users_parts.Length - 1
            If users_parts(i).Equals(Environment.UserName.ToLower) Then
                bSetProxy = True

                Exit For
            End If
        Next

        If bSetProxy = False Then
            oLogger.WriteToLog("Current user (" & Environment.UserName & ") will NOT use proxy")
            SetRegistryValue(cSiteKioskRegistry_Key__InternetSettings, cSiteKioskRegistry_Name__ProxyEnable, cSiteKioskRegistry_Value__ProxyDisable)

            Environment.Exit(0)
        Else
            oLogger.WriteToLog("Current user (" & Environment.UserName & ") will use proxy")
            SetRegistryValue(cSiteKioskRegistry_Key__InternetSettings, cSiteKioskRegistry_Name__MigrateProxy, cSiteKioskRegistry_Value__MigrateProxy)
            SetRegistryValue(cSiteKioskRegistry_Key__InternetSettings, cSiteKioskRegistry_Name__ProxyEnable, cSiteKioskRegistry_Value__ProxyEnable)
            SetRegistryValue(cSiteKioskRegistry_Key__InternetSettings, cSiteKioskRegistry_Name__ProxyHttp1_1, cSiteKioskRegistry_Value__ProxyHttp1_1)
            SetRegistryValue(cSiteKioskRegistry_Key__InternetSettings, cSiteKioskRegistry_Name__ProxyServer, cSiteKioskRegistry_Value__ProxyServer)
            SetRegistryValue(cSiteKioskRegistry_Key__InternetSettings, cSiteKioskRegistry_Name__ProxyOverride, cSiteKioskRegistry_Value__ProxyOverride)

            'A little delay, just to be sure
            Thread.Sleep(10000)

            Dim iRet As Integer

            oLogger.WriteToLog("Checking content filtering")
            iRet = CheckContentFiltering()

            If iRet <> 0 Then
                oLogger.WriteToLog("Proxy does not work, disabling", , 1)
                SetRegistryValue(cSiteKioskRegistry_Key__InternetSettings, cSiteKioskRegistry_Name__ProxyEnable, cSiteKioskRegistry_Value__ProxyDisable)
            Else
                oLogger.WriteToLog("All seems fine", , 1)
            End If

            oLogger.WriteToLog("bye (" & iRet.ToString & ")", , 1)
            Environment.Exit(iRet)
        End If
    End Sub
End Module
