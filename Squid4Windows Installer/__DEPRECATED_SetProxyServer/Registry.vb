Module Registry
    Public Function GetRegistryValueAsString(ByVal keyName As String, ByVal valueName As String) As String
        Dim sRet As String

        Try
            If g_DEBUG Then
                oLogger.WriteToLogRelative("GetRegistryValue", , 1)
                oLogger.WriteToLogRelative("key", , 2)
                oLogger.WriteToLogRelative(keyName, , 3)
                oLogger.WriteToLogRelative("name", , 2)
                oLogger.WriteToLogRelative(valueName, , 3)
            End If

            sRet = Microsoft.Win32.Registry.GetValue(keyName, valueName, "")
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
            oLogger.WriteToLog(ex.ToString, , 3)

            sRet = ""
        End Try

        Return sRet
    End Function

    Public Function SetRegistryValue(ByVal keyName As String, ByVal valueName As String, ByVal value As Object) As Boolean
        Dim bRet As Boolean = False

        Try
            If g_DEBUG Then
                oLogger.WriteToLogRelative("SetRegistryValue", , 1)
                oLogger.WriteToLogRelative("key", , 2)
                oLogger.WriteToLogRelative(keyName, , 3)
                oLogger.WriteToLogRelative("name", , 2)
                oLogger.WriteToLogRelative(valueName, , 3)
                oLogger.WriteToLogRelative("value", , 2)
                oLogger.WriteToLogRelative(value.ToString, , 3)
            End If

            If Integer.TryParse(value.ToString, 0) Then
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value, Microsoft.Win32.RegistryValueKind.DWord)
            Else
                Microsoft.Win32.Registry.SetValue(keyName, valueName, value)
            End If

            If g_DEBUG Then
                oLogger.WriteToLogRelative("ok", , 2)
            End If

            bRet = True
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
            oLogger.WriteToLog(ex.ToString, , 3)
        End Try

        Return bRet
    End Function
End Module
