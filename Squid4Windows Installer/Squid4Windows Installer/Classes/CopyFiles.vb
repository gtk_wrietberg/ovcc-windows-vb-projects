Public Class CopyFiles
    Private pBackupFolder As String
    Private pSourceDir As String
    Private pDestDir As String
    Private pFileCounterTotal As Integer
    Private pFileCounterSuccess As Integer
    Private pFileCounterFailed As Integer

    Public ReadOnly Property FileCounterTotal() As Integer
        Get
            Return pFileCounterTotal
        End Get
    End Property

    Public ReadOnly Property FileCounterSuccess() As Integer
        Get
            Return pFileCounterSuccess
        End Get
    End Property

    Public ReadOnly Property FileCounterFailed() As Integer
        Get
            Return pFileCounterFailed
        End Get
    End Property

    Public Sub FileCounterReset()
        pFileCounterTotal = 0
        pFileCounterSuccess = 0
        pFileCounterFailed = 0
    End Sub

    Public Property BackupDirectory() As String
        Get
            Return pBackupFolder
        End Get
        Set(ByVal value As String)
            pBackupFolder = value
        End Set
    End Property

    Public Property SourceDirectory() As String
        Get
            Return pSourceDir
        End Get
        Set(ByVal value As String)
            pSourceDir = value
        End Set
    End Property

    Public Property DestinationDirectory() As String
        Get
            Return pDestDir
        End Get
        Set(ByVal value As String)
            pDestDir = value
        End Set
    End Property

    Public Function CopyFiles() As Boolean
        oLogger.WriteToLogRelative("starting file copy", , 1)
        oLogger.WriteToLogRelative("source: " & pSourceDir, , 2)
        oLogger.WriteToLogRelative("dest  : " & pDestDir, , 2)

        RecursiveDirectoryCopy(pSourceDir, pDestDir)

        Return True
    End Function

    Public Function CopyFiles(ByVal SourceDir As String, ByVal DestinationDir As String) As Boolean
        pSourceDir = SourceDir
        pDestDir = DestinationDir

        Return CopyFiles()
    End Function

    Private Sub RecursiveDirectoryCopy(ByVal sourceDir As String, ByVal destDir As String)
        Dim sDir As String
        Dim dDirInfo As IO.DirectoryInfo
        Dim sDirInfo As IO.DirectoryInfo
        Dim sFile As String
        Dim sFileInfo As IO.FileInfo
        Dim dFileInfo As IO.FileInfo

        ' Add trailing separators to the supplied paths if they don't exist.
        If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            sourceDir &= System.IO.Path.DirectorySeparatorChar
        End If
        If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            destDir &= System.IO.Path.DirectorySeparatorChar
        End If

        'If destination directory does not exist, create it.
        dDirInfo = New System.IO.DirectoryInfo(destDir)
        If dDirInfo.Exists = False Then dDirInfo.Create()
        dDirInfo = Nothing


        ' Get a list of directories from the current parent.
        For Each sDir In System.IO.Directory.GetDirectories(sourceDir)
            sDirInfo = New System.IO.DirectoryInfo(sDir)
            dDirInfo = New System.IO.DirectoryInfo(destDir & sDirInfo.Name)
            ' Create the directory if it does not exist.
            If dDirInfo.Exists = False Then dDirInfo.Create()
            ' Since we are in recursive mode, copy the children also
            RecursiveDirectoryCopy(sDirInfo.FullName, dDirInfo.FullName)
            sDirInfo = Nothing
            dDirInfo = Nothing
        Next

        ' Get the files from the current parent.
        For Each sFile In System.IO.Directory.GetFiles(sourceDir)
            sFileInfo = New System.IO.FileInfo(sFile)
            dFileInfo = New System.IO.FileInfo(Replace(sFile, sourceDir, destDir))

            oLogger.WriteToLogRelative("copying", , 1)
            oLogger.WriteToLogRelative("source: " & sFileInfo.FullName, , 2)
            oLogger.WriteToLogRelative("dest  : " & dFileInfo.FullName, , 2)

            'If File does exists, backup.
            If dFileInfo.Exists Then
                oLogger.WriteToLogRelative("already exists", , 3)

                If dFileInfo.IsReadOnly Then
                    oLogger.WriteToLogRelative("file is read-only, fixing...", , 4)
                    dFileInfo.IsReadOnly = False
                End If
                oLogger.WriteToLogRelative("creating backup", , 3)
                If BackupFile(dFileInfo) Then

                Else

                End If
            End If

            Try
                pFileCounterTotal += 1
                sFileInfo.CopyTo(dFileInfo.FullName, True)
                If IO.File.Exists(dFileInfo.FullName) Then
                    pFileCounterSuccess += 1
                    oLogger.WriteToLogRelative("ok", , 2)
                Else
                    pFileCounterFailed += 1
                    oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                End If
            Catch ex As Exception
                pFileCounterFailed += 1
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try

            sFileInfo = Nothing
            dFileInfo = Nothing
        Next
    End Sub

    Public Function BackupFile(ByVal oFile As IO.FileInfo) As Boolean
        Dim sPath As String, sName As String, sFullDest As String
        Dim oDirInfo As System.IO.DirectoryInfo, oFileInfo As System.IO.FileInfo
        Dim bRet As Boolean = False

        Try
            sName = oFile.Name

            sPath = oFile.DirectoryName
            sPath = sPath.Replace("C:", "")
            sPath = sPath.Replace("c:", "")
            sPath = sPath.Replace("\\", "\")
            sPath = pBackupFolder & "\" & sPath

            sFullDest = sPath & "\" & sName

            oDirInfo = New System.IO.DirectoryInfo(sPath)
            If Not oDirInfo.Exists Then oDirInfo.Create()

            oLogger.WriteToLogRelative("backup: " & sFullDest, , 4)

            oFile.CopyTo(sFullDest, True)

            oFileInfo = New System.IO.FileInfo(sFullDest)
            If oFileInfo.Exists Then
                oLogger.WriteToLogRelative("ok", , 5)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
        End Try

        oDirInfo = Nothing
        oFileInfo = Nothing

        Return bRet
    End Function
End Class
