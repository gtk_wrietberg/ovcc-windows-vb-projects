Module Main
    Public Sub main()
        '------------------------------------------------------
        InitGlobals()
        g_LogInfoForEmailReport = New LogInfoForEmailReport

        '------------------------------------------------------
        'Logger initialisation
        oLogger = New Logger
        oLogger.LogFileDirectory = g_LogFileDirectory
        oLogger.LogFileName = g_LogFileName

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        If g_TESTMODE Then
            oLogger.WriteToLog("TEST MODE!!!!", , 0)
        End If
        oLogger.WriteToLog(New String("-", 25))


        '------------------------------------------------------
        'Command line params
        If Environment.GetCommandLineArgs().Length > 1 Then
            oLogger.WriteToLog("Command line params", , 0)

            For Each arg As String In Environment.GetCommandLineArgs()
                oLogger.WriteToLog("Found: " & arg, , 1)
                If arg = "-noreboot" Or arg = "--no-reboot" Then
                    g_NOREBOOT = True
                    oLogger.WriteToLog("Reboot disabled", , 2)
                End If
                If arg = "-test" Or arg = "--test" Then
                    g_TESTMODE = True
                    oLogger.WriteToLog("Test mode enabled", , 2)
                End If
            Next
        End If

        '------------------------------------------------------
        'Disable SiteKiosk content filter
        oSkCfg = New SkCfg
        oSkCfg.BackupFolder = g_BackupDirectory

        oLogger.WriteToLog("Update SiteKiosk config", , 0)
        g_LogInfoForEmailReport.SkCFG = oSkCfg.Update(True)


        '------------------------------------------------------
        'Prepare SquidGuard config file for proper blocked page
        Dim cCheck As New CheckPage
        Dim sBlockedUrl As String = ""

        oLogger.WriteToLog("Prepairing squidGuard config", , 0)

        oLogger.WriteToLog("Checking urls for blocked page", , 1)
        For i As Integer = 0 To c_BlockedPages.Length - 1
            oLogger.WriteToLog(c_BlockedPages(i), , 2)
            If cCheck.IsOk(c_BlockedPages(i)) Then
                If sBlockedUrl.Equals("") Then
                    sBlockedUrl = c_BlockedPages(i)
                End If
            End If
            oLogger.WriteToLog(cCheck.LastMessage, , 3)
        Next
        oLogger.WriteToLog("using: " & sBlockedUrl, , 2)


        Dim sFileRead As String, sFileWrite As String
        sFileRead = "files\SquidGuard\squidGuard.conf.TEMPLATE"
        sFileWrite = "files\SquidGuard\squidGuard.conf"

        oLogger.WriteToLog("Updating squidGuard config", , 1)

        If Not IO.File.Exists(sFileRead) Then
            oLogger.WriteToLog(sFileRead & " does not exist!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        Else
            Try
                Dim lines As New List(Of String)

                oLogger.WriteToLog("Reading: " & sFileRead, , 2)
                Using sr As New IO.StreamReader(sFileRead)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("%%BLOCKED_PAGE_URL%%") Then
                            line = line.Replace("%%BLOCKED_PAGE_URL%%", sBlockedUrl)
                        End If

                        lines.Add(line)
                    End While
                End Using

                oLogger.WriteToLog("Writing: " & sFileWrite, , 2)
                Using sw As New IO.StreamWriter(sFileWrite)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
            Catch ex As Exception
                oLogger.WriteToLog("!!ERROR!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            End Try
        End If


        '------------------------------------------------------
        'File copying
        oLogger.WriteToLog("Copying files", , 0)

        oCopyFiles = New CopyFiles
        oCopyFiles.BackupDirectory = g_BackupDirectory

        oLogger.WriteToLog("Squid files", , 1)
        oCopyFiles.FileCounterReset()
        oCopyFiles.SourceDirectory = "files\Squid"
        oCopyFiles.DestinationDirectory = "C:\Squid"
        oCopyFiles.CopyFiles()
        g_LogInfoForEmailReport.FileCopy_Squid__Total = oCopyFiles.FileCounterTotal
        g_LogInfoForEmailReport.FileCopy_Squid__Success = oCopyFiles.FileCounterSuccess
        g_LogInfoForEmailReport.FileCopy_Squid__Failed = oCopyFiles.FileCounterFailed

        oLogger.WriteToLog("SquidGuard files", , 1)
        oCopyFiles.FileCounterReset()
        oCopyFiles.SourceDirectory = "files\SquidGuard"
        oCopyFiles.DestinationDirectory = "C:\usr\local\SquidGuard"
        oCopyFiles.CopyFiles()
        g_LogInfoForEmailReport.FileCopy_SquidGuard__Total = oCopyFiles.FileCounterTotal
        g_LogInfoForEmailReport.FileCopy_SquidGuard__Success = oCopyFiles.FileCounterSuccess
        g_LogInfoForEmailReport.FileCopy_SquidGuard__Failed = oCopyFiles.FileCounterFailed

        oLogger.WriteToLog("Misc stuff", , 1)
        oCopyFiles.FileCounterReset()
        oCopyFiles.SourceDirectory = "files\Windows"
        oCopyFiles.DestinationDirectory = "C:\Windows\"
        oCopyFiles.CopyFiles()
        g_LogInfoForEmailReport.FileCopy_Windows__Total = oCopyFiles.FileCounterTotal
        g_LogInfoForEmailReport.FileCopy_Windows__Success = oCopyFiles.FileCounterSuccess
        g_LogInfoForEmailReport.FileCopy_Windows__Failed = oCopyFiles.FileCounterFailed

        oLogger.WriteToLog("Persistent Settings Tool", , 1)
        oCopyFiles.FileCounterReset()
        oCopyFiles.SourceDirectory = "files\PersistentSettingsTool"
        oCopyFiles.DestinationDirectory = "C:\Squid\"
        oCopyFiles.CopyFiles()
        g_LogInfoForEmailReport.FileCopy_PST__Total = oCopyFiles.FileCounterTotal
        g_LogInfoForEmailReport.FileCopy_PST__Success = oCopyFiles.FileCounterSuccess
        g_LogInfoForEmailReport.FileCopy_PST__Failed = oCopyFiles.FileCounterFailed


        '------------------------------------------------------
        'Initialise and start Squid
        oLogger.WriteToLog("Starting Squid", , 0)

        oLogger.WriteToLog("starting installer script", , 1)
        oLogger.WriteToLog("script", , 2)
        oLogger.WriteToLog(c__SquidInstallScript_Cmd, , 3)

        Dim startinfo_SquidInstallScript As New Diagnostics.ProcessStartInfo(c__SquidInstallScript_Cmd)
        startinfo_SquidInstallScript.UseShellExecute = False
        startinfo_SquidInstallScript.CreateNoWindow = True
        startinfo_SquidInstallScript.RedirectStandardOutput = True
        startinfo_SquidInstallScript.RedirectStandardError = True

        ' Make the process and set its start information.
        Dim procSquidInstallerScript As New Diagnostics.Process()
        procSquidInstallerScript.StartInfo = startinfo_SquidInstallScript

        ' Start the process.
        procSquidInstallerScript.Start()

        ' Attach to stdout and stderr.
        Dim std_out_SquidInstallerScript As IO.StreamReader = procSquidInstallerScript.StandardOutput()
        Dim std_err_SquidInstallerScript As IO.StreamReader = procSquidInstallerScript.StandardError()

        ' Display the results.
        oLogger.WriteToLog("output (stderr)", , 2)
        g_LogInfoForEmailReport.InstallScript_err = std_err_SquidInstallerScript.ReadToEnd
        oLogger.WriteToLogWithoutDate(g_LogInfoForEmailReport.InstallScript_err)

        oLogger.WriteToLog("output (stdout)", , 2)
        g_LogInfoForEmailReport.InstallScript_log = std_out_SquidInstallerScript.ReadToEnd
        oLogger.WriteToLogWithoutDate(g_LogInfoForEmailReport.InstallScript_log)

        ' Clean up.
        std_out_SquidInstallerScript.Close()
        std_err_SquidInstallerScript.Close()
        procSquidInstallerScript.Close()


        '------------------------------------------------------
        'Set proxy settings for SiteKiosk user
        'oLogger.WriteToLog("Set proxy settings for sitekiosk user", , 0)
        'oLogger.WriteToLog("make sure proxy settings are per user", , 1)

        'Try
        '    oLogger.WriteToLog("registry", , 2)
        '    oLogger.WriteToLog("key  : " & cPolicy_Key__ProxySettingsPerUser, , 4)
        '    oLogger.WriteToLog("name : " & cPolicy_Name__ProxySettingsPerUser, , 4)
        '    oLogger.WriteToLog("value: " & cPolicy_Value__ProxySettingsPerUser.ToString, , 4)

        '    Microsoft.Win32.Registry.SetValue(cPolicy_Key__ProxySettingsPerUser, cPolicy_Name__ProxySettingsPerUser, cPolicy_Value__ProxySettingsPerUser, Microsoft.Win32.RegistryValueKind.DWord)

        '    oLogger.WriteToLog("ok", , 2)
        'Catch ex As Exception
        '    oLogger.WriteToLog("fail", , 2)
        '    oLogger.WriteToLog(ex.ToString, , 3)
        'End Try

        'oLogger.WriteToLog("make registry changes", , 1)
        'If UpdateProxySettings() Then
        '    oLogger.WriteToLog("ok", , 2)
        'Else
        '    oLogger.WriteToLog("FAIL!!!!!!!!!!", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        'End If



        Try
            oLogger.WriteToLog("registry", , 2)
            oLogger.WriteToLog("key  : " & cPolicy_Key__ProxySettingsPerUser, , 3)
            oLogger.WriteToLog("name : " & cPolicy_Name__ProxySettingsPerUser, , 3)
            oLogger.WriteToLog("value: " & cPolicy_Value__ProxySettingsPerUser.ToString, , 3)

            Microsoft.Win32.Registry.SetValue(cPolicy_Key__ProxySettingsPerUser, cPolicy_Name__ProxySettingsPerUser, cPolicy_Value__ProxySettingsPerUser, Microsoft.Win32.RegistryValueKind.DWord)

            oLogger.WriteToLog("ok", , 4)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
            oLogger.WriteToLog(ex.ToString, , 3)
        End Try

        Try
            Dim regkeyInternetSettings As Microsoft.Win32.RegistryKey

            regkeyInternetSettings = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(cSiteKioskRegistry_Key__InternetSettings, True)

            If regkeyInternetSettings Is Nothing Then
                Throw New Exception("reg key does not exist: " & cSiteKioskRegistry_Key__InternetSettings)
            End If

            oLogger.WriteToLogRelative("key  : LOCALMACHINE\" & cSiteKioskRegistry_Key__InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__MigrateProxy, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__MigrateProxy.ToString, , 3)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__MigrateProxy, cSiteKioskRegistry_Value__MigrateProxy, Microsoft.Win32.RegistryValueKind.DWord)
            oLogger.WriteToLog("ok", , 4)

            oLogger.WriteToLogRelative("key  : LOCALMACHINE\" & cSiteKioskRegistry_Key__InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyHttp1_1, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyHttp1_1.ToString, , 3)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyHttp1_1, cSiteKioskRegistry_Value__ProxyHttp1_1, Microsoft.Win32.RegistryValueKind.DWord)
            oLogger.WriteToLog("ok", , 4)

            oLogger.WriteToLogRelative("key  : LOCALMACHINE\" & cSiteKioskRegistry_Key__InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyServer, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyServer.ToString, , 3)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyServer, cSiteKioskRegistry_Value__ProxyServer, Microsoft.Win32.RegistryValueKind.String)
            oLogger.WriteToLog("ok", , 4)

            oLogger.WriteToLogRelative("key  : LOCALMACHINE\" & cSiteKioskRegistry_Key__InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyOverride, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyOverride.ToString, , 3)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyOverride, cSiteKioskRegistry_Value__ProxyOverride, Microsoft.Win32.RegistryValueKind.String)
            oLogger.WriteToLog("ok", , 4)

            oLogger.WriteToLogRelative("key  : LOCALMACHINE\" & cSiteKioskRegistry_Key__InternetSettings, , 3)
            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyEnable, , 3)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyEnable.ToString, , 3)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyEnable, cSiteKioskRegistry_Value__ProxyEnable, Microsoft.Win32.RegistryValueKind.DWord)
            oLogger.WriteToLog("ok", , 4)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
            oLogger.WriteToLog(ex.ToString, , 3)
        End Try


        '------------------------------------------------------
        'Send report by email
        oLogger.WriteToLog("Sending email report", , 0)
        Try
            Dim oMail As New SendMail
            Dim sBody As String

            oMail.Server = c__SmtpServerName
            oMail.Port = c__SmtpServerPort
            oMail.Username = c__SmtpServerUserName
            oMail.Password = c__SmtpServerPassWord
            oMail.Sender = Environment.MachineName & "_noreply@lobbypc.com"
            oMail.Recipient = c__Recipient

            oMail.Subject = Environment.MachineName & " squid install report"

            oLogger.WriteToLog("composing body", , 1)
            sBody = My.Resources.DefaultMailBody
            sBody = sBody.Replace("%%MACHINENAME%%", Environment.MachineName)
            sBody = sBody.Replace("%%INSTALLERVERSION%%", myBuildInfo.FileVersion)
            sBody = sBody.Replace("%%EXECUTIONTIME%%", g_LogInfoForEmailReport.GetExecutionTime(LogInfoForEmailReport.ExecutionTimeUnits.Milliseconds))
            sBody = sBody.Replace("%%VERDICT%%", g_LogInfoForEmailReport.CalculateVerdict)
            sBody = sBody.Replace("%%RESULT_SKCFG%%", g_LogInfoForEmailReport.SkCFG.ToString)
            sBody = sBody.Replace("%%COPY_SQUID%%", g_LogInfoForEmailReport.FileCopy_Squid)
            sBody = sBody.Replace("%%COPY_SQUIDGUARD%%", g_LogInfoForEmailReport.FileCopy_SquidGuard)
            sBody = sBody.Replace("%%COPY_WINDOWS%%", g_LogInfoForEmailReport.FileCopy_Windows)
            sBody = sBody.Replace("%%COPY_PST%%", g_LogInfoForEmailReport.FileCopy_PST)
            sBody = sBody.Replace("%%STDERR_INSTALL_SCRIPT%%", g_LogInfoForEmailReport.InstallScript_err)
            sBody = sBody.Replace("%%STDOUT_INSTALL_SCRIPT%%", g_LogInfoForEmailReport.InstallScript_log)
            sBody = sBody.Replace("%%REGISTRY_SETTINGS_METHOD%%", g_LogInfoForEmailReport.Proxy_Registry_Settings_Method)
            sBody = sBody.Replace("%%REGISTRY_SETTINGS%%", g_LogInfoForEmailReport.Proxy_Registry_Settings_String)
            oMail.Body = sBody

            oLogger.WriteToLog("sending", , 1)
            oMail.SendEmail()
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("FAIL", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            oLogger.WriteToLog("ex.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.ToString", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(ex.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("ex.InnerException.ToString", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLog(ex.InnerException.ToString, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        '------------------------------------------------------
        'Done
        oLogger.WriteToLog("Done", , 0)
        If Not g_NOREBOOT Then
            oLogger.WriteToLog("restarting", , 1)
            If g_TESTMODE Then
                oLogger.WriteToLog("skipped due to test mode", , 2)
            Else
                WindowsController.ExitWindows(RestartOptions.Reboot, True)
            End If
        End If

        oLogger.WriteToLog("bye", , 1)

        Environment.Exit(0)
    End Sub
End Module
