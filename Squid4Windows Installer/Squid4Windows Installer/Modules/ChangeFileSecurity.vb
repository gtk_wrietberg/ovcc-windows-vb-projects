Imports System.IO
Imports System.Security.AccessControl
Imports System.Security.Principal

Module ChangeFileSecurity
    Public Sub AddFileSecurity(ByVal fileName As String, ByVal username As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType)
        Dim account As String

        account = Environment.MachineName & "\" & username

        Dim fSecurity As FileSecurity = File.GetAccessControl(fileName)
        Dim accessRule As FileSystemAccessRule = New FileSystemAccessRule(account, rights, controlType)

        fSecurity.AddAccessRule(accessRule)

        File.SetAccessControl(fileName, fSecurity)
    End Sub

    Public Sub AddDirectorySecurity(ByVal directoryName As String, ByVal username As String, ByVal rights As FileSystemRights, ByVal controlType As AccessControlType)
        Dim account As String

        account = Environment.MachineName & "\" & username

        Dim dSecurity As DirectorySecurity = Directory.GetAccessControl(directoryName)
        Dim accessRule As FileSystemAccessRule = New FileSystemAccessRule(account, rights, controlType)

        dSecurity.SetAccessRule(accessRule)
        dSecurity.AddAccessRule(accessRule)

        Directory.SetAccessControl(directoryName, dSecurity)


    End Sub
End Module
