Module Constants___Globals
    'Constants
    Public Const c__SetProxyServer_App As String = "SetProxyServer.exe"

    Public Const c__SquidInstallScript_Cmd As String = "install_script\squid_install.cmd"

    Public Const c__SetProxyServer_App_Log_Path As String = "SetProxyServer"
    Public Const c__SetProxyServer_App_Log_Name As String = "SetProxyServer.log"

    'Emailing
    Public Const c__SmtpServerName As String = "smtp.lobbypc.com"
    Public Const c__SmtpServerPort As Integer = 587
    Public Const c__SmtpServerUserName As String = "3MA1LY00ser"
    Public Const c__SmtpServerPassWord As String = "$3k00r3MI$MtP@cNt"
    Public Const c__Recipient As String = "lobbypc.report@gmail.com"

    'Registry
    Public Const cPolicy_Key__ProxySettingsPerUser As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings "
    Public Const cPolicy_Name__ProxySettingsPerUser As String = "ProxySettingsPerUser"
    Public Const cPolicy_Value__ProxySettingsPerUser As Integer = 0 'for all users

    Public Const cRun_Key__ProxySettings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run"
    Public Const cRun_Name__ProxySettings As String = "SetProxyServer"

    Public Const ciBAHN_Key__ProxySettings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\ProxySettings"
    Public Const ciBAHN_Name__ProxySetForUsers As String = "ProxySetForUsers"
    Public Const ciBAHN_Value__ProxySetForUsers As String = "sitekiosk"


    Public Const cSiteKioskRegistry_Key__InternetSettings As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings"

    Public Const cSiteKioskRegistry_Name__MigrateProxy As String = "MigrateProxy"
    Public Const cSiteKioskRegistry_Value__MigrateProxy As Long = 1

    Public Const cSiteKioskRegistry_Name__ProxyEnable As String = "ProxyEnable"
    Public Const cSiteKioskRegistry_Value__ProxyEnable As Long = 1
    Public Const cSiteKioskRegistry_Value__ProxyDisable As Long = 0

    Public Const cSiteKioskRegistry_Name__ProxyHttp1_1 As String = "ProxyHttp1.1"
    Public Const cSiteKioskRegistry_Value__ProxyHttp1_1 As Long = 1

    Public Const cSiteKioskRegistry_Name__ProxyServer As String = "ProxyServer"
    Public Const cSiteKioskRegistry_Value__ProxyServer As String = "127.0.0.1:8080"

    Public Const cSiteKioskRegistry_Name__ProxyOverride As String = "ProxyOverride"
    Public Const cSiteKioskRegistry_Value__ProxyOverride As String = "<local>"

    'Misc
    Public Const cSiteKiosk_Username As String = "sitekiosk"


    'Possible blocked pages
    Public ReadOnly c_BlockedPages() As String = {"lobbypc.ibahn.com:8050/blocked/", "172.18.84.26:8050/blocked/", "72.254.0.131:8050/blocked/"}


    'Globals. Me like globals, me are lazy
    Public oLogger As Logger
    Public oSkCfg As SkCfg
    Public oRunas As RunAs
    Public oComputerName As ComputerName

    Public g_TESTMODE As Boolean = False

    Public g_NOREBOOT As Boolean = False

    Public g_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String

    Public g_LogInfoForEmailReport As LogInfoForEmailReport

    Public oCopyFiles As CopyFiles

    Public Sub InitGlobals()
        oComputerName = New ComputerName

        If oComputerName.ComputerName.Contains("rietberg") Then
            g_TESTMODE = True
        End If

        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(g_BackupDirectory)
    End Sub
End Module
