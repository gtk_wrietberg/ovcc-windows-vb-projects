Imports System.IO
Imports System.Security.Principal

Module ChangeRegistrySecurity
    Private Const c_RegKey As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings"

    Public Function ChangeRegistrySecurityForProxySetter() As Boolean
        Dim sHiveFile As String
        Dim oHive As New RegistryHive

        oLogger.WriteToLogRelative("searching for registry hive file for " & cSiteKiosk_Username, , 1)

        If File.Exists("C:\Documents and Settings\" & cSiteKiosk_Username & "\ntuser.dat") Then
            sHiveFile = "C:\Documents and Settings\" & cSiteKiosk_Username & "\ntuser.dat"
        ElseIf File.Exists("C:\Users\" & cSiteKiosk_Username & "\ntuser.dat") Then
            sHiveFile = "C:\Users\" & cSiteKiosk_Username & "\ntuser.dat"
        Else
            'No hive file found, whoops!
            oLogger.WriteToLogRelative("found none, uh oh.", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

            Return False
        End If

        oLogger.WriteToLogRelative("found: " & sHiveFile, , 2)

        oLogger.WriteToLogRelative("creating backup, just to be sure", , 1)
        If BackupFile(sHiveFile) Then
            oLogger.WriteToLogRelative("ok", , 2)
        Else
            oLogger.WriteToLogRelative("fail", , 2)
        End If

        Try
            oLogger.WriteToLogRelative("load hive and try to set permission", , 1)
            oLogger.WriteToLogRelative("Initialise", , 2)
            oHive.Initialise()

            oLogger.WriteToLogRelative("LoadHive", , 2)
            oHive.LoadHive(sHiveFile)

            oLogger.WriteToLogRelative("Set security", , 2)
            oLogger.WriteToLogRelative("key", , 3)
            oLogger.WriteToLogRelative(oHive.LoadedHive(True) & "\" & c_RegKey, , 4)

            SetPermission(cSiteKiosk_Username, Microsoft.Win32.RegistryHive.Users, oHive.LoadedHive(True) & "\" & c_RegKey)

            oLogger.WriteToLogRelative("UnloadHive", , 2)
            oHive.UnloadHive()

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        Try
            oLogger.WriteToLogRelative("trying it the other way", , 1)
            oLogger.WriteToLogRelative("getting sid for " & cSiteKiosk_Username, , 2)

            Dim sSid As String, sRegKey As String

            sSid = GetSidFromUsername_String(cSiteKiosk_Username)

            oLogger.WriteToLogRelative(sSid, , 3)

            sRegKey = sSid & "\" & c_RegKey
            oLogger.WriteToLogRelative("Set security", , 2)
            oLogger.WriteToLogRelative("key", , 3)
            oLogger.WriteToLogRelative("HKEY_USERS\" & sRegKey, , 4)

            SetPermission(cSiteKiosk_Username, Microsoft.Win32.RegistryHive.Users, sRegKey)
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try
    End Function

    Private Function GetSidFromUsername_String(ByVal sUsername As String) As String
        Try
            Dim ntSiteKiosk As New NTAccount(sUsername)
            Dim sidSiteKiosk As SecurityIdentifier

            sidSiteKiosk = ntSiteKiosk.Translate(GetType(SecurityIdentifier))

            Return sidSiteKiosk.Value
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function GetSidFromUsername_SecurityIdentifier(ByVal sUsername As String) As SecurityIdentifier
        Try
            Dim ntSiteKiosk As New NTAccount(sUsername)
            Dim sidSiteKiosk As SecurityIdentifier

            sidSiteKiosk = ntSiteKiosk.Translate(GetType(SecurityIdentifier))

            Return sidSiteKiosk
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Function SetPermission(ByVal sUsername As String, ByVal hive As Microsoft.Win32.RegistryHive, ByVal sRegKey As String) As Boolean
        oLogger.WriteToLogRelative("SetPermission", , 3)

        oLogger.WriteToLogRelative("params", , 4)
        oLogger.WriteToLogRelative("sUsername", , 5)
        oLogger.WriteToLogRelative(sUsername, , 6)
        oLogger.WriteToLogRelative("hive", , 5)
        oLogger.WriteToLogRelative(hive.ToString, , 6)
        oLogger.WriteToLogRelative("sRegKey", , 5)
        oLogger.WriteToLogRelative(sRegKey, , 6)

        Dim rk As Microsoft.Win32.RegistryKey = Nothing
        Try
            Select Case hive
                Case Microsoft.Win32.RegistryHive.CurrentUser
                    rk = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(sRegKey, True)
                Case Microsoft.Win32.RegistryHive.LocalMachine
                    rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sRegKey, True)
                Case Microsoft.Win32.RegistryHive.Users
                    rk = Microsoft.Win32.Registry.Users.OpenSubKey(sRegKey, True)
                Case Else
                    Return False
            End Select

            Dim user As String = Environment.MachineName & "\" & sUsername
            Dim rs As Security.AccessControl.RegistrySecurity
            Dim sid As SecurityIdentifier

            rs = rk.GetAccessControl
            sid = GetSidFromUsername_SecurityIdentifier(sUsername)

            oLogger.WriteToLogRelative("Going to mess about with acl, yippie", , 4)
            oLogger.WriteToLogRelative("SecurityIdentifier for " & sUsername, , 4)
            oLogger.WriteToLogRelative(sUsername & " = " & sid.Value, , 5)

            oLogger.WriteToLogRelative("purging", , 5)
            rs.PurgeAccessRules(sid)

            oLogger.WriteToLogRelative("adding", , 5)
            rs.AddAccessRule(New Security.AccessControl.RegistryAccessRule(sid, _
                Security.AccessControl.RegistryRights.FullControl, _
                Security.AccessControl.InheritanceFlags.None, _
                Security.AccessControl.PropagationFlags.None, _
                Security.AccessControl.AccessControlType.Allow))

            oLogger.WriteToLogRelative("setting", , 5)
            rk.SetAccessControl(rs)

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
        End Try
    End Function
End Module
