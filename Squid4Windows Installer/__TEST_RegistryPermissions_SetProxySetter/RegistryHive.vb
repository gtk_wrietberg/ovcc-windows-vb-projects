Imports System.Runtime.InteropServices
Imports System.ComponentModel

Class RegistryHive
    Private Structure TOKEN_PRIVILEGES
        Public PrivilegeCount As UInteger
        Public Privileges As LUID_AND_ATTRIBUTES

        Public Function Size() As Integer
            Return Marshal.SizeOf(Me)
        End Function
    End Structure

    Private Structure LUID
        Private lowPart As UInt32
        Private highPart As Int32
    End Structure

    Private Structure LUID_AND_ATTRIBUTES
        Public luid As LUID
        Public attributes As UInt32
    End Structure

    Private Const TOKEN_ADJUST_PRIVLEGES As UInteger = &H20UI
    Private Const TOKEN_QUERY As UInteger = &H8UI
    Private Const SE_PRIVILEGE_ENABLED As UInteger = &H2UI
    Private Const HKEY_USERS As UInteger = &H80000003UI
    Private Const SE_RESTORE_NAME As String = "SeRestorePrivilege"
    Private Const SE_BACKUP_NAME As String = "SeBackupPrivilege"

    ' System.Diagnostics.Process.Handle
    <DllImport("kernel32")> _
    Private Shared Function GetCurrentProcess() As IntPtr
    End Function

    <DllImport("advapi32", SetLastError:=True)> _
    Private Shared Function OpenProcessToken( _
    ByVal ProcessHandle As IntPtr, _
    ByVal DesiredAccess As UInteger, _
    ByRef TokenHandle As IntPtr) As Integer
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function LookupPrivilegeValue( _
    ByVal lpSystemName As String, _
    ByVal lpName As String, _
    ByRef lpLuid As LUID) As Integer
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function AdjustTokenPrivileges( _
    ByVal TokenHandle As IntPtr, _
    ByVal DisableAllPrivileges As Integer, _
    ByRef NewState As TOKEN_PRIVILEGES, _
    ByVal BufferLength As Integer, _
    ByRef PreviousState As TOKEN_PRIVILEGES, _
    ByRef ReturnLength As Integer) As Integer
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function RegLoadKey( _
    ByVal hKey As UInteger, _
    ByVal lpSubKey As String, _
    ByVal lpFile As String) As Integer
    End Function

    <DllImport("advapi32", CharSet:=CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function RegUnLoadKey( _
    ByVal hKey As UInteger, _
    ByVal lpSubKey As String) As Integer
    End Function

    Private Const cHiveKey As String = "SK_TEMP_HIVE"

    Private MyToken As IntPtr
    Private TP1 As TOKEN_PRIVILEGES
    Private TP2 As TOKEN_PRIVILEGES
    Private hBlob As IntPtr

    Private mLastError As String
    Private mLoaded As Boolean

    Public Sub New()

    End Sub

    Public ReadOnly Property LastError() As String
        Get
            Return mLastError
        End Get
    End Property

    Public ReadOnly Property LoadedHive(Optional ByVal bWithoutHive As Boolean = False) As String
        Get
            If bWithoutHive Then
                Return cHiveKey
            Else
                Return "HKEY_USERS\" & cHiveKey
            End If

        End Get
    End Property

    Public Function Initialise() As Boolean
        Try
            Dim hProcess As IntPtr = GetCurrentProcess() ' is -1 as it should.

            Dim retVal As Integer = OpenProcessToken( _
            hProcess, _
            TOKEN_ADJUST_PRIVLEGES Or TOKEN_QUERY, MyToken)

            If retVal = 0 Then Throw New Win32Exception
            Dim RestoreLuid As LUID

            retVal = LookupPrivilegeValue(Nothing, SE_RESTORE_NAME, RestoreLuid)

            If retVal = 0 Then Throw New Win32Exception
            Dim BackupLuid As LUID
            retVal = LookupPrivilegeValue(Nothing, SE_BACKUP_NAME, BackupLuid)
            If retVal = 0 Then Throw New Win32Exception
            TP1.PrivilegeCount = 1
            TP1.Privileges.attributes = SE_PRIVILEGE_ENABLED
            TP1.Privileges.luid = RestoreLuid
            Dim returnLength As Integer = 0
            Dim oldPrivileges As TOKEN_PRIVILEGES
            retVal = AdjustTokenPrivileges(MyToken, 0, TP1, TP1.Size, oldPrivileges, returnLength)
            If retVal = 0 Then
                Throw New Win32Exception
            End If
            TP2.PrivilegeCount = 1
            TP2.Privileges.attributes = SE_PRIVILEGE_ENABLED
            TP2.Privileges.luid = BackupLuid
            retVal = AdjustTokenPrivileges(MyToken, 0, TP2, TP2.Size, oldPrivileges, returnLength)
            If retVal = 0 Then
                Throw New Win32Exception
            End If
        Catch ex As Exception
            mLastError = ex.Message

            Return False
        End Try

        Return True
    End Function

    Public Sub LoadHive(ByVal sHivePath As String)
        Dim retval As Integer = RegLoadKey(HKEY_USERS, cHiveKey, sHivePath)

        If retval <> 0 Then
            mLastError = retval.ToString

            Throw New Win32Exception(retval, "LoadHive failed with error code " & retval.ToString)
        End If

        mLoaded = True
    End Sub

    Public Sub UnloadHive()
        Dim retval As Integer = RegUnLoadKey(HKEY_USERS, cHiveKey)
        If retval <> 0 Then
            mLastError = retval.ToString

            Throw New Win32Exception(retval, "UnloadHive failed with error code " & retval.ToString)
        End If

        mLoaded = False
    End Sub

    Public Sub SetValueInLoadedHive(ByVal RegKey As String, ByVal ValueName As String, ByVal Value As String)
        _SetValueInLoadedHive(RegKey, ValueName, Value, Microsoft.Win32.RegistryValueKind.String)
    End Sub

    Public Sub SetValueInLoadedHive(ByVal RegKey As String, ByVal ValueName As String, ByVal Value As Long)
        _SetValueInLoadedHive(RegKey, ValueName, Value, Microsoft.Win32.RegistryValueKind.DWord)
    End Sub

    Private Sub _SetValueInLoadedHive(ByVal RegKey As String, ByVal ValueName As String, ByVal Value As Object, ByVal ValueType As Microsoft.Win32.RegistryValueKind)
        Try
            Microsoft.Win32.Registry.SetValue("HKEY_USERS\" & cHiveKey & "\" & RegKey, ValueName, Value, ValueType)
        Catch ex As Exception
            mLastError = ex.Message

            Throw New Win32Exception("_SetValueInLoadedHive failed with error '" & ex.Message & "'", ex)
        End Try
    End Sub

End Class
