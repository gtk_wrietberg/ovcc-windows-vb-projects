Module Constants___Globals
    'Constants
    Public c__SetProxyServer_App As String = "SetProxyServer.exe"

    Public c__SquidInstallScript_Cmd As String = "install_script\squid_install.cmd"

    Public c__SetProxyServer_App_Log_Path As String = "SetProxyServer"
    Public c__SetProxyServer_App_Log_Name As String = "SetProxyServer.log"

    'Emailing
    Public Const c__SmtpServerName As String = "smtp.lobbypc.com"
    Public Const c__SmtpServerPort As Integer = 587
    Public Const c__SmtpServerUserName As String = "3MA1LY00ser"
    Public Const c__SmtpServerPassWord As String = "$3k00r3MI$MtP@cNt"
    Public Const c__Recipient As String = "lobbypc.report@gmail.com"

    'Registry
    Public Const cPolicy_Key__ProxySettingsPerUser As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings "
    Public Const cPolicy_Name__ProxySettingsPerUser As String = "ProxySettingsPerUser"
    Public Const cPolicy_Value__ProxySettingsPerUser As Integer = 1

    Public Const cRun_Key__ProxySettings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run"
    Public Const cRun_Name__ProxySettings As String = "SetProxyServer"

    Public Const ciBAHN_Key__ProxySettings As String = "HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\ProxySettings"
    Public Const ciBAHN_Name__ProxySetForUsers As String = "ProxySetForUsers"
    Public Const ciBAHN_Value__ProxySetForUsers As String = "sitekiosk"

    'Misc
    Public Const cSiteKiosk_Username As String = "sitekiosk"


    'Globals. Me like globals, me are lazy
    Public oLogger As Logger

    Public g_LogFileDirectory As String
    Public g_SetProxyServer_LogFileDirectory As String
    Public g_LogFileName As String
    Public g_BackupDirectory As String
    Public g_SetProxyServerDirectory As String

    Public Sub InitGlobals()
        Dim dDate As Date = Now()

        g_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_LogFileDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion

        g_SetProxyServer_LogFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_logs"
        g_SetProxyServer_LogFileDirectory &= "\" & c__SetProxyServer_App_Log_Path

        g_LogFileName = dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_") & ".log"

        g_BackupDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\_backups"
        g_BackupDirectory &= "\" & Application.ProductName & "\" & Application.ProductVersion
        g_BackupDirectory &= "\" & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")

        g_SetProxyServerDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) & "\iBAHN\SquidHelpers"

        IO.Directory.CreateDirectory(g_LogFileDirectory)
        IO.Directory.CreateDirectory(g_SetProxyServer_LogFileDirectory)

        If IO.Directory.Exists(g_SetProxyServer_LogFileDirectory) Then
            Try
                AddDirectorySecurity(g_SetProxyServer_LogFileDirectory, "sitekiosk", Security.AccessControl.FileSystemRights.Write, Security.AccessControl.AccessControlType.Allow)
                IO.File.Create(g_SetProxyServer_LogFileDirectory & "\" & c__SetProxyServer_App_Log_Name).Dispose()
                AddFileSecurity(g_SetProxyServer_LogFileDirectory & "\" & c__SetProxyServer_App_Log_Name, "sitekiosk", Security.AccessControl.FileSystemRights.Write, Security.AccessControl.AccessControlType.Allow)
            Catch ex As Exception

            End Try
        End If

        IO.Directory.CreateDirectory(g_BackupDirectory)
    End Sub
End Module
