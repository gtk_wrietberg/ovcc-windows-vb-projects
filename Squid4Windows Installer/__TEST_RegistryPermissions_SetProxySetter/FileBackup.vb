Module FileBackup
    Public Function BackupFile(ByVal sFile As String) As Boolean
        Dim sBackupName As String, sBackupFullPath As String
        Dim bRet As Boolean = False

        Dim oFileOriginal As New IO.FileInfo(sFile), oFileBackup As IO.FileInfo

        If Not oFileOriginal.Exists Then
            oLogger.WriteToLogRelative("file does not exist" & sFile, Logger.MESSAGE_TYPE.LOG_ERROR, 1)
            Return False
        End If

        Try
            Dim dDate As Date = Now()


            sBackupName = oFileOriginal.Name & ".backup." & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")
            sBackupFullPath = oFileOriginal.DirectoryName & "\" & sBackupName

            oLogger.WriteToLogRelative("original: " & sFile, , 1)
            oLogger.WriteToLogRelative("backup: " & sBackupFullPath, , 1)

            oFileOriginal.CopyTo(sBackupFullPath, True)

            oFileBackup = New System.IO.FileInfo(sBackupFullPath)
            If oFileBackup.Exists Then
                oLogger.WriteToLogRelative("ok", , 2)
                bRet = True
            Else
                oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            End If
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
            oLogger.WriteToLogRelative("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End Try

        oFileOriginal = Nothing
        oFileBackup = Nothing

        Return bRet
    End Function

End Module
