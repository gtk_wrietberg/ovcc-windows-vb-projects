Module UpdateProxySettingsModule
    Public Function UpdateProxySettings() As Boolean
        g_LogInfoForEmailReport.Proxy_Registry_Settings_Max = 5

        Try
            Dim oHive As RegistryHive
            Dim sSiteKioskRegistryHive As String

            oLogger.WriteToLogRelative("updating registry for SiteKiosk user", , 1)
            oLogger.WriteToLogRelative("approach #1: Loading hive file", , 2)
            g_LogInfoForEmailReport.Proxy_Registry_Settings_Method = "hive"
            g_LogInfoForEmailReport.Proxy_Registry_Settings = 0

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("initialising", , 3)
            oHive = New RegistryHive

            If oHive.Initialise Then
                oLogger.WriteToLog("ok", , 4)
            Else
                Throw New Exception("hive init failed: " & oHive.LastError)
            End If

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("loading SiteKiosk hive", , 3)
            If IO.File.Exists("C:\Users\" & cSiteKiosk_Username & "\ntuser.dat") Then
                sSiteKioskRegistryHive = "C:\Users\" & cSiteKiosk_Username & "\ntuser.dat"
            ElseIf IO.File.Exists("C:\Documents and Settings\" & cSiteKiosk_Username & "\ntuser.dat") Then
                sSiteKioskRegistryHive = "C:\Documents and Settings\" & cSiteKiosk_Username & "\ntuser.dat"
            Else
                Throw New Exception("no hive files found")
            End If
            oLogger.WriteToLogRelative("hive found", , 4)
            oLogger.WriteToLogRelative(sSiteKioskRegistryHive, , 5)

            oLogger.WriteToLogRelative("creating backup, just to be sure", , 4)
            Try
                BackupFile(sSiteKioskRegistryHive)
                oLogger.WriteToLogRelative("ok", , 5)
            Catch ex As Exception
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End Try

            oLogger.WriteToLogRelative("loading", , 4)
            oHive.LoadHive(sSiteKioskRegistryHive)
            oLogger.WriteToLogRelative("loaded", , 4)

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("setting values", , 3)

            g_LogInfoForEmailReport.Proxy_Registry_Settings = 0
            Try
                oLogger.WriteToLogRelative("key  : " & oHive.LoadedHive & "\" & cSiteKioskRegistry_Key__InternetSettings, , 4)
                oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__MigrateProxy, , 4)
                oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__MigrateProxy.ToString, , 4)
                oHive.SetValueInLoadedHive( _
                        cSiteKioskRegistry_Key__InternetSettings, _
                        cSiteKioskRegistry_Name__MigrateProxy, _
                        cSiteKioskRegistry_Value__MigrateProxy)
                oLogger.WriteToLogRelative("ok", , 5)
                g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

                oLogger.WriteToLogRelative("key  : " & oHive.LoadedHive & "\" & cSiteKioskRegistry_Key__InternetSettings, , 4)
                oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyHttp1_1, , 4)
                oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyHttp1_1.ToString, , 4)
                oHive.SetValueInLoadedHive( _
                        cSiteKioskRegistry_Key__InternetSettings, _
                        cSiteKioskRegistry_Name__ProxyHttp1_1, _
                        cSiteKioskRegistry_Value__ProxyHttp1_1)
                oLogger.WriteToLogRelative("ok", , 5)
                g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

                oLogger.WriteToLogRelative("key  : " & oHive.LoadedHive & "\" & cSiteKioskRegistry_Key__InternetSettings, , 4)
                oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyServer, , 4)
                oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyServer.ToString, , 4)
                oHive.SetValueInLoadedHive( _
                        cSiteKioskRegistry_Key__InternetSettings, _
                        cSiteKioskRegistry_Name__ProxyServer, _
                        cSiteKioskRegistry_Value__ProxyServer)
                oLogger.WriteToLogRelative("ok", , 5)
                g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

                oLogger.WriteToLogRelative("key  : " & oHive.LoadedHive & "\" & cSiteKioskRegistry_Key__InternetSettings, , 4)
                oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyOverride, , 4)
                oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyOverride.ToString, , 4)
                oHive.SetValueInLoadedHive( _
                        cSiteKioskRegistry_Key__InternetSettings, _
                        cSiteKioskRegistry_Name__ProxyOverride, _
                        cSiteKioskRegistry_Value__ProxyOverride)
                oLogger.WriteToLogRelative("ok", , 5)
                g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

                oLogger.WriteToLogRelative("key  : " & oHive.LoadedHive & "\" & cSiteKioskRegistry_Key__InternetSettings, , 4)
                oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyEnable, , 4)
                oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyEnable.ToString, , 4)
                oHive.SetValueInLoadedHive( _
                        cSiteKioskRegistry_Key__InternetSettings, _
                        cSiteKioskRegistry_Name__ProxyEnable, _
                        cSiteKioskRegistry_Value__ProxyEnable)
                oLogger.WriteToLogRelative("ok", , 5)
                g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()
            Catch ex As Exception
                oLogger.WriteToLogRelative("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLogRelative("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                oLogger.WriteToLogRelative(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
            End Try

            '------------------------------------------------------------------------------------
            oLogger.WriteToLogRelative("unloading SiteKiosk hive", , 2)
            oHive.UnloadHive()
            oLogger.WriteToLogRelative("unloaded", , 3)

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("EXCEPTION", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative("ex.Message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            oLogger.WriteToLogRelative("ex.Source", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLogRelative(ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
        End Try

        '*************************************************************************************************************************
        oLogger.WriteToLogRelative("approach #2: Loading sid key", , 2)
        g_LogInfoForEmailReport.Proxy_Registry_Settings_Method = "sid"
        g_LogInfoForEmailReport.Proxy_Registry_Settings = 0

        Try
            Dim sSid As String, sRegKeyWithHive As String, sRegKey As String
            Dim regkeyInternetSettings As Microsoft.Win32.RegistryKey

            oLogger.WriteToLogRelative("getting sid for " & cSiteKiosk_Username, , 3)
            sSid = GetSidFromUsername(cSiteKiosk_Username)
            oLogger.WriteToLogRelative(sSid, , 3)

            sRegKey = sSid & "\" & cSiteKioskRegistry_Key__InternetSettings
            sRegKeyWithHive = "HKEY_USERS\" & sRegKey

            regkeyInternetSettings = Microsoft.Win32.Registry.Users.OpenSubKey(sRegKey, True)

            If regkeyInternetSettings Is Nothing Then
                Throw New Exception("reg key does not exist: " & sRegKeyWithHive)
            End If

            oLogger.WriteToLogRelative("setting values", , 3)

            oLogger.WriteToLogRelative("key  : " & sRegKeyWithHive, , 4)

            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__MigrateProxy, , 4)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__MigrateProxy.ToString, , 4)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__MigrateProxy, cSiteKioskRegistry_Value__MigrateProxy, Microsoft.Win32.RegistryValueKind.DWord)
            oLogger.WriteToLogRelative("ok", , 5)
            g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyHttp1_1, , 4)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyHttp1_1.ToString, , 4)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyHttp1_1, cSiteKioskRegistry_Value__ProxyHttp1_1, Microsoft.Win32.RegistryValueKind.DWord)
            oLogger.WriteToLogRelative("ok", , 5)
            g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyServer, , 4)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyServer.ToString, , 4)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyServer, cSiteKioskRegistry_Value__ProxyServer, Microsoft.Win32.RegistryValueKind.String)
            oLogger.WriteToLogRelative("ok", , 5)
            g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyOverride, 4)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyOverride.ToString, , 4)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyOverride, cSiteKioskRegistry_Value__ProxyOverride, Microsoft.Win32.RegistryValueKind.String)
            oLogger.WriteToLogRelative("ok", , 5)
            g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

            oLogger.WriteToLogRelative("name : " & cSiteKioskRegistry_Name__ProxyEnable, , 4)
            oLogger.WriteToLogRelative("value: " & cSiteKioskRegistry_Value__ProxyEnable.ToString, , 4)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyEnable, cSiteKioskRegistry_Value__ProxyEnable, Microsoft.Win32.RegistryValueKind.DWord)
            oLogger.WriteToLogRelative("ok", , 5)
            g_LogInfoForEmailReport.Proxy_Registry_Settings_Add()

            Try
                regkeyInternetSettings.Close()
            Catch ex As Exception
            End Try

            Return True
        Catch ex As Exception
            oLogger.WriteToLogRelative("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLogRelative(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
        End Try

            Return False
    End Function

    Private Function GetSidFromUsername(ByVal sUsername As String) As String
        Try
            Dim ntSiteKiosk As New Security.Principal.NTAccount(sUsername)
            Dim sidSiteKiosk As Security.Principal.SecurityIdentifier

            sidSiteKiosk = ntSiteKiosk.Translate(GetType(Security.Principal.SecurityIdentifier))

            Return sidSiteKiosk.Value
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Module
