Module FileBackup
    Public Sub BackupFile(ByVal sFile As String)
        Dim sBackupName As String, sBackupFullPath As String
        Dim bRet As Boolean = False

        Dim oFileOriginal As New IO.FileInfo(sFile), oFileBackup As IO.FileInfo

        If Not oFileOriginal.Exists Then
            Throw New IO.IOException("file does not exist" & sFile)
        End If

        Dim dDate As Date = Now()


        sBackupName = oFileOriginal.Name & ".backup." & dDate.ToString("s").Replace("-", "").Replace(":", "").Replace("T", "_")
        sBackupFullPath = oFileOriginal.DirectoryName & "\" & sBackupName

        oFileOriginal.CopyTo(sBackupFullPath, True)

        oFileBackup = New System.IO.FileInfo(sBackupFullPath)
        If oFileBackup.Exists Then
            bRet = True
        Else
            Throw New IO.IOException("backup file does not exist" & sBackupFullPath)
        End If
    End Sub
End Module
