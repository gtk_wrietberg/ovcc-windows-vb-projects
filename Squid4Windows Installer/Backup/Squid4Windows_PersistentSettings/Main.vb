Module Main
    Public Sub Main()
        Try
            Dim cCheck As New CheckPage
            Dim sBlockedUrl As String = ""
            For i As Integer = 0 To c_BlockedPages.Length - 1
                If cCheck.IsOk(c_BlockedPages(i)) Then
                    If sBlockedUrl.Equals("") Then
                        sBlockedUrl = c_BlockedPages(i)
                    End If
                End If
            Next

            Dim sFileRead As String, sFileWrite As String
            sFileRead = "C:\usr\local\SquidGuard\squidGuard.conf.TEMPLATE"
            sFileWrite = "C:\usr\local\SquidGuard\squidGuard.conf"

            If Not IO.File.Exists(sFileRead) Then
                Throw New Exception(sFileRead & " does not exist")
            Else
                Dim lines As New List(Of String)

                Using sr As New IO.StreamReader(sFileRead)
                    Dim line As String

                    While Not sr.EndOfStream
                        line = sr.ReadLine

                        If line.Contains("%%BLOCKED_PAGE_URL%%") Then
                            line = line.Replace("%%BLOCKED_PAGE_URL%%", sBlockedUrl)
                        End If

                        lines.Add(line)
                    End While
                End Using

                Using sw As New IO.StreamWriter(sFileWrite)
                    For Each line As String In lines
                        sw.WriteLine(line)
                    Next
                End Using
            End If
        Catch ex As Exception

        End Try


        Try
            Microsoft.Win32.Registry.SetValue(cPolicy_Key__ProxySettingsPerUser, cPolicy_Name__ProxySettingsPerUser, cPolicy_Value__ProxySettingsPerUser, Microsoft.Win32.RegistryValueKind.DWord)
        Catch ex As Exception

        End Try

        Try
            Dim regkeyInternetSettings As Microsoft.Win32.RegistryKey

            regkeyInternetSettings = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(cSiteKioskRegistry_Key__InternetSettings, True)

            If regkeyInternetSettings Is Nothing Then
                Throw New Exception("reg key does not exist: " & cSiteKioskRegistry_Key__InternetSettings)
            End If

            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__MigrateProxy, cSiteKioskRegistry_Value__MigrateProxy, Microsoft.Win32.RegistryValueKind.DWord)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyHttp1_1, cSiteKioskRegistry_Value__ProxyHttp1_1, Microsoft.Win32.RegistryValueKind.DWord)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyServer, cSiteKioskRegistry_Value__ProxyServer, Microsoft.Win32.RegistryValueKind.String)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyOverride, cSiteKioskRegistry_Value__ProxyOverride, Microsoft.Win32.RegistryValueKind.String)
            regkeyInternetSettings.SetValue(cSiteKioskRegistry_Name__ProxyEnable, cSiteKioskRegistry_Value__ProxyEnable, Microsoft.Win32.RegistryValueKind.DWord)
        Catch ex As Exception

        End Try


    End Sub
End Module
