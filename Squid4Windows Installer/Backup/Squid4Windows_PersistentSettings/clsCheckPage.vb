Public Class clsCheckPage
    Private mLastMessage As String



    Public ReadOnly Property LastMessage() As String
        Get
            Return mLastMessage
        End Get
    End Property

    Public Function IsOk(ByVal sUrl As String) As Boolean
        Try
            If Not sUrl.StartsWith("http://") And Not sUrl.StartsWith("https://") Then
                sUrl = "http://" & sUrl
            End If

            Dim myWebRequest As Net.HttpWebRequest = CType(Net.WebRequest.Create(sUrl), Net.HttpWebRequest)

            myWebRequest.Timeout = 5000

            Dim myWebResponse As Net.HttpWebResponse = CType(myWebRequest.GetResponse(), Net.HttpWebResponse)

            mLastMessage = myWebResponse.StatusCode

            If myWebResponse.StatusCode = Net.HttpStatusCode.OK Then
                Return True
            End If
        Catch ex As Exception
            mLastMessage = ex.Message
        End Try

        Return False
    End Function

    Public Sub New()

    End Sub
End Class
