Module Constants___Globals
    'Constants

    'Registry
    Public Const cPolicy_Key__ProxySettingsPerUser As String = "HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CurrentVersion\Internet Settings"
    Public Const cPolicy_Name__ProxySettingsPerUser As String = "ProxySettingsPerUser"
    Public Const cPolicy_Value__ProxySettingsPerUser As Integer = 0 'system wide


    Public Const cSiteKioskRegistry_Key__InternetSettings As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings"

    Public Const cSiteKioskRegistry_Name__MigrateProxy As String = "MigrateProxy"
    Public Const cSiteKioskRegistry_Value__MigrateProxy As Long = 1

    Public Const cSiteKioskRegistry_Name__ProxyEnable As String = "ProxyEnable"
    Public Const cSiteKioskRegistry_Value__ProxyEnable As Long = 1
    Public Const cSiteKioskRegistry_Value__ProxyDisable As Long = 0

    Public Const cSiteKioskRegistry_Name__ProxyHttp1_1 As String = "ProxyHttp1.1"
    Public Const cSiteKioskRegistry_Value__ProxyHttp1_1 As Long = 1

    Public Const cSiteKioskRegistry_Name__ProxyServer As String = "ProxyServer"
    Public Const cSiteKioskRegistry_Value__ProxyServer As String = "127.0.0.1:8080"

    Public Const cSiteKioskRegistry_Name__ProxyOverride As String = "ProxyOverride"
    Public Const cSiteKioskRegistry_Value__ProxyOverride As String = "<local>"

    'Misc
    Public Const cSiteKiosk_Username As String = "sitekiosk"


    'Possible blocked pages
    Public ReadOnly c_BlockedPages() As String = {"lobbypc.ibahn.com:8050/blocked/", "172.18.84.26:8050/blocked/", "72.254.0.131:8050/blocked/"}

End Module
