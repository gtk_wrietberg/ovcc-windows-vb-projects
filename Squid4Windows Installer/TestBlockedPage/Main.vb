Module Main
    Public Sub main()
        Dim sUrls() As String = {"lobbypc.ibahn.com:8050/blocked/", "172.18.84.26:8050/blocked/", "72.254.0.131:8050/blocked/"}
        Dim mCP As New clsCheckPage
        Dim s As String = ""
        Dim iMax As Integer = 0

        Console.WriteLine(" ")
        Console.WriteLine("Testing Blocked page...")
        Console.WriteLine(" ")

        For i As Integer = 0 To sUrls.Length - 1
            If sUrls(i).Length > iMax Then
                iMax = sUrls(i).Length
            End If
        Next

        For i As Integer = 0 To sUrls.Length - 1
            s = sUrls(i) & (New String(" ", (iMax + 1) - sUrls(i).Length))
            mCP.IsOk(sUrls(i))
            s &= mCP.LastMessage
            Console.WriteLine(s)
        Next

        Console.WriteLine(" ")
    End Sub
End Module
