Module modFindStringInFile
    Public Function CountStringInFile(ByVal sFilePath As String, ByVal sSearchString As String) As Integer
        Dim sReadAll As String = ""

        If Not IO.File.Exists(sFilePath) Then
            Return -1
        End If

        Dim oFileStream As New IO.StreamReader(sFilePath)

        sReadAll = oFileStream.ReadToEnd

        oFileStream.Close()

        Return System.Text.RegularExpressions.Regex.Matches(sReadAll, sSearchString).Count
    End Function
End Module
