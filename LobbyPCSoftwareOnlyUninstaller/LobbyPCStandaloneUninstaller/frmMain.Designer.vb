<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblTitle = New System.Windows.Forms.Label
        Me.picboxLogo = New System.Windows.Forms.PictureBox
        Me.pnlStartScreen = New System.Windows.Forms.Panel
        Me.lblWarning = New System.Windows.Forms.Label
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnNext = New System.Windows.Forms.Button
        Me.pnlPrerequisites = New System.Windows.Forms.Panel
        Me.lblPrerequisitesRestart = New System.Windows.Forms.Label
        Me.lblPrerequisitesError = New System.Windows.Forms.Label
        Me.pnlLicenseKeyError = New System.Windows.Forms.Panel
        Me.tmrLicenseValidationDelay = New System.Windows.Forms.Timer(Me.components)
        Me.pnlUninstallation = New System.Windows.Forms.Panel
        Me.lblUninstalling = New System.Windows.Forms.Label
        Me.lblProgress = New System.Windows.Forms.Label
        Me.progressMarquee = New System.Windows.Forms.ProgressBar
        Me.pnlDone = New System.Windows.Forms.Panel
        Me.lblDone = New System.Windows.Forms.Label
        Me.tmrInstallerWait = New System.Windows.Forms.Timer(Me.components)
        Me.btnRetry = New System.Windows.Forms.Button
        Me.lblLicenseKeyError = New System.Windows.Forms.Label
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlStartScreen.SuspendLayout()
        Me.pnlPrerequisites.SuspendLayout()
        Me.pnlLicenseKeyError.SuspendLayout()
        Me.pnlUninstallation.SuspendLayout()
        Me.pnlDone.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.BackColor = System.Drawing.Color.Transparent
        Me.lblTitle.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.lblTitle.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(134, 12)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(490, 68)
        Me.lblTitle.TabIndex = 4
        Me.lblTitle.Text = "LobbyPC Software uninstaller"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'picboxLogo
        '
        Me.picboxLogo.BackColor = System.Drawing.Color.Transparent
        Me.picboxLogo.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.picboxLogo.Image = Global.LobbyPCSoftwareOnlyUninstaller_.My.Resources.Resources.iBAHN_logo_01
        Me.picboxLogo.Location = New System.Drawing.Point(8, 10)
        Me.picboxLogo.Name = "picboxLogo"
        Me.picboxLogo.Size = New System.Drawing.Size(120, 68)
        Me.picboxLogo.TabIndex = 28
        Me.picboxLogo.TabStop = False
        '
        'pnlStartScreen
        '
        Me.pnlStartScreen.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlStartScreen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlStartScreen.Controls.Add(Me.lblWarning)
        Me.pnlStartScreen.Location = New System.Drawing.Point(16, 338)
        Me.pnlStartScreen.Name = "pnlStartScreen"
        Me.pnlStartScreen.Size = New System.Drawing.Size(608, 100)
        Me.pnlStartScreen.TabIndex = 29
        '
        'lblWarning
        '
        Me.lblWarning.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWarning.ForeColor = System.Drawing.Color.DarkRed
        Me.lblWarning.Location = New System.Drawing.Point(3, 21)
        Me.lblWarning.Name = "lblWarning"
        Me.lblWarning.Size = New System.Drawing.Size(600, 57)
        Me.lblWarning.TabIndex = 0
        Me.lblWarning.Text = "This uninstaller will remove the LobbyPC Software product from this computer." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
            "If you want to continue, please click 'NEXT'"
        Me.lblWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnCancel.Location = New System.Drawing.Point(8, 190)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(197, 36)
        Me.btnCancel.TabIndex = 30
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnNext
        '
        Me.btnNext.BackColor = System.Drawing.Color.Transparent
        Me.btnNext.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNext.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnNext.Location = New System.Drawing.Point(419, 190)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(197, 36)
        Me.btnNext.TabIndex = 31
        Me.btnNext.Text = "NEXT"
        Me.btnNext.UseVisualStyleBackColor = False
        '
        'pnlPrerequisites
        '
        Me.pnlPrerequisites.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlPrerequisites.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlPrerequisites.Controls.Add(Me.lblPrerequisitesRestart)
        Me.pnlPrerequisites.Controls.Add(Me.lblPrerequisitesError)
        Me.pnlPrerequisites.Location = New System.Drawing.Point(8, 83)
        Me.pnlPrerequisites.Name = "pnlPrerequisites"
        Me.pnlPrerequisites.Size = New System.Drawing.Size(608, 100)
        Me.pnlPrerequisites.TabIndex = 33
        '
        'lblPrerequisitesRestart
        '
        Me.lblPrerequisitesRestart.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrerequisitesRestart.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.lblPrerequisitesRestart.Location = New System.Drawing.Point(4, 80)
        Me.lblPrerequisitesRestart.Name = "lblPrerequisitesRestart"
        Me.lblPrerequisitesRestart.Size = New System.Drawing.Size(598, 16)
        Me.lblPrerequisitesRestart.TabIndex = 1
        Me.lblPrerequisitesRestart.Text = "Select CANCEL to exit the uninstallation."
        Me.lblPrerequisitesRestart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPrerequisitesError
        '
        Me.lblPrerequisitesError.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrerequisitesError.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.lblPrerequisitesError.Location = New System.Drawing.Point(4, 4)
        Me.lblPrerequisitesError.Name = "lblPrerequisitesError"
        Me.lblPrerequisitesError.Size = New System.Drawing.Size(598, 72)
        Me.lblPrerequisitesError.TabIndex = 0
        Me.lblPrerequisitesError.Text = "Here come the error messages"
        Me.lblPrerequisitesError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlLicenseKeyError
        '
        Me.pnlLicenseKeyError.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlLicenseKeyError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlLicenseKeyError.Controls.Add(Me.lblLicenseKeyError)
        Me.pnlLicenseKeyError.Location = New System.Drawing.Point(16, 550)
        Me.pnlLicenseKeyError.Name = "pnlLicenseKeyError"
        Me.pnlLicenseKeyError.Size = New System.Drawing.Size(608, 100)
        Me.pnlLicenseKeyError.TabIndex = 34
        '
        'tmrLicenseValidationDelay
        '
        Me.tmrLicenseValidationDelay.Interval = 500
        '
        'pnlUninstallation
        '
        Me.pnlUninstallation.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlUninstallation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlUninstallation.Controls.Add(Me.lblUninstalling)
        Me.pnlUninstallation.Controls.Add(Me.lblProgress)
        Me.pnlUninstallation.Controls.Add(Me.progressMarquee)
        Me.pnlUninstallation.Location = New System.Drawing.Point(16, 656)
        Me.pnlUninstallation.Name = "pnlUninstallation"
        Me.pnlUninstallation.Size = New System.Drawing.Size(608, 142)
        Me.pnlUninstallation.TabIndex = 34
        '
        'lblUninstalling
        '
        Me.lblUninstalling.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUninstalling.ForeColor = System.Drawing.Color.DarkRed
        Me.lblUninstalling.Location = New System.Drawing.Point(3, 0)
        Me.lblUninstalling.Name = "lblUninstalling"
        Me.lblUninstalling.Size = New System.Drawing.Size(600, 85)
        Me.lblUninstalling.TabIndex = 37
        Me.lblUninstalling.Text = "Removing LobbyPC Software and files." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Please do not reboot or shutdown the pc."
        Me.lblUninstalling.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblProgress
        '
        Me.lblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProgress.Location = New System.Drawing.Point(11, 121)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(584, 19)
        Me.lblProgress.TabIndex = 36
        Me.lblProgress.Text = "73/100"
        Me.lblProgress.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'progressMarquee
        '
        Me.progressMarquee.BackColor = System.Drawing.Color.FromArgb(CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.progressMarquee.ForeColor = System.Drawing.Color.Firebrick
        Me.progressMarquee.Location = New System.Drawing.Point(11, 88)
        Me.progressMarquee.Name = "progressMarquee"
        Me.progressMarquee.Size = New System.Drawing.Size(580, 30)
        Me.progressMarquee.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.progressMarquee.TabIndex = 35
        Me.progressMarquee.UseWaitCursor = True
        '
        'pnlDone
        '
        Me.pnlDone.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer), CType(CType(248, Byte), Integer))
        Me.pnlDone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlDone.Controls.Add(Me.lblDone)
        Me.pnlDone.Location = New System.Drawing.Point(16, 804)
        Me.pnlDone.Name = "pnlDone"
        Me.pnlDone.Size = New System.Drawing.Size(608, 100)
        Me.pnlDone.TabIndex = 36
        '
        'lblDone
        '
        Me.lblDone.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDone.ForeColor = System.Drawing.Color.DarkRed
        Me.lblDone.Location = New System.Drawing.Point(3, 21)
        Me.lblDone.Name = "lblDone"
        Me.lblDone.Size = New System.Drawing.Size(600, 57)
        Me.lblDone.TabIndex = 1
        Me.lblDone.Text = "Finished. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Not all items could be deleted." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Please reboot the pc, and the unin" & _
            "stallation again."
        Me.lblDone.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tmrInstallerWait
        '
        Me.tmrInstallerWait.Interval = 1000
        '
        'btnRetry
        '
        Me.btnRetry.BackColor = System.Drawing.Color.Transparent
        Me.btnRetry.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetry.ForeColor = System.Drawing.Color.FromArgb(CType(CType(158, Byte), Integer), CType(CType(19, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.btnRetry.Location = New System.Drawing.Point(213, 190)
        Me.btnRetry.Name = "btnRetry"
        Me.btnRetry.Size = New System.Drawing.Size(197, 36)
        Me.btnRetry.TabIndex = 37
        Me.btnRetry.Text = "RETRY"
        Me.btnRetry.UseVisualStyleBackColor = False
        '
        'lblLicenseKeyError
        '
        Me.lblLicenseKeyError.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLicenseKeyError.ForeColor = System.Drawing.Color.DarkRed
        Me.lblLicenseKeyError.Location = New System.Drawing.Point(3, 21)
        Me.lblLicenseKeyError.Name = "lblLicenseKeyError"
        Me.lblLicenseKeyError.Size = New System.Drawing.Size(600, 57)
        Me.lblLicenseKeyError.TabIndex = 1
        Me.lblLicenseKeyError.Text = "License validation error:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "This license key was not the one used for this insta" & _
            "llation"
        Me.lblLicenseKeyError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(1053, 779)
        Me.Controls.Add(Me.btnRetry)
        Me.Controls.Add(Me.pnlDone)
        Me.Controls.Add(Me.pnlUninstallation)
        Me.Controls.Add(Me.pnlLicenseKeyError)
        Me.Controls.Add(Me.pnlPrerequisites)
        Me.Controls.Add(Me.btnNext)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.pnlStartScreen)
        Me.Controls.Add(Me.picboxLogo)
        Me.Controls.Add(Me.lblTitle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.Text = "LobbyPC uninstaller"
        CType(Me.picboxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlStartScreen.ResumeLayout(False)
        Me.pnlPrerequisites.ResumeLayout(False)
        Me.pnlLicenseKeyError.ResumeLayout(False)
        Me.pnlUninstallation.ResumeLayout(False)
        Me.pnlDone.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents picboxLogo As System.Windows.Forms.PictureBox
    Friend WithEvents pnlStartScreen As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents lblWarning As System.Windows.Forms.Label
    Friend WithEvents pnlPrerequisites As System.Windows.Forms.Panel
    Friend WithEvents lblPrerequisitesRestart As System.Windows.Forms.Label
    Friend WithEvents lblPrerequisitesError As System.Windows.Forms.Label
    Friend WithEvents pnlLicenseKeyError As System.Windows.Forms.Panel
    Friend WithEvents tmrLicenseValidationDelay As System.Windows.Forms.Timer
    Friend WithEvents pnlUninstallation As System.Windows.Forms.Panel
    Friend WithEvents progressMarquee As System.Windows.Forms.ProgressBar
    Friend WithEvents pnlDone As System.Windows.Forms.Panel
    Friend WithEvents lblDone As System.Windows.Forms.Label
    Friend WithEvents tmrInstallerWait As System.Windows.Forms.Timer
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents lblUninstalling As System.Windows.Forms.Label
    Friend WithEvents btnRetry As System.Windows.Forms.Button
    Friend WithEvents lblLicenseKeyError As System.Windows.Forms.Label

End Class
