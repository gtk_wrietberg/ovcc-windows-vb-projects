Imports System.Net
Imports System.Environment

Public Class Prequisites
    Public Enum WINDOWS_VERSION
        UNKNOWN
        WINDOWS_95
        WINDOWS_98
        WINDOWS_MILLENIUM
        WINDOWS_NT_3
        WINDOWS_NT_4
        WINDOWS_2000
        WINDOWS_XP
        WINDOWS_VISTA
        WINDOWS_7
    End Enum

    Private mWindowsVersion As WINDOWS_VERSION

    Private mCheckedWindowsVersion As Boolean
    Private mCorrectWindowsVersion As Boolean
    Private mHigherWindowsVersionsAreAccepted As Boolean

    Private mCheckedInternetConnection As Boolean
    Private mCorrectInternetConnection As Boolean

    Public Sub New()
        mCheckedWindowsVersion = False
        mCorrectWindowsVersion = False

        mCheckedInternetConnection = False
        mCorrectInternetConnection = False
    End Sub

    Public ReadOnly Property CheckComplete() As Boolean
        Get
            Return (mCheckedWindowsVersion And mCheckedInternetConnection)
        End Get
    End Property

    Public ReadOnly Property AllIsOk() As Boolean
        Get
            Return (mCorrectWindowsVersion And mCorrectInternetConnection)
        End Get
    End Property

    Public ReadOnly Property CorrectWindowsVersion() As Boolean
        Get
            Return mCorrectWindowsVersion
        End Get
    End Property

    Public ReadOnly Property HigherWindowsVersionsAreAccepted() As Boolean
        Get
            Return mHigherWindowsVersionsAreAccepted
        End Get
    End Property

    Public ReadOnly Property InternetConnectivity() As Boolean
        Get
            Return mCorrectInternetConnection
        End Get
    End Property

    Public ReadOnly Property WindowsVersion() As String
        Get
            Return HumanReadableWindowsVersion(mWindowsVersion)
        End Get
    End Property

    Public Function CheckWindowsVersion(ByVal MinimumVersion As WINDOWS_VERSION, Optional ByVal OrHigher As Boolean = False) As Boolean
        mWindowsVersion = TranslateWindowsVersion(OSVersion.Platform, OSVersion.Version.Major.ToString, OSVersion.Version.Minor.ToString)
        mHigherWindowsVersionsAreAccepted = OrHigher

        If OrHigher Then
            mCorrectWindowsVersion = (mWindowsVersion >= MinimumVersion)
        Else
            mCorrectWindowsVersion = (mWindowsVersion = MinimumVersion)
        End If

        mCheckedWindowsVersion = True

        Return mCorrectWindowsVersion
    End Function

    Public Function CheckInternetConnection() As Boolean
        Dim objUrl As New System.Uri("http://www.google.com/")
        Dim objWebReq As WebRequest
        objWebReq = WebRequest.Create(objUrl)
        Dim objResp As WebResponse

        Try
            objResp = objWebReq.GetResponse
            objResp.Close()
            objWebReq = Nothing
            mCorrectInternetConnection = True
        Catch ex As Exception
            objResp.Close()
            objWebReq = Nothing
            mCorrectInternetConnection = False
        End Try

        mCheckedInternetConnection = True

        Return mCorrectInternetConnection
    End Function

    Private Function TranslateWindowsVersion(ByVal PlatformID As String, ByVal MajorVersion As String, ByVal MinorVersion As String) As WINDOWS_VERSION
        Dim sVersionString As String

        sVersionString = PlatformID & "." & MajorVersion & "." & MinorVersion
        Select Case sVersionString
            Case "1.4.0"
                Return WINDOWS_VERSION.WINDOWS_95
            Case "1.4.10"
                Return WINDOWS_VERSION.WINDOWS_98
            Case "1.4.98"
                Return WINDOWS_VERSION.WINDOWS_MILLENIUM
            Case "2.3.51"
                Return WINDOWS_VERSION.WINDOWS_NT_3
            Case "2.4.0"
                Return WINDOWS_VERSION.WINDOWS_NT_4
            Case "2.5.0"
                Return WINDOWS_VERSION.WINDOWS_2000
            Case "2.5.1"
                Return WINDOWS_VERSION.WINDOWS_XP
            Case "2.6.0"
                Return WINDOWS_VERSION.WINDOWS_VISTA
            Case "2.6.1"
                Return WINDOWS_VERSION.WINDOWS_7
            Case Else
                Return WINDOWS_VERSION.UNKNOWN
        End Select
    End Function

    Public Function HumanReadableWindowsVersion(ByVal WindowsVersion As WINDOWS_VERSION) As String
        Select Case WindowsVersion
            Case WINDOWS_VERSION.WINDOWS_95
                Return "Windows 95"
            Case WINDOWS_VERSION.WINDOWS_98
                Return "Windows 98"
            Case WINDOWS_VERSION.WINDOWS_MILLENIUM
                Return "Windows ME"
            Case WINDOWS_VERSION.WINDOWS_NT_3
                Return "Windows NT3"
            Case WINDOWS_VERSION.WINDOWS_NT_4
                Return "Windows NT4"
            Case WINDOWS_VERSION.WINDOWS_2000
                Return "Windows 2000"
            Case WINDOWS_VERSION.WINDOWS_XP
                Return "Windows XP"
            Case WINDOWS_VERSION.WINDOWS_VISTA
                Return "Windows Vista"
            Case WINDOWS_VERSION.WINDOWS_7
                Return "Windows 7"
            Case Else
                Return "Unknown Windows version"
        End Select
    End Function
End Class
