Imports System.Threading

Public Class frmMain
    Private oPrerequisites As Prerequisites
    Private WithEvents oProcess As ProcessRunner
    Private WithEvents oLicense As LicenseKey
    Private oComputerName As ComputerName
    Private Panels() As Panel
    Private iActiveGroupBox As Integer

    Private installThread As Thread

    Private iApplicationInstallationCount As Integer = 0
    Private iApplicationInstallationCountMax As Integer = 11

    Private bSiteKioskUninstallTimeout As Boolean = False

    Private iSeriousErrorCount As Integer = 0

    Private Enum GroupBoxes As Integer
        Prerequisites = 0
        StartScreen
        Uninstallation
        LicenseKeyError
        Done
    End Enum

    Private Enum InstallationProgress As Integer
        ValidateLicence = 0
        StopServices
        UninstallSiteKiosk
        UninstallSiteKioskBruteForce
        UninstallAltiris
        ResetComputerAndWorkgroupName
        CleanUpLocalUsers
        CleanUpRegistry
        CleanUpFiles
        UninstallLogMeIn
        Done
    End Enum

    Public Sub New()
        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Panels = New Panel() {Me.pnlPrerequisites, Me.pnlStartScreen, Me.pnlUninstallation, Me.pnlLicenseKeyError, Me.pnlDone}
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        gDebug = True
        gAllowBruteForce = False

        Me.Width = 630
        Me.Height = 260

        oPrerequisites = New Prerequisites
        oComputerName = New ComputerName
        oLicense = New LicenseKey
        oLogger = New Logger

        If InStr(oComputerName.ComputerName.ToLower, "rietberg".ToLower) > 0 _
        Or InStr(oComputerName.ComputerName.ToLower, "superlekkerding".ToLower) > 0 Then
            gTestMode = True

            If MsgBox("Test mode! Continue?", MsgBoxStyle.YesNo Or MsgBoxStyle.Question, Application.ProductName) <> MsgBoxResult.Yes Then
                Application.Exit()
            End If
        End If

        iApplicationInstallationCountMax = InstallationProgress.Done


        Dim sTmpLicenseKey As String

        sTmpLicenseKey = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & cREGKEY_LPCSOFTWARE, "LicenseKey", "")

        If sTmpLicenseKey Is Nothing Then
            sTmpLicenseKey = ""
        End If

        gLicenseKeyFromRegistry = sTmpLicenseKey


        Dim sArg As String

        For Each sArg In My.Application.CommandLineArgs
            sArg = sArg.ToLower

            If sArg = "-debug" Then
                gDebug = True
            End If
            If sArg = "-allow_brute_force" Then
                gAllowBruteForce = True
            End If
            If sArg = "--help" Or sArg = "/?" Or sArg = "-help" Or sArg = "-?" Or sArg = "-h" Then
                ShowHelpMessageBox()
            End If
            If sArg = "-unattended" Then
                gUnattendedUninstall = True
            End If
        Next

        oLicense.LicenseCodeLength = cLICENSE_KeyLength
        oLicense.PermitTestLicense = cLICENSE_PermitOfflineLicense

        'If gTestMode Then
        '    oLogger.LogFilePath = "C:\"
        '    MsgBox(Environment.GetFolderPath(Environment.SpecialFolder.Desktop))
        'End If
        oLogger.LogFilePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) & "\"
        oLogger.LogDebugMessages = gDebug
        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)
        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        'Panels
        Dim i As Integer
        For i = 0 To Panels.Length - 1
            Panels(i).Top = 83
            Panels(i).Left = 8

            Panels(i).Visible = False
        Next

        iApplicationInstallationCount = 0
        UpdateInstallationProgressCount()

        If gUnattendedUninstall Then
            iActiveGroupBox = GroupBoxes.Uninstallation - 1
        Else
            iActiveGroupBox = -1
        End If


        NextGroupBox()
    End Sub

    Private Sub ShowHelpMessageBox()
        Dim s As String = ""

        s &= "Command line parameters:" & vbCrLf & vbCrLf
        s &= "  --help" & vbCrLf
        s &= "  -help" & vbCrLf
        s &= "  -?" & vbCrLf
        s &= "  /?" & vbCrLf
        s &= "      This information, but you obviously already knew that" & vbCrLf & vbCrLf
        s &= "  --debug" & vbCrLf
        s &= "      Show debug information in log file" & vbCrLf & vbCrLf
        s &= "  --allow-brute-force" & vbCrLf
        s &= "      Allow the brute force removal of installed files, if normal uninstallation fails." & vbCrLf & vbCrLf

        MsgBox(s, MsgBoxStyle.OkOnly Or MsgBoxStyle.Information, Application.ProductName)

        Application.Exit()
    End Sub

#Region "Uninstallers"
    Private Sub StartUninstallation()
        InstallationController()
    End Sub

    Private Sub InstallationController()
        tmrInstallerWait.Enabled = False
        oProcess = New ProcessRunner

        If gTestMode Then
            If iApplicationInstallationCount > 0 Then
                iApplicationInstallationCount = InstallationProgress.Done
            End If
        End If

        oLogger.WriteToLog("Installation step: " & CStr(iApplicationInstallationCount), Logger.MESSAGE_TYPE.LOG_DEBUG, 0)
        UpdateInstallationProgressCount()

        Select Case iApplicationInstallationCount
            Case InstallationProgress.ValidateLicence
                CheckLicenseCode()
            Case InstallationProgress.StopServices
                _StopServices()
            Case InstallationProgress.UninstallSiteKiosk
                tmrInstallerWait.Enabled = True

                Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallSiteKiosk))
                Me.installThread.Start()
            Case InstallationProgress.UninstallSiteKioskBruteForce
                _UninstallSiteKioskBruteForce()
            Case InstallationProgress.UninstallAltiris
                tmrInstallerWait.Enabled = True

                Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallAltiris))
                Me.installThread.Start()
            Case InstallationProgress.ResetComputerAndWorkgroupName
                _ResetComputerAndWorkgroupName()
            Case InstallationProgress.CleanUpLocalUsers
                _CleanUpLocalUsers()
            Case InstallationProgress.CleanUpRegistry
                _CleanUpRegistry()
            Case InstallationProgress.CleanUpFiles
                _CleanUpFiles()
            Case InstallationProgress.UninstallLogMeIn
                tmrInstallerWait.Enabled = True

                Me.installThread = New Thread(New ThreadStart(AddressOf Me._UninstallLogMeIn))
                Me.installThread.Start()
            Case Else
                ProgressBarMarqueeStop()

                gBusyInstalling = False

                GoToGroupBox(GroupBoxes.Done)
        End Select
    End Sub

    Private Sub _UninstallSiteKiosk()
        oLogger.WriteToLog("Uninstalling SiteKiosk")

        bSiteKioskUninstallTimeout = False

        Dim sParams As String
        Dim sId As String

        oProcess = New ProcessRunner

        sId = ProductGUID.GetUninstallIdentifier("sitekiosk")

        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            bSiteKioskUninstallTimeout = True

            Exit Sub
        End If

        sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 1800", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = cUNINSTALL_COMMAND
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 1800
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _UninstallSiteKioskBruteForce()
        oLogger.WriteToLog("Uninstalling SiteKiosk - BRUTE FORCE")
        If gAllowBruteForce Then
            oLogger.WriteToLog("forced", , 1)
            bSiteKioskUninstallTimeout = True
        End If
        If Not bSiteKioskUninstallTimeout Then
            oLogger.WriteToLog("not needed", , 1)

            iApplicationInstallationCount += 1
            InstallationController()

            Exit Sub
        End If

        oLogger.WriteToLog("killing SiteKiosk processes", , 1)
        _KillProcess("Configure")
        _KillProcess("SiteKiosk")
        _KillProcess("skpcdsvc")
        _KillProcess("SmartCard")
        _KillProcess("SystemSecurity")
        _KillProcess("Watchdog")
        _KillProcess("SessionMonitor")
        _KillProcess("SiteRemoteClientService")
        _KillProcess("SKScreenshot")
        _KillProcess("msiexec")

        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK_1)
        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK_2)
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu) & "\Programs\SiteKiosk")

        __CleanUpLocalUser("sitekiosk")
        __CleanUpFolder("C:\Documents and Settings\sitekiosk", True)

        Dim rk_SERVICES As Microsoft.Win32.RegistryKey

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services", , 1)
            rk_SERVICES = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\CurrentControlSet\Services", True)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)

            ErrorDetected()
            Exit Sub
        End Try

        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\skpcdsvc
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteRemote Client

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\skpcdsvc", , 1)
            rk_SERVICES.DeleteSubKeyTree("skpcdsvc")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        'Oops, the keyboard no longer responds if we delete this key. So let's not do that then
        'Try
        '   oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey", , 1)
        '   rk_SERVICES.DeleteSubKeyTree("SiteKey")
        '   oLogger.WriteToLog("ok", , 2)
        'Catch ex As Exception
        '   oLogger.WriteToLog("fail", , 2)
        'End Try
        'We need to set the BlockKeyDuringStartup to 0 though
        'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters\BlockDuringStartup
        Try
            oLogger.WriteToLog("setting HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters\BlockDuringStartup to 0", , 1)
            Microsoft.Win32.Registry.SetValue("HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteKey\Parameters", "BlockDuringStartup", 0)
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SiteRemote Client", , 1)
            rk_SERVICES.DeleteSubKeyTree("SiteRemote Client")
            oLogger.WriteToLog("ok", , 2)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 2)
        End Try

        'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{ADE43478-C0B2-422D-B111-4DF94171B6C3}
        oLogger.WriteToLog("removing SiteKiosk from 'Add/Remove software' list", , 1)
        Dim sId As String
        Dim rk_UNINSTALL As Microsoft.Win32.RegistryKey

        sId = ProductGUID.GetUninstallIdentifier("sitekiosk")

        oLogger.WriteToLog("step 1 of 2", , 2)
        If sId <> "" Then
            Try
                oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", , 3)
                rk_UNINSTALL = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall", True)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)

                ErrorDetected()
                Exit Sub
            End Try

            Try
                oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{" & sId & "}", , 3)
                rk_UNINSTALL.DeleteSubKeyTree("{" & sId & "}")
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)
            End Try
        Else
            oLogger.WriteToLog("not found, so skipped", , 3)
        End If


        sId = ProductGUID.GetProductIdentifier("sitekiosk")
        oLogger.WriteToLog("step 2 of 2", , 2)
        If sId <> "" Then
            Try
                oLogger.WriteToLog("opening CLASSES_ROOT\Installer\Products", , 3)
                rk_UNINSTALL = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey("Installer\Products", True)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)

                ErrorDetected()
                Exit Sub
            End Try

            Try
                oLogger.WriteToLog("removing CLASSES_ROOT\Installer\Products\" & sId, , 3)
                rk_UNINSTALL.DeleteSubKeyTree(sId)
                oLogger.WriteToLog("ok", , 4)
            Catch ex As Exception
                oLogger.WriteToLog("fail", , 4)
            End Try
        Else
            oLogger.WriteToLog("not found, so skipped", , 3)
        End If


        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub _UninstallTaskbarAutohide()

    End Sub

    Private Sub _UninstallAltiris()
        oLogger.WriteToLog("Uninstalling remote administration application")

        If Not System.IO.File.Exists(cUNINSTALL_ALTIRIS_COMMAND) Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            Exit Sub
        End If

        oLogger.WriteToLog("Path   : " & cUNINSTALL_ALTIRIS_COMMAND, , 1)
        oLogger.WriteToLog("Params : " & cUNINSTALL_ALTIRIS_PARAMS, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = cUNINSTALL_ALTIRIS_COMMAND
        oProcess.Arguments = cUNINSTALL_ALTIRIS_PARAMS
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _UninstallLogMeIn()
        oLogger.WriteToLog("Uninstalling remote access application")

        Dim sParams As String
        Dim sId As String

        oProcess = New ProcessRunner

        sId = ProductGUID.GetUninstallIdentifier("logmein")

        If sId = "" Then
            oLogger.WriteToLog("Not found", Logger.MESSAGE_TYPE.LOG_WARNING, 1)

            oProcess.ProcessDone()

            Exit Sub
        End If

        sParams = cUNINSTALL_PARAMS.Replace("%%APP_IDENTIFIER%%", sId)

        oLogger.WriteToLog("Path   : " & cUNINSTALL_COMMAND, , 1)
        oLogger.WriteToLog("Params : " & sParams, , 1)
        oLogger.WriteToLog("Timeout: 300", , 1)

        oProcess = New ProcessRunner
        oProcess.FileName = cUNINSTALL_COMMAND
        oProcess.Arguments = sParams
        oProcess.MaxTimeout = 300
        oProcess.ProcessStyle = ProcessWindowStyle.Hidden
        oProcess.StartProcess()
    End Sub

    Private Sub _ResetComputerAndWorkgroupName()
        oLogger.WriteToLog("Resetting computer and workgroup name")

        Dim sComputerName As String = "", sWorkgroupName As String = ""

        oLogger.WriteToLog("currently", , 1)
        oLogger.WriteToLog("computer : " & oComputerName.ComputerName, , 2)
        oLogger.WriteToLog("workgroup: " & oComputerName.Workgroup, , 2)


        oLogger.WriteToLog("retrieving backup from registry", , 1)

        oLogger.WriteToLog("computer", , 2)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\" & cREGKEY_LPCSOFTWARE & "\Backup\Computer Name", , 3)
        Try
            sComputerName = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & cREGKEY_LPCSOFTWARE & "\Backup", "Computer Name", "")
        Catch ex As Exception
            sComputerName = ""
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            ErrorDetected()
        End Try
        oLogger.WriteToLog("value: " & sComputerName, , 3)

        oLogger.WriteToLog("workgroup", , 2)
        oLogger.WriteToLog("key  : HKEY_LOCAL_MACHINE\" & cREGKEY_LPCSOFTWARE & "\Backup\Workgroup Name", , 3)
        Try
            sWorkgroupName = Microsoft.Win32.Registry.GetValue("HKEY_LOCAL_MACHINE\" & cREGKEY_LPCSOFTWARE & "\Backup", "Workgroup Name", "")
        Catch ex As Exception
            sWorkgroupName = ""
            oLogger.WriteToLog("ERROR", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("ex.Message=" & ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("ex.Source =" & ex.Source, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

            ErrorDetected()
        End Try
        oLogger.WriteToLog("value: " & sWorkgroupName, , 3)


        oLogger.WriteToLog("updating", , 1)
        oLogger.WriteToLog("computer", , 2)
        If sComputerName <> "" Then
            oComputerName.ComputerName = sComputerName
            oLogger.WriteToLog("ok", , 3)

            oComputerName.Reload()

            If oComputerName.ComputerName = sComputerName Then
                oLogger.WriteToLog("verified", , 4)
            Else
                oLogger.WriteToLog("not verified (needs a reboot)", , 4)
            End If
        Else
            oLogger.WriteToLog("skipped", , 3)
        End If

        oLogger.WriteToLog("workgroup", , 2)
        If sWorkgroupName <> "" Then
            oComputerName.Workgroup = sWorkgroupName
            oLogger.WriteToLog("ok", , 3)

            oComputerName.Reload()

            If oComputerName.Workgroup = sWorkgroupName Then
                oLogger.WriteToLog("verified", , 4)
            Else
                oLogger.WriteToLog("not verified", , 4)
            End If
        Else
            oLogger.WriteToLog("skipped", , 3)
        End If


        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub _StopServices()
        oLogger.WriteToLog("Removing services")

        oLogger.WriteToLog("Killing LobbyPCWatchdog processes", , 1)
        __KillLobbyPCWatchDogProcesses()

        Dim oService As WindowsService
        Dim sStatus As String

        oLogger.WriteToLog("LobbyPCAgent", , 1)
        oService = New WindowsService
        oService.ServiceName = "LobbyPCAgent"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)

        oLogger.WriteToLog("LobbyPCWatchdogService", , 1)
        oService = New WindowsService
        oService.ServiceName = "LobbyPCWatchdogService"

        sStatus = oService.UninstallService
        oLogger.WriteToLog(sStatus, , 2)

        oLogger.WriteToLog("Killing any left-over LobbyPCWatchdog processes", , 1)
        __KillLobbyPCWatchDogProcesses()

        oLogger.WriteToLog("TaskbarAutohide", , 1)
        _KillProcess("TaskbarAutohide")

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __KillLobbyPCWatchDogProcesses()
        _KillProcess("LobbyPCWatchDogService")
        _KillProcess("LobbyPCWatchDogController")
        _KillProcess("LobbyPCWatchDogWarning")
        _KillProcess("LobbyPCWatchDogTrigger")
    End Sub

    Private Sub _KillProcess(ByVal sProcessName As String)
        Dim p As System.Diagnostics.Process

        oLogger.WriteToLog(sProcessName, , 2)
        For Each p In System.Diagnostics.Process.GetProcessesByName(sProcessName)
            Try
                p.Kill()
                oLogger.WriteToLog("dead", , 3)
            Catch ex As Exception
                oLogger.WriteToLog("zombie", , 3)
            End Try
        Next
    End Sub

    Private Sub _CleanUpFiles()
        oLogger.WriteToLog("Removing folders and files")

        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK_1)
        __CleanUpFolder(cCLEANFOLDER_SITEKIOSK_2)
        __CleanUpFolder(cCLEANFOLDER_ALTIRIS_1)
        __CleanUpFolder(cCLEANFOLDER_ALTIRIS_2)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_1)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_2)
        __CleanUpFolder(cCLEANFOLDER_iBAHN_3)
        __CleanUpFolder(cCLEANFOLDER_SERIELL)
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonPrograms) & "\Altiris")
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonProgramFiles) & "\Provisio")
        __CleanUpFolder(GetSpecialFolderPath(enuCSIDLPhysical.CommonProgramFiles) & "\Altiris")

        __CleanUpFile("C:\Windows\explorer_.exe")
        __CleanUpFile("C:\Documents and Settings\All Users\Application Data\Microsoft\User Account Pictures\iBAHN.bmp")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory) & "\Start LobbyPC.lnk")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonStartMenu) & "\Start LobbyPC.lnk")
        __CleanUpFile(GetSpecialFolderPath(enuCSIDLPhysical.CommonDesktopDirectory) & "\LobbyPC - Set Hotel Information Url.lnk")
        __CleanUpFile("c:\lobbypc.ver.txt.txt")

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __CleanUpFolder(ByVal sFolder As String, Optional ByVal bIgnoreError As Boolean = False)
        oLogger.WriteToLog("removing: " & sFolder, , 1)
        If System.IO.Directory.Exists(sFolder) Then
            Try
                __ClearAttributes(sFolder)
                System.IO.Directory.Delete(sFolder, True)
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                'If Not bIgnoreError Then ErrorDetected()
            End Try
            If System.IO.Directory.Exists(sFolder) Then
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                oLogger.WriteToLog("marking for delete on reboot", , 2)
                If DeleteFileOnReboot(sFolder) Then
                    oLogger.WriteToLog("ok", , 3)
                Else
                    oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                    'If Not bIgnoreError Then ErrorDetected()
                End If
            Else
                oLogger.WriteToLog("ok", , 2)
            End If
        Else
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub __CleanUpFile(ByVal sFile As String)
        oLogger.WriteToLog("removing: " & sFile, , 1)
        If System.IO.File.Exists(sFile) Then
            Try
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
                System.IO.File.Delete(sFile)
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 4)

                'ErrorDetected()
            End Try
            If System.IO.File.Exists(sFile) Then
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 2)

                oLogger.WriteToLog("marking for delete on reboot", , 2)
                If DeleteFileOnReboot(sFile) Then
                    oLogger.WriteToLog("ok", , 3)
                Else
                    oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

                    ErrorDetected()
                End If
            Else
                oLogger.WriteToLog("ok", , 2)
            End If
        Else
            oLogger.WriteToLog("not found", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub __ClearAttributes(ByVal sCurrentFolder As String)
        If System.IO.Directory.Exists(sCurrentFolder) Then
            Dim sSubFolders() As String = System.IO.Directory.GetDirectories(sCurrentFolder)
            Dim sSubFolder As String

            For Each sSubFolder In sSubFolders
                System.IO.File.SetAttributes(sSubFolder, IO.FileAttributes.Normal)
                __ClearAttributes(sSubFolder)
            Next

            Dim sFiles() As String = System.IO.Directory.GetFiles(sCurrentFolder)
            Dim sFile As String

            For Each sFile In sFiles
                System.IO.File.SetAttributes(sFile, IO.FileAttributes.Normal)
            Next
        End If
    End Sub

    Private Sub _CleanUpLocalUsers()
        oLogger.WriteToLog("Removing local user accounts")

        __CleanUpLocalUser("__LobbyPC_puppy__")
        __CleanUpFolder("C:\Documents and Settings\__LobbyPC_puppy__", True)
        __CleanUpFolder("C:\Users\__LobbyPC_puppy__", True)

        __CleanUpLocalUser("iBAHN")
        __CleanUpFolder("C:\Documents and Settings\iBAHN", True)
        __CleanUpFolder("C:\Users\iBAHN", True)

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub

    Private Sub __CleanUpLocalUser(ByVal sUserName As String)
        oLogger.WriteToLog("removing user: " & sUserName, , 1)

        If WindowsUser.RemoveUser(sUserName) Then
            oLogger.WriteToLog("ok", , 2)
        Else
            oLogger.WriteToLog("fail (does not exist?)", Logger.MESSAGE_TYPE.LOG_WARNING, 2)
        End If
    End Sub

    Private Sub _CleanUpRegistry()
        oLogger.WriteToLog("Cleaning registry")

        oLogger.WriteToLog("registry rollback", , 1)
        oLogger.WriteToLog("searching for rollback file", , 2)
        If IO.File.Exists(cPATHS_iBAHNProgramFilesFolder & "\LobbyPCSoftwareOnlyInstallation.registry.backup.xml") Then
            oLogger.WriteToLog("ok", , 3)

            Try
                Dim mXmlDoc As New Xml.XmlDocument
                Dim mXmlNodeRoot As Xml.XmlNode
                Dim mXmlNodelistKeys As Xml.XmlNodeList

                Dim mXmlNodeName As Xml.XmlNode, mXmlNodeValueName As Xml.XmlNode
                Dim mXmlNodeValue As Xml.XmlNode, mXmlNodeType As Xml.XmlNode

                oLogger.WriteToLog("opening", , 2)

                mXmlDoc.Load(cPATHS_iBAHNProgramFilesFolder & "\LobbyPCSoftwareOnlyInstallation.registry.backup.xml")
                oLogger.WriteToLog("ok", , 3)

                mXmlNodeRoot = mXmlDoc.SelectSingleNode("LobbyPCSoftwareOnly")
                mXmlNodelistKeys = mXmlNodeRoot.SelectSingleNode("registry-backup").SelectNodes("key")

                '<key>
                '  <name>HKEY_LOCAL_MACHINE\SOFTWARE\iBAHN\LobbyPCSoftwareOnly\Config</name>
                '  <valuename>BlockedDrives</valuename>
                '  <old>
                '    <value>(does not exist)</value>
                '    <type>0</type>
                '  </old>
                '  <new>
                '    <value>C,</value>
                '    <type>0</type>
                '  </new>
                '</key>

                Dim rk As Microsoft.Win32.RegistryKey, sRegKey As String

                oLogger.WriteToLog("iterating", , 2)
                For Each mXmlNodeKey As Xml.XmlNode In mXmlNodelistKeys
                    mXmlNodeName = mXmlNodeKey.SelectSingleNode("name")
                    mXmlNodeValueName = mXmlNodeKey.SelectSingleNode("valuename")
                    mXmlNodeValue = mXmlNodeKey.SelectSingleNode("old").SelectSingleNode("value")
                    mXmlNodeType = mXmlNodeKey.SelectSingleNode("old").SelectSingleNode("type")

                    oLogger.WriteToLog("found", , 3)
                    oLogger.WriteToLog(mXmlNodeKey.SelectSingleNode("name").InnerText & "\" & mXmlNodeValueName.InnerText, , 4)

                    sRegKey = mXmlNodeName.InnerText

                    If mXmlNodeValue.InnerText = "(does not exist)" And mXmlNodeType.InnerText = "0" Then
                        oLogger.WriteToLog("deleting", , 3)
                        Try
                            If mXmlNodeName.InnerText.StartsWith("HKEY_LOCAL_MACHINE\") Then
                                sRegKey = mXmlNodeName.InnerText.Replace("HKEY_LOCAL_MACHINE\", "")

                                rk = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(sRegKey, True)
                            ElseIf mXmlNodeName.InnerText.StartsWith("HKEY_USERS\") Then
                                sRegKey = mXmlNodeName.InnerText.Replace("HKEY_USERS\", "")

                                rk = Microsoft.Win32.Registry.Users.OpenSubKey(sRegKey, True)
                            End If

                            rk.DeleteValue(mXmlNodeValueName.InnerText)
                            oLogger.WriteToLog("ok", , 3)
                        Catch ex As Exception
                            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                            oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                        End Try
                    Else
                        oLogger.WriteToLog("restoring", , 3)

                        Try
                            Microsoft.Win32.Registry.SetValue(mXmlNodeName.InnerText, mXmlNodeValueName.InnerText, mXmlNodeValue.InnerText, Integer.Parse(mXmlNodeType.InnerText))
                            oLogger.WriteToLog("ok", , 4)
                        Catch ex As Exception
                            oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                            oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
                            oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
                        End Try
                    End If
                Next
            Catch ex As Exception
                oLogger.WriteToLog("fail", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
                oLogger.WriteToLog("exception", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
                oLogger.WriteToLog(ex.Message, Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            End Try
        Else
            oLogger.WriteToLog("does not exist", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        End If


        oLogger.WriteToLog("some more cleaning", , 1)
        Dim rk_SOFTWARE As Microsoft.Win32.RegistryKey
        Dim rk_RUN As Microsoft.Win32.RegistryKey

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE", , 2)
            rk_SOFTWARE = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", True)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)

            ErrorDetected()
            Exit Sub
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_SITEKIOSK, , 2)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_SITEKIOSK)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_ALTIRIS, , 2)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_ALTIRIS)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\" & cCLEANREGISTRY_IBAHN, , 2)
            rk_SOFTWARE.DeleteSubKeyTree(cCLEANREGISTRY_IBAHN)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("opening LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run", , 2)
            rk_RUN = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)

            ErrorDetected()
            Exit Sub
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\PostSiteKioskUpdating", , 2)
            rk_RUN.DeleteValue("PostSiteKioskUpdating")
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\LobbyPCWatchdogTrigger", , 2)
            rk_RUN.DeleteValue("LobbyPCWatchdogTrigger")
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\TaskbarAutohide", , 2)
            rk_RUN.DeleteValue("TaskbarAutohide")
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        Try
            oLogger.WriteToLog("removing LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\SiteKiosk", , 2)
            rk_RUN.DeleteValue("SiteKiosk")
            oLogger.WriteToLog("ok", , 3)
        Catch ex As Exception
            oLogger.WriteToLog("fail", , 3)
        End Try

        iApplicationInstallationCount += 1
        InstallationController()
    End Sub
#End Region

#Region "DragWindow events"
    Private Const WM_NCLBUTTONDOWN As Integer = &HA1
    Private Const HTCAPTION As Integer = &H2


    Private Sub picboxLogo_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picboxLogo.MouseDown
        Me.picboxLogo.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub

    Private Sub lblTitle_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lblTitle.MouseDown
        Me.lblTitle.Capture = False
        Dim msg As Message = Message.Create(Me.Handle, WM_NCLBUTTONDOWN, New IntPtr(HTCAPTION), IntPtr.Zero)
        Me.WndProc(msg)
    End Sub
#End Region

#Region "Prerequisites"
    Private Sub CheckPrerequisites()
        Dim sError As String = ""

        oPrerequisites.CheckWindowsVersion(cPREREQUISITES_NeededWindowsVersion, False)
        oPrerequisites.CheckInternetConnection()
        oPrerequisites.IsUserAdmin()
        oPrerequisites.IsUseriBAHNUser()

        If oPrerequisites.CheckComplete Then
            If oPrerequisites.AllIsOk Then
                NextGroupBox()
            Else
                sError = ""
                oLogger.WriteToLog("Prerequisites error", Logger.MESSAGE_TYPE.LOG_ERROR)

                If Not oPrerequisites.InternetConnectivity Then
                    oLogger.WriteToLog("No internet connection", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= "No internet connection."
                End If
                If Not oPrerequisites.CorrectWindowsVersion Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If

                    If oPrerequisites.HigherWindowsVersionsAreAccepted Then
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " < " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= "Incorrect Windows version! %%NEEDED_WINDOWS_VERSION%% (or higher) is needed."
                    Else
                        oLogger.WriteToLog(oPrerequisites.WindowsVersion & " != " & oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion), Logger.MESSAGE_TYPE.LOG_ERROR, 1)

                        sError &= "Incorrect Windows version! %%NEEDED_WINDOWS_VERSION%% is needed."
                    End If

                    sError = sError.Replace("%%NEEDED_WINDOWS_VERSION%%", oPrerequisites.HumanReadableWindowsVersion(cPREREQUISITES_NeededWindowsVersion))
                    sError = sError.Replace("%%RUNNING_WINDOWS_VERSION%%", oPrerequisites.WindowsVersion)
                End If
                If Not oPrerequisites.UserIsAdmin Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("No admin privileges", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= "This uninstallation needs to run with Administrator privileges."
                End If
                If Not oPrerequisites.UserIsNotiBAHNUser Then
                    If Not sError.Equals("") Then
                        sError &= vbCrLf & vbCrLf
                    End If
                    oLogger.WriteToLog("Running as iBAHN user", Logger.MESSAGE_TYPE.LOG_ERROR, 1)
                    sError &= "You can't run the uninstaller as the iBAHN user. Please choose another administrator account."
                End If

                lblPrerequisitesError.Text = sError

                If gTestMode Then
                    UpdateButtons(3, 3, 0, "CANCEL", "NEXT", "RETRY")
                Else
                    UpdateButtons(3, 0, 0, "CANCEL", "NEXT", "RETRY")

                End If
            End If
        Else
            lblPrerequisitesError.Text = "Unknown error 31. Please contact the helpdesk."

            UpdateButtons(3, 0, 0, "CANCEL", "NEXT", "RETRY")
        End If
    End Sub
#End Region

#Region "Groupbox handling"
    Private Sub NextGroupBox()
        iActiveGroupBox += 1
        UpdateGroupBox()
    End Sub

    Private Sub PreviousGroupBox()
        iActiveGroupBox -= 1
        'Skip these group box if we are going BACK
        'Windows version and internet connection error dialog
        If iActiveGroupBox = GroupBoxes.StartScreen Then
            iActiveGroupBox = -1
        End If
        UpdateGroupBox()
    End Sub

    Private Sub GoToGroupBox(ByVal iGroupBox As GroupBoxes)
        iActiveGroupBox = iGroupBox
        UpdateGroupBox()
    End Sub

    Private Sub UpdateGroupBox()
        oLogger.WriteToLog("Groupbox / action: " & iActiveGroupBox.ToString, Logger.MESSAGE_TYPE.LOG_DEBUG)

        Select Case iActiveGroupBox
            Case GroupBoxes.Prerequisites
                UpdateButtons(0, 0, 0, "CANCEL", "NEXT", "RETRY")
                CheckPrerequisites()
            Case GroupBoxes.StartScreen
                UpdateButtons(3, 3, 0, "CANCEL", "NEXT", "RETRY")

                Me.ControlBox = True
            Case GroupBoxes.Uninstallation
                UpdateButtons(0, 0, 0, "CANCEL", "NEXT", "RETRY")

                gBusyInstalling = True

                Me.ControlBox = False

                ProgressBarMarqueeStart()
                StartUninstallation()
            Case GroupBoxes.LicenseKeyError
                UpdateButtons(3, 3, 3, "CANCEL", "IGNORE", "RETRY")

                Me.ControlBox = True
            Case GroupBoxes.Done
                lblDone.Text = "Finished" & vbCrLf & vbCrLf
                If iSeriousErrorCount > 0 Then
                    lblDone.Text &= "Not all items could be deleted." & vbCrLf
                    lblDone.Text &= "Please reboot the pc, and run the uninstaller again."
                Else
                    lblDone.Text &= "Please reboot the pc to complete the uninstallation."
                End If

                oLogger.WriteToLog("Serious errors detected: " & iSeriousErrorCount.ToString, , 0)

                Me.ControlBox = True

                If gUnattendedUninstall Then
                    ApplicationExit(0)
                End If

                UpdateButtons(0, 3, 0, "EXIT", "EXIT", "RETRY")
            Case Else
                Application.Exit()
        End Select

        If iActiveGroupBox >= 0 And iActiveGroupBox < Panels.Length Then
            Panels(iActiveGroupBox).Visible = True
        End If

        Dim i As Integer
        For i = 0 To Panels.Length - 1
            If i <> iActiveGroupBox Then
                Panels(i).Visible = False
            End If
        Next
    End Sub
#End Region

#Region "License code validation"
    Private Sub CheckLicenseCode()
        oLogger.WriteToLog("Validating license code")

        oLicense.LicenseCode = gLicenseKeyFromRegistry

        oLogger.WriteToLog("license code: " & oLicense.LicenseCode, , 1)

        oLicense.CheckLicense()
    End Sub
#End Region

#Region "Button Handling"
    Private Sub UpdateButtons(ByVal iBack As Integer, ByVal iNext As Integer, ByVal iRetry As Integer, Optional ByVal sBack As String = "", Optional ByVal sNext As String = "", Optional ByVal sRetry As String = "")
        btnCancel.Enabled = iBack And 1
        btnCancel.Visible = iBack And 2
        If Not sBack.Equals("") Then
            btnCancel.Text = sBack
        End If

        btnNext.Enabled = iNext And 1
        btnNext.Visible = iNext And 2
        If Not sNext.Equals("") Then
            btnNext.Text = sNext
        End If

        btnRetry.Enabled = iRetry And 1
        btnRetry.Visible = iRetry And 2
        If Not sRetry.Equals("") Then
            btnRetry.Text = sRetry
        End If
    End Sub
#End Region

#Region "License Events"
    Private Sub onLicenseIsInvalid(ByVal ReturnCode As Integer, ByVal ReturnMessage As String) Handles oLicense.LicenseIsInvalid
        Dim sError As String = ""

        oLogger.WriteToLog("invalid", Logger.MESSAGE_TYPE.LOG_ERROR, 2)
        oLogger.WriteToLog(ReturnCode, Logger.MESSAGE_TYPE.LOG_ERROR, 3)
        oLogger.WriteToLog(ReturnMessage, Logger.MESSAGE_TYPE.LOG_ERROR, 3)

        sError = "License validation error:" & vbCrLf & vbCrLf
        Select Case ReturnCode
            Case 1
                sError &= "Invalid license key"
            Case 2
                sError &= "License key is valid but never used"
            Case 3
                sError &= "This license key was not the one used for this installation"
            Case -1
                sError &= "Could not connect to server"
            Case -2
                sError &= "Invalid server response"
            Case Else
                sError &= "Unknown error"
        End Select

        If gUnattendedUninstall Then
            oLogger.WriteToLog("Can't be bothered", Logger.MESSAGE_TYPE.LOG_DEFAULT, 3)

            iApplicationInstallationCount += 1
            InstallationController()
        Else
            lblLicenseKeyError.Text = sError
            GoToGroupBox(GroupBoxes.LicenseKeyError)
        End If
    End Sub

    Private Sub onLicenseValid() Handles oLicense.LicenseIsValid
        oLogger.WriteToLog("valid", , 2)

        'Go to next step
        iApplicationInstallationCount += 1
        InstallationController()
    End Sub
#End Region

#Region "Progress bars"
    Private Sub ProgressBarMarqueeStart()
        progressMarquee.Style = ProgressBarStyle.Marquee
        progressMarquee.MarqueeAnimationSpeed = 100
        progressMarquee.Value = 0
    End Sub

    Private Sub ProgressBarMarqueeStop()
        progressMarquee.Style = ProgressBarStyle.Blocks
        progressMarquee.MarqueeAnimationSpeed = 0
        progressMarquee.Value = 0
    End Sub
#End Region

#Region "Application cancel and exit"
    Private Sub ApplicationExit(Optional ByVal iExitCode As Integer = 0)
        oLogger.WriteToLog("exiting...")
        oLogger.WriteToLog("exit code: " & iExitCode.ToString, , 1)
        oLogger.WriteToLog(New String("=", 50))

        Environment.ExitCode = iExitCode
        Application.Exit()
    End Sub
#End Region

    Private Sub tmrLicenseValidationDelay_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrLicenseValidationDelay.Tick
        tmrLicenseValidationDelay.Enabled = False

        CheckLicenseCode()
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If iActiveGroupBox = GroupBoxes.LicenseKeyError Then
            iApplicationInstallationCount = InstallationProgress.ValidateLicence + 1
            GoToGroupBox(GroupBoxes.Uninstallation)
        Else
            NextGroupBox()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ApplicationExit()
    End Sub

    Private Sub tmrInstallerWait_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrInstallerWait.Tick
        If oProcess.IsProcessDone Then
            tmrInstallerWait.Enabled = False

            iApplicationInstallationCount += 1
            InstallationController()
        End If
    End Sub

#Region "Process Events"
    Private Sub oProcess_Done(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Done
        oLogger.WriteToLog("done", , 1)
        oLogger.WriteToLog("time elapsed: " & TimeElapsed.ToString, , 2)
        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Failed(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double, ByVal ErrorMessage As String) Handles oProcess.Failed
        oLogger.WriteToLog("failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)

        If iApplicationInstallationCount = InstallationProgress.UninstallSiteKiosk Then
            bSiteKioskUninstallTimeout = True
        Else
            ErrorDetected()
        End If

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_KillFailed(ByVal EventProcess As System.Diagnostics.Process, ByVal ErrorMessage As String) Handles oProcess.KillFailed
        oLogger.WriteToLog("killing process failed", , 1)
        oLogger.WriteToLog(ErrorMessage, , 2)

        oProcess.ProcessDone()
    End Sub

    Private Sub oProcess_Started(ByVal EventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.Started
        oLogger.WriteToLog("started", , 1)
        oLogger.WriteToLog("id  : " & EventProcess.Id.ToString, , 2)
        oLogger.WriteToLog("name: " & EventProcess.ProcessName, , 2)
    End Sub

    Private Sub oProcess_TimeOut(ByVal eventProcess As System.Diagnostics.Process, ByVal TimeElapsed As Double) Handles oProcess.TimeOut
        oLogger.WriteToLog("timed out", , 1)

        If iApplicationInstallationCount = InstallationProgress.UninstallSiteKiosk Then
            bSiteKioskUninstallTimeout = True
        Else
            ErrorDetected()
        End If

        oProcess.ProcessDone()
    End Sub
#End Region

    Private Sub UpdateInstallationProgressCount()
        lblProgress.Text = (iApplicationInstallationCount + 1).ToString & " / " & (iApplicationInstallationCountMax).ToString
        lblProgress.Visible = True
    End Sub

    Private Sub ErrorDetected()
        oLogger.WriteToLogRelative("Error detected", Logger.MESSAGE_TYPE.LOG_WARNING, 0)
        iSeriousErrorCount += 1
    End Sub

    Private Sub btnRetry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetry.Click
        iApplicationInstallationCount = 0
        InstallationController()
    End Sub
End Class
