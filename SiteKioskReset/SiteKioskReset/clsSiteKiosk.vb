Imports SiteKioskRuntimeLib
Imports System

Public Class clsSiteKiosk
    Private Declare Function CoGetClassObject Lib "ole32.dll" (ByRef rclsid As Guid, ByVal dwClsContext As Integer, ByVal pServerInfo As IntPtr, ByRef riid As Guid, ByRef ppv As IntPtr) As Integer

    Private mInterface As ISiteKiosk
    Private mDetected As Boolean
    Private mLastError As String

    Public Sub New()
        mDetected = GetSiteKioskInterface()
    End Sub

    Public ReadOnly Property Detected() As Boolean
        Get
            Return mDetected
        End Get
    End Property

    Public ReadOnly Property LastError() As String
        Get
            Return mLastError
        End Get
    End Property

    Public Function GetVersion() As String
        If Not mDetected Then
            Return String.Empty
        End If

        Return CType(mInterface.Version(), ISKVersion).VersionString
    End Function

    Public Function IsScreenSaverActive() As Boolean
        If Not mDetected Then
            Return False
        End If

        Return CType(mInterface.ScreenSaver, IScreenSaver).Active
    End Function

    Public Sub Restart()
        If Not mDetected Then
            Exit Sub
        End If

        mInterface.Reset()
    End Sub

    Public Sub Redetect()
        mDetected = GetSiteKioskInterface()
    End Sub

    Private Function GetSiteKioskInterface() As Boolean
        Dim IID_ISiteKioskFactory As Guid
        Dim CLSID_SiteKioskFactory As Guid
        Dim IID_ISiteKiosk As Guid

        IID_ISiteKioskFactory = GetType(SiteKioskFactory).GUID()
        CLSID_SiteKioskFactory = GetType(SiteKioskFactoryClass).GUID()
        IID_ISiteKiosk = GetType(SiteKiosk).GUID()

        Dim li_Unk_ As IntPtr = New IntPtr
        Dim li_Hr As Integer = CoGetClassObject(CLSID_SiteKioskFactory, 4, New IntPtr, IID_ISiteKioskFactory, li_Unk_)

        If li_Hr <> 0 Then
            mLastError = "could not get interface of SiteKioskFactory"
            Return False
        End If

        Dim lr_SiteKioskFactory_ As SiteKioskFactory = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(li_Unk_)
        Dim li_ISiteKioskUnknown_ As IntPtr

        lr_SiteKioskFactory_.CreateSiteKiosk(IID_ISiteKiosk, li_ISiteKioskUnknown_)
        If (li_ISiteKioskUnknown_.Equals(IntPtr.Zero)) Then
            mLastError = "could not get interface on the running instance of SiteKiosk"
            Return False
        End If

        mInterface = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(li_ISiteKioskUnknown_)
        mLastError = String.Empty

        Return True
    End Function
End Class
