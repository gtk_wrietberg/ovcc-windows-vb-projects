Public Class Form1

    Private oSiteKiosk As clsSiteKiosk
    Private oImpersonate As clsImpersonate

    Private lCount As Long

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        MsgBox("SiteKiosk version: " & oSiteKiosk.GetVersion)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        oSiteKiosk.Restart()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        oSiteKiosk = New clsSiteKiosk
        oImpersonate = New clsImpersonate
    End Sub

    Private Sub tmrScreensaver_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrCheckSiteKiosk.Tick
        lCount = lCount + 1
        lblCount.Text = String.Concat(New String("0", 10 - lCount.ToString.Length), lCount.ToString)

        If Not oSiteKiosk.Detected Then
            lblSKFound.Text = "No"
            oSiteKiosk.Redetect()
        Else
            lblSKFound.Text = "Yes"
        End If
        If oSiteKiosk.IsScreenSaverActive Then
            lblScreensaver.Text = "Active"
        Else
            lblScreensaver.Text = "off"
        End If

        lblLastError.Text = oSiteKiosk.LastError
    End Sub

    Private Sub tmrUsername_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrUsername.Tick
        lblUsername.Text = oImpersonate.CurrentIdentity
    End Sub

    Private Sub chkImpersonate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkImpersonate.CheckedChanged
        If chkImpersonate.Checked Then
            oImpersonate.DomainName = String.Empty
            oImpersonate.UserName = txtUsername.Text
            oImpersonate.PassWord = txtPassword.Text

            If Not oImpersonate.StartImpersonation() Then
                MsgBox("Fail! " & oImpersonate.LastError)
                chkImpersonate.Checked = False
            Else
                oSiteKiosk = New clsSiteKiosk
            End If
        Else
            If Not oImpersonate.StopImpersonation Then
                MsgBox("Fail! " & oImpersonate.LastError)
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        oSiteKiosk.Redetect()
    End Sub
End Class
