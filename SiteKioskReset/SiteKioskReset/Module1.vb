Imports SiteKioskRuntimeLib
Imports System



Module Module1
    Declare Function CoGetClassObject Lib "ole32.dll" (ByRef rclsid As Guid, ByVal dwClsContext As Integer, ByVal pServerInfo As IntPtr, ByRef riid As Guid, ByRef ppv As IntPtr) As Integer

    Public Sub ShowSiteKioskVersion()
        Dim s As String

        s = "HOER"

        If GetSiteKioskVersion(s) Then
            MsgBox(s)
        Else
            MsgBox("Sitekiosk is not running!")
        End If
    End Sub

    Private Function GetSiteKioskVersion(ByRef as_Version) As Boolean

        REM returns false, then SiteKiosk is not available
        REM otherwise, True is returned and as_Version will contain
        REM the current Siteiosk version

        Dim IID_ISiteKioskFactory As Guid
        Dim CLSID_SiteKioskFactory As Guid
        Dim IID_ISiteKiosk As Guid

        REM retrieve the needed interface-id's and class-id's

        IID_ISiteKioskFactory = GetType(SiteKioskFactory).GUID()
        CLSID_SiteKioskFactory = GetType(SiteKioskFactoryClass).GUID()
        IID_ISiteKiosk = GetType(SiteKiosk).GUID()

        REM get interface of SiteKioskFactory

        Dim li_Unk_ As IntPtr = New IntPtr
        Dim li_Hr As Integer = CoGetClassObject(CLSID_SiteKioskFactory, 4, New IntPtr, IID_ISiteKioskFactory, li_Unk_)

        If li_Hr <> 0 Then
            REM sitekiosk is not running
            Return False
        End If

        REM convert the IntPtr to the requested interface

        Dim lr_SiteKioskFactory_ As SiteKioskFactory = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(li_Unk_)

        Dim li_ISiteKioskUnknown_ As IntPtr

        REM get an interface on the running instance of SiteKiosk

        lr_SiteKioskFactory_.CreateSiteKiosk(IID_ISiteKiosk, li_ISiteKioskUnknown_)
        If (li_ISiteKioskUnknown_.Equals(IntPtr.Zero)) Then
            REM SiteKiosk was not running and could not be launched
            Return False
        End If

        REM convert the returned IntPtr to the requested interface

        Dim lr_ISiteKiosk_ As ISiteKiosk = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(li_ISiteKioskUnknown_)

        REM read out the version of SiteKiosk


        Dim lk_Version As ISKVersion = lr_ISiteKiosk_.Version()
        as_Version = lk_Version.VersionString

        Return True

    End Function

    Public Function ResetSiteKiosk() As Boolean

        REM returns false, then SiteKiosk is not available
        REM otherwise, True is returned and as_Version will contain
        REM the current Siteiosk version

        Dim IID_ISiteKioskFactory As Guid
        Dim CLSID_SiteKioskFactory As Guid
        Dim IID_ISiteKiosk As Guid

        REM retrieve the needed interface-id's and class-id's

        IID_ISiteKioskFactory = GetType(SiteKioskFactory).GUID()
        CLSID_SiteKioskFactory = GetType(SiteKioskFactoryClass).GUID()
        IID_ISiteKiosk = GetType(SiteKiosk).GUID()

        REM get interface of SiteKioskFactory

        Dim li_Unk_ As IntPtr = New IntPtr
        Dim li_Hr As Integer = CoGetClassObject(CLSID_SiteKioskFactory, 4, New IntPtr, IID_ISiteKioskFactory, li_Unk_)

        If li_Hr <> 0 Then
            REM sitekiosk is not running
            Return False
        End If

        REM convert the IntPtr to the requested interface

        Dim lr_SiteKioskFactory_ As SiteKioskFactory = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(li_Unk_)

        Dim li_ISiteKioskUnknown_ As IntPtr

        REM get an interface on the running instance of SiteKiosk

        lr_SiteKioskFactory_.CreateSiteKiosk(IID_ISiteKiosk, li_ISiteKioskUnknown_)
        If (li_ISiteKioskUnknown_.Equals(IntPtr.Zero)) Then
            REM SiteKiosk was not running and could not be launched
            Return False
        End If

        REM convert the returned IntPtr to the requested interface

        Dim lr_ISiteKiosk_ As ISiteKiosk = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(li_ISiteKioskUnknown_)

        REM reset SiteKiosk
        lr_ISiteKiosk_.Reset()

        Return True

    End Function

    Public Function IsSiteKioskScreensaverRunning(ByRef bScreensaver As Boolean) As Boolean

        REM returns false, then SiteKiosk is not available
        REM otherwise, True is returned and as_Version will contain
        REM the current Siteiosk version

        Dim IID_ISiteKioskFactory As Guid
        Dim CLSID_SiteKioskFactory As Guid
        Dim IID_ISiteKiosk As Guid

        REM retrieve the needed interface-id's and class-id's

        IID_ISiteKioskFactory = GetType(SiteKioskFactory).GUID()
        CLSID_SiteKioskFactory = GetType(SiteKioskFactoryClass).GUID()
        IID_ISiteKiosk = GetType(SiteKiosk).GUID()

        REM get interface of SiteKioskFactory

        Dim li_Unk_ As IntPtr = New IntPtr
        Dim li_Hr As Integer = CoGetClassObject(CLSID_SiteKioskFactory, 4, New IntPtr, IID_ISiteKioskFactory, li_Unk_)

        If li_Hr <> 0 Then
            REM sitekiosk is not running
            Return False
        End If

        REM convert the IntPtr to the requested interface

        Dim lr_SiteKioskFactory_ As SiteKioskFactory = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(li_Unk_)

        Dim li_ISiteKioskUnknown_ As IntPtr

        REM get an interface on the running instance of SiteKiosk

        lr_SiteKioskFactory_.CreateSiteKiosk(IID_ISiteKiosk, li_ISiteKioskUnknown_)
        If (li_ISiteKioskUnknown_.Equals(IntPtr.Zero)) Then
            REM SiteKiosk was not running and could not be launched
            Return False
        End If

        REM convert the returned IntPtr to the requested interface

        Dim lr_ISiteKiosk_ As ISiteKiosk = System.Runtime.InteropServices.Marshal.GetObjectForIUnknown(li_ISiteKioskUnknown_)

        bScreensaver = CType(lr_ISiteKiosk_.ScreenSaver.Active(), Boolean)

        Return True

    End Function
End Module
