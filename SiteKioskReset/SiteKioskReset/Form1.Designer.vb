<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblScreensaver = New System.Windows.Forms.Label
        Me.tmrCheckSiteKiosk = New System.Windows.Forms.Timer(Me.components)
        Me.lblLastError = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.grpImpersonation = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblUsername = New System.Windows.Forms.Label
        Me.tmrUsername = New System.Windows.Forms.Timer(Me.components)
        Me.lblSKFound = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkImpersonate = New System.Windows.Forms.CheckBox
        Me.txtUsername = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.lblCount = New System.Windows.Forms.Label
        Me.Button3 = New System.Windows.Forms.Button
        Me.grpImpersonation.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(53, 90)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(127, 52)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "SK version" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(55, 152)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(127, 52)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "Reset SiteKiosk"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(74, 286)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Screensaver:"
        '
        'lblScreensaver
        '
        Me.lblScreensaver.Location = New System.Drawing.Point(177, 286)
        Me.lblScreensaver.Name = "lblScreensaver"
        Me.lblScreensaver.Size = New System.Drawing.Size(100, 13)
        Me.lblScreensaver.TabIndex = 3
        Me.lblScreensaver.Text = "checking"
        '
        'tmrCheckSiteKiosk
        '
        Me.tmrCheckSiteKiosk.Enabled = True
        Me.tmrCheckSiteKiosk.Interval = 500
        '
        'lblLastError
        '
        Me.lblLastError.Location = New System.Drawing.Point(177, 244)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(575, 13)
        Me.lblLastError.TabIndex = 5
        Me.lblLastError.Text = "checking"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(74, 244)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Last error:"
        '
        'grpImpersonation
        '
        Me.grpImpersonation.Controls.Add(Me.Label6)
        Me.grpImpersonation.Controls.Add(Me.txtPassword)
        Me.grpImpersonation.Controls.Add(Me.Label4)
        Me.grpImpersonation.Controls.Add(Me.txtUsername)
        Me.grpImpersonation.Controls.Add(Me.chkImpersonate)
        Me.grpImpersonation.Controls.Add(Me.lblUsername)
        Me.grpImpersonation.Controls.Add(Me.Label2)
        Me.grpImpersonation.Location = New System.Drawing.Point(356, 31)
        Me.grpImpersonation.Name = "grpImpersonation"
        Me.grpImpersonation.Size = New System.Drawing.Size(344, 173)
        Me.grpImpersonation.TabIndex = 6
        Me.grpImpersonation.TabStop = False
        Me.grpImpersonation.Text = "Impersonation"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Current user:"
        '
        'lblUsername
        '
        Me.lblUsername.Location = New System.Drawing.Point(93, 29)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(224, 13)
        Me.lblUsername.TabIndex = 1
        Me.lblUsername.Text = "checking"
        '
        'tmrUsername
        '
        Me.tmrUsername.Enabled = True
        Me.tmrUsername.Interval = 500
        '
        'lblSKFound
        '
        Me.lblSKFound.Location = New System.Drawing.Point(177, 218)
        Me.lblSKFound.Name = "lblSKFound"
        Me.lblSKFound.Size = New System.Drawing.Size(575, 13)
        Me.lblSKFound.TabIndex = 8
        Me.lblSKFound.Text = "checking"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(74, 218)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(97, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "SiteKiosk found:"
        '
        'chkImpersonate
        '
        Me.chkImpersonate.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkImpersonate.Location = New System.Drawing.Point(9, 121)
        Me.chkImpersonate.Name = "chkImpersonate"
        Me.chkImpersonate.Size = New System.Drawing.Size(312, 37)
        Me.chkImpersonate.TabIndex = 2
        Me.chkImpersonate.Text = "Impersonate SiteKiosk user"
        Me.chkImpersonate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkImpersonate.UseVisualStyleBackColor = True
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(96, 59)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(177, 21)
        Me.txtUsername.TabIndex = 3
        Me.txtUsername.Text = "sitekiosk"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 59)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(68, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Username:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 83)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Password:"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(96, 83)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(177, 21)
        Me.txtPassword.TabIndex = 5
        Me.txtPassword.Text = "provisio"
        '
        'lblCount
        '
        Me.lblCount.Location = New System.Drawing.Point(40, 367)
        Me.lblCount.Name = "lblCount"
        Me.lblCount.Size = New System.Drawing.Size(140, 19)
        Me.lblCount.TabIndex = 9
        Me.lblCount.Text = "000000"
        Me.lblCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(46, 17)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(135, 55)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "Redetect"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(788, 470)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.lblCount)
        Me.Controls.Add(Me.lblSKFound)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.grpImpersonation)
        Me.Controls.Add(Me.lblLastError)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblScreensaver)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TopMost = True
        Me.grpImpersonation.ResumeLayout(False)
        Me.grpImpersonation.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblScreensaver As System.Windows.Forms.Label
    Friend WithEvents tmrCheckSiteKiosk As System.Windows.Forms.Timer
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grpImpersonation As System.Windows.Forms.GroupBox
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tmrUsername As System.Windows.Forms.Timer
    Friend WithEvents lblSKFound As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents chkImpersonate As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents lblCount As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button

End Class
