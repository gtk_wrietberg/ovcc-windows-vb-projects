Imports System
Imports System.Runtime.InteropServices
Imports System.Security.Principal
Imports System.Security.Permissions
Imports Microsoft.VisualBasic
<Assembly: SecurityPermissionAttribute(SecurityAction.RequestMinimum, UnmanagedCode:=True), Assembly: PermissionSetAttribute(SecurityAction.RequestMinimum, Name:="FullTrust")> 

Public Class clsImpersonate
    Private Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As [String], ByVal lpszDomain As [String], ByVal lpszPassword As [String], ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Boolean
    <DllImport("kernel32.dll")> _
    Private Shared Function FormatMessage(ByVal dwFlags As Integer, ByRef lpSource As IntPtr, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByRef lpBuffer As [String], ByVal nSize As Integer, ByRef Arguments As IntPtr) As Integer
    End Function
    Private Declare Auto Function CloseHandle Lib "kernel32.dll" (ByVal handle As IntPtr) As Boolean
    Private Declare Auto Function DuplicateToken Lib "advapi32.dll" (ByVal ExistingTokenHandle As IntPtr, ByVal SECURITY_IMPERSONATION_LEVEL As Integer, ByRef DuplicateTokenHandle As IntPtr) As Boolean

    Private mLastError As String = String.Empty
    Private tokenHandle As New IntPtr(0)
    Private dupeTokenHandle As New IntPtr(0)
    Private impersonatedUser As WindowsImpersonationContext
    Private impersonationActive As Boolean

    Private mDomainName As String
    Private mUserName As String
    Private mPassWord As String

    Public Property DomainName() As String
        Get
            Return mDomainName
        End Get
        Set(ByVal value As String)
            mDomainName = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set
    End Property

    Public Property PassWord() As String
        Get
            Return mPassWord
        End Get
        Set(ByVal value As String)
            mPassWord = value
        End Set
    End Property

    Public ReadOnly Property LastError() As String
        Get
            Return mLastError
        End Get
    End Property

    Public ReadOnly Property CurrentIdentity() As String
        Get
            Return WindowsIdentity.GetCurrent().Name
        End Get
    End Property

    ' If you incorporate this code into a DLL, be sure to demand FullTrust.
    <PermissionSetAttribute(SecurityAction.Demand, Name:="FullTrust")> _
    Public Function StartImpersonation() As Boolean
        Dim returnValue As Boolean
        Dim iErrorCode As Integer

        Try
            Const LOGON32_PROVIDER_DEFAULT As Integer = 0
            'This parameter causes LogonUser to create a primary token.
            Const LOGON32_LOGON_INTERACTIVE As Integer = 2
            Const SecurityImpersonation As Integer = 2

            tokenHandle = IntPtr.Zero
            dupeTokenHandle = IntPtr.Zero

            ' Call LogonUser to obtain a handle to an access token.
            returnValue = LogonUser(mUserName, mDomainName, mPassWord, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, tokenHandle)

            If Not returnValue Then
                iErrorCode = Marshal.GetLastWin32Error()
                mLastError = "Error: [" & iErrorCode.ToString & "] " & GetErrorMessage(iErrorCode)

                Return False
            End If

            returnValue = DuplicateToken(tokenHandle, SecurityImpersonation, dupeTokenHandle)
            If Not returnValue Then
                CloseHandle(tokenHandle)
                mLastError = "Exception thrown in trying to duplicate token."
                Return False
            End If

            ' TThe token that is passed to the following constructor must 
            ' be a primary token in order to use it for impersonation.
            Dim newId As New WindowsIdentity(dupeTokenHandle)

            impersonatedUser = newId.Impersonate()

            impersonationActive = True

            Return True
        Catch ex As Exception
            mLastError = "Exception occurred (StartImpersonation). " + ex.Message

            Return False
        End Try
    End Function

    <PermissionSetAttribute(SecurityAction.Demand, Name:="FullTrust")> _
Public Function StopImpersonation() As Boolean
        If Not impersonationActive Then
            mLastError = "impersonation not active"
            Return False
        End If

        Try
            ' Stop impersonating the user.
            impersonatedUser.Undo()

            ' Free the tokens.
            If Not System.IntPtr.op_Equality(tokenHandle, IntPtr.Zero) Then
                CloseHandle(tokenHandle)
            End If
            If Not System.IntPtr.op_Equality(dupeTokenHandle, IntPtr.Zero) Then
                CloseHandle(dupeTokenHandle)
            End If

            impersonationActive = False

            Return True
        Catch ex As Exception
            mLastError = "Exception occurred (StopImpersonation). " + ex.Message

            Return False
        End Try
    End Function


    'GetErrorMessage formats and returns an error message
    'corresponding to the input errorCode.
    Private Shared Function GetErrorMessage(ByVal errorCode As Integer) As String
        Dim FORMAT_MESSAGE_ALLOCATE_BUFFER As Integer = &H100
        Dim FORMAT_MESSAGE_IGNORE_INSERTS As Integer = &H200
        Dim FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000

        Dim messageSize As Integer = 255
        Dim lpMsgBuf As String = String.Empty
        Dim dwFlags As Integer = FORMAT_MESSAGE_ALLOCATE_BUFFER Or FORMAT_MESSAGE_FROM_SYSTEM Or FORMAT_MESSAGE_IGNORE_INSERTS

        Dim ptrlpSource As IntPtr = IntPtr.Zero
        Dim prtArguments As IntPtr = IntPtr.Zero

        Dim retVal As Integer = FormatMessage(dwFlags, ptrlpSource, errorCode, 0, lpMsgBuf, messageSize, prtArguments)
        If 0 = retVal Then
            Throw New Exception("Failed to format message for error code " + errorCode.ToString() + ". ")
        End If

        Return lpMsgBuf
    End Function
End Class
