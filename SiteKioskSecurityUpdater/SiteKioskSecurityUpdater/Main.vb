Module Main
    ''' <summary>
    ''' Mains this instance.
    ''' </summary>
    Public Sub Main()
        oLogger = New Logger

        oLogger.WriteToLog(New String("*", 50))

        Dim myBuildInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        oLogger.WriteToLog(Application.ProductName & " v" & myBuildInfo.FileVersion)

        oLogger.WriteToLog("Set SiteKiosk User Registry Keys", , 0)
        SetSiteKioskUserRegistryKeys()
    End Sub

    ''' <summary>
    ''' Sets the site kiosk user registry keys.
    ''' </summary><returns></returns>
    Private Function SetSiteKioskUserRegistryKeys() As Boolean
        Dim oHive As RegistryHive

        oLogger.WriteToLog("updating registry for SiteKiosk user", , 1)


        oLogger.WriteToLog("initialising", , 2)
        oHive = New RegistryHive

        If oHive.Initialise Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

            Return False
        End If


        oLogger.WriteToLog("loading SiteKiosk hive", , 2)
        oLogger.WriteToLog(cSiteKioskRegistryHive, , 3)
        If Not IO.File.Exists(cSiteKioskRegistryHive) Then
            oLogger.WriteToLog("does not exist!", Logger.MESSAGE_TYPE.LOG_ERROR, 3)

            Return False
        End If

        If oHive.LoadHive(cSiteKioskRegistryHive) Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

            oHive.UnloadHive()

            Return False
        End If


        oLogger.WriteToLog("setting values", , 2)

        oLogger.WriteToLog("key  : " & cSiteKioskRegistry_Key_NoViewOnDrive, , 3)
        oLogger.WriteToLog("name : " & cSiteKioskRegistry_Name_NoViewOnDrive, , 3)
        oLogger.WriteToLog("value: " & cSiteKioskRegistry_Value_NoViewOnDrive.ToString, , 3)
        If oHive.SetValueInLoadedHive(cSiteKioskRegistry_Key_NoViewOnDrive, cSiteKioskRegistry_Name_NoViewOnDrive, cSiteKioskRegistry_Value_NoViewOnDrive) Then
            oLogger.WriteToLog("ok", , 4)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 5)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 6)
        End If


        oLogger.WriteToLog("unloading SiteKiosk hive", , 2)
        If oHive.UnloadHive() Then
            oLogger.WriteToLog("ok", , 3)
        Else
            oLogger.WriteToLog("failed", Logger.MESSAGE_TYPE.LOG_ERROR, 3)
            oLogger.WriteToLog("error message", Logger.MESSAGE_TYPE.LOG_ERROR, 4)
            oLogger.WriteToLog(oHive.LastError, Logger.MESSAGE_TYPE.LOG_ERROR, 5)

            Return False
        End If

        Return True
    End Function
End Module
